# Pentagon Functions++



`PentagonFunctions++` is a C++ library for the fast numerical evaluation of the pentagon functions,
a basis of multiple polylogarithms to express the Feynman integrals required for 5-particle scattering amplitudes up to 2-loop order.
Currently the full set of massless pentagon functions ([`m0`][1]) and 
the pentagon functions for one external mass (`m1`, [planar][2] and [full set][3]) are available.
The library is designed for immediate application in cross section calculations.

The functions of weight 1 and 2 are evaluated from their representation in terms of logarithms and dilogarithms.
The functions of weight 3 and 4 are evaluated by integrating numerically their representation in terms of one-fold integrals.

For details on the classification and the implementation of the pentagon functions see [[1], [2], [3]].
If you use `PentagonFunctions++` in your research, please cite the corresponding publication(s).

`PentagonFunctions++` uses [Li2pp] for the numerical evaluation of dilogarithms.

`PentagonFunctions++` supports the evaluation with quadruple and octuple precision using the `dd_real` and `qd_real` types of the [qd] library.



## Installation

First, if evaluations in quadruple and octuple precision are needed, install the [qd] library.
Make sure that the [qd] installation is visible in your terminal environment
(environment variables `$LD_LIBRARY_PATH` (or `$DYLD_LIBRARY_PATH` on OSX), `$PKG_CONFIG_PATH`).

If double-precision evaluations are sufficient, no other dependencies need to be installed manually ([Li2pp] is handled automatically).

`PentagonFunctions++` uses the [meson] build system with a [ninja] back-end.
Both can be obtained through the standard python package manager *pip*.

To build the library in the directory *build* and install it into *$HOME/local*, do

```
meson setup build -D prefix=$HOME/local
cd build
ninja install
```

The first line sets the library up in the default configuration,
which is given by the default values in the second column of the table below.

| Option            |  Default value   |  Possible values            |  Description                                                |
|:------------------|:-----------------|:----------------------------|:------------------------------------------------------------|
| examples          |  true            |  [true, false]              |  Enable building of examples                                |
| high-precision    |  false           |  [true, false]              |  Enable higher precision with [qd]                          |
| m0_set            |  enabled         |  [enabled, disabled, auto]  |  Enable building massless pentagon functions                |
| m1_set            |  enabled         |  [enabled, disabled, auto]  |  Enable building pentagon functions with one external mass  |
                                                                                                                                   
Note that the compilation of the library can take significant amount of time (more than 10 minutes) if all features are enabled.
To reduce the compilation time and the library size we recommend to disable building the sets of functions which are not needed.
To change the configuration provide additional arguments of the form 

```
-D <option>=<value>
``` 
to the `meson setup` command in the first step. 
The options can be changed after the initial setup by using `meson configure` instead of `meson setup` with the same arguments. 
`meson configure` only changes the values of options that are given to it as arguments, keeping the others unchanged.


To set a particular C++ compiler set the environment variable `CXX` to point to the compiler during the setup stage.
For example,
```
CXX=g++ meson setup build -D prefix=$HOME/local
```
It is sufficient to do this *only* at the setup stage.



### OSX support

OSX support is experimental and installation might require some extra effort.

Note the following:

1. Instead of `$LD_LIBRARY_PATH` `$DYLD_LIBRARY_PATH` must be used to set up the environment. 
This is standard on OSX and unrelated to this library.

2. `PentagonFunctions++` most likely will not compile with *Apple Clang*, which is the default compiler on OSX.
Is is recommended to install `gcc` (e.g. with `brew install gcc`), and follow the instructions in the previous sections on how to choose a compiler. 
Do note that `g++` on OSX for some inexplicable reason is just a symlink to `clang`. 
So one has to explicitly point to the actual `g++` compiler, e.g. by giving the full path or specifying the version.
`meson` shows the compiler version during setup, so one can check that it doesn't mention `clang` there.



### Static library

By default only a shared library is built. 
If you need a static library the configuration option `default_library` to `both` during setup.


### Uninstallation

To delete all installed files, run 
```
ninja uninstall
```
from the build directory.


### Tests


To verify the validity of the implementation and the installation, we implemented a number of automated tests.
We suggest running these tests after each installation.

To run the tests, go to the *build* directory and execute

```
ninja test
```




## Usage

The main way to use `PentagonFunctions++` is to write a C++ program which links to the shared or static library.
The library is shipped with a *pkg-config* file **PentagonFunctions.pc**,
so the recommended way is to use the *pkg-config* facilities of your favorite build system.
Make sure that the `PentagonFunctions++` installation is visible in your terminal environment
(environment variables `$LD_LIBRARY_PATH` (or `$DYLD_LIBRARY_PATH` on OSX), `$PKG_CONFIG_PATH`).

The main interface is provided by the class `FunID<KinType>`, which is declared in the header **PentagonFunctions/FunctionID.h**.
The enumeration `KinType` serves to identify a set of functions for particular kinematics.
A `FunID<KinType>` object, constructed with indices `{w,n,i}`, represents the pentagon function `F[w,n,i]` (see [[1],[2],[3]] for the definitions).
The index `i` is used only for some of the functions.
Function objects for numerical type `T` *(double, dd_real, qd_real)* can be obtained from each `FunID<KinType>` instance with the method `FunID<KinType>::get_evaluator<T>()`.
Once the function objects are constructed, they are callable, and can be invoked with any number of kinematical points in a row.
See the examples for details.

The functions of weight 3 and 4 are evaluated by numerical one-fold integration.
The termination condition for the numerical integration in type `T` can be controlled by modifying the global variable `IntegrationTolerance<T>`.

```cpp
template <typename T> extern T IntegrationTolerance;
```

The default value is chosen such that the integration error is not greater than the rounding error of the numerical type `T`.
Finer control over integration precision might be potentially exploited for further improving performance.
Note, however, that loss of precision might still happen in the evaluation of the integrands.


`PentagonFunctions++` can also be used through its *Mathematica* interface.


### Mathematica interface

`PentagonFunctions++` provides a *Mathematica* interface through a program which interacts with a *Mathematica* package with the same name.
The package is installed in the directory **prefix/Mathematica**, so the only thing one needs to do is to include this folder in the *Mathematica* search path.
For example, by putting this line in your **$UserBaseDirectory/Kernel/init.m** file (in Linux **~/.Mathematica/Kernel/init.m**),

```Mathematica
$Path = Prepend[$Path, FileNameJoin[{$HomeDirectory,"local","Mathematica"}]];
```

See the documentation in **PentagonFunctions.m** for usage details.


### Python interface

A python interface for `PentagonFunctions++` by Giuseppe de Laurentis is available [here](https://github.com/GDeLaurentis/py-pentagon-functions).

It can be installed from `pip` as
```
pip install pentagon-functions
```

For more details see the documentation and examples in the linked git repository.



## Important subtleties


### Parity-odd functions

A very important subtlety arises due to the precise choice of normalization of certain pure five-point integrals.
The latter may be normalized alternatively by the square root of the order-four Gram determinant $`\Delta_5 = G(p_1,p_2,p_3,p_4)`$ or by $`\mathrm{tr}_5 = 4 i \varepsilon(p_1,p_2,p_3,p_4)`$. 
The two choices are related by $`\Delta_5 = \mathrm{tr}_5^2`$,
but $`\mathrm{tr}_5`$ changes its sign under parity conjugation or odd permutations of external momenta, whereas  $`\sqrt{\Delta_5}`$ does not. 
For historical reasons $`\mathrm{tr}_5`$ was preferred in [[1],[2]], while $`\sqrt{\Delta_5}`$ was chosen in [[3]].

The pentagon functions are always evaluated in their analyticity region:
$`P_0`$ with $`\delta > 0`$ as defined in eq. (4.15) of [[1]]; 
$`\mathcal{P}_{45}^+`$ as defined in eq. (2.3) of [[2]].
They accept only the Mandelstam invariants as arguments. 
It is left to the user to change the signs of the parity-odd functions, if necessary.
The lists of parity-odd functions can be found in the corresponding publications.

In view of the fact that the normalization has been changed between [[2]] and [[3]], note the following. 
The planar subset of the `m1` functions from [[3]] matches exactly the functions from [[2]], that is they are numerically identical. 
However, *the expressions of the master integrals in terms of pentagon functions are different*. 
No extra considerations are needed to this end as long as these definitions are not mixed up within a single calculation.



### Regularization scale

It is assumed that the Mandelstam invariants in the input are normalized by a dimensional regularization scale, i.e. they are dimensionless, see eq. (3.1) from [[2]].
To ensure optimal numerical stability this scale should be chosen to make the Mandelstam invariants to be roughly of order 1.
Typical dynamical renormalization scales employed in cross section calculations should work. 



### Functions with integrable square-root singularities

Certain nonplanar one-mass pentagon functions contain integrable square-root singularities associated to special co-dimension one surfaces (see [[3]] for details).
In the current implementation the numerical stability of these functions is statistically worse than that of other pentagon functions, if all evaluations are performed in the same fixed precision.
`PentagonFunctions++` can mitigate this issue by locally switching to higher precision when necessary, at the cost of somewhat reduced performance. 
This behavior is enabled by default (if compiled with high-precision support),
and it can be disabled or enabled at run time by adjusting the value of the boolean variable `always_switch_to_HP_for_sqrt_divergences`.





## Examples

We demonstrate the functionalities of the library with several example programs, which can be found in the directory **examples**.



## Comments, concerns, questions?

Feedback is very welcome, please contact the developers [via email](mailto:vasily.sotnikov@physik.uzh.ch) or any of the authors of the supporting publications.



## Licensing

This work is licensed under **GNU GPLv3**.



[1]: https://arxiv.org/abs/2009.07803
[2]: https://arxiv.org/abs/2110.10111
[3]: https://arxiv.org/abs/2306.15431
[meson]: https://mesonbuild.com/
[ninja]: https://ninja-build.org/
[Li2pp]: https://gitlab.com/VasilySotnikov/Li2pp
[qd]: https://www.davidhbailey.com/dhbsoftware/
[PentagonMI]: https://gitlab.com/pentagon-functions/PentagonMI
