#include "Kin.h"
#include "PhaseSpaceGen.h"
#include "Constants.h"

namespace  PentagonFunctions {


namespace  {

// we use normal metric with real types here
template <class T,size_t D> T square(const std::array<T,D>& a){
    T res(a[0]*a[0]);
    for(unsigned i=1; i<D; i++){
        res -= a[i]*a[i];
    }
    return res;
}

template <class T,size_t D> std::array<T,D> operator+ (std::array<T,D> v1, const std::array<T,D>& v2){
    for (size_t i = 0; i < D; ++i) {
        v1[i] += v2[i];
    }
    return v1;
}

template <class T,size_t D> std::array<T,D> operator- (std::array<T,D> v){
    for(auto& it : v){
        it *= T(-1);
    }
    return v;
}

template <class T,size_t D> std::array<T,D> operator/ (std::array<T,D> v, T x){
    for(auto& it : v){
        it /= x;
    }
    return v;
}
    
} // 


template <typename T> T PhaseSpaceGen<T>::s(const std::vector<int>& is) const {
    Momentum m{};

    for(auto& ii : is) {
        m = m + momenta.at(ii-1);
    }

    return square(m);
}




template <class T> void PhaseSpaceGen<T>::generatepart(int s, const std::vector<unsigned>& range) {
    using std::cos;
    using std::sin;
    using std::abs;
    using std::sqrt;
    using std::pow;
    //
    // first we generate ligh-like momenta (eq  3.1)
    Momentum Q{};
    for (size_t i = 0; i < range.size(); i++) {
        T rho[] = {grnd(), grnd(), grnd(), grnd()};
        T ci = T(2) * rho[0] - 1;
        T phi = T(2) * ::constants::pi<T> * rho[1];

        T q0 = -log(rho[2] * rho[3]);
        T qx = q0 * sqrt(T(1) - ci * ci) * cos(phi);
        T qy = q0 * sqrt(T(1) - ci * ci) * sin(phi);
        T qz = q0 * ci;

        momenta[range[i]] = Momentum{q0, qx, qy, qz};
        Q = Q + momenta[range[i]];
    }

    // then we transform them to impose Momentum conservation (eq 2.4 - 2.5)
    {
        T M = sqrt(square(Q));
        Momentum b = -Q / M;
        T x = T(s) * w / M;
        T gamma = Q[0] / M;
        T alpha = T(1) / (T(1) + gamma);

        for (size_t i = 0; i < range.size(); i++) {
            T q0 = (momenta[range[i]][0]), qx = (momenta[range[i]][1]),
              qy = (momenta[range[i]][2]), qz = (momenta[range[i]][3]);
            T bxq = (b[1]) * qx + (b[2]) * qy + (b[3]) * qz;

            T p0 = x * (gamma * q0 + bxq);
            T px = x * (qx + (b[1]) * (q0 + alpha * bxq));
            T py = x * (qy + (b[2]) * (q0 + alpha * bxq));
            T pz = x * (qz + (b[3]) * (q0 + alpha * bxq));

            momenta[range[i]] = Momentum{p0, px, py, pz};
        }
    }
}

/*template <class T> std::vector<typename PhaseSpaceGen<T>::Momentum> PhaseSpaceGen<T>::mom_double() const {*/
    /*std::vector<Momentum> dmom;*/
    /*for (unsigned i = 0; i < N; i++)*/
        /*dmom.push_back(Momentum(to_double(momenta[i][0]), to_double(momenta[i][1]),*/
                                            /*to_double(momenta[i][2]), to_double(momenta[i][3])));*/
    /*return dmom;*/
/*}*/

template <class T> void PhaseSpaceGen<T>::generate() {
    std::vector<unsigned> outpos;
    for (unsigned i = 0; i < N; i++)
        if (find(pos.begin(), pos.end(), i) == pos.end()) outpos.push_back(i);

    generatepart(-1, pos);   // first generate "ingoing" momenta
    generatepart(1, outpos); // then generate "outgoing"
}



template class PhaseSpaceGen<double>;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template class PhaseSpaceGen<dd_real>;
template class PhaseSpaceGen<qd_real>;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED



} // namespace PentagonFunctions
    
