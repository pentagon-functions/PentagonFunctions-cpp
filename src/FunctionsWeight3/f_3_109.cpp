#include "f_3_109.h"

namespace PentagonFunctions {

template <typename T> T f_3_109_abbreviated (const std::array<T,37>&);

template <typename T> class SpDLog_f_3_109_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_109_W_10 (const Kin<T>& kin) {
        c[0] = T(-4) * kin.v[0] * kin.v[2] + kin.v[2] * (T(6) * kin.v[2] + T(8) * kin.v[3]) + kin.v[1] * (T(-4) * kin.v[0] + T(2) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(-2) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[1] + T(8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[6] * T(-8) + abb[1] * (abb[3] * T(-4) + abb[2] * T(4)) + abb[2] * (abb[2] * T(-8) + abb[5] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[3] * T(4) + abb[4] * T(4)) + abb[3] * (abb[4] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[3] * T(4) + abb[5] * T(4)));
    }
};
template <typename T> class SpDLog_f_3_109_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_109_W_21 (const Kin<T>& kin) {
        c[0] = (T(5) / T(2) + (T(-25) * kin.v[2]) / T(8) + (T(-35) * kin.v[3]) / T(4) + (T(-5) * kin.v[4]) / T(2)) * kin.v[2] + (T(5) / T(2) + (T(-45) * kin.v[3]) / T(8) + (T(-5) * kin.v[4]) / T(2)) * kin.v[3] + (T(-5) / T(2) + (T(-45) * kin.v[1]) / T(8) + (T(35) * kin.v[2]) / T(4) + (T(45) * kin.v[3]) / T(4) + (T(5) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(-5) / T(2) + (T(-35) * kin.v[1]) / T(4) + (T(25) * kin.v[2]) / T(4) + (T(35) * kin.v[3]) / T(4) + (T(5) * kin.v[4]) / T(2) + (T(-25) / T(8) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(-3) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + (T(3) * kin.v[3]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[11] * ((abb[10] * T(3)) / T(2) + abb[4] * abb[9] * T(-4) + abb[9] * (abb[9] / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[5] * (-abb[5] / T(2) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[4] * T(4)) + abb[1] * (abb[5] * T(-4) + abb[9] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_109_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl16 = DLog_W_16<T>(kin),dl21 = DLog_W_21<T>(kin),dl15 = DLog_W_15<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl1 = DLog_W_1<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),spdl10 = SpDLog_f_3_109_W_10<T>(kin),spdl21 = SpDLog_f_3_109_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,37> abbr = 
            {dl10(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_7(kin_path), dl16(t), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), dl21(t), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl3(t), f_2_1_4(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl5(t), dl2(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl19(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl1(t), dl18(t), dl17(t), dl20(t), dl28(t) / kin_path.SqrtDelta, f_2_2_3(kin_path), f_1_3_1(kin_path), f_1_3_3(kin_path), f_1_3_4(kin_path), dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_3_109_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_3_109_abbreviated(const std::array<T,37>& abb)
{
using TR = typename T::value_type;
T z[81];
z[0] = abb[1];
z[1] = abb[19];
z[2] = abb[23];
z[3] = abb[28];
z[4] = abb[29];
z[5] = abb[2];
z[6] = abb[12];
z[7] = abb[15];
z[8] = abb[20];
z[9] = abb[27];
z[10] = abb[3];
z[11] = abb[4];
z[12] = abb[5];
z[13] = abb[8];
z[14] = abb[9];
z[15] = bc<TR>[0];
z[16] = abb[7];
z[17] = abb[26];
z[18] = abb[6];
z[19] = abb[10];
z[20] = abb[13];
z[21] = abb[14];
z[22] = abb[16];
z[23] = abb[17];
z[24] = abb[18];
z[25] = abb[21];
z[26] = abb[22];
z[27] = abb[24];
z[28] = abb[25];
z[29] = abb[30];
z[30] = abb[31];
z[31] = abb[32];
z[32] = abb[33];
z[33] = abb[34];
z[34] = abb[35];
z[35] = abb[36];
z[36] = T(2) * z[2];
z[37] = T(3) * z[17];
z[38] = z[36] + -z[37];
z[39] = T(3) * z[9] + z[38];
z[40] = T(2) * z[16];
z[41] = T(2) * z[6];
z[42] = z[40] + z[41];
z[43] = T(2) * z[3];
z[44] = T(4) * z[7];
z[45] = z[1] + -z[8];
z[46] = T(-4) * z[4] + -z[39] + z[42] + -z[43] + -z[44] + -z[45];
z[46] = z[14] * z[46];
z[47] = T(5) * z[3];
z[48] = T(4) * z[6];
z[49] = z[1] + z[4];
z[50] = z[7] + z[49];
z[38] = T(-7) * z[8] + z[38] + z[47] + z[48] + z[50];
z[51] = -(z[21] * z[38]);
z[52] = z[7] + -z[8];
z[53] = T(8) * z[3];
z[54] = z[2] + -z[48] + -z[49] + T(3) * z[52] + z[53];
z[54] = z[13] * z[54];
z[55] = z[1] + z[9];
z[56] = T(3) * z[8] + z[37] + z[55];
z[57] = T(2) * z[7];
z[58] = -z[40] + z[57];
z[47] = T(6) * z[2] + z[47] + -z[56] + z[58];
z[47] = z[10] * z[47];
z[59] = -z[8] + z[9];
z[60] = z[2] + z[59];
z[61] = -z[37] + z[60];
z[57] = -z[4] + z[57];
z[42] = z[3] + z[42] + z[57] + z[61];
z[42] = z[5] * z[42];
z[62] = T(2) * z[4];
z[63] = z[41] + z[62];
z[64] = z[7] + z[8];
z[65] = z[36] + -z[63] + T(3) * z[64];
z[66] = T(3) * z[1];
z[67] = -z[43] + -z[65] + z[66];
z[67] = z[0] * z[67];
z[68] = -z[2] + z[17];
z[68] = z[28] * z[68];
z[49] = -z[7] + -z[17] + z[49];
z[49] = z[24] * z[49];
z[49] = z[49] + z[68];
z[68] = z[32] + z[33];
z[68] = T(-2) * z[68];
z[69] = z[34] + z[35];
z[70] = T(2) * z[29] + z[69];
z[68] = z[68] * z[70];
z[71] = -z[1] + z[3];
z[72] = T(3) * z[4];
z[73] = T(3) * z[2] + -z[71] + -z[72];
z[73] = z[11] * z[73];
z[74] = T(4) * z[29] + T(2) * z[69];
z[74] = z[31] * z[74];
z[75] = -z[2] + z[59];
z[76] = z[3] + z[4] + -z[17] + z[75];
z[76] = T(3) * z[76];
z[77] = z[26] * z[76];
z[42] = z[42] + z[46] + z[47] + T(3) * z[49] + z[51] + z[54] + z[67] + z[68] + z[73] + z[74] + z[77];
z[42] = int_to_imaginary<T>(1) * z[42];
z[46] = -z[2] + z[4];
z[47] = z[9] + -z[17] + (T(23) * z[52]) / T(12);
z[47] = z[1] / T(3) + (T(107) * z[3]) / T(24) + (T(-10) * z[6]) / T(3) + (T(8) * z[29]) / T(3) + z[46] / T(12) + z[47] / T(2) + (T(4) * z[69]) / T(3);
z[47] = z[15] * z[47];
z[42] = z[42] + z[47];
z[42] = z[15] * z[42];
z[47] = T(2) * z[8];
z[49] = T(5) * z[2];
z[51] = z[47] + z[49];
z[52] = T(7) * z[7];
z[40] = z[37] + -z[40] + z[51] + z[52] + -z[63] + -z[66];
z[40] = int_to_imaginary<T>(1) * z[15] * z[40];
z[54] = -z[44] + z[63];
z[63] = T(3) * z[3];
z[67] = T(2) * z[1];
z[60] = -z[54] + z[60] + z[63] + z[67];
z[60] = z[5] * z[60];
z[68] = -z[37] + z[67];
z[69] = T(3) * z[59];
z[73] = z[3] + -z[16];
z[74] = z[4] + z[7];
z[77] = -z[68] + -z[69] + -z[73] + -z[74];
z[77] = z[14] * z[77];
z[78] = z[1] + z[16];
z[79] = T(3) * z[7];
z[43] = z[43] + z[78] + z[79];
z[80] = z[36] + -z[43] + -z[59];
z[80] = z[10] * z[80];
z[51] = z[3] + -z[51] + z[78] + -z[79];
z[51] = z[11] * z[51];
z[62] = -z[1] + -z[3] + z[36] + z[62] + T(2) * z[64];
z[62] = z[0] * z[62];
z[54] = z[54] + z[68] + -z[73];
z[54] = z[13] * z[54];
z[68] = z[1] / T(2);
z[57] = (T(5) * z[2]) / T(2) + -z[8] / T(2) + -z[16] + z[57] + z[68];
z[57] = z[12] * z[57];
z[40] = z[40] + z[51] + z[54] + z[57] + z[60] + z[62] + z[77] + z[80];
z[40] = z[12] * z[40];
z[51] = z[4] / T(2);
z[54] = (T(3) * z[3]) / T(2);
z[52] = z[9] + z[52];
z[52] = -z[2] + z[16] + -z[41] + -z[51] + z[52] / T(2) + z[54] + z[66];
z[52] = z[5] * z[52];
z[49] = z[7] + -z[16] + z[49] + z[53] + -z[56];
z[49] = z[10] * z[49];
z[43] = z[43] + z[61];
z[53] = -(z[11] * z[43]);
z[49] = z[49] + z[52] + z[53];
z[49] = z[5] * z[49];
z[52] = z[44] + -z[71] + z[75];
z[52] = z[10] * z[52];
z[53] = -z[2] + z[45] + -z[74];
z[53] = z[14] * z[53];
z[46] = z[3] + -z[46] + z[68];
z[46] = z[0] * z[46];
z[56] = z[11] * z[71];
z[46] = z[46] + z[52] + T(2) * z[53] + z[56] + -z[60];
z[46] = z[0] * z[46];
z[52] = z[7] / T(2) + -z[68];
z[47] = z[2] / T(2) + -z[47] + z[48] + -z[51] + z[52] + -z[54];
z[47] = z[13] * z[47];
z[41] = -z[41] + z[50] + z[73];
z[48] = z[5] + z[14];
z[48] = z[41] * z[48];
z[50] = z[3] + z[65] + -z[67];
z[50] = z[0] * z[50];
z[51] = z[2] + -z[8];
z[37] = T(-7) * z[3] + T(-5) * z[7] + z[16] + z[37] + T(-4) * z[51];
z[37] = z[10] * z[37];
z[37] = z[37] + z[47] + z[48] + z[50];
z[37] = z[13] * z[37];
z[44] = (T(-7) * z[3]) / T(2) + (T(3) * z[17]) / T(2) + -z[36] + -z[44] + z[59] / T(2) + z[68];
z[44] = prod_pow(z[10], 2) * z[44];
z[39] = -z[8] + z[39] + z[72] + z[73] + z[79];
z[39] = z[11] * z[39];
z[41] = -(z[5] * z[41]);
z[47] = -z[2] + -z[4] + -z[8] + (T(3) * z[9]) / T(2) + z[52];
z[47] = z[14] * z[47];
z[39] = z[39] + z[41] + z[47];
z[39] = z[14] * z[39];
z[36] = z[16] + -z[36] + z[55] + -z[63] + z[64];
z[36] = z[18] * z[36];
z[38] = z[20] * z[38];
z[41] = -z[1] + z[7];
z[41] = z[23] * z[41];
z[47] = z[23] + z[27];
z[47] = z[17] * z[47];
z[45] = z[3] + -z[7] + z[45];
z[45] = z[22] * z[45];
z[41] = z[41] + z[45] + z[47];
z[43] = z[10] * z[43];
z[45] = -z[17] + z[59];
z[45] = -z[1] + T(-3) * z[45];
z[45] = -z[3] + z[45] / T(2);
z[45] = z[11] * z[45];
z[43] = z[43] + z[45];
z[43] = z[11] * z[43];
z[45] = z[58] + z[69];
z[45] = z[19] * z[45];
z[47] = (T(-5) * z[19]) / T(2) + T(-3) * z[27];
z[47] = z[2] * z[47];
z[48] = -(z[23] * z[72]);
z[50] = -(z[30] * z[70]);
z[51] = -(z[25] * z[76]);
return T(2) * z[36] + z[37] + z[38] + z[39] + z[40] + T(3) * z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51];
}



template IntegrandConstructorType<double> f_3_109_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_109_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_109_construct (const Kin<qd_real>&);
#endif

}