#include "f_3_46.h"

namespace PentagonFunctions {

template <typename T> T f_3_46_abbreviated (const std::array<T,23>&);



template <typename T> IntegrandConstructorType<T> f_3_46_construct (const Kin<T>& kin) {
    return [&kin, 
            dl27 = DLog_W_27<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl31 = DLog_W_31<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,23> abbr = 
            {dl27(t) / kin_path.SqrtDelta, rlog(v_path[0]), rlog(-v_path[4]), rlog(v_path[2]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(v_path[3]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_1(kin_path), f_2_1_2(kin_path), f_2_1_7(kin_path), f_2_1_8(kin_path), dl2(t), f_2_2_4(kin_path), dl18(t), dl20(t), dl31(t), dl30(t) / kin_path.SqrtDelta, f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl26(t) / kin_path.SqrtDelta, f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl29(t) / kin_path.SqrtDelta}
;

        auto result = f_3_46_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_46_abbreviated(const std::array<T,23>& abb)
{
using TR = typename T::value_type;
T z[31];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[4];
z[4] = abb[3];
z[5] = abb[6];
z[6] = abb[5];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[22];
z[12] = abb[16];
z[13] = bc<TR>[0];
z[14] = abb[19];
z[15] = abb[11];
z[16] = abb[12];
z[17] = abb[13];
z[18] = abb[14];
z[19] = abb[15];
z[20] = abb[17];
z[21] = abb[18];
z[22] = abb[20];
z[23] = abb[21];
z[24] = z[10] + z[22];
z[25] = z[9] + z[20];
z[26] = z[24] + -z[25];
z[26] = z[14] * z[26];
z[27] = z[8] + -z[10];
z[25] = -z[25] + z[27];
z[25] = z[12] * z[25];
z[27] = -z[9] + -z[27];
z[27] = z[0] * z[27];
z[28] = z[15] + z[17] + z[18] + T(-2) * z[19];
z[28] = z[16] * z[28];
z[29] = -z[0] + z[11];
z[29] = z[7] * z[29];
z[25] = z[25] + z[26] + z[27] + z[28] + -z[29];
z[24] = z[20] + z[24];
z[26] = z[1] + -z[6];
z[27] = z[5] + z[26];
z[27] = -z[4] + T(2) * z[27];
z[27] = z[4] * z[27];
z[28] = prod_pow(z[6], 2);
z[29] = -z[1] + T(2) * z[6];
z[29] = z[1] * z[29];
z[24] = T(-2) * z[24] + z[27] + -z[28] + z[29];
z[24] = z[11] * z[24];
z[27] = z[11] + z[12];
z[28] = -z[0] + z[27];
z[26] = -(z[26] * z[28]);
z[29] = z[0] + z[14];
z[30] = -(z[5] * z[29]);
z[27] = z[2] * z[27];
z[26] = z[26] + z[27] + z[30];
z[27] = -(z[3] * z[28]);
z[26] = T(2) * z[26] + z[27];
z[26] = z[3] * z[26];
z[27] = z[4] + -z[6];
z[28] = -(z[27] * z[29]);
z[30] = -(z[5] * z[12]);
z[28] = T(2) * z[28] + z[30];
z[28] = z[5] * z[28];
z[29] = -z[11] + z[29];
z[29] = z[5] * z[29];
z[30] = -z[1] + z[4];
z[30] = z[0] * z[30];
z[29] = z[29] + z[30];
z[30] = -(z[0] * z[2]);
z[29] = T(2) * z[29] + z[30];
z[29] = z[2] * z[29];
z[24] = z[24] + T(2) * z[25] + z[26] + z[28] + z[29];
z[25] = z[21] + -z[23] + z[27];
z[25] = z[14] * z[25];
z[26] = z[6] + -z[21];
z[26] = -(z[12] * z[26]);
z[27] = -z[1] + -z[5] + z[21] + z[23];
z[27] = z[11] * z[27];
z[25] = z[25] + z[26] + z[27];
z[25] = int_to_imaginary<T>(1) * z[25];
z[26] = z[11] + -z[12];
z[26] = z[13] * z[26];
z[25] = T(6) * z[25] + z[26];
z[25] = z[13] * z[25];
return T(3) * z[24] + z[25];
}



template IntegrandConstructorType<double> f_3_46_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_46_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_46_construct (const Kin<qd_real>&);
#endif

}