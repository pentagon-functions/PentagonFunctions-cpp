#include "f_3_103.h"

namespace PentagonFunctions {

template <typename T> T f_3_103_abbreviated (const std::array<T,27>&);



template <typename T> IntegrandConstructorType<T> f_3_103_construct (const Kin<T>& kin) {
    return [&kin, 
            dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),dl31 = DLog_W_31<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,27> abbr = 
            {dl30(t) / kin_path.SqrtDelta, rlog(-v_path[1]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_3(kin_path), f_2_1_6(kin_path), f_2_1_13(kin_path), f_2_1_14(kin_path), rlog(v_path[0] + v_path[4]), rlog(v_path[0] + v_path[1]), rlog(-v_path[1] + v_path[3]), dl28(t) / kin_path.SqrtDelta, rlog(-v_path[4]), f_2_1_4(kin_path), dl29(t) / kin_path.SqrtDelta, f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl26(t) / kin_path.SqrtDelta, dl31(t), f_2_2_6(kin_path), f_1_3_4(kin_path), f_1_3_5(kin_path), dl1(t), dl17(t), dl19(t)}
;

        auto result = f_3_103_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_103_abbreviated(const std::array<T,27>& abb)
{
using TR = typename T::value_type;
T z[42];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[12];
z[14] = abb[19];
z[15] = abb[13];
z[16] = abb[14];
z[17] = abb[16];
z[18] = abb[15];
z[19] = abb[17];
z[20] = abb[18];
z[21] = abb[20];
z[22] = abb[21];
z[23] = abb[22];
z[24] = abb[23];
z[25] = abb[24];
z[26] = abb[25];
z[27] = abb[26];
z[28] = T(2) * z[6];
z[29] = z[4] + z[5];
z[30] = z[28] * z[29];
z[31] = T(2) * z[9];
z[30] = z[30] + -z[31];
z[32] = T(2) * z[1];
z[33] = -z[2] + z[4];
z[33] = z[32] * z[33];
z[34] = T(2) * z[7];
z[35] = prod_pow(z[4], 2);
z[36] = z[5] + z[6];
z[36] = -z[2] + T(2) * z[36];
z[36] = z[2] * z[36];
z[33] = -z[30] + z[33] + z[34] + z[35] + z[36];
z[33] = z[15] * z[33];
z[36] = -z[2] + T(2) * z[29];
z[36] = z[2] * z[36];
z[37] = T(2) * z[8];
z[36] = z[36] + -z[37];
z[35] = T(2) * z[19] + z[35];
z[38] = prod_pow(z[5], 2);
z[39] = z[4] * z[28];
z[40] = z[2] + -z[5];
z[40] = z[1] + T(2) * z[40];
z[40] = z[1] * z[40];
z[38] = -z[35] + -z[36] + z[38] + z[39] + z[40];
z[38] = z[14] * z[38];
z[39] = prod_pow(z[6], 2);
z[40] = T(2) * z[2];
z[41] = z[1] * z[40];
z[31] = z[31] + -z[34] + -z[36] + z[39] + z[41];
z[31] = z[0] * z[31];
z[34] = z[2] * z[28];
z[30] = -z[30] + z[34] + z[35] + z[37];
z[30] = z[17] * z[30];
z[34] = T(-2) * z[21] + z[25] + z[26] + z[27];
z[35] = z[22] * z[34];
z[36] = z[14] + -z[15];
z[36] = z[18] * z[36];
z[37] = z[0] + -z[15];
z[39] = z[14] + -z[17] + z[37];
z[39] = z[10] * z[39];
z[35] = z[35] + -z[36] + -z[39];
z[36] = z[15] + z[17];
z[39] = -z[4] + z[6];
z[39] = z[36] * z[39];
z[41] = -z[1] + -z[6] + z[29];
z[41] = z[14] * z[41];
z[36] = T(-2) * z[14] + z[36];
z[36] = int_to_imaginary<T>(1) * z[3] * z[36];
z[36] = z[36] + z[39] + z[41];
z[39] = z[16] * z[17];
z[36] = T(2) * z[36] + z[39];
z[36] = z[16] * z[36];
z[30] = z[30] + z[31] + z[33] + T(-2) * z[35] + z[36] + z[38];
z[31] = -z[23] + -z[24];
z[31] = z[31] * z[34];
z[28] = -z[11] + z[28];
z[32] = -z[2] + z[13] + z[28] + -z[32];
z[32] = z[15] * z[32];
z[28] = -z[12] + -z[20] + z[28] + z[29];
z[28] = z[17] * z[28];
z[33] = z[1] + -z[12] + z[40];
z[29] = -z[11] + -z[13] + z[29] + z[33];
z[29] = z[0] * z[29];
z[33] = -z[6] + z[20] + z[33];
z[33] = z[14] * z[33];
z[28] = z[28] + z[29] + T(2) * z[31] + z[32] + z[33];
z[28] = int_to_imaginary<T>(1) * z[28];
z[29] = T(-8) * z[34] + z[37];
z[29] = z[3] * z[29];
z[28] = T(6) * z[28] + z[29];
z[28] = z[3] * z[28];
return z[28] + T(3) * z[30];
}



template IntegrandConstructorType<double> f_3_103_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_103_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_103_construct (const Kin<qd_real>&);
#endif

}