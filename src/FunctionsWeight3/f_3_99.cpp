#include "f_3_99.h"

namespace PentagonFunctions {

template <typename T> T f_3_99_abbreviated (const std::array<T,37>&);

template <typename T> class SpDLog_f_3_99_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_99_W_23 (const Kin<T>& kin) {
        c[0] = (T(5) / T(2) + (T(45) * kin.v[3]) / T(8) + (T(-35) * kin.v[4]) / T(4)) * kin.v[3] + (T(5) / T(2) + (T(25) * kin.v[2]) / T(8) + (T(35) * kin.v[3]) / T(4) + (T(-25) * kin.v[4]) / T(4)) * kin.v[2] + ((T(-5) * kin.v[2]) / T(2) + (T(-5) * kin.v[3]) / T(2) + (T(5) * kin.v[4]) / T(2)) * kin.v[1] + (T(-5) / T(2) + (T(25) * kin.v[4]) / T(8)) * kin.v[4] + kin.v[0] * (T(-5) / T(2) + (T(5) * kin.v[1]) / T(2) + (T(-35) * kin.v[2]) / T(4) + (T(-45) * kin.v[3]) / T(4) + (T(35) * kin.v[4]) / T(4) + (T(45) / T(8) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[1] + T(4) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = (T(-3) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * ((abb[5] * T(-3)) / T(2) + abb[3] * (-abb[3] / T(2) + abb[4] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[1] * (abb[2] * T(-4) + abb[3] * T(4)) + abb[2] * (abb[2] / T(2) + bc<T>[0] * int_to_imaginary<T>(4) + abb[4] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_99_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl4 = DLog_W_4<T>(kin),dl9 = DLog_W_9<T>(kin),dl5 = DLog_W_5<T>(kin),dl25 = DLog_W_25<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl2 = DLog_W_2<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),spdl23 = SpDLog_f_3_99_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,37> abbr = 
            {dl23(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_8(kin_path), dl4(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_6(kin_path), f_2_1_11(kin_path), rlog(v_path[0] + v_path[1]), dl9(t), dl5(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl25(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl20(t), f_2_1_15(kin_path), dl18(t), dl16(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl19(t), dl17(t), dl1(t), dl2(t), dl27(t) / kin_path.SqrtDelta, f_2_2_5(kin_path), f_1_3_1(kin_path), f_1_3_3(kin_path), f_1_3_5(kin_path), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_3_99_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_3_99_abbreviated(const std::array<T,37>& abb)
{
using TR = typename T::value_type;
T z[79];
z[0] = abb[1];
z[1] = abb[6];
z[2] = abb[12];
z[3] = abb[20];
z[4] = abb[23];
z[5] = abb[26];
z[6] = abb[27];
z[7] = abb[28];
z[8] = abb[29];
z[9] = abb[2];
z[10] = abb[13];
z[11] = abb[22];
z[12] = abb[3];
z[13] = abb[4];
z[14] = abb[7];
z[15] = abb[8];
z[16] = bc<TR>[0];
z[17] = abb[17];
z[18] = abb[14];
z[19] = abb[5];
z[20] = abb[9];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[15];
z[24] = abb[16];
z[25] = abb[18];
z[26] = abb[19];
z[27] = abb[21];
z[28] = abb[24];
z[29] = abb[25];
z[30] = abb[30];
z[31] = abb[31];
z[32] = abb[32];
z[33] = abb[33];
z[34] = abb[34];
z[35] = abb[35];
z[36] = abb[36];
z[37] = T(2) * z[4];
z[38] = z[3] + z[37];
z[39] = T(2) * z[8];
z[40] = T(2) * z[10];
z[41] = T(2) * z[6];
z[42] = T(2) * z[17];
z[43] = z[1] + z[5];
z[44] = T(-3) * z[7] + z[38] + -z[39] + z[40] + z[41] + z[42] + z[43];
z[44] = z[12] * z[44];
z[45] = -z[5] + z[8];
z[46] = -z[1] + z[3];
z[47] = z[45] + z[46];
z[48] = T(2) * z[11];
z[40] = z[40] + -z[48];
z[49] = -z[37] + -z[40] + -z[47];
z[49] = z[14] * z[49];
z[50] = T(3) * z[11];
z[51] = T(3) * z[3];
z[52] = z[50] + z[51];
z[53] = T(3) * z[5] + -z[42] + -z[52];
z[54] = (T(3) * z[2]) / T(2);
z[55] = z[1] + (T(-21) * z[4]) / T(2) + z[7] + z[10] + z[41] + z[53] + z[54];
z[55] = z[9] * z[55];
z[56] = -z[4] + z[46];
z[57] = -z[8] + z[42];
z[40] = z[6] + -z[7] + z[40] + z[56] + z[57];
z[40] = z[15] * z[40];
z[58] = z[37] + z[52];
z[59] = z[5] + z[6];
z[60] = T(4) * z[17] + -z[59];
z[61] = T(-5) * z[8] + z[58] + z[60];
z[62] = -(z[26] * z[61]);
z[63] = z[32] + z[34];
z[63] = T(2) * z[63];
z[64] = -z[30] + T(2) * z[36];
z[65] = T(2) * z[35] + z[64];
z[63] = z[63] * z[65];
z[66] = -z[6] + z[46];
z[54] = -z[8] + z[54];
z[67] = (T(13) * z[4]) / T(2) + -z[54] + T(2) * z[66];
z[68] = z[22] * z[67];
z[52] = -z[4] + z[52] + -z[60];
z[52] = z[18] * z[52];
z[69] = z[6] + z[45];
z[70] = T(4) * z[4];
z[71] = T(3) * z[2] + z[69] + -z[70];
z[71] = z[0] * z[71];
z[72] = -z[10] + z[11];
z[73] = z[59] + -z[72];
z[74] = -z[4] + -z[7] + z[73];
z[74] = T(3) * z[74];
z[75] = -(z[24] * z[74]);
z[64] = T(-4) * z[35] + T(-2) * z[64];
z[64] = z[33] * z[64];
z[76] = T(3) * z[4];
z[77] = z[29] * z[76];
z[78] = z[14] + -z[29];
z[78] = -z[22] + z[26] + T(3) * z[78];
z[78] = z[7] * z[78];
z[40] = z[40] + z[44] + z[49] + z[52] + z[55] + z[62] + z[63] + z[64] + z[68] + z[71] + z[75] + z[77] + z[78];
z[40] = int_to_imaginary<T>(1) * z[40];
z[44] = z[3] + z[8];
z[49] = z[5] / T(2);
z[52] = -z[1] + (T(-5) * z[6]) / T(4) + T(4) * z[30] + T(-8) * z[36] + z[49];
z[44] = z[2] + -z[4] / T(4) + (T(9) * z[11]) / T(8) + (T(-10) * z[17]) / T(3) + (T(-8) * z[35]) / T(3) + (T(37) * z[44]) / T(24) + z[52] / T(3);
z[44] = z[16] * z[44];
z[40] = z[40] + z[44];
z[40] = z[16] * z[40];
z[44] = T(2) * z[3];
z[48] = z[44] + z[48];
z[52] = -z[1] + z[41];
z[55] = T(2) * z[5];
z[62] = z[48] + z[52] + -z[55] + z[76];
z[62] = z[12] * z[62];
z[50] = z[4] + -z[5] + T(-4) * z[8] + z[42] + z[44] + z[50] + -z[52];
z[50] = z[15] * z[50];
z[48] = -z[6] + z[8] + z[43] + -z[48] + -z[70];
z[48] = z[9] * z[48];
z[42] = T(3) * z[8] + -z[42] + -z[58] + T(2) * z[59];
z[42] = z[18] * z[42];
z[47] = z[4] + z[47];
z[47] = z[14] * z[47];
z[39] = z[4] + -z[6] + -z[39] + -z[53];
z[39] = int_to_imaginary<T>(1) * z[16] * z[39];
z[52] = z[3] / T(2);
z[53] = -z[4] + z[8] / T(2) + (T(-3) * z[11]) / T(2) + -z[52] + z[59];
z[53] = z[13] * z[53];
z[58] = z[4] + -z[69];
z[58] = z[0] * z[58];
z[39] = z[39] + z[42] + z[47] + z[48] + z[50] + z[53] + z[58] + z[62];
z[39] = z[13] * z[39];
z[42] = T(2) * z[7];
z[47] = (T(15) * z[4]) / T(2) + z[10] + z[11] + -z[42] + -z[43] + z[51] + -z[54];
z[47] = z[9] * z[47];
z[48] = z[6] / T(2);
z[50] = z[7] / T(2);
z[49] = (T(-3) * z[2]) / T(4) + (T(-5) * z[4]) / T(4) + -z[8] + -z[46] + z[48] + z[49] + z[50];
z[49] = z[0] * z[49];
z[51] = z[7] + z[8];
z[53] = z[6] + z[10];
z[38] = -z[1] + -z[11] + -z[38] + z[51] + -z[53];
z[38] = z[12] * z[38];
z[54] = z[4] + z[72];
z[58] = z[5] + -z[46] + z[54];
z[59] = z[14] * z[58];
z[62] = z[7] * z[14];
z[59] = z[59] + -z[62];
z[58] = -z[7] + z[58];
z[58] = z[15] * z[58];
z[38] = z[38] + z[47] + z[49] + z[58] + -z[59];
z[38] = z[0] * z[38];
z[47] = (T(5) * z[8]) / T(2);
z[49] = z[37] + z[43] / T(2) + z[47] + -z[52] + z[72];
z[52] = prod_pow(z[14], 2);
z[49] = z[49] * z[52];
z[41] = z[41] + -z[55] + z[56] + -z[57];
z[41] = z[9] * z[41];
z[42] = z[42] + -z[57] + -z[73];
z[55] = -z[12] + z[18];
z[42] = z[42] * z[55];
z[55] = -z[5] + -z[66];
z[55] = -z[4] + z[55] / T(2) + -z[57];
z[55] = z[15] * z[55];
z[41] = z[41] + z[42] + z[55] + -z[59];
z[41] = z[15] * z[41];
z[42] = z[7] + -z[10];
z[55] = z[3] + -z[5] + z[11];
z[56] = (T(3) * z[4]) / T(2);
z[42] = -z[1] / T(2) + (T(3) * z[42]) / T(2) + -z[48] + z[55] + z[56];
z[42] = z[12] * z[42];
z[48] = -z[3] + -z[6] + T(2) * z[43] + -z[51] + -z[54];
z[48] = z[9] * z[48];
z[42] = z[42] + z[48];
z[42] = z[12] * z[42];
z[47] = -z[47] + -z[50] + z[56] + z[60];
z[47] = z[18] * z[47];
z[48] = z[7] + z[57] + T(-2) * z[73] + z[76];
z[50] = z[9] + -z[12];
z[48] = z[48] * z[50];
z[50] = z[8] + -z[72];
z[50] = z[14] * z[50];
z[47] = z[47] + z[48] + z[50] + -z[62];
z[47] = z[18] * z[47];
z[44] = T(-2) * z[1] + z[6] + z[44] + z[56] + -z[72];
z[44] = z[19] * z[44];
z[46] = z[46] + -z[72];
z[37] = z[5] + -z[37] + -z[46];
z[37] = z[14] * z[37];
z[43] = -z[43] + z[53];
z[43] = (T(9) * z[2]) / T(4) + -z[3] + (T(-13) * z[4]) / T(4) + z[43] / T(2);
z[43] = z[9] * z[43];
z[37] = z[37] + z[43] + T(2) * z[62];
z[37] = z[9] * z[37];
z[43] = z[8] + -z[55];
z[43] = z[27] * z[43];
z[48] = z[4] + -z[7];
z[48] = z[28] * z[48];
z[43] = z[43] + z[48];
z[48] = -(z[20] * z[67]);
z[50] = -z[7] + z[61];
z[50] = z[25] * z[50];
z[51] = -(z[23] * z[74]);
z[45] = -z[45] + -z[46];
z[45] = z[21] * z[45];
z[46] = -(z[31] * z[65]);
z[52] = z[20] + (T(-3) * z[52]) / T(2);
z[52] = z[7] * z[52];
return z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + T(3) * z[43] + z[44] + T(2) * z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52];
}



template IntegrandConstructorType<double> f_3_99_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_99_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_99_construct (const Kin<qd_real>&);
#endif

}