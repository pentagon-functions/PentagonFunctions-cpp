#include "f_3_30.h"

namespace PentagonFunctions {

template <typename T> T f_3_30_abbreviated (const std::array<T,32>&);

template <typename T> class SpDLog_f_3_30_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_30_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(-13) * kin.v[4]) / T(2) + T(-7) + bc<T>[0] * int_to_imaginary<T>(-4) + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(4) + T(2) * kin.v[4]) + ((T(27) * kin.v[4]) / T(4) + T(7)) * kin.v[4];
c[1] = (T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[11] * (abb[9] * (abb[12] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[9] * T(2)) + abb[13] * T(3) + abb[8] * abb[9] * T(4) + abb[6] * (abb[8] * T(-4) + abb[6] * T(-2) + bc<T>[0] * int_to_imaginary<T>(4) + abb[12] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_30_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl13 = DLog_W_13<T>(kin),dl12 = DLog_W_12<T>(kin),dl6 = DLog_W_6<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),spdl12 = SpDLog_f_3_30_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,32> abbr = 
            {dl26(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[4]), f_2_1_1(kin_path), dl12(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_4(kin_path), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), dl19(t), f_2_1_11(kin_path), dl4(t), f_2_1_2(kin_path), dl18(t), dl17(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl3(t), dl5(t), dl1(t), dl20(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t)}
;

        auto result = f_3_30_abbreviated(abbr);
        result = result + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_3_30_abbreviated(const std::array<T,32>& abb)
{
using TR = typename T::value_type;
T z[83];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = bc<TR>[0];
z[11] = abb[10];
z[12] = abb[14];
z[13] = abb[17];
z[14] = abb[19];
z[15] = abb[21];
z[16] = abb[22];
z[17] = abb[25];
z[18] = abb[26];
z[19] = abb[27];
z[20] = abb[28];
z[21] = abb[31];
z[22] = abb[12];
z[23] = abb[15];
z[24] = abb[13];
z[25] = abb[16];
z[26] = abb[18];
z[27] = abb[20];
z[28] = abb[23];
z[29] = abb[24];
z[30] = abb[29];
z[31] = abb[30];
z[32] = T(2) * z[8];
z[33] = z[22] + z[32];
z[34] = T(2) * z[6];
z[35] = z[33] + -z[34];
z[36] = T(2) * z[9];
z[37] = z[23] + z[36];
z[38] = T(3) * z[29];
z[39] = T(-4) * z[5] + -z[7] + z[35] + -z[37] + z[38];
z[40] = int_to_imaginary<T>(1) * z[10];
z[39] = z[39] * z[40];
z[41] = z[6] + -z[8];
z[42] = z[5] + z[41];
z[43] = -z[7] + z[9] + T(2) * z[42];
z[43] = z[7] * z[43];
z[44] = T(3) * z[28];
z[43] = z[43] + -z[44];
z[45] = z[23] / T(2);
z[46] = -z[22] + z[41];
z[47] = z[45] + -z[46];
z[47] = z[23] * z[47];
z[48] = z[8] + -z[22];
z[49] = z[6] / T(2);
z[50] = -z[48] + z[49];
z[50] = z[6] * z[50];
z[51] = z[5] + -z[23];
z[52] = z[34] + z[51];
z[53] = z[48] + z[52];
z[53] = z[5] * z[53];
z[54] = T(2) * z[41];
z[55] = z[5] + z[54];
z[56] = z[9] / T(2);
z[57] = -z[55] + -z[56];
z[57] = z[9] * z[57];
z[58] = T(3) * z[27];
z[59] = prod_pow(z[22], 2);
z[60] = z[59] / T(2);
z[61] = T(2) * z[25];
z[62] = prod_pow(z[10], 2);
z[63] = z[8] + z[22];
z[63] = -(z[8] * z[63]);
z[39] = -z[11] + z[39] + z[43] + z[47] + z[50] + z[53] + z[57] + -z[58] + -z[60] + z[61] + (T(-4) * z[62]) / T(3) + z[63];
z[39] = z[19] * z[39];
z[50] = T(2) * z[5];
z[53] = T(3) * z[7] + -z[50];
z[57] = T(4) * z[6];
z[63] = T(5) * z[23];
z[64] = T(3) * z[8];
z[65] = z[9] + -z[53] + -z[57] + -z[63] + z[64];
z[65] = z[7] * z[65];
z[66] = T(4) * z[23];
z[67] = T(3) * z[6];
z[68] = T(3) * z[22];
z[69] = T(4) * z[8] + -z[66] + -z[67] + z[68];
z[69] = z[23] * z[69];
z[70] = prod_pow(z[8], 2);
z[71] = z[61] + (T(3) * z[70]) / T(2);
z[72] = -z[32] + z[68];
z[73] = z[34] + -z[72];
z[73] = z[6] * z[73];
z[57] = (T(9) * z[5]) / T(2) + T(-7) * z[8] + z[23] + z[57] + -z[68];
z[57] = z[5] * z[57];
z[68] = z[5] + z[6];
z[74] = -z[68] + z[72];
z[75] = z[56] + z[74];
z[75] = z[9] * z[75];
z[57] = -z[24] + z[44] + z[57] + (T(109) * z[62]) / T(24) + z[65] + z[69] + z[71] + z[73] + z[75];
z[57] = z[17] * z[57];
z[65] = z[62] / T(2);
z[69] = -z[61] + z[65];
z[73] = (T(3) * z[7]) / T(2);
z[52] = -z[9] + -z[52] + z[73];
z[52] = z[7] * z[52];
z[75] = z[6] + -z[29];
z[75] = z[40] * z[75];
z[76] = z[6] + -z[22];
z[77] = z[5] + z[76];
z[77] = z[9] * z[77];
z[78] = z[22] + z[49];
z[78] = z[6] * z[78];
z[79] = -(z[23] * z[76]);
z[80] = -z[22] + z[51];
z[80] = z[5] * z[80];
z[52] = z[24] + z[44] + z[52] + -z[69] + T(3) * z[75] + z[77] + z[78] + z[79] + z[80];
z[52] = z[16] * z[52];
z[75] = T(8) * z[23];
z[78] = T(2) * z[22];
z[79] = (T(-5) * z[5]) / T(2) + z[67] + z[75] + -z[78];
z[79] = z[5] * z[79];
z[80] = -z[8] + (T(-7) * z[23]) / T(2) + -z[34] + z[78];
z[80] = z[23] * z[80];
z[81] = T(3) * z[26];
z[82] = -z[8] / T(2) + z[22];
z[82] = z[8] * z[82];
z[78] = (T(3) * z[6]) / T(2) + -z[8] + -z[78];
z[78] = z[6] * z[78];
z[60] = T(-6) * z[25] + z[60] + -z[77] + z[78] + z[79] + z[80] + z[81] + z[82];
z[60] = z[15] * z[60];
z[77] = T(5) * z[5];
z[78] = T(-3) * z[23] + -z[32] + -z[76] + z[77];
z[78] = z[5] * z[78];
z[73] = T(-7) * z[5] + z[64] + z[66] + z[73];
z[73] = z[7] * z[73];
z[47] = T(3) * z[24] + z[47] + z[81];
z[35] = z[6] * z[35];
z[33] = -z[9] + -z[33] + z[67];
z[33] = z[9] * z[33];
z[33] = z[33] + z[35] + -z[47] + z[71] + z[73] + z[78];
z[33] = z[14] * z[33];
z[35] = z[6] + T(9) * z[7] + z[36] + -z[38] + -z[64] + z[75] + -z[77];
z[35] = z[17] * z[35];
z[53] = -z[34] + z[37] + -z[53] + -z[64];
z[53] = z[14] * z[53];
z[38] = z[38] + -z[50];
z[37] = T(-8) * z[6] + T(3) * z[31] + -z[32] + z[37] + z[38];
z[37] = z[21] * z[37];
z[64] = T(3) * z[20];
z[71] = -z[29] + -z[31] + z[68];
z[71] = z[64] * z[71];
z[73] = T(7) * z[23];
z[75] = T(-9) * z[5] + z[8] + z[73] + z[76];
z[75] = z[15] * z[75];
z[77] = T(4) * z[4];
z[78] = z[5] * z[77];
z[79] = T(8) * z[15] + z[21] + -z[77];
z[79] = z[7] * z[79];
z[35] = z[35] + z[37] + z[53] + z[71] + z[75] + z[78] + z[79];
z[35] = z[35] * z[40];
z[37] = (T(3) * z[59]) / T(2) + -z[70] + z[81];
z[36] = -z[7] + z[36] + z[38] + -z[54];
z[36] = z[36] * z[40];
z[38] = z[49] + -z[72];
z[38] = z[6] * z[38];
z[53] = -z[9] + z[74];
z[53] = z[9] * z[53];
z[54] = -(z[5] * z[55]);
z[36] = z[36] + z[37] + z[38] + z[43] + z[53] + z[54] + (T(11) * z[62]) / T(12);
z[36] = z[18] * z[36];
z[38] = z[40] * z[51];
z[40] = -z[22] + z[49];
z[43] = z[40] * z[67];
z[49] = z[5] / T(2) + -z[23];
z[51] = z[46] + z[49];
z[51] = z[5] * z[51];
z[53] = z[56] + -z[76];
z[53] = z[9] * z[53];
z[43] = z[38] + z[43] + z[47] + z[51] + T(3) * z[53] + -z[69];
z[43] = z[13] * z[43];
z[47] = z[46] + -z[50] + z[63];
z[47] = z[5] * z[47];
z[34] = T(-2) * z[23] + z[34] + -z[48];
z[34] = z[23] * z[34];
z[48] = T(2) * z[48];
z[51] = z[6] + z[48];
z[51] = z[6] * z[51];
z[48] = (T(-5) * z[9]) / T(2) + -z[48];
z[48] = z[9] * z[48];
z[34] = T(-7) * z[24] + T(-4) * z[25] + z[34] + -z[37] + -z[44] + z[47] + z[48] + z[51];
z[34] = z[21] * z[34];
z[32] = z[32] + z[50] + -z[66] + z[67];
z[32] = z[21] * z[32];
z[37] = z[23] + -z[68];
z[37] = z[37] * z[64];
z[41] = -z[9] + z[41];
z[44] = T(6) * z[5] + -z[41] + -z[73];
z[44] = z[15] * z[44];
z[47] = T(2) * z[4];
z[42] = -z[9] + z[42];
z[42] = z[42] * z[47];
z[48] = T(2) * z[21];
z[51] = T(-4) * z[15] + (T(3) * z[20]) / T(2) + z[47] + -z[48];
z[51] = z[7] * z[51];
z[32] = z[32] + z[37] + z[42] + z[44] + z[51];
z[32] = z[7] * z[32];
z[37] = -(z[6] * z[40]);
z[40] = -z[22] + z[45];
z[40] = z[23] * z[40];
z[42] = z[22] + z[49];
z[42] = z[5] * z[42];
z[44] = z[9] * z[76];
z[37] = -z[26] + z[28] + z[37] + z[40] + z[42] + z[44];
z[37] = z[37] * z[64];
z[40] = z[23] + -z[46] + -z[50];
z[40] = z[5] * z[40];
z[42] = z[23] + z[46];
z[42] = z[23] * z[42];
z[38] = T(2) * z[38] + z[40] + z[42] + -z[61];
z[38] = z[12] * z[38];
z[40] = T(7) * z[14] + T(-5) * z[15] + -z[17] + -z[18] + -z[48] + z[64] + -z[77];
z[40] = z[11] * z[40];
z[41] = -z[41] + -z[50];
z[41] = z[5] * z[41] * z[47];
z[42] = (T(-55) * z[14]) / T(12) + (T(41) * z[15]) / T(4) + -z[20] + (T(7) * z[21]) / T(6);
z[42] = z[42] * z[65];
z[44] = z[14] + -z[15] + z[17];
z[44] = z[44] * z[58];
z[45] = -z[0] + -z[2] + T(3) * z[3];
z[45] = z[1] * z[45];
z[46] = z[20] + -z[21];
z[46] = z[30] * z[46];
return z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + T(4) * z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + T(3) * z[46] + z[52] + z[57] + z[60];
}



template IntegrandConstructorType<double> f_3_30_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_30_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_30_construct (const Kin<qd_real>&);
#endif

}