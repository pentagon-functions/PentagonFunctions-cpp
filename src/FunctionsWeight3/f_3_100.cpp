#include "f_3_100.h"

namespace PentagonFunctions {

template <typename T> T f_3_100_abbreviated (const std::array<T,37>&);

template <typename T> class SpDLog_f_3_100_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_100_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(5) / T(2) + (T(-5) * kin.v[1]) / T(2) + (T(35) * kin.v[2]) / T(4) + (T(45) * kin.v[3]) / T(4) + (T(-35) * kin.v[4]) / T(4) + (T(-45) / T(8) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[1] + T(-4) * kin.v[3])) + ((T(5) * kin.v[2]) / T(2) + (T(5) * kin.v[3]) / T(2) + (T(-5) * kin.v[4]) / T(2)) * kin.v[1] + (T(5) / T(2) + (T(-25) * kin.v[4]) / T(8)) * kin.v[4] + (T(-5) / T(2) + (T(-25) * kin.v[2]) / T(8) + (T(-35) * kin.v[3]) / T(4) + (T(25) * kin.v[4]) / T(4)) * kin.v[2] + (T(-5) / T(2) + (T(-45) * kin.v[3]) / T(8) + (T(35) * kin.v[4]) / T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(3) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + (T(-3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + (T(3) * kin.v[4]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * ((abb[5] * T(3)) / T(2) + abb[2] * (-abb[2] / T(2) + abb[4] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[1] * (abb[3] * T(-4) + abb[2] * T(4)) + abb[3] * (abb[3] / T(2) + bc<T>[0] * int_to_imaginary<T>(4) + abb[4] * T(4)));
    }
};
template <typename T> class SpDLog_f_3_100_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_100_W_7 (const Kin<T>& kin) {
        c[0] = T(-4) * kin.v[2] * kin.v[4] + T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-4) * kin.v[2] + T(6) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[3] * (T(-4) + T(-4) * kin.v[1] + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[6] * (abb[2] * abb[8] * T(-4) + abb[1] * abb[8] * T(4) + abb[8] * (abb[8] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[4] * T(4)) + abb[9] * T(8) + abb[7] * (abb[1] * T(-4) + abb[4] * T(-4) + abb[8] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * T(4) + abb[7] * T(8)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_100_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl7 = DLog_W_7<T>(kin),dl5 = DLog_W_5<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl25 = DLog_W_25<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),spdl23 = SpDLog_f_3_100_W_23<T>(kin),spdl7 = SpDLog_f_3_100_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,37> abbr = 
            {dl23(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_8(kin_path), dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), dl5(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl4(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl2(t), f_2_1_12(kin_path), f_2_1_15(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl25(t), dl16(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl19(t), dl17(t), dl1(t), dl20(t), dl29(t) / kin_path.SqrtDelta, f_2_2_5(kin_path), f_1_3_1(kin_path), f_1_3_3(kin_path), f_1_3_5(kin_path), dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_3_100_abbreviated(abbr);
        result = result + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_3_100_abbreviated(const std::array<T,37>& abb)
{
using TR = typename T::value_type;
T z[80];
z[0] = abb[1];
z[1] = abb[12];
z[2] = abb[15];
z[3] = abb[26];
z[4] = abb[28];
z[5] = abb[29];
z[6] = abb[2];
z[7] = abb[10];
z[8] = abb[20];
z[9] = abb[23];
z[10] = abb[3];
z[11] = abb[27];
z[12] = abb[4];
z[13] = abb[7];
z[14] = abb[8];
z[15] = bc<TR>[0];
z[16] = abb[19];
z[17] = abb[11];
z[18] = abb[5];
z[19] = abb[9];
z[20] = abb[13];
z[21] = abb[14];
z[22] = abb[16];
z[23] = abb[17];
z[24] = abb[18];
z[25] = abb[21];
z[26] = abb[22];
z[27] = abb[24];
z[28] = abb[25];
z[29] = abb[30];
z[30] = abb[31];
z[31] = abb[32];
z[32] = abb[33];
z[33] = abb[34];
z[34] = abb[35];
z[35] = abb[36];
z[36] = T(3) * z[1];
z[37] = T(2) * z[7];
z[38] = z[36] + -z[37];
z[39] = z[8] + -z[16];
z[39] = T(2) * z[39];
z[40] = T(4) * z[9];
z[41] = T(3) * z[4];
z[42] = z[5] + -z[38] + -z[39] + -z[40] + z[41];
z[42] = z[10] * z[42];
z[43] = T(2) * z[8];
z[44] = -z[5] + z[43];
z[45] = T(2) * z[9];
z[44] = -z[1] + -z[37] + -z[41] + T(3) * z[44] + z[45];
z[44] = z[13] * z[44];
z[46] = T(2) * z[16];
z[37] = z[37] + -z[41] + z[46];
z[47] = T(2) * z[11];
z[48] = T(3) * z[3] + z[47];
z[49] = T(2) * z[5];
z[50] = T(7) * z[9] + z[49];
z[51] = T(5) * z[8];
z[52] = -z[37] + -z[48] + z[50] + z[51];
z[52] = z[6] * z[52];
z[53] = T(4) * z[16];
z[54] = T(3) * z[5];
z[55] = T(3) * z[9];
z[56] = z[8] + -z[53] + -z[54] + z[55];
z[56] = z[17] * z[56];
z[57] = z[1] + -z[5];
z[58] = z[8] + z[57];
z[37] = z[37] + z[45] + z[58];
z[37] = z[14] * z[37];
z[39] = z[39] + z[54] + z[55];
z[48] = -z[39] + z[48];
z[48] = z[12] * z[48];
z[59] = z[13] + -z[24];
z[60] = T(3) * z[21];
z[61] = T(2) * z[10];
z[59] = -z[0] + T(-2) * z[12] + z[14] + T(8) * z[17] + T(5) * z[59] + z[60] + -z[61];
z[59] = z[2] * z[59];
z[62] = -z[31] + z[32] + -z[33];
z[62] = T(-2) * z[62];
z[63] = z[29] + z[34];
z[64] = T(3) * z[35] + T(-2) * z[63];
z[62] = z[62] * z[64];
z[65] = z[8] + z[46];
z[65] = T(7) * z[5] + -z[9] + z[41] + T(-2) * z[65];
z[66] = z[24] * z[65];
z[67] = -z[8] + z[11];
z[68] = -z[4] + z[57];
z[69] = z[67] + z[68];
z[60] = z[60] * z[69];
z[70] = T(-4) * z[10] + -z[14] + -z[17] + -z[24];
z[70] = z[11] * z[70];
z[71] = z[10] + z[17];
z[72] = -z[13] + -z[24] + -z[71];
z[72] = z[3] * z[72];
z[73] = z[3] + z[11];
z[74] = -z[4] + -z[9] + z[73];
z[74] = T(3) * z[74];
z[75] = z[28] * z[74];
z[67] = z[3] + T(-3) * z[67];
z[67] = z[0] * z[67];
z[76] = z[4] + -z[8];
z[76] = T(3) * z[76];
z[77] = z[26] * z[76];
z[37] = z[37] + z[42] + z[44] + z[48] + z[52] + z[56] + z[59] + z[60] + z[62] + z[66] + z[67] + z[70] + z[72] + z[75] + z[77];
z[37] = int_to_imaginary<T>(1) * z[37];
z[42] = z[1] / T(2);
z[44] = (T(-23) * z[5]) / T(8) + -z[8] / T(4) + T(-10) * z[16];
z[44] = (T(107) * z[2]) / T(24) + -z[3] / T(6) + (T(35) * z[9]) / T(24) + (T(-5) * z[11]) / T(12) + T(-4) * z[35] + z[42] + z[44] / T(3) + (T(8) * z[63]) / T(3);
z[44] = z[15] * z[44];
z[37] = z[37] + z[44];
z[37] = z[15] * z[37];
z[43] = -z[7] + z[43];
z[44] = z[43] + -z[55] + -z[57];
z[44] = z[13] * z[44];
z[48] = T(3) * z[68];
z[52] = z[7] + -z[9];
z[56] = -z[48] + z[52];
z[56] = z[10] * z[56];
z[59] = z[40] + -z[46];
z[60] = -z[7] + z[41];
z[62] = z[59] + z[60];
z[63] = -(z[17] * z[62]);
z[66] = z[58] + z[59];
z[66] = z[14] * z[66];
z[67] = z[3] + -z[5] + z[51];
z[67] = -z[7] + -z[11] + z[45] + z[67] / T(2);
z[67] = z[6] * z[67];
z[68] = T(2) * z[14];
z[70] = -z[10] + T(2) * z[17] + -z[68];
z[70] = z[11] * z[70];
z[72] = -z[10] + z[17];
z[75] = z[14] + z[72];
z[75] = -z[13] + T(2) * z[75];
z[75] = z[3] * z[75];
z[77] = z[5] + z[9];
z[78] = z[8] + z[77];
z[79] = z[11] + z[78];
z[79] = -z[3] + T(2) * z[79];
z[79] = z[12] * z[79];
z[44] = z[44] + z[56] + z[63] + z[66] + z[67] + z[70] + z[75] + z[79];
z[44] = z[6] * z[44];
z[39] = z[17] * z[39];
z[56] = -(z[61] * z[78]);
z[50] = -z[1] + -z[8] + z[46] + -z[50];
z[50] = z[14] * z[50];
z[40] = -z[8] + z[40] + z[57];
z[40] = z[13] * z[40];
z[57] = z[14] + -z[71];
z[47] = z[47] * z[57];
z[57] = z[8] + -z[73] + (T(3) * z[77]) / T(2);
z[57] = z[12] * z[57];
z[63] = z[13] + z[14] + T(-2) * z[72];
z[63] = z[3] * z[63];
z[39] = z[39] + z[40] + z[47] + z[50] + z[56] + z[57] + z[63];
z[39] = z[12] * z[39];
z[40] = z[12] + T(2) * z[13];
z[47] = T(3) * z[14];
z[50] = -z[40] + z[47] + -z[71];
z[50] = z[6] * z[50];
z[47] = z[47] + z[72];
z[47] = z[14] * z[47];
z[56] = z[13] / T(2);
z[57] = T(-7) * z[17] + -z[56] + z[68];
z[57] = z[13] * z[57];
z[40] = -z[0] + z[6] + z[10] + z[40] + -z[68];
z[40] = z[0] * z[40];
z[63] = T(3) * z[23];
z[66] = z[10] + (T(-3) * z[17]) / T(2);
z[66] = z[17] * z[66];
z[67] = -z[12] / T(2) + -z[13] + z[17];
z[67] = z[12] * z[67];
z[40] = T(6) * z[19] + z[40] + z[47] + z[50] + z[57] + -z[63] + z[66] + z[67];
z[40] = z[2] * z[40];
z[47] = z[14] / T(2);
z[50] = z[47] + z[72];
z[50] = z[14] * z[50];
z[57] = -z[17] + z[61];
z[57] = z[17] * z[57];
z[61] = T(2) * z[19];
z[66] = prod_pow(z[10], 2);
z[56] = z[14] + -z[56];
z[56] = z[13] * z[56];
z[50] = z[50] + z[56] + -z[57] + -z[61] + -z[63] + z[66];
z[50] = z[3] * z[50];
z[43] = -z[5] + z[43];
z[36] = T(3) * z[11] + z[36] + -z[41] + z[43] + z[55];
z[36] = z[10] * z[36];
z[56] = -z[55] + -z[58] + z[60];
z[58] = z[13] + -z[14];
z[60] = -(z[56] * z[58]);
z[51] = z[3] + z[7] + -z[49] + -z[51] + -z[55];
z[51] = z[6] * z[51];
z[55] = -z[12] + z[58];
z[55] = z[3] * z[55];
z[48] = -z[3] + -z[48];
z[48] = z[0] * z[48];
z[36] = z[36] + z[48] / T(2) + z[51] + z[55] + z[60];
z[36] = z[0] * z[36];
z[48] = T(5) * z[9];
z[51] = z[5] + -z[8];
z[51] = z[7] + z[41] + -z[48] + T(4) * z[51];
z[51] = z[17] * z[51];
z[55] = -(z[14] * z[56]);
z[41] = -z[1] + z[41] + -z[54];
z[41] = -z[7] + z[41] / T(2) + -z[48];
z[41] = z[13] * z[41];
z[41] = z[41] + z[51] + z[55];
z[41] = z[13] * z[41];
z[48] = (T(3) * z[4]) / T(2);
z[49] = z[8] / T(2) + -z[9] + -z[48] + -z[49] + z[53];
z[49] = z[17] * z[49];
z[51] = z[10] * z[62];
z[49] = z[49] + z[51];
z[49] = z[17] * z[49];
z[46] = z[46] + z[52];
z[46] = -(z[46] * z[72]);
z[42] = z[5] / T(2) + z[8] + -z[42] + z[59];
z[42] = z[14] * z[42];
z[42] = z[42] + z[46];
z[42] = z[14] * z[42];
z[46] = -z[47] + z[72];
z[46] = z[14] * z[46];
z[46] = z[46] + -z[57] + z[66] / T(2);
z[46] = z[11] * z[46];
z[38] = (T(-5) * z[8]) / T(2) + z[38] + z[45] + -z[54];
z[38] = z[18] * z[38];
z[43] = -z[1] + -z[9] + z[43];
z[43] = z[43] * z[61];
z[45] = (T(3) * z[1]) / T(2) + -z[48] + -z[78];
z[45] = z[45] * z[66];
z[47] = z[63] * z[77];
z[48] = T(5) * z[2] + -z[65] + z[73];
z[48] = z[22] * z[48];
z[51] = -z[2] + -z[69];
z[51] = z[20] * z[51];
z[52] = z[27] * z[74];
z[53] = z[25] * z[76];
z[54] = -(z[30] * z[64]);
return z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + T(3) * z[51] + z[52] + z[53] + z[54];
}



template IntegrandConstructorType<double> f_3_100_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_100_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_100_construct (const Kin<qd_real>&);
#endif

}