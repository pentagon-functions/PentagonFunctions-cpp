#include "f_3_61.h"

namespace PentagonFunctions {

template <typename T> T f_3_61_abbreviated (const std::array<T,18>&);



template <typename T> IntegrandConstructorType<T> f_3_61_construct (const Kin<T>& kin) {
    return [&kin, 
            dl13 = DLog_W_13<T>(kin),dl25 = DLog_W_25<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,18> abbr = 
            {dl13(t), rlog(v_path[0]), rlog(v_path[2]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), dl1(t), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_1(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl5(t), dl18(t), f_2_1_12(kin_path), f_2_1_15(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl20(t), dl2(t)}
;

        auto result = f_3_61_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_61_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[31];
z[0] = abb[0];
z[1] = abb[1];
z[2] = bc<TR>[0];
z[3] = abb[2];
z[4] = abb[6];
z[5] = abb[11];
z[6] = abb[16];
z[7] = abb[7];
z[8] = abb[3];
z[9] = abb[4];
z[10] = abb[5];
z[11] = abb[12];
z[12] = abb[17];
z[13] = abb[8];
z[14] = abb[9];
z[15] = abb[10];
z[16] = abb[13];
z[17] = abb[14];
z[18] = abb[15];
z[19] = -z[11] + z[12];
z[20] = -(z[10] * z[19]);
z[21] = T(2) * z[11];
z[22] = z[6] + z[21];
z[23] = -z[12] + z[22];
z[24] = -(z[9] * z[23]);
z[25] = T(2) * z[4];
z[26] = z[5] + -z[6] + -z[25];
z[27] = -(z[1] * z[26]);
z[28] = z[4] + -z[5];
z[29] = -(z[3] * z[28]);
z[30] = z[4] + -z[11];
z[30] = z[7] * z[30];
z[20] = z[20] + z[24] + z[27] + z[29] + z[30] / T(2);
z[20] = z[7] * z[20];
z[22] = -z[8] + z[22];
z[22] = z[9] * z[22];
z[24] = z[0] + -z[4];
z[27] = z[3] * z[24];
z[22] = z[22] + -z[27];
z[27] = z[5] + z[6];
z[25] = T(2) * z[0] + -z[25] + -z[27];
z[25] = z[1] * z[25];
z[29] = -(z[18] * z[23]);
z[21] = -z[4] + -z[12] + z[21];
z[21] = z[7] * z[21];
z[30] = -(z[15] * z[28]);
z[21] = z[21] + T(2) * z[22] + z[25] + z[29] + z[30];
z[21] = int_to_imaginary<T>(1) * z[21];
z[22] = z[6] + z[12];
z[25] = T(-5) * z[4] + T(11) * z[11] + z[22];
z[25] = -z[8] + z[25] / T(6);
z[25] = z[2] * z[25];
z[21] = z[21] + z[25];
z[21] = z[2] * z[21];
z[24] = -z[24] + z[27] / T(2);
z[24] = prod_pow(z[1], 2) * z[24];
z[19] = z[17] * z[19];
z[25] = z[14] * z[28];
z[27] = z[11] + z[12];
z[27] = -z[8] + z[27] / T(2);
z[27] = prod_pow(z[10], 2) * z[27];
z[22] = z[8] + -z[11] + -z[22] / T(2);
z[22] = prod_pow(z[9], 2) * z[22];
z[28] = -z[4] + -z[5];
z[28] = z[0] + z[28] / T(2);
z[28] = prod_pow(z[3], 2) * z[28];
z[23] = z[16] * z[23];
z[26] = z[13] * z[26];
return z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28];
}



template IntegrandConstructorType<double> f_3_61_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_61_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_61_construct (const Kin<qd_real>&);
#endif

}