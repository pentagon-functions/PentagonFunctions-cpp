#include "f_3_79.h"

namespace PentagonFunctions {

template <typename T> T f_3_79_abbreviated (const std::array<T,6>&);



template <typename T> IntegrandConstructorType<T> f_3_79_construct (const Kin<T>& kin) {
    return [&kin, 
            dl25 = DLog_W_25<T>(kin),dl18 = DLog_W_18<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,6> abbr = 
            {dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl18(t)}
;

        auto result = f_3_79_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_79_abbreviated(const std::array<T,6>& abb)
{
using TR = typename T::value_type;
T z[10];
z[0] = abb[0];
z[1] = abb[1];
z[2] = bc<TR>[0];
z[3] = abb[2];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = prod_pow(z[1], 2);
z[8] = prod_pow(z[3], 2);
z[7] = z[7] + -z[8];
z[8] = T(3) * z[1] + -z[5];
z[8] = int_to_imaginary<T>(1) * z[8];
z[8] = (T(7) * z[2]) / T(6) + z[8];
z[8] = z[2] * z[8];
z[8] = z[4] + -z[7] + z[8];
z[8] = z[6] * z[8];
z[9] = -z[1] + -z[5];
z[9] = int_to_imaginary<T>(1) * z[9];
z[9] = (T(-5) * z[2]) / T(6) + z[9];
z[9] = z[2] * z[9];
z[7] = z[4] + z[7] + z[9];
z[7] = z[0] * z[7];
z[7] = z[7] + z[8];
return z[7] / T(2);
}



template IntegrandConstructorType<double> f_3_79_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_79_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_79_construct (const Kin<qd_real>&);
#endif

}