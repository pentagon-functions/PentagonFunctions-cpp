#include "f_3_67.h"

namespace PentagonFunctions {

template <typename T> T f_3_67_abbreviated (const std::array<T,32>&);

template <typename T> class SpDLog_f_3_67_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_67_W_21 (const Kin<T>& kin) {
        c[0] = (T(23) / T(2) + (T(69) * kin.v[1]) / T(8) + (T(-23) * kin.v[2]) / T(4) + (T(-69) * kin.v[3]) / T(4) + (T(-23) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(23) / T(2) + (T(23) * kin.v[1]) / T(4) + (T(23) * kin.v[2]) / T(4) + (T(-23) * kin.v[3]) / T(4) + (T(-23) * kin.v[4]) / T(2) + (T(-23) / T(8) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + (T(-23) / T(2) + (T(-23) * kin.v[2]) / T(8) + (T(23) * kin.v[3]) / T(4) + (T(23) * kin.v[4]) / T(2)) * kin.v[2] + (T(-23) / T(2) + (T(69) * kin.v[3]) / T(8) + (T(23) * kin.v[4]) / T(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(9) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + (T(9) * kin.v[1]) / T(2) + (T(-9) * kin.v[2]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + (T(-9) * kin.v[3]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[18] * ((abb[17] * T(-9)) / T(2) + abb[13] * ((abb[13] * T(5)) / T(4) + (abb[9] * T(9)) / T(2) + abb[8] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[9] * ((abb[9] * T(-23)) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[8] * abb[9] * T(4) + abb[6] * (abb[9] * T(-4) + abb[13] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_67_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl11 = DLog_W_11<T>(kin),dl6 = DLog_W_6<T>(kin),dl3 = DLog_W_3<T>(kin),dl21 = DLog_W_21<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),spdl21 = SpDLog_f_3_67_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,32> abbr = 
            {dl26(t) / kin_path.SqrtDelta, f_2_2_9(kin_path), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl11(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_2(kin_path), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_9(kin_path), dl3(t), f_2_1_14(kin_path), f_2_1_15(kin_path), dl21(t), dl4(t), f_2_1_1(kin_path), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl16(t), dl19(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t), dl1(t), dl17(t), dl20(t), dl18(t)}
;

        auto result = f_3_67_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_3_67_abbreviated(const std::array<T,32>& abb)
{
using TR = typename T::value_type;
T z[65];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = bc<TR>[0];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[19];
z[14] = abb[27];
z[15] = abb[29];
z[16] = abb[31];
z[17] = abb[15];
z[18] = abb[23];
z[19] = abb[28];
z[20] = abb[30];
z[21] = abb[12];
z[22] = abb[13];
z[23] = abb[24];
z[24] = abb[14];
z[25] = abb[16];
z[26] = abb[17];
z[27] = abb[20];
z[28] = abb[21];
z[29] = abb[22];
z[30] = abb[25];
z[31] = abb[26];
z[32] = -z[14] + z[15];
z[33] = T(3) * z[32];
z[34] = T(4) * z[4];
z[35] = -z[13] + z[16];
z[36] = -z[33] + -z[34] + -z[35];
z[36] = z[5] * z[36];
z[37] = T(2) * z[17];
z[38] = -z[18] + z[37];
z[39] = z[23] + z[38];
z[40] = T(3) * z[16];
z[41] = z[13] + z[14];
z[42] = T(-7) * z[20] + -z[39] + -z[40] + T(3) * z[41];
z[42] = z[9] * z[42];
z[43] = T(2) * z[23];
z[44] = T(2) * z[20];
z[45] = -z[17] + z[35] + z[43] + -z[44];
z[45] = z[6] * z[45];
z[46] = T(3) * z[18];
z[47] = T(3) * z[20];
z[48] = T(5) * z[13] + -z[17] + z[34] + -z[46] + z[47];
z[48] = z[7] * z[48];
z[38] = z[38] + z[43] + z[47];
z[43] = z[22] * z[38];
z[49] = z[16] + z[17];
z[50] = -z[47] + z[49];
z[50] = z[21] * z[50];
z[51] = z[21] + -z[31];
z[52] = T(3) * z[23];
z[53] = z[51] * z[52];
z[54] = T(3) * z[19];
z[51] = -z[7] + z[29] + z[51];
z[51] = z[51] * z[54];
z[55] = z[5] + -z[21];
z[56] = z[12] * z[55];
z[57] = z[16] + z[32];
z[58] = z[20] + z[57];
z[58] = z[31] * z[58];
z[41] = -z[18] + z[41];
z[41] = T(3) * z[41];
z[59] = -(z[29] * z[41]);
z[60] = -z[20] + z[23];
z[61] = -(z[8] * z[60]);
z[36] = z[36] + z[42] + z[43] + z[45] + z[48] + z[50] + z[51] + z[53] + T(4) * z[56] + T(3) * z[58] + z[59] + z[61];
z[36] = int_to_imaginary<T>(1) * z[10] * z[36];
z[42] = T(2) * z[4];
z[43] = z[17] + z[42];
z[45] = T(2) * z[16];
z[48] = -z[32] + -z[43] + z[45];
z[48] = z[5] * z[48];
z[50] = -z[14] + z[20];
z[51] = z[17] / T(2);
z[50] = -z[13] + -z[42] + (T(-3) * z[50]) / T(2) + z[51];
z[50] = z[7] * z[50];
z[53] = z[32] + z[35];
z[53] = z[21] * z[53];
z[58] = -z[37] + z[44];
z[42] = z[42] + z[58];
z[59] = T(2) * z[13];
z[61] = z[14] + z[15] + -z[16] + -z[42] + -z[59];
z[61] = z[9] * z[61];
z[40] = z[13] + -z[40] + z[43];
z[40] = z[6] * z[40];
z[40] = z[40] + z[48] + z[50] + z[53] + z[61];
z[40] = z[7] * z[40];
z[43] = z[14] + T(4) * z[15] + z[35] + -z[52] + -z[58];
z[43] = -(z[43] * z[55]);
z[38] = -(z[6] * z[38]);
z[48] = -z[17] + z[32];
z[50] = z[18] + z[23] + z[35] + z[44] + -z[48];
z[50] = z[8] * z[50];
z[53] = z[23] / T(2);
z[58] = T(5) * z[14];
z[61] = -z[18] + (T(9) * z[20]) / T(2) + -z[58];
z[61] = z[16] + -z[53] + z[61] / T(2);
z[61] = z[22] * z[61];
z[56] = T(2) * z[56];
z[62] = T(4) * z[14] + z[15];
z[63] = z[13] + T(-6) * z[16] + -z[17] + (T(-19) * z[20]) / T(2) + z[62];
z[63] = z[9] * z[63];
z[38] = z[38] + z[43] + z[50] + z[56] + z[61] + z[63];
z[38] = z[22] * z[38];
z[43] = z[13] / T(2);
z[50] = -z[16] / T(2) + z[43] + z[51] + -z[60];
z[50] = z[6] * z[50];
z[51] = T(3) * z[13];
z[61] = -z[49] + z[51];
z[61] = z[21] * z[61];
z[63] = -z[4] + z[45] + -z[59];
z[64] = T(2) * z[5];
z[63] = z[63] * z[64];
z[39] = T(4) * z[20] + z[39];
z[39] = z[9] * z[39];
z[39] = z[39] + z[50] + z[56] + z[61] + z[63];
z[39] = z[6] * z[39];
z[42] = z[35] + z[42] + z[46] + -z[62];
z[50] = -z[5] + z[7];
z[42] = z[42] * z[50];
z[61] = -z[15] + z[18];
z[47] = -z[14] + -z[16] + T(-3) * z[17] + -z[47] + z[51] + z[61];
z[47] = z[9] * z[47];
z[51] = -z[17] + z[61];
z[43] = z[20] / T(2) + z[43] + -z[51] + -z[53];
z[43] = z[8] * z[43];
z[53] = z[6] * z[60];
z[42] = z[42] + z[43] + z[47] + z[53];
z[42] = z[8] * z[42];
z[43] = z[13] + T(5) * z[20] + -z[37] + z[45] + -z[62];
z[43] = z[21] * z[43];
z[47] = z[4] + z[35];
z[47] = z[47] * z[64];
z[53] = (T(7) * z[16]) / T(2) + -z[18] / T(2) + (T(25) * z[20]) / T(4) + z[37] + -z[59];
z[53] = z[9] * z[53];
z[43] = z[43] + z[47] + z[53] + -z[56];
z[43] = z[9] * z[43];
z[44] = z[13] + -z[44] + z[49];
z[46] = z[34] + -z[44] + z[46] + -z[58];
z[46] = z[11] * z[46];
z[47] = -z[48] + -z[59];
z[47] = z[21] * z[47];
z[32] = z[13] + -z[16] + z[32];
z[32] = (T(5) * z[32]) / T(2) + z[34];
z[32] = z[5] * z[32];
z[32] = z[32] + z[47];
z[32] = z[5] * z[32];
z[33] = -z[17] + -z[33];
z[33] = z[33] / T(2) + -z[45];
z[34] = prod_pow(z[21], 2);
z[33] = z[33] * z[34];
z[45] = z[21] + -z[64];
z[45] = z[5] * z[45];
z[34] = z[34] + z[45];
z[45] = T(2) * z[12];
z[34] = z[34] * z[45];
z[45] = -z[13] + -z[51] + -z[60];
z[45] = z[25] * z[45];
z[47] = -z[57] + z[60];
z[47] = z[30] * z[47];
z[44] = T(-4) * z[12] + T(5) * z[15] + z[44] + -z[52];
z[44] = z[24] * z[44];
z[37] = T(5) * z[16] + T(-2) * z[18] + (T(21) * z[20]) / T(2) + z[23] + z[37] + -z[58];
z[37] = z[26] * z[37];
z[48] = -z[8] / T(2) + z[50];
z[48] = z[8] * z[48];
z[49] = z[22] / T(2) + z[55];
z[49] = z[22] * z[49];
z[48] = z[11] + -z[24] + -z[28] + z[30] + z[48] + z[49];
z[48] = z[48] * z[54];
z[49] = -z[0] + -z[2] + T(2) * z[3];
z[49] = z[1] * z[49];
z[41] = z[28] * z[41];
z[50] = (T(37) * z[13]) / T(12) + -z[14];
z[50] = (T(41) * z[16]) / T(24) + (T(-3) * z[17]) / T(8) + -z[18] / T(3) + -z[20] / T(4) + (T(13) * z[23]) / T(12) + z[50] / T(2);
z[50] = prod_pow(z[10], 2) * z[50];
z[35] = z[27] * z[35];
return z[32] + z[33] + z[34] + T(-6) * z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + T(2) * z[45] + z[46] + T(3) * z[47] + z[48] + z[49] + z[50];
}



template IntegrandConstructorType<double> f_3_67_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_67_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_67_construct (const Kin<qd_real>&);
#endif

}