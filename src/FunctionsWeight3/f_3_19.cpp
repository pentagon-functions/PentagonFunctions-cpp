#include "f_3_19.h"

namespace PentagonFunctions {

template <typename T> T f_3_19_abbreviated (const std::array<T,10>&);



template <typename T> IntegrandConstructorType<T> f_3_19_construct (const Kin<T>& kin) {
    return [&kin, 
            dl5 = DLog_W_5<T>(kin),dl13 = DLog_W_13<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,10> abbr = 
            {dl5(t), rlog(-v_path[4]), f_2_1_1(kin_path), dl13(t), rlog(v_path[0]), rlog(v_path[2]), dl1(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl20(t)}
;

        auto result = f_3_19_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_19_abbreviated(const std::array<T,10>& abb)
{
using TR = typename T::value_type;
T z[18];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[4];
z[5] = abb[6];
z[6] = abb[9];
z[7] = abb[5];
z[8] = abb[3];
z[9] = abb[7];
z[10] = abb[8];
z[11] = -z[5] + z[6];
z[12] = z[1] * z[11];
z[13] = -z[6] + z[8];
z[14] = z[13] / T(2);
z[14] = z[7] * z[14];
z[14] = z[12] + z[14];
z[14] = z[7] * z[14];
z[13] = z[4] * z[13];
z[12] = -z[12] + -z[13] / T(2);
z[12] = z[4] * z[12];
z[15] = -(z[9] * z[11]);
z[16] = -(z[0] * prod_pow(z[1], 2));
z[17] = -z[0] + (T(-3) * z[5]) / T(2) + z[6] + z[8] / T(2);
z[17] = z[2] * z[17];
z[12] = z[12] + z[14] + z[15] + z[16] + z[17];
z[11] = z[10] * z[11];
z[14] = z[5] + -z[8];
z[14] = z[7] * z[14];
z[11] = z[11] + z[13] + z[14];
z[11] = int_to_imaginary<T>(1) * z[11];
z[13] = z[0] * z[3];
z[11] = T(3) * z[11] + z[13] / T(4);
z[11] = z[3] * z[11];
return z[11] + T(3) * z[12];
}



template IntegrandConstructorType<double> f_3_19_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_19_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_19_construct (const Kin<qd_real>&);
#endif

}