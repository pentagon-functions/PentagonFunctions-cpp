#include "f_3_74.h"

namespace PentagonFunctions {

template <typename T> T f_3_74_abbreviated (const std::array<T,31>&);

template <typename T> class SpDLog_f_3_74_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_74_W_12 (const Kin<T>& kin) {
        c[0] = T(-2) * kin.v[0] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + T(4) * kin.v[3] * kin.v[4] + -prod_pow(kin.v[4], 2) + kin.v[1] * (-kin.v[1] + T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-4) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[18] * (abb[10] * T(-4) + abb[2] * abb[19] * T(-2) + abb[2] * (abb[4] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[2] * T(2) + abb[3] * T(2)) + abb[1] * (abb[1] * T(-4) + abb[3] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[2] * T(2) + abb[4] * T(2) + abb[19] * T(2)));
    }
};
template <typename T> class SpDLog_f_3_74_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_74_W_23 (const Kin<T>& kin) {
        c[0] = (T(-5) / T(2) + (T(39) * kin.v[2]) / T(8) + (T(29) * kin.v[3]) / T(4) + (T(-39) * kin.v[4]) / T(4)) * kin.v[2] + (T(-5) / T(2) + (T(19) * kin.v[3]) / T(8) + (T(-29) * kin.v[4]) / T(4)) * kin.v[3] + ((T(5) * kin.v[2]) / T(2) + (T(5) * kin.v[3]) / T(2) + (T(-5) * kin.v[4]) / T(2)) * kin.v[1] + (T(5) / T(2) + (T(39) * kin.v[4]) / T(8)) * kin.v[4] + kin.v[0] * (T(5) / T(2) + (T(-5) * kin.v[1]) / T(2) + (T(-29) * kin.v[2]) / T(4) + (T(-19) * kin.v[3]) / T(4) + (T(29) * kin.v[4]) / T(4) + (T(19) / T(8) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[1] + T(4) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = (T(3) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + (T(-3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[20] * ((abb[7] * T(3)) / T(2) + abb[4] * (abb[4] / T(2) + abb[6] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[19] * (abb[3] * T(-4) + abb[4] * T(4)) + abb[3] * (-abb[3] / T(2) + bc<T>[0] * int_to_imaginary<T>(4) + abb[6] * T(4)));
    }
};
template <typename T> class SpDLog_f_3_74_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_74_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(4) * kin.v[2] + T(-6) * kin.v[3] + T(-8) * kin.v[4]) + T(4) * kin.v[2] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[3] * (T(4) + T(4) * kin.v[1] + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[24] * (abb[8] * T(-8) + abb[5] * abb[19] * T(-4) + abb[3] * abb[5] * T(4) + abb[5] * (abb[6] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[5] * T(4)) + abb[1] * (abb[1] * T(-8) + abb[3] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[5] * T(4) + abb[6] * T(4) + abb[19] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_74_construct (const Kin<T>& kin) {
    return [&kin, 
            dl18 = DLog_W_18<T>(kin),dl3 = DLog_W_3<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl12 = DLog_W_12<T>(kin),dl23 = DLog_W_23<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl7 = DLog_W_7<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl4 = DLog_W_4<T>(kin),spdl12 = SpDLog_f_3_74_W_12<T>(kin),spdl23 = SpDLog_f_3_74_W_23<T>(kin),spdl7 = SpDLog_f_3_74_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,31> abbr = 
            {dl18(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_8(kin_path), f_2_1_11(kin_path), dl3(t), f_2_1_4(kin_path), f_2_1_7(kin_path), f_2_1_14(kin_path), f_2_1_15(kin_path), dl27(t) / kin_path.SqrtDelta, f_2_2_7(kin_path), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl12(t), rlog(v_path[3]), dl23(t), dl17(t), dl20(t), dl16(t), dl7(t), dl19(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl2(t), dl5(t), dl4(t)}
;

        auto result = f_3_74_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_3_74_abbreviated(const std::array<T,31>& abb)
{
using TR = typename T::value_type;
T z[60];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[4];
z[4] = abb[5];
z[5] = abb[6];
z[6] = abb[3];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[21];
z[11] = abb[22];
z[12] = abb[23];
z[13] = abb[25];
z[14] = abb[28];
z[15] = abb[29];
z[16] = abb[30];
z[17] = abb[19];
z[18] = bc<TR>[0];
z[19] = abb[10];
z[20] = abb[11];
z[21] = abb[12];
z[22] = abb[13];
z[23] = abb[14];
z[24] = abb[15];
z[25] = abb[16];
z[26] = abb[17];
z[27] = abb[26];
z[28] = abb[27];
z[29] = T(3) * z[15];
z[30] = T(2) * z[12];
z[31] = z[29] + z[30];
z[32] = -z[16] + z[31];
z[33] = T(3) * z[9];
z[34] = T(3) * z[11] + -z[33];
z[35] = T(3) * z[10];
z[36] = T(2) * z[13];
z[37] = z[0] + z[14];
z[38] = -z[32] + -z[34] + -z[35] + -z[36] + z[37];
z[38] = z[5] * z[38];
z[39] = T(4) * z[15];
z[40] = -z[11] + z[16];
z[41] = z[39] + -z[40];
z[42] = z[10] + -z[14];
z[43] = T(3) * z[13];
z[42] = z[12] + z[41] + T(-2) * z[42] + z[43];
z[42] = z[1] * z[42];
z[44] = z[0] + -z[13];
z[45] = -z[15] + z[44];
z[46] = T(3) * z[16];
z[47] = -z[10] + T(-2) * z[14] + -z[34] + z[45] + z[46];
z[47] = z[3] * z[47];
z[48] = T(2) * z[15];
z[49] = z[30] + z[48];
z[50] = T(2) * z[16];
z[51] = -z[14] + z[50];
z[52] = T(2) * z[10];
z[53] = -z[13] + z[49] + z[51] + z[52];
z[54] = z[17] * z[53];
z[55] = z[10] + z[14];
z[34] = z[34] + z[55];
z[34] = -z[0] + z[12] + (T(3) * z[13]) / T(2) + z[34] / T(2) + z[48] + -z[50];
z[34] = z[6] * z[34];
z[34] = z[34] + z[38] + z[42] + z[47] + z[54];
z[34] = z[6] * z[34];
z[29] = z[29] + z[36] + z[37] + -z[40];
z[36] = z[12] + z[29] + -z[33];
z[38] = z[5] * z[36];
z[29] = -z[29] + z[30];
z[29] = z[6] * z[29];
z[47] = z[0] + z[33];
z[50] = z[11] + z[14];
z[54] = T(5) * z[12] + T(8) * z[13] + z[15] + -z[46] + -z[47] + -z[50];
z[54] = z[1] * z[54];
z[56] = z[14] + z[33] + -z[40];
z[56] = (T(-7) * z[13]) / T(2) + -z[30] + -z[39] + z[56] / T(2);
z[56] = z[4] * z[56];
z[57] = z[13] + -z[14];
z[41] = -z[12] + z[41] + -z[57];
z[58] = int_to_imaginary<T>(-1) * z[18];
z[58] = z[17] + z[58];
z[41] = z[41] * z[58];
z[58] = z[12] + -z[16];
z[58] = T(-7) * z[13] + T(-5) * z[15] + z[47] + T(-4) * z[58];
z[58] = z[2] * z[58];
z[29] = z[29] + z[38] + z[41] + z[54] + z[56] + z[58];
z[29] = z[4] * z[29];
z[38] = T(7) * z[16];
z[41] = T(2) * z[55];
z[47] = T(6) * z[13] + -z[38] + z[41] + -z[47] + z[49];
z[47] = z[1] * z[47];
z[39] = -z[33] + -z[39] + z[41] + z[44];
z[39] = z[6] * z[39];
z[31] = z[31] + -z[41] + z[46];
z[41] = z[13] + z[31];
z[46] = z[17] * z[41];
z[45] = z[45] + -z[55];
z[49] = -(z[3] * z[45]);
z[54] = (T(3) * z[16]) / T(2) + -z[55];
z[56] = (T(3) * z[9]) / T(2);
z[58] = -z[12] / T(2) + T(-4) * z[13] + z[54] + z[56];
z[58] = z[2] * z[58];
z[39] = z[39] + z[46] + z[47] + z[49] + z[58];
z[39] = z[2] * z[39];
z[31] = z[13] + -z[31];
z[31] = z[17] * z[31];
z[46] = -(z[6] * z[53]);
z[41] = -(z[2] * z[41]);
z[47] = z[10] + z[12];
z[49] = -z[14] + z[15];
z[53] = z[16] + z[49];
z[58] = z[47] + z[53];
z[59] = T(2) * z[3];
z[58] = z[58] * z[59];
z[59] = z[5] * z[57];
z[58] = z[58] + -z[59];
z[53] = -z[13] + z[53];
z[53] = T(3) * z[53];
z[59] = z[28] * z[53];
z[31] = z[31] + z[41] + z[42] + z[46] + z[58] + z[59];
z[31] = int_to_imaginary<T>(1) * z[31];
z[41] = (T(17) * z[10]) / T(2) + z[14];
z[41] = -z[11] + (T(-11) * z[12]) / T(6) + (T(13) * z[13]) / T(4) + (T(-5) * z[15]) / T(12) + (T(-43) * z[16]) / T(12) + z[41] / T(3);
z[41] = z[18] * z[41];
z[31] = z[31] + z[41] / T(2);
z[31] = z[18] * z[31];
z[33] = T(-5) * z[13] + -z[15] + -z[30] + z[33] + z[38] + -z[55];
z[33] = z[19] * z[33];
z[36] = -(z[1] * z[36]);
z[35] = T(-3) * z[12] + z[35] + z[57];
z[35] = z[5] * z[35];
z[35] = z[35] / T(2) + z[36];
z[35] = z[5] * z[35];
z[36] = z[9] + -z[10];
z[38] = z[11] + -z[36];
z[32] = z[32] + T(3) * z[38] + -z[44];
z[32] = z[5] * z[32];
z[38] = z[1] * z[45];
z[41] = (T(3) * z[11]) / T(2) + -z[16] + -z[47] + z[49] / T(2);
z[41] = z[3] * z[41];
z[32] = z[32] + z[38] + z[41];
z[32] = z[3] * z[32];
z[36] = -z[12] + z[13] + -z[36] + -z[40];
z[36] = z[21] * z[36];
z[38] = z[9] + z[15] + -z[55];
z[38] = z[20] * z[38];
z[41] = z[9] + -z[12];
z[41] = z[22] * z[41];
z[36] = z[36] + z[38] + z[41];
z[37] = -z[10] + z[11] / T(2) + (T(-5) * z[13]) / T(2) + (T(9) * z[15]) / T(2) + T(5) * z[16] + -z[30] + z[37] + z[56];
z[37] = prod_pow(z[1], 2) * z[37];
z[38] = -z[11] + -z[12] + T(-7) * z[15] + -z[51] + z[52];
z[38] = z[1] * z[38];
z[41] = z[12] + -z[13] / T(2) + (T(3) * z[15]) / T(2) + z[54];
z[41] = z[17] * z[41];
z[38] = z[38] + z[41] + -z[58];
z[38] = z[17] * z[38];
z[30] = z[0] + z[15] + z[16] + -z[30] + -z[43] + z[50];
z[30] = z[8] * z[30];
z[40] = T(-2) * z[0] + (T(-5) * z[12]) / T(2) + T(-3) * z[40] + z[48];
z[40] = z[7] * z[40];
z[41] = z[23] + z[25] + T(2) * z[26];
z[41] = z[24] * z[41];
z[42] = z[27] * z[53];
return z[29] + T(2) * z[30] + z[31] + z[32] + z[33] + z[34] + z[35] + T(3) * z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42];
}



template IntegrandConstructorType<double> f_3_74_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_74_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_74_construct (const Kin<qd_real>&);
#endif

}