#include "f_3_59.h"

namespace PentagonFunctions {

template <typename T> T f_3_59_abbreviated (const std::array<T,10>&);

template <typename T> class SpDLog_f_3_59_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_3_59_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[1], 2) + -prod_pow(abb[2], 2));
    }
};
template <typename T> class SpDLog_f_3_59_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_3_59_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[3] * (prod_pow(abb[4], 2) + -prod_pow(abb[2], 2));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_59_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl3 = DLog_W_3<T>(kin),spdl22 = SpDLog_f_3_59_W_22<T>(kin),spdl21 = SpDLog_f_3_59_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,10> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), dl16(t), f_2_1_14(kin_path), f_2_1_15(kin_path), dl19(t), dl3(t)}
;

        auto result = f_3_59_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_59_abbreviated(const std::array<T,10>& abb)
{
using TR = typename T::value_type;
T z[14];
z[0] = abb[1];
z[1] = abb[5];
z[2] = abb[9];
z[3] = abb[4];
z[4] = abb[8];
z[5] = abb[2];
z[6] = abb[6];
z[7] = abb[7];
z[8] = bc<TR>[0];
z[9] = z[6] + z[7];
z[10] = z[1] + z[4];
z[11] = -z[2] + z[10];
z[12] = T(2) * z[11];
z[9] = z[9] * z[12];
z[12] = -(z[0] * z[12]);
z[13] = -z[2] + z[4];
z[13] = z[3] * z[13];
z[12] = z[12] + z[13];
z[12] = z[3] * z[12];
z[10] = prod_pow(z[5], 2) * z[10];
z[13] = z[1] + -z[2];
z[13] = prod_pow(z[0], 2) * z[13];
z[11] = prod_pow(z[8], 2) * z[11];
return z[9] + z[10] + z[11] / T(3) + z[12] + z[13];
}



template IntegrandConstructorType<double> f_3_59_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_59_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_59_construct (const Kin<qd_real>&);
#endif

}