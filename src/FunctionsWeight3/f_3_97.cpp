#include "f_3_97.h"

namespace PentagonFunctions {

template <typename T> T f_3_97_abbreviated (const std::array<T,40>&);

template <typename T> class SpDLog_f_3_97_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_97_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[2] * ((T(27) * kin.v[2]) / T(4) + T(-7) + T(-7) * kin.v[4]) + kin.v[1] * ((T(27) * kin.v[2]) / T(2) + T(-7) + (T(27) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[1] + T(-7) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]);
c[1] = (T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[23] * (abb[17] * (abb[15] * T(-4) + abb[17] * T(-2) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[20] * (bc<T>[0] * int_to_imaginary<T>(-4) + abb[20] * T(2)) + abb[22] * T(3) + abb[15] * abb[20] * T(4) + abb[14] * (abb[20] * T(-4) + abb[17] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_97_construct (const Kin<T>& kin) {
    return [&kin, 
            dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl11 = DLog_W_11<T>(kin),dl2 = DLog_W_2<T>(kin),dl10 = DLog_W_10<T>(kin),dl6 = DLog_W_6<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl5 = DLog_W_5<T>(kin),spdl10 = SpDLog_f_3_97_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);

        std::array<std::complex<T>,40> abbr = 
            {dl27(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), f_2_2_2(kin_path), f_2_2_3(kin_path), f_2_2_4(kin_path), f_2_2_5(kin_path), f_2_2_6(kin_path), f_2_2_7(kin_path), f_2_2_8(kin_path), f_2_2_9(kin_path), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl11(t), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[4]), f_2_1_2(kin_path), dl2(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_4(kin_path), f_2_1_7(kin_path), dl10(t), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), dl3(t), f_2_1_1(kin_path), dl4(t), dl16(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl18(t), dl19(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl1(t), dl17(t), dl5(t)}
;

        auto result = f_3_97_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_3_97_abbreviated(const std::array<T,40>& abb)
{
using TR = typename T::value_type;
T z[79];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[11];
z[12] = abb[12];
z[13] = abb[13];
z[14] = abb[14];
z[15] = abb[15];
z[16] = abb[16];
z[17] = abb[17];
z[18] = bc<TR>[0];
z[19] = abb[18];
z[20] = abb[19];
z[21] = abb[24];
z[22] = abb[27];
z[23] = abb[29];
z[24] = abb[30];
z[25] = abb[33];
z[26] = abb[34];
z[27] = abb[37];
z[28] = abb[38];
z[29] = abb[39];
z[30] = abb[20];
z[31] = abb[25];
z[32] = abb[21];
z[33] = abb[22];
z[34] = abb[26];
z[35] = abb[28];
z[36] = abb[31];
z[37] = abb[32];
z[38] = abb[35];
z[39] = abb[36];
z[40] = T(2) * z[13];
z[41] = -z[16] + z[40];
z[42] = T(2) * z[30];
z[43] = T(3) * z[37];
z[44] = T(2) * z[15];
z[45] = T(-8) * z[17] + z[31] + T(3) * z[39] + -z[41] + z[42] + z[43] + -z[44];
z[45] = z[29] * z[45];
z[46] = z[42] + -z[43];
z[47] = T(8) * z[16];
z[48] = T(5) * z[13];
z[49] = T(3) * z[15];
z[50] = z[17] + T(9) * z[31] + z[46] + z[47] + -z[48] + -z[49];
z[50] = z[25] * z[50];
z[51] = T(2) * z[17];
z[52] = -z[44] + z[51];
z[46] = T(-4) * z[13] + z[14] + -z[16] + -z[31] + -z[46] + -z[52];
z[46] = z[27] * z[46];
z[53] = -z[17] + z[30];
z[54] = T(2) * z[53];
z[55] = z[16] + T(-3) * z[31] + z[40] + -z[49] + z[54];
z[55] = z[22] * z[55];
z[56] = T(8) * z[12] + z[20];
z[57] = z[13] + -z[16];
z[56] = z[56] * z[57];
z[58] = z[15] + z[53];
z[59] = -z[13] + z[58];
z[60] = T(2) * z[59];
z[43] = -z[31] + z[43] + z[60];
z[43] = z[28] * z[43];
z[61] = T(3) * z[26];
z[62] = -z[17] + z[37];
z[63] = z[13] + -z[39] + -z[62];
z[63] = z[61] * z[63];
z[64] = T(7) * z[16];
z[65] = -z[14] + z[17];
z[66] = T(-9) * z[13] + z[15] + T(8) * z[31] + z[64] + z[65];
z[66] = z[23] * z[66];
z[67] = z[13] + -z[31];
z[67] = z[21] * z[67];
z[62] = -(z[24] * z[62]);
z[43] = z[43] + z[45] + z[46] + z[50] + z[55] + z[56] + T(3) * z[62] + z[63] + z[66] + T(4) * z[67];
z[43] = int_to_imaginary<T>(1) * z[43];
z[45] = z[28] + -z[29];
z[46] = -z[20] + z[26];
z[50] = (T(-55) * z[22]) / T(12) + (T(41) * z[23]) / T(4) + (T(121) * z[25]) / T(12) + (T(-11) * z[27]) / T(3) + (T(5) * z[45]) / T(6) + z[46];
z[50] = z[18] * z[50];
z[43] = z[43] + z[50] / T(2);
z[43] = z[18] * z[43];
z[50] = T(4) * z[16];
z[55] = z[31] / T(2);
z[56] = z[40] + z[44] + -z[50] + T(-3) * z[53] + -z[55];
z[56] = z[31] * z[56];
z[62] = T(2) * z[65];
z[63] = z[16] / T(2);
z[66] = -z[15] + z[62] + -z[63];
z[66] = z[16] * z[66];
z[67] = T(5) * z[16] + -z[40];
z[68] = -z[15] + z[65];
z[69] = z[67] + z[68];
z[69] = z[13] * z[69];
z[70] = T(2) * z[34];
z[71] = T(3) * z[36];
z[72] = prod_pow(z[14], 2);
z[73] = z[17] / T(2);
z[74] = -z[14] + z[73];
z[74] = z[17] * z[74];
z[75] = T(2) * z[14];
z[76] = -z[30] + z[75];
z[76] = z[30] * z[76];
z[77] = z[15] + -z[54];
z[77] = z[15] * z[77];
z[56] = T(-7) * z[33] + T(3) * z[38] + z[56] + z[66] + z[69] + -z[70] + z[71] + (T(3) * z[72]) / T(2) + T(5) * z[74] + z[76] + z[77];
z[56] = z[29] * z[56];
z[66] = T(3) * z[17] + -z[30];
z[47] = (T(-5) * z[13]) / T(2) + z[47] + z[66] + -z[75];
z[47] = z[13] * z[47];
z[64] = T(6) * z[13] + T(-4) * z[31] + z[58] + -z[64];
z[64] = z[31] * z[64];
z[69] = T(3) * z[35];
z[74] = z[14] * z[17];
z[76] = -(z[30] * z[65]);
z[77] = -z[15] / T(2) + -z[65];
z[77] = z[15] * z[77];
z[62] = -z[15] + (T(-7) * z[16]) / T(2) + -z[62];
z[62] = z[16] * z[62];
z[47] = T(-5) * z[34] + z[47] + z[62] + z[64] + -z[69] + -z[72] + z[74] + z[76] + z[77];
z[47] = z[23] * z[47];
z[62] = (T(-9) * z[31]) / T(2) + z[49] + T(4) * z[53] + -z[67];
z[62] = z[31] * z[62];
z[64] = T(3) * z[14];
z[67] = z[51] + -z[64];
z[67] = z[17] * z[67];
z[74] = T(4) * z[15] + -z[50] + T(-3) * z[65];
z[74] = z[16] * z[74];
z[76] = (T(3) * z[15]) / T(2) + -z[54];
z[76] = z[15] * z[76];
z[77] = -z[17] + z[64];
z[78] = -z[30] + z[77];
z[78] = z[30] * z[78];
z[64] = (T(9) * z[13]) / T(2) + T(-7) * z[15] + z[16] + T(4) * z[17] + -z[30] + -z[64];
z[64] = z[13] * z[64];
z[62] = -z[33] + -z[34] + z[62] + z[64] + z[67] + z[69] + -z[71] + z[74] + z[76] + z[78];
z[62] = z[25] * z[62];
z[44] = T(-3) * z[16] + -z[44] + z[48] + -z[65];
z[44] = z[13] * z[44];
z[48] = z[63] + -z[68];
z[48] = z[16] * z[48];
z[63] = z[48] + -z[69];
z[64] = -z[73] + -z[75];
z[64] = z[17] * z[64];
z[49] = T(-7) * z[13] + (T(3) * z[31]) / T(2) + z[49] + z[50];
z[49] = z[31] * z[49];
z[50] = z[72] / T(2);
z[67] = -z[33] + z[50];
z[69] = T(3) * z[32];
z[66] = -z[14] + z[66];
z[66] = z[30] * z[66];
z[44] = T(7) * z[34] + z[44] + z[49] + -z[63] + z[64] + z[66] + T(3) * z[67] + z[69] + z[76];
z[44] = z[22] * z[44];
z[49] = -z[55] + z[60];
z[49] = z[31] * z[49];
z[49] = -z[34] + -z[49] + z[71];
z[55] = -z[14] + z[57];
z[51] = -z[30] + z[51];
z[60] = z[15] + z[51] + z[55];
z[60] = z[13] * z[60];
z[64] = z[14] + z[73];
z[64] = z[17] * z[64];
z[51] = -(z[30] * z[51]);
z[42] = -z[14] + -z[15] + -z[17] + z[42];
z[42] = z[15] * z[42];
z[42] = z[42] + z[49] + -z[50] + z[51] + z[60] + z[63] + z[64];
z[42] = z[27] * z[42];
z[50] = z[54] + -z[57];
z[50] = z[31] * z[50];
z[51] = (T(-3) * z[30]) / T(2) + z[65];
z[51] = z[30] * z[51];
z[60] = -(z[16] * z[65]);
z[55] = z[30] + z[55];
z[55] = z[13] * z[55];
z[50] = z[33] + z[50] + z[51] + z[55] + z[60] + z[64] + -z[71];
z[50] = z[24] * z[50];
z[51] = z[30] / T(2);
z[55] = z[51] + z[77];
z[55] = z[30] * z[55];
z[52] = -z[13] + -z[30] + -z[52];
z[52] = z[13] * z[52];
z[60] = prod_pow(z[17], 2);
z[54] = -z[15] + -z[54];
z[54] = z[15] * z[54];
z[49] = z[49] + z[52] + z[54] + z[55] + -z[60];
z[49] = z[28] * z[49];
z[51] = z[51] + -z[65];
z[51] = z[30] * z[51];
z[52] = z[51] + -z[67];
z[54] = z[13] / T(2) + -z[16];
z[55] = z[54] + z[68];
z[55] = z[13] * z[55];
z[48] = z[48] + T(3) * z[52] + z[55];
z[48] = z[20] * z[48];
z[40] = -z[40] + z[58];
z[40] = z[13] * z[40];
z[52] = z[31] + -z[59];
z[52] = z[31] * z[52];
z[40] = z[40] + z[52] + -z[70];
z[40] = z[21] * z[40];
z[52] = T(-2) * z[12] + -z[29];
z[52] = z[20] + z[22] + T(-3) * z[23] + z[24] + z[25] + z[27] + T(2) * z[52];
z[52] = z[19] * z[52];
z[40] = z[40] + z[52];
z[52] = z[53] + -z[57];
z[52] = z[31] * z[52];
z[53] = z[14] + z[54];
z[53] = z[13] * z[53];
z[51] = z[34] + -z[36] + -z[38] + -z[51] + z[52] + z[53];
z[51] = z[51] * z[61];
z[52] = z[2] + z[5] + z[6] + z[7];
z[53] = z[0] + z[10];
z[54] = T(3) * z[53];
z[55] = -z[11] + z[54];
z[52] = z[52] * z[55];
z[55] = z[1] + -z[3] + z[4] + -z[8];
z[53] = z[53] * z[55];
z[41] = -z[41] + -z[68];
z[41] = z[13] * z[41];
z[57] = z[16] + z[68];
z[57] = z[16] * z[57];
z[41] = z[41] + z[57];
z[41] = z[12] * z[41];
z[45] = -z[23] + -z[45] + z[46];
z[45] = z[45] * z[69];
z[46] = -(z[9] * z[54]);
z[54] = z[9] + -z[55];
z[54] = z[11] * z[54];
return T(2) * z[40] + T(4) * z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + T(3) * z[53] + z[54] + z[56] + z[62];
}



template IntegrandConstructorType<double> f_3_97_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_97_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_97_construct (const Kin<qd_real>&);
#endif

}