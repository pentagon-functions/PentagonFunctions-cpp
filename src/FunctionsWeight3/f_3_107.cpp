#include "f_3_107.h"

namespace PentagonFunctions {

template <typename T> T f_3_107_abbreviated (const std::array<T,36>&);

template <typename T> class SpDLog_f_3_107_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_107_W_22 (const Kin<T>& kin) {
        c[0] = -prod_pow(kin.v[3], 2) + kin.v[2] * (T(5) * kin.v[2] + T(-4) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[0] + kin.v[1] + T(6) * kin.v[2] + T(-2) * kin.v[4]) + prod_pow(kin.v[4], 2) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[6] * T(-4) + abb[4] * abb[5] * T(-2) + abb[3] * abb[5] * T(2) + abb[1] * (abb[5] * T(-2) + abb[2] * T(2)) + abb[5] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[5] * T(2)) + abb[2] * (abb[2] * T(-4) + abb[3] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_107_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl3 = DLog_W_3<T>(kin),dl9 = DLog_W_9<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl15 = DLog_W_15<T>(kin),dl2 = DLog_W_2<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),spdl22 = SpDLog_f_3_107_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,36> abbr = 
            {dl22(t), rlog(v_path[2]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), dl3(t), rlog(-v_path[1]), f_2_1_4(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl9(t), rlog(v_path[3]), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), f_2_1_8(kin_path), dl16(t), dl19(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl5(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl17(t), dl20(t), dl1(t), dl15(t), dl2(t), dl28(t) / kin_path.SqrtDelta, f_2_2_6(kin_path), f_1_3_4(kin_path), f_1_3_5(kin_path), dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_3_107_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_107_abbreviated(const std::array<T,36>& abb)
{
using TR = typename T::value_type;
T z[84];
z[0] = abb[1];
z[1] = abb[7];
z[2] = abb[18];
z[3] = abb[19];
z[4] = abb[22];
z[5] = abb[25];
z[6] = abb[26];
z[7] = abb[27];
z[8] = abb[28];
z[9] = abb[2];
z[10] = abb[16];
z[11] = abb[3];
z[12] = abb[4];
z[13] = abb[5];
z[14] = abb[8];
z[15] = bc<TR>[0];
z[16] = abb[29];
z[17] = abb[12];
z[18] = abb[13];
z[19] = abb[6];
z[20] = abb[9];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[14];
z[24] = abb[15];
z[25] = abb[17];
z[26] = abb[20];
z[27] = abb[21];
z[28] = abb[23];
z[29] = abb[24];
z[30] = abb[30];
z[31] = abb[31];
z[32] = abb[32];
z[33] = abb[33];
z[34] = abb[34];
z[35] = abb[35];
z[36] = -z[13] + z[29];
z[37] = -z[12] + z[14];
z[38] = -z[27] + z[37];
z[39] = -z[9] + z[18];
z[40] = T(5) * z[11];
z[41] = T(3) * z[22];
z[36] = T(5) * z[0] + T(-7) * z[36] + T(-3) * z[38] + z[39] + z[40] + -z[41];
z[36] = z[4] * z[36];
z[38] = z[9] + -z[11];
z[42] = T(2) * z[14];
z[43] = z[38] + z[42];
z[44] = T(3) * z[12];
z[45] = z[18] + z[44];
z[46] = T(4) * z[13];
z[47] = T(2) * z[0];
z[48] = z[43] + z[45] + -z[46] + z[47];
z[48] = z[5] * z[48];
z[49] = T(2) * z[9];
z[50] = -z[41] + z[49];
z[51] = T(3) * z[14];
z[52] = -z[12] + -z[51];
z[40] = -z[0] + z[13] + T(8) * z[18] + z[40] + -z[50] + T(2) * z[52];
z[40] = z[6] * z[40];
z[52] = T(2) * z[13];
z[41] = -z[41] + z[52];
z[53] = T(3) * z[11];
z[45] = z[0] + -z[14] + z[41] + -z[45] + z[53];
z[45] = z[2] * z[45];
z[54] = T(2) * z[12];
z[55] = -z[51] + z[54];
z[56] = z[9] + z[11];
z[57] = z[55] + z[56];
z[41] = z[41] + z[47] + z[57];
z[41] = z[1] * z[41];
z[58] = z[32] + z[33];
z[59] = z[30] + z[34];
z[60] = T(-2) * z[35] + T(3) * z[59];
z[61] = T(2) * z[60];
z[58] = z[58] * z[61];
z[61] = T(4) * z[37];
z[62] = -z[53] + z[61];
z[63] = T(4) * z[0];
z[64] = z[62] + -z[63];
z[64] = z[8] * z[64];
z[65] = -z[51] + z[53];
z[66] = T(7) * z[18];
z[67] = -z[9] + z[12] + T(6) * z[13] + z[65] + z[66];
z[67] = z[10] * z[67];
z[68] = z[11] + -z[12];
z[69] = -z[14] + z[68];
z[70] = -z[9] + z[69];
z[71] = T(2) * z[18] + z[70];
z[72] = T(4) * z[17];
z[73] = -(z[71] * z[72]);
z[74] = T(3) * z[0];
z[75] = -z[9] + T(-4) * z[14] + -z[18] + T(3) * z[29] + z[68] + -z[74];
z[75] = z[16] * z[75];
z[43] = z[22] + -z[27] + z[43] + -z[52];
z[52] = T(3) * z[3];
z[43] = z[43] * z[52];
z[50] = -z[11] + T(-3) * z[13] + z[29] + -z[50];
z[50] = z[7] * z[50];
z[76] = z[5] + z[6];
z[77] = -z[2] + T(2) * z[4] + -z[7] + T(3) * z[10] + -z[16] + z[72] + -z[76];
z[77] = T(2) * z[77];
z[78] = -(z[24] * z[77]);
z[79] = z[5] + -z[8];
z[79] = z[6] + T(3) * z[79];
z[80] = -(z[29] * z[79]);
z[36] = z[36] + z[40] + z[41] + z[43] + z[45] + z[48] + z[50] + z[58] + z[64] + z[67] + z[73] + z[75] + z[78] + z[80];
z[36] = int_to_imaginary<T>(1) * z[15] * z[36];
z[40] = T(4) * z[18];
z[41] = z[13] / T(2);
z[43] = -z[40] + -z[41] + z[49] + z[53] + z[54] + -z[74];
z[43] = z[13] * z[43];
z[45] = (T(3) * z[18]) / T(2);
z[48] = T(2) * z[11];
z[50] = z[9] + z[48];
z[58] = -z[12] + -z[42] + z[45] + z[50];
z[58] = z[18] * z[58];
z[64] = prod_pow(z[14], 2);
z[67] = T(3) * z[20];
z[73] = (T(3) * z[64]) / T(2) + z[67];
z[74] = T(3) * z[21];
z[75] = prod_pow(z[12], 2);
z[78] = T(6) * z[11] + T(-5) * z[14] + z[54];
z[78] = z[11] * z[78];
z[80] = z[9] * z[69];
z[81] = T(2) * z[37];
z[82] = (T(5) * z[0]) / T(2) + T(-7) * z[11] + z[81];
z[82] = z[0] * z[82];
z[43] = T(-2) * z[19] + T(3) * z[26] + z[43] + z[58] + z[73] + z[74] + z[75] + z[78] + z[80] + z[82];
z[43] = z[4] * z[43];
z[58] = z[11] + -z[14];
z[78] = T(4) * z[9] + -z[54] + -z[58];
z[78] = z[9] * z[78];
z[45] = -z[45] + z[70];
z[45] = z[18] * z[45];
z[80] = z[0] / T(2);
z[82] = -z[14] + -z[54] + z[80];
z[82] = z[0] * z[82];
z[40] = T(-7) * z[9] + (T(3) * z[13]) / T(2) + z[40] + z[44];
z[40] = z[13] * z[40];
z[83] = (T(3) * z[75]) / T(2);
z[81] = z[11] + -z[81];
z[81] = z[11] * z[81];
z[40] = T(7) * z[19] + z[40] + z[45] + z[73] + z[78] + z[81] + z[82] + z[83];
z[40] = z[5] * z[40];
z[45] = T(4) * z[11];
z[73] = T(5) * z[18];
z[44] = (T(-9) * z[13]) / T(2) + z[44] + -z[45] + z[49] + z[63] + -z[73];
z[44] = z[13] * z[44];
z[63] = -z[50] + -z[55] + -z[80];
z[63] = z[0] * z[63];
z[55] = (T(5) * z[11]) / T(2) + z[55];
z[55] = z[11] * z[55];
z[45] = (T(7) * z[9]) / T(2) + T(-7) * z[12] + z[45] + -z[51];
z[45] = z[9] * z[45];
z[65] = T(3) * z[9] + T(4) * z[12] + -z[65] + -z[73];
z[65] = z[18] * z[65];
z[44] = -z[19] + z[44] + z[45] + z[55] + z[63] + z[65] + -z[74] + z[83];
z[44] = z[6] * z[44];
z[45] = z[18] / T(2);
z[49] = -z[12] + z[42] + -z[45] + -z[48] + z[49];
z[49] = z[18] * z[49];
z[55] = z[9] / T(2);
z[42] = -z[42] + z[53] + z[55];
z[42] = z[9] * z[42];
z[53] = -z[0] + z[68];
z[46] = T(6) * z[9] + -z[46] + -z[53] + -z[66];
z[46] = z[13] * z[46];
z[63] = z[37] * z[58];
z[65] = -z[9] + -z[58];
z[65] = z[0] * z[65];
z[42] = T(-5) * z[19] + z[42] + z[46] + z[49] + z[63] + z[65] + -z[67] + -z[75] / T(2);
z[42] = z[10] * z[42];
z[46] = z[9] + z[53];
z[41] = z[41] + T(2) * z[46];
z[41] = z[13] * z[41];
z[41] = z[19] + -z[41] + -z[74] + z[75];
z[45] = z[45] + z[70];
z[45] = z[18] * z[45];
z[46] = -z[37] + z[48];
z[46] = z[9] * z[46];
z[48] = -z[12] + -z[14] / T(2);
z[48] = z[14] * z[48];
z[37] = z[11] / T(2) + z[37];
z[37] = z[11] * z[37];
z[49] = z[9] + T(2) * z[68];
z[53] = z[0] + -z[49];
z[53] = z[0] * z[53];
z[37] = z[37] + -z[41] + -z[45] + z[46] + z[48] + z[53];
z[37] = z[2] * z[37];
z[46] = prod_pow(z[11], 2);
z[46] = -z[46] + -z[64];
z[48] = -z[55] + z[69];
z[48] = z[9] * z[48];
z[51] = z[0] * z[51];
z[45] = -z[45] + (T(3) * z[46]) / T(2) + z[48] + z[51] + -z[67];
z[45] = z[16] * z[45];
z[39] = -z[39] + -z[58];
z[39] = z[18] * z[39];
z[46] = -z[47] + z[50];
z[47] = -z[14] + z[46];
z[47] = z[0] * z[47];
z[46] = z[18] + -z[46];
z[46] = z[13] * z[46];
z[38] = -(z[14] * z[38]);
z[38] = -z[28] + z[38] + z[39] + z[46] + z[47] + -z[74];
z[38] = z[7] * z[38];
z[39] = -z[57] + z[80];
z[39] = z[0] * z[39];
z[46] = -z[11] + z[54];
z[46] = z[11] * z[46];
z[47] = -(z[9] * z[49]);
z[39] = z[39] + -z[41] + z[46] + z[47] + -z[67];
z[39] = z[1] * z[39];
z[41] = z[58] + -z[80];
z[41] = z[0] * z[41];
z[46] = z[14] + -z[18] + z[55];
z[46] = z[9] * z[46];
z[47] = z[0] + z[18] + -z[56];
z[47] = z[13] * z[47];
z[41] = z[19] + z[20] + -z[21] + -z[26] + z[41] + z[46] + z[47];
z[41] = z[41] * z[52];
z[46] = -z[62] + z[80];
z[46] = z[0] * z[46];
z[47] = (T(-7) * z[11]) / T(2) + z[61];
z[47] = z[11] * z[47];
z[46] = z[46] + z[47];
z[46] = z[8] * z[46];
z[47] = -z[9] + -z[69];
z[47] = z[9] * z[47];
z[48] = z[18] * z[71];
z[47] = z[47] + z[48];
z[47] = z[47] * z[72];
z[48] = z[23] * z[77];
z[49] = z[31] * z[60];
z[50] = T(7) * z[4] + T(-3) * z[16] + z[79];
z[50] = z[28] * z[50];
z[51] = (T(5) * z[1]) / T(12) + (T(-2) * z[2]) / T(3) + z[3] / T(2) + (T(-19) * z[4]) / T(12) + (T(3) * z[5]) / T(8) + (T(47) * z[6]) / T(8) + (T(5) * z[7]) / T(2) + -z[8] + (T(25) * z[10]) / T(8) + (T(-5) * z[16]) / T(6) + (T(-20) * z[17]) / T(3) + (T(-8) * z[35]) / T(3) + T(4) * z[59];
z[51] = prod_pow(z[15], 2) * z[51];
z[52] = -z[2] + -z[10] + z[76];
z[52] = z[25] * z[52];
return z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + T(3) * z[52];
}



template IntegrandConstructorType<double> f_3_107_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_107_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_107_construct (const Kin<qd_real>&);
#endif

}