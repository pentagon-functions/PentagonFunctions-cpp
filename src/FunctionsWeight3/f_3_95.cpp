#include "f_3_95.h"

namespace PentagonFunctions {

template <typename T> T f_3_95_abbreviated (const std::array<T,41>&);

template <typename T> class SpDLog_f_3_95_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_95_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * T(-4) + (bc<T>[1] * T(-2) + T(2)) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]) + (T(4) * kin.v[0] * kin.v[4]) / T(3) + T(-4) * kin.v[2] * kin.v[4] + T(-4) * kin.v[3] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + bc<T>[1] * kin.v[4] * (T(4) + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(-4) + T(-2) * kin.v[1]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = (T(-8) + bc<T>[1] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + bc<T>[1] * T(4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[21] * (abb[4] * (abb[4] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[3] * abb[4] * T(4) + abb[4] * abb[22] * T(4) + abb[1] * (abb[4] * T(-4) + abb[2] * T(4)) + abb[7] * T(8) + abb[2] * (abb[3] * T(-4) + abb[4] * T(-4) + abb[22] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * T(8)));
    }
};
template <typename T> class SpDLog_f_3_95_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_95_W_10 (const Kin<T>& kin) {
        c[0] = (T(-4) * kin.v[0] * kin.v[2]) / T(3) + kin.v[2] * (kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + (-bc<T>[1] + T(1)) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[1] * (T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + bc<T>[1] * T(2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + bc<T>[1] * T(2) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[23] * (abb[24] * T(-4) + abb[5] * abb[22] * T(-2) + abb[1] * (abb[4] * T(-2) + abb[5] * T(2)) + abb[5] * (abb[6] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[5] * T(2)) + abb[4] * (abb[4] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[5] * T(2) + abb[6] * T(2) + abb[22] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_95_construct (const Kin<T>& kin) {
    return [&kin, 
            dl19 = DLog_W_19<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),dl12 = DLog_W_12<T>(kin),dl10 = DLog_W_10<T>(kin),dl4 = DLog_W_4<T>(kin),dl6 = DLog_W_6<T>(kin),dl1 = DLog_W_1<T>(kin),dl2 = DLog_W_2<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),spdl12 = SpDLog_f_3_95_W_12<T>(kin),spdl10 = SpDLog_f_3_95_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,41> abbr = 
            {dl19(t), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[3]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_4(kin_path), f_2_1_9(kin_path), dl26(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), f_2_2_2(kin_path), f_2_2_3(kin_path), f_2_2_4(kin_path), f_2_2_5(kin_path), f_2_2_6(kin_path), f_2_2_7(kin_path), f_2_2_8(kin_path), f_2_2_9(kin_path), dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl12(t), rlog(v_path[2]), dl10(t), f_2_1_7(kin_path), dl4(t), f_2_1_2(kin_path), dl6(t), dl1(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl2(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl3(t), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl5(t), dl16(t), dl17(t)}
;

        auto result = f_3_95_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_3_95_abbreviated(const std::array<T,41>& abb)
{
using TR = typename T::value_type;
T z[83];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[5];
z[5] = abb[6];
z[6] = abb[4];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[25];
z[10] = abb[27];
z[11] = abb[28];
z[12] = abb[31];
z[13] = abb[34];
z[14] = abb[35];
z[15] = abb[38];
z[16] = abb[39];
z[17] = abb[40];
z[18] = abb[22];
z[19] = bc<TR>[0];
z[20] = abb[9];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[12];
z[24] = abb[13];
z[25] = abb[14];
z[26] = abb[15];
z[27] = abb[16];
z[28] = abb[17];
z[29] = abb[18];
z[30] = abb[19];
z[31] = abb[20];
z[32] = abb[24];
z[33] = abb[26];
z[34] = abb[29];
z[35] = abb[30];
z[36] = abb[32];
z[37] = abb[33];
z[38] = abb[36];
z[39] = abb[37];
z[40] = z[11] + z[13];
z[41] = T(2) * z[40];
z[42] = T(2) * z[17];
z[43] = z[41] + z[42];
z[44] = T(3) * z[9];
z[45] = T(3) * z[16];
z[46] = z[44] + -z[45];
z[47] = z[12] + z[46];
z[48] = -z[43] + z[47];
z[48] = z[1] * z[48];
z[49] = T(3) * z[12];
z[50] = z[9] + -z[13];
z[51] = z[11] + z[50];
z[52] = T(4) * z[17];
z[53] = z[49] + z[51] + z[52];
z[54] = -z[45] + z[53];
z[54] = z[6] * z[54];
z[55] = z[11] + z[12];
z[56] = -z[50] + -z[52] + z[55];
z[57] = z[2] * z[56];
z[58] = T(2) * z[18];
z[59] = T(-2) * z[4] + T(3) * z[39] + -z[58];
z[60] = z[1] + T(2) * z[6];
z[61] = T(3) * z[37];
z[62] = -z[2] + z[3] + z[5] + -z[59] + z[60] + -z[61];
z[62] = z[15] * z[62];
z[41] = -z[17] + z[41] + -z[44];
z[41] = z[5] * z[41];
z[63] = T(2) * z[11];
z[64] = T(3) * z[17];
z[65] = z[63] + z[64];
z[66] = T(3) * z[13];
z[67] = z[65] + z[66];
z[68] = z[12] + -z[67];
z[68] = z[18] * z[68];
z[69] = z[13] + z[17];
z[70] = -z[12] + z[69];
z[61] = z[61] * z[70];
z[67] = z[12] + z[67];
z[71] = -(z[4] * z[67]);
z[72] = -z[1] + z[5];
z[73] = z[10] * z[72];
z[74] = z[16] + z[17];
z[75] = z[39] * z[74];
z[76] = T(3) * z[11] + -z[12];
z[77] = z[3] * z[76];
z[78] = -z[11] + z[16];
z[78] = T(3) * z[78];
z[79] = z[35] * z[78];
z[41] = z[41] + z[48] + z[54] + z[57] + z[61] + z[62] + z[68] + z[71] + -z[73] + T(3) * z[75] + z[77] + z[79];
z[48] = int_to_imaginary<T>(1) * z[19];
z[41] = z[41] * z[48];
z[54] = T(3) * z[3];
z[57] = T(2) * z[5];
z[61] = z[1] + z[6];
z[59] = -z[54] + z[57] + -z[59] + z[61];
z[48] = z[48] * z[59];
z[58] = z[4] + -z[5] + z[58] + T(-2) * z[61];
z[58] = z[4] * z[58];
z[59] = T(3) * z[38];
z[62] = prod_pow(z[1], 2);
z[58] = z[32] + z[58] + z[59] + -z[62] / T(2);
z[68] = z[3] / T(2);
z[71] = z[68] + z[72];
z[54] = z[54] * z[71];
z[71] = T(3) * z[33];
z[75] = prod_pow(z[19], 2);
z[77] = T(2) * z[1];
z[79] = z[6] + z[77];
z[80] = -(z[6] * z[79]);
z[81] = -z[5] + -z[61];
z[81] = z[5] * z[81];
z[82] = z[6] + -z[72];
z[82] = -z[18] + T(2) * z[82];
z[82] = z[18] * z[82];
z[48] = z[48] + z[54] + -z[58] + z[71] + (T(11) * z[75]) / T(12) + z[80] + z[81] + z[82];
z[48] = z[14] * z[48];
z[54] = T(2) * z[12];
z[64] = z[54] + z[64];
z[51] = -z[45] + z[51] + z[64];
z[80] = z[6] * z[51];
z[65] = -z[13] + z[65];
z[46] = -z[46] + -z[54] + -z[65];
z[46] = z[1] * z[46];
z[65] = z[47] + z[65];
z[65] = z[5] * z[65];
z[51] = z[2] * z[51];
z[76] = -(z[68] * z[76]);
z[81] = z[12] * z[18];
z[46] = z[46] + z[51] + z[65] + T(-4) * z[73] + z[76] + -z[80] + z[81];
z[46] = z[3] * z[46];
z[51] = z[18] / T(2) + z[57] + -z[60];
z[51] = z[18] * z[51];
z[60] = z[2] + -z[6];
z[65] = z[1] + z[60];
z[68] = -z[18] + z[65] + -z[68];
z[68] = z[3] * z[68];
z[73] = (T(3) * z[6]) / T(2) + z[77];
z[73] = z[6] * z[73];
z[76] = -z[5] / T(2) + -z[79];
z[76] = z[5] * z[76];
z[77] = -z[1] + -z[2] / T(2) + z[6] + z[18];
z[77] = z[2] * z[77];
z[51] = z[51] + -z[58] + z[68] + z[73] + (T(2) * z[75]) / T(3) + z[76] + z[77];
z[51] = z[15] * z[51];
z[58] = T(5) * z[17];
z[68] = z[11] + -z[13];
z[68] = T(-7) * z[12] + z[45] + -z[58] + T(-4) * z[68];
z[68] = z[2] * z[68];
z[52] = -z[12] + -z[45] + -z[52];
z[52] = z[1] * z[52];
z[73] = T(-7) * z[13] + -z[45] + z[63];
z[76] = T(6) * z[12] + z[42] + z[73];
z[76] = z[6] * z[76];
z[67] = z[18] * z[67];
z[77] = (T(3) * z[16]) / T(2);
z[79] = -z[11] + z[66];
z[79] = T(-4) * z[12] + z[77] + z[79] / T(2);
z[79] = z[4] * z[79];
z[81] = z[12] + z[17];
z[82] = z[5] * z[81];
z[52] = z[52] + z[67] + z[68] + z[76] + z[79] + z[82];
z[52] = z[4] * z[52];
z[64] = -z[50] + z[63] + -z[64];
z[64] = z[1] * z[64];
z[56] = -(z[18] * z[56]);
z[45] = -z[9] + -z[12] + z[45] + -z[66];
z[45] = z[45] / T(2) + -z[58];
z[45] = z[2] * z[45];
z[45] = z[45] + z[56] + z[64] + z[80];
z[45] = z[2] * z[45];
z[56] = z[1] * z[53];
z[54] = -z[9] / T(2) + (T(5) * z[13]) / T(2) + z[42] + z[54] + z[77];
z[54] = z[6] * z[54];
z[54] = z[54] + z[56];
z[54] = z[6] * z[54];
z[53] = -(z[6] * z[53]);
z[43] = -z[12] + z[43];
z[43] = z[1] * z[43];
z[56] = -z[17] + -z[40];
z[56] = z[56] * z[57];
z[55] = z[18] * z[55];
z[43] = z[43] + z[53] + z[55] + z[56];
z[43] = z[18] * z[43];
z[53] = z[60] + -z[72];
z[55] = z[3] + z[4];
z[53] = z[53] * z[55];
z[55] = z[5] * z[61];
z[56] = -(z[2] * z[65]);
z[53] = z[53] + z[55] + z[56] + -z[62];
z[53] = z[0] * z[53];
z[55] = (T(3) * z[9]) / T(2);
z[56] = z[11] + (T(3) * z[12]) / T(2) + T(-2) * z[13] + z[42] + z[55] + -z[77];
z[56] = z[56] * z[62];
z[47] = -z[17] + -z[47] + z[66];
z[47] = z[1] * z[47];
z[40] = z[17] / T(2) + -z[40] + z[55];
z[40] = z[5] * z[40];
z[55] = -(z[6] * z[81]);
z[40] = z[40] + z[47] + z[55];
z[40] = z[5] * z[40];
z[42] = T(-2) * z[0] + (T(3) * z[10]) / T(2) + (T(-5) * z[11]) / T(2) + z[42] + z[44] + -z[66];
z[42] = z[8] * z[42];
z[44] = -z[0] + -z[9] + -z[15] + z[49] + z[63] + -z[69];
z[44] = z[7] * z[44];
z[47] = z[20] + z[30];
z[47] = -z[31] + T(3) * z[47];
z[49] = z[21] + z[22] + -z[23] + z[24] + z[25] + z[26] + z[27] + -z[28] + -z[29];
z[47] = z[47] * z[49];
z[49] = T(-5) * z[12] + -z[17] + -z[73];
z[49] = z[32] * z[49];
z[55] = prod_pow(z[5], 2);
z[55] = z[55] + -z[62];
z[57] = z[18] * z[72];
z[55] = z[55] / T(2) + T(4) * z[57];
z[55] = z[10] * z[55];
z[57] = z[11] + (T(-31) * z[13]) / T(2);
z[57] = (T(9) * z[12]) / T(2) + z[57] / T(3);
z[57] = -z[9] + -z[16] + (T(-17) * z[17]) / T(12) + z[57] / T(2);
z[57] = z[57] * z[75];
z[58] = z[59] * z[74];
z[50] = -z[11] + z[12] + -z[16] + z[50];
z[50] = z[50] * z[71];
z[59] = z[34] * z[78];
z[60] = z[15] + -z[70];
z[60] = z[36] * z[60];
return z[40] + z[41] + z[42] + z[43] + T(2) * z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] / T(2) + z[58] + z[59] + T(3) * z[60];
}



template IntegrandConstructorType<double> f_3_95_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_95_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_95_construct (const Kin<qd_real>&);
#endif

}