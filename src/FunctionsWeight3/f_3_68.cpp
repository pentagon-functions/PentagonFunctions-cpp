#include "f_3_68.h"

namespace PentagonFunctions {

template <typename T> T f_3_68_abbreviated (const std::array<T,32>&);

template <typename T> class SpDLog_f_3_68_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_68_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(27) * kin.v[1]) / T(4) + (T(-13) * kin.v[2]) / T(2) + (T(-27) * kin.v[3]) / T(2) + T(7) + T(-7) * kin.v[4]) + kin.v[0] * ((T(13) * kin.v[1]) / T(2) + kin.v[2] / T(2) + (T(-13) * kin.v[3]) / T(2) + T(7) + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-7) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + kin.v[2] * (-kin.v[2] / T(4) + (T(13) * kin.v[3]) / T(2) + T(-7) + T(7) * kin.v[4]) + kin.v[3] * ((T(27) * kin.v[3]) / T(4) + T(-7) + T(7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + T(-3) * kin.v[3];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[21] * (abb[14] * T(-3) + abb[9] * ((abb[9] * T(-7)) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[12] * (abb[12] / T(2) + abb[8] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[9] * T(3)) + abb[8] * abb[9] * T(4) + abb[6] * (abb[9] * T(-4) + abb[12] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_68_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl11 = DLog_W_11<T>(kin),dl16 = DLog_W_16<T>(kin),dl6 = DLog_W_6<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl21 = DLog_W_21<T>(kin),dl18 = DLog_W_18<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),spdl21 = SpDLog_f_3_68_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,32> abbr = 
            {dl26(t) / kin_path.SqrtDelta, f_2_2_9(kin_path), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl11(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_2(kin_path), dl16(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_14(kin_path), f_2_1_15(kin_path), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), dl3(t), f_2_1_1(kin_path), dl4(t), dl21(t), dl18(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t), dl17(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl1(t), dl19(t), dl20(t)}
;

        auto result = f_3_68_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_3_68_abbreviated(const std::array<T,32>& abb)
{
using TR = typename T::value_type;
T z[79];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = bc<TR>[0];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[15];
z[14] = abb[18];
z[15] = abb[20];
z[16] = abb[22];
z[17] = abb[25];
z[18] = abb[26];
z[19] = abb[29];
z[20] = abb[30];
z[21] = abb[31];
z[22] = abb[12];
z[23] = abb[16];
z[24] = abb[13];
z[25] = abb[14];
z[26] = abb[17];
z[27] = abb[19];
z[28] = abb[23];
z[29] = abb[24];
z[30] = abb[27];
z[31] = abb[28];
z[32] = T(8) * z[4];
z[33] = T(4) * z[13];
z[34] = z[32] + z[33];
z[35] = T(2) * z[14];
z[36] = z[12] + z[35];
z[37] = T(3) * z[18];
z[38] = T(5) * z[16] + -z[37];
z[39] = T(2) * z[20];
z[40] = T(2) * z[21];
z[41] = T(-9) * z[15] + T(-4) * z[19] + z[34] + z[36] + -z[38] + -z[39] + -z[40];
z[41] = z[5] * z[41];
z[42] = T(3) * z[16];
z[43] = z[40] + z[42];
z[44] = T(3) * z[14];
z[45] = z[19] + z[20];
z[46] = -z[43] + -z[44] + T(2) * z[45];
z[47] = z[15] + z[46];
z[47] = z[6] * z[47];
z[35] = -z[15] + z[35];
z[48] = z[35] + z[39];
z[49] = T(2) * z[19];
z[50] = -z[37] + z[49];
z[51] = z[16] + T(3) * z[17] + T(-8) * z[21] + -z[48] + -z[50];
z[51] = z[9] * z[51];
z[52] = z[14] + -z[19];
z[53] = z[16] + z[52];
z[54] = z[20] + z[21] + z[53];
z[55] = T(2) * z[22];
z[54] = z[54] * z[55];
z[33] = z[33] + z[45];
z[55] = T(8) * z[15] + -z[44];
z[56] = T(9) * z[16] + z[21] + -z[33] + z[55];
z[56] = z[23] * z[56];
z[57] = T(7) * z[15];
z[52] = -z[12] + z[52];
z[58] = z[52] + z[57];
z[32] = T(8) * z[16] + z[21] + -z[32] + z[58];
z[32] = z[7] * z[32];
z[59] = -z[18] + z[21];
z[60] = -z[17] + z[45];
z[61] = -z[16] + z[59] + z[60];
z[61] = T(3) * z[61];
z[62] = z[29] * z[61];
z[59] = T(3) * z[59];
z[63] = z[31] * z[59];
z[64] = z[15] + -z[19];
z[65] = -(z[8] * z[64]);
z[32] = z[32] + z[41] + z[47] + z[51] + z[54] + z[56] + z[62] + z[63] + z[65];
z[32] = int_to_imaginary<T>(1) * z[32];
z[41] = z[17] / T(2);
z[47] = -z[12] + -z[18] + (T(11) * z[20]) / T(6);
z[47] = (T(-55) * z[14]) / T(24) + (T(41) * z[15]) / T(8) + (T(109) * z[16]) / T(24) + (T(-4) * z[19]) / T(3) + (T(7) * z[21]) / T(12) + -z[41] + z[47] / T(2);
z[47] = z[10] * z[47];
z[32] = z[32] + z[47];
z[32] = z[10] * z[32];
z[47] = T(2) * z[13];
z[39] = z[39] + z[47];
z[51] = -z[21] + z[39];
z[56] = T(4) * z[16];
z[62] = T(4) * z[4];
z[63] = z[56] + -z[62];
z[65] = T(3) * z[15];
z[66] = -z[12] + z[14];
z[49] = z[49] + -z[51] + z[63] + z[65] + -z[66];
z[49] = z[9] * z[49];
z[67] = T(2) * z[16];
z[68] = z[50] + z[67];
z[69] = T(7) * z[14];
z[70] = -z[17] + z[40];
z[71] = T(6) * z[15] + z[39] + z[68] + -z[69] + z[70];
z[71] = z[23] * z[71];
z[72] = -z[16] + z[37];
z[73] = -z[17] + z[62];
z[74] = -z[12] + -z[19] + z[73];
z[75] = T(5) * z[21];
z[55] = z[55] + -z[72] + z[74] + z[75];
z[55] = z[7] * z[55];
z[36] = T(-7) * z[16] + z[19] + -z[36] + z[51] + z[62];
z[36] = z[6] * z[36];
z[51] = T(-2) * z[15] + -z[42] + z[52] + z[73];
z[62] = -z[21] + z[37] + z[51];
z[73] = z[8] * z[62];
z[76] = (T(3) * z[18]) / T(2);
z[34] = z[12] / T(2) + T(5) * z[14] + (T(-5) * z[15]) / T(2) + (T(9) * z[16]) / T(2) + z[19] + -z[20] + -z[34] + -z[70] + z[76];
z[34] = z[5] * z[34];
z[77] = -z[15] + z[47];
z[60] = -z[16] + -z[60] + z[77];
z[78] = z[22] * z[60];
z[34] = z[34] + z[36] + z[49] + z[55] + z[71] + z[73] + z[78];
z[34] = z[5] * z[34];
z[36] = T(2) * z[17];
z[39] = -z[15] + T(3) * z[21] + -z[36] + z[39] + z[50] + -z[56];
z[39] = z[23] * z[39];
z[49] = z[15] + z[20];
z[50] = T(7) * z[21];
z[36] = z[36] + -z[49] + -z[50] + -z[68];
z[36] = z[22] * z[36];
z[51] = z[40] + z[51];
z[51] = z[7] * z[51];
z[48] = -z[19] + z[40] + z[48] + z[67];
z[48] = z[6] * z[48];
z[55] = z[19] / T(2);
z[56] = (T(-3) * z[12]) / T(2) + z[14] + (T(5) * z[16]) / T(2) + -z[20] + T(6) * z[21] + z[55];
z[56] = z[9] * z[56];
z[36] = z[36] + z[39] + z[48] + z[51] + z[56];
z[36] = z[9] * z[36];
z[39] = -(z[7] * z[62]);
z[48] = z[12] + -z[18];
z[51] = z[20] + z[48];
z[51] = -z[14] + z[15] + z[42] + T(3) * z[51] + z[70];
z[51] = z[22] * z[51];
z[35] = z[17] + z[19] + -z[35] + -z[42] + -z[75];
z[35] = z[9] * z[35];
z[42] = -z[19] + z[44] + T(-3) * z[48];
z[42] = -z[15] + z[42] / T(2);
z[42] = z[8] * z[42];
z[44] = z[6] * z[64];
z[35] = z[35] + z[39] + z[42] + z[44] + z[51];
z[35] = z[8] * z[35];
z[39] = (T(3) * z[14]) / T(2) + -z[45];
z[42] = T(-4) * z[15] + (T(3) * z[17]) / T(2) + z[39] + -z[43] + z[47] + z[76];
z[42] = prod_pow(z[23], 2) * z[42];
z[43] = -z[14] + z[21];
z[38] = z[17] + -z[38] + T(-4) * z[43] + -z[57];
z[38] = z[23] * z[38];
z[37] = z[37] + -z[58];
z[37] = z[37] / T(2) + -z[40] + -z[63];
z[37] = z[7] * z[37];
z[37] = z[37] + z[38];
z[37] = z[7] * z[37];
z[38] = -z[46] + -z[77];
z[38] = z[23] * z[38];
z[43] = -z[15] + -z[21] + -z[52] + z[63];
z[43] = z[7] * z[43];
z[39] = -z[15] / T(2) + (T(3) * z[16]) / T(2) + z[21] + z[39];
z[39] = z[6] * z[39];
z[38] = z[38] + z[39] + z[43] + -z[54];
z[38] = z[6] * z[38];
z[33] = T(-5) * z[15] + -z[33] + -z[40] + z[69] + z[72];
z[33] = z[26] * z[33];
z[39] = -(z[23] * z[60]);
z[41] = z[14] / T(2) + z[16] + -z[20] + z[21] + -z[41] + -z[55];
z[41] = z[22] * z[41];
z[39] = z[39] + z[41];
z[39] = z[22] * z[39];
z[40] = z[14] + z[16] + -z[40] + -z[65] + -z[74];
z[40] = z[11] * z[40];
z[41] = z[18] + z[21] + -z[49] + z[66];
z[41] = z[24] * z[41];
z[43] = -z[15] + z[53];
z[43] = z[27] * z[43];
z[41] = z[41] + z[43];
z[43] = z[16] + -z[17] + z[50] + T(3) * z[66];
z[43] = z[25] * z[43];
z[44] = -(z[28] * z[61]);
z[45] = z[0] + z[2];
z[45] = T(2) * z[3] + T(-3) * z[45];
z[45] = z[1] * z[45];
z[46] = -(z[30] * z[59]);
return z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + T(2) * z[40] + T(3) * z[41] + z[42] + z[43] + z[44] + z[45] + z[46];
}



template IntegrandConstructorType<double> f_3_68_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_68_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_68_construct (const Kin<qd_real>&);
#endif

}