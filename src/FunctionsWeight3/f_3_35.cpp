#include "f_3_35.h"

namespace PentagonFunctions {

template <typename T> T f_3_35_abbreviated (const std::array<T,11>&);



template <typename T> IntegrandConstructorType<T> f_3_35_construct (const Kin<T>& kin) {
    return [&kin, 
            dl9 = DLog_W_9<T>(kin),dl11 = DLog_W_11<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl1 = DLog_W_1<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,11> abbr = 
            {dl9(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), dl11(t), rlog(v_path[0]), dl2(t), f_2_1_2(kin_path), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl16(t), dl1(t)}
;

        auto result = f_3_35_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_35_abbreviated(const std::array<T,11>& abb)
{
using TR = typename T::value_type;
T z[19];
z[0] = abb[0];
z[1] = abb[1];
z[2] = bc<TR>[0];
z[3] = abb[2];
z[4] = abb[3];
z[5] = abb[5];
z[6] = abb[9];
z[7] = abb[10];
z[8] = abb[4];
z[9] = abb[6];
z[10] = abb[7];
z[11] = abb[8];
z[12] = z[5] + -z[7];
z[13] = -z[6] + z[12];
z[14] = -(z[11] * z[13]);
z[12] = -z[4] + z[12];
z[15] = z[8] * z[12];
z[16] = z[0] + z[4];
z[17] = -z[6] + -z[7] + z[16];
z[17] = z[1] * z[17];
z[14] = z[14] + z[15] + z[17];
z[14] = int_to_imaginary<T>(1) * z[14];
z[15] = z[0] + -z[7];
z[17] = z[2] * z[15];
z[14] = T(2) * z[14] + z[17];
z[14] = z[2] * z[14];
z[17] = -z[9] + z[10];
z[17] = z[13] * z[17];
z[18] = -z[1] + z[8];
z[13] = z[13] * z[18];
z[15] = z[3] * z[15];
z[13] = T(2) * z[13] + z[15];
z[13] = z[3] * z[13];
z[12] = -(prod_pow(z[8], 2) * z[12]);
z[15] = z[5] + -z[16];
z[15] = prod_pow(z[1], 2) * z[15];
return z[12] + z[13] + z[14] + z[15] + T(2) * z[17];
}



template IntegrandConstructorType<double> f_3_35_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_35_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_35_construct (const Kin<qd_real>&);
#endif

}