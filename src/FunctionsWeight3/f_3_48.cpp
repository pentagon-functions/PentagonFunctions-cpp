#include "f_3_48.h"

namespace PentagonFunctions {

template <typename T> T f_3_48_abbreviated (const std::array<T,33>&);

template <typename T> class SpDLog_f_3_48_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_48_W_23 (const Kin<T>& kin) {
        c[0] = T(2) * prod_pow(kin.v[3], 2) + kin.v[0] * ((T(20) * kin.v[2]) / T(3) + (T(-4) * kin.v[3]) / T(3) + (T(-8) * kin.v[4]) / T(3) + (T(-2) / T(3) + bc<T>[1] * T(-2)) * kin.v[0] + T(4) * kin.v[1] + bc<T>[1] * (T(4) + T(-4) * kin.v[1] + T(4) * kin.v[3])) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(-4) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + bc<T>[1] * T(4) + T(8)) * kin.v[0] + T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[15] * (abb[3] * abb[6] * T(4) + abb[1] * (abb[6] * T(-4) + abb[5] * T(4)) + abb[6] * (abb[6] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[14] * T(4)) + abb[8] * T(8) + abb[5] * (abb[3] * T(-4) + abb[6] * T(-4) + abb[14] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[5] * T(8)));
    }
};
template <typename T> class SpDLog_f_3_48_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_48_W_10 (const Kin<T>& kin) {
        c[0] = (T(4) * kin.v[0] * kin.v[2]) / T(3) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4])) + bc<T>[1] * kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[1] * ((T(4) * kin.v[0]) / T(3) + (bc<T>[1] + T(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (T(-2) + T(2) * kin.v[2] + T(-2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2] + bc<T>[1] * T(-2) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[16] * (abb[2] * abb[6] * T(2) + abb[1] * (abb[6] * T(-2) + abb[4] * T(2)) + abb[6] * (abb[6] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[14] * T(2)) + abb[17] * T(4) + abb[4] * (abb[2] * T(-2) + abb[6] * T(-2) + abb[14] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_48_construct (const Kin<T>& kin) {
    return [&kin, 
            dl20 = DLog_W_20<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl13 = DLog_W_13<T>(kin),dl23 = DLog_W_23<T>(kin),dl10 = DLog_W_10<T>(kin),dl4 = DLog_W_4<T>(kin),dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),spdl23 = SpDLog_f_3_48_W_23<T>(kin),spdl10 = SpDLog_f_3_48_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,33> abbr = 
            {dl20(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[4]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_1(kin_path), f_2_1_8(kin_path), dl27(t) / kin_path.SqrtDelta, f_2_2_4(kin_path), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl13(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl23(t), dl10(t), f_2_1_7(kin_path), dl4(t), f_2_1_2(kin_path), dl1(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl3(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl2(t), dl16(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl17(t), dl18(t), dl5(t)}
;

        auto result = f_3_48_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_3_48_abbreviated(const std::array<T,33>& abb)
{
using TR = typename T::value_type;
T z[74];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[13];
z[10] = abb[18];
z[11] = abb[20];
z[12] = abb[23];
z[13] = abb[26];
z[14] = abb[27];
z[15] = abb[30];
z[16] = abb[31];
z[17] = abb[32];
z[18] = abb[14];
z[19] = bc<TR>[0];
z[20] = abb[9];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[12];
z[24] = abb[17];
z[25] = abb[19];
z[26] = abb[21];
z[27] = abb[22];
z[28] = abb[24];
z[29] = abb[25];
z[30] = abb[28];
z[31] = abb[29];
z[32] = z[4] + z[5];
z[33] = T(3) * z[1];
z[34] = T(3) * z[6];
z[35] = T(3) * z[3];
z[32] = T(4) * z[18] + T(-5) * z[32] + -z[33] + z[34] + z[35];
z[32] = z[5] * z[32];
z[36] = T(4) * z[6];
z[37] = -z[2] + z[4];
z[38] = T(4) * z[1] + T(-7) * z[18] + -z[35] + z[36] + z[37];
z[38] = z[6] * z[38];
z[39] = T(3) * z[31];
z[40] = T(3) * z[4];
z[41] = T(2) * z[1];
z[42] = T(3) * z[18] + -z[39] + z[40] + z[41];
z[36] = -z[2] + T(-4) * z[5] + z[36] + -z[42];
z[36] = z[19] * z[36];
z[43] = z[19] * z[29];
z[44] = T(3) * z[43];
z[36] = z[36] + z[44];
z[36] = int_to_imaginary<T>(1) * z[36];
z[45] = z[1] + -z[2];
z[46] = T(2) * z[45];
z[40] = (T(3) * z[18]) / T(2) + z[40] + z[46];
z[40] = z[18] * z[40];
z[47] = T(3) * z[30];
z[48] = prod_pow(z[2], 2);
z[49] = z[47] + -z[48];
z[50] = T(3) * z[28];
z[51] = z[49] + -z[50];
z[52] = -(z[35] * z[45]);
z[53] = T(2) * z[7];
z[54] = prod_pow(z[19], 2);
z[55] = -z[2] + z[41];
z[56] = z[1] * z[55];
z[57] = T(4) * z[45];
z[58] = -z[4] + -z[57];
z[58] = z[4] * z[58];
z[32] = z[24] + z[32] + z[36] + z[38] + z[40] + z[51] + z[52] + z[53] + (T(-17) * z[54]) / T(24) + z[56] + z[58];
z[32] = z[17] * z[32];
z[36] = T(2) * z[18];
z[38] = z[6] / T(2);
z[52] = z[36] + -z[38];
z[56] = -z[1] + z[3];
z[58] = -z[52] + z[56];
z[58] = z[6] * z[58];
z[59] = (T(3) * z[5]) / T(2);
z[60] = z[18] + z[56];
z[61] = z[6] + z[60];
z[62] = T(4) * z[4] + -z[59] + -z[61];
z[62] = z[5] * z[62];
z[63] = T(3) * z[7];
z[64] = prod_pow(z[4], 2);
z[65] = z[3] * z[45];
z[66] = T(3) * z[2] + -z[41];
z[66] = z[1] * z[66];
z[40] = T(-7) * z[24] + z[40] + z[49] + (T(-43) * z[54]) / T(24) + z[58] + z[62] + -z[63] + T(-2) * z[64] + z[65] + z[66];
z[40] = z[16] * z[40];
z[49] = T(2) * z[3];
z[33] = z[33] + z[34] + z[37] + -z[49];
z[33] = z[6] * z[33];
z[58] = z[18] + z[41];
z[62] = T(2) * z[6];
z[66] = z[5] / T(2);
z[67] = T(-7) * z[4] + z[49] + -z[58] + z[62] + -z[66];
z[67] = z[5] * z[67];
z[68] = z[3] / T(2);
z[69] = z[18] + z[68];
z[55] = -z[55] + z[69];
z[55] = z[3] * z[55];
z[70] = (T(3) * z[1]) / T(2) + -z[2];
z[70] = z[1] * z[70];
z[71] = (T(-3) * z[4]) / T(2) + -z[45];
z[71] = z[4] * z[71];
z[72] = -z[1] + z[4];
z[73] = -z[18] / T(2) + z[72];
z[73] = z[18] * z[73];
z[33] = T(5) * z[24] + z[33] + -z[47] + (T(13) * z[54]) / T(8) + z[55] + z[67] + z[70] + z[71] + z[73];
z[33] = z[14] * z[33];
z[47] = -z[3] + z[37] + z[38] + z[58];
z[47] = z[6] * z[47];
z[55] = z[61] + -z[66];
z[55] = z[5] * z[55];
z[58] = T(2) * z[2];
z[61] = z[1] / T(2);
z[66] = -z[58] + z[61];
z[66] = z[1] * z[66];
z[67] = T(2) * z[4];
z[70] = z[1] + z[67];
z[71] = -z[18] + z[58] + -z[70];
z[71] = z[18] * z[71];
z[46] = z[4] + z[46];
z[46] = z[4] * z[46];
z[46] = z[24] + z[46];
z[69] = z[1] + -z[69];
z[69] = z[3] * z[69];
z[47] = z[46] + z[47] + -z[51] + (T(2) * z[54]) / T(3) + z[55] + z[66] + z[69] + z[71];
z[47] = z[15] * z[47];
z[51] = z[1] + -z[67];
z[49] = z[6] + -z[18] + -z[49] + T(2) * z[51] + z[59];
z[49] = z[5] * z[49];
z[51] = prod_pow(z[1], 2);
z[51] = -z[48] + z[51];
z[59] = -z[24] + z[65];
z[66] = T(3) * z[26];
z[67] = (T(5) * z[54]) / T(12);
z[69] = z[4] + z[45];
z[71] = z[18] + T(2) * z[69];
z[71] = z[18] * z[71];
z[73] = z[6] + -z[60];
z[73] = z[6] * z[73];
z[49] = (T(-5) * z[7]) / T(2) + z[49] + z[51] + T(-2) * z[59] + z[64] / T(2) + z[66] + -z[67] + z[71] + z[73];
z[49] = z[11] * z[49];
z[41] = z[37] + -z[41] + z[52];
z[41] = z[6] * z[41];
z[36] = z[36] + z[70];
z[52] = z[6] + -z[35] + z[36] + z[58];
z[52] = z[19] * z[52];
z[52] = -z[44] + z[52];
z[52] = int_to_imaginary<T>(1) * z[52];
z[59] = -z[2] + z[61];
z[59] = z[1] * z[59];
z[48] = z[48] / T(2) + z[59];
z[59] = -z[45] + z[68];
z[61] = z[35] * z[59];
z[64] = -z[4] + z[45];
z[64] = -z[18] + T(2) * z[64];
z[64] = z[18] * z[64];
z[41] = z[41] + z[46] + z[48] + z[50] + z[52] + z[61] + z[64] + z[67];
z[41] = z[12] * z[41];
z[46] = z[48] + -z[65];
z[38] = -z[38] + -z[60];
z[38] = z[6] * z[38];
z[52] = z[5] + -z[6];
z[60] = T(3) * z[45] + -z[52];
z[61] = int_to_imaginary<T>(1) * z[19];
z[60] = z[60] * z[61];
z[54] = z[54] / T(2);
z[38] = z[38] + T(3) * z[46] + -z[54] + z[55] + z[60] + z[63];
z[38] = z[10] * z[38];
z[46] = z[45] * z[72];
z[55] = z[4] + z[56];
z[56] = -z[52] + z[55];
z[56] = z[5] * z[56];
z[37] = -z[3] + -z[37];
z[37] = z[6] * z[37];
z[37] = z[37] + z[46] + -z[53] + z[56] + z[65];
z[37] = z[0] * z[37];
z[34] = z[5] + z[18] + z[34] + -z[39] + -z[55];
z[34] = z[14] * z[34];
z[42] = -z[42] + z[52] + z[58];
z[42] = z[16] * z[42];
z[46] = -z[18] + -z[69];
z[35] = z[5] + z[6] + z[35] + T(2) * z[46];
z[35] = z[11] * z[35];
z[34] = z[34] + z[35] + z[42];
z[34] = z[19] * z[34];
z[35] = z[2] + z[3] + -z[5] + z[36] + -z[39] + z[62];
z[35] = z[19] * z[35];
z[35] = z[35] + -z[44];
z[35] = z[15] * z[35];
z[36] = -z[1] + -z[6];
z[36] = z[19] * z[36];
z[36] = z[36] + z[43];
z[36] = z[13] * z[36];
z[39] = -z[11] + z[13];
z[39] = z[19] * z[27] * z[39];
z[36] = z[36] + z[39];
z[34] = z[34] + z[35] + T(3) * z[36];
z[34] = int_to_imaginary<T>(1) * z[34];
z[35] = z[6] + -z[59];
z[35] = z[3] * z[35];
z[36] = -z[4] / T(2) + -z[45];
z[36] = z[4] * z[36];
z[39] = z[4] + -z[6];
z[39] = z[5] * z[39];
z[35] = -z[24] + z[35] + z[36] + z[39] + -z[48];
z[35] = T(3) * z[35] + -z[50] + z[54] + -z[66];
z[35] = z[13] * z[35];
z[36] = z[45] * z[61];
z[39] = -(z[18] * z[57]);
z[36] = (T(3) * z[7]) / T(2) + z[36] + z[39] + -z[51] / T(2) + T(4) * z[65];
z[36] = z[9] * z[36];
z[39] = z[10] + -z[11] + z[12] + -z[13] + z[14] + -z[16];
z[39] = z[25] * z[39];
z[42] = T(2) * z[20] + z[22] + z[23];
z[42] = z[21] * z[42];
z[43] = -z[0] + -z[10] + T(2) * z[11] + T(3) * z[14] + -z[15] + -z[16] + -z[17];
z[43] = z[8] * z[43];
return z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + T(3) * z[39] + z[40] + z[41] + z[42] + T(2) * z[43] + z[47] + z[49];
}



template IntegrandConstructorType<double> f_3_48_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_48_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_48_construct (const Kin<qd_real>&);
#endif

}