#include "f_3_47.h"

namespace PentagonFunctions {

template <typename T> T f_3_47_abbreviated (const std::array<T,33>&);

template <typename T> class SpDLog_f_3_47_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_47_W_10 (const Kin<T>& kin) {
        c[0] = (T(4) * kin.v[0] * kin.v[2]) / T(3) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4])) + bc<T>[1] * kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[1] * ((T(4) * kin.v[0]) / T(3) + (bc<T>[1] + T(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (T(-2) + T(2) * kin.v[2] + T(-2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2] + bc<T>[1] * T(-2) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[12] * (abb[6] * abb[14] * T(2) + abb[14] * (abb[14] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * T(2)) + abb[5] * (abb[14] * T(-2) + abb[13] * T(2)) + abb[15] * T(4) + abb[13] * (abb[6] * T(-2) + abb[8] * T(-2) + abb[14] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[13] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_47_construct (const Kin<T>& kin) {
    return [&kin, 
            dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl13 = DLog_W_13<T>(kin),dl11 = DLog_W_11<T>(kin),dl10 = DLog_W_10<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),spdl10 = SpDLog_f_3_47_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);

        std::array<std::complex<T>,33> abbr = 
            {dl27(t) / kin_path.SqrtDelta, f_2_2_4(kin_path), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), dl11(t), f_2_1_2(kin_path), dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), dl4(t), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_8(kin_path), dl2(t), f_2_1_5(kin_path), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[2]), dl1(t), dl20(t), dl3(t), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl5(t), dl16(t), dl17(t)}
;

        auto result = f_3_47_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_3_47_abbreviated(const std::array<T,33>& abb)
{
using TR = typename T::value_type;
T z[73];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[7];
z[7] = abb[8];
z[8] = bc<TR>[0];
z[9] = abb[6];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[16];
z[13] = abb[19];
z[14] = abb[25];
z[15] = abb[26];
z[16] = abb[31];
z[17] = abb[32];
z[18] = abb[24];
z[19] = abb[27];
z[20] = abb[30];
z[21] = abb[13];
z[22] = abb[14];
z[23] = abb[17];
z[24] = abb[11];
z[25] = abb[15];
z[26] = abb[18];
z[27] = abb[20];
z[28] = abb[21];
z[29] = abb[22];
z[30] = abb[23];
z[31] = abb[28];
z[32] = abb[29];
z[33] = z[16] + -z[20];
z[34] = -z[19] + z[33];
z[35] = z[21] * z[34];
z[36] = -z[5] + z[30];
z[36] = z[14] * z[36];
z[37] = z[5] + z[22];
z[38] = -z[29] + -z[30] + z[37];
z[38] = z[13] * z[38];
z[34] = z[17] + z[34];
z[34] = z[32] * z[34];
z[34] = z[34] + -z[35] + z[36] + z[38];
z[35] = T(3) * z[7];
z[36] = T(3) * z[30];
z[38] = T(2) * z[5];
z[39] = z[9] + z[35] + -z[36] + z[38];
z[39] = z[20] * z[39];
z[40] = T(2) * z[9];
z[41] = z[5] + -z[22];
z[42] = z[40] + -z[41];
z[43] = T(2) * z[7];
z[36] = T(2) * z[21] + -z[36] + z[43];
z[44] = z[6] + -z[36] + -z[42];
z[44] = z[15] * z[44];
z[36] = -z[23] + z[36];
z[45] = T(4) * z[5] + -z[22];
z[46] = T(4) * z[6];
z[47] = T(3) * z[29] + z[36] + z[45] + -z[46];
z[47] = z[18] * z[47];
z[48] = z[22] + z[38];
z[49] = -z[40] + z[48];
z[50] = z[7] + z[49];
z[50] = z[19] * z[50];
z[51] = T(2) * z[22];
z[52] = z[5] + z[51];
z[36] = -z[6] + -z[9] + -z[36] + -z[52];
z[36] = z[17] * z[36];
z[53] = -z[6] + z[41];
z[54] = -z[7] + -z[53];
z[54] = z[16] * z[54];
z[55] = -z[16] + -z[19];
z[55] = z[23] * z[55];
z[56] = -z[5] + z[9];
z[57] = -z[22] + z[56];
z[58] = z[23] + z[57];
z[58] = z[12] * z[58];
z[59] = z[4] * z[56];
z[34] = T(3) * z[34] + z[36] + z[39] + z[44] + z[47] + z[50] + z[54] + z[55] + z[58] + z[59];
z[36] = int_to_imaginary<T>(1) * z[8];
z[34] = z[34] * z[36];
z[39] = z[21] + -z[43] + z[49];
z[39] = z[21] * z[39];
z[39] = z[25] + z[39];
z[44] = -z[7] + z[53];
z[47] = z[23] / T(2);
z[50] = z[44] + z[47];
z[50] = z[23] * z[50];
z[54] = z[5] / T(2);
z[55] = -z[51] + -z[54];
z[55] = z[5] * z[55];
z[42] = z[7] + -z[42];
z[42] = z[7] * z[42];
z[58] = z[6] / T(2) + z[7];
z[59] = -z[41] + z[58];
z[59] = z[6] * z[59];
z[60] = T(3) * z[31];
z[61] = prod_pow(z[22], 2);
z[62] = z[61] / T(2);
z[63] = T(3) * z[27];
z[64] = -z[9] + z[38];
z[65] = z[22] + z[64];
z[65] = z[9] * z[65];
z[42] = -z[39] + z[42] + z[50] + z[55] + z[59] + z[60] + -z[62] + -z[63] + z[65];
z[42] = z[17] * z[42];
z[55] = z[37] + -z[40] + z[58];
z[55] = z[6] * z[55];
z[43] = (T(3) * z[21]) / T(2) + -z[43] + T(-3) * z[56];
z[43] = z[21] * z[43];
z[48] = T(2) * z[6] + -z[48];
z[47] = z[7] + z[47] + z[48];
z[47] = z[23] * z[47];
z[59] = T(3) * z[28];
z[65] = z[5] * z[22];
z[66] = (T(3) * z[9]) / T(2);
z[67] = -z[5] + z[66];
z[67] = z[9] * z[67];
z[45] = -z[7] + T(3) * z[9] + -z[45];
z[45] = z[7] * z[45];
z[43] = (T(3) * z[10]) / T(2) + (T(13) * z[24]) / T(2) + T(2) * z[25] + z[43] + z[45] + z[47] + z[55] + -z[59] + -z[61] + z[63] + -z[65] + z[67];
z[43] = z[18] * z[43];
z[45] = z[37] + -z[66];
z[45] = z[9] * z[45];
z[47] = -(z[21] * z[49]);
z[55] = T(2) * z[26];
z[67] = prod_pow(z[5], 2);
z[68] = z[67] / T(2);
z[69] = z[22] + z[56];
z[70] = z[6] * z[69];
z[71] = -z[10] + z[63];
z[72] = z[21] + -z[23] + -z[53];
z[72] = z[23] * z[72];
z[45] = z[45] + z[47] + -z[55] + z[68] + -z[70] + -z[71] + z[72];
z[45] = z[14] * z[45];
z[47] = -z[52] + z[66];
z[47] = z[9] * z[47];
z[66] = -z[21] / T(2) + z[51] + -z[56];
z[66] = z[21] * z[66];
z[72] = (T(3) * z[6]) / T(2) + z[57];
z[72] = z[6] * z[72];
z[48] = -z[21] + -z[48];
z[48] = z[23] * z[48];
z[47] = -z[24] + -z[25] + z[47] + z[48] + z[59] + z[63] + z[66] + -z[68] + z[72];
z[47] = z[13] * z[47];
z[48] = z[51] + -z[54];
z[48] = z[5] * z[48];
z[51] = z[9] / T(2);
z[41] = -z[41] + -z[51];
z[41] = z[9] * z[41];
z[52] = z[7] + z[40] + -z[52];
z[52] = z[7] * z[52];
z[59] = -z[58] + z[64];
z[59] = z[6] * z[59];
z[62] = T(2) * z[24] + -z[62];
z[39] = -z[39] + z[41] + z[48] + z[52] + z[59] + -z[62] + -z[71];
z[39] = z[15] * z[39];
z[41] = z[22] + z[54];
z[41] = z[5] * z[41];
z[37] = -(z[9] * z[37]);
z[48] = z[7] / T(2);
z[52] = z[5] + T(-4) * z[22] + z[48];
z[52] = z[7] * z[52];
z[54] = z[9] + -z[58];
z[54] = z[6] * z[54];
z[37] = z[24] + T(-5) * z[25] + z[37] + z[41] + z[52] + z[54] + z[60] + z[61];
z[37] = z[16] * z[37];
z[41] = -z[48] + T(2) * z[69];
z[41] = z[7] * z[41];
z[48] = T(3) * z[25] + -z[60];
z[52] = T(2) * z[10] + z[62];
z[54] = z[9] * z[56];
z[41] = z[41] + z[48] + z[52] + z[54] + z[65] + -z[70];
z[41] = z[19] * z[41];
z[53] = z[20] * z[53];
z[54] = z[7] * z[16];
z[44] = -(z[19] * z[44]);
z[58] = z[21] * z[33];
z[59] = (T(5) * z[16]) / T(2) + -z[19] / T(2) + z[20];
z[59] = z[23] * z[59];
z[44] = z[44] + z[53] + z[54] + z[58] + z[59];
z[44] = z[23] * z[44];
z[53] = -z[6] + -z[7];
z[53] = z[53] * z[57];
z[51] = z[38] + -z[51];
z[51] = z[9] * z[51];
z[54] = (T(-3) * z[5]) / T(2) + -z[22];
z[54] = z[5] * z[54];
z[50] = z[50] + z[51] + -z[52] + z[53] + z[54];
z[50] = z[12] * z[50];
z[51] = -z[6] + z[9];
z[51] = z[51] * z[57];
z[38] = (T(-3) * z[7]) / T(2) + T(3) * z[22] + -z[38] + z[40];
z[38] = z[7] * z[38];
z[38] = z[38] + z[48] + z[51] + z[71];
z[38] = z[20] * z[38];
z[40] = (T(-5) * z[21]) / T(2) + z[35] + z[69];
z[40] = z[16] * z[40];
z[48] = -z[35] + z[49];
z[48] = z[20] * z[48];
z[35] = -(z[19] * z[35]);
z[35] = z[35] + z[40] + z[48];
z[35] = z[21] * z[35];
z[40] = T(-4) * z[7] + z[46];
z[40] = z[40] * z[56];
z[46] = prod_pow(z[9], 2);
z[46] = T(-3) * z[10] + -z[46] + z[67];
z[40] = z[40] + z[46] / T(2);
z[40] = z[4] * z[40];
z[46] = prod_pow(z[6], 2);
z[46] = -z[24] + -z[46] + z[67];
z[48] = -z[5] + z[6];
z[36] = z[36] * z[48];
z[36] = z[36] + z[46] / T(2);
z[36] = z[11] * z[36];
z[33] = z[12] + z[17] + -z[19] + -z[33];
z[33] = z[33] * z[55];
z[46] = T(-2) * z[0] + z[2] + z[3];
z[46] = z[1] * z[46];
z[48] = (T(-23) * z[16]) / T(3) + T(3) * z[19] + T(13) * z[20];
z[48] = z[12] / T(3) + -z[13] / T(2) + (T(-7) * z[15]) / T(12) + (T(-2) * z[17]) / T(3) + (T(3) * z[18]) / T(4) + z[48] / T(8);
z[48] = prod_pow(z[8], 2) * z[48];
return z[33] + z[34] + z[35] + T(3) * z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[50];
}



template IntegrandConstructorType<double> f_3_47_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_47_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_47_construct (const Kin<qd_real>&);
#endif

}