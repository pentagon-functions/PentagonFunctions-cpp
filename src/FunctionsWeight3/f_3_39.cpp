#include "f_3_39.h"

namespace PentagonFunctions {

template <typename T> T f_3_39_abbreviated (const std::array<T,5>&);

template <typename T> class SpDLog_f_3_39_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_39_W_10 (const Kin<T>& kin) {
        c[0] = (-kin.v[4] / T(2) + T(-1) / T(2) + kin.v[2] / T(8)) * kin.v[2] + (-kin.v[4] / T(2) + T(-1) / T(2) + kin.v[1] / T(8) + kin.v[2] / T(4)) * kin.v[1];
c[1] = kin.v[1] / T(2) + kin.v[2] / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[4] * (prod_pow(abb[2], 2) / T(4) + -abb[3] / T(2) + abb[1] * (abb[2] / T(2) + (abb[1] * T(-3)) / T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_39_construct (const Kin<T>& kin) {
    return [&kin, 
            dl17 = DLog_W_17<T>(kin),dl10 = DLog_W_10<T>(kin),spdl10 = SpDLog_f_3_39_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);

        std::array<std::complex<T>,5> abbr = 
            {dl17(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), dl10(t)}
;

        auto result = f_3_39_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_3_39_abbreviated(const std::array<T,5>& abb)
{
T z[6];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = prod_pow(z[1], 2);
z[5] = z[1] + (T(-3) * z[2]) / T(2);
z[5] = z[2] * z[5];
z[4] = -z[3] + z[4] / T(2) + z[5];
return (z[0] * z[4]) / T(2);
}



template IntegrandConstructorType<double> f_3_39_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_39_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_39_construct (const Kin<qd_real>&);
#endif

}