#include "f_3_98.h"

namespace PentagonFunctions {

template <typename T> T f_3_98_abbreviated (const std::array<T,29>&);



template <typename T> IntegrandConstructorType<T> f_3_98_construct (const Kin<T>& kin) {
    return [&kin, 
            dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl31 = DLog_W_31<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,29> abbr = 
            {dl30(t) / kin_path.SqrtDelta, rlog(-v_path[1]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_3(kin_path), f_2_1_6(kin_path), f_2_1_8(kin_path), f_2_1_10(kin_path), rlog(v_path[0] + v_path[1]), rlog(-v_path[1] + v_path[3]), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl26(t) / kin_path.SqrtDelta, rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl27(t) / kin_path.SqrtDelta, f_2_1_11(kin_path), dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl1(t), f_2_2_5(kin_path), f_1_3_1(kin_path), f_1_3_3(kin_path), f_1_3_5(kin_path), dl5(t), dl20(t), dl31(t)}
;

        auto result = f_3_98_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_98_abbreviated(const std::array<T,29>& abb)
{
using TR = typename T::value_type;
T z[49];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[12];
z[14] = abb[19];
z[15] = abb[20];
z[16] = abb[17];
z[17] = abb[14];
z[18] = abb[13];
z[19] = abb[15];
z[20] = abb[16];
z[21] = abb[18];
z[22] = abb[21];
z[23] = abb[22];
z[24] = abb[23];
z[25] = abb[24];
z[26] = abb[25];
z[27] = abb[26];
z[28] = abb[27];
z[29] = abb[28];
z[30] = z[24] + -z[25] + z[26];
z[31] = T(-2) * z[30];
z[32] = z[22] + z[27] + z[28];
z[31] = z[31] * z[32];
z[33] = z[0] + z[14];
z[34] = z[18] + z[33];
z[35] = -(z[11] * z[34]);
z[36] = -z[0] + z[15];
z[37] = z[12] * z[36];
z[38] = z[14] + z[15];
z[39] = -z[18] + z[38];
z[40] = -z[16] + z[39];
z[41] = z[20] * z[40];
z[42] = z[16] + z[33];
z[42] = z[1] * z[42];
z[30] = z[29] * z[30];
z[43] = z[0] + -z[15] + z[18];
z[44] = z[4] * z[43];
z[45] = z[18] + T(-2) * z[38];
z[45] = z[17] * z[45];
z[46] = z[16] + -z[43];
z[47] = z[13] * z[46];
z[48] = T(2) * z[18] + -z[38];
z[48] = z[6] * z[48];
z[30] = T(4) * z[30] + z[31] + z[35] + z[37] + z[41] + z[42] + z[44] + z[45] + z[47] + z[48];
z[30] = int_to_imaginary<T>(1) * z[30];
z[31] = T(4) * z[32] + -z[36];
z[31] = -z[16] + z[18] + T(-16) * z[29] + T(2) * z[31];
z[31] = z[3] * z[31];
z[30] = T(6) * z[30] + z[31];
z[30] = z[3] * z[30];
z[31] = z[7] * z[36];
z[35] = -z[16] + z[33];
z[36] = -z[18] + z[35];
z[36] = z[9] * z[36];
z[37] = -(z[19] * z[40]);
z[40] = z[10] * z[46];
z[41] = z[16] + z[38];
z[41] = z[21] * z[41];
z[31] = z[31] + z[36] + z[37] + z[40] + z[41];
z[36] = -z[2] + z[6];
z[36] = z[35] * z[36];
z[37] = int_to_imaginary<T>(1) * z[3];
z[40] = z[37] * z[43];
z[39] = z[17] * z[39];
z[41] = -(z[1] * z[16]);
z[36] = z[36] + z[39] + z[40] + z[41];
z[35] = -(z[5] * z[35]);
z[35] = z[35] + T(2) * z[36];
z[35] = z[5] * z[35];
z[36] = z[1] + -z[4];
z[36] = z[33] * z[36];
z[40] = -z[16] + T(2) * z[33];
z[37] = z[37] * z[40];
z[40] = z[6] * z[18];
z[36] = z[36] + z[37] + z[40];
z[33] = z[2] * z[33];
z[33] = z[33] + T(2) * z[36];
z[33] = z[2] * z[33];
z[36] = z[1] * z[38];
z[37] = z[4] * z[38];
z[41] = T(-2) * z[36] + z[37];
z[41] = z[4] * z[41];
z[36] = -z[36] + z[37];
z[37] = z[16] + z[18];
z[37] = z[17] * z[37];
z[36] = T(2) * z[36] + z[37];
z[36] = z[17] * z[36];
z[37] = -(z[4] * z[18]);
z[37] = z[37] + -z[39];
z[37] = T(2) * z[37] + -z[40];
z[37] = z[6] * z[37];
z[38] = prod_pow(z[1], 2) * z[38];
z[31] = T(2) * z[31] + z[33] + z[35] + z[36] + z[37] + z[38] + z[41];
z[32] = T(-2) * z[29] + z[32];
z[32] = z[23] * z[32];
z[33] = z[8] * z[34];
z[32] = z[32] + z[33];
return z[30] + T(3) * z[31] + T(6) * z[32];
}



template IntegrandConstructorType<double> f_3_98_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_98_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_98_construct (const Kin<qd_real>&);
#endif

}