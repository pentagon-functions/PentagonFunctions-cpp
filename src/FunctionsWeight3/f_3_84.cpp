#include "f_3_84.h"

namespace PentagonFunctions {

template <typename T> T f_3_84_abbreviated (const std::array<T,33>&);

template <typename T> class SpDLog_f_3_84_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_84_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(-8) * kin.v[0]) / T(3) + (bc<T>[1] + T(-1)) * kin.v[1] + T(2) * kin.v[2] + T(4) * kin.v[3] + bc<T>[1] * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[3])) + kin.v[2] * (T(3) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(-3) * kin.v[3] + T(-2) * kin.v[4]) + prod_pow(kin.v[4], 2) + ((T(-8) * kin.v[2]) / T(3) + (T(8) * kin.v[3]) / T(3) + (T(8) * kin.v[4]) / T(3)) * kin.v[0] + bc<T>[1] * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[1] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[16] * (abb[18] * T(-4) + abb[6] * abb[17] * T(-2) + abb[7] * abb[17] * T(-2) + abb[5] * (abb[11] * T(-2) + abb[17] * T(2)) + abb[17] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[17] * T(2)) + abb[11] * (abb[11] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[6] * T(2) + abb[7] * T(2) + abb[17] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_84_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl13 = DLog_W_13<T>(kin),dl18 = DLog_W_18<T>(kin),dl6 = DLog_W_6<T>(kin),dl22 = DLog_W_22<T>(kin),dl5 = DLog_W_5<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl1 = DLog_W_1<T>(kin),spdl22 = SpDLog_f_3_84_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,33> abbr = 
            {dl26(t) / kin_path.SqrtDelta, f_2_2_8(kin_path), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), dl18(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), f_2_1_9(kin_path), dl6(t), dl22(t), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), dl5(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl3(t), dl4(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl20(t), dl17(t), dl16(t), dl19(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl1(t)}
;

        auto result = f_3_84_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_84_abbreviated(const std::array<T,33>& abb)
{
using TR = typename T::value_type;
T z[67];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[7];
z[7] = abb[8];
z[8] = bc<TR>[0];
z[9] = abb[6];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[15];
z[13] = abb[19];
z[14] = abb[22];
z[15] = abb[27];
z[16] = abb[28];
z[17] = abb[29];
z[18] = abb[23];
z[19] = abb[26];
z[20] = abb[32];
z[21] = abb[11];
z[22] = abb[12];
z[23] = abb[17];
z[24] = abb[13];
z[25] = abb[14];
z[26] = abb[18];
z[27] = abb[20];
z[28] = abb[21];
z[29] = abb[24];
z[30] = abb[25];
z[31] = abb[30];
z[32] = abb[31];
z[33] = T(2) * z[16];
z[34] = -z[18] + z[33];
z[35] = z[13] + -z[19];
z[36] = z[15] + z[20];
z[37] = z[14] + z[17];
z[38] = T(2) * z[11];
z[39] = z[34] + z[35] + -z[36] + -z[37] + z[38];
z[39] = z[9] * z[39];
z[40] = T(4) * z[4];
z[41] = z[17] + -z[20];
z[42] = z[11] + z[19];
z[43] = T(2) * z[14];
z[44] = z[16] + -z[18];
z[45] = z[13] + -z[40] + -z[41] + z[42] + z[43] + -z[44];
z[45] = z[7] * z[45];
z[33] = z[33] + z[43];
z[46] = T(3) * z[20];
z[47] = -z[15] + z[17];
z[48] = T(2) * z[19];
z[49] = T(-2) * z[13] + -z[33] + z[46] + z[47] + z[48];
z[49] = z[23] * z[49];
z[50] = T(2) * z[18];
z[48] = -z[11] + -z[16] + z[48] + z[50];
z[51] = T(4) * z[20];
z[52] = T(3) * z[12];
z[53] = z[51] + -z[52];
z[54] = T(3) * z[17];
z[55] = z[14] + -z[15];
z[56] = -z[4] + T(-3) * z[13] + z[48] + z[53] + z[54] + z[55];
z[57] = int_to_imaginary<T>(1) * z[8];
z[56] = z[56] * z[57];
z[58] = z[11] + z[44];
z[59] = -z[35] + z[58];
z[60] = T(2) * z[17];
z[61] = T(2) * z[20];
z[62] = z[59] + z[60] + -z[61];
z[62] = z[22] * z[62];
z[63] = z[40] + -z[55];
z[48] = -z[48] + -z[51] + z[63];
z[48] = z[6] * z[48];
z[51] = z[15] + -z[16];
z[52] = z[4] + T(-3) * z[11] + z[13] + -z[37] + z[51] + z[52];
z[52] = z[5] * z[52];
z[39] = z[39] + z[45] + z[48] + z[49] + z[52] / T(2) + z[56] + z[62];
z[39] = z[5] * z[39];
z[45] = T(-5) * z[16] + z[17] + z[18];
z[48] = T(3) * z[15];
z[52] = z[11] / T(2);
z[56] = z[14] + z[35];
z[45] = z[45] / T(2) + z[48] + -z[52] + z[56] + -z[61];
z[45] = z[21] * z[45];
z[33] = z[33] + -z[61];
z[62] = -z[15] + z[19];
z[64] = -z[13] + z[17];
z[65] = T(3) * z[18];
z[62] = -z[33] + T(4) * z[62] + z[64] + z[65];
z[62] = z[23] * z[62];
z[60] = -z[51] + z[56] + -z[60];
z[60] = z[9] * z[60];
z[34] = z[11] + z[20] + z[34];
z[54] = -z[34] + z[54] + -z[55];
z[54] = z[54] * z[57];
z[57] = -z[41] + z[59];
z[57] = z[7] * z[57];
z[34] = -z[15] + z[34] + -z[43];
z[66] = -z[5] + z[6];
z[34] = z[34] * z[66];
z[66] = T(-2) * z[15] + z[41] + z[59];
z[66] = z[22] * z[66];
z[34] = z[34] + z[45] + z[54] + z[57] + z[60] + z[62] + z[66];
z[34] = z[21] * z[34];
z[45] = T(3) * z[19];
z[54] = z[45] + z[65];
z[48] = z[33] + z[48] + -z[54];
z[48] = z[23] * z[48];
z[36] = z[36] + -z[58];
z[36] = z[22] * z[36];
z[36] = z[36] + z[48];
z[48] = z[16] + -z[17] + -z[20] + z[56];
z[48] = z[28] * z[48];
z[58] = -z[19] + z[44];
z[60] = z[15] + z[58];
z[62] = z[30] * z[60];
z[41] = -(z[32] * z[41]);
z[41] = z[41] + z[48] + z[62];
z[48] = z[4] + -z[16] + z[42] + -z[43] + -z[50];
z[48] = z[9] * z[48];
z[51] = z[14] + z[51];
z[53] = z[51] + -z[53];
z[53] = z[7] * z[53];
z[33] = -z[15] + z[18] + -z[33] + z[45];
z[33] = z[6] * z[33];
z[33] = z[33] + -z[36] + T(3) * z[41] + z[48] + z[53];
z[33] = int_to_imaginary<T>(1) * z[33];
z[41] = (T(-7) * z[14]) / T(6) + (T(-11) * z[15]) / T(12) + -z[16] / T(3) + -z[18] / T(4) + (T(9) * z[19]) / T(4) + z[20] / T(2);
z[41] = z[11] / T(3) + z[41] / T(2);
z[41] = z[8] * z[41];
z[33] = z[33] + z[41];
z[33] = z[8] * z[33];
z[41] = z[4] + z[14];
z[45] = (T(3) * z[20]) / T(2);
z[48] = (T(3) * z[17]) / T(2);
z[41] = (T(-3) * z[13]) / T(2) + -z[41] / T(2) + z[45] + z[48] + -z[52] + -z[58];
z[41] = z[9] * z[41];
z[41] = z[41] + -z[49];
z[41] = z[9] * z[41];
z[49] = z[14] + z[16];
z[53] = -z[35] + -z[49];
z[53] = z[27] * z[53];
z[62] = z[27] + z[31];
z[62] = z[20] * z[62];
z[65] = z[27] + -z[31];
z[65] = z[17] * z[65];
z[60] = z[29] * z[60];
z[53] = z[53] + -z[60] + z[62] + z[65];
z[42] = -z[18] + -z[42] + -z[61] + z[63] + z[64];
z[42] = z[9] * z[42];
z[60] = (T(3) * z[12]) / T(2);
z[62] = -z[51] / T(2) + -z[60] + z[61];
z[62] = z[7] * z[62];
z[42] = z[42] + z[62];
z[42] = z[7] * z[42];
z[44] = T(7) * z[15] + -z[44];
z[44] = z[44] / T(2) + -z[48] + -z[52] + z[61];
z[44] = z[22] * z[44];
z[35] = z[35] + -z[47];
z[35] = z[23] * z[35];
z[35] = z[35] + z[44] + -z[57];
z[35] = z[22] * z[35];
z[44] = z[14] + -z[58];
z[40] = -z[11] + -z[40] + T(2) * z[44] + z[46];
z[40] = z[9] * z[40];
z[44] = z[20] + -z[51];
z[44] = z[7] * z[44];
z[46] = -z[16] / T(2) + z[18] + -z[20] + z[55];
z[46] = z[6] * z[46];
z[36] = z[36] + z[40] + z[44] + z[46];
z[36] = z[6] * z[36];
z[38] = z[38] + -z[50];
z[40] = (T(13) * z[20]) / T(2) + -z[38] + -z[43] + -z[47] + -z[60];
z[40] = z[25] * z[40];
z[37] = T(5) * z[15] + z[16] + z[37] + -z[54] + -z[61];
z[37] = z[26] * z[37];
z[38] = (T(-3) * z[4]) / T(2) + -z[38] + z[45] + z[56];
z[38] = z[10] * z[38];
z[43] = z[20] + -z[49] + -z[54];
z[43] = prod_pow(z[23], 2) * z[43];
z[44] = z[15] + -z[59];
z[44] = z[24] * z[44];
z[45] = -z[0] + -z[2] + -z[3];
z[45] = z[1] * z[45];
return z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] / T(2) + T(2) * z[44] + z[45] + T(3) * z[53];
}



template IntegrandConstructorType<double> f_3_84_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_84_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_84_construct (const Kin<qd_real>&);
#endif

}