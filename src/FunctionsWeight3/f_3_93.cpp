#include "f_3_93.h"

namespace PentagonFunctions {

template <typename T> T f_3_93_abbreviated (const std::array<T,31>&);



template <typename T> IntegrandConstructorType<T> f_3_93_construct (const Kin<T>& kin) {
    return [&kin, 
            dl31 = DLog_W_31<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,31> abbr = 
            {dl31(t), f_2_2_1(kin_path), f_2_2_2(kin_path), f_2_2_3(kin_path), f_2_2_4(kin_path), f_2_2_5(kin_path), f_2_2_6(kin_path), f_2_2_7(kin_path), f_2_2_8(kin_path), f_2_2_9(kin_path), dl3(t), dl16(t), dl19(t), dl26(t) / kin_path.SqrtDelta, rlog(v_path[0]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[1]), rlog(-v_path[4]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_4(kin_path), f_2_1_7(kin_path), f_2_1_9(kin_path), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl30(t) / kin_path.SqrtDelta, rlog(v_path[3]), f_2_1_2(kin_path), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_3_93_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_93_abbreviated(const std::array<T,31>& abb)
{
using TR = typename T::value_type;
T z[45];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[11];
z[12] = abb[12];
z[13] = abb[13];
z[14] = abb[14];
z[15] = abb[15];
z[16] = abb[16];
z[17] = abb[17];
z[18] = abb[18];
z[19] = bc<TR>[0];
z[20] = abb[19];
z[21] = abb[20];
z[22] = abb[21];
z[23] = abb[22];
z[24] = abb[23];
z[25] = abb[24];
z[26] = abb[29];
z[27] = abb[30];
z[28] = abb[25];
z[29] = abb[26];
z[30] = abb[27];
z[31] = abb[28];
z[32] = z[10] + z[12];
z[33] = z[1] + z[2] + -z[3] + z[4] + z[5] + z[6] + -z[8] + -z[9];
z[34] = -(z[32] * z[33]);
z[35] = -z[25] + -z[26];
z[35] = z[21] * z[35];
z[36] = -z[25] + z[27];
z[37] = z[26] + z[36];
z[38] = z[23] * z[37];
z[39] = -(z[20] * z[27]);
z[40] = z[20] + -z[21] + -z[23];
z[40] = z[13] * z[40];
z[41] = z[26] + z[27];
z[42] = -z[13] + -z[41];
z[42] = z[22] * z[42];
z[43] = z[30] * z[36];
z[32] = T(2) * z[0] + -z[11] + -z[32];
z[32] = z[7] * z[32];
z[44] = z[25] + -z[26];
z[44] = z[29] * z[44];
z[32] = z[32] + z[34] + z[35] + z[38] + z[39] + z[40] + z[42] + z[43] + z[44];
z[34] = z[18] * z[37];
z[35] = z[28] * z[37];
z[38] = z[13] + z[25];
z[38] = z[15] * z[38];
z[39] = -(z[16] * z[27]);
z[35] = z[34] + z[35] + z[38] + z[39];
z[38] = z[14] * z[37];
z[35] = T(2) * z[35] + -z[38];
z[35] = z[14] * z[35];
z[39] = z[18] * z[41];
z[40] = z[16] + -z[28];
z[40] = z[25] * z[40];
z[41] = -z[13] + z[36];
z[41] = z[15] * z[41];
z[40] = z[39] + z[40] + -z[41];
z[42] = -(z[17] * z[25]);
z[40] = T(2) * z[40] + z[42];
z[40] = z[17] * z[40];
z[42] = T(4) * z[0] + T(-2) * z[11];
z[33] = z[33] * z[42];
z[42] = z[26] * z[28];
z[34] = T(-2) * z[34] + -z[42];
z[34] = z[28] * z[34];
z[39] = T(-2) * z[39] + z[41];
z[39] = z[15] * z[39];
z[43] = z[25] * z[28];
z[41] = z[41] + z[43];
z[43] = -(z[16] * z[25]);
z[41] = T(2) * z[41] + z[43];
z[41] = z[16] * z[41];
z[43] = -(z[13] * prod_pow(z[18], 2));
z[32] = T(2) * z[32] + z[33] + z[34] + z[35] + z[39] + z[40] + z[41] + z[43];
z[33] = -z[13] + z[37];
z[33] = z[24] * z[33];
z[34] = z[13] * z[18];
z[35] = z[31] * z[36];
z[37] = -(z[17] * z[37]);
z[33] = z[33] + z[34] + z[35] + z[37] + -z[38] + z[42];
z[33] = int_to_imaginary<T>(1) * z[33];
z[34] = T(2) * z[13] + z[26] + -z[36];
z[34] = z[19] * z[34];
z[33] = T(6) * z[33] + z[34];
z[33] = z[19] * z[33];
return T(3) * z[32] + z[33];
}



template IntegrandConstructorType<double> f_3_93_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_93_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_93_construct (const Kin<qd_real>&);
#endif

}