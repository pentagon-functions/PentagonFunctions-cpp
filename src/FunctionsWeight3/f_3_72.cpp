#include "f_3_72.h"

namespace PentagonFunctions {

template <typename T> T f_3_72_abbreviated (const std::array<T,22>&);



template <typename T> IntegrandConstructorType<T> f_3_72_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl31 = DLog_W_31<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,22> abbr = 
            {dl26(t) / kin_path.SqrtDelta, rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_4(kin_path), f_2_1_7(kin_path), f_2_1_8(kin_path), f_2_1_14(kin_path), dl30(t) / kin_path.SqrtDelta, rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_15(kin_path), dl29(t) / kin_path.SqrtDelta, f_2_1_11(kin_path), dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl3(t), f_2_2_7(kin_path), dl4(t), dl18(t), dl31(t)}
;

        auto result = f_3_72_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_72_abbreviated(const std::array<T,22>& abb)
{
T z[35];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[3];
z[3] = abb[5];
z[4] = abb[2];
z[5] = abb[4];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[13];
z[11] = abb[15];
z[12] = abb[10];
z[13] = abb[11];
z[14] = abb[16];
z[15] = abb[12];
z[16] = abb[14];
z[17] = abb[17];
z[18] = abb[18];
z[19] = abb[19];
z[20] = abb[20];
z[21] = abb[21];
z[22] = z[11] + z[14];
z[23] = z[15] + z[16];
z[23] = z[22] * z[23];
z[24] = z[10] + z[12];
z[25] = z[0] + z[11];
z[26] = z[24] + -z[25];
z[26] = z[8] * z[26];
z[27] = z[7] * z[25];
z[28] = -z[15] + z[16];
z[28] = z[10] * z[28];
z[29] = z[7] + -z[15];
z[29] = z[12] * z[29];
z[30] = z[19] + z[20] + T(-2) * z[21];
z[30] = z[18] * z[30];
z[31] = z[0] + z[12] + -z[14];
z[32] = z[10] + -z[31];
z[32] = z[9] * z[32];
z[33] = -z[0] + z[14];
z[33] = z[6] * z[33];
z[34] = z[17] * z[18];
z[23] = z[23] + z[26] + z[27] + z[28] + z[29] + z[30] + z[32] + z[33] + z[34];
z[26] = -z[4] + z[13];
z[26] = z[22] * z[26];
z[27] = -(z[1] * z[12]);
z[28] = -(z[2] * z[25]);
z[26] = z[26] + z[27] + z[28];
z[27] = z[5] * z[22];
z[26] = T(2) * z[26] + z[27];
z[26] = z[5] * z[26];
z[27] = z[4] * z[22];
z[28] = z[3] * z[10];
z[29] = z[27] + T(-2) * z[28];
z[29] = z[4] * z[29];
z[27] = -z[27] + z[28];
z[28] = z[13] * z[22];
z[27] = T(2) * z[27] + z[28];
z[27] = z[13] * z[27];
z[28] = -z[10] + z[25];
z[30] = z[3] * z[28];
z[22] = z[12] + -z[22];
z[22] = z[13] * z[22];
z[22] = z[22] + z[30];
z[30] = -(z[1] * z[28]);
z[22] = T(2) * z[22] + z[30];
z[22] = z[1] * z[22];
z[30] = z[1] + -z[3];
z[28] = z[28] * z[30];
z[25] = z[4] * z[25];
z[25] = z[25] + z[28];
z[24] = z[2] * z[24];
z[24] = z[24] + T(2) * z[25];
z[24] = z[2] * z[24];
z[25] = -(prod_pow(z[3], 2) * z[31]);
z[22] = z[22] + T(2) * z[23] + z[24] + z[25] + z[26] + z[27] + z[29];
return T(3) * z[22];
}



template IntegrandConstructorType<double> f_3_72_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_72_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_72_construct (const Kin<qd_real>&);
#endif

}