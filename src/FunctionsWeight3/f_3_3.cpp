#include "f_3_3.h"

namespace PentagonFunctions {

template <typename T> T f_3_3_abbreviated (const std::array<T,10>&);



template <typename T> IntegrandConstructorType<T> f_3_3_construct (const Kin<T>& kin) {
    return [&kin, 
            dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,10> abbr = 
            {dl5(t), f_2_1_3(kin_path), f_2_1_4(kin_path), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3]), dl2(t), rlog(v_path[3]), rlog(-v_path[4]), dl4(t), dl19(t)}
;

        auto result = f_3_3_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_3_abbreviated(const std::array<T,10>& abb)
{
using TR = typename T::value_type;
T z[16];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[8];
z[8] = abb[9];
z[9] = abb[6];
z[10] = abb[7];
z[11] = z[6] + -z[8];
z[12] = z[10] * z[11];
z[13] = -(z[3] * z[7]);
z[14] = z[7] + -z[11];
z[14] = z[5] * z[14];
z[15] = z[3] + -z[5];
z[15] = z[0] * z[15];
z[12] = z[12] + z[13] + z[14] + z[15];
z[12] = int_to_imaginary<T>(1) * z[12];
z[13] = z[0] + -z[7];
z[14] = z[4] * z[13];
z[12] = z[12] + -z[14] / T(6);
z[12] = z[4] * z[12];
z[14] = -z[3] + z[9];
z[15] = z[3] + -z[10];
z[14] = z[11] * z[14] * z[15];
z[15] = -z[11] + z[13];
z[15] = z[2] * z[15];
z[11] = -z[11] + -z[13];
z[11] = z[1] * z[11];
return z[11] + z[12] + z[14] + z[15];
}



template IntegrandConstructorType<double> f_3_3_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_3_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_3_construct (const Kin<qd_real>&);
#endif

}