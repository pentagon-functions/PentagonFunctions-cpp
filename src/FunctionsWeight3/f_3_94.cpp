#include "f_3_94.h"

namespace PentagonFunctions {

template <typename T> T f_3_94_abbreviated (const std::array<T,41>&);

template <typename T> class SpDLog_f_3_94_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_94_W_10 (const Kin<T>& kin) {
        c[0] = (T(-4) * kin.v[0] * kin.v[2]) / T(3) + kin.v[2] * (kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + (-bc<T>[1] + T(1)) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[1] * (T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + bc<T>[1] * T(2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + bc<T>[1] * T(2) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[16] * (abb[21] * T(-4) + abb[18] * abb[19] * T(-2) + abb[13] * (abb[17] * T(-2) + abb[19] * T(2)) + abb[19] * (abb[20] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[19] * T(2)) + abb[17] * (abb[17] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[18] * T(2) + abb[19] * T(2) + abb[20] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_94_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl11 = DLog_W_11<T>(kin),dl10 = DLog_W_10<T>(kin),dl6 = DLog_W_6<T>(kin),dl4 = DLog_W_4<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),spdl10 = SpDLog_f_3_94_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);

        std::array<std::complex<T>,41> abbr = 
            {dl26(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), f_2_2_2(kin_path), f_2_2_3(kin_path), f_2_2_4(kin_path), f_2_2_5(kin_path), f_2_2_6(kin_path), f_2_2_7(kin_path), f_2_2_8(kin_path), f_2_2_9(kin_path), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl11(t), rlog(v_path[0]), rlog(v_path[3]), f_2_1_2(kin_path), dl10(t), rlog(-v_path[4]), rlog(v_path[2]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_7(kin_path), dl6(t), f_2_1_9(kin_path), dl4(t), rlog(-v_path[1]), f_2_1_4(kin_path), dl3(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl2(t), dl16(t), f_2_1_3(kin_path), f_2_1_10(kin_path), rlog(-v_path[1] + v_path[3]), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl1(t), dl17(t), dl19(t), dl18(t), dl5(t)}
;

        auto result = f_3_94_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_3_94_abbreviated(const std::array<T,41>& abb)
{
using TR = typename T::value_type;
T z[80];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[11];
z[12] = abb[12];
z[13] = abb[13];
z[14] = bc<TR>[0];
z[15] = abb[14];
z[16] = abb[15];
z[17] = abb[22];
z[18] = abb[24];
z[19] = abb[30];
z[20] = abb[31];
z[21] = abb[38];
z[22] = abb[39];
z[23] = abb[40];
z[24] = abb[27];
z[25] = abb[36];
z[26] = abb[37];
z[27] = abb[17];
z[28] = abb[18];
z[29] = abb[19];
z[30] = abb[20];
z[31] = abb[25];
z[32] = abb[21];
z[33] = abb[23];
z[34] = abb[26];
z[35] = abb[28];
z[36] = abb[29];
z[37] = abb[32];
z[38] = abb[33];
z[39] = abb[34];
z[40] = abb[35];
z[41] = T(2) * z[28];
z[42] = T(2) * z[29];
z[43] = z[41] + z[42];
z[44] = T(2) * z[27];
z[45] = z[15] + z[30];
z[46] = -z[13] + -z[43] + -z[44] + -z[45];
z[46] = z[23] * z[46];
z[47] = -z[27] + z[43];
z[48] = T(4) * z[13];
z[49] = T(4) * z[15];
z[50] = T(3) * z[39] + z[47] + z[48] + -z[49];
z[50] = z[25] * z[50];
z[51] = z[13] + -z[15];
z[52] = z[12] * z[51];
z[53] = z[13] + z[27] + -z[39];
z[53] = z[20] * z[53];
z[52] = z[52] + -z[53];
z[53] = T(3) * z[29];
z[54] = z[13] + -z[30];
z[55] = z[53] + T(2) * z[54];
z[56] = z[27] + z[28] + z[55];
z[56] = z[24] * z[56];
z[57] = T(2) * z[30];
z[58] = -z[15] + z[57];
z[59] = -z[13] + z[58];
z[43] = -z[27] + -z[43] + -z[59];
z[43] = z[22] * z[43];
z[60] = T(2) * z[13];
z[61] = z[30] + z[60];
z[62] = T(3) * z[28] + z[53] + z[61];
z[62] = z[26] * z[62];
z[63] = -z[27] + z[51];
z[64] = -z[28] + -z[53] + -z[63];
z[64] = z[19] * z[64];
z[65] = T(3) * z[13];
z[66] = -(z[21] * z[65]);
z[67] = -z[27] + -z[54];
z[67] = z[18] * z[67];
z[68] = -z[21] + z[26];
z[69] = -z[22] + z[68];
z[70] = z[20] + -z[23] + z[25] + z[69];
z[70] = T(3) * z[70];
z[71] = -(z[40] * z[70]);
z[72] = z[23] + -z[24];
z[73] = z[19] + -z[26] + z[72];
z[73] = T(3) * z[73];
z[74] = z[36] * z[73];
z[75] = -(z[17] * z[54]);
z[76] = z[18] + z[72];
z[77] = -z[19] + -z[25] + z[76];
z[77] = z[31] * z[77];
z[43] = z[43] + z[46] + z[50] + T(-3) * z[52] + z[56] + z[62] + z[64] + z[66] + z[67] + z[71] + z[74] + z[75] + z[77];
z[43] = int_to_imaginary<T>(1) * z[43];
z[46] = z[20] + -z[21] + (T(-13) * z[22]) / T(6) + -z[24] / T(4) + (T(13) * z[26]) / T(4);
z[46] = z[18] / T(3) + (T(-11) * z[19]) / T(24) + (T(-2) * z[23]) / T(3) + (T(3) * z[25]) / T(4) + z[46] / T(2);
z[46] = z[14] * z[46];
z[43] = z[43] + z[46];
z[43] = z[14] * z[43];
z[46] = T(4) * z[29];
z[50] = -z[13] + z[28];
z[52] = -z[30] + z[44] + -z[46] + -z[50];
z[52] = z[27] * z[52];
z[51] = -z[28] + z[51];
z[56] = z[51] + z[53];
z[56] = z[28] * z[56];
z[62] = prod_pow(z[15], 2);
z[64] = z[62] / T(2);
z[66] = T(2) * z[34];
z[67] = z[15] * z[30];
z[71] = z[13] / T(2);
z[74] = -z[30] + z[71];
z[74] = z[13] * z[74];
z[75] = -(z[29] * z[54]);
z[52] = z[52] + z[56] + -z[64] + -z[66] + z[67] + z[74] + z[75];
z[52] = z[19] * z[52];
z[50] = z[42] + z[50] + z[58];
z[50] = z[28] * z[50];
z[47] = -z[47] + z[61];
z[47] = z[27] * z[47];
z[56] = -z[29] + z[61];
z[56] = z[29] * z[56];
z[74] = -z[15] + z[30];
z[75] = z[30] * z[74];
z[77] = T(2) * z[15] + -z[30] + -z[71];
z[77] = z[13] * z[77];
z[47] = z[47] + z[50] + -z[56] + -z[64] + z[75] + z[77];
z[47] = z[22] * z[47];
z[44] = z[42] + -z[44] + -z[51];
z[44] = z[27] * z[44];
z[50] = -z[32] + z[62] + -z[67];
z[48] = z[15] + -z[28] + T(3) * z[30] + -z[42] + -z[48];
z[48] = z[28] * z[48];
z[64] = T(3) * z[37];
z[77] = z[13] * z[74];
z[65] = -z[29] + z[65];
z[65] = z[29] * z[65];
z[44] = z[44] + z[48] + T(2) * z[50] + z[64] + z[65] + -z[77];
z[44] = z[25] * z[44];
z[48] = -z[29] + z[63];
z[48] = z[48] * z[68];
z[50] = -z[27] + z[51];
z[50] = z[50] * z[76];
z[63] = z[18] + -z[24];
z[65] = z[23] + z[63];
z[65] = (T(5) * z[19]) / T(2) + (T(-3) * z[20]) / T(2) + T(2) * z[25] + z[65] / T(2) + z[68];
z[65] = z[31] * z[65];
z[76] = z[15] + z[27] + z[60];
z[78] = -z[29] + z[76];
z[78] = z[20] * z[78];
z[79] = z[28] + z[29];
z[79] = z[19] * z[79];
z[76] = z[28] + -z[76];
z[76] = z[25] * z[76];
z[48] = z[48] + z[50] + z[65] + z[76] + z[78] + z[79];
z[48] = z[31] * z[48];
z[50] = z[28] + -z[55];
z[50] = z[28] * z[50];
z[65] = z[27] / T(2);
z[51] = -z[51] + z[65];
z[76] = -z[51] + z[53];
z[76] = z[27] * z[76];
z[78] = prod_pow(z[29], 2);
z[50] = z[50] + z[75] + z[76] + -z[77] + (T(-3) * z[78]) / T(2);
z[50] = z[24] * z[50];
z[53] = -z[53] + z[61];
z[53] = z[29] * z[53];
z[55] = -(z[28] * z[55]);
z[46] = z[46] + -z[74];
z[46] = z[27] * z[46];
z[75] = z[30] / T(2);
z[76] = z[15] + z[75];
z[76] = z[30] * z[76];
z[46] = z[46] + z[53] + z[55] + -z[76] + -z[77];
z[46] = z[26] * z[46];
z[41] = (T(-3) * z[27]) / T(2) + z[41] + -z[42] + z[45] + -z[60];
z[41] = z[27] * z[41];
z[53] = prod_pow(z[30], 2);
z[53] = z[53] + z[62];
z[55] = z[58] + -z[71];
z[55] = z[13] * z[55];
z[42] = -z[28] / T(2) + z[42] + -z[59];
z[42] = z[28] * z[42];
z[41] = z[41] + z[42] + z[53] / T(2) + z[55] + -z[56];
z[41] = z[23] * z[41];
z[42] = z[27] * z[51];
z[51] = z[28] * z[54];
z[53] = z[15] + z[57];
z[54] = (T(-3) * z[13]) / T(2) + z[53];
z[54] = z[13] * z[54];
z[42] = z[42] + z[51] + z[54] + z[66] + -z[76];
z[42] = z[18] * z[42];
z[45] = z[45] + z[71];
z[45] = z[13] * z[45];
z[45] = z[45] + -z[67];
z[54] = (T(3) * z[29]) / T(2);
z[55] = z[13] + -z[54] + z[57];
z[55] = z[29] * z[55];
z[53] = z[29] + -z[53] + z[65];
z[53] = z[27] * z[53];
z[53] = -z[45] + z[53] + z[55] + -z[64];
z[53] = z[20] * z[53];
z[54] = z[54] + -z[61];
z[54] = z[29] * z[54];
z[55] = -z[29] + z[74];
z[55] = z[27] * z[55];
z[45] = z[45] + z[54] + z[55];
z[45] = z[21] * z[45];
z[54] = z[49] + -z[75];
z[54] = z[30] * z[54];
z[49] = -z[49] + z[71];
z[49] = z[13] * z[49];
z[49] = z[49] + T(4) * z[51] + z[54];
z[49] = z[17] * z[49];
z[51] = z[17] + -z[25];
z[54] = T(-2) * z[63];
z[51] = (T(-3) * z[51]) / T(2) + z[54] + -z[69];
z[51] = z[33] * z[51];
z[55] = z[0] + z[10] + z[11];
z[56] = -z[1] + -z[2] + z[3] + -z[4] + -z[5] + -z[6] + -z[7] + z[8] + z[9];
z[55] = z[55] * z[56];
z[56] = prod_pow(z[13], 2);
z[56] = z[56] + -z[62];
z[57] = (T(3) * z[12]) / T(2);
z[56] = z[56] * z[57];
z[58] = z[68] + z[72];
z[58] = z[58] * z[66];
z[54] = z[19] + -z[20] + T(-2) * z[22] + (T(13) * z[25]) / T(2) + z[54] + -z[57];
z[54] = z[16] * z[54];
z[57] = z[24] + z[26];
z[57] = T(5) * z[19] + z[20] + z[22] + z[23] + T(-3) * z[57];
z[57] = z[32] * z[57];
z[59] = -(z[35] * z[73]);
z[60] = -(z[38] * z[70]);
return z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60];
}



template IntegrandConstructorType<double> f_3_94_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_94_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_94_construct (const Kin<qd_real>&);
#endif

}