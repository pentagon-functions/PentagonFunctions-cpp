#include "f_3_21.h"

namespace PentagonFunctions {

template <typename T> T f_3_21_abbreviated (const std::array<T,11>&);



template <typename T> IntegrandConstructorType<T> f_3_21_construct (const Kin<T>& kin) {
    return [&kin, 
            dl11 = DLog_W_11<T>(kin),dl18 = DLog_W_18<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl1 = DLog_W_1<T>(kin),dl13 = DLog_W_13<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,11> abbr = 
            {dl11(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), f_2_1_2(kin_path), dl18(t), f_2_1_1(kin_path), dl3(t), dl4(t), dl1(t), dl13(t)}
;

        auto result = f_3_21_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_21_abbreviated(const std::array<T,11>& abb)
{
using TR = typename T::value_type;
T z[19];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[9];
z[7] = abb[10];
z[8] = bc<TR>[0];
z[9] = abb[6];
z[10] = abb[7];
z[11] = abb[8];
z[12] = prod_pow(z[2], 2);
z[13] = prod_pow(z[1], 2);
z[12] = z[9] + z[12] + -z[13];
z[13] = z[6] + -z[7];
z[12] = z[12] * z[13];
z[14] = -z[1] + z[3];
z[15] = z[1] + -z[2];
z[14] = z[14] * z[15];
z[14] = -z[4] + z[14];
z[16] = -(z[0] * z[14]);
z[14] = -z[9] + z[14];
z[14] = z[5] * z[14];
z[17] = z[10] + z[11];
z[18] = z[9] * z[17];
z[12] = z[12] / T(2) + z[14] + z[16] + z[18];
z[13] = int_to_imaginary<T>(1) * z[13] * z[15];
z[14] = z[5] + -z[17];
z[14] = z[8] * z[14];
z[13] = T(3) * z[13] + z[14] / T(2);
z[13] = z[8] * z[13];
return T(3) * z[12] + z[13];
}



template IntegrandConstructorType<double> f_3_21_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_21_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_21_construct (const Kin<qd_real>&);
#endif

}