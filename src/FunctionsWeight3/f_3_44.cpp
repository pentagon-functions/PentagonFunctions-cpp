#include "f_3_44.h"

namespace PentagonFunctions {

template <typename T> T f_3_44_abbreviated (const std::array<T,18>&);

template <typename T> class SpDLog_f_3_44_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_44_W_23 (const Kin<T>& kin) {
        c[0] = prod_pow(kin.v[3], 2) + kin.v[0] * ((T(10) * kin.v[2]) / T(3) + (T(-2) * kin.v[3]) / T(3) + (T(-4) * kin.v[4]) / T(3) + (T(-1) / T(3) + -bc<T>[1]) * kin.v[0] + T(2) * kin.v[1] + bc<T>[1] * (T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3])) + -prod_pow(kin.v[4], 2) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (T(-3) * kin.v[2] + T(-2) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[1] * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + bc<T>[1] * T(2) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[3] * abb[4] * T(2) + abb[1] * (abb[4] * T(-2) + abb[2] * T(2)) + abb[4] * (abb[4] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[5] * T(2)) + abb[6] * T(4) + abb[2] * (abb[3] * T(-2) + abb[4] * T(-2) + abb[5] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[2] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_44_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),spdl23 = SpDLog_f_3_44_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,18> abbr = 
            {dl23(t), rlog(v_path[0]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(v_path[3]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_8(kin_path), dl1(t), dl20(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl16(t), dl5(t), dl17(t)}
;

        auto result = f_3_44_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_3_44_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[33];
z[0] = abb[1];
z[1] = abb[15];
z[2] = abb[17];
z[3] = abb[2];
z[4] = abb[7];
z[5] = abb[11];
z[6] = abb[12];
z[7] = abb[3];
z[8] = abb[4];
z[9] = abb[5];
z[10] = bc<TR>[0];
z[11] = abb[8];
z[12] = abb[16];
z[13] = abb[6];
z[14] = abb[9];
z[15] = abb[10];
z[16] = abb[13];
z[17] = abb[14];
z[18] = z[1] + z[6];
z[19] = T(2) * z[2] + -z[18];
z[20] = T(3) * z[11];
z[21] = T(2) * z[5] + z[19] + -z[20];
z[21] = z[8] * z[21];
z[22] = z[2] + z[5];
z[23] = T(2) * z[1] + -z[22];
z[24] = T(3) * z[12];
z[25] = T(2) * z[6] + z[23] + -z[24];
z[26] = -(z[9] * z[25]);
z[27] = z[4] + z[25];
z[27] = int_to_imaginary<T>(1) * z[10] * z[27];
z[24] = z[20] + z[24];
z[28] = T(5) * z[1] + -z[6] + -z[22] + -z[24];
z[29] = T(2) * z[4];
z[28] = z[28] / T(2) + z[29];
z[28] = z[3] * z[28];
z[23] = -z[6] + z[23];
z[30] = -(z[7] * z[23]);
z[31] = z[7] + z[9];
z[32] = -z[8] + -z[31];
z[32] = z[4] * z[32];
z[23] = z[4] + z[23];
z[23] = z[0] * z[23];
z[23] = -z[21] + z[23] + z[26] + z[27] + z[28] + z[30] + z[32];
z[23] = z[3] * z[23];
z[22] = z[18] + z[22] + -z[24] + z[29];
z[22] = z[13] * z[22];
z[24] = -z[0] + T(-2) * z[31];
z[26] = -z[1] + z[2];
z[24] = z[24] * z[26];
z[27] = -z[5] + z[11];
z[28] = z[26] + -z[27];
z[29] = z[15] * z[28];
z[30] = -z[6] + z[12] + z[26];
z[32] = z[17] * z[30];
z[29] = z[29] + z[32];
z[32] = z[4] * z[8];
z[24] = -z[21] + z[24] + T(3) * z[29] + -z[32];
z[24] = int_to_imaginary<T>(1) * z[24];
z[27] = (T(-2) * z[26]) / T(3) + -z[27] / T(2);
z[27] = z[10] * z[27];
z[24] = z[24] + z[27];
z[24] = z[10] * z[24];
z[20] = T(-3) * z[5] + z[20] + -z[26];
z[20] = z[7] * z[20];
z[20] = z[20] / T(2) + z[21];
z[20] = z[7] * z[20];
z[21] = -z[7] + z[9];
z[21] = z[21] * z[26];
z[25] = z[8] * z[25];
z[21] = z[21] + z[25];
z[21] = z[9] * z[21];
z[25] = -z[0] / T(2) + z[31];
z[25] = z[25] * z[26];
z[19] = z[5] + -z[19];
z[19] = z[8] * z[19];
z[19] = z[19] + z[25] + -z[32];
z[19] = z[0] * z[19];
z[25] = -(z[14] * z[28]);
z[26] = z[16] * z[30];
z[25] = z[25] + z[26];
z[26] = prod_pow(z[8], 2);
z[18] = -z[2] + -z[18];
z[18] = z[5] + (T(3) * z[12]) / T(2) + z[18] / T(2);
z[18] = z[18] * z[26];
z[27] = z[8] * z[31];
z[26] = -z[26] + z[27];
z[26] = z[4] * z[26];
return z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + T(3) * z[25] + z[26];
}



template IntegrandConstructorType<double> f_3_44_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_44_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_44_construct (const Kin<qd_real>&);
#endif

}