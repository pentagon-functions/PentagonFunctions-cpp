#include "f_3_50.h"

namespace PentagonFunctions {

template <typename T> T f_3_50_abbreviated (const std::array<T,32>&);

template <typename T> class SpDLog_f_3_50_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_50_W_10 (const Kin<T>& kin) {
        c[0] = bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(4) + T(7) + T(7) * kin.v[4]) + kin.v[1] * (-kin.v[2] / T(2) + T(7) + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(7) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[2] + T(4) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[1] + T(3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[18] * (abb[16] * T(-3) + abb[8] * ((abb[8] * T(-7)) / T(2) + abb[9] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[8] * abb[13] * T(4) + abb[7] * (abb[7] / T(2) + abb[13] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[8] * T(3) + abb[9] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_50_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl13 = DLog_W_13<T>(kin),dl11 = DLog_W_11<T>(kin),dl16 = DLog_W_16<T>(kin),dl10 = DLog_W_10<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),spdl10 = SpDLog_f_3_50_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);

        std::array<std::complex<T>,32> abbr = 
            {dl26(t) / kin_path.SqrtDelta, f_2_2_4(kin_path), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), dl11(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_2(kin_path), dl16(t), f_2_1_7(kin_path), f_2_1_8(kin_path), dl10(t), dl18(t), f_2_1_9(kin_path), dl4(t), dl2(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl3(t), dl20(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl5(t), dl1(t), dl17(t)}
;

        auto result = f_3_50_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_3_50_abbreviated(const std::array<T,32>& abb)
{
using TR = typename T::value_type;
T z[71];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = bc<TR>[0];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[15];
z[14] = abb[19];
z[15] = abb[21];
z[16] = abb[22];
z[17] = abb[25];
z[18] = abb[26];
z[19] = abb[29];
z[20] = abb[30];
z[21] = abb[31];
z[22] = abb[12];
z[23] = abb[13];
z[24] = abb[14];
z[25] = abb[16];
z[26] = abb[17];
z[27] = abb[20];
z[28] = abb[23];
z[29] = abb[24];
z[30] = abb[27];
z[31] = abb[28];
z[32] = z[19] + T(2) * z[20];
z[33] = T(2) * z[14];
z[34] = z[13] + z[33];
z[35] = T(5) * z[17];
z[36] = T(2) * z[21];
z[37] = T(-9) * z[15] + T(-2) * z[32] + z[34] + -z[35] + -z[36];
z[37] = z[5] * z[37];
z[38] = z[19] + z[20];
z[39] = T(2) * z[38];
z[40] = -z[15] + z[39];
z[41] = T(3) * z[16] + z[17] + -z[33] + -z[40];
z[41] = z[8] * z[41];
z[42] = T(3) * z[17];
z[43] = z[36] + z[42];
z[44] = T(3) * z[14];
z[45] = z[43] + z[44];
z[39] = z[15] + z[39] + -z[45];
z[39] = z[9] * z[39];
z[46] = T(8) * z[15] + -z[44];
z[47] = T(9) * z[17] + z[21] + -z[38] + z[46];
z[47] = z[6] * z[47];
z[48] = z[14] + -z[20];
z[49] = -z[13] + z[48];
z[50] = z[21] + z[49];
z[51] = T(7) * z[15];
z[52] = T(8) * z[17] + z[50] + z[51];
z[52] = z[22] * z[52];
z[53] = T(3) * z[18];
z[54] = z[5] + z[8];
z[55] = -z[31] + z[54];
z[55] = z[53] * z[55];
z[56] = T(-8) * z[8] + T(3) * z[31];
z[56] = z[21] * z[56];
z[48] = z[17] + z[48];
z[57] = z[19] + z[21] + z[48];
z[57] = T(2) * z[57];
z[58] = z[7] * z[57];
z[59] = -z[5] + z[22];
z[60] = z[12] * z[59];
z[61] = -z[5] + z[6];
z[62] = z[4] * z[61];
z[63] = z[15] + -z[20];
z[64] = -(z[23] * z[63]);
z[65] = -z[16] + z[38];
z[66] = -z[17] + z[21] + z[65];
z[67] = -z[18] + z[66];
z[67] = z[29] * z[67];
z[37] = z[37] + z[39] + z[41] + z[47] + z[52] + z[55] + z[56] + z[58] + T(-8) * z[60] + T(-4) * z[62] + z[64] + T(3) * z[67];
z[37] = int_to_imaginary<T>(1) * z[37];
z[39] = z[16] / T(2);
z[41] = z[13] / T(2);
z[47] = (T(11) * z[19]) / T(4) + T(-4) * z[20];
z[47] = (T(-55) * z[14]) / T(24) + (T(41) * z[15]) / T(8) + (T(109) * z[17]) / T(24) + -z[18] / T(2) + (T(7) * z[21]) / T(12) + -z[39] + -z[41] + z[47] / T(3);
z[47] = z[10] * z[47];
z[37] = z[37] + z[47];
z[37] = z[10] * z[37];
z[41] = -z[36] + z[41];
z[47] = z[14] / T(2);
z[52] = T(4) * z[17];
z[55] = z[20] + -z[51];
z[55] = z[41] + -z[47] + -z[52] + z[55] / T(2);
z[55] = z[22] * z[55];
z[56] = -z[14] + z[21];
z[35] = z[16] + -z[35] + -z[51] + T(-4) * z[56];
z[35] = z[6] * z[35];
z[49] = T(-2) * z[15] + -z[16] + -z[42] + z[49];
z[51] = z[8] * z[49];
z[50] = -z[15] + -z[50] + z[52];
z[50] = z[9] * z[50];
z[56] = z[16] + z[20];
z[58] = z[13] + z[56];
z[46] = z[17] + T(5) * z[21] + z[46] + -z[58];
z[46] = z[5] * z[46];
z[60] = z[8] * z[36];
z[35] = z[35] + z[46] + z[50] + z[51] + z[55] + z[60];
z[35] = z[22] * z[35];
z[33] = -z[15] + z[33];
z[42] = -z[33] + -z[42] + z[56];
z[42] = z[8] * z[42];
z[46] = (T(3) * z[13]) / T(2);
z[50] = z[20] / T(2);
z[51] = (T(3) * z[14]) / T(2);
z[55] = -z[15] + -z[46] + -z[50] + z[51];
z[55] = z[23] * z[55];
z[62] = T(4) * z[12];
z[49] = z[21] + -z[49] + -z[62];
z[49] = z[49] * z[59];
z[64] = T(3) * z[13];
z[67] = -z[14] + z[15] + -z[16] + T(3) * z[19] + z[43] + z[64];
z[67] = z[7] * z[67];
z[68] = z[8] * z[21];
z[63] = z[9] * z[63];
z[42] = z[42] + z[49] + z[55] + z[63] + z[67] + T(-5) * z[68];
z[42] = z[23] * z[42];
z[49] = T(2) * z[16];
z[55] = z[40] + -z[49] + -z[52];
z[55] = z[8] * z[55];
z[63] = T(3) * z[15];
z[67] = z[38] + z[63];
z[69] = T(7) * z[14] + -z[36];
z[70] = T(2) * z[17];
z[67] = -z[16] + T(2) * z[67] + -z[69] + z[70];
z[67] = z[5] * z[67];
z[51] = -z[38] + z[51];
z[43] = T(-4) * z[15] + (T(3) * z[16]) / T(2) + -z[43] + z[51];
z[43] = z[6] * z[43];
z[40] = -z[40] + z[45];
z[40] = z[9] * z[40];
z[40] = z[40] + z[43] + z[55] + z[67] + T(3) * z[68];
z[40] = z[6] * z[40];
z[43] = -z[19] + z[21];
z[39] = z[17] + -z[39] + z[43] + z[47] + -z[50];
z[39] = z[7] * z[39];
z[32] = -z[15] + -z[32] + z[49] + -z[70];
z[32] = z[8] * z[32];
z[45] = z[15] + z[17] + z[65];
z[45] = z[45] * z[61];
z[47] = -(z[9] * z[57]);
z[32] = z[32] + z[39] + z[45] + z[47] + T(-7) * z[68];
z[32] = z[7] * z[32];
z[39] = prod_pow(z[5], 2);
z[45] = z[6] / T(2) + -z[54];
z[45] = z[6] * z[45];
z[47] = z[22] / T(2) + z[61];
z[47] = z[22] * z[47];
z[49] = z[7] * z[8];
z[55] = -z[7] + z[23] / T(2) + -z[59];
z[55] = z[23] * z[55];
z[39] = z[11] + z[26] + z[28] + z[30] + z[39] / T(2) + z[45] + z[47] + z[49] + z[55];
z[39] = z[39] * z[53];
z[45] = -z[13] + z[14];
z[43] = -z[15] + z[43] + z[45];
z[43] = z[26] * z[43];
z[47] = prod_pow(z[8], 2);
z[49] = -z[30] + T(2) * z[47];
z[49] = z[21] * z[49];
z[53] = z[28] * z[66];
z[48] = -z[15] + z[48];
z[48] = z[27] * z[48];
z[43] = -z[43] + -z[48] + -z[49] + z[53];
z[48] = -z[19] + z[20];
z[45] = -z[45] + T(2) * z[48] + z[52] + z[63];
z[45] = z[8] * z[45];
z[48] = T(2) * z[19];
z[34] = T(-7) * z[17] + z[20] + -z[21] + -z[34] + z[48];
z[34] = z[9] * z[34];
z[41] = T(5) * z[14] + (T(-5) * z[15]) / T(2) + (T(9) * z[17]) / T(2) + -z[19] + z[41] + z[56];
z[41] = z[5] * z[41];
z[34] = z[34] + z[41] + z[45] + z[68];
z[34] = z[5] * z[34];
z[33] = -z[20] + z[33] + z[48] + z[70];
z[33] = z[8] * z[33];
z[41] = -z[15] / T(2) + (T(3) * z[17]) / T(2) + z[21] + z[51];
z[41] = z[9] * z[41];
z[33] = z[33] + z[41] + z[60];
z[33] = z[9] * z[33];
z[36] = z[14] + z[17] + -z[36] + z[58] + -z[63];
z[41] = T(2) * z[24];
z[36] = z[36] * z[41];
z[45] = T(2) * z[5] + z[8] + -z[9];
z[45] = z[5] * z[45];
z[48] = -z[9] + z[54];
z[49] = z[22] + z[48];
z[49] = z[22] * z[49];
z[41] = -z[41] + -z[45] + z[49];
z[41] = z[41] * z[62];
z[48] = z[6] + z[48];
z[48] = z[6] * z[48];
z[49] = -(z[7] * z[61]);
z[45] = T(-2) * z[11] + -z[45] + z[48] + z[49];
z[45] = z[4] * z[45];
z[46] = z[14] + (T(5) * z[17]) / T(2) + -z[19] + -z[46] + z[50];
z[46] = z[46] * z[47];
z[38] = T(-5) * z[15] + -z[17] + -z[38] + z[69];
z[38] = z[11] * z[38];
z[44] = -z[16] + z[17] + T(7) * z[21] + z[44] + -z[64];
z[44] = z[25] * z[44];
z[47] = z[0] + z[2];
z[47] = T(-3) * z[3] + T(2) * z[47];
z[47] = z[1] * z[47];
return z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + T(-3) * z[43] + z[44] + T(2) * z[45] + z[46] + z[47];
}



template IntegrandConstructorType<double> f_3_50_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_50_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_50_construct (const Kin<qd_real>&);
#endif

}