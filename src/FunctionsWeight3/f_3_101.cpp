#include "f_3_101.h"

namespace PentagonFunctions {

template <typename T> T f_3_101_abbreviated (const std::array<T,37>&);

template <typename T> class SpDLog_f_3_101_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_101_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(5) * kin.v[2] + T(8) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * (T(3) * kin.v[3] + T(-4) * kin.v[4]) + prod_pow(kin.v[4], 2) + kin.v[0] * (T(3) * kin.v[0] + T(-8) * kin.v[2] + T(-6) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + T(-4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[6] * T(-4) + abb[2] * (abb[3] * T(-2) + abb[5] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[2] * T(2) + abb[4] * T(2)) + abb[1] * (abb[1] * T(-4) + abb[4] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[2] * T(2) + abb[3] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_101_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl9 = DLog_W_9<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),dl2 = DLog_W_2<T>(kin),dl25 = DLog_W_25<T>(kin),dl1 = DLog_W_1<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),spdl23 = SpDLog_f_3_101_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,37> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_8(kin_path), dl9(t), rlog(-v_path[1]), rlog(v_path[3]), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), f_2_1_3(kin_path), f_2_1_11(kin_path), f_2_1_14(kin_path), rlog(-v_path[1] + v_path[3]), dl5(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl16(t), dl19(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl17(t), dl20(t), dl18(t), dl2(t), dl25(t), dl1(t), dl28(t) / kin_path.SqrtDelta, f_2_2_5(kin_path), f_1_3_1(kin_path), f_1_3_3(kin_path), f_1_3_5(kin_path), dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_3_101_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_3_101_abbreviated(const std::array<T,37>& abb)
{
using TR = typename T::value_type;
T z[81];
z[0] = abb[1];
z[1] = abb[7];
z[2] = abb[12];
z[3] = abb[17];
z[4] = abb[20];
z[5] = abb[21];
z[6] = abb[24];
z[7] = abb[25];
z[8] = abb[27];
z[9] = abb[2];
z[10] = abb[29];
z[11] = abb[3];
z[12] = abb[26];
z[13] = abb[4];
z[14] = abb[5];
z[15] = abb[8];
z[16] = abb[9];
z[17] = bc<TR>[0];
z[18] = abb[28];
z[19] = abb[6];
z[20] = abb[10];
z[21] = abb[11];
z[22] = abb[13];
z[23] = abb[14];
z[24] = abb[15];
z[25] = abb[16];
z[26] = abb[18];
z[27] = abb[19];
z[28] = abb[22];
z[29] = abb[23];
z[30] = abb[30];
z[31] = abb[31];
z[32] = abb[32];
z[33] = abb[33];
z[34] = abb[34];
z[35] = abb[35];
z[36] = abb[36];
z[37] = T(4) * z[5];
z[38] = T(2) * z[1];
z[39] = z[37] + z[38];
z[40] = T(3) * z[10];
z[41] = T(2) * z[3];
z[42] = z[40] + z[41];
z[43] = T(4) * z[18];
z[44] = -z[12] + z[43];
z[45] = T(2) * z[2];
z[46] = T(2) * z[6];
z[47] = T(3) * z[4];
z[48] = z[7] + T(-4) * z[8] + -z[39] + z[42] + z[44] + z[45] + -z[46] + -z[47];
z[48] = z[15] * z[48];
z[49] = T(2) * z[7];
z[50] = -z[47] + z[49];
z[51] = T(3) * z[12];
z[52] = T(5) * z[3] + -z[51];
z[53] = T(3) * z[8];
z[54] = T(5) * z[10];
z[39] = -z[39] + T(2) * z[50] + z[52] + -z[53] + z[54];
z[39] = z[0] * z[39];
z[55] = (T(9) * z[18]) / T(2);
z[56] = z[54] + z[55];
z[57] = z[2] + -z[7];
z[41] = (T(23) * z[5]) / T(2) + T(7) * z[6] + z[38] + -z[41] + -z[56] + z[57];
z[41] = z[13] * z[41];
z[58] = z[47] + z[53];
z[59] = T(4) * z[1] + -z[7];
z[60] = T(3) * z[5];
z[61] = T(5) * z[2] + -z[58] + z[59] + z[60];
z[61] = z[16] * z[61];
z[62] = z[6] + T(2) * z[57];
z[63] = T(5) * z[5];
z[42] = -z[42] + z[62] + z[63];
z[42] = z[9] * z[42];
z[64] = T(3) * z[6];
z[65] = T(2) * z[12];
z[50] = T(6) * z[5] + -z[8] + T(-7) * z[18] + z[50] + z[64] + z[65];
z[50] = z[11] * z[50];
z[66] = T(2) * z[5];
z[67] = z[47] + z[66];
z[53] = z[53] + z[67];
z[68] = z[2] + z[6] + -z[53] + z[54] + -z[59];
z[69] = -(z[21] * z[68]);
z[64] = -z[38] + z[64];
z[43] = z[8] + z[43];
z[60] = z[43] + -z[60];
z[65] = z[60] + -z[65];
z[70] = -z[57] + z[64] + -z[65];
z[70] = z[14] * z[70];
z[71] = T(2) * z[35] + -z[36];
z[72] = T(-4) * z[30] + T(-2) * z[71];
z[73] = z[32] + z[34];
z[72] = z[72] * z[73];
z[73] = -z[7] + z[8];
z[56] = (T(21) * z[5]) / T(2) + T(5) * z[6] + z[12] + -z[56] + T(-2) * z[73];
z[73] = -(z[29] * z[56]);
z[74] = z[5] + -z[12];
z[75] = z[3] + -z[10];
z[76] = -z[4] + z[6] + z[74] + z[75];
z[76] = T(3) * z[76];
z[77] = -(z[27] * z[76]);
z[78] = -z[2] + z[4] + z[8] + -z[10];
z[78] = T(3) * z[78];
z[79] = z[25] * z[78];
z[71] = T(2) * z[30] + z[71];
z[80] = z[33] * z[71];
z[39] = z[39] + z[41] + z[42] + z[48] + z[50] + z[61] + z[69] + z[70] + z[72] + z[73] + z[77] + z[79] + T(2) * z[80];
z[39] = int_to_imaginary<T>(1) * z[39];
z[41] = z[30] + z[35];
z[41] = (T(10) * z[1]) / T(3) + (T(41) * z[2]) / T(24) + -z[4] + (T(11) * z[5]) / T(12) + (T(-19) * z[6]) / T(24) + (T(-3) * z[7]) / T(8) + z[8] / T(3) + (T(3) * z[12]) / T(4) + T(-2) * z[18] + (T(-4) * z[36]) / T(3) + (T(8) * z[41]) / T(3);
z[41] = z[17] * z[41];
z[39] = z[39] + z[41];
z[39] = z[17] * z[39];
z[41] = z[3] + z[10];
z[42] = z[38] + -z[41] + z[62] + z[66];
z[42] = z[16] * z[42];
z[48] = z[3] + T(4) * z[10];
z[50] = z[48] + z[49];
z[61] = -z[2] + z[6];
z[53] = z[38] + -z[50] + z[53] + z[61];
z[53] = z[0] * z[53];
z[62] = -z[7] + z[75];
z[44] = z[8] + -z[44] + z[61] + -z[62] + z[66];
z[44] = z[11] * z[44];
z[66] = T(3) * z[2];
z[41] = -z[6] + T(-3) * z[7] + -z[41] + z[60] + z[66];
z[41] = z[13] * z[41];
z[60] = -z[3] + z[57];
z[69] = z[8] / T(2);
z[70] = z[12] / T(2);
z[72] = z[5] / T(2) + (T(-3) * z[10]) / T(2) + -z[60] + z[69] + -z[70];
z[72] = z[15] * z[72];
z[41] = z[41] + z[42] + z[44] + -z[53] + z[72];
z[41] = z[15] * z[41];
z[37] = z[12] + z[37] + -z[43] + z[49];
z[37] = z[13] * z[37];
z[43] = T(2) * z[0];
z[44] = -z[1] + T(2) * z[61];
z[44] = z[43] * z[44];
z[64] = z[2] + z[7] + -z[64];
z[64] = z[16] * z[64];
z[72] = z[6] + z[7];
z[73] = z[66] + -z[72];
z[73] = z[9] * z[73];
z[65] = -z[49] + z[65];
z[65] = z[11] * z[65];
z[77] = z[7] + -z[61];
z[77] = z[74] + z[77] / T(2);
z[77] = z[14] * z[77];
z[79] = -(z[15] * z[74]);
z[37] = z[37] + z[44] + z[64] + z[65] + z[73] + z[77] + z[79];
z[37] = z[14] * z[37];
z[44] = z[2] + z[46] + -z[50] + z[63];
z[44] = z[9] * z[44];
z[50] = (T(25) * z[5]) / T(4) + (T(7) * z[6]) / T(2) + (T(-23) * z[18]) / T(4) + -z[45] + z[49] + -z[69];
z[50] = z[13] * z[50];
z[63] = z[1] + z[61];
z[43] = z[43] * z[63];
z[42] = -z[42] + z[43] + z[44] + z[50];
z[42] = z[13] * z[42];
z[43] = z[7] + -z[58];
z[38] = (T(5) * z[3]) / T(2) + -z[5] + z[38] + z[43] / T(2) + -z[46] + z[66];
z[38] = prod_pow(z[0], 2) * z[38];
z[43] = z[5] + -z[10];
z[43] = z[6] / T(2) + (T(-5) * z[43]) / T(2) + -z[45] + -z[59];
z[43] = z[16] * z[43];
z[43] = z[43] + z[53];
z[43] = z[16] * z[43];
z[44] = -z[45] + -z[62];
z[44] = z[0] * z[44];
z[45] = z[61] + z[75];
z[45] = z[16] * z[45];
z[40] = -z[7] + z[40];
z[40] = (T(-3) * z[3]) / T(2) + z[40] / T(2) + -z[46];
z[40] = z[9] * z[40];
z[40] = z[40] + z[44] + z[45];
z[40] = z[9] * z[40];
z[44] = T(-4) * z[3] + -z[10] + -z[49] + z[51] + -z[61] + z[67];
z[45] = z[0] + -z[9];
z[44] = z[44] * z[45];
z[45] = (T(-19) * z[5]) / T(2) + T(-6) * z[6] + z[48] + z[55] + z[57];
z[45] = z[13] * z[45];
z[46] = -z[8] + (T(5) * z[18]) / T(2) + z[47] + -z[54];
z[46] = (T(9) * z[5]) / T(4) + z[6] + z[46] / T(2) + -z[70];
z[46] = z[11] * z[46];
z[44] = z[44] + z[45] + z[46];
z[44] = z[11] * z[44];
z[45] = z[20] * z[68];
z[46] = z[2] + z[52] + -z[67] + z[72];
z[46] = z[19] * z[46];
z[47] = -z[8] + -z[60] + z[74];
z[47] = z[23] * z[47];
z[48] = z[28] * z[56];
z[49] = -(z[26] * z[76]);
z[50] = z[31] * z[71];
z[51] = z[22] * z[78];
z[52] = z[24] * z[61];
return z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + T(2) * z[47] + z[48] + z[49] + z[50] + z[51] + T(-6) * z[52];
}



template IntegrandConstructorType<double> f_3_101_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_101_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_101_construct (const Kin<qd_real>&);
#endif

}