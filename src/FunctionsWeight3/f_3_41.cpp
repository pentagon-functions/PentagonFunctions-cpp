#include "f_3_41.h"

namespace PentagonFunctions {

template <typename T> T f_3_41_abbreviated (const std::array<T,9>&);



template <typename T> IntegrandConstructorType<T> f_3_41_construct (const Kin<T>& kin) {
    return [&kin, 
            dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl24 = DLog_W_24<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,9> abbr = 
            {dl5(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl18(t), dl17(t), dl24(t)}
;

        auto result = f_3_41_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_41_abbreviated(const std::array<T,9>& abb)
{
using TR = typename T::value_type;
T z[17];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = int_to_imaginary<T>(1) * z[6];
z[11] = int_to_imaginary<T>(1) * z[2];
z[10] = T(3) * z[10] + -z[11];
z[11] = (T(5) * z[4]) / T(12) + -z[10];
z[11] = z[4] * z[11];
z[12] = int_to_imaginary<T>(1) * z[4];
z[12] = z[2] + z[12];
z[13] = z[3] + -z[12];
z[14] = z[1] + T(2) * z[13];
z[14] = z[1] * z[14];
z[15] = -z[3] + T(2) * z[12];
z[15] = z[3] * z[15];
z[16] = prod_pow(z[2], 2);
z[16] = T(3) * z[5] + z[16];
z[11] = z[11] + -z[14] + z[15] + -z[16];
z[14] = z[0] + z[7];
z[11] = z[11] * z[14];
z[14] = z[4] / T(3) + -z[10] / T(2);
z[14] = z[4] * z[14];
z[12] = -z[3] / T(2) + z[12];
z[12] = z[3] * z[12];
z[15] = z[1] + -z[13];
z[15] = z[1] * z[15];
z[14] = z[12] + z[14] + z[15] + -z[16] / T(2);
z[14] = z[8] * z[14];
z[10] = -z[4] / T(2) + z[10];
z[10] = z[4] * z[10];
z[10] = z[10] + z[16];
z[13] = z[1] * z[13];
z[10] = z[10] / T(2) + -z[12] + z[13];
z[10] = z[9] * z[10];
return T(3) * z[10] + z[11] + z[14];
}



template IntegrandConstructorType<double> f_3_41_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_41_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_41_construct (const Kin<qd_real>&);
#endif

}