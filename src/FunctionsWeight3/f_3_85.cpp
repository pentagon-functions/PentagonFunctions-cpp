#include "f_3_85.h"

namespace PentagonFunctions {

template <typename T> T f_3_85_abbreviated (const std::array<T,33>&);

template <typename T> class SpDLog_f_3_85_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_85_W_23 (const Kin<T>& kin) {
        c[0] = T(-2) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(6) * kin.v[2] + T(4) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + kin.v[0] * ((T(-20) * kin.v[2]) / T(3) + (T(4) * kin.v[3]) / T(3) + (T(8) * kin.v[4]) / T(3) + (T(2) / T(3) + bc<T>[1] * T(2)) * kin.v[0] + T(-4) * kin.v[1] + bc<T>[1] * (T(-4) + T(4) * kin.v[1] + T(-4) * kin.v[3])) + bc<T>[1] * (kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(-8) + bc<T>[1] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(8) * kin.v[2] + T(8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + bc<T>[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(-8) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[17] * (abb[8] * T(-8) + abb[4] * abb[14] * T(-4) + abb[1] * (abb[3] * T(-4) + abb[4] * T(4)) + abb[4] * (abb[5] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[4] * T(4)) + abb[3] * (abb[3] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[4] * T(4) + abb[5] * T(4) + abb[14] * T(4)));
    }
};
template <typename T> class SpDLog_f_3_85_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_85_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(-8) * kin.v[0]) / T(3) + (bc<T>[1] + T(-1)) * kin.v[1] + T(2) * kin.v[2] + T(4) * kin.v[3] + bc<T>[1] * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[3])) + kin.v[2] * (T(3) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(-3) * kin.v[3] + T(-2) * kin.v[4]) + prod_pow(kin.v[4], 2) + ((T(-8) * kin.v[2]) / T(3) + (T(8) * kin.v[3]) / T(3) + (T(8) * kin.v[4]) / T(3)) * kin.v[0] + bc<T>[1] * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[1] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[18] * (abb[19] * T(-4) + abb[2] * abb[6] * T(-2) + abb[6] * abb[14] * T(-2) + abb[1] * (abb[3] * T(-2) + abb[6] * T(2)) + abb[6] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[6] * T(2)) + abb[3] * (abb[3] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[2] * T(2) + abb[6] * T(2) + abb[14] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_85_construct (const Kin<T>& kin) {
    return [&kin, 
            dl5 = DLog_W_5<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl13 = DLog_W_13<T>(kin),dl18 = DLog_W_18<T>(kin),dl23 = DLog_W_23<T>(kin),dl22 = DLog_W_22<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin),spdl23 = SpDLog_f_3_85_W_23<T>(kin),spdl22 = SpDLog_f_3_85_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,33> abbr = 
            {dl5(t), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_1(kin_path), f_2_1_8(kin_path), dl26(t) / kin_path.SqrtDelta, f_2_2_8(kin_path), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[3]), dl18(t), f_2_1_9(kin_path), dl23(t), dl22(t), f_2_1_14(kin_path), dl3(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl17(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl16(t), dl4(t), dl20(t), dl1(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl19(t)}
;

        auto result = f_3_85_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_3_85_abbreviated(const std::array<T,33>& abb)
{
using TR = typename T::value_type;
T z[72];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[4];
z[4] = abb[5];
z[5] = abb[6];
z[6] = abb[3];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[13];
z[10] = abb[15];
z[11] = abb[20];
z[12] = abb[23];
z[13] = abb[26];
z[14] = abb[27];
z[15] = abb[28];
z[16] = abb[29];
z[17] = abb[32];
z[18] = abb[14];
z[19] = bc<TR>[0];
z[20] = abb[9];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[12];
z[24] = abb[16];
z[25] = abb[19];
z[26] = abb[21];
z[27] = abb[22];
z[28] = abb[24];
z[29] = abb[25];
z[30] = abb[30];
z[31] = abb[31];
z[32] = z[11] + z[12];
z[33] = (T(3) * z[17]) / T(2);
z[34] = T(2) * z[16];
z[35] = -z[0] + z[34];
z[36] = T(3) * z[15];
z[37] = T(7) * z[14];
z[38] = z[10] + T(5) * z[13] + z[37];
z[38] = -z[32] + z[33] + -z[35] + z[36] + z[38] / T(2);
z[38] = z[6] * z[38];
z[39] = T(3) * z[12];
z[40] = T(2) * z[13];
z[41] = T(4) * z[15];
z[42] = z[10] + -z[14];
z[43] = z[39] + z[40] + z[41] + z[42];
z[44] = T(2) * z[11];
z[45] = z[16] + z[43] + -z[44];
z[46] = z[1] + -z[18];
z[45] = z[45] * z[46];
z[46] = T(2) * z[15];
z[47] = z[44] + z[46];
z[37] = -z[34] + z[37];
z[48] = T(3) * z[17];
z[49] = z[0] + z[48];
z[50] = T(6) * z[12] + -z[37] + z[40] + z[47] + -z[49];
z[50] = z[5] * z[50];
z[51] = -z[16] + z[48];
z[43] = z[11] + z[43] + -z[51];
z[52] = int_to_imaginary<T>(1) * z[19];
z[43] = z[43] * z[52];
z[38] = z[38] + z[43] + z[45] + z[50];
z[38] = z[6] * z[38];
z[43] = -z[0] + z[12];
z[45] = z[11] + z[13];
z[50] = z[41] + z[43] + T(-2) * z[45] + z[48];
z[53] = z[5] * z[50];
z[54] = T(3) * z[10];
z[55] = -z[49] + z[54];
z[56] = T(3) * z[14];
z[40] = -z[40] + z[56];
z[32] = -z[15] + -z[32] + z[40] + -z[55];
z[32] = z[1] * z[32];
z[57] = T(2) * z[14];
z[58] = z[34] + z[57];
z[54] = -z[9] + -z[54] + z[58];
z[59] = -z[13] + z[15];
z[60] = z[44] + z[54] + -z[59];
z[60] = z[52] * z[60];
z[61] = z[34] + z[36];
z[62] = T(4) * z[9];
z[55] = -z[14] + z[55] + z[61] + -z[62];
z[63] = T(3) * z[11];
z[64] = z[12] + z[63];
z[65] = z[55] + z[64];
z[65] = z[4] * z[65];
z[66] = (T(3) * z[10]) / T(2) + z[11] / T(2) + -z[33];
z[67] = z[9] / T(2) + -z[16];
z[68] = z[14] + z[15];
z[69] = -z[13] + z[68];
z[70] = z[66] + z[67] + -z[69];
z[70] = z[2] * z[70];
z[43] = -z[15] + -z[43] + -z[45];
z[43] = z[6] * z[43];
z[71] = T(2) * z[9] + -z[11] + -z[16] + -z[69];
z[71] = z[18] * z[71];
z[32] = z[32] + z[43] + z[53] + z[60] + z[65] + z[70] + T(2) * z[71];
z[32] = z[2] * z[32];
z[43] = z[13] + z[42];
z[33] = (T(-7) * z[12]) / T(2) + z[33] + -z[34] + -z[41] + z[43] / T(2);
z[33] = z[3] * z[33];
z[34] = z[12] + z[16];
z[41] = z[34] + -z[41] + -z[43];
z[53] = -z[18] + z[52];
z[41] = z[41] * z[53];
z[53] = T(2) * z[12];
z[43] = z[36] + z[43] + z[53];
z[60] = z[35] + -z[43];
z[60] = z[1] * z[60];
z[65] = -z[10] + T(8) * z[12] + T(5) * z[16] + -z[49] + -z[56] + z[59];
z[65] = z[6] * z[65];
z[43] = -z[0] + -z[43] + z[51];
z[51] = -(z[4] * z[43]);
z[70] = z[14] + -z[16];
z[49] = T(-7) * z[12] + T(-5) * z[15] + z[49] + T(4) * z[70];
z[49] = z[5] * z[49];
z[33] = z[33] + z[41] + z[49] + z[51] + z[60] + z[65];
z[33] = z[3] * z[33];
z[41] = z[13] / T(2);
z[49] = -z[0] + (T(3) * z[12]) / T(2) + z[41] + z[46] + -z[57] + z[66] + -z[67];
z[49] = z[1] * z[49];
z[47] = -z[12] + -z[13] + z[47] + z[58] + -z[62];
z[47] = z[18] * z[47];
z[47] = z[47] + z[49];
z[47] = z[1] * z[47];
z[49] = -(z[1] * z[50]);
z[40] = z[40] + -z[44] + z[61];
z[44] = z[12] + z[40];
z[50] = z[18] * z[44];
z[36] = -z[16] + -z[36] + z[45] + z[56];
z[36] = T(-4) * z[12] + z[36] / T(2);
z[36] = z[5] * z[36];
z[36] = z[36] + z[49] + z[50];
z[36] = z[5] * z[36];
z[40] = z[12] + -z[40];
z[40] = z[18] * z[40];
z[45] = z[45] + -z[48];
z[48] = z[12] + z[45] + -z[46] + -z[54];
z[48] = z[1] * z[48];
z[44] = -(z[5] * z[44]);
z[49] = -z[11] + z[17];
z[50] = z[49] + z[59];
z[51] = z[27] * z[50];
z[54] = -z[16] + z[17];
z[56] = z[31] * z[54];
z[51] = z[51] + z[56];
z[40] = z[40] + z[44] + z[48] + T(3) * z[51];
z[40] = int_to_imaginary<T>(1) * z[40];
z[44] = z[13] + (T(-31) * z[14]) / T(4);
z[44] = -z[10] + (T(5) * z[11]) / T(6) + (T(9) * z[12]) / T(4) + (T(-5) * z[15]) / T(12) + z[16] / T(6) + z[44] / T(3);
z[44] = z[19] * z[44];
z[40] = z[40] + z[44] / T(2);
z[40] = z[19] * z[40];
z[44] = z[12] + -z[16] + z[42] + -z[49];
z[44] = z[24] * z[44];
z[48] = z[26] * z[50];
z[49] = z[30] * z[54];
z[50] = z[22] + z[23];
z[50] = z[21] * z[50];
z[44] = z[44] + -z[48] + z[49] + z[50];
z[48] = z[13] + -z[53] + -z[55] + -z[63];
z[48] = z[1] * z[48];
z[43] = z[6] * z[43];
z[49] = -z[4] / T(2) + z[52];
z[50] = z[13] + T(3) * z[16] + -z[64];
z[49] = z[49] * z[50];
z[50] = z[12] + -z[13];
z[50] = z[18] * z[50];
z[43] = z[43] + z[48] + z[49] + z[50];
z[43] = z[4] * z[43];
z[35] = z[10] + z[13] + -z[35] + -z[39] + z[68];
z[35] = z[8] * z[35];
z[37] = T(-5) * z[12] + -z[15] + z[37] + -z[45];
z[37] = z[25] * z[37];
z[39] = z[29] * z[52];
z[39] = -z[28] + z[39];
z[39] = T(3) * z[39];
z[45] = -z[12] + z[69];
z[39] = z[39] * z[45];
z[34] = -z[11] + z[34] + z[41];
z[34] = prod_pow(z[18], 2) * z[34];
z[41] = T(-2) * z[0] + (T(3) * z[9]) / T(2) + (T(-5) * z[16]) / T(2) + T(3) * z[42] + z[46];
z[41] = z[7] * z[41];
z[42] = -(z[20] * z[21]);
return z[32] + z[33] + z[34] + T(2) * z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + T(3) * z[44] + z[47];
}



template IntegrandConstructorType<double> f_3_85_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_85_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_85_construct (const Kin<qd_real>&);
#endif

}