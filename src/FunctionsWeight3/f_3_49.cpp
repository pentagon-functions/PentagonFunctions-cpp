#include "f_3_49.h"

namespace PentagonFunctions {

template <typename T> T f_3_49_abbreviated (const std::array<T,32>&);

template <typename T> class SpDLog_f_3_49_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_49_W_10 (const Kin<T>& kin) {
        c[0] = bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + (T(23) / T(2) + (T(-23) * kin.v[2]) / T(8) + (T(23) * kin.v[4]) / T(2)) * kin.v[2] + kin.v[1] * (T(23) / T(2) + (T(-23) * kin.v[2]) / T(4) + (T(23) * kin.v[4]) / T(2) + (T(-23) / T(8) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[2] + T(4) * kin.v[4]));
c[1] = (T(9) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + (T(9) * kin.v[2]) / T(2) + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[15] * ((abb[16] * T(-9)) / T(2) + abb[8] * ((abb[8] * T(-23)) / T(4) + abb[9] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[8] * abb[13] * T(4) + abb[7] * ((abb[7] * T(5)) / T(4) + (abb[8] * T(9)) / T(2) + abb[13] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[9] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_49_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl13 = DLog_W_13<T>(kin),dl11 = DLog_W_11<T>(kin),dl10 = DLog_W_10<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),spdl10 = SpDLog_f_3_49_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);

        std::array<std::complex<T>,32> abbr = 
            {dl26(t) / kin_path.SqrtDelta, f_2_2_4(kin_path), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), dl11(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_2(kin_path), dl10(t), f_2_1_7(kin_path), dl18(t), f_2_1_8(kin_path), dl4(t), f_2_1_6(kin_path), f_2_1_9(kin_path), rlog(v_path[0] + v_path[1]), dl16(t), dl5(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl2(t), dl1(t), dl20(t), dl17(t), dl3(t)}
;

        auto result = f_3_49_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_3_49_abbreviated(const std::array<T,32>& abb)
{
using TR = typename T::value_type;
T z[64];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = bc<TR>[0];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[19];
z[14] = abb[27];
z[15] = abb[29];
z[16] = abb[31];
z[17] = abb[17];
z[18] = abb[24];
z[19] = abb[28];
z[20] = abb[30];
z[21] = abb[12];
z[22] = abb[13];
z[23] = abb[23];
z[24] = abb[14];
z[25] = abb[16];
z[26] = abb[18];
z[27] = abb[20];
z[28] = abb[21];
z[29] = abb[22];
z[30] = abb[25];
z[31] = abb[26];
z[32] = T(2) * z[20];
z[33] = z[17] + z[32];
z[34] = T(2) * z[18];
z[35] = -z[13] + z[16];
z[36] = -z[33] + z[34] + z[35];
z[36] = z[9] * z[36];
z[37] = T(3) * z[20];
z[38] = T(2) * z[17];
z[34] = -z[23] + z[34] + z[37] + z[38];
z[39] = z[7] * z[34];
z[40] = T(3) * z[19];
z[41] = z[37] + -z[40];
z[42] = z[16] + z[17];
z[43] = T(4) * z[4];
z[44] = z[42] + -z[43];
z[45] = T(3) * z[18];
z[46] = -z[41] + z[44] + z[45];
z[46] = z[6] * z[46];
z[47] = -z[14] + z[15];
z[48] = -z[35] + z[43] + T(-3) * z[47];
z[48] = z[5] * z[48];
z[49] = z[18] + -z[20];
z[50] = -z[16] + z[19] + -z[47] + z[49];
z[50] = T(3) * z[50];
z[51] = -(z[31] * z[50]);
z[52] = z[18] + z[38];
z[53] = z[14] + -z[35];
z[53] = T(-7) * z[20] + -z[52] + T(3) * z[53];
z[53] = z[8] * z[53];
z[54] = T(3) * z[23];
z[41] = T(5) * z[13] + -z[17] + z[41] + -z[54];
z[41] = z[21] * z[41];
z[55] = z[8] * z[23];
z[56] = -(z[22] * z[49]);
z[57] = z[13] + z[14] + -z[19] + -z[23];
z[57] = T(3) * z[57];
z[58] = -(z[29] * z[57]);
z[59] = -z[5] + z[21];
z[59] = z[12] * z[59];
z[36] = z[36] + z[39] + z[41] + z[46] + z[48] + z[51] + z[53] + z[55] + z[56] + z[58] + T(4) * z[59];
z[36] = int_to_imaginary<T>(1) * z[36];
z[39] = T(37) * z[13] + T(41) * z[16];
z[39] = -z[14] + (T(-3) * z[17]) / T(4) + (T(13) * z[18]) / T(6) + -z[20] / T(2) + z[39] / T(12);
z[39] = -z[23] / T(3) + z[39] / T(2);
z[39] = z[10] * z[39];
z[36] = z[36] + z[39];
z[36] = z[10] * z[36];
z[39] = z[32] + z[40];
z[41] = z[39] + z[45];
z[45] = z[14] + T(4) * z[15] + z[35] + z[38] + -z[41];
z[45] = z[7] * z[45];
z[46] = z[8] + -z[9];
z[48] = -z[7] + z[46];
z[51] = T(2) * z[4];
z[48] = z[48] * z[51];
z[45] = z[45] + z[48];
z[39] = z[39] + z[54];
z[48] = T(4) * z[14] + z[15];
z[53] = z[38] + z[48];
z[54] = z[35] + z[39] + -z[53];
z[54] = z[22] * z[54];
z[56] = T(2) * z[13];
z[58] = z[17] + -z[47] + z[51] + -z[56];
z[58] = z[6] * z[58];
z[59] = z[8] + T(2) * z[9];
z[59] = z[35] * z[59];
z[60] = T(2) * z[16];
z[61] = -z[17] + -z[47] + z[60];
z[61] = z[21] * z[61];
z[62] = -z[35] + z[47];
z[43] = -z[43] + (T(5) * z[62]) / T(2);
z[43] = z[5] * z[43];
z[43] = z[43] + -z[45] + -z[54] + z[58] + T(2) * z[59] + z[61];
z[43] = z[5] * z[43];
z[58] = (T(3) * z[14]) / T(2);
z[59] = z[17] / T(2);
z[51] = (T(-3) * z[15]) / T(2) + z[51] + z[58] + -z[59] + -z[60];
z[51] = z[6] * z[51];
z[61] = T(3) * z[13];
z[63] = -z[42] + z[61];
z[63] = z[9] * z[63];
z[53] = z[13] + T(5) * z[20] + -z[53] + z[60];
z[53] = z[8] * z[53];
z[47] = z[35] + z[47];
z[47] = z[21] * z[47];
z[45] = z[45] + z[47] + z[51] + z[53] + z[63];
z[45] = z[6] * z[45];
z[47] = z[18] + z[23];
z[33] = z[33] + z[47] + -z[62];
z[33] = z[7] * z[33];
z[37] = -z[14] + -z[15] + -z[16] + T(-3) * z[17] + -z[37] + z[61];
z[37] = z[8] * z[37];
z[40] = z[13] + -z[40] + -z[49];
z[51] = -z[15] + -z[17] + z[23];
z[40] = z[40] / T(2) + -z[51];
z[40] = z[22] * z[40];
z[53] = z[9] * z[49];
z[33] = z[33] + z[37] + z[40] + z[53] + z[55];
z[33] = z[22] * z[33];
z[37] = -z[38] + z[56];
z[38] = z[14] + -z[16];
z[32] = z[15] + -z[32] + -z[37] + z[38];
z[32] = z[8] * z[32];
z[40] = -z[13] + (T(-3) * z[20]) / T(2) + z[58] + z[59];
z[40] = z[21] * z[40];
z[53] = z[13] + T(-3) * z[16] + z[17];
z[53] = z[9] * z[53];
z[32] = z[32] + z[40] + z[53] + z[54];
z[32] = z[21] * z[32];
z[40] = -z[22] + z[46];
z[46] = -z[21] + -z[40];
z[46] = z[21] * z[46];
z[40] = T(2) * z[5] + -z[21] + z[40];
z[40] = z[5] * z[40];
z[40] = T(2) * z[24] + z[40] + z[46];
z[40] = z[12] * z[40];
z[46] = -z[13] + -z[49] + -z[51];
z[46] = z[26] * z[46];
z[40] = z[40] + z[46];
z[46] = -z[35] / T(2) + -z[49] + z[59];
z[46] = prod_pow(z[9], 2) * z[46];
z[34] = -(z[9] * z[34]);
z[48] = z[13] + T(-6) * z[16] + -z[17] + (T(-19) * z[20]) / T(2) + z[48];
z[48] = z[8] * z[48];
z[47] = (T(-5) * z[14]) / T(2) + z[16] + (T(3) * z[19]) / T(2) + (T(9) * z[20]) / T(4) + -z[47] / T(2);
z[47] = z[7] * z[47];
z[34] = z[34] + z[47] + z[48];
z[34] = z[7] * z[34];
z[41] = z[13] + T(5) * z[15] + -z[41] + z[44];
z[41] = z[11] * z[41];
z[37] = (T(7) * z[16]) / T(2) + (T(25) * z[20]) / T(4) + -z[37];
z[37] = z[8] * z[37];
z[44] = T(4) * z[20] + z[52];
z[44] = z[9] * z[44];
z[37] = z[37] + z[44];
z[37] = z[8] * z[37];
z[44] = -z[8] / T(2) + -z[9];
z[44] = z[44] * z[55];
z[38] = (T(21) * z[20]) / T(2) + T(-2) * z[23] + T(-5) * z[38] + z[52];
z[38] = z[25] * z[38];
z[39] = -z[13] + T(-5) * z[14] + z[39] + -z[42];
z[39] = z[24] * z[39];
z[42] = z[30] * z[50];
z[47] = z[0] + z[2];
z[47] = -z[3] + T(2) * z[47];
z[47] = z[1] * z[47];
z[35] = z[28] * z[35];
z[48] = z[27] * z[57];
return z[32] + z[33] + z[34] + T(-6) * z[35] + z[36] + z[37] + z[38] + z[39] + T(2) * z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48];
}



template IntegrandConstructorType<double> f_3_49_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_49_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_49_construct (const Kin<qd_real>&);
#endif

}