#include "f_3_5.h"

namespace PentagonFunctions {

template <typename T> T f_3_5_abbreviated (const std::array<T,11>&);

template <typename T> class SpDLog_f_3_5_W_7 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_3_5_W_7 (const Kin<T>& kin) {
        c[0] = T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[3] * (T(-2) + T(-2) * kin.v[1] + kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) + -prod_pow(abb[1], 2));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_5_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl14 = DLog_W_14<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),spdl7 = SpDLog_f_3_5_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,11> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), dl14(t), rlog(v_path[3]), dl4(t), f_2_1_3(kin_path), f_2_1_11(kin_path), rlog(-v_path[1] + v_path[3]), dl19(t), dl5(t)}
;

        auto result = f_3_5_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_3_5_abbreviated(const std::array<T,11>& abb)
{
using TR = typename T::value_type;
T z[18];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[5];
z[3] = abb[9];
z[4] = abb[10];
z[5] = abb[2];
z[6] = abb[4];
z[7] = bc<TR>[0];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[8];
z[11] = z[2] + z[3];
z[12] = -z[4] + z[11];
z[13] = z[5] * z[12];
z[14] = -(z[10] * z[12]);
z[15] = z[1] + -z[3];
z[16] = -(z[6] * z[15]);
z[14] = z[13] + z[14] + z[16];
z[14] = int_to_imaginary<T>(1) * z[14];
z[16] = -(z[7] * z[15]);
z[14] = T(2) * z[14] + z[16];
z[14] = z[7] * z[14];
z[16] = -z[8] + -z[9];
z[16] = z[12] * z[16];
z[12] = z[6] * z[12];
z[17] = -(z[2] * z[5]);
z[17] = T(-2) * z[12] + z[17];
z[17] = z[5] * z[17];
z[12] = z[12] + z[13];
z[11] = -z[1] + T(2) * z[4] + -z[11];
z[11] = z[0] * z[11];
z[11] = z[11] + T(2) * z[12];
z[11] = z[0] * z[11];
z[12] = prod_pow(z[6], 2) * z[15];
return z[11] + z[12] + z[14] + T(2) * z[16] + z[17];
}



template IntegrandConstructorType<double> f_3_5_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_5_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_5_construct (const Kin<qd_real>&);
#endif

}