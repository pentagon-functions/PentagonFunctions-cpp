#include "f_3_66.h"

namespace PentagonFunctions {

template <typename T> T f_3_66_abbreviated (const std::array<T,33>&);

template <typename T> class SpDLog_f_3_66_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_66_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(16) * kin.v[0]) / T(3) + (bc<T>[1] * T(-2) + T(2)) * kin.v[1] + T(-4) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[1] * (T(4) + T(-4) * kin.v[0] + T(4) * kin.v[3])) + ((T(16) * kin.v[2]) / T(3) + (T(-16) * kin.v[3]) / T(3) + (T(-16) * kin.v[4]) / T(3)) * kin.v[0] + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(8) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[1] * (T(4) + T(-4) * kin.v[0] + T(-2) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + bc<T>[1] * T(4) + T(8)) * kin.v[1] + T(8) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + bc<T>[1] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(-8) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[15] * (abb[6] * (abb[6] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[2] * abb[6] * T(4) + abb[6] * abb[14] * T(4) + abb[1] * (abb[6] * T(-4) + abb[3] * T(4)) + abb[8] * T(8) + abb[3] * (abb[2] * T(-4) + abb[6] * T(-4) + abb[14] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[3] * T(8)));
    }
};
template <typename T> class SpDLog_f_3_66_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_66_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (kin.v[1] + T(-4) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (T(3) * kin.v[3] + T(2) * kin.v[4]) + kin.v[0] * ((T(4) * kin.v[1]) / T(3) + (T(2) * kin.v[2]) / T(3) + (T(-10) * kin.v[3]) / T(3) + (bc<T>[1] + T(1) / T(3)) * kin.v[0] + T(-2) * kin.v[4] + bc<T>[1] * (T(-2) + T(-2) * kin.v[2] + T(2) * kin.v[4])) + bc<T>[1] * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3]) + bc<T>[1] * (T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[16] * (abb[6] * (abb[6] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) + abb[4] * abb[6] * T(2) + abb[6] * abb[14] * T(2) + abb[1] * (abb[6] * T(-2) + abb[5] * T(2)) + abb[17] * T(4) + abb[5] * (abb[4] * T(-2) + abb[6] * T(-2) + abb[14] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[5] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_66_construct (const Kin<T>& kin) {
    return [&kin, 
            dl17 = DLog_W_17<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl6 = DLog_W_6<T>(kin),dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),spdl22 = SpDLog_f_3_66_W_22<T>(kin),spdl21 = SpDLog_f_3_66_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,33> abbr = 
            {dl17(t), rlog(v_path[0]), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_9(kin_path), f_2_1_14(kin_path), dl26(t) / kin_path.SqrtDelta, f_2_2_9(kin_path), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl6(t), rlog(v_path[2]), dl22(t), dl21(t), f_2_1_15(kin_path), dl4(t), f_2_1_2(kin_path), dl16(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl3(t), dl1(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl18(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl20(t), dl19(t), dl2(t)}
;

        auto result = f_3_66_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_66_abbreviated(const std::array<T,33>& abb)
{
using TR = typename T::value_type;
T z[70];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[13];
z[10] = abb[18];
z[11] = abb[20];
z[12] = abb[23];
z[13] = abb[24];
z[14] = abb[27];
z[15] = abb[30];
z[16] = abb[31];
z[17] = abb[32];
z[18] = abb[14];
z[19] = bc<TR>[0];
z[20] = abb[9];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[12];
z[24] = abb[17];
z[25] = abb[19];
z[26] = abb[21];
z[27] = abb[22];
z[28] = abb[25];
z[29] = abb[26];
z[30] = abb[28];
z[31] = abb[29];
z[32] = T(3) * z[27];
z[33] = T(3) * z[5] + -z[32];
z[34] = T(3) * z[18];
z[35] = T(4) * z[3] + z[34];
z[36] = T(3) * z[31];
z[37] = T(2) * z[1];
z[38] = -z[4] + -z[33] + -z[35] + z[36] + -z[37];
z[38] = z[16] * z[38];
z[39] = T(2) * z[5];
z[40] = T(2) * z[18];
z[41] = z[39] + z[40];
z[36] = -z[36] + z[41];
z[42] = z[1] + z[6];
z[43] = T(2) * z[4];
z[44] = T(3) * z[2];
z[45] = z[36] + z[42] + z[43] + -z[44];
z[45] = z[14] * z[45];
z[46] = -z[2] + z[3];
z[36] = z[1] + z[4] + -z[32] + z[36] + -z[46];
z[36] = z[15] * z[36];
z[47] = z[1] + -z[4];
z[48] = T(2) * z[47];
z[49] = -z[3] + z[48];
z[41] = z[6] + -z[41] + z[44] + -z[49];
z[41] = z[13] * z[41];
z[33] = -z[6] + -z[33] + -z[34] + -z[49];
z[33] = z[12] * z[33];
z[49] = -z[1] + z[2];
z[50] = -z[3] + z[49];
z[32] = -z[5] + z[18] + -z[32] + -z[50];
z[32] = z[11] * z[32];
z[51] = T(3) * z[11];
z[52] = T(2) * z[16];
z[53] = z[15] + z[52];
z[53] = z[51] + T(2) * z[53];
z[53] = z[6] * z[53];
z[54] = T(3) * z[17];
z[42] = z[29] + z[31] + -z[42];
z[42] = z[42] * z[54];
z[55] = -z[3] + z[6] + T(3) * z[47];
z[55] = z[10] * z[55];
z[56] = T(3) * z[13];
z[57] = -(z[29] * z[56]);
z[32] = z[32] + z[33] + z[36] + z[38] + z[41] + z[42] + z[45] + z[53] + z[55] + z[57];
z[32] = int_to_imaginary<T>(1) * z[32];
z[33] = -z[13] + z[14];
z[36] = z[15] + (T(-5) * z[16]) / T(4);
z[33] = -z[10] + (T(9) * z[11]) / T(4) + (T(-31) * z[12]) / T(12) + z[17] + (T(5) * z[33]) / T(6) + z[36] / T(3);
z[33] = z[19] * z[33];
z[32] = z[32] + z[33] / T(2);
z[32] = z[19] * z[32];
z[33] = prod_pow(z[1], 2);
z[36] = prod_pow(z[4], 2);
z[33] = z[33] + -z[36];
z[38] = (T(3) * z[3]) / T(2);
z[41] = T(2) * z[49];
z[42] = -z[18] + z[38] + -z[41];
z[42] = z[3] * z[42];
z[45] = z[5] / T(2);
z[53] = T(2) * z[3];
z[55] = z[18] + -z[53];
z[55] = z[45] + T(2) * z[55];
z[55] = z[5] * z[55];
z[57] = z[18] + z[49];
z[58] = -z[3] + z[57];
z[59] = -z[6] + z[58];
z[59] = z[6] * z[59];
z[60] = T(3) * z[25];
z[61] = z[2] * z[47];
z[62] = z[18] + z[48];
z[62] = z[18] * z[62];
z[42] = z[33] + z[42] + z[55] + -z[59] + -z[60] + T(-2) * z[61] + z[62];
z[42] = z[13] * z[42];
z[55] = z[40] * z[47];
z[55] = -z[36] + z[55];
z[38] = -z[38] + -z[57];
z[38] = z[3] * z[38];
z[35] = z[35] + -z[39];
z[35] = z[5] * z[35];
z[39] = T(3) * z[4] + -z[37];
z[39] = z[1] * z[39];
z[35] = z[35] + z[38] + z[39] + z[55] + z[59] + -z[60] + z[61];
z[35] = z[12] * z[35];
z[38] = -z[18] + z[47];
z[39] = z[5] + T(2) * z[38];
z[39] = z[5] * z[39];
z[59] = T(3) * z[30];
z[39] = z[39] + z[59];
z[62] = z[3] / T(2);
z[63] = -z[57] + z[62];
z[63] = z[3] * z[63];
z[64] = z[1] / T(2);
z[65] = -z[43] + z[64];
z[65] = z[1] * z[65];
z[66] = z[2] / T(2);
z[67] = z[1] + -z[66];
z[67] = z[2] * z[67];
z[43] = -z[1] + -z[2] + z[18] / T(2) + z[43];
z[43] = z[18] * z[43];
z[43] = z[36] + z[39] + z[43] + -z[63] + z[65] + z[67];
z[43] = z[15] * z[43];
z[65] = T(5) * z[3];
z[67] = T(4) * z[18];
z[49] = T(3) * z[49] + -z[65] + z[67];
z[49] = z[3] * z[49];
z[65] = -z[5] + z[34] + T(-4) * z[47] + -z[65];
z[65] = z[5] * z[65];
z[68] = -z[4] + z[37];
z[69] = z[1] * z[68];
z[49] = z[49] + z[55] + -z[59] + T(-3) * z[61] + z[65] + z[69];
z[49] = z[16] * z[49];
z[55] = z[66] + -z[68];
z[55] = z[2] * z[55];
z[41] = -z[18] + z[41] + -z[62];
z[41] = z[3] * z[41];
z[59] = (T(3) * z[1]) / T(2) + -z[4];
z[59] = z[1] * z[59];
z[57] = z[18] * z[57];
z[38] = T(-7) * z[3] + (T(-3) * z[5]) / T(2) + -z[38];
z[38] = z[5] * z[38];
z[38] = z[38] + z[41] + z[55] + z[57] + z[59] + z[60];
z[38] = z[11] * z[38];
z[37] = z[37] + -z[40];
z[40] = z[6] / T(2);
z[41] = -z[4] + z[5];
z[55] = -z[37] + -z[40] + z[41];
z[55] = z[6] * z[55];
z[57] = -z[4] + z[64];
z[57] = z[1] * z[57];
z[36] = z[36] / T(2) + z[57];
z[57] = z[24] + z[36];
z[59] = -z[47] + z[66];
z[62] = z[44] * z[59];
z[48] = -z[18] + z[48];
z[48] = z[18] * z[48];
z[39] = z[39] + z[48] + z[55] + z[57] + z[60] + z[62];
z[39] = z[14] * z[39];
z[37] = z[37] + z[41] + z[46];
z[37] = z[15] * z[37];
z[44] = T(4) * z[1] + T(3) * z[3] + z[41] + -z[44] + -z[67];
z[44] = z[16] * z[44];
z[34] = T(3) * z[1] + T(-2) * z[2] + -z[34] + z[41] + z[53];
z[34] = z[11] * z[34];
z[48] = (T(9) * z[11]) / T(2) + T(2) * z[15] + (T(5) * z[16]) / T(2);
z[48] = z[6] * z[48];
z[34] = z[34] + z[37] + z[44] + z[48];
z[34] = z[6] * z[34];
z[37] = -z[41] + z[46];
z[37] = z[6] * z[37];
z[41] = -(z[1] * z[47]);
z[44] = z[3] * z[50];
z[48] = z[3] + z[47];
z[48] = z[5] * z[48];
z[37] = T(-2) * z[7] + z[37] + z[41] + z[44] + z[48] + z[61];
z[37] = z[0] * z[37];
z[36] = z[7] + z[25] + z[36] + -z[61];
z[40] = -z[40] + -z[58];
z[40] = z[6] * z[40];
z[36] = T(3) * z[36] + z[40] + -z[63];
z[36] = z[10] * z[36];
z[40] = -(z[2] * z[59]);
z[41] = z[3] + -z[45] + -z[47];
z[41] = z[5] * z[41];
z[44] = -(z[6] * z[46]);
z[40] = -z[25] + -z[28] + -z[30] + z[40] + z[41] + z[44] + -z[57];
z[40] = z[40] * z[54];
z[41] = z[12] + z[16];
z[44] = T(2) * z[13];
z[45] = -z[0] + -z[10] + -z[15] + -z[41] + z[44] + z[51];
z[45] = z[8] * z[45];
z[46] = int_to_imaginary<T>(1) * z[19];
z[46] = z[46] + -z[67];
z[46] = z[46] * z[47];
z[33] = (T(3) * z[7]) / T(2) + -z[33] / T(2) + z[46] + T(4) * z[61];
z[33] = z[9] * z[33];
z[46] = T(-3) * z[12] + (T(-5) * z[13]) / T(2) + z[52];
z[46] = z[7] * z[46];
z[47] = z[28] * z[56];
z[44] = T(5) * z[11] + T(-7) * z[12] + z[15] + z[16] + z[44];
z[44] = z[24] * z[44];
z[41] = z[11] + z[15] + -z[41];
z[41] = z[26] * z[41];
z[48] = z[20] + T(-3) * z[22] + z[23];
z[48] = z[21] * z[48];
return z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + T(3) * z[41] + z[42] + z[43] + z[44] + T(2) * z[45] + z[46] + z[47] + z[48] + z[49];
}



template IntegrandConstructorType<double> f_3_66_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_66_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_66_construct (const Kin<qd_real>&);
#endif

}