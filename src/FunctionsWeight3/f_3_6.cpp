#include "f_3_6.h"

namespace PentagonFunctions {

template <typename T> T f_3_6_abbreviated (const std::array<T,9>&);



template <typename T> IntegrandConstructorType<T> f_3_6_construct (const Kin<T>& kin) {
    return [&kin, 
            dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,9> abbr = 
            {dl4(t), f_2_1_2(kin_path), f_2_1_9(kin_path), dl18(t), dl1(t), rlog(v_path[0]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl3(t)}
;

        auto result = f_3_6_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_6_abbreviated(const std::array<T,9>& abb)
{
using TR = typename T::value_type;
T z[14];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[8];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = bc<TR>[0];
z[10] = prod_pow(z[6], 2);
z[11] = -(z[6] * z[8]);
z[12] = prod_pow(z[9], 2);
z[13] = -z[6] + z[8];
z[13] = z[7] * z[13];
z[10] = z[10] + z[11] + -z[12] / T(6) + z[13];
z[11] = -z[4] + z[5];
z[10] = z[10] * z[11];
z[12] = -z[0] + z[3];
z[13] = z[11] + z[12];
z[13] = z[2] * z[13];
z[11] = z[11] + -z[12];
z[11] = z[1] * z[11];
return z[10] + z[11] + z[13];
}



template IntegrandConstructorType<double> f_3_6_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_6_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_6_construct (const Kin<qd_real>&);
#endif

}