#include "FunctionID.h"
#include "Integrate.h"
#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "FunctionsMap.hpp"

namespace PentagonFunctions {

template <> std::unordered_map<FunID<KinType::m0>, FunctionObjectType<qd_real,KinType::m0>> functions_map<qd_real,KinType::m0> = {};

template void detail::fill_function_map<qd_real, KinType::m0>();

template FunctionObjectType<qd_real,KinType::m0> FunID<KinType::m0>::get_evaluator<qd_real>() const;

#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
template <> std::unordered_map<FunID<KinType::m1>, FunctionObjectType<qd_real,KinType::m1>> functions_map<qd_real,KinType::m1> = {};

template void detail::fill_function_map<qd_real, KinType::m1>();

template FunctionObjectType<qd_real,KinType::m1> FunID<KinType::m1>::get_evaluator<qd_real>() const;
#endif // PENTAGON_FUNCTIONS_M1_ENABLED


} // namespace PentagonFunctions

