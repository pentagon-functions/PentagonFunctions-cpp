#include "PentagonFunctions_config.h"

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
#include <qd/qd_real.h>
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED


#include "tanh_sinh.hpp"


namespace boost {
namespace math {
namespace quadrature {
namespace detail {

template class tanh_sinh_detail<double>;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template class tanh_sinh_detail<dd_real>;
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template class tanh_sinh_detail<qd_real>;
#endif

} // namespace detail
} // namespace quadrature
} // namespace math
} // namespace boost

