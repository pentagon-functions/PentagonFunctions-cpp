#pragma once

#include "Kin.h"
#include "Constants.h"

namespace PentagonFunctions {

#ifdef PENTAGON_FUNCTIONS_M0_ENABLED 

template <typename T> class SqrtDeltaOnPath {
    using TC = std::complex<T>;
    std::array<T, 4> c;

  public:
    SqrtDeltaOnPath(const Kin<T>& kin) {
        c[0] = T(-3) +
               kin.v[2] * (kin.v[2] * (T(4) + (T(-4) + kin.v[3]) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3] * (T(-4) + T(-2) * kin.v[4])) + T(-4) * kin.v[4]) +
               T(4) * prod_pow(kin.v[4], 2) +
               kin.v[1] * (kin.v[1] * (T(4) + kin.v[2] * (T(4) + kin.v[2])) + kin.v[3] * (T(-4) + T(-8) * kin.v[4]) + T(-4) * kin.v[4] +
                           kin.v[2] * (T(2) + kin.v[2] * (T(4) + T(-2) * kin.v[3]) + T(-8) * kin.v[4] + T(2) * kin.v[3] * kin.v[4])) +
               kin.v[3] * (kin.v[4] * (T(2) + T(4) * kin.v[4]) + kin.v[3] * (T(4) + kin.v[4] * (T(4) + kin.v[4]))) +
               kin.v[0] *
                   (T(4) + (T(2) + T(-4) * kin.v[4]) * kin.v[4] + kin.v[3] * (T(-4) + (T(-4) + T(-2) * kin.v[4]) * kin.v[4]) +
                    kin.v[0] * (kin.v[1] * (kin.v[1] + T(-2) * kin.v[4]) + prod_pow(kin.v[4], 2)) + kin.v[2] * (T(-4) + kin.v[3] * (T(4) + T(2) * kin.v[4])) +
                    kin.v[1] * (T(2) + kin.v[1] * (T(-4) + T(-2) * kin.v[2]) + T(8) * kin.v[4] + T(2) * kin.v[3] * kin.v[4] +
                                kin.v[2] * (T(-4) + T(2) * kin.v[3] + T(2) * kin.v[4])));
        c[1] = T(12) + T(-8) * prod_pow(kin.v[4], 2) +
               kin.v[2] * (kin.v[2] * (T(-8) + T(4) * kin.v[3]) + kin.v[3] * (T(-4) + T(4) * kin.v[3]) + T(8) * kin.v[4]) +
               kin.v[3] * (kin.v[3] * (T(-8) + T(-4) * kin.v[4]) + (T(-4) + T(-4) * kin.v[4]) * kin.v[4]) +
               kin.v[0] * (T(-12) + kin.v[2] * (T(8) + T(-4) * kin.v[3]) + kin.v[1] * (T(-4) + T(4) * kin.v[1] + T(4) * kin.v[2] + T(-8) * kin.v[4]) +
                           kin.v[4] * (T(-4) + T(4) * kin.v[4]) + kin.v[3] * (T(8) + T(4) * kin.v[4])) +
               kin.v[1] * (kin.v[1] * (T(-8) + T(-4) * kin.v[2]) + T(8) * kin.v[4] + kin.v[3] * (T(8) + T(8) * kin.v[4]) +
                           kin.v[2] * (T(-4) + T(-4) * kin.v[2] + T(8) * kin.v[4]));
        c[2] = T(-18) + kin.v[1] * (T(4) * kin.v[1] + T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) +
               kin.v[2] * (T(4) * kin.v[2] + T(2) * kin.v[3] + T(-4) * kin.v[4]) + T(4) * prod_pow(kin.v[4], 2) +
               kin.v[0] * (T(12) + T(2) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (T(4) * kin.v[3] + T(2) * kin.v[4]);
        c[3] = T(12) + T(-4) * kin.v[0];
    }

    TC operator()(T t) const { return sqrt(static_cast<TC>(T(-3) + t * (t * (t * (t * c[0] + c[1]) + c[2]) + c[3]))); }
};

template <typename T> class DLog_W_1 {
    std::array<T, 2> c;

  public:
    DLog_W_1(const Kin<T>& kin) {
        c[0] = T(-3) + kin.v[0];
        c[1] = T(-3) + kin.v[0];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(3)); }
};

template <typename T> class DLog_W_2 {
    std::array<T, 2> c;

  public:
    DLog_W_2(const Kin<T>& kin) {
        c[0] = T(1) + kin.v[1];
        c[1] = T(1) + kin.v[1];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(-1)); }
};

template <typename T> class DLog_W_3 {
    std::array<T, 2> c;

  public:
    DLog_W_3(const Kin<T>& kin) {
        c[0] = T(-1) + kin.v[2];
        c[1] = T(-1) + kin.v[2];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(1)); }
};

template <typename T> class DLog_W_4 {
    std::array<T, 2> c;

  public:
    DLog_W_4(const Kin<T>& kin) {
        c[0] = T(-1) + kin.v[3];
        c[1] = T(-1) + kin.v[3];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(1)); }
};

template <typename T> class DLog_W_5 {
    std::array<T, 2> c;

  public:
    DLog_W_5(const Kin<T>& kin) {
        c[0] = T(1) + kin.v[4];
        c[1] = T(1) + kin.v[4];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(-1)); }
};

template <typename T> class DLog_W_6 {
    std::array<T, 2> c;

  public:
    DLog_W_6(const Kin<T>& kin) {
        c[0] = T(-2) + kin.v[2] + kin.v[3];
        c[1] = T(-2) + kin.v[2] + kin.v[3];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(2)); }
};

template <typename T> class DLog_W_7 {
    bool vanishing{false};

  public:
    DLog_W_7(const Kin<T>& kin) {
        if (abs(kin.W[6]) < detail::VanishingDLogThreshold<T>) vanishing = true;
    }

    T operator()(T t) const {
        if (vanishing) return static_cast<T>(0);
        return T(1) / t;
    }
};

template <typename T> class DLog_W_8 {
    std::array<T, 2> c;

  public:
    DLog_W_8(const Kin<T>& kin) {
        c[0] = T(-2) + kin.v[0] + kin.v[4];
        c[1] = T(-2) + kin.v[0] + kin.v[4];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(2)); }
};

template <typename T> class DLog_W_9 {
    std::array<T, 2> c;

  public:
    DLog_W_9(const Kin<T>& kin) {
        c[0] = T(-2) + kin.v[0] + kin.v[1];
        c[1] = T(-2) + kin.v[0] + kin.v[1];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(2)); }
};

template <typename T> class DLog_W_10 {
    bool vanishing{false};

  public:
    DLog_W_10(const Kin<T>& kin) {
        if (abs(kin.W[9]) < detail::VanishingDLogThreshold<T>) vanishing = true;
    }

    T operator()(T t) const {
        if (vanishing) return static_cast<T>(0);
        return T(1) / t;
    }
};

template <typename T> class DLog_W_11 {
    std::array<T, 2> c;

  public:
    DLog_W_11(const Kin<T>& kin) {
        c[0] = T(-2) + kin.v[0] + -kin.v[3];
        c[1] = T(-2) + kin.v[0] + -kin.v[3];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(2)); }
};

template <typename T> class DLog_W_12 {
    bool vanishing{false};

  public:
    DLog_W_12(const Kin<T>& kin) {
        if (abs(kin.W[11]) < detail::VanishingDLogThreshold<T>) vanishing = true;
    }

    T operator()(T t) const {
        if (vanishing) return static_cast<T>(0);
        return T(1) / t;
    }
};

template <typename T> class DLog_W_13 {
    std::array<T, 2> c;

  public:
    DLog_W_13(const Kin<T>& kin) {
        c[0] = T(-2) + kin.v[0] + -kin.v[2];
        c[1] = T(-2) + kin.v[0] + -kin.v[2];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(2)); }
};

template <typename T> class DLog_W_14 {
    std::array<T, 2> c;

  public:
    DLog_W_14(const Kin<T>& kin) {
        c[0] = T(2) + kin.v[1] + -kin.v[3];
        c[1] = T(2) + kin.v[1] + -kin.v[3];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(-2)); }
};

template <typename T> class DLog_W_15 {
    std::array<T, 2> c;

  public:
    DLog_W_15(const Kin<T>& kin) {
        c[0] = T(-2) + kin.v[2] + -kin.v[4];
        c[1] = T(-2) + kin.v[2] + -kin.v[4];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(2)); }
};

template <typename T> class DLog_W_16 {
    std::array<T, 2> c;

  public:
    DLog_W_16(const Kin<T>& kin) {
        c[0] = T(-1) + kin.v[0] + kin.v[1] + -kin.v[3];
        c[1] = T(-1) + kin.v[0] + kin.v[1] + -kin.v[3];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(1)); }
};

template <typename T> class DLog_W_17 {
    std::array<T, 2> c;

  public:
    DLog_W_17(const Kin<T>& kin) {
        c[0] = T(-1) + kin.v[1] + kin.v[2] + -kin.v[4];
        c[1] = T(-1) + kin.v[1] + kin.v[2] + -kin.v[4];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(1)); }
};

template <typename T> class DLog_W_18 {
    std::array<T, 2> c;

  public:
    DLog_W_18(const Kin<T>& kin) {
        c[0] = T(-1) + kin.v[0] + -kin.v[2] + -kin.v[3];
        c[1] = T(-1) + kin.v[0] + -kin.v[2] + -kin.v[3];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(1)); }
};

template <typename T> class DLog_W_19 {
    std::array<T, 2> c;

  public:
    DLog_W_19(const Kin<T>& kin) {
        c[0] = T(1) + kin.v[1] + -kin.v[3] + -kin.v[4];
        c[1] = T(1) + kin.v[1] + -kin.v[3] + -kin.v[4];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(-1)); }
};

template <typename T> class DLog_W_20 {
    std::array<T, 2> c;

  public:
    DLog_W_20(const Kin<T>& kin) {
        c[0] = T(-1) + kin.v[0] + -kin.v[2] + kin.v[4];
        c[1] = T(-1) + kin.v[0] + -kin.v[2] + kin.v[4];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(1)); }
};

template <typename T> class DLog_W_21 {
    bool vanishing{false};

  public:
    DLog_W_21(const Kin<T>& kin) {
        if (abs(kin.W[20]) < detail::VanishingDLogThreshold<T>) vanishing = true;
    }

    T operator()(T t) const {
        if (vanishing) return static_cast<T>(0);
        return T(1) / t;
    }
};

template <typename T> class DLog_W_22 {
    bool vanishing{false};

  public:
    DLog_W_22(const Kin<T>& kin) {
        if (abs(kin.W[21]) < detail::VanishingDLogThreshold<T>) vanishing = true;
    }

    T operator()(T t) const {
        if (vanishing) return static_cast<T>(0);
        return T(1) / t;
    }
};

template <typename T> class DLog_W_23 {
    bool vanishing{false};

  public:
    DLog_W_23(const Kin<T>& kin) {
        if (abs(kin.W[22]) < detail::VanishingDLogThreshold<T>) vanishing = true;
    }

    T operator()(T t) const {
        if (vanishing) return static_cast<T>(0);
        return T(1) / t;
    }
};

template <typename T> class DLog_W_24 {
    std::array<T, 2> c;

  public:
    DLog_W_24(const Kin<T>& kin) {
        c[0] = T(-2) + kin.v[0] + kin.v[1] + -kin.v[3] + -kin.v[4];
        c[1] = T(-2) + kin.v[0] + kin.v[1] + -kin.v[3] + -kin.v[4];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(2)); }
};

template <typename T> class DLog_W_25 {
    std::array<T, 2> c;

  public:
    DLog_W_25(const Kin<T>& kin) {
        c[0] = T(-2) + kin.v[0] + -kin.v[1] + -kin.v[2] + kin.v[4];
        c[1] = T(-2) + kin.v[0] + -kin.v[1] + -kin.v[2] + kin.v[4];
    }

    T operator()(T t) const { return c[0] / (t * c[1] + T(2)); }
};

template <typename T> class DLog_W_26 {
    std::array<T, 9> c;

  public:
    DLog_W_26(const Kin<T>& kin) {
        c[0] = kin.v[4] * (T(-6) + T(-6) * kin.v[4]) +
               kin.v[3] * (T(3) + prod_pow(kin.v[4], 2) * (T(-9) + T(-6) * kin.v[4]) +
                           kin.v[3] * (T(-6) + kin.v[4] * (T(-9) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])))) +
               kin.v[1] * (T(3) + T(6) * kin.v[4] + kin.v[3] * (T(6) + T(3) * kin.v[3] * prod_pow(kin.v[4], 2) + kin.v[4] * (T(18) + T(12) * kin.v[4])) +
                           kin.v[1] * (T(-6) + T(-6) * kin.v[3] * kin.v[4] + kin.v[2] * (T(-9) + T(-6) * kin.v[4] + kin.v[3] * (T(6) + T(3) * kin.v[4]))) +
                           kin.v[2] * (kin.v[4] * (T(18) + T(6) * kin.v[4]) +
                                       kin.v[3] * (kin.v[4] * (T(-18) + T(-3) * kin.v[4]) + kin.v[3] * (T(-6) + T(-3) * kin.v[4])) +
                                       kin.v[2] * (T(-9) + T(-6) * kin.v[4] + kin.v[3] * (T(6) + T(3) * kin.v[4])))) +
               kin.v[2] * (T(3) + T(6) * kin.v[4] + kin.v[2] * (T(-6) + kin.v[3] * (T(9) + kin.v[3] * (T(-6) + T(-3) * kin.v[4]))) +
                           kin.v[3] * (T(-6) + T(6) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(9) + kin.v[4] * (T(12) + T(6) * kin.v[4])))) +
               kin.v[0] * (T(-3) + kin.v[4] * (T(4) + kin.v[4] * (T(9) + T(2) * kin.v[4])) +
                           kin.v[0] * (kin.v[4] * (T(-3) + (-kin.v[4] + T(-4)) * kin.v[4]) + kin.v[3] * kin.v[4] * (T(2) + T(2) * kin.v[4]) +
                                       kin.v[2] * (-(kin.v[3] * kin.v[4]) + kin.v[4] * (T(2) + kin.v[4])) +
                                       kin.v[1] * (T(3) + kin.v[1] * (-kin.v[4] + T(-2) + kin.v[3]) + kin.v[2] * (-kin.v[4] + T(-2) + kin.v[3]) +
                                                   kin.v[3] * (T(-2) + T(-3) * kin.v[4]) + kin.v[4] * (T(6) + T(2) * kin.v[4]))) +
                           kin.v[2] * (T(4) + kin.v[3] * (T(-6) + kin.v[3] * kin.v[4] + kin.v[4] * (T(-10) + T(-7) * kin.v[4])) +
                                       kin.v[2] * (T(-2) * kin.v[4] + kin.v[3] * (kin.v[3] + T(3) * kin.v[4]))) +
                           kin.v[3] * (T(4) + kin.v[3] * kin.v[4] * (T(-2) + T(-2) * kin.v[4]) + kin.v[4] * (T(6) + kin.v[4] * (T(6) + T(4) * kin.v[4]))) +
                           kin.v[1] * (T(-8) + kin.v[4] * (T(-18) + T(-6) * kin.v[4]) +
                                       kin.v[1] * (T(9) + T(4) * kin.v[4] + kin.v[3] * (T(-4) + kin.v[4]) + kin.v[2] * (-kin.v[3] + T(2) + kin.v[4])) +
                                       kin.v[3] * (kin.v[4] * (T(-2) + T(-5) * kin.v[4]) + kin.v[3] * (T(2) + T(3) * kin.v[4])) +
                                       kin.v[2] * (T(6) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[3] + T(2) + kin.v[4]) +
                                                   kin.v[3] * (T(-2) + kin.v[3] + T(6) * kin.v[4]))));
        c[1] = kin.v[4] * (T(24) + T(18) * kin.v[4]) +
               kin.v[2] * (T(-12) + kin.v[2] * (T(18) + kin.v[3] * (T(-18) + T(6) * kin.v[3])) + T(-18) * kin.v[4] +
                           kin.v[3] * (T(18) + T(-6) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-18) + T(-12) * kin.v[4]))) +
               kin.v[3] * (T(-12) + prod_pow(kin.v[4], 2) * (T(18) + T(6) * kin.v[4]) + kin.v[3] * (T(18) + kin.v[4] * (T(18) + T(6) * kin.v[4]))) +
               kin.v[0] * (T(12) + kin.v[3] * (T(-12) + T(2) * kin.v[3] * kin.v[4] + kin.v[4] * (T(-12) + T(-6) * kin.v[4])) +
                           kin.v[4] * (T(-12) + kin.v[4] * (T(-18) + T(-2) * kin.v[4])) +
                           kin.v[0] * (T(-2) * kin.v[2] * kin.v[4] + T(-2) * kin.v[3] * kin.v[4] +
                                       kin.v[1] * (T(-6) + T(2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-6) * kin.v[4]) +
                                       kin.v[4] * (T(6) + T(4) * kin.v[4])) +
                           kin.v[1] * (T(24) + kin.v[1] * (T(-18) + T(-2) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) +
                                       kin.v[3] * (T(-2) * kin.v[3] + T(2) * kin.v[4]) +
                                       kin.v[2] * (T(-12) + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[4] * (T(36) + T(6) * kin.v[4])) +
                           kin.v[2] * (T(-12) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * (T(12) + T(10) * kin.v[4]))) +
               kin.v[1] * (T(-12) + T(-18) * kin.v[4] + kin.v[3] * (T(-18) + kin.v[4] * (T(-36) + T(-12) * kin.v[4])) +
                           kin.v[1] * (T(18) + T(6) * kin.v[3] * kin.v[4] + kin.v[2] * (T(18) + T(-6) * kin.v[3] + T(6) * kin.v[4])) +
                           kin.v[2] * (kin.v[4] * (T(-36) + T(-6) * kin.v[4]) + kin.v[2] * (T(18) + T(-6) * kin.v[3] + T(6) * kin.v[4]) +
                                       kin.v[3] * (T(6) * kin.v[3] + T(18) * kin.v[4])));
        c[2] = kin.v[4] * (T(-36) + T(-18) * kin.v[4]) +
               kin.v[2] * (T(18) + kin.v[2] * (T(-18) + T(9) * kin.v[3]) + kin.v[3] * (T(-18) + T(9) * kin.v[3]) + T(18) * kin.v[4]) +
               kin.v[3] * (T(18) + T(-9) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-18) + T(-9) * kin.v[4])) +
               kin.v[0] * (T(-18) + kin.v[2] * (T(12) + T(-6) * kin.v[3]) + kin.v[1] * (T(-24) + T(9) * kin.v[1] + T(6) * kin.v[2] + T(-18) * kin.v[4]) +
                           kin.v[0] * (T(3) * kin.v[1] + T(-3) * kin.v[4]) + kin.v[3] * (T(12) + T(6) * kin.v[4]) + kin.v[4] * (T(12) + T(9) * kin.v[4])) +
               kin.v[1] * (T(18) + kin.v[1] * (T(-18) + T(-9) * kin.v[2]) + T(18) * kin.v[4] + kin.v[3] * (T(18) + T(18) * kin.v[4]) +
                           kin.v[2] * (T(-9) * kin.v[2] + T(18) * kin.v[4]));
        c[3] = kin.v[3] * (T(-12) + T(6) * kin.v[3]) + kin.v[1] * (T(-12) + T(6) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) +
               kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) +
               kin.v[0] * (T(12) + T(8) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(24) + T(6) * kin.v[4]);
        c[4] = T(-3) * kin.v[0] + T(3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-6) * kin.v[4];
        c[5] = T(-3) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) + T(3) * kin.v[4] + kin.v[3] * (T(-3) + T(-3) * kin.v[4])) +
               kin.v[2] * (T(3) + T(3) * kin.v[4] + kin.v[3] * (T(-3) + T(-3) * kin.v[4])) + kin.v[3] * (T(3) + kin.v[4] * (T(6) + T(3) * kin.v[4])) +
               kin.v[0] * (T(1) + kin.v[4] * (T(2) + kin.v[4]) + kin.v[3] * (T(-1) + (-kin.v[4] + T(-2)) * kin.v[4]) +
                           kin.v[1] * (-kin.v[4] + T(-1) + kin.v[3] * (T(1) + kin.v[4])) + kin.v[2] * (-kin.v[4] + T(-1) + kin.v[3] * (T(1) + kin.v[4])));
        c[6] = T(12) + kin.v[4] * (T(18) + T(6) * kin.v[4]) + kin.v[3] * (T(-9) + kin.v[4] * (T(-12) + T(-3) * kin.v[4])) +
               kin.v[0] * (T(-3) + (-kin.v[4] + T(-4)) * kin.v[4] + kin.v[1] * (-kin.v[3] + T(2) + kin.v[4]) + kin.v[2] * (-kin.v[3] + T(2) + kin.v[4]) +
                           kin.v[3] * (T(2) + T(2) * kin.v[4])) +
               kin.v[1] * (T(-9) + T(-6) * kin.v[4] + kin.v[3] * (T(6) + T(3) * kin.v[4])) +
               kin.v[2] * (T(-9) + T(-6) * kin.v[4] + kin.v[3] * (T(6) + T(3) * kin.v[4]));
        c[7] = T(-18) + kin.v[4] * (T(-18) + T(-3) * kin.v[4]) + kin.v[0] * (-kin.v[1] + -kin.v[2] + -kin.v[3] + T(3) + T(2) * kin.v[4]) +
               kin.v[1] * (T(9) + T(-3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[2] * (T(9) + T(-3) * kin.v[3] + T(3) * kin.v[4]) +
               kin.v[3] * (T(9) + T(6) * kin.v[4]);
        c[8] = T(12) + -kin.v[0] + T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(6) * kin.v[4];
    }

    T operator()(T t) const { return (t * (t * (t * (t * c[0] + c[1]) + c[2]) + c[3]) + c[4]) / (t * (t * (t * (t * c[5] + c[6]) + c[7]) + c[8]) + T(-3)); }
};

template <typename T> class DLog_W_27 {
    std::array<T, 9> c;

  public:
    DLog_W_27(const Kin<T>& kin) {
        c[0] = kin.v[4] * (T(3) + T(6) * kin.v[4]) +
               kin.v[2] * (T(3) + T(-6) * kin.v[4] + kin.v[2] * (T(6) + kin.v[3] * (T(-9) + T(-6) * kin.v[4])) +
                           kin.v[3] * (T(6) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-9) + T(-6) * kin.v[4]))) +
               kin.v[3] * (T(3) + kin.v[4] * (T(6) + T(9) * kin.v[4]) + kin.v[3] * (T(6) + kin.v[4] * (T(9) + T(6) * kin.v[4]))) +
               kin.v[1] * (T(3) + T(-6) * kin.v[4] + kin.v[3] * (T(-6) + T(-18) * kin.v[4] + T(3) * kin.v[3] * prod_pow(kin.v[4], 2)) +
                           kin.v[2] * (T(6) + kin.v[4] * (T(-18) + T(-6) * kin.v[4]) + kin.v[2] * (T(9) + kin.v[3] * (T(-6) + T(-3) * kin.v[4])) +
                                       kin.v[3] * (kin.v[3] * (T(-6) + T(-3) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]))) +
                           kin.v[1] * (T(6) + T(-6) * kin.v[3] * kin.v[4] +
                                       kin.v[2] * (T(9) + kin.v[2] * (T(6) + T(3) * kin.v[4]) + kin.v[3] * (T(6) + T(3) * kin.v[4])))) +
               kin.v[0] *
                   (kin.v[4] * (T(-2) + T(-9) * kin.v[4]) + kin.v[3] * (-(kin.v[3] * prod_pow(kin.v[4], 2)) + T(-8) + kin.v[4] * (T(-12) + T(-12) * kin.v[4])) +
                    kin.v[2] * (T(-8) + T(-4) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(2) * kin.v[4] + kin.v[3] * (T(2) + kin.v[4])) +
                                kin.v[3] * (T(12) + (-kin.v[4] + T(10)) * kin.v[4] + kin.v[3] * (T(2) + kin.v[4]))) +
                    kin.v[0] * (T(2) + T(2) * kin.v[3] * prod_pow(kin.v[4], 2) + kin.v[4] * (T(3) + T(6) * kin.v[4]) +
                                kin.v[2] * ((-kin.v[4] + T(-2)) * kin.v[3] + kin.v[4] * (T(-2) + kin.v[4])) +
                                kin.v[1] * (T(3) + kin.v[1] * (T(6) + T(2) * kin.v[2] + kin.v[3]) + T(-8) * kin.v[4] +
                                            kin.v[2] * (-kin.v[3] + T(-5) * kin.v[4]) + kin.v[3] * (T(-2) + T(-5) * kin.v[4])) +
                                kin.v[0] * (-prod_pow(kin.v[4], 2) + kin.v[1] * (-kin.v[1] + T(2) * kin.v[4]))) +
                    kin.v[1] *
                        (T(-2) + kin.v[4] * (T(18) + T(2) * kin.v[4]) +
                         kin.v[1] * (T(-9) + T(2) * kin.v[4] + kin.v[3] * (T(-4) + kin.v[4]) + kin.v[2] * (-kin.v[2] + -kin.v[3] + T(-12) + T(-3) * kin.v[4])) +
                         kin.v[3] * (kin.v[4] * (T(10) + T(-3) * kin.v[4]) + kin.v[3] * (T(2) + T(3) * kin.v[4])) +
                         kin.v[2] * (T(-12) + kin.v[4] * (T(10) + kin.v[4]) + kin.v[2] * (kin.v[3] + T(3) * kin.v[4]) +
                                     kin.v[3] * (T(10) + kin.v[3] + T(10) * kin.v[4]))));
        c[1] = kin.v[4] * (T(-12) + T(-18) * kin.v[4]) +
               kin.v[3] * (T(-12) + kin.v[4] * (T(-18) + T(-18) * kin.v[4]) + kin.v[3] * (T(-18) + kin.v[4] * (T(-18) + T(-6) * kin.v[4]))) +
               kin.v[2] * (T(-12) + T(18) * kin.v[4] + kin.v[2] * (T(-18) + kin.v[3] * (T(18) + T(6) * kin.v[4])) +
                           kin.v[3] * (T(-6) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(18) + T(6) * kin.v[4]))) +
               kin.v[1] * (T(-12) + T(18) * kin.v[4] + kin.v[3] * (T(18) + T(36) * kin.v[4]) +
                           kin.v[1] * (T(-18) + kin.v[2] * (T(-18) + T(-6) * kin.v[2] + T(-6) * kin.v[3]) + T(6) * kin.v[3] * kin.v[4]) +
                           kin.v[2] * (T(-18) + kin.v[2] * (T(-18) + T(6) * kin.v[3]) + kin.v[4] * (T(36) + T(6) * kin.v[4]) +
                                       kin.v[3] * (T(6) * kin.v[3] + T(6) * kin.v[4]))) +
               kin.v[0] * (kin.v[4] * (T(6) + T(18) * kin.v[4]) +
                           kin.v[2] * (T(24) + T(4) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-24) + T(-2) * kin.v[3] + T(-10) * kin.v[4]) +
                                       kin.v[2] * (T(-2) * kin.v[3] + T(-2) * kin.v[4])) +
                           kin.v[1] * (T(6) + kin.v[2] * (T(24) + T(-10) * kin.v[3] + T(-10) * kin.v[4]) + kin.v[3] * (T(-2) * kin.v[3] + T(-10) * kin.v[4]) +
                                       kin.v[4] * (T(-36) + T(-2) * kin.v[4]) + kin.v[1] * (T(18) + T(12) * kin.v[2] + T(4) * kin.v[3] + T(-2) * kin.v[4])) +
                           kin.v[0] * (T(-6) + kin.v[4] * (T(-6) + T(-6) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[3] + T(2) * kin.v[4]) +
                                       kin.v[1] * (T(-6) + T(-6) * kin.v[1] + T(2) * kin.v[3] + T(8) * kin.v[4])) +
                           kin.v[3] * (T(24) + kin.v[4] * (T(24) + T(12) * kin.v[4])));
        c[2] =
            kin.v[2] * (T(18) + T(-9) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(18) + T(-9) * kin.v[3]) + T(-18) * kin.v[4]) +
            kin.v[1] * (T(18) + kin.v[1] * (T(18) + T(9) * kin.v[2]) + T(-18) * kin.v[4] + kin.v[3] * (T(-18) + T(-18) * kin.v[4]) +
                        kin.v[2] * (T(18) + T(9) * kin.v[2] + T(-18) * kin.v[4])) +
            kin.v[4] * (T(18) + T(18) * kin.v[4]) + kin.v[3] * (T(18) + kin.v[3] * (T(18) + T(9) * kin.v[4]) + kin.v[4] * (T(18) + T(9) * kin.v[4])) +
            kin.v[0] * (kin.v[2] * (T(-24) + T(12) * kin.v[3]) + kin.v[3] * (T(-24) + T(-12) * kin.v[4]) + kin.v[4] * (T(-6) + T(-9) * kin.v[4]) +
                        kin.v[0] * (T(6) + T(3) * kin.v[1] + T(3) * kin.v[4]) + kin.v[1] * (T(-6) + T(-9) * kin.v[1] + T(-12) * kin.v[2] + T(18) * kin.v[4]));
        c[3] = kin.v[3] * (T(-12) + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) +
               kin.v[0] * (T(-2) * kin.v[0] + T(2) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3] + T(2) * kin.v[4]) +
               kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(6) * kin.v[4]) +
               kin.v[1] * (T(-12) + T(-6) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]);
        c[4] = T(3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4];
        c[5] = T(3) + T(3) * kin.v[4] + kin.v[2] * (T(3) + T(3) * kin.v[4]) + kin.v[3] * (T(3) + T(3) * kin.v[4]) +
               kin.v[1] * (T(3) + T(3) * kin.v[4] + kin.v[2] * (T(3) + T(3) * kin.v[4]) + kin.v[3] * (T(3) + T(3) * kin.v[4])) +
               kin.v[0] * (T(-4) + (-kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[4] + T(-1)) * kin.v[3] + T(-4) * kin.v[4] +
                           kin.v[1] * (T(-4) + (-kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[4] + T(-1)) * kin.v[3] + T(-4) * kin.v[4]) +
                           kin.v[0] * (T(1) + kin.v[4] + kin.v[1] * (T(1) + kin.v[4])));
        c[6] = T(-12) + kin.v[2] * (T(-9) + T(-6) * kin.v[4]) + kin.v[3] * (T(-9) + T(-6) * kin.v[4]) +
               kin.v[1] * (T(-9) + T(-6) * kin.v[4] + kin.v[2] * (T(-6) + T(-3) * kin.v[4]) + kin.v[3] * (T(-6) + T(-3) * kin.v[4])) + T(-9) * kin.v[4] +
               kin.v[0] * (T(12) + (-kin.v[1] + -kin.v[4] + T(-2)) * kin.v[0] + T(8) * kin.v[4] + kin.v[2] * (T(2) + kin.v[4]) + kin.v[3] * (T(2) + kin.v[4]) +
                           kin.v[1] * (T(8) + kin.v[2] + kin.v[3] + T(4) * kin.v[4]));
        c[7] = T(18) + kin.v[0] * (-kin.v[2] + -kin.v[3] + T(-12) + kin.v[0] + T(-4) * kin.v[1] + T(-4) * kin.v[4]) + T(9) * kin.v[4] +
               kin.v[2] * (T(9) + T(3) * kin.v[4]) + kin.v[3] * (T(9) + T(3) * kin.v[4]) +
               kin.v[1] * (T(9) + T(3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);
        c[8] = T(-12) + T(4) * kin.v[0] + T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4];
    }

    T operator()(T t) const { return (t * (t * (t * (t * c[0] + c[1]) + c[2]) + c[3]) + c[4]) / (t * (t * (t * (t * c[5] + c[6]) + c[7]) + c[8]) + T(3)); }
};

template <typename T> class DLog_W_28 {
    std::array<T, 9> c;

  public:
    DLog_W_28(const Kin<T>& kin) {
        c[0] =
            kin.v[4] * (T(-3) + T(6) * kin.v[4]) + kin.v[3] * (T(-3) + T(9) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) + T(9) * kin.v[4])) +
            kin.v[2] * (T(-3) + T(-6) * kin.v[4] + kin.v[3] * (T(6) + T(-6) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-9) + T(-6) * kin.v[4])) +
                        kin.v[2] * (T(6) + kin.v[3] * (T(-9) + T(6) * kin.v[3] + T(6) * kin.v[4]))) +
            kin.v[1] * (T(6) + T(-6) * kin.v[4] + kin.v[3] * (T(-6) + T(6) * kin.v[3] * kin.v[4] + kin.v[4] * (T(-18) + T(6) * kin.v[4])) +
                        kin.v[1] * (T(6) + kin.v[1] * kin.v[2] * (T(6) + T(3) * kin.v[2]) + T(-6) * kin.v[3] * kin.v[4] +
                                    kin.v[2] * (T(9) + T(-12) * kin.v[4] + kin.v[2] * (T(6) + T(-6) * kin.v[3] + T(-3) * kin.v[4]) +
                                                kin.v[3] * (T(-6) + T(3) * kin.v[4]))) +
                        kin.v[2] * (kin.v[4] * (T(-18) + T(6) * kin.v[4]) + kin.v[3] * (T(-3) * kin.v[3] * kin.v[4] + kin.v[4] * (T(18) + T(-3) * kin.v[4])) +
                                    kin.v[2] * (T(9) + kin.v[3] * (T(-12) + T(3) * kin.v[3] + T(3) * kin.v[4])))) +
            kin.v[0] * (T(3) + kin.v[4] * (T(8) + T(-9) * kin.v[4]) + kin.v[3] * (T(-4) + T(-2) * kin.v[3] * kin.v[4] + kin.v[4] * (T(-6) + T(-2) * kin.v[4])) +
                        kin.v[2] * (T(-4) + T(4) * prod_pow(kin.v[4], 2) + kin.v[2] * ((-kin.v[3] + -kin.v[4]) * kin.v[3] + T(-2) * kin.v[4]) +
                                    kin.v[3] * (T(6) + kin.v[3] * kin.v[4] + kin.v[4] * (T(2) + kin.v[4]))) +
                        kin.v[0] * (T(2) * kin.v[3] * kin.v[4] + kin.v[4] * (T(-3) + T(2) * kin.v[4]) +
                                    kin.v[2] * (-(kin.v[3] * kin.v[4]) + (-kin.v[4] + T(2)) * kin.v[4]) +
                                    kin.v[1] * (T(3) + kin.v[4] * (T(-6) + kin.v[4]) + kin.v[3] * (T(-2) + kin.v[4]) +
                                                kin.v[1] * (-kin.v[3] + T(4) + kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[4]) +
                                                kin.v[2] * (T(-2) + kin.v[3] + T(3) * kin.v[4]))) +
                        kin.v[1] * (T(-4) + kin.v[4] * (T(18) + T(-4) * kin.v[4]) + kin.v[3] * ((-kin.v[4] + T(2)) * kin.v[3] + (-kin.v[4] + T(2)) * kin.v[4]) +
                                    kin.v[2] * (T(-6) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[3] * (T(10) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) +
                                                kin.v[2] * (-kin.v[3] + T(2) + T(-3) * kin.v[4])) +
                                    kin.v[1] * (T(-9) + kin.v[1] * (T(-2) + T(-4) * kin.v[2]) + T(6) * kin.v[4] + kin.v[3] * kin.v[4] +
                                                kin.v[2] * (T(-6) + T(2) * kin.v[2] + T(7) * kin.v[3] + T(5) * kin.v[4]))));
        c[1] = kin.v[4] * (T(12) + T(-18) * kin.v[4]) + kin.v[3] * (T(12) + T(-18) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-18) + T(-18) * kin.v[4])) +
               kin.v[2] * (T(12) + T(18) * kin.v[4] + kin.v[2] * (T(-18) + kin.v[3] * (T(18) + T(-6) * kin.v[3] + T(-6) * kin.v[4])) +
                           kin.v[3] * (T(-18) + T(6) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(18) + T(6) * kin.v[4]))) +
               kin.v[0] * (T(-12) + kin.v[4] * (T(-24) + T(18) * kin.v[4]) +
                           kin.v[2] * (T(12) + T(2) * kin.v[2] * kin.v[4] + T(-4) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-12) + T(-2) * kin.v[4])) +
                           kin.v[3] * (T(12) + T(2) * kin.v[3] * kin.v[4] + kin.v[4] * (T(12) + T(2) * kin.v[4])) +
                           kin.v[1] * (T(12) + kin.v[1] * (T(18) + T(2) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4]) +
                                       kin.v[2] * (T(12) + T(-2) * kin.v[2] + T(-10) * kin.v[3] + T(-2) * kin.v[4]) +
                                       kin.v[3] * (T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[4] * (T(-36) + T(4) * kin.v[4])) +
                           kin.v[0] * (T(-2) * kin.v[2] * kin.v[4] + T(-2) * kin.v[3] * kin.v[4] + kin.v[4] * (T(6) + T(-2) * kin.v[4]) +
                                       kin.v[1] * (T(-6) + T(-4) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(6) * kin.v[4]))) +
               kin.v[1] * (T(-24) + T(18) * kin.v[4] +
                           kin.v[2] * (kin.v[2] * (T(-18) + T(12) * kin.v[3]) + T(-18) * kin.v[3] * kin.v[4] + kin.v[4] * (T(36) + T(-6) * kin.v[4])) +
                           kin.v[3] * (T(18) + T(-6) * kin.v[3] * kin.v[4] + kin.v[4] * (T(36) + T(-6) * kin.v[4])) +
                           kin.v[1] * (T(-18) + T(-6) * kin.v[1] * kin.v[2] + T(6) * kin.v[3] * kin.v[4] +
                                       kin.v[2] * (T(-18) + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(12) * kin.v[4])));
        c[2] = kin.v[2] * (T(-18) + kin.v[2] * (T(18) + T(-9) * kin.v[3]) + kin.v[3] * (T(18) + T(-9) * kin.v[3]) + T(-18) * kin.v[4]) +
               kin.v[1] * (T(36) + kin.v[1] * (T(18) + T(9) * kin.v[2]) + T(-18) * kin.v[4] + kin.v[3] * (T(-18) + T(-18) * kin.v[4]) +
                           kin.v[2] * (T(9) * kin.v[2] + T(-18) * kin.v[4])) +
               kin.v[4] * (T(-18) + T(18) * kin.v[4]) + kin.v[3] * (T(-18) + T(9) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(18) + T(9) * kin.v[4])) +
               kin.v[0] * (T(18) + kin.v[2] * (T(-12) + T(6) * kin.v[3]) + kin.v[4] * (T(24) + T(-9) * kin.v[4]) + kin.v[3] * (T(-12) + T(-6) * kin.v[4]) +
                           kin.v[0] * (T(3) * kin.v[1] + T(-3) * kin.v[4]) + kin.v[1] * (T(-12) + T(-9) * kin.v[1] + T(-6) * kin.v[2] + T(18) * kin.v[4]));
        c[3] = kin.v[3] * (T(12) + T(-6) * kin.v[3]) + kin.v[0] * (T(-12) + T(4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3] + T(-8) * kin.v[4]) +
               kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) +
               kin.v[1] * (T(-24) + T(-6) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4]);
        c[4] = T(3) * kin.v[0] + T(6) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4];
        c[5] = T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4] + kin.v[2] * (T(-3) + T(3) * kin.v[3] + T(3) * kin.v[4]) +
               kin.v[0] * (T(-1) + (-kin.v[3] + -kin.v[4] + T(1)) * kin.v[2] + kin.v[3] + kin.v[4] +
                           kin.v[1] * (T(-2) + (-kin.v[3] + -kin.v[4] + T(2)) * kin.v[2] + kin.v[1] * (T(-1) + kin.v[2]) + kin.v[3] + kin.v[4])) +
               kin.v[1] *
                   (T(6) + kin.v[1] * (T(3) + T(-3) * kin.v[2]) + T(-3) * kin.v[3] + T(-3) * kin.v[4] + kin.v[2] * (T(-6) + T(3) * kin.v[3] + T(3) * kin.v[4]));
        c[6] = T(-12) + T(9) * kin.v[3] + kin.v[2] * (T(9) + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + T(9) * kin.v[4] +
               kin.v[1] * (T(-18) + kin.v[1] * (T(-6) + T(3) * kin.v[2]) + T(6) * kin.v[3] + T(6) * kin.v[4] +
                           kin.v[2] * (T(12) + T(-3) * kin.v[3] + T(-3) * kin.v[4])) +
               kin.v[0] * (T(3) + kin.v[1] * (-kin.v[3] + -kin.v[4] + T(4) + kin.v[1] + T(-2) * kin.v[2]) + T(-2) * kin.v[3] + T(-2) * kin.v[4] +
                           kin.v[2] * (T(-2) + kin.v[3] + kin.v[4]));
        c[7] = T(18) + T(-9) * kin.v[3] + kin.v[1] * (T(18) + T(3) * kin.v[1] + T(-6) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + T(-9) * kin.v[4] +
               kin.v[0] * (T(-3) + T(-2) * kin.v[1] + kin.v[2] + kin.v[3] + kin.v[4]) + kin.v[2] * (T(-9) + T(3) * kin.v[3] + T(3) * kin.v[4]);
        c[8] = T(-12) + kin.v[0] + T(-6) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4];
    }

    T operator()(T t) const { return (t * (t * (t * (t * c[0] + c[1]) + c[2]) + c[3]) + c[4]) / (t * (t * (t * (t * c[5] + c[6]) + c[7]) + c[8]) + T(3)); }
};

template <typename T> class DLog_W_29 {
    std::array<T, 9> c;

  public:
    DLog_W_29(const Kin<T>& kin) {
        c[0] =
            kin.v[4] * (T(3) + T(-2) * kin.v[4]) +
            kin.v[3] * (T(3) + kin.v[4] * (T(2) + T(-3) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[4] * (T(-3) + T(2) * kin.v[4]))) +
            kin.v[2] *
                (T(2) * kin.v[4] + kin.v[3] * (T(-4) + (-prod_pow(kin.v[4], 2) + T(3)) * kin.v[3] + T(4) * prod_pow(kin.v[4], 2)) +
                 kin.v[2] * (T(-2) + (-kin.v[3] + T(2)) * kin.v[2] * kin.v[3] + kin.v[3] * (T(3) + T(-6) * kin.v[4] + kin.v[3] * (T(-2) + T(2) * kin.v[4])))) +
            kin.v[0] *
                (T(-1) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[3] * (T(2) * kin.v[3] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2)) +
                 kin.v[0] * (T(3) * kin.v[4] + T(-2) * kin.v[3] * kin.v[4] +
                             kin.v[1] * (T(-3) + (-kin.v[3] + -kin.v[4] + T(2)) * kin.v[2] + (-kin.v[4] + T(2)) * kin.v[3] +
                                         kin.v[1] * (T(-2) + kin.v[2] + kin.v[3]) + T(2) * kin.v[4]) +
                             kin.v[2] * (T(-2) * kin.v[4] + kin.v[3] * kin.v[4])) +
                 kin.v[2] * (T(2) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[2] * (kin.v[3] * (-kin.v[4] + T(-2) + kin.v[3]) + T(2) * kin.v[4]) +
                             kin.v[3] * (-(kin.v[3] * kin.v[4]) + kin.v[4] * (T(4) + kin.v[4]))) +
                 kin.v[1] * (T(4) + kin.v[4] * (T(-6) + T(2) * kin.v[4]) + kin.v[3] * (-prod_pow(kin.v[4], 2) + kin.v[3] * (T(-2) + kin.v[4])) +
                             kin.v[1] * (T(3) + T(-2) * kin.v[4] + kin.v[3] * kin.v[4] + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(-5) * kin.v[3] + kin.v[4])) +
                             kin.v[2] * (-prod_pow(kin.v[4], 2) + T(6) + kin.v[2] * (T(-4) + kin.v[3] + kin.v[4]) +
                                         kin.v[3] * (T(-8) + T(3) * kin.v[3] + T(4) * kin.v[4])))) +
            kin.v[1] * (T(-3) + T(2) * kin.v[4] + kin.v[3] * (T(2) + kin.v[3] * kin.v[4] * (T(-4) + kin.v[4]) + kin.v[4] * (T(6) + T(-4) * kin.v[4])) +
                        kin.v[1] * (T(-2) + T(2) * kin.v[3] * kin.v[4] +
                                    kin.v[2] * (T(-3) + kin.v[2] * (-kin.v[4] + T(-2) + kin.v[2] + T(4) * kin.v[3]) + T(4) * kin.v[4] +
                                                kin.v[3] * (T(2) + T(-5) * kin.v[4]))) +
                        kin.v[2] * (T(-4) + kin.v[4] * (T(6) + T(-2) * kin.v[4]) +
                                    kin.v[2] * (T(-3) + T(2) * kin.v[2] + kin.v[3] * (T(8) + T(-4) * kin.v[3] + T(-5) * kin.v[4])) +
                                    kin.v[3] * (kin.v[3] * (T(2) + T(3) * kin.v[4]) + kin.v[4] * (T(-8) + T(5) * kin.v[4]))));
        c[1] = kin.v[4] * (T(-12) + T(6) * kin.v[4]) +
               kin.v[3] * (T(-12) + kin.v[4] * (T(-6) + T(6) * kin.v[4]) + kin.v[3] * (T(6) + kin.v[4] * (T(6) + T(-2) * kin.v[4]))) +
               kin.v[0] * (T(4) + kin.v[4] * (T(18) + T(-6) * kin.v[4]) + kin.v[3] * (T(-2) * kin.v[3] * kin.v[4] + T(2) * prod_pow(kin.v[4], 2)) +
                           kin.v[0] * (T(-6) * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] +
                                       kin.v[1] * (T(6) + T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) +
                           kin.v[2] * (T(-6) + T(-4) * kin.v[3] * kin.v[4] + T(2) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(2) * kin.v[3] + T(-2) * kin.v[4])) +
                           kin.v[1] * (T(-12) + T(2) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(-12) + T(4) * kin.v[2] + T(8) * kin.v[3]) +
                                       kin.v[4] * (T(12) + T(-2) * kin.v[4]) + kin.v[1] * (T(-6) + T(-4) * kin.v[2] + T(2) * kin.v[4]))) +
               kin.v[2] * (T(-6) * kin.v[4] + kin.v[3] * (T(12) + T(-6) * kin.v[3] + T(-4) * prod_pow(kin.v[4], 2)) +
                           kin.v[2] * (T(6) + T(-2) * kin.v[2] * kin.v[3] + kin.v[3] * (T(-6) + T(2) * kin.v[3] + T(6) * kin.v[4]))) +
               kin.v[1] * (T(12) + T(-6) * kin.v[4] +
                           kin.v[1] * (T(6) + T(-2) * kin.v[3] * kin.v[4] + kin.v[2] * (T(6) + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-4) * kin.v[4])) +
                           kin.v[3] * (T(-6) + T(4) * kin.v[3] * kin.v[4] + kin.v[4] * (T(-12) + T(4) * kin.v[4])) +
                           kin.v[2] * (T(12) + kin.v[2] * (T(6) + T(-2) * kin.v[2] + T(-8) * kin.v[3]) + kin.v[4] * (T(-12) + T(2) * kin.v[4]) +
                                       kin.v[3] * (T(-2) * kin.v[3] + T(8) * kin.v[4])));
        c[2] = kin.v[4] * (T(18) + T(-6) * kin.v[4]) +
               kin.v[2] * (kin.v[3] * (T(-12) + T(3) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[3]) + T(6) * kin.v[4]) +
               kin.v[3] * (T(18) + kin.v[3] * (T(-6) + T(-3) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4])) +
               kin.v[0] * (T(-6) + T(6) * kin.v[2] + kin.v[1] * (T(12) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4]) +
                           kin.v[4] * (T(-18) + T(3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[1] + T(3) * kin.v[4])) +
               kin.v[1] * (T(-18) + kin.v[1] * (T(-6) + T(-3) * kin.v[2]) + T(6) * kin.v[4] + kin.v[3] * (T(6) + T(6) * kin.v[4]) +
                           kin.v[2] * (T(-12) + T(-3) * kin.v[2] + T(6) * kin.v[4]));
        c[3] = kin.v[1] * (T(12) + T(2) * kin.v[1] + T(4) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) +
               kin.v[3] * (T(-12) + T(2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[2] + T(4) * kin.v[3] + T(-2) * kin.v[4]) +
               kin.v[4] * (T(-12) + T(2) * kin.v[4]) + kin.v[0] * (T(4) + T(-4) * kin.v[1] + T(-2) * kin.v[2] + T(6) * kin.v[4]);
        c[4] = -kin.v[0] + T(-3) * kin.v[1] + T(3) * kin.v[3] + T(3) * kin.v[4];
        c[5] = T(1) + kin.v[0] * (T(-1) + (-kin.v[3] + T(1)) * kin.v[2] + kin.v[3] + kin.v[1] * (T(-1) + (-kin.v[3] + T(1)) * kin.v[2] + kin.v[3])) +
               kin.v[3] * (T(-1) + kin.v[4]) + -kin.v[4] + kin.v[2] * (-(kin.v[3] * kin.v[4]) + kin.v[2] * (T(-1) + kin.v[3]) + kin.v[4]) +
               kin.v[1] * (-kin.v[4] + T(1) + kin.v[3] * (T(-1) + kin.v[4]) + kin.v[2] * (-(kin.v[3] * kin.v[4]) + kin.v[2] * (T(-1) + kin.v[3]) + kin.v[4]));
        c[6] = T(-4) + kin.v[0] * (T(3) + (-kin.v[2] + -kin.v[3] + T(2)) * kin.v[1] + T(-2) * kin.v[3] + kin.v[2] * (T(-2) + kin.v[3])) +
               kin.v[3] * (T(3) + T(-2) * kin.v[4]) + T(3) * kin.v[4] +
               kin.v[1] * (T(-3) + kin.v[2] * (-kin.v[4] + kin.v[2]) + (-kin.v[4] + T(2)) * kin.v[3] + T(2) * kin.v[4]) +
               kin.v[2] * ((-kin.v[3] + T(2)) * kin.v[2] + T(-2) * kin.v[4] + kin.v[3] * kin.v[4]);
        c[7] = T(6) + kin.v[0] * (-kin.v[1] + T(-3) + kin.v[2] + kin.v[3]) + (-kin.v[3] + -kin.v[4] + T(3)) * kin.v[1] + kin.v[3] * (T(-3) + kin.v[4]) +
               T(-3) * kin.v[4] + kin.v[2] * (-kin.v[2] + kin.v[4]);
        c[8] = T(-4) + kin.v[0] + -kin.v[1] + kin.v[3] + kin.v[4];
    }

    T operator()(T t) const { return (t * (t * (t * (t * c[0] + c[1]) + c[2]) + c[3]) + c[4]) / (t * (t * (t * (t * c[5] + c[6]) + c[7]) + c[8]) + T(1)); }
};

template <typename T> class DLog_W_30 {
    std::array<T, 9> c;

  public:
    DLog_W_30(const Kin<T>& kin) {
        c[0] =
            kin.v[4] * (T(3) + T(2) * kin.v[4]) +
            kin.v[3] *
                (kin.v[4] * (T(4) + T(3) * kin.v[4]) + kin.v[3] * (T(2) + (-kin.v[4] + T(-2)) * kin.v[3] * kin.v[4] + kin.v[4] * (T(3) + T(2) * kin.v[4]))) +
            kin.v[2] * (T(-3) + T(-2) * kin.v[4] +
                        kin.v[3] * (T(4) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-3) + T(-2) * kin.v[3] + kin.v[4] * (T(-8) + T(-4) * kin.v[4]))) +
                        kin.v[2] * (T(2) + kin.v[3] * (T(-3) + T(-2) * kin.v[4] + kin.v[3] * (T(2) + kin.v[3] + T(4) * kin.v[4])))) +
            kin.v[0] * (T(1) + kin.v[4] * (T(-4) + T(-3) * kin.v[4]) +
                        kin.v[3] * (T(-2) + kin.v[4] * (T(-6) + T(-4) * kin.v[4]) + kin.v[3] * kin.v[4] * (T(4) + T(2) * kin.v[4])) +
                        kin.v[0] * ((-kin.v[4] + T(-2)) * kin.v[3] * kin.v[4] + kin.v[4] * (T(3) + T(2) * kin.v[4]) +
                                    kin.v[2] * ((-kin.v[4] + T(-2)) * kin.v[4] + kin.v[3] * kin.v[4]) +
                                    kin.v[1] * (T(-3) + T(-2) * kin.v[4] + kin.v[3] * (T(2) + kin.v[4]) + kin.v[2] * (-kin.v[3] + T(2) + kin.v[4]))) +
                        kin.v[1] * (T(6) + kin.v[3] * (-prod_pow(kin.v[4], 2) + (-kin.v[4] + T(-2)) * kin.v[3]) + kin.v[4] * (T(6) + T(2) * kin.v[4]) +
                                    kin.v[1] * (T(-3) + T(-2) * kin.v[4] + kin.v[3] * (T(2) + kin.v[4]) + kin.v[2] * (-kin.v[3] + T(2) + kin.v[4])) +
                                    kin.v[2] * (-prod_pow(kin.v[4], 2) + kin.v[2] * (-kin.v[4] + T(-2) + kin.v[3]) +
                                                kin.v[3] * (T(-4) + kin.v[3] + T(-4) * kin.v[4]))) +
                        kin.v[2] * (kin.v[2] * (T(2) * kin.v[4] + kin.v[3] * (-kin.v[3] + T(-3) * kin.v[4])) +
                                    kin.v[3] * ((-kin.v[4] + T(2)) * kin.v[3] + kin.v[4] * (T(8) + T(5) * kin.v[4])))) +
            kin.v[1] * (T(-3) + T(-2) * kin.v[4] + kin.v[3] * (T(-2) + kin.v[3] * prod_pow(kin.v[4], 2) + kin.v[4] * (T(-6) + T(-4) * kin.v[4])) +
                        kin.v[1] * (T(2) + T(2) * kin.v[3] * kin.v[4] +
                                    kin.v[2] * (T(3) + kin.v[2] * (-kin.v[4] + T(-2) + kin.v[3]) + T(4) * kin.v[4] + kin.v[3] * (T(-4) + T(-5) * kin.v[4]))) +
                        kin.v[2] * (T(-2) + kin.v[4] * (T(-6) + T(-2) * kin.v[4]) +
                                    kin.v[2] * (T(3) + T(4) * kin.v[4] + kin.v[3] * (T(-2) * kin.v[3] + T(-3) * kin.v[4])) +
                                    kin.v[3] * (kin.v[3] * (T(6) + T(5) * kin.v[4]) + kin.v[4] * (T(8) + T(5) * kin.v[4]))));
        c[1] = kin.v[4] * (T(-12) + T(-6) * kin.v[4]) +
               kin.v[3] * (kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[3] * (T(-6) + T(2) * kin.v[3] * kin.v[4] + kin.v[4] * (T(-6) + T(-2) * kin.v[4]))) +
               kin.v[1] * (T(12) + T(6) * kin.v[4] +
                           kin.v[1] * (T(-6) + T(-2) * kin.v[3] * kin.v[4] + kin.v[2] * (T(-6) + T(2) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4])) +
                           kin.v[2] * (T(6) + kin.v[3] * (T(-6) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (T(-6) + T(-4) * kin.v[4]) +
                                       kin.v[4] * (T(12) + T(2) * kin.v[4])) +
                           kin.v[3] * (T(6) + kin.v[4] * (T(12) + T(4) * kin.v[4]))) +
               kin.v[0] *
                   (T(-4) + kin.v[4] * (T(12) + T(6) * kin.v[4]) + kin.v[2] * (T(-2) * kin.v[2] * kin.v[4] + kin.v[3] * (T(-2) * kin.v[3] + T(-8) * kin.v[4])) +
                    kin.v[1] * (T(-18) + T(2) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(2) * kin.v[2] + T(4) * kin.v[3]) +
                                kin.v[4] * (T(-12) + T(-2) * kin.v[4]) + kin.v[1] * (T(6) + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) +
                    kin.v[0] * (T(2) * kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + kin.v[4] * (T(-6) + T(-2) * kin.v[4]) +
                                kin.v[1] * (T(6) + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) +
                    kin.v[3] * (T(6) + T(-4) * kin.v[3] * kin.v[4] + kin.v[4] * (T(12) + T(4) * kin.v[4]))) +
               kin.v[2] * (T(12) + T(6) * kin.v[4] + kin.v[2] * (T(-6) + kin.v[3] * (T(6) + T(-2) * kin.v[3] + T(2) * kin.v[4])) +
                           kin.v[3] * (T(-12) + T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) + T(2) * kin.v[3] + T(8) * kin.v[4])));
        c[2] = kin.v[2] * (T(-18) + kin.v[2] * (T(6) + T(-3) * kin.v[3]) + kin.v[3] * (T(12) + T(-3) * kin.v[3]) + T(-6) * kin.v[4]) +
               kin.v[1] * (T(-18) + kin.v[1] * (T(6) + T(3) * kin.v[2]) + T(-6) * kin.v[4] + kin.v[3] * (T(-6) + T(-6) * kin.v[4]) +
                           kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4])) +
               kin.v[4] * (T(18) + T(6) * kin.v[4]) + kin.v[3] * (kin.v[3] * (T(6) + T(3) * kin.v[4]) + kin.v[4] * (T(12) + T(3) * kin.v[4])) +
               kin.v[0] * (T(6) + kin.v[3] * (T(-6) + T(-6) * kin.v[4]) + kin.v[4] * (T(-12) + T(-3) * kin.v[4]) +
                           kin.v[0] * (T(-3) * kin.v[1] + T(3) * kin.v[4]) + kin.v[1] * (T(18) + T(-3) * kin.v[1] + T(6) * kin.v[4]));
        c[3] = kin.v[3] * (T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(-12) + T(-2) * kin.v[4]) +
               kin.v[2] * (T(12) + T(-2) * kin.v[2] + T(-4) * kin.v[3] + T(2) * kin.v[4]) +
               kin.v[1] * (T(12) + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) +
               kin.v[0] * (T(-4) + T(-6) * kin.v[1] + T(2) * kin.v[3] + T(4) * kin.v[4]);
        c[4] = kin.v[0] + T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[4];
        c[5] = T(-1) + -kin.v[4] + prod_pow(kin.v[3], 2) * (T(1) + kin.v[4]) + kin.v[2] * (T(1) + (-kin.v[4] + T(-1)) * prod_pow(kin.v[3], 2) + kin.v[4]) +
               kin.v[0] * (T(1) + (-kin.v[4] + T(-1)) * kin.v[3] + kin.v[4] + kin.v[2] * (-kin.v[4] + T(-1) + kin.v[3] * (T(1) + kin.v[4]))) +
               kin.v[1] * (T(1) + (-kin.v[4] + T(-1)) * kin.v[3] + kin.v[4] + kin.v[2] * (-kin.v[4] + T(-1) + kin.v[3] * (T(1) + kin.v[4])));
        c[6] = T(4) + kin.v[2] * (T(-3) + prod_pow(kin.v[3], 2) + T(-2) * kin.v[4]) + (-kin.v[4] + T(-2)) * prod_pow(kin.v[3], 2) + T(3) * kin.v[4] +
               kin.v[0] * (T(-3) + T(-2) * kin.v[4] + kin.v[3] * (T(2) + kin.v[4]) + kin.v[2] * (-kin.v[3] + T(2) + kin.v[4])) +
               kin.v[1] * (T(-3) + T(-2) * kin.v[4] + kin.v[3] * (T(2) + kin.v[4]) + kin.v[2] * (-kin.v[3] + T(2) + kin.v[4]));
        c[7] = T(-6) + prod_pow(kin.v[3], 2) + T(-3) * kin.v[4] + kin.v[2] * (T(3) + kin.v[4]) + kin.v[0] * (-kin.v[2] + -kin.v[3] + T(3) + kin.v[4]) +
               kin.v[1] * (-kin.v[2] + -kin.v[3] + T(3) + kin.v[4]);
        c[8] = T(4) + -kin.v[0] + -kin.v[1] + -kin.v[2] + kin.v[4];
    }

    T operator()(T t) const { return (t * (t * (t * (t * c[0] + c[1]) + c[2]) + c[3]) + c[4]) / (t * (t * (t * (t * c[5] + c[6]) + c[7]) + c[8]) + T(-1)); }
};

template <typename T> class DLog_W_31 {
    std::array<T, 8> c;

  public:
    DLog_W_31(const Kin<T>& kin) {
        c[0] = T(-6) +
               kin.v[2] *
                   (kin.v[2] * (T(8) + kin.v[3] * (T(-8) + T(2) * kin.v[3])) + T(-8) * kin.v[4] + kin.v[3] * (T(4) + kin.v[3] * (T(-8) + T(-4) * kin.v[4]))) +
               T(8) * prod_pow(kin.v[4], 2) +
               kin.v[1] * (kin.v[1] * (T(8) + kin.v[2] * (T(8) + T(2) * kin.v[2])) + T(-8) * kin.v[4] + kin.v[3] * (T(-8) + T(-16) * kin.v[4]) +
                           kin.v[2] * (T(4) + kin.v[2] * (T(8) + T(-4) * kin.v[3]) + T(-16) * kin.v[4] + T(4) * kin.v[3] * kin.v[4])) +
               kin.v[3] * (kin.v[4] * (T(4) + T(8) * kin.v[4]) + kin.v[3] * (T(8) + kin.v[4] * (T(8) + T(2) * kin.v[4]))) +
               kin.v[0] * (T(8) + kin.v[4] * (T(4) + T(-8) * kin.v[4]) + kin.v[3] * (T(-8) + kin.v[4] * (T(-8) + T(-4) * kin.v[4])) +
                           kin.v[0] * (T(2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(2) * kin.v[1] + T(-4) * kin.v[4])) +
                           kin.v[2] * (T(-8) + kin.v[3] * (T(8) + T(4) * kin.v[4])) +
                           kin.v[1] * (T(4) + kin.v[1] * (T(-8) + T(-4) * kin.v[2]) + T(16) * kin.v[4] + T(4) * kin.v[3] * kin.v[4] +
                                       kin.v[2] * (T(-8) + T(4) * kin.v[3] + T(4) * kin.v[4])));
        c[1] = T(18) + T(-12) * prod_pow(kin.v[4], 2) +
               kin.v[2] * (kin.v[2] * (T(-12) + T(6) * kin.v[3]) + kin.v[3] * (T(-6) + T(6) * kin.v[3]) + T(12) * kin.v[4]) +
               kin.v[3] * (kin.v[3] * (T(-12) + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(-6) * kin.v[4])) +
               kin.v[0] * (T(-18) + kin.v[2] * (T(12) + T(-6) * kin.v[3]) + kin.v[1] * (T(-6) + T(6) * kin.v[1] + T(6) * kin.v[2] + T(-12) * kin.v[4]) +
                           kin.v[4] * (T(-6) + T(6) * kin.v[4]) + kin.v[3] * (T(12) + T(6) * kin.v[4])) +
               kin.v[1] * (kin.v[1] * (T(-12) + T(-6) * kin.v[2]) + T(12) * kin.v[4] + kin.v[3] * (T(12) + T(12) * kin.v[4]) +
                           kin.v[2] * (T(-6) + T(-6) * kin.v[2] + T(12) * kin.v[4]));
        c[2] = T(-18) + kin.v[1] * (T(4) * kin.v[1] + T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) +
               kin.v[2] * (T(4) * kin.v[2] + T(2) * kin.v[3] + T(-4) * kin.v[4]) + T(4) * prod_pow(kin.v[4], 2) +
               kin.v[0] * (T(12) + T(2) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (T(4) * kin.v[3] + T(2) * kin.v[4]);
        c[3] = T(6) + T(-2) * kin.v[0];
        c[4] = T(-3) +
               kin.v[2] * (kin.v[2] * (T(4) + kin.v[3] * (T(-4) + kin.v[3])) + T(-4) * kin.v[4] + kin.v[3] * (T(2) + kin.v[3] * (T(-4) + T(-2) * kin.v[4]))) +
               T(4) * prod_pow(kin.v[4], 2) +
               kin.v[1] * (kin.v[1] * (T(4) + kin.v[2] * (T(4) + kin.v[2])) + T(-4) * kin.v[4] + kin.v[3] * (T(-4) + T(-8) * kin.v[4]) +
                           kin.v[2] * (T(2) + kin.v[2] * (T(4) + T(-2) * kin.v[3]) + T(-8) * kin.v[4] + T(2) * kin.v[3] * kin.v[4])) +
               kin.v[3] * (kin.v[4] * (T(2) + T(4) * kin.v[4]) + kin.v[3] * (T(4) + kin.v[4] * (T(4) + kin.v[4]))) +
               kin.v[0] *
                   (T(4) + kin.v[4] * (T(2) + T(-4) * kin.v[4]) + kin.v[3] * (T(-4) + kin.v[4] * (T(-4) + T(-2) * kin.v[4])) +
                    kin.v[0] * (prod_pow(kin.v[4], 2) + kin.v[1] * (kin.v[1] + T(-2) * kin.v[4])) + kin.v[2] * (T(-4) + kin.v[3] * (T(4) + T(2) * kin.v[4])) +
                    kin.v[1] * (T(2) + kin.v[1] * (T(-4) + T(-2) * kin.v[2]) + T(8) * kin.v[4] + T(2) * kin.v[3] * kin.v[4] +
                                kin.v[2] * (T(-4) + T(2) * kin.v[3] + T(2) * kin.v[4])));
        c[5] = T(12) + T(-8) * prod_pow(kin.v[4], 2) +
               kin.v[2] * (kin.v[2] * (T(-8) + T(4) * kin.v[3]) + kin.v[3] * (T(-4) + T(4) * kin.v[3]) + T(8) * kin.v[4]) +
               kin.v[3] * (kin.v[3] * (T(-8) + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(-4) * kin.v[4])) +
               kin.v[0] * (T(-12) + kin.v[2] * (T(8) + T(-4) * kin.v[3]) + kin.v[1] * (T(-4) + T(4) * kin.v[1] + T(4) * kin.v[2] + T(-8) * kin.v[4]) +
                           kin.v[4] * (T(-4) + T(4) * kin.v[4]) + kin.v[3] * (T(8) + T(4) * kin.v[4])) +
               kin.v[1] * (kin.v[1] * (T(-8) + T(-4) * kin.v[2]) + T(8) * kin.v[4] + kin.v[3] * (T(8) + T(8) * kin.v[4]) +
                           kin.v[2] * (T(-4) + T(-4) * kin.v[2] + T(8) * kin.v[4]));
        c[6] = T(-18) + kin.v[1] * (T(4) * kin.v[1] + T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) +
               kin.v[2] * (T(4) * kin.v[2] + T(2) * kin.v[3] + T(-4) * kin.v[4]) + T(4) * prod_pow(kin.v[4], 2) +
               kin.v[0] * (T(12) + T(2) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (T(4) * kin.v[3] + T(2) * kin.v[4]);
        c[7] = T(12) + T(-4) * kin.v[0];
    }

    T operator()(T t) const { return (t * (t * (t * c[0] + c[1]) + c[2]) + c[3]) / (t * (t * (t * (t * c[4] + c[5]) + c[6]) + c[7]) + T(-3)); }
};

#endif // PENTAGON_FUNCTIONS_M0_ENABLED


} // namespace PentagonFunctions
