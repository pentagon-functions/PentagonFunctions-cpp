#include "f_4_130.h"

namespace PentagonFunctions {

template <typename T> T f_4_130_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_130_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_130_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(16) * kin.v[0]) / T(3) + (bc<T>[1] * T(-2) + T(2)) * kin.v[1] + T(-4) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[1] * (T(4) + T(-4) * kin.v[0] + T(4) * kin.v[3])) + ((T(16) * kin.v[2]) / T(3) + (T(-16) * kin.v[3]) / T(3) + (T(-16) * kin.v[4]) / T(3)) * kin.v[0] + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(8) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[1] * (T(4) + T(-4) * kin.v[0] + T(-2) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[1] * ((T(16) * kin.v[0]) / T(3) + T(2) * kin.v[1] + T(-4) * kin.v[2] + T(-8) * kin.v[3]) + ((T(16) * kin.v[2]) / T(3) + (T(-16) * kin.v[3]) / T(3) + (T(-16) * kin.v[4]) / T(3)) * kin.v[0] + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(8) * kin.v[4]) + rlog(kin.v[2]) * (prod_pow(kin.v[4], 2) / T(2) + ((T(-4) * kin.v[2]) / T(3) + (T(4) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(2) + -kin.v[4]) * kin.v[3] + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[0]) + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (prod_pow(kin.v[4], 2) / T(2) + ((T(-4) * kin.v[2]) / T(3) + (T(4) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(2) + -kin.v[4]) * kin.v[3] + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[0]) + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(kin.v[3]) * (prod_pow(kin.v[4], 2) / T(2) + ((T(-4) * kin.v[2]) / T(3) + (T(4) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(2) + -kin.v[4]) * kin.v[3] + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[0]) + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + ((T(-4) * kin.v[2]) / T(3) + (T(4) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(2) + -kin.v[4]) * kin.v[3] + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[0]) + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(kin.v[0]) * (((T(-8) * kin.v[2]) / T(3) + (T(8) * kin.v[3]) / T(3) + (T(8) * kin.v[4]) / T(3)) * kin.v[0] + kin.v[1] * ((T(-8) * kin.v[0]) / T(3) + (bc<T>[1] + T(-1)) * kin.v[1] + T(2) * kin.v[2] + T(4) * kin.v[3] + bc<T>[1] * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[3])) + prod_pow(kin.v[4], 2) + kin.v[2] * (T(3) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(-3) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + bc<T>[0] * int_to_imaginary<T>(1) * (-prod_pow(kin.v[4], 2) + ((T(8) * kin.v[2]) / T(3) + (T(-8) * kin.v[3]) / T(3) + (T(-8) * kin.v[4]) / T(3)) * kin.v[0] + kin.v[1] * ((T(8) * kin.v[0]) / T(3) + kin.v[1] + T(-2) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[3] * (T(3) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (T(-3) * kin.v[2] + T(4) * kin.v[4]) + bc<T>[1] * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]))) + bc<T>[1] * (((T(16) * kin.v[2]) / T(3) + (T(-16) * kin.v[3]) / T(3) + (T(-16) * kin.v[4]) / T(3)) * kin.v[0] + kin.v[1] * ((T(16) * kin.v[0]) / T(3) + T(2) * kin.v[1] + T(-4) * kin.v[2] + T(-8) * kin.v[3]) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(8) * kin.v[4]) + bc<T>[1] * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[1] * (T(4) * kin.v[0] + ((bc<T>[1] * T(-3)) / T(2) + T(3) / T(2)) * kin.v[1] + T(-3) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[1] * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(3) * kin.v[4]) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(6) * kin.v[4]) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[1] * (T(4) * kin.v[0] + ((bc<T>[1] * T(-3)) / T(2) + T(3) / T(2)) * kin.v[1] + T(-3) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[1] * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(3) * kin.v[4]) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(6) * kin.v[4]) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + bc<T>[1] * T(4) + T(8)) * kin.v[1] + T(8) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + bc<T>[1] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(-8) * kin.v[4];
c[3] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[1] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[1] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4])) + bc<T>[1] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * (T(2) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(kin.v[2]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(kin.v[3]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(kin.v[0]) * ((T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[1] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[21] * (abb[22] * (abb[23] * T(-4) + abb[3] * abb[18] * T(-2) + abb[4] * abb[18] * T(-2) + abb[18] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[18] * T(2))) + abb[3] * abb[18] * (-abb[1] + -abb[8] + -abb[9] + -abb[10] + abb[24] * T(-4) + abb[7] * T(3) + abb[11] * T(3)) + abb[4] * abb[18] * (-abb[1] + -abb[8] + -abb[9] + -abb[10] + abb[24] * T(-4) + abb[7] * T(3) + abb[11] * T(3)) + abb[18] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[24] * bc<T>[0] * int_to_imaginary<T>(4) + abb[18] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[24] * T(4))) + abb[2] * (abb[18] * abb[22] * T(2) + abb[20] * (-abb[1] + -abb[8] + -abb[9] + -abb[10] + abb[24] * T(-4) + abb[22] * T(-2) + abb[7] * T(3) + abb[11] * T(3)) + abb[18] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[24] * T(4))) + abb[23] * (abb[24] * T(-8) + abb[1] * T(-2) + abb[8] * T(-2) + abb[9] * T(-2) + abb[10] * T(-2) + abb[7] * T(6) + abb[11] * T(6)) + abb[20] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(3) + abb[24] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[22] * (bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2) + abb[4] * T(2) + abb[18] * T(2)) + abb[3] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[24] * T(4)) + abb[4] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[24] * T(4)) + abb[18] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[24] * T(4)) + abb[20] * (abb[24] * T(-8) + abb[22] * T(-4) + abb[1] * T(-2) + abb[8] * T(-2) + abb[9] * T(-2) + abb[10] * T(-2) + abb[7] * T(6) + abb[11] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_130_construct (const Kin<T>& kin) {
    return [&kin, 
            dl18 = DLog_W_18<T>(kin),dl27 = DLog_W_27<T>(kin),dl8 = DLog_W_8<T>(kin),dl9 = DLog_W_9<T>(kin),dl22 = DLog_W_22<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),spdl22 = SpDLog_f_4_130_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl18(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), f_2_1_1(kin_path), f_2_1_2(kin_path), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), dl27(t) / kin_path.SqrtDelta, f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl8(t), rlog(-v_path[2] + v_path[0] + v_path[4]), dl9(t), rlog(-v_path[3] + v_path[0] + v_path[1]), dl22(t), rlog(kin.W[0] / kin_path.W[0]), f_2_1_14(kin_path), -rlog(t), dl5(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl2(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl1(t), dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl19(t), dl4(t), dl16(t), dl17(t), dl3(t), dl20(t)}
;

        auto result = f_4_130_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_130_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[132];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[25];
z[14] = abb[28];
z[15] = abb[31];
z[16] = abb[38];
z[17] = abb[41];
z[18] = abb[18];
z[19] = abb[37];
z[20] = abb[40];
z[21] = abb[20];
z[22] = abb[17];
z[23] = abb[36];
z[24] = abb[39];
z[25] = abb[23];
z[26] = abb[26];
z[27] = abb[27];
z[28] = abb[29];
z[29] = abb[30];
z[30] = abb[12];
z[31] = abb[13];
z[32] = abb[14];
z[33] = abb[15];
z[34] = abb[16];
z[35] = abb[33];
z[36] = abb[35];
z[37] = abb[22];
z[38] = abb[24];
z[39] = abb[34];
z[40] = abb[19];
z[41] = abb[32];
z[42] = bc<TR>[1];
z[43] = bc<TR>[3];
z[44] = bc<TR>[5];
z[45] = bc<TR>[2];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = bc<TR>[9];
z[50] = -z[8] + z[10];
z[51] = z[1] + -z[12];
z[52] = z[50] + -z[51];
z[53] = z[9] + -z[11];
z[54] = -z[52] + z[53];
z[55] = -(z[0] * z[54]);
z[56] = -z[31] + -z[32] + z[33] + z[34];
z[57] = -(z[30] * z[56]);
z[58] = -(z[39] * z[56]);
z[59] = z[41] * z[56];
z[60] = -z[55] + z[57] + -z[58] + z[59];
z[61] = z[9] + z[10];
z[62] = -z[38] / T(3) + (T(13) * z[45]) / T(2);
z[63] = (T(3) * z[12]) / T(2);
z[64] = z[37] / T(3);
z[65] = -z[8] + -z[64];
z[65] = (T(-4) * z[1]) / T(3) + (T(-5) * z[11]) / T(6) + (T(2) * z[61]) / T(3) + z[62] + z[63] + z[65] / T(2);
z[65] = z[16] * z[65];
z[66] = T(3) * z[8];
z[64] = z[64] + -z[66];
z[67] = z[1] + z[11];
z[68] = z[12] / T(2);
z[62] = (T(5) * z[9]) / T(6) + (T(4) * z[10]) / T(3) + -z[62] + z[64] / T(2) + (T(-2) * z[67]) / T(3) + z[68];
z[62] = z[17] * z[62];
z[64] = z[51] + -z[53];
z[64] = z[22] * z[64];
z[64] = T(3) * z[64];
z[69] = z[50] + z[53];
z[70] = z[40] * z[69];
z[70] = T(3) * z[70];
z[71] = -(z[36] * z[56]);
z[72] = -(z[35] * z[56]);
z[73] = z[71] + z[72];
z[73] = z[73] / T(2);
z[74] = z[8] / T(2);
z[75] = (T(3) * z[1]) / T(2) + (T(-5) * z[12]) / T(2) + -z[74];
z[76] = z[10] / T(2);
z[77] = z[75] + -z[76];
z[78] = z[37] + T(7) * z[45];
z[79] = (T(19) * z[9]) / T(6) + (T(-13) * z[11]) / T(6) + z[77] + z[78];
z[79] = z[38] + z[79] / T(2);
z[79] = z[20] * z[79];
z[80] = z[1] / T(2);
z[81] = z[68] + z[80];
z[82] = (T(5) * z[8]) / T(2) + (T(-3) * z[10]) / T(2);
z[83] = z[81] + z[82];
z[84] = (T(13) * z[9]) / T(6) + (T(-19) * z[11]) / T(6) + -z[78] + z[83];
z[84] = -z[38] + z[84] / T(2);
z[84] = z[19] * z[84];
z[85] = z[52] + (T(11) * z[53]) / T(3);
z[86] = z[15] / T(4);
z[85] = z[85] * z[86];
z[87] = z[9] / T(2);
z[88] = z[76] + z[87];
z[89] = (T(3) * z[11]) / T(2) + -z[88];
z[75] = z[75] + z[89];
z[90] = -z[75] + -z[78];
z[90] = -z[38] + z[90] / T(2);
z[90] = z[23] * z[90];
z[91] = z[11] / T(2);
z[92] = z[80] + z[91];
z[68] = (T(-3) * z[9]) / T(2) + z[68] + z[92];
z[82] = z[68] + z[82];
z[78] = z[78] + -z[82];
z[78] = z[38] + z[78] / T(2);
z[78] = z[24] * z[78];
z[93] = -z[16] + z[17];
z[94] = z[19] + -z[20] + z[23];
z[95] = -z[24] + z[94];
z[96] = z[95] / T(2);
z[97] = (T(7) * z[93]) / T(3) + z[96];
z[97] = z[42] * z[97];
z[60] = z[60] / T(4) + z[62] + z[64] + z[65] + -z[70] + -z[73] + z[78] + z[79] + z[84] + z[85] + z[90] + z[97] / T(2);
z[60] = z[7] * z[60];
z[62] = -z[35] + z[36] + z[39] + -z[41];
z[65] = z[43] * z[62];
z[60] = z[60] + z[65];
z[60] = z[7] * z[60];
z[65] = z[37] + T(2) * z[38];
z[74] = -z[65] + z[74];
z[68] = z[68] + z[74] + z[76];
z[68] = z[17] * z[68];
z[76] = z[65] + z[75];
z[78] = z[16] + z[20];
z[79] = z[76] * z[78];
z[50] = z[50] + z[51];
z[84] = z[50] + -z[53];
z[85] = z[15] / T(2);
z[90] = z[84] * z[85];
z[97] = z[13] * z[84];
z[98] = z[97] / T(2);
z[76] = z[23] * z[76];
z[68] = -z[58] / T(2) + z[68] + -z[73] + -z[76] + z[79] + z[90] + -z[98];
z[79] = z[27] * z[68];
z[74] = z[74] + z[81] + -z[89];
z[74] = z[16] * z[74];
z[81] = z[50] + z[53];
z[89] = z[14] * z[81];
z[90] = z[59] + z[89];
z[99] = -z[65] + z[82];
z[100] = z[17] + z[19];
z[101] = z[99] * z[100];
z[102] = z[81] * z[85];
z[73] = z[73] + z[74] + -z[90] / T(2) + -z[101] + z[102];
z[74] = z[24] * z[99];
z[90] = -z[73] + -z[74];
z[90] = z[29] * z[90];
z[101] = z[46] * z[93];
z[79] = z[79] + z[90] + z[101];
z[90] = -z[9] + T(2) * z[11] + z[65];
z[101] = z[1] + z[10];
z[102] = z[90] + -z[101];
z[103] = z[16] * z[102];
z[104] = T(2) * z[9] + -z[11] + z[65];
z[105] = z[1] + T(-2) * z[10] + z[66] + -z[104];
z[106] = -(z[17] * z[105]);
z[107] = z[20] * z[53];
z[108] = z[15] * z[53];
z[109] = T(2) * z[53];
z[110] = z[19] * z[109];
z[106] = -z[70] + -z[103] + z[106] + -z[107] + z[108] + z[110];
z[106] = z[4] * z[106];
z[111] = z[19] * z[53];
z[112] = -z[64] + z[111];
z[113] = T(3) * z[12];
z[90] = T(-2) * z[1] + z[10] + -z[90] + z[113];
z[114] = z[16] * z[90];
z[104] = -z[101] + z[104];
z[115] = z[17] * z[104];
z[109] = z[20] * z[109];
z[115] = z[108] + z[109] + -z[112] + z[114] + z[115];
z[115] = z[3] * z[115];
z[106] = z[106] + z[115];
z[115] = (T(3) * z[8]) / T(2);
z[116] = z[63] + z[115];
z[92] = z[88] + z[92] + -z[116];
z[117] = z[65] + z[92];
z[118] = z[20] * z[117];
z[119] = z[16] * z[117];
z[56] = (T(-3) * z[56]) / T(2);
z[120] = z[35] * z[56];
z[119] = z[119] + -z[120];
z[121] = z[118] + z[119];
z[122] = z[15] * z[117];
z[123] = T(2) * z[17];
z[124] = z[105] * z[123];
z[105] = z[19] * z[105];
z[125] = (T(3) * z[59]) / T(2);
z[124] = T(2) * z[105] + z[121] + -z[122] + z[124] + z[125];
z[124] = z[18] * z[124];
z[74] = T(3) * z[74];
z[126] = z[18] * z[74];
z[126] = -z[124] + z[126];
z[127] = z[17] * z[117];
z[128] = z[36] * z[56];
z[127] = z[127] + z[128];
z[109] = z[109] + z[110] + -z[119] + z[127];
z[110] = (T(3) * z[97]) / T(2);
z[119] = (T(3) * z[89]) / T(2);
z[108] = z[108] + z[109] + z[110] + -z[119];
z[108] = z[2] * z[108];
z[117] = z[19] * z[117];
z[127] = z[117] + z[127];
z[90] = z[20] * z[90];
z[90] = z[90] + z[114];
z[114] = z[39] * z[56];
z[76] = T(3) * z[76] + z[114];
z[114] = z[76] + T(2) * z[90] + -z[122] + z[127];
z[122] = z[21] * z[114];
z[129] = (T(11) * z[62]) / T(180) + -z[93] + z[96];
z[129] = prod_pow(z[7], 2) * z[129];
z[130] = T(-2) * z[16] + (T(3) * z[62]) / T(20) + z[123];
z[130] = z[42] * z[130];
z[131] = -(z[45] * z[93]);
z[130] = z[130] + T(3) * z[131];
z[130] = z[42] * z[130];
z[79] = T(3) * z[79] + T(2) * z[106] + z[108] + z[122] + z[126] + z[129] + z[130];
z[79] = z[7] * z[79];
z[106] = -(z[42] * z[43]);
z[106] = (T(-12) * z[44]) / T(5) + z[106];
z[62] = z[62] * z[106];
z[62] = T(3) * z[62] + z[79];
z[62] = int_to_imaginary<T>(1) * z[62];
z[79] = (T(9) * z[12]) / T(2) + -z[115];
z[106] = (T(5) * z[9]) / T(2);
z[67] = (T(-5) * z[10]) / T(2) + z[65] + (T(7) * z[67]) / T(2) + -z[79] + -z[106];
z[67] = -(z[67] * z[78]);
z[78] = (T(5) * z[11]) / T(2);
z[63] = (T(-5) * z[1]) / T(2) + (T(-9) * z[8]) / T(2) + (T(7) * z[61]) / T(2) + z[63] + z[65] + -z[78];
z[63] = -(z[63] * z[100]);
z[65] = T(2) * z[37] + T(4) * z[38];
z[66] = -z[9] + -z[11] + -z[65] + z[66] + -z[101] + z[113];
z[66] = z[15] * z[66];
z[63] = z[63] + z[66] + z[67] + -z[74] + z[76] + -z[120] + z[125] + z[128];
z[63] = z[25] * z[63];
z[65] = -z[65] + -z[78] + z[80] + z[88] + z[116];
z[65] = z[15] * z[65];
z[66] = z[37] + z[92];
z[66] = z[38] + z[66] / T(2);
z[67] = z[19] + z[20];
z[67] = z[66] * z[67];
z[74] = z[17] * z[66];
z[74] = (T(3) * z[71]) / T(4) + z[74];
z[61] = (T(-23) * z[11]) / T(2) + T(-5) * z[37] + (T(13) * z[61]) / T(2) + z[79] + z[80];
z[61] = T(-5) * z[38] + z[61] / T(2);
z[61] = z[16] * z[61];
z[75] = z[37] + z[75];
z[75] = z[38] + z[75] / T(2);
z[76] = T(3) * z[23];
z[75] = z[75] * z[76];
z[58] = (T(3) * z[58]) / T(4) + z[75];
z[59] = (T(3) * z[59]) / T(4) + -z[70];
z[70] = -z[37] + z[82];
z[70] = -z[38] + z[70] / T(2);
z[75] = z[24] * z[70];
z[76] = (T(3) * z[72]) / T(4);
z[61] = z[58] + z[59] + z[61] + z[65] + z[67] + z[74] + T(-3) * z[75] + -z[76];
z[61] = z[21] * z[61];
z[65] = z[15] * z[102];
z[65] = -z[65] + T(-2) * z[103] + z[118] + -z[119] + z[127];
z[67] = z[2] + -z[4];
z[65] = z[65] * z[67];
z[67] = -(z[3] * z[114]);
z[61] = z[61] + z[65] + z[67] + z[126];
z[61] = z[21] * z[61];
z[65] = z[26] * z[68];
z[67] = z[28] * z[73];
z[65] = -z[65] + z[67];
z[51] = -z[51] + z[69];
z[67] = z[15] * z[51];
z[57] = -z[55] + z[57] + z[67];
z[67] = z[20] * z[51];
z[68] = z[19] * z[54];
z[69] = z[17] * z[84];
z[67] = -z[57] + z[67] + z[68] + z[69] + z[71] + -z[97];
z[67] = z[5] * z[67];
z[68] = -(z[16] * z[81]);
z[54] = z[20] * z[54];
z[51] = z[19] * z[51];
z[51] = z[51] + z[54] + -z[57] + z[68] + z[72] + z[89];
z[51] = z[6] * z[51];
z[51] = z[51] + z[67];
z[54] = z[16] * z[66];
z[54] = z[54] + -z[74] + -z[76];
z[57] = -z[37] + -z[77];
z[57] = T(3) * z[57] + -z[91] + -z[106];
z[66] = T(3) * z[38];
z[57] = z[57] / T(2) + -z[66];
z[57] = z[20] * z[57];
z[50] = T(3) * z[50];
z[67] = -z[50] + -z[53];
z[67] = z[67] * z[86];
z[57] = -z[54] + z[57] + z[58] + z[67] + (T(3) * z[97]) / T(4) + z[112];
z[57] = z[3] * z[57];
z[58] = z[15] + z[123];
z[58] = z[58] * z[104];
z[67] = -z[58] + -z[110] + z[117] + z[121];
z[67] = z[18] * z[67];
z[68] = T(3) * z[52] + z[53];
z[68] = z[68] * z[85];
z[56] = z[30] * z[56];
z[68] = (T(-3) * z[55]) / T(2) + z[56] + z[68] + -z[109];
z[69] = z[4] * z[68];
z[67] = z[67] + -z[69];
z[57] = z[57] + z[67];
z[57] = z[3] * z[57];
z[54] = z[54] + -z[107];
z[55] = z[55] + -z[98];
z[69] = (T(3) * z[89]) / T(4);
z[52] = (T(-3) * z[52]) / T(2) + -z[53];
z[52] = z[15] * z[52];
z[52] = z[52] + -z[54] + (T(3) * z[55]) / T(2) + -z[56] + z[69] + z[111];
z[52] = z[2] * z[52];
z[55] = z[3] * z[68];
z[52] = z[52] + z[55] + -z[67];
z[52] = z[2] * z[52];
z[55] = z[37] + -z[83];
z[55] = T(3) * z[55] + z[78] + z[87];
z[55] = z[55] / T(2) + z[66];
z[55] = z[19] * z[55];
z[50] = z[50] + -z[53];
z[50] = z[50] * z[86];
z[50] = z[50] + -z[54] + z[55] + -z[59] + -z[69];
z[50] = z[4] * z[50];
z[50] = z[50] + z[124];
z[50] = z[4] * z[50];
z[53] = z[58] + z[64] + z[90] + z[105];
z[53] = prod_pow(z[18], 2) * z[53];
z[54] = -(z[18] * z[99]);
z[55] = z[4] * z[70];
z[54] = z[54] + z[55];
z[54] = z[4] * z[54];
z[55] = T(3) * z[48];
z[56] = (T(31) * z[49]) / T(2) + -z[55];
z[58] = prod_pow(z[45], 3);
z[58] = T(2) * z[58];
z[56] = z[56] / T(2) + z[58];
z[59] = T(3) * z[99];
z[59] = z[28] * z[59];
z[54] = T(3) * z[54] + z[56] + z[59];
z[54] = z[24] * z[54];
z[59] = z[93] + z[95];
z[64] = z[46] * z[59];
z[66] = (T(2) * z[93]) / T(3) + z[96];
z[66] = prod_pow(z[42], 2) * z[66];
z[64] = T(3) * z[64] + z[66];
z[64] = z[42] * z[64];
z[55] = (T(149) * z[49]) / T(6) + z[55];
z[55] = z[55] / T(2) + -z[58];
z[55] = z[55] * z[93];
z[56] = -(z[56] * z[94]);
z[58] = z[47] * z[59];
return z[50] + (T(3) * z[51]) / T(2) + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + T(6) * z[58] + z[60] + z[61] + z[62] + z[63] + z[64] + T(3) * z[65];
}



template IntegrandConstructorType<double> f_4_130_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_130_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_130_construct (const Kin<qd_real>&);
#endif

}