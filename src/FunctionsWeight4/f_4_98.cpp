#include "f_4_98.h"

namespace PentagonFunctions {

template <typename T> T f_4_98_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_98_W_10 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_98_W_10 (const Kin<T>& kin) {
        c[0] = (T(3) / T(2) + (T(-3) * kin.v[1]) / T(8) + (T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[4]) / T(2)) * kin.v[1] + (T(3) / T(2) + (T(-3) * kin.v[2]) / T(8) + (T(3) * kin.v[4]) / T(2)) * kin.v[2];
c[1] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (-kin.v[2] / T(2) + T(-2) + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (-kin.v[2] + T(-2) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[2] + T(-2) * kin.v[4]))) + rlog(-kin.v[4]) * (kin.v[2] * (-kin.v[2] / T(2) + T(-2) + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (-kin.v[2] + T(-2) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[2] + T(-2) * kin.v[4]))) + (T(3) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(4) + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(3) / T(2) + (T(3) * kin.v[4]) / T(2) + (T(-3) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-8) * kin.v[2] + T(4) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * (kin.v[2] / T(2) + T(2) + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(2) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + kin.v[2] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(17) / T(2) + (T(7) * kin.v[2]) / T(8) + (T(17) * kin.v[4]) / T(2)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(17) / T(2) + (T(7) * kin.v[2]) / T(4) + (T(17) * kin.v[4]) / T(2) + (T(7) / T(8) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(6) + T(-6) * kin.v[2] + T(6) * kin.v[4])));
c[2] = (T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2);
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2]) + rlog(-kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + T(2) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(7) * kin.v[2]) / T(2) + (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(6)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(6) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[9] * (abb[11] * ((abb[6] * T(-7)) / T(2) + (abb[12] * T(-3)) / T(2) + abb[8] * T(-2) + abb[7] * T(2)) + abb[1] * (abb[2] * abb[13] * T(-2) + abb[11] * T(2) + abb[2] * (abb[2] + bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2))) + abb[2] * (abb[2] * (abb[7] + (abb[6] * T(-17)) / T(4) + (abb[12] * T(3)) / T(4) + -abb[8]) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(6) + abb[3] * (abb[6] * T(-6) + abb[8] * T(-2) + abb[7] * T(2))) + abb[2] * abb[13] * (abb[7] * T(-2) + abb[8] * T(2) + abb[6] * T(6)) + abb[10] * (abb[10] * (abb[1] + abb[7] + (abb[12] * T(-9)) / T(4) + (abb[6] * T(3)) / T(4) + -abb[8]) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[7] * bc<T>[0] * int_to_imaginary<T>(2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-6) + abb[13] * (abb[6] * T(-6) + abb[8] * T(-2) + abb[7] * T(2)) + abb[2] * ((abb[12] * T(3)) / T(2) + (abb[6] * T(7)) / T(2) + abb[7] * T(-2) + abb[8] * T(2)) + abb[1] * (abb[2] * T(-2) + abb[3] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[13] * T(2)) + abb[3] * (abb[7] * T(-2) + abb[8] * T(2) + abb[6] * T(6))));
    }
};
template <typename T> class SpDLog_f_4_98_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_98_W_23 (const Kin<T>& kin) {
        c[0] = ((T(3) * kin.v[3]) / T(2) + -kin.v[4] + T(2)) * kin.v[3] + (-kin.v[4] / T(2) + T(-2)) * kin.v[4] + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + -kin.v[2] + T(-2) + T(2) * kin.v[1] + T(-3) * kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(2) + kin.v[3] + kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]);
c[1] = kin.v[0] * (T(-2) + T(2) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[4])) + kin.v[4] * (T(-2) + T(-2) * kin.v[4]) + kin.v[3] * (T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (T(2) + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[4] * (T(-4) + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(4) + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(-3) * kin.v[2]) / T(4) + (T(-5) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + ((T(-7) * kin.v[3]) / T(4) + (T(5) * kin.v[4]) / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[0] * ((T(5) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + (T(-5) * kin.v[4]) / T(2) + -kin.v[1] + T(1) + (T(-7) / T(4) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3])) + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[4]) * (((T(-3) * kin.v[2]) / T(4) + (T(-5) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + ((T(-7) * kin.v[3]) / T(4) + (T(5) * kin.v[4]) / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[0] * ((T(5) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + (T(-5) * kin.v[4]) / T(2) + -kin.v[1] + T(1) + (T(-7) / T(4) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3])) + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (((T(3) * kin.v[2]) / T(4) + (T(5) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + ((T(7) * kin.v[3]) / T(4) + (T(-5) * kin.v[4]) / T(2) + T(1)) * kin.v[3] + kin.v[0] * ((T(-5) * kin.v[2]) / T(2) + (T(-7) * kin.v[3]) / T(2) + (T(5) * kin.v[4]) / T(2) + T(-1) + (T(7) / T(4) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[1] + T(-2) * kin.v[3])) + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(5) * kin.v[2]) / T(4) + (T(19) * kin.v[3]) / T(2) + (T(-5) * kin.v[4]) / T(2) + T(7)) * kin.v[2] + ((T(33) * kin.v[3]) / T(4) + (T(-19) * kin.v[4]) / T(2) + T(7)) * kin.v[3] + kin.v[0] * ((T(-19) * kin.v[2]) / T(2) + (T(-33) * kin.v[3]) / T(2) + (T(19) * kin.v[4]) / T(2) + T(-7) + (T(33) / T(4) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[0] + T(7) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-6) + T(6) * kin.v[1] + T(-6) * kin.v[3])) + ((T(5) * kin.v[4]) / T(4) + T(-7)) * kin.v[4] + kin.v[1] * (T(-7) * kin.v[2] + T(-7) * kin.v[3] + T(7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4])));
c[2] = T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4];
c[3] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-5) + bc<T>[0] * int_to_imaginary<T>(-6)) * kin.v[0] + T(5) * kin.v[2] + T(5) * kin.v[3] + T(-5) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(3)) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(3)) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[14] * (abb[2] * (abb[2] * (abb[16] + abb[7] / T(2) + -abb[8] / T(2) + (abb[6] * T(-7)) / T(2)) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(6) + abb[3] * (abb[6] * T(-6) + abb[8] * T(-2) + abb[7] * T(2))) + abb[15] * (abb[6] * T(-5) + abb[8] * T(-3) + abb[16] * T(-2) + abb[7] * T(3)) + abb[1] * (abb[2] * (abb[2] / T(2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2)) + abb[15] * T(3)) + abb[10] * (abb[1] * abb[2] * T(-2) + abb[13] * (abb[6] * T(-6) + abb[8] * T(-2) + abb[1] * T(2) + abb[7] * T(2)) + abb[2] * (abb[7] * T(-2) + abb[8] * T(2) + abb[6] * T(6))) + abb[13] * (abb[13] * ((abb[8] * T(-5)) / T(2) + (abb[6] * T(-3)) / T(2) + (abb[1] * T(5)) / T(2) + (abb[7] * T(5)) / T(2) + abb[16] * T(-3)) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[7] * bc<T>[0] * int_to_imaginary<T>(2) + abb[1] * (abb[2] * T(-3) + abb[3] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2)) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-6) + abb[2] * (abb[7] * T(-3) + abb[16] * T(2) + abb[8] * T(3) + abb[6] * T(5)) + abb[3] * (abb[7] * T(-2) + abb[8] * T(2) + abb[6] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_98_construct (const Kin<T>& kin) {
    return [&kin, 
            dl24 = DLog_W_24<T>(kin),dl10 = DLog_W_10<T>(kin),dl23 = DLog_W_23<T>(kin),dl17 = DLog_W_17<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),spdl10 = SpDLog_f_4_98_W_10<T>(kin),spdl23 = SpDLog_f_4_98_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,21> abbr = 
            {dl24(t), rlog(kin.W[4] / kin_path.W[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), dl10(t), rlog(-v_path[4]), f_2_1_7(kin_path), -rlog(t), rlog(-v_path[3] + v_path[0] + v_path[1]), dl23(t), f_2_1_8(kin_path), -rlog(t), dl17(t), dl18(t), dl16(t), dl5(t)}
;

        auto result = f_4_98_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_98_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[63];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[17];
z[11] = abb[18];
z[12] = abb[20];
z[13] = abb[19];
z[14] = abb[10];
z[15] = abb[13];
z[16] = abb[11];
z[17] = abb[15];
z[18] = abb[12];
z[19] = abb[16];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = T(3) * z[7];
z[23] = -z[1] + z[22];
z[24] = T(2) * z[9];
z[25] = z[23] + z[24];
z[26] = T(6) * z[18];
z[27] = T(2) * z[8];
z[28] = T(2) * z[19];
z[29] = -z[25] + z[26] + -z[27] + -z[28];
z[30] = z[12] * z[29];
z[31] = T(3) * z[1];
z[32] = -z[7] + T(5) * z[8] + -z[9] + -z[26] + -z[28] + z[31];
z[32] = z[10] * z[32];
z[33] = T(2) * z[1] + z[27];
z[34] = z[9] + T(3) * z[18] + z[19] + -z[33];
z[35] = T(2) * z[11];
z[34] = z[34] * z[35];
z[36] = z[8] + z[9];
z[37] = z[1] + z[7];
z[38] = z[36] + -z[37];
z[38] = z[0] * z[38];
z[30] = z[30] + z[32] + z[34] + z[38];
z[32] = -(z[13] * z[29]);
z[32] = z[30] + z[32];
z[32] = z[6] * z[32];
z[34] = T(2) * z[18];
z[39] = -z[27] + z[34] + -z[37];
z[39] = z[13] * z[39];
z[40] = -z[1] + z[9];
z[41] = z[22] + z[40];
z[42] = -z[18] + z[19] + z[27] + -z[41];
z[42] = z[11] * z[42];
z[43] = -z[9] + z[19];
z[44] = -(z[12] * z[43]);
z[42] = z[42] + z[44];
z[44] = T(5) * z[7];
z[45] = -z[34] + z[44];
z[46] = -z[1] + z[45];
z[47] = T(4) * z[19];
z[48] = T(5) * z[9] + -z[47];
z[46] = -z[8] + T(2) * z[46] + z[48];
z[49] = -(z[10] * z[46]);
z[39] = -z[38] + z[39] + T(2) * z[42] + z[49];
z[39] = z[2] * z[39];
z[36] = -z[28] + z[36];
z[42] = -z[7] + z[18];
z[49] = T(2) * z[42];
z[50] = z[36] + -z[49];
z[50] = z[10] * z[50];
z[51] = -z[28] + z[37];
z[52] = z[13] * z[51];
z[53] = z[12] * z[49];
z[50] = -z[38] + z[50] + -z[52] + z[53];
z[50] = z[4] * z[50];
z[52] = z[27] + z[47];
z[53] = -z[34] + -z[37] + z[52];
z[53] = z[15] * z[53];
z[54] = T(4) * z[42];
z[55] = z[14] * z[54];
z[53] = z[53] + -z[55];
z[56] = prod_pow(z[20], 2);
z[56] = z[56] / T(2);
z[57] = z[53] + -z[56];
z[57] = z[12] * z[57];
z[31] = z[31] + -z[44];
z[58] = T(4) * z[8] + -z[24] + z[31] + -z[34];
z[58] = z[15] * z[58];
z[59] = z[8] + z[19];
z[25] = -z[25] + z[59];
z[60] = T(2) * z[14];
z[25] = z[25] * z[60];
z[25] = z[25] + z[58];
z[58] = -z[25] + -z[56];
z[58] = z[10] * z[58];
z[51] = z[15] * z[51];
z[56] = T(2) * z[51] + z[56];
z[61] = z[43] + z[49];
z[62] = z[60] * z[61];
z[62] = z[56] + z[62];
z[62] = z[13] * z[62];
z[55] = -z[55] + z[56];
z[55] = z[11] * z[55];
z[32] = z[32] + z[39] + T(-2) * z[50] + z[55] + z[57] + z[58] + z[62];
z[32] = int_to_imaginary<T>(1) * z[32];
z[39] = (T(5) * z[20]) / T(4);
z[55] = -z[39] + (T(-17) * z[42]) / T(12) + z[43];
z[55] = z[12] * z[55];
z[56] = z[8] / T(3);
z[57] = -z[18] + z[24];
z[58] = T(2) * z[7] + z[57];
z[58] = -z[19] + z[39] + -z[56] + z[58] / T(3);
z[58] = z[11] * z[58];
z[39] = (T(4) * z[19]) / T(3) + -z[39];
z[62] = (T(7) * z[1]) / T(3) + z[7];
z[56] = (T(-2) * z[9]) / T(3) + z[18] / T(3) + -z[39] + z[56] + z[62] / T(2);
z[56] = z[13] * z[56];
z[62] = -z[9] + (T(17) * z[42]) / T(6);
z[39] = (T(-5) * z[8]) / T(6) + z[39] + z[62] / T(2);
z[39] = z[10] * z[39];
z[62] = (T(-5) * z[10]) / T(6) + -z[11] / T(3) + z[12] / T(2);
z[62] = int_to_imaginary<T>(1) * z[3] * z[62];
z[39] = (T(7) * z[38]) / T(6) + z[39] + z[55] + z[56] + z[58] + z[62] / T(2);
z[39] = z[3] * z[39];
z[32] = z[32] + z[39];
z[32] = z[3] * z[32];
z[39] = T(-5) * z[1] + T(11) * z[7];
z[55] = T(6) * z[9];
z[56] = T(-6) * z[8] + -z[28] + z[34] + z[39] + z[55];
z[56] = z[10] * z[56];
z[26] = z[23] + -z[26] + z[52];
z[26] = z[12] * z[26];
z[52] = -z[1] + z[34];
z[22] = z[22] + -z[28] + -z[52];
z[22] = z[22] * z[35];
z[40] = -z[7] + -z[19] + z[34] + -z[40];
z[58] = T(2) * z[13];
z[40] = z[40] * z[58];
z[22] = z[22] + z[26] + z[40] + z[56];
z[22] = z[2] * z[22];
z[26] = -(z[12] * z[53]);
z[40] = z[10] * z[25];
z[53] = -(z[14] * z[61]);
z[53] = -z[51] + z[53];
z[53] = z[53] * z[58];
z[56] = z[14] * z[49];
z[51] = -z[51] + z[56];
z[35] = z[35] * z[51];
z[22] = z[22] + z[26] + z[35] + z[40] + z[50] + z[53];
z[22] = z[4] * z[22];
z[26] = T(3) * z[9];
z[35] = -z[19] + z[26];
z[40] = T(3) * z[8];
z[39] = z[18] + z[35] + z[39] / T(2) + -z[40];
z[39] = z[11] * z[39];
z[44] = z[1] + -z[44];
z[35] = -z[8] + T(4) * z[18] + -z[35] + z[44] / T(2);
z[35] = z[12] * z[35];
z[44] = z[1] + (T(33) * z[7]) / T(2) + T(9) * z[9] + (T(-29) * z[18]) / T(2);
z[44] = (T(3) * z[8]) / T(2) + T(-5) * z[19] + z[44] / T(2);
z[44] = z[10] * z[44];
z[28] = z[8] + z[28];
z[50] = -z[18] + z[28];
z[51] = -z[9] + z[50];
z[51] = z[13] * z[51];
z[35] = z[35] + z[38] + z[39] + z[44] + z[51];
z[35] = z[2] * z[35];
z[25] = z[11] * z[25];
z[33] = (T(21) * z[7]) / T(2) + (T(-7) * z[18]) / T(2) + -z[33];
z[38] = -z[33] + z[47] + -z[55];
z[38] = z[14] * z[38];
z[39] = -(z[15] * z[46]);
z[38] = z[38] + z[39];
z[38] = z[10] * z[38];
z[39] = z[42] + z[43];
z[43] = -(z[12] * z[39] * z[60]);
z[44] = z[18] + -z[59];
z[44] = z[15] * z[44] * z[58];
z[25] = z[25] + z[35] + z[38] + z[43] + z[44];
z[25] = z[2] * z[25];
z[30] = z[5] * z[30];
z[29] = -(z[5] * z[29]);
z[26] = T(3) * z[19] + -z[26];
z[35] = z[26] + z[54];
z[38] = z[16] * z[35];
z[24] = z[1] + -z[24] + z[59];
z[43] = z[24] + z[42];
z[44] = z[17] * z[43];
z[38] = z[38] + z[44];
z[44] = T(4) * z[14];
z[39] = z[39] * z[44];
z[43] = z[15] * z[43];
z[43] = -z[39] + z[43];
z[43] = z[15] * z[43];
z[44] = prod_pow(z[14], 2);
z[46] = z[44] * z[61];
z[29] = (T(3) * z[21]) / T(8) + z[29] + T(2) * z[38] + z[43] + z[46];
z[29] = z[13] * z[29];
z[34] = T(3) * z[23] + -z[34] + -z[40] + z[48];
z[34] = z[10] * z[34];
z[27] = z[1] + T(4) * z[9] + T(-8) * z[19] + -z[27] + z[45];
z[38] = z[11] + -z[12];
z[27] = z[27] * z[38];
z[27] = z[27] + z[34];
z[27] = z[17] * z[27];
z[34] = -z[37] / T(2) + z[50];
z[34] = z[15] * z[34];
z[34] = z[34] + -z[39];
z[34] = z[15] * z[34];
z[37] = z[26] + z[49];
z[37] = z[37] * z[44];
z[34] = (T(25) * z[21]) / T(8) + z[34] + z[37];
z[34] = z[12] * z[34];
z[23] = z[23] / T(2) + -z[28] + z[57];
z[23] = z[15] * z[23];
z[28] = z[7] + -z[36] + z[52];
z[28] = z[28] * z[60];
z[23] = z[23] + z[28];
z[23] = z[15] * z[23];
z[23] = (T(-5) * z[21]) / T(8) + z[23] + -z[37];
z[23] = z[11] * z[23];
z[28] = -z[8] + z[41];
z[28] = z[28] * z[60];
z[31] = (T(-5) * z[8]) / T(2) + (T(3) * z[9]) / T(2) + z[18] + -z[31] / T(2);
z[31] = z[15] * z[31];
z[28] = z[28] + z[31];
z[28] = z[15] * z[28];
z[24] = (T(11) * z[7]) / T(4) + -z[18] / T(4) + -z[24];
z[24] = z[24] * z[44];
z[24] = (T(-71) * z[21]) / T(8) + z[24] + z[28];
z[24] = z[10] * z[24];
z[26] = z[26] + z[42];
z[26] = z[12] * z[26];
z[28] = -(z[11] * z[35]);
z[26] = z[26] + z[28];
z[28] = T(8) * z[9] + T(-6) * z[19] + z[33];
z[28] = z[10] * z[28];
z[26] = T(2) * z[26] + z[28];
z[26] = z[16] * z[26];
return z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[29] + z[30] + z[32] + z[34];
}



template IntegrandConstructorType<double> f_4_98_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_98_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_98_construct (const Kin<qd_real>&);
#endif

}