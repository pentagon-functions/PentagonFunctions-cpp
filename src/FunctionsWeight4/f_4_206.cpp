#include "f_4_206.h"

namespace PentagonFunctions {

template <typename T> T f_4_206_abbreviated (const std::array<T,18>&);



template <typename T> IntegrandConstructorType<T> f_4_206_construct (const Kin<T>& kin) {
    return [&kin, 
            dl15 = DLog_W_15<T>(kin),dl8 = DLog_W_8<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,18> abbr = 
            {dl15(t), rlog(kin.W[0] / kin_path.W[0]), rlog(v_path[2]), rlog(-v_path[4]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[19] / kin_path.W[19]), dl8(t), rlog(-v_path[2] + v_path[0] + v_path[4]), dl3(t), rlog(kin.W[7] / kin_path.W[7]), rlog(kin.W[14] / kin_path.W[14]), dl5(t), f_2_1_5(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(-v_path[4] + v_path[2]), dl20(t), dl1(t)}
;

        auto result = f_4_206_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_206_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[39];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[11];
z[9] = abb[16];
z[10] = abb[17];
z[11] = abb[7];
z[12] = abb[12];
z[13] = abb[13];
z[14] = abb[14];
z[15] = abb[15];
z[16] = abb[8];
z[17] = abb[9];
z[18] = abb[10];
z[19] = bc<TR>[1];
z[20] = bc<TR>[2];
z[21] = bc<TR>[4];
z[22] = bc<TR>[9];
z[23] = (T(-8) * z[19]) / T(3) + T(4) * z[20];
z[24] = z[8] + z[9];
z[25] = -z[10] + z[24];
z[23] = z[23] * z[25];
z[26] = z[0] + z[7];
z[27] = (T(10) * z[10]) / T(3) + T(-2) * z[26];
z[28] = z[1] + -z[6];
z[29] = -z[5] + z[28];
z[27] = z[27] * z[29];
z[24] = (T(4) * z[10]) / T(3) + -z[24] / T(2) + (T(-5) * z[26]) / T(6);
z[30] = int_to_imaginary<T>(1) * z[3];
z[24] = z[24] * z[30];
z[31] = -z[5] + -z[6] + z[17] + z[18];
z[31] = z[16] * z[31];
z[32] = z[8] * z[18];
z[33] = z[5] * z[9];
z[34] = -z[17] / T(2) + T(-4) * z[28];
z[34] = z[9] * z[34];
z[35] = -z[1] + z[5];
z[35] = (T(3) * z[6]) / T(2) + (T(4) * z[35]) / T(3);
z[35] = z[8] * z[35];
z[23] = z[23] + z[24] + z[27] + (T(13) * z[31]) / T(6) + -z[32] / T(6) + (T(3) * z[33]) / T(2) + z[34] / T(3) + z[35];
z[23] = z[3] * z[23];
z[24] = -z[8] + z[10];
z[24] = z[24] * z[29];
z[27] = z[9] * z[28];
z[27] = z[27] + -z[33];
z[24] = z[24] + -z[27];
z[28] = z[11] * z[24];
z[34] = z[4] * z[24];
z[28] = z[28] + z[34];
z[35] = z[14] + z[15];
z[35] = z[24] * z[35];
z[36] = z[8] + -z[26];
z[36] = z[29] * z[36];
z[36] = z[27] + z[31] + z[36];
z[36] = z[2] * z[36];
z[37] = prod_pow(z[19], 2);
z[38] = z[19] * z[20];
z[37] = z[37] + T(-2) * z[38];
z[37] = z[25] * z[37];
z[35] = -z[28] + z[35] + z[36] + z[37];
z[35] = int_to_imaginary<T>(1) * z[35];
z[23] = z[23] + T(4) * z[35];
z[23] = z[3] * z[23];
z[35] = -(z[6] * z[8]);
z[36] = -(z[0] * z[29]);
z[32] = z[27] + z[32] + z[35] + z[36];
z[32] = prod_pow(z[4], 2) * z[32];
z[35] = z[7] + -z[8];
z[35] = -(z[29] * z[35]);
z[36] = z[9] * z[17];
z[33] = -z[33] + z[35] + z[36];
z[33] = z[11] * z[33];
z[33] = z[33] + T(-2) * z[34];
z[33] = z[11] * z[33];
z[26] = z[8] + T(-2) * z[10] + z[26];
z[26] = z[26] * z[29];
z[26] = z[26] + z[27] + -z[31];
z[26] = z[2] * z[26];
z[26] = z[26] + T(2) * z[28];
z[26] = z[2] * z[26];
z[24] = T(2) * z[24];
z[27] = -z[12] + -z[13];
z[24] = z[24] * z[27];
z[27] = z[22] * z[25];
z[24] = z[24] + z[26] + (T(-13) * z[27]) / T(3) + z[32] + z[33];
z[25] = z[21] * z[25] * z[30];
return z[23] + T(2) * z[24] + T(8) * z[25];
}



template IntegrandConstructorType<double> f_4_206_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_206_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_206_construct (const Kin<qd_real>&);
#endif

}