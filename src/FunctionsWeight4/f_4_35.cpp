#include "f_4_35.h"

namespace PentagonFunctions {

template <typename T> T f_4_35_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_35_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_35_W_7 (const Kin<T>& kin) {
        c[0] = T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(2) * kin.v[3] + T(4) * kin.v[4]) + rlog(kin.v[3]) * (((T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1) + kin.v[1]) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1) + kin.v[1]) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]) + rlog(-kin.v[1]) * (-(kin.v[1] * kin.v[4]) + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4]) + rlog(-kin.v[4]) * (-(kin.v[1] * kin.v[4]) + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4]);
c[1] = (-kin.v[3] + -kin.v[4]) * rlog(kin.v[3]) + (-kin.v[3] + -kin.v[4]) * rlog(-kin.v[1] + kin.v[3] + kin.v[4]) + rlog(-kin.v[1]) * (kin.v[3] + kin.v[4]) + rlog(-kin.v[4]) * (kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * (prod_pow(abb[2], 2) + -abb[3]) + abb[3] * (abb[5] + abb[7] + -abb[6]) + prod_pow(abb[1], 2) * (abb[5] + abb[7] + -abb[4] + -abb[6]) + prod_pow(abb[2], 2) * (abb[6] + -abb[5] + -abb[7]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_35_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl19 = DLog_W_19<T>(kin),dl14 = DLog_W_14<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl4 = DLog_W_4<T>(kin),spdl7 = SpDLog_f_4_35_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), dl19(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), rlog(v_path[3]), rlog(kin.W[13] / kin_path.W[13]), dl14(t), dl2(t), dl5(t), dl4(t)}
;

        auto result = f_4_35_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_35_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[41];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[8];
z[3] = abb[13];
z[4] = abb[14];
z[5] = abb[15];
z[6] = abb[16];
z[7] = abb[5];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[12];
z[11] = abb[2];
z[12] = abb[11];
z[13] = bc<TR>[0];
z[14] = abb[3];
z[15] = abb[9];
z[16] = abb[10];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = T(2) * z[7];
z[20] = T(2) * z[9];
z[21] = z[19] + z[20];
z[22] = T(2) * z[10];
z[23] = -z[8] + z[22];
z[24] = T(3) * z[1];
z[25] = z[21] + z[23] + -z[24];
z[26] = z[5] * z[25];
z[27] = z[7] + z[9];
z[28] = T(3) * z[27];
z[29] = T(3) * z[8];
z[30] = -z[1] + z[22] + z[28] + -z[29];
z[30] = z[3] * z[30];
z[31] = z[1] + z[8];
z[32] = -z[22] + z[31];
z[33] = z[2] * z[32];
z[29] = -z[1] + z[29];
z[34] = -z[21] + z[29];
z[35] = z[4] * z[34];
z[36] = z[6] * z[32];
z[37] = -z[26] + z[30] + -z[33] + z[35] + -z[36];
z[37] = z[0] * z[37];
z[21] = -z[8] + z[21];
z[38] = -z[10] + z[21];
z[38] = z[3] * z[38];
z[38] = -z[36] + z[38];
z[39] = T(2) * z[2];
z[32] = z[32] * z[39];
z[32] = z[26] + z[32] + -z[35] + T(-2) * z[38];
z[35] = -(z[11] * z[32]);
z[32] = z[12] * z[32];
z[32] = z[32] + -z[35] + z[37];
z[32] = z[0] * z[32];
z[37] = -z[22] + z[27];
z[37] = z[4] * z[37];
z[38] = T(2) * z[6];
z[40] = -z[1] + z[27];
z[40] = z[38] * z[40];
z[34] = -(z[2] * z[34]);
z[30] = -z[30] + z[34] + z[37] + z[40];
z[30] = z[0] * z[30];
z[34] = T(4) * z[10];
z[31] = -z[27] + -z[31] + z[34];
z[31] = z[3] * z[31];
z[31] = z[31] + z[33] + z[37];
z[31] = z[12] * z[31];
z[21] = z[21] + z[24] + -z[34];
z[21] = z[2] * z[21];
z[24] = -z[22] + z[28] + -z[29];
z[24] = z[4] * z[24];
z[23] = z[23] + -z[27];
z[23] = z[23] * z[38];
z[28] = -z[1] + T(-5) * z[8] + T(7) * z[27];
z[28] = z[3] * z[28];
z[21] = -z[21] + z[23] + -z[24] + z[28];
z[23] = z[21] + -z[26];
z[23] = z[16] * z[23];
z[24] = prod_pow(z[13], 2);
z[28] = z[4] + z[5];
z[28] = (T(5) * z[2]) / T(3) + (T(-7) * z[3]) / T(3) + z[6] + z[28] / T(6);
z[28] = z[24] * z[28];
z[23] = z[23] + z[28] / T(2) + z[30] + T(2) * z[31] + -z[35];
z[28] = int_to_imaginary<T>(1) * z[13];
z[23] = z[23] * z[28];
z[30] = -z[31] + z[35];
z[30] = z[12] * z[30];
z[21] = z[15] * z[21];
z[25] = z[6] * z[25];
z[29] = -z[22] + -z[27] + z[29];
z[29] = z[4] * z[29];
z[31] = T(-2) * z[8] + z[10] + z[27];
z[31] = z[31] * z[39];
z[25] = z[25] + z[29] + z[31];
z[25] = z[14] * z[25];
z[29] = T(-7) * z[1] + z[7] + z[9];
z[31] = (T(11) * z[8]) / T(6);
z[29] = z[29] / T(6) + -z[31] + z[34];
z[29] = z[3] * z[29];
z[19] = z[1] + -z[19] + z[22];
z[19] = -z[9] + z[19] / T(3);
z[19] = z[6] * z[19];
z[22] = -z[1] + z[9];
z[22] = -z[7] + (T(-5) * z[10]) / T(3) + (T(-5) * z[22]) / T(6) + z[31];
z[22] = z[2] * z[22];
z[27] = -z[10] + z[27] / T(2);
z[27] = z[4] * z[27];
z[19] = z[19] + z[22] + (T(5) * z[27]) / T(3) + z[29];
z[19] = z[19] * z[24];
z[20] = -(z[2] * z[20]);
z[20] = z[20] + -z[36] + z[37];
z[20] = prod_pow(z[11], 2) * z[20];
z[22] = -z[14] + -z[15];
z[22] = z[22] * z[26];
z[26] = z[2] + z[6];
z[27] = T(4) * z[3] + (T(-5) * z[4]) / T(4) + (T(-9) * z[5]) / T(4) + (T(3) * z[26]) / T(4);
z[24] = z[24] * z[27];
z[26] = -z[4] + T(-5) * z[5] + T(7) * z[26];
z[26] = z[17] * z[26] * z[28];
z[24] = z[24] + z[26] / T(2);
z[24] = z[17] * z[24];
z[26] = -z[3] + (T(3) * z[6]) / T(8);
z[26] = (T(-41) * z[2]) / T(24) + z[4] / T(8) + (T(5) * z[5]) / T(8) + T(7) * z[26];
z[26] = z[18] * z[26];
return z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[30] + z[32];
}



template IntegrandConstructorType<double> f_4_35_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_35_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_35_construct (const Kin<qd_real>&);
#endif

}