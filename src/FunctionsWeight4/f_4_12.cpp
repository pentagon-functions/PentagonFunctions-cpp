#include "f_4_12.h"

namespace PentagonFunctions {

template <typename T> T f_4_12_abbreviated (const std::array<T,16>&);



template <typename T> IntegrandConstructorType<T> f_4_12_construct (const Kin<T>& kin) {
    return [&kin, 
            dl1 = DLog_W_1<T>(kin),dl11 = DLog_W_11<T>(kin),dl6 = DLog_W_6<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl3 = DLog_W_3<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,16> abbr = 
            {dl1(t), rlog(kin.W[5] / kin_path.W[5]), rlog(v_path[0]), rlog(kin.W[10] / kin_path.W[10]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[17] / kin_path.W[17]), dl11(t), rlog(v_path[3]), rlog(kin.W[2] / kin_path.W[2]), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl4(t), f_2_1_2(kin_path), f_2_1_9(kin_path), dl18(t), dl3(t)}
;

        auto result = f_4_12_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_12_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[42];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[10];
z[8] = abb[14];
z[9] = abb[6];
z[10] = abb[9];
z[11] = abb[11];
z[12] = abb[15];
z[13] = abb[8];
z[14] = abb[7];
z[15] = abb[12];
z[16] = abb[13];
z[17] = bc<TR>[1];
z[18] = bc<TR>[2];
z[19] = bc<TR>[4];
z[20] = bc<TR>[7];
z[21] = bc<TR>[8];
z[22] = bc<TR>[9];
z[23] = z[10] + -z[11];
z[24] = z[5] + z[6];
z[25] = -z[13] + z[24];
z[23] = z[23] * z[25];
z[26] = z[5] * z[8];
z[27] = z[1] * z[8];
z[23] = z[23] + -z[26] + z[27];
z[23] = z[7] * z[23];
z[26] = -z[8] + z[9];
z[26] = z[25] * z[26];
z[28] = z[4] + -z[6];
z[28] = z[11] * z[28];
z[26] = z[26] + z[28];
z[28] = z[14] * z[26];
z[29] = z[9] + z[10];
z[30] = z[8] + z[11];
z[31] = z[29] + -z[30];
z[32] = -(z[25] * z[31]);
z[24] = -z[1] + -z[4] + z[24];
z[24] = z[0] * z[24];
z[32] = z[24] + z[32];
z[32] = z[2] * z[32];
z[33] = -z[12] + z[30];
z[34] = z[19] * z[33];
z[28] = z[23] + z[28] + z[32] + T(-2) * z[34];
z[32] = T(2) * z[12];
z[35] = z[29] + z[30] + -z[32];
z[36] = prod_pow(z[17], 2);
z[37] = -(z[35] * z[36]);
z[38] = prod_pow(z[3], 2);
z[39] = z[38] / T(3);
z[40] = z[33] * z[39];
z[28] = T(2) * z[28] + z[37] + z[40];
z[28] = int_to_imaginary<T>(1) * z[3] * z[28];
z[35] = z[25] * z[35];
z[35] = -z[24] + z[35];
z[35] = z[2] * z[35];
z[37] = z[25] * z[33];
z[40] = z[14] * z[37];
z[35] = z[35] + T(-2) * z[40];
z[35] = z[2] * z[35];
z[40] = -z[2] + z[14];
z[40] = z[37] * z[40];
z[23] = -z[23] + T(2) * z[40];
z[23] = z[7] * z[23];
z[40] = z[16] * z[37];
z[41] = prod_pow(z[18], 3) * z[33];
z[40] = z[40] + (T(-2) * z[41]) / T(3);
z[26] = -(prod_pow(z[14], 2) * z[26]);
z[23] = z[23] + z[26] + z[28] + z[35] + T(2) * z[40];
z[24] = z[24] + z[27];
z[25] = z[25] * z[32];
z[26] = T(2) * z[13];
z[27] = (T(-17) * z[5]) / T(2) + T(-2) * z[6] + z[26];
z[27] = z[8] * z[27];
z[26] = (T(13) * z[4]) / T(2) + T(-2) * z[5] + (T(-17) * z[6]) / T(2) + z[26];
z[26] = z[11] * z[26];
z[28] = T(2) * z[33];
z[28] = -(z[18] * z[28]);
z[24] = (T(13) * z[24]) / T(2) + z[25] + z[26] + z[27] + z[28];
z[24] = z[24] * z[39];
z[25] = z[31] * z[38];
z[26] = (T(-4) * z[12]) / T(3) + z[29] / T(3) + z[30];
z[26] = z[26] * z[36];
z[25] = (T(-13) * z[25]) / T(6) + T(2) * z[26] + T(8) * z[34];
z[25] = z[17] * z[25];
z[26] = T(8) * z[20] + T(2) * z[21] + (T(-37) * z[22]) / T(3);
z[26] = z[26] * z[33];
z[27] = z[15] * z[37];
return T(2) * z[23] + z[24] + z[25] + z[26] + T(4) * z[27];
}



template IntegrandConstructorType<double> f_4_12_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_12_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_12_construct (const Kin<qd_real>&);
#endif

}