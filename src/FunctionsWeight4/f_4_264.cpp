#include "f_4_264.h"

namespace PentagonFunctions {

template <typename T> T f_4_264_abbreviated (const std::array<T,8>&);

template <typename T> class SpDLog_f_4_264_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_264_W_7 (const Kin<T>& kin) {
        c[0] = (-kin.v[3] / T(8) + -kin.v[4] / T(4) + T(1) / T(2) + kin.v[1] / T(2)) * kin.v[3] + (kin.v[1] * kin.v[4]) / T(2) + (-kin.v[4] / T(8) + T(1) / T(2)) * kin.v[4];
c[1] = (-kin.v[3] / T(2) + T(1) / T(2) + kin.v[1] / T(2) + -kin.v[4]) * kin.v[3] + (kin.v[1] * kin.v[4]) / T(2) + (-kin.v[4] / T(2) + T(1) / T(2)) * kin.v[4] + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-(kin.v[1] * kin.v[4]) / T(2) + (-kin.v[1] / T(2) + T(-1) / T(2) + kin.v[3] / T(8) + kin.v[4] / T(4)) * kin.v[3] + (T(-1) / T(2) + kin.v[4] / T(8)) * kin.v[4]);
c[2] = -kin.v[3] / T(2) + -kin.v[4] / T(2);
c[3] = (kin.v[3] / T(2) + kin.v[4] / T(2)) * rlog(-kin.v[1] + kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * ((prod_pow(abb[2], 2) * abb[5]) / T(4) + abb[4] * (prod_pow(abb[2], 2) / T(4) + -abb[3] / T(2)) + -(abb[3] * abb[5]) / T(2) + abb[1] * ((abb[2] * abb[4]) / T(2) + (abb[2] * abb[5]) / T(2) + abb[1] * ((abb[4] * T(-3)) / T(4) + (abb[5] * T(-3)) / T(4))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_264_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),spdl7 = SpDLog_f_4_264_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,8> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), -rlog(t), rlog(kin.W[18] / kin_path.W[18]), dl2(t), dl19(t)}
;

        auto result = f_4_264_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_264_abbreviated(const std::array<T,8>& abb)
{
using TR = typename T::value_type;
T z[14];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[6];
z[3] = abb[7];
z[4] = abb[5];
z[5] = abb[2];
z[6] = abb[3];
z[7] = bc<TR>[0];
z[8] = prod_pow(z[0], 2);
z[9] = -z[6] + z[8] / T(2);
z[10] = z[1] + z[4];
z[9] = z[9] * z[10];
z[10] = z[0] * z[10];
z[11] = z[1] + T(-7) * z[4];
z[11] = z[5] * z[11];
z[10] = z[10] + z[11] / T(2);
z[10] = z[5] * z[10];
z[11] = prod_pow(z[7], 2);
z[12] = z[1] + -z[4];
z[13] = z[11] * z[12];
z[9] = z[9] + z[10] + -z[13] / T(6);
z[9] = z[3] * z[9];
z[8] = -z[8] + z[11] / T(12);
z[8] = z[2] * z[8] * z[12];
return z[8] + z[9] / T(2);
}



template IntegrandConstructorType<double> f_4_264_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_264_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_264_construct (const Kin<qd_real>&);
#endif

}