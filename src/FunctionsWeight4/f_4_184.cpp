#include "f_4_184.h"

namespace PentagonFunctions {

template <typename T> T f_4_184_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_184_W_10 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_184_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4]);
c[1] = kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4]);
c[2] = kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] + prod_pow(abb[2], 2) * (abb[4] + abb[5]) + prod_pow(abb[1], 2) * (-abb[3] + -abb[4] + -abb[5]));
    }
};
template <typename T> class SpDLog_f_4_184_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_184_W_23 (const Kin<T>& kin) {
        c[0] = ((T(3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (-kin.v[3] / T(4) + -kin.v[4] / T(2) + T(-1)) * kin.v[3] + (-kin.v[0] / T(4) + -kin.v[2] / T(2) + kin.v[3] / T(2) + kin.v[4] / T(2) + -kin.v[1] + T(1)) * kin.v[0] + ((T(3) * kin.v[4]) / T(4) + T(1)) * kin.v[4];
c[1] = -kin.v[0] + kin.v[2] + kin.v[3] + -kin.v[4];
c[2] = ((T(3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (-kin.v[3] / T(4) + -kin.v[4] / T(2) + T(-1)) * kin.v[3] + (-kin.v[0] / T(4) + -kin.v[2] / T(2) + kin.v[3] / T(2) + kin.v[4] / T(2) + -kin.v[1] + T(1)) * kin.v[0] + ((T(3) * kin.v[4]) / T(4) + T(1)) * kin.v[4];
c[3] = -kin.v[0] + kin.v[2] + kin.v[3] + -kin.v[4];
c[4] = kin.v[0] * (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[0] / T(4) + kin.v[2] / T(2) + T(-1) + kin.v[1]) + (kin.v[3] / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + (-kin.v[3] / T(2) + (T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(1)) * kin.v[2];
c[5] = kin.v[0] + -kin.v[2] + -kin.v[3] + kin.v[4];
c[6] = kin.v[0] * (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[0] / T(4) + kin.v[2] / T(2) + T(-1) + kin.v[1]) + (kin.v[3] / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + (-kin.v[3] / T(2) + (T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(1)) * kin.v[2];
c[7] = kin.v[0] + -kin.v[2] + -kin.v[3] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]) + abb[5] * (t * c[6] + c[7]);
        }

        return abb[6] * (abb[8] * (abb[5] + abb[9] + -abb[4]) + prod_pow(abb[7], 2) * (abb[5] + abb[9] + -abb[3] + -abb[4]) + abb[3] * (prod_pow(abb[2], 2) + -abb[8]) + prod_pow(abb[2], 2) * (abb[4] + -abb[5] + -abb[9]));
    }
};
template <typename T> class SpDLog_f_4_184_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_184_W_12 (const Kin<T>& kin) {
        c[0] = -(kin.v[0] * kin.v[4]) + kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + ((T(-5) * kin.v[4]) / T(2) + T(-4)) * kin.v[4] + bc<T>[0] * (-kin.v[4] / T(2) + T(-1)) * int_to_imaginary<T>(1) * kin.v[4] + kin.v[1] * (-kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) + T(4) + kin.v[0] + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + T(-2) * kin.v[3] + kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4];
c[2] = kin.v[1] * (-kin.v[0] + -kin.v[4] + T(-4) + bc<T>[0] * int_to_imaginary<T>(-1) + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[0] * kin.v[4] + -(kin.v[2] * kin.v[4]) + T(-2) * kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[4] / T(2) + T(1)) * kin.v[4] + ((T(5) * kin.v[4]) / T(2) + T(4)) * kin.v[4];
c[3] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[21] * (t * c[0] + c[1]) + abb[3] * (t * c[2] + c[3]);
        }

        return abb[25] * (abb[24] * (abb[1] * abb[3] + -(abb[1] * abb[21])) + abb[21] * abb[26] * T(-2) + abb[1] * (abb[7] * abb[21] + -(abb[1] * abb[21]) + -(abb[2] * abb[21]) + abb[21] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[23] * (abb[1] * abb[21] + abb[2] * abb[21] + abb[24] * (abb[21] + -abb[3]) + -(abb[7] * abb[21]) + abb[21] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[3] * (abb[7] + -abb[1] + -abb[2] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[3] * (abb[1] * (abb[1] + abb[2] + -abb[7] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[26] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_184_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl23 = DLog_W_23<T>(kin),dl27 = DLog_W_27<T>(kin),dl18 = DLog_W_18<T>(kin),dl14 = DLog_W_14<T>(kin),dl12 = DLog_W_12<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl9 = DLog_W_9<T>(kin),dl5 = DLog_W_5<T>(kin),dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl31 = DLog_W_31<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),spdl10 = SpDLog_f_4_184_W_10<T>(kin),spdl23 = SpDLog_f_4_184_W_23<T>(kin),spdl12 = SpDLog_f_4_184_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_8(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[2] / kin_path.W[2]), f_2_1_7(kin_path), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), dl18(t), f_1_3_3(kin) - f_1_3_3(kin_path), dl14(t), rlog(-v_path[1]), rlog(v_path[3]), dl12(t), f_2_1_4(kin_path), dl20(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl1(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl9(t), dl5(t), dl4(t), dl17(t), dl2(t), dl16(t), dl3(t), dl19(t), dl31(t), dl30(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_184_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_184_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[109];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[20];
z[3] = abb[34];
z[4] = abb[35];
z[5] = abb[36];
z[6] = abb[37];
z[7] = abb[38];
z[8] = abb[39];
z[9] = abb[4];
z[10] = abb[5];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[13];
z[14] = abb[14];
z[15] = abb[15];
z[16] = abb[16];
z[17] = abb[17];
z[18] = abb[18];
z[19] = abb[19];
z[20] = abb[42];
z[21] = abb[43];
z[22] = abb[21];
z[23] = abb[2];
z[24] = abb[7];
z[25] = abb[23];
z[26] = abb[40];
z[27] = abb[24];
z[28] = bc<TR>[0];
z[29] = abb[45];
z[30] = abb[9];
z[31] = abb[27];
z[32] = abb[44];
z[33] = abb[33];
z[34] = abb[30];
z[35] = abb[8];
z[36] = abb[12];
z[37] = abb[26];
z[38] = abb[28];
z[39] = abb[29];
z[40] = abb[31];
z[41] = abb[32];
z[42] = abb[22];
z[43] = bc<TR>[3];
z[44] = abb[41];
z[45] = bc<TR>[2];
z[46] = bc<TR>[9];
z[47] = bc<TR>[1];
z[48] = bc<TR>[4];
z[49] = z[2] / T(4) + -z[34];
z[50] = z[8] / T(4);
z[51] = z[7] / T(2);
z[52] = (T(-7) * z[3]) / T(2) + -z[49] + z[50] + -z[51];
z[52] = z[22] * z[52];
z[53] = -z[1] + z[22];
z[54] = z[53] / T(2);
z[55] = -z[30] + z[54];
z[56] = -z[10] + z[55] / T(2);
z[56] = z[31] * z[56];
z[57] = z[2] + -z[7];
z[58] = -z[3] + z[57] / T(2);
z[58] = z[9] * z[58];
z[52] = z[52] + z[56] + z[58];
z[58] = z[12] + z[13];
z[59] = z[15] + z[16] + z[17] + z[19];
z[60] = -z[18] + z[58] / T(2) + (T(-3) * z[59]) / T(2);
z[61] = z[14] + z[60] / T(2);
z[62] = z[21] * z[61];
z[58] = -z[18] / T(3) + z[58] / T(6) + -z[59] / T(2);
z[59] = z[14] / T(3) + z[58] / T(2);
z[63] = z[11] * z[59];
z[64] = z[4] / T(3);
z[65] = z[30] + z[54];
z[66] = z[64] * z[65];
z[67] = z[10] + -z[22];
z[68] = z[42] * z[67];
z[69] = z[10] + z[30];
z[70] = z[1] + z[69];
z[70] = z[33] * z[70];
z[71] = z[7] * z[30];
z[49] = (T(5) * z[3]) / T(6) + -z[7] / T(3) + -z[8] / T(12) + z[49];
z[49] = z[1] * z[49];
z[72] = -z[3] + z[8] / T(3);
z[73] = z[6] / T(3) + -z[72];
z[73] = z[10] * z[73];
z[74] = z[53] / T(4);
z[75] = z[30] + z[74];
z[76] = -z[10] / T(3) + -z[75];
z[76] = z[5] * z[76];
z[77] = (T(5) * z[21]) / T(4);
z[78] = z[29] + z[77];
z[78] = z[45] * z[78];
z[79] = -z[21] + z[29] / T(3);
z[79] = z[47] * z[79];
z[49] = z[49] + z[52] / T(3) + -z[62] + z[63] + z[66] + -z[68] + (T(4) * z[70]) / T(3) + -z[71] / T(6) + z[73] + z[76] + z[78] + z[79];
z[49] = z[28] * z[49];
z[52] = z[4] + z[5] + z[6];
z[63] = T(3) * z[3];
z[52] = T(2) * z[7] + -z[8] + -z[26] + T(3) * z[52] + z[63];
z[52] = z[43] * z[52];
z[49] = z[49] + z[52];
z[49] = z[28] * z[49];
z[52] = z[5] * z[65];
z[65] = z[52] / T(2) + z[62];
z[66] = z[6] / T(4);
z[73] = T(3) * z[34];
z[76] = z[7] + z[73];
z[76] = -z[3] + z[66] + z[76] / T(4);
z[76] = z[1] * z[76];
z[78] = z[7] + -z[34];
z[66] = z[3] + z[66] + z[78] / T(4);
z[66] = z[22] * z[66];
z[78] = z[20] * z[61];
z[79] = z[30] * z[51];
z[80] = -(z[3] * z[10]);
z[75] = -z[10] / T(2) + -z[75];
z[75] = z[4] * z[75];
z[66] = -z[56] + z[65] + z[66] + z[68] + (T(-3) * z[70]) / T(2) + z[75] + z[76] + z[78] + z[79] + z[80];
z[66] = z[27] * z[66];
z[75] = -z[3] / T(2) + (T(3) * z[34]) / T(2);
z[76] = z[6] + z[7];
z[78] = z[75] + -z[76];
z[78] = z[1] * z[78];
z[79] = z[3] + -z[34];
z[80] = z[22] / T(2);
z[79] = z[79] * z[80];
z[60] = T(2) * z[14] + z[60];
z[81] = z[21] * z[60];
z[82] = z[70] + -z[81];
z[83] = -z[10] + z[54];
z[83] = z[4] * z[83];
z[52] = -z[52] + -z[78] + -z[79] + z[82] + z[83];
z[52] = z[24] * z[52];
z[78] = z[29] * z[60];
z[84] = T(2) * z[10];
z[85] = (T(3) * z[53]) / T(2) + -z[84];
z[86] = z[26] * z[85];
z[78] = z[78] + z[86];
z[86] = z[3] * z[84];
z[86] = z[78] + z[86];
z[87] = z[6] / T(2);
z[88] = T(2) * z[3];
z[89] = z[87] + z[88];
z[51] = -z[51] + z[89];
z[51] = -(z[51] * z[53]);
z[90] = -z[10] + z[53];
z[91] = T(2) * z[4];
z[92] = z[90] * z[91];
z[93] = z[5] * z[54];
z[93] = z[81] + z[93];
z[51] = -z[51] + -z[86] + z[92] + z[93];
z[92] = z[0] * z[51];
z[94] = -z[3] + z[76];
z[95] = z[22] * z[94];
z[96] = z[1] * z[94];
z[95] = z[95] + -z[96];
z[91] = z[69] * z[91];
z[55] = z[55] + -z[84];
z[97] = z[31] * z[55];
z[97] = -z[71] + z[97];
z[98] = T(2) * z[69];
z[98] = z[5] * z[98];
z[91] = z[91] + -z[95] / T(2) + z[97] + z[98];
z[91] = z[23] * z[91];
z[52] = z[52] + z[91] + z[92];
z[51] = -(z[25] * z[51]);
z[67] = z[3] * z[67];
z[95] = -(z[5] * z[69]);
z[67] = z[67] + -z[68] + z[70] + z[95] + -z[96];
z[95] = int_to_imaginary<T>(1) * z[28];
z[67] = z[67] * z[95];
z[96] = z[0] + -z[25];
z[96] = z[60] * z[96];
z[98] = z[23] * z[60];
z[96] = z[96] + -z[98];
z[96] = z[20] * z[96];
z[51] = z[51] + z[52] + z[66] + T(2) * z[67] + z[96];
z[51] = z[27] * z[51];
z[66] = z[11] * z[60];
z[67] = T(3) * z[2];
z[96] = -z[7] + -z[63] + z[67];
z[96] = z[1] * z[96];
z[99] = z[3] + -z[7];
z[100] = -z[2] + z[99];
z[101] = z[22] * z[100];
z[96] = z[96] + z[101];
z[101] = -(z[4] * z[55]);
z[102] = -z[3] + z[57];
z[103] = z[9] * z[102];
z[104] = z[10] + T(2) * z[30] + z[54];
z[105] = z[5] * z[104];
z[96] = z[66] + z[81] + z[96] / T(2) + z[97] + z[101] + z[103] + z[105];
z[96] = z[35] * z[96];
z[101] = (T(5) * z[3]) / T(2) + -z[7] + z[8] / T(2) + z[87];
z[101] = z[53] * z[101];
z[103] = z[6] + -z[8];
z[105] = -z[3] + z[103];
z[106] = z[84] * z[105];
z[107] = z[54] + z[84];
z[107] = z[5] * z[107];
z[108] = (T(5) * z[53]) / T(2) + -z[84];
z[108] = z[4] * z[108];
z[101] = -z[78] + z[81] + z[101] + z[106] + z[107] + z[108];
z[101] = z[37] * z[101];
z[63] = -z[7] + z[63];
z[106] = z[6] + z[34] + z[63];
z[106] = z[80] * z[106];
z[107] = -z[34] + z[94];
z[108] = (T(3) * z[1]) / T(2);
z[107] = z[107] * z[108];
z[85] = z[4] * z[85];
z[85] = z[85] + -z[86] + z[106] + z[107];
z[86] = z[41] * z[85];
z[106] = T(3) * z[1] + -z[22];
z[76] = -z[34] + z[76];
z[76] = z[76] * z[106];
z[55] = z[5] * z[55];
z[104] = z[4] * z[104];
z[55] = -z[55] + z[76] / T(2) + z[82] + z[97] + z[104];
z[76] = z[39] * z[55];
z[72] = -z[5] + -z[6] + (T(-2) * z[7]) / T(3) + z[72];
z[64] = z[26] / T(9) + -z[29] / T(4) + -z[64] + z[72] / T(3) + -z[77];
z[72] = prod_pow(z[28], 2);
z[64] = z[64] * z[72];
z[77] = z[4] * z[54];
z[82] = z[77] + z[93];
z[75] = T(-2) * z[6] + -z[7] + z[75];
z[75] = z[1] * z[75];
z[75] = z[75] + z[79] + z[82];
z[79] = z[25] * z[75];
z[93] = -z[45] + z[47] / T(2);
z[93] = z[47] * z[93];
z[93] = z[48] + z[93];
z[97] = T(3) * z[21] + -z[29];
z[93] = z[93] * z[97];
z[97] = prod_pow(z[45], 2);
z[97] = z[97] / T(2);
z[104] = z[21] * z[97];
z[52] = -z[52] + z[64] / T(3) + z[76] + z[79] + z[86] + z[93] + z[104];
z[52] = z[52] * z[95];
z[64] = -(z[0] * z[60]);
z[76] = z[25] * z[61];
z[64] = z[64] + z[76] + z[98];
z[64] = z[25] * z[64];
z[76] = z[24] * z[61];
z[76] = z[76] + -z[98];
z[76] = z[24] * z[76];
z[79] = z[0] * z[61];
z[79] = z[79] + -z[98];
z[79] = z[0] * z[79];
z[86] = z[97] + z[98];
z[93] = -z[0] + -z[39] + z[41];
z[93] = z[60] * z[93];
z[97] = z[72] / T(12);
z[93] = z[86] + z[93] + z[97];
z[93] = z[93] * z[95];
z[104] = -z[35] + z[38];
z[106] = z[36] + z[40] + z[104];
z[106] = z[60] * z[106];
z[58] = (T(-2) * z[14]) / T(3) + z[45] / T(4) + -z[58];
z[58] = z[58] * z[72];
z[58] = (T(-5) * z[46]) / T(8) + z[58] + z[64] + -z[76] + z[79] + z[93] + z[106];
z[58] = z[20] * z[58];
z[64] = z[3] + z[7];
z[79] = -z[64] + z[67];
z[79] = -z[6] + z[79] / T(2);
z[79] = z[1] * z[79];
z[93] = z[80] * z[100];
z[100] = z[9] * z[57];
z[70] = -z[70] + z[71] + -z[79] + z[83] + -z[93] + -z[100];
z[71] = z[11] * z[61];
z[56] = z[56] + z[65] + -z[70] / T(2) + z[71];
z[56] = z[24] * z[56];
z[56] = z[56] + -z[91];
z[56] = z[24] * z[56];
z[57] = z[57] + -z[88];
z[57] = z[9] * z[57];
z[57] = z[57] + z[66];
z[65] = (T(-3) * z[2]) / T(2) + z[7] + z[89];
z[65] = z[1] * z[65];
z[66] = z[2] + -z[6];
z[66] = z[66] * z[80];
z[65] = -z[57] + z[65] + z[66] + -z[82];
z[65] = z[24] * z[65];
z[66] = z[4] * z[90];
z[70] = z[5] * z[74];
z[62] = z[62] + z[66] + z[70];
z[63] = -z[63] + z[67] + -z[103];
z[63] = z[1] * z[63];
z[66] = -z[2] + -z[8] + z[94];
z[66] = z[22] * z[66];
z[63] = z[63] + z[66];
z[67] = z[8] * z[10];
z[70] = z[2] + z[7];
z[70] = -z[3] + z[70] / T(2);
z[70] = z[9] * z[70];
z[63] = z[62] + z[63] / T(4) + z[67] + z[70] + z[71];
z[63] = z[0] * z[63];
z[67] = z[8] + z[99];
z[53] = -(z[53] * z[67]);
z[67] = z[84] * z[103];
z[70] = z[5] * z[84];
z[53] = -z[53] / T(2) + z[67] + z[70] + z[77];
z[53] = z[23] * z[53];
z[63] = z[53] + z[63] + z[65];
z[63] = z[0] * z[63];
z[65] = T(3) * z[102] + -z[103];
z[65] = z[1] * z[65];
z[65] = z[65] + z[66];
z[54] = z[54] + -z[84];
z[54] = z[5] * z[54];
z[54] = z[54] + z[57] + z[65] / T(2) + -z[67] + z[81];
z[54] = z[36] * z[54];
z[50] = z[3] + z[50];
z[57] = z[7] + -z[73];
z[57] = -z[50] + z[57] / T(4) + z[87];
z[57] = z[1] * z[57];
z[65] = T(-3) * z[7] + z[34];
z[50] = z[50] + z[65] / T(4) + z[87];
z[50] = z[22] * z[50];
z[65] = z[10] * z[105];
z[50] = z[50] + z[57] + z[62] + z[65] + -z[68] + -z[78];
z[50] = z[25] * z[50];
z[57] = z[24] * z[75];
z[50] = z[50] + -z[53] + z[57] + -z[92];
z[50] = z[25] * z[50];
z[53] = -(z[39] * z[60]);
z[53] = z[53] + z[86] + -z[97];
z[53] = z[53] * z[95];
z[57] = z[27] * z[61];
z[57] = z[57] + -z[98];
z[57] = z[27] * z[57];
z[60] = z[60] * z[104];
z[59] = (T(5) * z[45]) / T(4) + -z[59];
z[59] = z[59] * z[72];
z[53] = -z[46] / T(8) + z[53] + z[57] + z[59] + z[60] + -z[76];
z[53] = z[32] * z[53];
z[57] = z[40] * z[85];
z[55] = -(z[38] * z[55]);
z[59] = -z[1] + -z[9];
z[59] = z[59] * z[64];
z[60] = -(z[6] * z[10]);
z[61] = z[4] * z[69];
z[59] = z[59] + z[60] + z[61];
z[59] = prod_pow(z[23], 2) * z[59];
z[60] = -(z[28] * z[43]);
z[61] = int_to_imaginary<T>(1) * prod_pow(z[28], 3);
z[60] = z[60] + z[61] / T(27);
z[60] = z[44] * z[60];
z[61] = (T(-9) * z[21]) / T(8) + (T(-8) * z[29]) / T(3);
z[61] = z[46] * z[61];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + T(4) * z[60] + z[61] + z[63] + z[96] + z[101];
}



template IntegrandConstructorType<double> f_4_184_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_184_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_184_construct (const Kin<qd_real>&);
#endif

}