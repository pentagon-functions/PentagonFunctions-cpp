#include "f_4_115.h"

namespace PentagonFunctions {

template <typename T> T f_4_115_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_115_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_115_W_21 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[0] / T(4) + -kin.v[3] / T(2) + kin.v[1] / T(2) + kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[0] + (-kin.v[2] / T(2) + (T(3) * kin.v[1]) / T(4) + (T(-3) * kin.v[3]) / T(2) + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(4) + kin.v[3] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(4) + T(-1) + kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((-kin.v[0] / T(4) + -kin.v[3] / T(2) + kin.v[1] / T(2) + kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[0] + (-kin.v[2] / T(2) + (T(3) * kin.v[1]) / T(4) + (T(-3) * kin.v[3]) / T(2) + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(4) + kin.v[3] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(4) + T(-1) + kin.v[4])) + rlog(kin.v[2]) * ((-kin.v[3] / T(2) + kin.v[2] / T(4) + -kin.v[4] + T(1)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (-kin.v[1] / T(2) + -kin.v[2] / T(2) + kin.v[0] / T(4) + kin.v[3] / T(2) + T(-1) + kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + T(-1) + kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-kin.v[3] / T(2) + kin.v[2] / T(4) + -kin.v[4] + T(1)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (-kin.v[1] / T(2) + -kin.v[2] / T(2) + kin.v[0] / T(4) + kin.v[3] / T(2) + T(-1) + kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + T(-1) + kin.v[4]));
c[1] = rlog(kin.v[2]) * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[1]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[1]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (prod_pow(abb[2], 2) * (abb[7] / T(2) + -abb[5] / T(2) + -abb[6] / T(2)) + abb[4] * (prod_pow(abb[2], 2) / T(2) + -abb[3]) + abb[1] * (abb[2] * abb[4] + abb[1] * ((abb[4] * T(-3)) / T(2) + (abb[7] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2) + (abb[6] * T(3)) / T(2)) + abb[2] * (abb[7] + -abb[5] + -abb[6])) + abb[3] * (abb[5] + abb[6] + -abb[7]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_115_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl8 = DLog_W_8<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),spdl21 = SpDLog_f_4_115_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl8(t), rlog(v_path[2]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl19(t), -rlog(t), dl16(t), dl3(t), dl20(t)}
;

        auto result = f_4_115_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_4_115_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[42];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[12];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[13];
z[10] = abb[2];
z[11] = abb[9];
z[12] = bc<TR>[0];
z[13] = abb[8];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = int_to_imaginary<T>(1) * z[12];
z[20] = -z[11] + z[19];
z[21] = z[0] / T(2);
z[22] = z[20] + z[21];
z[22] = z[0] * z[22];
z[23] = z[16] * z[19];
z[23] = -z[15] + z[23];
z[24] = T(-3) * z[23];
z[25] = T(3) * z[14];
z[26] = prod_pow(z[11], 2);
z[27] = prod_pow(z[12], 2);
z[28] = T(3) * z[22] + z[24] + z[25] + (T(3) * z[26]) / T(2) + -z[27] / T(2);
z[28] = z[3] * z[28];
z[22] = -z[14] + -z[22] + z[23] + -z[26] / T(2) + z[27] / T(6);
z[29] = z[5] * z[22];
z[30] = T(2) * z[19];
z[31] = (T(3) * z[11]) / T(2) + -z[30];
z[31] = z[11] * z[31];
z[31] = -z[23] + z[31];
z[32] = z[10] / T(2);
z[33] = z[20] + -z[32];
z[33] = z[10] * z[33];
z[34] = (T(4) * z[27]) / T(3) + -z[31] + -z[33];
z[34] = z[13] * z[34];
z[35] = T(2) * z[20];
z[36] = z[21] + z[35];
z[36] = z[0] * z[36];
z[25] = z[25] + z[36];
z[36] = z[0] + z[35];
z[37] = -z[32] + z[36];
z[37] = z[10] * z[37];
z[38] = z[27] / T(3);
z[37] = -z[25] + z[37] + -z[38];
z[37] = z[4] * z[37];
z[39] = z[11] / T(2);
z[40] = z[30] + z[39];
z[40] = z[11] * z[40];
z[24] = z[24] + z[38] + z[40];
z[40] = T(-2) * z[0] + -z[20] + -z[32];
z[40] = z[10] * z[40];
z[41] = -(z[0] * z[35]);
z[40] = -z[24] + z[40] + z[41];
z[40] = z[2] * z[40];
z[28] = z[28] + z[29] + z[34] + z[37] + z[40];
z[28] = z[8] * z[28];
z[29] = z[0] * z[36];
z[36] = T(2) * z[14];
z[29] = z[29] + z[36];
z[30] = z[30] + -z[39];
z[30] = z[11] * z[30];
z[30] = -z[23] + z[27] + z[29] + z[30] + -z[33];
z[30] = z[2] * z[30];
z[33] = z[3] + z[5];
z[22] = z[22] * z[33];
z[33] = T(2) * z[23] + -z[26] + z[38];
z[21] = -z[21] + z[35];
z[21] = z[0] * z[21];
z[32] = z[0] + z[32];
z[32] = z[10] * z[32];
z[21] = z[14] + z[21] + z[32] + -z[33];
z[21] = z[4] * z[21];
z[21] = z[21] + z[22] + z[30] + -z[34];
z[21] = z[6] * z[21];
z[22] = z[23] + z[38];
z[23] = T(2) * z[10];
z[20] = z[0] + z[20];
z[23] = z[20] * z[23];
z[30] = prod_pow(z[0], 2);
z[22] = T(2) * z[22] + -z[23] + -z[26] + z[30] + z[36];
z[23] = z[2] + -z[4];
z[22] = z[22] * z[23];
z[26] = z[29] + -z[33];
z[29] = z[3] * z[26];
z[22] = z[22] + z[29];
z[29] = z[22] + z[34];
z[29] = z[7] * z[29];
z[30] = z[22] + -z[34];
z[30] = z[1] * z[30];
z[26] = z[5] * z[26];
z[22] = -z[22] + z[26];
z[22] = z[9] * z[22];
z[20] = z[10] * z[20];
z[25] = z[20] + -z[25] + z[27] + -z[31];
z[25] = z[1] * z[25];
z[26] = (T(-3) * z[0]) / T(2) + -z[35];
z[26] = z[0] * z[26];
z[20] = -z[14] + -z[20] + -z[24] + z[26];
z[20] = z[7] * z[20];
z[19] = z[17] * z[19];
z[19] = z[19] + (T(5) * z[27]) / T(2);
z[19] = z[17] * z[19];
z[24] = int_to_imaginary<T>(1) * prod_pow(z[12], 3);
z[26] = -z[19] + z[24] / T(6);
z[20] = z[20] + z[25] + -z[26] / T(2);
z[20] = z[5] * z[20];
z[19] = -z[19] + z[24] / T(3);
z[19] = -(z[19] * z[23]);
z[24] = z[3] * z[26];
z[19] = z[19] + z[24];
z[23] = z[3] + -z[5] + T(-21) * z[23];
z[23] = z[18] * z[23];
return z[19] / T(2) + z[20] + z[21] + T(2) * z[22] + z[23] / T(8) + z[28] + z[29] + z[30];
}



template IntegrandConstructorType<double> f_4_115_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_115_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_115_construct (const Kin<qd_real>&);
#endif

}