#include "f_4_289.h"

namespace PentagonFunctions {

template <typename T> T f_4_289_abbreviated (const std::array<T,55>&);

template <typename T> class SpDLog_f_4_289_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_289_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]);
c[1] = kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[2], 2) * abb[4] * T(3) + prod_pow(abb[1], 2) * (abb[4] * T(-3) + abb[3] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_289_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_289_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]);
c[1] = kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]);
c[2] = kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]);
c[3] = kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[9] * c[1] + abb[4] * c[2] + abb[8] * c[3]);
        }

        return abb[5] * (abb[3] * prod_pow(abb[7], 2) * T(-3) + prod_pow(abb[7], 2) * (abb[9] * T(-3) + abb[4] * T(3) + abb[8] * T(3)) + prod_pow(abb[6], 2) * (abb[4] * T(-3) + abb[8] * T(-3) + abb[3] * T(3) + abb[9] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_289_W_10 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_289_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[2] * ((T(11) * kin.v[2]) / T(4) + T(1) + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * ((T(11) * kin.v[2]) / T(2) + T(1) + (T(11) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[2] + T(4) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[1] + T(3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[2];
c[2] = kin.v[2] * ((T(11) * kin.v[2]) / T(4) + T(1) + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * ((T(11) * kin.v[2]) / T(2) + T(1) + (T(11) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[2] + T(4) * kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[1] + T(3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[2];
c[4] = kin.v[2] * ((T(11) * kin.v[2]) / T(4) + T(1) + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * ((T(11) * kin.v[2]) / T(2) + T(1) + (T(11) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[2] + T(4) * kin.v[4]));
c[5] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[1] + T(3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[20] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]);
        }

        return abb[18] * (abb[19] * (abb[9] * T(-3) + abb[20] * T(-3)) + abb[11] * (abb[11] * (-abb[9] / T(2) + -abb[20] / T(2)) + abb[12] * (abb[9] * T(-4) + abb[20] * T(-4)) + abb[9] * bc<T>[0] * int_to_imaginary<T>(4) + abb[20] * bc<T>[0] * int_to_imaginary<T>(4)) + abb[3] * (abb[19] * T(-3) + abb[11] * (-abb[11] / T(2) + abb[12] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[6] * abb[11] * T(4)) + abb[6] * abb[11] * (abb[9] * T(4) + abb[20] * T(4)) + abb[2] * (abb[2] * ((abb[3] * T(-5)) / T(2) + (abb[9] * T(-5)) / T(2) + (abb[20] * T(-5)) / T(2)) + abb[6] * (abb[9] * T(-4) + abb[20] * T(-4)) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[20] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[11] * (abb[9] * T(3) + abb[20] * T(3)) + abb[3] * (abb[6] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[11] * T(3) + abb[12] * T(4)) + abb[12] * (abb[9] * T(4) + abb[20] * T(4))));
    }
};
template <typename T> class SpDLog_f_4_289_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_289_W_7 (const Kin<T>& kin) {
        c[0] = -(kin.v[0] * kin.v[4]) + T(2) * kin.v[2] * kin.v[4] + prod_pow(kin.v[4], 2) / T(2) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] / T(2) + -kin.v[4] + T(1) + kin.v[1]) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];
c[2] = -(kin.v[0] * kin.v[4]) + T(2) * kin.v[2] * kin.v[4] + prod_pow(kin.v[4], 2) / T(2) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] / T(2) + -kin.v[4] + T(1) + kin.v[1]) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]);
c[3] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];
c[4] = -(kin.v[0] * kin.v[4]) + T(2) * kin.v[2] * kin.v[4] + prod_pow(kin.v[4], 2) / T(2) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] / T(2) + -kin.v[4] + T(1) + kin.v[1]) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]);
c[5] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[20] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[8] * (t * c[4] + c[5]);
        }

        return abb[21] * ((abb[4] + abb[8]) * abb[11] * abb[13] + abb[2] * abb[13] * (-abb[4] + -abb[8]) + abb[12] * abb[13] * (-abb[4] + -abb[8]) + abb[22] * (abb[4] * T(-2) + abb[8] * T(-2)) + abb[1] * (abb[2] * (abb[4] + abb[8]) + (abb[4] + abb[8]) * abb[12] + (abb[4] + abb[8]) * abb[13] + abb[11] * (-abb[4] + -abb[8]) + abb[1] * (abb[4] * T(-2) + abb[8] * T(-2) + abb[20] * T(-2)) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[20] * (abb[2] + abb[12] + abb[13] + -abb[11] + bc<T>[0] * int_to_imaginary<T>(-1))) + abb[13] * ((abb[4] + abb[8]) * abb[13] + abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[20] * (abb[11] * abb[13] + -(abb[2] * abb[13]) + -(abb[12] * abb[13]) + abb[22] * T(-2) + abb[13] * (abb[13] + bc<T>[0] * int_to_imaginary<T>(1))));
    }
};
template <typename T> class SpDLog_f_4_289_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_289_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-4) * kin.v[3]) + (T(-5) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[1] * (-kin.v[1] / T(2) + T(2) * kin.v[2] + T(3) * kin.v[3]) + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(-2) * kin.v[1] + T(3) * kin.v[2] + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + T(1) + kin.v[2]) + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(-2) * kin.v[3];
c[2] = kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-4) * kin.v[3]) + (T(-5) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[1] * (-kin.v[1] / T(2) + T(2) * kin.v[2] + T(3) * kin.v[3]) + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(-2) * kin.v[1] + T(3) * kin.v[2] + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + T(1) + kin.v[2]) + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(-2) * kin.v[3];
c[4] = kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(2) * kin.v[1] + T(-3) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[1] * (kin.v[1] / T(2) + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + (T(5) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[5] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]);
c[6] = kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(2) * kin.v[1] + T(-3) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[1] * (kin.v[1] / T(2) + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + (T(5) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[7] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[20] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[8] * (t * c[4] + c[5]) + abb[9] * (t * c[6] + c[7]);
        }

        return abb[23] * (abb[7] * abb[12] * (abb[8] + abb[9] + -abb[4]) + abb[7] * abb[11] * (abb[4] + -abb[8] + -abb[9]) + abb[6] * (abb[7] * (abb[8] + abb[9] + -abb[4]) + abb[13] * (abb[4] + abb[20] + -abb[8] + -abb[9]) + -(abb[7] * abb[20])) + abb[7] * (abb[7] * (abb[4] + -abb[8] + -abb[9]) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[20] * (abb[7] * abb[11] + -(abb[7] * abb[12]) + abb[24] * T(-2) + abb[7] * (abb[7] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[24] * (abb[4] * T(-2) + abb[8] * T(2) + abb[9] * T(2)) + abb[13] * (abb[11] * (abb[8] + abb[9] + -abb[4]) + abb[7] * (abb[4] + -abb[8] + -abb[9]) + abb[12] * (abb[4] + -abb[8] + -abb[9]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[20] * (abb[7] + abb[12] + -abb[11] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1) + abb[13] * (abb[4] * T(-2) + abb[20] * T(-2) + abb[8] * T(2) + abb[9] * T(2))));
    }
};
template <typename T> class SpDLog_f_4_289_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_289_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * ((T(13) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(-13) * kin.v[4]) / T(2) + -kin.v[1] + T(1) + (T(-15) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[1] + T(4) * kin.v[3])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + ((T(-11) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + ((T(-11) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(2) + (T(11) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2) + T(-1)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]);
c[2] = kin.v[0] * ((T(13) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(-13) * kin.v[4]) / T(2) + -kin.v[1] + T(1) + (T(-15) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[1] + T(4) * kin.v[3])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + ((T(-11) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + ((T(-11) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(2) + (T(11) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2) + T(-1)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]);
c[4] = ((T(15) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2) + T(1)) * kin.v[3] + ((T(11) * kin.v[2]) / T(4) + (T(13) * kin.v[3]) / T(2) + (T(-11) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + ((T(11) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * ((T(-13) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(13) * kin.v[4]) / T(2) + T(-1) + (T(15) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[1] + T(-4) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));
c[5] = (T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(-3) * kin.v[4];
c[6] = ((T(15) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2) + T(1)) * kin.v[3] + ((T(11) * kin.v[2]) / T(4) + (T(13) * kin.v[3]) / T(2) + (T(-11) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + ((T(11) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * ((T(-13) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(13) * kin.v[4]) / T(2) + T(-1) + (T(15) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[1] + T(-4) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));
c[7] = (T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(-3) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[20] * (t * c[2] + c[3]) + abb[8] * (t * c[4] + c[5]) + abb[9] * (t * c[6] + c[7]);
        }

        return abb[41] * (abb[26] * (abb[8] * T(-3) + abb[9] * T(-3) + abb[20] * T(3)) + abb[6] * (abb[6] * ((abb[8] * T(-5)) / T(2) + (abb[9] * T(-5)) / T(2) + (abb[3] * T(5)) / T(2) + (abb[20] * T(5)) / T(2)) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[20] * bc<T>[0] * int_to_imaginary<T>(4) + abb[3] * (abb[12] * T(-4) + abb[11] * T(-3) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[11] * (abb[20] * T(-3) + abb[8] * T(3) + abb[9] * T(3)) + abb[12] * (abb[20] * T(-4) + abb[8] * T(4) + abb[9] * T(4))) + abb[3] * (abb[26] * T(3) + abb[11] * (abb[11] / T(2) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[12] * T(4))) + abb[11] * (abb[11] * (abb[20] / T(2) + -abb[8] / T(2) + -abb[9] / T(2)) + abb[20] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[8] * bc<T>[0] * int_to_imaginary<T>(4) + abb[9] * bc<T>[0] * int_to_imaginary<T>(4) + abb[12] * (abb[8] * T(-4) + abb[9] * T(-4) + abb[20] * T(4))) + abb[2] * (abb[3] * abb[11] * T(-4) + abb[11] * (abb[20] * T(-4) + abb[8] * T(4) + abb[9] * T(4)) + abb[6] * (abb[8] * T(-4) + abb[9] * T(-4) + abb[3] * T(4) + abb[20] * T(4))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_289_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl22 = DLog_W_22<T>(kin),dl1 = DLog_W_1<T>(kin),dl10 = DLog_W_10<T>(kin),dl7 = DLog_W_7<T>(kin),dl21 = DLog_W_21<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl23 = DLog_W_23<T>(kin),dl26 = DLog_W_26<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl31 = DLog_W_31<T>(kin),dl18 = DLog_W_18<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),spdl12 = SpDLog_f_4_289_W_12<T>(kin),spdl22 = SpDLog_f_4_289_W_22<T>(kin),spdl10 = SpDLog_f_4_289_W_10<T>(kin),spdl7 = SpDLog_f_4_289_W_7<T>(kin),spdl21 = SpDLog_f_4_289_W_21<T>(kin),spdl23 = SpDLog_f_4_289_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,55> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl1(t), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_10(kin_path), f_2_1_12(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl10(t), f_2_1_7(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), dl7(t), f_2_1_11(kin_path), dl21(t), f_2_1_15(kin_path), dl27(t) / kin_path.SqrtDelta, f_2_1_8(kin_path), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), f_2_2_7(kin_path), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[15] / kin_path.W[15]), dl28(t) / kin_path.SqrtDelta, f_2_1_4(kin_path), f_2_1_14(kin_path), dl23(t), dl26(t) / kin_path.SqrtDelta, dl20(t), dl2(t), dl19(t), dl16(t), dl5(t), dl17(t), dl31(t), dl18(t), dl3(t), dl4(t), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_4_289_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_289_abbreviated(const std::array<T,55>& abb)
{
using TR = typename T::value_type;
T z[194];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[43];
z[3] = abb[44];
z[4] = abb[45];
z[5] = abb[47];
z[6] = abb[48];
z[7] = abb[50];
z[8] = abb[51];
z[9] = abb[52];
z[10] = abb[4];
z[11] = abb[8];
z[12] = abb[20];
z[13] = abb[25];
z[14] = abb[27];
z[15] = abb[28];
z[16] = abb[29];
z[17] = abb[30];
z[18] = abb[31];
z[19] = abb[33];
z[20] = abb[34];
z[21] = abb[35];
z[22] = abb[36];
z[23] = abb[42];
z[24] = abb[53];
z[25] = abb[38];
z[26] = abb[2];
z[27] = abb[7];
z[28] = abb[11];
z[29] = abb[12];
z[30] = abb[13];
z[31] = bc<TR>[0];
z[32] = abb[46];
z[33] = abb[9];
z[34] = abb[54];
z[35] = abb[6];
z[36] = abb[19];
z[37] = abb[22];
z[38] = abb[24];
z[39] = abb[32];
z[40] = abb[26];
z[41] = abb[39];
z[42] = abb[40];
z[43] = abb[14];
z[44] = abb[15];
z[45] = abb[16];
z[46] = abb[17];
z[47] = abb[37];
z[48] = abb[10];
z[49] = abb[49];
z[50] = bc<TR>[3];
z[51] = bc<TR>[1];
z[52] = bc<TR>[5];
z[53] = bc<TR>[9];
z[54] = bc<TR>[2];
z[55] = bc<TR>[4];
z[56] = z[17] + z[18];
z[56] = z[15] + z[56] / T(2);
z[57] = T(3) * z[56];
z[58] = z[16] / T(2);
z[59] = z[57] + -z[58];
z[60] = (T(3) * z[14]) / T(4);
z[61] = -z[19] + z[60];
z[62] = (T(3) * z[47]) / T(4);
z[59] = -z[20] / T(4) + -z[22] + z[59] / T(2) + z[61] + z[62];
z[63] = -z[24] + -z[34];
z[63] = z[59] * z[63];
z[64] = (T(7) * z[16]) / T(2);
z[65] = z[57] + z[64];
z[60] = T(5) * z[19] + -z[60];
z[65] = (T(7) * z[20]) / T(4) + z[22] + -z[60] + z[62] + z[65] / T(2);
z[66] = z[13] + z[25];
z[66] = z[65] * z[66];
z[67] = T(9) * z[10];
z[68] = z[12] + z[67];
z[69] = T(3) * z[11];
z[70] = z[68] + z[69];
z[71] = (T(9) * z[33]) / T(4);
z[72] = T(2) * z[1];
z[73] = z[70] / T(4) + -z[71] + -z[72];
z[73] = z[8] * z[73];
z[74] = z[33] / T(4);
z[75] = T(7) * z[10];
z[76] = -z[12] + z[75];
z[77] = T(-13) * z[11] + -z[76];
z[77] = z[72] + -z[74] + z[77] / T(4);
z[77] = z[4] * z[77];
z[78] = T(5) * z[10];
z[79] = T(3) * z[12];
z[80] = z[78] + -z[79];
z[81] = z[69] + z[80];
z[81] = -z[72] + -z[74] + z[81] / T(4);
z[81] = z[7] * z[81];
z[82] = T(5) * z[12];
z[83] = z[10] + z[82];
z[84] = -z[11] + z[83];
z[85] = -z[1] + z[74] + -z[84] / T(4);
z[86] = -(z[32] * z[85]);
z[87] = z[11] + z[33];
z[88] = z[10] + z[12];
z[89] = -z[87] + -z[88];
z[89] = z[6] * z[89];
z[90] = T(5) * z[11];
z[91] = -z[83] + z[90];
z[92] = (T(3) * z[33]) / T(4);
z[93] = z[1] + z[92];
z[94] = -z[91] / T(4) + z[93];
z[94] = z[3] * z[94];
z[95] = -z[10] + z[79];
z[96] = z[11] + z[95];
z[97] = z[1] + z[96] / T(4);
z[98] = -z[74] + -z[97];
z[98] = z[2] * z[98];
z[99] = T(2) * z[10] + z[12];
z[100] = -z[1] + z[99];
z[101] = -(z[5] * z[100]);
z[63] = z[63] + z[66] + z[73] + z[77] + z[81] + z[86] + z[89] / T(2) + z[94] + z[98] + z[101];
z[63] = z[30] * z[63];
z[66] = T(2) * z[33];
z[73] = z[1] + -z[11];
z[77] = z[66] + z[73] + -z[99];
z[81] = z[32] * z[77];
z[86] = (T(3) * z[14]) / T(2);
z[89] = T(2) * z[19];
z[94] = z[58] + -z[86] + z[89];
z[57] = (T(3) * z[47]) / T(2) + z[57];
z[98] = T(2) * z[22];
z[99] = -z[20] / T(2) + z[57] + -z[94] + -z[98];
z[101] = z[23] * z[99];
z[86] = T(-10) * z[19] + z[64] + z[86];
z[57] = (T(7) * z[20]) / T(2) + z[57] + z[86] + z[98];
z[102] = z[13] * z[57];
z[103] = z[24] * z[99];
z[104] = (T(9) * z[33]) / T(2);
z[105] = z[72] + z[104];
z[67] = z[67] + z[82];
z[106] = -z[67] + -z[69];
z[106] = z[105] + z[106] / T(2);
z[106] = z[8] * z[106];
z[107] = z[1] + z[12];
z[108] = -z[11] + z[107];
z[109] = z[9] * z[108];
z[110] = T(2) * z[109];
z[111] = z[16] + z[20] + z[22] + -z[89];
z[112] = T(4) * z[111];
z[113] = z[25] * z[112];
z[114] = z[34] * z[112];
z[106] = T(-2) * z[81] + -z[101] + -z[102] + z[103] + z[106] + z[110] + -z[113] + z[114];
z[106] = z[35] * z[106];
z[114] = z[33] * z[48];
z[115] = z[20] + -z[21] + -z[22] + z[47];
z[116] = z[13] * z[115];
z[117] = z[114] + z[116];
z[118] = -z[103] + (T(3) * z[117]) / T(2);
z[119] = z[34] * z[99];
z[120] = z[33] / T(2);
z[121] = -z[72] + z[120];
z[84] = z[84] / T(2) + -z[121];
z[122] = z[32] * z[84];
z[122] = -z[119] + z[122];
z[123] = z[72] + z[120];
z[96] = z[96] / T(2);
z[124] = z[96] + z[123];
z[124] = z[2] * z[124];
z[125] = z[90] + -z[95];
z[126] = z[125] / T(2);
z[127] = -z[72] + z[126];
z[127] = z[3] * z[127];
z[128] = T(2) * z[4];
z[128] = z[87] * z[128];
z[129] = -z[69] + z[83];
z[129] = z[72] + z[129] / T(2);
z[130] = z[5] * z[129];
z[131] = -z[11] + z[120];
z[132] = z[7] * z[131];
z[133] = z[6] * z[87];
z[124] = -z[118] + -z[122] + z[124] + z[127] + z[128] + z[130] + z[132] + z[133];
z[127] = z[29] * z[124];
z[128] = z[21] / T(2) + z[56];
z[132] = T(3) * z[128];
z[134] = T(2) * z[20];
z[86] = (T(7) * z[22]) / T(2) + z[86] + z[132] + z[134];
z[135] = z[13] * z[86];
z[136] = -z[11] + z[95];
z[137] = z[136] / T(2);
z[138] = z[72] + z[137];
z[139] = z[7] * z[138];
z[139] = -z[135] + z[139];
z[94] = -z[22] / T(2) + -z[94] + z[132] + -z[134];
z[140] = z[23] * z[94];
z[141] = z[4] + z[5];
z[142] = T(2) * z[100];
z[142] = -(z[141] * z[142]);
z[143] = z[6] * z[138];
z[67] = z[11] + z[67];
z[67] = z[67] / T(2) + -z[72];
z[67] = z[9] * z[67];
z[144] = T(2) * z[107];
z[145] = z[8] * z[144];
z[113] = z[67] + z[113] + -z[139] + z[140] + z[142] + z[143] + -z[145];
z[142] = z[0] * z[113];
z[143] = z[11] + z[88];
z[146] = z[3] * z[143];
z[113] = z[113] + z[146] / T(2);
z[113] = z[26] * z[113];
z[99] = z[25] * z[99];
z[101] = -z[99] + z[101];
z[146] = z[101] + -z[122];
z[147] = z[24] * z[112];
z[145] = z[145] + z[147];
z[147] = -z[110] + z[145] + z[146];
z[147] = z[27] * z[147];
z[148] = -z[69] + z[95];
z[149] = z[148] / T(2);
z[150] = z[123] + z[149];
z[151] = z[35] * z[150];
z[91] = -z[91] / T(2) + -z[121];
z[91] = z[27] * z[91];
z[91] = z[91] + -z[151];
z[91] = z[6] * z[91];
z[152] = z[35] * z[77];
z[153] = z[27] * z[84];
z[152] = T(-2) * z[152] + -z[153];
z[152] = z[4] * z[152];
z[154] = T(2) * z[27];
z[155] = -z[10] + z[11];
z[156] = z[1] + z[155];
z[157] = z[33] + z[156];
z[157] = z[154] * z[157];
z[157] = z[151] + z[157];
z[157] = z[7] * z[157];
z[158] = z[0] / T(2);
z[159] = z[143] * z[158];
z[160] = T(3) * z[10];
z[161] = -z[12] + z[160];
z[162] = -z[69] + z[161];
z[163] = -z[72] + z[162] / T(2);
z[164] = (T(3) * z[33]) / T(2);
z[165] = z[163] + -z[164];
z[165] = z[27] * z[165];
z[166] = z[35] * z[108];
z[167] = T(2) * z[166];
z[165] = -z[159] + z[165] + -z[167];
z[165] = z[3] * z[165];
z[168] = -z[0] + z[26];
z[168] = z[144] * z[168];
z[169] = T(2) * z[11];
z[170] = -z[10] + z[33];
z[171] = z[169] + -z[170];
z[172] = z[12] + z[72];
z[173] = -z[171] + z[172];
z[174] = z[154] * z[173];
z[155] = -z[12] + z[155];
z[175] = z[33] + z[155];
z[176] = z[35] * z[175];
z[168] = z[168] + z[174] + z[176] / T(2);
z[168] = z[2] * z[168];
z[63] = z[63] + z[91] + z[106] + z[113] + z[127] + -z[142] + z[147] + z[152] + z[157] + z[165] + z[168];
z[63] = z[30] * z[63];
z[91] = z[88] / T(2);
z[106] = -z[91] + z[120] + -z[169];
z[106] = z[2] * z[106];
z[113] = z[4] * z[131];
z[127] = z[23] * z[115];
z[131] = z[113] / T(2) + (T(3) * z[127]) / T(4);
z[147] = z[25] * z[115];
z[152] = z[117] + z[147];
z[157] = z[1] / T(2);
z[165] = z[10] + (T(3) * z[12]) / T(2) + z[157];
z[71] = -z[71] + z[165];
z[71] = z[5] * z[71];
z[168] = z[11] / T(2);
z[165] = (T(-5) * z[33]) / T(4) + -z[165] + z[168];
z[165] = z[32] * z[165];
z[91] = z[91] + -z[169];
z[91] = z[3] * z[91];
z[174] = T(2) * z[111];
z[176] = z[24] * z[174];
z[177] = T(2) * z[16] + T(-4) * z[19] + (T(11) * z[20]) / T(4) + (T(-3) * z[21]) / T(4) + (T(5) * z[22]) / T(4) + z[62];
z[177] = z[34] * z[177];
z[178] = (T(13) * z[33]) / T(2) + z[90];
z[179] = z[7] * z[178];
z[180] = -(z[6] * z[92]);
z[71] = z[71] + z[91] + z[106] + z[131] + (T(-3) * z[152]) / T(4) + z[165] + z[176] + z[177] + z[179] / T(2) + z[180];
z[71] = z[28] * z[71];
z[91] = z[34] * z[94];
z[106] = z[3] * z[129];
z[91] = z[91] + z[106];
z[75] = z[75] + z[79];
z[165] = -z[69] + -z[75];
z[177] = (T(5) * z[33]) / T(2);
z[165] = z[72] + z[165] / T(2) + z[177];
z[165] = z[5] * z[165];
z[179] = z[127] + -z[147];
z[180] = z[2] * z[84];
z[75] = z[11] + z[75];
z[181] = z[33] + z[72];
z[75] = z[75] / T(2) + -z[181];
z[75] = z[32] * z[75];
z[182] = T(2) * z[7];
z[183] = z[87] * z[182];
z[178] = z[6] * z[178];
z[75] = z[75] + z[91] + z[103] + -z[113] + z[165] + z[178] + (T(-3) * z[179]) / T(2) + -z[180] + z[183];
z[75] = z[29] * z[75];
z[113] = z[30] * z[124];
z[84] = z[35] * z[84];
z[124] = z[84] + -z[153];
z[124] = z[4] * z[124];
z[113] = z[113] + -z[124];
z[165] = z[25] * z[94];
z[178] = z[4] * z[129];
z[165] = z[165] + z[178];
z[82] = -z[82] + z[160];
z[160] = -z[69] + z[82];
z[178] = T(4) * z[1];
z[160] = z[160] / T(2) + -z[178];
z[160] = z[8] * z[160];
z[112] = z[23] * z[112];
z[110] = z[110] + -z[112] + z[160] + -z[165];
z[160] = T(3) * z[33];
z[179] = -z[72] + z[160];
z[183] = T(7) * z[11];
z[184] = z[95] + -z[183];
z[184] = -z[179] + z[184] / T(2);
z[184] = z[7] * z[184];
z[185] = z[10] + z[79];
z[186] = z[69] + -z[185];
z[186] = -z[1] + z[186] / T(2);
z[186] = z[5] * z[186];
z[187] = z[95] + z[178];
z[188] = -z[169] + z[187];
z[189] = T(-4) * z[33] + z[188];
z[189] = z[6] * z[189];
z[190] = -z[10] + z[169] + z[172];
z[190] = z[3] * z[190];
z[184] = z[110] + -z[135] + z[184] + z[186] + z[189] + z[190];
z[184] = z[26] * z[184];
z[186] = z[164] + z[178];
z[189] = T(11) * z[11] + z[82];
z[189] = -z[186] + z[189] / T(2);
z[189] = z[9] * z[189];
z[99] = -z[99] + -z[102] + -z[112] + z[145] + z[189];
z[112] = z[11] + z[185];
z[112] = z[1] + z[112] / T(2) + -z[120];
z[112] = z[32] * z[112];
z[112] = -z[99] + z[112] + -z[119];
z[112] = z[35] * z[112];
z[99] = z[99] + -z[122];
z[122] = z[27] * z[99];
z[145] = -z[169] + -z[187];
z[145] = z[6] * z[145];
z[110] = -z[110] + z[130] + -z[139] + z[145];
z[110] = z[0] * z[110];
z[139] = z[69] + z[95];
z[145] = (T(7) * z[33]) / T(2);
z[190] = -z[72] + -z[139] / T(2) + -z[145];
z[190] = z[35] * z[190];
z[191] = z[27] * z[150];
z[190] = z[190] + z[191];
z[190] = z[7] * z[190];
z[191] = z[0] + z[26];
z[144] = z[144] * z[191];
z[172] = T(4) * z[11] + -z[170] + -z[172];
z[172] = z[35] * z[172];
z[191] = T(2) * z[12];
z[192] = T(3) * z[1] + z[191];
z[171] = -z[171] + z[192];
z[193] = z[154] * z[171];
z[144] = z[144] + z[172] + z[193];
z[144] = z[2] * z[144];
z[172] = T(5) * z[33];
z[188] = -z[172] + -z[188];
z[188] = z[35] * z[188];
z[187] = T(-6) * z[11] + z[33] + z[187];
z[193] = z[27] * z[187];
z[188] = z[188] + z[193];
z[188] = z[6] * z[188];
z[193] = z[10] + z[11];
z[192] = -z[192] + z[193];
z[192] = z[0] * z[192];
z[108] = -(z[27] * z[108]);
z[108] = z[108] + -z[166] + z[192];
z[108] = z[3] * z[108];
z[71] = z[71] + z[75] + T(2) * z[108] + z[110] + z[112] + -z[113] + z[122] + z[144] + z[184] + z[188] + z[190];
z[71] = z[28] * z[71];
z[75] = -z[149] + -z[181];
z[75] = z[5] * z[75];
z[108] = -z[121] + z[137];
z[108] = z[32] * z[108];
z[110] = -z[145] + -z[169];
z[110] = z[7] * z[110];
z[87] = z[4] * z[87];
z[75] = z[75] + -z[87] + -z[106] + z[108] + z[110] + z[118] + -z[119] + T(-5) * z[133] + z[180];
z[75] = z[28] * z[75];
z[106] = -z[140] + z[165];
z[108] = z[69] + z[83];
z[108] = z[72] + z[108] / T(2) + z[160];
z[108] = z[6] * z[108];
z[110] = z[1] + z[170];
z[112] = z[110] * z[182];
z[118] = (T(3) * z[155]) / T(2);
z[121] = z[33] + z[118];
z[121] = z[32] * z[121];
z[122] = z[5] * z[110];
z[91] = z[91] + -z[106] + z[108] + z[112] + -z[121] + T(2) * z[122];
z[91] = z[26] * z[91];
z[94] = z[24] * z[94];
z[108] = z[6] * z[129];
z[112] = -z[10] + z[73];
z[129] = z[112] * z[182];
z[94] = -z[94] + -z[106] + z[108] + z[129] + -z[130];
z[94] = z[0] * z[94];
z[106] = T(3) * z[143];
z[108] = -z[33] + z[106];
z[108] = z[5] * z[108];
z[129] = z[32] * z[112];
z[101] = z[101] + -z[103] + z[108] / T(2) + T(2) * z[129];
z[101] = z[35] * z[101];
z[103] = z[27] * z[146];
z[91] = z[91] + -z[94] + -z[101] + z[103];
z[94] = z[122] + -z[129];
z[101] = -(z[3] * z[112]);
z[103] = z[2] * z[110];
z[87] = -z[87] + z[94] + z[101] + z[103] + -z[133];
z[87] = z[29] * z[87];
z[101] = z[110] * z[154];
z[103] = T(3) * z[88];
z[122] = -z[11] + z[103];
z[129] = z[122] * z[158];
z[101] = -z[84] + z[101] + -z[129];
z[88] = z[88] + -z[120];
z[129] = T(3) * z[46];
z[130] = z[88] * z[129];
z[133] = z[101] + z[130];
z[133] = z[2] * z[133];
z[137] = z[34] * z[115];
z[137] = z[127] + z[137] + -z[152];
z[140] = z[6] + -z[7];
z[144] = -z[4] + z[140];
z[144] = z[120] * z[144];
z[146] = -z[5] + z[32];
z[146] = z[88] * z[146];
z[137] = -z[137] / T(2) + z[144] + z[146];
z[137] = T(3) * z[137];
z[144] = -(z[45] * z[137]);
z[115] = z[24] * z[115];
z[115] = z[115] + -z[117] + z[147];
z[117] = z[115] + -z[127];
z[117] = z[117] / T(2);
z[127] = z[11] + z[120];
z[146] = z[4] * z[127];
z[146] = z[117] + z[146];
z[146] = z[129] * z[146];
z[149] = z[35] * z[112];
z[110] = z[27] * z[110];
z[149] = -z[110] + z[149];
z[129] = -(z[127] * z[129]);
z[129] = z[129] + T(-2) * z[149];
z[129] = z[7] * z[129];
z[83] = -z[83] + z[183];
z[83] = -z[72] + z[83] / T(2) + z[145];
z[83] = z[35] * z[83];
z[83] = z[83] + z[153];
z[152] = z[46] * z[164];
z[152] = z[83] + z[152];
z[152] = z[6] * z[152];
z[103] = -z[11] + -z[103] + z[160];
z[153] = z[103] / T(2);
z[155] = z[27] * z[153];
z[158] = z[0] * z[112];
z[155] = z[155] + T(2) * z[158];
z[130] = -z[130] + -z[155];
z[130] = z[3] * z[130];
z[158] = z[8] + z[9];
z[160] = T(2) * z[49];
z[165] = z[7] + z[158] + -z[160];
z[165] = (T(3) * z[34]) / T(2) + (T(4) * z[165]) / T(5);
z[165] = z[51] * z[165];
z[170] = T(3) * z[34];
z[180] = -(z[54] * z[170]);
z[165] = z[165] + z[180];
z[165] = z[51] * z[165];
z[170] = z[55] * z[170];
z[75] = z[75] + T(2) * z[87] + z[91] + -z[113] + z[129] + z[130] + z[133] + z[144] + z[146] + z[152] + z[165] + z[170];
z[75] = int_to_imaginary<T>(1) * z[75];
z[87] = z[2] + z[3] + z[4];
z[113] = z[5] + z[32];
z[129] = T(112) * z[49] + T(-11) * z[158];
z[129] = (T(-4) * z[6]) / T(3) + (T(-26) * z[7]) / T(45) + (T(-5) * z[24]) / T(4) + T(-2) * z[34] + (T(-2) * z[87]) / T(3) + -z[113] / T(3) + z[129] / T(45);
z[129] = int_to_imaginary<T>(1) * z[31] * z[129];
z[130] = z[11] + z[177];
z[130] = z[4] * z[130];
z[133] = z[33] + -z[168];
z[133] = z[7] * z[133];
z[129] = z[129] + z[130] + z[133];
z[130] = z[14] / T(2);
z[56] = z[56] + z[130];
z[133] = z[16] / T(6) + (T(2) * z[19]) / T(3);
z[62] = z[20] / T(12) + -z[21] / T(4) + (T(-11) * z[22]) / T(12) + z[56] + z[62] + -z[133];
z[62] = z[24] * z[62];
z[144] = z[156] + z[177];
z[144] = z[2] * z[144];
z[146] = -z[33] + z[90];
z[146] = z[6] * z[146];
z[144] = z[144] + z[146];
z[56] = z[47] / T(2) + z[56];
z[133] = -z[20] / T(6) + (T(-2) * z[22]) / T(3) + -z[51] + z[56] + -z[133];
z[133] = z[34] * z[133];
z[116] = -z[116] + z[147];
z[112] = z[74] + z[112];
z[112] = z[9] * z[112];
z[146] = (T(-7) * z[1]) / T(6) + z[10] / T(6) + -z[12];
z[147] = z[33] / T(3) + -z[146] + -z[168];
z[147] = z[5] * z[147];
z[152] = z[10] / T(2);
z[156] = z[11] + z[152] + -z[157];
z[156] = z[74] + z[156] / T(3);
z[156] = z[3] * z[156];
z[93] = z[10] + -z[93];
z[93] = z[8] * z[93];
z[146] = (T(2) * z[11]) / T(3) + z[33] / T(6) + z[146];
z[146] = z[32] * z[146];
z[62] = z[62] + z[93] + z[112] + -z[114] + z[116] / T(2) + z[129] / T(3) + z[133] + z[144] / T(6) + z[146] + z[147] + z[156];
z[62] = z[31] * z[62];
z[87] = T(12) * z[6] + T(-8) * z[49] + T(6) * z[87] + T(3) * z[113] + T(-5) * z[158] + -z[182];
z[87] = z[50] * z[87];
z[62] = z[62] + z[75] + z[87];
z[62] = z[31] * z[62];
z[75] = (T(-7) * z[1]) / T(2) + -z[79] + z[120] + z[152] + z[169];
z[75] = z[32] * z[75];
z[65] = -(z[13] * z[65]);
z[59] = -(z[25] * z[59]);
z[87] = z[8] * z[103];
z[93] = -(z[23] * z[174]);
z[59] = z[59] + z[65] + z[75] + (T(-3) * z[87]) / T(4) + z[93] + -z[108] / T(4) + z[119] + z[176] + z[189];
z[65] = prod_pow(z[35], 2);
z[59] = z[59] * z[65];
z[64] = z[64] + z[132];
z[60] = -z[20] + (T(-7) * z[22]) / T(4) + z[60] + -z[64] / T(2);
z[60] = z[13] * z[60];
z[64] = z[25] * z[174];
z[60] = z[60] + -z[64];
z[58] = -z[58] + z[132];
z[58] = -z[20] + -z[22] / T(4) + z[58] / T(2) + z[61];
z[61] = z[23] + z[34];
z[64] = z[58] * z[61];
z[75] = z[1] + -z[162] / T(4);
z[75] = z[8] * z[75];
z[87] = T(-11) * z[10] + -z[69] + z[79];
z[87] = T(7) * z[1] + z[87] / T(2);
z[87] = z[33] + z[87] / T(2);
z[87] = z[5] * z[87];
z[93] = z[1] + z[148] / T(4);
z[108] = -z[93] + z[164];
z[108] = z[7] * z[108];
z[112] = z[1] + z[139] / T(4) + z[164];
z[112] = z[6] * z[112];
z[93] = z[3] * z[93];
z[114] = -(z[4] * z[100]);
z[64] = -z[60] + z[64] + z[75] + z[87] + z[93] + z[108] + z[112] + z[114] + -z[121] / T(2);
z[64] = z[26] * z[64];
z[75] = z[8] * z[107];
z[75] = z[75] + -z[109];
z[87] = z[66] * z[113];
z[93] = -(z[34] * z[174]);
z[108] = z[7] * z[11];
z[109] = z[11] + z[66];
z[109] = z[6] * z[109];
z[87] = z[75] + z[87] + z[93] + z[108] + z[109];
z[87] = z[35] * z[87];
z[93] = -z[159] + z[167];
z[93] = z[3] * z[93];
z[64] = z[64] + T(2) * z[87] + z[93] + -z[142];
z[64] = z[26] * z[64];
z[87] = z[161] + z[183];
z[87] = -z[72] + z[87] / T(2) + -z[164];
z[87] = z[40] * z[87];
z[93] = z[177] + z[178];
z[108] = z[80] + z[183];
z[108] = -z[93] + z[108] / T(2);
z[108] = z[38] * z[108];
z[95] = z[95] / T(2) + -z[169];
z[109] = z[95] + z[123];
z[109] = z[65] * z[109];
z[112] = T(3) * z[42];
z[113] = z[112] * z[150];
z[114] = -z[107] + z[169];
z[114] = z[114] * z[154];
z[116] = z[11] + -z[82];
z[116] = z[72] + z[116] / T(4);
z[116] = z[0] * z[116];
z[114] = z[114] + z[116];
z[114] = z[0] * z[114];
z[116] = -(z[35] * z[171]);
z[116] = z[110] + z[116];
z[116] = z[116] * z[154];
z[119] = -z[82] + -z[183];
z[119] = z[119] / T(2) + z[178];
z[119] = z[37] * z[119];
z[121] = -z[0] + -z[35];
z[121] = z[26] * z[107] * z[121];
z[108] = -z[87] + z[108] + z[109] + z[113] + z[114] + z[116] + z[119] + T(2) * z[121];
z[108] = z[2] * z[108];
z[109] = z[69] + z[161];
z[109] = z[109] / T(2) + z[179];
z[109] = z[36] * z[109];
z[95] = z[66] + z[72] + z[95];
z[95] = z[65] * z[95];
z[113] = -(z[35] * z[187]);
z[114] = z[27] * z[173];
z[113] = z[113] + z[114];
z[113] = z[27] * z[113];
z[114] = -z[123] + z[126];
z[116] = -(z[112] * z[114]);
z[96] = z[72] + z[96];
z[119] = T(3) * z[41];
z[96] = z[96] * z[119];
z[121] = -z[11] + -z[161];
z[105] = z[105] + z[121] / T(2);
z[105] = z[40] * z[105];
z[121] = z[38] * z[175];
z[123] = -(z[37] * z[143]);
z[95] = z[95] + z[96] + z[105] + z[109] + z[113] + z[116] + z[121] + z[123];
z[95] = z[6] * z[95];
z[105] = z[185] / T(2);
z[73] = z[73] + -z[92] + z[105];
z[73] = z[3] * z[73];
z[92] = -z[1] + -z[74] + -z[105];
z[92] = z[2] * z[92];
z[105] = z[7] * z[127];
z[74] = z[11] + z[74];
z[74] = z[6] * z[74];
z[73] = z[73] + z[74] + z[92] + -z[94] + (T(3) * z[105]) / T(2) + (T(-3) * z[115]) / T(4) + z[131];
z[73] = z[29] * z[73];
z[74] = -(z[2] * z[101]);
z[83] = -(z[6] * z[83]);
z[92] = z[149] * z[182];
z[94] = z[3] * z[155];
z[73] = z[73] + z[74] + z[83] + -z[91] + z[92] + z[94] + -z[124];
z[73] = z[29] * z[73];
z[74] = -z[11] + z[80];
z[74] = z[74] / T(2) + -z[93];
z[74] = z[38] * z[74];
z[83] = -z[110] + -z[151];
z[83] = z[27] * z[83];
z[91] = T(7) * z[33] + z[106];
z[92] = z[40] / T(2);
z[91] = z[91] * z[92];
z[92] = z[118] + z[172];
z[92] = z[36] * z[92];
z[93] = z[1] + (T(7) * z[33]) / T(4) + z[136] / T(4);
z[93] = z[65] * z[93];
z[94] = T(9) * z[11] + z[80];
z[94] = z[94] / T(2) + -z[178];
z[94] = z[37] * z[94];
z[74] = z[74] + z[83] + z[91] + z[92] + z[93] + z[94];
z[74] = z[7] * z[74];
z[83] = -(z[100] * z[141]);
z[93] = z[23] + z[24];
z[58] = z[58] * z[93];
z[93] = z[6] + T(-3) * z[8];
z[93] = z[93] * z[97];
z[94] = -z[1] + z[125] / T(4);
z[94] = z[7] * z[94];
z[58] = z[58] + -z[60] + z[67] + z[83] + z[93] + z[94];
z[58] = z[0] * z[58];
z[60] = z[11] * z[140];
z[60] = z[60] + -z[75] + -z[176];
z[60] = z[27] * z[60];
z[58] = z[58] + T(2) * z[60];
z[58] = z[0] * z[58];
z[60] = -z[11] + -z[82];
z[60] = z[60] / T(2) + z[186];
z[60] = z[38] * z[60];
z[67] = z[69] + z[78] + z[79];
z[67] = -z[1] + z[67] / T(2);
z[67] = z[0] * z[67];
z[75] = z[11] + z[107];
z[75] = z[75] * z[154];
z[67] = z[67] + z[75];
z[67] = z[0] * z[67];
z[75] = z[36] * z[163];
z[78] = z[119] * z[138];
z[79] = z[11] * z[27];
z[79] = z[79] + z[166];
z[79] = z[79] * z[154];
z[80] = z[11] + z[80];
z[80] = z[80] / T(2) + -z[178];
z[80] = z[37] * z[80];
z[60] = z[60] + z[67] + z[75] + z[78] + z[79] + z[80];
z[60] = z[3] * z[60];
z[67] = (T(3) * z[16]) / T(2) + -z[89];
z[78] = (T(3) * z[22]) / T(2) + z[67] + -z[128] + -z[130] + z[134];
z[78] = z[78] * z[119];
z[79] = z[36] * z[86];
z[80] = z[40] * z[57];
z[82] = -z[78] + z[79] + -z[80];
z[82] = z[23] * z[82];
z[83] = -(z[35] * z[99]);
z[61] = -z[25] + z[61];
z[89] = -(z[61] * z[111]);
z[81] = z[81] + T(2) * z[89];
z[81] = z[27] * z[81];
z[81] = z[81] + z[83];
z[81] = z[27] * z[81];
z[83] = z[76] + z[90];
z[83] = z[83] / T(2) + -z[145] + -z[178];
z[83] = z[38] * z[83];
z[77] = z[27] * z[77];
z[77] = z[77] + z[84];
z[77] = z[27] * z[77];
z[65] = z[65] * z[85];
z[84] = z[42] * z[103];
z[85] = z[41] * z[122];
z[85] = (T(3) * z[85]) / T(2);
z[65] = z[65] + z[77] + -z[83] + (T(3) * z[84]) / T(2) + -z[85];
z[65] = z[4] * z[65];
z[77] = z[24] + z[25];
z[77] = z[77] * z[86];
z[69] = z[69] + z[76];
z[69] = z[69] / T(2) + -z[178];
z[69] = -(z[69] * z[141]);
z[68] = z[68] + z[90];
z[68] = z[68] / T(2) + -z[178];
z[68] = z[9] * z[68];
z[68] = z[68] + z[69] + z[77] + z[135];
z[68] = z[37] * z[68];
z[57] = z[38] * z[57];
z[69] = z[57] + -z[80];
z[76] = (T(-115) * z[53]) / T(6) + -z[69];
z[76] = z[24] * z[76];
z[57] = z[57] + z[78];
z[57] = z[25] * z[57];
z[70] = z[70] / T(2) + -z[104] + -z[178];
z[70] = z[38] * z[70];
z[70] = z[70] + -z[75] + -z[96];
z[70] = z[8] * z[70];
z[69] = T(-7) * z[53] + -z[69] + z[79];
z[69] = z[34] * z[69];
z[75] = -z[12] + z[193];
z[75] = -z[1] + z[75] / T(2);
z[75] = T(3) * z[75];
z[77] = z[75] + -z[145];
z[77] = z[40] * z[77];
z[77] = z[77] + -z[83] + -z[92];
z[77] = z[32] * z[77];
z[56] = (T(3) * z[20]) / T(2) + -z[56] + z[67] + z[98];
z[61] = z[24] + -z[61];
z[56] = z[56] * z[61];
z[61] = z[9] * z[114];
z[67] = -z[8] + z[32];
z[67] = z[67] * z[153];
z[56] = z[56] + z[61] + z[67];
z[56] = z[56] * z[112];
z[61] = -z[11] + z[72] + z[191];
z[67] = -z[13] + -z[23];
z[61] = z[61] * z[67];
z[67] = z[7] + z[158];
z[72] = z[67] * z[174];
z[78] = -(z[49] * z[111]);
z[79] = z[24] * z[107];
z[61] = z[61] + z[72] + T(4) * z[78] + z[79];
z[61] = z[39] * z[61];
z[72] = -(z[43] * z[137]);
z[78] = -z[2] + z[3];
z[78] = z[78] * z[88];
z[79] = -z[4] + z[7];
z[79] = z[79] * z[127];
z[80] = -(z[6] * z[120]);
z[78] = z[78] + z[79] + z[80] + -z[117];
z[78] = z[44] * z[78];
z[79] = z[38] + -z[40];
z[79] = z[79] * z[102];
z[80] = z[36] * z[135];
z[83] = z[85] + z[87];
z[83] = z[9] * z[83];
z[66] = -z[66] + -z[75];
z[66] = z[36] * z[66];
z[66] = z[66] + -z[85] + -z[91];
z[66] = z[5] * z[66];
z[67] = -z[67] + z[160];
z[67] = int_to_imaginary<T>(1) * z[52] * z[67];
return z[56] + z[57] + z[58] + z[59] + z[60] + T(2) * z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + (T(192) * z[67]) / T(5) + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[74] + z[76] + z[77] + T(3) * z[78] + z[79] + z[80] + z[81] + z[82] + z[83] + z[95] + z[108];
}



template IntegrandConstructorType<double> f_4_289_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_289_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_289_construct (const Kin<qd_real>&);
#endif

}