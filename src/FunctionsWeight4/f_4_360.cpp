#include "f_4_360.h"

namespace PentagonFunctions {

template <typename T> T f_4_360_abbreviated (const std::array<T,51>&);

template <typename T> class SpDLog_f_4_360_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_360_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(12) + T(6) * kin.v[1]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]);
c[1] = kin.v[1] * (T(-12) + T(-6) * kin.v[1]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[3] * c[1]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[4] * T(-6) + prod_pow(abb[2], 2) * abb[3] * T(6) + prod_pow(abb[1], 2) * (abb[3] * T(-6) + abb[4] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_360_W_10 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_360_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(12) + T(-6) * kin.v[1] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[1] = kin.v[1] * (T(12) + T(-6) * kin.v[1] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[2] = kin.v[1] * (T(12) + T(-6) * kin.v[1] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[7] * c[1] + abb[8] * c[2]);
        }

        return abb[5] * (abb[3] * prod_pow(abb[6], 2) * T(-6) + prod_pow(abb[6], 2) * (abb[7] * T(-6) + abb[8] * T(-6)) + prod_pow(abb[2], 2) * (abb[3] * T(6) + abb[7] * T(6) + abb[8] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_360_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl10 = DLog_W_10<T>(kin),dl11 = DLog_W_11<T>(kin),dl6 = DLog_W_6<T>(kin),dl14 = DLog_W_14<T>(kin),dl24 = DLog_W_24<T>(kin),dl2 = DLog_W_2<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl31 = DLog_W_31<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),spdl12 = SpDLog_f_4_360_W_12<T>(kin),spdl10 = SpDLog_f_4_360_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,51> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl10(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl11(t), rlog(v_path[0]), rlog(v_path[3]), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl14(t), dl24(t), dl2(t), f_2_1_4(kin_path), f_2_1_7(kin_path), f_2_1_2(kin_path), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl4(t), f_2_1_9(kin_path), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl17(t), dl5(t), dl1(t), dl31(t), f_2_2_1(kin_path), f_2_2_2(kin_path), f_2_2_3(kin_path), f_2_2_4(kin_path), f_2_2_5(kin_path), f_2_2_6(kin_path), f_2_2_7(kin_path), f_2_2_8(kin_path), f_2_2_9(kin_path), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[30] / kin_path.W[30]), dl3(t), dl19(t), dl16(t), dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_4_360_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_360_abbreviated(const std::array<T,51>& abb)
{
using TR = typename T::value_type;
T z[106];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[44];
z[3] = abb[4];
z[4] = abb[14];
z[5] = abb[46];
z[6] = abb[8];
z[7] = abb[16];
z[8] = abb[22];
z[9] = abb[28];
z[10] = abb[45];
z[11] = abb[40];
z[12] = abb[49];
z[13] = abb[41];
z[14] = abb[42];
z[15] = abb[43];
z[16] = abb[2];
z[17] = abb[6];
z[18] = abb[27];
z[19] = abb[10];
z[20] = abb[29];
z[21] = abb[47];
z[22] = abb[50];
z[23] = abb[11];
z[24] = abb[7];
z[25] = abb[13];
z[26] = abb[24];
z[27] = bc<TR>[0];
z[28] = abb[48];
z[29] = abb[15];
z[30] = abb[12];
z[31] = abb[17];
z[32] = abb[18];
z[33] = abb[23];
z[34] = abb[25];
z[35] = abb[26];
z[36] = abb[19];
z[37] = abb[20];
z[38] = abb[21];
z[39] = abb[9];
z[40] = abb[30];
z[41] = abb[31];
z[42] = abb[32];
z[43] = abb[33];
z[44] = abb[34];
z[45] = abb[35];
z[46] = abb[36];
z[47] = abb[37];
z[48] = abb[38];
z[49] = abb[39];
z[50] = bc<TR>[1];
z[51] = bc<TR>[3];
z[52] = bc<TR>[5];
z[53] = bc<TR>[2];
z[54] = bc<TR>[4];
z[55] = bc<TR>[7];
z[56] = bc<TR>[8];
z[57] = bc<TR>[9];
z[58] = z[11] + z[13] + z[14] + T(-2) * z[15];
z[59] = -(z[12] * z[58]);
z[60] = z[6] * z[10];
z[61] = z[59] + -z[60];
z[62] = -(z[22] * z[58]);
z[63] = -(z[28] * z[58]);
z[64] = -(z[21] * z[58]);
z[65] = z[3] * z[5];
z[66] = T(2) * z[3];
z[67] = -z[6] + z[66];
z[67] = z[7] * z[67];
z[67] = -z[61] + T(-2) * z[62] + -z[63] + z[64] + -z[65] + z[67];
z[68] = -z[3] + z[6];
z[69] = z[4] * z[68];
z[70] = -z[24] + z[68];
z[71] = z[29] * z[70];
z[72] = z[69] + z[71];
z[73] = (T(13) * z[24]) / T(2);
z[74] = T(-6) * z[68] + z[73];
z[74] = z[9] * z[74];
z[75] = T(2) * z[2];
z[76] = z[1] + z[24];
z[76] = z[75] * z[76];
z[73] = (T(-3) * z[1]) / T(2) + T(-8) * z[68] + z[73];
z[73] = z[20] * z[73];
z[77] = z[1] + z[6];
z[78] = -z[24] / T(2) + z[66] + T(-4) * z[77];
z[78] = z[18] * z[78];
z[66] = T(2) * z[6] + (T(-17) * z[24]) / T(2) + -z[66];
z[66] = z[8] * z[66];
z[79] = z[10] + T(3) * z[29];
z[79] = (T(-5) * z[7]) / T(2) + z[9] / T(2) + T(2) * z[79];
z[79] = z[1] * z[79];
z[80] = (T(-13) * z[1]) / T(2) + T(2) * z[70];
z[80] = z[26] * z[80];
z[81] = -z[12] + z[21];
z[82] = T(7) * z[22] + T(-8) * z[28] + T(-15) * z[81];
z[82] = z[53] * z[82];
z[66] = z[66] + T(2) * z[67] + T(6) * z[72] + z[73] + z[74] + z[76] + z[78] + z[79] + z[80] + z[82];
z[66] = z[27] * z[66];
z[67] = -z[5] + -z[10] + T(2) * z[40];
z[72] = -z[2] + z[67];
z[73] = -(z[51] * z[72]);
z[66] = z[66] + T(-16) * z[73];
z[66] = z[27] * z[66];
z[74] = z[6] * z[8];
z[76] = z[6] * z[9];
z[78] = z[74] + z[76];
z[79] = z[1] + z[68];
z[80] = z[2] * z[79];
z[82] = -(z[6] * z[7]);
z[82] = z[59] + T(-2) * z[60] + z[65] + z[69] + z[78] + z[80] + z[82];
z[82] = z[0] * z[82];
z[83] = z[61] + z[78];
z[84] = -(z[23] * z[83]);
z[85] = z[7] + z[20];
z[85] = z[3] * z[85];
z[86] = -z[65] + z[85];
z[87] = z[64] + z[86];
z[87] = z[19] * z[87];
z[84] = z[84] + z[87];
z[82] = z[82] + T(2) * z[84];
z[82] = z[0] * z[82];
z[84] = z[20] * z[70];
z[87] = z[6] + -z[24];
z[87] = z[39] * z[87];
z[88] = z[1] * z[10];
z[84] = z[84] + -z[87] + -z[88];
z[89] = z[26] * z[70];
z[90] = z[63] + -z[89];
z[91] = -z[59] + z[64];
z[92] = z[90] + z[91];
z[93] = -z[60] + z[65];
z[94] = z[3] + z[24];
z[95] = z[1] + z[94];
z[95] = z[30] * z[95];
z[96] = z[2] * z[70];
z[97] = z[8] * z[70];
z[98] = z[84] + z[92] + z[93] + z[95] + T(2) * z[96] + -z[97];
z[98] = z[19] * z[98];
z[96] = z[96] + -z[97];
z[92] = z[92] + z[96];
z[99] = z[23] * z[92];
z[92] = -(z[25] * z[92]);
z[92] = z[92] + -z[99];
z[92] = T(2) * z[92] + z[98];
z[92] = z[19] * z[92];
z[93] = -z[91] + z[93];
z[68] = z[7] * z[68];
z[68] = z[62] + z[68];
z[98] = z[5] * z[24];
z[100] = z[7] + -z[10] + z[29];
z[100] = z[1] * z[100];
z[101] = z[24] + z[77];
z[101] = z[18] * z[101];
z[100] = z[68] + z[71] + z[93] + z[98] + z[100] + z[101];
z[100] = z[17] * z[100];
z[101] = z[1] * z[7];
z[79] = z[18] * z[79];
z[68] = z[68] + z[79] + -z[80] + z[101];
z[79] = z[68] + -z[91];
z[91] = z[0] * z[79];
z[101] = z[26] * z[94];
z[101] = -z[98] + z[101];
z[94] = z[9] * z[94];
z[102] = z[94] + z[101];
z[103] = z[63] + z[64];
z[104] = -z[65] + z[102] + z[103];
z[104] = z[25] * z[104];
z[105] = z[18] + z[20];
z[77] = z[77] * z[105];
z[62] = z[62] + z[77];
z[77] = z[62] + -z[88];
z[61] = -z[61] + -z[77];
z[61] = z[19] * z[61];
z[61] = z[61] + z[91] + z[104];
z[61] = T(2) * z[61] + z[100];
z[61] = z[17] * z[61];
z[70] = -(z[9] * z[70]);
z[70] = z[63] + z[70] + -z[74] + -z[93] + z[101];
z[88] = int_to_imaginary<T>(1) * z[27];
z[70] = z[70] * z[88];
z[79] = -(z[17] * z[79]);
z[91] = -z[0] + z[23];
z[83] = z[83] * z[91];
z[70] = z[70] + z[79] + z[83] + -z[104];
z[65] = z[59] + -z[65];
z[79] = -z[1] + z[3];
z[79] = z[9] * z[79];
z[74] = z[65] + z[74] + z[79] + -z[80] + -z[98];
z[74] = z[16] * z[74];
z[70] = T(2) * z[70] + z[74];
z[70] = z[16] * z[70];
z[74] = z[95] + z[97];
z[79] = z[26] + -z[29];
z[79] = z[1] * z[79];
z[62] = -z[62] + z[71] + z[74] + -z[79] + z[94];
z[62] = z[25] * z[62];
z[71] = -z[62] + T(2) * z[99];
z[71] = z[25] * z[71];
z[61] = z[61] + z[70] + z[71] + z[82] + z[92];
z[70] = z[63] + z[102];
z[71] = z[70] + -z[77] + -z[93];
z[79] = z[34] * z[71];
z[80] = -z[89] + z[96];
z[59] = -z[59] + z[63] + z[80] + -z[86];
z[59] = z[36] * z[59];
z[63] = z[65] + z[68] + z[70];
z[63] = z[32] * z[63];
z[64] = -z[60] + z[64] + -z[68] + z[78];
z[64] = z[31] * z[64];
z[60] = -z[60] + z[77] + z[80] + z[103];
z[60] = z[33] * z[60];
z[65] = -z[78] + z[85] + -z[93];
z[68] = z[37] * z[65];
z[59] = z[59] + z[60] + z[63] + z[64] + -z[68] + -z[79];
z[60] = z[8] * z[24];
z[60] = -z[60] + z[69] + -z[76] + z[85] + -z[87] + -z[90];
z[63] = z[23] * z[60];
z[64] = -z[74] + -z[84] + z[90] + -z[93];
z[64] = z[19] * z[64];
z[68] = -(z[35] * z[71]);
z[65] = -(z[38] * z[65]);
z[69] = -z[22] + -z[28];
z[69] = z[54] * z[69];
z[62] = z[62] + z[63] + z[64] + z[65] + z[68] + T(2) * z[69];
z[63] = T(3) * z[28];
z[64] = T(3) * z[22] + z[63] + T(4) * z[72];
z[65] = prod_pow(z[27], 2);
z[64] = z[64] * z[65];
z[62] = T(12) * z[62] + z[64];
z[62] = z[27] * z[62];
z[64] = -(z[52] * z[72]);
z[62] = z[62] + T(192) * z[64];
z[62] = int_to_imaginary<T>(1) * z[62];
z[60] = T(-6) * z[60];
z[60] = prod_pow(z[23], 2) * z[60];
z[64] = T(-3) * z[81];
z[63] = T(-6) * z[22] + -z[63] + -z[64] + T(2) * z[67] + -z[75];
z[63] = z[63] * z[88];
z[67] = z[28] + z[81];
z[68] = z[50] * z[67];
z[63] = z[63] + T(3) * z[68];
z[63] = z[50] * z[63];
z[68] = -z[22] / T(2) + (T(13) * z[28]) / T(2) + T(7) * z[81];
z[65] = z[65] * z[68];
z[68] = z[22] + -z[81];
z[68] = z[27] * z[53] * z[68];
z[68] = z[68] + T(4) * z[73];
z[68] = int_to_imaginary<T>(1) * z[68];
z[67] = z[54] * z[67];
z[63] = T(2) * z[63] + z[65] + T(24) * z[67] + T(12) * z[68];
z[63] = z[50] * z[63];
z[65] = T(4) * z[55] + z[56];
z[64] = -z[22] + T(2) * z[28] + -z[64];
z[65] = z[64] * z[65];
z[58] = z[58] * z[72];
z[67] = z[42] + z[44] + z[46] + -z[48] + -z[49];
z[67] = z[58] * z[67];
z[65] = z[65] + T(4) * z[67];
z[58] = T(-12) * z[58];
z[67] = -z[41] + z[43] + -z[45] + -z[47];
z[58] = z[58] * z[67];
z[64] = prod_pow(z[53], 3) * z[64];
z[67] = (T(5) * z[22]) / T(2) + T(-4) * z[28] + (T(-13) * z[81]) / T(2);
z[67] = z[57] * z[67];
return z[58] + T(12) * z[59] + z[60] + T(6) * z[61] + z[62] + z[63] + T(-4) * z[64] + T(3) * z[65] + z[66] + T(7) * z[67];
}



template IntegrandConstructorType<double> f_4_360_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_360_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_360_construct (const Kin<qd_real>&);
#endif

}