#include "f_4_71.h"

namespace PentagonFunctions {

template <typename T> T f_4_71_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_71_W_10 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_71_W_10 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(2) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[1] * (T(4) + T(-2) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (T(4) + T(-2) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-2) + prod_pow(abb[2], 2) * (abb[5] * T(-2) + abb[4] * T(2)) + prod_pow(abb[1], 2) * (abb[4] * T(-2) + abb[3] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_71_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl17 = DLog_W_17<T>(kin),dl24 = DLog_W_24<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),spdl10 = SpDLog_f_4_71_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);

        std::array<std::complex<T>,17> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), dl17(t), -rlog(t), rlog(kin.W[23] / kin_path.W[23]), dl24(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl16(t), f_2_1_7(kin_path), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl18(t), dl5(t)}
;

        auto result = f_4_71_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_4_71_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[31];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[11];
z[3] = abb[16];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[7];
z[7] = abb[10];
z[8] = abb[15];
z[9] = bc<TR>[0];
z[10] = abb[2];
z[11] = abb[6];
z[12] = abb[9];
z[13] = abb[8];
z[14] = abb[12];
z[15] = abb[13];
z[16] = abb[14];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = -z[3] + z[12];
z[20] = z[1] + z[5];
z[21] = -z[4] + z[20];
z[19] = z[19] * z[21];
z[22] = z[1] + -z[13];
z[22] = z[8] * z[22];
z[19] = z[19] + -z[22];
z[19] = z[7] * z[19];
z[23] = -z[2] + z[8];
z[24] = z[3] + z[23];
z[25] = z[21] * z[24];
z[26] = -z[0] + z[10];
z[26] = z[25] * z[26];
z[26] = z[19] + T(2) * z[26];
z[26] = z[7] * z[26];
z[27] = z[2] + -z[12];
z[27] = z[21] * z[27];
z[20] = -z[6] + -z[13] + z[20];
z[20] = z[11] * z[20];
z[27] = z[20] + z[27];
z[27] = prod_pow(z[10], 2) * z[27];
z[28] = -(z[2] * z[21]);
z[29] = -z[1] + z[4];
z[30] = z[6] + -z[29];
z[30] = z[3] * z[30];
z[28] = z[28] + z[30];
z[28] = prod_pow(z[0], 2) * z[28];
z[26] = z[26] + z[27] + z[28];
z[21] = z[12] * z[21];
z[27] = z[17] * z[24];
z[21] = z[21] + z[27];
z[27] = (T(13) * z[5]) / T(6) + -z[6] / T(6) + T(-2) * z[29];
z[27] = z[3] * z[27];
z[20] = -z[20] / T(6) + T(-2) * z[21] + (T(13) * z[22]) / T(6) + z[27];
z[21] = prod_pow(z[9], 2);
z[20] = z[20] * z[21];
z[22] = z[0] + -z[16];
z[22] = z[22] * z[25];
z[19] = -z[19] + z[22];
z[22] = -z[3] / T(2) + (T(5) * z[12]) / T(6) + z[23] / T(3);
z[21] = z[21] * z[22];
z[19] = T(4) * z[19] + z[21];
z[19] = int_to_imaginary<T>(1) * z[9] * z[19];
z[21] = z[14] + -z[15];
z[21] = T(4) * z[21];
z[21] = z[21] * z[25];
z[22] = z[18] * z[24];
return z[19] + z[20] + z[21] + T(3) * z[22] + T(2) * z[26];
}



template IntegrandConstructorType<double> f_4_71_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_71_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_71_construct (const Kin<qd_real>&);
#endif

}