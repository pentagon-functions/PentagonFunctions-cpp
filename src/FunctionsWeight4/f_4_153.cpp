#include "f_4_153.h"

namespace PentagonFunctions {

template <typename T> T f_4_153_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_153_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_153_W_22 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (T(-12) + T(12) * kin.v[0] + T(6) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[0] * (T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[1] * (T(12) + T(-12) * kin.v[0] + T(-6) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[0] * (T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-6) + prod_pow(abb[2], 2) * abb[4] * T(6) + prod_pow(abb[1], 2) * (abb[4] * T(-6) + abb[3] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_153_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_153_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[3] * ((T(-9) * kin.v[3]) / T(4) + T(3) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(4) + (T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[1] * ((T(-9) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]);
c[1] = kin.v[2] * (T(3) + T(-6) * kin.v[2] + T(-15) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * (T(3) + T(-9) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * (T(-3) + T(-9) * kin.v[1] + T(15) * kin.v[2] + T(18) * kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * (T(-3) + (T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-15) * kin.v[1] + T(12) * kin.v[2] + T(15) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-24) * kin.v[1] + T(16) * kin.v[2] + T(24) * kin.v[3] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(-8) * kin.v[2] + T(-24) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-16) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-16) * kin.v[1] + T(24) * kin.v[2] + T(32) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(12) + T(-6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) + T(-6) * kin.v[1] + T(12) * kin.v[3] + T(12) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * ((T(27) * kin.v[1]) / T(2) + T(14) + T(-13) * kin.v[2] + T(-27) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[3] * ((T(27) * kin.v[3]) / T(2) + T(-14) + T(14) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(-14) + T(13) * kin.v[3] + T(14) * kin.v[4]) + kin.v[0] * (T(14) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(13) * kin.v[1] + kin.v[2] + T(-13) * kin.v[3] + T(-14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[2] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(8) + T(4) * kin.v[1] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[3] * (T(-8) + T(4) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[3] * ((T(-39) * kin.v[3]) / T(4) + T(5) + T(-5) * kin.v[4]) + kin.v[2] * ((T(-19) * kin.v[2]) / T(4) + (T(-29) * kin.v[3]) / T(2) + T(5) + T(-5) * kin.v[4]) + kin.v[1] * ((T(-39) * kin.v[1]) / T(4) + (T(29) * kin.v[2]) / T(2) + (T(39) * kin.v[3]) / T(2) + T(-5) + T(5) * kin.v[4]) + kin.v[0] * ((T(-29) * kin.v[1]) / T(2) + (T(19) * kin.v[2]) / T(2) + (T(29) * kin.v[3]) / T(2) + T(-5) + (T(-19) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(5) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[2] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-4) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-4) * kin.v[1] + T(8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(kin.v[2]) * (kin.v[3] * ((T(-27) * kin.v[3]) / T(2) + T(14) + T(-14) * kin.v[4]) + kin.v[2] * (kin.v[2] / T(2) + T(14) + T(-13) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[1] * ((T(-27) * kin.v[1]) / T(2) + T(-14) + T(13) * kin.v[2] + T(27) * kin.v[3] + T(14) * kin.v[4]) + kin.v[0] * (-kin.v[2] + T(-14) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(-13) * kin.v[1] + T(13) * kin.v[3] + T(14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[2] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-4) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-4) * kin.v[1] + T(8) * kin.v[3] + T(8) * kin.v[4])));
c[2] = T(3) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3];
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] + T(-8) * kin.v[2] + T(-8) * kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3])) + rlog(kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[2] + T(6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[5] * (abb[7] * (abb[3] * T(-6) + abb[8] * T(3) + abb[11] * T(3)) + abb[2] * (abb[11] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[3] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * ((abb[8] * T(-3)) / T(2) + (abb[11] * T(5)) / T(2) + abb[3] * T(-7) + abb[4] * T(6))) + abb[9] * (abb[1] * abb[2] * T(-8) + abb[7] * T(6) + abb[2] * (bc<T>[0] * int_to_imaginary<T>(-8) + abb[2] * T(7))) + abb[1] * abb[2] * (abb[11] * T(-8) + abb[3] * T(8)) + abb[6] * (abb[6] * (abb[3] + abb[11] / T(2) + (abb[8] * T(9)) / T(2) + -abb[9] + abb[4] * T(-6)) + abb[3] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[11] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * (abb[8] * T(-3) + abb[11] * T(-3) + abb[3] * T(6)) + abb[9] * (abb[2] * T(-6) + bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * T(8)) + abb[1] * (abb[3] * T(-8) + abb[11] * T(8))) + abb[10] * (abb[2] * abb[9] * T(8) + abb[6] * (abb[9] * T(-8) + abb[11] * T(-8) + abb[3] * T(8)) + abb[2] * (abb[3] * T(-8) + abb[11] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_153_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl8 = DLog_W_8<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),spdl22 = SpDLog_f_4_153_W_22<T>(kin),spdl21 = SpDLog_f_4_153_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,21> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[19] / kin_path.W[19]), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_15(kin_path), -rlog(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[2]), rlog(kin.W[18] / kin_path.W[18]), dl8(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[7] / kin_path.W[7]), dl16(t), f_2_1_14(kin_path), dl20(t), dl3(t), dl19(t)}
;

        auto result = f_4_153_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_153_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[81];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[16];
z[3] = abb[18];
z[4] = abb[19];
z[5] = abb[20];
z[6] = abb[4];
z[7] = abb[8];
z[8] = abb[9];
z[9] = abb[11];
z[10] = abb[15];
z[11] = abb[2];
z[12] = abb[12];
z[13] = abb[6];
z[14] = abb[10];
z[15] = bc<TR>[0];
z[16] = abb[7];
z[17] = abb[13];
z[18] = abb[14];
z[19] = abb[17];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = T(4) * z[4];
z[23] = T(2) * z[2];
z[24] = z[22] + z[23];
z[25] = T(3) * z[3];
z[26] = T(3) * z[12];
z[27] = z[5] + z[24] + z[25] + -z[26];
z[28] = T(2) * z[8];
z[27] = z[27] * z[28];
z[29] = T(4) * z[2];
z[30] = -z[5] + z[29];
z[31] = T(2) * z[4];
z[32] = -z[26] + z[30] + -z[31];
z[32] = z[1] * z[32];
z[33] = z[25] + z[31];
z[34] = T(4) * z[5];
z[35] = -z[29] + z[34];
z[36] = z[33] + -z[35];
z[36] = z[9] * z[36];
z[37] = -z[3] + -z[4] + z[12];
z[37] = T(3) * z[37];
z[38] = T(5) * z[5];
z[39] = T(-8) * z[2] + z[37] + z[38];
z[39] = z[10] * z[39];
z[40] = T(7) * z[3];
z[41] = -z[34] + z[40];
z[42] = T(3) * z[4];
z[43] = z[41] + z[42];
z[43] = z[6] * z[43];
z[44] = -(z[7] * z[41]);
z[27] = z[27] + z[32] + z[36] + z[39] + z[43] + z[44];
z[27] = z[11] * z[27];
z[32] = T(3) * z[5];
z[36] = z[32] + z[42];
z[39] = z[3] + z[36];
z[43] = T(3) * z[2];
z[44] = z[26] + z[39] + -z[43];
z[44] = z[28] * z[44];
z[45] = T(2) * z[5];
z[46] = z[31] + z[45];
z[47] = T(5) * z[2];
z[25] = z[25] + z[46] + -z[47];
z[25] = z[6] * z[25];
z[48] = T(5) * z[3];
z[49] = T(6) * z[4];
z[50] = -z[26] + z[48] + z[49];
z[51] = T(6) * z[5];
z[52] = z[50] + z[51];
z[53] = z[1] * z[52];
z[54] = z[2] + z[12];
z[55] = -z[46] + z[54];
z[56] = T(3) * z[10];
z[55] = z[55] * z[56];
z[57] = T(8) * z[7];
z[58] = z[2] + -z[4];
z[59] = -z[5] + z[58];
z[57] = z[57] * z[59];
z[60] = T(2) * z[9];
z[39] = z[39] * z[60];
z[25] = -z[25] + -z[39] + -z[44] + z[53] + z[55] + -z[57];
z[39] = -(z[14] * z[25]);
z[44] = T(2) * z[3];
z[53] = -z[44] + z[45] + z[58];
z[53] = z[8] * z[53];
z[22] = z[22] + -z[29];
z[55] = z[3] / T(2);
z[51] = z[22] + -z[51] + z[55];
z[51] = z[7] * z[51];
z[57] = (T(5) * z[3]) / T(2);
z[24] = (T(7) * z[5]) / T(2) + -z[24] + z[57];
z[24] = z[1] * z[24];
z[61] = z[23] + -z[31];
z[62] = (T(-7) * z[3]) / T(2) + z[38] + -z[61];
z[62] = z[9] * z[62];
z[63] = T(11) * z[2];
z[64] = z[3] + -z[4];
z[65] = T(-11) * z[5] + z[63] + T(5) * z[64];
z[65] = z[10] * z[65];
z[66] = (T(5) * z[2]) / T(2) + z[4] / T(2) + z[32] + -z[44];
z[66] = z[6] * z[66];
z[24] = z[24] + z[51] + z[53] + z[62] + z[65] / T(2) + z[66];
z[24] = z[13] * z[24];
z[24] = z[24] + z[27] + z[39];
z[24] = z[13] * z[24];
z[27] = T(8) * z[12];
z[39] = (T(5) * z[5]) / T(2);
z[51] = T(17) * z[4] + z[40];
z[51] = (T(-7) * z[2]) / T(2) + z[27] + -z[39] + z[51] / T(12);
z[51] = z[10] * z[51];
z[53] = z[5] + z[42];
z[57] = (T(23) * z[12]) / T(2) + z[53] + -z[57];
z[57] = -z[47] + z[57] / T(2);
z[57] = z[20] * z[57];
z[62] = T(4) * z[3];
z[65] = (T(11) * z[4]) / T(4) + -z[62];
z[27] = (T(35) * z[2]) / T(12) + (T(85) * z[5]) / T(12) + -z[27] + z[65] / T(3);
z[27] = z[1] * z[27];
z[65] = T(2) * z[12];
z[66] = (T(43) * z[4]) / T(3) + z[40];
z[66] = (T(-4) * z[2]) / T(3) + -z[5] + -z[65] + z[66] / T(4);
z[66] = z[6] * z[66];
z[67] = z[2] / T(3);
z[68] = -z[3] + z[5];
z[69] = z[4] / T(3) + -z[67] + z[68];
z[70] = T(2) * z[7];
z[69] = z[69] * z[70];
z[71] = (T(19) * z[2]) / T(12) + z[3] + (T(-91) * z[4]) / T(12) + (T(-79) * z[5]) / T(12) + T(4) * z[12];
z[71] = z[8] * z[71];
z[72] = (T(31) * z[2]) / T(12) + z[3] + (T(-79) * z[4]) / T(12) + (T(-67) * z[5]) / T(12) + z[65];
z[72] = z[9] * z[72];
z[27] = z[27] + z[51] + (T(3) * z[57]) / T(2) + z[66] + z[69] + z[71] + z[72];
z[51] = prod_pow(z[15], 2);
z[27] = z[27] * z[51];
z[25] = z[13] * z[25];
z[57] = z[3] + z[31];
z[66] = z[23] + -z[32] + z[57] + z[65];
z[66] = z[1] * z[66];
z[69] = (T(5) * z[12]) / T(2);
z[49] = z[43] + z[49] + z[55] + -z[69];
z[49] = z[10] * z[49];
z[71] = z[3] + z[12];
z[72] = z[2] + -z[5];
z[73] = z[71] / T(2) + z[72];
z[73] = z[6] * z[73];
z[74] = z[3] + z[46];
z[75] = T(-5) * z[12] + z[23] + z[74];
z[75] = z[9] * z[75];
z[29] = T(7) * z[12] + -z[29] + -z[74];
z[74] = z[8] * z[29];
z[49] = -z[49] + z[66] + z[73] + -z[74] + z[75];
z[66] = -(z[18] * z[49]);
z[69] = z[55] + z[69];
z[73] = T(7) * z[4];
z[74] = z[38] + -z[69] + z[73];
z[74] = -z[2] + z[74] / T(2);
z[74] = prod_pow(z[20], 2) * z[74];
z[66] = z[66] + z[74];
z[74] = (T(3) * z[12]) / T(2);
z[75] = (T(-13) * z[3]) / T(2) + z[32] + z[61] + -z[74];
z[75] = z[6] * z[75];
z[76] = z[8] + z[9];
z[52] = z[52] * z[76];
z[77] = z[3] + z[42];
z[78] = -z[26] + z[32] + T(2) * z[77];
z[78] = z[1] * z[78];
z[71] = z[10] * z[71];
z[52] = z[52] + (T(-3) * z[71]) / T(2) + -z[75] + -z[78];
z[52] = z[11] * z[52];
z[71] = -z[58] + z[68];
z[75] = z[12] + z[71];
z[75] = -(z[75] * z[76]);
z[65] = -z[2] + -z[4] + -z[5] + z[65];
z[78] = -(z[1] * z[65]);
z[75] = z[75] + z[78];
z[29] = z[10] * z[29];
z[29] = z[29] + T(2) * z[75];
z[50] = -z[45] + z[50];
z[50] = z[6] * z[50];
z[75] = z[7] * z[68];
z[29] = T(3) * z[29] + z[50] + T(8) * z[75];
z[29] = z[14] * z[29];
z[50] = -z[58] + z[62];
z[78] = z[11] * z[50];
z[79] = z[18] * z[71];
z[79] = -z[78] + T(-3) * z[79];
z[79] = z[70] * z[79];
z[80] = -z[3] + (T(11) * z[4]) / T(3);
z[67] = (T(49) * z[5]) / T(3) + T(-11) * z[12] + -z[67] + T(5) * z[80];
z[51] = z[51] * z[67];
z[25] = z[25] + z[29] + z[51] / T(4) + z[52] + T(3) * z[66] + z[79];
z[29] = int_to_imaginary<T>(1) * z[15];
z[25] = z[25] * z[29];
z[51] = -z[26] + z[36] + z[44];
z[60] = z[28] + z[60];
z[51] = z[51] * z[60];
z[60] = -z[2] + z[26] + -z[45] + -z[77];
z[60] = z[1] * z[60];
z[37] = z[2] + -z[34] + z[37];
z[37] = z[10] * z[37];
z[50] = z[6] * z[50];
z[37] = z[37] + z[50] + z[51] + z[60];
z[37] = z[11] * z[37];
z[50] = z[4] + -z[45] + z[54];
z[50] = z[1] * z[50];
z[31] = -z[5] + z[31];
z[51] = -z[12] + z[23] + z[31];
z[51] = z[10] * z[51];
z[54] = z[65] * z[76];
z[50] = z[50] + -z[51] + -z[54];
z[51] = z[6] * z[68];
z[50] = T(3) * z[50] + z[51] + -z[75];
z[29] = -z[14] + z[29];
z[29] = z[29] * z[50];
z[44] = -z[43] + z[44];
z[36] = -z[36] + z[44];
z[36] = z[36] * z[76];
z[50] = T(5) * z[4];
z[32] = -z[32] + z[44] + z[50];
z[32] = z[1] * z[32];
z[44] = T(2) * z[6];
z[51] = T(4) * z[7] + z[44];
z[54] = z[5] + z[58];
z[51] = z[51] * z[54];
z[60] = z[10] * z[64];
z[32] = z[32] + z[36] + z[51] + T(-4) * z[60];
z[32] = z[13] * z[32];
z[36] = -(z[7] * z[78]);
z[29] = z[29] + z[32] + z[36] + z[37];
z[32] = T(3) * z[76];
z[32] = z[32] * z[59];
z[36] = -z[35] + -z[57];
z[36] = z[1] * z[36];
z[37] = T(7) * z[2] + -z[64];
z[51] = T(7) * z[5];
z[57] = -z[37] + z[51];
z[57] = z[10] * z[57];
z[59] = z[47] + z[68];
z[60] = z[4] + z[59];
z[60] = z[6] * z[60];
z[59] = z[50] + -z[59];
z[59] = z[7] * z[59];
z[32] = z[32] + z[36] + z[57] + z[59] + z[60];
z[32] = z[0] * z[32];
z[29] = T(2) * z[29] + z[32];
z[29] = z[0] * z[29];
z[30] = z[30] + T(-2) * z[33];
z[28] = z[28] * z[30];
z[30] = z[3] + T(13) * z[5] + z[61];
z[30] = z[1] * z[30];
z[32] = T(-9) * z[3] + z[34] + z[61];
z[32] = z[9] * z[32];
z[33] = z[10] * z[71];
z[34] = T(-10) * z[3] + -z[45] + z[58];
z[34] = z[6] * z[34];
z[28] = z[28] + z[30] + z[32] + T(-11) * z[33] + z[34];
z[28] = z[16] * z[28];
z[30] = T(-6) * z[12] + z[47] + z[51] + z[62] + z[73];
z[30] = z[1] * z[30];
z[32] = -z[5] + -z[22] + -z[40] + z[74];
z[32] = z[9] * z[32];
z[33] = (T(-63) * z[3]) / T(2) + -z[38] + -z[50] + -z[74];
z[23] = -z[23] + z[33] / T(2);
z[23] = z[6] * z[23];
z[33] = (T(-23) * z[3]) / T(2) + (T(9) * z[12]) / T(2) + -z[35] + -z[73];
z[33] = z[8] * z[33];
z[34] = (T(11) * z[3]) / T(2) + (T(7) * z[12]) / T(2) + -z[53];
z[34] = -z[2] + z[34] / T(2);
z[34] = z[34] * z[56];
z[23] = z[23] + z[30] + z[32] + z[33] + z[34];
z[30] = prod_pow(z[11], 2);
z[23] = z[23] * z[30];
z[32] = z[2] + z[46] + -z[69];
z[32] = z[8] * z[32];
z[26] = z[26] + -z[39] + z[55];
z[26] = z[1] * z[26];
z[33] = z[46] + -z[55] + -z[74];
z[33] = z[9] * z[33];
z[34] = (T(9) * z[12]) / T(4);
z[31] = z[2] / T(2) + z[3] / T(4) + -z[31] + -z[34];
z[31] = z[10] * z[31];
z[26] = z[26] + z[31] + z[32] + z[33];
z[31] = z[42] + -z[43] + -z[68];
z[31] = z[7] * z[31];
z[32] = (T(3) * z[2]) / T(2) + (T(-7) * z[3]) / T(4) + -z[5] / T(2) + z[34] + -z[42];
z[32] = z[6] * z[32];
z[26] = T(3) * z[26] + z[31] + z[32];
z[26] = z[14] * z[26];
z[31] = z[70] * z[78];
z[26] = z[26] + z[31] + -z[52];
z[26] = z[14] * z[26];
z[31] = z[70] * z[71];
z[31] = z[31] + z[49];
z[31] = z[17] * z[31];
z[32] = T(6) * z[76];
z[33] = z[2] + -z[3];
z[32] = z[32] * z[33];
z[33] = -z[5] + z[37];
z[33] = z[1] * z[33];
z[32] = z[32] + z[33];
z[33] = z[64] + -z[72];
z[33] = z[10] * z[33];
z[34] = -z[48] + -z[54];
z[34] = z[34] * z[44];
z[32] = T(2) * z[32] + T(14) * z[33] + z[34];
z[32] = z[19] * z[32];
z[33] = -z[58] + -z[68];
z[33] = z[19] * z[33];
z[34] = z[41] + T(-16) * z[58];
z[34] = z[16] * z[34];
z[22] = (T(21) * z[3]) / T(2) + z[22] + -z[45];
z[22] = z[22] * z[30];
z[22] = z[22] + T(10) * z[33] + z[34];
z[22] = z[7] * z[22];
z[30] = (T(45) * z[3]) / T(2) + T(133) * z[4];
z[30] = T(453) * z[5] + (T(-1085) * z[12]) / T(2) + T(3) * z[30];
z[30] = z[30] / T(2) + z[63];
z[30] = z[21] * z[30];
return z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] / T(4) + T(3) * z[31] + z[32];
}



template IntegrandConstructorType<double> f_4_153_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_153_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_153_construct (const Kin<qd_real>&);
#endif

}