#include "f_4_106.h"

namespace PentagonFunctions {

template <typename T> T f_4_106_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_106_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_106_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[2] * (T(6) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + T(6) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(8) * kin.v[3] + T(-16) * kin.v[4]) + kin.v[2] * (T(8) * kin.v[2] + T(16) * kin.v[3] + T(-16) * kin.v[4])) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[0] + T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) * kin.v[2] + T(-16) * kin.v[3] + T(16) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[2] * (kin.v[2] / T(2) + -kin.v[4] + T(-14) + T(-13) * kin.v[3]) + (kin.v[4] / T(2) + T(14)) * kin.v[4] + kin.v[1] * (T(14) * kin.v[2] + T(14) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[0] * (T(14) + (T(-27) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-14) * kin.v[1] + T(13) * kin.v[2] + T(27) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[1] + T(8) * kin.v[3]) + T(-13) * kin.v[4]) + kin.v[3] * ((T(-27) * kin.v[3]) / T(2) + T(-14) + T(13) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(4) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[4] / T(2) + T(-14)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(14) + T(13) * kin.v[3] + kin.v[4]) + kin.v[3] * ((T(27) * kin.v[3]) / T(2) + T(14) + T(-13) * kin.v[4]) + kin.v[0] * (T(-14) + (T(27) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(14) * kin.v[1] + T(-13) * kin.v[2] + T(-27) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(-8) * kin.v[3]) + T(13) * kin.v[4]) + kin.v[1] * (T(-14) * kin.v[2] + T(-14) * kin.v[3] + T(14) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4])));
c[1] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[1] * (abb[6] * T(-6) + abb[4] * (abb[5] * T(-8) + abb[4] * T(-7) + bc<T>[0] * int_to_imaginary<T>(8))) + abb[6] * abb[7] * T(6) + abb[4] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[4] * abb[7] * T(7) + abb[5] * abb[7] * T(8)) + abb[2] * (abb[2] * (abb[1] + -abb[7]) + abb[5] * abb[7] * T(-8) + abb[4] * abb[7] * T(-6) + abb[7] * bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * (bc<T>[0] * int_to_imaginary<T>(-8) + abb[4] * T(6) + abb[5] * T(8))) + abb[3] * (abb[4] * abb[7] * T(-8) + abb[1] * abb[4] * T(8) + abb[2] * (abb[1] * T(-8) + abb[7] * T(8))));
    }
};
template <typename T> class SpDLog_f_4_106_W_10 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_106_W_10 (const Kin<T>& kin) {
        c[0] = (T(-9) / T(2) + (T(9) * kin.v[2]) / T(8) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + (T(-9) / T(2) + (T(9) * kin.v[1]) / T(8) + (T(9) * kin.v[2]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[1];
c[1] = rlog(-kin.v[4]) * (kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(-12) + T(6) * kin.v[1] + T(12) * kin.v[2] + T(-12) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * ((T(23) * kin.v[2]) / T(4) + T(-23) + T(-23) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * ((T(23) * kin.v[2]) / T(2) + T(-23) + (T(23) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(-23) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[2] + T(-8) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-31) / T(2) + (T(7) * kin.v[2]) / T(8) + (T(-31) * kin.v[4]) / T(2)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(-31) / T(2) + (T(7) * kin.v[2]) / T(4) + (T(-31) * kin.v[4]) / T(2) + (T(7) / T(8) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[2] + T(-8) * kin.v[4]))) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(-8) * kin.v[4]) + (T(-9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + kin.v[1] * (T(-9) / T(2) + (T(-9) * kin.v[1]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-9) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[2] * ((T(-23) * kin.v[2]) / T(4) + T(23) + T(23) * kin.v[4]) + kin.v[1] * ((T(-23) * kin.v[2]) / T(2) + T(23) + (T(-23) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(23) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[2] + T(8) * kin.v[4])));
c[2] = (T(9) * kin.v[1]) / T(2) + (T(9) * kin.v[2]) / T(2);
c[3] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-9) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-9) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-9) * kin.v[2]) / T(2) + (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(9)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[2] + T(9) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[8] * ((abb[10] * abb[11] * T(9)) / T(2) + prod_pow(abb[4], 2) * abb[9] * T(6) + abb[4] * (abb[4] * ((abb[1] * T(-23)) / T(2) + (abb[11] * T(-9)) / T(4) + (abb[12] * T(23)) / T(2) + (abb[7] * T(31)) / T(4)) + abb[2] * (abb[7] * T(-8) + abb[12] * T(-8)) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[1] * (abb[5] * T(-8) + bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * T(8)) + abb[5] * (abb[7] * T(8) + abb[12] * T(8))) + abb[10] * ((abb[7] * T(9)) / T(2) + abb[1] * T(-9) + abb[12] * T(9)) + abb[3] * (abb[5] * (abb[7] * T(-8) + abb[12] * T(-8)) + abb[3] * ((abb[7] * T(-13)) / T(4) + (abb[12] * T(-5)) / T(2) + (abb[1] * T(5)) / T(2) + (abb[11] * T(27)) / T(4) + abb[9] * T(-6)) + abb[7] * bc<T>[0] * int_to_imaginary<T>(8) + abb[12] * bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * (abb[2] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[5] * T(8)) + abb[2] * (abb[7] * T(8) + abb[12] * T(8)) + abb[4] * ((abb[7] * T(-9)) / T(2) + (abb[11] * T(-9)) / T(2) + abb[12] * T(-9) + abb[1] * T(9))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_106_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl10 = DLog_W_10<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl5 = DLog_W_5<T>(kin),dl24 = DLog_W_24<T>(kin),spdl23 = SpDLog_f_4_106_W_23<T>(kin),spdl10 = SpDLog_f_4_106_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,21> abbr = 
            {dl23(t), rlog(kin.W[15] / kin_path.W[15]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_8(kin_path), rlog(kin.W[16] / kin_path.W[16]), dl10(t), rlog(kin.W[4] / kin_path.W[4]), f_2_1_7(kin_path), -rlog(t), rlog(kin.W[17] / kin_path.W[17]), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(kin.W[23] / kin_path.W[23]), dl16(t), dl17(t), dl5(t), dl24(t)}
;

        auto result = f_4_106_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_106_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[82];
z[0] = abb[1];
z[1] = abb[2];
z[2] = abb[13];
z[3] = abb[18];
z[4] = abb[3];
z[5] = abb[17];
z[6] = abb[19];
z[7] = abb[4];
z[8] = abb[5];
z[9] = bc<TR>[0];
z[10] = abb[20];
z[11] = abb[6];
z[12] = abb[10];
z[13] = abb[14];
z[14] = abb[15];
z[15] = abb[7];
z[16] = abb[9];
z[17] = abb[11];
z[18] = abb[12];
z[19] = abb[16];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = T(2) * z[15];
z[23] = T(2) * z[17];
z[24] = T(5) * z[16] + z[22] + -z[23];
z[24] = z[5] * z[24];
z[25] = T(2) * z[5];
z[26] = -z[6] + -z[10] + z[25];
z[27] = T(2) * z[2];
z[28] = z[26] + z[27];
z[28] = z[19] * z[28];
z[29] = z[16] + -z[17];
z[30] = -z[0] + z[29];
z[27] = z[27] * z[30];
z[30] = z[0] + -z[17];
z[31] = T(2) * z[3];
z[30] = z[30] * z[31];
z[31] = T(2) * z[18];
z[26] = z[26] * z[31];
z[29] = z[15] + z[29];
z[29] = -z[0] + T(2) * z[29];
z[29] = z[6] * z[29];
z[32] = T(-3) * z[5] + T(2) * z[10];
z[32] = z[0] * z[32];
z[33] = z[10] * z[16];
z[34] = T(3) * z[33];
z[24] = z[24] + z[26] + -z[27] + -z[28] + -z[29] + z[30] + z[32] + -z[34];
z[24] = T(3) * z[24];
z[26] = -(z[14] * z[24]);
z[27] = T(6) * z[1];
z[28] = T(7) * z[4];
z[29] = z[27] + z[28];
z[30] = T(7) * z[7];
z[32] = T(2) * z[8];
z[35] = z[29] + -z[30] + -z[32];
z[35] = z[0] * z[35];
z[36] = T(4) * z[8];
z[37] = T(4) * z[7];
z[38] = -z[1] + z[37];
z[39] = -z[36] + -z[38];
z[39] = z[23] * z[39];
z[40] = T(2) * z[4];
z[41] = -z[1] + z[40];
z[42] = z[7] + -z[41];
z[43] = z[32] + -z[42];
z[43] = z[16] * z[43];
z[44] = T(9) * z[4];
z[45] = T(7) * z[1] + z[44];
z[46] = T(16) * z[7] + -z[45];
z[47] = T(8) * z[8];
z[48] = z[46] + z[47];
z[48] = z[15] * z[48];
z[35] = z[35] + z[39] + z[43] + z[48];
z[35] = z[3] * z[35];
z[39] = T(6) * z[4];
z[43] = T(6) * z[8];
z[48] = -z[1] + z[7];
z[49] = -z[39] + -z[43] + -z[48];
z[49] = z[15] * z[49];
z[50] = T(5) * z[4];
z[51] = -z[1] + z[50];
z[52] = T(5) * z[8];
z[53] = -z[7] + z[51] + z[52];
z[54] = z[16] * z[53];
z[54] = T(2) * z[54];
z[55] = T(4) * z[4];
z[56] = -z[1] + z[55];
z[57] = z[36] + z[56];
z[57] = z[23] * z[57];
z[58] = -z[7] + T(4) * z[41] + z[47];
z[58] = z[0] * z[58];
z[59] = prod_pow(z[20], 2);
z[49] = z[49] + -z[54] + z[57] + z[58] + (T(9) * z[59]) / T(2);
z[49] = z[6] * z[49];
z[57] = -z[7] + z[55];
z[23] = z[23] * z[57];
z[23] = z[23] + T(3) * z[59];
z[57] = T(3) * z[4];
z[58] = z[38] + -z[57];
z[60] = z[32] + z[58];
z[22] = z[22] * z[60];
z[61] = T(3) * z[1];
z[62] = z[55] + z[61];
z[63] = T(3) * z[8];
z[64] = z[62] + z[63];
z[65] = -z[37] + z[64];
z[65] = z[0] * z[65];
z[22] = z[22] + z[23] + -z[54] + T(2) * z[65];
z[22] = z[2] * z[22];
z[54] = T(2) * z[7];
z[65] = -z[50] + z[54] + z[61];
z[66] = -z[63] + z[65];
z[66] = z[2] * z[66];
z[64] = -z[7] + z[64];
z[64] = z[5] * z[64];
z[60] = z[3] * z[60];
z[53] = -(z[6] * z[53]);
z[61] = -(z[10] * z[61]);
z[53] = z[53] + z[60] + z[61] + z[64] + z[66];
z[31] = z[31] * z[53];
z[53] = T(2) * z[1];
z[60] = -z[7] + z[53];
z[61] = z[28] + -z[32] + -z[60];
z[61] = z[15] * z[61];
z[64] = z[1] + z[28];
z[66] = -z[7] + z[32] + T(2) * z[64];
z[66] = z[16] * z[66];
z[23] = -z[23] + z[61] + z[66];
z[23] = z[5] * z[23];
z[61] = -z[1] + -z[8];
z[25] = z[25] * z[61];
z[61] = z[1] + z[32];
z[61] = z[10] * z[61];
z[66] = z[1] * z[6];
z[67] = -(z[2] * z[53]);
z[25] = z[25] + z[61] + z[66] + z[67];
z[61] = T(3) * z[19];
z[25] = z[25] * z[61];
z[66] = z[10] / T(2);
z[59] = -(z[59] * z[66]);
z[67] = z[1] + z[40];
z[68] = z[32] + -z[67];
z[68] = z[33] * z[68];
z[59] = z[59] + z[68];
z[68] = T(13) * z[4];
z[69] = -z[27] + z[68];
z[43] = z[43] + -z[54] + -z[69];
z[43] = z[5] * z[43];
z[70] = T(6) * z[10];
z[71] = z[4] + -z[32];
z[71] = z[70] * z[71];
z[43] = z[43] + z[71];
z[43] = z[0] * z[43];
z[71] = prod_pow(z[9], 2);
z[72] = (T(15) * z[2]) / T(4) + -z[3] / T(6) + -z[5] + (T(29) * z[6]) / T(12) + (T(-5) * z[10]) / T(4);
z[72] = z[71] * z[72];
z[22] = z[22] + z[23] + z[25] + z[26] + z[31] + z[35] + z[43] + z[49] + T(3) * z[59] + z[72];
z[22] = int_to_imaginary<T>(1) * z[9] * z[22];
z[23] = -(z[13] * z[24]);
z[24] = z[20] * z[66];
z[24] = z[24] + z[33];
z[25] = z[15] + -z[17];
z[26] = z[0] + -z[16];
z[25] = (T(13) * z[25]) / T(2) + (T(-5) * z[26]) / T(3);
z[25] = z[3] * z[25];
z[26] = (T(3) * z[20]) / T(2);
z[31] = -z[17] / T(3) + -z[26];
z[31] = (T(-5) * z[15]) / T(6) + (T(13) * z[16]) / T(12) + T(5) * z[31];
z[31] = z[5] * z[31];
z[33] = (T(47) * z[5]) / T(12) + T(-7) * z[10];
z[33] = z[0] * z[33];
z[35] = (T(67) * z[0]) / T(3) + (T(-29) * z[15]) / T(3) + (T(-77) * z[16]) / T(3) + T(13) * z[17] + T(21) * z[20];
z[35] = z[6] * z[35];
z[26] = (T(31) * z[0]) / T(12) + (T(3) * z[15]) / T(2) + (T(-27) * z[16]) / T(4) + (T(5) * z[17]) / T(3) + z[26];
z[26] = z[2] * z[26];
z[43] = (T(-25) * z[2]) / T(4) + (T(3) * z[3]) / T(2) + (T(17) * z[5]) / T(4) + (T(-27) * z[6]) / T(4) + z[10];
z[43] = z[18] * z[43];
z[49] = -z[5] + z[10];
z[49] = z[2] + (T(5) * z[49]) / T(2);
z[49] = z[19] * z[49];
z[24] = (T(9) * z[24]) / T(2) + z[25] / T(2) + z[26] + z[31] + z[33] + z[35] / T(4) + z[43] + z[49];
z[24] = z[24] * z[71];
z[25] = T(5) * z[7];
z[26] = z[8] + z[25] + -z[28] + z[53];
z[26] = z[8] * z[26];
z[28] = (T(7) * z[4]) / T(2);
z[31] = T(4) * z[1];
z[33] = z[28] + -z[31];
z[33] = z[4] * z[33];
z[35] = T(2) * z[11];
z[43] = prod_pow(z[1], 2);
z[49] = T(5) * z[43];
z[59] = prod_pow(z[7], 2);
z[66] = T(11) * z[12];
z[26] = z[26] + z[33] + z[35] + z[49] + (T(-3) * z[59]) / T(2) + z[66];
z[26] = z[15] * z[26];
z[33] = -z[4] + z[53];
z[33] = z[33] * z[55];
z[71] = -z[4] + z[7];
z[47] = z[47] * z[71];
z[60] = z[7] * z[60];
z[72] = T(10) * z[11];
z[33] = T(-16) * z[12] + z[33] + -z[47] + -z[49] + z[60] + -z[72];
z[33] = z[17] * z[33];
z[47] = z[50] + -z[53];
z[47] = z[4] * z[47];
z[49] = T(4) * z[12];
z[50] = z[43] / T(2);
z[60] = z[49] + z[50];
z[73] = T(8) * z[7];
z[64] = -z[64] + z[73];
z[64] = -z[8] + T(2) * z[64];
z[64] = z[8] * z[64];
z[74] = T(5) * z[11];
z[75] = (T(-15) * z[7]) / T(2) + z[56];
z[75] = z[7] * z[75];
z[60] = z[47] + T(3) * z[60] + z[64] + z[74] + z[75];
z[60] = z[16] * z[60];
z[26] = z[26] + z[33] + z[60];
z[26] = z[5] * z[26];
z[60] = T(8) * z[11];
z[64] = z[60] + z[66];
z[28] = z[28] + z[31];
z[28] = z[4] * z[28];
z[75] = (T(11) * z[7]) / T(2) + T(-2) * z[62];
z[75] = z[7] * z[75];
z[29] = T(13) * z[7] + z[8] + -z[29];
z[29] = z[8] * z[29];
z[76] = T(4) * z[43];
z[28] = z[28] + z[29] + z[64] + z[75] + z[76];
z[28] = z[0] * z[28];
z[29] = z[43] + z[72];
z[72] = z[32] + z[48];
z[72] = z[32] * z[72];
z[75] = prod_pow(z[4], 2);
z[77] = T(8) * z[1];
z[78] = (T(-17) * z[4]) / T(2) + (T(75) * z[7]) / T(4) + -z[77];
z[78] = z[7] * z[78];
z[72] = (T(17) * z[12]) / T(2) + z[29] + z[72] + -z[75] / T(4) + z[78];
z[72] = z[17] * z[72];
z[75] = T(2) * z[12] + z[50] + z[74];
z[78] = z[4] + z[31];
z[78] = z[4] * z[78];
z[42] = -z[8] + z[42];
z[79] = z[8] * z[42];
z[78] = (T(-15) * z[59]) / T(2) + -z[75] + z[78] + z[79];
z[78] = z[16] * z[78];
z[46] = -z[36] + -z[46];
z[46] = z[8] * z[46];
z[79] = T(3) * z[43];
z[80] = (T(-17) * z[4]) / T(4) + -z[77];
z[80] = z[4] * z[80];
z[81] = T(17) * z[1] + (T(33) * z[4]) / T(2) + (T(-85) * z[7]) / T(4);
z[81] = z[7] * z[81];
z[46] = T(-16) * z[11] + (T(-35) * z[12]) / T(2) + z[46] + -z[79] + z[80] + z[81];
z[46] = z[15] * z[46];
z[28] = z[28] + z[46] + z[72] + z[78];
z[28] = z[3] * z[28];
z[46] = -z[30] + z[62];
z[46] = T(2) * z[46] + z[63];
z[46] = z[8] * z[46];
z[27] = (T(19) * z[7]) / T(2) + -z[27] + -z[68];
z[27] = z[7] * z[27];
z[62] = T(7) * z[12];
z[68] = T(14) * z[1] + -z[4] / T(2);
z[68] = z[4] * z[68];
z[27] = z[27] + -z[35] + -z[46] + z[62] + z[68] + -z[76];
z[27] = z[0] * z[27];
z[58] = z[8] + z[58];
z[58] = z[32] * z[58];
z[68] = (T(5) * z[4]) / T(2) + z[31];
z[68] = z[4] * z[68];
z[58] = z[58] + z[68];
z[45] = z[45] + -z[73];
z[45] = z[7] * z[45];
z[45] = (T(-3) * z[43]) / T(2) + z[45] + -z[58] + -z[66] + -z[74];
z[45] = z[15] * z[45];
z[66] = -z[4] + z[48];
z[66] = z[37] * z[66];
z[37] = -z[37] + z[51];
z[37] = T(2) * z[37] + z[52];
z[37] = z[8] * z[37];
z[37] = z[37] + -z[43];
z[51] = z[4] + z[53];
z[52] = z[4] * z[51];
z[68] = T(12) * z[12];
z[52] = z[37] + -z[52] + -z[66] + -z[68];
z[66] = z[16] * z[52];
z[27] = z[27] + -z[33] + z[45] + z[66];
z[27] = z[2] * z[27];
z[33] = T(3) * z[7];
z[45] = -z[32] + z[33] + -z[56];
z[45] = z[32] * z[45];
z[56] = -z[44] + z[77];
z[56] = z[4] * z[56];
z[66] = -z[25] + z[55];
z[66] = z[7] * z[66];
z[29] = -z[29] + z[45] + -z[49] + z[56] + z[66];
z[29] = z[17] * z[29];
z[45] = z[33] + -z[51];
z[45] = z[45] * z[54];
z[36] = z[36] * z[42];
z[35] = z[35] + z[36] + z[45] + z[47] + z[68];
z[35] = z[0] * z[35];
z[25] = -z[1] + -z[25] + z[39] + z[63];
z[25] = z[8] * z[25];
z[36] = -z[1] + z[4];
z[36] = z[36] * z[55];
z[39] = (T(7) * z[7]) / T(2) + -z[55];
z[39] = z[7] * z[39];
z[25] = z[25] + z[36] + z[39] + z[75];
z[25] = z[15] * z[25];
z[36] = -z[33] + T(2) * z[51];
z[36] = z[7] * z[36];
z[39] = -(z[4] * z[53]);
z[36] = T(-10) * z[12] + z[36] + z[37] + z[39];
z[36] = z[16] * z[36];
z[25] = z[25] + z[29] + z[35] + z[36];
z[25] = z[6] * z[25];
z[29] = -z[1] + z[57];
z[29] = z[29] * z[40];
z[35] = T(14) * z[12];
z[36] = -z[7] + -z[55];
z[36] = z[7] * z[36];
z[29] = z[29] + z[35] + z[36] + z[43] + -z[46] + z[60];
z[29] = z[5] * z[29];
z[36] = z[63] + T(-2) * z[65];
z[36] = z[8] * z[36];
z[37] = -z[1] + -z[4];
z[37] = z[37] * z[40];
z[31] = z[31] + z[57];
z[31] = -z[30] + T(2) * z[31];
z[31] = z[7] * z[31];
z[31] = z[31] + -z[35] + z[36] + z[37] + -z[79];
z[31] = z[2] * z[31];
z[35] = z[6] * z[52];
z[36] = T(10) * z[1] + (T(-25) * z[7]) / T(2) + z[44];
z[36] = z[7] * z[36];
z[36] = z[36] + -z[58] + -z[64];
z[36] = z[3] * z[36];
z[37] = z[8] * z[48];
z[39] = z[7] * z[48];
z[39] = -z[37] + z[39];
z[39] = z[39] * z[70];
z[29] = z[29] + z[31] + z[35] + z[36] + z[39];
z[29] = z[18] * z[29];
z[31] = -z[1] + z[7] / T(2);
z[31] = z[7] * z[31];
z[35] = z[11] + z[50];
z[36] = z[8] + T(-2) * z[48];
z[36] = z[8] * z[36];
z[36] = z[31] + -z[35] + z[36];
z[36] = z[5] * z[36];
z[35] = z[35] + z[37] + -z[59] / T(2);
z[35] = z[6] * z[35];
z[32] = -(z[32] * z[48]);
z[37] = -z[11] + z[50];
z[31] = z[31] + z[32] + z[37];
z[31] = z[2] * z[31];
z[32] = z[1] * z[7];
z[39] = -z[8] + z[48];
z[39] = z[8] * z[39];
z[32] = z[32] + z[39];
z[32] = z[10] * z[32];
z[39] = -z[1] + (T(3) * z[7]) / T(2);
z[39] = z[7] * z[39];
z[37] = -z[37] + z[39];
z[37] = z[3] * z[37];
z[31] = z[31] + z[32] + z[35] + z[36] + z[37];
z[31] = z[31] * z[61];
z[30] = -z[30] + -z[63] + z[69];
z[30] = z[8] * z[30];
z[32] = (T(-9) * z[4]) / T(2) + -z[53];
z[32] = z[4] * z[32];
z[35] = (T(17) * z[7]) / T(2) + T(-2) * z[41];
z[35] = z[7] * z[35];
z[30] = T(6) * z[11] + z[30] + z[32] + z[35] + -z[62];
z[30] = z[5] * z[30];
z[32] = z[4] + -z[54];
z[32] = z[7] * z[32];
z[35] = z[8] + z[71];
z[35] = z[8] * z[35];
z[32] = z[32] + z[35];
z[32] = z[32] * z[70];
z[30] = z[30] + z[32];
z[30] = z[0] * z[30];
z[32] = z[38] + -z[40];
z[32] = z[7] * z[32];
z[33] = -z[8] + -z[33] + z[67];
z[33] = z[8] * z[33];
z[32] = z[32] + z[33];
z[32] = z[32] * z[34];
z[33] = T(-95) * z[5] + (T(-21) * z[10]) / T(2);
z[33] = (T(105) * z[2]) / T(4) + (T(32) * z[3]) / T(3) + (T(377) * z[6]) / T(24) + z[33] / T(4);
z[33] = z[21] * z[33];
return z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[31] + z[32] + z[33];
}



template IntegrandConstructorType<double> f_4_106_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_106_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_106_construct (const Kin<qd_real>&);
#endif

}