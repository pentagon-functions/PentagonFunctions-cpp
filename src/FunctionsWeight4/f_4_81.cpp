#include "f_4_81.h"

namespace PentagonFunctions {

template <typename T> T f_4_81_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_81_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_81_W_23 (const Kin<T>& kin) {
        c[0] = T(2) * prod_pow(kin.v[3], 2) + kin.v[0] * ((T(20) * kin.v[2]) / T(3) + (T(-4) * kin.v[3]) / T(3) + (T(-8) * kin.v[4]) / T(3) + (T(-2) / T(3) + bc<T>[1] * T(-2)) * kin.v[0] + T(4) * kin.v[1] + bc<T>[1] * (T(4) + T(-4) * kin.v[1] + T(4) * kin.v[3])) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(-4) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = T(2) * prod_pow(kin.v[3], 2) + kin.v[0] * ((T(-2) * kin.v[0]) / T(3) + (T(20) * kin.v[2]) / T(3) + (T(-4) * kin.v[3]) / T(3) + (T(-8) * kin.v[4]) / T(3) + T(4) * kin.v[1]) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(-4) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-prod_pow(kin.v[4], 2) + kin.v[0] * (-kin.v[0] / T(3) + (T(10) * kin.v[2]) / T(3) + (T(-2) * kin.v[3]) / T(3) + (T(-4) * kin.v[4]) / T(3) + T(2) * kin.v[1]) + prod_pow(kin.v[3], 2) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (T(-3) * kin.v[2] + T(-2) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[1] * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + bc<T>[1] * (kin.v[0] * ((T(-2) * kin.v[0]) / T(3) + (T(20) * kin.v[2]) / T(3) + (T(-4) * kin.v[3]) / T(3) + (T(-8) * kin.v[4]) / T(3) + T(4) * kin.v[1]) + T(2) * prod_pow(kin.v[3], 2) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(-4) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[1] * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * ((T(-5) * kin.v[2]) / T(3) + kin.v[3] / T(3) + (T(2) * kin.v[4]) / T(3) + -kin.v[1] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[1])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * ((T(-5) * kin.v[2]) / T(3) + kin.v[3] / T(3) + (T(2) * kin.v[4]) / T(3) + -kin.v[1] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[1])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(kin.v[3]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * ((T(-5) * kin.v[2]) / T(3) + kin.v[3] / T(3) + (T(2) * kin.v[4]) / T(3) + -kin.v[1] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[1])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * ((T(-5) * kin.v[2]) / T(3) + kin.v[3] / T(3) + (T(2) * kin.v[4]) / T(3) + -kin.v[1] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[1])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[4]) * ((T(3) * prod_pow(kin.v[3], 2)) / T(2) + (T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[0] * (-kin.v[3] + ((bc<T>[1] * T(-3)) / T(2) + T(-1) / T(2)) * kin.v[0] + T(3) * kin.v[1] + T(5) * kin.v[2] + bc<T>[1] * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + T(-2) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(-3) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(3) * prod_pow(kin.v[3], 2)) / T(2) + (T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[0] * (-kin.v[3] + ((bc<T>[1] * T(-3)) / T(2) + T(-1) / T(2)) * kin.v[0] + T(3) * kin.v[1] + T(5) * kin.v[2] + bc<T>[1] * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + T(-2) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(-3) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]))) + rlog(kin.v[0]) * (-prod_pow(kin.v[3], 2) + kin.v[0] * ((T(-10) * kin.v[2]) / T(3) + (T(2) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3) + (bc<T>[1] + T(1) / T(3)) * kin.v[0] + T(-2) * kin.v[1] + bc<T>[1] * (T(-2) + T(2) * kin.v[1] + T(-2) * kin.v[3])) + prod_pow(kin.v[4], 2) + kin.v[2] * (T(3) * kin.v[2] + T(2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + bc<T>[1] * T(4) + T(8)) * kin.v[0] + T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]);
c[3] = rlog(kin.v[0]) * ((T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(kin.v[3]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + bc<T>[1] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[1] * (T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[14] * (abb[15] * (abb[17] * T(-4) + abb[3] * abb[16] * T(-2) + abb[16] * (abb[4] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[16] * T(2))) + abb[3] * abb[16] * (-abb[1] + -abb[7] + -abb[9] + -abb[11] + abb[18] * T(-4) + abb[8] * T(3) + abb[10] * T(3)) + abb[16] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[18] * bc<T>[0] * int_to_imaginary<T>(4) + abb[4] * (-abb[1] + -abb[7] + -abb[9] + -abb[11] + abb[18] * T(-4) + abb[8] * T(3) + abb[10] * T(3)) + abb[16] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[18] * T(4))) + abb[2] * (abb[15] * abb[16] * T(2) + abb[13] * (-abb[1] + -abb[7] + -abb[9] + -abb[11] + abb[18] * T(-4) + abb[15] * T(-2) + abb[8] * T(3) + abb[10] * T(3)) + abb[16] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[18] * T(4))) + abb[17] * (abb[18] * T(-8) + abb[1] * T(-2) + abb[7] * T(-2) + abb[9] * T(-2) + abb[11] * T(-2) + abb[8] * T(6) + abb[10] * T(6)) + abb[13] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3) + abb[18] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[15] * (bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2) + abb[4] * T(2) + abb[16] * T(2)) + abb[3] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[18] * T(4)) + abb[4] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[18] * T(4)) + abb[16] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[18] * T(4)) + abb[13] * (abb[18] * T(-8) + abb[15] * T(-4) + abb[1] * T(-2) + abb[7] * T(-2) + abb[9] * T(-2) + abb[11] * T(-2) + abb[8] * T(6) + abb[10] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_81_construct (const Kin<T>& kin) {
    return [&kin, 
            dl3 = DLog_W_3<T>(kin),dl9 = DLog_W_9<T>(kin),dl23 = DLog_W_23<T>(kin),dl24 = DLog_W_24<T>(kin),dl19 = DLog_W_19<T>(kin),dl28 = DLog_W_28<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),spdl23 = SpDLog_f_4_81_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl3(t), rlog(kin.W[16] / kin_path.W[16]), rlog(v_path[0]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_2(kin_path), f_2_1_9(kin_path), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), dl9(t), rlog(-v_path[3] + v_path[0] + v_path[1]), dl23(t), rlog(kin.W[0] / kin_path.W[0]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), -rlog(t), dl24(t), dl19(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl28(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl2(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl1(t), dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl4(t), dl5(t), dl16(t), dl20(t), dl18(t), dl17(t)}
;

        auto result = f_4_81_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_81_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[121];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[20];
z[14] = abb[28];
z[15] = abb[31];
z[16] = abb[36];
z[17] = abb[38];
z[18] = abb[40];
z[19] = abb[41];
z[20] = abb[13];
z[21] = abb[16];
z[22] = abb[12];
z[23] = abb[39];
z[24] = abb[19];
z[25] = abb[37];
z[26] = abb[17];
z[27] = abb[21];
z[28] = abb[22];
z[29] = abb[29];
z[30] = abb[30];
z[31] = abb[15];
z[32] = abb[18];
z[33] = abb[23];
z[34] = abb[24];
z[35] = abb[25];
z[36] = abb[26];
z[37] = abb[27];
z[38] = abb[33];
z[39] = abb[35];
z[40] = abb[32];
z[41] = abb[34];
z[42] = bc<TR>[1];
z[43] = bc<TR>[3];
z[44] = bc<TR>[5];
z[45] = bc<TR>[2];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = bc<TR>[9];
z[50] = z[10] + z[12];
z[51] = -z[1] + -z[9] + z[50];
z[52] = -z[8] + z[11];
z[53] = z[51] + z[52];
z[54] = z[0] * z[53];
z[55] = -z[34] + -z[35] + z[36] + z[37];
z[56] = z[39] * z[55];
z[57] = z[38] * z[55];
z[58] = z[56] + z[57];
z[59] = z[54] + -z[58];
z[51] = z[51] + -z[52];
z[60] = z[13] * z[51];
z[61] = z[33] * z[55];
z[60] = z[60] + z[61];
z[61] = z[41] * z[55];
z[62] = -z[59] + z[60] + z[61];
z[63] = T(7) * z[45];
z[64] = z[8] / T(2);
z[65] = z[11] / T(2) + z[64];
z[66] = -z[31] + z[65];
z[67] = T(3) * z[10];
z[68] = T(5) * z[9] + -z[67];
z[69] = (T(13) * z[1]) / T(3) + (T(-19) * z[12]) / T(3) + z[68];
z[69] = -z[63] + z[66] + z[69] / T(2);
z[69] = -z[32] + z[69] / T(2);
z[69] = z[16] * z[69];
z[70] = z[1] + -z[12];
z[71] = z[52] + z[70];
z[72] = T(3) * z[24];
z[71] = z[71] * z[72];
z[72] = (T(2) * z[31]) / T(3) + (T(4) * z[32]) / T(3) + (T(-13) * z[45]) / T(2);
z[73] = z[1] + z[10];
z[74] = T(-19) * z[12] + T(11) * z[73];
z[74] = -z[9] + z[74] / T(3);
z[74] = (T(-25) * z[8]) / T(12) + (T(11) * z[11]) / T(4) + -z[72] + z[74] / T(4);
z[74] = z[17] * z[74];
z[75] = T(13) * z[10];
z[76] = T(19) * z[1] + T(-11) * z[12] + z[75];
z[76] = T(-7) * z[9] + z[11] + z[76] / T(3);
z[72] = (T(-11) * z[8]) / T(12) + z[72] + z[76] / T(4);
z[72] = z[19] * z[72];
z[76] = z[63] + (T(11) * z[70]) / T(3);
z[77] = z[18] / T(2);
z[76] = z[76] * z[77];
z[78] = z[52] + (T(7) * z[70]) / T(3);
z[79] = z[15] / T(2);
z[78] = z[78] * z[79];
z[80] = T(3) * z[1];
z[68] = z[12] + z[68] + -z[80];
z[68] = z[68] / T(2);
z[66] = z[66] + z[68];
z[63] = z[63] + -z[66];
z[63] = z[32] + z[63] / T(2);
z[63] = z[23] * z[63];
z[81] = -z[16] + z[23] + -z[25];
z[77] = z[77] + z[81] / T(2);
z[82] = -z[17] + z[19];
z[83] = -z[77] + (T(7) * z[82]) / T(3);
z[83] = z[42] * z[83];
z[84] = -z[9] + -z[12] + z[73];
z[85] = z[22] * z[84];
z[85] = T(3) * z[85];
z[86] = z[25] * z[45];
z[62] = z[62] / T(4) + z[63] + z[69] + -z[71] + z[72] + z[74] + z[76] + z[78] + z[83] / T(2) + -z[85] + (T(-7) * z[86]) / T(2);
z[62] = z[7] * z[62];
z[63] = -z[38] + z[39] + -z[40] + z[41];
z[69] = z[43] * z[63];
z[62] = z[62] + z[69];
z[62] = z[7] * z[62];
z[72] = T(3) * z[9];
z[74] = z[12] + -z[72] + z[73];
z[76] = (T(3) * z[11]) / T(2);
z[78] = z[64] + z[74] / T(2) + -z[76];
z[83] = z[31] + z[78];
z[83] = z[32] + z[83] / T(2);
z[86] = z[17] * z[83];
z[87] = T(7) * z[1];
z[88] = T(-5) * z[50] + z[72] + z[87];
z[89] = (T(5) * z[8]) / T(2) + -z[76];
z[88] = z[31] + z[88] / T(2) + -z[89];
z[88] = z[32] + z[88] / T(2);
z[88] = z[19] * z[88];
z[90] = z[18] * z[83];
z[91] = -z[9] + T(3) * z[12] + -z[73];
z[91] = z[91] / T(2);
z[92] = (T(3) * z[8]) / T(2);
z[93] = (T(5) * z[11]) / T(2) + -z[91] + -z[92];
z[94] = -z[31] + z[93];
z[94] = -z[32] + z[94] / T(2);
z[95] = T(3) * z[25];
z[94] = z[94] * z[95];
z[95] = (T(3) * z[60]) / T(4);
z[55] = z[40] * z[55];
z[96] = -z[55] + z[58];
z[97] = z[31] + T(2) * z[32];
z[98] = -z[8] + z[97];
z[99] = T(2) * z[1] + z[98];
z[100] = T(2) * z[10] + -z[12] + -z[72] + z[99];
z[101] = -(z[16] * z[100]);
z[102] = T(5) * z[1] + -z[72];
z[103] = -z[8] + -z[50] + z[102];
z[103] = (T(-3) * z[11]) / T(4) + z[97] + z[103] / T(4);
z[103] = z[15] * z[103];
z[88] = -z[71] + z[86] + z[88] + z[90] + z[94] + -z[95] + (T(3) * z[96]) / T(4) + z[101] + z[103];
z[88] = z[21] * z[88];
z[78] = -z[78] + -z[97];
z[90] = z[18] * z[78];
z[101] = z[17] * z[78];
z[103] = z[90] + z[101];
z[104] = z[16] * z[78];
z[105] = (T(3) * z[60]) / T(2);
z[99] = -z[50] + z[99];
z[106] = z[19] * z[99];
z[107] = z[55] + -z[56];
z[99] = z[15] * z[99];
z[99] = z[99] + z[103] + z[104] + z[105] + T(2) * z[106] + (T(3) * z[107]) / T(2);
z[99] = z[2] * z[99];
z[65] = z[65] + -z[97];
z[68] = z[65] + z[68];
z[108] = T(3) * z[68];
z[109] = z[23] * z[108];
z[110] = z[15] * z[78];
z[111] = T(2) * z[100];
z[112] = z[16] + z[19];
z[111] = -(z[111] * z[112]);
z[113] = z[61] + -z[107];
z[103] = -z[103] + -z[109] + z[110] + z[111] + (T(3) * z[113]) / T(2);
z[111] = z[3] * z[103];
z[88] = z[88] + z[99] + z[111];
z[88] = z[21] * z[88];
z[99] = T(2) * z[12] + -z[73];
z[111] = T(3) * z[11];
z[114] = T(2) * z[8] + z[97] + z[99] + -z[111];
z[115] = z[17] * z[114];
z[116] = z[16] * z[70];
z[117] = z[15] * z[70];
z[118] = T(2) * z[70];
z[119] = z[18] * z[118];
z[71] = -z[71] + z[106] + -z[115] + -z[116] + z[117] + z[119];
z[71] = z[4] * z[71];
z[98] = z[98] + z[99];
z[99] = -(z[17] * z[98]);
z[100] = z[19] * z[100];
z[106] = z[18] * z[70];
z[115] = z[16] * z[118];
z[99] = -z[85] + z[99] + z[100] + -z[106] + z[115] + z[117];
z[99] = z[3] * z[99];
z[99] = z[71] + z[99];
z[100] = z[21] * z[103];
z[96] = z[60] + -z[96];
z[50] = -z[9] + -z[50] + z[80];
z[50] = z[50] / T(2) + -z[65];
z[50] = z[19] * z[50];
z[80] = z[93] + -z[97];
z[93] = z[17] + z[18];
z[103] = z[80] * z[93];
z[118] = z[51] * z[79];
z[80] = z[25] * z[80];
z[50] = z[50] + -z[80] + z[96] / T(2) + z[103] + -z[118];
z[96] = -(z[28] * z[50]);
z[103] = z[46] * z[82];
z[96] = z[96] + z[103];
z[68] = z[68] * z[112];
z[103] = -z[52] + z[84];
z[118] = z[14] * z[103];
z[58] = z[58] + z[118];
z[61] = z[58] + z[61];
z[65] = -z[65] + z[91];
z[91] = z[17] * z[65];
z[120] = -(z[79] * z[103]);
z[68] = z[61] / T(2) + z[68] + z[91] + z[120];
z[68] = T(3) * z[68] + -z[109];
z[68] = z[30] * z[68];
z[78] = z[19] * z[78];
z[91] = z[78] + z[104];
z[104] = T(2) * z[17];
z[109] = T(2) * z[18] + z[104];
z[109] = z[109] * z[114];
z[80] = T(3) * z[80];
z[109] = (T(3) * z[57]) / T(2) + z[80] + z[91] + z[109] + -z[110];
z[110] = -(z[20] * z[109]);
z[78] = -z[78] + z[101] + z[115] + z[119];
z[58] = (T(-3) * z[58]) / T(2) + z[78] + z[105] + z[117];
z[58] = z[2] * z[58];
z[101] = (T(11) * z[63]) / T(180) + -z[77] + -z[82];
z[101] = prod_pow(z[7], 2) * z[101];
z[105] = T(2) * z[19] + (T(3) * z[63]) / T(20) + -z[104];
z[105] = z[42] * z[105];
z[114] = -(z[45] * z[82]);
z[105] = z[105] + T(3) * z[114];
z[105] = z[42] * z[105];
z[58] = z[58] + z[68] + T(3) * z[96] + T(2) * z[99] + -z[100] + z[101] + z[105] + z[110];
z[58] = z[7] * z[58];
z[63] = z[44] * z[63];
z[68] = -(z[42] * z[69]);
z[63] = (T(-12) * z[63]) / T(5) + z[68];
z[58] = z[58] + T(3) * z[63];
z[58] = int_to_imaginary<T>(1) * z[58];
z[63] = -(z[17] * z[103]);
z[68] = -(z[18] * z[53]);
z[52] = z[52] + z[84];
z[69] = z[16] * z[52];
z[63] = z[54] + z[63] + z[68] + z[69] + z[107] + z[118];
z[63] = z[5] * z[63];
z[68] = z[29] * z[61];
z[69] = -z[5] + -z[6];
z[69] = z[52] * z[69];
z[84] = z[29] * z[103];
z[69] = z[69] + z[84];
z[69] = z[15] * z[69];
z[63] = -z[63] + z[68] + -z[69];
z[68] = T(5) * z[12];
z[69] = -z[68] + z[72] + z[73];
z[73] = T(2) * z[31] + T(4) * z[32];
z[69] = z[64] + z[69] / T(2) + -z[73] + z[76];
z[69] = z[15] * z[69];
z[84] = z[18] + z[112];
z[84] = z[83] * z[84];
z[75] = T(13) * z[1] + T(-23) * z[12] + -z[72] + z[75];
z[96] = (T(9) * z[11]) / T(2);
z[64] = T(-5) * z[31] + z[64] + z[75] / T(2) + z[96];
z[64] = T(-5) * z[32] + z[64] / T(2);
z[64] = z[17] * z[64];
z[75] = -z[57] + z[113];
z[66] = -z[32] + z[66] / T(2);
z[66] = T(3) * z[66];
z[99] = -(z[23] * z[66]);
z[64] = z[64] + z[69] + (T(3) * z[75]) / T(4) + z[84] + -z[85] + -z[94] + z[99];
z[64] = z[20] * z[64];
z[55] = z[55] + z[57];
z[69] = z[55] + z[118];
z[84] = z[15] + z[104];
z[84] = z[84] * z[98];
z[69] = (T(3) * z[69]) / T(2) + z[84] + z[90] + z[91];
z[84] = -z[2] + z[3];
z[69] = z[69] * z[84];
z[64] = z[64] + z[69] + -z[100];
z[64] = z[20] * z[64];
z[69] = z[20] + -z[21];
z[69] = z[69] * z[109];
z[67] = z[67] + -z[72];
z[72] = T(3) * z[8] + -z[70] + -z[111];
z[90] = z[67] + -z[72];
z[79] = z[79] * z[90];
z[59] = (T(-3) * z[59]) / T(2) + -z[78] + z[79];
z[78] = -(z[59] * z[84]);
z[69] = z[69] + -z[71] + z[78];
z[69] = z[4] * z[69];
z[71] = T(5) * z[10] + T(-7) * z[12] + z[102];
z[71] = (T(-7) * z[8]) / T(2) + z[71] / T(2) + z[96] + -z[97];
z[71] = z[71] * z[93];
z[78] = T(-9) * z[9] + T(7) * z[10] + -z[68] + z[87];
z[78] = z[78] / T(2) + -z[89] + z[97];
z[78] = -(z[78] * z[112]);
z[73] = -z[8] + -z[73] + -z[74] + z[111];
z[73] = z[15] * z[73];
z[71] = z[71] + z[73] + (T(3) * z[75]) / T(2) + z[78] + -z[80];
z[71] = z[26] * z[71];
z[73] = z[19] * z[83];
z[73] = z[73] + -z[86] + z[106];
z[68] = z[1] + T(-15) * z[9] + T(9) * z[10] + z[68];
z[68] = T(3) * z[31] + z[68] / T(2) + -z[76] + -z[92];
z[68] = T(3) * z[32] + z[68] / T(2);
z[68] = z[16] * z[68];
z[67] = z[67] + z[72];
z[67] = z[15] * z[67];
z[61] = (T(-3) * z[61]) / T(4) + z[67] / T(4) + z[68] + z[73] + z[85];
z[61] = z[3] * z[61];
z[59] = z[2] * z[59];
z[59] = z[59] + z[61];
z[59] = z[3] * z[59];
z[50] = z[27] * z[50];
z[51] = z[19] * z[51];
z[51] = z[51] + z[54] + -z[55] + -z[60];
z[55] = (T(3) * z[6]) / T(2);
z[51] = z[51] * z[55];
z[60] = z[9] + -z[10];
z[60] = (T(3) * z[60]) / T(2) + -z[70] + -z[76] + z[92];
z[60] = z[15] * z[60];
z[56] = z[56] + z[57] + -z[118];
z[54] = z[54] + -z[56] / T(2);
z[54] = (T(3) * z[54]) / T(2) + z[60] + z[73] + -z[95] + z[116];
z[54] = prod_pow(z[2], 2) * z[54];
z[56] = z[29] * z[108];
z[57] = prod_pow(z[45], 3);
z[57] = T(6) * z[47] + (T(3) * z[48]) / T(2) + T(-2) * z[57];
z[60] = (T(31) * z[49]) / T(4) + -z[57];
z[61] = z[56] + z[60];
z[67] = -(z[26] * z[108]);
z[66] = prod_pow(z[3], 2) * z[66];
z[66] = z[61] + z[66] + z[67];
z[66] = z[23] * z[66];
z[56] = -z[56] + z[57];
z[56] = z[19] * z[56];
z[53] = -(z[53] * z[55]);
z[53] = z[53] + -z[61];
z[53] = z[16] * z[53];
z[61] = -z[18] + -z[81] + z[82];
z[61] = z[46] * z[61];
z[67] = -z[77] + (T(2) * z[82]) / T(3);
z[67] = prod_pow(z[42], 2) * z[67];
z[61] = T(3) * z[61] + z[67];
z[61] = z[42] * z[61];
z[65] = z[29] * z[65];
z[57] = -z[57] + T(-3) * z[65];
z[57] = z[17] * z[57];
z[52] = z[52] * z[55];
z[52] = z[52] + z[60];
z[52] = z[18] * z[52];
z[55] = z[49] * z[82];
z[60] = -(z[25] * z[60]);
return T(-3) * z[50] + z[51] + z[52] + z[53] + z[54] + (T(149) * z[55]) / T(12) + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + (T(-3) * z[63]) / T(2) + z[64] + z[66] + z[69] + z[71] + z[88];
}



template IntegrandConstructorType<double> f_4_81_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_81_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_81_construct (const Kin<qd_real>&);
#endif

}