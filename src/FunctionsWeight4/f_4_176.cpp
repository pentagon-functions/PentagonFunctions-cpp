#include "f_4_176.h"

namespace PentagonFunctions {

template <typename T> T f_4_176_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_176_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_176_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(2) * kin.v[3] + T(4) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[1] / T(4) + -kin.v[4] / T(2) + kin.v[2] / T(2) + kin.v[3] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[3] / T(2) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + (-kin.v[3] / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((-kin.v[1] / T(4) + -kin.v[4] / T(2) + kin.v[2] / T(2) + kin.v[3] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[3] / T(2) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + (-kin.v[3] / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(kin.v[3]) * ((-kin.v[2] / T(2) + -kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[4] / T(2) + T(-1) + kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (-kin.v[4] / T(2) + kin.v[3] / T(4) + T(1)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-kin.v[2] / T(2) + -kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[4] / T(2) + T(-1) + kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (-kin.v[4] / T(2) + kin.v[3] / T(4) + T(1)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4]);
c[1] = rlog(kin.v[3]) * (-kin.v[3] + -kin.v[4] + kin.v[1] + kin.v[2]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-kin.v[3] + -kin.v[4] + kin.v[1] + kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * (abb[3] + -prod_pow(abb[2], 2)) + abb[3] * (abb[7] + -abb[5] + -abb[6]) + prod_pow(abb[1], 2) * (abb[4] + abb[7] + -abb[5] + -abb[6]) + prod_pow(abb[2], 2) * (abb[5] + abb[6] + -abb[7]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_176_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl9 = DLog_W_9<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),spdl22 = SpDLog_f_4_176_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), dl9(t), rlog(v_path[3]), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), rlog(kin.W[8] / kin_path.W[8]), dl16(t), dl17(t), dl4(t), dl20(t)}
;

        auto result = f_4_176_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_176_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[53];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[8];
z[3] = abb[13];
z[4] = abb[14];
z[5] = abb[15];
z[6] = abb[16];
z[7] = abb[5];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[12];
z[11] = abb[2];
z[12] = abb[9];
z[13] = bc<TR>[0];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = prod_pow(z[0], 2);
z[20] = prod_pow(z[12], 2);
z[21] = z[19] + -z[20];
z[22] = T(3) * z[14];
z[23] = int_to_imaginary<T>(1) * z[13];
z[24] = z[16] * z[23];
z[24] = -z[15] + z[24];
z[25] = T(-3) * z[24];
z[26] = -z[12] + z[23];
z[27] = z[0] + z[26];
z[28] = z[11] * z[27];
z[29] = prod_pow(z[13], 2);
z[30] = (T(-3) * z[21]) / T(2) + -z[22] + z[25] + T(3) * z[28] + -z[29] / T(2);
z[31] = -(z[3] * z[30]);
z[32] = T(2) * z[11];
z[32] = z[27] * z[32];
z[33] = T(3) * z[26];
z[34] = z[0] / T(2);
z[35] = z[33] + -z[34];
z[35] = z[0] * z[35];
z[36] = T(2) * z[23];
z[37] = (T(7) * z[12]) / T(2) + -z[36];
z[37] = z[12] * z[37];
z[35] = T(-5) * z[24] + (T(-8) * z[29]) / T(3) + z[32] + z[35] + z[37];
z[35] = z[2] * z[35];
z[21] = -z[14] + -z[21] / T(2) + -z[24] + z[28] + -z[29] / T(6);
z[37] = -(z[4] * z[21]);
z[38] = (T(3) * z[12]) / T(2) + -z[36];
z[38] = z[12] * z[38];
z[38] = -z[24] + z[38];
z[39] = z[26] + z[34];
z[39] = z[0] * z[39];
z[40] = T(-4) * z[14] + T(2) * z[29] + z[32] + -z[38] + T(-3) * z[39];
z[40] = z[6] * z[40];
z[31] = z[31] + z[35] + z[37] + z[40];
z[31] = z[8] * z[31];
z[27] = T(2) * z[27];
z[35] = -z[11] + z[27];
z[35] = z[11] * z[35];
z[37] = z[0] + T(2) * z[26];
z[37] = z[0] * z[37];
z[40] = z[29] / T(3);
z[22] = -z[22] + z[35] + -z[37] + z[40];
z[22] = z[7] * z[22];
z[41] = T(2) * z[14];
z[42] = z[37] + z[41];
z[43] = T(2) * z[16];
z[43] = z[23] * z[43];
z[20] = -z[20] + z[43];
z[44] = (T(2) * z[29]) / T(3);
z[45] = -z[15] + z[44];
z[45] = -z[20] + z[42] + T(-2) * z[45];
z[46] = z[9] * z[45];
z[47] = -z[29] + z[41];
z[48] = T(2) * z[15] + -z[20];
z[37] = z[37] + z[47] + z[48];
z[37] = z[1] * z[37];
z[35] = -z[14] + z[35];
z[49] = -z[15] + z[40];
z[20] = z[20] + -z[35] + T(2) * z[49];
z[20] = z[10] * z[20];
z[49] = z[17] * z[23];
z[50] = (T(3) * z[29]) / T(2) + T(7) * z[49];
z[50] = z[17] * z[50];
z[51] = int_to_imaginary<T>(1) * prod_pow(z[13], 3);
z[52] = (T(21) * z[18]) / T(4) + z[50] + z[51];
z[48] = -z[40] + z[48];
z[35] = z[35] + z[48];
z[35] = z[8] * z[35];
z[20] = T(2) * z[20] + z[22] + z[35] + z[37] + z[46] + z[52] / T(2);
z[20] = z[5] * z[20];
z[22] = z[33] + z[34];
z[22] = z[0] * z[22];
z[33] = (T(5) * z[12]) / T(2) + z[36];
z[33] = z[12] * z[33];
z[35] = (T(4) * z[29]) / T(3);
z[37] = T(4) * z[28];
z[22] = -z[22] + T(7) * z[24] + -z[33] + z[35] + -z[37];
z[22] = z[2] * z[22];
z[19] = -z[19] + z[32] + -z[41] + z[48];
z[19] = z[4] * z[19];
z[33] = z[12] / T(2) + z[36];
z[33] = z[12] * z[33];
z[25] = z[25] + z[33] + z[40];
z[33] = -z[26] + z[34];
z[33] = z[0] * z[33];
z[27] = z[11] + z[27];
z[27] = z[11] * z[27];
z[27] = -z[14] + z[25] + z[27] + -z[33];
z[27] = z[3] * z[27];
z[34] = z[6] * z[45];
z[22] = z[19] + z[22] + z[27] + z[34];
z[27] = z[1] + z[9];
z[22] = z[22] * z[27];
z[27] = -(z[4] * z[30]);
z[21] = z[3] * z[21];
z[25] = z[25] + z[32] + z[39];
z[25] = z[6] * z[25];
z[30] = -z[33] + -z[35] + z[38];
z[30] = z[2] * z[30];
z[21] = z[21] + z[25] + z[27] + z[30];
z[21] = z[7] * z[21];
z[25] = prod_pow(z[11], 2);
z[25] = -z[15] + -z[25] + -z[44];
z[23] = T(4) * z[23];
z[27] = z[12] + -z[23];
z[27] = z[12] * z[27];
z[25] = T(2) * z[25] + z[27] + -z[42] + z[43];
z[25] = z[3] * z[25];
z[23] = -(z[12] * z[23]);
z[23] = z[23] + T(4) * z[24] + -z[37] + z[47];
z[23] = z[6] * z[23];
z[24] = -z[12] + z[36];
z[24] = z[12] * z[24];
z[24] = z[24] + z[29];
z[26] = z[0] + -z[26];
z[26] = z[0] * z[26];
z[24] = T(2) * z[24] + z[26] + z[28];
z[24] = z[2] * z[24];
z[19] = z[19] + z[23] + T(2) * z[24] + z[25];
z[19] = z[10] * z[19];
z[23] = (T(-9) * z[29]) / T(2) + T(-5) * z[49];
z[23] = z[17] * z[23];
z[24] = z[51] / T(6);
z[23] = (T(5) * z[18]) / T(4) + z[23] + z[24];
z[23] = z[4] * z[23];
z[25] = (T(-5) * z[29]) / T(2) + -z[49];
z[25] = z[17] * z[25];
z[24] = z[18] / T(4) + z[24] + z[25];
z[24] = z[3] * z[24];
z[25] = (T(29) * z[18]) / T(4) + z[50] + (T(5) * z[51]) / T(3);
z[25] = z[6] * z[25];
z[23] = z[23] + z[24] + z[25];
z[24] = z[17] * z[29];
z[24] = T(-7) * z[18] + T(4) * z[24] + (T(-7) * z[51]) / T(6);
z[24] = z[2] * z[24];
return z[19] + z[20] + z[21] + z[22] + z[23] / T(2) + z[24] + z[31];
}



template IntegrandConstructorType<double> f_4_176_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_176_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_176_construct (const Kin<qd_real>&);
#endif

}