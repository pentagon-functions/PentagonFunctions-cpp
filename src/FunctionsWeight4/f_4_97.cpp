#include "f_4_97.h"

namespace PentagonFunctions {

template <typename T> T f_4_97_abbreviated (const std::array<T,20>&);

template <typename T> class SpDLog_f_4_97_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_97_W_23 (const Kin<T>& kin) {
        c[0] = ((T(9) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(-3)) * kin.v[4] + kin.v[0] * ((T(9) * kin.v[0]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-3) + T(3) * kin.v[1]) + ((T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[2] + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);
c[1] = kin.v[0] * (T(-3) + T(-3) * kin.v[0] + T(3) * kin.v[1] + T(9) * kin.v[2] + T(6) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(8) * kin.v[2] + T(-8) * kin.v[4])) + kin.v[4] * (T(-3) + T(-6) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[3] * (T(3) + T(-3) * kin.v[3] + T(9) * kin.v[4]) + kin.v[2] * (T(3) + T(-6) * kin.v[2] + T(-9) * kin.v[3] + T(12) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[4] * (T(-8) + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(8) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]) + kin.v[2] * (T(8) + T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(16) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[2] / T(4) + (T(-23) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-11)) * kin.v[2] + ((T(-45) * kin.v[3]) / T(4) + (T(23) * kin.v[4]) / T(2) + T(-11)) * kin.v[3] + kin.v[0] * ((T(23) * kin.v[2]) / T(2) + (T(45) * kin.v[3]) / T(2) + (T(-23) * kin.v[4]) / T(2) + T(11) + (T(-45) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-11) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[1] + T(8) * kin.v[3])) + (-kin.v[4] / T(4) + T(11)) * kin.v[4] + kin.v[1] * (T(11) * kin.v[2] + T(11) * kin.v[3] + T(-11) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(4) * kin.v[4]))) + rlog(-kin.v[4]) * (kin.v[2] * (kin.v[2] / T(2) + -kin.v[4] + T(-14) + T(-13) * kin.v[3]) + (kin.v[4] / T(2) + T(14)) * kin.v[4] + kin.v[1] * (T(14) * kin.v[2] + T(14) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[0] * (T(14) + (T(-27) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-14) * kin.v[1] + T(13) * kin.v[2] + T(27) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[1] + T(8) * kin.v[3]) + T(-13) * kin.v[4]) + kin.v[3] * ((T(-27) * kin.v[3]) / T(2) + T(-14) + T(13) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(4) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((-kin.v[4] / T(2) + T(-14)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(14) + T(13) * kin.v[3] + kin.v[4]) + kin.v[3] * ((T(27) * kin.v[3]) / T(2) + T(14) + T(-13) * kin.v[4]) + kin.v[0] * (T(-14) + (T(27) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(14) * kin.v[1] + T(-13) * kin.v[2] + T(-27) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(-8) * kin.v[3]) + T(13) * kin.v[4]) + kin.v[1] * (T(-14) * kin.v[2] + T(-14) * kin.v[3] + T(14) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((-kin.v[4] + T(-28)) * kin.v[4] + kin.v[3] * (T(28) + T(27) * kin.v[3] + T(-26) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(28) + T(26) * kin.v[3] + T(2) * kin.v[4]) + kin.v[0] * (T(-28) + (bc<T>[0] * int_to_imaginary<T>(8) + T(27)) * kin.v[0] + T(28) * kin.v[1] + T(-26) * kin.v[2] + T(-54) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) + T(16) * kin.v[1] + T(-16) * kin.v[3]) + T(26) * kin.v[4]) + kin.v[1] * (T(-28) * kin.v[2] + T(-28) * kin.v[3] + T(28) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(16) + T(8) * kin.v[3]) + kin.v[4] * (T(-16) + T(-8) * kin.v[4]) + kin.v[2] * (T(16) + T(-8) * kin.v[2] + T(16) * kin.v[4]) + kin.v[1] * (T(-16) * kin.v[2] + T(-16) * kin.v[3] + T(16) * kin.v[4])));
c[2] = T(3) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4];
c[3] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-12) + bc<T>[0] * int_to_imaginary<T>(-16)) * kin.v[0] + T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) * kin.v[2] + T(16) * kin.v[3] + T(-16) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(9)) * kin.v[0] + T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (abb[9] * (abb[3] * T(6) + abb[2] * (bc<T>[0] * int_to_imaginary<T>(-8) + abb[2] * T(7) + abb[7] * T(8))) + abb[2] * (abb[2] * ((abb[4] * T(3)) / T(2) + (abb[8] * T(11)) / T(2) + abb[10] * T(-14) + abb[5] * T(-7)) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(16) + abb[7] * (abb[10] * T(-16) + abb[5] * T(-8) + abb[8] * T(8))) + abb[3] * (abb[10] * T(-12) + abb[5] * T(-6) + abb[4] * T(-3) + abb[8] * T(9)) + abb[6] * (abb[2] * abb[9] * T(-8) + abb[1] * (abb[10] * T(-16) + abb[5] * T(-8) + abb[8] * T(8) + abb[9] * T(8)) + abb[2] * (abb[8] * T(-8) + abb[5] * T(8) + abb[10] * T(16))) + abb[1] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(8) + abb[9] * (abb[7] * T(-8) + abb[2] * T(-6) + bc<T>[0] * int_to_imaginary<T>(8)) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-16) + abb[1] * (abb[5] + (abb[4] * T(-9)) / T(2) + (abb[8] * T(7)) / T(2) + -abb[9] + abb[10] * T(2)) + abb[2] * (abb[8] * T(-9) + abb[4] * T(3) + abb[5] * T(6) + abb[10] * T(12)) + abb[7] * (abb[8] * T(-8) + abb[5] * T(8) + abb[10] * T(16))));
    }
};
template <typename T> class SpDLog_f_4_97_W_10 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_97_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(-3) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(4) + T(3) + T(3) * kin.v[4]);
c[1] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (kin.v[2] / T(2) + T(-14) + T(-14) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(-14) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + kin.v[2] + T(-14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[2] + T(-8) * kin.v[4]))) + rlog(-kin.v[4]) * (kin.v[2] * (-kin.v[2] / T(4) + T(-11) + T(-11) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (-kin.v[2] / T(2) + T(-11) + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(-11) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[2] + T(-8) * kin.v[4]))) + kin.v[2] * (T(3) + T(-6) * kin.v[2] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(8) + T(-8) * kin.v[2] + T(8) * kin.v[4]) + kin.v[1] * (T(3) + (T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-12) * kin.v[2] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-16) * kin.v[2] + T(8) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(14) + T(14) * kin.v[4]) + kin.v[1] * (-kin.v[2] + T(14) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[2] + T(8) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(16) + T(-8) * kin.v[2] + T(16) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(28) + T(28) * kin.v[4]) + kin.v[1] * (T(28) + (T(-1) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-2) * kin.v[2] + T(28) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) + T(-16) * kin.v[2] + T(16) * kin.v[4])));
c[2] = T(-3) * kin.v[1] + T(-3) * kin.v[2];
c[3] = rlog(-kin.v[4]) * ((T(-9) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-9) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-6) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[2]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[2] + T(6) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(16) + T(12)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(16) * kin.v[2] + T(12) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[11] * (abb[12] * (abb[10] * T(-12) + abb[5] * T(-6) + abb[13] * T(-3) + abb[8] * T(6)) + abb[2] * (abb[8] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(16) + abb[2] * ((abb[13] * T(3)) / T(2) + abb[10] * T(-14) + abb[5] * T(-7) + abb[8] * T(7)) + abb[7] * (abb[10] * T(-16) + abb[5] * T(-8) + abb[8] * T(8))) + abb[9] * (abb[1] * abb[2] * T(-8) + abb[2] * ((abb[2] * T(11)) / T(2) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[7] * T(8)) + abb[12] * T(9)) + abb[1] * abb[2] * (abb[8] * T(-8) + abb[5] * T(8) + abb[10] * T(16)) + abb[6] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-16) + abb[6] * (abb[5] + (abb[13] * T(-9)) / T(2) + (abb[9] * T(7)) / T(2) + -abb[8] + abb[10] * T(2)) + abb[9] * (abb[2] * T(-9) + abb[7] * T(-8) + bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * T(8)) + abb[1] * (abb[10] * T(-16) + abb[5] * T(-8) + abb[8] * T(8)) + abb[2] * (abb[8] * T(-6) + abb[13] * T(3) + abb[5] * T(6) + abb[10] * T(12)) + abb[7] * (abb[8] * T(-8) + abb[5] * T(8) + abb[10] * T(16))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_97_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl10 = DLog_W_10<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),spdl23 = SpDLog_f_4_97_W_23<T>(kin),spdl10 = SpDLog_f_4_97_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,20> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), -rlog(t), rlog(kin.W[17] / kin_path.W[17]), rlog(-v_path[4]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[16] / kin_path.W[16]), dl10(t), f_2_1_7(kin_path), -rlog(t), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl17(t), dl16(t), dl5(t)}
;

        auto result = f_4_97_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_97_abbreviated(const std::array<T,20>& abb)
{
using TR = typename T::value_type;
T z[63];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[14];
z[3] = abb[17];
z[4] = abb[18];
z[5] = abb[19];
z[6] = abb[5];
z[7] = abb[8];
z[8] = abb[9];
z[9] = abb[10];
z[10] = abb[13];
z[11] = abb[2];
z[12] = abb[6];
z[13] = abb[7];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[12];
z[17] = abb[15];
z[18] = abb[16];
z[19] = bc<TR>[9];
z[20] = T(2) * z[9];
z[21] = z[6] + z[20];
z[22] = z[7] + -z[8];
z[23] = z[21] + z[22];
z[24] = T(2) * z[1];
z[25] = z[23] + -z[24];
z[26] = T(2) * z[4];
z[27] = z[25] * z[26];
z[28] = T(2) * z[6] + T(4) * z[9];
z[29] = T(2) * z[7];
z[30] = z[28] + z[29];
z[31] = z[8] + z[10] + -z[30];
z[31] = z[5] * z[31];
z[32] = z[2] + -z[5];
z[33] = z[1] * z[32];
z[34] = T(4) * z[33];
z[31] = z[31] + -z[34];
z[35] = T(2) * z[2];
z[23] = z[23] * z[35];
z[36] = T(4) * z[6] + T(8) * z[9];
z[37] = T(3) * z[8];
z[38] = -z[10] + z[36] + -z[37];
z[39] = T(4) * z[7];
z[40] = z[38] + -z[39];
z[41] = z[3] * z[40];
z[23] = z[23] + z[27] + z[31] + z[41];
z[23] = z[0] * z[23];
z[27] = z[6] + z[8];
z[41] = T(2) * z[10];
z[42] = z[27] + -z[41];
z[43] = -z[7] + z[20];
z[44] = z[42] + z[43];
z[45] = z[2] * z[44];
z[44] = z[5] * z[44];
z[45] = z[44] + z[45];
z[46] = z[6] + -z[8];
z[47] = z[20] + z[46];
z[47] = T(4) * z[47];
z[48] = T(3) * z[7];
z[49] = z[1] + z[48];
z[50] = z[47] + -z[49];
z[50] = z[3] * z[50];
z[20] = z[20] + z[42];
z[42] = T(2) * z[20];
z[51] = z[1] + z[7];
z[52] = z[42] + -z[51];
z[52] = z[4] * z[52];
z[45] = T(2) * z[45] + z[50] + -z[52];
z[45] = z[12] * z[45];
z[23] = z[23] + z[45];
z[45] = z[1] + z[10];
z[50] = z[7] + z[8];
z[53] = -z[28] + z[45] + z[50];
z[53] = z[3] * z[53];
z[54] = -z[38] + z[48];
z[54] = z[2] * z[54];
z[55] = z[5] * z[7];
z[56] = -z[8] + z[10];
z[57] = -(z[4] * z[56]);
z[53] = z[33] + T(4) * z[53] + z[54] + z[55] + z[57];
z[53] = z[11] * z[53];
z[22] = z[10] + z[22];
z[54] = -z[1] + z[22];
z[54] = z[4] * z[54];
z[55] = -z[7] + z[56];
z[57] = z[1] + z[55];
z[57] = z[3] * z[57];
z[55] = z[2] * z[55];
z[22] = z[5] * z[22];
z[22] = z[22] + z[33] + -z[54] + z[55] + -z[57];
z[54] = z[18] * z[22];
z[25] = z[4] * z[25];
z[55] = -z[10] + z[21];
z[57] = -z[1] + z[55];
z[58] = T(2) * z[3];
z[59] = z[57] * z[58];
z[43] = z[43] + z[46];
z[46] = z[2] * z[43];
z[25] = -z[25] + -z[44] + z[46] + z[59];
z[44] = T(2) * z[13];
z[46] = -(z[25] * z[44]);
z[46] = z[23] + z[46] + z[53] + T(3) * z[54];
z[46] = int_to_imaginary<T>(1) * z[46];
z[53] = (T(19) * z[7]) / T(6);
z[27] = z[9] + -z[10] + z[27] / T(2);
z[27] = T(3) * z[27] + -z[53];
z[27] = z[5] * z[27];
z[54] = T(3) * z[6];
z[59] = (T(19) * z[8]) / T(3) + -z[54];
z[59] = T(-3) * z[9] + (T(-5) * z[10]) / T(3) + z[59] / T(2);
z[53] = z[53] + z[59];
z[53] = z[2] * z[53];
z[60] = T(3) * z[1];
z[59] = (T(3) * z[7]) / T(2) + -z[59] + -z[60];
z[59] = z[4] * z[59];
z[57] = z[3] * z[57];
z[61] = -z[4] + z[32];
z[62] = z[3] + z[61] / T(2);
z[62] = int_to_imaginary<T>(1) * z[14] * z[62];
z[27] = z[27] + (T(-5) * z[33]) / T(3) + z[53] + T(-3) * z[57] + z[59] + z[62] / T(3);
z[27] = z[14] * z[27];
z[27] = z[27] + T(2) * z[46];
z[27] = z[14] * z[27];
z[46] = z[48] + T(4) * z[56] + -z[60];
z[46] = z[4] * z[46];
z[53] = -z[39] + T(-3) * z[56];
z[53] = z[5] * z[53];
z[56] = T(8) * z[6] + T(16) * z[9];
z[57] = T(7) * z[50] + -z[56];
z[59] = -z[45] + -z[57];
z[59] = z[3] * z[59];
z[60] = z[2] * z[55];
z[34] = -z[34] + z[46] + z[53] + z[59] + T(4) * z[60];
z[34] = z[11] * z[34];
z[25] = z[13] * z[25];
z[23] = -z[23] + z[25] + z[34];
z[23] = z[23] * z[44];
z[25] = z[21] + -z[37] + z[41] + -z[48];
z[25] = z[2] * z[25];
z[34] = z[7] + z[20];
z[41] = z[5] * z[34];
z[43] = z[43] * z[58];
z[34] = -z[24] + z[34];
z[34] = z[4] * z[34];
z[25] = z[25] + T(2) * z[33] + z[34] + z[41] + z[43];
z[25] = z[0] * z[25];
z[29] = T(-3) * z[20] + -z[29];
z[29] = z[5] * z[29];
z[34] = z[42] + z[48];
z[34] = z[2] * z[34];
z[41] = (T(-7) * z[8]) / T(2) + -z[10] / T(2) + z[36] + -z[49];
z[41] = z[3] * z[41];
z[42] = T(5) * z[33];
z[29] = z[29] + z[34] + z[41] + -z[42] + -z[52];
z[29] = z[12] * z[29];
z[34] = z[5] * z[20];
z[41] = -z[47] + z[48];
z[41] = z[2] * z[41];
z[34] = z[33] + z[34] + z[41];
z[41] = T(16) * z[6] + T(32) * z[9];
z[43] = T(9) * z[8] + T(7) * z[10] + -z[41];
z[44] = z[43] + T(8) * z[51];
z[44] = z[3] * z[44];
z[34] = T(2) * z[34] + z[44];
z[34] = z[11] * z[34];
z[25] = T(4) * z[25] + z[29] + z[34];
z[25] = z[12] * z[25];
z[29] = z[1] + -z[9];
z[34] = T(5) * z[10];
z[29] = T(-2) * z[8] + T(6) * z[29] + z[34] + -z[48] + -z[54];
z[29] = z[4] * z[29];
z[30] = z[30] + -z[34] + z[37];
z[30] = z[2] * z[30];
z[38] = -z[1] / T(2) + (T(-7) * z[7]) / T(2) + z[38];
z[38] = z[3] * z[38];
z[29] = z[29] + z[30] + z[31] + z[38];
z[29] = prod_pow(z[0], 2) * z[29];
z[30] = z[41] + (T(-27) * z[45]) / T(2) + (T(-5) * z[50]) / T(2);
z[30] = z[3] * z[30];
z[31] = T(5) * z[1];
z[38] = z[31] + -z[39] + -z[55];
z[38] = z[4] * z[38];
z[41] = T(4) * z[8] + -z[34];
z[44] = -z[21] + -z[41];
z[44] = z[5] * z[44];
z[45] = -z[10] + -z[57];
z[45] = z[2] * z[45];
z[30] = z[30] + -z[33] + z[38] + z[44] + z[45];
z[30] = z[11] * z[30];
z[35] = -(z[35] * z[40]);
z[38] = T(7) * z[1] + T(9) * z[7];
z[28] = z[8] + z[10] + -z[28];
z[28] = T(8) * z[28] + z[38];
z[28] = z[3] * z[28];
z[21] = -z[7] + -z[21] + z[24];
z[24] = -z[10] + -z[21];
z[24] = z[24] * z[26];
z[24] = z[24] + z[28] + z[35];
z[24] = z[0] * z[24];
z[24] = z[24] + z[30];
z[24] = z[11] * z[24];
z[22] = z[17] * z[22];
z[28] = z[8] + -z[34] + z[36] + z[39];
z[28] = z[28] * z[32];
z[28] = z[28] + T(-8) * z[33];
z[30] = -z[34] + -z[37] + z[56];
z[30] = T(2) * z[30] + -z[38];
z[30] = z[3] * z[30];
z[21] = z[21] + -z[41];
z[21] = z[21] * z[26];
z[21] = z[21] + T(2) * z[28] + z[30];
z[21] = z[15] * z[21];
z[28] = -z[20] + -z[39];
z[28] = z[5] * z[28];
z[20] = z[7] + T(4) * z[20];
z[30] = z[2] * z[20];
z[28] = z[28] + z[30] + -z[42];
z[20] = -z[20] + z[31];
z[20] = z[20] * z[26];
z[26] = T(-10) * z[1] + T(-6) * z[7] + -z[43];
z[26] = z[3] * z[26];
z[20] = z[20] + z[26] + T(2) * z[28];
z[20] = z[16] * z[20];
z[26] = -z[58] + -z[61];
z[26] = z[19] * z[26];
return z[20] + z[21] + T(6) * z[22] + z[23] + z[24] + z[25] + (T(32) * z[26]) / T(3) + z[27] + z[29];
}



template IntegrandConstructorType<double> f_4_97_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_97_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_97_construct (const Kin<qd_real>&);
#endif

}