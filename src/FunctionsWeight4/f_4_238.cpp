#include "f_4_238.h"

namespace PentagonFunctions {

template <typename T> T f_4_238_abbreviated (const std::array<T,18>&);



template <typename T> IntegrandConstructorType<T> f_4_238_construct (const Kin<T>& kin) {
    return [&kin, 
            dl24 = DLog_W_24<T>(kin),dl6 = DLog_W_6<T>(kin),dl18 = DLog_W_18<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin),dl25 = DLog_W_25<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,18> abbr = 
            {dl24(t), rlog(kin.W[16] / kin_path.W[16]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[17] / kin_path.W[17]), dl6(t), rlog(v_path[0]), f_2_1_9(kin_path), dl18(t), rlog(kin.W[24] / kin_path.W[24]), dl1(t), dl19(t), dl17(t), dl25(t)}
;

        auto result = f_4_238_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_238_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[53];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[12];
z[11] = abb[14];
z[12] = abb[15];
z[13] = abb[16];
z[14] = abb[10];
z[15] = abb[9];
z[16] = abb[11];
z[17] = abb[13];
z[18] = bc<TR>[1];
z[19] = bc<TR>[2];
z[20] = bc<TR>[4];
z[21] = bc<TR>[7];
z[22] = bc<TR>[8];
z[23] = bc<TR>[9];
z[24] = abb[17];
z[25] = z[1] + -z[8];
z[26] = z[7] + -z[9];
z[27] = z[25] + -z[26];
z[27] = z[15] * z[27];
z[28] = T(2) * z[27];
z[29] = T(2) * z[17];
z[30] = z[1] + z[8] + -z[29];
z[31] = z[7] + z[30];
z[32] = T(2) * z[13];
z[33] = z[31] * z[32];
z[34] = z[25] + z[26];
z[34] = z[0] * z[34];
z[25] = z[10] * z[25];
z[35] = T(2) * z[30];
z[36] = -z[7] + T(3) * z[9] + z[35];
z[37] = -(z[11] * z[36]);
z[37] = z[25] + z[28] + z[33] + -z[34] + z[37];
z[37] = z[3] * z[37];
z[38] = z[1] + T(3) * z[8];
z[39] = -z[7] + -z[9] + T(4) * z[17];
z[40] = -z[38] + z[39];
z[40] = z[10] * z[40];
z[41] = z[11] + -z[12];
z[42] = z[36] * z[41];
z[33] = -z[33] + -z[34] + -z[40] + z[42];
z[40] = z[6] * z[33];
z[42] = T(2) * z[11];
z[43] = z[26] * z[42];
z[44] = z[12] * z[36];
z[43] = z[43] + z[44];
z[44] = z[7] + z[9];
z[45] = -z[8] + z[17];
z[45] = -z[44] + T(4) * z[45];
z[45] = z[10] * z[45];
z[28] = -z[28] + z[43] + z[45];
z[28] = z[14] * z[28];
z[45] = T(4) * z[24];
z[46] = T(3) * z[10] + -z[12] + z[32] + z[42] + -z[45];
z[46] = z[18] * z[46];
z[41] = z[10] + -z[13] + z[41];
z[41] = z[19] * z[41];
z[41] = z[41] / T(2) + z[46];
z[41] = z[19] * z[41];
z[46] = T(2) * z[24];
z[47] = -z[32] + z[46];
z[48] = (T(-7) * z[10]) / T(2) + (T(5) * z[12]) / T(2) + z[47];
z[49] = prod_pow(z[18], 2);
z[48] = z[48] * z[49];
z[50] = prod_pow(z[4], 2);
z[51] = z[10] + z[11];
z[51] = -z[12] / T(3) + z[13] / T(6) + -z[24] + (T(5) * z[51]) / T(6);
z[51] = z[50] * z[51];
z[52] = T(-5) * z[10] + T(3) * z[12] + T(-4) * z[13] + z[45];
z[52] = z[20] * z[52];
z[28] = z[28] + z[37] + z[40] + z[41] + z[48] + z[51] + z[52];
z[28] = int_to_imaginary<T>(1) * z[4] * z[28];
z[37] = z[7] / T(6);
z[40] = z[9] / T(2);
z[41] = z[30] / T(3) + -z[37] + z[40];
z[41] = z[12] * z[41];
z[44] = z[35] + z[44];
z[48] = z[13] * z[44];
z[37] = z[1] + (T(-5) * z[8]) / T(3) + -z[9] / T(6) + (T(2) * z[17]) / T(3) + -z[37];
z[37] = z[10] * z[37];
z[35] = T(-5) * z[9] + -z[35];
z[35] = z[7] + z[35] / T(3);
z[35] = z[11] * z[35];
z[51] = z[11] / T(2);
z[52] = (T(-17) * z[10]) / T(12) + (T(11) * z[12]) / T(12) + -z[13] / T(2) + z[24] + -z[51];
z[52] = z[18] * z[52];
z[35] = (T(-2) * z[34]) / T(3) + z[35] + z[37] + z[41] + z[48] / T(3) + z[52];
z[35] = z[35] * z[50];
z[37] = z[7] / T(2);
z[41] = (T(3) * z[9]) / T(2) + z[30] + -z[37];
z[41] = z[12] * z[41];
z[37] = z[37] + z[40];
z[29] = -z[29] + z[37];
z[38] = z[29] + z[38] / T(2);
z[38] = z[10] * z[38];
z[37] = z[30] + z[37];
z[40] = z[13] * z[37];
z[52] = -(z[11] * z[31]);
z[38] = (T(3) * z[27]) / T(2) + z[38] + z[40] + -z[41] + z[52];
z[38] = z[14] * z[38];
z[40] = -(z[10] * z[44]);
z[52] = z[26] * z[32];
z[40] = z[40] + z[43] + z[52];
z[40] = z[2] * z[40];
z[30] = z[9] + z[30];
z[43] = z[30] * z[42];
z[43] = -z[25] + -z[27] + z[43] + -z[48];
z[43] = z[3] * z[43];
z[38] = z[38] + z[40] + z[43];
z[38] = z[14] * z[38];
z[40] = -z[12] + z[13];
z[36] = z[36] * z[40];
z[40] = T(3) * z[1] + z[8];
z[39] = -z[39] + z[40];
z[39] = z[10] * z[39];
z[42] = -(z[31] * z[42]);
z[36] = z[27] + z[36] + z[39] + z[42];
z[36] = z[16] * z[36];
z[30] = z[30] * z[32];
z[32] = -(z[11] * z[44]);
z[25] = z[25] + z[30] + z[32] + z[34];
z[25] = z[2] * z[25];
z[30] = -z[11] + -z[13];
z[26] = z[26] * z[30];
z[26] = z[26] + -z[27] + z[34];
z[26] = z[3] * z[26];
z[25] = z[25] + z[26] / T(2);
z[25] = z[3] * z[25];
z[26] = z[5] * z[33];
z[27] = z[29] + z[40] / T(2);
z[27] = z[10] * z[27];
z[29] = -(z[13] * z[31]);
z[27] = z[27] + z[29] + (T(-3) * z[34]) / T(2) + -z[41];
z[29] = prod_pow(z[2], 2);
z[27] = z[27] * z[29];
z[30] = z[10] + -z[47];
z[30] = z[20] * z[30];
z[31] = z[10] + -z[24];
z[31] = -z[11] + -z[12] + T(2) * z[31];
z[31] = z[13] + z[31] / T(3);
z[31] = z[31] * z[49];
z[30] = T(2) * z[30] + z[31];
z[30] = z[18] * z[30];
z[31] = z[10] + z[24];
z[31] = -z[12] + (T(-7) * z[13]) / T(6) + (T(2) * z[31]) / T(3) + z[51];
z[31] = z[31] * z[50];
z[32] = z[11] + z[12];
z[33] = -z[10] + T(8) * z[24];
z[33] = (T(-5) * z[13]) / T(3) + -z[32] + z[33] / T(3);
z[33] = prod_pow(z[19], 2) * z[33];
z[31] = z[31] + z[33];
z[31] = z[19] * z[31];
z[29] = z[29] * z[37];
z[29] = (T(-7) * z[23]) / T(3) + z[29];
z[29] = z[11] * z[29];
z[33] = z[10] + T(3) * z[13] + z[32] + -z[45];
z[33] = z[21] * z[33];
z[32] = z[10] / T(4) + (T(5) * z[13]) / T(4) + (T(3) * z[32]) / T(4) + -z[46];
z[32] = z[22] * z[32];
z[34] = (T(-17) * z[10]) / T(4) + (T(-19) * z[12]) / T(12) + (T(-28) * z[13]) / T(3) + (T(35) * z[24]) / T(3);
z[34] = z[23] * z[34];
return z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[31] + z[32] + T(2) * z[33] + z[34] + z[35] + z[36] + z[38];
}



template IntegrandConstructorType<double> f_4_238_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_238_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_238_construct (const Kin<qd_real>&);
#endif

}