#include "f_4_333.h"

namespace PentagonFunctions {

template <typename T> T f_4_333_abbreviated (const std::array<T,55>&);

template <typename T> class SpDLog_f_4_333_W_7 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_333_W_7 (const Kin<T>& kin) {
        c[0] = T(-3) * kin.v[1] * kin.v[4] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]);
c[1] = T(-3) * kin.v[1] * kin.v[4] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]);
c[2] = T(-3) * kin.v[1] * kin.v[4] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[1], 2) * ((abb[3] * T(-3)) / T(2) + (abb[4] * T(-3)) / T(2) + (abb[5] * T(-3)) / T(2)) + (prod_pow(abb[2], 2) * abb[3] * T(3)) / T(2) + prod_pow(abb[2], 2) * ((abb[4] * T(3)) / T(2) + (abb[5] * T(3)) / T(2)));
    }
};
template <typename T> class SpDLog_f_4_333_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_333_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3]) + ((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);
c[1] = kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3]) + ((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);
c[2] = ((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4];
c[3] = ((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[9] * c[0] + abb[3] * c[1] + abb[5] * c[2] + abb[10] * c[3]);
        }

        return abb[6] * ((prod_pow(abb[8], 2) * abb[9] * T(-3)) / T(2) + prod_pow(abb[7], 2) * ((abb[5] * T(-3)) / T(2) + (abb[10] * T(-3)) / T(2) + (abb[3] * T(3)) / T(2) + (abb[9] * T(3)) / T(2)) + prod_pow(abb[8], 2) * ((abb[3] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2) + (abb[10] * T(3)) / T(2)));
    }
};
template <typename T> class SpDLog_f_4_333_W_10 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_333_W_10 (const Kin<T>& kin) {
        c[0] = bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) * kin.v[2] + kin.v[2] * ((T(-15) * kin.v[2]) / T(2) + kin.v[3] / T(2) + T(7) + T(7) * kin.v[4]) + kin.v[1] * ((T(-29) * kin.v[2]) / T(2) + kin.v[3] / T(2) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(7) + (T(-7) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4))) * kin.v[1] + T(7) * kin.v[4]);
c[1] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[1] + T(4) * kin.v[2] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) * kin.v[2];
c[2] = bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) * kin.v[2] + kin.v[2] * ((T(-15) * kin.v[2]) / T(2) + kin.v[3] / T(2) + T(7) + T(7) * kin.v[4]) + kin.v[1] * ((T(-29) * kin.v[2]) / T(2) + kin.v[3] / T(2) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(7) + (T(-7) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4))) * kin.v[1] + T(7) * kin.v[4]);
c[3] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[1] + T(4) * kin.v[2] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) * kin.v[2];
c[4] = bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) * kin.v[2] + kin.v[2] * ((T(-15) * kin.v[2]) / T(2) + kin.v[3] / T(2) + T(7) + T(7) * kin.v[4]) + kin.v[1] * ((T(-29) * kin.v[2]) / T(2) + kin.v[3] / T(2) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(7) + (T(-7) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4))) * kin.v[1] + T(7) * kin.v[4]);
c[5] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[1] + T(4) * kin.v[2] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[9] * (t * c[0] + c[1]) + abb[3] * (t * c[2] + c[3]) + abb[10] * (t * c[4] + c[5]);
        }

        return abb[11] * (abb[7] * abb[8] * (-abb[3] / T(2) + -abb[10] / T(2)) + abb[8] * abb[13] * ((abb[3] * T(-9)) / T(2) + (abb[10] * T(-9)) / T(2)) + abb[15] * (abb[3] * T(-4) + abb[10] * T(-4)) + abb[12] * ((abb[3] + abb[9] + abb[10]) * abb[12] + abb[7] * (abb[3] / T(2) + abb[10] / T(2)) + abb[8] * (abb[3] / T(2) + abb[10] / T(2)) + abb[14] * (-abb[3] / T(2) + -abb[10] / T(2)) + abb[13] * ((abb[3] * T(9)) / T(2) + (abb[10] * T(9)) / T(2)) + abb[3] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[10] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[9] * (abb[7] / T(2) + abb[8] / T(2) + -abb[14] / T(2) + (abb[13] * T(9)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)))) + abb[8] * (abb[14] * (abb[3] / T(2) + abb[10] / T(2)) + abb[8] * ((abb[3] * T(-3)) / T(2) + (abb[10] * T(-3)) / T(2)) + abb[3] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[10] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2))) + abb[9] * (-(abb[7] * abb[8]) / T(2) + (abb[8] * abb[13] * T(-9)) / T(2) + abb[15] * T(-4) + abb[8] * (abb[14] / T(2) + (abb[8] * T(-3)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)))) + abb[1] * (abb[12] * (abb[3] * T(-4) + abb[9] * T(-4) + abb[10] * T(-4)) + abb[8] * abb[9] * T(4) + abb[8] * (abb[3] * T(4) + abb[10] * T(4))));
    }
};
template <typename T> class SpDLog_f_4_333_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_333_W_22 (const Kin<T>& kin) {
        c[0] = ((T(11) * kin.v[2]) / T(2) + (T(-11) * kin.v[3]) / T(2) + (T(-11) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[1] * (T(-13) / T(2) + (T(11) * kin.v[0]) / T(2) + (T(25) * kin.v[2]) / T(4) + (T(-39) * kin.v[3]) / T(4) + (T(-17) * kin.v[4]) / T(4) + (T(43) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3])) + (T(-13) / T(2) + (T(7) * kin.v[2]) / T(8) + (T(-21) * kin.v[3]) / T(4) + kin.v[4] / T(4)) * kin.v[2] + (T(13) / T(2) + (T(-9) * kin.v[4]) / T(8)) * kin.v[4] + (T(13) / T(2) + (T(35) * kin.v[3]) / T(8) + (T(13) * kin.v[4]) / T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]));
c[1] = ((T(11) * kin.v[2]) / T(2) + (T(-11) * kin.v[3]) / T(2) + (T(-11) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[1] * (T(-13) / T(2) + (T(11) * kin.v[0]) / T(2) + (T(25) * kin.v[2]) / T(4) + (T(-39) * kin.v[3]) / T(4) + (T(-17) * kin.v[4]) / T(4) + (T(43) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3])) + (T(-13) / T(2) + (T(7) * kin.v[2]) / T(8) + (T(-21) * kin.v[3]) / T(4) + kin.v[4] / T(4)) * kin.v[2] + (T(13) / T(2) + (T(-9) * kin.v[4]) / T(8)) * kin.v[4] + (T(13) / T(2) + (T(35) * kin.v[3]) / T(8) + (T(13) * kin.v[4]) / T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]));
c[2] = (T(-13) / T(2) + (T(-35) * kin.v[3]) / T(8) + (T(-13) * kin.v[4]) / T(4)) * kin.v[3] + (-kin.v[4] / T(4) + T(13) / T(2) + (T(-7) * kin.v[2]) / T(8) + (T(21) * kin.v[3]) / T(4)) * kin.v[2] + (T(-13) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4] + kin.v[1] * (T(13) / T(2) + (T(-11) * kin.v[0]) / T(2) + (T(-25) * kin.v[2]) / T(4) + (T(39) * kin.v[3]) / T(4) + (T(17) * kin.v[4]) / T(4) + (T(-43) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + ((T(-11) * kin.v[2]) / T(2) + (T(11) * kin.v[3]) / T(2) + (T(11) * kin.v[4]) / T(2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]));
c[3] = (T(-13) / T(2) + (T(-35) * kin.v[3]) / T(8) + (T(-13) * kin.v[4]) / T(4)) * kin.v[3] + (-kin.v[4] / T(4) + T(13) / T(2) + (T(-7) * kin.v[2]) / T(8) + (T(21) * kin.v[3]) / T(4)) * kin.v[2] + (T(-13) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4] + kin.v[1] * (T(13) / T(2) + (T(-11) * kin.v[0]) / T(2) + (T(-25) * kin.v[2]) / T(4) + (T(39) * kin.v[3]) / T(4) + (T(17) * kin.v[4]) / T(4) + (T(-43) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + ((T(-11) * kin.v[2]) / T(2) + (T(11) * kin.v[3]) / T(2) + (T(11) * kin.v[4]) / T(2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]));
c[4] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + (T(-7) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + (T(7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);
c[5] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + (T(-7) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + (T(7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);
c[6] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[1] + (T(7) * kin.v[2]) / T(2) + (T(-7) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + (T(-7) * kin.v[4]) / T(2);
c[7] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[1] + (T(7) * kin.v[2]) / T(2) + (T(-7) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + (T(-7) * kin.v[4]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[5] * c[1] + abb[9] * c[2] + abb[10] * c[3]) + abb[4] * c[4] + abb[5] * c[5] + abb[9] * c[6] + abb[10] * c[7];
        }

        return abb[16] * (abb[17] * ((abb[4] * T(-7)) / T(2) + (abb[5] * T(-7)) / T(2) + (abb[10] * T(7)) / T(2)) + abb[8] * abb[14] * (abb[10] + -abb[4] + -abb[5]) + abb[12] * abb[14] * (abb[4] + abb[5] + -abb[10]) + abb[9] * (abb[8] * abb[14] + (abb[17] * T(7)) / T(2) + -(abb[12] * abb[14]) + abb[2] * abb[14] * T(-2) + abb[14] * ((abb[14] * T(9)) / T(4) + bc<T>[0] * int_to_imaginary<T>(-3))) + abb[14] * (abb[14] * ((abb[4] * T(-9)) / T(4) + (abb[5] * T(-9)) / T(4) + (abb[10] * T(9)) / T(4)) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3) + abb[5] * bc<T>[0] * int_to_imaginary<T>(3)) + abb[2] * abb[14] * (abb[10] * T(-2) + abb[4] * T(2) + abb[5] * T(2)) + abb[7] * (abb[7] * (abb[9] / T(4) + abb[10] / T(4) + -abb[4] / T(4) + -abb[5] / T(4)) + abb[14] * ((abb[10] * T(-5)) / T(2) + (abb[4] * T(5)) / T(2) + (abb[5] * T(5)) / T(2)) + abb[12] * (abb[10] + -abb[4] + -abb[5]) + abb[8] * (abb[4] + abb[5] + -abb[10]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3) + abb[9] * (abb[12] + (abb[14] * T(-5)) / T(2) + -abb[8] + bc<T>[0] * int_to_imaginary<T>(3) + abb[2] * T(2)) + abb[2] * (abb[4] * T(-2) + abb[5] * T(-2) + abb[10] * T(2))) + abb[13] * (abb[9] * abb[14] * T(3) + abb[7] * (abb[9] * T(-3) + abb[10] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + abb[14] * (abb[4] * T(-3) + abb[5] * T(-3) + abb[10] * T(3))));
    }
};
template <typename T> class SpDLog_f_4_333_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_333_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (-kin.v[3] / T(2) + kin.v[0] / T(2) + -kin.v[2] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(7) + (bc<T>[0] * (int_to_imaginary<T>(9) / T(4)) + T(7)) * kin.v[1] + T(-7) * kin.v[4]) + T(-7) * kin.v[4] + -(kin.v[0] * kin.v[4]) / T(2) + kin.v[2] * kin.v[4] + (kin.v[3] * kin.v[4]) / T(2) + bc<T>[0] * (T(-9) / T(2) + (T(-9) * kin.v[4]) / T(4)) * int_to_imaginary<T>(1) * kin.v[4];
c[1] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) * kin.v[4];
c[2] = T(7) * kin.v[4] + (kin.v[0] * kin.v[4]) / T(2) + -(kin.v[2] * kin.v[4]) + -(kin.v[3] * kin.v[4]) / T(2) + bc<T>[0] * (T(9) / T(2) + (T(9) * kin.v[4]) / T(4)) * int_to_imaginary<T>(1) * kin.v[4] + kin.v[1] * (-kin.v[0] / T(2) + kin.v[3] / T(2) + T(-7) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + (T(-7) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4))) * kin.v[1] + kin.v[2] + T(7) * kin.v[4]);
c[3] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[1] + T(4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[4] * (t * c[0] + c[1]) + abb[9] * (t * c[2] + c[3]);
        }

        return abb[38] * ((abb[4] * abb[12] * abb[13] * T(-9)) / T(2) + abb[4] * abb[39] * T(-4) + abb[12] * ((abb[4] * abb[14]) / T(2) + -(abb[2] * abb[4]) / T(2) + abb[4] * abb[12] * T(-3) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[4] * abb[8] * T(4)) + abb[9] * ((abb[12] * abb[13] * T(9)) / T(2) + abb[12] * (abb[2] / T(2) + -abb[14] / T(2) + abb[8] * T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[12] * T(3)) + abb[39] * T(4)) + abb[1] * ((abb[2] * abb[4]) / T(2) + abb[1] * (abb[9] / T(2) + -abb[4] / T(2)) + -(abb[4] * abb[14]) / T(2) + (abb[4] * abb[12] * T(7)) / T(2) + (abb[4] * abb[13] * T(9)) / T(2) + abb[4] * abb[8] * T(-4) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[9] * (abb[14] / T(2) + -abb[2] / T(2) + (abb[13] * T(-9)) / T(2) + (abb[12] * T(-7)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[8] * T(4))));
    }
};
template <typename T> class SpDLog_f_4_333_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_333_W_21 (const Kin<T>& kin) {
        c[0] = (T(13) / T(2) + (T(-35) * kin.v[3]) / T(8) + (T(-13) * kin.v[4]) / T(2)) * kin.v[3] + (T(13) / T(2) + kin.v[2] / T(8) + (T(-17) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2)) * kin.v[2] + (T(-13) / T(2) + (T(-43) * kin.v[1]) / T(8) + (T(21) * kin.v[2]) / T(4) + (T(39) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(-13) / T(2) + (T(-17) * kin.v[1]) / T(4) + (T(-5) * kin.v[2]) / T(4) + (T(13) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2) + (T(9) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(-3) * kin.v[2] + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[3] + T(3) * kin.v[4]));
c[1] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + (T(-7) * kin.v[1]) / T(2) + (T(7) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3]);
c[2] = (T(13) / T(2) + (T(-35) * kin.v[3]) / T(8) + (T(-13) * kin.v[4]) / T(2)) * kin.v[3] + (T(13) / T(2) + kin.v[2] / T(8) + (T(-17) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2)) * kin.v[2] + (T(-13) / T(2) + (T(-43) * kin.v[1]) / T(8) + (T(21) * kin.v[2]) / T(4) + (T(39) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(-13) / T(2) + (T(-17) * kin.v[1]) / T(4) + (T(-5) * kin.v[2]) / T(4) + (T(13) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2) + (T(9) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(-3) * kin.v[2] + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[3] + T(3) * kin.v[4]));
c[3] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + (T(-7) * kin.v[1]) / T(2) + (T(7) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3]);
c[4] = (T(13) / T(2) + (T(43) * kin.v[1]) / T(8) + (T(-21) * kin.v[2]) / T(4) + (T(-39) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(13) / T(2) + (T(17) * kin.v[1]) / T(4) + (T(5) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2) + (T(-9) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4])) + (-kin.v[2] / T(8) + T(-13) / T(2) + (T(17) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2)) * kin.v[2] + (T(-13) / T(2) + (T(35) * kin.v[3]) / T(8) + (T(13) * kin.v[4]) / T(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]));
c[5] = (T(13) / T(2) + (T(43) * kin.v[1]) / T(8) + (T(-21) * kin.v[2]) / T(4) + (T(-39) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(13) / T(2) + (T(17) * kin.v[1]) / T(4) + (T(5) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2) + (T(-9) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4])) + (-kin.v[2] / T(8) + T(-13) / T(2) + (T(17) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2)) * kin.v[2] + (T(-13) / T(2) + (T(35) * kin.v[3]) / T(8) + (T(13) * kin.v[4]) / T(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]));
c[6] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[0] + (T(7) * kin.v[1]) / T(2) + (T(-7) * kin.v[2]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3]) + (T(-7) * kin.v[3]) / T(2);
c[7] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[0] + (T(7) * kin.v[1]) / T(2) + (T(-7) * kin.v[2]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3]) + (T(-7) * kin.v[3]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[5] * (t * c[0] + c[1]) + abb[10] * (t * c[2] + c[3]) + t * (abb[3] * c[4] + abb[4] * c[5]) + abb[3] * c[6] + abb[4] * c[7];
        }

        return abb[40] * (abb[23] * ((abb[4] * T(-7)) / T(2) + (abb[5] * T(7)) / T(2) + (abb[10] * T(7)) / T(2)) + abb[12] * abb[14] * (abb[4] + -abb[5] + -abb[10]) + abb[1] * (abb[14] * (abb[5] + abb[10] + -abb[4]) + abb[2] * (abb[3] + abb[4] + -abb[5] + -abb[10]) + -(abb[3] * abb[14])) + abb[14] * (abb[14] * ((abb[4] * T(-9)) / T(4) + (abb[5] * T(9)) / T(4) + (abb[10] * T(9)) / T(4)) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3)) + abb[7] * abb[14] * (abb[5] * T(-2) + abb[10] * T(-2) + abb[4] * T(2)) + abb[3] * (abb[12] * abb[14] + (abb[23] * T(-7)) / T(2) + abb[13] * abb[14] * T(-3) + abb[14] * ((abb[14] * T(-9)) / T(4) + bc<T>[0] * int_to_imaginary<T>(3)) + abb[7] * abb[14] * T(2)) + abb[13] * abb[14] * (abb[4] * T(-3) + abb[5] * T(3) + abb[10] * T(3)) + abb[2] * (abb[2] * (abb[5] / T(4) + abb[10] / T(4) + -abb[3] / T(4) + -abb[4] / T(4)) + abb[14] * ((abb[5] * T(-5)) / T(2) + (abb[10] * T(-5)) / T(2) + (abb[4] * T(5)) / T(2)) + abb[12] * (abb[5] + abb[10] + -abb[4]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[5] * bc<T>[0] * int_to_imaginary<T>(3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3) + abb[7] * (abb[4] * T(-2) + abb[5] * T(2) + abb[10] * T(2)) + abb[13] * (abb[5] * T(-3) + abb[10] * T(-3) + abb[4] * T(3)) + abb[3] * ((abb[14] * T(5)) / T(2) + -abb[12] + abb[7] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-3) + abb[13] * T(3))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_333_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl23 = DLog_W_23<T>(kin),dl10 = DLog_W_10<T>(kin),dl22 = DLog_W_22<T>(kin),dl29 = DLog_W_29<T>(kin),dl1 = DLog_W_1<T>(kin),dl12 = DLog_W_12<T>(kin),dl21 = DLog_W_21<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl3 = DLog_W_3<T>(kin),dl31 = DLog_W_31<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),spdl7 = SpDLog_f_4_333_W_7<T>(kin),spdl23 = SpDLog_f_4_333_W_23<T>(kin),spdl10 = SpDLog_f_4_333_W_10<T>(kin),spdl22 = SpDLog_f_4_333_W_22<T>(kin),spdl12 = SpDLog_f_4_333_W_12<T>(kin),spdl21 = SpDLog_f_4_333_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,55> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl10(t), rlog(-v_path[4]), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_7(kin_path), dl22(t), f_2_1_14(kin_path), dl29(t) / kin_path.SqrtDelta, f_2_2_7(kin_path), rlog(kin.W[2] / kin_path.W[2]), f_2_1_8(kin_path), f_2_1_11(kin_path), f_2_1_15(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[1] / kin_path.W[1]), f_2_1_5(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(-v_path[4] + v_path[2]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), dl1(t), dl12(t), f_2_1_4(kin_path), dl21(t), dl17(t), dl2(t), dl3(t), dl31(t), dl18(t), dl4(t), dl19(t), dl16(t), dl20(t), dl5(t), dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_333_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_333_abbreviated(const std::array<T,55>& abb)
{
using TR = typename T::value_type;
T z[204];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[41];
z[3] = abb[42];
z[4] = abb[43];
z[5] = abb[45];
z[6] = abb[46];
z[7] = abb[50];
z[8] = abb[4];
z[9] = abb[49];
z[10] = abb[5];
z[11] = abb[47];
z[12] = abb[9];
z[13] = abb[10];
z[14] = abb[18];
z[15] = abb[20];
z[16] = abb[24];
z[17] = abb[25];
z[18] = abb[26];
z[19] = abb[27];
z[20] = abb[32];
z[21] = abb[33];
z[22] = abb[34];
z[23] = abb[35];
z[24] = abb[36];
z[25] = abb[52];
z[26] = abb[54];
z[27] = abb[2];
z[28] = abb[48];
z[29] = abb[8];
z[30] = abb[12];
z[31] = abb[13];
z[32] = abb[14];
z[33] = bc<TR>[0];
z[34] = abb[51];
z[35] = abb[53];
z[36] = abb[7];
z[37] = abb[37];
z[38] = abb[15];
z[39] = abb[17];
z[40] = abb[19];
z[41] = abb[21];
z[42] = abb[22];
z[43] = abb[23];
z[44] = abb[28];
z[45] = abb[29];
z[46] = abb[30];
z[47] = abb[31];
z[48] = abb[39];
z[49] = bc<TR>[3];
z[50] = abb[44];
z[51] = bc<TR>[1];
z[52] = bc<TR>[2];
z[53] = bc<TR>[4];
z[54] = bc<TR>[9];
z[55] = z[1] + -z[8];
z[55] = z[12] + z[55] / T(2);
z[56] = z[13] / T(2);
z[57] = z[55] + z[56];
z[58] = z[37] * z[57];
z[59] = z[20] + -z[23];
z[60] = -z[22] + z[59];
z[61] = z[21] / T(2);
z[60] = z[19] + -z[24] + z[60] / T(2) + z[61];
z[62] = z[14] + -z[26];
z[62] = z[62] / T(4);
z[62] = z[60] * z[62];
z[63] = (T(4) * z[10]) / T(3);
z[64] = T(3) * z[8];
z[65] = T(-7) * z[1] + -z[64];
z[65] = -z[12] + (T(-35) * z[13]) / T(12) + z[65] / T(4);
z[65] = -z[63] + z[65] / T(2);
z[65] = z[2] * z[65];
z[66] = T(5) * z[1];
z[67] = (T(47) * z[8]) / T(3) + z[66];
z[67] = (T(-4) * z[12]) / T(3) + (T(-3) * z[13]) / T(8) + z[63] + z[67] / T(8);
z[67] = z[28] * z[67];
z[68] = z[12] / T(3);
z[69] = T(3) * z[1];
z[70] = (T(-11) * z[8]) / T(3) + -z[69];
z[70] = (T(11) * z[13]) / T(6) + z[68] + z[70] / T(2);
z[70] = z[9] * z[70];
z[71] = z[1] + z[64];
z[71] = -z[12] + z[71] / T(2);
z[72] = -z[56] + -z[71];
z[72] = -z[10] + z[72] / T(2);
z[73] = z[6] / T(2);
z[72] = z[72] * z[73];
z[74] = (T(3) * z[13]) / T(4);
z[75] = (T(35) * z[8]) / T(3) + -z[69];
z[75] = (T(-11) * z[12]) / T(3) + z[74] + z[75] / T(4);
z[75] = z[63] + z[75] / T(2);
z[75] = z[3] * z[75];
z[76] = (T(3) * z[13]) / T(2);
z[77] = -z[55] + z[76];
z[77] = z[10] + z[77] / T(2);
z[78] = z[5] / T(2);
z[77] = z[77] * z[78];
z[79] = z[1] + z[8];
z[80] = (T(-47) * z[13]) / T(3) + T(3) * z[79];
z[63] = -z[63] + z[80] / T(8);
z[63] = z[11] * z[63];
z[80] = z[1] + -z[8] / T(3);
z[68] = z[13] / T(12) + z[68] + z[80] / T(4);
z[68] = z[7] * z[68];
z[80] = z[21] + -z[22];
z[80] = (T(-5) * z[16]) / T(4) + -z[24] + (T(-3) * z[59]) / T(8) + (T(7) * z[80]) / T(8);
z[81] = z[15] / T(4) + z[18];
z[82] = z[80] + z[81];
z[82] = z[35] * z[82];
z[83] = z[17] + -z[81];
z[84] = T(3) * z[22] + T(-7) * z[23];
z[84] = T(5) * z[16] + (T(7) * z[20]) / T(2) + z[84] / T(2);
z[84] = z[19] + (T(-3) * z[21]) / T(8) + z[83] + z[84] / T(4);
z[84] = z[34] * z[84];
z[85] = T(7) * z[8];
z[86] = -z[69] + -z[85];
z[86] = z[12] + (T(7) * z[13]) / T(4) + z[86] / T(4);
z[87] = z[4] / T(2);
z[86] = z[86] * z[87];
z[80] = z[80] + -z[83];
z[80] = z[25] * z[80];
z[88] = z[5] + z[6] + -z[11] + -z[28];
z[89] = -z[2] + -z[3] + T(2) * z[50];
z[88] = -z[4] + T(5) * z[7] + T(4) * z[9] + (T(-5) * z[88]) / T(2) + T(-2) * z[89];
z[89] = z[34] + -z[35];
z[89] = (T(7) * z[25]) / T(8) + z[26] / T(8) + -z[88] / T(9) + (T(-3) * z[89]) / T(4);
z[90] = int_to_imaginary<T>(1) * z[33];
z[89] = z[89] * z[90];
z[91] = z[26] + z[35];
z[92] = -z[34] + z[91];
z[92] = z[25] + z[92] / T(2);
z[92] = z[51] * z[92];
z[93] = z[17] * z[35];
z[62] = (T(5) * z[58]) / T(4) + z[62] + z[63] + z[65] + z[67] + (T(5) * z[68]) / T(2) + z[70] / T(4) + z[72] + z[75] + z[77] + z[80] + z[82] + z[84] + z[86] + z[89] + z[92] + -z[93];
z[62] = z[33] * z[62];
z[63] = z[22] + z[24];
z[65] = (T(5) * z[16]) / T(2);
z[63] = z[59] + (T(3) * z[63]) / T(2) + z[65];
z[63] = z[19] / T(4) + (T(-3) * z[21]) / T(4) + z[63] / T(2);
z[67] = z[63] + z[83];
z[68] = z[34] * z[67];
z[70] = z[25] * z[67];
z[72] = (T(3) * z[1]) / T(2);
z[75] = z[8] + z[72];
z[77] = z[12] / T(2);
z[80] = -z[10] + z[77];
z[82] = z[75] + z[80];
z[84] = z[56] + z[82];
z[86] = z[2] / T(2);
z[89] = z[84] * z[86];
z[89] = z[70] + z[89];
z[94] = z[14] * z[67];
z[95] = z[1] + z[12];
z[96] = -z[13] + T(3) * z[95];
z[96] = -z[10] + z[96] / T(2);
z[97] = z[11] / T(2);
z[96] = z[96] * z[97];
z[98] = z[13] + z[95];
z[99] = -z[10] + (T(3) * z[98]) / T(2);
z[100] = z[9] / T(2);
z[99] = z[99] * z[100];
z[101] = z[7] / T(2);
z[102] = z[84] * z[101];
z[103] = -z[8] + z[12];
z[104] = -z[10] + z[103];
z[105] = z[28] * z[104];
z[106] = z[4] * z[104];
z[96] = z[68] + -z[89] + z[94] + z[96] + -z[99] + z[102] + -z[105] + -z[106];
z[96] = z[36] * z[96];
z[63] = -z[63] + z[81];
z[99] = z[35] * z[63];
z[99] = -z[93] + z[99];
z[102] = z[58] / T(2);
z[59] = z[22] + z[24] / T(2) + (T(3) * z[59]) / T(2) + z[65];
z[59] = (T(3) * z[19]) / T(4) + z[59] / T(2) + -z[61];
z[61] = z[59] + z[83];
z[65] = z[34] * z[61];
z[83] = z[65] + z[99] + z[102];
z[107] = z[13] / T(4);
z[108] = (T(3) * z[1]) / T(4) + -z[107];
z[109] = z[12] / T(4);
z[110] = z[10] / T(2);
z[111] = z[8] + z[108] + -z[109] + z[110];
z[111] = z[28] * z[111];
z[112] = z[8] / T(2);
z[113] = z[1] + z[112];
z[114] = z[77] + z[113];
z[115] = z[13] + z[110] + -z[114] / T(2);
z[115] = z[11] * z[115];
z[116] = z[13] + z[103];
z[117] = z[101] * z[116];
z[118] = z[57] * z[87];
z[119] = z[13] + z[80];
z[120] = z[113] + z[119];
z[121] = z[3] / T(2);
z[122] = z[120] * z[121];
z[123] = z[9] * z[116];
z[89] = -z[83] + z[89] + -z[111] + z[115] + z[117] + z[118] + -z[122] + (T(3) * z[123]) / T(2);
z[89] = z[32] * z[89];
z[111] = z[9] * z[120];
z[115] = (T(3) * z[8]) / T(2);
z[117] = z[1] + z[115];
z[118] = z[117] + -z[119];
z[122] = z[2] * z[118];
z[111] = z[111] + z[122];
z[122] = z[14] * z[61];
z[124] = z[65] + z[122];
z[125] = (T(5) * z[8]) / T(2);
z[126] = (T(3) * z[12]) / T(2);
z[127] = z[125] + -z[126];
z[128] = z[10] + z[13];
z[129] = -z[1] + z[128];
z[130] = z[127] + -z[129];
z[130] = z[101] * z[130];
z[131] = z[97] * z[120];
z[132] = z[25] * z[61];
z[104] = z[3] * z[104];
z[106] = -z[104] + -z[106] + -z[111] / T(2) + z[124] + z[130] + z[131] + -z[132];
z[106] = z[0] * z[106];
z[89] = z[89] + z[96] + z[106];
z[96] = z[8] + z[10];
z[106] = (T(5) * z[12]) / T(2);
z[111] = -z[72] + z[96] + -z[106];
z[130] = (T(5) * z[13]) / T(2);
z[131] = z[111] + -z[130];
z[131] = z[101] * z[131];
z[133] = z[84] * z[100];
z[134] = -z[76] + z[82];
z[135] = z[121] * z[134];
z[84] = z[28] * z[84];
z[136] = z[2] * z[128];
z[131] = z[84] / T(2) + -z[99] + -z[131] + -z[133] + z[135] + z[136];
z[133] = -(z[29] * z[131]);
z[135] = z[55] + -z[56];
z[135] = -z[10] + z[135] / T(2);
z[137] = z[11] + -z[28];
z[137] = z[135] * z[137];
z[138] = z[71] + -z[76];
z[139] = z[100] * z[138];
z[137] = z[137] + -z[139];
z[139] = z[60] / T(2);
z[140] = z[34] + z[35];
z[141] = z[139] * z[140];
z[142] = -(z[57] * z[101]);
z[141] = z[102] + -z[137] + z[141] + z[142];
z[141] = z[46] * z[141];
z[142] = z[11] * z[128];
z[143] = z[7] * z[116];
z[105] = z[105] + z[142] + -z[143];
z[104] = -z[104] + -z[105] + z[123] + -z[136];
z[104] = z[31] * z[104];
z[136] = -z[2] + z[3];
z[135] = z[135] * z[136];
z[136] = z[57] * z[100];
z[135] = z[135] + -z[136];
z[136] = -z[13] + z[79];
z[144] = z[136] / T(4);
z[145] = z[7] * z[144];
z[102] = z[102] + z[135] + z[145];
z[102] = z[47] * z[102];
z[145] = z[46] * z[138];
z[144] = -(z[47] * z[144]);
z[146] = -(z[29] * z[128]);
z[144] = z[144] + -z[145] / T(2) + z[146];
z[144] = z[4] * z[144];
z[145] = -(z[29] * z[67]);
z[146] = z[46] + -z[47];
z[146] = z[139] * z[146];
z[145] = z[145] + z[146];
z[145] = z[14] * z[145];
z[146] = z[34] / T(2);
z[91] = -z[91] / T(2) + z[146];
z[91] = z[53] * z[91];
z[147] = z[47] * z[139];
z[147] = -z[53] + z[147];
z[147] = z[25] * z[147];
z[148] = -z[51] / T(2) + z[52];
z[92] = z[92] * z[148];
z[91] = z[89] + z[91] + z[92] + z[102] + z[104] + z[133] + z[141] + z[144] + z[145] + z[147];
z[91] = int_to_imaginary<T>(1) * z[91];
z[88] = z[49] * z[88];
z[88] = z[88] + z[91];
z[62] = z[62] + T(3) * z[88];
z[62] = z[33] * z[62];
z[88] = T(3) * z[67];
z[91] = z[14] * z[88];
z[92] = T(3) * z[35];
z[63] = z[63] * z[92];
z[102] = T(3) * z[93];
z[63] = z[63] + -z[102];
z[104] = T(2) * z[13];
z[133] = -z[10] + z[104];
z[141] = z[5] * z[133];
z[144] = z[63] + z[141];
z[145] = (T(5) * z[12]) / T(4);
z[147] = T(2) * z[8];
z[148] = (T(13) * z[1]) / T(4) + z[145] + z[147];
z[149] = (T(3) * z[10]) / T(2);
z[107] = z[107] + z[148] + -z[149];
z[107] = z[9] * z[107];
z[150] = T(3) * z[13];
z[151] = z[95] + z[150];
z[151] = -z[10] + z[151] / T(4);
z[151] = z[28] * z[151];
z[152] = -z[10] + z[69];
z[153] = z[12] + z[147] + z[152];
z[153] = z[6] * z[153];
z[154] = -z[10] + z[150];
z[155] = z[97] * z[154];
z[156] = T(5) * z[13];
z[157] = T(7) * z[10];
z[158] = -z[156] + -z[157];
z[158] = z[86] * z[158];
z[159] = T(2) * z[10];
z[148] = (T(9) * z[13]) / T(4) + -z[148] + z[159];
z[148] = z[3] * z[148];
z[160] = T(9) * z[13] + z[95];
z[160] = z[160] / T(2);
z[161] = T(3) * z[10];
z[162] = -z[160] + z[161];
z[162] = z[101] * z[162];
z[163] = T(-5) * z[12] + -z[69] + z[147];
z[164] = z[13] + z[161];
z[165] = -z[163] + -z[164];
z[165] = z[4] * z[165];
z[107] = -z[91] + z[107] + z[144] + z[148] + -z[151] + -z[153] + -z[155] + z[158] + z[162] + z[165];
z[107] = z[29] * z[107];
z[148] = z[26] * z[88];
z[158] = T(3) * z[70];
z[148] = z[148] + z[158];
z[162] = -z[76] + z[115];
z[165] = T(4) * z[1];
z[166] = -z[106] + z[159] + -z[162] + -z[165];
z[166] = z[7] * z[166];
z[167] = -z[110] + z[114];
z[167] = z[3] * z[167];
z[155] = z[155] + z[167];
z[168] = -z[153] + z[155];
z[169] = (T(5) * z[10]) / T(2);
z[170] = -z[1] + -z[106] + z[115] + z[169];
z[170] = z[28] * z[170];
z[171] = (T(5) * z[1]) / T(2);
z[172] = z[8] + z[126] + z[171];
z[173] = T(3) * z[172];
z[174] = (T(7) * z[13]) / T(2);
z[175] = T(5) * z[10];
z[176] = z[173] + -z[174] + -z[175];
z[176] = z[78] * z[176];
z[82] = z[82] + z[130];
z[82] = z[82] * z[87];
z[177] = z[75] + z[77];
z[177] = T(3) * z[177];
z[178] = -z[56] + -z[177];
z[178] = z[10] + z[178] / T(2);
z[178] = z[2] * z[178];
z[179] = (T(-11) * z[1]) / T(4) + -z[8] + (T(-7) * z[12]) / T(4) + -z[74] + z[149];
z[179] = z[9] * z[179];
z[91] = z[82] + z[91] + -z[148] + z[166] + -z[168] + z[170] + z[176] + z[178] + z[179];
z[91] = z[36] * z[91];
z[166] = z[69] + -z[150];
z[170] = (T(7) * z[8]) / T(2);
z[176] = -z[77] + z[166] + z[170] + z[175];
z[178] = z[73] * z[176];
z[179] = T(3) * z[61];
z[180] = z[14] * z[179];
z[178] = -z[141] + z[178] + -z[180];
z[181] = -z[77] + z[110];
z[182] = T(2) * z[1];
z[115] = z[115] + z[182];
z[183] = -z[115] + z[181];
z[184] = z[28] * z[183];
z[185] = -z[112] + z[126];
z[186] = z[1] + z[185];
z[187] = -z[104] + z[149] + -z[186] / T(2);
z[187] = z[9] * z[187];
z[188] = (T(7) * z[12]) / T(2);
z[162] = z[162] + -z[182] + -z[188];
z[189] = z[159] + -z[162];
z[189] = z[7] * z[189];
z[190] = (T(13) * z[12]) / T(2);
z[152] = T(7) * z[13] + z[152] + -z[170] + z[190];
z[152] = z[87] * z[152];
z[164] = z[86] * z[164];
z[170] = -z[150] + z[186];
z[170] = z[121] * z[170];
z[186] = -z[150] + z[175];
z[186] = z[97] * z[186];
z[152] = z[152] + z[164] + z[170] + -z[178] + -z[184] + z[186] + z[187] + z[189];
z[152] = z[0] * z[152];
z[148] = -z[63] + z[148];
z[164] = z[1] / T(2);
z[170] = -z[64] + z[106] + z[130] + -z[164];
z[170] = z[9] * z[170];
z[186] = (T(7) * z[1]) / T(2);
z[187] = z[64] + z[186];
z[189] = z[10] + z[77];
z[191] = -z[76] + z[187] + z[189];
z[191] = z[28] * z[191];
z[192] = -z[56] + z[177];
z[193] = z[10] + z[192];
z[193] = z[2] * z[193];
z[193] = -z[191] + z[193];
z[194] = (T(17) * z[1]) / T(2);
z[195] = (T(9) * z[13]) / T(2);
z[196] = T(9) * z[8] + -z[189] + z[194] + -z[195];
z[196] = z[121] * z[196];
z[194] = -z[157] + z[194];
z[197] = (T(11) * z[12]) / T(2);
z[198] = z[64] + z[194] + z[197];
z[199] = z[76] + z[198];
z[200] = z[101] * z[199];
z[171] = z[64] + -z[171] + -z[197];
z[197] = (T(13) * z[13]) / T(2);
z[171] = z[157] + T(3) * z[171] + -z[197];
z[171] = z[87] * z[171];
z[173] = -z[157] + z[173];
z[201] = z[56] + z[173];
z[78] = -(z[78] * z[201]);
z[202] = z[10] + -z[13];
z[203] = z[11] * z[202];
z[78] = z[78] + z[148] + z[170] + z[171] + z[193] / T(2) + z[196] + z[200] + T(-3) * z[203];
z[78] = z[30] * z[78];
z[170] = -z[10] + z[192];
z[170] = z[2] * z[170];
z[171] = z[114] + -z[154];
z[171] = z[3] * z[171];
z[192] = z[13] + -z[114];
z[192] = z[10] + T(3) * z[192];
z[192] = z[11] * z[192];
z[170] = T(7) * z[123] + z[170] + z[171] + -z[191] + z[192];
z[171] = T(3) * z[65];
z[191] = z[4] * z[136];
z[158] = (T(-3) * z[58]) / T(2) + -z[63] + T(3) * z[143] + z[158] + z[170] / T(2) + -z[171] + (T(3) * z[191]) / T(4);
z[158] = z[32] * z[158];
z[75] = z[75] + z[189];
z[170] = z[56] + z[75];
z[170] = z[2] * z[170];
z[129] = -z[129] + z[185];
z[129] = z[3] * z[129];
z[185] = z[11] * z[120];
z[84] = -z[84] + -z[123] + -z[129] + -z[170] + z[185];
z[129] = -z[1] + z[64];
z[129] = -z[12] + -z[74] + z[129] / T(4);
z[129] = z[4] * z[129];
z[83] = -z[70] + z[83] + z[84] / T(2) + z[129] + T(-2) * z[143];
z[84] = T(3) * z[31];
z[90] = -z[84] + T(3) * z[90];
z[83] = -(z[83] * z[90]);
z[78] = z[78] / T(2) + z[83] + z[91] + z[107] + z[152] + z[158];
z[78] = z[30] * z[78];
z[72] = (T(5) * z[10]) / T(4) + z[72] + -z[74] + -z[77] + z[147];
z[72] = z[28] * z[72];
z[83] = T(19) * z[13];
z[91] = T(9) * z[95];
z[107] = z[83] + -z[91];
z[107] = z[107] / T(2) + z[161];
z[129] = z[11] * z[107];
z[152] = (T(11) * z[13]) / T(2);
z[75] = z[75] + -z[152];
z[75] = z[7] * z[75];
z[75] = -z[75] + z[129];
z[111] = z[111] + z[195];
z[111] = z[73] * z[111];
z[129] = (T(3) * z[26]) / T(2);
z[129] = z[67] * z[129];
z[158] = (T(3) * z[79]) / T(4) + -z[104];
z[158] = z[9] * z[158];
z[170] = -z[10] + z[172];
z[172] = -z[130] + z[170];
z[185] = z[5] * z[172];
z[191] = (T(5) * z[13]) / T(4);
z[192] = -(z[2] * z[191]);
z[193] = z[10] / T(4);
z[196] = (T(-9) * z[1]) / T(8) + -z[8] + -z[12] / T(8) + (T(15) * z[13]) / T(8) + -z[193];
z[196] = z[4] * z[196];
z[68] = z[63] + (T(3) * z[68]) / T(2) + z[72] + -z[75] / T(4) + z[111] + z[129] + z[158] + (T(-3) * z[185]) / T(4) + z[192] + z[196];
z[68] = z[36] * z[68];
z[72] = z[111] + z[144];
z[75] = -z[10] + z[160];
z[75] = z[75] * z[101];
z[111] = z[114] + -z[150];
z[111] = z[9] * z[111];
z[129] = z[10] + z[156];
z[144] = z[86] * z[129];
z[75] = z[72] + -z[75] + z[82] + z[111] + -z[144] + -z[151] + -z[155];
z[75] = z[29] * z[75];
z[68] = z[68] + -z[75];
z[68] = z[36] * z[68];
z[82] = z[1] + (T(3) * z[8]) / T(4) + z[109];
z[109] = T(3) * z[82];
z[111] = z[109] + -z[110] + z[130];
z[111] = z[5] * z[111];
z[130] = T(11) * z[1] + (T(19) * z[8]) / T(2) + z[126] + z[161];
z[144] = z[28] / T(2);
z[130] = z[130] * z[144];
z[111] = -z[111] + z[130];
z[130] = z[26] * z[179];
z[130] = z[130] + z[171];
z[144] = (T(3) * z[12]) / T(4);
z[151] = z[144] + z[149];
z[156] = (T(19) * z[8]) / T(4) + -z[151] + z[165];
z[156] = z[9] * z[156];
z[158] = z[144] + -z[149];
z[160] = z[76] + z[158];
z[165] = (T(13) * z[8]) / T(4) + z[160] + z[165];
z[165] = z[3] * z[165];
z[185] = T(11) * z[13] + T(-3) * z[114];
z[192] = T(4) * z[10];
z[196] = -z[185] / T(2) + -z[192];
z[196] = z[11] * z[196];
z[79] = -z[79] + z[128];
z[79] = z[7] * z[79];
z[59] = -z[59] + z[81];
z[81] = z[59] * z[92];
z[92] = z[69] + z[125];
z[200] = z[77] + z[92];
z[203] = -z[13] + -z[169] + (T(-3) * z[200]) / T(2);
z[203] = z[4] * z[203];
z[79] = z[79] + z[81] + -z[102] + z[111] + z[130] + z[156] + z[165] + -z[180] + z[196] + z[203];
z[79] = z[43] * z[79];
z[81] = z[56] + z[109] + z[110];
z[81] = z[5] * z[81];
z[81] = z[81] + -z[130] + z[153];
z[102] = T(4) * z[8];
z[109] = z[76] + z[77] + -z[102] + -z[186];
z[109] = z[9] * z[109];
z[125] = z[125] + z[181] + z[182];
z[130] = -(z[28] * z[125]);
z[156] = z[181] + -z[187];
z[156] = z[3] * z[156];
z[165] = -z[112] + z[188];
z[180] = z[69] + z[165];
z[181] = z[104] + -z[110] + z[180] / T(2);
z[181] = z[4] * z[181];
z[182] = z[86] * z[202];
z[186] = (T(3) * z[114]) / T(2);
z[188] = z[13] + z[149] + -z[186];
z[188] = z[11] * z[188];
z[196] = -z[13] + z[110] + (T(-5) * z[114]) / T(2);
z[196] = z[7] * z[196];
z[109] = z[81] + z[109] + z[130] + z[156] + z[181] + -z[182] + z[188] + z[196];
z[109] = z[32] * z[109];
z[115] = -z[76] + z[77] + z[115];
z[130] = z[115] + z[159];
z[130] = z[7] * z[130];
z[156] = z[182] + z[184];
z[119] = z[92] + z[119];
z[119] = z[87] * z[119];
z[119] = z[119] + z[156];
z[181] = z[114] + z[150];
z[181] = -z[10] + z[181] / T(2);
z[181] = z[3] * z[181];
z[182] = -z[77] + z[117];
z[149] = z[13] + -z[149] + z[182] / T(2);
z[149] = z[9] * z[149];
z[175] = z[150] + z[175];
z[175] = z[97] * z[175];
z[130] = z[119] + z[130] + -z[149] + z[175] + -z[178] + -z[181];
z[149] = z[0] + -z[30];
z[130] = z[130] * z[149];
z[149] = z[1] + (T(7) * z[8]) / T(4) + -z[151];
z[149] = z[9] * z[149];
z[151] = z[1] + z[8] / T(4) + z[160];
z[151] = z[3] * z[151];
z[111] = z[111] + z[149] + z[151];
z[59] = z[35] * z[59];
z[59] = z[59] + -z[93];
z[65] = z[59] + z[65];
z[93] = (T(3) * z[122]) / T(2);
z[61] = z[26] * z[61];
z[149] = (T(3) * z[61]) / T(2);
z[151] = -z[159] + -z[185] / T(4);
z[151] = z[11] * z[151];
z[159] = z[13] + z[193] + (T(-3) * z[200]) / T(4);
z[159] = z[4] * z[159];
z[136] = z[136] / T(2);
z[160] = -z[10] + -z[136];
z[160] = z[7] * z[160];
z[65] = (T(3) * z[65]) / T(2) + -z[93] + z[111] / T(2) + z[149] + z[151] + z[159] + z[160];
z[65] = z[27] * z[65];
z[111] = -z[117] + z[189];
z[111] = z[9] * z[111];
z[117] = z[10] + z[114];
z[117] = z[28] * z[117];
z[151] = z[3] * z[120];
z[111] = z[111] + -z[117] + -z[151];
z[117] = z[101] * z[120];
z[120] = z[4] * z[128];
z[59] = -z[59] + z[111] / T(2) + z[117] + z[120] + z[122] + z[142];
z[59] = -(z[59] * z[90]);
z[90] = z[7] * z[115];
z[111] = T(4) * z[13];
z[102] = -z[12] + z[69] + z[102] + -z[111];
z[102] = z[4] * z[102];
z[115] = -z[76] + z[182];
z[115] = z[9] * z[115];
z[90] = z[90] + z[102] + z[115] + z[141] + z[156] + z[168];
z[90] = z[36] * z[90];
z[59] = z[59] + z[65] + z[90] + z[109] + z[130];
z[59] = z[27] * z[59];
z[65] = z[1] / T(4) + z[8];
z[90] = z[65] + -z[110] + -z[144] + z[191];
z[90] = z[7] * z[90];
z[102] = -z[108] + z[110] + z[145] + -z[147];
z[102] = z[4] * z[102];
z[65] = -z[65] + z[74] + z[158];
z[65] = z[28] * z[65];
z[74] = -z[110] + z[150];
z[74] = z[2] * z[74];
z[108] = z[111] + -z[182];
z[108] = z[9] * z[108];
z[109] = z[97] * z[129];
z[65] = z[65] + -z[72] + z[74] + z[90] + z[102] + z[108] + z[109] + -z[167];
z[65] = z[36] * z[65];
z[72] = -z[56] + z[77] + z[187];
z[72] = z[9] * z[72];
z[74] = z[3] * z[125];
z[90] = -z[10] + z[186];
z[90] = z[11] * z[90];
z[66] = (T(9) * z[8]) / T(2) + z[66] + z[80];
z[66] = z[66] * z[101];
z[66] = z[66] + z[72] + z[74] + -z[81] + z[90] + -z[119];
z[66] = z[0] * z[66];
z[72] = z[10] + -z[174];
z[72] = z[2] * z[72];
z[74] = T(9) * z[1] + z[85];
z[74] = -z[10] + z[12] + z[74] / T(2);
z[74] = z[3] * z[74];
z[81] = z[4] * z[116];
z[72] = z[72] + z[74] + (T(-9) * z[81]) / T(2) + (T(-7) * z[105]) / T(2) + -z[123];
z[72] = z[32] * z[72];
z[65] = z[65] + z[66] + z[72] / T(2) + z[75];
z[65] = z[32] * z[65];
z[66] = z[94] + z[120] + z[131];
z[66] = z[29] * z[66];
z[71] = -z[56] + z[71];
z[72] = -z[3] + -z[28];
z[71] = z[71] * z[72];
z[55] = z[55] + z[76];
z[72] = z[2] + z[11];
z[55] = z[55] * z[72];
z[72] = -(z[60] * z[140]);
z[55] = z[55] + z[71] + z[72] + -z[143];
z[71] = z[8] + -z[13] + -z[77] + z[164];
z[71] = z[4] * z[71];
z[72] = -(z[25] * z[139]);
z[55] = z[55] / T(2) + -z[58] + z[71] + z[72];
z[55] = z[31] * z[55];
z[55] = z[55] / T(2) + z[66] + -z[89];
z[55] = z[55] * z[84];
z[66] = z[73] + -z[101];
z[71] = -z[13] + z[92] + z[189];
z[66] = z[66] * z[71];
z[71] = -z[56] + z[82] + z[110];
z[72] = z[5] + -z[9];
z[72] = z[71] * z[72];
z[74] = -(z[118] * z[121]);
z[75] = -z[114] + -z[128];
z[75] = z[75] * z[97];
z[61] = -z[61] + z[66] + z[72] + z[74] + z[75] + -z[124];
z[61] = z[42] * z[61];
z[66] = -z[56] + -z[96] + z[126] + z[164];
z[66] = z[28] * z[66];
z[72] = -z[76] + z[170];
z[74] = -z[6] + z[9];
z[72] = z[72] * z[74];
z[74] = -z[5] + z[7];
z[74] = z[74] * z[172];
z[75] = z[2] * z[134];
z[66] = z[66] + z[72] + z[74] + z[75];
z[72] = z[26] * z[67];
z[66] = z[66] / T(2) + z[70] + z[72] + -z[94] + z[99];
z[66] = z[41] * z[66];
z[70] = z[44] * z[60];
z[60] = z[45] * z[60];
z[72] = -z[60] + z[70];
z[67] = z[39] * z[67];
z[67] = z[67] + z[72] / T(2);
z[67] = z[14] * z[67];
z[72] = z[45] * z[137];
z[74] = -(z[44] * z[135]);
z[57] = z[45] * z[57];
z[75] = z[44] * z[136];
z[57] = z[57] + -z[75];
z[57] = z[57] * z[101];
z[57] = z[57] + z[61] + z[66] + z[67] + z[72] + z[74];
z[61] = z[161] + -z[177] + -z[197];
z[61] = z[61] * z[86];
z[66] = (T(9) * z[1]) / T(2);
z[67] = T(11) * z[8] + z[66] + -z[76] + -z[190];
z[67] = z[67] / T(2) + z[192];
z[67] = z[28] * z[67];
z[66] = T(5) * z[8] + z[66] + -z[189] + z[195];
z[66] = z[66] * z[73];
z[72] = z[34] * z[88];
z[73] = -z[83] + -z[91];
z[73] = z[73] / T(2) + z[161];
z[73] = z[73] * z[100];
z[74] = -(z[97] * z[107]);
z[76] = -z[10] + z[116];
z[76] = z[7] * z[76];
z[61] = z[61] + z[63] + z[66] + z[67] + z[72] + z[73] + z[74] + z[76];
z[61] = z[39] * z[61];
z[63] = z[6] * z[176];
z[66] = -z[103] / T(2) + z[150] + -z[161];
z[66] = z[9] * z[66];
z[67] = -z[13] + z[182];
z[67] = T(3) * z[67] + z[157];
z[67] = z[2] * z[67];
z[66] = z[66] + -z[67];
z[63] = z[63] + z[66] / T(2);
z[66] = z[5] * z[71];
z[66] = -z[66] + z[132];
z[71] = -z[165] + -z[166];
z[71] = z[10] + z[71] / T(2);
z[71] = z[3] * z[71];
z[72] = -z[154] + z[180];
z[72] = z[4] * z[72];
z[73] = -(z[10] * z[11]);
z[74] = -z[10] + (T(-3) * z[95]) / T(2);
z[74] = z[7] * z[74];
z[63] = z[63] / T(2) + (T(-3) * z[66]) / T(2) + z[71] + z[72] / T(4) + z[73] + z[74] + -z[93] + -z[149];
z[63] = z[0] * z[63];
z[66] = z[7] * z[162];
z[71] = z[56] + -z[114];
z[71] = z[9] * z[71];
z[72] = -z[104] + z[163];
z[72] = z[4] * z[72];
z[66] = z[66] + z[71] + z[72] + -z[141] + z[153] + z[155] + z[156];
z[66] = z[29] * z[66];
z[63] = z[63] + z[66];
z[63] = z[0] * z[63];
z[66] = z[106] + -z[157];
z[69] = -z[66] + -z[69] + -z[112] + -z[150];
z[69] = z[6] * z[69];
z[71] = (T(11) * z[8]) / T(2);
z[72] = (T(-17) * z[12]) / T(2) + z[71] + z[157] + -z[166];
z[72] = z[3] * z[72];
z[73] = z[13] + z[114];
z[73] = T(3) * z[73] + -z[157];
z[73] = z[11] * z[73];
z[74] = z[1] + -z[13] + z[127];
z[74] = T(3) * z[74] + -z[157];
z[74] = z[7] * z[74];
z[76] = z[9] * z[103];
z[67] = -z[67] + z[69] + z[72] + z[73] + z[74] + z[76];
z[66] = -z[66] + z[71] + z[166];
z[66] = z[66] * z[87];
z[69] = -(z[25] * z[179]);
z[66] = z[66] + z[67] / T(2) + z[69] + z[171];
z[66] = z[48] * z[66];
z[67] = z[152] + z[157] + z[177];
z[67] = z[2] * z[67];
z[69] = z[28] * z[199];
z[71] = -z[195] + z[198];
z[71] = z[3] * z[71];
z[72] = -(z[5] * z[201]);
z[64] = (T(23) * z[12]) / T(2) + (T(15) * z[13]) / T(2) + -z[64] + z[194];
z[64] = z[7] * z[64];
z[73] = -(z[9] * z[98]);
z[64] = z[64] + z[67] + z[69] + z[71] + z[72] + z[73];
z[67] = z[152] + -z[173];
z[67] = z[67] * z[87];
z[64] = z[64] / T(2) + z[67] + z[148];
z[64] = z[38] * z[64];
z[67] = z[35] * z[183];
z[56] = z[56] + z[80] + z[113];
z[56] = z[14] * z[56];
z[69] = -(z[146] * z[154]);
z[71] = -(z[26] * z[133]);
z[72] = z[25] * z[202];
z[56] = z[56] + z[67] + z[69] + z[71] + z[72] / T(2);
z[56] = z[40] * z[56];
z[67] = z[7] + z[28];
z[69] = -z[10] + z[95];
z[67] = z[67] * z[69];
z[69] = -(z[9] * z[13]);
z[67] = z[67] + z[69];
z[69] = prod_pow(z[29], 2);
z[67] = z[67] * z[69];
z[71] = -z[44] + -z[45];
z[58] = z[58] * z[71];
z[58] = z[58] + z[67];
z[67] = z[45] * z[138];
z[69] = z[69] * z[128];
z[67] = z[67] + z[69] + z[75];
z[69] = (T(-9) * z[1]) / T(4) + z[8] + (T(-13) * z[12]) / T(4) + (T(15) * z[13]) / T(4) + z[169];
z[69] = z[39] * z[69];
z[67] = (T(3) * z[67]) / T(2) + z[69];
z[67] = z[4] * z[67];
z[69] = -(z[39] * z[88]);
z[70] = (T(77) * z[54]) / T(2) + T(-3) * z[70];
z[69] = z[69] + z[70] / T(2);
z[69] = z[25] * z[69];
z[60] = (T(3) * z[60]) / T(2);
z[70] = (T(76) * z[54]) / T(3);
z[71] = -z[60] + z[70];
z[71] = z[35] * z[71];
z[60] = -z[60] + -z[70];
z[60] = z[34] * z[60];
z[70] = z[26] * z[54];
return z[55] + z[56] + T(3) * z[57] + (T(3) * z[58]) / T(2) + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + (T(-73) * z[70]) / T(12) + z[71] + z[78] + z[79];
}



template IntegrandConstructorType<double> f_4_333_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_333_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_333_construct (const Kin<qd_real>&);
#endif

}