#include "f_4_107.h"

namespace PentagonFunctions {

template <typename T> T f_4_107_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_107_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_107_W_23 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-6) + prod_pow(abb[2], 2) * abb[4] * T(6) + prod_pow(abb[1], 2) * (abb[4] * T(-6) + abb[3] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_107_W_10 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_107_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[2] * ((T(3) * kin.v[2]) / T(4) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]);
c[1] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(-12) + T(6) * kin.v[1] + T(12) * kin.v[2] + T(-12) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * (kin.v[2] / T(2) + T(-14) + T(-14) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(-14) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + kin.v[2] + T(-14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[2] + T(-8) * kin.v[4]))) + rlog(-kin.v[4]) * (bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[2] * ((T(-19) * kin.v[2]) / T(4) + T(-5) + T(-5) * kin.v[4]) + kin.v[1] * ((T(-19) * kin.v[2]) / T(2) + T(-5) + (T(-19) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(-5) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[2] + T(-8) * kin.v[4]))) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(-8) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[2] * (T(-3) + T(-6) * kin.v[2] + T(-3) * kin.v[4]) + kin.v[1] * (T(-3) + (T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-12) * kin.v[2] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-16) * kin.v[2] + T(-8) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(14) + T(14) * kin.v[4]) + kin.v[1] * (-kin.v[2] + T(14) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[2] + T(8) * kin.v[4])));
c[2] = T(3) * kin.v[1] + T(3) * kin.v[2];
c[3] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-6) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[2]) + rlog(-kin.v[4]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[2] + T(6) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[5] * (abb[7] * (abb[3] * T(-6) + abb[8] * T(3) + abb[10] * T(6)) + abb[1] * abb[2] * (abb[10] * T(-8) + abb[3] * T(8)) + abb[11] * (abb[1] * abb[2] * T(-8) + abb[7] * T(3) + abb[2] * ((abb[2] * T(5)) / T(2) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[9] * T(8))) + abb[6] * (abb[6] * (abb[3] + abb[11] / T(2) + (abb[8] * T(9)) / T(2) + -abb[10] + abb[4] * T(-6)) + abb[3] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * (abb[10] * T(-6) + abb[8] * T(-3) + abb[3] * T(6)) + abb[11] * (abb[9] * T(-8) + abb[2] * T(-3) + bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * T(8)) + abb[9] * (abb[10] * T(-8) + abb[3] * T(8)) + abb[1] * (abb[3] * T(-8) + abb[10] * T(8))) + abb[2] * (abb[10] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[3] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * ((abb[8] * T(-3)) / T(2) + abb[3] * T(-7) + abb[4] * T(6) + abb[10] * T(7)) + abb[9] * (abb[3] * T(-8) + abb[10] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_107_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl10 = DLog_W_10<T>(kin),dl24 = DLog_W_24<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),spdl23 = SpDLog_f_4_107_W_23<T>(kin),spdl10 = SpDLog_f_4_107_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,21> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), dl10(t), rlog(-v_path[4]), f_2_1_7(kin_path), -rlog(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[4] / kin_path.W[4]), dl24(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(kin.W[23] / kin_path.W[23]), dl16(t), f_2_1_8(kin_path), dl17(t), dl18(t), dl5(t)}
;

        auto result = f_4_107_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_107_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[73];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[16];
z[3] = abb[18];
z[4] = abb[19];
z[5] = abb[20];
z[6] = abb[4];
z[7] = abb[8];
z[8] = abb[10];
z[9] = abb[11];
z[10] = abb[15];
z[11] = abb[2];
z[12] = abb[12];
z[13] = abb[6];
z[14] = abb[9];
z[15] = bc<TR>[0];
z[16] = abb[7];
z[17] = abb[13];
z[18] = abb[14];
z[19] = abb[17];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = T(2) * z[4];
z[23] = z[3] + z[22];
z[24] = T(2) * z[12];
z[25] = T(3) * z[5];
z[26] = T(2) * z[2];
z[27] = -z[23] + -z[24] + z[25] + -z[26];
z[27] = z[1] * z[27];
z[28] = (T(5) * z[12]) / T(2);
z[29] = z[3] / T(2);
z[30] = T(6) * z[4];
z[31] = T(3) * z[2];
z[32] = -z[28] + z[29] + z[30] + z[31];
z[32] = z[10] * z[32];
z[33] = z[2] + -z[5];
z[34] = z[12] / T(2);
z[35] = z[29] + z[33] + z[34];
z[35] = z[6] * z[35];
z[36] = T(2) * z[5];
z[37] = z[23] + z[36];
z[38] = T(7) * z[12];
z[39] = T(4) * z[2];
z[40] = -z[37] + z[38] + -z[39];
z[41] = z[8] * z[40];
z[37] = T(-5) * z[12] + z[26] + z[37];
z[37] = z[9] * z[37];
z[42] = z[2] + -z[4];
z[43] = -z[3] + z[5];
z[44] = -z[42] + z[43];
z[45] = T(2) * z[7];
z[46] = z[44] * z[45];
z[27] = -z[27] + -z[32] + z[35] + z[37] + -z[41] + z[46];
z[32] = z[17] * z[27];
z[35] = T(11) * z[2];
z[37] = T(5) * z[4];
z[41] = T(5) * z[3];
z[46] = T(-11) * z[5] + z[35] + -z[37] + z[41];
z[46] = z[10] * z[46];
z[47] = T(4) * z[4];
z[48] = z[26] + z[47];
z[49] = T(7) * z[5];
z[50] = z[41] + z[49];
z[50] = -z[48] + z[50] / T(2);
z[50] = z[1] * z[50];
z[47] = -z[39] + z[47];
z[51] = T(6) * z[5];
z[52] = z[29] + z[47] + -z[51];
z[52] = z[7] * z[52];
z[53] = z[42] + T(2) * z[43];
z[53] = z[8] * z[53];
z[54] = -z[22] + z[26];
z[55] = T(5) * z[5];
z[56] = (T(-7) * z[3]) / T(2) + -z[54] + z[55];
z[56] = z[9] * z[56];
z[57] = T(2) * z[3];
z[58] = (T(5) * z[2]) / T(2) + z[4] / T(2) + z[25] + -z[57];
z[58] = z[6] * z[58];
z[46] = z[46] / T(2) + z[50] + z[52] + z[53] + z[56] + z[58];
z[46] = prod_pow(z[13], 2) * z[46];
z[50] = T(3) * z[3];
z[52] = T(3) * z[12];
z[53] = z[50] + -z[52];
z[56] = z[5] + z[48] + z[53];
z[58] = T(2) * z[8];
z[56] = z[56] * z[58];
z[59] = T(4) * z[5];
z[60] = -z[39] + z[59];
z[61] = z[22] + z[50] + -z[60];
z[61] = z[9] * z[61];
z[39] = -z[5] + -z[22] + z[39] + -z[52];
z[39] = z[1] * z[39];
z[62] = T(3) * z[4];
z[53] = z[53] + z[62];
z[63] = T(-8) * z[2] + -z[53] + z[55];
z[63] = z[10] * z[63];
z[64] = T(7) * z[3] + -z[59];
z[65] = z[62] + z[64];
z[65] = z[6] * z[65];
z[66] = -(z[7] * z[64]);
z[39] = z[39] + z[56] + z[61] + z[63] + z[65] + z[66];
z[39] = z[13] * z[39];
z[56] = -z[2] + z[59];
z[61] = T(7) * z[4];
z[63] = T(9) * z[12];
z[65] = (T(-17) * z[3]) / T(2) + -z[56] + -z[61] + z[63];
z[65] = z[9] * z[65];
z[34] = (T(3) * z[4]) / T(2) + z[33] / T(2) + z[34] + z[50];
z[66] = T(3) * z[10];
z[34] = z[34] * z[66];
z[67] = T(23) * z[5] + z[41];
z[48] = z[48] + -z[63] + z[67] / T(2);
z[48] = z[1] * z[48];
z[67] = T(-13) * z[3] + T(-10) * z[4] + T(15) * z[12] + -z[26] + -z[49];
z[67] = z[8] * z[67];
z[68] = (T(27) * z[3]) / T(2) + -z[42] + -z[55];
z[68] = z[7] * z[68];
z[69] = (T(3) * z[12]) / T(2);
z[70] = z[5] + z[69];
z[71] = (T(-7) * z[2]) / T(2) + (T(-33) * z[3]) / T(2) + (T(-5) * z[4]) / T(2) + -z[70];
z[71] = z[6] * z[71];
z[34] = z[34] + z[48] + z[65] + z[67] + z[68] + z[71];
z[34] = z[11] * z[34];
z[48] = T(6) * z[2];
z[65] = T(12) * z[4] + z[41] + z[48] + -z[51] + z[52];
z[65] = z[1] * z[65];
z[48] = -z[3] + z[48] + -z[63];
z[48] = z[8] * z[48];
z[63] = -z[3] + T(-6) * z[12] + z[31];
z[63] = z[9] * z[63];
z[48] = z[48] + z[63];
z[63] = T(5) * z[2];
z[67] = -z[22] + z[63];
z[68] = -z[41] + z[67];
z[68] = z[6] * z[68];
z[71] = z[3] + z[25] + z[47];
z[71] = z[45] * z[71];
z[22] = -z[12] + z[22];
z[72] = -z[2] + -z[22];
z[72] = z[10] * z[72];
z[48] = T(2) * z[48] + z[65] + z[68] + z[71] + T(9) * z[72];
z[48] = z[14] * z[48];
z[34] = z[34] + z[39] + z[48];
z[34] = z[11] * z[34];
z[27] = -(z[18] * z[27]);
z[28] = -z[28] + -z[29] + z[55] + z[61];
z[28] = -z[2] + z[28] / T(2);
z[28] = prod_pow(z[20], 2) * z[28];
z[27] = z[27] + z[28];
z[28] = z[25] + z[62];
z[39] = z[3] + z[28];
z[48] = -z[31] + z[39] + z[52];
z[48] = z[8] * z[48];
z[39] = z[9] * z[39];
z[39] = z[39] + z[48];
z[48] = -z[36] + -z[50] + z[67];
z[48] = z[6] * z[48];
z[55] = z[2] + z[12];
z[61] = z[4] + z[5];
z[65] = -z[55] + T(2) * z[61];
z[65] = z[65] * z[66];
z[30] = z[30] + -z[52];
z[66] = z[30] + z[41];
z[51] = z[51] + z[66];
z[67] = z[1] * z[51];
z[68] = -z[5] + z[42];
z[71] = z[7] * z[68];
z[39] = T(2) * z[39] + -z[48] + z[65] + -z[67] + T(8) * z[71];
z[39] = z[13] * z[39];
z[30] = -z[25] + -z[30] + -z[57];
z[30] = z[1] * z[30];
z[48] = (T(13) * z[3]) / T(2) + -z[25] + -z[54] + z[69];
z[48] = z[6] * z[48];
z[65] = z[8] + z[9];
z[51] = z[51] * z[65];
z[67] = T(4) * z[3] + -z[42];
z[45] = -(z[45] * z[67]);
z[69] = -z[3] + -z[12];
z[69] = z[10] * z[69];
z[30] = z[30] + z[45] + z[48] + z[51] + (T(3) * z[69]) / T(2);
z[30] = z[11] * z[30];
z[45] = z[12] + z[44];
z[45] = z[45] * z[65];
z[24] = -z[2] + z[24] + -z[61];
z[48] = z[1] * z[24];
z[45] = z[45] + z[48];
z[40] = z[10] * z[40];
z[40] = z[40] + T(-2) * z[45];
z[48] = -z[36] + z[66];
z[48] = z[6] * z[48];
z[51] = z[7] * z[43];
z[40] = T(3) * z[40] + z[48] + T(8) * z[51];
z[40] = z[14] * z[40];
z[27] = T(3) * z[27] + z[30] + -z[39] + z[40];
z[27] = int_to_imaginary<T>(1) * z[27];
z[30] = (T(5) * z[3]) / T(2);
z[40] = z[5] + (T(23) * z[12]) / T(2) + -z[30] + z[62];
z[40] = z[40] / T(2) + -z[63];
z[40] = z[20] * z[40];
z[48] = (T(43) * z[2]) / T(6) + z[50];
z[50] = (T(-79) * z[4]) / T(6) + (T(-67) * z[5]) / T(6) + z[12] + z[48];
z[50] = z[8] * z[50];
z[48] = (T(-67) * z[4]) / T(6) + (T(-55) * z[5]) / T(6) + -z[12] + z[48];
z[48] = z[9] * z[48];
z[40] = T(3) * z[40] + z[48] + z[50];
z[48] = (T(23) * z[4]) / T(2) + (T(67) * z[5]) / T(2) + -z[41];
z[38] = (T(47) * z[2]) / T(12) + -z[38] + z[48] / T(6);
z[38] = z[1] * z[38];
z[48] = (T(-5) * z[42]) / T(3) + T(3) * z[43];
z[48] = z[7] * z[48];
z[50] = int_to_imaginary<T>(1) * z[15];
z[66] = -z[2] / T(3) + (T(55) * z[4]) / T(3) + (T(49) * z[5]) / T(3) + T(-11) * z[12] + -z[41];
z[66] = z[50] * z[66];
z[69] = z[3] / T(3) + (T(-19) * z[4]) / T(12) + (T(-5) * z[5]) / T(2) + (T(37) * z[12]) / T(4) + -z[63];
z[69] = z[10] * z[69];
z[71] = (T(-5) * z[2]) / T(6) + (T(43) * z[4]) / T(12) + (T(-3) * z[5]) / T(2) + (T(-7) * z[12]) / T(4) + z[57];
z[71] = z[6] * z[71];
z[38] = z[38] + z[40] / T(2) + z[48] + z[66] / T(4) + z[69] + z[71];
z[38] = z[15] * z[38];
z[27] = z[27] + z[38];
z[27] = z[15] * z[27];
z[38] = z[28] + -z[52] + z[57];
z[38] = z[38] * z[65];
z[40] = -z[2] + -z[3] + -z[36] + z[52] + -z[62];
z[40] = z[1] * z[40];
z[48] = z[6] + -z[7];
z[48] = z[48] * z[67];
z[52] = -z[53] + -z[56];
z[52] = z[10] * z[52];
z[38] = T(2) * z[38] + z[40] + z[48] + z[52];
z[38] = z[11] * z[38];
z[22] = -z[5] + z[22] + z[26];
z[22] = z[10] * z[22];
z[36] = z[4] + -z[36] + z[55];
z[36] = z[1] * z[36];
z[24] = z[24] * z[65];
z[22] = -z[22] + -z[24] + z[36];
z[24] = z[6] * z[43];
z[22] = T(3) * z[22] + z[24] + -z[51];
z[24] = -z[14] + z[50];
z[22] = z[22] * z[24];
z[24] = -z[31] + z[57];
z[28] = z[24] + -z[28];
z[28] = z[28] * z[65];
z[24] = z[24] + -z[25] + z[37];
z[24] = z[1] * z[24];
z[25] = T(2) * z[6] + T(4) * z[7];
z[31] = z[5] + z[42];
z[25] = z[25] * z[31];
z[36] = -z[3] + z[4];
z[40] = z[10] * z[36];
z[24] = z[24] + z[25] + z[28] + T(4) * z[40];
z[24] = z[13] * z[24];
z[22] = z[22] + z[24] + z[38];
z[24] = z[37] + -z[43] + -z[63];
z[24] = z[7] * z[24];
z[25] = z[65] * z[68];
z[28] = T(7) * z[2] + z[36];
z[37] = -z[28] + z[49];
z[37] = z[10] * z[37];
z[23] = -z[23] + -z[60];
z[23] = z[1] * z[23];
z[38] = -z[3] + z[61] + z[63];
z[38] = z[6] * z[38];
z[23] = z[23] + z[24] + T(3) * z[25] + z[37] + z[38];
z[23] = z[0] * z[23];
z[22] = T(2) * z[22] + z[23];
z[22] = z[0] * z[22];
z[23] = (T(-7) * z[12]) / T(2) + z[26] + z[29] + z[61];
z[23] = z[10] * z[23];
z[23] = z[23] + z[45];
z[24] = -z[30] + -z[62] + z[70];
z[24] = z[6] * z[24];
z[23] = T(3) * z[23] + z[24] + T(-4) * z[51];
z[23] = z[14] * z[23];
z[23] = z[23] + z[39];
z[23] = z[14] * z[23];
z[24] = T(-6) * z[3] + -z[5] + -z[47];
z[24] = z[24] * z[58];
z[25] = T(-9) * z[3] + z[54] + z[59];
z[25] = z[9] * z[25];
z[24] = z[24] + z[25];
z[24] = z[16] * z[24];
z[25] = -z[33] + -z[36];
z[25] = z[19] * z[25];
z[26] = z[16] * z[44];
z[25] = T(14) * z[25] + T(-11) * z[26];
z[25] = z[10] * z[25];
z[26] = T(2) * z[19];
z[28] = -z[5] + z[28];
z[28] = z[26] * z[28];
z[29] = z[3] + T(13) * z[5] + z[54];
z[29] = z[16] * z[29];
z[28] = z[28] + z[29];
z[28] = z[1] * z[28];
z[29] = -z[31] + -z[41];
z[26] = z[26] * z[29];
z[29] = -z[5] + -z[41];
z[29] = T(2) * z[29] + z[42];
z[29] = z[16] * z[29];
z[26] = z[26] + z[29];
z[26] = z[6] * z[26];
z[29] = -z[42] + -z[43];
z[29] = z[19] * z[29];
z[30] = T(-16) * z[42] + z[64];
z[30] = z[16] * z[30];
z[29] = T(10) * z[29] + z[30];
z[29] = z[7] * z[29];
z[30] = (T(45) * z[3]) / T(2) + T(133) * z[4] + T(151) * z[5];
z[30] = (T(-1085) * z[12]) / T(2) + T(3) * z[30];
z[30] = z[30] / T(2) + z[35];
z[30] = z[21] * z[30];
z[31] = z[2] + -z[3];
z[31] = z[19] * z[31] * z[65];
return z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] / T(4) + T(12) * z[31] + T(-3) * z[32] + z[34] + z[46];
}



template IntegrandConstructorType<double> f_4_107_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_107_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_107_construct (const Kin<qd_real>&);
#endif

}