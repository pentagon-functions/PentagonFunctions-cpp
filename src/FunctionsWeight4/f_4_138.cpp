#include "f_4_138.h"

namespace PentagonFunctions {

template <typename T> T f_4_138_abbreviated (const std::array<T,29>&);

template <typename T> class SpDLog_f_4_138_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_138_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (-kin.v[3] + T(-2) * kin.v[4]) + -prod_pow(kin.v[4], 2) + kin.v[1] * (-kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((-kin.v[4] / T(2) + (T(3) * kin.v[1]) / T(4) + kin.v[2] / T(2) + (T(-3) * kin.v[3]) / T(2) + T(-1) + kin.v[0]) * kin.v[1] + (-kin.v[2] / T(4) + -kin.v[3] / T(2) + kin.v[4] / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + ((T(3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(kin.v[3]) * ((-kin.v[2] / T(2) + (T(-3) * kin.v[1]) / T(4) + (T(3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + kin.v[2] / T(4) + kin.v[3] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(-3) * kin.v[3]) / T(4) + T(-1)) * kin.v[3] + (kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]));
c[1] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-kin.v[3] + -kin.v[4] + kin.v[1] + kin.v[2]) + rlog(kin.v[3]) * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[3] * abb[5] + -(prod_pow(abb[2], 2) * abb[5]) / T(2) + abb[4] * (prod_pow(abb[2], 2) / T(2) + -abb[3]) + abb[1] * (abb[2] * abb[4] + abb[1] * ((abb[4] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2)) + -(abb[2] * abb[5])));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_138_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl9 = DLog_W_9<T>(kin),dl8 = DLog_W_8<T>(kin),dl13 = DLog_W_13<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),spdl22 = SpDLog_f_4_138_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,29> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), dl16(t), rlog(kin.W[8] / kin_path.W[8]), dl3(t), rlog(kin.W[12] / kin_path.W[12]), rlog(v_path[2]), rlog(kin.W[0] / kin_path.W[0]), dl9(t), rlog(v_path[3]), rlog(kin.W[19] / kin_path.W[19]), dl8(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[4] / kin_path.W[4]), dl13(t), rlog(v_path[0]), dl17(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl20(t), f_2_1_1(kin_path), dl4(t), dl1(t), dl5(t)}
;

        auto result = f_4_138_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_138_abbreviated(const std::array<T,29>& abb)
{
using TR = typename T::value_type;
T z[69];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[6];
z[3] = abb[12];
z[4] = abb[21];
z[5] = abb[24];
z[6] = abb[26];
z[7] = abb[5];
z[8] = abb[7];
z[9] = abb[14];
z[10] = abb[2];
z[11] = abb[11];
z[12] = abb[15];
z[13] = abb[27];
z[14] = abb[18];
z[15] = abb[13];
z[16] = bc<TR>[0];
z[17] = abb[10];
z[18] = abb[28];
z[19] = abb[20];
z[20] = abb[3];
z[21] = abb[22];
z[22] = abb[23];
z[23] = abb[8];
z[24] = abb[9];
z[25] = abb[19];
z[26] = abb[16];
z[27] = abb[17];
z[28] = abb[25];
z[29] = bc<TR>[1];
z[30] = bc<TR>[2];
z[31] = bc<TR>[4];
z[32] = bc<TR>[7];
z[33] = bc<TR>[8];
z[34] = bc<TR>[9];
z[35] = T(2) * z[11];
z[36] = z[9] + -z[14];
z[37] = z[35] + z[36];
z[38] = z[25] * z[37];
z[39] = z[13] * z[37];
z[40] = z[11] + -z[24];
z[40] = z[23] * z[40];
z[41] = T(2) * z[40];
z[42] = z[11] + -z[14];
z[43] = z[5] * z[42];
z[44] = z[12] * z[42];
z[45] = -z[38] + z[39] + z[41] + z[43] + -z[44];
z[45] = z[17] * z[45];
z[46] = T(2) * z[1];
z[47] = -z[7] + z[9];
z[48] = z[46] + z[47];
z[49] = z[3] * z[48];
z[50] = -(z[15] * z[49]);
z[51] = prod_pow(z[29], 2);
z[52] = z[25] * z[51];
z[45] = z[45] + z[50] + z[52];
z[50] = T(2) * z[15];
z[52] = z[48] * z[50];
z[53] = -z[7] + -z[11] + z[46];
z[53] = z[10] * z[53];
z[54] = -(z[29] * z[30]);
z[53] = z[51] / T(2) + z[52] + z[53] + z[54];
z[53] = z[5] * z[53];
z[54] = z[10] * z[37];
z[35] = z[9] + z[14] + T(-2) * z[24] + z[35];
z[35] = z[19] * z[35];
z[55] = z[30] / T(2);
z[56] = T(-2) * z[29] + -z[55];
z[56] = z[30] * z[56];
z[56] = T(-2) * z[35] + z[51] + -z[54] + z[56];
z[56] = z[13] * z[56];
z[57] = z[10] * z[48];
z[46] = z[9] + z[46];
z[58] = -z[8] + z[46];
z[50] = z[50] * z[58];
z[50] = z[50] + z[57];
z[58] = T(2) * z[6];
z[50] = z[50] * z[58];
z[59] = z[18] * z[37];
z[39] = -z[39] + -z[44] + z[59];
z[60] = z[39] + -z[43];
z[61] = z[27] * z[60];
z[62] = -(z[4] * z[57]);
z[55] = z[29] + z[55];
z[55] = z[30] * z[55];
z[51] = (T(-5) * z[51]) / T(2) + z[55];
z[51] = z[18] * z[51];
z[55] = -z[5] + -z[18] + T(2) * z[25];
z[55] = z[19] * z[37] * z[55];
z[58] = -z[4] + z[5] + z[58];
z[58] = z[48] * z[58];
z[63] = -(z[22] * z[58]);
z[64] = prod_pow(z[16], 2);
z[65] = T(5) * z[3] + -z[4] / T(2);
z[65] = (T(-5) * z[5]) / T(6) + T(-3) * z[6] + z[65] / T(3);
z[65] = z[18] / T(3) + z[65] / T(2);
z[65] = z[64] * z[65];
z[66] = z[10] * z[44];
z[67] = z[5] + T(2) * z[13] + -z[18];
z[68] = z[31] * z[67];
z[45] = T(2) * z[45] + z[50] + z[51] + z[53] + z[55] + z[56] + z[61] + z[62] + z[63] + z[65] + z[66] + T(3) * z[68];
z[45] = int_to_imaginary<T>(1) * z[16] * z[45];
z[50] = z[25] * z[29];
z[40] = z[40] + z[50];
z[50] = z[5] / T(2);
z[51] = (T(5) * z[11]) / T(3) + (T(3) * z[29]) / T(2);
z[53] = (T(11) * z[1]) / T(3) + (T(-5) * z[7]) / T(3) + z[9] / T(3) + (T(-7) * z[30]) / T(3) + -z[51];
z[53] = z[50] * z[53];
z[55] = -z[1] + z[8];
z[55] = z[2] * z[55];
z[47] = z[1] + z[47] / T(2);
z[56] = z[30] + z[47] / T(3);
z[56] = z[4] * z[56];
z[61] = T(11) * z[1] + z[7] + (T(-13) * z[8]) / T(2) + (T(11) * z[9]) / T(2);
z[61] = T(-2) * z[30] + z[61] / T(3);
z[61] = z[6] * z[61];
z[62] = -z[29] + z[30];
z[62] = z[18] * z[62];
z[51] = (T(-5) * z[9]) / T(6) + (T(-4) * z[14]) / T(3) + (T(13) * z[24]) / T(6) + (T(-19) * z[30]) / T(12) + -z[51];
z[51] = z[13] * z[51];
z[40] = (T(13) * z[40]) / T(6) + (T(-4) * z[44]) / T(3) + -z[49] + z[51] + z[53] + z[55] / T(6) + z[56] + z[61] + (T(17) * z[62]) / T(12);
z[40] = z[40] * z[64];
z[36] = z[11] + z[36] / T(2);
z[51] = z[18] * z[36];
z[38] = -z[38] + z[51];
z[51] = -(z[13] * z[36]);
z[41] = -z[38] + -z[41] + -z[43] / T(2) + (T(3) * z[44]) / T(2) + z[51];
z[41] = z[17] * z[41];
z[39] = z[10] * z[39];
z[43] = z[5] * z[10];
z[42] = -(z[42] * z[43]);
z[39] = z[39] + z[41] + z[42];
z[39] = z[17] * z[39];
z[41] = T(2) * z[32];
z[42] = prod_pow(z[30], 3);
z[51] = -z[33] / T(2) + -z[41] + (T(2) * z[42]) / T(3);
z[53] = prod_pow(z[15], 2);
z[56] = z[47] * z[53];
z[61] = z[15] * z[48];
z[62] = -z[1] + z[11];
z[62] = z[10] * z[62];
z[61] = -z[61] + z[62] / T(2);
z[61] = z[10] * z[61];
z[62] = prod_pow(z[29], 3);
z[63] = z[62] / T(3);
z[61] = (T(-49) * z[34]) / T(12) + -z[51] + -z[56] + z[61] + z[63];
z[61] = z[5] * z[61];
z[64] = z[4] + z[6];
z[64] = z[47] * z[64];
z[65] = z[1] + -z[7];
z[50] = z[50] * z[65];
z[50] = -z[49] + z[50] + T(-2) * z[55] + z[64];
z[50] = z[0] * z[50];
z[55] = -z[4] + z[6];
z[64] = z[55] * z[57];
z[66] = z[43] * z[65];
z[50] = z[50] + z[64] + z[66];
z[50] = z[0] * z[50];
z[43] = z[37] * z[43];
z[59] = -(z[10] * z[59]);
z[64] = z[5] * z[36];
z[38] = z[38] + z[64];
z[38] = z[19] * z[38];
z[38] = z[38] + z[43] + z[59];
z[38] = z[19] * z[38];
z[41] = z[41] + z[63];
z[43] = prod_pow(z[10], 2);
z[36] = z[36] * z[43];
z[35] = z[35] + T(2) * z[54];
z[35] = z[19] * z[35];
z[35] = z[33] + (T(41) * z[34]) / T(8) + z[35] + z[36] + T(2) * z[41] + (T(-4) * z[42]) / T(3);
z[35] = z[13] * z[35];
z[36] = z[15] * z[57];
z[36] = z[36] + -z[56];
z[36] = z[4] * z[36];
z[41] = -z[7] + T(2) * z[8] + -z[46];
z[41] = z[41] * z[53];
z[42] = -(z[10] * z[47]);
z[42] = z[42] + -z[52];
z[42] = z[10] * z[42];
z[41] = (T(-32) * z[34]) / T(3) + z[41] + z[42];
z[41] = z[6] * z[41];
z[42] = -(z[26] * z[60]);
z[46] = z[49] * z[53];
z[43] = z[43] * z[44];
z[44] = (T(35) * z[34]) / T(24) + z[51] + z[63];
z[44] = z[18] * z[44];
z[47] = z[21] * z[58];
z[48] = -(z[48] * z[55]);
z[49] = -(z[5] * z[65]);
z[48] = z[48] + z[49];
z[48] = z[20] * z[48];
z[37] = -(z[28] * z[37] * z[67]);
z[49] = z[25] * z[62];
return z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + -z[43] / T(2) + z[44] + z[45] + z[46] + z[47] + z[48] + (T(-2) * z[49]) / T(3) + z[50] + z[61];
}



template IntegrandConstructorType<double> f_4_138_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_138_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_138_construct (const Kin<qd_real>&);
#endif

}