#include "f_4_179.h"

namespace PentagonFunctions {

template <typename T> T f_4_179_abbreviated (const std::array<T,18>&);

template <typename T> class SpDLog_f_4_179_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_179_W_22 (const Kin<T>& kin) {
        c[0] = (T(3) / T(2) + (T(9) * kin.v[2]) / T(8) + (T(-3) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(4)) * kin.v[2] + (T(3) / T(2) + (T(-3) * kin.v[0]) / T(2) + (T(-3) * kin.v[1]) / T(8) + (T(3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[1] + (T(-3) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(-3) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[0];
c[1] = kin.v[2] * (T(3) / T(2) + (T(-21) * kin.v[3]) / T(2) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(3) / T(2) + (T(-3) * kin.v[0]) / T(2) + (T(9) * kin.v[1]) / T(2) + (T(21) * kin.v[2]) / T(2) + (T(-21) * kin.v[4]) / T(2) + T(-9) * kin.v[3]) + ((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[4] * (T(-3) / T(2) + T(6) * kin.v[4]) + (T(-3) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(21) * kin.v[4]) / T(2)) * kin.v[3] + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[0] + (T(3) / T(2) + (T(-3) * kin.v[0]) / T(2) + (T(-3) * kin.v[1]) / T(8) + (T(3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[1] + (T(3) / T(2) + (T(9) * kin.v[2]) / T(8) + (T(-3) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(4)) * kin.v[2] + (T(-3) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(-3) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(21) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(-21) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + T(9) * kin.v[0]) * kin.v[1] + ((T(-15) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(21) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[0] * (T(9) * kin.v[2] + T(-9) * kin.v[3] + T(-9) * kin.v[4])) + rlog(kin.v[3]) * (((T(-21) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[0]) * kin.v[1] + ((T(15) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-21) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * (T(-9) * kin.v[2] + T(9) * kin.v[3] + T(9) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(-21) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[0]) * kin.v[1] + ((T(15) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-21) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * (T(-9) * kin.v[2] + T(9) * kin.v[3] + T(9) * kin.v[4]));
c[2] = (T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2);
c[3] = rlog(kin.v[3]) * (T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * rlog(-kin.v[3] + kin.v[0] + kin.v[1]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (abb[4] * (prod_pow(abb[2], 2) * T(3) + abb[3] * T(3)) + prod_pow(abb[1], 2) * ((abb[5] * T(-3)) / T(2) + (abb[8] * T(3)) / T(2) + abb[4] * T(-3) + abb[7] * T(-3) + abb[6] * T(3)) + prod_pow(abb[2], 2) * ((abb[8] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2) + abb[6] * T(-3) + abb[7] * T(3)) + abb[3] * ((abb[5] * T(-3)) / T(2) + (abb[8] * T(3)) / T(2) + abb[6] * T(-3) + abb[7] * T(3)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_179_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl16 = DLog_W_16<T>(kin),dl9 = DLog_W_9<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),spdl22 = SpDLog_f_4_179_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,18> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), -rlog(t), dl16(t), rlog(kin.W[8] / kin_path.W[8]), dl9(t), rlog(v_path[3]), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), dl20(t), dl17(t)}
;

        auto result = f_4_179_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_179_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[38];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[9];
z[3] = abb[11];
z[4] = abb[17];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[10];
z[10] = abb[2];
z[11] = abb[15];
z[12] = abb[16];
z[13] = abb[12];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[13];
z[17] = abb[14];
z[18] = bc<TR>[2];
z[19] = bc<TR>[9];
z[20] = T(2) * z[10];
z[21] = z[3] + -z[4];
z[20] = z[20] * z[21];
z[22] = -z[2] + z[21];
z[22] = z[0] * z[22];
z[22] = -z[20] + z[22];
z[22] = z[0] * z[22];
z[23] = z[13] * z[21];
z[20] = -z[20] + z[23];
z[20] = z[13] * z[20];
z[23] = z[11] + z[12];
z[24] = T(2) * z[23];
z[25] = -z[2] + T(2) * z[4] + -z[24];
z[25] = z[15] * z[25];
z[26] = z[16] * z[21];
z[27] = prod_pow(z[10], 2);
z[28] = z[2] * z[27];
z[20] = -z[20] + z[22] + -z[25] + T(-2) * z[26] + z[28];
z[22] = -(z[1] * z[20]);
z[25] = z[5] + -z[8];
z[26] = z[2] * z[25];
z[28] = z[26] / T(2);
z[25] = z[12] * z[25];
z[29] = z[6] * z[11];
z[30] = z[2] + -z[11];
z[30] = z[9] * z[30];
z[29] = -z[25] + -z[28] + z[29] + z[30];
z[27] = z[27] * z[29];
z[29] = -z[4] + z[23];
z[30] = z[6] * z[29];
z[29] = z[2] + -z[29];
z[29] = z[9] * z[29];
z[28] = -z[28] + z[29] + T(3) * z[30];
z[28] = z[15] * z[28];
z[29] = z[6] * z[21];
z[29] = z[26] + z[29];
z[30] = z[3] / T(2);
z[31] = z[4] / T(2);
z[32] = z[30] + -z[31];
z[33] = -z[2] + -z[32];
z[33] = z[9] * z[33];
z[29] = (T(3) * z[29]) / T(2) + z[33];
z[29] = z[0] * z[29];
z[33] = -z[3] + T(3) * z[4] + -z[24];
z[33] = z[6] * z[33];
z[24] = -z[3] + -z[4] + z[24];
z[24] = z[9] * z[24];
z[24] = z[24] + z[33];
z[33] = z[10] * z[24];
z[29] = z[29] + z[33];
z[29] = z[0] * z[29];
z[34] = (T(3) * z[4]) / T(2);
z[35] = -z[11] + z[34];
z[36] = (T(3) * z[3]) / T(2);
z[37] = z[35] + -z[36];
z[37] = z[6] * z[37];
z[32] = z[11] + z[32];
z[32] = z[9] * z[32];
z[32] = z[32] + z[37];
z[32] = z[13] * z[32];
z[32] = z[32] + -z[33];
z[32] = z[13] * z[32];
z[33] = z[16] * z[24];
z[37] = (T(7) * z[3]) / T(2) + (T(-25) * z[4]) / T(2) + T(9) * z[23];
z[37] = z[19] * z[37];
z[22] = z[22] + z[27] + z[28] + z[29] + z[32] + z[33] + z[37] / T(4);
z[25] = z[25] + -z[26];
z[26] = T(2) * z[3];
z[27] = T(-2) * z[11] + (T(3) * z[12]) / T(2) + -z[26] + z[31];
z[27] = z[9] * z[27];
z[28] = (T(5) * z[4]) / T(2) + -z[23] + -z[36];
z[28] = z[18] * z[28];
z[26] = -z[4] + -z[23] + z[26];
z[29] = -(z[1] * z[26]);
z[32] = T(4) * z[3] + (T(-5) * z[12]) / T(2) + -z[35];
z[32] = z[6] * z[32];
z[25] = z[25] / T(4) + z[27] + (T(3) * z[28]) / T(2) + z[29] + z[32];
z[27] = prod_pow(z[14], 2);
z[25] = z[25] * z[27];
z[26] = -(z[26] * z[27]);
z[20] = T(-3) * z[20] + z[26];
z[20] = z[7] * z[20];
z[26] = z[6] + -z[9];
z[28] = z[3] + -z[12];
z[26] = z[13] * z[26] * z[28];
z[28] = -z[10] + z[17];
z[21] = -(z[21] * z[28]);
z[29] = z[1] * z[21];
z[26] = z[26] + z[29];
z[29] = -z[23] + z[30] + z[31];
z[29] = prod_pow(z[18], 2) * z[29];
z[24] = -(z[24] * z[28]);
z[24] = z[24] + T(2) * z[26] + z[29];
z[23] = (T(5) * z[3]) / T(2) + -z[23] + -z[34];
z[23] = z[23] * z[27];
z[21] = z[7] * z[21];
z[21] = T(6) * z[21] + z[23] / T(2) + T(3) * z[24];
z[21] = int_to_imaginary<T>(1) * z[14] * z[21];
return z[20] + z[21] + T(3) * z[22] + z[25];
}



template IntegrandConstructorType<double> f_4_179_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_179_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_179_construct (const Kin<qd_real>&);
#endif

}