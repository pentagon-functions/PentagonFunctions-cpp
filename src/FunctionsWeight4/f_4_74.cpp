#include "f_4_74.h"

namespace PentagonFunctions {

template <typename T> T f_4_74_abbreviated (const std::array<T,16>&);

template <typename T> class SpDLog_f_4_74_W_10 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_74_W_10 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(2) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(2) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[1] * (T(4) + T(-2) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(2) + prod_pow(abb[2], 2) * (abb[5] * T(-2) + abb[4] * T(2)) + prod_pow(abb[1], 2) * (abb[3] * T(-2) + abb[4] * T(-2) + abb[5] * T(2)));
    }
};
template <typename T> class SpDLog_f_4_74_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_74_W_23 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[6] * (prod_pow(abb[2], 2) * abb[3] * T(2) + prod_pow(abb[2], 2) * (abb[5] * T(-2) + abb[4] * T(2)) + prod_pow(abb[7], 2) * (abb[3] * T(-2) + abb[4] * T(-2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_74_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl23 = DLog_W_23<T>(kin),dl17 = DLog_W_17<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),spdl10 = SpDLog_f_4_74_W_10<T>(kin),spdl23 = SpDLog_f_4_74_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,16> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), dl17(t), -rlog(t), -rlog(t), dl18(t), f_2_1_7(kin_path), f_2_1_8(kin_path), dl5(t), dl16(t)}
;

        auto result = f_4_74_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_74_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[25];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[11];
z[3] = abb[14];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[9];
z[7] = abb[7];
z[8] = abb[15];
z[9] = abb[2];
z[10] = abb[8];
z[11] = abb[10];
z[12] = bc<TR>[0];
z[13] = abb[12];
z[14] = abb[13];
z[15] = bc<TR>[9];
z[16] = z[3] + z[8];
z[17] = -z[2] + z[16];
z[18] = z[1] + z[4];
z[19] = -z[5] + z[18];
z[17] = z[17] * z[19];
z[20] = z[0] * z[17];
z[21] = z[2] * z[19];
z[22] = -z[4] + z[5] + -z[11];
z[22] = z[8] * z[22];
z[21] = z[21] + z[22];
z[21] = z[7] * z[21];
z[20] = T(2) * z[20] + z[21];
z[20] = z[7] * z[20];
z[21] = prod_pow(z[12], 2);
z[22] = z[21] / T(3);
z[23] = prod_pow(z[0], 2);
z[24] = z[22] + z[23];
z[24] = z[19] * z[24];
z[24] = T(-2) * z[15] + z[24];
z[24] = z[2] * z[24];
z[16] = -(z[16] * z[19]);
z[18] = z[6] + z[11] + -z[18];
z[18] = z[10] * z[18];
z[16] = z[16] + z[18];
z[16] = prod_pow(z[9], 2) * z[16];
z[16] = z[16] + z[20] + z[24];
z[19] = T(2) * z[5];
z[20] = T(-2) * z[1] + (T(-5) * z[4]) / T(2) + z[6] / T(2) + z[19];
z[20] = z[20] * z[22];
z[24] = -z[1] + z[5] + -z[6];
z[23] = z[23] * z[24];
z[24] = T(4) * z[15];
z[20] = z[20] + T(2) * z[23] + z[24];
z[20] = z[3] * z[20];
z[17] = T(4) * z[17];
z[23] = -z[13] + -z[14];
z[17] = z[17] * z[23];
z[19] = (T(-5) * z[1]) / T(2) + T(-2) * z[4] + z[11] / T(2) + z[19];
z[19] = z[19] * z[22];
z[19] = z[19] + z[24];
z[19] = z[8] * z[19];
z[18] = z[18] * z[21];
return T(2) * z[16] + z[17] + -z[18] / T(6) + z[19] + z[20];
}



template IntegrandConstructorType<double> f_4_74_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_74_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_74_construct (const Kin<qd_real>&);
#endif

}