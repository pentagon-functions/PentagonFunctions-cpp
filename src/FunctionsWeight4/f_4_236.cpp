#include "f_4_236.h"

namespace PentagonFunctions {

template <typename T> T f_4_236_abbreviated (const std::array<T,19>&);



template <typename T> IntegrandConstructorType<T> f_4_236_construct (const Kin<T>& kin) {
    return [&kin, 
            dl24 = DLog_W_24<T>(kin),dl25 = DLog_W_25<T>(kin),dl1 = DLog_W_1<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl6 = DLog_W_6<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,19> abbr = 
            {dl24(t), rlog(kin.W[17] / kin_path.W[17]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[16] / kin_path.W[16]), dl25(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl1(t), rlog(kin.W[5] / kin_path.W[5]), dl18(t), dl17(t), dl19(t), dl6(t)}
;

        auto result = f_4_236_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_236_abbreviated(const std::array<T,19>& abb)
{
using TR = typename T::value_type;
T z[48];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[13];
z[11] = abb[15];
z[12] = abb[17];
z[13] = abb[16];
z[14] = abb[10];
z[15] = abb[9];
z[16] = abb[11];
z[17] = abb[12];
z[18] = abb[14];
z[19] = bc<TR>[1];
z[20] = bc<TR>[2];
z[21] = bc<TR>[4];
z[22] = bc<TR>[9];
z[23] = abb[18];
z[24] = T(2) * z[12];
z[25] = T(2) * z[13];
z[26] = z[24] + z[25];
z[27] = -z[10] + T(-4) * z[23] + z[26];
z[28] = z[11] + z[27] / T(3);
z[28] = z[19] * z[28];
z[29] = T(2) * z[18];
z[30] = z[7] + z[9] + -z[29];
z[31] = T(2) * z[30];
z[32] = -z[8] + z[31];
z[33] = z[1] + z[32] / T(3);
z[33] = z[10] * z[33];
z[28] = z[28] + z[33];
z[33] = -z[7] + z[9];
z[34] = z[1] + -z[8];
z[35] = z[33] + z[34];
z[35] = z[15] * z[35];
z[36] = z[33] + -z[34];
z[36] = z[0] * z[36];
z[37] = z[8] / T(2);
z[38] = (T(-5) * z[1]) / T(6) + -z[30] / T(3) + z[37];
z[38] = z[13] * z[38];
z[39] = z[1] + z[8];
z[40] = T(3) * z[9];
z[41] = (T(-7) * z[7]) / T(3) + -z[40];
z[41] = (T(8) * z[18]) / T(3) + (T(-2) * z[39]) / T(3) + z[41] / T(2);
z[41] = z[11] * z[41];
z[42] = -z[1] + T(2) * z[8] + z[30];
z[42] = z[12] * z[42];
z[43] = -z[10] + z[11];
z[44] = z[20] * z[43];
z[28] = T(2) * z[28] + (T(-5) * z[35]) / T(6) + (T(2) * z[36]) / T(3) + z[38] + z[41] + z[42] / T(3) + (T(-3) * z[44]) / T(2);
z[38] = prod_pow(z[4], 2);
z[28] = z[28] * z[38];
z[40] = z[7] + z[40];
z[41] = -z[1] + -z[8] + T(4) * z[18];
z[42] = -z[40] + z[41];
z[42] = z[11] * z[42];
z[44] = -z[10] + z[13];
z[32] = T(3) * z[1] + z[32];
z[44] = z[32] * z[44];
z[45] = z[8] + z[30];
z[24] = z[24] * z[45];
z[24] = z[24] + -z[35] + z[42] + -z[44];
z[42] = z[17] * z[24];
z[44] = z[10] + -z[12];
z[44] = z[32] * z[44];
z[46] = z[25] * z[45];
z[41] = T(3) * z[7] + z[9] + -z[41];
z[41] = z[11] * z[41];
z[41] = -z[36] + z[41] + -z[44] + -z[46];
z[44] = -(z[6] * z[41]);
z[46] = z[12] + z[13];
z[47] = z[34] * z[46];
z[47] = -z[35] + z[36] + z[47];
z[47] = z[3] * z[47];
z[31] = z[31] + z[39];
z[39] = z[11] * z[31];
z[26] = z[26] * z[34];
z[32] = z[10] * z[32];
z[26] = -z[26] + z[32] + -z[39];
z[32] = -z[2] + -z[14];
z[32] = z[26] * z[32];
z[27] = T(3) * z[11] + z[27];
z[39] = -z[19] + T(2) * z[20];
z[39] = z[19] * z[39];
z[39] = T(-2) * z[21] + z[39];
z[27] = z[27] * z[39];
z[39] = z[11] + T(-2) * z[23] + z[46];
z[38] = z[38] * z[39];
z[39] = -(prod_pow(z[20], 2) * z[43]);
z[27] = z[27] + z[32] + z[38] + z[39] + z[42] + z[44] + z[47];
z[27] = int_to_imaginary<T>(1) * z[4] * z[27];
z[32] = z[1] / T(2) + z[37];
z[29] = -z[29] + z[32] + z[40] / T(2);
z[29] = z[11] * z[29];
z[37] = (T(3) * z[1]) / T(2) + z[30] + -z[37];
z[37] = z[10] * z[37];
z[29] = z[29] + -z[37];
z[32] = z[30] + z[32];
z[32] = z[12] * z[32];
z[30] = z[1] + z[30];
z[37] = z[13] * z[30];
z[37] = z[29] + -z[32] + z[35] + -z[36] / T(2) + z[37];
z[37] = z[3] * z[37];
z[26] = z[14] * z[26];
z[37] = z[26] + z[37];
z[37] = z[3] * z[37];
z[31] = z[12] * z[31];
z[25] = -(z[25] * z[30]);
z[30] = z[11] * z[33];
z[25] = z[25] + -z[30] + z[31] + -z[36];
z[25] = z[3] * z[25];
z[31] = z[13] * z[45];
z[29] = -z[29] + z[31] + -z[32] + (T(3) * z[36]) / T(2);
z[29] = z[2] * z[29];
z[25] = z[25] + -z[26] + z[29];
z[25] = z[2] * z[25];
z[24] = -(z[16] * z[24]);
z[26] = -(z[5] * z[41]);
z[29] = z[13] * z[34];
z[29] = z[29] + z[30] + -z[35];
z[29] = prod_pow(z[14], 2) * z[29];
z[30] = (T(-65) * z[10]) / T(4) + T(28) * z[23] + T(-14) * z[46];
z[30] = (T(3) * z[11]) / T(4) + z[30] / T(3);
z[30] = z[22] * z[30];
return z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[37];
}



template IntegrandConstructorType<double> f_4_236_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_236_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_236_construct (const Kin<qd_real>&);
#endif

}