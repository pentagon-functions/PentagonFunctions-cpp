#include "f_4_140.h"

namespace PentagonFunctions {

template <typename T> T f_4_140_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_140_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_140_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(-6) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[1] + T(8) * kin.v[3] + T(4) * kin.v[4]) + kin.v[0] * ((T(-8) * kin.v[1]) / T(3) + (T(-4) * kin.v[2]) / T(3) + (T(20) * kin.v[3]) / T(3) + (T(-2) / T(3) + bc<T>[1] * T(-2)) * kin.v[0] + T(4) * kin.v[4] + bc<T>[1] * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + bc<T>[1] * (kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(4) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[3] * (T(-6) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * ((T(-2) * kin.v[0]) / T(3) + (T(-8) * kin.v[1]) / T(3) + (T(-4) * kin.v[2]) / T(3) + (T(20) * kin.v[3]) / T(3) + T(4) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[1] + T(8) * kin.v[3] + T(4) * kin.v[4]) + rlog(kin.v[2]) * (kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-2) * kin.v[3]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + kin.v[3] + kin.v[4]) + kin.v[0] * ((T(2) * kin.v[1]) / T(3) + kin.v[2] / T(3) + (T(-5) * kin.v[3]) / T(3) + -kin.v[4] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[1] * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-2) * kin.v[3]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + kin.v[3] + kin.v[4]) + kin.v[0] * ((T(2) * kin.v[1]) / T(3) + kin.v[2] / T(3) + (T(-5) * kin.v[3]) / T(3) + -kin.v[4] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[1] * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-2) * kin.v[3]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + kin.v[3] + kin.v[4]) + kin.v[0] * ((T(2) * kin.v[1]) / T(3) + kin.v[2] / T(3) + (T(-5) * kin.v[3]) / T(3) + -kin.v[4] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[1] * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-2) * kin.v[3]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + kin.v[3] + kin.v[4]) + kin.v[0] * ((T(2) * kin.v[1]) / T(3) + kin.v[2] / T(3) + (T(-5) * kin.v[3]) / T(3) + -kin.v[4] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[1] * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-3) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[0] * (-kin.v[0] / T(3) + (T(-4) * kin.v[1]) / T(3) + (T(-2) * kin.v[2]) / T(3) + (T(10) * kin.v[3]) / T(3) + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(4) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]))) + bc<T>[1] * (kin.v[3] * (T(-6) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * ((T(-2) * kin.v[0]) / T(3) + (T(-8) * kin.v[1]) / T(3) + (T(-4) * kin.v[2]) / T(3) + (T(20) * kin.v[3]) / T(3) + T(4) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[1] + T(8) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]))) + rlog(kin.v[0]) * (kin.v[1] * (kin.v[1] + T(-4) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (T(3) * kin.v[3] + T(2) * kin.v[4]) + kin.v[0] * ((T(4) * kin.v[1]) / T(3) + (T(2) * kin.v[2]) / T(3) + (T(-10) * kin.v[3]) / T(3) + (bc<T>[1] + T(1) / T(3)) * kin.v[0] + T(-2) * kin.v[4] + bc<T>[1] * (T(-2) + T(-2) * kin.v[2] + T(2) * kin.v[4])) + bc<T>[1] * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[1]) * (kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(6) * kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * (-kin.v[2] + ((bc<T>[1] * T(-3)) / T(2) + T(-1) / T(2)) * kin.v[0] + T(-2) * kin.v[1] + T(5) * kin.v[3] + T(3) * kin.v[4] + bc<T>[1] * (T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4])) + bc<T>[1] * (kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(6) * kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * (-kin.v[2] + ((bc<T>[1] * T(-3)) / T(2) + T(-1) / T(2)) * kin.v[0] + T(-2) * kin.v[1] + T(5) * kin.v[3] + T(3) * kin.v[4] + bc<T>[1] * (T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4])) + bc<T>[1] * (kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + bc<T>[1] * T(4) + T(8)) * kin.v[0] + T(8) * kin.v[1] + T(-8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + bc<T>[1] * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + T(-8) * kin.v[3];
c[3] = rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[1] * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[1] * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3])) + bc<T>[1] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3]) + bc<T>[1] * (T(2) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3])) + rlog(kin.v[2]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[1] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[1] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[1] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[1] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(kin.v[0]) * ((T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[1] * (T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[16] * (abb[13] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[1] * bc<T>[0] * int_to_imaginary<T>(3) + abb[9] * bc<T>[0] * int_to_imaginary<T>(3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[13] * (-abb[7] + -abb[8] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[1] * T(3) + abb[9] * T(3))) + abb[17] * (abb[13] * (abb[13] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) + abb[3] * abb[13] * T(2) + abb[4] * abb[13] * T(2) + abb[18] * T(4)) + abb[3] * abb[13] * (abb[7] + abb[8] + abb[10] + abb[11] + abb[1] * T(-3) + abb[9] * T(-3) + abb[19] * T(4)) + abb[4] * abb[13] * (abb[7] + abb[8] + abb[10] + abb[11] + abb[1] * T(-3) + abb[9] * T(-3) + abb[19] * T(4)) + abb[2] * (abb[13] * abb[17] * T(-2) + abb[13] * (-abb[7] + -abb[8] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[1] * T(3) + abb[9] * T(3)) + abb[15] * (abb[7] + abb[8] + abb[10] + abb[11] + abb[1] * T(-3) + abb[9] * T(-3) + abb[17] * T(2) + abb[19] * T(4))) + abb[18] * (abb[1] * T(-6) + abb[9] * T(-6) + abb[7] * T(2) + abb[8] * T(2) + abb[10] * T(2) + abb[11] * T(2) + abb[19] * T(8)) + abb[15] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[17] * (abb[3] * T(-2) + abb[4] * T(-2) + abb[13] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2)) + abb[1] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(4) + abb[3] * (-abb[7] + -abb[8] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[1] * T(3) + abb[9] * T(3)) + abb[4] * (-abb[7] + -abb[8] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[1] * T(3) + abb[9] * T(3)) + abb[13] * (-abb[7] + -abb[8] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[1] * T(3) + abb[9] * T(3)) + abb[15] * (abb[1] * T(-6) + abb[9] * T(-6) + abb[7] * T(2) + abb[8] * T(2) + abb[10] * T(2) + abb[11] * T(2) + abb[17] * T(4) + abb[19] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_140_construct (const Kin<T>& kin) {
    return [&kin, 
            dl4 = DLog_W_4<T>(kin),dl8 = DLog_W_8<T>(kin),dl25 = DLog_W_25<T>(kin),dl21 = DLog_W_21<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl26 = DLog_W_26<T>(kin),dl1 = DLog_W_1<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl2 = DLog_W_2<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),spdl21 = SpDLog_f_4_140_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,42> abbr = 
            {dl4(t), rlog(kin.W[15] / kin_path.W[15]), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), f_2_1_9(kin_path), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[19] / kin_path.W[19]), dl8(t), rlog(-v_path[2] + v_path[0] + v_path[4]), dl25(t), rlog(-v_path[1] + v_path[3] + v_path[4]), dl21(t), rlog(kin.W[0] / kin_path.W[0]), f_2_1_15(kin_path), -rlog(t), dl5(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl17(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl26(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl1(t), dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl2(t), dl3(t), dl20(t), dl16(t), dl18(t), dl19(t)}
;

        auto result = f_4_140_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_4_140_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[130];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[20];
z[14] = abb[23];
z[15] = abb[31];
z[16] = abb[38];
z[17] = abb[41];
z[18] = abb[13];
z[19] = abb[37];
z[20] = abb[40];
z[21] = abb[15];
z[22] = abb[12];
z[23] = abb[39];
z[24] = abb[36];
z[25] = abb[18];
z[26] = abb[21];
z[27] = abb[22];
z[28] = abb[24];
z[29] = abb[25];
z[30] = abb[17];
z[31] = abb[19];
z[32] = abb[26];
z[33] = abb[27];
z[34] = abb[28];
z[35] = abb[29];
z[36] = abb[30];
z[37] = abb[33];
z[38] = abb[35];
z[39] = abb[32];
z[40] = abb[34];
z[41] = abb[14];
z[42] = bc<TR>[1];
z[43] = bc<TR>[3];
z[44] = bc<TR>[5];
z[45] = bc<TR>[2];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = bc<TR>[9];
z[50] = z[30] / T(3);
z[51] = T(3) * z[10];
z[52] = -z[50] + z[51];
z[53] = z[9] + z[11];
z[54] = z[31] / T(3) + (T(7) * z[42]) / T(6);
z[55] = z[1] / T(2);
z[52] = (T(-4) * z[8]) / T(3) + (T(-5) * z[12]) / T(6) + z[52] / T(2) + (T(2) * z[53]) / T(3) + -z[54] + -z[55];
z[52] = z[16] * z[52];
z[56] = z[8] + z[12];
z[57] = (T(3) * z[1]) / T(2);
z[50] = z[10] + z[50];
z[50] = (T(5) * z[9]) / T(6) + (T(4) * z[11]) / T(3) + z[50] / T(2) + z[54] + (T(-2) * z[56]) / T(3) + -z[57];
z[50] = z[17] * z[50];
z[54] = -z[30] + z[55];
z[58] = (T(-3) * z[8]) / T(2) + (T(5) * z[10]) / T(2);
z[59] = z[11] / T(2);
z[60] = z[54] + z[58] + z[59];
z[61] = z[42] / T(2);
z[62] = (T(19) * z[9]) / T(6) + (T(-13) * z[12]) / T(6) + -z[60] + -z[61];
z[62] = z[31] + z[62] / T(2);
z[62] = z[20] * z[62];
z[63] = z[12] / T(2);
z[64] = z[8] / T(2);
z[65] = z[63] + z[64];
z[66] = z[10] / T(2);
z[67] = (T(3) * z[9]) / T(2) + -z[66];
z[68] = (T(-5) * z[1]) / T(2) + (T(3) * z[11]) / T(2);
z[69] = -z[65] + z[67] + z[68];
z[70] = z[30] + z[69];
z[71] = -z[61] + z[70];
z[71] = z[31] + z[71] / T(2);
z[71] = z[23] * z[71];
z[72] = z[1] + -z[11];
z[73] = z[8] + -z[10];
z[74] = z[72] + z[73];
z[75] = z[9] + -z[12];
z[76] = z[74] + z[75];
z[77] = z[0] * z[76];
z[78] = -z[33] + -z[34] + z[35] + z[36];
z[79] = z[39] * z[78];
z[80] = z[77] + -z[79];
z[81] = z[37] * z[78];
z[82] = z[81] / T(2);
z[83] = z[38] * z[78];
z[84] = z[82] + z[83] / T(2);
z[85] = z[72] + -z[75];
z[85] = z[22] * z[85];
z[86] = T(3) * z[85];
z[87] = z[73] + -z[75];
z[88] = T(3) * z[41];
z[87] = z[87] * z[88];
z[88] = z[9] / T(2);
z[89] = z[59] + z[88];
z[90] = (T(3) * z[12]) / T(2) + -z[89];
z[58] = z[58] + -z[90];
z[54] = z[54] + z[58];
z[91] = z[54] + z[61];
z[91] = -z[31] + z[91] / T(2);
z[91] = z[24] * z[91];
z[64] = z[64] + z[66];
z[66] = -z[30] + z[64] + -z[68];
z[61] = (T(13) * z[9]) / T(6) + (T(-19) * z[12]) / T(6) + z[61] + z[66];
z[61] = -z[31] + z[61] / T(2);
z[61] = z[19] * z[61];
z[68] = -z[74] + (T(11) * z[75]) / T(3);
z[92] = z[15] / T(4);
z[68] = z[68] * z[92];
z[93] = z[20] + -z[24];
z[94] = z[19] + -z[23];
z[95] = -z[93] + z[94];
z[96] = -z[37] + z[38] + -z[39] + z[40];
z[97] = z[95] + (T(11) * z[96]) / T(90);
z[98] = -z[16] + z[17];
z[97] = z[97] / T(2) + -z[98];
z[99] = int_to_imaginary<T>(1) * z[7];
z[97] = z[97] * z[99];
z[100] = z[40] * z[78];
z[101] = z[100] / T(4);
z[50] = z[50] + z[52] + z[61] + z[62] + z[68] + z[71] + z[80] / T(4) + z[84] + z[86] + z[87] + z[91] + z[97] + z[101];
z[50] = z[7] * z[50];
z[52] = (T(3) * z[10]) / T(2);
z[61] = z[52] + z[57];
z[62] = z[61] + -z[65] + -z[89];
z[68] = z[30] + T(2) * z[31];
z[71] = -z[62] + z[68];
z[80] = z[20] * z[71];
z[89] = z[16] * z[71];
z[91] = z[80] + z[89];
z[97] = -z[8] + T(2) * z[9] + -z[12];
z[102] = T(3) * z[1];
z[103] = T(2) * z[11] + z[68] + z[97] + -z[102];
z[104] = z[19] * z[103];
z[103] = z[17] * z[103];
z[105] = z[103] + z[104];
z[106] = z[15] * z[71];
z[69] = z[68] + z[69];
z[107] = T(3) * z[69];
z[108] = z[23] * z[107];
z[109] = z[79] + -z[83];
z[110] = z[100] + -z[109];
z[105] = z[91] + T(-2) * z[105] + -z[106] + z[108] + (T(3) * z[110]) / T(2);
z[105] = z[21] * z[105];
z[111] = z[19] * z[71];
z[71] = z[17] * z[71];
z[112] = z[71] + z[111];
z[55] = z[55] + -z[68];
z[58] = z[55] + z[58];
z[113] = z[24] * z[58];
z[82] = z[82] + z[113];
z[114] = -z[11] + z[68];
z[115] = -z[9] + T(2) * z[12] + z[114];
z[116] = T(-2) * z[8] + z[51] + -z[115];
z[117] = T(2) * z[116];
z[118] = z[16] + z[20];
z[117] = z[117] * z[118];
z[82] = T(-3) * z[82] + -z[106] + z[112] + z[117];
z[82] = z[18] * z[82];
z[106] = T(2) * z[75];
z[117] = z[20] * z[106];
z[106] = z[19] * z[106];
z[71] = z[71] + -z[89] + z[106] + z[117];
z[83] = z[81] + z[83];
z[72] = z[72] + -z[73];
z[73] = z[72] + -z[75];
z[89] = z[13] * z[73];
z[119] = z[83] + -z[89];
z[120] = z[9] + -z[12] + z[72];
z[120] = z[14] * z[120];
z[78] = z[32] * z[78];
z[78] = -z[78] + z[120];
z[120] = -z[78] + -z[119];
z[121] = z[15] * z[75];
z[120] = z[71] + (T(3) * z[120]) / T(2) + z[121];
z[120] = z[2] * z[120];
z[122] = T(3) * z[46];
z[123] = prod_pow(z[42], 2);
z[124] = z[122] + T(2) * z[123];
z[124] = z[98] * z[124];
z[125] = z[96] * z[123];
z[120] = z[82] + -z[105] + z[120] + z[124] + (T(3) * z[125]) / T(20);
z[120] = int_to_imaginary<T>(1) * z[120];
z[124] = z[43] * z[96];
z[50] = z[50] + z[120] + z[124];
z[50] = z[7] * z[50];
z[120] = z[78] + -z[109];
z[97] = z[97] + z[114];
z[114] = z[15] * z[97];
z[97] = z[17] * z[97];
z[91] = -z[91] + T(2) * z[97] + -z[111] + z[114] + (T(-3) * z[120]) / T(2);
z[91] = z[21] * z[91];
z[62] = -z[30] + z[62];
z[62] = -z[31] + z[62] / T(2);
z[111] = z[17] * z[62];
z[114] = z[16] * z[62];
z[111] = z[111] + -z[114];
z[120] = z[81] + z[120];
z[54] = -z[31] + z[54] / T(2);
z[54] = z[24] * z[54];
z[124] = -z[54] + -z[120] / T(4);
z[125] = (T(5) * z[9]) / T(2);
z[60] = T(3) * z[60] + -z[63] + -z[125];
z[63] = T(3) * z[31];
z[60] = z[60] / T(2) + -z[63];
z[60] = z[20] * z[60];
z[126] = z[19] * z[75];
z[127] = -z[87] + z[126];
z[128] = T(3) * z[72];
z[129] = -z[75] + z[128];
z[129] = z[92] * z[129];
z[60] = z[60] + -z[111] + T(3) * z[124] + z[127] + z[129];
z[60] = z[4] * z[60];
z[83] = z[77] + z[83];
z[124] = T(3) * z[74] + -z[75];
z[129] = z[15] / T(2);
z[124] = z[124] * z[129];
z[71] = z[71] + (T(-3) * z[83]) / T(2) + z[124];
z[83] = -(z[2] * z[71]);
z[124] = z[16] * z[116];
z[97] = z[97] + z[117] + z[121] + z[124] + -z[127];
z[117] = T(2) * z[99];
z[97] = z[97] * z[117];
z[60] = z[60] + -z[82] + z[83] + -z[91] + z[97];
z[60] = z[4] * z[60];
z[83] = z[20] * z[75];
z[97] = -z[83] + z[111];
z[70] = z[31] + z[70] / T(2);
z[111] = T(3) * z[23];
z[70] = z[70] * z[111];
z[85] = -z[85] + -z[101] + -z[119] / T(4);
z[101] = (T(5) * z[12]) / T(2);
z[66] = T(-3) * z[66] + z[88] + z[101];
z[63] = z[63] + z[66] / T(2);
z[63] = z[19] * z[63];
z[66] = -z[75] + -z[128];
z[66] = z[66] * z[92];
z[63] = z[63] + z[66] + -z[70] + T(3) * z[85] + -z[97];
z[63] = z[3] * z[63];
z[66] = z[79] + z[81];
z[79] = z[66] + -z[89];
z[85] = z[15] + T(2) * z[16];
z[88] = -z[8] + z[115];
z[85] = z[85] * z[88];
z[79] = (T(3) * z[79]) / T(2) + -z[80] + z[85] + -z[112];
z[79] = z[18] * z[79];
z[80] = -z[2] + z[4];
z[71] = z[71] * z[80];
z[80] = z[86] + z[103];
z[86] = -(z[16] * z[88]);
z[83] = z[80] + -z[83] + z[86] + z[106] + z[121];
z[83] = z[83] * z[117];
z[63] = z[63] + z[71] + z[79] + z[83] + z[105];
z[63] = z[3] * z[63];
z[71] = z[74] + -z[75];
z[83] = z[15] * z[71];
z[83] = -z[77] + z[83];
z[86] = -(z[20] * z[71]);
z[88] = z[19] * z[76];
z[72] = z[72] + z[75];
z[92] = -(z[17] * z[72]);
z[66] = -z[66] + z[78] + z[83] + z[86] + z[88] + z[92];
z[66] = z[6] * z[66];
z[76] = z[20] * z[76];
z[71] = -(z[19] * z[71]);
z[86] = z[16] * z[73];
z[71] = z[71] + z[76] + z[83] + z[86] + -z[89] + z[109];
z[71] = z[5] * z[71];
z[66] = z[66] + z[71];
z[71] = z[19] + z[20];
z[62] = z[62] * z[71];
z[65] = z[59] + z[65];
z[71] = T(2) * z[30] + T(4) * z[31];
z[61] = -z[61] + -z[65] + z[71] + z[125];
z[61] = z[15] * z[61];
z[76] = -z[81] + z[110];
z[54] = z[54] + -z[76] / T(4);
z[52] = (T(9) * z[1]) / T(2) + -z[52];
z[59] = (T(23) * z[9]) / T(2) + T(5) * z[30] + -z[52] + (T(-13) * z[56]) / T(2) + -z[59];
z[59] = T(5) * z[31] + z[59] / T(2);
z[59] = z[17] * z[59];
z[54] = T(3) * z[54] + z[59] + z[61] + z[62] + -z[70] + z[87] + z[114];
z[54] = z[21] * z[54];
z[54] = z[54] + z[82];
z[54] = z[21] * z[54];
z[51] = -z[51] + z[53] + z[56] + z[71] + -z[102];
z[51] = z[15] * z[51];
z[52] = (T(-5) * z[8]) / T(2) + -z[52] + (T(7) * z[53]) / T(2) + z[68] + -z[101];
z[53] = z[17] + z[19];
z[52] = z[52] * z[53];
z[53] = (T(-9) * z[10]) / T(2) + (T(-5) * z[11]) / T(2) + (T(7) * z[56]) / T(2) + z[57] + z[68] + -z[125];
z[53] = z[53] * z[118];
z[56] = -z[76] / T(2) + z[113];
z[51] = z[51] + z[52] + z[53] + T(3) * z[56] + -z[108];
z[51] = z[25] * z[51];
z[52] = z[78] + -z[89];
z[52] = z[52] / T(2) + -z[77] + -z[84];
z[53] = (T(3) * z[74]) / T(2) + -z[75];
z[53] = z[15] * z[53];
z[52] = (T(3) * z[52]) / T(2) + z[53] + -z[97] + z[126];
z[52] = z[2] * z[52];
z[52] = z[52] + -z[79] + z[91];
z[52] = z[2] * z[52];
z[53] = z[55] + z[64] + -z[90];
z[56] = -(z[16] * z[53]);
z[57] = z[100] + z[119];
z[59] = -z[17] + -z[94];
z[59] = z[59] * z[69];
z[61] = z[73] * z[129];
z[56] = z[56] + z[57] / T(2) + z[59] + z[61];
z[56] = z[27] * z[56];
z[58] = z[58] * z[118];
z[59] = z[72] * z[129];
z[58] = z[58] + z[59] + -z[113] + -z[120] / T(2);
z[55] = z[55] + z[65] + -z[67];
z[59] = z[17] * z[55];
z[59] = -z[58] + z[59];
z[59] = z[29] * z[59];
z[56] = z[56] + z[59];
z[56] = T(3) * z[56];
z[56] = z[56] * z[99];
z[59] = T(3) * z[28];
z[58] = z[58] * z[59];
z[61] = -(z[20] * z[116]);
z[61] = z[61] + z[80] + -z[85] + z[104];
z[61] = prod_pow(z[18], 2) * z[61];
z[53] = z[26] * z[53];
z[62] = z[122] + (T(2) * z[123]) / T(3);
z[62] = z[42] * z[62];
z[64] = T(6) * z[47] + (T(3) * z[48]) / T(2);
z[62] = (T(149) * z[49]) / T(12) + z[62] + z[64];
z[53] = T(3) * z[53] + -z[62];
z[53] = z[16] * z[53];
z[65] = z[26] * z[107];
z[55] = -(z[55] * z[59]);
z[55] = z[55] + z[62] + z[65];
z[55] = z[17] * z[55];
z[59] = z[122] + z[123] / T(2);
z[59] = z[42] * z[59];
z[59] = (T(-31) * z[49]) / T(4) + z[59] + z[64];
z[62] = z[59] + z[65];
z[62] = z[62] * z[94];
z[64] = z[15] * z[73];
z[57] = z[57] + z[64];
z[57] = (T(-3) * z[57]) / T(2);
z[57] = z[26] * z[57];
z[64] = int_to_imaginary<T>(3);
z[65] = z[42] * z[43];
z[65] = (T(12) * z[44]) / T(5) + z[65];
z[65] = -(z[64] * z[65] * z[96]);
z[64] = -(z[42] * z[64] * z[98]);
z[67] = T(-7) * z[95] + T(-13) * z[98];
z[67] = z[7] * z[67];
z[64] = z[64] + z[67] / T(2);
z[64] = z[7] * z[64];
z[67] = -z[95] + -z[98];
z[67] = prod_pow(z[45], 2) * z[67];
z[64] = z[64] + T(2) * z[67];
z[64] = z[45] * z[64];
z[59] = -(z[59] * z[93]);
return z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + (T(3) * z[66]) / T(2);
}



template IntegrandConstructorType<double> f_4_140_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_140_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_140_construct (const Kin<qd_real>&);
#endif

}