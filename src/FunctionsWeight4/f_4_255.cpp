#include "f_4_255.h"

namespace PentagonFunctions {

template <typename T> T f_4_255_abbreviated (const std::array<T,39>&);

template <typename T> class SpDLog_f_4_255_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_255_W_23 (const Kin<T>& kin) {
        c[0] = ((T(-9) * kin.v[0]) / T(4) + (T(9) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(3)) * kin.v[0] + ((T(-9) * kin.v[4]) / T(4) + T(3)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[3] + ((T(-9) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-3)) * kin.v[2];
c[1] = T(3) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4];
c[2] = ((T(-9) * kin.v[0]) / T(4) + (T(9) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(3)) * kin.v[0] + ((T(-9) * kin.v[4]) / T(4) + T(3)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[3] + ((T(-9) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-3)) * kin.v[2];
c[3] = T(3) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4];
c[4] = ((T(9) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(3)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[3] + ((T(9) * kin.v[4]) / T(4) + T(-3)) * kin.v[4] + ((T(9) * kin.v[0]) / T(4) + (T(-9) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-3)) * kin.v[0];
c[5] = T(-3) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4];
c[6] = ((T(9) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(3)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[3] + ((T(9) * kin.v[4]) / T(4) + T(-3)) * kin.v[4] + ((T(9) * kin.v[0]) / T(4) + (T(-9) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-3)) * kin.v[0];
c[7] = T(-3) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[5] * (t * c[0] + c[1]) + abb[6] * (t * c[2] + c[3]) + abb[7] * (t * c[4] + c[5]) + abb[8] * (t * c[6] + c[7]);
        }

        return abb[0] * (abb[5] * (abb[2] * abb[3] * T(3) + abb[4] * T(3)) + abb[2] * abb[3] * (abb[7] * T(-3) + abb[8] * T(-3) + abb[6] * T(3)) + abb[4] * (abb[7] * T(-3) + abb[8] * T(-3) + abb[6] * T(3)) + abb[1] * (abb[5] * (abb[2] * T(-3) + abb[3] * T(-3)) + abb[1] * (abb[7] * T(-3) + abb[8] * T(-3) + abb[5] * T(3) + abb[6] * T(3)) + abb[2] * (abb[6] * T(-3) + abb[7] * T(3) + abb[8] * T(3)) + abb[3] * (abb[6] * T(-3) + abb[7] * T(3) + abb[8] * T(3))));
    }
};
template <typename T> class SpDLog_f_4_255_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_255_W_22 (const Kin<T>& kin) {
        c[0] = ((T(-15) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + kin.v[1] * ((T(-15) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + T(-3) * kin.v[0] + (T(-15) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3])) + ((T(9) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + ((T(9) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(-3) + T(9)) * kin.v[1] + T(9) * kin.v[2] + T(-9) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);
c[2] = ((T(-9) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(15) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + kin.v[1] * ((T(15) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + T(3) * kin.v[0] + (T(15) / T(4) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + ((T(-9) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]));
c[3] = (T(-9) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[1] + T(-9) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + T(9) * kin.v[4];
c[4] = ((T(-9) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(15) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + kin.v[1] * ((T(15) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + T(3) * kin.v[0] + (T(15) / T(4) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + ((T(-9) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]));
c[5] = (T(-9) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[1] + T(-9) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + T(9) * kin.v[4];
c[6] = ((T(-15) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + kin.v[1] * ((T(-15) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + T(-3) * kin.v[0] + (T(-15) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3])) + ((T(9) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + ((T(9) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]));
c[7] = (bc<T>[0] * int_to_imaginary<T>(-3) + T(9)) * kin.v[1] + T(9) * kin.v[2] + T(-9) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[5] * (t * c[0] + c[1]) + abb[18] * (t * c[2] + c[3]) + abb[7] * (t * c[4] + c[5]) + abb[8] * (t * c[6] + c[7]);
        }

        return abb[25] * (abb[3] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[18] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[8] * bc<T>[0] * int_to_imaginary<T>(3)) + abb[2] * abb[3] * (abb[7] * T(-3) + abb[18] * T(-3) + abb[8] * T(3)) + abb[3] * abb[11] * (abb[7] * T(-3) + abb[18] * T(-3) + abb[8] * T(3)) + abb[3] * abb[10] * (abb[8] * T(-3) + abb[7] * T(3) + abb[18] * T(3)) + abb[1] * (abb[8] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[7] * bc<T>[0] * int_to_imaginary<T>(3) + abb[18] * bc<T>[0] * int_to_imaginary<T>(3) + abb[10] * (abb[7] * T(-3) + abb[18] * T(-3) + abb[8] * T(3)) + abb[1] * (abb[7] * T(-3) + abb[18] * T(-3) + abb[5] * T(3) + abb[8] * T(3)) + abb[5] * (abb[2] * T(-3) + abb[3] * T(-3) + abb[11] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * T(3)) + abb[2] * (abb[8] * T(-3) + abb[7] * T(3) + abb[18] * T(3)) + abb[3] * (abb[8] * T(-3) + abb[7] * T(3) + abb[18] * T(3)) + abb[11] * (abb[8] * T(-3) + abb[7] * T(3) + abb[18] * T(3))) + abb[20] * (abb[7] * T(-9) + abb[18] * T(-9) + abb[8] * T(9)) + abb[5] * (abb[3] * abb[10] * T(-3) + abb[3] * bc<T>[0] * int_to_imaginary<T>(3) + abb[2] * abb[3] * T(3) + abb[3] * abb[11] * T(3) + abb[20] * T(9)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_255_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl24 = DLog_W_24<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl22 = DLog_W_22<T>(kin),dl4 = DLog_W_4<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),spdl23 = SpDLog_f_4_255_W_23<T>(kin),spdl22 = SpDLog_f_4_255_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,39> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_8(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl2(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_12(kin_path), f_2_1_15(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl1(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), f_1_3_3(kin) - f_1_3_3(kin_path), dl17(t), f_2_1_14(kin_path), dl20(t), dl24(t), dl18(t), dl19(t), dl22(t), dl4(t), dl3(t), dl16(t), dl5(t), dl29(t) / kin_path.SqrtDelta, rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_255_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_255_abbreviated(const std::array<T,39>& abb)
{
using TR = typename T::value_type;
T z[93];
z[0] = abb[1];
z[1] = abb[5];
z[2] = abb[26];
z[3] = abb[28];
z[4] = abb[6];
z[5] = abb[27];
z[6] = abb[7];
z[7] = abb[8];
z[8] = abb[23];
z[9] = abb[24];
z[10] = abb[29];
z[11] = abb[18];
z[12] = abb[31];
z[13] = abb[35];
z[14] = abb[36];
z[15] = abb[38];
z[16] = abb[32];
z[17] = abb[33];
z[18] = abb[34];
z[19] = abb[2];
z[20] = abb[3];
z[21] = abb[10];
z[22] = abb[11];
z[23] = bc<TR>[0];
z[24] = abb[19];
z[25] = abb[21];
z[26] = abb[30];
z[27] = abb[37];
z[28] = abb[22];
z[29] = abb[15];
z[30] = abb[9];
z[31] = abb[4];
z[32] = abb[12];
z[33] = abb[14];
z[34] = abb[16];
z[35] = abb[17];
z[36] = abb[20];
z[37] = abb[13];
z[38] = bc<TR>[3];
z[39] = bc<TR>[9];
z[40] = bc<TR>[1];
z[41] = bc<TR>[2];
z[42] = bc<TR>[4];
z[43] = z[14] + z[15];
z[44] = z[16] * z[43];
z[45] = -z[12] + z[17] + z[18];
z[46] = z[15] * z[45];
z[44] = z[44] + -z[46];
z[47] = z[2] + -z[9];
z[48] = z[3] + -z[8];
z[49] = z[47] + -z[48];
z[49] = z[7] * z[49];
z[50] = z[14] * z[45];
z[51] = z[16] + -z[45];
z[52] = z[13] * z[51];
z[53] = z[50] + z[52];
z[49] = -z[44] + z[49] + z[53];
z[54] = z[0] / T(2);
z[55] = -(z[49] * z[54]);
z[56] = z[3] * z[11];
z[57] = z[2] + z[3];
z[58] = -(z[1] * z[57]);
z[58] = z[49] / T(2) + z[56] + z[58];
z[58] = z[20] * z[58];
z[45] = z[27] * z[45];
z[59] = z[45] + -z[46];
z[60] = -z[15] + z[27];
z[61] = -z[14] + z[60];
z[62] = z[16] * z[61];
z[62] = z[53] + -z[59] + z[62];
z[63] = z[7] * z[29];
z[64] = z[62] + z[63];
z[65] = z[4] + z[11];
z[66] = z[24] / T(2);
z[67] = -z[28] + z[29] / T(2) + z[66];
z[67] = -(z[65] * z[67]);
z[68] = z[9] + z[24];
z[69] = z[10] + z[68];
z[70] = -z[48] + z[69];
z[71] = -z[28] + z[70] / T(4);
z[71] = z[7] * z[71];
z[72] = z[8] + z[9];
z[73] = -z[28] + z[72] / T(2);
z[73] = z[1] * z[73];
z[64] = z[64] / T(4) + z[67] + z[71] + z[73];
z[64] = z[19] * z[64];
z[67] = z[16] * z[60];
z[67] = z[59] + -z[67];
z[51] = z[26] * z[51];
z[71] = z[51] + z[67];
z[69] = z[25] + -z[69];
z[69] = z[28] + z[69] / T(2);
z[69] = z[7] * z[69];
z[69] = z[69] + z[71] / T(2);
z[69] = z[21] * z[69];
z[73] = -z[24] + z[28];
z[73] = z[21] * z[73];
z[74] = z[0] * z[3];
z[75] = -z[73] + -z[74];
z[75] = z[11] * z[75];
z[76] = z[0] * z[57];
z[77] = -z[9] + z[28];
z[77] = z[21] * z[77];
z[77] = z[76] + z[77];
z[77] = z[1] * z[77];
z[78] = z[0] + -z[20];
z[78] = z[2] * z[78];
z[73] = -z[73] + z[78];
z[73] = z[4] * z[73];
z[57] = z[20] * z[57];
z[57] = z[57] + -z[76];
z[57] = z[6] * z[57];
z[55] = z[55] + z[57] + z[58] + z[64] + z[69] + z[73] + z[75] + z[77];
z[55] = z[19] * z[55];
z[57] = z[7] * z[30];
z[58] = z[3] + z[68];
z[58] = z[7] * z[58];
z[58] = -z[52] + -z[57] + z[58] + z[67];
z[64] = z[1] * z[3];
z[56] = -z[56] + z[58] / T(2) + z[64];
z[56] = z[20] * z[56];
z[64] = z[51] + z[53];
z[67] = z[14] * z[16];
z[67] = -z[64] + z[67];
z[69] = -z[25] + z[48];
z[73] = z[28] + z[69] / T(2);
z[73] = z[7] * z[73];
z[75] = z[63] / T(2);
z[67] = z[67] / T(2) + z[73] + -z[75];
z[73] = -z[28] + z[29];
z[77] = z[65] * z[73];
z[78] = -z[8] + z[28];
z[79] = z[1] * z[78];
z[77] = z[67] + z[77] + z[79];
z[79] = z[19] * z[77];
z[80] = z[3] * z[20];
z[80] = -z[74] + z[80];
z[80] = z[6] * z[80];
z[56] = -z[56] + z[79] + z[80];
z[79] = -z[3] + z[5];
z[80] = z[25] + z[79];
z[81] = -z[72] + z[80];
z[81] = z[7] * z[81];
z[81] = -z[44] + z[64] + z[81];
z[82] = -z[57] + -z[81];
z[83] = z[65] * z[79];
z[82] = z[82] / T(2) + z[83];
z[82] = z[22] * z[82];
z[67] = -(z[21] * z[67]);
z[83] = z[14] + z[27];
z[84] = z[16] * z[83];
z[45] = -z[45] + z[84];
z[84] = z[50] + z[51];
z[85] = z[24] + z[25];
z[86] = z[5] + z[85];
z[87] = -z[8] + z[86];
z[87] = z[7] * z[87];
z[87] = -z[45] + z[84] + z[87];
z[87] = z[54] * z[87];
z[73] = z[21] * z[73];
z[88] = -(z[0] * z[5]);
z[88] = -z[73] + z[88];
z[88] = z[11] * z[88];
z[89] = -(z[21] * z[78]);
z[89] = z[74] + z[89];
z[89] = z[1] * z[89];
z[90] = -(z[0] * z[79]);
z[73] = -z[73] + z[90];
z[73] = z[4] * z[73];
z[67] = z[56] + z[67] + z[73] + z[82] / T(2) + z[87] + z[88] + z[89];
z[67] = z[22] * z[67];
z[73] = -z[25] + z[68];
z[82] = z[8] + -z[73];
z[82] = z[7] * z[82];
z[60] = z[14] + z[60];
z[60] = z[16] * z[60];
z[59] = z[59] + -z[60] + z[63] + z[82] + z[84];
z[60] = prod_pow(z[21], 2);
z[82] = -z[32] / T(2) + -z[60] / T(4);
z[82] = z[59] * z[82];
z[58] = z[21] * z[58];
z[49] = -(z[0] * z[49]);
z[49] = z[49] + z[58];
z[58] = z[3] * z[21];
z[84] = -z[58] + -z[74];
z[84] = z[11] * z[84];
z[76] = z[58] + z[76];
z[87] = z[1] * z[76];
z[49] = z[49] / T(2) + z[84] + z[87];
z[49] = z[20] * z[49];
z[60] = z[32] + z[60] / T(2);
z[84] = -z[24] + z[29];
z[87] = z[60] * z[84];
z[88] = T(2) * z[28];
z[89] = -z[24] + -z[29] + z[88];
z[90] = z[34] * z[89];
z[91] = z[37] * z[79];
z[87] = z[87] + z[90] + z[91];
z[90] = z[2] * z[31];
z[80] = -z[2] + -z[9] + z[24] + z[80];
z[80] = z[36] * z[80];
z[91] = -z[2] + z[79] / T(2);
z[91] = prod_pow(z[0], 2) * z[91];
z[92] = z[0] * z[2] * z[20];
z[80] = z[80] + z[87] + -z[90] + z[91] + z[92];
z[80] = z[4] * z[80];
z[81] = -(z[37] * z[81]);
z[69] = z[2] + -z[10] + z[24] + -z[69];
z[69] = z[7] * z[69];
z[45] = -z[45] + z[64] + z[69];
z[45] = z[31] * z[45];
z[64] = -z[37] + z[60];
z[64] = z[57] * z[64];
z[45] = z[45] + z[64] + z[81];
z[48] = z[48] + z[86];
z[48] = z[36] * z[48];
z[64] = z[3] + z[5];
z[64] = z[54] * z[64];
z[58] = z[58] + z[64];
z[58] = z[0] * z[58];
z[48] = z[48] + z[58] + z[87];
z[48] = z[11] * z[48];
z[58] = z[0] * z[76];
z[64] = -z[8] + z[9];
z[69] = z[2] + z[64];
z[81] = T(2) * z[3] + z[69];
z[81] = z[36] * z[81];
z[58] = z[58] + z[81] + z[90];
z[60] = z[60] * z[64];
z[72] = z[72] + -z[88];
z[81] = z[34] * z[72];
z[60] = -z[58] + z[60] + z[81];
z[60] = z[1] * z[60];
z[70] = z[70] / T(2) + -z[88];
z[70] = z[7] * z[70];
z[62] = z[62] / T(2) + z[70] + z[75];
z[70] = z[34] * z[62];
z[75] = z[9] / T(2);
z[81] = z[3] / T(2) + z[75];
z[86] = z[5] / T(2);
z[85] = z[2] / T(2) + z[8] + -z[81] + (T(-3) * z[85]) / T(2) + -z[86];
z[85] = z[7] * z[85];
z[87] = z[52] + -z[71];
z[85] = z[85] + z[87] / T(2);
z[85] = z[36] * z[85];
z[44] = -z[44] + z[50];
z[50] = z[2] + -z[3] + (T(3) * z[8]) / T(2) + -z[10] / T(2) + -z[75] + -z[86];
z[50] = z[7] * z[50];
z[44] = z[44] / T(2) + z[50] + z[52];
z[44] = z[0] * z[44];
z[50] = -z[10] + z[25] + z[68];
z[50] = z[7] * z[50];
z[50] = z[50] + z[71];
z[52] = -(z[21] * z[50]);
z[44] = z[44] + z[52];
z[44] = z[44] * z[54];
z[52] = -(z[20] * z[76]);
z[52] = z[52] + z[58];
z[52] = z[6] * z[52];
z[44] = z[44] + z[45] / T(2) + z[48] + z[49] + z[52] + z[55] + z[60] + z[67] + z[70] + z[80] + z[82] + z[85];
z[45] = -z[57] + z[59];
z[48] = -(z[65] * z[84]);
z[49] = -(z[1] * z[64]);
z[45] = z[45] / T(2) + z[48] + z[49];
z[45] = z[33] * z[45];
z[48] = z[4] * z[89];
z[48] = z[48] + z[62];
z[48] = z[35] * z[48];
z[49] = z[22] * z[77];
z[50] = z[50] * z[54];
z[52] = z[35] * z[89];
z[52] = z[52] + -z[74];
z[52] = z[11] * z[52];
z[54] = z[35] * z[72];
z[54] = z[54] + z[74];
z[54] = z[1] * z[54];
z[55] = z[13] + z[61];
z[55] = z[41] * z[55];
z[58] = -z[13] + T(2) * z[43];
z[59] = z[40] * z[58];
z[60] = -z[55] + -z[59];
z[60] = z[41] * z[60];
z[43] = -z[13] / T(2) + z[43];
z[43] = prod_pow(z[40], 2) * z[43];
z[58] = z[42] * z[58];
z[43] = z[43] + z[45] + z[48] + z[49] + z[50] + z[52] + z[54] + z[56] + z[58] + z[60];
z[43] = int_to_imaginary<T>(1) * z[43];
z[45] = z[2] + z[79];
z[48] = -z[10] + z[45];
z[49] = z[38] * z[48];
z[43] = z[43] + z[49];
z[49] = -z[14] + z[15];
z[49] = z[16] * z[49];
z[50] = z[3] + -z[69];
z[50] = z[6] * z[50];
z[46] = -z[46] + z[49] + z[50] + (T(3) * z[51]) / T(2) + z[53];
z[49] = z[2] + z[5];
z[49] = (T(5) * z[25]) / T(4) + -z[49] / T(4) + -z[66] + -z[78] + -z[81];
z[49] = z[7] * z[49];
z[50] = -z[28] + T(2) * z[29];
z[51] = z[5] + z[8] + z[24] + -z[25];
z[51] = -z[50] + z[51] / T(2);
z[51] = z[11] * z[51];
z[45] = z[45] + z[73];
z[45] = z[45] / T(2) + -z[50];
z[45] = z[4] * z[45];
z[50] = z[26] + z[83];
z[48] = -z[13] / T(4) + -z[15] + -z[48] / T(9) + -z[50] / T(2);
z[48] = int_to_imaginary<T>(1) * z[23] * z[48];
z[47] = -z[3] + T(3) * z[8] + z[47];
z[47] = -z[28] + z[47] / T(2);
z[47] = z[1] * z[47];
z[45] = z[45] + z[46] / T(2) + z[47] + z[48] + z[49] + z[51] + (T(-3) * z[55]) / T(2) + -z[57] / T(4) + -z[59] + z[63];
z[45] = z[23] * z[45];
z[43] = T(3) * z[43] + z[45];
z[43] = z[23] * z[43];
z[45] = T(25) * z[14] + T(37) * z[15] + T(27) * z[27];
z[45] = (T(13) * z[13]) / T(4) + T(3) * z[26] + z[45] / T(4);
z[45] = z[39] * z[45];
return z[43] + T(3) * z[44] + z[45];
}



template IntegrandConstructorType<double> f_4_255_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_255_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_255_construct (const Kin<qd_real>&);
#endif

}