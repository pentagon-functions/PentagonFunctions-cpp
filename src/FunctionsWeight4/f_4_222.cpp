#include "f_4_222.h"

namespace PentagonFunctions {

template <typename T> T f_4_222_abbreviated (const std::array<T,20>&);

template <typename T> class SpDLog_f_4_222_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_222_W_23 (const Kin<T>& kin) {
        c[0] = ((T(9) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-3)) * kin.v[2] + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(3) + T(-3) * kin.v[1]) + ((T(9) * kin.v[4]) / T(4) + T(3)) * kin.v[4];
c[1] = kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[4] * (T(3) + T(-15) * kin.v[4]) + kin.v[2] * (T(-3) + T(-15) * kin.v[2] + T(-33) * kin.v[3] + T(30) * kin.v[4]) + kin.v[3] * (T(-3) + T(-18) * kin.v[3] + T(33) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(24) * kin.v[3] + T(-40) * kin.v[4]) + kin.v[2] * (T(8) + T(16) * kin.v[2] + T(40) * kin.v[3] + T(-32) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]) + kin.v[4] * (T(-8) + T(16) * kin.v[4])) + kin.v[0] * (T(3) + (T(-18) + bc<T>[0] * int_to_imaginary<T>(24)) * kin.v[0] + T(-3) * kin.v[1] + T(33) * kin.v[2] + T(36) * kin.v[3] + T(-33) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(-40) * kin.v[2] + T(-48) * kin.v[3] + T(40) * kin.v[4])) + rlog(kin.v[3]) * (((T(5) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + kin.v[3] * ((T(33) * kin.v[3]) / T(2) + T(14) + T(-19) * kin.v[4]) + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(14) + T(19) * kin.v[3] + T(-5) * kin.v[4]) + kin.v[1] * (T(-14) * kin.v[2] + T(-14) * kin.v[3] + T(14) * kin.v[4]) + kin.v[0] * (T(-14) + (T(33) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(14) * kin.v[1] + T(-19) * kin.v[2] + T(-33) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[1] + T(8) * kin.v[3]) + T(19) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(4) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(19) * kin.v[2]) / T(4) + (T(41) * kin.v[3]) / T(2) + (T(-19) * kin.v[4]) / T(2) + T(11)) * kin.v[2] + ((T(63) * kin.v[3]) / T(4) + (T(-41) * kin.v[4]) / T(2) + T(11)) * kin.v[3] + kin.v[0] * ((T(-41) * kin.v[2]) / T(2) + (T(-63) * kin.v[3]) / T(2) + (T(41) * kin.v[4]) / T(2) + T(-11) + (T(63) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(11) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[1] + T(8) * kin.v[3])) + ((T(19) * kin.v[4]) / T(4) + T(-11)) * kin.v[4] + kin.v[1] * (T(-11) * kin.v[2] + T(-11) * kin.v[3] + T(11) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(4) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(-5) * kin.v[4]) / T(2) + T(14)) * kin.v[4] + kin.v[0] * (T(14) + (T(-33) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(-14) * kin.v[1] + T(19) * kin.v[2] + T(33) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(-8) * kin.v[3]) + T(-19) * kin.v[4]) + kin.v[1] * (T(14) * kin.v[2] + T(14) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(-14) + T(-19) * kin.v[3] + T(5) * kin.v[4]) + kin.v[3] * ((T(-33) * kin.v[3]) / T(2) + T(-14) + T(19) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * (T(28) + (T(-33) + bc<T>[0] * int_to_imaginary<T>(8)) * kin.v[0] + T(-28) * kin.v[1] + T(38) * kin.v[2] + T(66) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) + T(16) * kin.v[1] + T(-16) * kin.v[3]) + T(-38) * kin.v[4]) + kin.v[1] * (T(28) * kin.v[2] + T(28) * kin.v[3] + T(-28) * kin.v[4]) + kin.v[4] * (T(28) + T(-5) * kin.v[4]) + kin.v[2] * (T(-28) + T(-5) * kin.v[2] + T(-38) * kin.v[3] + T(10) * kin.v[4]) + kin.v[3] * (T(-28) + T(-33) * kin.v[3] + T(38) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(16) + T(8) * kin.v[3]) + kin.v[4] * (T(-16) + T(-8) * kin.v[4]) + kin.v[2] * (T(16) + T(-8) * kin.v[2] + T(16) * kin.v[4]) + kin.v[1] * (T(-16) * kin.v[2] + T(-16) * kin.v[3] + T(16) * kin.v[4])));
c[2] = T(-3) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4];
c[3] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(-8) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(-16) + T(12)) * kin.v[0] + T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) * kin.v[2] + T(16) * kin.v[3] + T(-16) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-9) + bc<T>[0] * int_to_imaginary<T>(8)) * kin.v[0] + T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4])) + rlog(kin.v[3]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(8)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (abb[10] * (abb[3] * T(-6) + abb[2] * (abb[7] * T(-8) + abb[2] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-8))) + abb[1] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-16) + abb[1] * (abb[9] + abb[8] * T(-8) + abb[5] * T(-4) + abb[4] * T(3) + abb[10] * T(4)) + abb[10] * (bc<T>[0] * int_to_imaginary<T>(8) + abb[7] * T(8)) + abb[7] * (abb[8] * T(-16) + abb[5] * T(-8) + abb[9] * T(8))) + abb[3] * (abb[9] * T(-9) + abb[4] * T(3) + abb[5] * T(6) + abb[8] * T(12)) + abb[2] * (abb[9] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(16) + abb[2] * (-abb[9] + abb[4] * T(-3) + abb[5] * T(4) + abb[8] * T(8)) + abb[7] * (abb[9] * T(-8) + abb[5] * T(8) + abb[8] * T(16))) + abb[6] * (abb[2] * abb[10] * T(8) + abb[2] * (abb[8] * T(-16) + abb[5] * T(-8) + abb[9] * T(8)) + abb[1] * (abb[9] * T(-8) + abb[10] * T(-8) + abb[5] * T(8) + abb[8] * T(16))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_222_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl9 = DLog_W_9<T>(kin),dl20 = DLog_W_20<T>(kin),spdl23 = SpDLog_f_4_222_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,20> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), -rlog(t), rlog(kin.W[19] / kin_path.W[19]), rlog(v_path[3]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[3] / kin_path.W[3]), dl17(t), rlog(kin.W[8] / kin_path.W[8]), f_2_1_6(kin_path), f_2_1_14(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), dl16(t), dl9(t), dl20(t)}
;

        auto result = f_4_222_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_222_abbreviated(const std::array<T,20>& abb)
{
using TR = typename T::value_type;
T z[79];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[11];
z[3] = abb[16];
z[4] = abb[17];
z[5] = abb[19];
z[6] = abb[5];
z[7] = abb[18];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[12];
z[12] = abb[2];
z[13] = abb[6];
z[14] = abb[7];
z[15] = bc<TR>[0];
z[16] = abb[3];
z[17] = abb[13];
z[18] = abb[14];
z[19] = abb[15];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = int_to_imaginary<T>(1) * z[15];
z[23] = -z[13] + z[22];
z[24] = T(2) * z[23];
z[25] = z[14] + z[24];
z[26] = T(2) * z[14];
z[27] = z[25] * z[26];
z[28] = z[13] * z[22];
z[29] = T(4) * z[28];
z[27] = z[27] + z[29];
z[30] = z[19] * z[22];
z[30] = -z[17] + z[30];
z[31] = T(-2) * z[30];
z[32] = prod_pow(z[13], 2);
z[33] = T(3) * z[32];
z[34] = z[31] + z[33];
z[35] = T(2) * z[12];
z[36] = T(3) * z[23];
z[37] = T(4) * z[0] + -z[14] + z[36];
z[37] = -z[12] + T(2) * z[37];
z[37] = z[35] * z[37];
z[38] = T(8) * z[16];
z[39] = T(6) * z[18];
z[40] = prod_pow(z[15], 2);
z[41] = T(-11) * z[0] + T(-6) * z[23];
z[41] = z[0] * z[41];
z[37] = -z[27] + z[34] + z[37] + -z[38] + -z[39] + -z[40] / T(2) + z[41];
z[37] = z[9] * z[37];
z[41] = z[23] * z[26];
z[42] = T(-4) * z[30];
z[43] = T(2) * z[28];
z[44] = -z[0] + z[26];
z[45] = z[0] * z[44];
z[46] = z[40] / T(3);
z[41] = z[32] + z[41] + z[42] + z[43] + z[45] + z[46];
z[47] = z[0] + z[23];
z[48] = T(4) * z[12];
z[49] = z[47] * z[48];
z[50] = T(3) * z[18];
z[51] = T(5) * z[16];
z[41] = T(2) * z[41] + z[49] + -z[50] + -z[51];
z[41] = T(2) * z[41];
z[49] = -(z[11] * z[41]);
z[52] = T(4) * z[14];
z[53] = z[25] * z[52];
z[54] = T(8) * z[28];
z[53] = T(-16) * z[30] + T(4) * z[32] + -z[46] + z[53] + z[54];
z[55] = z[14] + z[23];
z[56] = T(2) * z[55];
z[57] = z[12] + z[56];
z[58] = z[48] * z[57];
z[56] = z[0] + z[56];
z[56] = z[0] * z[56];
z[59] = T(16) * z[16];
z[60] = z[53] + T(8) * z[56] + -z[58] + -z[59];
z[61] = z[8] * z[60];
z[62] = z[23] + z[52];
z[63] = T(7) * z[0];
z[62] = T(2) * z[62] + -z[63];
z[62] = z[0] * z[62];
z[64] = -z[14] + z[23];
z[65] = T(2) * z[0];
z[66] = z[64] + z[65];
z[66] = -z[12] + T(2) * z[66];
z[48] = z[48] * z[66];
z[48] = z[48] + -z[59];
z[59] = z[39] + (T(2) * z[40]) / T(3);
z[67] = T(-10) * z[30];
z[62] = T(5) * z[32] + z[48] + -z[59] + z[62] + z[67];
z[68] = -(z[1] * z[62]);
z[69] = -z[14] + z[24];
z[70] = z[26] * z[69];
z[70] = z[29] + z[70];
z[71] = -z[39] + z[70];
z[72] = T(-8) * z[30] + T(2) * z[32];
z[73] = (T(3) * z[40]) / T(2) + z[71] + z[72];
z[74] = z[14] + z[36] + z[65];
z[74] = z[12] + T(2) * z[74];
z[74] = z[35] * z[74];
z[75] = T(2) * z[16];
z[47] = T(8) * z[47];
z[76] = -(z[0] * z[47]);
z[74] = z[73] + z[74] + -z[75] + z[76];
z[74] = z[10] * z[74];
z[27] = -z[27] + z[40] / T(6) + -z[72];
z[57] = z[35] * z[57];
z[56] = -z[27] + -z[38] + T(4) * z[56] + -z[57];
z[56] = z[6] * z[56];
z[72] = prod_pow(z[20], 2);
z[46] = z[46] + T(3) * z[72];
z[76] = z[22] * z[46];
z[77] = z[20] * z[40];
z[78] = (T(5) * z[77]) / T(2);
z[76] = (T(187) * z[21]) / T(12) + z[76] + -z[78];
z[37] = z[37] + z[49] + z[56] + z[61] + z[68] + z[74] + z[76];
z[37] = z[5] * z[37];
z[49] = z[57] + -z[75];
z[56] = T(3) * z[0];
z[44] = z[44] * z[56];
z[57] = z[26] * z[64];
z[29] = -z[29] + -z[31] + z[32] + -z[39] + (T(-5) * z[40]) / T(6) + z[44] + -z[49] + z[57];
z[29] = z[9] * z[29];
z[31] = T(4) * z[16] + -z[58];
z[53] = z[31] + -z[53];
z[53] = z[8] * z[53];
z[57] = z[35] * z[66];
z[38] = -z[38] + z[57];
z[45] = z[38] + T(4) * z[45] + z[73];
z[57] = -(z[10] * z[45]);
z[29] = z[29] + z[53] + z[57] + -z[76];
z[29] = z[2] * z[29];
z[53] = (T(-17) * z[0]) / T(2) + T(-7) * z[23] + -z[26];
z[53] = z[0] * z[53];
z[57] = z[14] + z[35];
z[58] = -z[0] + z[57];
z[58] = z[35] * z[58];
z[28] = T(6) * z[28];
z[61] = prod_pow(z[14], 2);
z[53] = T(10) * z[16] + -z[28] + T(7) * z[30] + -z[32] / T(2) + z[53] + z[58] + -z[59] + T(-4) * z[61];
z[53] = z[4] * z[53];
z[41] = z[2] * z[41];
z[58] = z[34] + -z[70];
z[61] = T(3) * z[14];
z[64] = z[24] + -z[61];
z[64] = z[0] * z[64];
z[66] = T(4) * z[23];
z[68] = T(5) * z[0] + z[66];
z[57] = -z[57] + z[68];
z[57] = z[12] * z[57];
z[50] = (T(-8) * z[40]) / T(3) + z[50] + -z[51] + z[57] + z[58] + z[64];
z[50] = z[3] * z[50];
z[51] = z[0] / T(2);
z[57] = -z[23] + z[51];
z[57] = z[56] * z[57];
z[28] = z[28] + T(3) * z[30] + (T(-9) * z[32]) / T(2) + T(4) * z[40] + z[57];
z[28] = z[7] * z[28];
z[28] = z[28] + z[41] + T(2) * z[50] + z[53];
z[28] = z[11] * z[28];
z[41] = z[52] * z[69];
z[24] = -z[24] + z[56];
z[50] = z[24] * z[65];
z[41] = T(-6) * z[32] + (T(13) * z[40]) / T(3) + z[41] + -z[42] + -z[48] + z[50] + z[54];
z[41] = z[8] * z[41];
z[42] = -z[23] + z[61];
z[42] = T(2) * z[42] + -z[56];
z[42] = z[0] * z[42];
z[25] = -z[0] + z[12] + -z[25];
z[25] = z[25] * z[35];
z[25] = z[25] + -z[34] + (T(19) * z[40]) / T(6) + z[42] + z[71] + z[75];
z[25] = z[10] * z[25];
z[34] = -(z[9] * z[45]);
z[42] = (T(-11) * z[40]) / T(3) + T(-9) * z[72];
z[42] = z[22] * z[42];
z[42] = (T(-185) * z[21]) / T(12) + z[42] + -z[78];
z[25] = z[25] + z[34] + z[41] + z[42] / T(2);
z[25] = z[3] * z[25];
z[34] = z[23] + z[26];
z[26] = z[26] * z[34];
z[26] = z[26] + z[32] + z[54] + z[59] + z[67];
z[34] = -z[14] + -z[68];
z[34] = z[34] * z[65];
z[41] = prod_pow(z[12], 2);
z[34] = T(7) * z[16] + -z[26] + z[34] + T(3) * z[41];
z[34] = z[4] * z[34];
z[26] = z[26] + -z[31] + -z[44];
z[26] = z[2] * z[26];
z[31] = z[3] * z[62];
z[26] = z[26] + z[31] + z[34];
z[26] = z[1] * z[26];
z[31] = z[36] + z[52];
z[31] = T(2) * z[31] + z[63];
z[31] = z[0] * z[31];
z[34] = z[12] * z[47];
z[36] = z[14] * z[23];
z[41] = T(8) * z[36];
z[34] = z[34] + -z[41];
z[42] = T(-6) * z[30];
z[44] = T(2) * z[40];
z[31] = z[31] + -z[32] + -z[34] + z[42] + z[44] + z[54];
z[45] = z[9] * z[31];
z[30] = T(-9) * z[30] + (T(7) * z[32]) / T(2) + z[43];
z[23] = T(9) * z[23];
z[32] = (T(11) * z[0]) / T(2) + T(8) * z[14] + z[23];
z[32] = z[0] * z[32];
z[32] = z[30] + z[32] + -z[34] + -z[44];
z[32] = z[10] * z[32];
z[34] = z[40] + z[72];
z[34] = z[22] * z[34];
z[34] = (T(-21) * z[21]) / T(4) + z[34] + z[78];
z[32] = z[32] + (T(3) * z[34]) / T(2) + z[45];
z[32] = z[7] * z[32];
z[34] = z[39] + z[40];
z[39] = z[0] + T(-7) * z[14] + -z[66];
z[39] = z[39] * z[65];
z[43] = z[12] + T(8) * z[55];
z[43] = z[12] * z[43];
z[33] = T(9) * z[16] + -z[33] + z[34] + T(-6) * z[36] + z[39] + -z[42] + z[43];
z[33] = z[9] * z[33];
z[23] = T(-14) * z[14] + -z[23] + z[51];
z[23] = z[0] * z[23];
z[36] = z[0] + z[61] + z[66];
z[35] = z[35] * z[36];
z[23] = T(6) * z[16] + z[23] + -z[30] + z[34] + z[35] + -z[41];
z[23] = z[10] * z[23];
z[22] = T(2) * z[22];
z[22] = z[22] * z[46];
z[22] = (T(187) * z[21]) / T(6) + z[22] + z[23] + z[33] + T(-5) * z[77];
z[22] = z[4] * z[22];
z[23] = z[7] * z[31];
z[30] = z[4] * z[60];
z[23] = z[23] + -z[30];
z[30] = z[8] * z[23];
z[24] = z[0] * z[24];
z[24] = z[24] + -z[38] + (T(13) * z[40]) / T(6) + -z[58];
z[24] = z[3] * z[24];
z[27] = z[27] + -z[49];
z[27] = z[2] * z[27];
z[23] = -z[23] + z[24] + z[27];
z[23] = z[6] * z[23];
return z[22] + z[23] + z[25] + z[26] + z[28] + z[29] + T(-2) * z[30] + z[32] + z[37];
}



template IntegrandConstructorType<double> f_4_222_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_222_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_222_construct (const Kin<qd_real>&);
#endif

}