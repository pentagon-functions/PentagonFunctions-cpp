#include "f_4_416.h"

namespace PentagonFunctions {

template <typename T> T f_4_416_abbreviated (const std::array<T,58>&);

template <typename T> class SpDLog_f_4_416_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_416_W_22 (const Kin<T>& kin) {
        c[0] = (T(13) / T(2) + (T(41) * kin.v[2]) / T(8) + (T(-27) * kin.v[3]) / T(4) + (T(-45) * kin.v[4]) / T(4)) * kin.v[2] + kin.v[1] * (T(13) / T(2) + (T(-11) * kin.v[0]) / T(2) + (T(19) * kin.v[2]) / T(4) + (T(-5) * kin.v[3]) / T(4) + (T(-23) * kin.v[4]) / T(4) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[0]) + (T(-3) / T(8) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1]) + ((T(-11) * kin.v[2]) / T(2) + (T(11) * kin.v[3]) / T(2) + (T(11) * kin.v[4]) / T(2)) * kin.v[0] + (T(-13) / T(2) + (T(49) * kin.v[4]) / T(8)) * kin.v[4] + (T(-13) / T(2) + (T(13) * kin.v[3]) / T(8) + (T(31) * kin.v[4]) / T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]));
c[1] = (T(13) / T(2) + (T(41) * kin.v[2]) / T(8) + (T(-27) * kin.v[3]) / T(4) + (T(-45) * kin.v[4]) / T(4)) * kin.v[2] + kin.v[1] * (T(13) / T(2) + (T(-11) * kin.v[0]) / T(2) + (T(19) * kin.v[2]) / T(4) + (T(-5) * kin.v[3]) / T(4) + (T(-23) * kin.v[4]) / T(4) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[0]) + (T(-3) / T(8) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1]) + ((T(-11) * kin.v[2]) / T(2) + (T(11) * kin.v[3]) / T(2) + (T(11) * kin.v[4]) / T(2)) * kin.v[0] + (T(-13) / T(2) + (T(49) * kin.v[4]) / T(8)) * kin.v[4] + (T(-13) / T(2) + (T(13) * kin.v[3]) / T(8) + (T(31) * kin.v[4]) / T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]));
c[2] = (T(13) / T(2) + (T(-13) * kin.v[3]) / T(8) + (T(-31) * kin.v[4]) / T(4)) * kin.v[3] + ((T(11) * kin.v[2]) / T(2) + (T(-11) * kin.v[3]) / T(2) + (T(-11) * kin.v[4]) / T(2)) * kin.v[0] + (T(13) / T(2) + (T(-49) * kin.v[4]) / T(8)) * kin.v[4] + kin.v[1] * (T(-13) / T(2) + (T(11) * kin.v[0]) / T(2) + (T(-19) * kin.v[2]) / T(4) + (T(5) * kin.v[3]) / T(4) + (T(23) * kin.v[4]) / T(4) + (T(3) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[0] + T(1) + kin.v[3])) + (T(-13) / T(2) + (T(-41) * kin.v[2]) / T(8) + (T(27) * kin.v[3]) / T(4) + (T(45) * kin.v[4]) / T(4)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]));
c[3] = (T(13) / T(2) + (T(-13) * kin.v[3]) / T(8) + (T(-31) * kin.v[4]) / T(4)) * kin.v[3] + ((T(11) * kin.v[2]) / T(2) + (T(-11) * kin.v[3]) / T(2) + (T(-11) * kin.v[4]) / T(2)) * kin.v[0] + (T(13) / T(2) + (T(-49) * kin.v[4]) / T(8)) * kin.v[4] + kin.v[1] * (T(-13) / T(2) + (T(11) * kin.v[0]) / T(2) + (T(-19) * kin.v[2]) / T(4) + (T(5) * kin.v[3]) / T(4) + (T(23) * kin.v[4]) / T(4) + (T(3) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[0] + T(1) + kin.v[3])) + (T(-13) / T(2) + (T(-41) * kin.v[2]) / T(8) + (T(27) * kin.v[3]) / T(4) + (T(45) * kin.v[4]) / T(4)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]));
c[4] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + (T(7) * kin.v[2]) / T(2) + (T(-7) * kin.v[3]) / T(2) + (T(-7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4]);
c[5] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + (T(7) * kin.v[2]) / T(2) + (T(-7) * kin.v[3]) / T(2) + (T(-7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4]);
c[6] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + (T(-7) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (T(7) * kin.v[4]) / T(2);
c[7] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + (T(-7) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (T(7) * kin.v[4]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[14] * c[0] + abb[13] * c[1] + abb[23] * c[2] + abb[18] * c[3]) + abb[14] * c[4] + abb[13] * c[5] + abb[23] * c[6] + abb[18] * c[7];
        }

        return abb[22] * (abb[24] * ((abb[18] * T(-7)) / T(2) + (abb[23] * T(-7)) / T(2) + (abb[13] * T(7)) / T(2)) + abb[4] * abb[20] * (abb[13] + -abb[18] + -abb[23]) + abb[2] * (abb[4] * (abb[18] + abb[23] + -abb[13]) + -(abb[4] * abb[14]) + abb[21] * (abb[13] + abb[14] + -abb[18] + -abb[23])) + abb[4] * (abb[4] * ((abb[18] * T(-9)) / T(4) + (abb[23] * T(-9)) / T(4) + (abb[13] * T(9)) / T(4)) + abb[18] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[23] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[13] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[4] * abb[16] * (abb[13] * T(-2) + abb[18] * T(2) + abb[23] * T(2)) + abb[14] * (abb[4] * abb[20] + (abb[24] * T(7)) / T(2) + abb[4] * abb[16] * T(-2) + abb[4] * ((abb[4] * T(9)) / T(4) + bc<T>[0] * int_to_imaginary<T>(1)) + abb[3] * abb[4] * T(3)) + abb[3] * abb[4] * (abb[18] * T(-3) + abb[23] * T(-3) + abb[13] * T(3)) + abb[21] * (abb[21] * (abb[13] / T(4) + abb[14] / T(4) + -abb[18] / T(4) + -abb[23] / T(4)) + abb[4] * ((abb[13] * T(-5)) / T(2) + (abb[18] * T(5)) / T(2) + (abb[23] * T(5)) / T(2)) + abb[20] * (abb[18] + abb[23] + -abb[13]) + abb[13] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[18] * bc<T>[0] * int_to_imaginary<T>(1) + abb[23] * bc<T>[0] * int_to_imaginary<T>(1) + abb[16] * (abb[18] * T(-2) + abb[23] * T(-2) + abb[13] * T(2)) + abb[14] * ((abb[4] * T(-5)) / T(2) + -abb[20] + abb[3] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-1) + abb[16] * T(2)) + abb[3] * (abb[13] * T(-3) + abb[18] * T(3) + abb[23] * T(3))));
    }
};
template <typename T> class SpDLog_f_4_416_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_416_W_12 (const Kin<T>& kin) {
        c[0] = (kin.v[0] * kin.v[4]) / T(2) + -(kin.v[2] * kin.v[4]) + -(kin.v[3] * kin.v[4]) / T(2) + bc<T>[0] * (T(-7) / T(2) + (T(-7) * kin.v[4]) / T(4)) * int_to_imaginary<T>(1) * kin.v[4] + ((T(-3) * kin.v[4]) / T(2) + T(7)) * kin.v[4] + kin.v[1] * (-kin.v[0] / T(2) + kin.v[3] / T(2) + T(-7) + bc<T>[0] * (int_to_imaginary<T>(7) / T(2)) + (T(-17) / T(2) + bc<T>[0] * (int_to_imaginary<T>(7) / T(4))) * kin.v[1] + kin.v[2] + T(10) * kin.v[4]);
c[1] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(7) / T(2))) * kin.v[1] + T(4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(-7) / T(2)) * kin.v[4];
c[2] = kin.v[1] * (-kin.v[3] / T(2) + kin.v[0] / T(2) + -kin.v[2] + bc<T>[0] * (int_to_imaginary<T>(-7) / T(2)) + T(7) + (T(17) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-7) / T(4))) * kin.v[1] + T(-10) * kin.v[4]) + -(kin.v[0] * kin.v[4]) / T(2) + kin.v[2] * kin.v[4] + (kin.v[3] * kin.v[4]) / T(2) + ((T(3) * kin.v[4]) / T(2) + T(-7)) * kin.v[4] + bc<T>[0] * (T(7) / T(2) + (T(7) * kin.v[4]) / T(4)) * int_to_imaginary<T>(1) * kin.v[4];
c[3] = (bc<T>[0] * (int_to_imaginary<T>(-7) / T(2)) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(7) / T(2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[14] * (t * c[0] + c[1]) + abb[23] * (t * c[2] + c[3]);
        }

        return abb[28] * (-(abb[16] * abb[17] * abb[23]) / T(2) + abb[23] * abb[29] * T(-4) + abb[17] * ((abb[4] * abb[23]) / T(2) + (abb[3] * abb[23] * T(-9)) / T(2) + (abb[17] * abb[23] * T(-3)) / T(2) + abb[23] * bc<T>[0] * (int_to_imaginary<T>(-7) / T(2))) + abb[17] * abb[20] * abb[23] * T(4) + abb[14] * ((abb[16] * abb[17]) / T(2) + abb[17] * abb[20] * T(-4) + abb[17] * (-abb[4] / T(2) + (abb[17] * T(3)) / T(2) + (abb[3] * T(9)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(7) / T(2))) + abb[29] * T(4)) + abb[2] * ((abb[16] * abb[23]) / T(2) + (abb[17] * abb[23]) / T(2) + -(abb[4] * abb[23]) / T(2) + (abb[3] * abb[23] * T(9)) / T(2) + abb[2] * (abb[23] + -abb[14]) + abb[20] * abb[23] * T(-4) + abb[23] * bc<T>[0] * (int_to_imaginary<T>(7) / T(2)) + abb[14] * (abb[4] / T(2) + -abb[16] / T(2) + -abb[17] / T(2) + (abb[3] * T(-9)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-7) / T(2)) + abb[20] * T(4))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_416_construct (const Kin<T>& kin) {
    return [&kin, 
            dl27 = DLog_W_27<T>(kin),dl18 = DLog_W_18<T>(kin),dl15 = DLog_W_15<T>(kin),dl9 = DLog_W_9<T>(kin),dl22 = DLog_W_22<T>(kin),dl8 = DLog_W_8<T>(kin),dl12 = DLog_W_12<T>(kin),dl14 = DLog_W_14<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl19 = DLog_W_19<T>(kin),dl31 = DLog_W_31<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),spdl22 = SpDLog_f_4_416_W_22<T>(kin),spdl12 = SpDLog_f_4_416_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,58> abbr = 
            {dl27(t) / kin_path.SqrtDelta, rlog(kin.W[3] / kin_path.W[3]), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_11(kin_path), f_2_1_15(kin_path), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), dl18(t), f_1_3_5(kin) - f_1_3_5(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), dl15(t), rlog(v_path[2]), rlog(-v_path[4]), f_1_3_4(kin) - f_1_3_4(kin_path), dl9(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), dl22(t), f_1_3_3(kin) - f_1_3_3(kin_path), f_2_1_14(kin_path), dl8(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl12(t), f_2_1_4(kin_path), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl4(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl5(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl19(t), dl31(t), dl17(t), dl1(t), dl3(t), dl16(t), dl20(t), dl2(t), dl28(t) / kin_path.SqrtDelta, rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[30] / kin_path.W[30]), f_2_2_6(kin_path), f_1_3_4(kin_path), f_1_3_5(kin_path), dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_4_416_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_416_abbreviated(const std::array<T,58>& abb)
{
using TR = typename T::value_type;
T z[227];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[4];
z[4] = abb[3];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[55];
z[14] = abb[56];
z[15] = abb[57];
z[16] = abb[47];
z[17] = abb[16];
z[18] = abb[17];
z[19] = abb[20];
z[20] = abb[21];
z[21] = abb[24];
z[22] = abb[26];
z[23] = abb[27];
z[24] = abb[29];
z[25] = abb[31];
z[26] = abb[32];
z[27] = abb[34];
z[28] = abb[35];
z[29] = abb[37];
z[30] = abb[38];
z[31] = abb[12];
z[32] = abb[13];
z[33] = abb[14];
z[34] = abb[30];
z[35] = abb[33];
z[36] = abb[36];
z[37] = abb[39];
z[38] = abb[41];
z[39] = abb[42];
z[40] = abb[43];
z[41] = abb[44];
z[42] = abb[45];
z[43] = abb[46];
z[44] = abb[18];
z[45] = abb[23];
z[46] = abb[48];
z[47] = abb[49];
z[48] = abb[50];
z[49] = abb[51];
z[50] = abb[25];
z[51] = abb[19];
z[52] = abb[52];
z[53] = abb[53];
z[54] = abb[54];
z[55] = abb[15];
z[56] = bc<TR>[3];
z[57] = abb[40];
z[58] = bc<TR>[2];
z[59] = bc<TR>[9];
z[60] = bc<TR>[1];
z[61] = bc<TR>[4];
z[62] = z[15] + z[37];
z[63] = -z[38] + -z[39] + z[40] + z[41];
z[64] = -z[13] + z[63];
z[65] = z[35] + z[36];
z[66] = -z[42] + z[57];
z[62] = (T(37) * z[14]) / T(8) + -z[16] + (T(-5) * z[43]) / T(3) + z[62] / T(3) + (T(-5) * z[64]) / T(6) + (T(-2) * z[65]) / T(3) + (T(4) * z[66]) / T(3);
z[62] = int_to_imaginary<T>(1) * z[7] * z[62];
z[64] = z[32] + z[44];
z[67] = z[45] + z[64];
z[68] = z[50] * z[67];
z[62] = z[62] + z[68];
z[69] = -z[46] + z[49];
z[70] = (T(-5) * z[47]) / T(4) + z[48] / T(4) + z[69];
z[71] = (T(4) * z[44]) / T(3);
z[72] = (T(8) * z[45]) / T(3);
z[73] = z[9] + -z[11];
z[74] = T(7) * z[10] + T(3) * z[73];
z[74] = -z[1] + (T(-7) * z[8]) / T(8) + (T(2) * z[32]) / T(3) + (T(4) * z[33]) / T(3) + z[70] + -z[71] + -z[72] + z[74] / T(8);
z[74] = z[14] * z[74];
z[75] = -z[32] + z[44];
z[76] = (T(5) * z[47]) / T(8) + -z[48] / T(8) + -z[69] / T(2);
z[77] = T(4) * z[33];
z[78] = (T(-3) * z[1]) / T(8) + (T(7) * z[12]) / T(8) + T(-8) * z[45] + (T(-5) * z[73]) / T(8) + (T(-8) * z[75]) / T(3) + z[76] + z[77];
z[78] = z[16] * z[78];
z[79] = -z[10] + z[73];
z[79] = z[1] + z[8] / T(2) + z[79] / T(2);
z[80] = T(4) * z[45];
z[81] = z[12] / T(4);
z[71] = z[71] + -z[79] / T(4) + z[80] + z[81];
z[71] = z[15] * z[71];
z[82] = -z[33] + z[44];
z[82] = z[55] * z[82];
z[83] = z[32] + -z[33];
z[84] = z[31] * z[83];
z[79] = -z[12] + z[79];
z[85] = z[0] * z[79];
z[86] = T(-3) * z[82] + z[84] / T(4) + z[85] / T(2);
z[87] = z[8] + -z[10];
z[88] = (T(-5) * z[1]) / T(4) + z[73] / T(4) + -z[87];
z[72] = z[12] / T(8) + (T(2) * z[44]) / T(3) + z[72] + -z[76] + z[88] / T(2);
z[72] = z[13] * z[72];
z[76] = (T(7) * z[45]) / T(2);
z[88] = (T(7) * z[33]) / T(2);
z[89] = (T(23) * z[64]) / T(3) + -z[76] + z[88];
z[90] = z[39] / T(4);
z[89] = z[89] * z[90];
z[91] = z[33] / T(4);
z[92] = z[32] + z[91];
z[93] = (T(3) * z[44]) / T(2);
z[94] = z[92] + z[93];
z[95] = (T(3) * z[45]) / T(4);
z[96] = -z[94] + -z[95];
z[97] = z[35] / T(2);
z[96] = z[96] * z[97];
z[98] = z[38] / T(2);
z[99] = z[83] / T(4);
z[100] = z[44] + z[99];
z[100] = z[98] * z[100];
z[101] = z[45] / T(2);
z[102] = -z[64] + z[101];
z[103] = (T(3) * z[33]) / T(2);
z[104] = -z[102] + z[103];
z[104] = z[36] * z[104];
z[105] = (T(17) * z[33]) / T(2);
z[106] = T(5) * z[32];
z[107] = (T(13) * z[44]) / T(3) + (T(53) * z[45]) / T(6) + -z[105] + -z[106];
z[108] = z[43] / T(4);
z[107] = z[107] * z[108];
z[109] = z[33] / T(2);
z[110] = T(3) * z[44];
z[111] = -z[32] + (T(17) * z[45]) / T(2) + -z[109] + z[110];
z[112] = z[41] / T(4);
z[111] = z[111] * z[112];
z[113] = z[40] / T(4);
z[114] = z[32] + z[33];
z[115] = -z[45] + (T(5) * z[114]) / T(2);
z[115] = z[113] * z[115];
z[116] = z[33] + z[64];
z[116] = z[51] * z[116];
z[117] = (T(3) * z[116]) / T(2);
z[118] = -z[32] + z[45];
z[119] = z[34] * z[118];
z[120] = (T(3) * z[45]) / T(8);
z[121] = (T(-7) * z[32]) / T(3) + (T(-5) * z[33]) / T(8) + (T(-19) * z[44]) / T(12) + -z[120];
z[121] = z[37] * z[121];
z[122] = T(11) * z[33];
z[123] = T(9) * z[32] + z[122];
z[123] = -z[44] + z[123] / T(4);
z[123] = (T(-4) * z[45]) / T(3) + z[123] / T(2);
z[123] = z[42] * z[123];
z[124] = T(-3) * z[13] + T(-19) * z[14] + z[16];
z[124] = z[58] * z[124];
z[125] = (T(3) * z[13]) / T(2);
z[126] = T(2) * z[14] + z[15] / T(2) + z[125];
z[127] = z[60] * z[126];
z[62] = z[62] / T(3) + z[71] + z[72] + z[74] + z[78] + z[86] / T(2) + z[89] + z[96] + z[100] + z[104] / T(4) + z[107] + z[111] + z[115] + -z[117] + (T(-5) * z[119]) / T(6) + z[121] + z[123] + z[124] / T(8) + z[127];
z[62] = z[7] * z[62];
z[71] = (T(21) * z[45]) / T(2);
z[72] = T(7) * z[44];
z[74] = z[71] + z[72];
z[78] = T(3) * z[33];
z[86] = z[32] / T(2);
z[89] = z[74] + -z[78] + -z[86];
z[96] = z[39] / T(2);
z[100] = z[89] * z[96];
z[104] = z[32] + z[103];
z[107] = T(3) * z[104];
z[111] = T(5) * z[44];
z[115] = (T(15) * z[45]) / T(2);
z[121] = -z[107] + z[111] + z[115];
z[123] = z[36] / T(2);
z[121] = z[121] * z[123];
z[124] = T(2) * z[33];
z[127] = z[80] + -z[124];
z[128] = T(4) * z[44];
z[129] = T(3) * z[32];
z[130] = z[127] + z[128] + -z[129];
z[130] = z[43] * z[130];
z[131] = -z[33] + z[45];
z[132] = T(2) * z[44];
z[133] = z[129] + -z[131] + z[132];
z[133] = z[40] * z[133];
z[134] = -z[8] + z[10] + z[73];
z[69] = -z[1] + -z[12] + (T(-5) * z[47]) / T(2) + z[48] / T(2) + T(2) * z[69] + (T(5) * z[134]) / T(4);
z[134] = T(3) * z[16];
z[135] = T(-3) * z[14] + -z[134];
z[135] = z[69] * z[135];
z[136] = z[1] / T(2) + (T(-3) * z[73]) / T(2) + z[87];
z[136] = (T(3) * z[12]) / T(4) + -z[70] + z[136] / T(2);
z[137] = T(3) * z[136];
z[138] = z[15] * z[137];
z[139] = T(9) * z[33];
z[140] = -z[106] + z[139];
z[141] = (T(9) * z[45]) / T(2);
z[142] = z[44] + z[141];
z[140] = z[140] / T(4) + -z[142];
z[140] = z[37] * z[140];
z[143] = (T(13) * z[45]) / T(2);
z[144] = z[110] + z[143];
z[145] = T(-13) * z[33] + -z[129];
z[145] = z[144] + z[145] / T(4);
z[145] = z[42] * z[145];
z[146] = (T(11) * z[45]) / T(2);
z[147] = z[44] + z[146];
z[148] = (T(3) * z[32]) / T(2);
z[149] = z[33] + z[148];
z[150] = z[147] + -z[149];
z[150] = z[97] * z[150];
z[151] = (T(5) * z[33]) / T(2);
z[152] = z[32] + z[151];
z[152] = T(3) * z[152];
z[74] = -z[74] + z[152];
z[153] = z[74] * z[98];
z[154] = T(2) * z[45];
z[155] = z[44] + z[154];
z[156] = z[41] * z[155];
z[121] = -z[68] + -z[100] + (T(-7) * z[119]) / T(2) + z[121] + z[130] + z[133] + z[135] + -z[138] + z[140] + z[145] + z[150] + z[153] + T(3) * z[156];
z[121] = z[2] * z[121];
z[130] = (T(9) * z[33]) / T(2);
z[71] = -z[44] + -z[71] + z[106] + z[130];
z[106] = z[71] * z[96];
z[87] = z[1] + z[87];
z[73] = -z[73] + (T(3) * z[87]) / T(2);
z[70] = -z[70] + z[73] / T(2) + z[81];
z[73] = T(3) * z[70];
z[81] = z[13] * z[73];
z[87] = z[119] / T(2);
z[133] = z[81] + z[87];
z[135] = z[15] * z[73];
z[140] = (T(13) * z[45]) / T(4);
z[145] = z[32] + z[88];
z[145] = -z[132] + -z[140] + z[145] / T(2);
z[145] = z[40] * z[145];
z[150] = (T(3) * z[32]) / T(4) + z[33];
z[153] = T(3) * z[150];
z[156] = z[44] / T(2);
z[157] = (T(21) * z[45]) / T(4) + -z[153] + z[156];
z[157] = z[38] * z[157];
z[158] = T(13) * z[32];
z[159] = -z[139] + -z[158];
z[159] = z[142] + z[159] / T(4);
z[159] = z[37] * z[159];
z[160] = T(5) * z[33];
z[161] = z[129] + z[160];
z[162] = T(5) * z[45];
z[161] = z[161] / T(2) + -z[162];
z[161] = z[42] * z[161];
z[163] = T(2) * z[32];
z[164] = z[44] + -z[131] + z[163];
z[164] = z[35] * z[164];
z[158] = z[122] + z[158];
z[158] = -z[147] + z[158] / T(4);
z[158] = z[43] * z[158];
z[165] = z[33] + z[86];
z[166] = T(3) * z[165];
z[167] = -z[44] + -z[115] + z[166];
z[168] = z[41] / T(2);
z[167] = z[167] * z[168];
z[169] = z[69] * z[134];
z[170] = (T(5) * z[68]) / T(2);
z[171] = z[36] * z[142];
z[145] = -z[106] + z[133] + z[135] + z[145] + z[157] + z[158] + z[159] + z[161] + z[164] + z[167] + z[169] + z[170] + -z[171];
z[145] = z[3] * z[145];
z[73] = z[14] * z[73];
z[158] = z[70] * z[134];
z[159] = z[73] + z[158];
z[161] = z[77] + z[129];
z[164] = z[132] + z[154];
z[167] = z[161] + z[164];
z[167] = z[43] * z[167];
z[169] = (T(7) * z[32]) / T(2);
z[172] = z[78] + z[169];
z[172] = -z[142] + T(3) * z[172];
z[173] = z[37] / T(2);
z[172] = z[172] * z[173];
z[174] = z[144] + z[149];
z[175] = z[42] / T(2);
z[174] = z[174] * z[175];
z[176] = z[44] + z[76];
z[177] = z[165] + z[176];
z[178] = z[97] * z[177];
z[80] = z[44] + z[80];
z[161] = z[80] + -z[161];
z[161] = z[40] * z[161];
z[179] = (T(5) * z[32]) / T(2);
z[180] = z[78] + z[179];
z[181] = -z[44] + z[101];
z[182] = z[180] + -z[181];
z[183] = (T(3) * z[39]) / T(2);
z[184] = -(z[182] * z[183]);
z[185] = z[44] + T(3) * z[45];
z[186] = z[38] * z[185];
z[187] = T(2) * z[186];
z[188] = T(2) * z[68];
z[189] = z[36] * z[155];
z[190] = -z[45] + z[132];
z[190] = z[41] * z[190];
z[161] = -z[87] + z[159] + z[161] + z[167] + z[172] + z[174] + z[178] + z[184] + -z[187] + -z[188] + z[189] + z[190];
z[161] = z[20] * z[161];
z[167] = -z[75] + z[124] + -z[154];
z[167] = z[35] * z[167];
z[172] = -z[88] + z[129] + -z[147];
z[174] = z[43] / T(2);
z[172] = z[172] * z[174];
z[178] = z[88] + z[129];
z[184] = z[178] + -z[181];
z[189] = z[40] / T(2);
z[190] = z[184] * z[189];
z[134] = z[134] * z[136];
z[191] = -z[68] + z[134];
z[192] = z[32] + -z[151];
z[192] = z[142] + T(3) * z[192];
z[192] = z[173] * z[192];
z[193] = (T(5) * z[45]) / T(2);
z[194] = -z[32] + -z[124] + -z[193];
z[194] = z[42] * z[194];
z[151] = -z[75] + z[151];
z[195] = -z[101] + z[151];
z[183] = z[183] * z[195];
z[196] = T(4) * z[119];
z[197] = z[44] + -z[45];
z[197] = z[36] * z[197];
z[198] = -(z[41] * z[80]);
z[167] = z[167] + z[172] + z[183] + z[187] + -z[190] + -z[191] + z[192] + z[194] + z[196] + z[197] + z[198];
z[167] = z[18] * z[167];
z[95] = -z[95] + z[128];
z[128] = T(11) * z[32] + z[130];
z[128] = z[95] + z[128] / T(2);
z[128] = z[40] * z[128];
z[130] = (T(5) * z[44]) / T(2);
z[172] = (T(9) * z[45]) / T(4);
z[183] = z[32] + (T(-9) * z[33]) / T(4) + z[130] + -z[172];
z[183] = z[37] * z[183];
z[187] = z[75] + -z[103];
z[192] = (T(25) * z[45]) / T(2);
z[194] = T(3) * z[187] + z[192];
z[194] = z[123] * z[194];
z[197] = -z[44] + z[103];
z[197] = T(3) * z[197];
z[192] = z[192] + -z[197];
z[192] = z[175] * z[192];
z[198] = z[13] * z[137];
z[197] = z[143] + z[197];
z[199] = z[168] * z[197];
z[137] = z[14] * z[137];
z[67] = z[43] * z[67];
z[67] = -z[67] + (T(-7) * z[68]) / T(2) + z[128] + z[134] + z[137] + z[183] + z[192] + z[194] + z[198] + z[199];
z[128] = z[23] * z[67];
z[183] = z[40] * z[64];
z[192] = -z[68] + z[183];
z[194] = z[13] + z[14];
z[69] = z[69] * z[194];
z[194] = z[32] + z[160];
z[194] = z[194] / T(4);
z[198] = z[44] + z[193];
z[199] = z[194] + -z[198];
z[199] = z[42] * z[199];
z[200] = T(7) * z[32];
z[201] = z[160] + z[200];
z[202] = z[198] + -z[201] / T(4);
z[202] = z[43] * z[202];
z[203] = (T(3) * z[45]) / T(2);
z[194] = z[44] + -z[194] + z[203];
z[194] = z[41] * z[194];
z[204] = (T(3) * z[119]) / T(2);
z[205] = z[35] * z[64];
z[206] = z[37] * z[64];
z[207] = -z[45] + (T(5) * z[114]) / T(4);
z[207] = z[36] * z[207];
z[69] = z[69] + -z[192] + z[194] + z[199] + z[202] + -z[204] + -z[205] + T(-2) * z[206] + z[207];
z[194] = T(3) * z[4];
z[69] = z[69] * z[194];
z[199] = z[98] + -z[174];
z[151] = z[101] + z[151];
z[199] = z[151] * z[199];
z[202] = -z[96] + z[175];
z[195] = z[195] * z[202];
z[102] = z[102] + z[109];
z[102] = z[102] * z[189];
z[202] = z[101] + z[187];
z[207] = z[123] * z[202];
z[208] = z[14] * z[136];
z[209] = z[15] * z[136];
z[102] = -z[102] + -z[195] + z[199] + z[207] + z[208] + -z[209];
z[102] = T(3) * z[102];
z[195] = -(z[30] * z[102]);
z[199] = z[45] / T(4);
z[150] = z[150] + z[156] + z[199];
z[207] = z[38] * z[150];
z[210] = z[14] * z[70];
z[211] = z[44] + z[101];
z[212] = z[149] + z[211];
z[213] = z[97] * z[212];
z[207] = z[207] + -z[210] + -z[213];
z[150] = z[42] * z[150];
z[213] = z[15] * z[70];
z[214] = z[13] * z[70];
z[215] = -z[165] + z[181];
z[215] = z[168] * z[215];
z[150] = -z[150] + z[207] + -z[213] + -z[214] + z[215];
z[215] = -(z[174] * z[182]);
z[215] = z[150] + z[215];
z[215] = z[28] * z[215];
z[216] = z[44] + z[131];
z[217] = z[43] * z[216];
z[217] = -z[82] + z[217];
z[218] = -z[36] + -z[42];
z[218] = z[45] * z[218];
z[218] = z[68] + -z[206] + z[217] + z[218];
z[218] = z[17] * z[218];
z[116] = z[116] + z[119] + z[206];
z[219] = z[41] + z[43];
z[220] = z[44] + z[45];
z[219] = z[219] * z[220];
z[221] = z[33] + -z[118];
z[221] = z[42] * z[221];
z[219] = -z[116] + z[219] + z[221];
z[219] = z[19] * z[219];
z[221] = z[58] * z[126];
z[222] = (T(-3) * z[13]) / T(4) + -z[14] + -z[15] / T(4);
z[222] = z[60] * z[222];
z[221] = z[221] + z[222];
z[221] = z[60] * z[221];
z[126] = z[61] * z[126];
z[126] = -z[126] + z[215] + z[218] + z[219] + z[221];
z[215] = (T(11) * z[32]) / T(2) + z[72];
z[218] = -z[78] + -z[203] + z[215];
z[218] = z[35] * z[218];
z[219] = z[33] + z[179];
z[221] = (T(23) * z[45]) / T(2) + z[72];
z[222] = T(3) * z[219] + -z[221];
z[222] = z[43] * z[222];
z[223] = z[72] + T(3) * z[149];
z[224] = z[146] + z[223];
z[224] = z[36] * z[224];
z[225] = z[78] + z[141];
z[215] = z[215] + z[225];
z[215] = z[37] * z[215];
z[226] = z[166] + -z[221];
z[226] = z[41] * z[226];
z[118] = z[42] * z[118];
z[118] = z[118] + z[215] + z[218] + z[222] + -z[224] + z[226];
z[81] = -z[81] + z[158];
z[100] = -z[81] + z[100] + z[118] / T(2) + z[196];
z[118] = z[26] * z[100];
z[158] = z[78] + z[163];
z[215] = T(6) * z[45] + z[132] + -z[158];
z[215] = z[16] * z[215];
z[218] = z[15] * z[185];
z[215] = z[215] + -z[218];
z[127] = -z[32] + z[127] + z[132];
z[127] = z[14] * z[127];
z[80] = z[13] * z[80];
z[80] = z[80] + -z[127] + T(-2) * z[215];
z[127] = z[53] + z[54];
z[80] = z[80] * z[127];
z[71] = z[23] * z[71];
z[127] = z[28] * z[182];
z[71] = z[71] + T(3) * z[127];
z[71] = z[71] * z[96];
z[127] = T(-15) * z[13] + T(-7) * z[14] + z[16];
z[127] = prod_pow(z[58], 2) * z[127];
z[69] = z[69] + z[71] + z[80] + z[118] + z[121] + T(3) * z[126] + z[127] / T(4) + z[128] + z[145] + z[161] + z[167] + z[195];
z[69] = int_to_imaginary<T>(1) * z[69];
z[63] = -z[37] + T(5) * z[43] + (T(5) * z[63]) / T(2) + T(2) * z[65] + T(-4) * z[66];
z[63] = z[56] * z[63];
z[62] = z[62] + T(3) * z[63] + z[69];
z[62] = z[7] * z[62];
z[63] = (T(3) * z[214]) / T(2);
z[65] = z[89] * z[90];
z[66] = z[84] / T(2) + z[85];
z[69] = z[14] * z[79];
z[71] = z[66] + z[69];
z[80] = (T(3) * z[209]) / T(2);
z[74] = z[38] * z[74];
z[84] = z[78] + -z[86];
z[85] = z[37] * z[84];
z[89] = z[42] * z[201];
z[90] = -z[32] + (T(-13) * z[33]) / T(2) + z[72] + z[76];
z[90] = z[90] * z[97];
z[118] = -z[76] + -z[223];
z[118] = z[118] * z[123];
z[121] = T(7) * z[33];
z[126] = -z[121] + z[129];
z[126] = z[108] * z[126];
z[127] = z[166] + z[181];
z[128] = z[112] * z[127];
z[145] = z[113] * z[184];
z[65] = z[63] + z[65] + (T(3) * z[71]) / T(4) + z[74] / T(4) + -z[80] + z[85] + z[89] / T(8) + z[90] + z[118] + -z[119] + z[126] + z[128] + z[145];
z[65] = z[2] * z[65];
z[71] = z[124] + z[148];
z[85] = z[154] + z[156];
z[89] = z[71] + -z[85];
z[89] = z[40] * z[89];
z[90] = (T(7) * z[44]) / T(2);
z[118] = -z[90] + -z[131] + -z[179];
z[118] = z[35] * z[118];
z[115] = z[110] + z[115];
z[119] = (T(9) * z[32]) / T(2);
z[121] = z[115] + -z[119] + -z[121];
z[121] = z[121] * z[174];
z[126] = (T(3) * z[165]) / T(2);
z[128] = (T(7) * z[45]) / T(4);
z[145] = z[44] + z[128];
z[161] = z[126] + -z[145];
z[161] = z[41] * z[161];
z[158] = z[158] + -z[185];
z[158] = z[39] * z[158];
z[166] = z[158] + z[186];
z[167] = -z[32] + -z[78] + -z[110];
z[167] = z[37] * z[167];
z[184] = -z[115] + z[165];
z[184] = z[175] * z[184];
z[128] = z[128] + z[132];
z[185] = z[128] + (T(3) * z[149]) / T(2);
z[185] = z[36] * z[185];
z[118] = -z[73] + -z[89] + z[118] + z[121] + -z[133] + -z[161] + z[166] + z[167] + z[184] + z[185];
z[118] = z[19] * z[118];
z[121] = z[166] + z[196];
z[91] = -z[91] + z[145];
z[91] = z[40] * z[91];
z[145] = z[41] * z[85];
z[167] = -z[78] + z[163];
z[184] = z[110] + z[167];
z[184] = z[37] * z[184];
z[185] = (T(13) * z[33]) / T(4) + z[163];
z[195] = (T(15) * z[45]) / T(4) + z[93] + -z[185];
z[195] = z[42] * z[195];
z[128] = -z[128] + z[185];
z[128] = z[35] * z[128];
z[115] = z[109] + -z[115];
z[115] = z[115] * z[174];
z[185] = z[45] + z[90];
z[185] = z[36] * z[185];
z[115] = -z[91] + z[115] + z[121] + z[128] + z[137] + -z[145] + z[184] + z[185] + z[195];
z[115] = z[18] * z[115];
z[128] = z[111] + z[169] + z[225];
z[128] = z[96] * z[128];
z[184] = z[71] + z[164];
z[184] = z[43] * z[184];
z[185] = z[45] + z[156];
z[195] = z[36] * z[185];
z[89] = z[89] + -z[195];
z[196] = z[142] + -z[180];
z[196] = z[173] * z[196];
z[87] = z[87] + z[89] + z[196];
z[196] = (T(11) * z[45]) / T(4);
z[201] = z[44] + z[196];
z[209] = z[165] / T(2) + -z[201];
z[209] = z[35] * z[209];
z[144] = z[144] + -z[149];
z[144] = z[144] * z[175];
z[218] = z[45] + z[130];
z[218] = z[41] * z[218];
z[128] = z[87] + z[128] + -z[144] + -z[159] + -z[184] + z[186] + z[209] + -z[218];
z[128] = z[20] * z[128];
z[65] = z[65] + z[115] + z[118] + -z[128];
z[65] = z[2] * z[65];
z[115] = (T(3) * z[33]) / T(4);
z[118] = z[115] + -z[156] + z[163] + -z[172];
z[118] = z[37] * z[118];
z[92] = -z[92] + z[156] + z[196];
z[92] = z[43] * z[92];
z[144] = -z[64] + z[103];
z[159] = z[141] + z[144];
z[159] = z[96] * z[159];
z[184] = z[159] + z[186];
z[185] = -z[165] + z[185];
z[185] = z[35] * z[185];
z[186] = z[149] + z[193];
z[186] = z[42] * z[186];
z[196] = z[44] + T(7) * z[45];
z[196] = z[123] * z[196];
z[154] = z[154] + -z[156];
z[209] = z[41] * z[154];
z[218] = (T(5) * z[45]) / T(4);
z[94] = z[94] + z[218];
z[94] = z[40] * z[94];
z[92] = z[92] + z[94] + z[118] + z[134] + -z[170] + -z[184] + -z[185] + z[186] + z[196] + z[209];
z[92] = z[17] * z[92];
z[81] = -z[81] + z[135];
z[94] = -z[153] + -z[156] + z[172];
z[94] = z[38] * z[94];
z[94] = z[81] + z[94] + -z[158];
z[118] = T(4) * z[32];
z[88] = z[88] + z[118] + -z[193];
z[88] = z[42] * z[88];
z[76] = -z[76] + -z[156] + z[178];
z[76] = z[35] * z[76];
z[135] = -z[124] + z[154] + -z[179];
z[153] = -(z[40] * z[135]);
z[84] = -z[84] + z[142];
z[84] = z[84] * z[173];
z[170] = -z[147] + T(5) * z[165];
z[170] = z[170] * z[174];
z[172] = -z[44] + z[165];
z[172] = T(3) * z[172] + -z[193];
z[172] = z[168] * z[172];
z[76] = z[76] + z[84] + z[88] + z[94] + z[153] + z[170] + z[172] + -z[188] + z[195];
z[76] = z[20] * z[76];
z[84] = -(z[83] * z[173]);
z[88] = -z[165] + z[198];
z[153] = z[35] * z[88];
z[170] = z[187] + z[193];
z[172] = -(z[36] * z[170]);
z[179] = -z[165] + -z[181];
z[179] = z[41] * z[179];
z[181] = -(z[42] * z[129]);
z[186] = -(z[32] * z[43]);
z[195] = z[16] * z[79];
z[84] = z[66] + z[84] + z[153] + z[172] + z[179] + z[181] + z[186] + z[195];
z[115] = -z[32] + -z[115] + -z[156] + z[199];
z[115] = z[40] * z[115];
z[84] = z[68] + z[84] / T(2) + z[115] + -z[214];
z[84] = z[84] * z[194];
z[115] = -z[139] + -z[200];
z[115] = z[115] / T(2) + z[142];
z[115] = z[35] * z[115];
z[139] = -(z[37] * z[119]);
z[153] = -(z[32] * z[42]);
z[156] = z[43] * z[169];
z[90] = z[41] * z[90];
z[90] = (T(9) * z[68]) / T(2) + z[90] + z[115] + z[139] + z[153] + z[156] + -z[171] + (T(-7) * z[183]) / T(2);
z[90] = z[3] * z[90];
z[115] = z[145] + z[185];
z[139] = z[141] + z[187];
z[139] = z[139] * z[173];
z[139] = -z[115] + z[139] + -z[191];
z[109] = z[109] + -z[147];
z[109] = z[109] * z[174];
z[145] = z[165] + z[193];
z[145] = z[42] * z[145];
z[153] = z[36] * z[154];
z[91] = z[91] + -z[109] + -z[139] + z[145] + z[153] + -z[184];
z[91] = z[18] * z[91];
z[109] = z[35] * z[135];
z[119] = -z[119] + z[147] + -z[160];
z[119] = z[119] * z[174];
z[135] = -z[178] + z[193];
z[135] = z[42] * z[135];
z[87] = -z[87] + -z[94] + z[109] + z[119] + z[135] + -z[161];
z[87] = z[19] * z[87];
z[94] = z[114] * z[173];
z[94] = z[66] + z[94];
z[109] = -(z[35] * z[177]);
z[119] = -z[107] + z[176];
z[119] = z[36] * z[119];
z[135] = z[42] * z[200];
z[109] = T(-3) * z[94] + z[109] + z[119] + z[135];
z[119] = z[127] * z[168];
z[127] = (T(3) * z[16]) / T(2);
z[135] = -(z[79] * z[127]);
z[145] = z[43] * z[129];
z[109] = -z[68] + z[109] / T(2) + z[119] + z[133] + z[135] + z[145] + z[190];
z[109] = z[2] * z[109];
z[76] = z[76] + z[84] + z[87] + z[90] / T(2) + -z[91] + z[92] + z[109];
z[76] = z[3] * z[76];
z[84] = -z[125] + -z[127];
z[84] = z[84] * z[136];
z[64] = -z[64] + -z[103] + -z[146];
z[64] = z[64] * z[108];
z[87] = T(-3) * z[114] + -z[162];
z[90] = z[42] / T(4);
z[87] = z[87] * z[90];
z[92] = z[38] * z[151];
z[109] = -(z[112] * z[197]);
z[112] = (T(-5) * z[44]) / T(4) + -z[103] + -z[163];
z[112] = z[40] * z[112];
z[119] = z[33] + z[45];
z[119] = z[32] + z[44] / T(4) + (T(9) * z[119]) / T(8);
z[119] = z[37] * z[119];
z[125] = -(z[36] * z[218]);
z[64] = z[64] + z[68] / T(4) + -z[80] + (T(3) * z[82]) / T(2) + z[84] + z[87] + (T(3) * z[92]) / T(4) + z[109] + z[112] + z[119] + z[125] + z[159];
z[64] = z[17] * z[64];
z[68] = z[77] + z[148] + -z[164];
z[68] = z[43] * z[68];
z[80] = -z[130] + -z[131] + -z[148];
z[80] = z[40] * z[80];
z[82] = z[93] + z[140];
z[84] = z[32] + (T(11) * z[33]) / T(4) + -z[82];
z[84] = z[42] * z[84];
z[87] = -z[141] + z[152];
z[92] = -z[87] + z[111];
z[92] = z[92] * z[98];
z[98] = (T(3) * z[104]) / T(2) + -z[201];
z[98] = z[36] * z[98];
z[68] = z[68] + z[80] + z[84] + z[92] + z[98] + -z[137] + z[138] + z[139] + -z[158];
z[68] = z[2] * z[68];
z[80] = z[123] + -z[174];
z[80] = z[80] * z[170];
z[84] = -z[103] + z[198];
z[84] = z[84] * z[175];
z[92] = z[103] + -z[211];
z[92] = z[92] * z[168];
z[98] = z[13] * z[136];
z[80] = z[80] + z[84] + z[92] + z[98] + z[192] + z[206] + z[208];
z[80] = z[80] * z[194];
z[84] = z[89] + z[115];
z[78] = -z[78] + -z[118];
z[78] = z[37] * z[78];
z[71] = -(z[43] * z[71]);
z[89] = -(z[42] * z[149]);
z[71] = z[71] + z[78] + z[84] + z[89] + z[166] + z[188];
z[71] = z[20] * z[71];
z[64] = z[64] + z[68] + z[71] + z[80] + z[91];
z[64] = z[17] * z[64];
z[67] = -z[67] + -z[106];
z[67] = z[22] * z[67];
z[68] = z[88] * z[168];
z[68] = z[68] + -z[214];
z[71] = z[68] + -z[204];
z[78] = -(z[90] * z[114]);
z[80] = z[101] + z[110];
z[89] = -z[32] + -z[80] + z[103];
z[89] = z[89] * z[97];
z[80] = z[80] + z[149];
z[80] = z[80] * z[123];
z[90] = z[33] + -z[200];
z[90] = z[90] * z[108];
z[91] = -(z[170] * z[189]);
z[92] = -(z[32] * z[37]);
z[79] = z[79] / T(2);
z[98] = -(z[14] * z[79]);
z[78] = z[71] + z[78] + z[80] + z[89] + z[90] + z[91] + z[92] + z[98];
z[78] = z[2] * z[78];
z[80] = z[88] * z[175];
z[89] = -(z[123] * z[212]);
z[90] = -z[198] + z[219];
z[90] = z[90] * z[174];
z[71] = -z[71] + z[80] + z[89] + z[90] + z[205] + z[206] + z[210];
z[71] = z[19] * z[71];
z[80] = z[83] * z[175];
z[80] = -z[69] + z[80];
z[89] = z[33] + z[129];
z[90] = -(z[89] * z[173]);
z[90] = -z[66] + -z[80] + z[90];
z[91] = z[13] * z[79];
z[79] = z[16] * z[79];
z[91] = -z[79] + z[91];
z[92] = -(z[89] * z[108]);
z[98] = z[45] + z[83] / T(2);
z[98] = z[98] * z[168];
z[101] = -z[45] + z[89] / T(2);
z[101] = z[101] * z[189];
z[103] = z[36] * z[44];
z[90] = z[90] / T(2) + z[91] + z[92] + z[98] + z[101] + -z[103] + z[205];
z[90] = z[4] * z[90];
z[92] = -z[97] + z[174];
z[88] = z[88] * z[92];
z[92] = z[149] + -z[198];
z[92] = z[92] * z[175];
z[98] = z[165] + z[211];
z[98] = z[98] * z[189];
z[101] = z[37] * z[44];
z[104] = -(z[41] * z[44]);
z[88] = z[88] + z[92] + z[98] + -z[101] + z[104] + -z[210];
z[88] = z[20] * z[88];
z[92] = -z[175] + z[189];
z[92] = z[92] * z[170];
z[97] = z[97] * z[202];
z[98] = -z[144] + z[193];
z[98] = z[98] * z[174];
z[92] = z[92] + z[97] + z[98] + -z[101] + -z[103] + -z[208];
z[92] = z[18] * z[92];
z[71] = z[71] + z[78] + z[88] + z[90] / T(2) + z[92];
z[71] = z[71] * z[194];
z[78] = -z[86] + z[198];
z[78] = z[78] * z[175];
z[86] = z[180] + -z[198];
z[86] = z[86] * z[174];
z[88] = -(z[96] * z[182]);
z[68] = -z[68] + z[78] + z[86] + z[88] + z[116] + -z[207] + z[213];
z[68] = z[19] * z[68];
z[68] = (T(3) * z[68]) / T(2) + z[128];
z[68] = z[19] * z[68];
z[73] = -z[73] + z[157];
z[78] = T(3) * z[180];
z[86] = z[78] + z[111] + -z[141];
z[86] = z[86] * z[173];
z[88] = (T(19) * z[32]) / T(2) + z[110] + z[122] + -z[143];
z[90] = -(z[88] * z[189]);
z[77] = (T(-25) * z[45]) / T(4) + z[77] + -z[93];
z[92] = (T(-19) * z[32]) / T(4) + -z[77];
z[92] = z[42] * z[92];
z[77] = (T(-13) * z[32]) / T(4) + -z[77];
z[77] = z[35] * z[77];
z[75] = -z[75] + -z[131];
z[75] = z[43] * z[75];
z[93] = z[95] + -z[126];
z[93] = z[41] * z[93];
z[75] = -z[73] + z[75] + z[77] + -z[81] + z[86] + z[90] + z[92] + z[93];
z[75] = z[21] * z[75];
z[77] = z[78] + -z[142];
z[77] = z[77] * z[173];
z[78] = -z[33] + z[82];
z[81] = (T(-7) * z[32]) / T(4) + z[78];
z[81] = z[42] * z[81];
z[78] = -z[32] / T(4) + z[78];
z[78] = z[35] * z[78];
z[73] = -z[73] + z[77] + z[78] + z[81];
z[77] = -z[120] + z[132] + (T(-3) * z[165]) / T(4);
z[77] = z[41] * z[77];
z[70] = z[70] * z[127];
z[78] = -(z[88] * z[113]);
z[81] = z[114] / T(2) + z[220];
z[81] = z[43] * z[81];
z[63] = -z[63] + z[70] + z[73] / T(2) + z[77] + z[78] + z[81] + -z[117] + (T(-3) * z[213]) / T(2);
z[63] = prod_pow(z[20], 2) * z[63];
z[70] = z[29] * z[102];
z[73] = -z[96] + z[174];
z[73] = z[73] * z[182];
z[73] = z[73] + -z[150];
z[73] = z[27] * z[73];
z[77] = -z[173] + z[175];
z[77] = z[77] * z[89];
z[78] = z[83] * z[174];
z[66] = -z[66] + z[69] + z[77] + z[78];
z[69] = z[99] + z[211];
z[77] = -z[40] + z[41];
z[77] = z[69] * z[77];
z[66] = z[66] / T(2) + z[77] + z[91];
z[66] = z[6] * z[66];
z[77] = -z[80] + z[94];
z[78] = z[35] + -z[36];
z[69] = z[69] * z[78];
z[78] = -(z[108] * z[114]);
z[69] = z[69] + z[77] / T(2) + z[78] + z[79];
z[69] = z[5] * z[69];
z[66] = z[66] + z[69] + z[73];
z[69] = z[72] + -z[105] + -z[129] + z[146];
z[69] = z[35] * z[69];
z[73] = -z[72] + -z[107] + z[203];
z[73] = z[36] * z[73];
z[72] = -z[72] + z[87];
z[72] = z[37] * z[72];
z[77] = z[105] + -z[221];
z[78] = -z[77] + z[129];
z[78] = z[43] * z[78];
z[79] = -(z[42] * z[131]);
z[69] = z[69] + z[72] + z[73] + z[74] + z[78] + z[79];
z[72] = -z[77] + -z[129];
z[72] = z[72] * z[189];
z[69] = z[69] / T(2) + z[72] + z[134] + -z[138];
z[69] = z[24] * z[69];
z[72] = z[25] * z[100];
z[73] = -(z[37] * z[167]);
z[74] = z[124] + -z[148];
z[74] = z[43] * z[74];
z[77] = z[42] * z[165];
z[73] = z[73] + z[74] + z[77] + z[84] + -z[121];
z[73] = z[19] * z[73];
z[74] = -(z[42] * z[45]);
z[77] = z[40] * z[216];
z[74] = z[74] + z[77] + -z[101] + z[217];
z[74] = z[18] * z[74];
z[73] = z[73] + (T(3) * z[74]) / T(2);
z[73] = z[18] * z[73];
z[74] = -z[155] + z[165];
z[74] = z[14] * z[74];
z[77] = z[13] * z[85];
z[74] = z[74] + z[77] + -z[215];
z[74] = z[52] * z[74];
z[77] = T(25) * z[14] + T(-23) * z[16];
z[77] = (T(-23) * z[13]) / T(48) + (T(-8) * z[15]) / T(3) + (T(7) * z[77]) / T(16);
z[77] = z[59] * z[77];
return z[62] + z[63] + z[64] + z[65] + T(3) * z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[74] + z[75] + z[76] + z[77];
}



template IntegrandConstructorType<double> f_4_416_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_416_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_416_construct (const Kin<qd_real>&);
#endif

}