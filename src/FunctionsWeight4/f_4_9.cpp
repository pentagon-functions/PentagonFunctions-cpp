#include "f_4_9.h"

namespace PentagonFunctions {

template <typename T> T f_4_9_abbreviated (const std::array<T,8>&);



template <typename T> IntegrandConstructorType<T> f_4_9_construct (const Kin<T>& kin) {
    return [&kin, 
            dl18 = DLog_W_18<T>(kin),dl1 = DLog_W_1<T>(kin),dl6 = DLog_W_6<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,8> abbr = 
            {dl18(t), rlog(kin.W[0] / kin_path.W[0]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(kin.W[5] / kin_path.W[5]), dl1(t), rlog(v_path[0]), f_2_1_9(kin_path), dl6(t)}
;

        auto result = f_4_9_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_9_abbreviated(const std::array<T,8>& abb)
{
using TR = typename T::value_type;
T z[23];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[7];
z[7] = abb[5];
z[8] = abb[6];
z[9] = bc<TR>[1];
z[10] = bc<TR>[2];
z[11] = bc<TR>[4];
z[12] = bc<TR>[7];
z[13] = bc<TR>[8];
z[14] = bc<TR>[9];
z[15] = int_to_imaginary<T>(1) * z[3];
z[16] = z[2] / T(2) + -z[15];
z[16] = z[2] * z[16];
z[17] = z[7] / T(2) + -z[15];
z[17] = z[7] * z[17];
z[18] = prod_pow(z[3], 2);
z[19] = (T(13) * z[18]) / T(12);
z[20] = z[8] / T(2);
z[21] = z[16] + T(-3) * z[17] + z[19] + z[20];
z[21] = z[5] * z[21];
z[22] = -z[2] + T(2) * z[15];
z[22] = z[2] * z[22];
z[22] = z[19] + z[22];
z[22] = z[0] * z[22];
z[17] = z[17] + z[20];
z[20] = -z[16] + z[17];
z[20] = z[6] * z[20];
z[21] = z[20] + z[21] + -z[22];
z[21] = z[1] * z[21];
z[16] = z[16] + z[17] + -z[19];
z[16] = z[5] * z[16];
z[16] = z[16] + z[20] + z[22];
z[16] = z[4] * z[16];
z[17] = prod_pow(z[10], 2);
z[17] = z[17] + z[18] / T(4);
z[18] = z[10] / T(3);
z[17] = z[17] * z[18];
z[18] = -z[12] + -z[13] / T(2) + (T(7) * z[14]) / T(4);
z[20] = z[11] * z[15];
z[17] = z[17] + z[18] / T(2) + -z[20];
z[18] = z[9] / T(6) + -z[15];
z[18] = z[9] * z[18];
z[18] = z[18] + -z[19];
z[18] = z[9] * z[18];
z[18] = z[17] + z[18];
z[18] = z[6] * z[18];
z[15] = -z[9] / T(2) + z[15];
z[15] = z[9] * z[15];
z[15] = z[15] + z[19];
z[15] = z[9] * z[15];
z[15] = z[15] + z[17];
z[15] = z[5] * z[15];
return z[15] + z[16] + z[18] + z[21];
}



template IntegrandConstructorType<double> f_4_9_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_9_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_9_construct (const Kin<qd_real>&);
#endif

}