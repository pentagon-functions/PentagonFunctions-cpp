#include "f_4_244.h"

namespace PentagonFunctions {

template <typename T> T f_4_244_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_244_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_244_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]);
c[1] = kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]);
c[2] = (-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[4] * (T(2) + kin.v[4]);
c[3] = (-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[4] * (T(2) + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2] + abb[6] * c[3]);
        }

        return abb[0] * (-(prod_pow(abb[2], 2) * abb[3]) + prod_pow(abb[2], 2) * (abb[5] + abb[6] + -abb[4]) + prod_pow(abb[1], 2) * (abb[3] + abb[4] + -abb[5] + -abb[6]));
    }
};
template <typename T> class SpDLog_f_4_244_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_244_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[1] = kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[2] = kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]);
c[3] = kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[10] * c[1] + abb[5] * c[2] + abb[6] * c[3]);
        }

        return abb[7] * (abb[4] * prod_pow(abb[9], 2) + prod_pow(abb[9], 2) * (abb[10] + -abb[5] + -abb[6]) + prod_pow(abb[8], 2) * (abb[5] + abb[6] + -abb[4] + -abb[10]));
    }
};
template <typename T> class SpDLog_f_4_244_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_244_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (-kin.v[3] + T(-4) + T(2) * kin.v[0] + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[0] + T(1) + kin.v[3])) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(4) + T(-2) * kin.v[4]) + ((T(-3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + kin.v[2] * (kin.v[2] / T(2) + T(-4) + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]));
c[1] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + T(2) * kin.v[4];
c[2] = kin.v[1] * (-kin.v[3] + T(-4) + T(2) * kin.v[0] + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[0] + T(1) + kin.v[3])) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(4) + T(-2) * kin.v[4]) + ((T(-3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + kin.v[2] * (kin.v[2] / T(2) + T(-4) + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]));
c[3] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + T(2) * kin.v[4];
c[4] = kin.v[1] * (T(4) + T(-2) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[0]) + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + T(-2) * kin.v[2] + kin.v[3]) + (-kin.v[2] / T(2) + -kin.v[4] + T(4)) * kin.v[2] + ((T(3) * kin.v[4]) / T(2) + T(-4)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]));
c[5] = (bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4]);
c[6] = kin.v[1] * (T(4) + T(-2) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[0]) + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + T(-2) * kin.v[2] + kin.v[3]) + (-kin.v[2] / T(2) + -kin.v[4] + T(4)) * kin.v[2] + ((T(3) * kin.v[4]) / T(2) + T(-4)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]));
c[7] = (bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[10] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[3] * (t * c[4] + c[5]) + abb[6] * (t * c[6] + c[7]);
        }

        return abb[21] * (abb[9] * abb[12] * (abb[5] + abb[10] + -abb[6]) + abb[8] * abb[9] * (abb[6] + -abb[5] + -abb[10]) + abb[2] * (abb[3] * abb[9] + abb[9] * (abb[6] + -abb[5] + -abb[10])) + abb[9] * (abb[9] * (abb[6] + -abb[5] + -abb[10]) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[1] * (abb[8] * (abb[5] + abb[10] + -abb[6]) + abb[9] * (abb[5] + abb[10] + -abb[6]) + abb[2] * (abb[5] + abb[10] + -abb[3] + -abb[6]) + abb[12] * (abb[6] + -abb[5] + -abb[10]) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[3] * (abb[12] + -abb[8] + -abb[9] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[22] * (abb[5] * T(-2) + abb[10] * T(-2) + abb[6] * T(2)) + abb[3] * (abb[8] * abb[9] + -(abb[9] * abb[12]) + abb[9] * (abb[9] + bc<T>[0] * int_to_imaginary<T>(1)) + abb[22] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_244_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl21 = DLog_W_21<T>(kin),dl24 = DLog_W_24<T>(kin),dl25 = DLog_W_25<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl22 = DLog_W_22<T>(kin),dl5 = DLog_W_5<T>(kin),dl31 = DLog_W_31<T>(kin),dl4 = DLog_W_4<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),spdl23 = SpDLog_f_4_244_W_23<T>(kin),spdl21 = SpDLog_f_4_244_W_21<T>(kin),spdl22 = SpDLog_f_4_244_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_3(kin) - f_1_3_3(kin_path), dl24(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl25(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t), f_2_1_15(kin_path), dl1(t), dl22(t), f_2_1_14(kin_path), dl5(t), f_2_1_8(kin_path), dl31(t), dl4(t), dl3(t), dl18(t), dl19(t), dl20(t), dl16(t), dl17(t), dl30(t) / kin_path.SqrtDelta, rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl29(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_244_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_244_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[153];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[23];
z[3] = abb[26];
z[4] = abb[27];
z[5] = abb[28];
z[6] = abb[29];
z[7] = abb[31];
z[8] = abb[4];
z[9] = abb[30];
z[10] = abb[32];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[10];
z[14] = abb[33];
z[15] = abb[34];
z[16] = abb[35];
z[17] = abb[36];
z[18] = abb[37];
z[19] = abb[38];
z[20] = abb[39];
z[21] = abb[40];
z[22] = abb[41];
z[23] = abb[43];
z[24] = abb[45];
z[25] = abb[2];
z[26] = abb[8];
z[27] = abb[9];
z[28] = abb[12];
z[29] = bc<TR>[0];
z[30] = abb[42];
z[31] = abb[44];
z[32] = abb[11];
z[33] = abb[20];
z[34] = abb[15];
z[35] = abb[18];
z[36] = abb[13];
z[37] = abb[14];
z[38] = abb[16];
z[39] = abb[17];
z[40] = abb[19];
z[41] = abb[22];
z[42] = abb[24];
z[43] = abb[25];
z[44] = bc<TR>[3];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = int_to_imaginary<T>(1) * z[29];
z[50] = -z[28] + z[49];
z[51] = -z[25] + -z[50];
z[52] = T(2) * z[51];
z[53] = -z[26] + z[52];
z[53] = z[26] * z[53];
z[54] = z[25] * z[49];
z[55] = T(2) * z[54];
z[53] = z[53] + -z[55];
z[56] = z[39] * z[49];
z[57] = -z[38] + z[56];
z[58] = prod_pow(z[27], 2);
z[59] = prod_pow(z[29], 2);
z[60] = z[59] / T(6);
z[61] = z[37] * z[49];
z[62] = T(2) * z[61];
z[63] = T(2) * z[41];
z[64] = z[36] + -z[40];
z[65] = -(z[28] * z[49]);
z[66] = T(2) * z[26];
z[67] = z[0] * z[66];
z[64] = z[53] + z[57] + -z[58] + z[60] + z[62] + -z[63] + T(2) * z[64] + z[65] + z[67];
z[64] = z[6] * z[64];
z[65] = T(-2) * z[57];
z[67] = T(2) * z[42];
z[68] = z[65] + -z[67];
z[69] = -z[53] + z[68];
z[70] = prod_pow(z[25], 2);
z[71] = z[70] / T(2);
z[72] = z[36] + z[71];
z[73] = z[61] + z[72];
z[74] = z[59] / T(3);
z[75] = z[73] + z[74];
z[76] = z[28] / T(2);
z[77] = -z[25] + z[76];
z[78] = -z[49] + -z[77];
z[78] = z[28] * z[78];
z[79] = T(2) * z[25];
z[80] = -z[0] + z[79];
z[80] = z[0] * z[80];
z[81] = z[0] + -z[25];
z[82] = z[27] + T(-2) * z[81];
z[82] = z[27] * z[82];
z[78] = z[63] + -z[69] + z[75] + z[78] + -z[80] + z[82];
z[78] = z[10] * z[78];
z[82] = z[28] * z[50];
z[83] = prod_pow(z[26], 2);
z[82] = z[57] + (T(5) * z[59]) / T(6) + z[82] + z[83];
z[82] = z[34] * z[82];
z[84] = z[25] + -z[49] + z[76];
z[84] = z[28] * z[84];
z[85] = (T(3) * z[70]) / T(2);
z[86] = (T(2) * z[59]) / T(3);
z[84] = -z[36] + -z[61] + z[84] + -z[85] + -z[86];
z[84] = z[32] * z[84];
z[87] = T(2) * z[40];
z[88] = z[63] + z[87];
z[89] = -z[0] + z[66];
z[89] = z[0] * z[89];
z[89] = -z[74] + -z[88] + z[89];
z[58] = -z[58] + z[89];
z[58] = z[7] * z[58];
z[89] = z[83] + -z[89];
z[89] = z[4] * z[89];
z[67] = z[63] + z[67];
z[90] = z[27] * z[81];
z[91] = T(2) * z[90];
z[92] = z[67] + -z[74] + -z[91];
z[93] = z[0] * z[81];
z[94] = T(2) * z[93];
z[95] = -z[92] + -z[94];
z[95] = z[3] * z[95];
z[80] = z[70] + -z[80] + z[92];
z[80] = z[9] * z[80];
z[58] = z[58] + z[64] + z[78] + z[80] + z[82] + -z[84] + z[89] + z[95];
z[58] = z[8] * z[58];
z[64] = z[26] / T(2);
z[78] = -z[51] + z[64];
z[80] = z[26] * z[78];
z[80] = z[54] + z[80];
z[89] = prod_pow(z[28], 2);
z[92] = (T(3) * z[89]) / T(2);
z[95] = T(3) * z[56];
z[96] = T(3) * z[40];
z[97] = (T(3) * z[0]) / T(2) + -z[25] + -z[26];
z[97] = z[0] * z[97];
z[97] = z[42] + z[59] + z[72] + -z[80] + -z[92] + z[95] + z[96] + z[97];
z[98] = z[25] / T(2);
z[99] = z[0] / T(2);
z[100] = z[98] + -z[99];
z[101] = z[26] + z[50];
z[102] = z[27] + z[100] + -z[101];
z[102] = z[27] * z[102];
z[102] = z[41] + z[102];
z[103] = z[61] / T(2);
z[104] = (T(3) * z[38]) / T(2);
z[97] = z[97] / T(2) + z[102] + z[103] + -z[104];
z[97] = z[12] * z[97];
z[105] = z[0] / T(4);
z[106] = z[50] + -z[64] + -z[98] + z[105];
z[106] = z[0] * z[106];
z[102] = z[102] + z[106];
z[107] = -z[54] + z[96];
z[108] = T(3) * z[42];
z[109] = z[59] + z[108];
z[85] = -z[36] + z[85] + z[107] + z[109];
z[78] = z[64] * z[78];
z[56] = (T(3) * z[56]) / T(2) + -z[104];
z[104] = z[25] + (T(-3) * z[28]) / T(4);
z[104] = z[28] * z[104];
z[85] = z[56] + -z[78] + z[85] / T(2) + z[102] + -z[103] + z[104];
z[85] = z[1] * z[85];
z[71] = T(3) * z[36] + -z[71];
z[104] = z[71] + -z[108];
z[110] = -z[54] + z[74] + -z[96] + z[104];
z[111] = z[51] + z[64];
z[111] = z[64] * z[111];
z[112] = -z[57] / T(2);
z[113] = (T(3) * z[61]) / T(2);
z[114] = z[28] / T(4);
z[115] = -z[25] + z[114];
z[115] = z[28] * z[115];
z[102] = -z[102] + z[110] / T(2) + z[111] + z[112] + z[113] + z[115];
z[102] = z[13] * z[102];
z[110] = -z[40] + z[71];
z[111] = z[90] + z[110];
z[115] = -z[74] + z[112];
z[116] = z[54] / T(2);
z[117] = z[115] + z[116];
z[118] = z[78] + -z[113];
z[119] = (T(3) * z[42]) / T(2);
z[120] = -z[25] + -z[114];
z[120] = z[28] * z[120];
z[106] = -z[41] + -z[106] + z[111] / T(2) + -z[117] + -z[118] + -z[119] + z[120];
z[106] = z[11] * z[106];
z[111] = z[29] * z[44];
z[120] = int_to_imaginary<T>(1) * prod_pow(z[29], 3);
z[85] = z[85] + z[97] + z[102] + z[106] + T(-2) * z[111] + (T(2) * z[120]) / T(27);
z[85] = z[5] * z[85];
z[97] = z[51] / T(2) + -z[66] + z[105];
z[97] = z[0] * z[97];
z[102] = z[0] + -z[26] + z[51];
z[105] = -z[27] + z[102] / T(2);
z[105] = z[27] * z[105];
z[106] = (T(5) * z[41]) / T(2);
z[105] = z[105] + -z[106];
z[97] = z[97] + -z[105];
z[121] = z[72] + -z[96];
z[122] = z[76] * z[77];
z[115] = (T(-3) * z[83]) / T(4) + -z[97] + z[103] + z[115] + z[121] / T(2) + z[122];
z[115] = z[1] * z[115];
z[121] = z[51] + z[99];
z[122] = z[0] * z[121];
z[123] = z[28] * z[77];
z[124] = -z[41] + z[57] + -z[123];
z[125] = z[40] + z[83] / T(2);
z[102] = z[27] * z[102];
z[122] = z[73] + -z[74] + -z[102] + z[122] + -z[124] + -z[125];
z[126] = z[13] / T(2);
z[127] = z[122] * z[126];
z[114] = z[49] + -z[98] + z[114];
z[114] = z[28] * z[114];
z[114] = -z[56] + z[114];
z[128] = -z[113] + z[114];
z[129] = z[55] + z[128];
z[71] = -z[71] + z[96];
z[130] = (T(3) * z[26]) / T(4) + -z[52];
z[130] = z[26] * z[130];
z[71] = z[71] / T(2) + -z[74] + z[97] + z[129] + z[130];
z[71] = z[11] * z[71];
z[97] = z[41] / T(2);
z[130] = -z[97] + z[102] / T(2);
z[131] = -z[59] + -z[110];
z[132] = -(z[99] * z[121]);
z[133] = z[26] / T(4) + -z[52];
z[133] = z[26] * z[133];
z[129] = z[129] + z[130] + z[131] / T(2) + z[132] + z[133];
z[129] = z[12] * z[129];
z[131] = z[120] / T(9);
z[132] = T(3) * z[111] + -z[131];
z[71] = z[71] + z[115] + z[127] + z[129] + -z[132];
z[71] = z[6] * z[71];
z[115] = T(3) * z[80] + -z[109];
z[127] = -z[25] + z[26];
z[129] = z[99] + z[127];
z[133] = T(3) * z[0];
z[134] = z[129] * z[133];
z[135] = T(3) * z[27];
z[136] = z[81] * z[135];
z[95] = T(3) * z[38] + -z[95];
z[92] = z[92] + z[95];
z[61] = -z[40] + z[61] + z[72];
z[61] = T(3) * z[61];
z[134] = z[61] + -z[92] + -z[115] + z[134] + -z[136];
z[134] = z[23] * z[134];
z[137] = T(3) * z[41];
z[61] = -z[61] + T(3) * z[102] + -z[137];
z[121] = z[121] * z[133];
z[138] = (T(3) * z[83]) / T(2);
z[95] = -z[59] + -z[61] + z[95] + z[121] + T(3) * z[123] + -z[138];
z[95] = z[24] * z[95];
z[121] = z[0] * z[101];
z[124] = z[42] + z[121] + z[124];
z[139] = z[27] * z[101];
z[140] = z[73] + -z[124] + z[139];
z[141] = z[31] * z[140];
z[80] = z[40] + z[80];
z[142] = z[59] / T(2);
z[124] = z[80] + -z[124] + -z[142];
z[143] = z[30] * z[124];
z[141] = z[141] + -z[143];
z[95] = z[95] + z[134] + T(-3) * z[141];
z[81] = z[81] * z[133];
z[61] = -z[61] + z[81] + -z[115];
z[115] = z[14] * z[61];
z[115] = -z[95] + z[115];
z[134] = z[20] + z[21] + z[22];
z[134] = -z[134] / T(2);
z[115] = z[115] * z[134];
z[134] = z[18] * z[122];
z[62] = T(2) * z[36] + z[62] + z[70];
z[87] = z[62] + -z[87];
z[143] = T(2) * z[102];
z[144] = z[87] + -z[143];
z[145] = -z[28] + z[79];
z[145] = z[28] * z[145];
z[63] = -z[63] + z[145];
z[146] = -z[0] + -z[52];
z[146] = z[0] * z[146];
z[83] = z[63] + -z[65] + z[83] + z[86] + -z[144] + z[146];
z[83] = z[17] * z[83];
z[146] = z[46] * z[49];
z[147] = z[74] + z[146];
z[148] = z[49] / T(2);
z[149] = z[45] * z[148];
z[149] = -z[147] + z[149];
z[149] = z[45] * z[149];
z[150] = z[47] * z[49];
z[149] = (T(19) * z[48]) / T(6) + -z[120] / T(4) + z[149] + z[150];
z[83] = z[83] + z[134] + z[149];
z[83] = z[24] * z[83];
z[134] = z[19] / T(2);
z[95] = z[95] * z[134];
z[151] = z[26] * z[51];
z[151] = -z[54] + z[151];
z[152] = (T(3) * z[72]) / T(2);
z[56] = -z[56] + -z[59] + (T(3) * z[89]) / T(4) + -z[113] + (T(-3) * z[151]) / T(2) + -z[152];
z[113] = -z[11] + -z[12];
z[56] = z[56] * z[113];
z[52] = -(z[26] * z[52]);
z[52] = z[52] + z[55] + (T(-4) * z[59]) / T(3) + -z[62] + z[65] + z[89];
z[52] = z[8] * z[52];
z[51] = z[51] * z[64];
z[51] = -z[51] + -z[72] / T(2) + z[89] / T(4) + -z[103] + z[117];
z[55] = z[1] + z[13];
z[51] = z[51] * z[55];
z[51] = z[51] + z[52] + z[56];
z[51] = z[33] * z[51];
z[52] = z[24] * z[122];
z[55] = z[75] + -z[80];
z[56] = z[57] + -z[89] / T(2);
z[57] = z[0] * z[129];
z[57] = z[42] + z[55] + z[56] + z[57] + -z[90];
z[65] = z[23] * z[57];
z[75] = z[41] + z[42] + z[93];
z[55] = z[55] + z[75] + -z[102];
z[80] = z[14] * z[55];
z[52] = z[52] + z[65] + -z[80] + -z[141];
z[65] = z[15] + z[16];
z[65] = -z[65] / T(2);
z[52] = z[52] * z[65];
z[65] = T(2) * z[121];
z[80] = z[59] + z[65] + -z[69] + -z[88] + z[145];
z[80] = z[30] * z[80];
z[88] = T(2) * z[139];
z[62] = z[62] + -z[63] + -z[65] + z[68] + z[88];
z[62] = z[31] * z[62];
z[62] = z[62] + z[80];
z[62] = z[17] * z[62];
z[63] = -z[0] + T(-2) * z[127];
z[63] = z[0] * z[63];
z[63] = z[63] + z[69] + -z[86] + -z[87] + z[89] + z[91];
z[63] = z[17] * z[63];
z[57] = z[18] * z[57];
z[65] = (T(3) * z[59]) / T(2) + z[146];
z[65] = z[46] * z[65];
z[68] = z[45] * z[49];
z[69] = z[68] + T(-2) * z[147];
z[69] = z[45] * z[69];
z[57] = (T(11) * z[48]) / T(12) + z[57] + z[63] + z[65] + T(2) * z[69] + -z[120] / T(2) + T(4) * z[150];
z[57] = z[23] * z[57];
z[63] = z[64] + -z[76] + z[79] + z[148];
z[64] = -z[0] + z[63];
z[64] = z[0] * z[64];
z[64] = z[64] + -z[106];
z[69] = z[64] + -z[119];
z[76] = z[78] + z[114];
z[70] = (T(3) * z[40]) / T(2) + z[59] / T(12) + z[69] + -z[70] + -z[76] + -z[116] + z[143];
z[78] = z[12] + -z[13];
z[70] = z[70] * z[78];
z[78] = z[99] * z[101];
z[78] = z[78] + -z[97];
z[79] = z[42] + z[107] + -z[142];
z[76] = -z[76] + z[78] + z[79] / T(2) + -z[88];
z[76] = z[1] * z[76];
z[79] = z[11] / T(2);
z[80] = z[79] * z[124];
z[70] = z[70] + z[76] + z[80] + -z[132];
z[70] = z[9] * z[70];
z[63] = T(-2) * z[0] + z[27] + z[63];
z[63] = z[27] * z[63];
z[76] = -z[72] + -z[74] + z[108] + -z[123];
z[64] = z[63] + -z[64] + z[76] / T(2) + -z[103] + -z[112];
z[64] = z[13] * z[64];
z[76] = -z[53] + z[128];
z[63] = -z[63] + z[69] + -z[74] + z[76] + -z[152];
z[63] = z[12] * z[63];
z[69] = -z[42] + -z[59] + T(-3) * z[72] + z[139];
z[69] = z[69] / T(2) + z[76] + -z[78];
z[69] = z[11] * z[69];
z[72] = z[1] / T(2);
z[76] = -(z[72] * z[140]);
z[63] = z[63] + z[64] + z[69] + z[76] + -z[132];
z[63] = z[10] * z[63];
z[53] = z[53] + z[67] + z[86] + z[94] + z[144];
z[53] = z[17] * z[53];
z[61] = -(z[61] * z[134]);
z[64] = T(3) * z[49];
z[67] = z[46] * z[64];
z[59] = z[59] + z[67] + (T(-3) * z[68]) / T(2);
z[59] = z[45] * z[59];
z[65] = -z[65] + z[120] / T(3);
z[64] = z[47] * z[64];
z[59] = (T(11) * z[48]) / T(8) + z[59] + -z[64] + z[65] / T(2);
z[64] = -(z[18] * z[55]);
z[53] = z[53] + z[59] + z[61] + z[64];
z[53] = z[14] * z[53];
z[61] = z[81] + z[108] + -z[136] + z[137] + -z[142];
z[64] = z[3] * z[61];
z[65] = z[50] + z[99];
z[65] = z[0] * z[65];
z[67] = z[25] * z[28];
z[65] = z[42] + z[65] + z[67] + -z[73];
z[67] = -(z[2] * z[65]);
z[55] = -(z[7] * z[55]);
z[68] = z[60] + z[125];
z[69] = -z[26] + z[99];
z[73] = z[0] * z[69];
z[73] = z[41] + z[68] + z[73];
z[76] = -(z[4] * z[73]);
z[55] = z[55] + z[64] + z[67] + z[76];
z[55] = z[12] * z[55];
z[64] = z[96] + z[138] + z[142];
z[67] = z[101] * z[135];
z[67] = -z[64] + z[67] + z[92];
z[76] = -z[1] + z[13];
z[78] = -z[12] + z[76];
z[67] = -(z[67] * z[78]);
z[56] = z[56] + z[68] + -z[139];
z[56] = z[11] * z[56];
z[56] = z[56] + z[67];
z[56] = z[35] * z[56];
z[55] = z[55] + z[56];
z[50] = T(-2) * z[50];
z[56] = z[50] + -z[66] + -z[100];
z[56] = z[0] * z[56];
z[66] = -z[74] + z[104] + z[107];
z[49] = -z[49] + z[77];
z[49] = z[28] * z[49];
z[49] = z[49] + -z[118];
z[56] = z[49] + z[56] + z[66] / T(2) + -z[105];
z[66] = -(z[1] * z[56]);
z[66] = z[66] + z[131];
z[66] = z[7] * z[66];
z[54] = -z[54] + -z[109] + z[110];
z[50] = z[50] + -z[98] + -z[99];
z[50] = z[0] * z[50];
z[49] = z[49] + z[50] + z[54] / T(2) + -z[130];
z[49] = z[7] * z[49];
z[50] = (T(3) * z[2]) / T(2);
z[50] = z[50] * z[65];
z[54] = z[4] / T(2);
z[65] = z[54] * z[73];
z[49] = z[49] + z[50] + z[65];
z[49] = z[13] * z[49];
z[56] = z[7] * z[56];
z[65] = z[69] * z[133];
z[64] = z[64] + z[65] + z[137];
z[54] = -(z[54] * z[64]);
z[54] = z[50] + z[54] + z[56];
z[54] = z[11] * z[54];
z[56] = -(z[61] * z[126]);
z[60] = -z[60] + z[75] + -z[90];
z[61] = -z[72] + z[79];
z[60] = z[60] * z[61];
z[61] = z[120] / T(27);
z[56] = z[56] + z[60] + -z[61];
z[56] = z[3] * z[56];
z[60] = -(z[18] * z[141]);
z[65] = -z[11] + -z[76];
z[65] = z[65] * z[82];
z[67] = -(z[78] * z[84]);
z[59] = z[30] * z[59];
z[68] = z[31] * z[149];
z[50] = -(z[1] * z[50]);
z[64] = z[64] * z[72];
z[61] = -z[61] + z[64];
z[61] = z[4] * z[61];
z[64] = z[3] + z[4] + T(-3) * z[7] + T(4) * z[43];
z[64] = z[64] * z[111];
z[69] = z[43] * z[120];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] / T(2) + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + (T(-4) * z[69]) / T(27) + z[70] + z[71] + z[83] + z[85] + z[95] + z[115];
}



template IntegrandConstructorType<double> f_4_244_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_244_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_244_construct (const Kin<qd_real>&);
#endif

}