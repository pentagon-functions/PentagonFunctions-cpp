#include "f_4_308.h"

namespace PentagonFunctions {

template <typename T> T f_4_308_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_308_W_7 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_308_W_7 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (T(6) * kin.v[1] * kin.v[4] + kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(6) * kin.v[1] * kin.v[4] + kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4])) + rlog(-kin.v[1]) * (T(-6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[3] * (T(-6) + T(-6) * kin.v[1] + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(-6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[3] * (T(-6) + T(-6) * kin.v[1] + T(3) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(3) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[6] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(-3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_308_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_308_W_22 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[7] * (prod_pow(abb[9], 2) * abb[10] * T(3) + prod_pow(abb[9], 2) * (abb[5] * T(-3) + abb[11] * T(-3) + abb[6] * T(3)) + prod_pow(abb[8], 2) * (abb[6] * T(-3) + abb[10] * T(-3) + abb[5] * T(3) + abb[11] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_308_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_308_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(-10) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[2] + T(-8) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(12) * kin.v[3] + T(4) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[1] + T(12) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[3] * (T(-10) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[2] + T(-8) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[1] + T(12) * kin.v[3] + T(4) * kin.v[4]) + kin.v[0] * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-4) * kin.v[1] + T(12) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[1] + T(-6) * kin.v[3] + T(-2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (kin.v[1] + T(-6) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(4) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (T(5) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-3) * kin.v[3]) + kin.v[0] * (kin.v[0] / T(2) + -kin.v[4] + kin.v[1] + T(-3) * kin.v[3]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(2) * kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + T(1) + kin.v[2]) + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-3) * kin.v[3]) + kin.v[0] * (kin.v[0] / T(2) + -kin.v[4] + kin.v[1] + T(-3) * kin.v[3]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(2) * kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + T(1) + kin.v[2]) + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-3) * kin.v[3]) + kin.v[0] * (kin.v[0] / T(2) + -kin.v[4] + kin.v[1] + T(-3) * kin.v[3]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(2) * kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + T(1) + kin.v[2]) + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-3) * kin.v[3]) + kin.v[0] * (kin.v[0] / T(2) + -kin.v[4] + kin.v[1] + T(-3) * kin.v[3]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(2) * kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + T(1) + kin.v[2]) + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]))) + rlog(kin.v[3]) * (kin.v[1] * (kin.v[1] + T(-6) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[0] * (kin.v[0] + T(2) * kin.v[1] + T(-6) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(4) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (T(5) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]))) + rlog(kin.v[2]) * (kin.v[3] * ((T(-15) * kin.v[3]) / T(2) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-6) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(9) * kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(-3) * kin.v[1] + T(9) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) + T(-3) * kin.v[2] + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[3] * ((T(-15) * kin.v[3]) / T(2) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-6) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(9) * kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(-3) * kin.v[1] + T(9) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) + T(-3) * kin.v[2] + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[3] + T(3) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(-4) + T(8)) * kin.v[0] + T(8) * kin.v[1] + T(-8) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]);
c[3] = rlog(-kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-2) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(2) * kin.v[2] + T(2) * kin.v[3]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-2) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(2) * kin.v[2] + T(2) * kin.v[3]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-2) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(2) * kin.v[2] + T(2) * kin.v[3]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-2) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(2) * kin.v[2] + T(2) * kin.v[3]) + rlog(kin.v[3]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3])) + rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(-3) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(-3) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[12] * (abb[3] * (abb[8] * abb[9] + -(abb[9] * abb[13]) + abb[9] * (-abb[9] + bc<T>[0] * int_to_imaginary<T>(1)) + abb[14] * T(2)) + abb[9] * abb[13] * (-abb[5] + -abb[6] + -abb[11] + abb[16] * T(-4) + abb[15] * T(-2) + abb[4] * T(3) + abb[10] * T(3)) + abb[9] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[15] * bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[16] * bc<T>[0] * int_to_imaginary<T>(4) + abb[9] * (-abb[5] + -abb[6] + -abb[11] + abb[16] * T(-4) + abb[15] * T(-2) + abb[4] * T(3) + abb[10] * T(3))) + abb[8] * abb[9] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[10] * T(-3) + abb[15] * T(2) + abb[16] * T(4)) + abb[1] * (abb[3] * abb[9] + abb[2] * (-abb[3] + -abb[5] + -abb[6] + -abb[11] + abb[16] * T(-4) + abb[15] * T(-2) + abb[4] * T(3) + abb[10] * T(3)) + abb[9] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[10] * T(-3) + abb[15] * T(2) + abb[16] * T(4))) + abb[14] * (abb[4] * T(-6) + abb[10] * T(-6) + abb[5] * T(2) + abb[6] * T(2) + abb[11] * T(2) + abb[15] * T(4) + abb[16] * T(8)) + abb[2] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[3] * (abb[13] + -abb[8] + -abb[9] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[15] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3) + abb[16] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[8] * (-abb[5] + -abb[6] + -abb[11] + abb[16] * T(-4) + abb[15] * T(-2) + abb[4] * T(3) + abb[10] * T(3)) + abb[9] * (-abb[5] + -abb[6] + -abb[11] + abb[16] * T(-4) + abb[15] * T(-2) + abb[4] * T(3) + abb[10] * T(3)) + abb[13] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[10] * T(-3) + abb[15] * T(2) + abb[16] * T(4)) + abb[2] * (abb[4] * T(-6) + abb[10] * T(-6) + abb[3] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[11] * T(2) + abb[15] * T(4) + abb[16] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_308_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl17 = DLog_W_17<T>(kin),dl26 = DLog_W_26<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl4 = DLog_W_4<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),spdl7 = SpDLog_f_4_308_W_7<T>(kin),spdl22 = SpDLog_f_4_308_W_22<T>(kin),spdl21 = SpDLog_f_4_308_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), dl21(t), rlog(v_path[3]), f_2_1_15(kin_path), rlog(kin.W[3] / kin_path.W[3]), -rlog(t), dl17(t), f_2_1_6(kin_path), f_2_1_14(kin_path), rlog(v_path[0] + v_path[1]), dl26(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl5(t), f_2_1_3(kin_path), f_2_1_11(kin_path), rlog(-v_path[1] + v_path[3]), dl1(t), dl4(t), dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl20(t), dl18(t), dl16(t), dl3(t), dl2(t), dl19(t)}
;

        auto result = f_4_308_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_308_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[134];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[26];
z[3] = abb[30];
z[4] = abb[31];
z[5] = abb[36];
z[6] = abb[37];
z[7] = abb[38];
z[8] = abb[40];
z[9] = abb[41];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[10];
z[14] = abb[11];
z[15] = abb[15];
z[16] = abb[16];
z[17] = abb[22];
z[18] = abb[32];
z[19] = abb[34];
z[20] = abb[35];
z[21] = abb[23];
z[22] = abb[24];
z[23] = abb[25];
z[24] = abb[2];
z[25] = abb[8];
z[26] = abb[9];
z[27] = abb[13];
z[28] = bc<TR>[0];
z[29] = abb[33];
z[30] = abb[39];
z[31] = abb[17];
z[32] = abb[14];
z[33] = abb[18];
z[34] = abb[19];
z[35] = abb[20];
z[36] = abb[27];
z[37] = abb[28];
z[38] = abb[29];
z[39] = abb[21];
z[40] = bc<TR>[3];
z[41] = bc<TR>[1];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[9];
z[45] = z[10] + z[12];
z[46] = z[13] + z[45];
z[47] = z[11] + z[14];
z[48] = z[1] + z[47];
z[49] = z[46] + -z[48];
z[50] = z[31] * z[49];
z[51] = -z[17] + -z[21] + z[22] + z[23];
z[52] = z[39] * z[51];
z[52] = z[50] + z[52];
z[53] = T(3) * z[1];
z[54] = T(-5) * z[10] + -z[13] + -z[47] + z[53];
z[55] = z[6] * z[54];
z[56] = z[18] * z[51];
z[57] = -z[55] + z[56];
z[58] = z[52] + -z[57];
z[59] = z[13] + -z[14];
z[60] = z[1] + z[11];
z[61] = z[45] + -z[59] + -z[60];
z[61] = z[3] * z[61];
z[62] = z[12] / T(3);
z[63] = z[11] / T(3);
z[64] = -z[10] + z[59];
z[65] = -z[1] + z[62] + -z[63] + -z[64];
z[65] = z[4] * z[65];
z[66] = z[29] * z[51];
z[67] = z[20] * z[51];
z[68] = z[66] + z[67];
z[69] = z[6] + -z[8];
z[70] = z[7] + z[69];
z[71] = -z[30] + z[70];
z[72] = -z[5] + z[9];
z[73] = z[71] + (T(-5) * z[72]) / T(3);
z[74] = z[15] / T(2) + z[16];
z[73] = z[73] * z[74];
z[75] = -z[10] + z[14];
z[75] = -z[13] + z[63] + z[75] / T(2);
z[75] = z[7] * z[75];
z[76] = T(3) * z[10];
z[77] = z[14] + -z[63] + z[76];
z[77] = -z[1] + z[77] / T(2);
z[77] = z[8] * z[77];
z[78] = T(3) * z[13];
z[63] = (T(-13) * z[1]) / T(3) + z[10] + -z[14] / T(3) + -z[63] + -z[78];
z[79] = z[5] / T(4);
z[63] = z[63] * z[79];
z[80] = z[7] / T(2);
z[81] = -z[8] + z[80];
z[82] = (T(3) * z[6]) / T(4);
z[83] = (T(11) * z[5]) / T(12) + z[81] / T(3) + z[82];
z[83] = z[12] * z[83];
z[84] = z[13] / T(2);
z[62] = z[1] / T(3) + z[10] / T(2) + (T(-7) * z[11]) / T(6) + (T(4) * z[14]) / T(3) + z[41] + z[62] + -z[84];
z[62] = z[9] * z[62];
z[85] = T(-5) * z[13] + T(3) * z[14];
z[86] = T(3) * z[11];
z[87] = -z[1] + z[85] + z[86];
z[45] = -z[45] + z[87];
z[88] = z[41] + -z[45] / T(4);
z[88] = z[30] * z[88];
z[89] = z[19] * z[51];
z[90] = (T(3) * z[89]) / T(4);
z[91] = z[5] + z[70];
z[92] = -(z[41] * z[91]);
z[58] = z[58] / T(4) + -z[61] + z[62] + z[63] + z[65] + z[68] / T(2) + z[73] + z[75] + z[77] + z[83] + z[88] + z[90] + z[92];
z[58] = z[28] * z[58];
z[62] = z[18] + -z[19] + -z[20] + z[29];
z[63] = z[40] * z[62];
z[58] = z[58] + T(3) * z[63];
z[58] = z[28] * z[58];
z[63] = (T(3) * z[61]) / T(4);
z[65] = (T(9) * z[6]) / T(2);
z[68] = z[5] / T(2);
z[73] = z[65] + -z[68] + -z[81];
z[75] = z[12] / T(2);
z[73] = z[73] * z[75];
z[77] = z[1] + z[14];
z[75] = -z[11] + z[75] + z[77] / T(2);
z[75] = z[9] * z[75];
z[81] = T(3) * z[69] + z[72];
z[81] = z[74] * z[81];
z[83] = z[76] + z[78];
z[88] = -z[48] + z[83];
z[92] = z[5] * z[88];
z[90] = -z[90] + z[92] / T(4);
z[93] = z[53] + T(3) * z[64];
z[94] = -z[11] + z[93];
z[94] = z[7] * z[94];
z[95] = z[12] + z[77];
z[96] = z[10] + z[13];
z[97] = -z[11] + z[95] + -z[96];
z[98] = z[2] * z[97];
z[99] = T(2) * z[11];
z[84] = z[10] + z[84];
z[84] = (T(-3) * z[1]) / T(2) + T(3) * z[84] + -z[99];
z[84] = z[8] * z[84];
z[100] = -z[11] + z[12];
z[101] = z[53] + z[100];
z[102] = z[76] + -z[101];
z[103] = z[4] / T(2);
z[102] = z[102] * z[103];
z[104] = (T(3) * z[67]) / T(4);
z[73] = (T(-3) * z[57]) / T(4) + -z[63] + z[73] + -z[75] + z[81] + z[84] + z[90] + -z[94] / T(4) + (T(3) * z[98]) / T(2) + z[102] + -z[104];
z[73] = z[0] * z[73];
z[81] = T(4) * z[5];
z[65] = T(-4) * z[8] + z[65] + z[80] + -z[81];
z[65] = z[12] * z[65];
z[80] = T(2) * z[8];
z[84] = T(2) * z[5];
z[102] = z[80] + z[84];
z[105] = T(2) * z[1] + -z[47] + -z[76];
z[102] = z[102] * z[105];
z[106] = -z[12] + z[88];
z[107] = z[106] / T(2);
z[108] = z[4] * z[107];
z[107] = z[9] * z[107];
z[51] = (T(3) * z[51]) / T(2);
z[109] = z[29] * z[51];
z[107] = z[107] + z[109];
z[110] = z[20] * z[51];
z[111] = z[107] + -z[110];
z[112] = z[7] * z[88];
z[113] = z[18] * z[51];
z[55] = (T(3) * z[55]) / T(2) + z[65] + -z[102] + z[108] + -z[111] + -z[112] / T(2) + -z[113];
z[55] = z[26] * z[55];
z[65] = z[11] + z[93];
z[65] = z[8] * z[65];
z[65] = z[65] + -z[94];
z[94] = T(3) * z[89];
z[92] = -z[92] + z[94];
z[102] = z[7] + z[8];
z[113] = z[5] + z[102];
z[114] = z[12] * z[113];
z[114] = z[92] + z[114];
z[115] = T(3) * z[98] + -z[114];
z[116] = -z[65] + -z[115];
z[117] = -z[95] + z[99];
z[118] = z[9] * z[117];
z[119] = z[4] * z[100];
z[118] = -z[118] + z[119];
z[116] = z[110] + z[116] / T(2) + z[118];
z[116] = z[27] * z[116];
z[80] = -z[7] + z[80];
z[119] = T(3) * z[6];
z[120] = z[4] + -z[9] + z[80] + z[84] + -z[119];
z[120] = z[26] * z[120];
z[121] = z[27] * z[72];
z[122] = z[120] + -z[121];
z[123] = z[15] + T(2) * z[16];
z[122] = z[122] * z[123];
z[73] = -z[55] + z[73] + z[116] + z[122];
z[73] = z[0] * z[73];
z[116] = z[5] + z[8];
z[122] = T(2) * z[9];
z[124] = z[4] + z[122];
z[125] = T(2) * z[7];
z[126] = T(3) * z[30];
z[127] = z[116] + -z[124] + -z[125] + z[126];
z[127] = z[123] * z[127];
z[128] = z[30] * z[45];
z[129] = (T(3) * z[128]) / T(2);
z[130] = z[68] * z[88];
z[131] = z[19] * z[51];
z[130] = -z[130] + z[131];
z[88] = z[8] * z[88];
z[99] = T(2) * z[14] + -z[78] + z[99];
z[131] = -z[1] + z[99];
z[132] = -(z[125] * z[131]);
z[125] = z[8] / T(2) + z[68] + z[125];
z[125] = z[12] * z[125];
z[133] = z[1] + z[12];
z[99] = z[99] + -z[133];
z[122] = -(z[99] * z[122]);
z[108] = -z[88] / T(2) + z[108] + z[122] + z[125] + z[127] + z[129] + z[130] + z[132];
z[108] = z[24] * z[108];
z[122] = z[9] * z[106];
z[85] = -z[10] + z[85];
z[53] = z[11] + -z[53] + T(3) * z[85];
z[53] = z[7] * z[53];
z[53] = -z[53] + z[122];
z[85] = z[66] + z[128];
z[122] = T(-3) * z[7] + -z[72] + z[126];
z[74] = -(z[74] * z[122]);
z[104] = (T(3) * z[52]) / T(4) + z[104];
z[59] = -z[10] + -z[59];
z[59] = T(3) * z[59] + z[101];
z[101] = z[4] / T(4);
z[59] = z[59] * z[101];
z[122] = z[8] * z[11];
z[79] = (T(5) * z[7]) / T(4) + -z[8] + -z[79];
z[79] = z[12] * z[79];
z[53] = -z[53] / T(4) + z[59] + z[74] + z[79] + (T(-3) * z[85]) / T(4) + z[90] + z[104] + z[122];
z[53] = z[25] * z[53];
z[59] = z[68] + T(2) * z[102];
z[59] = z[12] * z[59];
z[74] = z[93] + z[100];
z[79] = z[74] * z[103];
z[85] = z[7] * z[11];
z[85] = z[85] + z[122];
z[59] = z[59] + (T(-3) * z[61]) / T(2) + -z[79] + T(-2) * z[85] + z[107] + z[130];
z[79] = z[72] * z[123];
z[79] = -z[59] + z[79];
z[90] = z[0] * z[79];
z[88] = z[88] + z[112];
z[93] = -z[81] + z[102] / T(2);
z[93] = z[12] * z[93];
z[100] = z[48] * z[84];
z[103] = T(2) * z[12] + -z[48];
z[103] = z[4] * z[103];
z[100] = -z[100] + z[103];
z[51] = z[39] * z[51];
z[50] = (T(-3) * z[50]) / T(2) + -z[51] + z[88] / T(2) + -z[93] + z[100] + z[111];
z[50] = z[26] * z[50];
z[51] = z[84] + -z[102];
z[84] = z[4] + -z[9] + z[51];
z[93] = z[26] * z[84];
z[103] = z[93] + -z[121];
z[103] = z[103] * z[123];
z[90] = z[50] + z[90] + z[103];
z[59] = z[27] * z[59];
z[103] = int_to_imaginary<T>(1) * z[28];
z[79] = z[79] * z[103];
z[53] = z[53] + z[59] + z[79] + z[90] + z[108];
z[53] = z[25] * z[53];
z[59] = z[10] + -z[13] + z[47];
z[79] = z[59] + -z[133];
z[79] = z[4] * z[79];
z[61] = -z[61] + z[79];
z[79] = -z[48] + z[96];
z[79] = z[5] * z[79];
z[60] = z[60] + z[64];
z[64] = z[8] * z[60];
z[107] = -z[5] + z[102];
z[107] = z[12] * z[107];
z[59] = -z[1] + z[59];
z[108] = z[7] * z[59];
z[64] = z[52] + z[61] + -z[64] + z[67] + -z[79] + z[89] + z[107] + -z[108];
z[64] = (T(3) * z[64]) / T(2);
z[79] = z[35] * z[64];
z[107] = z[9] * z[97];
z[60] = z[7] * z[60];
z[108] = z[66] + -z[67];
z[102] = z[12] * z[102];
z[59] = z[8] * z[59];
z[59] = -z[59] + -z[60] + z[61] + z[98] + z[102] + -z[107] + z[108];
z[59] = (T(3) * z[59]) / T(2);
z[60] = z[38] * z[59];
z[51] = z[12] * z[51];
z[61] = -(z[5] * z[48]);
z[51] = z[51] + z[61] + z[85] + z[118];
z[51] = z[27] * z[51];
z[61] = -z[7] + z[8] + z[30];
z[61] = z[9] / T(2) + (T(3) * z[61]) / T(4) + -z[62] / T(9) + -z[68] + -z[82];
z[61] = prod_pow(z[28], 2) * z[61];
z[62] = z[41] / T(2) + -z[42];
z[62] = z[41] * z[62];
z[68] = -z[9] + z[91];
z[68] = z[62] * z[68];
z[70] = z[70] + -z[72];
z[70] = z[43] * z[70];
z[68] = z[68] + z[70];
z[62] = -z[43] + -z[62];
z[62] = z[62] * z[126];
z[51] = z[51] + z[60] + z[61] + z[62] + T(3) * z[68] + z[79] + z[90];
z[51] = z[51] * z[103];
z[60] = -z[10] + z[87];
z[60] = z[7] * z[60];
z[48] = z[48] + z[96];
z[48] = z[5] * z[48];
z[45] = z[9] * z[45];
z[49] = -(z[4] * z[49]);
z[61] = T(-3) * z[5] + -z[7];
z[61] = z[12] * z[61];
z[45] = z[45] + z[48] + z[49] + z[52] + z[60] + z[61] + -z[89] + -z[108] + -z[128];
z[48] = z[7] + -z[30] + z[72];
z[48] = z[48] * z[123];
z[45] = z[45] / T(2) + z[48];
z[45] = z[34] * z[45];
z[46] = -z[46] + -z[77] + z[86];
z[46] = z[9] * z[46];
z[48] = -(z[54] * z[116]);
z[49] = -(z[4] * z[97]);
z[52] = -z[5] + z[69];
z[52] = z[12] * z[52];
z[46] = z[46] + z[48] + z[49] + T(3) * z[52] + -z[57] + -z[66] + -z[89] + z[98];
z[46] = z[37] * z[46];
z[48] = -(z[33] * z[64]);
z[49] = z[65] + -z[114];
z[52] = z[74] * z[101];
z[49] = z[49] / T(4) + z[52] + z[63] + -z[75] + -z[104];
z[49] = z[27] * z[49];
z[49] = z[49] + -z[50];
z[49] = z[27] * z[49];
z[50] = z[56] + -z[67];
z[52] = z[54] * z[119];
z[50] = T(3) * z[50] + -z[52];
z[52] = T(9) * z[6];
z[54] = -z[52] + -z[113];
z[54] = z[12] * z[54];
z[56] = T(9) * z[13] + -z[76];
z[57] = T(23) * z[11] + -z[14] + -z[56] + T(-13) * z[133];
z[57] = z[9] * z[57];
z[54] = z[50] + z[54] + z[57] + z[88] + -z[92];
z[57] = T(5) * z[11];
z[60] = z[57] + -z[83] + -z[95];
z[60] = z[4] * z[60];
z[61] = -z[109] + z[129];
z[54] = z[54] / T(2) + z[60] + -z[61];
z[60] = T(-5) * z[9] + z[113] + z[119];
z[62] = T(2) * z[4];
z[63] = (T(-3) * z[30]) / T(2) + -z[60] / T(2) + z[62];
z[63] = z[15] * z[63];
z[60] = T(4) * z[4] + -z[60] + -z[126];
z[60] = z[16] * z[60];
z[54] = z[54] / T(2) + z[60] + z[63];
z[54] = z[24] * z[54];
z[60] = z[88] + z[115];
z[63] = z[117] * z[124];
z[60] = z[60] / T(2) + z[63] + -z[110];
z[63] = -z[113] + z[124];
z[64] = z[63] * z[123];
z[64] = z[60] + z[64];
z[65] = -z[0] + -z[103];
z[64] = z[64] * z[65];
z[60] = z[27] * z[60];
z[63] = z[27] * z[63];
z[63] = z[63] + -z[120];
z[63] = z[63] * z[123];
z[54] = z[54] + z[55] + z[60] + z[63] + z[64];
z[54] = z[24] * z[54];
z[55] = z[36] * z[59];
z[57] = T(-7) * z[1] + T(9) * z[10] + T(5) * z[14] + z[57] + -z[78];
z[57] = -(z[57] * z[116]);
z[47] = T(5) * z[1] + T(-7) * z[47] + z[56];
z[56] = -(z[7] * z[47]);
z[52] = T(-5) * z[7] + -z[52] + T(7) * z[116];
z[52] = z[12] * z[52];
z[47] = T(-5) * z[12] + -z[47];
z[47] = z[9] * z[47];
z[47] = z[47] + z[50] + z[52] + z[56] + z[57] + -z[94];
z[50] = -(z[4] * z[106]);
z[47] = z[47] / T(2) + z[50] + -z[61];
z[47] = z[32] * z[47];
z[50] = z[7] * z[131];
z[52] = z[8] * z[105];
z[56] = z[80] + -z[81];
z[56] = z[12] * z[56];
z[57] = z[9] * z[99];
z[50] = z[50] + z[52] + z[56] + z[57] + -z[100];
z[52] = prod_pow(z[26], 2);
z[50] = z[50] * z[52];
z[56] = z[9] + z[62] + z[113] + -z[119];
z[57] = z[56] + -z[126];
z[57] = z[32] * z[57];
z[59] = z[69] + z[72];
z[59] = z[37] * z[59];
z[52] = z[52] * z[84];
z[60] = -z[93] + z[121] / T(2);
z[60] = z[27] * z[60];
z[57] = -z[52] + z[57] + T(3) * z[59] + z[60];
z[57] = z[15] * z[57];
z[56] = T(-6) * z[30] + T(2) * z[56];
z[56] = z[32] * z[56];
z[60] = T(-2) * z[93] + z[121];
z[60] = z[27] * z[60];
z[52] = T(-2) * z[52] + z[56] + T(6) * z[59] + z[60];
z[52] = z[16] * z[52];
z[56] = (T(-5) * z[71]) / T(2) + (T(11) * z[72]) / T(3);
z[56] = z[44] * z[56];
return T(3) * z[45] + (T(3) * z[46]) / T(2) + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[73];
}



template IntegrandConstructorType<double> f_4_308_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_308_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_308_construct (const Kin<qd_real>&);
#endif

}