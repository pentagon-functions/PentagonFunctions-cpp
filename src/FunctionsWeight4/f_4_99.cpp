#include "f_4_99.h"

namespace PentagonFunctions {

template <typename T> T f_4_99_abbreviated (const std::array<T,40>&);



template <typename T> IntegrandConstructorType<T> f_4_99_construct (const Kin<T>& kin) {
    return [&kin, 
            dl4 = DLog_W_4<T>(kin),dl15 = DLog_W_15<T>(kin),dl2 = DLog_W_2<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl24 = DLog_W_24<T>(kin),dl6 = DLog_W_6<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin),dl18 = DLog_W_18<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,40> abbr = 
            {dl4(t), rlog(kin.W[4] / kin_path.W[4]), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), f_2_1_9(kin_path), rlog(kin.W[5] / kin_path.W[5]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), dl15(t), rlog(-v_path[4]), dl2(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_5(kin_path), f_2_1_7(kin_path), rlog(-v_path[4] + v_path[2]), dl26(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), f_1_3_3(kin) - f_1_3_3(kin_path), dl29(t) / kin_path.SqrtDelta, dl20(t), dl16(t), rlog(kin.W[23] / kin_path.W[23]), dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl24(t), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[16] / kin_path.W[16]), dl6(t), dl5(t), dl1(t), dl19(t), dl18(t), dl3(t), dl17(t)}
;

        auto result = f_4_99_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_99_abbreviated(const std::array<T,40>& abb)
{
using TR = typename T::value_type;
T z[137];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[24];
z[13] = abb[34];
z[14] = abb[35];
z[15] = abb[36];
z[16] = abb[37];
z[17] = abb[38];
z[18] = abb[39];
z[19] = abb[12];
z[20] = abb[14];
z[21] = abb[11];
z[22] = abb[13];
z[23] = abb[30];
z[24] = abb[25];
z[25] = abb[15];
z[26] = abb[16];
z[27] = abb[17];
z[28] = abb[20];
z[29] = abb[21];
z[30] = abb[33];
z[31] = abb[18];
z[32] = abb[19];
z[33] = abb[22];
z[34] = abb[23];
z[35] = abb[29];
z[36] = abb[31];
z[37] = abb[32];
z[38] = abb[28];
z[39] = abb[26];
z[40] = abb[27];
z[41] = bc<TR>[1];
z[42] = bc<TR>[3];
z[43] = bc<TR>[5];
z[44] = bc<TR>[2];
z[45] = bc<TR>[9];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = (T(3) * z[37]) / T(2);
z[50] = T(2) * z[36];
z[51] = z[1] + z[10];
z[52] = T(2) * z[11] + z[39] / T(2) + -z[49] + -z[50] + -z[51];
z[52] = z[23] * z[52];
z[53] = -z[11] + z[51];
z[54] = -z[39] + z[53];
z[55] = z[9] + z[37] + z[54];
z[56] = z[18] / T(2);
z[55] = z[55] * z[56];
z[57] = z[9] / T(2);
z[58] = -z[36] + z[53] / T(2) + z[57];
z[59] = z[14] * z[58];
z[60] = z[11] / T(2);
z[61] = z[57] + z[60];
z[62] = z[36] + z[37];
z[63] = z[61] + -z[62];
z[64] = T(2) * z[39] + (T(-3) * z[51]) / T(2) + z[63];
z[65] = z[13] + z[16];
z[64] = z[64] * z[65];
z[66] = z[15] * z[58];
z[67] = z[51] / T(2);
z[68] = z[60] + z[67];
z[69] = -z[39] + -z[57] + z[68];
z[70] = z[24] * z[69];
z[52] = -z[52] + -z[55] + -z[59] + z[64] + z[66] + z[70];
z[55] = z[28] * z[52];
z[59] = z[35] + z[38];
z[64] = z[31] + z[40];
z[71] = z[59] + -z[64];
z[72] = z[71] / T(2);
z[73] = z[32] + z[33];
z[74] = -(z[72] * z[73]);
z[52] = z[52] + z[74];
z[74] = int_to_imaginary<T>(1) * z[7];
z[52] = z[29] * z[52] * z[74];
z[75] = T(3) * z[36];
z[76] = z[37] + z[75];
z[77] = -z[61] + -z[67] + z[76];
z[77] = z[14] * z[77];
z[78] = (T(3) * z[9]) / T(2);
z[79] = z[62] + z[78];
z[80] = -z[1] + T(3) * z[10];
z[80] = T(-2) * z[8] + -z[60] + z[79] + z[80] / T(2);
z[81] = -(z[17] * z[80]);
z[82] = -z[1] + z[10];
z[61] = -z[8] + z[61] + z[82] / T(2);
z[82] = z[0] * z[61];
z[83] = -z[10] + z[11];
z[84] = -z[8] + z[9] + z[62] + -z[83];
z[85] = z[30] * z[84];
z[86] = -(z[18] * z[58]);
z[77] = z[77] + z[81] + z[82] + z[85] + z[86];
z[77] = z[6] * z[77];
z[81] = z[17] + z[18];
z[85] = z[15] + z[16];
z[86] = T(3) * z[14];
z[87] = T(3) * z[30];
z[88] = T(2) * z[13];
z[89] = -z[81] + -z[85] + -z[86] + z[87] + z[88];
z[89] = z[47] * z[89];
z[90] = z[38] + z[64];
z[90] = z[90] / T(2);
z[91] = -(z[73] * z[90]);
z[92] = -z[18] + z[65];
z[69] = z[69] * z[92];
z[92] = z[17] / T(2);
z[53] = -z[9] + z[53];
z[93] = -(z[53] * z[92]);
z[94] = z[22] * z[53];
z[69] = z[69] + -z[70] + z[91] + z[93] + z[94] / T(2);
z[69] = z[26] * z[69];
z[91] = -z[1] + z[9];
z[83] = -z[83] + z[91];
z[93] = z[14] * z[83];
z[95] = z[12] * z[83];
z[53] = z[18] * z[53];
z[53] = z[53] + -z[93] + -z[94] + z[95];
z[68] = -z[68] + z[79];
z[68] = z[17] * z[68];
z[79] = T(3) * z[1] + -z[10];
z[63] = -z[63] + z[79] / T(2);
z[63] = z[13] * z[63];
z[53] = z[53] / T(2) + z[63] + -z[68];
z[63] = z[25] * z[53];
z[68] = z[82] + -z[95] / T(2);
z[68] = z[5] * z[68];
z[61] = z[5] * z[61];
z[79] = -z[48] + z[61];
z[79] = z[14] * z[79];
z[61] = z[48] / T(2) + z[61];
z[93] = -(z[17] * z[61]);
z[83] = z[5] * z[83];
z[83] = z[48] + z[83];
z[96] = z[13] / T(2);
z[83] = z[83] * z[96];
z[56] = z[30] + -z[56];
z[56] = z[48] * z[56];
z[52] = z[52] + z[55] + z[56] + z[63] + z[68] + z[69] + z[77] + z[79] + z[83] + z[89] + z[93];
z[55] = (T(3) * z[11]) / T(2);
z[56] = z[55] + -z[57] + -z[62] + -z[67];
z[63] = z[18] * z[56];
z[68] = z[14] * z[56];
z[69] = z[63] + z[68];
z[77] = z[69] + T(3) * z[70];
z[79] = z[17] * z[56];
z[83] = T(3) * z[39];
z[89] = T(2) * z[51] + -z[83];
z[93] = -z[9] + z[62] + z[89];
z[97] = T(2) * z[16];
z[98] = z[93] * z[97];
z[54] = z[54] + z[62];
z[99] = T(3) * z[23];
z[54] = z[54] * z[99];
z[100] = z[88] * z[93];
z[98] = -z[54] + -z[77] + z[79] + z[98] + z[100];
z[101] = z[19] * z[98];
z[102] = T(2) * z[9];
z[103] = z[62] + z[102];
z[104] = -z[1] + T(2) * z[10];
z[105] = T(3) * z[8];
z[106] = z[103] + z[104] + -z[105];
z[107] = z[16] * z[106];
z[91] = z[21] * z[91];
z[91] = T(3) * z[91];
z[108] = z[91] + z[107];
z[109] = -(z[14] * z[106]);
z[103] = -z[51] + z[103];
z[110] = z[18] * z[103];
z[111] = T(2) * z[17];
z[112] = z[103] * z[111];
z[113] = z[9] + z[10];
z[114] = T(2) * z[1] + z[62] + -z[113];
z[115] = z[13] * z[114];
z[109] = z[108] + z[109] + -z[110] + -z[112] + z[115];
z[109] = z[3] * z[109];
z[89] = z[37] + z[89];
z[115] = z[50] + -z[102];
z[116] = z[89] + -z[115];
z[117] = z[18] + z[97];
z[117] = z[116] * z[117];
z[118] = z[106] * z[111];
z[89] = -(z[89] * z[99]);
z[113] = -z[8] + -z[36] + z[113];
z[119] = z[30] * z[113];
z[119] = T(6) * z[119];
z[120] = z[9] + z[51];
z[121] = T(4) * z[36] + z[37] + -z[120];
z[122] = z[14] * z[121];
z[89] = z[89] + z[100] + z[117] + z[118] + -z[119] + T(-2) * z[122];
z[89] = z[4] * z[89];
z[100] = z[13] * z[56];
z[117] = z[97] * z[106];
z[117] = z[100] + z[117] + z[118];
z[118] = T(2) * z[37];
z[123] = -z[55] + -z[57] + z[118];
z[124] = T(5) * z[36];
z[67] = -z[67] + z[123] + z[124];
z[125] = z[18] * z[67];
z[126] = T(3) * z[66];
z[127] = (T(3) * z[95]) / T(2);
z[102] = z[102] + z[104];
z[104] = T(3) * z[11];
z[128] = T(-6) * z[8] + T(11) * z[36] + T(5) * z[37] + -z[102] + z[104];
z[128] = z[14] * z[128];
z[119] = -z[117] + z[119] + z[125] + -z[126] + z[127] + z[128];
z[119] = z[2] * z[119];
z[56] = z[16] * z[56];
z[125] = (T(3) * z[94]) / T(2);
z[112] = z[56] + z[100] + z[112] + z[125];
z[128] = (T(3) * z[39]) / T(2);
z[129] = z[37] / T(2) + -z[128];
z[130] = z[51] + z[129];
z[115] = z[115] + z[130];
z[115] = z[18] * z[115];
z[67] = z[14] * z[67];
z[131] = z[37] + z[39];
z[132] = -z[11] + z[36];
z[131] = z[131] / T(2) + z[132];
z[131] = z[99] * z[131];
z[67] = z[67] + -z[112] + z[115] + -z[131];
z[67] = z[20] * z[67];
z[53] = -(z[27] * z[53]);
z[115] = prod_pow(z[44], 2);
z[133] = z[14] + (T(3) * z[18]) / T(4) + -z[23] / T(4) + z[96];
z[133] = z[115] * z[133];
z[134] = prod_pow(z[41], 2);
z[135] = -z[115] + z[134] / T(2);
z[135] = z[15] * z[135];
z[53] = z[53] + z[133] + z[135];
z[133] = T(2) * z[30];
z[86] = z[13] + -z[86] + z[97] + z[111] + -z[133];
z[97] = T(3) * z[46];
z[86] = z[86] * z[97];
z[135] = -z[59] / T(20) + -z[133];
z[111] = z[14] + (T(5) * z[18]) / T(2) + z[88] + z[111] + T(3) * z[135];
z[111] = z[41] * z[111];
z[135] = T(3) * z[44];
z[136] = -z[13] + z[14];
z[136] = z[135] * z[136];
z[111] = z[111] + z[136];
z[111] = z[41] * z[111];
z[115] = (T(3) * z[115]) / T(2) + T(2) * z[134];
z[115] = z[16] * z[115];
z[134] = prod_pow(z[7], 2);
z[134] = z[134] / T(2);
z[136] = z[14] + z[18] + (T(-11) * z[59]) / T(30);
z[136] = (T(-11) * z[13]) / T(6) + z[16] / T(6) + (T(13) * z[17]) / T(6) + -z[23] / T(2) + z[136] / T(3);
z[136] = z[134] * z[136];
z[53] = T(3) * z[53] + z[67] + z[86] + z[89] + z[101] + T(2) * z[109] + z[111] + z[115] + z[119] + z[136];
z[53] = z[7] * z[53];
z[67] = z[41] * z[42];
z[67] = (T(12) * z[43]) / T(5) + z[67];
z[67] = z[59] * z[67];
z[53] = z[53] + T(3) * z[67];
z[53] = int_to_imaginary<T>(1) * z[53];
z[67] = z[100] / T(2);
z[86] = z[17] * z[106];
z[89] = z[51] / T(4);
z[106] = z[9] / T(4);
z[109] = (T(9) * z[11]) / T(4) + (T(-7) * z[36]) / T(2) + -z[89] + -z[106] + -z[118];
z[109] = z[18] * z[109];
z[95] = (T(3) * z[95]) / T(4);
z[66] = (T(3) * z[66]) / T(2);
z[111] = T(3) * z[82];
z[115] = -z[11] + z[37] + z[50];
z[115] = z[87] * z[115];
z[57] = -z[1] + z[10] / T(2) + (T(-13) * z[36]) / T(2) + (T(-7) * z[37]) / T(2) + z[57] + z[105];
z[57] = z[14] * z[57];
z[57] = z[57] + z[66] + -z[67] + -z[86] + -z[95] + -z[107] + z[109] + z[111] + z[115];
z[57] = z[2] * z[57];
z[84] = z[84] * z[87];
z[69] = z[69] + z[84] + z[111] + -z[117];
z[84] = z[3] * z[69];
z[56] = z[56] + z[79];
z[79] = z[88] * z[114];
z[88] = z[14] * z[114];
z[63] = z[56] + -z[63] + z[79] + z[88] + z[127];
z[79] = z[19] * z[63];
z[69] = -(z[4] * z[69]);
z[104] = T(-10) * z[36] + T(-4) * z[37] + z[104] + z[120];
z[104] = z[14] * z[104];
z[107] = -(z[18] * z[121]);
z[56] = -z[56] + -z[100] + z[104] + z[107] + -z[126];
z[56] = z[20] * z[56];
z[56] = z[56] + z[57] + z[69] + z[79] + -z[84];
z[56] = z[2] * z[56];
z[57] = T(5) * z[9] + (T(-9) * z[41]) / T(2) + (T(-7) * z[44]) / T(2) + -z[124] + T(3) * z[130];
z[57] = z[16] * z[57];
z[69] = (T(5) * z[41]) / T(2) + -z[58] + -z[135];
z[69] = z[15] * z[69];
z[57] = -z[57] + -z[69] + z[82];
z[69] = T(5) * z[1] + T(7) * z[10];
z[69] = T(-5) * z[39] + z[60] + z[62] + z[69] / T(2) + -z[78];
z[69] = z[69] * z[96];
z[78] = T(7) * z[1] + T(9) * z[10];
z[60] = -z[8] + (T(-13) * z[9]) / T(2) + T(-3) * z[37] + -z[60] + -z[75] + z[78] / T(2);
z[60] = z[60] * z[92];
z[75] = z[11] + z[76] + -z[102];
z[75] = z[14] * z[75];
z[49] = (T(17) * z[39]) / T(2) + -z[49] + T(-5) * z[51];
z[49] = z[49] / T(2) + z[132];
z[49] = z[23] * z[49];
z[76] = (T(-7) * z[9]) / T(4) + z[11] / T(4) + z[36] + -z[39] + (T(3) * z[51]) / T(4);
z[76] = z[18] * z[76];
z[78] = (T(-23) * z[13]) / T(4) + (T(5) * z[14]) / T(2) + (T(7) * z[17]) / T(2) + z[18] / T(8) + (T(33) * z[23]) / T(8) + -z[30];
z[78] = z[44] * z[78];
z[79] = z[13] + (T(13) * z[14]) / T(4) + (T(-9) * z[17]) / T(4) + z[18];
z[79] = z[41] * z[79];
z[49] = z[49] + -z[57] / T(2) + z[60] + z[69] + z[75] + z[76] + z[78] + z[79] + z[91] + z[94] / T(4);
z[49] = z[7] * z[49];
z[57] = -(z[42] * z[59]);
z[49] = z[49] + z[57];
z[49] = z[7] * z[49];
z[57] = z[31] + z[35];
z[60] = z[34] + z[57];
z[69] = z[2] / T(2);
z[60] = z[60] * z[69];
z[69] = z[4] * z[31];
z[75] = z[20] * z[59];
z[76] = z[31] + -z[34];
z[78] = z[38] + z[76];
z[79] = z[19] * z[78];
z[82] = z[3] * z[31];
z[60] = z[60] + -z[69] + -z[75] + z[79] + -z[82];
z[60] = z[2] * z[60];
z[69] = z[20] * z[72];
z[72] = -z[3] + z[19];
z[72] = z[38] * z[72];
z[75] = z[4] * z[64];
z[69] = z[69] + -z[72] + z[75];
z[69] = z[20] * z[69];
z[72] = prod_pow(z[3], 2);
z[75] = z[25] + z[72] / T(2);
z[75] = z[75] * z[76];
z[78] = z[3] * z[78];
z[79] = z[19] * z[90];
z[78] = z[78] + -z[79];
z[78] = z[19] * z[78];
z[64] = z[19] * z[64];
z[79] = -z[64] + z[82];
z[79] = z[4] * z[79];
z[71] = z[28] * z[71];
z[59] = z[31] + z[59];
z[59] = z[6] * z[59];
z[82] = -z[34] + z[38];
z[82] = z[5] * z[82];
z[59] = z[59] + z[60] + z[69] + z[71] + z[75] + -z[78] + z[79] + -z[82];
z[60] = -z[35] + z[76];
z[60] = z[2] * z[60];
z[69] = z[20] * z[38];
z[71] = z[27] * z[76];
z[60] = -z[60] + -z[64] + z[69] + z[71];
z[64] = T(3) * z[74];
z[60] = z[60] * z[64];
z[57] = z[57] * z[134];
z[57] = -z[57] + T(3) * z[59] + -z[60];
z[59] = -z[73] / T(2);
z[57] = z[57] * z[59];
z[59] = -(z[16] * z[116]);
z[60] = z[87] * z[113];
z[64] = z[36] + -z[120] + -z[129];
z[64] = z[18] * z[64];
z[69] = z[99] * z[130];
z[71] = -(z[13] * z[93]);
z[59] = z[59] + z[60] + z[64] + z[69] + z[71] + -z[86] + z[122];
z[59] = z[4] * z[59];
z[59] = z[59] + z[84] + -z[101];
z[59] = z[4] * z[59];
z[60] = (T(5) * z[51]) / T(2);
z[64] = z[50] + z[60] + -z[83] + z[123];
z[64] = z[64] * z[65];
z[69] = z[18] * z[93];
z[71] = -(z[17] * z[103]);
z[54] = -z[54] + z[64] + -z[68] + z[69] + z[71] + -z[125];
z[54] = z[19] * z[54];
z[64] = z[4] * z[98];
z[69] = (T(3) * z[11]) / T(4);
z[71] = -z[62] + z[69];
z[73] = (T(5) * z[51]) / T(4) + -z[71] + -z[106] + -z[128];
z[65] = -(z[65] * z[73]);
z[51] = -z[51] + z[62];
z[51] = z[9] + z[51] / T(2);
z[51] = z[17] * z[51];
z[51] = z[51] + z[65] + z[66] + z[77] / T(2) + z[131];
z[51] = z[20] * z[51];
z[65] = -z[68] + z[110] + z[112];
z[65] = z[3] * z[65];
z[51] = z[51] + z[54] + z[64] + z[65];
z[51] = z[20] * z[51];
z[54] = (T(5) * z[9]) / T(4);
z[64] = T(5) * z[10];
z[65] = -z[1] + z[64];
z[65] = z[54] + z[62] + z[65] / T(4) + z[69] + -z[105];
z[65] = z[14] * z[65];
z[54] = z[54] + -z[71] + -z[89];
z[66] = z[18] * z[54];
z[55] = (T(7) * z[9]) / T(2) + z[55] + -z[60] + z[62];
z[55] = z[55] * z[92];
z[60] = (T(3) * z[94]) / T(4);
z[55] = z[55] + -z[60] + z[65] + z[66] + -z[67] + z[95] + -z[108];
z[55] = z[55] * z[72];
z[62] = T(19) * z[1] + -z[64];
z[50] = (T(-11) * z[9]) / T(4) + z[50] + z[62] / T(4) + z[69] + z[118] + -z[128];
z[50] = z[13] * z[50];
z[62] = -z[16] + z[18];
z[62] = z[62] * z[73];
z[54] = -(z[17] * z[54]);
z[50] = z[50] + z[54] + z[60] + z[62] + (T(-3) * z[70]) / T(2) + z[88] + z[91];
z[50] = z[19] * z[50];
z[54] = -(z[3] * z[63]);
z[50] = z[50] + z[54];
z[50] = z[19] * z[50];
z[54] = -(z[6] * z[80]);
z[54] = z[54] + -z[61];
z[60] = prod_pow(z[44], 3);
z[60] = T(2) * z[60];
z[61] = prod_pow(z[41], 3);
z[54] = T(3) * z[54] + z[60] + (T(-5) * z[61]) / T(6);
z[54] = z[16] * z[54];
z[62] = z[13] + z[133];
z[63] = T(2) * z[14] + -z[62] + z[81];
z[60] = z[60] * z[63];
z[63] = (T(2) * z[13]) / T(3) + (T(-13) * z[14]) / T(6) + (T(-5) * z[17]) / T(6) + (T(-7) * z[18]) / T(6) + z[87];
z[63] = z[61] * z[63];
z[58] = z[6] * z[58];
z[58] = T(3) * z[58] + -z[61];
z[58] = z[15] * z[58];
z[61] = -z[17] + z[62] + -z[85];
z[61] = z[41] * z[61] * z[97];
z[62] = (T(23) * z[18]) / T(24) + -z[30];
z[62] = (T(157) * z[13]) / T(12) + (T(109) * z[14]) / T(6) + (T(11) * z[15]) / T(2) + (T(109) * z[16]) / T(12) + (T(215) * z[17]) / T(6) + (T(-105) * z[23]) / T(8) + T(35) * z[62];
z[62] = z[45] * z[62];
return z[49] + z[50] + z[51] + T(3) * z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] / T(2) + z[63];
}



template IntegrandConstructorType<double> f_4_99_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_99_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_99_construct (const Kin<qd_real>&);
#endif

}