#include "f_4_143.h"

namespace PentagonFunctions {

template <typename T> T f_4_143_abbreviated (const std::array<T,20>&);

template <typename T> class SpDLog_f_4_143_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_143_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(9) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * ((T(-3) * kin.v[0]) / T(4) + (T(3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(9) * kin.v[3]) / T(4) + T(-3) + T(3) * kin.v[4]);
c[1] = kin.v[1] * (T(3) + T(-3) * kin.v[1] + T(9) * kin.v[2] + T(6) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[0] * (T(3) + (T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-9) * kin.v[1] + T(12) * kin.v[2] + T(9) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[1] + T(16) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4])) + kin.v[2] * (T(-3) + T(-6) * kin.v[2] + T(-9) * kin.v[3] + T(3) * kin.v[4]) + kin.v[3] * (T(-3) + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(8) + T(8) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[3] * (T(-8) + T(8) * kin.v[4]) + kin.v[2] * (T(-8) + T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[1] * ((T(27) * kin.v[1]) / T(2) + T(14) + T(-13) * kin.v[2] + T(-27) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[3] * ((T(27) * kin.v[3]) / T(2) + T(-14) + T(14) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(-14) + T(13) * kin.v[3] + T(14) * kin.v[4]) + kin.v[0] * (T(14) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(13) * kin.v[1] + kin.v[2] + T(-13) * kin.v[3] + T(-14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[2] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(8) + T(4) * kin.v[1] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[3] * (T(-8) + T(4) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[3] * ((T(-45) * kin.v[3]) / T(4) + T(11) + T(-11) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(4) + (T(-23) * kin.v[3]) / T(2) + T(11) + T(-11) * kin.v[4]) + kin.v[1] * ((T(-45) * kin.v[1]) / T(4) + (T(23) * kin.v[2]) / T(2) + (T(45) * kin.v[3]) / T(2) + T(-11) + T(11) * kin.v[4]) + kin.v[0] * ((T(-23) * kin.v[1]) / T(2) + kin.v[2] / T(2) + (T(23) * kin.v[3]) / T(2) + T(-11) + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(11) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[2] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-4) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-4) * kin.v[1] + T(8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * ((T(-27) * kin.v[3]) / T(2) + T(14) + T(-14) * kin.v[4]) + kin.v[2] * (kin.v[2] / T(2) + T(14) + T(-13) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[1] * ((T(-27) * kin.v[1]) / T(2) + T(-14) + T(13) * kin.v[2] + T(27) * kin.v[3] + T(14) * kin.v[4]) + kin.v[0] * (-kin.v[2] + T(-14) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(-13) * kin.v[1] + T(13) * kin.v[3] + T(14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[2] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-4) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-4) * kin.v[1] + T(8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[1] * (T(28) + T(27) * kin.v[1] + T(-26) * kin.v[2] + T(-54) * kin.v[3] + T(-28) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-28) + T(26) * kin.v[3] + T(28) * kin.v[4]) + kin.v[3] * (T(-28) + T(27) * kin.v[3] + T(28) * kin.v[4]) + kin.v[0] * (T(28) + (T(-1) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(26) * kin.v[1] + T(2) * kin.v[2] + T(-26) * kin.v[3] + T(-28) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) + T(16) * kin.v[2] + T(-16) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(16) + T(8) * kin.v[1] + T(-16) * kin.v[3] + T(-16) * kin.v[4]) + kin.v[2] * (T(-16) + T(-8) * kin.v[2] + T(16) * kin.v[4]) + kin.v[3] * (T(-16) + T(8) * kin.v[3] + T(16) * kin.v[4])));
c[2] = T(-3) * kin.v[0] + T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3];
c[3] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(16) + T(12)) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[2] + T(-12) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) * kin.v[1] + T(-16) * kin.v[2] + T(-16) * kin.v[3])) + rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] + T(-8) * kin.v[2] + T(-8) * kin.v[3])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[2] + T(6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-9) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-9) * kin.v[1] + T(9) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (abb[2] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(16) + abb[2] * ((abb[4] * T(3)) / T(2) + (abb[8] * T(11)) / T(2) + abb[10] * T(-14) + abb[5] * T(7))) + abb[9] * (abb[3] * T(-6) + abb[2] * (abb[2] * T(-7) + bc<T>[0] * int_to_imaginary<T>(8)) + abb[2] * abb[7] * T(8)) + abb[3] * (abb[10] * T(-12) + abb[4] * T(-3) + abb[5] * T(6) + abb[8] * T(9)) + abb[1] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-16) + abb[1] * (abb[9] + (abb[4] * T(-9)) / T(2) + (abb[8] * T(7)) / T(2) + -abb[5] + abb[10] * T(2)) + abb[9] * (abb[7] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[2] * T(6)) + abb[7] * (abb[10] * T(-16) + abb[5] * T(8) + abb[8] * T(8)) + abb[2] * (abb[8] * T(-9) + abb[5] * T(-6) + abb[4] * T(3) + abb[10] * T(12))) + abb[2] * abb[7] * (abb[5] * T(-8) + abb[8] * T(-8) + abb[10] * T(16)) + abb[6] * (abb[2] * abb[9] * T(-8) + abb[2] * (abb[10] * T(-16) + abb[5] * T(8) + abb[8] * T(8)) + abb[1] * (abb[5] * T(-8) + abb[8] * T(-8) + abb[9] * T(8) + abb[10] * T(16))));
    }
};
template <typename T> class SpDLog_f_4_143_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_143_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(9) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-3) + T(3) * kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(3)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[2] + ((T(9) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[3];
c[1] = kin.v[3] * (T(3) + T(-3) * kin.v[3] + T(-9) * kin.v[4]) + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[4] * (T(3) + T(-6) * kin.v[4]) + kin.v[2] * (T(-3) + T(-6) * kin.v[2] + T(9) * kin.v[3] + T(12) * kin.v[4]) + kin.v[1] * (T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[1] + T(-9) * kin.v[2] + T(6) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[0] + T(-8) * kin.v[2] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(-8) * kin.v[4]) + kin.v[0] * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (T(-8) + T(-8) * kin.v[2] + T(8) * kin.v[3] + T(16) * kin.v[4])) + rlog(kin.v[2]) * ((-kin.v[4] / T(2) + T(14)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-14) + T(-13) * kin.v[3] + kin.v[4]) + kin.v[0] * (T(14) * kin.v[2] + T(-14) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[1] * (T(-14) + T(14) * kin.v[0] + (T(27) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(13) * kin.v[2] + T(-27) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[0] + T(-8) * kin.v[3]) + T(-13) * kin.v[4]) + kin.v[3] * ((T(27) * kin.v[3]) / T(2) + T(14) + T(13) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[0] * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(-4) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[2] / T(4) + (T(23) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(11)) * kin.v[2] + ((T(-45) * kin.v[3]) / T(4) + (T(-23) * kin.v[4]) / T(2) + T(-11)) * kin.v[3] + kin.v[1] * ((T(-23) * kin.v[2]) / T(2) + (T(45) * kin.v[3]) / T(2) + (T(23) * kin.v[4]) / T(2) + T(11) + T(-11) * kin.v[0] + (T(-45) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[0] + T(8) * kin.v[3])) + (-kin.v[4] / T(4) + T(-11)) * kin.v[4] + kin.v[0] * (T(-11) * kin.v[2] + T(11) * kin.v[3] + T(11) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[4] * (T(-8) + T(4) * kin.v[4]) + kin.v[0] * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[2] * (kin.v[2] / T(2) + -kin.v[4] + T(14) + T(13) * kin.v[3]) + (kin.v[4] / T(2) + T(-14)) * kin.v[4] + kin.v[3] * ((T(-27) * kin.v[3]) / T(2) + T(-14) + T(-13) * kin.v[4]) + kin.v[1] * (T(14) + T(-14) * kin.v[0] + (T(-27) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-13) * kin.v[2] + T(27) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[0] + T(8) * kin.v[3]) + T(13) * kin.v[4]) + kin.v[0] * (T(-14) * kin.v[2] + T(14) * kin.v[3] + T(14) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[4] * (T(-8) + T(4) * kin.v[4]) + kin.v[0] * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-kin.v[4] + T(28)) * kin.v[4] + kin.v[0] * (T(28) * kin.v[2] + T(-28) * kin.v[3] + T(-28) * kin.v[4]) + kin.v[1] * (T(-28) + T(28) * kin.v[0] + (bc<T>[0] * int_to_imaginary<T>(8) + T(27)) * kin.v[1] + T(26) * kin.v[2] + T(-54) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) + T(16) * kin.v[0] + T(-16) * kin.v[3]) + T(-26) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-28) + T(-26) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (T(28) + T(27) * kin.v[3] + T(26) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(16) + T(8) * kin.v[3]) + kin.v[0] * (T(16) * kin.v[2] + T(-16) * kin.v[3] + T(-16) * kin.v[4]) + kin.v[4] * (T(16) + T(-8) * kin.v[4]) + kin.v[2] * (T(-16) + T(-8) * kin.v[2] + T(16) * kin.v[4])));
c[2] = T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4];
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(9)) * kin.v[1] + T(9) * kin.v[2] + T(-9) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-12) + bc<T>[0] * int_to_imaginary<T>(-16)) * kin.v[1] + T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) * kin.v[2] + T(16) * kin.v[3] + T(16) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[11] * (abb[2] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(16) + abb[2] * ((abb[13] * T(3)) / T(2) + (abb[5] * T(11)) / T(2) + abb[10] * T(-14) + abb[8] * T(7))) + abb[9] * (abb[12] * T(-6) + abb[2] * (abb[2] * T(-7) + bc<T>[0] * int_to_imaginary<T>(8)) + abb[1] * abb[2] * T(8)) + abb[12] * (abb[10] * T(-12) + abb[13] * T(-3) + abb[8] * T(6) + abb[5] * T(9)) + abb[7] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-16) + abb[7] * (abb[9] + (abb[13] * T(-9)) / T(2) + (abb[5] * T(7)) / T(2) + -abb[8] + abb[10] * T(2)) + abb[9] * (abb[1] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[2] * T(6)) + abb[1] * (abb[10] * T(-16) + abb[5] * T(8) + abb[8] * T(8)) + abb[2] * (abb[5] * T(-9) + abb[8] * T(-6) + abb[13] * T(3) + abb[10] * T(12))) + abb[1] * abb[2] * (abb[5] * T(-8) + abb[8] * T(-8) + abb[10] * T(16)) + abb[6] * (abb[2] * abb[9] * T(-8) + abb[2] * (abb[10] * T(-16) + abb[5] * T(8) + abb[8] * T(8)) + abb[7] * (abb[5] * T(-8) + abb[8] * T(-8) + abb[9] * T(8) + abb[10] * T(16))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_143_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl22 = DLog_W_22<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),spdl21 = SpDLog_f_4_143_W_21<T>(kin),spdl22 = SpDLog_f_4_143_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,20> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), -rlog(t), rlog(kin.W[15] / kin_path.W[15]), rlog(v_path[2]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[19] / kin_path.W[19]), dl22(t), f_2_1_14(kin_path), -rlog(t), dl3(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl20(t), dl19(t), dl16(t)}
;

        auto result = f_4_143_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_143_abbreviated(const std::array<T,20>& abb)
{
using TR = typename T::value_type;
T z[73];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[14];
z[3] = abb[17];
z[4] = abb[18];
z[5] = abb[19];
z[6] = abb[5];
z[7] = abb[8];
z[8] = abb[9];
z[9] = abb[10];
z[10] = abb[13];
z[11] = abb[2];
z[12] = abb[6];
z[13] = abb[7];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[12];
z[17] = abb[15];
z[18] = abb[16];
z[19] = bc<TR>[9];
z[20] = z[3] + -z[4];
z[21] = -z[2] + z[5];
z[22] = z[20] + z[21];
z[23] = T(3) * z[18];
z[24] = z[22] * z[23];
z[25] = T(2) * z[12];
z[26] = -z[4] + z[21];
z[27] = -(z[25] * z[26]);
z[28] = T(3) * z[3];
z[29] = -z[4] + z[28];
z[30] = z[2] + z[5];
z[30] = z[29] + T(2) * z[30];
z[31] = -(z[13] * z[30]);
z[32] = T(2) * z[3];
z[33] = z[26] + z[32];
z[34] = T(2) * z[0];
z[35] = -(z[33] * z[34]);
z[36] = T(3) * z[2];
z[37] = T(4) * z[3];
z[38] = z[36] + z[37];
z[39] = z[5] + z[38];
z[40] = z[11] * z[39];
z[27] = z[24] + z[27] + z[31] + z[35] + z[40];
z[27] = z[7] * z[27];
z[31] = -z[2] + z[4];
z[35] = -z[5] + z[31];
z[40] = z[3] + z[35];
z[23] = z[23] * z[40];
z[41] = z[32] + z[35];
z[42] = -(z[13] * z[41]);
z[43] = z[12] * z[35];
z[42] = z[42] + -z[43];
z[44] = T(2) * z[4];
z[45] = -z[5] + z[28] + z[44];
z[46] = T(2) * z[2];
z[47] = z[45] + z[46];
z[48] = -(z[0] * z[47]);
z[49] = z[4] + z[38];
z[50] = z[11] * z[49];
z[42] = z[23] + T(2) * z[42] + z[48] + z[50];
z[42] = z[6] * z[42];
z[48] = T(4) * z[12];
z[50] = -z[13] + z[48];
z[50] = z[20] * z[50];
z[51] = -z[21] + z[37];
z[52] = z[11] * z[51];
z[53] = z[0] * z[26];
z[24] = -z[24] + z[50] + z[52] + T(4) * z[53];
z[24] = z[1] * z[24];
z[50] = z[3] + -z[5];
z[52] = z[12] * z[50];
z[53] = z[13] * z[35];
z[53] = z[52] + z[53];
z[54] = -z[31] + z[37];
z[55] = z[11] * z[54];
z[56] = -(z[0] * z[50]);
z[23] = -z[23] + T(4) * z[53] + z[55] + z[56];
z[23] = z[10] * z[23];
z[53] = -z[21] + z[32];
z[55] = -z[4] + z[53];
z[56] = z[12] * z[55];
z[53] = z[4] + z[53];
z[57] = z[0] * z[53];
z[58] = -z[32] + z[35];
z[59] = z[13] * z[58];
z[60] = z[2] + z[32];
z[61] = z[11] * z[60];
z[56] = -z[56] + z[57] + -z[59] + T(-2) * z[61];
z[56] = T(4) * z[56];
z[57] = z[9] * z[56];
z[23] = z[23] + z[24] + z[27] + z[42] + z[57];
z[23] = int_to_imaginary<T>(1) * z[23];
z[24] = z[20] + z[21] / T(3);
z[24] = z[1] * z[24];
z[27] = z[31] / T(3) + z[50];
z[27] = z[10] * z[27];
z[24] = z[24] + z[27];
z[27] = z[9] * z[55];
z[42] = z[5] + (T(-13) * z[31]) / T(3);
z[42] = z[3] + z[42] / T(2);
z[42] = z[6] * z[42];
z[57] = z[4] + (T(-13) * z[21]) / T(3);
z[57] = z[3] + z[57] / T(2);
z[57] = z[7] * z[57];
z[59] = z[4] + z[21];
z[61] = -z[3] + z[59] / T(2);
z[61] = z[14] * z[61];
z[62] = int_to_imaginary<T>(1) * z[61];
z[24] = T(2) * z[24] + T(-3) * z[27] + z[42] + z[57] + -z[62] / T(3);
z[24] = z[14] * z[24];
z[23] = T(2) * z[23] + z[24];
z[23] = z[14] * z[23];
z[24] = T(4) * z[2];
z[27] = z[5] + z[24];
z[42] = (T(11) * z[3]) / T(2);
z[57] = T(3) * z[4];
z[62] = -z[27] + -z[42] + -z[57];
z[62] = z[11] * z[62];
z[49] = -(z[25] * z[49]);
z[63] = T(9) * z[3];
z[27] = T(2) * z[27] + z[63];
z[27] = z[13] * z[27];
z[64] = z[34] * z[38];
z[27] = z[27] + z[49] + z[62] + z[64];
z[27] = z[11] * z[27];
z[49] = T(4) * z[4];
z[62] = -z[21] + -z[28] + -z[49];
z[62] = z[15] * z[62];
z[41] = z[41] * z[48];
z[44] = -z[44] + z[46];
z[46] = T(3) * z[5];
z[64] = z[44] + -z[46];
z[65] = (T(7) * z[3]) / T(2);
z[66] = z[64] + -z[65];
z[66] = z[13] * z[66];
z[41] = z[41] + z[66];
z[41] = z[13] * z[41];
z[66] = z[4] + z[5];
z[32] = z[32] + z[36] + -z[66];
z[32] = z[13] * z[32];
z[47] = z[12] * z[47];
z[47] = T(-2) * z[32] + z[47];
z[45] = z[36] + -z[45];
z[45] = z[0] * z[45];
z[45] = z[45] + T(2) * z[47];
z[45] = z[0] * z[45];
z[35] = -z[28] + -z[35];
z[47] = prod_pow(z[12], 2);
z[35] = z[35] * z[47];
z[67] = -z[5] + z[24] + -z[49];
z[68] = -z[63] + T(2) * z[67];
z[68] = z[16] * z[68];
z[27] = z[27] + z[35] + z[41] + z[45] + T(2) * z[62] + z[68];
z[27] = z[6] * z[27];
z[35] = -z[12] + z[13];
z[35] = z[35] * z[51];
z[41] = T(7) * z[3];
z[45] = z[41] + -z[49];
z[45] = z[0] * z[45];
z[51] = (T(21) * z[3]) / T(2);
z[62] = T(2) * z[21];
z[68] = z[4] + z[62];
z[68] = -z[51] + T(2) * z[68];
z[68] = z[11] * z[68];
z[35] = T(2) * z[35] + z[45] + z[68];
z[35] = z[11] * z[35];
z[45] = z[20] * z[25];
z[68] = -z[20] + T(5) * z[21];
z[68] = z[13] * z[68];
z[45] = z[45] + z[68];
z[45] = z[13] * z[45];
z[59] = z[13] * z[59];
z[68] = -(z[12] * z[26]);
z[68] = -z[59] + z[68];
z[62] = z[57] + z[62];
z[69] = z[3] / T(2);
z[70] = T(2) * z[62] + -z[69];
z[70] = z[0] * z[70];
z[68] = T(8) * z[68] + z[70];
z[68] = z[0] * z[68];
z[70] = z[4] + T(4) * z[21];
z[71] = -z[41] + T(4) * z[70];
z[71] = z[15] * z[71];
z[72] = -z[20] + T(3) * z[21];
z[72] = z[47] * z[72];
z[20] = z[20] + -z[21];
z[20] = T(10) * z[20];
z[21] = -(z[16] * z[20]);
z[21] = z[21] + z[35] + z[45] + z[68] + z[71] + z[72];
z[21] = z[1] * z[21];
z[34] = -z[25] + z[34];
z[34] = z[34] * z[54];
z[35] = T(4) * z[5];
z[45] = -z[35] + z[41];
z[45] = z[13] * z[45];
z[44] = z[5] + -z[44];
z[44] = T(2) * z[44] + -z[51];
z[44] = z[11] * z[44];
z[34] = z[34] + z[44] + z[45];
z[34] = z[11] * z[34];
z[44] = -z[36] + -z[50] + z[57];
z[44] = z[44] * z[47];
z[45] = z[52] + T(-4) * z[59];
z[50] = T(5) * z[31] + -z[50];
z[50] = z[0] * z[50];
z[45] = T(2) * z[45] + z[50];
z[45] = z[0] * z[45];
z[20] = -(z[15] * z[20]);
z[41] = -z[41] + T(-4) * z[67];
z[41] = z[16] * z[41];
z[50] = T(-2) * z[64] + -z[69];
z[50] = z[13] * z[50];
z[43] = T(-8) * z[43] + z[50];
z[43] = z[13] * z[43];
z[20] = z[20] + z[34] + z[41] + z[43] + z[44] + z[45];
z[20] = z[10] * z[20];
z[34] = -(z[12] * z[39]);
z[38] = z[13] * z[38];
z[34] = z[34] + z[38];
z[38] = z[4] + z[24];
z[39] = -z[38] + -z[42] + -z[46];
z[39] = z[11] * z[39];
z[38] = T(2) * z[38] + z[63];
z[38] = z[0] * z[38];
z[34] = T(2) * z[34] + z[38] + z[39];
z[34] = z[11] * z[34];
z[31] = -z[28] + -z[31] + -z[35];
z[31] = z[16] * z[31];
z[25] = z[25] * z[30];
z[29] = T(-2) * z[5] + -z[29] + z[36];
z[29] = z[13] * z[29];
z[25] = z[25] + z[29];
z[25] = z[13] * z[25];
z[29] = z[12] * z[33];
z[29] = z[29] + -z[32];
z[30] = -z[62] + -z[65];
z[30] = z[0] * z[30];
z[29] = T(4) * z[29] + z[30];
z[29] = z[0] * z[29];
z[30] = -z[63] + T(-2) * z[70];
z[30] = z[15] * z[30];
z[26] = -z[26] + -z[28];
z[26] = z[26] * z[47];
z[25] = z[25] + z[26] + z[29] + z[30] + T(2) * z[31] + z[34];
z[25] = z[7] * z[25];
z[26] = T(8) * z[3];
z[24] = z[24] + z[26];
z[28] = -z[4] + z[24];
z[28] = z[0] * z[28];
z[29] = z[48] * z[60];
z[24] = -z[5] + z[24];
z[30] = z[13] * z[24];
z[28] = -z[28] + z[29] + -z[30];
z[29] = T(8) * z[2] + T(16) * z[3] + -z[66];
z[29] = z[11] * z[29];
z[28] = T(2) * z[28] + z[29];
z[28] = z[11] * z[28];
z[26] = -z[26] + z[70];
z[26] = z[15] * z[26];
z[24] = z[24] + -z[49];
z[24] = z[16] * z[24];
z[29] = z[47] * z[55];
z[24] = -z[24] + z[26] + -z[29];
z[26] = z[60] + z[66];
z[26] = z[13] * z[26];
z[29] = z[12] * z[53];
z[26] = z[26] + -z[29];
z[29] = -z[37] + z[62];
z[29] = z[0] * z[29];
z[26] = T(4) * z[26] + -z[29];
z[26] = z[0] * z[26];
z[29] = z[37] + z[64];
z[29] = z[13] * z[29];
z[30] = z[48] * z[58];
z[29] = z[29] + z[30];
z[29] = z[13] * z[29];
z[24] = T(2) * z[24] + -z[26] + -z[28] + -z[29];
z[26] = z[9] * z[24];
z[28] = int_to_imaginary<T>(1) * z[56];
z[28] = z[28] + T(3) * z[61];
z[28] = z[14] * z[28];
z[24] = -z[24] + z[28];
z[24] = z[8] * z[24];
z[28] = -z[6] + z[10];
z[28] = z[28] * z[40];
z[29] = z[1] + -z[7];
z[22] = z[22] * z[29];
z[22] = z[22] + z[28];
z[22] = z[17] * z[22];
z[28] = z[19] * z[55];
return z[20] + z[21] + T(6) * z[22] + z[23] + z[24] + z[25] + T(-2) * z[26] + z[27] + (T(-32) * z[28]) / T(3);
}



template IntegrandConstructorType<double> f_4_143_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_143_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_143_construct (const Kin<qd_real>&);
#endif

}