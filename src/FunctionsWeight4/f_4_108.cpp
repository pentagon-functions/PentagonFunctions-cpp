#include "f_4_108.h"

namespace PentagonFunctions {

template <typename T> T f_4_108_abbreviated (const std::array<T,21>&);



template <typename T> IntegrandConstructorType<T> f_4_108_construct (const Kin<T>& kin) {
    return [&kin, 
            dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin),dl13 = DLog_W_13<T>(kin),dl4 = DLog_W_4<T>(kin),dl6 = DLog_W_6<T>(kin),dl11 = DLog_W_11<T>(kin),dl18 = DLog_W_18<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,21> abbr = 
            {dl1(t), rlog(kin.W[10] / kin_path.W[10]), rlog(v_path[0]), rlog(v_path[3]), f_2_1_2(kin_path), rlog(kin.W[12] / kin_path.W[12]), rlog(v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), f_2_1_9(kin_path), rlog(kin.W[5] / kin_path.W[5]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[0] / kin_path.W[0]), dl3(t), dl13(t), dl4(t), dl6(t), dl11(t), dl18(t)}
;

        auto result = f_4_108_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_108_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[94];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[10];
z[8] = abb[11];
z[9] = abb[12];
z[10] = abb[14];
z[11] = abb[7];
z[12] = abb[6];
z[13] = abb[13];
z[14] = abb[8];
z[15] = abb[9];
z[16] = bc<TR>[1];
z[17] = bc<TR>[2];
z[18] = bc<TR>[4];
z[19] = bc<TR>[7];
z[20] = bc<TR>[8];
z[21] = bc<TR>[9];
z[22] = abb[19];
z[23] = abb[17];
z[24] = abb[15];
z[25] = abb[16];
z[26] = abb[20];
z[27] = abb[18];
z[28] = T(7) * z[13];
z[29] = T(2) * z[10];
z[30] = T(4) * z[6];
z[31] = z[8] + z[9];
z[32] = z[28] + -z[29] + -z[30] + -z[31];
z[32] = z[12] * z[32];
z[33] = T(4) * z[8];
z[34] = T(3) * z[7];
z[35] = z[33] + -z[34];
z[36] = z[9] + z[13] + -z[29] + z[35];
z[37] = z[11] * z[36];
z[38] = T(2) * z[9];
z[39] = -z[34] + z[38];
z[40] = T(5) * z[8];
z[41] = z[39] + z[40];
z[42] = T(-6) * z[13] + z[30] + z[41];
z[42] = z[2] * z[42];
z[43] = int_to_imaginary<T>(1) * z[3];
z[44] = z[36] * z[43];
z[32] = z[32] + -z[37] + z[42] + z[44];
z[36] = -(z[4] * z[36]);
z[32] = T(2) * z[32] + z[36];
z[32] = z[4] * z[32];
z[36] = -z[13] + z[31];
z[42] = T(3) * z[10];
z[44] = z[34] + z[36] + -z[42];
z[44] = z[11] * z[44];
z[45] = -z[7] + z[8];
z[46] = -z[13] + z[30];
z[47] = -z[42] + T(3) * z[45] + z[46];
z[48] = -(z[2] * z[47]);
z[49] = z[30] + -z[31];
z[50] = T(2) * z[13];
z[51] = -z[10] + z[49] + -z[50];
z[52] = z[12] * z[51];
z[53] = prod_pow(z[16], 2);
z[54] = -z[18] + z[53];
z[48] = -z[44] + z[48] + z[52] + z[54];
z[52] = prod_pow(z[3], 2);
z[48] = T(2) * z[48] + (T(11) * z[52]) / T(4);
z[48] = z[43] * z[48];
z[55] = T(2) * z[12];
z[56] = -(z[51] * z[55]);
z[44] = z[44] + z[56];
z[44] = z[11] * z[44];
z[47] = z[11] * z[47];
z[56] = T(8) * z[6];
z[57] = z[10] + T(-9) * z[13] + z[56];
z[57] = z[12] * z[57];
z[47] = z[47] + z[57];
z[57] = T(12) * z[6];
z[58] = -z[10] + T(15) * z[13] + -z[41] + -z[57];
z[58] = z[2] * z[58];
z[47] = T(2) * z[47] + z[58];
z[47] = z[2] * z[47];
z[58] = T(5) * z[10];
z[59] = T(16) * z[6] + -z[58];
z[60] = T(-11) * z[13] + -z[38] + z[59];
z[60] = z[14] * z[60];
z[61] = T(3) * z[8];
z[62] = -z[7] + z[61];
z[63] = -z[38] + z[62];
z[63] = T(3) * z[63];
z[64] = T(10) * z[6];
z[65] = T(8) * z[10] + -z[64];
z[66] = T(-8) * z[13] + z[63] + -z[65];
z[66] = z[5] * z[66];
z[67] = T(5) * z[19];
z[68] = prod_pow(z[17], 3);
z[69] = -z[67] + T(2) * z[68];
z[70] = T(2) * z[18];
z[71] = -z[53] + -z[70];
z[71] = z[16] * z[71];
z[72] = prod_pow(z[12], 2);
z[73] = -(z[51] * z[72]);
z[74] = T(3) * z[17];
z[75] = -z[7] + -z[8] / T(3);
z[75] = (T(-41) * z[9]) / T(6) + (T(-5) * z[13]) / T(2) + z[74] + (T(13) * z[75]) / T(2);
z[76] = (T(8) * z[6]) / T(3);
z[75] = (T(35) * z[10]) / T(12) + -z[16] / T(3) + z[75] / T(2) + z[76];
z[75] = z[52] * z[75];
z[77] = -z[9] + z[50];
z[78] = T(2) * z[8];
z[79] = -z[7] + z[78];
z[80] = -z[6] + z[77] + -z[79];
z[80] = z[15] * z[80];
z[32] = (T(-9) * z[20]) / T(2) + (T(605) * z[21]) / T(12) + z[32] + z[44] + z[47] + z[48] + -z[60] + -z[66] + T(3) * z[69] + z[71] + z[73] + z[75] + T(6) * z[80];
z[32] = z[26] * z[32];
z[44] = z[4] + T(-2) * z[43] + z[55];
z[47] = -z[13] + -z[29] + z[49];
z[44] = z[44] * z[47];
z[48] = T(7) * z[8] + -z[34];
z[49] = T(2) * z[6];
z[69] = T(-8) * z[9] + z[48] + z[49];
z[71] = T(4) * z[10];
z[73] = -z[50] + z[69] + -z[71];
z[73] = z[11] * z[73];
z[40] = -z[34] + z[40];
z[75] = T(4) * z[13];
z[65] = T(10) * z[9] + -z[40] + z[65] + z[75];
z[65] = z[2] * z[65];
z[44] = z[44] + z[65] + z[73];
z[44] = z[4] * z[44];
z[63] = T(6) * z[6] + z[29] + -z[50] + -z[63];
z[63] = z[11] * z[63];
z[65] = z[10] + -z[13];
z[73] = z[55] * z[65];
z[69] = -z[13] + -z[58] + z[69];
z[69] = z[2] * z[69];
z[63] = z[63] + z[69] + z[73];
z[63] = z[2] * z[63];
z[51] = z[11] * z[51];
z[69] = -z[6] + z[31] + z[65];
z[73] = -(z[12] * z[69]);
z[80] = -(z[2] * z[65]);
z[73] = T(9) * z[18] + z[51] + T(4) * z[53] + z[73] + z[80];
z[73] = (T(5) * z[52]) / T(12) + T(2) * z[73];
z[73] = z[43] * z[73];
z[80] = -z[10] + T(3) * z[13];
z[81] = T(-4) * z[9] + z[45] + -z[49] + z[80];
z[82] = T(3) * z[15];
z[81] = z[81] * z[82];
z[69] = z[69] * z[72];
z[83] = -z[11] + -z[55];
z[51] = z[51] * z[83];
z[83] = (T(3) * z[6]) / T(4);
z[84] = z[7] + (T(-13) * z[8]) / T(2);
z[84] = (T(-13) * z[9]) / T(12) + -z[10] / T(12) + (T(25) * z[13]) / T(12) + (T(2) * z[16]) / T(3) + (T(8) * z[17]) / T(3) + z[83] + z[84] / T(2);
z[84] = z[52] * z[84];
z[85] = T(-16) * z[18] + (T(-19) * z[53]) / T(3);
z[85] = z[16] * z[85];
z[44] = T(-8) * z[20] + (T(176) * z[21]) / T(3) + z[44] + z[51] + z[63] + z[66] + -z[67] + (T(32) * z[68]) / T(3) + z[69] + z[73] + z[81] + z[84] + z[85];
z[44] = z[24] * z[44];
z[51] = T(7) * z[10];
z[63] = T(3) * z[6];
z[66] = -z[51] + z[63] + T(2) * z[77];
z[66] = z[12] * z[66];
z[67] = z[38] + z[79];
z[67] = T(9) * z[10] + -z[56] + T(3) * z[67] + -z[75];
z[67] = z[11] * z[67];
z[34] = -z[34] + z[78];
z[69] = T(15) * z[10];
z[73] = T(-9) * z[1] + T(-5) * z[6] + T(6) * z[9] + -z[34] + -z[50] + z[69];
z[73] = z[2] * z[73];
z[66] = T(-13) * z[18] + z[52] / T(3) + z[53] + z[66] + z[67] + z[73];
z[66] = z[43] * z[66];
z[67] = -z[9] + z[13];
z[73] = z[8] + z[67];
z[75] = -z[29] + z[73];
z[75] = z[55] * z[75];
z[81] = z[38] + -z[50];
z[71] = -z[6] + -z[8] + z[71] + z[81];
z[71] = z[11] * z[71];
z[84] = z[6] + -z[8];
z[84] = z[2] * z[84];
z[71] = z[71] + z[75] + z[84];
z[75] = T(5) * z[9];
z[84] = z[34] + z[75];
z[85] = -z[13] + -z[84];
z[86] = T(3) * z[1];
z[87] = T(11) * z[10] + -z[86];
z[85] = z[56] + T(2) * z[85] + -z[87];
z[85] = z[43] * z[85];
z[88] = (T(3) * z[1]) / T(2);
z[89] = (T(11) * z[10]) / T(2) + -z[88];
z[46] = -z[46] + z[84] + z[89];
z[46] = z[4] * z[46];
z[46] = z[46] + T(2) * z[71] + z[85];
z[46] = z[4] * z[46];
z[71] = T(3) * z[9];
z[84] = z[33] + -z[71];
z[85] = T(5) * z[13];
z[69] = z[49] + -z[69] + T(2) * z[84] + z[85];
z[69] = z[11] * z[69];
z[84] = z[6] / T(2);
z[90] = z[10] / T(2);
z[91] = z[7] / T(2);
z[92] = -z[8] + -z[91];
z[92] = (T(9) * z[1]) / T(2) + -z[9] + z[84] + -z[90] + T(3) * z[92];
z[92] = z[2] * z[92];
z[80] = T(2) * z[31] + -z[80];
z[80] = z[12] * z[80];
z[69] = z[69] + z[80] + z[92];
z[69] = z[2] * z[69];
z[80] = z[10] + z[16];
z[92] = (T(13) * z[1]) / T(4);
z[80] = (T(13) * z[7]) / T(4) + (T(2) * z[8]) / T(3) + -z[9] / T(3) + (T(7) * z[17]) / T(4) + -z[50] + (T(25) * z[80]) / T(12) + -z[83] + -z[92];
z[80] = z[52] * z[80];
z[34] = T(11) * z[9] + (T(35) * z[10]) / T(2) + z[34] + -z[64] + -z[85] + -z[88];
z[34] = z[5] * z[34];
z[83] = -z[90] + z[91];
z[88] = z[49] + -z[61] + z[67] + z[83];
z[88] = z[82] * z[88];
z[93] = -z[31] + z[91];
z[93] = (T(-9) * z[10]) / T(2) + z[30] + z[50] + T(3) * z[93];
z[93] = z[11] * z[93];
z[61] = z[9] + -z[61];
z[51] = -z[13] + z[51] + T(2) * z[61];
z[51] = z[12] * z[51];
z[51] = z[51] + z[93];
z[51] = z[11] * z[51];
z[33] = (T(-11) * z[6]) / T(2) + (T(29) * z[10]) / T(2) + -z[33] + z[75] + -z[85];
z[33] = z[14] * z[33];
z[61] = (T(3) * z[6]) / T(2);
z[75] = (T(7) * z[10]) / T(2) + -z[61] + -z[77];
z[75] = z[72] * z[75];
z[77] = T(-4) * z[18] + (T(-3) * z[53]) / T(2);
z[77] = z[16] * z[77];
z[33] = (T(-69) * z[19]) / T(2) + (T(-21) * z[20]) / T(4) + (T(227) * z[21]) / T(8) + z[33] + z[34] + z[46] + z[51] + z[66] + T(7) * z[68] + z[69] + z[75] + z[77] + z[80] + z[88];
z[33] = z[0] * z[33];
z[34] = -z[9] + z[40];
z[34] = z[28] + T(2) * z[34] + -z[42] + -z[56];
z[34] = z[11] * z[34];
z[40] = -z[10] + -z[28] + z[38] + -z[48] + z[57];
z[40] = z[2] * z[40];
z[42] = -z[9] + z[78];
z[28] = z[28] + T(2) * z[42] + -z[59];
z[28] = z[12] * z[28];
z[28] = z[28] + z[34] + z[40];
z[28] = z[2] * z[28];
z[34] = T(2) * z[16];
z[40] = z[8] + z[13];
z[40] = -z[7] + (T(17) * z[9]) / T(6) + (T(-11) * z[17]) / T(3) + (T(31) * z[40]) / T(6);
z[40] = (T(-21) * z[10]) / T(4) + -z[34] + z[40] / T(2) + -z[76] + z[92];
z[40] = z[40] * z[52];
z[30] = -z[30] + z[41] + z[50];
z[30] = z[2] * z[30];
z[41] = z[12] * z[47];
z[30] = z[30] + -z[37] + z[41];
z[41] = -z[18] + -z[30] + -z[53];
z[41] = T(2) * z[41] + (T(-3) * z[52]) / T(4);
z[41] = z[41] * z[43];
z[31] = z[13] + z[31] + -z[58] + z[86];
z[42] = z[31] * z[43];
z[30] = z[30] + z[42];
z[31] = -(z[4] * z[31]);
z[30] = T(2) * z[30] + z[31];
z[30] = z[4] * z[30];
z[31] = -z[8] + z[38];
z[31] = -z[10] + T(2) * z[31] + z[56] + -z[85];
z[31] = z[12] * z[31];
z[31] = z[31] + -z[37];
z[31] = z[11] * z[31];
z[29] = T(-11) * z[8] + -z[29] + -z[39] + z[64];
z[29] = z[5] * z[29];
z[37] = z[9] + -z[79];
z[37] = T(2) * z[37] + z[49] + z[65];
z[37] = z[37] * z[82];
z[39] = T(3) * z[18];
z[42] = z[39] + z[53];
z[34] = z[34] * z[42];
z[46] = z[47] * z[72];
z[28] = T(13) * z[19] + (T(11) * z[20]) / T(2) + (T(-523) * z[21]) / T(12) + z[28] + z[29] + z[30] + z[31] + z[34] + z[37] + z[40] + z[41] + z[46] + z[60] + (T(-22) * z[68]) / T(3);
z[28] = z[23] * z[28];
z[29] = -z[58] + z[63] + T(2) * z[67];
z[30] = z[2] + -z[12];
z[29] = z[29] * z[30];
z[29] = z[29] + z[39] + -z[53];
z[29] = z[29] * z[43];
z[31] = (T(5) * z[10]) / T(2) + -z[67];
z[34] = z[31] + -z[61];
z[37] = -(z[34] * z[72]);
z[34] = z[2] * z[34];
z[39] = -z[10] + z[67];
z[40] = z[11] * z[39];
z[40] = T(8) * z[40];
z[34] = z[34] + -z[40];
z[34] = z[2] * z[34];
z[41] = z[52] / T(4);
z[46] = T(-3) * z[16] + (T(-19) * z[17]) / T(3);
z[46] = z[41] * z[46];
z[30] = z[4] * z[30] * z[39];
z[39] = T(8) * z[18] + (T(17) * z[53]) / T(6);
z[39] = z[16] * z[39];
z[40] = z[12] * z[40];
z[29] = (T(35) * z[19]) / T(2) + (T(19) * z[20]) / T(4) + (T(-623) * z[21]) / T(24) + z[29] + T(8) * z[30] + z[34] + z[37] + z[39] + z[40] + z[46] + (T(-19) * z[68]) / T(3);
z[29] = z[25] * z[29];
z[30] = z[11] + -z[12];
z[30] = z[30] * z[65];
z[30] = T(8) * z[30];
z[34] = -z[8] + z[9];
z[34] = -z[13] + T(3) * z[34];
z[37] = T(2) * z[34] + z[87];
z[39] = z[37] * z[43];
z[34] = z[34] + z[89];
z[40] = -(z[4] * z[34]);
z[39] = -z[30] + z[39] + z[40];
z[39] = z[4] * z[39];
z[34] = z[2] * z[34];
z[30] = z[30] + z[34];
z[30] = z[2] * z[30];
z[34] = T(13) * z[16];
z[40] = -z[17] + -z[34];
z[40] = z[40] * z[41];
z[37] = -(z[2] * z[37]);
z[37] = z[37] + T(-3) * z[54];
z[37] = z[37] * z[43];
z[46] = prod_pow(z[16], 3);
z[46] = z[19] + z[46];
z[47] = -z[1] / T(2) + (T(-3) * z[10]) / T(2) + z[73];
z[47] = z[5] * z[47];
z[30] = (T(3) * z[20]) / T(4) + (T(-21) * z[21]) / T(8) + z[30] + z[37] + z[39] + z[40] + (T(3) * z[46]) / T(2) + T(3) * z[47] + -z[68];
z[30] = z[22] * z[30];
z[37] = -z[8] + z[13];
z[37] = z[37] * z[55];
z[39] = -z[62] + -z[81];
z[39] = z[11] * z[39];
z[36] = z[36] + z[83];
z[36] = z[2] * z[36];
z[36] = z[36] + z[37] + z[39];
z[36] = z[2] * z[36];
z[39] = (T(-3) * z[7]) / T(2) + -z[67] + z[78] + z[90];
z[39] = z[11] * z[39];
z[37] = -z[37] + z[39];
z[37] = z[11] * z[37];
z[35] = z[10] + z[35] + z[81];
z[39] = z[2] + -z[11];
z[35] = z[35] * z[39];
z[35] = z[35] + -z[42];
z[35] = z[35] * z[43];
z[40] = z[53] / T(2) + z[70];
z[40] = z[16] * z[40];
z[38] = z[38] + z[45];
z[38] = -(z[4] * z[38] * z[39]);
z[35] = (T(5) * z[19]) / T(2) + z[35] + z[36] + z[37] + z[38] + z[40] + -z[68];
z[36] = T(6) * z[8] + z[71] + -z[85] + -z[90] + -z[91];
z[36] = z[36] * z[82];
z[34] = z[34] + -z[74];
z[34] = z[34] * z[41];
z[34] = (T(9) * z[20]) / T(4) + (T(-119) * z[21]) / T(8) + z[34] + T(3) * z[35] + z[36];
z[34] = z[27] * z[34];
z[31] = -z[31] + z[78] + -z[84];
z[31] = z[25] * z[31];
z[35] = -z[8] + -z[10] + z[49];
z[35] = z[24] * z[35];
z[31] = T(3) * z[31] + T(2) * z[35];
z[31] = z[14] * z[31];
return z[28] + z[29] + z[30] + z[31] + z[32] + z[33] + z[34] + z[44];
}



template IntegrandConstructorType<double> f_4_108_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_108_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_108_construct (const Kin<qd_real>&);
#endif

}