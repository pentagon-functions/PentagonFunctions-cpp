#include "f_4_197.h"

namespace PentagonFunctions {

template <typename T> T f_4_197_abbreviated (const std::array<T,56>&);

template <typename T> class SpDLog_f_4_197_W_10 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_197_W_10 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(3) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[7] * T(-3) + abb[8] * T(-3) + abb[4] * T(3) + abb[5] * T(3) + abb[6] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(-3) + abb[6] * T(-3) + abb[7] * T(3) + abb[8] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_197_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_197_W_21 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[9] * (abb[4] * prod_pow(abb[11], 2) * T(-3) + prod_pow(abb[11], 2) * (abb[13] * T(-3) + abb[6] * T(3) + abb[12] * T(3)) + prod_pow(abb[10], 2) * (abb[6] * T(-3) + abb[12] * T(-3) + abb[4] * T(3) + abb[13] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_197_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_197_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(2) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[0] * (T(-2) * kin.v[0] + T(8) * kin.v[3] + T(-4) * kin.v[4]) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-6) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[2] * (T(16) * kin.v[2] + T(24) * kin.v[3] + T(-28) * kin.v[4]) + kin.v[3] * (T(8) * kin.v[3] + T(-20) * kin.v[4]) + T(12) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(6) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(8) * kin.v[2] + T(12) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[3] * (T(4) * kin.v[3] + T(-10) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4])) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(6) + T(12)) * kin.v[0] + T(-28) * kin.v[2] + T(-20) * kin.v[3] + T(24) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-14) * kin.v[2] + T(-10) * kin.v[3] + T(12) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[2] * (T(-6) + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * (T(-6) + T(-4) * kin.v[4]) + kin.v[0] * (T(6) + (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-6) * kin.v[1] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + T(2) * kin.v[4]) + kin.v[4] * (T(6) + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[2] * (T(-6) + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * (T(-6) + T(-4) * kin.v[4]) + kin.v[0] * (T(6) + (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-6) * kin.v[1] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + T(2) * kin.v[4]) + kin.v[4] * (T(6) + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[1]) * (kin.v[2] * (-kin.v[2] + T(2) * kin.v[3]) + prod_pow(kin.v[4], 2) + kin.v[3] * (T(3) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (kin.v[0] + T(-4) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * (T(-6) + T(4) * kin.v[2] + T(-2) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(6) + (T(-4) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-6) * kin.v[1] + T(10) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[1] + T(-2) * kin.v[3]) + T(-2) * kin.v[4]) + kin.v[4] * (T(6) + T(2) * kin.v[4]) + kin.v[3] * (T(-6) + T(-6) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[4] * (T(-6) + T(-5) * kin.v[4]) + kin.v[0] * (T(-6) + (bc<T>[0] * int_to_imaginary<T>(2) + T(1)) * kin.v[0] + T(6) * kin.v[1] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[1] + T(-4) * kin.v[3]) + T(-4) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(6) + T(-4) * kin.v[3] + T(6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4])));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(8) * kin.v[2] + T(8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(-8) * kin.v[4];
c[3] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[22] * (abb[3] * (abb[2] * (abb[2] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) + abb[1] * abb[2] * T(2) + abb[2] * abb[20] * T(2) + abb[25] * T(4)) + abb[1] * abb[2] * (abb[13] * T(-4) + abb[8] * T(-2) + abb[7] * T(2) + abb[23] * T(2) + abb[26] * T(4)) + abb[2] * abb[20] * (abb[13] * T(-4) + abb[8] * T(-2) + abb[7] * T(2) + abb[23] * T(2) + abb[26] * T(4)) + abb[24] * (abb[2] * abb[3] * T(-2) + abb[2] * (abb[26] * T(-4) + abb[7] * T(-2) + abb[23] * T(-2) + abb[8] * T(2) + abb[13] * T(4)) + abb[21] * (abb[13] * T(-4) + abb[8] * T(-2) + abb[3] * T(2) + abb[7] * T(2) + abb[23] * T(2) + abb[26] * T(4))) + abb[2] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[23] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2) + abb[26] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[13] * bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * (abb[7] + abb[13] + abb[23] + abb[26] * T(-4) + abb[5] * T(-3) + abb[6] * T(-3) + abb[8] * T(5))) + abb[25] * (abb[13] * T(-8) + abb[8] * T(-4) + abb[7] * T(4) + abb[23] * T(4) + abb[26] * T(8)) + abb[21] * (abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[7] * bc<T>[0] * int_to_imaginary<T>(2) + abb[23] * bc<T>[0] * int_to_imaginary<T>(2) + abb[3] * (abb[1] * T(-2) + abb[2] * T(-2) + abb[20] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2)) + abb[13] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[26] * bc<T>[0] * int_to_imaginary<T>(4) + abb[1] * (abb[26] * T(-4) + abb[7] * T(-2) + abb[23] * T(-2) + abb[8] * T(2) + abb[13] * T(4)) + abb[2] * (abb[26] * T(-4) + abb[7] * T(-2) + abb[23] * T(-2) + abb[8] * T(2) + abb[13] * T(4)) + abb[20] * (abb[26] * T(-4) + abb[7] * T(-2) + abb[23] * T(-2) + abb[8] * T(2) + abb[13] * T(4)) + abb[21] * (abb[7] + abb[23] + abb[8] * T(-7) + abb[13] * T(-5) + abb[5] * T(3) + abb[6] * T(3) + abb[3] * T(4) + abb[26] * T(8))));
    }
};
template <typename T> class SpDLog_f_4_197_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_197_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(-2) * kin.v[2] + T(-8) * kin.v[3]) + kin.v[1] * (T(4) * kin.v[0] + T(2) * kin.v[1] + T(-12) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(10) * kin.v[3] + T(12) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[1] * (T(4) + T(-4) * kin.v[0] + T(-2) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[3] * (T(-4) * kin.v[3] + T(-16) * kin.v[4]) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) + T(-12) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(-16) * kin.v[2] + T(20) * kin.v[3] + T(28) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4])) + kin.v[1] * ((T(-12) + bc<T>[0] * int_to_imaginary<T>(-6)) * kin.v[1] + T(-28) * kin.v[2] + T(16) * kin.v[3] + T(24) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-14) * kin.v[2] + T(8) * kin.v[3] + T(12) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-6) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-2) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (T(-8) * kin.v[2] + T(10) * kin.v[3] + T(14) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[3] * (T(6) + T(-2) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-4) * kin.v[4]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-6) + T(4) * kin.v[0] + (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[3]) + T(2) * kin.v[4]) + kin.v[2] * (T(-6) + T(-2) * kin.v[2] + T(4) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[3] * (T(6) + T(-2) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-4) * kin.v[4]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-6) + T(4) * kin.v[0] + (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[3]) + T(2) * kin.v[4]) + kin.v[2] * (T(-6) + T(-2) * kin.v[2] + T(4) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + rlog(-kin.v[1]) * (-prod_pow(kin.v[4], 2) + kin.v[2] * (kin.v[2] + T(4) * kin.v[3]) + kin.v[3] * (T(-5) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) * kin.v[0] + T(6) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + rlog(kin.v[2]) * (kin.v[0] * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(6) + T(-2) * kin.v[4]) + kin.v[1] * (T(-6) + T(8) * kin.v[0] + (bc<T>[0] * int_to_imaginary<T>(-1) + T(4)) * kin.v[1] + T(-12) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + T(-2) * kin.v[4]) + kin.v[2] * (T(-6) + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(6) * kin.v[4]) + kin.v[3] * (T(6) + T(8) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[2] * (T(6) + kin.v[2] + T(-8) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(-2) * kin.v[0] + (T(-1) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[0] + T(4) * kin.v[3]) + T(-4) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[4] * (T(-6) + T(5) * kin.v[4]) + kin.v[3] * (T(-6) + T(7) * kin.v[3] + T(12) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[1] + T(8) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(-8) * kin.v[4];
c[3] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[1] + T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4])) + rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[1]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4])) + rlog(kin.v[3]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[27] * (abb[3] * (abb[28] * T(-4) + abb[10] * abb[11] * T(-2) + abb[11] * abb[20] * T(-2) + abb[11] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[11] * T(2))) + abb[10] * abb[11] * (abb[29] * T(-4) + abb[13] * T(-2) + abb[23] * T(-2) + abb[4] * T(2) + abb[7] * T(4)) + abb[11] * abb[20] * (abb[29] * T(-4) + abb[13] * T(-2) + abb[23] * T(-2) + abb[4] * T(2) + abb[7] * T(4)) + abb[24] * (abb[3] * abb[11] * T(2) + abb[21] * (abb[29] * T(-4) + abb[3] * T(-2) + abb[13] * T(-2) + abb[23] * T(-2) + abb[4] * T(2) + abb[7] * T(4)) + abb[11] * (abb[7] * T(-4) + abb[4] * T(-2) + abb[13] * T(2) + abb[23] * T(2) + abb[29] * T(4))) + abb[11] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[13] * bc<T>[0] * int_to_imaginary<T>(2) + abb[23] * bc<T>[0] * int_to_imaginary<T>(2) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[29] * bc<T>[0] * int_to_imaginary<T>(4) + abb[11] * (-abb[7] + -abb[13] + -abb[23] + abb[4] * T(-5) + abb[6] * T(3) + abb[12] * T(3) + abb[29] * T(4))) + abb[21] * (abb[13] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[23] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(2) + abb[29] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[7] * bc<T>[0] * int_to_imaginary<T>(4) + abb[3] * (bc<T>[0] * int_to_imaginary<T>(-2) + abb[10] * T(2) + abb[11] * T(2) + abb[20] * T(2)) + abb[10] * (abb[7] * T(-4) + abb[4] * T(-2) + abb[13] * T(2) + abb[23] * T(2) + abb[29] * T(4)) + abb[11] * (abb[7] * T(-4) + abb[4] * T(-2) + abb[13] * T(2) + abb[23] * T(2) + abb[29] * T(4)) + abb[20] * (abb[7] * T(-4) + abb[4] * T(-2) + abb[13] * T(2) + abb[23] * T(2) + abb[29] * T(4)) + abb[21] * (-abb[13] + -abb[23] + abb[29] * T(-8) + abb[3] * T(-4) + abb[6] * T(-3) + abb[12] * T(-3) + abb[7] * T(5) + abb[4] * T(7))) + abb[28] * (abb[29] * T(-8) + abb[13] * T(-4) + abb[23] * T(-4) + abb[4] * T(4) + abb[7] * T(8)));
    }
};
template <typename T> class SpDLog_f_4_197_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_197_W_7 (const Kin<T>& kin) {
        c[0] = T(3) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(4) * prod_pow(kin.v[4], 2) + kin.v[3] * ((bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[1]) * (((T(-27) * kin.v[4]) / T(4) + T(7)) * kin.v[4] + T(7) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-27) * kin.v[4]) / T(2) + T(7) + T(7) * kin.v[1] + (T(-27) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[1] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(4) + T(-2) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (((T(-27) * kin.v[4]) / T(4) + T(7)) * kin.v[4] + T(7) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-27) * kin.v[4]) / T(2) + T(7) + T(7) * kin.v[1] + (T(-27) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[1] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(4) + T(-2) * kin.v[4]))) + rlog(kin.v[3]) * (((T(-27) * kin.v[4]) / T(4) + T(7)) * kin.v[4] + T(7) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-27) * kin.v[4]) / T(2) + T(7) + T(7) * kin.v[1] + (T(-27) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[1] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(4) + T(-2) * kin.v[4]))) + rlog(-kin.v[4]) * (((T(27) * kin.v[4]) / T(4) + T(-7)) * kin.v[4] + T(-7) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4])) + kin.v[3] * ((T(27) * kin.v[4]) / T(2) + T(-7) + T(-7) * kin.v[1] + (T(27) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[1] + T(4) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(27) * kin.v[4]) / T(4) + T(-7)) * kin.v[4] + T(-7) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4])) + kin.v[3] * ((T(27) * kin.v[4]) / T(2) + T(-7) + T(-7) * kin.v[1] + (T(27) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[1] + T(4) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(27) * kin.v[4]) / T(4) + T(-7)) * kin.v[4] + T(-7) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4])) + kin.v[3] * ((T(27) * kin.v[4]) / T(2) + T(-7) + T(-7) * kin.v[1] + (T(27) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[1] + T(4) * kin.v[4])));
c[1] = rlog(-kin.v[4]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4]) + rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + T(3) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + T(3) * kin.v[4]) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[37] * (abb[10] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[13] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[8] * bc<T>[0] * int_to_imaginary<T>(4) + abb[23] * bc<T>[0] * int_to_imaginary<T>(4) + abb[10] * (abb[8] * T(-2) + abb[23] * T(-2) + abb[5] * T(2) + abb[12] * T(2) + abb[13] * T(2))) + abb[38] * (abb[8] * T(-3) + abb[23] * T(-3) + abb[5] * T(3) + abb[12] * T(3) + abb[13] * T(3)) + abb[3] * (abb[10] * abb[20] * T(-4) + abb[38] * T(-3) + abb[10] * (abb[10] * T(-2) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[1] * abb[10] * T(4)) + abb[10] * abb[20] * (abb[8] * T(-4) + abb[23] * T(-4) + abb[5] * T(4) + abb[12] * T(4) + abb[13] * T(4)) + abb[1] * abb[10] * (abb[5] * T(-4) + abb[12] * T(-4) + abb[13] * T(-4) + abb[8] * T(4) + abb[23] * T(4)) + abb[24] * (abb[8] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[23] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[5] * bc<T>[0] * int_to_imaginary<T>(4) + abb[12] * bc<T>[0] * int_to_imaginary<T>(4) + abb[13] * bc<T>[0] * int_to_imaginary<T>(4) + abb[24] * (abb[5] * T(-2) + abb[12] * T(-2) + abb[13] * T(-2) + abb[3] * T(2) + abb[8] * T(2) + abb[23] * T(2)) + abb[1] * (abb[8] * T(-4) + abb[23] * T(-4) + abb[5] * T(4) + abb[12] * T(4) + abb[13] * T(4)) + abb[3] * (abb[1] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[20] * T(4)) + abb[20] * (abb[5] * T(-4) + abb[12] * T(-4) + abb[13] * T(-4) + abb[8] * T(4) + abb[23] * T(4))));
    }
};
template <typename T> class SpDLog_f_4_197_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_197_W_12 (const Kin<T>& kin) {
        c[0] = (T(-9) / T(2) + (T(-27) * kin.v[4]) / T(8)) * kin.v[4] + (T(9) / T(2) + (T(9) * kin.v[1]) / T(8) + (T(9) * kin.v[4]) / T(4)) * kin.v[1];
c[1] = bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[4] * (T(-9) / T(2) + T(-3) * kin.v[4]) + kin.v[1] * (T(9) / T(2) + (T(3) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(8) + (T(3) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1]) + rlog(-kin.v[4]) * (((T(9) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + kin.v[1] * (T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(4) + T(2) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(-21) * kin.v[4]) / T(2) + T(-16)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[1] * (bc<T>[0] * int_to_imaginary<T>(4) + T(16) + (T(11) / T(2) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[1] + T(5) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[1] * ((T(13) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(4) + T(7) + (T(1) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[1]) + ((T(-27) * kin.v[4]) / T(4) + T(-7)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-4) + T(-2) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * ((T(-13) * kin.v[4]) / T(2) + T(-7) + bc<T>[0] * int_to_imaginary<T>(-4) + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1]) + ((T(27) * kin.v[4]) / T(4) + T(7)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(4) + T(2) * kin.v[4])) + rlog(kin.v[2]) * (((T(21) * kin.v[4]) / T(2) + T(16)) * kin.v[4] + kin.v[1] * (T(-16) + bc<T>[0] * int_to_imaginary<T>(-4) + (T(-11) / T(2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(4) + T(2) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (T(63) / T(2) + (T(75) * kin.v[4]) / T(4) + bc<T>[0] * int_to_imaginary<T>(12) + (T(51) / T(8) + bc<T>[0] * int_to_imaginary<T>(6)) * kin.v[1]) + (T(-63) / T(2) + (T(-201) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-12) + T(-6) * kin.v[4]));
c[2] = (T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(6)) * kin.v[1] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4]) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[1] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + T(3) * kin.v[4]) + rlog(kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[4]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[1]) * ((T(-21) * kin.v[4]) / T(2) + (T(21) / T(2) + bc<T>[0] * int_to_imaginary<T>(12)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(-12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[40] * (abb[1] * abb[20] * (abb[7] * T(-4) + abb[23] * T(-4) + abb[4] * T(4) + abb[5] * T(4) + abb[12] * T(4)) + abb[1] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[7] * bc<T>[0] * int_to_imaginary<T>(4) + abb[23] * bc<T>[0] * int_to_imaginary<T>(4) + abb[10] * (abb[4] * T(-4) + abb[5] * T(-4) + abb[12] * T(-4) + abb[7] * T(4) + abb[23] * T(4)) + abb[1] * ((abb[42] * T(9)) / T(2) + -abb[5] + abb[7] * T(-5) + abb[23] * T(-2) + abb[12] * T(2) + abb[4] * T(5))) + abb[41] * ((abb[42] * T(-9)) / T(2) + abb[7] * T(-6) + abb[23] * T(-3) + abb[12] * T(3) + abb[4] * T(6) + abb[5] * T(6)) + abb[3] * ((abb[41] * T(-21)) / T(2) + abb[1] * abb[20] * T(-12) + abb[1] * ((abb[1] * T(-21)) / T(2) + bc<T>[0] * int_to_imaginary<T>(12) + abb[10] * T(12))) + abb[24] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[23] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[4] * bc<T>[0] * int_to_imaginary<T>(4) + abb[5] * bc<T>[0] * int_to_imaginary<T>(4) + abb[12] * bc<T>[0] * int_to_imaginary<T>(4) + abb[10] * (abb[7] * T(-4) + abb[23] * T(-4) + abb[4] * T(4) + abb[5] * T(4) + abb[12] * T(4)) + abb[20] * (abb[4] * T(-4) + abb[5] * T(-4) + abb[12] * T(-4) + abb[7] * T(4) + abb[23] * T(4)) + abb[24] * (abb[5] + (abb[42] * T(-9)) / T(2) + (abb[3] * T(21)) / T(2) + abb[4] * T(-5) + abb[12] * T(-2) + abb[23] * T(2) + abb[7] * T(5)) + abb[3] * (abb[10] * T(-12) + bc<T>[0] * int_to_imaginary<T>(-12) + abb[20] * T(12))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_197_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl21 = DLog_W_21<T>(kin),dl31 = DLog_W_31<T>(kin),dl9 = DLog_W_9<T>(kin),dl23 = DLog_W_23<T>(kin),dl22 = DLog_W_22<T>(kin),dl30 = DLog_W_30<T>(kin),dl7 = DLog_W_7<T>(kin),dl1 = DLog_W_1<T>(kin),dl12 = DLog_W_12<T>(kin),dl28 = DLog_W_28<T>(kin),dl16 = DLog_W_16<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),spdl10 = SpDLog_f_4_197_W_10<T>(kin),spdl21 = SpDLog_f_4_197_W_21<T>(kin),spdl23 = SpDLog_f_4_197_W_23<T>(kin),spdl22 = SpDLog_f_4_197_W_22<T>(kin),spdl7 = SpDLog_f_4_197_W_7<T>(kin),spdl12 = SpDLog_f_4_197_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,56> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl31(t), f_2_2_7(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl9(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), dl23(t), rlog(kin.W[3] / kin_path.W[3]), rlog(-v_path[1]), f_2_1_8(kin_path), -rlog(t), dl22(t), f_2_1_14(kin_path), -rlog(t), dl30(t) / kin_path.SqrtDelta, f_2_1_3(kin_path), f_2_1_6(kin_path), f_2_1_7(kin_path), rlog(v_path[0] + v_path[1]), rlog(-v_path[1] + v_path[3]), f_2_1_15(kin_path), dl7(t), f_2_1_11(kin_path), dl1(t), dl12(t), f_2_1_4(kin_path), -rlog(t), dl28(t) / kin_path.SqrtDelta, dl16(t), dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl5(t), dl2(t), dl17(t), dl18(t), dl19(t), dl3(t), dl20(t), dl4(t)}
;

        auto result = f_4_197_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_197_abbreviated(const std::array<T,56>& abb)
{
using TR = typename T::value_type;
T z[231];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[44];
z[3] = abb[48];
z[4] = abb[49];
z[5] = abb[50];
z[6] = abb[51];
z[7] = abb[52];
z[8] = abb[53];
z[9] = abb[55];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[7];
z[14] = abb[8];
z[15] = abb[12];
z[16] = abb[13];
z[17] = abb[16];
z[18] = abb[30];
z[19] = abb[43];
z[20] = abb[45];
z[21] = abb[46];
z[22] = abb[17];
z[23] = abb[18];
z[24] = abb[23];
z[25] = abb[26];
z[26] = abb[42];
z[27] = abb[2];
z[28] = abb[54];
z[29] = abb[10];
z[30] = abb[20];
z[31] = abb[21];
z[32] = abb[24];
z[33] = bc<TR>[0];
z[34] = abb[47];
z[35] = abb[11];
z[36] = abb[15];
z[37] = abb[25];
z[38] = abb[28];
z[39] = abb[32];
z[40] = abb[33];
z[41] = abb[34];
z[42] = abb[36];
z[43] = abb[38];
z[44] = abb[41];
z[45] = abb[39];
z[46] = abb[31];
z[47] = abb[35];
z[48] = abb[19];
z[49] = abb[29];
z[50] = abb[14];
z[51] = bc<TR>[1];
z[52] = bc<TR>[3];
z[53] = bc<TR>[5];
z[54] = bc<TR>[9];
z[55] = bc<TR>[2];
z[56] = bc<TR>[4];
z[57] = (T(3) * z[24]) / T(2);
z[58] = T(5) * z[11];
z[59] = z[57] + -z[58];
z[60] = z[10] + -z[14];
z[61] = z[13] + -z[16];
z[62] = (T(5) * z[26]) / T(3);
z[63] = (T(3) * z[1]) / T(2);
z[64] = -z[25] + z[49];
z[59] = (T(-2) * z[15]) / T(3) + z[59] / T(2) + z[60] + (T(-3) * z[61]) / T(4) + z[62] + z[63] + -z[64];
z[59] = z[9] * z[59];
z[65] = z[16] / T(2);
z[66] = -z[49] + z[65];
z[67] = -z[12] + -z[15] + z[66];
z[68] = T(2) * z[51];
z[69] = (T(3) * z[13]) / T(2);
z[70] = z[14] / T(2);
z[71] = z[10] / T(2);
z[72] = -z[1] + -z[67] + -z[68] + -z[69] + -z[70] + z[71];
z[72] = z[8] * z[72];
z[73] = z[1] / T(2);
z[74] = z[11] + z[73];
z[75] = z[13] / T(2);
z[76] = -z[25] + z[75];
z[77] = -z[12] + z[76];
z[78] = z[14] + z[65] + z[68] + z[71] + -z[74] + z[77];
z[78] = z[6] * z[78];
z[79] = z[13] + -z[14];
z[80] = (T(7) * z[16]) / T(12);
z[81] = (T(5) * z[12]) / T(4);
z[82] = T(2) * z[24];
z[83] = (T(19) * z[11]) / T(4) + -z[15] + z[82];
z[62] = (T(-7) * z[1]) / T(12) + (T(5) * z[10]) / T(3) + -z[49] + -z[62] + (T(2) * z[79]) / T(3) + z[80] + -z[81] + z[83] / T(3);
z[62] = z[7] * z[62];
z[83] = -z[11] + z[15];
z[84] = (T(2) * z[25]) / T(3);
z[85] = (T(2) * z[49]) / T(3);
z[86] = (T(7) * z[13]) / T(12);
z[80] = z[80] + (T(-5) * z[83]) / T(4) + -z[84] + z[85] + -z[86];
z[80] = z[2] * z[80];
z[87] = z[10] + -z[16];
z[88] = T(7) * z[11];
z[89] = (T(19) * z[15]) / T(2) + T(-13) * z[24] + -z[88];
z[81] = (T(-8) * z[1]) / T(3) + (T(-5) * z[14]) / T(3) + z[25] + (T(13) * z[26]) / T(4) + z[81] + -z[86] + (T(2) * z[87]) / T(3) + z[89] / T(6);
z[81] = z[3] * z[81];
z[86] = T(6) * z[55];
z[89] = -z[12] / T(4) + -z[68] + z[86];
z[90] = (T(5) * z[24]) / T(3);
z[91] = -z[11] + -z[13] + -z[90];
z[84] = (T(13) * z[1]) / T(4) + (T(-13) * z[10]) / T(6) + (T(-5) * z[14]) / T(6) + (T(11) * z[16]) / T(6) + z[49] + z[84] + z[89] + z[91] / T(4);
z[84] = z[5] * z[84];
z[90] = z[15] + -z[16] + z[90];
z[85] = (T(-9) * z[1]) / T(4) + (T(5) * z[10]) / T(6) + (T(-11) * z[13]) / T(6) + (T(7) * z[14]) / T(6) + -z[25] + -z[85] + -z[89] + z[90] / T(4);
z[85] = z[28] * z[85];
z[89] = z[6] + -z[8];
z[90] = -z[5] + z[28];
z[91] = z[20] + z[21];
z[92] = z[34] + z[91];
z[93] = (T(7) * z[3]) / T(12) + -z[4] / T(6) + (T(-5) * z[7]) / T(12) + -z[9] / T(12) + z[89] / T(2) + (T(5) * z[90]) / T(4) + (T(28) * z[92]) / T(135);
z[94] = int_to_imaginary<T>(1) * z[33];
z[93] = z[93] * z[94];
z[95] = z[17] + z[22];
z[96] = T(-5) * z[23] + T(7) * z[95];
z[96] = z[34] * z[96];
z[97] = T(-7) * z[23] + T(5) * z[95];
z[97] = z[21] * z[97];
z[96] = z[96] + z[97];
z[97] = z[60] + z[61];
z[98] = z[45] * z[97];
z[99] = T(3) * z[24];
z[83] = (T(19) * z[1]) / T(2) + (T(35) * z[13]) / T(6) + -z[14] + (T(-23) * z[16]) / T(6) + (T(-13) * z[26]) / T(2) + (T(-11) * z[83]) / T(6) + z[99];
z[83] = -z[10] + z[83] / T(2);
z[83] = z[4] * z[83];
z[100] = z[48] * z[61];
z[101] = T(6) * z[100];
z[102] = -z[23] + z[95];
z[103] = z[18] * z[102];
z[104] = T(2) * z[95];
z[105] = -z[23] + z[104];
z[106] = z[19] * z[105];
z[107] = z[20] * z[102];
z[59] = z[59] + z[62] + z[72] + z[78] + z[80] + z[81] + z[83] + z[84] + z[85] + z[93] + z[96] / T(6) + T(-2) * z[98] + z[101] + z[103] + z[106] / T(3) + -z[107] / T(2);
z[59] = z[33] * z[59];
z[62] = T(3) * z[13];
z[72] = T(3) * z[1];
z[78] = z[62] + z[72];
z[80] = T(2) * z[16];
z[81] = T(2) * z[25];
z[83] = z[80] + -z[81];
z[84] = z[14] + -z[24];
z[85] = T(2) * z[10];
z[93] = -z[78] + z[83] + z[84] + z[85];
z[93] = z[5] * z[93];
z[96] = T(2) * z[13];
z[107] = T(2) * z[49];
z[108] = z[96] + -z[107];
z[109] = z[10] + z[16];
z[110] = T(2) * z[14];
z[111] = z[1] + -z[24];
z[112] = z[108] + z[109] + -z[110] + z[111];
z[113] = z[7] * z[112];
z[113] = z[106] + z[113];
z[114] = z[13] + z[14];
z[115] = z[83] + -z[85] + z[111] + z[114];
z[116] = z[3] * z[115];
z[117] = T(3) * z[16];
z[118] = -z[110] + z[117];
z[119] = -z[10] + z[72];
z[120] = z[24] + -z[108] + z[118] + z[119];
z[120] = z[28] * z[120];
z[121] = T(3) * z[10];
z[122] = T(3) * z[14];
z[123] = z[121] + -z[122];
z[124] = T(2) * z[64];
z[125] = z[61] + z[123] + -z[124];
z[126] = z[9] * z[125];
z[127] = T(2) * z[23];
z[128] = -z[95] + z[127];
z[129] = z[21] * z[128];
z[130] = z[34] * z[105];
z[131] = -z[129] + z[130];
z[132] = T(3) * z[98] + -z[131];
z[133] = z[61] + z[64];
z[134] = T(2) * z[4];
z[135] = z[133] * z[134];
z[133] = z[2] * z[133];
z[93] = z[93] + -z[113] + z[116] + z[120] + -z[126] + z[132] + T(-4) * z[133] + -z[135];
z[120] = z[31] * z[93];
z[126] = T(6) * z[1];
z[135] = z[11] + -z[24];
z[136] = T(2) * z[15];
z[137] = T(2) * z[26];
z[138] = -z[10] + z[96] + -z[118] + z[126] + T(-3) * z[135] + -z[136] + -z[137];
z[138] = z[4] * z[138];
z[139] = z[2] * z[112];
z[140] = z[18] * z[105];
z[139] = -z[139] + z[140];
z[140] = z[130] + z[139];
z[141] = z[28] * z[112];
z[142] = z[87] + -z[96] + z[110];
z[143] = z[137] + z[142];
z[144] = z[11] + z[24];
z[145] = z[136] + -z[144];
z[146] = T(4) * z[1];
z[147] = z[143] + -z[145] + -z[146];
z[147] = z[3] * z[147];
z[148] = z[1] + -z[11];
z[149] = T(2) * z[9];
z[150] = z[148] * z[149];
z[151] = z[7] * z[148];
z[138] = z[138] + -z[140] + -z[141] + z[147] + z[150] + T(2) * z[151];
z[138] = z[29] * z[138];
z[120] = -z[120] + z[138];
z[138] = z[5] * z[115];
z[123] = z[123] + z[137];
z[147] = -z[62] + z[117];
z[150] = T(8) * z[1];
z[152] = -z[15] + T(-2) * z[135];
z[152] = -z[123] + -z[147] + z[150] + T(2) * z[152];
z[152] = z[9] * z[152];
z[153] = T(8) * z[24];
z[154] = T(5) * z[15];
z[155] = T(16) * z[1] + z[153] + -z[154];
z[156] = z[80] + -z[96];
z[157] = T(8) * z[26];
z[158] = T(3) * z[11];
z[159] = z[155] + -z[156] + -z[157] + -z[158];
z[159] = z[4] * z[159];
z[160] = T(2) * z[1];
z[161] = z[79] + z[160];
z[162] = z[80] + -z[85];
z[163] = T(2) * z[11];
z[164] = -z[24] + z[163];
z[165] = -z[15] + z[161] + z[162] + z[164];
z[165] = z[3] * z[165];
z[166] = z[135] + z[136];
z[143] = z[143] + -z[160] + -z[166];
z[143] = z[7] * z[143];
z[133] = T(2) * z[133];
z[106] = -z[106] + z[132] + -z[133] + z[138] + -z[141] + z[143] + z[152] + z[159] + z[165];
z[106] = z[32] * z[106];
z[132] = z[11] + z[82];
z[143] = T(4) * z[26];
z[152] = z[15] + z[143];
z[159] = z[132] + z[146] + -z[152];
z[165] = z[4] * z[159];
z[152] = -z[72] + z[152];
z[167] = T(2) * z[144] + -z[152];
z[168] = z[3] * z[167];
z[151] = z[151] + z[168];
z[169] = z[24] + z[81];
z[170] = z[13] + z[169];
z[171] = -z[117] + z[170];
z[172] = -(z[5] * z[171]);
z[173] = z[24] + z[107];
z[174] = z[16] + z[173];
z[175] = -z[62] + z[174];
z[176] = z[28] * z[175];
z[177] = -z[160] + z[164];
z[178] = -z[61] + -z[177];
z[178] = z[9] * z[178];
z[133] = z[101] + -z[133] + -z[151] + z[165] + z[172] + z[176] + z[178];
z[133] = z[30] * z[133];
z[165] = T(5) * z[24];
z[172] = T(6) * z[11] + -z[165];
z[176] = T(3) * z[15];
z[178] = -z[14] + z[176];
z[179] = T(10) * z[1] + z[62] + -z[80] + -z[85] + -z[172] + -z[178];
z[179] = z[4] * z[179];
z[180] = z[15] + z[79];
z[162] = z[157] + z[162] + -z[165] + z[180];
z[181] = -z[146] + z[162] + -z[163];
z[181] = z[7] * z[181];
z[182] = z[2] * z[115];
z[183] = z[18] * z[128];
z[182] = -z[182] + z[183];
z[183] = z[129] + z[182];
z[184] = z[19] * z[128];
z[184] = -z[138] + z[184];
z[185] = z[149] * z[167];
z[168] = T(2) * z[168] + z[179] + z[181] + z[183] + -z[184] + z[185];
z[179] = -(z[0] * z[168]);
z[116] = -z[116] + z[182];
z[181] = T(5) * z[13];
z[182] = -z[85] + z[181];
z[185] = T(8) * z[16];
z[186] = T(3) * z[169];
z[187] = -z[14] + z[72] + z[182] + -z[185] + z[186];
z[187] = z[28] * z[187];
z[115] = z[4] * z[115];
z[188] = T(2) * z[5];
z[189] = z[149] + z[188];
z[189] = z[171] * z[189];
z[190] = z[34] * z[128];
z[115] = z[115] + z[116] + z[187] + -z[189] + z[190];
z[187] = z[27] * z[115];
z[189] = z[24] + z[25] + z[49];
z[189] = -z[109] + -z[114] + z[160] + T(2) * z[189];
z[90] = -(z[90] * z[189]);
z[190] = z[21] + z[34];
z[190] = z[102] * z[190];
z[190] = -z[98] + z[103] + z[190];
z[147] = -z[60] + z[124] + z[147];
z[191] = -(z[9] * z[147]);
z[192] = z[60] + -z[61];
z[124] = z[124] + -z[192];
z[193] = z[2] * z[124];
z[194] = z[4] * z[97];
z[90] = z[90] + z[190] + z[191] + z[193] + z[194];
z[90] = z[41] * z[90];
z[191] = -z[87] + z[137];
z[132] = -z[132] + z[180] + z[191];
z[193] = -z[3] + z[7];
z[132] = z[132] * z[193];
z[98] = z[98] + z[103];
z[193] = z[2] * z[97];
z[102] = z[19] * z[102];
z[102] = z[98] + -z[102] + -z[132] + -z[193];
z[132] = -z[137] + z[163] + z[192];
z[193] = z[9] * z[132];
z[194] = z[4] * z[132];
z[193] = -z[102] + z[193] + -z[194];
z[193] = T(3) * z[193];
z[193] = z[47] * z[193];
z[68] = z[55] * z[68];
z[194] = prod_pow(z[51], 2);
z[195] = T(2) * z[56] + z[194];
z[68] = z[68] + -z[195];
z[196] = T(3) * z[5];
z[197] = T(3) * z[28] + -z[196];
z[68] = z[68] * z[197];
z[197] = T(3) * z[6];
z[198] = T(3) * z[8];
z[199] = -z[197] + z[198];
z[195] = z[195] * z[199];
z[194] = -(z[92] * z[194]);
z[86] = z[51] * z[86] * z[89];
z[68] = z[68] + z[86] + T(3) * z[90] + z[106] + -z[120] + T(2) * z[133] + z[179] + z[187] + z[193] + (T(2) * z[194]) / T(5) + z[195];
z[68] = int_to_imaginary<T>(1) * z[68];
z[86] = z[52] * z[92];
z[59] = z[59] + z[68] + T(-2) * z[86];
z[59] = z[33] * z[59];
z[68] = -z[99] + -z[163];
z[86] = T(12) * z[26];
z[89] = T(5) * z[1];
z[68] = T(2) * z[68] + z[86] + -z[89] + z[117] + -z[121];
z[68] = z[7] * z[68];
z[90] = T(4) * z[14];
z[106] = T(4) * z[10];
z[86] = -z[86] + z[88] + -z[90] + z[106] + z[154];
z[86] = z[9] * z[86];
z[88] = (T(3) * z[10]) / T(2);
z[133] = (T(3) * z[14]) / T(2);
z[154] = z[88] + -z[133];
z[179] = z[64] + z[65] + -z[75] + -z[154];
z[179] = z[2] * z[179];
z[187] = (T(9) * z[13]) / T(2);
z[193] = T(6) * z[10];
z[194] = z[14] + z[99];
z[194] = (T(-13) * z[1]) / T(2) + -z[80] + -z[187] + z[193] + z[194] / T(2);
z[194] = z[8] * z[194];
z[195] = z[24] / T(2);
z[63] = z[63] + z[195];
z[199] = -z[25] + -z[63] + -z[69] + z[70] + z[109];
z[199] = z[5] * z[199];
z[200] = (T(3) * z[16]) / T(2);
z[63] = z[49] + z[63] + -z[71] + -z[114] + z[200];
z[63] = z[28] * z[63];
z[137] = -z[78] + z[122] + z[137] + T(-2) * z[145];
z[137] = z[3] * z[137];
z[201] = z[96] + z[160];
z[202] = z[16] + -z[24];
z[203] = -z[14] + z[202];
z[204] = -z[71] + z[201] + (T(3) * z[203]) / T(2);
z[204] = z[6] * z[204];
z[205] = T(7) * z[10];
z[206] = -z[11] + z[70];
z[156] = (T(19) * z[26]) / T(2) + -z[156] + -z[205] + T(5) * z[206];
z[156] = z[4] * z[156];
z[206] = z[23] / T(2);
z[207] = -z[95] + z[206];
z[208] = z[34] * z[207];
z[104] = -z[104] + -z[206];
z[104] = z[20] * z[104];
z[206] = z[95] / T(2);
z[209] = -z[23] + z[206];
z[210] = z[21] * z[209];
z[211] = z[19] * z[23];
z[63] = z[63] + z[68] + z[86] + (T(-3) * z[98]) / T(2) + z[104] + z[137] + z[156] + -z[179] + z[194] + z[199] + z[204] + z[208] + -z[210] + T(-3) * z[211];
z[63] = prod_pow(z[32], 2) * z[63];
z[68] = T(3) * z[12];
z[86] = (T(7) * z[1]) / T(2) + z[68];
z[98] = (T(9) * z[10]) / T(2);
z[104] = -z[15] + z[195];
z[137] = z[49] + -z[104];
z[65] = z[65] + z[86] + -z[98] + -z[110] + T(3) * z[137];
z[65] = z[8] * z[65];
z[137] = T(3) * z[25];
z[156] = -z[24] + z[49];
z[187] = (T(-7) * z[14]) / T(2) + (T(17) * z[16]) / T(2) + z[88] + -z[137] + z[156] + z[160] + -z[187];
z[187] = z[28] * z[187];
z[194] = -z[11] + z[133];
z[199] = -z[25] + z[194] + z[195];
z[75] = -z[75] + z[85] + -z[86] + T(3) * z[199];
z[75] = z[6] * z[75];
z[86] = T(3) * z[49];
z[199] = -z[24] + z[25];
z[204] = (T(7) * z[10]) / T(2) + (T(-17) * z[13]) / T(2) + (T(9) * z[16]) / T(2) + z[86] + -z[133] + -z[160] + -z[199];
z[204] = z[5] * z[204];
z[73] = z[73] + -z[195];
z[76] = z[70] + z[73] + z[76] + -z[87];
z[195] = z[3] * z[76];
z[66] = z[66] + z[71] + z[73] + z[79];
z[71] = -(z[7] * z[66]);
z[208] = (T(-7) * z[61]) / T(2);
z[211] = T(-5) * z[64] + z[154] + z[208];
z[211] = z[2] * z[211];
z[212] = (T(-11) * z[60]) / T(2) + z[64] + -z[208];
z[212] = z[9] * z[212];
z[213] = z[21] + -z[34];
z[214] = z[23] + z[95];
z[213] = z[213] * z[214];
z[213] = z[103] + z[213];
z[127] = z[127] + z[206];
z[127] = z[20] * z[127];
z[206] = z[19] * z[207];
z[64] = T(-4) * z[64] + z[97];
z[64] = z[4] * z[64];
z[64] = z[64] + z[65] + z[71] + z[75] + z[101] + z[127] + z[187] + z[195] + z[204] + z[206] + z[211] + z[212] + (T(3) * z[213]) / T(2);
z[64] = z[31] * z[64];
z[65] = z[32] * z[93];
z[64] = z[64] + z[65];
z[64] = z[31] * z[64];
z[65] = z[1] + z[24];
z[71] = z[65] + -z[87] + -z[108];
z[75] = z[28] * z[71];
z[93] = T(4) * z[13];
z[101] = -z[90] + z[93];
z[127] = T(10) * z[26];
z[187] = T(4) * z[15];
z[195] = -z[101] + z[127] + -z[187];
z[204] = -z[144] + -z[150] + z[195];
z[206] = z[87] + -z[204];
z[206] = z[3] * z[206];
z[211] = -z[15] + -z[97] + z[143];
z[212] = z[65] + z[163] + -z[211];
z[213] = T(2) * z[7];
z[212] = z[212] * z[213];
z[215] = -z[85] + z[110];
z[216] = z[143] + -z[176] + z[215];
z[217] = z[65] + -z[163] + z[216];
z[217] = z[149] * z[217];
z[166] = z[160] + -z[166] + z[191];
z[166] = z[4] * z[166];
z[191] = T(2) * z[214];
z[218] = z[20] * z[191];
z[219] = z[1] + z[13];
z[220] = -z[10] + z[219];
z[221] = T(2) * z[220];
z[222] = z[6] * z[221];
z[218] = z[218] + -z[222];
z[222] = -z[14] + z[16];
z[223] = z[1] + z[222];
z[224] = T(2) * z[223];
z[225] = z[8] * z[224];
z[226] = z[218] + z[225];
z[221] = z[5] * z[221];
z[227] = z[19] * z[191];
z[140] = -z[75] + z[140] + z[166] + z[206] + z[212] + z[217] + z[221] + z[226] + z[227];
z[140] = z[32] * z[140];
z[166] = z[68] + -z[82] + z[160];
z[178] = z[61] + -z[106] + z[107] + z[166] + z[178];
z[206] = z[2] * z[178];
z[212] = z[178] * z[213];
z[174] = z[72] + -z[121] + z[174];
z[213] = z[9] * z[174];
z[213] = z[213] + -z[221];
z[217] = -z[24] + z[107] + z[136];
z[217] = T(-9) * z[10] + T(3) * z[217];
z[221] = T(6) * z[12];
z[227] = z[89] + z[221];
z[228] = z[16] + z[110] + -z[217] + -z[227];
z[228] = z[8] * z[228];
z[174] = z[4] * z[174];
z[229] = -z[18] + z[19];
z[230] = z[191] * z[229];
z[75] = -z[75] + z[130] + z[174] + T(2) * z[206] + z[212] + -z[213] + z[218] + z[228] + z[230];
z[75] = z[31] * z[75];
z[57] = z[57] + -z[200];
z[74] = z[57] + -z[74] + -z[86] + z[98] + -z[176];
z[74] = z[7] * z[74];
z[98] = z[2] + -z[28];
z[66] = z[66] * z[98];
z[67] = -z[67] + z[73] + -z[88];
z[67] = z[67] * z[198];
z[98] = -z[144] + -z[160] + z[211];
z[130] = z[3] * z[98];
z[57] = (T(5) * z[10]) / T(2) + z[13] + z[57] + -z[70] + -z[152];
z[57] = z[4] * z[57];
z[70] = z[18] + z[34];
z[152] = z[70] * z[207];
z[60] = z[60] + z[202];
z[174] = z[6] * z[60];
z[207] = (T(3) * z[20]) / T(2);
z[211] = (T(-3) * z[19]) / T(2) + -z[207];
z[211] = z[23] * z[211];
z[148] = z[9] * z[148];
z[57] = z[57] + z[66] + z[67] + z[74] + z[130] + z[148] + z[152] + (T(-3) * z[174]) / T(2) + z[211];
z[57] = z[29] * z[57];
z[57] = z[57] + z[75] + z[140];
z[57] = z[29] * z[57];
z[66] = -z[2] + z[5];
z[66] = z[66] * z[76];
z[67] = -z[16] + (T(-9) * z[26]) / T(2) + z[69] + (T(-5) * z[84]) / T(2) + z[85] + z[89];
z[67] = z[4] * z[67];
z[73] = -z[73] + z[77] + z[194];
z[73] = z[73] * z[197];
z[74] = z[69] + z[137];
z[75] = (T(15) * z[1]) / T(2) + (T(-9) * z[14]) / T(2) + T(-7) * z[26] + z[58] + z[74] + z[104];
z[75] = z[3] * z[75];
z[76] = z[7] * z[98];
z[77] = -(z[95] * z[207]);
z[78] = z[78] + z[84];
z[84] = z[78] / T(2) + -z[85];
z[84] = z[84] * z[198];
z[104] = z[9] * z[167];
z[130] = -(z[18] * z[209]);
z[140] = -(z[19] * z[214]);
z[66] = z[66] + z[67] + z[73] + z[75] + z[76] + z[77] + z[84] + z[104] + z[130] + z[140] + -z[210];
z[66] = z[0] * z[66];
z[61] = -z[10] + -z[61] + z[81] + -z[90] + z[158] + z[166];
z[67] = z[2] * z[61];
z[73] = T(2) * z[3];
z[75] = -(z[61] * z[73]);
z[76] = z[65] + z[79] + -z[83];
z[77] = z[5] * z[76];
z[84] = z[81] + -z[122] + z[164];
z[84] = T(3) * z[84];
z[104] = -z[13] + z[84] + -z[85] + z[227];
z[104] = z[6] * z[104];
z[130] = -z[72] + z[122];
z[140] = z[130] + -z[170];
z[148] = z[9] * z[140];
z[152] = z[28] * z[224];
z[148] = z[148] + z[152];
z[128] = -(z[20] * z[128]);
z[140] = z[4] * z[140];
z[164] = z[18] * z[191];
z[75] = T(-2) * z[67] + z[75] + z[77] + z[104] + z[128] + -z[129] + z[140] + -z[148] + z[164] + z[225];
z[75] = z[31] * z[75];
z[58] = -z[58] + -z[65] + -z[136] + z[157] + z[215];
z[58] = z[58] * z[149];
z[65] = T(4) * z[16];
z[104] = z[65] + -z[106];
z[128] = T(4) * z[11];
z[129] = T(7) * z[24] + T(-16) * z[26] + -z[104] + z[128] + z[150] + z[180];
z[140] = z[7] * z[129];
z[145] = -z[1] + z[97] + z[145];
z[73] = z[73] * z[145];
z[145] = z[177] + -z[180];
z[145] = z[4] * z[145];
z[150] = T(4) * z[23] + z[95];
z[157] = z[19] * z[150];
z[58] = z[58] + z[73] + z[77] + z[140] + z[145] + -z[152] + z[157] + -z[183] + z[226];
z[58] = z[32] * z[58];
z[73] = z[3] + z[7];
z[73] = z[73] * z[98];
z[77] = -z[24] + z[158] + -z[160] + -z[216];
z[77] = z[9] * z[77];
z[98] = z[15] + z[135];
z[98] = T(2) * z[98] + -z[146] + z[192];
z[98] = z[4] * z[98];
z[135] = -z[19] + -z[20];
z[135] = z[135] * z[214];
z[145] = -z[5] + z[6];
z[145] = z[145] * z[220];
z[152] = -z[8] + z[28];
z[152] = z[152] * z[223];
z[73] = z[73] + z[77] + z[98] + z[135] + z[145] + z[152];
z[73] = z[29] * z[73];
z[77] = z[30] * z[168];
z[58] = z[58] + z[66] + T(2) * z[73] + z[75] + z[77];
z[58] = z[0] * z[58];
z[66] = T(6) * z[26] + -z[142] + T(-5) * z[144] + -z[160] + z[187];
z[66] = z[3] * z[66];
z[73] = z[128] + z[160] + -z[162];
z[73] = z[7] * z[73];
z[75] = T(5) * z[16];
z[77] = T(9) * z[11] + z[75] + z[123] + -z[155] + -z[181];
z[77] = z[4] * z[77];
z[98] = -(z[2] * z[125]);
z[123] = -(z[149] * z[159]);
z[66] = z[66] + z[73] + z[77] + z[98] + T(3) * z[103] + z[123] + z[131] + z[141] + z[184];
z[66] = z[32] * z[66];
z[73] = z[86] + -z[137] + -z[154] + z[177] + z[208];
z[73] = z[9] * z[73];
z[77] = z[72] + z[82] + -z[88] + -z[133];
z[82] = (T(5) * z[13]) / T(2) + -z[25] + -z[77] + -z[86] + -z[200];
z[82] = z[5] * z[82];
z[74] = (T(-5) * z[16]) / T(2) + z[49] + z[74] + z[77];
z[74] = z[28] * z[74];
z[69] = -z[69] + -z[154] + -z[159] + z[200];
z[69] = z[4] * z[69];
z[77] = T(-2) * z[100] + -z[190] / T(2);
z[69] = z[69] + z[73] + z[74] + T(3) * z[77] + z[82] + z[151] + -z[179];
z[69] = z[30] * z[69];
z[66] = z[66] + z[69] + z[120];
z[66] = z[30] * z[66];
z[69] = -(z[7] * z[178]);
z[73] = -z[24] + z[72];
z[68] = -z[68] + z[73];
z[74] = T(4) * z[49] + z[68] + z[75] + -z[176];
z[74] = z[28] * z[74];
z[77] = T(2) * z[156];
z[82] = -z[10] + z[77] + z[80] + z[161];
z[82] = z[4] * z[82];
z[86] = -z[21] + z[229];
z[88] = -(z[86] * z[214]);
z[98] = z[5] * z[192];
z[100] = -(z[9] * z[175]);
z[69] = z[69] + z[74] + z[82] + z[88] + z[98] + z[100] + -z[206];
z[69] = z[35] * z[69];
z[74] = z[113] + -z[139];
z[82] = z[34] * z[191];
z[82] = z[74] + z[82];
z[88] = z[77] + -z[87] + z[219];
z[98] = T(2) * z[28];
z[88] = z[88] * z[98];
z[77] = z[65] + z[77] + -z[130];
z[77] = z[4] * z[77];
z[100] = T(3) * z[203];
z[103] = z[10] + z[100] + z[201];
z[103] = z[6] * z[103];
z[113] = z[20] * z[105];
z[77] = z[77] + z[82] + z[88] + -z[103] + z[113] + -z[213] + -z[225];
z[88] = -z[29] + z[32];
z[77] = z[77] * z[88];
z[88] = -z[93] + z[107] + z[109] + -z[111];
z[88] = z[4] * z[88];
z[103] = z[118] + z[173] + -z[182];
z[103] = z[103] * z[149];
z[109] = T(3) * z[173];
z[113] = T(7) * z[16] + -z[90] + z[109];
z[118] = -z[1] + T(12) * z[13] + -z[113] + -z[121];
z[118] = z[5] * z[118];
z[120] = z[23] + T(4) * z[95];
z[123] = -(z[21] * z[120]);
z[82] = z[82] + z[88] + z[103] + z[118] + z[123] + T(-2) * z[141] + -z[226];
z[82] = z[31] * z[82];
z[88] = T(-8) * z[13] + z[75] + z[109] + -z[110] + z[119];
z[88] = z[5] * z[88];
z[103] = z[4] * z[112];
z[98] = z[98] + z[149];
z[98] = z[98] * z[175];
z[105] = z[21] * z[105];
z[74] = -z[74] + z[88] + -z[98] + z[103] + z[105];
z[88] = z[30] + -z[94];
z[74] = z[74] * z[88];
z[88] = -z[1] + z[85] + -z[96] + z[222];
z[88] = z[5] * z[88];
z[94] = -z[4] + -z[149];
z[94] = z[94] * z[192];
z[98] = -z[34] + z[91];
z[98] = z[98] * z[214];
z[103] = z[8] * z[223];
z[105] = -(z[6] * z[220]);
z[109] = z[1] + z[10] + -z[13] + T(2) * z[222];
z[109] = z[28] * z[109];
z[88] = z[88] + z[94] + z[98] + z[103] + z[105] + z[109];
z[88] = z[27] * z[88];
z[69] = z[69] + z[74] + z[77] + z[82] + T(2) * z[88];
z[69] = z[35] * z[69];
z[61] = z[3] * z[61];
z[74] = z[16] + T(2) * z[199];
z[77] = z[14] + -z[74] + -z[89] + -z[96] + z[106];
z[77] = z[4] * z[77];
z[82] = -(z[70] * z[214]);
z[68] = z[13] + T(-4) * z[25] + -z[68] + z[158];
z[68] = z[5] * z[68];
z[88] = z[28] * z[192];
z[94] = z[9] * z[171];
z[61] = z[61] + z[67] + z[68] + z[77] + z[82] + z[88] + z[94];
z[61] = z[27] * z[61];
z[67] = z[21] * z[191];
z[67] = z[67] + -z[116];
z[68] = -z[74] + -z[79] + z[121] + -z[146];
z[68] = z[68] * z[188];
z[74] = z[126] + z[181] + -z[193] + z[199];
z[74] = z[74] * z[134];
z[77] = T(12) * z[10];
z[82] = T(9) * z[13] + -z[77] + -z[99];
z[80] = T(11) * z[1] + z[14] + z[80] + z[82];
z[80] = z[8] * z[80];
z[68] = -z[67] + z[68] + -z[74] + z[80] + -z[148] + -z[218];
z[74] = -z[0] + z[32];
z[68] = z[68] * z[74];
z[62] = -z[62] + z[75] + -z[169] + -z[215];
z[62] = z[62] * z[149];
z[74] = z[65] + -z[81] + z[111] + -z[114];
z[74] = z[4] * z[74];
z[80] = -z[14] + z[65];
z[88] = -z[80] + z[169];
z[94] = T(7) * z[13] + -z[106];
z[88] = z[1] + T(3) * z[88] + z[94];
z[88] = z[28] * z[88];
z[96] = z[34] * z[150];
z[62] = z[62] + -z[67] + z[74] + z[88] + z[96] + T(2) * z[138] + -z[226];
z[62] = z[31] * z[62];
z[67] = -(z[30] * z[115]);
z[61] = z[61] + z[62] + z[67] + z[68];
z[61] = z[27] * z[61];
z[62] = T(12) * z[1] + z[93] + z[122] + -z[127] + z[153] + -z[158] + -z[176] + -z[185];
z[62] = z[43] * z[62];
z[67] = z[37] * z[76];
z[68] = z[38] * z[71];
z[67] = z[67] + -z[68];
z[68] = T(3) * z[40];
z[71] = -(z[68] * z[78]);
z[74] = T(3) * z[39];
z[76] = -(z[74] * z[97]);
z[88] = T(12) * z[40] + z[43];
z[88] = z[10] * z[88];
z[62] = (T(-4) * z[54]) / T(3) + z[62] + T(2) * z[67] + z[71] + z[76] + z[88];
z[62] = z[4] * z[62];
z[67] = -z[79] + z[104] + z[111] + z[143] + z[163] + -z[187];
z[67] = z[3] * z[67];
z[71] = z[9] * z[129];
z[76] = T(4) * z[24] + -z[158];
z[65] = (T(61) * z[1]) / T(2) + T(17) * z[13] + -z[14] + (T(-17) * z[26]) / T(2) + -z[65] + T(2) * z[76] + -z[77] + -z[176];
z[65] = z[4] * z[65];
z[76] = T(13) * z[1] + z[80] + z[82];
z[77] = z[5] + -z[8];
z[76] = z[76] * z[77];
z[77] = -z[19] + z[21];
z[77] = z[77] * z[150];
z[65] = z[65] + z[67] + z[71] + z[76] + z[77] + -z[140];
z[65] = z[44] * z[65];
z[67] = T(7) * z[14];
z[71] = -z[13] + z[81];
z[76] = -z[67] + z[71] + z[104] + z[172] + z[227];
z[76] = z[37] * z[76];
z[77] = -z[16] + z[107];
z[80] = T(6) * z[15] + z[77] + z[101] + -z[165] + -z[205] + z[227];
z[80] = z[38] * z[80];
z[82] = T(2) * z[12] + z[111];
z[71] = z[71] + z[82] + -z[122] + z[163];
z[88] = z[40] * z[71];
z[96] = -(z[39] * z[124]);
z[88] = z[88] + z[96];
z[88] = z[76] + -z[80] + T(3) * z[88];
z[88] = z[2] * z[88];
z[67] = T(-10) * z[16] + z[67] + -z[72] + -z[106] + z[169] + z[181];
z[67] = z[37] * z[67];
z[72] = T(10) * z[13] + z[72] + -z[75] + z[90] + -z[173] + -z[205];
z[72] = z[38] * z[72];
z[75] = z[16] + z[204];
z[75] = z[43] * z[75];
z[96] = z[10] * z[43];
z[75] = z[75] + -z[96];
z[97] = z[74] * z[147];
z[67] = (T(16) * z[54]) / T(3) + z[67] + z[72] + -z[75] + z[97];
z[67] = z[9] * z[67];
z[72] = z[77] + z[82] + -z[121] + z[136];
z[77] = -z[2] + -z[7] + z[8];
z[72] = z[72] * z[77];
z[77] = -z[87] + z[107] + z[111];
z[77] = z[28] * z[77];
z[60] = z[4] * z[60];
z[82] = -z[20] + z[34] + -z[229];
z[82] = z[23] * z[82];
z[60] = z[60] + z[72] + z[77] + z[82] + -z[174];
z[60] = z[42] * z[60];
z[72] = -z[4] + z[9];
z[72] = z[72] * z[132];
z[72] = z[72] + -z[102];
z[72] = z[46] * z[72];
z[60] = z[60] + z[72];
z[72] = -z[73] + -z[79] + -z[81] + z[85];
z[72] = z[40] * z[72];
z[77] = -(z[39] * z[189]);
z[72] = z[72] + z[77];
z[72] = z[72] * z[196];
z[77] = z[74] * z[189];
z[73] = z[10] + z[73] + -z[90] + z[108] + z[117];
z[73] = z[38] * z[73];
z[79] = z[93] + z[100] + z[146];
z[79] = z[43] * z[79];
z[79] = z[79] + -z[96];
z[81] = z[1] + T(-5) * z[14] + T(14) * z[16] + -z[94] + -z[186];
z[81] = z[37] * z[81];
z[73] = z[73] + z[77] + -z[79] + z[81];
z[73] = z[28] * z[73];
z[71] = z[68] * z[71];
z[75] = z[71] + z[75] + z[76];
z[75] = z[3] * z[75];
z[76] = -z[89] + -z[128] + z[195] + z[202];
z[76] = z[43] * z[76];
z[76] = (T(-9) * z[54]) / T(2) + z[76] + -z[80] + -z[96];
z[76] = z[7] * z[76];
z[71] = -z[71] + z[79];
z[71] = z[6] * z[71];
z[77] = T(7) * z[1] + z[221];
z[79] = -z[13] + -z[77] + -z[84] + z[106];
z[79] = z[6] * z[79];
z[78] = z[78] + -z[106];
z[80] = -z[78] + -z[83];
z[80] = z[5] * z[80];
z[70] = -z[70] + z[91];
z[70] = z[70] * z[150];
z[70] = z[70] + z[79] + z[80];
z[70] = z[37] * z[70];
z[77] = z[16] + z[77] + -z[90] + z[217];
z[77] = z[8] * z[77];
z[79] = -z[34] + -z[86];
z[79] = z[79] * z[120];
z[80] = -z[1] + T(5) * z[10] + T(-14) * z[13] + z[113];
z[80] = z[5] * z[80];
z[77] = z[77] + z[79] + z[80];
z[77] = z[38] * z[77];
z[79] = z[91] * z[220];
z[80] = -z[6] + -z[8] + -z[9];
z[80] = z[80] * z[214];
z[81] = z[34] * z[223];
z[82] = z[50] * z[191];
z[79] = z[79] + z[80] + z[81] + z[82];
z[79] = z[36] * z[79];
z[74] = -(z[74] * z[190]);
z[68] = z[68] * z[95];
z[80] = -z[18] + -z[21];
z[80] = z[68] * z[80];
z[81] = -z[19] + -z[34];
z[82] = z[43] * z[120];
z[81] = z[81] * z[82];
z[68] = -z[68] + -z[82];
z[68] = z[20] * z[68];
z[78] = z[40] * z[78] * z[198];
z[82] = T(-29) * z[6] + T(65) * z[8];
z[82] = (T(-37) * z[3]) / T(6) + T(-25) * z[5] + T(29) * z[28] + z[82] / T(3);
z[82] = z[54] * z[82];
z[83] = int_to_imaginary<T>(1) * z[53] * z[92];
return z[57] + z[58] + z[59] + T(3) * z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[74] + z[75] + z[76] + z[77] + z[78] + T(2) * z[79] + z[80] + z[81] + z[82] + (T(96) * z[83]) / T(5) + z[88];
}



template IntegrandConstructorType<double> f_4_197_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_197_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_197_construct (const Kin<qd_real>&);
#endif

}