#include "f_4_5.h"

namespace PentagonFunctions {

template <typename T> T f_4_5_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_5_W_7 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_5_W_7 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[3]) * (T(4) * kin.v[1] * kin.v[4] + kin.v[3] * (T(4) + T(4) * kin.v[1] + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(4) * kin.v[1] * kin.v[4] + kin.v[3] * (T(4) + T(4) * kin.v[1] + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4])) + rlog(-kin.v[4]) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[3] * (T(-4) + T(-4) * kin.v[1] + T(2) * kin.v[3] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-2) + prod_pow(abb[2], 2) * (abb[5] * T(-2) + abb[4] * T(2)) + prod_pow(abb[1], 2) * (abb[4] * T(-2) + abb[3] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_5_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl2 = DLog_W_2<T>(kin),dl14 = DLog_W_14<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),spdl7 = SpDLog_f_4_5_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), dl2(t), -rlog(t), rlog(kin.W[13] / kin_path.W[13]), dl14(t), rlog(v_path[3]), dl4(t), f_2_1_3(kin_path), f_2_1_11(kin_path), rlog(-v_path[1] + v_path[3]), dl19(t), dl5(t)}
;

        auto result = f_4_5_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_5_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[32];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[6];
z[3] = abb[9];
z[4] = abb[11];
z[5] = abb[15];
z[6] = abb[16];
z[7] = abb[4];
z[8] = abb[5];
z[9] = abb[7];
z[10] = abb[8];
z[11] = abb[2];
z[12] = abb[10];
z[13] = bc<TR>[0];
z[14] = abb[12];
z[15] = abb[13];
z[16] = abb[14];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = z[1] + z[8];
z[20] = -z[7] + z[19];
z[21] = z[0] * z[20];
z[22] = z[4] + -z[6];
z[23] = z[21] * z[22];
z[24] = -z[1] + z[7];
z[24] = int_to_imaginary<T>(1) * z[24];
z[25] = int_to_imaginary<T>(1) * z[8];
z[24] = z[24] + -z[25];
z[26] = z[13] * z[24];
z[21] = -z[21] + z[26];
z[21] = z[5] * z[21];
z[21] = -z[21] + z[23];
z[23] = int_to_imaginary<T>(1) * z[10];
z[23] = z[23] + -z[25];
z[23] = z[4] * z[23];
z[24] = -(z[3] * z[24]);
z[23] = z[23] + z[24];
z[23] = z[13] * z[23];
z[23] = -z[21] + z[23];
z[24] = -z[3] + z[5];
z[24] = z[20] * z[24];
z[25] = z[8] + -z[10];
z[27] = z[4] * z[25];
z[24] = z[24] + z[27];
z[24] = z[12] * z[24];
z[23] = T(2) * z[23] + z[24];
z[23] = z[12] * z[23];
z[24] = z[22] * z[26];
z[27] = z[5] + z[22];
z[28] = z[20] * z[27];
z[29] = z[12] * z[28];
z[21] = -z[21] + z[24] + z[29];
z[24] = z[4] * z[20];
z[29] = z[1] + -z[9];
z[29] = z[5] * z[29];
z[24] = z[24] + z[29];
z[24] = z[11] * z[24];
z[21] = T(2) * z[21] + z[24];
z[21] = z[11] * z[21];
z[21] = z[21] + z[23];
z[23] = int_to_imaginary<T>(1) * z[13];
z[24] = (T(-5) * z[3]) / T(2) + -z[22];
z[24] = z[23] * z[24];
z[29] = T(2) * z[6];
z[30] = -(z[17] * z[29]);
z[25] = T(2) * z[17] + (T(-13) * z[25]) / T(6);
z[25] = z[4] * z[25];
z[31] = z[3] * z[20];
z[24] = z[24] / T(3) + z[25] + z[30] + T(2) * z[31];
z[25] = prod_pow(z[13], 2);
z[24] = z[24] * z[25];
z[28] = z[15] * z[28];
z[26] = -(z[16] * z[26] * z[27]);
z[26] = z[26] + z[28];
z[27] = z[3] + z[4] + -z[29];
z[28] = prod_pow(z[0], 2);
z[28] = T(2) * z[28];
z[27] = z[20] * z[27] * z[28];
z[29] = T(4) * z[14];
z[30] = z[28] + z[29];
z[30] = z[20] * z[30];
z[31] = z[7] + -z[8] + z[17];
z[23] = (T(-13) * z[1]) / T(6) + z[9] / T(6) + z[23] / T(2) + T(2) * z[31];
z[23] = z[23] * z[25];
z[31] = T(3) * z[18];
z[23] = z[23] + z[30] + -z[31];
z[23] = z[5] * z[23];
z[20] = z[20] * z[29];
z[20] = z[20] + -z[31];
z[20] = z[20] * z[22];
z[22] = z[25] / T(6) + -z[28];
z[19] = -z[9] + -z[10] + z[19];
z[19] = z[2] * z[19] * z[22];
return z[19] + z[20] + T(2) * z[21] + z[23] + z[24] + T(4) * z[26] + z[27];
}



template IntegrandConstructorType<double> f_4_5_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_5_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_5_construct (const Kin<qd_real>&);
#endif

}