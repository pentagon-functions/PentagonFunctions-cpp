#include "f_4_46.h"

namespace PentagonFunctions {

template <typename T> T f_4_46_abbreviated (const std::array<T,20>&);

template <typename T> class SpDLog_f_4_46_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_46_W_7 (const Kin<T>& kin) {
        c[0] = T(-3) * kin.v[1] * kin.v[4] + ((T(9) * kin.v[4]) / T(4) + T(-3)) * kin.v[4] + ((T(9) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2) + T(-3) + T(-3) * kin.v[1]) * kin.v[3];
c[1] = kin.v[3] * (T(-3) + T(-3) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1]) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + T(-3) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-3) + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[4] + T(-8) * kin.v[1] * kin.v[4]) + rlog(-kin.v[4]) * (((T(-27) * kin.v[4]) / T(2) + T(14)) * kin.v[4] + T(14) * kin.v[1] * kin.v[4] + kin.v[3] * (T(14) + T(14) * kin.v[1] + (T(-27) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-27) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-4) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(-45) * kin.v[4]) / T(4) + T(11)) * kin.v[4] + T(11) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-45) * kin.v[4]) / T(2) + T(11) + T(11) * kin.v[1] + (T(-45) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-4) * kin.v[4]))) + rlog(kin.v[3]) * (((T(27) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + T(-14) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-8) + T(4) * kin.v[4])) + kin.v[3] * (T(-14) + T(-14) * kin.v[1] + (T(27) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[3] + T(27) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[4]))) + rlog(-kin.v[1]) * (T(-28) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-28) + T(27) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-16) + T(8) * kin.v[4])) + kin.v[3] * (T(-28) + T(-28) * kin.v[1] + (bc<T>[0] * int_to_imaginary<T>(8) + T(27)) * kin.v[3] + T(54) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) + T(-16) * kin.v[1] + T(16) * kin.v[4])));
c[2] = T(3) * kin.v[3] + T(3) * kin.v[4];
c[3] = rlog(kin.v[3]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(9)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(9) * kin.v[4]) + rlog(-kin.v[1]) * ((T(-12) + bc<T>[0] * int_to_imaginary<T>(-16)) * kin.v[3] + T(-12) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-16) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (abb[2] * (abb[10] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * (-abb[8] + abb[9] * T(-4) + abb[4] * T(-3) + abb[10] * T(4))) + abb[3] * (abb[8] * T(-9) + abb[9] * T(-6) + abb[4] * T(3) + abb[10] * T(6)) + abb[2] * abb[7] * (abb[10] * T(-8) + abb[8] * T(8) + abb[9] * T(8)) + abb[2] * abb[6] * (abb[8] * T(-8) + abb[9] * T(-8) + abb[10] * T(8)) + abb[5] * (abb[2] * abb[7] * T(-16) + abb[2] * (bc<T>[0] * int_to_imaginary<T>(-16) + abb[2] * T(8)) + abb[3] * T(12) + abb[2] * abb[6] * T(16)) + abb[1] * (abb[8] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * (abb[8] + abb[5] * T(-8) + abb[10] * T(-4) + abb[4] * T(3) + abb[9] * T(4)) + abb[6] * (abb[10] * T(-8) + abb[8] * T(8) + abb[9] * T(8)) + abb[7] * (abb[8] * T(-8) + abb[9] * T(-8) + abb[10] * T(8)) + abb[5] * (abb[6] * T(-16) + bc<T>[0] * int_to_imaginary<T>(16) + abb[7] * T(16))));
    }
};
template <typename T> class SpDLog_f_4_46_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_46_W_12 (const Kin<T>& kin) {
        c[0] = ((T(-3) * kin.v[1]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[1] + ((T(9) * kin.v[4]) / T(4) + T(3)) * kin.v[4];
c[1] = bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + kin.v[4] * (T(3) + T(-3) * kin.v[4]) + kin.v[1] * (T(-3) + (T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * ((T(23) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(8) + T(11) + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1]) + ((T(-45) * kin.v[4]) / T(4) + T(-11)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-4) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(-27) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[1] * (bc<T>[0] * int_to_imaginary<T>(8) + T(14) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(13) * kin.v[4])) + rlog(kin.v[3]) * (((T(27) * kin.v[4]) / T(2) + T(14)) * kin.v[4] + kin.v[1] * (T(-14) + bc<T>[0] * int_to_imaginary<T>(-8) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-13) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(8) + T(4) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (T(-28) + bc<T>[0] * int_to_imaginary<T>(-16) + (T(-1) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-26) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(16) + T(8) * kin.v[4]) + kin.v[4] * (T(28) + T(27) * kin.v[4]));
c[2] = T(3) * kin.v[1] + T(-3) * kin.v[4];
c[3] = rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(9)) * kin.v[1] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[1] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(kin.v[3]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[1]) * ((T(-12) + bc<T>[0] * int_to_imaginary<T>(-16)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(16) * kin.v[4] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[11] * (abb[12] * (abb[9] * T(-9) + abb[8] * T(-6) + abb[13] * T(3) + abb[10] * T(6)) + abb[6] * abb[7] * (abb[8] * T(-8) + abb[9] * T(-8) + abb[10] * T(8)) + abb[7] * (abb[10] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(8) + abb[7] * (-abb[9] + abb[8] * T(-4) + abb[13] * T(-3) + abb[10] * T(4)) + abb[2] * (abb[10] * T(-8) + abb[8] * T(8) + abb[9] * T(8))) + abb[5] * (abb[7] * (abb[2] * T(-16) + bc<T>[0] * int_to_imaginary<T>(-16) + abb[7] * T(8)) + abb[12] * T(12) + abb[6] * abb[7] * T(16)) + abb[1] * (abb[8] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * (abb[9] + abb[5] * T(-8) + abb[10] * T(-4) + abb[13] * T(3) + abb[8] * T(4)) + abb[6] * (abb[10] * T(-8) + abb[8] * T(8) + abb[9] * T(8)) + abb[2] * (abb[8] * T(-8) + abb[9] * T(-8) + abb[10] * T(8)) + abb[5] * (abb[6] * T(-16) + bc<T>[0] * int_to_imaginary<T>(16) + abb[2] * T(16))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_46_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl12 = DLog_W_12<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl19 = DLog_W_19<T>(kin),spdl7 = SpDLog_f_4_46_W_7<T>(kin),spdl12 = SpDLog_f_4_46_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,20> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), -rlog(t), rlog(kin.W[1] / kin_path.W[1]), rlog(v_path[3]), rlog(-v_path[4]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[3] / kin_path.W[3]), dl12(t), f_2_1_4(kin_path), -rlog(t), dl4(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl2(t), dl5(t), dl19(t)}
;

        auto result = f_4_46_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_46_abbreviated(const std::array<T,20>& abb)
{
using TR = typename T::value_type;
T z[61];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[14];
z[3] = abb[17];
z[4] = abb[18];
z[5] = abb[19];
z[6] = abb[5];
z[7] = abb[8];
z[8] = abb[9];
z[9] = abb[10];
z[10] = abb[13];
z[11] = abb[2];
z[12] = abb[6];
z[13] = abb[7];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[12];
z[17] = abb[15];
z[18] = abb[16];
z[19] = bc<TR>[9];
z[20] = T(2) * z[6] + z[9];
z[21] = int_to_imaginary<T>(1) * z[14];
z[22] = -z[12] + z[21];
z[23] = z[20] * z[22];
z[24] = T(4) * z[23];
z[25] = z[7] + z[8];
z[26] = z[20] + -z[25];
z[27] = T(4) * z[11];
z[26] = z[26] * z[27];
z[28] = z[1] * z[22];
z[29] = z[7] * z[22];
z[30] = z[8] * z[22];
z[31] = -z[1] + z[7];
z[32] = z[0] * z[31];
z[26] = z[24] + z[26] + -z[28] + T(-3) * z[29] + T(-4) * z[30] + z[32];
z[32] = T(3) * z[10];
z[33] = z[1] + -z[20];
z[33] = z[8] + z[32] + T(4) * z[33];
z[33] = z[13] * z[33];
z[26] = T(2) * z[26] + z[33];
z[26] = z[13] * z[26];
z[33] = T(4) * z[21];
z[34] = T(7) * z[12] + -z[33];
z[25] = -(z[25] * z[34]);
z[34] = z[1] + z[10];
z[33] = -z[12] + z[33];
z[33] = z[33] * z[34];
z[35] = z[8] + -z[10];
z[36] = z[11] * z[35];
z[25] = T(-8) * z[23] + z[25] + z[33] + z[36];
z[33] = z[31] + z[35];
z[36] = z[0] * z[33];
z[25] = T(2) * z[25] + T(5) * z[36];
z[25] = z[0] * z[25];
z[36] = -z[12] + T(2) * z[21];
z[37] = T(4) * z[12];
z[38] = z[36] * z[37];
z[39] = prod_pow(z[14], 2);
z[38] = z[38] + T(3) * z[39];
z[40] = T(6) * z[18];
z[40] = z[21] * z[40];
z[41] = z[38] + -z[40];
z[42] = z[1] * z[41];
z[41] = z[10] * z[41];
z[43] = T(3) * z[7];
z[44] = T(5) * z[1];
z[45] = T(-8) * z[20] + z[43] + z[44];
z[45] = T(9) * z[8] + T(7) * z[10] + T(2) * z[45];
z[45] = z[16] * z[45];
z[46] = z[23] + -z[29];
z[47] = z[10] * z[22];
z[46] = T(-3) * z[30] + T(4) * z[46] + -z[47];
z[48] = T(3) * z[1];
z[49] = T(4) * z[20];
z[50] = z[7] + T(4) * z[10] + z[48] + -z[49];
z[50] = z[11] * z[50];
z[46] = T(2) * z[46] + z[50];
z[46] = z[11] * z[46];
z[50] = -(z[20] * z[38]);
z[51] = z[7] * z[40];
z[52] = int_to_imaginary<T>(1) * prod_pow(z[14], 3);
z[53] = T(7) * z[1] + T(9) * z[7] + T(10) * z[10] + T(-16) * z[20];
z[53] = z[15] * z[53];
z[54] = z[18] * z[21];
z[54] = z[15] + z[54];
z[54] = z[8] * z[54];
z[25] = (T(-64) * z[19]) / T(3) + z[25] + z[26] + z[41] + z[42] + z[45] + z[46] + z[50] + z[51] + z[52] / T(3) + z[53] + T(6) * z[54];
z[25] = z[3] * z[25];
z[26] = T(2) * z[1];
z[45] = z[22] * z[26];
z[45] = -z[23] + z[45];
z[46] = z[29] + -z[30];
z[50] = z[45] + -z[46];
z[51] = -z[7] + -z[20] + z[26];
z[53] = z[8] + z[51];
z[53] = z[11] * z[53];
z[50] = T(2) * z[50] + -z[53];
z[53] = T(2) * z[11];
z[50] = z[50] * z[53];
z[54] = T(4) * z[8];
z[49] = -z[7] + T(8) * z[10] + z[44] + -z[49] + -z[54];
z[55] = T(2) * z[16];
z[56] = z[49] * z[55];
z[57] = (T(5) * z[39]) / T(3) + -z[40];
z[58] = z[10] * z[57];
z[56] = z[56] + -z[58];
z[38] = z[6] * z[38];
z[58] = T(2) * z[12];
z[36] = z[36] * z[58];
z[58] = (T(3) * z[39]) / T(2);
z[59] = z[36] + z[58];
z[59] = z[9] * z[59];
z[38] = (T(32) * z[19]) / T(3) + z[38] + -z[52] / T(6) + z[59];
z[36] = z[36] + -z[40];
z[40] = z[36] + z[58];
z[52] = z[7] * z[40];
z[36] = z[36] + (T(19) * z[39]) / T(6);
z[39] = T(8) * z[15] + -z[36];
z[39] = z[8] * z[39];
z[58] = T(5) * z[10];
z[59] = -z[51] + -z[58];
z[60] = T(2) * z[15];
z[59] = z[59] * z[60];
z[39] = z[38] + z[39] + -z[42] + -z[50] + z[52] + -z[56] + z[59];
z[39] = z[5] * z[39];
z[42] = T(3) * z[8];
z[43] = z[42] + z[43];
z[52] = z[21] * z[43];
z[21] = -z[21] + z[37];
z[34] = -(z[21] * z[34]);
z[37] = T(2) * z[31] + z[35];
z[37] = z[27] * z[37];
z[24] = -z[24] + z[34] + z[37] + z[52];
z[24] = z[2] * z[24];
z[32] = -z[32] + z[42];
z[32] = z[12] * z[32];
z[34] = -(z[21] * z[31]);
z[37] = T(4) * z[51] + z[58];
z[42] = -z[8] + z[37];
z[42] = z[11] * z[42];
z[32] = z[32] + z[34] + z[42];
z[32] = z[4] * z[32];
z[34] = z[2] * z[33];
z[42] = T(2) * z[7] + z[20] + -z[48];
z[42] = -z[8] + -z[10] + T(2) * z[42];
z[42] = z[4] * z[42];
z[48] = -z[1] + -z[7] + T(-6) * z[10] + T(2) * z[20] + z[54];
z[48] = z[5] * z[48];
z[42] = T(-6) * z[34] + z[42] + z[48];
z[42] = z[0] * z[42];
z[21] = -z[21] + -z[27];
z[21] = z[21] * z[35];
z[27] = z[12] * z[31];
z[21] = z[21] + T(3) * z[27];
z[21] = z[5] * z[21];
z[21] = z[21] + z[24] + z[32] + z[42];
z[21] = z[0] * z[21];
z[24] = T(2) * z[10];
z[22] = z[22] * z[24];
z[22] = z[22] + -z[23] + z[46];
z[26] = z[20] + z[24] + z[26] + -z[43];
z[26] = z[11] * z[26];
z[26] = -z[22] + z[26];
z[26] = z[2] * z[26];
z[27] = -z[8] + z[24];
z[32] = z[27] + z[51];
z[42] = z[11] * z[32];
z[22] = -z[22] + -z[42];
z[22] = z[4] * z[22];
z[22] = z[22] + z[26];
z[23] = z[23] + z[30];
z[26] = -(z[32] * z[53]);
z[23] = T(-2) * z[23] + z[26] + z[28] + z[29] + T(4) * z[47];
z[23] = z[5] * z[23];
z[26] = z[5] * z[49];
z[24] = T(2) * z[8] + -z[24] + z[31];
z[24] = z[2] * z[24];
z[28] = z[4] * z[31];
z[24] = -z[24] + z[28];
z[24] = T(-4) * z[24] + z[26];
z[24] = z[0] * z[24];
z[20] = -z[20] + z[27];
z[26] = z[7] + z[20];
z[27] = z[2] + z[4];
z[26] = z[26] * z[27];
z[27] = -(z[5] * z[32]);
z[26] = z[26] + z[27];
z[26] = z[13] * z[26];
z[22] = T(2) * z[22] + z[23] + z[24] + z[26];
z[22] = z[13] * z[22];
z[21] = z[21] + z[22];
z[22] = z[7] * z[36];
z[23] = z[1] * z[57];
z[24] = z[37] * z[60];
z[22] = z[22] + -z[23] + z[24] + -z[38];
z[23] = -z[29] + z[45];
z[23] = T(2) * z[23] + z[30] + -z[42] + z[47];
z[23] = z[23] * z[53];
z[24] = z[40] + z[60];
z[24] = z[8] * z[24];
z[20] = T(4) * z[7] + -z[20] + -z[44];
z[20] = z[20] * z[55];
z[20] = z[20] + -z[22] + z[23] + z[24] + -z[41];
z[20] = z[4] * z[20];
z[23] = z[36] + -z[60];
z[23] = z[8] * z[23];
z[22] = z[22] + z[23] + -z[50] + z[56];
z[22] = z[2] * z[22];
z[23] = z[31] + -z[35];
z[24] = z[4] + -z[5];
z[23] = z[23] * z[24];
z[24] = z[3] * z[33];
z[23] = z[23] + z[24] + -z[34];
z[23] = z[17] * z[23];
return z[20] + T(2) * z[21] + z[22] + T(6) * z[23] + z[25] + z[39];
}



template IntegrandConstructorType<double> f_4_46_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_46_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_46_construct (const Kin<qd_real>&);
#endif

}