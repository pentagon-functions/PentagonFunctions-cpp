#include "f_4_334.h"

namespace PentagonFunctions {

template <typename T> T f_4_334_abbreviated (const std::array<T,53>&);

template <typename T> class SpDLog_f_4_334_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_334_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-14) + bc<T>[0] * int_to_imaginary<T>(8) + (T(5) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(-19) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + ((T(33) * kin.v[4]) / T(2) + T(14)) * kin.v[4];
c[1] = (T(-6) + bc<T>[0] * int_to_imaginary<T>(8)) * kin.v[1] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4];
c[2] = ((T(-33) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(8) + T(4) * kin.v[4]) + kin.v[1] * (bc<T>[0] * int_to_imaginary<T>(-8) + T(14) + (T(-5) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(19) * kin.v[4]);
c[3] = (bc<T>[0] * int_to_imaginary<T>(-8) + T(6)) * kin.v[1] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[1] * (t * c[0] + c[1]) + abb[9] * (t * c[2] + c[3]);
        }

        return abb[28] * (abb[9] * abb[29] * T(-6) + abb[5] * (abb[6] * abb[9] * T(-8) + abb[5] * abb[9] * T(-7) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-8)) + abb[3] * abb[5] * abb[9] * T(8) + abb[19] * (abb[19] * (abb[9] + -abb[1]) + abb[3] * abb[9] * T(-8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(8) + abb[5] * abb[9] * T(6) + abb[6] * abb[9] * T(8) + abb[1] * (abb[6] * T(-8) + abb[5] * T(-6) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[3] * T(8))) + abb[1] * (abb[3] * abb[5] * T(-8) + abb[29] * T(6) + abb[5] * (bc<T>[0] * int_to_imaginary<T>(8) + abb[5] * T(7) + abb[6] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_334_construct (const Kin<T>& kin) {
    return [&kin, 
            dl6 = DLog_W_6<T>(kin),dl16 = DLog_W_16<T>(kin),dl25 = DLog_W_25<T>(kin),dl15 = DLog_W_15<T>(kin),dl30 = DLog_W_30<T>(kin),dl12 = DLog_W_12<T>(kin),dl2 = DLog_W_2<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl31 = DLog_W_31<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),spdl12 = SpDLog_f_4_334_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,53> abbr = 
            {dl6(t), f_1_3_1(kin) - f_1_3_1(kin_path), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_9(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl16(t), f_1_3_4(kin) - f_1_3_4(kin_path), f_2_1_7(kin_path), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl25(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), rlog(-v_path[1]), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl30(t) / kin_path.SqrtDelta, rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[19] / kin_path.W[19]), dl12(t), f_2_1_4(kin_path), dl2(t), f_2_1_11(kin_path), dl20(t), f_2_2_1(kin_path), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[30] / kin_path.W[30]), f_2_1_1(kin_path), dl4(t), dl17(t), dl3(t), dl1(t), dl19(t), dl18(t), dl5(t), dl31(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_334_abbreviated(abbr);
        result = result + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_334_abbreviated(const std::array<T,53>& abb)
{
using TR = typename T::value_type;
T z[222];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[32];
z[12] = abb[37];
z[13] = abb[38];
z[14] = abb[39];
z[15] = abb[40];
z[16] = abb[41];
z[17] = abb[42];
z[18] = abb[43];
z[19] = abb[16];
z[20] = abb[20];
z[21] = abb[30];
z[22] = abb[19];
z[23] = abb[15];
z[24] = abb[12];
z[25] = abb[13];
z[26] = abb[14];
z[27] = abb[17];
z[28] = abb[18];
z[29] = abb[21];
z[30] = abb[22];
z[31] = abb[29];
z[32] = abb[31];
z[33] = abb[36];
z[34] = abb[33];
z[35] = abb[50];
z[36] = abb[51];
z[37] = abb[52];
z[38] = abb[11];
z[39] = abb[24];
z[40] = abb[25];
z[41] = abb[45];
z[42] = abb[27];
z[43] = abb[34];
z[44] = abb[35];
z[45] = abb[46];
z[46] = abb[47];
z[47] = abb[48];
z[48] = abb[49];
z[49] = abb[26];
z[50] = abb[10];
z[51] = abb[23];
z[52] = abb[44];
z[53] = bc<TR>[3];
z[54] = bc<TR>[1];
z[55] = bc<TR>[5];
z[56] = bc<TR>[2];
z[57] = bc<TR>[4];
z[58] = bc<TR>[7];
z[59] = bc<TR>[8];
z[60] = bc<TR>[9];
z[61] = T(5) * z[6];
z[62] = T(4) * z[3];
z[63] = z[61] + z[62];
z[64] = T(4) * z[4];
z[65] = z[19] + z[64];
z[66] = z[63] + z[65];
z[67] = T(6) * z[30];
z[68] = T(8) * z[2];
z[69] = T(3) * z[26];
z[70] = T(2) * z[5];
z[71] = -z[66] + z[67] + -z[68] + -z[69] + -z[70];
z[71] = z[38] * z[71];
z[72] = T(4) * z[2];
z[73] = z[6] + z[62] + z[69] + z[72];
z[74] = T(11) * z[30];
z[75] = T(3) * z[19];
z[76] = T(3) * z[5];
z[73] = T(2) * z[73] + -z[74] + -z[75] + z[76];
z[73] = z[1] * z[73];
z[77] = T(2) * z[2];
z[78] = T(2) * z[3];
z[79] = -z[6] + z[69] + z[77] + z[78];
z[80] = T(3) * z[30];
z[81] = z[5] + z[80];
z[79] = T(3) * z[28] + -z[64] + -z[75] + T(2) * z[79] + -z[81];
z[79] = z[9] * z[79];
z[82] = -z[76] + z[80];
z[83] = z[2] + z[6];
z[84] = T(8) * z[4];
z[85] = -z[19] + z[82] + T(-4) * z[83] + -z[84];
z[85] = z[10] * z[85];
z[86] = prod_pow(z[54], 2);
z[87] = (T(3) * z[86]) / T(10);
z[88] = z[10] + z[38];
z[89] = T(-5) * z[1] + T(13) * z[88];
z[89] = z[28] * z[89];
z[71] = z[71] + z[73] + z[79] + z[85] + z[87] + z[89];
z[71] = z[15] * z[71];
z[73] = z[68] + -z[76];
z[79] = T(8) * z[30];
z[85] = T(11) * z[22] + z[69] + z[73] + -z[79];
z[85] = z[38] * z[85];
z[89] = T(4) * z[5];
z[90] = -z[64] + z[89];
z[91] = T(6) * z[26];
z[92] = T(9) * z[2] + -z[22] + -z[80] + z[90] + z[91];
z[92] = z[10] * z[92];
z[93] = z[2] + z[22];
z[94] = -z[62] + z[80] + z[93];
z[95] = T(5) * z[28];
z[96] = T(4) * z[19];
z[97] = z[94] + -z[95] + z[96];
z[97] = z[9] * z[97];
z[98] = -z[30] + z[93];
z[99] = T(3) * z[1];
z[100] = -(z[98] * z[99]);
z[101] = T(-11) * z[88] + z[99];
z[101] = z[28] * z[101];
z[85] = z[85] + (T(-43) * z[86]) / T(30) + z[92] + z[97] + z[100] + z[101];
z[85] = z[13] * z[85];
z[92] = T(3) * z[2];
z[91] = z[91] + -z[92];
z[97] = -z[64] + z[95];
z[100] = z[5] + -z[22];
z[101] = z[97] + z[100];
z[102] = T(4) * z[6];
z[103] = -z[91] + z[96] + -z[101] + z[102];
z[103] = z[9] * z[103];
z[104] = z[69] + -z[72];
z[105] = -z[64] + z[100] + z[104];
z[105] = z[38] * z[105];
z[106] = T(5) * z[22];
z[107] = T(5) * z[5];
z[91] = -z[91] + z[106] + -z[107];
z[91] = z[1] * z[91];
z[108] = -z[5] + z[93];
z[109] = -z[64] + -z[108];
z[109] = z[10] * z[109];
z[110] = T(5) * z[88];
z[111] = z[99] + z[110];
z[111] = z[28] * z[111];
z[91] = z[87] + z[91] + z[103] + z[105] + z[109] + z[111];
z[91] = z[16] * z[91];
z[103] = z[62] + -z[72];
z[105] = T(4) * z[22];
z[109] = z[103] + -z[105];
z[111] = T(3) * z[6];
z[112] = z[69] + -z[111];
z[113] = z[19] + z[112];
z[114] = z[109] + -z[113];
z[115] = z[38] * z[114];
z[116] = z[6] + z[70];
z[117] = T(2) * z[22];
z[118] = z[116] + -z[117];
z[119] = -z[69] + z[118];
z[119] = -z[65] + T(2) * z[119];
z[119] = z[10] * z[119];
z[120] = T(5) * z[19];
z[121] = -z[3] + z[93];
z[122] = z[120] + T(8) * z[121];
z[123] = z[1] * z[122];
z[124] = T(13) * z[1];
z[125] = z[110] + -z[124];
z[125] = z[28] * z[125];
z[126] = z[19] + T(4) * z[121];
z[127] = -z[95] + z[126];
z[128] = z[9] * z[127];
z[115] = z[87] + z[115] + z[119] + z[123] + z[125] + z[128];
z[115] = z[17] * z[115];
z[67] = z[22] + -z[62] + z[67] + -z[77] + z[113];
z[67] = z[38] * z[67];
z[113] = T(7) * z[22];
z[119] = z[92] + z[113];
z[123] = T(10) * z[5] + z[65] + -z[80] + z[102] + -z[119];
z[123] = z[10] * z[123];
z[125] = T(8) * z[3];
z[74] = -z[74] + T(3) * z[93] + -z[120] + z[125];
z[74] = z[1] * z[74];
z[128] = T(2) * z[28];
z[94] = -z[19] + -z[94] + z[128];
z[94] = z[9] * z[94];
z[129] = -z[1] + z[88];
z[130] = z[128] * z[129];
z[67] = z[67] + z[74] + (T(3) * z[86]) / T(5) + z[94] + z[123] + -z[130];
z[67] = z[18] * z[67];
z[74] = z[6] + z[22];
z[80] = T(8) * z[74] + -z[80] + z[84] + -z[107] + z[120];
z[80] = z[21] * z[80];
z[94] = -z[2] + z[4];
z[123] = T(8) * z[0];
z[94] = z[94] * z[123];
z[80] = z[80] + z[94];
z[80] = z[1] * z[80];
z[84] = z[68] + -z[82] + -z[84];
z[84] = z[1] * z[84];
z[123] = -z[2] + z[3];
z[131] = z[6] + z[123];
z[131] = -z[64] + z[81] + T(-4) * z[131];
z[131] = z[9] * z[131];
z[132] = T(2) * z[30];
z[133] = z[4] + -z[5] + -z[93] + z[132];
z[133] = z[38] * z[133];
z[134] = T(3) * z[10];
z[135] = -z[5] + z[30];
z[135] = z[134] * z[135];
z[84] = z[84] + z[87] + z[131] + T(4) * z[133] + z[135];
z[84] = z[14] * z[84];
z[87] = z[95] + z[114];
z[87] = z[42] * z[87];
z[114] = -(z[43] * z[127]);
z[127] = T(6) * z[45];
z[131] = -z[19] + z[28];
z[133] = -(z[127] * z[131]);
z[135] = T(3) * z[54] + z[56];
z[135] = z[56] * z[135];
z[87] = T(-14) * z[57] + T(-5) * z[86] + z[87] + z[114] + z[133] + T(2) * z[135];
z[87] = z[41] * z[87];
z[114] = T(8) * z[28];
z[133] = z[2] + z[105];
z[90] = z[69] + -z[90] + -z[114] + z[133];
z[90] = z[37] * z[90];
z[109] = -z[96] + z[109] + z[112] + z[114];
z[109] = z[41] * z[109];
z[112] = T(8) * z[22];
z[135] = T(5) * z[2];
z[136] = z[112] + z[135];
z[114] = T(-5) * z[30] + -z[114] + z[136];
z[69] = z[69] + -z[76];
z[137] = -z[69] + -z[114];
z[137] = z[35] * z[137];
z[114] = z[36] * z[114];
z[138] = z[5] + -z[26];
z[139] = z[51] * z[138];
z[140] = T(3) * z[139];
z[90] = z[90] + z[109] + z[114] + z[137] + z[140];
z[90] = z[40] * z[90];
z[109] = -z[2] + z[30];
z[114] = -z[109] + z[138];
z[114] = z[35] * z[114];
z[137] = -z[2] + z[26];
z[137] = z[37] * z[137];
z[109] = z[36] * z[109];
z[141] = z[6] + -z[26];
z[142] = -(z[41] * z[141]);
z[109] = z[109] + z[114] + z[137] + z[139] + z[142];
z[114] = T(3) * z[39];
z[109] = z[109] * z[114];
z[66] = z[5] + -z[66] + z[79] + -z[112];
z[66] = z[38] * z[66];
z[81] = -z[65] + T(-4) * z[74] + z[81];
z[137] = z[10] * z[81];
z[66] = z[66] + z[137];
z[66] = z[21] * z[66];
z[137] = T(11) * z[1];
z[142] = z[134] + -z[137];
z[142] = z[98] * z[142];
z[143] = z[22] + z[77];
z[132] = -z[132] + z[143];
z[144] = T(3) * z[38];
z[132] = z[132] * z[144];
z[137] = T(-3) * z[88] + z[137];
z[137] = z[28] * z[137];
z[132] = (T(-26) * z[86]) / T(15) + z[132] + z[137] + z[142];
z[132] = z[11] * z[132];
z[81] = -z[81] + -z[95];
z[81] = z[21] * z[81];
z[137] = T(3) * z[11];
z[98] = -z[28] + z[98];
z[142] = -(z[98] * z[137]);
z[81] = z[81] + z[94] + z[142];
z[81] = z[9] * z[81];
z[142] = T(8) * z[54] + -z[56];
z[142] = z[56] * z[142];
z[142] = T(-12) * z[57] + T(-7) * z[86] + z[142];
z[97] = -z[97] + z[108];
z[97] = z[43] * z[97];
z[145] = -z[28] + z[108];
z[146] = -(z[127] * z[145]);
z[97] = z[97] + T(2) * z[142] + z[146];
z[97] = z[37] * z[97];
z[142] = -z[6] + z[100];
z[65] = z[65] + -z[142];
z[146] = z[1] + -z[38];
z[147] = -z[10] + z[146];
z[148] = -(z[65] * z[147]);
z[65] = -z[65] + z[128];
z[65] = z[9] * z[65];
z[65] = z[65] + -z[130] + z[148];
z[128] = T(2) * z[23];
z[65] = z[65] * z[128];
z[130] = z[68] + z[106];
z[79] = -z[79] + -z[95] + z[130];
z[69] = z[69] + -z[79];
z[69] = z[35] * z[69];
z[95] = -z[101] + -z[104];
z[95] = z[37] * z[95];
z[69] = z[69] + z[95] + -z[140];
z[69] = z[42] * z[69];
z[79] = z[42] * z[79];
z[95] = T(5) * z[43];
z[101] = -z[95] + z[127];
z[101] = z[98] * z[101];
z[104] = T(16) * z[54] + T(-5) * z[56];
z[104] = z[56] * z[104];
z[79] = T(-16) * z[57] + z[79] + T(-12) * z[86] + -z[101] + z[104];
z[79] = z[36] * z[79];
z[104] = -z[35] + z[37];
z[127] = -z[26] + z[28] + z[100];
z[104] = z[104] * z[127];
z[127] = -z[131] + z[141];
z[127] = z[41] * z[127];
z[140] = -z[22] + z[28];
z[140] = z[36] * z[140];
z[104] = z[104] + z[127] + -z[139] + z[140];
z[104] = z[49] * z[104];
z[127] = -z[35] + z[36];
z[98] = -(z[98] * z[127]);
z[131] = z[41] * z[131];
z[139] = z[37] * z[145];
z[131] = -z[98] + z[131] + z[139];
z[139] = z[46] + z[47] + z[48];
z[140] = T(3) * z[139];
z[131] = -(z[131] * z[140]);
z[141] = T(-2) * z[54] + z[56];
z[141] = z[56] * z[141];
z[141] = z[86] + z[141];
z[101] = T(2) * z[57] + z[101] + T(5) * z[141];
z[101] = z[35] * z[101];
z[141] = T(2) * z[4];
z[145] = z[28] + z[108] + -z[141];
z[145] = z[37] * z[145];
z[148] = T(2) * z[121];
z[149] = -z[19] + z[148];
z[150] = -z[28] + z[149];
z[150] = z[41] * z[150];
z[98] = z[98] + z[145] + z[150];
z[98] = z[44] * z[98];
z[145] = T(2) * z[20];
z[145] = z[145] * z[146];
z[150] = z[6] + -z[22];
z[151] = z[3] + z[150];
z[82] = -z[82] + T(4) * z[151];
z[82] = -(z[82] * z[145]);
z[151] = z[15] + z[16];
z[152] = z[17] + z[151];
z[153] = (T(4) * z[36]) / T(3);
z[154] = (T(-106) * z[11]) / T(15) + z[21] + (T(149) * z[52]) / T(15);
z[154] = (T(-451) * z[12]) / T(270) + (T(-391) * z[13]) / T(270) + (T(11) * z[14]) / T(90) + (T(31) * z[18]) / T(45) + z[35] + (T(19) * z[37]) / T(6) + (T(11) * z[41]) / T(6) + (T(31) * z[152]) / T(90) + z[153] + (T(2) * z[154]) / T(9);
z[155] = prod_pow(z[7], 2);
z[154] = z[154] * z[155];
z[156] = (T(-43) * z[12]) / T(30) + (T(34) * z[52]) / T(15);
z[86] = z[86] * z[156];
z[94] = z[10] * z[94];
z[156] = z[1] * z[21];
z[110] = z[21] * z[110];
z[110] = z[110] + T(-13) * z[156];
z[110] = z[28] * z[110];
z[157] = z[50] * z[144];
z[138] = z[138] * z[157];
z[65] = z[65] + z[66] + z[67] + z[69] + z[71] + z[79] + z[80] + z[81] + z[82] + z[84] + z[85] + z[86] + z[87] + z[90] + z[91] + z[94] + z[97] + T(4) * z[98] + z[101] + T(3) * z[104] + z[109] + z[110] + z[115] + z[131] + z[132] + z[138] + z[154];
z[65] = z[7] * z[65];
z[66] = z[12] + z[13];
z[67] = z[11] + -z[52];
z[67] = T(3) * z[66] + T(4) * z[67] + -z[151];
z[69] = z[53] * z[54];
z[67] = z[67] * z[69];
z[71] = T(13) * z[11] + T(-17) * z[52];
z[71] = T(43) * z[66] + T(4) * z[71] + T(-9) * z[151];
z[71] = z[55] * z[71];
z[79] = z[14] + z[17];
z[79] = T(-6) * z[18] + T(-3) * z[79];
z[69] = (T(12) * z[55]) / T(5) + z[69];
z[69] = z[69] * z[79];
z[67] = T(3) * z[67] + z[69] + (T(4) * z[71]) / T(5);
z[65] = z[65] + T(2) * z[67];
z[65] = int_to_imaginary<T>(1) * z[65];
z[67] = (T(11) * z[1]) / T(3);
z[69] = z[67] + -z[88];
z[69] = z[11] * z[69];
z[71] = z[10] + -z[144];
z[71] = z[21] * z[71];
z[79] = z[11] + (T(7) * z[21]) / T(3);
z[79] = z[9] * z[79];
z[80] = z[12] * z[38];
z[81] = -z[35] + -z[37];
z[81] = z[39] * z[81];
z[69] = -z[69] + -z[71] + -z[79] + z[80] + -z[81];
z[71] = (T(4) * z[38]) / T(3);
z[79] = z[10] / T(2);
z[67] = (T(23) * z[9]) / T(6) + z[67] + -z[71] + z[79];
z[67] = z[15] * z[67];
z[80] = z[9] + -z[38];
z[80] = (T(-7) * z[10]) / T(2) + T(-5) * z[80] + -z[124];
z[80] = z[17] * z[80];
z[80] = -z[80] + z[156];
z[81] = z[36] + z[37];
z[82] = -z[35] + z[81];
z[82] = -z[41] + z[82] / T(2);
z[82] = z[82] * z[139];
z[84] = T(11) * z[56];
z[85] = T(13) * z[54] + -z[84] + z[95];
z[85] = (T(5) * z[42]) / T(3) + T(-2) * z[45] + z[85] / T(3);
z[85] = z[41] * z[85];
z[86] = T(2) * z[44];
z[87] = -z[37] + (T(-2) * z[41]) / T(3) + z[127] / T(3);
z[87] = z[86] * z[87];
z[90] = (T(5) * z[35]) / T(3) + -z[37];
z[90] = (T(8) * z[41]) / T(3) + z[90] / T(2) + -z[153];
z[90] = z[40] * z[90];
z[91] = (T(5) * z[43]) / T(3) + T(11) * z[54] + T(-15) * z[56];
z[91] = -z[45] + z[91] / T(2);
z[91] = z[35] * z[91];
z[84] = (T(-5) * z[54]) / T(2) + z[84];
z[84] = z[43] / T(2) + z[45] + z[84] / T(3);
z[84] = z[37] * z[84];
z[71] = z[1] / T(2) + (T(-13) * z[9]) / T(6) + (T(-3) * z[10]) / T(2) + -z[71];
z[71] = z[13] * z[71];
z[79] = (T(3) * z[1]) / T(2) + -z[38] + -z[79];
z[79] = z[16] * z[79];
z[94] = -z[9] + z[129];
z[94] = z[23] * z[94];
z[97] = (T(4) * z[35]) / T(3) + z[37];
z[97] = z[42] * z[97];
z[98] = T(-19) * z[54] + T(41) * z[56] + -z[95];
z[98] = (T(-5) * z[42]) / T(6) + z[45] + z[98] / T(6);
z[98] = z[36] * z[98];
z[101] = T(2) * z[1];
z[104] = (T(-17) * z[9]) / T(6) + (T(11) * z[38]) / T(6) + -z[101];
z[104] = z[14] * z[104];
z[109] = (T(5) * z[1]) / T(2) + (T(7) * z[9]) / T(6) + (T(-2) * z[38]) / T(3);
z[109] = z[18] * z[109];
z[67] = z[67] + -z[69] / T(2) + z[71] + z[79] + -z[80] / T(3) + z[82] + z[84] + z[85] + z[87] + z[90] + z[91] + (T(8) * z[94]) / T(3) + z[97] + z[98] + z[104] + z[109] + -z[145];
z[67] = z[7] * z[67];
z[69] = z[13] + z[152];
z[69] = z[12] + z[14] + T(-4) * z[18] + T(-3) * z[21] + T(8) * z[52] + T(-2) * z[69] + -z[137];
z[69] = z[53] * z[69];
z[67] = z[67] + T(2) * z[69];
z[67] = z[7] * z[67];
z[69] = prod_pow(z[22], 2);
z[71] = prod_pow(z[2], 2);
z[79] = z[69] + -z[71];
z[80] = T(4) * z[150];
z[82] = -z[3] + z[80];
z[82] = z[3] * z[82];
z[84] = T(6) * z[29];
z[82] = z[82] + -z[84];
z[85] = T(2) * z[19];
z[87] = z[6] + z[85];
z[90] = z[72] + z[87];
z[90] = z[19] * z[90];
z[91] = T(6) * z[33];
z[94] = T(2) * z[27];
z[97] = z[91] + z[94];
z[98] = z[22] + z[72];
z[104] = (T(3) * z[6]) / T(2);
z[109] = z[98] + -z[104];
z[109] = z[6] * z[109];
z[110] = T(3) * z[25];
z[109] = z[109] + -z[110];
z[115] = z[77] + -z[78];
z[124] = z[5] / T(2);
z[127] = -z[6] + -z[115] + z[124];
z[127] = z[76] * z[127];
z[131] = -z[4] + z[120];
z[132] = T(3) * z[22];
z[138] = -z[131] + z[132];
z[138] = z[4] * z[138];
z[139] = T(5) * z[31];
z[144] = T(3) * z[24];
z[127] = -z[79] + z[82] + z[90] + z[97] + -z[109] + z[127] + z[138] + z[139] + z[144];
z[127] = z[38] * z[127];
z[138] = -z[2] + -z[117];
z[138] = z[61] + z[76] + -z[78] + T(2) * z[138];
z[138] = z[70] * z[138];
z[145] = (T(3) * z[3]) / T(2);
z[151] = -z[111] + z[133] + z[145];
z[151] = z[3] * z[151];
z[152] = T(3) * z[29];
z[153] = T(3) * z[33];
z[154] = z[152] + z[153];
z[151] = z[151] + z[154];
z[158] = T(2) * z[6];
z[159] = z[132] + z[158];
z[160] = -z[70] + -z[123] + z[159];
z[161] = -z[4] + -z[120] + z[160];
z[161] = z[4] * z[161];
z[162] = T(2) * z[8];
z[163] = -z[94] + z[162];
z[119] = -z[119] + z[158];
z[119] = z[6] * z[119];
z[164] = T(11) * z[31];
z[165] = T(2) * z[69];
z[166] = z[71] / T(2);
z[90] = z[90] + z[119] + z[138] + z[151] + z[161] + -z[163] + z[164] + z[165] + z[166];
z[90] = z[10] * z[90];
z[119] = -z[61] + -z[92] + z[145];
z[119] = z[3] * z[119];
z[138] = -z[61] + -z[96] + T(-6) * z[100];
z[138] = z[19] * z[138];
z[161] = T(3) * z[69] + z[164];
z[167] = z[94] + z[162];
z[131] = T(-3) * z[121] + z[131];
z[131] = z[4] * z[131];
z[168] = z[6] * z[93];
z[169] = T(5) * z[168];
z[170] = T(11) * z[29] + -z[153];
z[171] = (T(3) * z[71]) / T(2);
z[172] = z[5] * z[121];
z[119] = z[119] + z[131] + z[138] + -z[161] + -z[167] + z[169] + z[170] + z[171] + T(6) * z[172];
z[119] = z[1] * z[119];
z[131] = z[70] * z[121];
z[138] = -z[69] + z[131] + -z[167];
z[173] = T(3) * z[32];
z[174] = -z[168] + z[173];
z[175] = T(3) * z[31];
z[176] = -z[152] + z[175];
z[118] = -z[85] + -z[118];
z[118] = z[19] * z[118];
z[177] = z[3] / T(2);
z[178] = -z[2] + z[177];
z[179] = -z[6] + -z[178];
z[179] = z[3] * z[179];
z[180] = -z[4] + z[19] + z[121];
z[181] = z[4] * z[180];
z[118] = z[118] + z[138] + -z[153] + -z[166] + -z[174] + -z[176] + z[179] + z[181];
z[118] = z[9] * z[118];
z[179] = z[1] + z[88];
z[179] = z[173] * z[179];
z[90] = z[90] + z[118] + z[119] + z[127] + -z[179];
z[90] = z[18] * z[90];
z[118] = z[5] + -z[6];
z[119] = z[118] + -z[123];
z[127] = z[4] / T(2);
z[75] = z[75] + -z[119] + z[127];
z[75] = z[4] * z[75];
z[181] = T(5) * z[8];
z[182] = -z[153] + z[181];
z[83] = -z[83] + -z[177];
z[83] = z[3] * z[83];
z[183] = -z[72] + z[78];
z[184] = z[5] + z[6];
z[185] = -z[183] + z[184];
z[185] = z[5] * z[185];
z[186] = T(3) * z[27];
z[187] = T(6) * z[25];
z[188] = z[6] + -z[72];
z[188] = z[6] * z[188];
z[189] = -(z[19] * z[111]);
z[75] = -z[71] + z[75] + z[83] + z[152] + z[182] + z[185] + -z[186] + z[187] + z[188] + z[189];
z[75] = z[9] * z[75];
z[83] = z[105] + -z[123];
z[185] = z[76] + -z[111];
z[188] = (T(5) * z[4]) / T(2);
z[189] = -z[19] + -z[83] + z[185] + z[188];
z[189] = z[4] * z[189];
z[190] = prod_pow(z[6], 2);
z[191] = -z[71] + z[190];
z[192] = T(3) * z[8];
z[193] = -z[187] + z[192];
z[194] = -z[2] + z[105];
z[195] = z[145] + z[194];
z[196] = -z[61] + z[195];
z[196] = z[3] * z[196];
z[197] = T(7) * z[5];
z[198] = z[61] + z[197];
z[199] = z[2] + -z[22];
z[200] = T(-10) * z[3] + z[198] + T(4) * z[199];
z[200] = z[5] * z[200];
z[201] = T(4) * z[93];
z[202] = -z[111] + z[201];
z[203] = -z[89] + z[202];
z[204] = z[19] * z[203];
z[205] = T(5) * z[27];
z[189] = z[170] + z[189] + T(3) * z[191] + -z[193] + z[196] + z[200] + z[204] + z[205];
z[189] = z[1] * z[189];
z[61] = -z[61] + z[107];
z[196] = z[61] + z[188];
z[200] = T(9) * z[19];
z[204] = z[105] + z[123] + -z[196] + z[200];
z[204] = z[4] * z[204];
z[87] = z[87] + -z[89] + T(4) * z[143];
z[87] = z[19] * z[87];
z[89] = T(13) * z[27];
z[87] = z[87] + z[89];
z[143] = z[6] + z[77];
z[143] = -(z[143] * z[158]);
z[203] = z[62] + z[203];
z[203] = z[5] * z[203];
z[206] = T(13) * z[8];
z[143] = T(3) * z[71] + -z[87] + z[143] + -z[151] + z[203] + z[204] + z[206];
z[143] = z[10] * z[143];
z[151] = z[110] + (T(3) * z[190]) / T(2);
z[203] = T(4) * z[71];
z[204] = z[151] + -z[203];
z[207] = T(2) * z[150];
z[208] = T(3) * z[3];
z[209] = -z[207] + z[208];
z[209] = -z[107] + T(2) * z[209];
z[209] = z[5] * z[209];
z[210] = T(4) * z[142];
z[200] = (T(-9) * z[4]) / T(2) + z[200] + -z[210];
z[200] = z[4] * z[200];
z[211] = T(8) * z[8];
z[82] = z[82] + -z[87] + z[200] + -z[204] + z[209] + z[211];
z[82] = z[38] * z[82];
z[75] = z[75] + z[82] + z[143] + z[189];
z[75] = z[15] * z[75];
z[82] = T(7) * z[3];
z[87] = z[2] + -z[82] + z[102] + z[107] + -z[132];
z[87] = z[5] * z[87];
z[107] = T(8) * z[19];
z[143] = (T(15) * z[4]) / T(2) + -z[107] + -z[160];
z[143] = z[4] * z[143];
z[73] = z[73] + z[132];
z[73] = z[19] * z[73];
z[160] = T(11) * z[27];
z[73] = z[73] + z[160];
z[189] = -(z[6] * z[202]);
z[195] = z[3] * z[195];
z[200] = z[69] / T(2);
z[87] = (T(-7) * z[71]) / T(2) + z[73] + z[87] + z[143] + z[152] + z[175] + z[189] + -z[193] + z[195] + -z[200];
z[87] = z[10] * z[87];
z[143] = z[85] + -z[100] + z[102];
z[143] = z[19] * z[143];
z[189] = z[166] + z[181];
z[193] = z[172] + -z[200];
z[195] = z[102] + z[178];
z[195] = z[3] * z[195];
z[202] = -z[96] + z[188];
z[209] = -z[121] + z[202];
z[209] = z[4] * z[209];
z[143] = z[143] + T(-4) * z[168] + z[176] + z[189] + z[193] + z[195] + z[205] + z[209];
z[143] = z[9] * z[143];
z[195] = z[144] + z[211];
z[209] = z[31] + z[200];
z[204] = -z[195] + z[204] + T(5) * z[209];
z[212] = (T(11) * z[4]) / T(2);
z[107] = -z[107] + T(3) * z[142] + z[212];
z[107] = z[4] * z[107];
z[213] = z[3] + -z[117];
z[213] = z[62] * z[213];
z[214] = -z[22] + z[124];
z[76] = -(z[76] * z[214]);
z[215] = T(8) * z[29];
z[73] = z[73] + z[76] + z[107] + z[204] + z[213] + z[215];
z[73] = z[38] * z[73];
z[76] = z[121] + -z[127];
z[76] = z[4] * z[76];
z[107] = z[19] * z[100];
z[213] = z[31] + z[107];
z[178] = z[3] * z[178];
z[166] = z[8] + z[166];
z[216] = z[27] + z[29];
z[76] = -z[76] + z[166] + z[178] + z[193] + -z[213] + z[216];
z[99] = -(z[76] * z[99]);
z[73] = z[73] + z[87] + z[99] + z[143];
z[73] = z[13] * z[73];
z[87] = z[62] * z[121];
z[87] = z[87] + -z[215];
z[99] = (T(-7) * z[5]) / T(2) + -z[6] + z[103];
z[99] = z[5] * z[99];
z[143] = -z[4] + T(2) * z[119];
z[143] = z[141] * z[143];
z[217] = T(2) * z[71];
z[98] = z[6] * z[98];
z[98] = z[87] + z[98] + z[99] + -z[139] + z[143] + -z[195] + -z[200] + -z[217];
z[98] = z[38] * z[98];
z[61] = -z[61] + z[64] + -z[68] + z[125];
z[61] = z[4] * z[61];
z[99] = z[135] + z[185] + -z[208];
z[99] = z[5] * z[99];
z[143] = z[203] + z[206];
z[132] = z[132] + -z[135];
z[132] = z[6] * z[132];
z[135] = -z[68] + z[145];
z[135] = z[3] * z[135];
z[145] = T(3) * z[209];
z[206] = T(6) * z[24];
z[61] = z[61] + z[99] + z[132] + z[135] + z[143] + -z[145] + z[154] + z[206];
z[61] = z[1] * z[61];
z[99] = z[103] + -z[118] + z[141];
z[99] = z[4] * z[99];
z[132] = -z[152] + z[206];
z[135] = z[153] + z[181];
z[152] = -z[2] + z[159];
z[152] = z[6] * z[152];
z[154] = -z[2] + z[6];
z[159] = T(4) * z[154] + z[177];
z[159] = z[3] * z[159];
z[206] = T(7) * z[6] + -z[70];
z[218] = -z[123] + -z[206];
z[218] = z[5] * z[218];
z[99] = z[99] + z[132] + z[135] + -z[145] + z[152] + z[159] + z[217] + z[218];
z[99] = z[9] * z[99];
z[145] = z[8] + -z[33];
z[152] = z[4] * z[118];
z[159] = prod_pow(z[3], 2);
z[218] = -z[6] + z[123];
z[218] = z[5] * z[218];
z[218] = -z[29] + -z[145] + z[152] + -z[159] / T(2) + z[168] + -z[209] + z[218];
z[134] = z[134] * z[218];
z[61] = z[61] + z[98] + z[99] + z[134];
z[61] = z[14] * z[61];
z[98] = z[96] * z[108];
z[99] = -z[117] + -z[123];
z[99] = z[70] * z[99];
z[108] = -z[22] + z[85] + z[115];
z[108] = -z[4] + T(2) * z[108];
z[108] = z[4] * z[108];
z[115] = T(6) * z[32];
z[134] = T(2) * z[29];
z[218] = -z[3] + z[201];
z[218] = z[3] * z[218];
z[97] = T(3) * z[79] + -z[97] + -z[98] + z[99] + z[108] + z[115] + -z[134] + z[218];
z[97] = z[36] * z[97];
z[99] = z[131] + z[167];
z[108] = z[85] * z[100];
z[131] = -z[4] + z[148];
z[131] = z[4] * z[131];
z[148] = -z[3] + z[77];
z[167] = z[3] * z[148];
z[218] = T(2) * z[31];
z[108] = -z[79] + z[99] + -z[108] + -z[131] + z[134] + -z[167] + -z[218];
z[131] = z[35] * z[108];
z[167] = T(2) * z[168];
z[115] = z[115] + z[167];
z[162] = -z[71] + z[162];
z[219] = -z[117] + z[118];
z[219] = z[70] * z[219];
z[220] = z[85] + z[142];
z[220] = z[4] + T(2) * z[220];
z[220] = z[4] * z[220];
z[221] = z[31] + z[69];
z[219] = -z[94] + -z[98] + z[115] + z[162] + z[219] + z[220] + T(2) * z[221];
z[219] = z[37] * z[219];
z[220] = -z[6] + -z[148];
z[220] = z[78] * z[220];
z[221] = z[19] + z[142];
z[221] = z[85] * z[221];
z[149] = z[4] + T(-2) * z[149];
z[149] = z[4] * z[149];
z[91] = z[91] + z[115] + -z[138] + z[149] + z[217] + z[220] + z[221];
z[91] = z[41] * z[91];
z[91] = z[91] + z[97] + z[131] + z[219];
z[91] = z[86] * z[91];
z[97] = T(5) * z[3] + -z[136];
z[115] = -z[64] + -z[97] + -z[185];
z[115] = z[4] * z[115];
z[131] = (T(5) * z[71]) / T(2) + z[181];
z[136] = z[69] + z[218];
z[138] = (T(3) * z[5]) / T(2);
z[97] = z[97] + z[138];
z[97] = z[5] * z[97];
z[149] = T(5) * z[29];
z[181] = T(8) * z[27];
z[97] = z[97] + T(8) * z[107] + z[115] + -z[131] + T(4) * z[136] + z[144] + -z[149] + -z[151] + T(-5) * z[178] + -z[181];
z[97] = z[35] * z[97];
z[104] = z[104] + z[133];
z[104] = z[6] * z[104];
z[104] = z[104] + z[110];
z[110] = -z[5] + -z[207];
z[110] = z[70] * z[110];
z[115] = -z[19] + z[142];
z[133] = z[4] + T(2) * z[115];
z[133] = z[133] * z[141];
z[136] = T(-4) * z[31] + -z[69];
z[110] = z[98] + z[104] + z[110] + z[133] + T(2) * z[136] + z[181] + -z[189];
z[110] = z[37] * z[110];
z[133] = z[6] + z[183];
z[133] = z[3] * z[133];
z[133] = z[133] + z[135];
z[135] = -z[111] + z[138];
z[136] = -z[83] + z[135];
z[136] = z[5] * z[136];
z[151] = T(2) * z[142];
z[183] = -z[19] + z[151];
z[183] = z[85] * z[183];
z[180] = z[64] * z[180];
z[180] = z[180] + -z[181];
z[104] = T(2) * z[79] + z[104] + -z[133] + z[136] + z[144] + z[180] + z[183];
z[104] = z[41] * z[104];
z[136] = z[5] * z[83];
z[181] = (T(5) * z[3]) / T(2) + -z[201];
z[181] = z[3] * z[181];
z[136] = z[136] + z[149] + z[153] + z[181];
z[149] = z[98] + z[136] + z[171] + -z[180];
z[149] = z[36] * z[149];
z[180] = prod_pow(z[5], 2);
z[180] = z[180] + -z[190];
z[181] = z[24] + -z[25];
z[152] = -z[152] + z[180] / T(2) + z[181];
z[180] = z[51] * z[152];
z[207] = T(3) * z[180];
z[97] = z[97] + z[104] + z[110] + z[149] + z[207];
z[97] = z[40] * z[97];
z[104] = z[92] + z[106] + -z[111];
z[104] = z[6] * z[104];
z[77] = z[77] + z[106];
z[77] = z[62] + T(2) * z[77] + -z[198];
z[77] = z[5] * z[77];
z[106] = -z[22] + z[103];
z[110] = (T(-3) * z[4]) / T(2) + z[96] + z[106] + -z[185];
z[110] = z[4] * z[110];
z[93] = -(z[62] * z[93]);
z[149] = -z[2] + -z[100];
z[149] = z[96] * z[149];
z[77] = z[77] + z[93] + z[104] + z[110] + z[131] + z[149] + -z[161] + -z[186] + -z[187];
z[77] = z[1] * z[77];
z[93] = z[69] + z[173] + z[175];
z[104] = z[92] + -z[150];
z[104] = z[6] * z[104];
z[104] = z[104] + -z[187] + -z[192];
z[110] = z[127] + z[142];
z[131] = -z[96] + z[110];
z[131] = z[4] * z[131];
z[149] = z[117] + -z[184];
z[149] = z[5] * z[149];
z[149] = -z[93] + z[104] + z[131] + z[149] + -z[171] + -z[183] + z[205];
z[149] = z[9] * z[149];
z[116] = -z[105] + z[116];
z[116] = z[5] * z[116];
z[161] = z[98] + z[205];
z[116] = -z[116] + z[131] + -z[139] + z[161] + -z[165];
z[131] = z[109] + z[116] + -z[211] + -z[217];
z[171] = -(z[38] * z[131]);
z[116] = z[116] + -z[189];
z[175] = -z[116] + -z[168];
z[175] = z[10] * z[175];
z[129] = z[129] * z[173];
z[77] = z[77] + z[129] + z[149] + z[171] + z[175];
z[77] = z[16] * z[77];
z[149] = z[72] + -z[113] + z[197];
z[149] = z[19] * z[149];
z[171] = T(7) * z[2];
z[82] = -z[82] + z[105] + z[171];
z[175] = z[82] + -z[96];
z[175] = z[4] * z[175];
z[171] = -z[105] + z[171];
z[171] = z[3] * z[171];
z[103] = -z[103] + z[113];
z[113] = -(z[5] * z[103]);
z[113] = T(-11) * z[8] + T(7) * z[79] + z[113] + z[149] + -z[153] + z[164] + z[171] + z[175];
z[113] = z[1] * z[113];
z[72] = z[72] + z[100];
z[72] = z[19] * z[72];
z[100] = z[5] * z[106];
z[72] = z[72] + z[100] + z[139];
z[100] = -(z[62] * z[199]);
z[121] = z[19] + -z[121];
z[64] = z[64] * z[121];
z[64] = z[64] + -z[69] + -z[72] + z[100] + z[203] + z[211];
z[64] = z[38] * z[64];
z[83] = -z[83] + z[96];
z[83] = z[4] * z[83];
z[100] = z[3] * z[194];
z[72] = -z[72] + -z[79] + z[83] + z[100] + z[182];
z[72] = z[10] * z[72];
z[83] = z[2] * z[3];
z[100] = -(z[5] * z[22]);
z[121] = -(z[4] * z[123]);
z[100] = -z[8] + z[32] + -z[33] + z[79] + z[83] + z[100] + z[121] + z[213];
z[100] = z[9] * z[100];
z[64] = z[64] + z[72] + T(3) * z[100] + z[113] + z[179];
z[64] = z[12] * z[64];
z[72] = -z[96] + T(5) * z[142];
z[72] = z[19] * z[72];
z[72] = z[72] + -z[89];
z[63] = z[63] + -z[68];
z[63] = z[3] * z[63];
z[68] = (T(13) * z[4]) / T(2) + -z[122];
z[68] = z[4] * z[68];
z[63] = z[63] + z[68] + (T(-5) * z[69]) / T(2) + -z[72] + z[143] + z[153] + -z[169] + T(5) * z[172];
z[63] = z[1] * z[63];
z[68] = -z[5] + -z[22] + -z[92] + z[158] + z[208];
z[68] = z[5] * z[68];
z[89] = z[85] + -z[142];
z[89] = z[19] * z[89];
z[92] = z[89] + z[205];
z[100] = -z[19] + z[127];
z[113] = -z[100] + -z[210];
z[113] = z[4] * z[113];
z[111] = -(z[3] * z[111]);
z[68] = z[68] + -z[92] + z[104] + z[111] + z[113] + z[153] + z[200];
z[68] = z[10] * z[68];
z[104] = -z[106] + z[135];
z[104] = z[5] * z[104];
z[111] = z[3] + T(2) * z[154];
z[111] = z[78] * z[111];
z[104] = -z[104] + z[109] + -z[111] + -z[195] + z[200];
z[109] = z[126] + -z[188];
z[109] = z[4] * z[109];
z[109] = -z[92] + z[109] + -z[217];
z[111] = z[104] + z[109];
z[111] = z[38] * z[111];
z[109] = z[109] + -z[173];
z[113] = -z[168] + z[193];
z[121] = -z[109] + z[113] + z[133];
z[122] = z[9] * z[121];
z[63] = z[63] + z[68] + z[111] + z[122] + -z[129];
z[63] = z[17] * z[63];
z[68] = z[100] + -z[105] + z[118];
z[68] = z[4] * z[68];
z[68] = z[68] + z[92];
z[92] = -z[3] + T(2) * z[74];
z[92] = z[78] * z[92];
z[102] = -z[102] + -z[214];
z[102] = z[5] * z[102];
z[92] = -z[68] + T(-4) * z[69] + z[92] + z[102] + -z[139] + z[144] + -z[215];
z[92] = z[38] * z[92];
z[102] = -z[6] + z[177];
z[105] = z[102] * z[208];
z[68] = z[68] + z[105];
z[105] = z[6] + z[117];
z[105] = z[105] * z[158];
z[111] = z[5] + z[74];
z[111] = z[5] * z[111];
z[111] = -z[68] + -z[105] + z[111] + -z[165] + z[176];
z[111] = z[10] * z[111];
z[92] = z[92] + z[111];
z[92] = z[21] * z[92];
z[82] = -(z[5] * z[82]);
z[96] = z[96] + z[103] + -z[212];
z[96] = z[4] * z[96];
z[103] = (T(3) * z[79]) / T(2);
z[111] = (T(-11) * z[3]) / T(2) + z[201];
z[111] = z[3] * z[111];
z[82] = z[82] + z[96] + -z[98] + -z[103] + z[111] + -z[160] + -z[170];
z[82] = z[1] * z[82];
z[96] = z[70] * z[123];
z[98] = T(2) * z[33];
z[96] = z[96] + z[98] + -z[134];
z[111] = -z[71] + z[159];
z[122] = -z[22] + z[127];
z[122] = z[4] * z[122];
z[126] = z[122] + z[200];
z[127] = z[27] + -z[96] + z[111] + z[126];
z[127] = z[38] * z[127];
z[79] = z[79] + z[159];
z[133] = z[5] * z[123];
z[133] = z[33] + z[133];
z[122] = -z[79] / T(2) + -z[122] + z[133] + -z[216];
z[134] = -(z[10] * z[122]);
z[127] = z[127] + z[134];
z[82] = z[82] + T(3) * z[127] + z[129];
z[82] = z[11] * z[82];
z[127] = T(3) * z[45];
z[108] = z[108] * z[127];
z[95] = -(z[76] * z[95]);
z[129] = prod_pow(z[56], 3);
z[134] = T(8) * z[129];
z[135] = z[54] * z[57];
z[139] = prod_pow(z[54], 3);
z[95] = T(20) * z[58] + (T(-217) * z[60]) / T(4) + z[95] + z[108] + -z[134] + T(14) * z[135] + (T(11) * z[139]) / T(3);
z[95] = z[35] * z[95];
z[108] = -z[125] + z[130];
z[125] = z[108] + z[185] + -z[188];
z[125] = z[4] * z[125];
z[62] = z[62] * z[148];
z[108] = -z[108] + -z[138];
z[108] = z[5] * z[108];
z[62] = z[62] + T(5) * z[107] + z[108] + z[125] + z[204] + -z[205] + -z[215];
z[62] = z[35] * z[62];
z[108] = z[131] + -z[173];
z[108] = z[37] * z[108];
z[62] = z[62] + z[108] + -z[207];
z[62] = z[42] * z[62];
z[108] = T(2) * z[32];
z[125] = z[108] + z[167];
z[130] = -(z[85] * z[142]);
z[78] = z[6] * z[78];
z[85] = z[4] + -z[85];
z[85] = z[4] * z[85];
z[78] = -z[69] + z[78] + z[85] + -z[98] + z[99] + -z[125] + z[130];
z[78] = z[78] * z[127];
z[85] = z[104] + z[109];
z[85] = z[42] * z[85];
z[98] = -(z[43] * z[121]);
z[99] = -z[134] + T(5) * z[139];
z[104] = T(4) * z[58];
z[109] = T(2) * z[59];
z[78] = (T(-21) * z[60]) / T(2) + z[78] + z[85] + z[98] + z[99] / T(3) + z[104] + z[109] + T(6) * z[135];
z[78] = z[41] * z[78];
z[76] = z[35] * z[76];
z[85] = z[19] * z[142];
z[98] = z[4] * z[100];
z[85] = -z[27] + z[32] + z[85] + -z[98];
z[98] = z[3] * z[6];
z[98] = z[98] + z[145];
z[99] = z[85] + -z[98] + -z[113];
z[99] = z[41] * z[99];
z[100] = -z[32] + z[122];
z[113] = z[36] * z[100];
z[110] = z[4] * z[110];
z[121] = z[5] * z[6];
z[122] = z[27] + z[32];
z[121] = z[31] + -z[110] + z[121] + -z[122];
z[130] = z[121] + z[166] + -z[168];
z[130] = z[37] * z[130];
z[76] = z[76] + -z[99] + z[113] + z[130];
z[76] = z[76] * z[140];
z[99] = z[22] + z[206];
z[99] = z[5] * z[99];
z[68] = z[68] + z[93] + z[99] + -z[105] + -z[132];
z[68] = z[21] * z[68];
z[93] = z[119] + -z[141];
z[93] = z[4] * z[93];
z[99] = -(z[2] * z[118]);
z[83] = z[83] + z[93] + z[99] + -z[162];
z[93] = T(2) * z[0];
z[83] = z[83] * z[93];
z[93] = z[100] * z[137];
z[68] = z[68] + z[83] + z[93];
z[68] = z[9] * z[68];
z[70] = z[6] * z[70];
z[93] = -z[4] + -z[151];
z[93] = z[4] * z[93];
z[70] = z[70] + z[71] + z[93] + -z[125] + z[163] + z[218];
z[70] = z[70] * z[127];
z[71] = z[116] + -z[174];
z[71] = z[43] * z[71];
z[93] = z[134] + z[139];
z[99] = T(2) * z[135];
z[70] = T(-12) * z[58] + (T(17) * z[60]) / T(18) + z[70] + z[71] + z[93] / T(3) + -z[99];
z[70] = z[37] * z[70];
z[71] = -z[4] + z[117];
z[71] = z[4] * z[71];
z[71] = z[71] + -z[79] + -z[94] + z[96] + -z[108];
z[71] = z[71] * z[127];
z[79] = z[106] + z[202];
z[79] = z[4] * z[79];
z[79] = z[79] + z[161] + -z[173];
z[93] = z[79] + -z[103] + z[136];
z[93] = z[43] * z[93];
z[69] = (T(-3) * z[69]) / T(2) + z[79] + -z[87] + T(4) * z[172];
z[69] = z[42] * z[69];
z[79] = T(4) * z[129] + -z[139];
z[79] = -z[59] + z[79] / T(3) + -z[99] + -z[104];
z[69] = (T(349) * z[60]) / T(36) + z[69] + z[71] + T(4) * z[79] + z[93];
z[69] = z[36] * z[69];
z[71] = -z[123] + z[124];
z[79] = z[5] * z[71];
z[87] = -(z[4] * z[119]);
z[79] = z[8] + z[29] + z[79] + z[87] + z[178] + z[181] + -z[191] / T(2);
z[79] = z[35] * z[79];
z[71] = -z[6] + z[71];
z[71] = z[5] * z[71];
z[87] = z[6] / T(2);
z[93] = -z[2] + z[87];
z[93] = z[6] * z[93];
z[96] = z[24] + z[25];
z[71] = z[71] + z[93] + z[96] + z[98];
z[71] = z[41] * z[71];
z[93] = z[25] + z[93] + z[166];
z[93] = z[37] * z[93];
z[98] = -z[29] + -z[111] / T(2) + z[133];
z[98] = z[36] * z[98];
z[71] = z[71] + z[79] + z[93] + z[98] + z[180];
z[71] = z[71] * z[114];
z[79] = -(z[5] * z[214]);
z[79] = z[27] + z[79] + -z[107] + z[110] + -z[181] + z[190] / T(2) + -z[209];
z[79] = z[35] * z[79];
z[74] = z[74] + -z[124];
z[74] = z[5] * z[74];
z[87] = z[22] + z[87];
z[87] = z[6] * z[87];
z[74] = z[74] + -z[85] + -z[87] + -z[96] + -z[200];
z[74] = z[41] * z[74];
z[85] = -z[25] + -z[87] + z[121];
z[85] = z[37] * z[85];
z[87] = -z[122] + -z[126];
z[87] = z[36] * z[87];
z[74] = z[74] + z[79] + z[85] + z[87] + -z[180];
z[79] = z[36] / T(2) + z[37] + -z[41];
z[79] = z[79] * z[155];
z[74] = T(3) * z[74] + z[79];
z[74] = z[49] * z[74];
z[79] = -z[112] + -z[120] + z[196];
z[79] = z[4] * z[79];
z[85] = z[3] * z[102];
z[87] = -(z[5] * z[142]);
z[85] = -z[31] + z[85] + z[87];
z[72] = -z[72] + z[79] + T(3) * z[85] + -z[132];
z[72] = z[21] * z[72];
z[72] = z[72] + z[83];
z[72] = z[1] * z[72];
z[79] = z[10] * z[83];
z[83] = -z[4] + z[115];
z[83] = z[4] * z[83];
z[83] = z[83] + z[89] + z[94];
z[85] = -z[9] + -z[147];
z[83] = z[83] * z[85] * z[128];
z[85] = z[152] * z[157];
z[80] = z[80] + -z[208];
z[80] = T(2) * z[80] + z[197];
z[80] = z[5] * z[80];
z[87] = z[3] + T(8) * z[150];
z[87] = z[3] * z[87];
z[80] = z[80] + z[84] + -z[87];
z[80] = -(z[20] * z[80] * z[146]);
z[84] = -z[40] + -z[42] + -z[43] + z[86];
z[66] = -z[11] + T(2) * z[52] + -z[66];
z[66] = z[66] * z[84];
z[84] = -z[88] + z[101];
z[84] = z[35] * z[84];
z[81] = -(z[1] * z[81]);
z[66] = z[66] + z[81] + z[84];
z[66] = z[34] * z[66];
z[81] = -(z[21] * z[88]);
z[81] = z[81] + z[156];
z[81] = z[81] * z[173];
z[84] = T(3) * z[35] + -z[37];
z[84] = z[84] * z[109];
return z[61] + z[62] + z[63] + z[64] + z[65] + T(4) * z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[74] + z[75] + z[76] + z[77] + z[78] + z[79] + z[80] + z[81] + z[82] + z[83] + z[84] + z[85] + z[90] + z[91] + z[92] + z[95] + z[97];
}



template IntegrandConstructorType<double> f_4_334_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_334_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_334_construct (const Kin<qd_real>&);
#endif

}