#include "f_4_208.h"

namespace PentagonFunctions {

template <typename T> T f_4_208_abbreviated (const std::array<T,17>&);



template <typename T> IntegrandConstructorType<T> f_4_208_construct (const Kin<T>& kin) {
    return [&kin, 
            dl8 = DLog_W_8<T>(kin),dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,17> abbr = 
            {dl8(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[0] / kin_path.W[0]), rlog(-v_path[4]), rlog(kin.W[7] / kin_path.W[7]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[19] / kin_path.W[19]), dl3(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl1(t), dl5(t), dl20(t)}
;

        auto result = f_4_208_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_208_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[46];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[7];
z[12] = bc<TR>[1];
z[13] = bc<TR>[2];
z[14] = bc<TR>[4];
z[15] = bc<TR>[9];
z[16] = abb[11];
z[17] = abb[14];
z[18] = abb[15];
z[19] = abb[16];
z[20] = abb[12];
z[21] = abb[13];
z[22] = z[0] / T(2);
z[23] = (T(3) * z[16]) / T(2);
z[24] = z[18] + z[19];
z[25] = -z[22] + z[23] + z[24];
z[25] = -z[17] + z[25] / T(2);
z[25] = int_to_imaginary<T>(1) * z[3] * z[25];
z[26] = T(2) * z[8];
z[27] = z[9] + z[10];
z[28] = (T(5) * z[27]) / T(3);
z[29] = -z[7] + -z[28];
z[29] = (T(-2) * z[1]) / T(3) + z[26] + z[29] / T(2);
z[29] = z[18] * z[29];
z[30] = T(2) * z[7];
z[31] = (T(-5) * z[1]) / T(4) + (T(7) * z[8]) / T(4) + (T(3) * z[27]) / T(2) + -z[30];
z[31] = z[0] * z[31];
z[32] = (T(3) * z[8]) / T(2);
z[33] = -z[1] / T(3) + (T(5) * z[7]) / T(2) + (T(-2) * z[27]) / T(3) + -z[32];
z[33] = z[16] * z[33];
z[23] = (T(5) * z[0]) / T(2) + -z[23] + z[24];
z[23] = -z[17] + z[23] / T(2);
z[23] = z[13] * z[23];
z[34] = z[1] + -z[8];
z[35] = z[17] * z[34];
z[36] = -z[0] + z[16];
z[37] = T(2) * z[36];
z[38] = z[12] * z[37];
z[28] = (T(-11) * z[1]) / T(6) + (T(7) * z[8]) / T(2) + -z[28];
z[28] = z[19] * z[28];
z[23] = (T(3) * z[23]) / T(2) + z[25] + z[28] / T(2) + z[29] + z[31] + z[33] + (T(5) * z[35]) / T(2) + z[38];
z[23] = z[3] * z[23];
z[25] = z[8] / T(2);
z[28] = z[1] / T(2);
z[29] = z[25] + T(-3) * z[27] + z[28] + z[30];
z[29] = z[0] * z[29];
z[30] = T(2) * z[27];
z[31] = z[1] + z[30];
z[26] = z[7] + z[26] + -z[31];
z[33] = z[18] * z[26];
z[39] = z[27] + z[28];
z[40] = -z[7] + -z[25] + z[39];
z[41] = -(z[16] * z[40]);
z[42] = -z[8] + z[27];
z[42] = T(2) * z[42];
z[43] = -(z[19] * z[42]);
z[33] = -z[29] + z[33] + z[35] + z[41] + z[43];
z[33] = z[6] * z[33];
z[41] = -z[16] + z[18];
z[43] = -z[7] + z[8];
z[41] = z[41] * z[43];
z[44] = -(z[19] * z[34]);
z[41] = z[35] + z[41] + z[44];
z[41] = z[21] * z[41];
z[22] = -z[16] / T(2) + -z[22] + T(3) * z[24];
z[22] = -z[17] + z[22] / T(2);
z[22] = z[13] * z[22];
z[22] = z[22] + z[38];
z[22] = z[13] * z[22];
z[36] = -(prod_pow(z[12], 2) * z[36]);
z[37] = -(z[14] * z[37]);
z[22] = z[22] + z[33] + z[36] + z[37] + z[41];
z[33] = T(3) * z[8] + -z[31];
z[36] = z[24] * z[33];
z[37] = T(3) * z[7];
z[31] = -z[31] + z[37];
z[31] = z[16] * z[31];
z[30] = -z[7] + -z[8] + z[30];
z[38] = T(3) * z[0];
z[30] = z[30] * z[38];
z[41] = T(3) * z[17];
z[41] = z[34] * z[41];
z[30] = z[30] + z[31] + T(2) * z[36] + z[41];
z[31] = z[4] + z[11];
z[31] = z[30] * z[31];
z[44] = -z[32] + z[39];
z[45] = -z[16] + z[38];
z[44] = z[44] * z[45];
z[44] = z[36] + z[44];
z[45] = -(z[2] * z[44]);
z[22] = T(3) * z[22] + -z[31] + z[45];
z[22] = int_to_imaginary<T>(1) * z[22];
z[22] = z[22] + z[23];
z[22] = z[3] * z[22];
z[23] = z[5] * z[29];
z[29] = z[20] * z[34];
z[34] = z[5] * z[42];
z[29] = z[29] + z[34];
z[29] = z[19] * z[29];
z[26] = -(z[5] * z[26]);
z[34] = z[20] * z[43];
z[26] = z[26] + -z[34];
z[26] = z[18] * z[26];
z[40] = z[5] * z[40];
z[34] = z[34] + z[40];
z[34] = z[16] * z[34];
z[40] = z[5] + z[20];
z[35] = -(z[35] * z[40]);
z[23] = z[23] + z[26] + z[29] + z[34] + z[35];
z[25] = z[7] + -z[25] + -z[27] + z[28];
z[25] = z[25] * z[38];
z[26] = z[32] + -z[37] + z[39];
z[26] = z[16] * z[26];
z[25] = z[25] + z[26] + -z[36] + -z[41];
z[25] = z[2] * z[25];
z[25] = z[25] + z[31];
z[25] = z[2] * z[25];
z[26] = -(z[11] * z[30]);
z[28] = -(z[4] * z[44]);
z[26] = z[26] + z[28];
z[26] = z[4] * z[26];
z[28] = z[16] + -z[19];
z[28] = z[28] * z[33];
z[27] = -z[1] + z[27];
z[27] = z[18] * z[27];
z[27] = T(2) * z[27] + z[28];
z[27] = prod_pow(z[11], 2) * z[27];
z[24] = (T(-119) * z[0]) / T(2) + (T(-157) * z[16]) / T(6) + (T(23) * z[24]) / T(3);
z[24] = T(39) * z[17] + z[24] / T(2);
z[24] = z[15] * z[24];
return z[22] + T(3) * z[23] + z[24] / T(4) + z[25] + z[26] + z[27];
}



template IntegrandConstructorType<double> f_4_208_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_208_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_208_construct (const Kin<qd_real>&);
#endif

}