#include "f_4_269.h"

namespace PentagonFunctions {

template <typename T> T f_4_269_abbreviated (const std::array<T,18>&);

template <typename T> class SpDLog_f_4_269_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_269_W_7 (const Kin<T>& kin) {
        c[0] = (T(3) / T(2) + (T(3) * kin.v[1]) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) * kin.v[1] * kin.v[4]) / T(2) + (T(3) / T(2) + (T(-3) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = (T(3) * kin.v[1] * kin.v[4]) / T(2) + (T(3) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[4] + kin.v[3] * (T(3) / T(2) + (T(3) * kin.v[1]) / T(2) + (T(9) * kin.v[3]) / T(2) + T(9) * kin.v[4]) + rlog(-kin.v[1]) * (((T(-21) * kin.v[3]) / T(4) + (T(-21) * kin.v[4]) / T(2) + T(9) + T(9) * kin.v[1]) * kin.v[3] + ((T(-21) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + T(9) * kin.v[1] * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (((T(-21) * kin.v[3]) / T(4) + (T(-21) * kin.v[4]) / T(2) + T(9) + T(9) * kin.v[1]) * kin.v[3] + ((T(-21) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + T(9) * kin.v[1] * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(3) * kin.v[1] * kin.v[4]) / T(2) + (T(3) / T(2) + (T(3) * kin.v[1]) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) / T(2) + (T(-3) * kin.v[4]) / T(8)) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(21) * kin.v[3]) / T(4) + (T(21) * kin.v[4]) / T(2) + T(-9) + T(-9) * kin.v[1]) * kin.v[3] + ((T(21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + T(-9) * kin.v[1] * kin.v[4]);
c[2] = (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * rlog(-kin.v[1] + kin.v[3] + kin.v[4]) + rlog(-kin.v[1]) * (T(3) * kin.v[3] + T(3) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (T(3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (prod_pow(abb[2], 2) * ((abb[6] * T(-9)) / T(2) + (abb[7] * T(-3)) / T(4) + (abb[5] * T(3)) / T(4) + (abb[8] * T(9)) / T(2)) + abb[4] * ((prod_pow(abb[2], 2) * T(-9)) / T(2) + abb[3] * T(-3)) + abb[3] * ((abb[5] * T(-3)) / T(2) + (abb[7] * T(3)) / T(2) + abb[6] * T(-3) + abb[8] * T(3)) + abb[1] * (abb[1] * ((abb[5] * T(-9)) / T(4) + (abb[8] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2) + (abb[6] * T(3)) / T(2) + (abb[7] * T(9)) / T(4)) + abb[2] * abb[4] * T(3) + abb[2] * ((abb[7] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2) + abb[8] * T(-3) + abb[6] * T(3))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_269_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl19 = DLog_W_19<T>(kin),dl25 = DLog_W_25<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),spdl7 = SpDLog_f_4_269_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,18> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), rlog(kin.W[1] / kin_path.W[1]), -rlog(t), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl19(t), rlog(kin.W[24] / kin_path.W[24]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t), dl18(t), dl20(t)}
;

        auto result = f_4_269_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_269_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[40];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[9];
z[3] = abb[15];
z[4] = abb[16];
z[5] = abb[17];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[10];
z[11] = abb[2];
z[12] = abb[11];
z[13] = abb[12];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[13];
z[17] = abb[14];
z[18] = bc<TR>[2];
z[19] = bc<TR>[9];
z[20] = z[3] + z[4];
z[21] = -z[12] + z[20];
z[22] = z[2] + T(2) * z[21];
z[22] = z[11] * z[22];
z[23] = -z[5] + z[20];
z[24] = z[2] / T(2) + -z[23];
z[24] = z[0] * z[24];
z[25] = z[5] + -z[12];
z[26] = T(2) * z[25];
z[27] = z[13] * z[26];
z[22] = z[22] + z[24] + -z[27];
z[22] = z[0] * z[22];
z[24] = (T(3) * z[2]) / T(2) + z[21];
z[27] = prod_pow(z[11], 2);
z[24] = z[24] * z[27];
z[28] = prod_pow(z[13], 2);
z[29] = z[25] * z[28];
z[30] = z[2] + T(2) * z[23];
z[30] = z[15] * z[30];
z[26] = z[16] * z[26];
z[22] = -z[22] + z[24] + -z[26] + -z[29] + z[30];
z[24] = z[1] + z[7];
z[22] = z[22] * z[24];
z[26] = T(2) * z[4];
z[29] = T(2) * z[3] + z[26];
z[30] = T(3) * z[5];
z[31] = -z[12] + -z[29] + z[30];
z[31] = z[9] * z[31];
z[32] = z[5] + z[12];
z[29] = -z[29] + z[32];
z[29] = z[10] * z[29];
z[29] = -z[29] + z[31];
z[31] = -(z[13] * z[29]);
z[33] = -z[6] + z[8];
z[34] = z[2] * z[33];
z[35] = z[34] / T(2);
z[36] = z[9] * z[21];
z[37] = z[2] + z[21];
z[37] = z[10] * z[37];
z[37] = -z[35] + z[36] + z[37];
z[37] = z[11] * z[37];
z[38] = z[3] + -z[5];
z[39] = z[2] + -z[4] + z[38];
z[39] = z[10] * z[39];
z[38] = -z[4] + T(-3) * z[38];
z[38] = z[9] * z[38];
z[38] = z[38] + z[39];
z[33] = z[3] * z[33];
z[38] = -z[33] + -z[34] / T(4) + z[38] / T(2);
z[38] = z[0] * z[38];
z[31] = z[31] + z[37] + z[38];
z[31] = z[0] * z[31];
z[21] = T(-3) * z[2] + z[21];
z[21] = z[10] * z[21];
z[21] = z[21] + (T(7) * z[34]) / T(2) + T(-3) * z[36];
z[21] = z[21] * z[27];
z[27] = -z[2] + z[23];
z[27] = z[10] * z[27];
z[23] = z[9] * z[23];
z[23] = T(-3) * z[23] + z[27] + z[35];
z[23] = z[15] * z[23];
z[27] = z[16] * z[29];
z[35] = z[4] + -z[25] / T(2);
z[35] = z[10] * z[35];
z[36] = -z[4] + (T(3) * z[25]) / T(2);
z[36] = z[9] * z[36];
z[35] = z[35] + z[36];
z[28] = z[28] * z[35];
z[35] = T(-25) * z[5] + T(7) * z[12];
z[35] = T(9) * z[20] + z[35] / T(2);
z[35] = z[19] * z[35];
z[21] = z[21] / T(2) + z[22] + z[23] + z[27] + z[28] + z[31] + z[35] / T(4);
z[22] = T(2) * z[12];
z[23] = -z[5] + -z[20] + z[22];
z[23] = -(z[23] * z[24]);
z[27] = z[33] + -z[34];
z[22] = (T(3) * z[3]) / T(2) + z[5] / T(2) + -z[22] + -z[26];
z[22] = z[10] * z[22];
z[26] = T(5) * z[5] + T(-3) * z[12];
z[26] = -z[20] + z[26] / T(2);
z[26] = z[18] * z[26];
z[28] = (T(-5) * z[3]) / T(2) + z[4] + (T(-3) * z[5]) / T(2) + T(4) * z[12];
z[28] = z[9] * z[28];
z[22] = z[22] + z[23] + (T(3) * z[26]) / T(2) + z[27] / T(4) + z[28];
z[23] = prod_pow(z[14], 2);
z[22] = z[22] * z[23];
z[24] = T(-2) * z[24];
z[24] = z[24] * z[25];
z[24] = z[24] + z[29];
z[25] = z[0] + -z[17];
z[24] = z[24] * z[25];
z[25] = -z[9] + z[10];
z[26] = z[3] + -z[12];
z[25] = z[13] * z[25] * z[26];
z[26] = -z[20] + z[32] / T(2);
z[26] = prod_pow(z[18], 2) * z[26];
z[24] = z[24] + T(2) * z[25] + z[26];
z[25] = T(5) * z[12] + -z[30];
z[20] = -z[20] + z[25] / T(2);
z[20] = z[20] * z[23];
z[20] = z[20] / T(2) + T(3) * z[24];
z[20] = int_to_imaginary<T>(1) * z[14] * z[20];
return z[20] + T(3) * z[21] + z[22];
}



template IntegrandConstructorType<double> f_4_269_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_269_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_269_construct (const Kin<qd_real>&);
#endif

}