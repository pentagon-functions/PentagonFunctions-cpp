#include "f_4_210.h"

namespace PentagonFunctions {

template <typename T> T f_4_210_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_210_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_210_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + ((T(21) * kin.v[1]) / T(4) + (T(9) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-9) + T(6) * kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(9) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[3];
c[1] = kin.v[2] * (T(-9) + T(-3) * kin.v[3]) + kin.v[1] * (T(-9) + T(6) * kin.v[0] + T(6) * kin.v[1] + T(6) * kin.v[2] + T(-9) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + T(9) * kin.v[4] + kin.v[3] * (T(9) + T(3) * kin.v[3] + T(3) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(9) * kin.v[4]) / T(2) + T(6)) * kin.v[4] + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(-6) + T(-3) * kin.v[3] + T(-9) * kin.v[4]) + kin.v[1] * ((T(9) * kin.v[1]) / T(2) + T(-6) + T(9) * kin.v[2] + T(-3) * kin.v[3] + T(-9) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(6) + T(3) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(9) * kin.v[4]) / T(2) + T(6)) * kin.v[4] + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(-6) + T(-3) * kin.v[3] + T(-9) * kin.v[4]) + kin.v[1] * ((T(9) * kin.v[1]) / T(2) + T(-6) + T(9) * kin.v[2] + T(-3) * kin.v[3] + T(-9) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(6) + T(3) * kin.v[4])) + rlog(kin.v[3]) * (((T(3) * kin.v[1]) / T(4) + (T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-3) + T(6) * kin.v[0]) * kin.v[1] + ((T(-21) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(21) * kin.v[4]) / T(2) + T(-3)) * kin.v[2] + ((T(15) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[3] + ((T(-21) * kin.v[4]) / T(4) + T(3)) * kin.v[4] + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]));
c[2] = T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4];
c[3] = rlog(kin.v[3]) * (T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(-6) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(-6) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (prod_pow(abb[3], 2) * abb[8] * T(3) + abb[5] * (prod_pow(abb[3], 2) * T(-3) + abb[2] * abb[3] * T(3) + abb[4] * T(3)) + abb[2] * abb[3] * (abb[6] * T(-6) + abb[7] * T(-6) + abb[8] * T(3)) + abb[4] * (abb[6] * T(-6) + abb[7] * T(-6) + abb[8] * T(3)) + abb[1] * (abb[5] * (abb[2] * T(-3) + abb[3] * T(-3)) + abb[1] * (abb[6] * T(-6) + abb[7] * T(-6) + abb[5] * T(6)) + abb[2] * (abb[8] * T(-3) + abb[6] * T(6) + abb[7] * T(6)) + abb[3] * (abb[8] * T(-3) + abb[6] * T(6) + abb[7] * T(6))));
    }
};
template <typename T> class SpDLog_f_4_210_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_210_W_23 (const Kin<T>& kin) {
        c[0] = (T(-3) / T(2) + (T(9) * kin.v[2]) / T(8) + (T(3) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(4)) * kin.v[2] + ((T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + (T(-3) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) / T(2) + (T(-3) * kin.v[0]) / T(8) + (T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(4)) * kin.v[0] + (T(3) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = (T(-3) / T(2) + (T(-3) * kin.v[3]) / T(2)) * kin.v[2] + ((T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(3) / T(2) + (T(-3) * kin.v[0]) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(3) * kin.v[3]) + (T(3) * kin.v[4]) / T(2) + (T(-3) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[3] + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(3) / T(2) + (T(-3) * kin.v[0]) / T(8) + (T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(4)) * kin.v[0] + ((T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + (T(-3) / T(2) + (T(9) * kin.v[2]) / T(8) + (T(3) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(4)) * kin.v[2] + (T(-3) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[0] * ((T(-21) * kin.v[0]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[1]) + ((T(15) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-21) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[1] * (T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * ((T(-21) * kin.v[0]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[1]) + ((T(15) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-21) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[1] * (T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[0] * ((T(21) * kin.v[0]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(-21) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-9) + T(9) * kin.v[1]) + ((T(-15) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(21) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[1] * (T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4]));
c[2] = (T(-3) * kin.v[0]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2);
c[3] = rlog(kin.v[3]) * (T(-3) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[0]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * rlog(-kin.v[3] + kin.v[0] + kin.v[1]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(3) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(3) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[9] * (abb[5] * (prod_pow(abb[2], 2) * T(-3) + abb[10] * T(-3)) + prod_pow(abb[1], 2) * ((abb[11] * T(-3)) / T(2) + (abb[12] * T(3)) / T(2) + abb[6] * T(-3) + abb[7] * T(-3) + abb[5] * T(3)) + prod_pow(abb[2], 2) * ((abb[12] * T(-3)) / T(2) + (abb[11] * T(3)) / T(2) + abb[6] * T(3) + abb[7] * T(3)) + abb[10] * ((abb[11] * T(-3)) / T(2) + (abb[12] * T(3)) / T(2) + abb[6] * T(3) + abb[7] * T(3)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_210_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl23 = DLog_W_23<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),spdl22 = SpDLog_f_4_210_W_22<T>(kin),spdl23 = SpDLog_f_4_210_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), -rlog(t), dl23(t), f_2_1_8(kin_path), rlog(kin.W[15] / kin_path.W[15]), -rlog(t), dl16(t), dl17(t), dl20(t), dl4(t)}
;

        auto result = f_4_210_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_210_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[27];
z[0] = abb[1];
z[1] = abb[5];
z[2] = abb[14];
z[3] = abb[15];
z[4] = abb[16];
z[5] = abb[6];
z[6] = abb[13];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[11];
z[10] = abb[12];
z[11] = abb[2];
z[12] = abb[3];
z[13] = abb[4];
z[14] = abb[10];
z[15] = bc<TR>[0];
z[16] = bc<TR>[9];
z[17] = -z[9] + z[10];
z[18] = z[5] + z[7];
z[19] = -z[8] + z[18];
z[20] = -z[17] / T(2) + z[19];
z[20] = z[6] * z[20];
z[21] = z[2] + z[3];
z[22] = -z[4] + z[21];
z[23] = T(3) * z[1] + -z[8] + T(-2) * z[18];
z[24] = z[22] * z[23];
z[24] = -z[20] + z[24];
z[24] = z[14] * z[24];
z[25] = -z[1] + z[8];
z[26] = -(z[21] * z[25]);
z[19] = (T(-3) * z[17]) / T(2) + z[19];
z[19] = z[6] * z[19];
z[23] = z[4] * z[23];
z[19] = z[19] + -z[23] + z[26];
z[19] = z[0] * z[19];
z[21] = T(2) * z[21];
z[21] = z[21] * z[25];
z[21] = z[21] + z[23];
z[23] = z[12] * z[21];
z[19] = z[19] + z[23];
z[19] = z[0] * z[19];
z[23] = z[2] * z[17];
z[26] = -(z[3] * z[25]);
z[20] = -z[20] + z[23] + z[26];
z[20] = z[11] * z[20];
z[23] = z[0] + -z[12];
z[23] = z[21] * z[23];
z[20] = z[20] + z[23];
z[20] = z[11] * z[20];
z[21] = z[13] * z[21];
z[22] = -(z[16] * z[22]);
z[23] = -(z[2] * prod_pow(z[12], 2) * z[25]);
z[19] = z[19] + z[20] + -z[21] + T(3) * z[22] + z[23] + z[24];
z[20] = (T(3) * z[1]) / T(2) + -z[8] / T(2) + -z[18];
z[21] = -z[17] / T(4) + -z[20];
z[21] = z[2] * z[21];
z[17] = z[6] * z[17];
z[20] = z[4] * z[20];
z[18] = -z[1] + z[18];
z[18] = z[3] * z[18];
z[17] = z[17] / T(4) + z[18] + z[20] + z[21];
z[17] = prod_pow(z[15], 2) * z[17];
return z[17] + T(3) * z[19];
}



template IntegrandConstructorType<double> f_4_210_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_210_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_210_construct (const Kin<qd_real>&);
#endif

}