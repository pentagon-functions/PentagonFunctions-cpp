#include "f_4_276.h"

namespace PentagonFunctions {

template <typename T> T f_4_276_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_276_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_276_W_22 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(3) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[6] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(-3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_276_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_276_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(-6) * kin.v[2] + T(-16) * kin.v[3]) + T(-10) * prod_pow(kin.v[3], 2) + kin.v[1] * (T(-2) * kin.v[1] + T(8) * kin.v[2] + T(12) * kin.v[3]) + kin.v[0] * (T(-6) * kin.v[0] + T(-8) * kin.v[1] + T(12) * kin.v[2] + T(16) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(4) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[1] * (T(10) * kin.v[1] + T(-16) * kin.v[2] + T(-12) * kin.v[3]) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[0] + T(16) * kin.v[1] + T(-12) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] + T(-6) * kin.v[2] + T(-4) * kin.v[3])) + T(2) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(6) * kin.v[2] + T(8) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (prod_pow(kin.v[3], 2) + kin.v[1] * (T(5) * kin.v[1] + T(-8) * kin.v[2] + T(-6) * kin.v[3]) + kin.v[2] * (T(3) * kin.v[2] + T(4) * kin.v[3])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(5) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(2) * kin.v[1] + T(-3) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[1] * (kin.v[1] / T(2) + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(5) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(2) * kin.v[1] + T(-3) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[1] * (kin.v[1] / T(2) + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(5) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(2) * kin.v[1] + T(-3) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[1] * (kin.v[1] / T(2) + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(5) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(2) * kin.v[1] + T(-3) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[1] * (kin.v[1] / T(2) + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(5) * prod_pow(kin.v[3], 2) + kin.v[0] * (T(3) * kin.v[0] + T(4) * kin.v[1] + T(-6) * kin.v[2] + T(-8) * kin.v[3]) + kin.v[1] * (kin.v[1] + T(-4) * kin.v[2] + T(-6) * kin.v[3]) + kin.v[2] * (T(3) * kin.v[2] + T(8) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[1]) * ((T(-15) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(-12) * kin.v[3]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(6) * kin.v[2] + T(9) * kin.v[3]) + kin.v[0] * ((T(-9) * kin.v[0]) / T(2) + T(-6) * kin.v[1] + T(9) * kin.v[2] + T(12) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]))) + rlog(kin.v[2]) * ((T(-15) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(-12) * kin.v[3]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(6) * kin.v[2] + T(9) * kin.v[3]) + kin.v[0] * ((T(-9) * kin.v[0]) / T(2) + T(-6) * kin.v[1] + T(9) * kin.v[2] + T(12) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[0] + T(8) * kin.v[1] + T(-8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + T(-8) * kin.v[3];
c[3] = rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3])) + rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[16] * (abb[2] * abb[8] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[19] * T(-4) + abb[18] * T(-2) + abb[3] * T(3)) + abb[12] * (abb[17] * T(-6) + abb[2] * abb[14] * T(-3) + abb[2] * abb[8] * T(3) + abb[2] * (bc<T>[0] * int_to_imaginary<T>(3) + abb[2] * T(3))) + abb[2] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[18] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * bc<T>[0] * int_to_imaginary<T>(3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[2] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[19] * T(-4) + abb[18] * T(-2) + abb[3] * T(3))) + abb[2] * abb[14] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[3] * T(-3) + abb[18] * T(2) + abb[19] * T(4)) + abb[1] * (abb[2] * abb[12] * T(-3) + abb[15] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[19] * T(-4) + abb[18] * T(-2) + abb[3] * T(3) + abb[12] * T(3)) + abb[2] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[3] * T(-3) + abb[18] * T(2) + abb[19] * T(4))) + abb[17] * (abb[3] * T(-6) + abb[4] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[11] * T(2) + abb[18] * T(4) + abb[19] * T(8)) + abb[15] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[18] * bc<T>[0] * int_to_imaginary<T>(2) + abb[3] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[19] * T(-4) + abb[18] * T(-2) + abb[3] * T(3)) + abb[14] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[19] * T(-4) + abb[18] * T(-2) + abb[3] * T(3)) + abb[12] * (abb[8] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3) + abb[2] * T(3) + abb[14] * T(3)) + abb[8] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[3] * T(-3) + abb[18] * T(2) + abb[19] * T(4)) + abb[15] * (abb[3] * T(-6) + abb[12] * T(-6) + abb[4] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[11] * T(2) + abb[18] * T(4) + abb[19] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_276_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl4 = DLog_W_4<T>(kin),dl25 = DLog_W_25<T>(kin),dl21 = DLog_W_21<T>(kin),dl26 = DLog_W_26<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl3 = DLog_W_3<T>(kin),spdl22 = SpDLog_f_4_276_W_22<T>(kin),spdl21 = SpDLog_f_4_276_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl4(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), f_2_1_14(kin_path), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[1] / kin_path.W[1]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), dl21(t), f_2_1_15(kin_path), rlog(kin.W[16] / kin_path.W[16]), -rlog(t), dl26(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_2_1_10(kin_path), f_2_1_12(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl1(t), dl5(t), dl17(t), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl16(t), dl20(t), dl18(t), dl2(t), dl19(t), dl3(t)}
;

        auto result = f_4_276_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_276_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[137];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[30];
z[4] = abb[31];
z[5] = abb[36];
z[6] = abb[38];
z[7] = abb[40];
z[8] = abb[41];
z[9] = abb[4];
z[10] = abb[37];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[11];
z[14] = abb[12];
z[15] = abb[18];
z[16] = abb[19];
z[17] = abb[21];
z[18] = abb[33];
z[19] = abb[34];
z[20] = abb[35];
z[21] = abb[26];
z[22] = abb[27];
z[23] = abb[28];
z[24] = abb[2];
z[25] = abb[8];
z[26] = abb[14];
z[27] = abb[15];
z[28] = bc<TR>[0];
z[29] = abb[20];
z[30] = abb[32];
z[31] = abb[39];
z[32] = abb[9];
z[33] = abb[10];
z[34] = abb[29];
z[35] = abb[17];
z[36] = abb[22];
z[37] = abb[23];
z[38] = abb[24];
z[39] = abb[25];
z[40] = abb[13];
z[41] = bc<TR>[3];
z[42] = bc<TR>[1];
z[43] = bc<TR>[2];
z[44] = bc<TR>[4];
z[45] = bc<TR>[9];
z[46] = z[9] / T(2);
z[47] = z[13] / T(2);
z[48] = z[46] + z[47];
z[49] = z[1] / T(2);
z[50] = z[14] / T(2);
z[51] = -z[11] + z[12];
z[52] = z[48] + -z[49] + -z[50] + z[51] / T(3);
z[52] = z[4] * z[52];
z[53] = (T(5) * z[14]) / T(4);
z[54] = z[9] + z[11];
z[55] = z[1] + T(-3) * z[12] + z[54];
z[55] = z[55] / T(2);
z[56] = -z[15] + z[55];
z[57] = -z[42] + T(3) * z[43];
z[58] = (T(-3) * z[13]) / T(4) + -z[16] + z[53] + z[56] / T(2) + -z[57];
z[58] = z[31] * z[58];
z[59] = z[15] / T(3) + (T(2) * z[16]) / T(3) + z[57];
z[60] = T(5) * z[9];
z[61] = -z[12] + z[60];
z[62] = -z[11] + z[61];
z[62] = -z[1] + z[62] / T(3);
z[62] = -z[13] / T(6) + z[50] + -z[59] + z[62] / T(2);
z[62] = z[7] * z[62];
z[63] = T(3) * z[9];
z[64] = -z[12] + z[63];
z[65] = T(3) * z[11];
z[66] = T(5) * z[1] + -z[64] + -z[65];
z[66] = z[66] / T(2);
z[67] = z[47] + z[50];
z[68] = -z[15] + z[66] + z[67];
z[68] = -z[16] + z[68] / T(2);
z[69] = z[42] + z[68];
z[69] = z[8] * z[69];
z[70] = -z[17] + -z[21] + z[22] + z[23];
z[71] = z[19] * z[70];
z[72] = z[20] * z[70];
z[73] = z[71] + -z[72];
z[74] = -z[13] + z[14];
z[75] = -z[51] + z[74];
z[75] = z[40] * z[75];
z[76] = T(3) * z[75];
z[77] = -z[1] + -z[12] + z[54];
z[78] = -z[74] + z[77];
z[79] = z[2] * z[78];
z[80] = z[9] + z[12];
z[81] = -z[1] + -z[11] + z[80];
z[82] = -z[74] + z[81];
z[83] = z[34] * z[82];
z[84] = int_to_imaginary<T>(1) * z[28];
z[85] = -z[18] + z[19] + z[20] + -z[30];
z[86] = z[8] / T(4) + (T(3) * z[31]) / T(4) + z[85] / T(9);
z[86] = z[84] * z[86];
z[87] = z[18] * z[70];
z[88] = z[87] / T(2);
z[89] = z[30] * z[70];
z[52] = z[52] + z[58] + z[62] + z[69] + -z[73] / T(2) + -z[76] + z[79] / T(4) + -z[83] + z[86] + z[88] + (T(3) * z[89]) / T(4);
z[52] = z[28] * z[52];
z[58] = z[29] * z[70];
z[62] = z[58] + -z[72];
z[69] = z[74] + z[81];
z[70] = z[3] * z[69];
z[73] = z[62] + z[70];
z[81] = -z[71] + z[87];
z[86] = z[81] + -z[83];
z[90] = z[73] + z[86];
z[91] = z[38] * z[90];
z[73] = z[73] + -z[89];
z[92] = z[0] * z[73];
z[91] = -z[91] + z[92];
z[83] = z[71] + z[83];
z[92] = z[62] + z[83];
z[93] = (T(3) * z[13]) / T(2) + (T(-5) * z[14]) / T(2);
z[94] = z[15] + T(2) * z[16];
z[95] = -z[55] + z[93] + z[94];
z[96] = z[31] * z[95];
z[97] = z[89] / T(2);
z[98] = z[92] / T(2) + z[96] + -z[97];
z[99] = -(z[39] * z[98]);
z[88] = z[88] + z[96];
z[96] = z[62] / T(2) + z[88];
z[96] = z[24] * z[96];
z[86] = z[86] + z[89];
z[100] = z[25] / T(2);
z[101] = -(z[86] * z[100]);
z[102] = z[42] / T(2) + -z[43];
z[102] = z[42] * z[102];
z[102] = z[44] + z[102];
z[103] = -(z[31] * z[102]);
z[91] = -z[91] / T(2) + z[96] + z[99] + z[101] + z[103];
z[99] = T(3) * z[1];
z[101] = z[12] + z[54] + -z[99];
z[103] = (T(3) * z[14]) / T(2);
z[104] = z[47] + z[101] / T(2) + -z[103];
z[105] = -z[94] + -z[104];
z[106] = z[0] * z[105];
z[107] = -z[13] + z[94];
z[108] = T(2) * z[11] + z[107];
z[109] = -z[80] + z[108];
z[110] = z[25] * z[109];
z[111] = z[106] + z[110];
z[112] = z[24] * z[105];
z[65] = -z[1] + z[65] + -z[80];
z[67] = z[67] + -z[94];
z[65] = z[65] / T(2) + -z[67];
z[113] = z[39] * z[65];
z[114] = (T(3) * z[38]) / T(2);
z[115] = z[82] * z[114];
z[102] = T(3) * z[102];
z[113] = -z[102] + z[111] + -z[112] + T(-3) * z[113] + z[115];
z[113] = z[7] * z[113];
z[115] = z[27] * z[109];
z[115] = z[112] + z[115];
z[74] = z[74] + z[77];
z[77] = z[74] * z[114];
z[63] = z[63] + -z[99];
z[116] = T(3) * z[14];
z[117] = T(3) * z[13] + -z[116];
z[118] = -z[63] + z[117];
z[119] = z[51] + z[118];
z[120] = z[0] * z[119];
z[121] = (T(3) * z[82]) / T(2);
z[122] = z[39] * z[121];
z[123] = z[25] * z[51];
z[122] = -z[77] + z[115] + -z[120] / T(2) + z[122] + -z[123];
z[122] = z[4] * z[122];
z[109] = z[7] * z[109];
z[124] = (T(3) * z[86]) / T(2) + T(-2) * z[109];
z[124] = z[27] * z[124];
z[125] = -(z[8] * z[102]);
z[91] = T(3) * z[91] + z[113] + z[122] + -z[124] + z[125];
z[91] = int_to_imaginary<T>(1) * z[91];
z[85] = -(z[41] * z[85]);
z[52] = z[52] + T(3) * z[85] + z[91];
z[52] = z[28] * z[52];
z[85] = z[0] * z[51];
z[91] = -z[85] + z[123];
z[113] = z[51] * z[84];
z[122] = z[91] + z[113];
z[123] = z[27] * z[105];
z[122] = z[112] + T(2) * z[122] + -z[123];
z[122] = z[5] * z[122];
z[105] = z[25] * z[105];
z[125] = z[105] + -z[106];
z[126] = T(2) * z[12];
z[127] = -z[54] + z[126];
z[128] = T(2) * z[13] + z[94] + -z[116] + z[127];
z[129] = T(2) * z[24];
z[130] = z[128] * z[129];
z[131] = -z[125] + z[130];
z[132] = z[84] * z[128];
z[132] = -z[123] + z[131] + T(-2) * z[132];
z[132] = z[10] * z[132];
z[56] = z[56] + -z[93];
z[56] = -z[16] + z[56] / T(2);
z[56] = z[31] * z[56];
z[56] = z[56] + -z[75];
z[75] = z[89] / T(4);
z[92] = -z[56] + -z[75] + z[92] / T(4);
z[93] = z[15] + z[104];
z[93] = z[16] + z[93] / T(2);
z[104] = z[7] + -z[10];
z[104] = z[93] * z[104];
z[63] = -z[51] + z[63];
z[117] = -z[63] + -z[117];
z[117] = z[4] * z[117];
z[133] = T(5) * z[11];
z[134] = -z[99] + z[133];
z[64] = z[64] + -z[134];
z[64] = (T(-9) * z[13]) / T(2) + (T(15) * z[14]) / T(2) + T(-3) * z[15] + z[64] / T(2);
z[135] = T(3) * z[16];
z[64] = z[64] / T(2) + -z[135];
z[64] = z[6] * z[64];
z[136] = -(z[5] * z[51]);
z[64] = z[64] + T(3) * z[92] + z[104] + z[117] / T(4) + z[136];
z[64] = z[26] * z[64];
z[92] = -z[0] + z[25];
z[73] = -(z[73] * z[92]);
z[73] = z[73] / T(2) + -z[96];
z[104] = z[112] + z[125];
z[104] = z[7] * z[104];
z[117] = z[25] * z[119];
z[136] = -z[117] + z[120];
z[115] = -z[115] + z[136] / T(2);
z[115] = z[4] * z[115];
z[128] = z[24] * z[128];
z[91] = z[91] + z[128];
z[91] = T(2) * z[91] + T(-4) * z[113] + -z[123];
z[91] = z[6] * z[91];
z[113] = -(z[4] * z[51]);
z[76] = -z[76] + z[109] + z[113];
z[76] = z[76] * z[84];
z[64] = z[64] + T(3) * z[73] + T(2) * z[76] + z[91] + z[104] + z[115] + z[122] + z[124] + z[132];
z[64] = z[26] * z[64];
z[73] = z[79] + -z[81];
z[76] = z[58] + z[70];
z[81] = z[4] + -z[5];
z[81] = z[74] * z[81];
z[91] = z[6] * z[69];
z[104] = z[10] * z[78];
z[81] = -z[73] + -z[76] + z[81] + z[89] + z[91] + z[104];
z[81] = z[32] * z[81];
z[91] = z[71] + z[72];
z[104] = z[91] / T(2);
z[70] = -z[70] / T(2) + -z[79] + z[87] + -z[104];
z[109] = prod_pow(z[0], 2);
z[70] = z[70] * z[109];
z[90] = z[90] * z[100];
z[73] = z[72] + z[73];
z[113] = z[0] * z[73];
z[90] = z[90] + z[113];
z[90] = z[25] * z[90];
z[62] = z[62] + -z[79] + -z[89];
z[62] = z[33] * z[62];
z[72] = -z[72] + z[76] + -z[83];
z[72] = z[36] * z[72];
z[73] = -(z[24] * z[73] * z[92]);
z[76] = z[33] + z[36];
z[76] = z[76] * z[87];
z[62] = z[62] + z[70] + z[72] + z[73] + z[76] + z[81] + z[90];
z[70] = -z[80] + z[134];
z[72] = T(2) * z[15] + T(4) * z[16];
z[70] = -z[47] + z[70] / T(2) + z[72] + -z[103];
z[70] = z[27] * z[70];
z[70] = z[70] + z[111] + z[112];
z[70] = z[27] * z[70];
z[73] = z[36] * z[74];
z[74] = z[33] * z[78];
z[74] = -z[73] + z[74];
z[63] = z[63] * z[109];
z[63] = z[63] + T(3) * z[74];
z[72] = z[13] + z[72] + z[101] + -z[116];
z[72] = z[35] * z[72];
z[74] = z[85] + z[117] / T(4);
z[74] = z[25] * z[74];
z[76] = z[107] + z[127];
z[78] = z[24] + z[92];
z[78] = z[76] * z[78];
z[79] = -(z[24] * z[78]);
z[80] = -(z[37] * z[121]);
z[63] = z[63] / T(2) + z[70] + z[72] + z[74] + z[79] + z[80];
z[63] = z[4] * z[63];
z[70] = T(2) * z[85] + z[102];
z[72] = z[100] * z[119];
z[74] = -(z[69] * z[114]);
z[74] = z[70] + -z[72] + z[74] + -z[112];
z[74] = int_to_imaginary<T>(1) * z[74];
z[79] = z[11] / T(2);
z[80] = z[79] + -z[126];
z[81] = z[15] / T(2) + z[16];
z[46] = -z[1] + -z[42] + z[46] + -z[50] + -z[80] / T(3) + z[81] + -z[84] / T(4);
z[46] = z[28] * z[46];
z[50] = int_to_imaginary<T>(1) * z[123];
z[46] = z[46] + z[50] + z[74];
z[46] = z[28] * z[46];
z[74] = z[9] + z[15];
z[74] = (T(3) * z[74]) / T(2) + -z[80] + -z[99] + -z[103] + z[135];
z[74] = z[74] * z[109];
z[80] = z[27] * z[93];
z[83] = T(2) * z[9] + -z[12] + -z[99] + z[108];
z[85] = z[0] * z[83];
z[90] = -z[80] + T(-2) * z[85] + z[105] + -z[112];
z[90] = z[27] * z[90];
z[83] = z[24] * z[83];
z[83] = z[83] + -z[125];
z[83] = z[24] * z[83];
z[92] = T(9) * z[1];
z[54] = T(-5) * z[12] + T(7) * z[54] + -z[92];
z[54] = (T(-5) * z[13]) / T(2) + z[54] / T(2) + z[94] + z[103];
z[54] = z[35] * z[54];
z[54] = z[54] + z[83];
z[66] = z[66] + z[67];
z[83] = z[33] * z[66];
z[101] = z[36] / T(2);
z[69] = -(z[69] * z[101]);
z[69] = z[69] + -z[83];
z[51] = -z[51] + z[118];
z[51] = z[51] * z[100];
z[100] = -z[0] + z[100];
z[100] = z[51] * z[100];
z[105] = (T(5) * z[45]) / T(2);
z[46] = z[46] + z[54] + T(3) * z[69] + z[74] + z[90] + z[100] + -z[105];
z[46] = z[5] * z[46];
z[69] = T(3) * z[95];
z[74] = z[39] * z[69];
z[51] = z[51] + z[70] + z[74] + z[77] + -z[130];
z[51] = int_to_imaginary<T>(1) * z[51];
z[70] = (T(11) * z[11]) / T(2) + T(-4) * z[12];
z[47] = -z[14] + z[47] + -z[49] + z[57] + z[70] / T(3) + z[81] + (T(-3) * z[84]) / T(4);
z[47] = z[28] * z[47];
z[47] = z[47] + z[50] + z[51];
z[47] = z[28] * z[47];
z[49] = z[80] + z[131];
z[49] = z[27] * z[49];
z[51] = z[37] * z[69];
z[57] = T(-7) * z[12] + z[60] + z[134];
z[57] = (T(7) * z[13]) / T(2) + (T(-9) * z[14]) / T(2) + -z[57] / T(2) + z[94];
z[57] = z[35] * z[57];
z[49] = -z[49] + -z[51] + z[57];
z[51] = -z[72] + z[120];
z[51] = z[25] * z[51];
z[57] = z[109] * z[119];
z[51] = z[51] + -z[57] / T(2) + T(3) * z[73];
z[57] = -z[125] + z[128];
z[57] = z[24] * z[57];
z[47] = -z[45] + z[47] + z[49] + z[51] / T(2) + z[57];
z[47] = z[6] * z[47];
z[51] = z[74] + z[102] + -z[131];
z[51] = int_to_imaginary<T>(1) * z[51];
z[57] = z[61] + z[133];
z[57] = z[57] / T(3) + -z[99];
z[53] = (T(-19) * z[13]) / T(12) + z[53] + z[57] / T(4) + z[59];
z[53] = z[28] * z[53];
z[50] = z[50] + z[51] + z[53];
z[50] = z[28] * z[50];
z[48] = -z[12] + z[48] + z[79] + -z[81];
z[51] = z[48] * z[109];
z[48] = z[25] * z[48];
z[53] = z[0] * z[76];
z[48] = z[48] + z[53];
z[48] = z[25] * z[48];
z[53] = z[55] + z[67];
z[53] = z[33] * z[53];
z[55] = -(z[78] * z[129]);
z[57] = (T(85) * z[45]) / T(6);
z[48] = z[48] + z[49] + z[50] + z[51] + T(3) * z[53] + z[55] + -z[57];
z[48] = z[10] * z[48];
z[49] = z[25] * z[93];
z[49] = z[49] + z[106];
z[49] = z[25] * z[49];
z[50] = z[82] * z[101];
z[50] = z[50] + -z[83];
z[51] = z[93] * z[109];
z[49] = z[49] + T(3) * z[50] + z[51] + z[54] + z[57];
z[49] = z[7] * z[49];
z[50] = -z[58] + -z[71] + z[89];
z[50] = z[0] * z[50];
z[51] = -(z[25] * z[86]);
z[50] = z[50] + z[51];
z[50] = z[50] / T(2) + z[96];
z[51] = -z[85] + z[110];
z[51] = T(2) * z[51] + -z[112];
z[51] = z[7] * z[51];
z[53] = z[8] * z[68];
z[54] = z[87] + -z[91];
z[53] = z[53] + -z[54] / T(4) + z[56] + -z[75];
z[54] = -z[9] + T(23) * z[11] + T(-13) * z[12] + -z[92];
z[54] = (T(-13) * z[13]) / T(2) + T(5) * z[15] + z[54] / T(2) + z[103];
z[54] = T(5) * z[16] + z[54] / T(2);
z[54] = z[7] * z[54];
z[53] = T(3) * z[53] + z[54];
z[53] = z[27] * z[53];
z[54] = T(3) * z[66];
z[55] = -(z[0] * z[8] * z[54]);
z[50] = T(3) * z[50] + z[51] + z[53] + z[55];
z[50] = z[27] * z[50];
z[51] = z[7] * z[65];
z[51] = z[51] + z[98];
z[51] = z[37] * z[51];
z[53] = -z[88] + -z[97] + z[104];
z[53] = z[35] * z[53];
z[51] = z[51] + z[53];
z[53] = z[68] * z[109];
z[53] = z[53] + z[83];
z[54] = z[35] * z[54];
z[53] = T(3) * z[53] + z[54] + z[105];
z[53] = z[8] * z[53];
z[54] = z[31] * z[45];
return z[46] + z[47] + z[48] + z[49] + z[50] + T(3) * z[51] + z[52] + z[53] + z[54] + (T(3) * z[62]) / T(2) + z[63] + z[64];
}



template IntegrandConstructorType<double> f_4_276_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_276_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_276_construct (const Kin<qd_real>&);
#endif

}