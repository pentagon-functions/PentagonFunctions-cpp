#include "f_4_56.h"

namespace PentagonFunctions {

template <typename T> T f_4_56_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_56_W_7 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_56_W_7 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1]) * (T(12) * kin.v[1] * kin.v[4] + kin.v[3] * (T(12) + T(12) * kin.v[1] + T(-6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-6) + prod_pow(abb[2], 2) * abb[4] * T(6) + prod_pow(abb[1], 2) * (abb[4] * T(-6) + abb[3] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_56_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_56_W_12 (const Kin<T>& kin) {
        c[0] = ((T(-9) * kin.v[4]) / T(4) + T(-3)) * kin.v[4] + ((T(3) * kin.v[1]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[1];
c[1] = bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-16) * kin.v[4]) + kin.v[4] * (T(-3) + T(-9) * kin.v[4]) + rlog(-kin.v[1]) * (kin.v[1] * (T(12) + T(6) * kin.v[1]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4])) + rlog(kin.v[3]) * (((T(-27) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[1] * (bc<T>[0] * int_to_imaginary<T>(8) + T(14) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(13) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(27) * kin.v[4]) / T(2) + T(14)) * kin.v[4] + kin.v[1] * (T(-14) + bc<T>[0] * int_to_imaginary<T>(-8) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-13) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(8) + T(4) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * ((T(29) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(8) + T(5) + (T(-19) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1]) + ((T(-39) * kin.v[4]) / T(4) + T(-5)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-4) * kin.v[4])) + kin.v[1] * (T(3) + (T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(15) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(24) * kin.v[4]));
c[2] = T(-3) * kin.v[1] + T(3) * kin.v[4];
c[3] = rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[1] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(3)) * kin.v[1] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[5] * (abb[3] * prod_pow(abb[6], 2) * T(-6) + abb[7] * abb[9] * T(-6) + abb[7] * (abb[8] * T(-3) + abb[11] * T(-3) + abb[4] * T(6)) + abb[6] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[11] * bc<T>[0] * int_to_imaginary<T>(8) + abb[6] * (-abb[11] + abb[9] * T(-4) + abb[8] * T(3) + abb[4] * T(4)) + abb[9] * (abb[10] * T(-8) + bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * T(8)) + abb[10] * (abb[11] * T(-8) + abb[4] * T(8)) + abb[2] * (abb[4] * T(-8) + abb[11] * T(8))) + abb[1] * (abb[11] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[4] * bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * (abb[11] + abb[4] * T(-4) + abb[8] * T(-3) + abb[9] * T(4) + abb[3] * T(6)) + abb[2] * (abb[11] * T(-8) + abb[4] * T(8)) + abb[9] * (abb[2] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[10] * T(8)) + abb[10] * (abb[4] * T(-8) + abb[11] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_56_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl12 = DLog_W_12<T>(kin),dl14 = DLog_W_14<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),spdl7 = SpDLog_f_4_56_W_7<T>(kin),spdl12 = SpDLog_f_4_56_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,21> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[18] / kin_path.W[18]), dl12(t), rlog(-v_path[4]), f_2_1_4(kin_path), -rlog(t), rlog(kin.W[3] / kin_path.W[3]), rlog(v_path[3]), rlog(kin.W[4] / kin_path.W[4]), dl14(t), rlog(kin.W[13] / kin_path.W[13]), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl19(t), f_2_1_11(kin_path), dl2(t), dl4(t), dl5(t)}
;

        auto result = f_4_56_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_56_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[88];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[12];
z[3] = abb[16];
z[4] = abb[18];
z[5] = abb[19];
z[6] = abb[20];
z[7] = abb[4];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[11];
z[11] = abb[13];
z[12] = abb[2];
z[13] = abb[6];
z[14] = abb[10];
z[15] = bc<TR>[0];
z[16] = abb[7];
z[17] = abb[14];
z[18] = abb[15];
z[19] = abb[17];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = T(4) * z[8];
z[23] = T(3) * z[9];
z[24] = z[22] + -z[23];
z[24] = T(2) * z[24];
z[25] = T(5) * z[1];
z[26] = T(3) * z[11];
z[27] = z[24] + -z[25] + -z[26];
z[28] = -(z[13] * z[27]);
z[29] = T(2) * z[9];
z[30] = -z[8] + z[29];
z[31] = z[10] + z[30];
z[32] = T(2) * z[7];
z[33] = z[1] + z[32];
z[31] = -z[26] + T(2) * z[31] + z[33];
z[34] = T(3) * z[18];
z[35] = -(z[31] * z[34]);
z[36] = T(2) * z[0];
z[37] = z[1] + -z[8];
z[38] = -(z[36] * z[37]);
z[39] = z[9] + z[10];
z[40] = z[7] + z[39];
z[41] = T(2) * z[11] + -z[40];
z[42] = z[12] * z[41];
z[43] = T(6) * z[42];
z[44] = prod_pow(z[20], 2);
z[45] = prod_pow(z[15], 2);
z[46] = T(6) * z[41];
z[46] = -(z[14] * z[46]);
z[28] = z[28] + z[35] + z[38] + -z[43] + T(-3) * z[44] + -z[45] / T(12) + z[46];
z[35] = int_to_imaginary<T>(1) * z[15];
z[28] = z[28] * z[35];
z[38] = T(3) * z[10];
z[46] = z[23] + z[38];
z[47] = -z[22] + z[46];
z[48] = T(2) * z[1];
z[49] = T(3) * z[7];
z[50] = -z[47] + z[48] + -z[49];
z[51] = T(2) * z[12];
z[52] = z[13] + z[51];
z[52] = z[50] * z[52];
z[24] = -z[24] + z[38];
z[53] = T(6) * z[7];
z[54] = z[1] + z[26];
z[24] = T(2) * z[24] + z[53] + z[54];
z[24] = z[0] * z[24];
z[27] = z[14] * z[27];
z[24] = z[24] + z[27] + z[52];
z[24] = z[13] * z[24];
z[27] = T(2) * z[8];
z[52] = -z[9] + z[27];
z[52] = -z[10] + T(4) * z[52];
z[55] = T(11) * z[11];
z[52] = -z[33] + T(2) * z[52] + -z[55];
z[52] = z[16] * z[52];
z[56] = T(3) * z[17];
z[31] = -(z[31] * z[56]);
z[57] = -z[49] + z[54];
z[58] = -z[27] + z[46] + -z[57];
z[59] = T(4) * z[0];
z[58] = z[58] * z[59];
z[60] = -z[40] + z[48];
z[60] = z[12] * z[60];
z[58] = z[58] + T(3) * z[60];
z[58] = z[12] * z[58];
z[60] = z[23] + -z[27];
z[60] = z[38] + T(2) * z[60];
z[61] = T(9) * z[11] + -z[53];
z[60] = z[25] + T(2) * z[60] + -z[61];
z[60] = z[0] * z[60];
z[62] = T(3) * z[14];
z[41] = z[41] * z[62];
z[41] = z[41] + z[43] + z[60];
z[41] = z[14] * z[41];
z[60] = (T(5) * z[1]) / T(2);
z[63] = T(5) * z[8];
z[64] = (T(43) * z[39]) / T(4) + -z[60] + -z[63];
z[65] = T(5) * z[11];
z[64] = (T(47) * z[7]) / T(12) + (T(-15) * z[20]) / T(2) + z[64] / T(3) + -z[65];
z[64] = z[45] * z[64];
z[66] = T(6) * z[39] + -z[63];
z[67] = T(7) * z[11];
z[68] = z[66] + -z[67];
z[69] = T(7) * z[7];
z[70] = z[1] + -z[68] + -z[69];
z[71] = T(2) * z[19];
z[70] = z[70] * z[71];
z[72] = z[8] + -z[9];
z[72] = -z[10] + T(2) * z[72];
z[72] = z[11] + -z[33] + T(2) * z[72];
z[73] = prod_pow(z[0], 2);
z[72] = z[72] * z[73];
z[24] = (T(11) * z[21]) / T(4) + z[24] + z[28] + z[31] + z[41] + z[52] + z[58] + z[64] + z[70] + T(3) * z[72];
z[24] = z[3] * z[24];
z[28] = z[47] + z[57];
z[31] = z[13] * z[28];
z[41] = T(2) * z[31];
z[28] = z[14] * z[28];
z[57] = -z[8] + z[46];
z[54] = z[53] + -z[54] + -z[57];
z[58] = -(z[51] * z[54]);
z[64] = z[1] + z[7];
z[70] = T(2) * z[39];
z[72] = -z[64] + z[70];
z[72] = z[0] * z[72];
z[72] = (T(5) * z[44]) / T(2) + z[72];
z[74] = z[1] + z[49];
z[75] = z[8] + z[39];
z[75] = -z[74] + T(2) * z[75];
z[76] = -(z[34] * z[75]);
z[58] = T(-2) * z[28] + -z[41] + (T(49) * z[45]) / T(12) + z[58] + T(3) * z[72] + z[76];
z[58] = z[35] * z[58];
z[50] = z[12] * z[50];
z[66] = z[26] + z[66] + -z[74];
z[66] = z[36] * z[66];
z[66] = z[50] + z[66];
z[66] = z[12] * z[66];
z[54] = z[12] * z[54];
z[72] = T(3) * z[0];
z[74] = -z[7] + z[8];
z[74] = z[72] * z[74];
z[54] = z[54] + z[74];
z[54] = z[28] + T(2) * z[54];
z[54] = z[14] * z[54];
z[74] = -z[1] + -z[11] + z[32];
z[72] = z[72] * z[74];
z[50] = z[28] + z[50] + z[72];
z[72] = T(4) * z[1] + z[47] + -z[49];
z[72] = z[13] * z[72];
z[50] = T(2) * z[50] + z[72];
z[50] = z[13] * z[50];
z[64] = z[63] + z[64] + -z[67];
z[72] = z[64] * z[71];
z[53] = z[1] + -z[26] + z[53] + T(-2) * z[57];
z[53] = z[53] * z[73];
z[57] = z[7] + -z[9];
z[74] = T(3) * z[8];
z[76] = (T(5) * z[11]) / T(2);
z[57] = (T(-3) * z[1]) / T(2) + (T(-55) * z[10]) / T(12) + (T(3) * z[20]) / T(4) + (T(67) * z[57]) / T(12) + z[74] + -z[76];
z[57] = z[45] * z[57];
z[75] = -(z[56] * z[75]);
z[77] = z[1] + z[27];
z[78] = T(2) * z[10];
z[79] = -z[9] + z[78];
z[80] = z[77] + -z[79];
z[80] = T(-13) * z[7] + z[55] + T(2) * z[80];
z[80] = z[16] * z[80];
z[50] = (T(453) * z[21]) / T(8) + z[50] + z[53] + z[54] + z[57] + z[58] + z[66] + z[72] + z[75] + z[80];
z[50] = z[6] * z[50];
z[53] = T(5) * z[9];
z[54] = T(8) * z[8];
z[57] = (T(13) * z[1]) / T(2);
z[58] = (T(3) * z[11]) / T(2);
z[66] = T(5) * z[10] + -z[32];
z[72] = z[53] + -z[54] + z[57] + -z[58] + z[66];
z[72] = z[0] * z[72];
z[75] = z[25] + -z[26] + T(2) * z[47];
z[75] = z[14] * z[75];
z[80] = T(3) * z[1];
z[81] = T(5) * z[7];
z[82] = z[70] + z[80] + -z[81];
z[83] = -(z[13] * z[82]);
z[84] = z[1] / T(2);
z[85] = z[11] / T(2) + -z[84];
z[86] = z[27] + -z[40] + z[85];
z[34] = z[34] * z[86];
z[87] = z[37] * z[51];
z[34] = z[34] + (T(-3) * z[44]) / T(4) + (T(-5) * z[45]) / T(4) + z[72] + z[75] + z[83] + -z[87];
z[34] = z[34] * z[35];
z[72] = (T(7) * z[11]) / T(2);
z[57] = z[49] + -z[57] + z[63] + -z[72] + z[79];
z[57] = z[57] * z[73];
z[75] = T(4) * z[11];
z[30] = -z[30] + -z[33] + z[75] + -z[78];
z[30] = z[0] * z[30];
z[33] = z[46] + z[48];
z[79] = -z[26] + -z[27] + z[33];
z[79] = z[12] * z[79];
z[30] = z[30] + z[79];
z[30] = z[30] * z[51];
z[79] = z[8] + -z[39];
z[79] = -z[25] + T(2) * z[79] + z[81];
z[79] = z[0] * z[79];
z[47] = -z[47] + z[58] + -z[60];
z[47] = z[14] * z[47];
z[47] = z[47] + z[79] + z[87];
z[47] = z[14] * z[47];
z[58] = T(4) * z[9] + z[10] + -z[26] + z[32] + -z[74] + z[80];
z[58] = z[13] * z[58];
z[60] = z[14] * z[82];
z[74] = z[1] + z[70];
z[74] = z[7] + T(8) * z[11] + T(-3) * z[74];
z[74] = z[0] * z[74];
z[58] = T(-4) * z[42] + z[58] + z[60] + z[74];
z[58] = z[13] * z[58];
z[60] = z[56] * z[86];
z[25] = z[7] + z[25] + z[68];
z[25] = z[25] * z[71];
z[68] = -z[8] + z[39] / T(2);
z[48] = (T(-5) * z[7]) / T(6) + z[11] / T(3) + (T(-15) * z[20]) / T(8) + z[48] + T(3) * z[68];
z[48] = z[45] * z[48];
z[55] = T(10) * z[1] + -z[7] + T(-7) * z[8] + T(12) * z[9] + T(9) * z[10] + -z[55];
z[55] = z[16] * z[55];
z[25] = (T(135) * z[21]) / T(16) + z[25] + z[30] + z[34] + z[47] + z[48] + z[55] + z[57] + z[58] + z[60];
z[25] = z[4] * z[25];
z[30] = -z[7] + z[11];
z[34] = -z[1] + z[30] + z[39];
z[47] = z[14] * z[34];
z[40] = z[8] + -z[26] + z[40];
z[48] = z[18] * z[40];
z[47] = z[47] + z[48];
z[37] = z[37] + z[46] + -z[49];
z[36] = z[36] * z[37];
z[36] = z[36] + -z[41] + -z[43] + (T(21) * z[44]) / T(2) + (T(55) * z[45]) / T(12) + T(-6) * z[47];
z[36] = z[35] * z[36];
z[33] = -z[22] + -z[33] + z[75] + z[81];
z[33] = z[12] * z[33];
z[37] = -z[1] + z[67];
z[41] = -z[32] + -z[37] + z[54];
z[41] = z[0] * z[41];
z[28] = z[28] + z[33] + z[41];
z[28] = T(2) * z[28] + z[31];
z[28] = z[13] * z[28];
z[31] = -(z[19] * z[64]);
z[33] = -(z[40] * z[56]);
z[31] = z[31] + z[33];
z[33] = -z[7] + z[46] + -z[65] + z[77];
z[33] = z[33] * z[59];
z[40] = T(3) * z[42];
z[33] = z[33] + z[40];
z[33] = z[12] * z[33];
z[22] = -z[1] + z[22] + -z[61];
z[22] = z[0] * z[22];
z[22] = z[22] + z[40];
z[34] = z[34] * z[62];
z[22] = T(2) * z[22] + z[34];
z[22] = z[14] * z[22];
z[27] = -z[27] + -z[39];
z[27] = -z[1] + T(2) * z[27];
z[34] = T(4) * z[7];
z[27] = T(17) * z[11] + T(3) * z[27] + z[34];
z[27] = z[27] * z[73];
z[40] = (T(43) * z[1]) / T(4) + (T(-79) * z[9]) / T(4) + (T(-67) * z[10]) / T(4) + z[63];
z[40] = (T(23) * z[7]) / T(12) + (T(-19) * z[11]) / T(12) + (T(9) * z[20]) / T(4) + z[40] / T(3);
z[40] = z[40] * z[45];
z[22] = (T(399) * z[21]) / T(8) + z[22] + z[27] + z[28] + T(2) * z[31] + z[33] + z[36] + z[40] + -z[52];
z[22] = z[5] * z[22];
z[27] = z[38] + -z[49] + z[53] + z[85];
z[27] = z[27] * z[73];
z[28] = z[32] + z[39] + -z[72] + z[84];
z[28] = z[14] * z[28];
z[31] = -z[7] + -z[11] + z[70];
z[31] = z[31] * z[51];
z[23] = -z[23] + -z[78];
z[23] = z[7] + T(2) * z[23] + z[26];
z[23] = z[0] * z[23];
z[23] = z[23] + z[28] + z[31];
z[23] = z[14] * z[23];
z[26] = T(7) * z[9] + z[66] + -z[76] + -z[84];
z[28] = z[17] * z[26];
z[32] = -(z[0] * z[31]);
z[29] = -z[29] + z[30];
z[29] = z[13] * z[29];
z[30] = z[0] + -z[14];
z[30] = z[29] * z[30];
z[23] = z[23] + z[27] + z[28] + z[30] + z[32];
z[27] = -z[34] + z[37] + -z[70];
z[27] = z[14] * z[27];
z[26] = z[18] * z[26];
z[28] = z[7] + -z[39] + -z[85];
z[28] = z[0] * z[28];
z[26] = z[26] + z[27] + z[28] + z[29] + -z[31] + (T(-5) * z[44]) / T(4);
z[26] = T(3) * z[26] + (T(-11) * z[45]) / T(4);
z[26] = z[26] * z[35];
z[27] = (T(-7) * z[1]) / T(2) + z[9] + -z[10] + (T(69) * z[20]) / T(4);
z[27] = (T(37) * z[11]) / T(4) + z[27] / T(2) + -z[69];
z[27] = z[27] * z[45];
z[23] = (T(-1085) * z[21]) / T(16) + T(3) * z[23] + z[26] + z[27];
z[23] = z[2] * z[23];
return z[22] + z[23] + z[24] + z[25] + z[50];
}



template IntegrandConstructorType<double> f_4_56_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_56_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_56_construct (const Kin<qd_real>&);
#endif

}