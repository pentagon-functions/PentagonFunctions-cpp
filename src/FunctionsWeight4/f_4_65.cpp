#include "f_4_65.h"

namespace PentagonFunctions {

template <typename T> T f_4_65_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_65_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_65_W_10 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((kin.v[1] / T(4) + kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[1] + (kin.v[2] / T(4) + -kin.v[4] + T(-1)) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((kin.v[1] / T(4) + kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[1] + (kin.v[2] / T(4) + -kin.v[4] + T(-1)) * kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (-kin.v[1] / T(4) + -kin.v[2] / T(2) + T(1) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(4) + T(1) + kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (-kin.v[1] / T(4) + -kin.v[2] / T(2) + T(1) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(4) + T(1) + kin.v[4]));
c[1] = (-kin.v[1] + -kin.v[2]) * rlog(-kin.v[3] + kin.v[0] + kin.v[1]) + (-kin.v[1] + -kin.v[2]) * rlog(-kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[1] + kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] + kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * (abb[3] + -prod_pow(abb[2], 2) / T(2)) + prod_pow(abb[2], 2) * (abb[6] / T(2) + abb[7] / T(2) + -abb[5] / T(2)) + abb[1] * (abb[1] * ((abb[6] * T(-3)) / T(2) + (abb[7] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2) + (abb[5] * T(3)) / T(2)) + -(abb[2] * abb[4]) + abb[2] * (abb[6] + abb[7] + -abb[5])) + abb[3] * (abb[5] + -abb[6] + -abb[7]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_65_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl24 = DLog_W_24<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),spdl10 = SpDLog_f_4_65_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), dl24(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl5(t), -rlog(t), dl16(t), dl18(t), dl17(t)}
;

        auto result = f_4_65_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_4_65_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[43];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[12];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[13];
z[10] = abb[2];
z[11] = abb[9];
z[12] = bc<TR>[0];
z[13] = abb[8];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = int_to_imaginary<T>(1) * z[12];
z[20] = -z[11] + z[19];
z[21] = z[0] / T(2);
z[22] = z[20] + z[21];
z[22] = z[0] * z[22];
z[23] = z[10] / T(2);
z[24] = -z[11] + z[23];
z[24] = z[10] * z[24];
z[25] = z[16] * z[19];
z[25] = z[15] + z[25];
z[26] = -z[14] + z[25];
z[22] = z[22] + -z[24] + -z[26];
z[24] = z[3] * z[22];
z[27] = T(2) * z[19];
z[28] = -z[11] + z[27];
z[28] = z[11] * z[28];
z[29] = z[25] + z[28];
z[30] = -z[10] + z[19];
z[30] = z[10] * z[30];
z[31] = prod_pow(z[12], 2);
z[32] = (T(7) * z[31]) / T(6);
z[30] = -z[29] + z[30] + -z[32];
z[30] = z[13] * z[30];
z[21] = -z[10] + z[21];
z[33] = T(2) * z[20];
z[34] = z[21] + z[33];
z[34] = z[0] * z[34];
z[34] = T(3) * z[14] + z[34];
z[35] = -z[23] + z[33];
z[35] = z[10] * z[35];
z[36] = z[31] / T(3);
z[35] = -z[34] + z[35] + -z[36];
z[35] = z[4] * z[35];
z[37] = (T(5) * z[31]) / T(6);
z[38] = T(3) * z[25] + -z[28] + -z[37];
z[39] = T(2) * z[11];
z[40] = -z[10] + z[39];
z[41] = -z[19] + -z[40];
z[41] = z[10] * z[41];
z[20] = -z[10] + -z[20];
z[20] = z[0] * z[20];
z[20] = T(2) * z[20] + z[38] + z[41];
z[20] = z[2] * z[20];
z[20] = z[20] + T(3) * z[24] + -z[30] + z[35];
z[20] = z[7] * z[20];
z[35] = -z[19] + z[39];
z[35] = z[10] * z[35];
z[41] = z[0] + z[33];
z[41] = z[0] * z[41];
z[42] = T(2) * z[14];
z[28] = -z[25] + z[28] + z[32] + z[35] + z[41] + z[42];
z[28] = z[2] * z[28];
z[23] = z[23] + -z[39];
z[32] = -(z[10] * z[23]);
z[21] = -z[21] + z[33];
z[21] = z[0] * z[21];
z[25] = T(2) * z[25];
z[21] = z[14] + z[21] + -z[25] + z[32];
z[21] = z[4] * z[21];
z[21] = z[21] + -z[24] + z[28] + z[30];
z[21] = z[6] * z[21];
z[23] = z[19] + z[23];
z[23] = z[10] * z[23];
z[23] = z[23] + z[29] + -z[34] + z[37];
z[23] = z[8] * z[23];
z[24] = -z[6] + -z[7];
z[22] = z[22] * z[24];
z[24] = z[10] * z[40];
z[24] = z[24] + T(-2) * z[26] + z[41];
z[26] = T(2) * z[9];
z[28] = z[24] * z[26];
z[29] = (T(3) * z[10]) / T(2) + -z[19] + -z[39];
z[29] = z[10] * z[29];
z[32] = (T(-3) * z[0]) / T(2) + -z[10] + -z[33];
z[32] = z[0] * z[32];
z[29] = -z[14] + z[29] + z[32] + z[38];
z[29] = z[1] * z[29];
z[19] = z[17] * z[19];
z[19] = z[19] + (T(5) * z[31]) / T(2);
z[19] = z[17] * z[19];
z[31] = int_to_imaginary<T>(1) * prod_pow(z[12], 3);
z[31] = z[31] / T(3);
z[32] = z[18] / T(2) + z[31];
z[32] = -z[19] + z[32] / T(2);
z[22] = z[22] + z[23] + z[28] + z[29] + -z[32] / T(2);
z[22] = z[5] * z[22];
z[23] = -z[0] + T(2) * z[10];
z[23] = z[0] * z[23];
z[27] = -z[10] + z[27];
z[27] = z[10] * z[27];
z[23] = z[23] + -z[25] + z[27] + -z[36] + -z[42];
z[25] = z[2] + -z[4];
z[23] = -(z[23] * z[25]);
z[24] = z[3] * z[24];
z[23] = z[23] + z[24];
z[24] = -(z[23] * z[26]);
z[26] = z[23] + -z[30];
z[26] = z[1] * z[26];
z[23] = z[23] + z[30];
z[23] = z[8] * z[23];
z[19] = (T(21) * z[18]) / T(4) + -z[19] + z[31];
z[19] = -(z[19] * z[25]);
z[25] = z[3] * z[32];
z[19] = z[19] + z[25];
return z[19] / T(2) + z[20] + z[21] + z[22] + z[23] + z[24] + z[26];
}



template IntegrandConstructorType<double> f_4_65_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_65_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_65_construct (const Kin<qd_real>&);
#endif

}