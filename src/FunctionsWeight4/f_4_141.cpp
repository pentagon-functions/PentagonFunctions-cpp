#include "f_4_141.h"

namespace PentagonFunctions {

template <typename T> T f_4_141_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_141_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_141_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(4) + T(-2) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[0] * ((T(-2) * kin.v[1]) / T(3) + (T(11) * kin.v[2]) / T(3) + (T(5) * kin.v[3]) / T(3) + T(4) + (-bc<T>[1] / T(2) + T(-13) / T(6) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + bc<T>[1] * (-kin.v[4] + T(1) + kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[2]) + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + -kin.v[3] + T(-4) + T(3) * kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-4) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4])) + bc<T>[1] * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[1] = (bc<T>[1] + bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + bc<T>[1] * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(-2) * kin.v[3];
c[2] = kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(4) + T(-2) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[0] * ((T(-2) * kin.v[1]) / T(3) + (T(11) * kin.v[2]) / T(3) + (T(5) * kin.v[3]) / T(3) + T(4) + (-bc<T>[1] / T(2) + T(-13) / T(6) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + bc<T>[1] * (-kin.v[4] + T(1) + kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[2]) + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + -kin.v[3] + T(-4) + T(3) * kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-4) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4])) + bc<T>[1] * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[3] = (bc<T>[1] + bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + bc<T>[1] * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(-2) * kin.v[3];
c[4] = kin.v[3] * (-kin.v[3] / T(2) + T(4) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) + kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-4) + T(2) * kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * ((T(2) * kin.v[1]) / T(3) + (T(-11) * kin.v[2]) / T(3) + (T(-5) * kin.v[3]) / T(3) + T(-4) + (bc<T>[1] / T(2) + T(13) / T(6) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(3) * kin.v[4] + bc<T>[1] * (-kin.v[2] + T(-1) + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4])) + bc<T>[1] * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[5] = (-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]) + bc<T>[1] * (-kin.v[1] + kin.v[2] + kin.v[3]);
c[6] = kin.v[3] * (-kin.v[3] / T(2) + T(4) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) + kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-4) + T(2) * kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * ((T(2) * kin.v[1]) / T(3) + (T(-11) * kin.v[2]) / T(3) + (T(-5) * kin.v[3]) / T(3) + T(-4) + (bc<T>[1] / T(2) + T(13) / T(6) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(3) * kin.v[4] + bc<T>[1] * (-kin.v[2] + T(-1) + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4])) + bc<T>[1] * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[7] = (-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]) + bc<T>[1] * (-kin.v[1] + kin.v[2] + kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[1] * (t * c[0] + c[1]) + abb[7] * (t * c[2] + c[3]) + abb[8] * (t * c[4] + c[5]) + abb[9] * (t * c[6] + c[7]);
        }

        return abb[17] * (abb[3] * abb[11] * (abb[8] + abb[9] + -abb[7]) + abb[4] * abb[11] * (abb[8] + abb[9] + -abb[7]) + abb[2] * (abb[1] * abb[11] + abb[16] * (abb[8] + abb[9] + -abb[1] + -abb[7]) + abb[11] * (abb[7] + -abb[8] + -abb[9])) + abb[11] * (abb[11] * (abb[8] + abb[9] + -abb[7]) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[16] * (abb[3] * (abb[7] + -abb[8] + -abb[9]) + abb[4] * (abb[7] + -abb[8] + -abb[9]) + abb[11] * (abb[7] + -abb[8] + -abb[9]) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[1] * (abb[3] + abb[4] + abb[11] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[1] * (-(abb[3] * abb[11]) + -(abb[4] * abb[11]) + abb[18] * T(-2) + abb[11] * (-abb[11] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[18] * (abb[7] * T(-2) + abb[8] * T(2) + abb[9] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_141_construct (const Kin<T>& kin) {
    return [&kin, 
            dl4 = DLog_W_4<T>(kin),dl8 = DLog_W_8<T>(kin),dl13 = DLog_W_13<T>(kin),dl6 = DLog_W_6<T>(kin),dl25 = DLog_W_25<T>(kin),dl21 = DLog_W_21<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl31 = DLog_W_31<T>(kin),dl16 = DLog_W_16<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin),spdl21 = SpDLog_f_4_141_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,46> abbr = 
            {dl4(t), f_1_3_2(kin) - f_1_3_2(kin_path), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), f_2_1_9(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl8(t), rlog(-v_path[2] + v_path[0] + v_path[4]), dl13(t), dl6(t), f_1_3_1(kin) - f_1_3_1(kin_path), dl25(t), rlog(-v_path[1] + v_path[3] + v_path[4]), dl21(t), f_2_1_15(kin_path), dl5(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl17(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl31(t), dl16(t), dl2(t), dl1(t), dl3(t), dl18(t), dl20(t), dl19(t), dl30(t) / kin_path.SqrtDelta, rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_141_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_4_141_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[129];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[12];
z[12] = abb[13];
z[13] = abb[19];
z[14] = abb[22];
z[15] = abb[28];
z[16] = abb[31];
z[17] = abb[32];
z[18] = abb[11];
z[19] = abb[29];
z[20] = abb[30];
z[21] = abb[16];
z[22] = abb[26];
z[23] = abb[15];
z[24] = abb[27];
z[25] = abb[18];
z[26] = abb[20];
z[27] = abb[21];
z[28] = abb[23];
z[29] = abb[24];
z[30] = abb[14];
z[31] = abb[34];
z[32] = abb[42];
z[33] = abb[43];
z[34] = abb[44];
z[35] = abb[35];
z[36] = abb[36];
z[37] = abb[37];
z[38] = abb[38];
z[39] = abb[39];
z[40] = abb[40];
z[41] = abb[41];
z[42] = abb[45];
z[43] = abb[10];
z[44] = abb[33];
z[45] = abb[25];
z[46] = bc<TR>[1];
z[47] = bc<TR>[3];
z[48] = bc<TR>[5];
z[49] = bc<TR>[2];
z[50] = bc<TR>[4];
z[51] = bc<TR>[7];
z[52] = bc<TR>[8];
z[53] = bc<TR>[9];
z[54] = z[9] + z[10];
z[55] = z[8] + z[54];
z[55] = z[43] * z[55];
z[56] = z[1] + z[8];
z[57] = z[15] * z[56];
z[58] = z[55] + -z[57];
z[59] = z[1] * z[16];
z[60] = z[8] * z[16];
z[59] = z[59] + z[60];
z[61] = z[8] + z[9];
z[62] = z[20] * z[61];
z[63] = z[1] + -z[9];
z[64] = -z[10] + z[63];
z[65] = z[17] * z[64];
z[66] = z[11] * z[63];
z[67] = z[58] + -z[59] + z[62] + z[65] + z[66];
z[67] = z[3] * z[67];
z[68] = z[19] * z[61];
z[69] = z[30] + z[63];
z[70] = -z[8] + z[69];
z[70] = z[23] * z[70];
z[71] = z[68] + -z[70];
z[72] = z[17] * z[69];
z[69] = z[15] * z[69];
z[73] = z[30] + z[56];
z[73] = z[12] * z[73];
z[74] = -z[60] + z[69] + z[71] + z[72] + -z[73];
z[74] = z[4] * z[74];
z[67] = z[67] + z[74];
z[74] = T(3) * z[1];
z[75] = T(3) * z[8];
z[76] = z[74] + z[75];
z[77] = -z[54] + z[76];
z[78] = z[13] * z[77];
z[79] = z[16] * z[64];
z[80] = z[60] + z[79];
z[81] = z[78] + z[80];
z[82] = z[19] + z[20];
z[83] = T(2) * z[61];
z[83] = z[82] * z[83];
z[84] = z[38] + z[39] + z[40] + z[41];
z[85] = z[31] + z[35];
z[85] = -z[37] + z[85] / T(2);
z[86] = (T(3) * z[84]) / T(2) + -z[85];
z[87] = T(-2) * z[36] + z[86];
z[88] = z[33] * z[87];
z[83] = z[83] + z[88];
z[89] = z[32] * z[87];
z[90] = T(3) * z[63];
z[91] = z[8] + -z[10];
z[92] = z[90] + z[91];
z[92] = z[30] + z[92] / T(2);
z[93] = z[14] * z[92];
z[89] = z[89] + -z[93];
z[94] = -z[66] + z[73];
z[95] = z[63] + z[91];
z[96] = z[95] / T(2);
z[97] = z[17] * z[96];
z[98] = z[30] + -z[61];
z[98] = z[15] * z[98];
z[94] = z[81] / T(2) + -z[83] + z[89] + T(2) * z[94] + -z[97] + z[98];
z[94] = z[2] * z[94];
z[98] = -z[64] + z[75];
z[99] = z[24] * z[98];
z[100] = z[34] * z[87];
z[101] = z[99] / T(2) + -z[100];
z[102] = z[19] * z[96];
z[103] = z[97] + z[102];
z[104] = z[8] * z[20];
z[105] = z[60] + z[104];
z[106] = z[15] * z[96];
z[105] = z[101] + -z[103] + T(-2) * z[105] + z[106];
z[105] = z[18] * z[105];
z[107] = z[54] + z[74];
z[107] = z[16] * z[107];
z[77] = z[15] * z[77];
z[108] = z[16] * z[75];
z[109] = -z[78] + z[108];
z[110] = -z[8] + T(3) * z[64];
z[111] = z[17] * z[110];
z[112] = z[19] * z[110];
z[77] = z[77] + z[107] + z[109] + -z[111] + -z[112];
z[77] = z[77] / T(2);
z[107] = z[44] * z[87];
z[110] = z[22] * z[110];
z[111] = z[110] / T(2);
z[107] = z[107] + z[111];
z[113] = z[77] + z[88] + z[100] + z[107];
z[113] = z[27] * z[113];
z[114] = z[20] * z[95];
z[114] = z[80] + z[114];
z[115] = z[42] * z[87];
z[88] = -z[88] + z[115];
z[116] = z[88] + z[114] / T(2);
z[117] = z[19] * z[64];
z[65] = z[65] + z[117];
z[106] = T(-2) * z[65] + z[106] + z[107] + -z[116];
z[117] = -(z[21] * z[106]);
z[108] = -z[79] + z[108];
z[118] = z[29] * z[108];
z[119] = z[29] * z[98];
z[120] = prod_pow(z[46], 2);
z[121] = (T(3) * z[120]) / T(10);
z[122] = z[119] + z[121];
z[122] = z[20] * z[122];
z[123] = z[120] / T(10);
z[119] = -z[119] + -z[123];
z[119] = z[24] * z[119];
z[121] = z[16] * z[121];
z[118] = z[118] + z[119] + z[121] + z[122];
z[119] = -(z[29] * z[87]);
z[121] = z[46] * z[49];
z[121] = -z[119] + z[121];
z[122] = z[50] + z[121];
z[124] = -(z[2] * z[87]);
z[124] = z[122] + z[124];
z[124] = z[34] * z[124];
z[125] = (T(3) * z[19]) / T(20) + -z[22] / T(20) + -z[45] / T(5);
z[125] = z[120] * z[125];
z[90] = z[90] + -z[91];
z[90] = T(2) * z[30] + z[90] / T(2);
z[126] = -(z[29] * z[90]);
z[126] = (T(3) * z[120]) / T(20) + z[126];
z[126] = z[17] * z[126];
z[127] = -(z[29] * z[92]);
z[123] = z[123] + z[127];
z[123] = z[15] * z[123];
z[119] = z[42] * z[119];
z[127] = z[29] * z[93];
z[120] = T(3) * z[50] + T(2) * z[120] + -z[121];
z[120] = z[32] * z[120];
z[121] = z[33] * z[122];
z[122] = z[33] + z[34];
z[82] = z[16] + z[17] + z[82];
z[128] = z[15] / T(3) + z[82] / T(2);
z[128] = (T(11) * z[24]) / T(90) + -z[42] + z[44] + z[122] + (T(-11) * z[128]) / T(15);
z[128] = (T(-11) * z[22]) / T(180) + -z[32] + (T(-11) * z[45]) / T(45) + -z[128] / T(2);
z[128] = prod_pow(z[7], 2) * z[128];
z[67] = T(2) * z[67] + z[94] + z[105] + z[113] + z[117] + z[118] / T(2) + z[119] + z[120] + z[121] + z[123] + z[124] + z[125] + z[126] + z[127] + z[128] / T(3);
z[67] = z[7] * z[67];
z[94] = z[22] + z[24];
z[113] = T(2) * z[15] + T(-4) * z[45] + T(3) * z[82] + -z[94];
z[117] = -(z[46] * z[47] * z[113]);
z[113] = -(z[48] * z[113]);
z[67] = z[67] + (T(12) * z[113]) / T(5) + z[117];
z[67] = int_to_imaginary<T>(1) * z[67];
z[113] = -z[78] + z[80];
z[93] = -z[73] + z[93] / T(2);
z[117] = z[17] * z[95];
z[62] = z[62] + z[117] / T(4);
z[86] = -z[36] + z[86] / T(2);
z[118] = z[33] * z[86];
z[119] = T(3) * z[9];
z[75] = z[75] + z[119];
z[120] = -z[1] + z[10];
z[121] = z[75] + z[120];
z[123] = z[0] * z[121];
z[124] = z[123] / T(2);
z[125] = z[30] + z[120];
z[125] = z[61] + -z[125] / T(2);
z[125] = z[15] * z[125];
z[126] = -(z[32] * z[86]);
z[68] = -z[62] + z[66] + -z[68] + z[93] + z[113] / T(4) + -z[118] + z[124] + z[125] + z[126];
z[113] = prod_pow(z[2], 2);
z[68] = z[68] * z[113];
z[75] = z[75] + -z[120];
z[125] = z[20] / T(2);
z[126] = -(z[75] * z[125]);
z[127] = -(z[17] * z[92]);
z[128] = z[121] / T(2);
z[128] = -(z[19] * z[128]);
z[119] = -z[1] + z[91] + z[119];
z[119] = -z[30] + z[119] / T(2);
z[119] = z[15] * z[119];
z[73] = -z[73] + -z[89] + -z[115] + z[119] + z[124] + z[126] + z[127] + z[128];
z[73] = z[6] * z[73];
z[119] = z[61] + -z[120];
z[126] = z[15] / T(2);
z[119] = z[119] * z[126];
z[83] = z[80] / T(2) + -z[83] + -z[97] + z[119] + z[124];
z[97] = z[2] * z[83];
z[119] = z[2] * z[100];
z[97] = z[97] + -z[119];
z[96] = z[20] * z[96];
z[57] = z[57] + T(2) * z[59] + -z[78] / T(2) + z[96] + z[103] + z[115];
z[59] = z[57] + z[100];
z[59] = z[18] * z[59];
z[56] = z[54] + z[56];
z[56] = z[15] * z[56];
z[56] = z[56] + z[81] + z[112];
z[78] = z[44] * z[86];
z[78] = z[78] + z[110] / T(4) + z[118];
z[81] = z[34] * z[86];
z[56] = -z[55] + z[56] / T(4) + -z[62] + -z[66] + -z[78] + -z[81];
z[56] = z[3] * z[56];
z[56] = z[56] + z[59] + -z[97];
z[56] = z[3] * z[56];
z[59] = z[20] * z[98];
z[62] = -z[59] + z[80] + -z[117];
z[80] = -z[81] + z[99] / T(4);
z[81] = z[32] + z[42];
z[81] = z[81] * z[86];
z[63] = -z[63] + z[91];
z[63] = -z[30] + z[63] / T(2);
z[91] = z[63] * z[126];
z[62] = z[62] / T(4) + -z[71] + z[80] + z[81] + z[91] + -z[93] + -z[118];
z[62] = z[4] * z[62];
z[71] = z[83] + -z[100];
z[71] = z[3] * z[71];
z[62] = z[62] + z[71] + -z[97] + -z[105];
z[62] = z[4] * z[62];
z[71] = z[19] * z[95];
z[81] = z[8] + T(5) * z[64];
z[83] = z[17] * z[81];
z[71] = z[71] + z[83] + z[114];
z[63] = -(z[15] * z[63]);
z[91] = z[42] * z[86];
z[63] = z[63] + -z[70] + z[71] / T(4) + -z[78] + -z[80] + z[91];
z[63] = z[21] * z[63];
z[69] = z[69] + T(2) * z[72] + z[89] + z[102] + z[116];
z[71] = z[2] + -z[4];
z[69] = z[69] * z[71];
z[71] = z[3] * z[106];
z[63] = z[63] + z[69] + z[71] + z[105];
z[63] = z[21] * z[63];
z[60] = T(5) * z[60] + z[79];
z[69] = T(7) * z[8] + T(5) * z[9] + z[120];
z[69] = z[69] * z[125];
z[69] = -z[60] + z[69] + z[83];
z[71] = -z[61] + -z[120] / T(3);
z[72] = z[0] / T(2);
z[71] = z[71] * z[72];
z[72] = (T(5) * z[8]) / T(3) + (T(7) * z[9]) / T(3) + z[120];
z[72] = z[19] * z[72];
z[69] = z[69] / T(3) + z[71] + z[72] / T(2);
z[71] = -z[8] + z[64] / T(3);
z[71] = z[24] * z[71];
z[72] = -z[8] / T(3) + z[64];
z[72] = z[22] * z[72];
z[71] = z[71] + z[72];
z[72] = z[84] / T(2) + -z[85] / T(3);
z[78] = z[46] / T(6) + (T(5) * z[49]) / T(3) + z[72];
z[78] = -z[36] / T(3) + z[78] / T(2);
z[79] = -z[42] + z[44];
z[78] = z[78] * z[79];
z[72] = (T(2) * z[36]) / T(3) + (T(7) * z[46]) / T(6) + (T(-7) * z[49]) / T(3) + -z[72];
z[72] = -(z[72] * z[122]);
z[61] = T(-13) * z[61] + z[120];
z[61] = z[30] + z[61] / T(12);
z[61] = z[15] * z[61];
z[79] = (T(13) * z[46]) / T(2) + T(-7) * z[49];
z[79] = z[32] * z[79];
z[55] = z[55] + z[61] + z[69] / T(2) + -z[70] + z[71] / T(4) + z[72] + z[78] + z[79] / T(6);
z[55] = z[7] * z[55];
z[61] = (T(2) * z[15]) / T(3) + (T(-4) * z[45]) / T(3) + z[82] + -z[94] / T(3);
z[61] = z[47] * z[61];
z[55] = z[55] + z[61];
z[55] = z[7] * z[55];
z[59] = -z[59] + -z[108];
z[61] = z[17] * z[90];
z[69] = z[15] * z[92];
z[59] = z[59] / T(2) + z[61] + z[69] + z[88] + z[89] + z[101];
z[59] = z[28] * z[59];
z[54] = -z[54] + z[74];
z[54] = z[16] * z[54];
z[61] = -(z[20] * z[121]);
z[69] = -(z[19] * z[75]);
z[70] = z[9] + -z[10] + z[76];
z[70] = z[15] * z[70];
z[54] = z[54] + z[61] + z[69] + z[70] + z[109] + z[123];
z[54] = z[54] / T(2) + z[66] + z[88];
z[54] = z[5] * z[54];
z[57] = -(z[2] * z[57]);
z[58] = z[58] + z[65] + z[104];
z[58] = z[18] * z[58];
z[57] = z[57] + z[58] + -z[119];
z[57] = z[18] * z[57];
z[58] = T(5) * z[8] + z[64];
z[58] = z[20] * z[58];
z[61] = z[19] * z[81];
z[58] = z[58] + z[60] + z[61] + z[83];
z[60] = -(z[15] * z[95]);
z[58] = z[58] / T(2) + z[60] + z[88] + -z[101] + -z[107];
z[58] = z[25] * z[58];
z[60] = -z[77] + -z[111];
z[60] = z[26] * z[60];
z[61] = z[26] * z[87];
z[64] = z[46] * z[50];
z[65] = prod_pow(z[46], 3);
z[66] = prod_pow(z[49], 3);
z[69] = T(5) * z[51] + z[52] + z[61] + T(3) * z[64] + (T(2) * z[65]) / T(3) + (T(-4) * z[66]) / T(3);
z[70] = -(z[33] * z[69]);
z[71] = -(z[6] * z[87]);
z[72] = -(z[86] * z[113]);
z[69] = -z[69] + z[71] + z[72];
z[69] = z[34] * z[69];
z[66] = z[52] / T(2) + (T(-2) * z[66]) / T(3);
z[65] = z[65] / T(6);
z[64] = T(2) * z[51] + z[64] + z[65] + z[66];
z[71] = -(z[42] * z[64]);
z[61] = -z[61] + z[64];
z[61] = z[44] * z[61];
z[64] = z[51] + -z[65] + z[66];
z[64] = z[32] * z[64];
z[65] = T(-7) * z[32] + T(67) * z[42];
z[65] = (T(-67) * z[44]) / T(2) + z[65] / T(2) + T(11) * z[122];
z[65] = z[53] * z[65];
return z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] / T(6) + z[67] + z[68] + z[69] + z[70] + z[71] + z[73];
}



template IntegrandConstructorType<double> f_4_141_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_141_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_141_construct (const Kin<qd_real>&);
#endif

}