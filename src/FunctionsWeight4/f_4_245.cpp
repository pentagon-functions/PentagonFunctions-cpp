#include "f_4_245.h"

namespace PentagonFunctions {

template <typename T> T f_4_245_abbreviated (const std::array<T,41>&);



template <typename T> IntegrandConstructorType<T> f_4_245_construct (const Kin<T>& kin) {
    return [&kin, 
            dl11 = DLog_W_11<T>(kin),dl24 = DLog_W_24<T>(kin),dl29 = DLog_W_29<T>(kin),dl20 = DLog_W_20<T>(kin),dl30 = DLog_W_30<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl6 = DLog_W_6<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,41> abbr = 
            {dl11(t), rlog(v_path[0]), rlog(v_path[3]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[17] / kin_path.W[17]), dl24(t), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), dl29(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_6(kin_path), f_2_1_8(kin_path), rlog(v_path[0] + v_path[1]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl20(t), dl30(t) / kin_path.SqrtDelta, f_2_1_2(kin_path), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl2(t), dl5(t), dl26(t) / kin_path.SqrtDelta, f_2_1_9(kin_path), dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl18(t), rlog(kin.W[5] / kin_path.W[5]), rlog(kin.W[15] / kin_path.W[15]), dl4(t), dl16(t), dl6(t), dl17(t), dl1(t), dl3(t), dl19(t)}
;

        auto result = f_4_245_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_245_abbreviated(const std::array<T,41>& abb)
{
using TR = typename T::value_type;
T z[140];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[3];
z[3] = abb[4];
z[4] = abb[5];
z[5] = abb[6];
z[6] = bc<TR>[0];
z[7] = abb[2];
z[8] = abb[25];
z[9] = abb[31];
z[10] = abb[34];
z[11] = abb[35];
z[12] = abb[36];
z[13] = abb[37];
z[14] = abb[38];
z[15] = abb[39];
z[16] = abb[40];
z[17] = abb[10];
z[18] = abb[11];
z[19] = abb[13];
z[20] = abb[21];
z[21] = abb[27];
z[22] = abb[30];
z[23] = abb[18];
z[24] = abb[19];
z[25] = abb[32];
z[26] = abb[33];
z[27] = abb[8];
z[28] = abb[9];
z[29] = abb[14];
z[30] = abb[29];
z[31] = abb[20];
z[32] = abb[12];
z[33] = abb[7];
z[34] = abb[26];
z[35] = abb[15];
z[36] = abb[16];
z[37] = abb[17];
z[38] = abb[22];
z[39] = abb[23];
z[40] = abb[24];
z[41] = abb[28];
z[42] = bc<TR>[1];
z[43] = bc<TR>[3];
z[44] = bc<TR>[5];
z[45] = bc<TR>[2];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = bc<TR>[9];
z[50] = T(3) * z[18];
z[51] = T(3) * z[3];
z[52] = z[50] + z[51];
z[53] = z[2] + z[17];
z[54] = T(4) * z[25];
z[55] = T(2) * z[26] + z[54];
z[56] = z[4] + z[5];
z[57] = z[52] + -z[53] + -z[55] + -z[56];
z[58] = z[12] * z[57];
z[59] = T(2) * z[58];
z[60] = T(2) * z[25] + z[26];
z[61] = z[51] + z[53] + T(-2) * z[56] + -z[60];
z[62] = T(2) * z[9];
z[63] = T(2) * z[10] + z[62];
z[63] = z[61] * z[63];
z[64] = T(5) * z[2];
z[65] = -z[56] + z[64];
z[66] = (T(3) * z[18]) / T(2);
z[67] = (T(3) * z[3]) / T(2);
z[68] = z[66] + z[67];
z[69] = z[17] / T(2);
z[70] = -z[55] + -z[65] / T(2) + z[68] + z[69];
z[71] = -(z[11] * z[70]);
z[72] = z[2] + z[56];
z[72] = z[69] + z[72] / T(2);
z[68] = z[60] + -z[68] + z[72];
z[73] = z[13] * z[68];
z[74] = T(3) * z[2] + -z[56];
z[74] = z[74] / T(2);
z[75] = (T(3) * z[17]) / T(2);
z[76] = z[74] + z[75];
z[77] = z[3] / T(2);
z[78] = -z[60] + z[77];
z[79] = (T(5) * z[18]) / T(2) + -z[76] + z[78];
z[80] = z[16] * z[79];
z[81] = z[19] + z[23] + z[24];
z[82] = z[20] * z[81];
z[83] = z[21] * z[81];
z[84] = z[82] + z[83];
z[85] = z[22] * z[81];
z[86] = -z[84] + z[85];
z[87] = z[86] / T(2);
z[88] = -z[3] + z[56];
z[89] = -z[2] + z[88];
z[90] = z[0] * z[89];
z[91] = z[80] + -z[87] + T(2) * z[90];
z[92] = T(2) * z[2] + -z[56];
z[93] = z[26] + z[92];
z[54] = z[17] + -z[50] + z[54] + T(2) * z[93];
z[54] = z[14] * z[54];
z[93] = -z[17] + z[18];
z[89] = z[89] + -z[93];
z[94] = z[8] * z[89];
z[95] = (T(3) * z[94]) / T(2);
z[54] = T(2) * z[54] + -z[59] + z[63] + z[71] + z[73] + T(3) * z[91] + -z[95];
z[54] = z[1] * z[54];
z[71] = z[2] + z[4];
z[91] = -z[3] + -z[5] + z[71];
z[96] = z[91] + -z[93];
z[97] = z[11] * z[96];
z[98] = z[34] * z[96];
z[97] = z[97] + -z[98];
z[99] = z[13] + z[14];
z[79] = z[79] * z[99];
z[100] = z[30] * z[81];
z[80] = -z[80] + z[100] / T(2);
z[101] = z[18] / T(2) + z[69];
z[78] = z[78] + z[101];
z[102] = T(3) * z[5] + -z[71];
z[102] = -z[78] + z[102] / T(2);
z[102] = z[9] * z[102];
z[79] = z[79] + z[80] + z[87] + -z[97] / T(2) + z[102];
z[79] = T(3) * z[79];
z[87] = z[40] * z[79];
z[97] = T(3) * z[17];
z[102] = -z[50] + z[97];
z[103] = T(3) * z[4];
z[51] = z[51] + z[102] + -z[103];
z[104] = z[2] + -z[5];
z[105] = z[51] + z[104];
z[106] = z[11] * z[105];
z[107] = -z[82] + z[100];
z[91] = z[91] + z[93];
z[108] = z[31] * z[91];
z[81] = z[32] * z[81];
z[109] = z[81] + -z[108];
z[110] = z[107] + -z[109];
z[111] = z[106] + T(3) * z[110];
z[112] = z[9] * z[68];
z[113] = z[14] * z[68];
z[114] = T(2) * z[104];
z[115] = z[13] * z[114];
z[116] = z[10] * z[114];
z[111] = -z[111] / T(2) + -z[112] + z[113] + z[115] + z[116];
z[111] = z[27] * z[111];
z[88] = z[18] + -z[53] + z[88];
z[115] = z[11] * z[88];
z[109] = z[84] + -z[94] + z[109] + z[115];
z[117] = (T(3) * z[37]) / T(2);
z[118] = -(z[109] * z[117]);
z[119] = z[51] + -z[104];
z[120] = z[10] * z[119];
z[105] = z[13] * z[105];
z[121] = -z[105] + z[120];
z[121] = -z[121] / T(2);
z[70] = -(z[14] * z[70]);
z[122] = -z[83] + z[98];
z[123] = (T(3) * z[122]) / T(2);
z[124] = -z[17] + z[60];
z[71] = T(2) * z[5] + -z[71] + z[124];
z[125] = z[62] * z[71];
z[114] = z[11] * z[114];
z[70] = z[70] + z[114] + -z[121] + -z[123] + -z[125];
z[70] = z[29] * z[70];
z[114] = z[9] * z[61];
z[126] = -z[11] + z[13];
z[126] = z[104] * z[126];
z[124] = z[92] + z[124];
z[127] = z[14] * z[124];
z[116] = T(-3) * z[90] + -z[114] + -z[116] + z[126] + -z[127];
z[116] = z[7] * z[116];
z[126] = z[21] + z[22] + z[30];
z[128] = -z[126] / T(20);
z[129] = prod_pow(z[42], 2);
z[128] = z[128] * z[129];
z[130] = T(5) * z[46];
z[131] = (T(7) * z[129]) / T(2);
z[132] = -z[130] + -z[131];
z[132] = z[16] * z[132];
z[128] = z[128] + z[132];
z[132] = z[9] + -z[10] + -z[11];
z[133] = -z[14] + z[15];
z[134] = -z[12] + z[133];
z[132] = (T(-5) * z[13]) / T(4) + T(2) * z[16] + (T(-11) * z[126]) / T(180) + (T(3) * z[132]) / T(4) + (T(3) * z[134]) / T(2);
z[134] = prod_pow(z[6], 2);
z[132] = z[132] * z[134];
z[135] = -(z[91] * z[117]);
z[135] = T(8) * z[46] + T(5) * z[129] + z[135];
z[135] = z[13] * z[135];
z[136] = -(z[89] * z[117]);
z[131] = -z[46] + z[131] + z[136];
z[131] = z[14] * z[131];
z[117] = z[88] * z[117];
z[136] = T(4) * z[46] + z[129];
z[117] = z[117] + T(2) * z[136];
z[117] = z[10] * z[117];
z[62] = z[62] * z[136];
z[136] = T(7) * z[46] + (T(11) * z[129]) / T(2);
z[136] = z[11] * z[136];
z[137] = z[46] + z[129] / T(2);
z[138] = z[15] * z[137];
z[139] = T(14) * z[46] + T(11) * z[129];
z[139] = z[12] * z[139];
z[54] = z[54] + z[62] + z[70] + z[87] + z[111] + T(2) * z[116] + z[117] + z[118] + T(3) * z[128] + z[131] + z[132] + z[135] + z[136] + T(-15) * z[138] + z[139];
z[54] = z[6] * z[54];
z[62] = z[42] * z[43];
z[62] = (T(12) * z[44]) / T(5) + z[62];
z[62] = z[62] * z[126];
z[54] = z[54] + T(3) * z[62];
z[54] = int_to_imaginary<T>(1) * z[54];
z[62] = T(11) * z[25] + (T(11) * z[26]) / T(2);
z[70] = T(11) * z[5];
z[53] = (T(17) * z[4]) / T(2) + (T(-25) * z[42]) / T(4) + (T(11) * z[53]) / T(2) + -z[62] + -z[70];
z[53] = -z[3] + z[53] / T(3);
z[53] = z[9] * z[53];
z[87] = z[108] / T(4);
z[93] = z[93] + -z[104];
z[108] = T(3) * z[33];
z[93] = z[93] * z[108];
z[108] = z[84] + z[85];
z[116] = -z[81] + -z[108];
z[117] = z[3] / T(4);
z[118] = (T(5) * z[18]) / T(4) + -z[25] + z[117];
z[128] = -z[26] + -z[74];
z[128] = (T(-3) * z[17]) / T(4) + T(-3) * z[42] + z[118] + z[128] / T(2);
z[128] = z[16] * z[128];
z[102] = -z[3] + z[4] + (T(19) * z[42]) / T(3) + z[102] + (T(25) * z[104]) / T(3);
z[131] = z[11] / T(4);
z[102] = z[102] * z[131];
z[132] = (T(5) * z[17]) / T(2);
z[135] = (T(17) * z[2]) / T(3) + (T(-11) * z[5]) / T(3) + -z[103];
z[135] = z[26] + (T(17) * z[42]) / T(6) + z[132] + z[135] / T(2);
z[117] = (T(-7) * z[18]) / T(4) + z[25] + z[117] + z[135] / T(2);
z[117] = z[13] * z[117];
z[135] = T(7) * z[56];
z[62] = (T(25) * z[2]) / T(2) + T(5) * z[17] + T(11) * z[42] + z[62] + -z[135];
z[62] = (T(-7) * z[18]) / T(2) + z[62] / T(3) + z[77];
z[62] = z[14] * z[62];
z[77] = (T(-31) * z[2]) / T(3) + T(5) * z[4] + (T(37) * z[5]) / T(3);
z[75] = z[26] + (T(-25) * z[42]) / T(6) + -z[75] + z[77] / T(2);
z[77] = z[18] / T(4);
z[75] = (T(-7) * z[3]) / T(4) + z[25] + z[75] / T(2) + z[77];
z[75] = z[10] * z[75];
z[136] = -z[2] + T(3) * z[56];
z[136] = z[136] / T(2);
z[138] = -z[26] + z[42] + z[69] + -z[136];
z[77] = (T(5) * z[3]) / T(4) + -z[25] + z[77] + z[138] / T(2);
z[77] = z[15] * z[77];
z[138] = z[12] * z[42];
z[53] = z[53] + z[62] + z[75] + z[77] + z[87] + z[93] + z[102] + z[116] / T(4) + z[117] + z[128] + (T(19) * z[138]) / T(6);
z[53] = z[6] * z[53];
z[62] = -(z[43] * z[126]);
z[53] = z[53] + z[62];
z[53] = z[6] * z[53];
z[61] = z[10] * z[61];
z[62] = T(7) * z[2];
z[75] = -z[56] + -z[62];
z[77] = z[3] + z[18];
z[102] = z[17] / T(4);
z[55] = -z[55] + z[75] / T(4) + (T(9) * z[77]) / T(4) + -z[102];
z[55] = z[11] * z[55];
z[75] = (T(5) * z[3]) / T(2) + -z[60] + z[101] + -z[136];
z[77] = z[15] * z[75];
z[101] = T(3) * z[77];
z[76] = z[26] + z[76];
z[76] = z[76] / T(2) + -z[118];
z[76] = z[16] * z[76];
z[108] = z[76] + -z[90] + z[108] / T(4);
z[62] = T(5) * z[56] + -z[62];
z[62] = -z[60] + z[62] / T(2) + -z[67];
z[67] = z[62] + z[66] + -z[69];
z[67] = z[14] * z[67];
z[116] = (T(3) * z[94]) / T(4);
z[72] = z[26] + z[72];
z[117] = (T(3) * z[3]) / T(4);
z[118] = (T(3) * z[18]) / T(4) + z[117];
z[72] = z[25] + z[72] / T(2) + -z[118];
z[126] = z[13] * z[72];
z[55] = z[55] + z[59] + z[61] + z[67] + -z[101] + T(3) * z[108] + z[114] + z[116] + z[126];
z[55] = z[1] * z[55];
z[50] = T(2) * z[17] + -z[50] + z[60] + z[92];
z[50] = z[50] * z[99];
z[59] = z[11] * z[68];
z[67] = z[10] * z[68];
z[68] = z[82] + -z[85];
z[68] = z[68] / T(2) + -z[80];
z[68] = T(2) * z[50] + -z[58] + z[59] + -z[67] + T(3) * z[68] + -z[112];
z[68] = z[27] * z[68];
z[82] = z[83] + z[100];
z[82] = (T(3) * z[82]) / T(2) + -z[95] + z[112];
z[73] = z[58] + z[73];
z[92] = -(z[11] * z[124]);
z[92] = z[67] + z[73] + z[82] + z[92] + T(-2) * z[127];
z[92] = z[29] * z[92];
z[55] = z[55] + z[68] + z[92];
z[55] = z[1] * z[55];
z[51] = T(11) * z[2] + z[51] + -z[70];
z[51] = z[10] * z[51];
z[51] = -z[51] + z[105];
z[65] = z[60] + z[65] / T(4) + -z[102] + -z[118];
z[65] = z[14] * z[65];
z[68] = z[81] + z[84];
z[68] = z[68] / T(4) + -z[87] + z[90];
z[70] = -(z[119] * z[131]);
z[51] = -z[51] / T(4) + z[65] + T(3) * z[68] + z[70] + z[114] + -z[116];
z[51] = z[7] * z[51];
z[65] = z[73] + z[113];
z[59] = -z[59] + z[63] + z[65] + (T(3) * z[84]) / T(2) + -z[101];
z[63] = -(z[1] * z[59]);
z[68] = z[11] * z[104];
z[68] = z[68] + -z[82] + z[121] + z[127];
z[68] = z[29] * z[68];
z[51] = z[51] + z[63] + z[68] + -z[111];
z[51] = z[7] * z[51];
z[52] = z[52] + -z[97] + -z[103] + z[104];
z[52] = z[52] * z[131];
z[63] = z[86] + z[98] + z[100];
z[63] = z[63] / T(4) + z[76];
z[68] = z[9] * z[72];
z[70] = -z[2] + T(-5) * z[5] + z[103];
z[70] = (T(-9) * z[17]) / T(2) + T(-3) * z[26] + z[70] / T(2);
z[70] = (T(15) * z[18]) / T(4) + T(-3) * z[25] + z[70] / T(2) + z[117];
z[70] = z[13] * z[70];
z[72] = -(z[14] * z[72]);
z[73] = -(z[10] * z[104]);
z[52] = z[52] + T(3) * z[63] + z[68] + z[70] + z[72] + z[73] + z[93];
z[52] = prod_pow(z[27], 2) * z[52];
z[63] = z[11] * z[71];
z[63] = z[63] + z[125];
z[50] = -z[50] + -z[58] + z[61] + z[63] + -z[93];
z[58] = int_to_imaginary<T>(1) * z[6];
z[61] = z[28] + T(-2) * z[58];
z[50] = z[50] * z[61];
z[61] = -z[1] + z[7];
z[59] = z[59] * z[61];
z[61] = -z[63] + z[65] + z[67] + -z[123];
z[63] = z[27] + -z[29];
z[61] = z[61] * z[63];
z[50] = z[50] + z[59] + z[61];
z[50] = z[28] * z[50];
z[59] = -z[110] + z[122];
z[61] = z[13] * z[88];
z[63] = -(z[10] * z[91]);
z[65] = z[9] * z[96];
z[61] = -z[59] + z[61] + z[63] + z[65] + -z[115];
z[61] = z[36] * z[61];
z[63] = z[13] * z[91];
z[65] = z[14] * z[89];
z[67] = -(z[10] * z[88]);
z[63] = z[63] + z[65] + z[67] + z[109];
z[63] = z[35] * z[63];
z[61] = z[61] + z[63];
z[59] = T(-3) * z[59] + -z[105] + z[106] + z[120];
z[56] = -z[26] + z[56];
z[56] = -z[2] + -z[25] + z[56] / T(2) + z[69];
z[56] = z[14] * z[56];
z[56] = z[56] + z[59] / T(4) + z[68];
z[56] = z[29] * z[56];
z[56] = z[56] + z[111];
z[56] = z[29] * z[56];
z[59] = z[39] * z[79];
z[63] = -(z[11] * z[89]);
z[63] = z[63] + z[94] + -z[107];
z[65] = z[9] + z[10];
z[67] = z[65] * z[75];
z[68] = z[74] + -z[78];
z[68] = z[14] * z[68];
z[63] = z[63] / T(2) + z[67] + z[68] + -z[77];
z[63] = z[38] * z[63];
z[67] = z[83] + z[85];
z[67] = z[67] / T(2) + z[80];
z[67] = z[41] * z[67];
z[68] = z[42] * z[137];
z[68] = -z[48] / T(2) + z[68];
z[68] = z[16] * z[68];
z[63] = z[63] + z[67] + z[68];
z[64] = -z[64] + z[135];
z[60] = (T(-9) * z[3]) / T(2) + z[60] + z[64] / T(2) + z[66] + -z[132];
z[60] = z[41] * z[60];
z[64] = T(9) * z[46] + (T(13) * z[129]) / T(6);
z[64] = z[42] * z[64];
z[60] = T(10) * z[47] + T(2) * z[48] + z[60] + z[64];
z[60] = -(z[60] * z[65]);
z[57] = z[41] * z[57];
z[64] = T(6) * z[46] + (T(7) * z[129]) / T(3);
z[64] = z[42] * z[64];
z[65] = T(8) * z[47];
z[57] = z[48] + -z[57] + z[64] + z[65];
z[64] = z[11] + T(2) * z[12];
z[57] = -(z[57] * z[64]);
z[66] = z[10] + z[12];
z[67] = (T(11) * z[9]) / T(3) + z[11] / T(3) + (T(-23) * z[13]) / T(6) + (T(-10) * z[14]) / T(3) + -z[15] + (T(7) * z[16]) / T(2) + (T(2) * z[66]) / T(3);
z[67] = z[67] * z[134];
z[64] = T(-2) * z[13] + T(3) * z[16] + -z[64] + z[133];
z[58] = z[42] * z[58] * z[64];
z[64] = -z[11] + z[14];
z[66] = z[9] + z[66];
z[64] = (T(-5) * z[13]) / T(3) + T(-2) * z[15] + z[16] + (T(-2) * z[64]) / T(3) + (T(4) * z[66]) / T(3);
z[64] = prod_pow(z[45], 2) * z[64];
z[58] = T(3) * z[58] + T(2) * z[64] + z[67];
z[58] = z[45] * z[58];
z[62] = (T(-7) * z[17]) / T(2) + (T(9) * z[18]) / T(2) + z[62];
z[62] = z[41] * z[62];
z[64] = T(3) * z[46];
z[66] = z[64] + (T(5) * z[129]) / T(6);
z[66] = z[42] * z[66];
z[65] = (T(5) * z[48]) / T(2) + z[62] + z[65] + z[66];
z[65] = z[13] * z[65];
z[64] = z[64] + -z[129] / T(6);
z[64] = z[42] * z[64];
z[62] = T(2) * z[47] + z[48] + z[62] + z[64];
z[62] = z[14] * z[62];
z[64] = -(z[41] * z[75]);
z[66] = (T(3) * z[129]) / T(2) + z[130];
z[66] = z[42] * z[66];
z[64] = T(6) * z[47] + z[48] + z[64] + z[66];
z[66] = T(3) * z[15];
z[64] = z[64] * z[66];
z[66] = (T(74) * z[9]) / T(3) + (T(23) * z[10]) / T(2) + T(14) * z[11] + T(28) * z[12] + (T(-39) * z[13]) / T(4) + (T(-32) * z[14]) / T(3) + (T(-51) * z[15]) / T(2) + (T(-17) * z[16]) / T(4);
z[66] = z[49] * z[66];
return z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + (T(3) * z[61]) / T(2) + z[62] + T(3) * z[63] + z[64] + z[65] + z[66];
}



template IntegrandConstructorType<double> f_4_245_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_245_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_245_construct (const Kin<qd_real>&);
#endif

}