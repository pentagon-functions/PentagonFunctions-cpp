#include "f_4_282.h"

namespace PentagonFunctions {

template <typename T> T f_4_282_abbreviated (const std::array<T,39>&);

template <typename T> class SpDLog_f_4_282_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_282_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * ((T(-9) * kin.v[4]) / T(2) + T(9) + T(-3) * kin.v[0] + T(9) * kin.v[1] + T(6) * kin.v[2] + (T(-3) / T(4) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4])) + T(-3) * kin.v[0] * kin.v[4] + T(9) * kin.v[1] * kin.v[4] + T(6) * kin.v[2] * kin.v[4] + ((T(-15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(3) + T(9)) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4];
c[2] = kin.v[3] * ((T(-9) * kin.v[4]) / T(2) + T(9) + T(-3) * kin.v[0] + T(9) * kin.v[1] + T(6) * kin.v[2] + (T(-3) / T(4) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4])) + T(-3) * kin.v[0] * kin.v[4] + T(9) * kin.v[1] * kin.v[4] + T(6) * kin.v[2] * kin.v[4] + ((T(-15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4]);
c[3] = (bc<T>[0] * int_to_imaginary<T>(3) + T(9)) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4];
c[4] = kin.v[3] * ((T(-9) * kin.v[4]) / T(2) + T(9) + T(-3) * kin.v[0] + T(9) * kin.v[1] + T(6) * kin.v[2] + (T(-3) / T(4) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4])) + T(-3) * kin.v[0] * kin.v[4] + T(9) * kin.v[1] * kin.v[4] + T(6) * kin.v[2] * kin.v[4] + ((T(-15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4]);
c[5] = (bc<T>[0] * int_to_imaginary<T>(3) + T(9)) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[9] * (t * c[0] + c[1]) + abb[1] * (t * c[2] + c[3]) + abb[7] * (t * c[4] + c[5]);
        }

        return abb[8] * (abb[12] * (abb[1] * T(-9) + abb[7] * T(-9)) + abb[3] * abb[10] * (abb[1] * T(-3) + abb[7] * T(-3)) + abb[10] * abb[11] * (abb[1] * T(-3) + abb[7] * T(-3)) + abb[10] * (abb[10] * ((abb[1] * T(-3)) / T(2) + (abb[7] * T(-3)) / T(2)) + abb[1] * bc<T>[0] * int_to_imaginary<T>(3) + abb[7] * bc<T>[0] * int_to_imaginary<T>(3)) + abb[4] * abb[10] * (abb[1] * T(3) + abb[7] * T(3)) + abb[9] * (abb[12] * T(-9) + abb[3] * abb[10] * T(-3) + abb[10] * abb[11] * T(-3) + abb[10] * ((abb[10] * T(-3)) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) + abb[4] * abb[10] * T(3)) + abb[2] * (abb[2] * ((abb[1] * T(-9)) / T(2) + (abb[7] * T(-9)) / T(2) + (abb[9] * T(-9)) / T(2)) + abb[4] * (abb[1] * T(-3) + abb[7] * T(-3)) + abb[1] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[3] * (abb[1] * T(3) + abb[7] * T(3)) + abb[11] * (abb[1] * T(3) + abb[7] * T(3)) + abb[10] * (abb[1] * T(6) + abb[7] * T(6)) + abb[9] * (abb[4] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3) + abb[3] * T(3) + abb[11] * T(3) + abb[10] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_282_construct (const Kin<T>& kin) {
    return [&kin, 
            dl3 = DLog_W_3<T>(kin),dl7 = DLog_W_7<T>(kin),dl5 = DLog_W_5<T>(kin),dl24 = DLog_W_24<T>(kin),dl25 = DLog_W_25<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl1 = DLog_W_1<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),spdl7 = SpDLog_f_4_282_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,39> abbr = 
            {dl3(t), f_1_3_3(kin) - f_1_3_3(kin_path), rlog(-v_path[1]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_4(kin_path), f_2_1_7(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl7(t), f_1_3_2(kin) - f_1_3_2(kin_path), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_11(kin_path), dl5(t), f_1_3_1(kin) - f_1_3_1(kin_path), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), f_1_3_5(kin) - f_1_3_5(kin_path), dl24(t), dl25(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t), dl16(t), dl17(t), dl18(t), dl19(t), dl1(t), dl4(t), dl20(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[18] / kin_path.W[18]), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_4_282_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_282_abbreviated(const std::array<T,39>& abb)
{
using TR = typename T::value_type;
T z[92];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[4];
z[4] = abb[3];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[22];
z[10] = abb[24];
z[11] = abb[25];
z[12] = abb[28];
z[13] = abb[29];
z[14] = abb[13];
z[15] = abb[26];
z[16] = abb[10];
z[17] = abb[19];
z[18] = abb[11];
z[19] = abb[23];
z[20] = abb[18];
z[21] = abb[27];
z[22] = abb[12];
z[23] = abb[15];
z[24] = abb[16];
z[25] = abb[20];
z[26] = abb[21];
z[27] = abb[9];
z[28] = abb[14];
z[29] = abb[30];
z[30] = abb[31];
z[31] = abb[32];
z[32] = abb[33];
z[33] = abb[34];
z[34] = abb[36];
z[35] = abb[38];
z[36] = abb[35];
z[37] = abb[17];
z[38] = abb[37];
z[39] = bc<TR>[3];
z[40] = bc<TR>[2];
z[41] = bc<TR>[9];
z[42] = bc<TR>[1];
z[43] = bc<TR>[4];
z[44] = z[3] / T(2);
z[45] = -z[18] + z[44];
z[46] = T(3) * z[3];
z[47] = z[45] * z[46];
z[48] = int_to_imaginary<T>(1) * z[7];
z[49] = T(3) * z[48];
z[50] = z[24] * z[49];
z[50] = T(3) * z[23] + z[50];
z[51] = T(3) * z[22];
z[52] = z[50] + z[51];
z[53] = z[47] + z[52];
z[54] = z[3] + -z[18];
z[55] = z[48] + z[54];
z[56] = T(3) * z[2];
z[57] = z[55] * z[56];
z[58] = T(3) * z[5];
z[57] = z[57] + -z[58];
z[59] = T(3) * z[4];
z[60] = z[55] * z[59];
z[61] = z[26] * z[49];
z[62] = prod_pow(z[18], 2);
z[61] = T(3) * z[25] + -z[61] + (T(3) * z[62]) / T(2);
z[63] = prod_pow(z[7], 2);
z[64] = z[61] + -z[63];
z[65] = z[53] + z[57] + -z[60] + z[64];
z[66] = z[15] * z[65];
z[67] = z[2] / T(2);
z[68] = -z[3] + z[67];
z[68] = z[56] * z[68];
z[69] = T(3) * z[6];
z[70] = -z[58] + z[69];
z[71] = z[4] / T(2);
z[72] = -z[3] + z[71];
z[73] = z[59] * z[72];
z[74] = z[63] / T(2);
z[68] = -z[68] + z[70] + z[73] + -z[74];
z[68] = z[0] * z[68];
z[75] = z[24] * z[48];
z[75] = z[23] + z[75];
z[45] = z[3] * z[45];
z[45] = -z[6] + z[45] + z[75];
z[76] = -z[18] + z[48];
z[71] = z[71] + z[76];
z[77] = z[4] * z[71];
z[77] = z[45] + -z[77];
z[77] = T(3) * z[77];
z[78] = z[19] * z[77];
z[79] = z[2] + -z[16];
z[80] = z[56] * z[79];
z[81] = z[59] * z[79];
z[81] = z[74] + z[81];
z[80] = z[58] + z[80] + -z[81];
z[82] = z[12] * z[80];
z[78] = z[78] + -z[82];
z[82] = z[67] + z[76];
z[82] = z[56] * z[82];
z[82] = z[51] + z[61] + -z[74] + z[82];
z[83] = z[13] * z[82];
z[66] = -z[66] + z[68] + z[78] + z[83];
z[58] = z[58] + z[69];
z[68] = z[46] * z[76];
z[84] = -z[16] + z[55];
z[85] = T(3) * z[16];
z[86] = z[84] * z[85];
z[61] = -z[58] + z[61] + (T(-5) * z[63]) / T(2) + z[68] + z[86];
z[68] = -z[2] + (T(3) * z[16]) / T(2);
z[68] = z[56] * z[68];
z[86] = z[71] + -z[79];
z[87] = z[4] * z[86];
z[61] = (T(-9) * z[22]) / T(2) + z[61] / T(2) + z[68] + (T(-3) * z[87]) / T(2);
z[61] = z[9] * z[61];
z[44] = z[44] + -z[48];
z[68] = z[3] * z[44];
z[88] = z[16] * z[55];
z[89] = z[26] * z[48];
z[89] = -z[25] + z[89];
z[88] = z[68] + z[75] + -z[88] + z[89];
z[88] = (T(-3) * z[62]) / T(4) + z[63] + (T(3) * z[88]) / T(2);
z[88] = z[21] * z[88];
z[90] = z[12] * z[22];
z[61] = z[61] + z[66] / T(2) + z[88] + (T(-3) * z[90]) / T(2);
z[66] = (T(3) * z[2]) / T(2);
z[88] = z[3] + z[66] + -z[85];
z[88] = z[2] * z[88];
z[90] = z[72] + z[79];
z[90] = z[4] * z[90];
z[84] = z[16] * z[84];
z[62] = z[62] / T(2) + -z[89];
z[68] = -z[6] + z[51] + -z[62] + -z[68] + -z[75] + -z[84] + z[88] + -z[90];
z[68] = (T(3) * z[68]) / T(2);
z[84] = z[11] * z[68];
z[66] = -z[16] + z[66] + z[76];
z[66] = z[56] * z[66];
z[66] = z[64] + z[66];
z[47] = z[47] + z[50];
z[86] = -(z[59] * z[86]);
z[58] = z[47] + -z[58] + -z[66] + z[86];
z[58] = -z[51] + z[58] / T(2);
z[58] = z[10] * z[58];
z[85] = z[55] * z[85];
z[57] = z[57] + -z[85];
z[44] = z[44] * z[46];
z[44] = z[44] + z[69];
z[73] = z[44] + z[73];
z[86] = z[52] + z[57] + z[63] + z[73];
z[88] = z[14] / T(2);
z[86] = z[86] * z[88];
z[89] = z[16] + z[76];
z[89] = z[56] * z[89];
z[90] = prod_pow(z[16], 2);
z[91] = (T(3) * z[90]) / T(2);
z[89] = z[64] + z[89] + -z[91];
z[89] = z[17] * z[89];
z[58] = z[58] + -z[61] + z[84] + z[86] + z[89];
z[58] = z[8] * z[58];
z[72] = z[72] + -z[79];
z[72] = z[59] * z[72];
z[67] = -z[16] + z[67];
z[84] = z[3] + z[67];
z[84] = z[56] * z[84];
z[44] = z[44] + z[52] + -z[64] + z[72] + z[84] + -z[85];
z[52] = z[30] + z[31];
z[64] = -(z[44] * z[52]);
z[72] = z[40] * z[48];
z[72] = z[72] + z[74];
z[72] = z[40] * z[72];
z[84] = int_to_imaginary<T>(1) * prod_pow(z[7], 3);
z[86] = z[84] / T(2);
z[64] = (T(-93) * z[41]) / T(4) + z[64] + T(9) * z[72] + -z[86];
z[64] = z[29] * z[64];
z[86] = T(-3) * z[72] + -z[86];
z[86] = z[35] * z[86];
z[64] = z[64] + z[86];
z[44] = z[29] * z[44];
z[76] = z[67] + -z[76];
z[76] = z[2] * z[76];
z[45] = -z[5] + z[45] + z[62] + -z[76] + -z[87];
z[62] = T(3) * z[35];
z[45] = z[45] * z[62];
z[62] = z[36] * z[65];
z[65] = z[34] * z[82];
z[76] = z[38] * z[77];
z[45] = z[45] + -z[62] + -z[65] + z[76];
z[44] = z[44] + -z[45];
z[62] = z[32] + z[33];
z[62] = z[62] / T(2);
z[44] = z[44] * z[62];
z[54] = -z[48] + z[54];
z[46] = z[46] * z[54];
z[46] = z[46] + -z[60] + z[63] + T(6) * z[75];
z[46] = z[20] * z[46];
z[54] = z[46] + z[89];
z[60] = z[63] + T(3) * z[90];
z[62] = z[56] * z[67];
z[65] = z[51] + z[62];
z[60] = z[60] / T(2) + z[65];
z[67] = -z[9] + z[14];
z[60] = z[60] * z[67];
z[67] = z[51] + z[80];
z[67] = z[15] * z[67];
z[75] = -(z[12] * z[51]);
z[60] = -z[54] + z[60] + z[67] + z[75] + z[78] + z[83];
z[60] = z[28] * z[60];
z[67] = z[71] + z[79];
z[67] = z[59] * z[67];
z[47] = -z[47] + -z[66] + z[67] + z[70];
z[47] = z[47] / T(2) + -z[51];
z[47] = z[10] * z[47];
z[50] = z[50] + z[73];
z[66] = z[50] + z[63];
z[51] = z[51] + z[57] + -z[66];
z[51] = z[51] * z[88];
z[47] = z[47] + z[51] + z[54] + -z[61];
z[47] = z[1] * z[47];
z[51] = z[1] * z[68];
z[50] = -z[50] + z[65] + -z[74] + z[91];
z[54] = -(z[28] * z[50]);
z[51] = z[51] + z[54];
z[51] = z[11] * z[51];
z[52] = z[52] / T(2);
z[45] = z[45] * z[52];
z[52] = z[59] * z[71];
z[52] = z[52] + -z[53] + -z[62] + z[69] + -z[74] + -z[91];
z[52] = z[27] * z[52];
z[53] = z[37] * z[77];
z[52] = z[52] + z[53];
z[52] = z[10] * z[52];
z[53] = z[55] + z[79];
z[53] = z[53] * z[56];
z[53] = T(6) * z[22] + z[53] + -z[81] + -z[85];
z[53] = z[15] * z[53];
z[53] = z[53] + -z[89];
z[53] = z[27] * z[53];
z[54] = z[27] * z[82];
z[55] = T(3) * z[7];
z[55] = z[39] * z[55];
z[55] = -z[55] + z[84] / T(9);
z[54] = z[54] + z[55];
z[54] = z[13] * z[54];
z[50] = z[27] * z[50];
z[56] = z[37] * z[66];
z[50] = z[50] + z[56];
z[50] = z[14] * z[50];
z[56] = z[27] + -z[37];
z[46] = z[46] * z[56];
z[56] = z[42] * z[48];
z[49] = z[40] * z[49];
z[49] = -z[49] + (T(3) * z[56]) / T(2) + -z[63];
z[49] = z[42] * z[49];
z[56] = z[72] / T(2);
z[48] = z[43] * z[48];
z[57] = z[48] + z[56];
z[57] = z[49] + T(3) * z[57];
z[57] = z[36] * z[57];
z[48] = z[48] + -z[56];
z[48] = T(3) * z[48] + z[49];
z[48] = z[34] * z[48];
z[49] = z[12] + -z[15] + -z[21];
z[49] = z[49] * z[55];
z[55] = z[41] / T(4) + -z[72];
z[55] = z[38] * z[55];
z[56] = T(67) * z[34] + T(15) * z[35] + T(61) * z[36];
z[56] = z[41] * z[56];
return z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + T(3) * z[55] + z[56] / T(8) + z[57] + z[58] + z[60] + z[64] / T(2);
}



template IntegrandConstructorType<double> f_4_282_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_282_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_282_construct (const Kin<qd_real>&);
#endif

}