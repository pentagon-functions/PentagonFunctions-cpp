#include "f_4_213.h"

namespace PentagonFunctions {

template <typename T> T f_4_213_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_213_W_10 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_213_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]);
c[1] = kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]);
c[2] = kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[1], 2) * (abb[3] + abb[4] + abb[5]) + -(prod_pow(abb[2], 2) * abb[3]) + prod_pow(abb[2], 2) * (-abb[4] + -abb[5]));
    }
};
template <typename T> class SpDLog_f_4_213_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_213_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]);
c[1] = kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]);
c[2] = (-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[3] = (-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[9] * c[0] + abb[10] * c[1] + abb[3] * c[2] + abb[5] * c[3]);
        }

        return abb[6] * (abb[3] * prod_pow(abb[8], 2) + prod_pow(abb[7], 2) * (abb[9] + abb[10] + -abb[3] + -abb[5]) + prod_pow(abb[8], 2) * (abb[5] + -abb[9] + -abb[10]));
    }
};
template <typename T> class SpDLog_f_4_213_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_213_W_23 (const Kin<T>& kin) {
        c[0] = ((T(-3) * kin.v[3]) / T(2) + T(-4)) * kin.v[3] + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + -kin.v[4] + T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (-kin.v[4] + T(4) + (T(-5) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(-3) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(3) * kin.v[2] + T(4) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(2) * kin.v[4];
c[2] = ((T(-3) * kin.v[3]) / T(2) + T(-4)) * kin.v[3] + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + -kin.v[4] + T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (-kin.v[4] + T(4) + (T(-5) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(-3) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(3) * kin.v[2] + T(4) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(2) * kin.v[4];
c[4] = ((T(3) * kin.v[3]) / T(2) + T(4)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-4)) * kin.v[4] + kin.v[2] * (kin.v[2] / T(2) + T(4) + T(2) * kin.v[3] + kin.v[4]) + kin.v[0] * (T(-4) + (T(5) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(1) + kin.v[3]) + kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4]);
c[5] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4]);
c[6] = ((T(3) * kin.v[3]) / T(2) + T(4)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-4)) * kin.v[4] + kin.v[2] * (kin.v[2] / T(2) + T(4) + T(2) * kin.v[3] + kin.v[4]) + kin.v[0] * (T(-4) + (T(5) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(1) + kin.v[3]) + kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4]);
c[7] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[10] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[3] * (t * c[4] + c[5]) + abb[4] * (t * c[6] + c[7]);
        }

        return abb[11] * (abb[1] * abb[2] * (abb[5] + abb[10] + -abb[4]) + abb[12] * (abb[2] * abb[3] + abb[7] * (abb[5] + abb[10] + -abb[3] + -abb[4]) + abb[2] * (abb[4] + -abb[5] + -abb[10])) + abb[3] * (-(abb[1] * abb[2]) + abb[13] * T(-2) + abb[2] * (-abb[2] + -abb[8] + bc<T>[0] * int_to_imaginary<T>(-1))) + abb[2] * (abb[2] * (abb[5] + abb[10] + -abb[4]) + abb[8] * (abb[5] + abb[10] + -abb[4]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[7] * (abb[1] * (abb[4] + -abb[5] + -abb[10]) + abb[2] * (abb[4] + -abb[5] + -abb[10]) + abb[8] * (abb[4] + -abb[5] + -abb[10]) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[3] * (abb[1] + abb[2] + abb[8] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[13] * (abb[4] * T(-2) + abb[5] * T(2) + abb[10] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_213_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl22 = DLog_W_22<T>(kin),dl23 = DLog_W_23<T>(kin),dl8 = DLog_W_8<T>(kin),dl19 = DLog_W_19<T>(kin),dl15 = DLog_W_15<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl31 = DLog_W_31<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),spdl10 = SpDLog_f_4_213_W_10<T>(kin),spdl22 = SpDLog_f_4_213_W_22<T>(kin),spdl23 = SpDLog_f_4_213_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl23(t), rlog(v_path[2]), f_2_1_8(kin_path), dl8(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl19(t), f_2_1_14(kin_path), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl2(t), f_2_1_7(kin_path), dl1(t), dl17(t), dl16(t), dl5(t), dl20(t), dl3(t), dl18(t), dl4(t), dl31(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[30] / kin_path.W[30]), dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta}
;

        auto result = f_4_213_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_213_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[126];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[19];
z[3] = abb[22];
z[4] = abb[25];
z[5] = abb[26];
z[6] = abb[27];
z[7] = abb[28];
z[8] = abb[29];
z[9] = abb[30];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[10];
z[13] = abb[33];
z[14] = abb[34];
z[15] = abb[35];
z[16] = abb[36];
z[17] = abb[37];
z[18] = abb[38];
z[19] = abb[39];
z[20] = abb[40];
z[21] = abb[41];
z[22] = abb[42];
z[23] = abb[43];
z[24] = abb[2];
z[25] = abb[45];
z[26] = abb[7];
z[27] = abb[8];
z[28] = abb[24];
z[29] = abb[12];
z[30] = bc<TR>[0];
z[31] = abb[31];
z[32] = abb[17];
z[33] = abb[13];
z[34] = abb[15];
z[35] = abb[16];
z[36] = abb[18];
z[37] = abb[20];
z[38] = abb[21];
z[39] = abb[23];
z[40] = abb[14];
z[41] = abb[9];
z[42] = abb[44];
z[43] = bc<TR>[3];
z[44] = abb[32];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[5] / T(3);
z[50] = -z[32] + z[49];
z[51] = (T(7) * z[8]) / T(3) + T(5) * z[28];
z[52] = T(3) * z[7];
z[53] = (T(-7) * z[6]) / T(3) + z[50] + z[51] + -z[52];
z[54] = z[1] / T(2);
z[53] = z[53] * z[54];
z[55] = z[32] / T(3);
z[56] = T(3) * z[6];
z[51] = -z[3] + z[4] / T(3) + -z[5] + (T(-7) * z[7]) / T(3) + -z[31] + z[51] + z[55] + -z[56];
z[51] = (T(-5) * z[40]) / T(3) + z[51] / T(2);
z[51] = z[11] * z[51];
z[57] = z[12] + z[41];
z[57] = z[40] * z[57];
z[58] = z[1] + -z[12];
z[59] = z[2] * z[58];
z[60] = z[57] + z[59];
z[61] = z[32] * z[41];
z[62] = z[8] * z[41];
z[63] = z[61] + -z[62];
z[64] = z[7] / T(3);
z[65] = -z[8] + z[28];
z[66] = z[6] + z[64] + (T(5) * z[65]) / T(3);
z[50] = -z[50] + -z[66];
z[67] = z[12] / T(2);
z[50] = z[50] * z[67];
z[55] = (T(7) * z[5]) / T(3) + -z[55] + z[66];
z[66] = z[10] / T(2);
z[55] = z[55] * z[66];
z[68] = z[4] / T(2);
z[69] = z[1] + z[10];
z[70] = -z[12] + -z[69];
z[70] = z[68] * z[70];
z[71] = z[22] + z[23];
z[72] = T(3) * z[71];
z[73] = z[25] + z[42];
z[74] = -z[13] + z[72] + z[73];
z[75] = z[74] / T(2);
z[76] = -(z[17] * z[75]);
z[77] = z[9] / T(2);
z[78] = -z[12] + z[69];
z[79] = -z[11] / T(3) + -z[78];
z[79] = z[77] * z[79];
z[80] = z[42] + z[71];
z[81] = (T(-3) * z[80]) / T(2);
z[81] = z[46] * z[81];
z[82] = z[5] * z[41];
z[83] = z[31] * z[78];
z[84] = -z[12] + z[69] / T(3);
z[84] = z[3] * z[84];
z[50] = z[50] + z[51] + z[53] + z[55] + (T(-5) * z[60]) / T(3) + z[63] / T(3) + z[70] + z[76] + z[79] + z[81] + z[82] + -z[83] / T(6) + z[84] / T(2);
z[51] = z[71] / T(2);
z[49] = -z[6] / T(3) + (T(-2) * z[8]) / T(9) + (T(4) * z[44]) / T(9) + -z[49] + z[51] + -z[64];
z[53] = z[9] + z[31];
z[55] = z[13] + -z[25];
z[49] = -z[4] / T(9) + z[42] / T(12) + z[49] / T(3) + z[53] / T(27) + z[55] / T(4);
z[49] = int_to_imaginary<T>(1) * z[30] * z[49];
z[60] = -z[55] / T(3);
z[64] = z[42] / T(3) + z[60] + z[71];
z[70] = z[19] + z[20];
z[76] = z[21] + z[70] / T(4);
z[76] = z[64] * z[76];
z[60] = -z[60] + z[71];
z[60] = z[45] * z[60];
z[74] = z[16] * z[74];
z[49] = z[49] + z[50] / T(2) + z[60] + -z[74] / T(4) + z[76];
z[49] = z[30] * z[49];
z[50] = T(3) * z[28];
z[60] = z[8] + z[50];
z[74] = T(2) * z[6];
z[76] = T(2) * z[7];
z[79] = -z[60] / T(2) + z[74] + z[76];
z[81] = z[5] / T(2);
z[84] = -z[68] + z[79] + z[81];
z[84] = z[11] * z[84];
z[79] = z[79] + -z[81];
z[79] = z[1] * z[79];
z[85] = -z[10] + z[12];
z[86] = z[5] + -z[8];
z[87] = z[28] + z[86];
z[85] = z[85] * z[87];
z[88] = (T(3) * z[17]) / T(2);
z[89] = z[71] * z[88];
z[68] = z[68] * z[78];
z[79] = z[68] + z[79] + z[84] + z[85] / T(2) + z[89];
z[84] = z[27] * z[79];
z[85] = z[6] / T(2);
z[89] = z[7] / T(2);
z[90] = z[8] + z[85] + z[89];
z[91] = T(2) * z[4];
z[92] = z[81] + z[90] + z[91];
z[92] = z[12] * z[92];
z[93] = -z[54] + -z[66];
z[94] = z[6] + z[7];
z[95] = z[5] + z[94];
z[93] = z[93] * z[95];
z[95] = T(3) * z[12];
z[96] = -z[69] + z[95];
z[97] = z[3] * z[96];
z[98] = z[97] / T(2);
z[92] = z[92] + z[93] + -z[98];
z[92] = z[24] * z[92];
z[84] = -z[84] + z[92];
z[92] = (T(3) * z[32]) / T(2);
z[93] = T(2) * z[5];
z[99] = -z[90] + z[92] + -z[93];
z[58] = z[58] * z[99];
z[99] = z[41] * z[93];
z[99] = -z[63] + -z[68] + z[99];
z[100] = -z[32] + z[94];
z[101] = z[66] * z[100];
z[58] = z[58] + z[99] + -z[101];
z[58] = z[26] * z[58];
z[58] = z[58] + -z[84];
z[101] = T(3) * z[5];
z[102] = -z[7] + T(3) * z[32] + -z[101];
z[103] = z[6] + -z[8];
z[104] = -z[28] + z[102] + z[103];
z[104] = z[67] * z[104];
z[105] = z[7] + -z[28];
z[106] = z[103] + z[105];
z[107] = -z[5] + z[32] + z[106];
z[108] = (T(3) * z[1]) / T(2);
z[107] = z[107] * z[108];
z[108] = -z[7] + z[32];
z[87] = z[6] + -z[87] + z[108];
z[87] = z[66] * z[87];
z[87] = z[57] + -z[63] + z[82] + z[87] + -z[104] + z[107];
z[104] = -(z[35] * z[87]);
z[107] = T(3) * z[8];
z[109] = -z[6] + z[107];
z[110] = z[105] + z[109];
z[110] = z[12] * z[110];
z[111] = z[52] + z[56];
z[60] = -z[60] + z[111];
z[60] = z[1] * z[60];
z[105] = -z[103] + z[105];
z[105] = z[10] * z[105];
z[96] = z[4] * z[96];
z[60] = z[60] + -z[96] + z[105] + -z[110];
z[60] = z[59] + z[60] / T(2) + z[98];
z[96] = -(z[38] * z[60]);
z[98] = z[57] + -z[82];
z[105] = z[4] + z[40];
z[110] = z[94] + -z[105];
z[110] = z[11] * z[110];
z[112] = -z[4] + z[5];
z[112] = z[12] * z[112];
z[94] = -z[5] + z[94];
z[94] = z[1] * z[94];
z[94] = -z[59] + z[94] + -z[98] + z[110] + z[112];
z[94] = z[29] * z[94];
z[91] = z[81] + z[91];
z[90] = (T(3) * z[3]) / T(2) + -z[90] + -z[91];
z[90] = z[24] * z[90];
z[100] = z[4] + z[100];
z[110] = z[26] / T(2);
z[100] = z[100] * z[110];
z[100] = z[90] + z[100];
z[50] = -z[32] + -z[50] + z[86] + z[111];
z[112] = -z[40] + -z[50] / T(2);
z[112] = z[35] * z[112];
z[106] = z[3] + -z[4] + z[106];
z[113] = z[38] * z[106];
z[112] = z[100] + z[112] + (T(-3) * z[113]) / T(2);
z[112] = z[11] * z[112];
z[113] = -z[22] + z[25];
z[114] = z[42] + z[113];
z[114] = z[26] * z[114];
z[115] = z[23] + z[25];
z[116] = z[24] * z[115];
z[117] = z[114] + -z[116];
z[118] = z[38] * z[113];
z[115] = z[42] + z[115];
z[119] = z[35] * z[115];
z[118] = z[118] + -z[119];
z[119] = z[117] + z[118];
z[119] = z[88] * z[119];
z[72] = z[55] + z[72];
z[120] = -z[45] / T(2) + z[46];
z[120] = z[45] * z[120];
z[120] = -z[47] + z[120];
z[72] = z[72] * z[120];
z[120] = z[42] / T(2);
z[51] = -z[51] + -z[120];
z[51] = prod_pow(z[46], 2) * z[51];
z[51] = z[51] + z[58] + z[72] + z[94] + z[96] + z[104] + z[112] + z[119];
z[51] = int_to_imaginary<T>(1) * z[51];
z[72] = int_to_imaginary<T>(1) * z[79];
z[94] = T(2) * z[21];
z[96] = int_to_imaginary<T>(1) * z[71];
z[104] = -(z[94] * z[96]);
z[72] = z[72] + z[104];
z[72] = z[0] * z[72];
z[104] = T(3) * z[4];
z[53] = T(2) * z[8] + T(-4) * z[44] + -z[53] + z[101] + z[104] + z[111];
z[53] = z[43] * z[53];
z[111] = z[27] * z[71];
z[111] = z[111] + -z[116];
z[112] = z[111] + z[114];
z[114] = z[112] + z[118];
z[114] = int_to_imaginary<T>(1) * z[114];
z[96] = z[0] * z[96];
z[96] = z[96] + z[114];
z[70] = (T(3) * z[16]) / T(2) + -z[70] / T(2);
z[118] = z[70] * z[96];
z[114] = -(z[94] * z[114]);
z[49] = z[49] + z[51] + z[53] + z[72] + z[114] + z[118];
z[49] = z[30] * z[49];
z[51] = -z[56] + z[86];
z[51] = z[12] * z[51];
z[53] = -z[8] + z[56];
z[56] = -z[5] + z[53];
z[56] = z[10] * z[56];
z[72] = z[11] + T(3) * z[78];
z[114] = z[9] * z[72];
z[51] = z[51] + z[56] + -z[114];
z[56] = z[5] / T(4);
z[114] = z[4] / T(4);
z[118] = z[56] + -z[114];
z[119] = (T(3) * z[3]) / T(4);
z[121] = z[7] + z[103] / T(4) + z[118] + -z[119];
z[121] = z[11] * z[121];
z[59] = z[59] + z[97] / T(4);
z[114] = z[78] * z[114];
z[122] = z[13] + z[71];
z[123] = (T(3) * z[17]) / T(4);
z[124] = -z[21] + z[123];
z[124] = z[122] * z[124];
z[125] = z[7] + z[53] / T(4) + -z[56];
z[125] = z[1] * z[125];
z[51] = z[51] / T(4) + -z[59] + z[114] + z[121] + z[124] + z[125];
z[51] = z[0] * z[51];
z[79] = -(z[29] * z[79]);
z[74] = z[8] / T(2) + -z[74] + -z[89] + -z[93];
z[74] = z[74] * z[78];
z[93] = z[72] * z[77];
z[74] = -z[68] + z[74] + z[93];
z[74] = z[26] * z[74];
z[121] = z[4] + z[7];
z[124] = -z[8] + z[121];
z[110] = z[110] * z[124];
z[90] = z[90] + z[110];
z[90] = z[11] * z[90];
z[110] = z[29] * z[71];
z[124] = -z[13] + z[113];
z[125] = z[26] * z[124];
z[110] = z[110] + -z[111] + -z[125];
z[111] = z[94] * z[110];
z[116] = -z[116] + z[125];
z[116] = z[88] * z[116];
z[51] = z[51] + z[74] + z[79] + -z[84] + z[90] + z[111] + z[116];
z[51] = z[0] * z[51];
z[65] = z[65] + z[81];
z[74] = z[65] + -z[92];
z[74] = z[12] * z[74];
z[79] = z[32] / T(2);
z[65] = -z[65] + z[79];
z[65] = z[10] * z[65];
z[63] = -z[63] + z[65] + z[74];
z[65] = -z[7] + (T(3) * z[28]) / T(2) + -z[103];
z[74] = z[32] / T(4);
z[84] = z[40] + -z[65] + -z[74] + z[118] + z[119];
z[84] = z[11] * z[84];
z[56] = (T(3) * z[32]) / T(4) + -z[56] + -z[65];
z[56] = z[1] * z[56];
z[65] = z[80] * z[123];
z[56] = z[56] + z[57] + z[59] + z[63] / T(2) + z[65] + z[84] + z[114];
z[56] = z[29] * z[56];
z[57] = -(z[11] * z[100]);
z[59] = -(z[88] * z[117]);
z[56] = z[56] + z[57] + -z[58] + z[59];
z[56] = z[29] * z[56];
z[57] = z[34] * z[87];
z[58] = z[5] + z[6];
z[59] = T(-3) * z[3] + z[58] + z[104] + z[107];
z[59] = z[39] * z[59];
z[50] = z[34] * z[50];
z[52] = T(3) * z[31] + -z[32] + -z[52] + -z[86] + -z[104];
z[52] = z[36] * z[52];
z[63] = z[37] * z[106];
z[50] = z[50] + z[52] + z[59] + T(3) * z[63];
z[52] = (T(3) * z[31]) / T(2);
z[59] = z[103] / T(2);
z[63] = -z[52] + z[59] + z[76] + z[91];
z[65] = z[24] * z[63];
z[52] = z[52] + -z[81];
z[74] = z[52] + -z[74] + z[109] / T(4) + -z[121];
z[74] = z[26] * z[74];
z[65] = z[65] + z[74];
z[65] = z[26] * z[65];
z[74] = z[7] + -z[8];
z[76] = prod_pow(z[24], 2);
z[74] = -(z[74] * z[76]);
z[81] = z[34] * z[40];
z[50] = z[50] / T(2) + z[65] + z[74] + z[81];
z[50] = z[11] * z[50];
z[52] = z[8] + z[52] + -z[85] + (T(-5) * z[121]) / T(2);
z[52] = z[11] * z[52];
z[65] = -z[88] + z[94];
z[74] = z[23] + z[124];
z[65] = z[65] * z[74];
z[81] = -z[8] + (T(5) * z[58]) / T(2) + z[89];
z[78] = z[78] * z[81];
z[81] = z[83] / T(2);
z[52] = z[52] + z[65] + z[68] + z[78] + z[81] + -z[93];
z[52] = z[33] * z[52];
z[53] = z[53] + z[101];
z[53] = z[53] * z[69];
z[65] = z[6] + z[86];
z[68] = -(z[65] * z[95]);
z[78] = z[69] + z[95];
z[78] = z[4] * z[78];
z[53] = z[53] + z[68] + z[78] + -z[97];
z[53] = z[39] * z[53];
z[55] = (T(7) * z[42]) / T(4) + (T(-19) * z[55]) / T(3) + (T(11) * z[71]) / T(4);
z[55] = z[48] * z[55];
z[53] = z[53] + z[55];
z[55] = -z[71] + z[73];
z[55] = z[36] * z[55];
z[68] = z[39] * z[122];
z[71] = z[37] * z[113];
z[73] = z[34] * z[115];
z[55] = -z[55] + -z[68] + z[71] + -z[73];
z[68] = z[23] * z[26];
z[71] = z[23] * z[24];
z[68] = z[68] + -z[71];
z[73] = z[27] * z[68];
z[73] = z[55] + -z[73];
z[78] = z[0] * z[122];
z[78] = z[78] / T(2) + -z[110];
z[78] = z[0] * z[78];
z[84] = z[13] / T(2) + -z[23] + z[120];
z[84] = z[26] * z[84];
z[84] = z[71] + z[84];
z[84] = z[26] * z[84];
z[80] = z[29] * z[80];
z[85] = z[80] / T(2) + -z[112];
z[85] = z[29] * z[85];
z[74] = z[33] * z[74];
z[74] = z[73] + z[74] + -z[78] + -z[84] + -z[85];
z[75] = z[30] * z[75];
z[75] = -z[75] + T(3) * z[96];
z[75] = z[30] * z[75];
z[75] = T(-3) * z[74] + z[75];
z[78] = z[14] + z[15];
z[78] = z[78] / T(2);
z[75] = z[75] * z[78];
z[70] = -(z[70] * z[74]);
z[60] = z[37] * z[60];
z[54] = -z[54] + z[67];
z[59] = z[5] + z[59] + z[92];
z[59] = -(z[54] * z[59]);
z[67] = z[79] + z[101] + -z[109] / T(2);
z[67] = z[66] * z[67];
z[61] = z[61] + z[62];
z[59] = z[59] + -z[61] / T(2) + z[67] + z[81] + z[82];
z[59] = z[26] * z[59];
z[61] = z[65] * z[69];
z[65] = z[12] * z[65];
z[61] = z[61] + -z[65] + z[83];
z[67] = z[24] * z[61];
z[59] = z[59] + -z[67] / T(2);
z[59] = z[26] * z[59];
z[67] = z[5] + z[8];
z[67] = z[12] * z[67];
z[78] = -z[5] + z[103];
z[78] = z[1] * z[78];
z[79] = z[6] + -z[105];
z[79] = z[11] * z[79];
z[62] = z[62] + z[67] + z[78] + z[79] + -z[98];
z[62] = z[27] * z[62];
z[67] = -z[24] + z[26];
z[63] = z[11] * z[63] * z[67];
z[61] = -(z[61] * z[67]);
z[67] = z[17] * z[68];
z[61] = z[61] + T(3) * z[67];
z[61] = z[61] / T(2) + z[62] + z[63];
z[61] = z[27] * z[61];
z[62] = z[30] / T(2);
z[62] = -(z[62] * z[64]);
z[62] = z[62] + z[96];
z[62] = z[30] * z[62];
z[62] = z[62] + -z[74];
z[62] = z[18] * z[62];
z[63] = z[102] + -z[107];
z[54] = -(z[54] * z[63]);
z[63] = z[86] + z[108];
z[63] = z[63] * z[66];
z[54] = z[54] + z[63] + z[81] + z[99];
z[54] = z[36] * z[54];
z[55] = z[55] + -z[84];
z[55] = -(z[55] * z[88]);
z[63] = -z[13] + T(2) * z[23] + -z[42];
z[63] = z[26] * z[63];
z[63] = z[63] + T(-2) * z[71];
z[63] = z[26] * z[63];
z[64] = -z[80] + T(2) * z[112];
z[64] = z[29] * z[64];
z[63] = z[63] + z[64] + T(2) * z[73];
z[63] = z[21] * z[63];
z[64] = prod_pow(z[26], 2);
z[64] = z[39] + z[64] / T(2);
z[64] = -(z[64] * z[72] * z[77]);
z[58] = z[58] * z[69];
z[58] = z[58] + -z[65];
z[58] = z[58] * z[76];
return z[49] + z[50] + z[51] + z[52] + z[53] / T(2) + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[70] + z[75];
}



template IntegrandConstructorType<double> f_4_213_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_213_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_213_construct (const Kin<qd_real>&);
#endif

}