#include "f_4_350.h"

namespace PentagonFunctions {

template <typename T> T f_4_350_abbreviated (const std::array<T,43>&);

template <typename T> class SpDLog_f_4_350_W_7 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_350_W_7 (const Kin<T>& kin) {
        c[0] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[1] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[2] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[1], 2) * (abb[3] * T(-6) + abb[4] * T(-6) + abb[5] * T(-6)) + prod_pow(abb[2], 2) * abb[3] * T(6) + prod_pow(abb[2], 2) * (abb[4] * T(6) + abb[5] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_350_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_350_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(12) + T(-6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) + T(-6) * kin.v[1] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[1] = kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(12) + T(-6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) + T(-6) * kin.v[1] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[2] = kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(12) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) + T(6) * kin.v[1] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[3] * (T(-12) + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[3] = kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(12) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) + T(6) * kin.v[1] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[3] * (T(-12) + T(6) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2] + abb[8] * c[3]);
        }

        return abb[6] * (abb[3] * prod_pow(abb[7], 2) * T(6) + prod_pow(abb[7], 2) * (abb[5] * T(-6) + abb[8] * T(-6) + abb[4] * T(6)) + prod_pow(abb[2], 2) * (abb[3] * T(-6) + abb[4] * T(-6) + abb[5] * T(6) + abb[8] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_350_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl21 = DLog_W_21<T>(kin),dl27 = DLog_W_27<T>(kin),dl13 = DLog_W_13<T>(kin),dl11 = DLog_W_11<T>(kin),dl8 = DLog_W_8<T>(kin),dl14 = DLog_W_14<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl28 = DLog_W_28<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl31 = DLog_W_31<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),spdl7 = SpDLog_f_4_350_W_7<T>(kin),spdl21 = SpDLog_f_4_350_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,43> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl21(t), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_5(kin) - f_1_3_5(kin_path), dl27(t) / kin_path.SqrtDelta, rlog(v_path[2]), rlog(v_path[3]), rlog(v_path[0]), f_2_1_1(kin_path), f_2_1_2(kin_path), f_2_1_11(kin_path), f_2_1_15(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[30] / kin_path.W[30]), dl13(t), dl11(t), dl8(t), dl14(t), dl3(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl4(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl2(t), dl19(t), dl28(t) / kin_path.SqrtDelta, dl20(t), dl1(t), dl18(t), f_2_2_2(kin_path), dl16(t), dl5(t), dl31(t), dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta}
;

        auto result = f_4_350_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_350_abbreviated(const std::array<T,43>& abb)
{
using TR = typename T::value_type;
T z[108];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[28];
z[3] = abb[31];
z[4] = abb[32];
z[5] = abb[38];
z[6] = abb[39];
z[7] = abb[4];
z[8] = abb[24];
z[9] = abb[5];
z[10] = abb[36];
z[11] = abb[8];
z[12] = abb[9];
z[13] = abb[17];
z[14] = abb[18];
z[15] = abb[19];
z[16] = abb[20];
z[17] = abb[33];
z[18] = abb[41];
z[19] = abb[2];
z[20] = abb[11];
z[21] = abb[12];
z[22] = abb[35];
z[23] = abb[7];
z[24] = abb[34];
z[25] = abb[42];
z[26] = abb[10];
z[27] = abb[25];
z[28] = bc<TR>[0];
z[29] = abb[21];
z[30] = abb[22];
z[31] = abb[13];
z[32] = abb[14];
z[33] = abb[15];
z[34] = abb[16];
z[35] = abb[26];
z[36] = abb[27];
z[37] = abb[29];
z[38] = abb[30];
z[39] = abb[23];
z[40] = abb[37];
z[41] = abb[40];
z[42] = bc<TR>[1];
z[43] = bc<TR>[3];
z[44] = bc<TR>[5];
z[45] = bc<TR>[2];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = bc<TR>[9];
z[50] = prod_pow(z[26], 2);
z[51] = T(2) * z[35];
z[52] = z[50] + z[51];
z[53] = prod_pow(z[21], 2);
z[54] = z[52] + -z[53];
z[55] = z[21] + -z[26];
z[56] = T(2) * z[55];
z[57] = z[23] * z[56];
z[58] = T(2) * z[31];
z[57] = z[57] + -z[58];
z[59] = z[0] + -z[19];
z[60] = T(2) * z[59];
z[61] = z[20] * z[60];
z[62] = z[57] + z[61];
z[63] = z[0] * z[60];
z[63] = -z[54] + -z[62] + z[63];
z[63] = z[6] * z[63];
z[64] = prod_pow(z[20], 2);
z[65] = -z[53] + z[64];
z[65] = z[30] * z[65];
z[63] = z[63] + -z[65];
z[65] = prod_pow(z[23], 2);
z[66] = z[51] + z[65];
z[67] = T(2) * z[19];
z[68] = -z[0] + z[67];
z[68] = z[0] * z[68];
z[69] = prod_pow(z[19], 2);
z[70] = z[68] + -z[69];
z[71] = T(2) * z[34];
z[72] = z[70] + -z[71];
z[73] = z[26] * z[67];
z[73] = z[72] + z[73];
z[74] = -z[20] + z[60];
z[74] = z[20] * z[74];
z[75] = z[66] + -z[73] + -z[74];
z[76] = z[4] * z[75];
z[67] = -z[26] + z[67];
z[67] = z[26] * z[67];
z[77] = -z[51] + z[67] + -z[71];
z[78] = prod_pow(z[0], 2);
z[79] = -z[69] + z[78];
z[80] = T(2) * z[0] + -z[21];
z[80] = z[21] * z[80];
z[80] = -z[77] + -z[79] + z[80];
z[80] = z[5] * z[80];
z[81] = z[0] * z[21];
z[82] = T(2) * z[81];
z[83] = z[64] + z[78] + -z[82];
z[83] = z[3] * z[83];
z[84] = z[31] + -z[35];
z[81] = -z[81] + -z[84];
z[85] = z[23] + z[56];
z[85] = z[23] * z[85];
z[81] = z[64] + T(2) * z[81] + z[85];
z[81] = z[22] * z[81];
z[66] = -z[66] + z[67] + -z[71];
z[66] = z[27] * z[66];
z[67] = -z[50] + z[53];
z[67] = z[29] * z[67];
z[61] = z[61] + z[70];
z[61] = z[2] * z[61];
z[86] = z[23] * z[55];
z[86] = -z[84] + z[86];
z[87] = T(2) * z[24];
z[86] = z[86] * z[87];
z[88] = z[3] + z[22];
z[89] = -z[5] + z[88];
z[90] = z[2] + z[4] + -z[6];
z[91] = z[89] + -z[90];
z[92] = T(2) * z[37];
z[93] = z[91] * z[92];
z[94] = T(2) * z[32];
z[89] = z[89] * z[94];
z[95] = T(2) * z[33];
z[96] = -(z[90] * z[95]);
z[61] = z[61] + z[63] + z[66] + -z[67] + -z[76] + z[80] + z[81] + z[83] + z[86] + z[89] + z[93] + z[96];
z[66] = z[29] * z[55];
z[80] = -z[26] + z[36];
z[81] = z[24] * z[80];
z[66] = z[66] + -z[81];
z[83] = z[4] + -z[22];
z[86] = z[20] + z[80];
z[83] = z[83] * z[86];
z[89] = z[5] + -z[6];
z[93] = z[21] + -z[36];
z[96] = z[19] + z[93];
z[89] = z[89] * z[96];
z[91] = z[38] * z[91];
z[97] = z[20] + -z[21];
z[98] = z[30] * z[97];
z[99] = -(z[3] * z[20]);
z[100] = z[2] * z[19];
z[101] = -z[19] + z[36];
z[102] = z[27] * z[101];
z[83] = z[66] + z[83] + z[89] + z[91] + z[98] + z[99] + z[100] + z[102];
z[83] = int_to_imaginary<T>(1) * z[83];
z[88] = z[4] + z[27] + -z[88];
z[88] = z[28] * z[88];
z[83] = T(6) * z[83] + z[88];
z[83] = z[28] * z[83];
z[61] = T(3) * z[61] + z[83];
z[61] = z[1] * z[61];
z[83] = z[3] + z[39];
z[83] = z[23] * z[83];
z[88] = z[3] * z[60];
z[83] = z[83] + z[88];
z[83] = z[23] * z[83];
z[88] = z[55] + -z[59];
z[89] = z[23] * z[88];
z[89] = z[34] + z[84] + -z[89];
z[91] = z[87] * z[89];
z[99] = -z[3] + z[10];
z[100] = z[71] * z[99];
z[102] = z[39] * z[50];
z[83] = z[83] + z[91] + -z[100] + -z[102];
z[91] = -z[21] + T(2) * z[26];
z[91] = z[21] * z[91];
z[100] = -z[58] + z[91];
z[103] = T(2) * z[20];
z[103] = z[88] * z[103];
z[104] = -z[50] + -z[70] + z[100] + z[103];
z[104] = z[2] * z[104];
z[51] = -z[51] + z[53] + z[58] + -z[85];
z[51] = z[22] * z[51];
z[53] = z[23] * z[60];
z[53] = z[53] + -z[79];
z[60] = z[21] * z[56];
z[58] = -z[53] + z[58] + z[60];
z[58] = z[10] * z[58];
z[60] = z[3] + -z[8];
z[60] = z[60] * z[78];
z[78] = z[8] * z[20];
z[79] = -(z[10] * z[56]);
z[79] = -z[78] + z[79];
z[79] = z[20] * z[79];
z[68] = -z[68] + -z[74];
z[68] = z[4] * z[68];
z[56] = -z[20] + z[56];
z[56] = z[20] * z[56];
z[56] = z[56] + z[100];
z[56] = z[27] * z[56];
z[100] = z[90] * z[92];
z[105] = z[2] + z[27];
z[106] = z[10] + -z[105];
z[106] = z[94] * z[106];
z[99] = -z[24] + z[99];
z[107] = z[90] + z[99];
z[107] = z[95] * z[107];
z[51] = z[51] + z[56] + z[58] + -z[60] + -z[63] + z[68] + z[79] + z[83] + z[100] + z[104] + z[106] + z[107];
z[56] = z[38] * z[90];
z[56] = z[56] + z[78];
z[58] = z[26] * z[39];
z[55] = -z[19] + -z[55];
z[55] = z[2] * z[55];
z[63] = z[6] * z[96];
z[68] = -z[21] + z[80];
z[68] = z[22] * z[68];
z[78] = z[19] + z[20];
z[79] = -(z[4] * z[78]);
z[97] = z[27] * z[97];
z[55] = z[55] + z[56] + z[58] + z[63] + z[68] + z[79] + z[81] + z[97] + -z[98];
z[55] = int_to_imaginary<T>(1) * z[55];
z[63] = z[4] + z[22];
z[68] = -z[8] + -z[39] + z[63];
z[68] = -z[3] + -z[24] + T(-3) * z[68] + z[105];
z[68] = z[28] * z[68];
z[55] = T(6) * z[55] + z[68];
z[55] = z[28] * z[55];
z[51] = T(3) * z[51] + z[55];
z[51] = z[11] * z[51];
z[51] = z[51] + z[61];
z[55] = z[69] + -z[77];
z[55] = z[5] * z[55];
z[61] = T(2) * z[6];
z[68] = z[0] + -z[20];
z[59] = z[59] * z[61] * z[68];
z[68] = z[19] * z[26];
z[68] = -z[35] + z[68];
z[68] = -z[65] + T(2) * z[68] + -z[71];
z[68] = z[27] * z[68];
z[71] = -z[24] + z[39];
z[65] = z[65] * z[71];
z[71] = -z[92] + -z[95];
z[71] = z[71] * z[90];
z[70] = z[70] + z[74];
z[70] = z[2] * z[70];
z[64] = z[8] * z[64];
z[55] = z[55] + z[59] + z[60] + z[64] + z[65] + z[68] + z[70] + z[71] + -z[76] + -z[102];
z[59] = -(z[5] * z[101]);
z[60] = z[2] * z[78];
z[64] = -(z[6] * z[19]);
z[65] = z[4] * z[86];
z[68] = -z[19] + z[80];
z[68] = z[27] * z[68];
z[56] = -z[56] + z[58] + z[59] + z[60] + z[64] + z[65] + z[68];
z[56] = int_to_imaginary<T>(1) * z[56];
z[59] = -z[3] + z[24];
z[60] = -z[8] + z[39];
z[60] = z[4] + -z[5] + T(3) * z[60];
z[59] = (T(13) * z[2]) / T(2) + (T(-9) * z[27]) / T(2) + z[59] / T(2) + T(2) * z[60];
z[59] = z[28] * z[59];
z[56] = T(12) * z[56] + z[59];
z[56] = z[28] * z[56];
z[55] = T(6) * z[55] + z[56];
z[55] = z[7] * z[55];
z[56] = T(2) * z[23];
z[56] = z[56] * z[88];
z[52] = -z[52] + -z[56] + -z[72] + z[91] + z[103];
z[52] = z[18] * z[52];
z[56] = -z[62] + -z[73] + z[82];
z[56] = z[12] * z[56];
z[59] = z[17] * z[75];
z[60] = z[12] + z[17] + z[25];
z[60] = z[60] * z[95];
z[62] = z[12] + -z[18];
z[62] = z[62] * z[94];
z[64] = T(2) * z[25];
z[65] = z[64] * z[89];
z[68] = z[17] + -z[18];
z[70] = z[68] * z[92];
z[71] = z[5] + z[10] + T(-2) * z[41];
z[72] = z[6] + z[71];
z[73] = z[40] * z[72];
z[52] = z[52] + -z[56] + -z[59] + -z[60] + z[62] + z[65] + -z[70] + T(2) * z[73];
z[56] = z[25] * z[80];
z[59] = z[38] * z[68];
z[60] = z[17] * z[86];
z[62] = z[18] * z[96];
z[56] = z[56] + -z[59] + z[60] + -z[62];
z[59] = int_to_imaginary<T>(6);
z[56] = z[56] * z[59];
z[59] = -z[17] + z[25];
z[59] = z[28] * z[59];
z[56] = -z[56] + z[59];
z[56] = z[28] * z[56];
z[52] = T(3) * z[52] + -z[56];
z[56] = z[13] + z[14] + z[15];
z[56] = T(4) * z[16] + T(-2) * z[56];
z[52] = z[52] * z[56];
z[54] = z[54] + z[57];
z[54] = z[6] * z[54];
z[53] = -(z[10] * z[53]);
z[56] = T(2) * z[84] + -z[85];
z[56] = z[22] * z[56];
z[57] = -(z[4] * z[69]);
z[50] = z[27] * z[50];
z[50] = z[50] + z[53] + z[54] + z[56] + z[57] + z[67] + z[83];
z[53] = z[33] * z[99];
z[50] = T(6) * z[50] + T(12) * z[53];
z[50] = z[9] * z[50];
z[53] = z[6] * z[93];
z[54] = z[22] * z[80];
z[56] = -(z[26] * z[27]);
z[53] = z[53] + z[54] + z[56] + z[58] + -z[66];
z[53] = z[9] * z[53];
z[54] = -(z[46] * z[64]);
z[53] = z[53] + z[54];
z[54] = T(3) * z[18];
z[56] = T(-6) * z[25] + -z[54] + z[61] + T(2) * z[71];
z[56] = z[42] * z[56];
z[57] = z[18] + z[25];
z[57] = z[45] * z[57];
z[56] = z[56] + T(6) * z[57];
z[56] = z[42] * z[56];
z[53] = T(6) * z[53] + z[56];
z[53] = int_to_imaginary<T>(1) * z[53];
z[56] = (T(-5) * z[3]) / T(2) + T(2) * z[10] + (T(-13) * z[27]) / T(2) + T(6) * z[39] + z[63] / T(2) + -z[87];
z[56] = z[9] * z[56];
z[57] = T(15) * z[18] + T(7) * z[25];
z[57] = z[45] * z[57];
z[58] = T(-7) * z[18] + -z[25] / T(2);
z[58] = z[42] * z[58];
z[59] = T(3) * z[25] + T(4) * z[72];
z[59] = int_to_imaginary<T>(1) * z[28] * z[59];
z[56] = z[56] + z[57] + z[58] + z[59];
z[56] = z[28] * z[56];
z[53] = T(2) * z[53] + z[56];
z[53] = z[28] * z[53];
z[56] = int_to_imaginary<T>(1) * z[44];
z[57] = int_to_imaginary<T>(1) * z[42];
z[57] = z[28] + T(-3) * z[57];
z[57] = z[43] * z[57];
z[56] = T(-192) * z[56] + T(16) * z[57];
z[56] = z[56] * z[72];
z[57] = prod_pow(z[45], 3);
z[57] = T(-12) * z[47] + T(-3) * z[48] + T(4) * z[57];
z[54] = z[25] + z[54];
z[54] = z[54] * z[57];
z[57] = -prod_pow(z[42], 2);
z[57] = T(-4) * z[46] + z[57];
z[57] = z[18] * z[42] * z[57];
z[58] = T(13) * z[18] + T(5) * z[25];
z[58] = z[49] * z[58];
return z[50] + T(2) * z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + T(6) * z[57] + (T(7) * z[58]) / T(2);
}



template IntegrandConstructorType<double> f_4_350_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_350_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_350_construct (const Kin<qd_real>&);
#endif

}