#include "f_4_212.h"

namespace PentagonFunctions {

template <typename T> T f_4_212_abbreviated (const std::array<T,29>&);

template <typename T> class SpDLog_f_4_212_W_10 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_212_W_10 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1]) * (kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(4) + T(-2) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] + prod_pow(abb[2], 2) * (-abb[4] + abb[5] * T(-2)) + prod_pow(abb[1], 2) * (abb[4] + -abb[3] + abb[5] * T(2)));
    }
};
template <typename T> class SpDLog_f_4_212_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_212_W_22 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (T(-4) + T(4) * kin.v[0] + T(2) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[6] * (-(abb[4] * prod_pow(abb[8], 2)) + prod_pow(abb[8], 2) * (abb[10] + abb[9] * T(-2)) + prod_pow(abb[7], 2) * (abb[4] + -abb[10] + abb[9] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_212_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl22 = DLog_W_22<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl8 = DLog_W_8<T>(kin),dl15 = DLog_W_15<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),spdl10 = SpDLog_f_4_212_W_10<T>(kin),spdl22 = SpDLog_f_4_212_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,29> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[16] / kin_path.W[16]), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), dl5(t), -rlog(t), dl20(t), -rlog(t), dl8(t), rlog(v_path[2]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl3(t), f_2_1_7(kin_path), f_2_1_14(kin_path), dl16(t), dl2(t), dl17(t), dl19(t)}
;

        auto result = f_4_212_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_212_abbreviated(const std::array<T,29>& abb)
{
using TR = typename T::value_type;
T z[57];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[19];
z[3] = abb[22];
z[4] = abb[26];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[11];
z[8] = abb[12];
z[9] = abb[2];
z[10] = abb[27];
z[11] = abb[16];
z[12] = bc<TR>[0];
z[13] = abb[20];
z[14] = abb[21];
z[15] = abb[23];
z[16] = abb[7];
z[17] = abb[25];
z[18] = abb[28];
z[19] = abb[8];
z[20] = abb[17];
z[21] = abb[18];
z[22] = abb[24];
z[23] = abb[9];
z[24] = abb[10];
z[25] = abb[14];
z[26] = abb[13];
z[27] = abb[15];
z[28] = bc<TR>[2];
z[29] = bc<TR>[9];
z[30] = z[1] + -z[6];
z[31] = z[3] * z[30];
z[32] = T(2) * z[6];
z[33] = z[5] + z[32];
z[34] = -z[1] + z[33];
z[35] = z[4] * z[34];
z[36] = z[10] * z[34];
z[30] = z[2] * z[30];
z[31] = z[30] + z[31] + z[35] + -z[36];
z[35] = -(z[14] * z[31]);
z[37] = -z[23] + z[24];
z[38] = z[27] * z[37];
z[39] = -z[30] + z[38];
z[40] = z[5] + T(2) * z[23];
z[41] = -z[24] + z[40];
z[42] = z[17] * z[41];
z[43] = -z[6] + z[23];
z[44] = z[3] * z[43];
z[36] = -z[36] + z[39] + z[42] + z[44];
z[36] = z[11] * z[36];
z[44] = z[18] * z[41];
z[37] = z[3] * z[37];
z[44] = z[37] + z[38] + z[44];
z[42] = -z[42] + z[44];
z[42] = z[21] * z[42];
z[45] = T(2) * z[10];
z[46] = z[3] + -z[4] + z[45];
z[46] = z[34] * z[46];
z[47] = z[9] * z[46];
z[48] = z[4] + -z[18];
z[49] = -z[10] + z[17];
z[50] = z[48] / T(4) + -z[49];
z[51] = prod_pow(z[12], 2);
z[50] = z[50] * z[51];
z[52] = z[3] + T(2) * z[17] + -z[18];
z[52] = z[41] * z[52];
z[53] = z[16] * z[52];
z[49] = z[48] + z[49];
z[54] = prod_pow(z[28], 2) * z[49];
z[35] = z[35] + z[36] + z[42] + -z[47] + z[50] / T(3) + z[53] + -z[54] / T(2);
z[35] = int_to_imaginary<T>(1) * z[12] * z[35];
z[36] = z[1] / T(2);
z[42] = z[5] / T(2);
z[50] = -z[6] + z[36] + -z[42];
z[50] = z[4] * z[50];
z[53] = z[24] / T(2);
z[42] = -z[23] + -z[42] + z[53];
z[54] = z[18] * z[42];
z[55] = z[50] + -z[54];
z[43] = -z[36] + T(2) * z[43] + z[53];
z[43] = z[3] * z[43];
z[56] = -z[6] + z[8];
z[56] = -z[56] / T(2);
z[56] = z[7] * z[56];
z[43] = (T(5) * z[39]) / T(2) + z[43] + z[55] + z[56];
z[56] = (T(2) * z[5]) / T(3);
z[36] = (T(-4) * z[6]) / T(3) + z[8] / T(6) + z[36] + -z[56];
z[36] = z[10] * z[36];
z[53] = (T(4) * z[23]) / T(3) + -z[25] / T(6) + -z[53] + z[56];
z[53] = z[17] * z[53];
z[49] = z[28] * z[49];
z[36] = z[36] + z[43] / T(3) + (T(-3) * z[49]) / T(4) + z[53];
z[36] = z[36] * z[51];
z[31] = z[13] * z[31];
z[43] = z[3] / T(2);
z[49] = z[1] + -z[24];
z[49] = z[43] * z[49];
z[39] = -z[39] + z[49] + -z[55];
z[39] = z[11] * z[39];
z[39] = z[39] + z[47];
z[39] = z[11] * z[39];
z[49] = z[1] + z[5];
z[43] = z[43] * z[49];
z[32] = T(2) * z[8] + -z[32];
z[32] = z[7] * z[32];
z[30] = -z[30] + z[32] + z[43] + z[50];
z[30] = z[0] * z[30];
z[30] = z[30] + -z[47];
z[30] = z[0] * z[30];
z[32] = -(z[20] * z[44]);
z[42] = z[3] * z[42];
z[40] = -z[24] + T(2) * z[25] + -z[40];
z[40] = z[17] * z[40];
z[40] = z[40] + z[42] + z[54];
z[40] = z[16] * z[40];
z[42] = -(z[11] * z[52]);
z[40] = z[40] + z[42];
z[40] = z[16] * z[40];
z[37] = -z[37] + z[38];
z[38] = prod_pow(z[19], 2);
z[37] = z[37] * z[38];
z[42] = z[15] * z[46];
z[43] = z[20] + z[38];
z[41] = z[41] * z[43];
z[43] = (T(151) * z[29]) / T(24);
z[41] = z[41] + -z[43];
z[41] = z[17] * z[41];
z[33] = -z[8] + z[33];
z[33] = z[33] * z[45];
z[34] = z[3] * z[34];
z[33] = z[33] + z[34];
z[33] = prod_pow(z[9], 2) * z[33];
z[34] = z[22] * z[52];
z[38] = T(-2) * z[38] + z[51] / T(6);
z[44] = -z[23] + z[25];
z[38] = z[26] * z[38] * z[44];
z[44] = z[29] * z[48];
z[43] = z[10] * z[43];
return z[30] + z[31] + z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + (T(7) * z[44]) / T(8);
}



template IntegrandConstructorType<double> f_4_212_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_212_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_212_construct (const Kin<qd_real>&);
#endif

}