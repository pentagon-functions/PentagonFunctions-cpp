#include "f_4_278.h"

namespace PentagonFunctions {

template <typename T> T f_4_278_abbreviated (const std::array<T,41>&);

template <typename T> class SpDLog_f_4_278_W_12 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_278_W_12 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(3) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[6] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(-3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_278_W_10 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_278_W_10 (const Kin<T>& kin) {
        c[0] = T(-4) * kin.v[0] * kin.v[2] + kin.v[2] * (T(6) * kin.v[2] + T(8) * kin.v[3]) + kin.v[1] * (T(-4) * kin.v[0] + T(2) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(-2) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));
c[1] = kin.v[0] * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-3) * kin.v[2] + T(4) * kin.v[3]) + kin.v[2] * (T(-6) * kin.v[2] + T(8) * kin.v[3]) + kin.v[1] * ((T(-10) + bc<T>[0] * int_to_imaginary<T>(-5)) * kin.v[1] + T(-16) * kin.v[2] + T(8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(4) * kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(2) * kin.v[0] * kin.v[2] + kin.v[1] * (-kin.v[1] + T(2) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[2] * (T(-3) * kin.v[2] + T(-4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4]))) + rlog(-kin.v[1]) * (kin.v[0] * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2])) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-2) * kin.v[3]) + kin.v[1] * (-kin.v[1] / T(2) + kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2])) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-2) * kin.v[3]) + kin.v[1] * (-kin.v[1] / T(2) + kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[0] * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2])) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-2) * kin.v[3]) + kin.v[1] * (-kin.v[1] / T(2) + kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3])) + rlog(-kin.v[4]) * (kin.v[0] * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2])) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-2) * kin.v[3]) + kin.v[1] * (-kin.v[1] / T(2) + kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3])) + rlog(kin.v[2]) * (T(-3) * kin.v[0] * kin.v[2] + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(6) * kin.v[3]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[2] + T(3) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (T(-3) * kin.v[0] * kin.v[2] + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(6) * kin.v[3]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[2] + T(3) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[1] + T(8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[2];
c[3] = rlog(-kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2]) + rlog(-kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2]) + rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[2] + T(6) * kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[2] + T(6) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[27] * (abb[3] * (abb[14] * (abb[15] + -abb[8] + -abb[14] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[28] * T(2)) + abb[1] * (abb[3] * abb[14] + abb[2] * (-abb[3] + -abb[5] + -abb[6] + -abb[12] + abb[29] * T(-4) + abb[30] * T(-2) + abb[4] * T(3) + abb[11] * T(3)) + abb[14] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[11] * T(-3) + abb[30] * T(2) + abb[29] * T(4))) + abb[14] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[30] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(3) + abb[29] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[8] * (-abb[5] + -abb[6] + -abb[12] + abb[29] * T(-4) + abb[30] * T(-2) + abb[4] * T(3) + abb[11] * T(3)) + abb[14] * (-abb[5] + -abb[6] + -abb[12] + abb[29] * T(-4) + abb[30] * T(-2) + abb[4] * T(3) + abb[11] * T(3)) + abb[15] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[11] * T(-3) + abb[30] * T(2) + abb[29] * T(4))) + abb[28] * (abb[4] * T(-6) + abb[11] * T(-6) + abb[5] * T(2) + abb[6] * T(2) + abb[12] * T(2) + abb[30] * T(4) + abb[29] * T(8)) + abb[2] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(1) + abb[3] * (abb[8] + -abb[14] + -abb[15] + bc<T>[0] * int_to_imaginary<T>(1)) + abb[30] * bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[29] * bc<T>[0] * int_to_imaginary<T>(4) + abb[14] * (-abb[5] + -abb[6] + -abb[12] + abb[29] * T(-4) + abb[30] * T(-2) + abb[4] * T(3) + abb[11] * T(3)) + abb[15] * (-abb[5] + -abb[6] + -abb[12] + abb[29] * T(-4) + abb[30] * T(-2) + abb[4] * T(3) + abb[11] * T(3)) + abb[8] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[11] * T(-3) + abb[30] * T(2) + abb[29] * T(4)) + abb[2] * (abb[4] * T(-6) + abb[11] * T(-6) + abb[3] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[12] * T(2) + abb[30] * T(4) + abb[29] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_278_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl4 = DLog_W_4<T>(kin),dl24 = DLog_W_24<T>(kin),dl20 = DLog_W_20<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl1 = DLog_W_1<T>(kin),dl10 = DLog_W_10<T>(kin),dl19 = DLog_W_19<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),spdl12 = SpDLog_f_4_278_W_12<T>(kin),spdl10 = SpDLog_f_4_278_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,41> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[16] / kin_path.W[16]), dl4(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_4(kin_path), f_2_1_11(kin_path), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), dl24(t), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl20(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl29(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl28(t) / kin_path.SqrtDelta, f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl1(t), dl10(t), f_2_1_7(kin_path), -rlog(t), rlog(kin.W[18] / kin_path.W[18]), dl19(t), dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl3(t), dl17(t), dl16(t), dl18(t), dl5(t), dl2(t)}
;

        auto result = f_4_278_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_278_abbreviated(const std::array<T,41>& abb)
{
using TR = typename T::value_type;
T z[127];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[16];
z[4] = abb[31];
z[5] = abb[35];
z[6] = abb[36];
z[7] = abb[38];
z[8] = abb[39];
z[9] = abb[40];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[11];
z[14] = abb[12];
z[15] = abb[19];
z[16] = abb[20];
z[17] = abb[21];
z[18] = abb[22];
z[19] = abb[32];
z[20] = abb[33];
z[21] = abb[29];
z[22] = abb[30];
z[23] = abb[2];
z[24] = abb[8];
z[25] = abb[14];
z[26] = abb[15];
z[27] = bc<TR>[0];
z[28] = abb[23];
z[29] = abb[37];
z[30] = abb[34];
z[31] = abb[9];
z[32] = abb[10];
z[33] = abb[26];
z[34] = abb[17];
z[35] = abb[18];
z[36] = abb[24];
z[37] = abb[25];
z[38] = abb[28];
z[39] = abb[13];
z[40] = bc<TR>[3];
z[41] = bc<TR>[1];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[9];
z[45] = z[13] / T(2);
z[46] = z[14] / T(2);
z[47] = z[45] + -z[46];
z[48] = -z[41] + T(3) * z[42];
z[49] = z[10] / T(2);
z[50] = z[48] + -z[49];
z[51] = z[1] / T(2);
z[52] = -z[11] + z[12];
z[53] = -z[47] + -z[50] + -z[51] + (T(-7) * z[52]) / T(3);
z[53] = z[7] * z[53];
z[54] = -z[11] + T(3) * z[12];
z[54] = z[54] / T(2);
z[55] = (T(3) * z[1]) / T(2);
z[56] = (T(-5) * z[10]) / T(2) + -z[45] + -z[46] + z[54] + z[55];
z[57] = z[22] + z[56];
z[57] = z[21] + z[57] / T(2);
z[58] = -z[41] + z[57];
z[58] = z[5] * z[58];
z[59] = z[22] / T(2);
z[60] = T(2) * z[12];
z[61] = (T(5) * z[11]) / T(2) + z[59] + -z[60];
z[62] = z[21] / T(3);
z[63] = (T(5) * z[13]) / T(2);
z[50] = (T(-2) * z[1]) / T(3) + (T(7) * z[14]) / T(3) + -z[50] + z[61] / T(3) + z[62] + -z[63];
z[50] = z[8] * z[50];
z[61] = -z[13] + z[14];
z[64] = z[52] + z[61];
z[65] = z[1] + -z[10];
z[66] = z[64] + -z[65];
z[67] = z[3] * z[66];
z[68] = z[16] + z[17] + z[18];
z[69] = z[15] * z[68];
z[67] = z[67] + z[69];
z[64] = z[64] + z[65];
z[69] = z[2] * z[64];
z[70] = z[67] + -z[69];
z[71] = z[11] / T(2) + -z[12];
z[72] = z[46] + -z[59] + z[71];
z[62] = (T(-5) * z[1]) / T(6) + z[10] + z[48] + -z[62] + z[72] / T(3);
z[62] = z[6] * z[62];
z[72] = (T(3) * z[13]) / T(2);
z[73] = (T(3) * z[10]) / T(2);
z[74] = z[72] + z[73];
z[75] = z[46] + z[51];
z[76] = z[74] + -z[75];
z[77] = -z[22] + z[76];
z[78] = T(5) * z[11];
z[79] = -z[12] + -z[78];
z[79] = z[77] + z[79] / T(6);
z[79] = -z[21] + z[41] + z[79] / T(2);
z[79] = z[9] * z[79];
z[61] = -z[52] + z[61];
z[80] = -z[1] + z[10] + -z[61];
z[80] = z[33] * z[80];
z[81] = z[19] * z[68];
z[82] = z[28] * z[68];
z[83] = z[81] + -z[82];
z[84] = z[39] * z[61];
z[85] = T(3) * z[84];
z[47] = z[47] + z[52] / T(3);
z[47] = z[4] * z[47];
z[86] = -z[5] + z[9];
z[87] = z[19] + z[20] + z[30];
z[88] = (T(3) * z[7]) / T(4) + z[86] / T(4) + z[87] / T(9);
z[88] = int_to_imaginary<T>(1) * z[88];
z[89] = int_to_imaginary<T>(1) * z[29];
z[88] = z[88] + (T(-3) * z[89]) / T(4);
z[88] = z[27] * z[88];
z[48] = z[29] * z[48];
z[47] = z[47] + z[48] + z[50] + z[53] + z[58] + z[62] + z[70] / T(4) + z[79] + -z[80] + -z[83] / T(2) + -z[85] + z[88];
z[47] = z[27] * z[47];
z[48] = z[20] * z[68];
z[50] = -z[48] + z[82];
z[53] = z[50] + z[67];
z[58] = -z[80] + -z[81];
z[62] = z[53] + z[58];
z[79] = z[9] * z[66];
z[80] = z[61] + -z[65];
z[83] = z[7] * z[80];
z[61] = z[61] + z[65];
z[65] = z[6] * z[61];
z[88] = z[4] * z[80];
z[65] = -z[62] + z[65] + z[79] + -z[83] + z[88];
z[65] = (T(3) * z[65]) / T(2);
z[79] = -(z[35] * z[65]);
z[83] = T(3) * z[10];
z[90] = T(3) * z[1] + -z[83];
z[91] = T(3) * z[13];
z[92] = T(3) * z[14] + -z[90] + -z[91];
z[93] = -z[52] + z[92];
z[94] = z[93] / T(2);
z[95] = z[9] * z[94];
z[96] = z[52] + z[92];
z[97] = z[7] / T(2);
z[98] = z[96] * z[97];
z[60] = -z[11] + -z[14] + z[60];
z[99] = T(2) * z[21] + z[22];
z[100] = -z[1] + z[99];
z[101] = z[60] + z[100];
z[102] = z[6] * z[101];
z[103] = z[4] * z[52];
z[104] = z[102] + z[103];
z[105] = (T(3) * z[58]) / T(2);
z[95] = -z[95] + z[98] + z[104] + z[105];
z[98] = -(z[24] * z[95]);
z[106] = z[11] + z[12];
z[107] = z[106] / T(2);
z[76] = z[76] + -z[99] + -z[107];
z[108] = z[0] * z[76];
z[109] = z[23] * z[76];
z[110] = z[108] + -z[109];
z[111] = z[41] * z[42];
z[112] = prod_pow(z[41], 2);
z[111] = -z[111] + z[112] / T(2);
z[112] = T(3) * z[111];
z[113] = -z[110] + z[112];
z[113] = z[6] * z[113];
z[114] = z[25] * z[76];
z[115] = z[0] * z[52];
z[116] = T(2) * z[115];
z[117] = -z[112] + z[116];
z[118] = T(2) * z[11];
z[100] = -z[12] + z[100] + z[118];
z[119] = T(2) * z[14] + -z[91] + z[100];
z[120] = T(2) * z[119];
z[121] = z[23] * z[120];
z[122] = -z[114] + z[117] + z[121];
z[122] = z[7] * z[122];
z[101] = z[25] * z[101];
z[94] = z[0] * z[94];
z[123] = z[94] + -z[101] + -z[109];
z[123] = z[4] * z[123];
z[124] = -z[109] + z[114];
z[117] = z[117] + -z[124];
z[117] = z[9] * z[117];
z[125] = T(2) * z[7];
z[126] = z[9] + -z[125];
z[126] = z[52] * z[126];
z[85] = -z[85] + -z[104] + z[126];
z[85] = z[26] * z[85];
z[102] = T(2) * z[102] + z[105];
z[102] = z[25] * z[102];
z[53] = z[0] * z[53];
z[68] = z[30] * z[68];
z[104] = -z[50] + z[68];
z[105] = z[23] * z[104];
z[53] = z[53] + z[105];
z[53] = (T(3) * z[53]) / T(2);
z[105] = z[5] * z[112];
z[79] = -z[53] + z[79] + T(2) * z[85] + z[98] + -z[102] + z[105] + z[113] + z[117] + z[122] + z[123];
z[79] = int_to_imaginary<T>(1) * z[79];
z[85] = z[108] + z[121];
z[98] = -(z[24] * z[76]);
z[105] = z[26] * z[120];
z[98] = z[85] + z[98] + z[105] + -z[112] + -z[114];
z[105] = int_to_imaginary<T>(1) * z[8];
z[98] = z[98] * z[105];
z[108] = z[49] + -z[99];
z[45] = z[45] + z[75] + z[108];
z[54] = z[45] + -z[54];
z[54] = z[6] * z[54];
z[112] = z[58] + z[104];
z[113] = z[4] / T(2);
z[61] = z[61] * z[113];
z[54] = -z[54] + -z[61] + z[112] / T(2);
z[61] = T(3) * z[11] + -z[12];
z[61] = z[61] / T(2);
z[51] = (T(-3) * z[14]) / T(2) + z[51] + -z[61] + z[63];
z[63] = z[51] + z[108];
z[108] = z[7] * z[63];
z[108] = z[54] + z[108];
z[108] = int_to_imaginary<T>(1) * z[108];
z[105] = -z[89] + z[105];
z[112] = z[63] * z[105];
z[108] = z[108] + z[112];
z[108] = z[37] * z[108];
z[112] = z[23] * z[63];
z[111] = z[111] + z[112];
z[89] = z[89] * z[111];
z[111] = z[6] + -z[7] + -z[86];
z[111] = int_to_imaginary<T>(1) * z[111];
z[105] = -z[105] + z[111];
z[105] = z[43] * z[105];
z[87] = z[40] * z[87];
z[87] = -z[87] + z[89] + z[105] + z[108];
z[47] = z[47] + z[79] + T(3) * z[87] + z[98];
z[47] = z[27] * z[47];
z[79] = z[26] * z[95];
z[87] = z[6] * z[110];
z[48] = z[48] + z[81];
z[89] = z[48] + -z[69];
z[95] = z[23] * z[89];
z[89] = z[0] * z[89];
z[89] = z[89] + -z[95];
z[94] = z[94] + z[124];
z[98] = -(z[7] * z[94]);
z[105] = z[0] / T(2);
z[108] = z[96] * z[105];
z[108] = z[108] + -z[124];
z[108] = z[9] * z[108];
z[100] = -z[14] + z[100];
z[111] = z[23] * z[100];
z[101] = -z[101] + z[111] + z[115];
z[101] = z[4] * z[101];
z[97] = z[9] / T(2) + z[97];
z[97] = z[52] * z[97];
z[59] = z[21] + z[59] + -z[75];
z[71] = -z[59] + z[71];
z[71] = z[6] * z[71];
z[71] = z[71] + z[97] + -z[103] / T(2);
z[71] = z[24] * z[71];
z[71] = z[71] + z[79] + -z[87] + (T(3) * z[89]) / T(2) + z[98] + z[101] + -z[102] + z[108];
z[71] = z[24] * z[71];
z[79] = z[26] * z[76];
z[89] = T(2) * z[111];
z[97] = -(z[0] * z[100]);
z[98] = z[12] / T(2);
z[59] = z[11] + z[59] + -z[98];
z[100] = z[24] * z[59];
z[79] = z[79] + z[89] + z[97] + z[100] + -z[114];
z[79] = z[24] * z[79];
z[77] = z[77] + -z[107];
z[77] = -z[21] + z[77] / T(2);
z[97] = z[25] * z[77];
z[97] = z[97] + z[110];
z[97] = z[25] * z[97];
z[73] = (T(9) * z[13]) / T(2) + -z[73];
z[100] = T(5) * z[12];
z[101] = T(7) * z[11] + -z[100];
z[102] = (T(5) * z[1]) / T(2);
z[101] = (T(7) * z[14]) / T(2) + -z[73] + z[99] + z[101] / T(2) + -z[102];
z[103] = z[38] * z[101];
z[107] = z[25] * z[120];
z[108] = -(z[26] * z[119]);
z[85] = -z[85] + z[107] + z[108];
z[85] = z[26] * z[85];
z[107] = T(23) * z[11] + T(-13) * z[12];
z[46] = (T(-13) * z[1]) / T(2) + T(5) * z[22] + -z[46] + -z[73] + z[107] / T(2);
z[46] = T(5) * z[21] + z[46] / T(2);
z[73] = prod_pow(z[23], 2);
z[46] = z[46] * z[73];
z[59] = z[0] * z[59];
z[59] = z[59] + -z[89];
z[59] = z[0] * z[59];
z[45] = -z[45] + z[61];
z[61] = T(3) * z[31];
z[45] = z[45] * z[61];
z[45] = (T(85) * z[44]) / T(6) + z[45] + z[46] + z[59] + z[79] + z[85] + z[97] + z[103];
z[45] = z[8] * z[45];
z[46] = T(11) * z[52] + -z[92];
z[46] = z[7] * z[46];
z[59] = z[4] * z[96];
z[46] = z[46] + z[59];
z[59] = -z[62] / T(4) + z[84];
z[62] = z[1] + z[14];
z[79] = z[11] + z[62] + -z[100];
z[85] = z[10] + z[13];
z[79] = -z[79] / T(4) + (T(-3) * z[85]) / T(4) + z[99];
z[85] = z[6] * z[79];
z[89] = z[93] / T(4);
z[92] = z[9] * z[89];
z[46] = z[46] / T(4) + T(3) * z[59] + z[85] + z[92];
z[46] = z[26] * z[46];
z[59] = -(z[4] * z[94]);
z[85] = z[116] + -z[124];
z[85] = -(z[9] * z[85]);
z[76] = z[6] * z[76];
z[76] = z[76] + (T(-3) * z[104]) / T(2);
z[76] = z[25] * z[76];
z[92] = -z[23] + z[25];
z[93] = z[92] * z[119];
z[93] = z[93] + -z[115];
z[93] = z[93] * z[125];
z[46] = z[46] + z[53] + z[59] + z[76] + z[85] + z[87] + z[93];
z[46] = z[26] * z[46];
z[53] = z[81] + z[82];
z[59] = z[7] * z[66];
z[66] = -(z[9] * z[80]);
z[76] = -(z[8] * z[64]);
z[59] = -z[53] + z[59] + z[66] + -z[70] + z[76] + z[88];
z[59] = z[32] * z[59];
z[66] = -z[48] + -z[67];
z[66] = z[66] / T(2) + z[69];
z[66] = z[0] * z[66];
z[66] = z[66] + z[95];
z[66] = z[0] * z[66];
z[48] = z[48] + z[68];
z[67] = z[48] * z[73];
z[59] = z[59] + z[66] + -z[67] / T(2);
z[66] = T(4) * z[21] + T(2) * z[22];
z[62] = z[62] + z[66] + -z[83] + -z[91] + z[106];
z[62] = z[38] * z[62];
z[67] = -z[12] + z[78];
z[66] = z[66] + z[67] / T(2) + -z[74] + -z[75];
z[66] = z[66] * z[73];
z[67] = -(z[25] * z[79]);
z[67] = z[67] + -z[110];
z[67] = z[25] * z[67];
z[52] = -z[52] + -z[90];
z[52] = z[52] * z[105];
z[52] = z[52] + -z[111];
z[52] = z[0] * z[52];
z[52] = z[52] + z[62] + z[66] + z[67];
z[52] = z[4] * z[52];
z[55] = T(-3) * z[21] + (T(-3) * z[22]) / T(2) + -z[55] + z[72] + z[83] + z[98] + -z[118];
z[55] = z[0] * z[55];
z[55] = z[55] + -z[109];
z[55] = z[0] * z[55];
z[60] = T(2) * z[1] + z[60] + -z[83] + z[99];
z[62] = z[0] + -z[23];
z[66] = -(z[60] * z[62]);
z[66] = T(2) * z[66];
z[60] = z[25] * z[60];
z[60] = z[60] + -z[66];
z[60] = z[25] * z[60];
z[67] = z[73] * z[77];
z[70] = T(7) * z[12] + -z[78];
z[70] = (T(-5) * z[14]) / T(2) + z[70] / T(2);
z[72] = (T(7) * z[1]) / T(2) + (T(-9) * z[10]) / T(2) + z[70] + z[72] + z[99];
z[75] = z[38] * z[72];
z[55] = z[55] + z[60] + z[67] + z[75];
z[55] = z[9] * z[55];
z[60] = z[34] * z[65];
z[56] = z[56] + z[99];
z[65] = -(z[25] * z[62]);
z[65] = -z[38] + z[65];
z[65] = z[56] * z[65];
z[75] = prod_pow(z[0], 2);
z[75] = -z[73] + z[75];
z[57] = z[57] * z[75];
z[57] = z[57] + z[65];
z[57] = z[5] * z[57];
z[65] = z[7] + z[8] + -z[29];
z[65] = z[63] * z[65];
z[54] = z[54] + z[65];
z[54] = z[36] * z[54];
z[54] = z[54] + z[57];
z[57] = z[0] * z[89];
z[57] = z[57] + -z[109];
z[57] = z[0] * z[57];
z[57] = z[57] + z[67] + z[97];
z[57] = z[7] * z[57];
z[49] = -z[22] + z[49] + z[51];
z[49] = -z[21] + z[49] / T(2);
z[51] = -prod_pow(z[25], 2);
z[51] = z[51] + z[73];
z[49] = z[49] * z[51];
z[51] = z[38] * z[63];
z[63] = z[26] * z[63] * z[92];
z[49] = z[49] + z[51] + z[63];
z[49] = -z[44] + T(3) * z[49];
z[49] = z[29] * z[49];
z[51] = z[0] * z[77];
z[51] = z[51] + -z[109];
z[51] = z[0] * z[51];
z[51] = z[51] + z[67];
z[51] = z[6] * z[51];
z[58] = -z[50] + z[58] + z[68];
z[58] = z[58] / T(4) + -z[84];
z[63] = -z[22] + -z[70] + -z[74] + z[102];
z[63] = -z[21] + z[63] / T(2);
z[63] = z[6] * z[63];
z[58] = T(3) * z[58] + z[63];
z[58] = z[25] * z[58];
z[63] = -(z[6] * z[66]);
z[53] = -(z[53] * z[62]);
z[53] = (T(3) * z[53]) / T(2) + z[58] + z[63];
z[53] = z[25] * z[53];
z[58] = z[7] * z[101];
z[62] = z[6] * z[72];
z[48] = (T(-3) * z[48]) / T(2) + z[58] + z[62];
z[48] = z[38] * z[48];
z[58] = -z[6] + -z[86];
z[56] = z[56] * z[58];
z[50] = z[50] + z[69];
z[58] = -(z[64] * z[113]);
z[50] = z[50] / T(2) + z[56] + z[58];
z[50] = z[50] * z[61];
z[56] = (T(-85) * z[6]) / T(6) + z[7] + (T(5) * z[86]) / T(2);
z[56] = z[44] * z[56];
return z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + z[53] + T(3) * z[54] + z[55] + z[56] + z[57] + (T(3) * z[59]) / T(2) + z[60] + z[71];
}



template IntegrandConstructorType<double> f_4_278_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_278_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_278_construct (const Kin<qd_real>&);
#endif

}