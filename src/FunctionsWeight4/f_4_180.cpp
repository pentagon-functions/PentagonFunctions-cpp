#include "f_4_180.h"

namespace PentagonFunctions {

template <typename T> T f_4_180_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_180_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_180_W_21 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(3) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[6] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(-3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_180_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_180_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(2) * kin.v[2] + T(8) * kin.v[3]) + kin.v[3] * (T(-10) * kin.v[3] + T(-12) * kin.v[4]) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[0] + T(-2) * kin.v[1] + T(12) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(-4) + T(4) * kin.v[0] + T(2) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));
c[1] = kin.v[2] * (T(14) * kin.v[2] + T(-16) * kin.v[3] + T(-24) * kin.v[4]) + kin.v[1] * ((bc<T>[0] * int_to_imaginary<T>(5) + T(10)) * kin.v[1] + T(24) * kin.v[2] + T(-12) * kin.v[3] + T(-20) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(12) * kin.v[2] + T(-6) * kin.v[3] + T(-10) * kin.v[4])) + T(10) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(2) * kin.v[3] + T(12) * kin.v[4]) + kin.v[0] * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(7) * kin.v[2] + T(-8) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[3] * (kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + kin.v[0] + T(-3) * kin.v[3]) + kin.v[2] * (-kin.v[2] / T(2) + T(-2) * kin.v[3]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[0] + T(1) + kin.v[3]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]))) + rlog(kin.v[3]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + kin.v[0] + T(-3) * kin.v[3]) + kin.v[2] * (-kin.v[2] / T(2) + T(-2) * kin.v[3]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[0] + T(1) + kin.v[3]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + kin.v[0] + T(-3) * kin.v[3]) + kin.v[2] * (-kin.v[2] / T(2) + T(-2) * kin.v[3]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[0] + T(1) + kin.v[3]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + kin.v[0] + T(-3) * kin.v[3]) + kin.v[2] * (-kin.v[2] / T(2) + T(-2) * kin.v[3]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[0] + T(1) + kin.v[3]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]))) + rlog(-kin.v[1]) * (kin.v[2] * (-kin.v[2] + T(-4) * kin.v[3]) + prod_pow(kin.v[4], 2) + kin.v[1] * (T(2) * kin.v[0] + kin.v[1] + T(-6) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * (T(5) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(kin.v[2]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(6) * kin.v[3]) + kin.v[3] * ((T(-15) * kin.v[3]) / T(2) + T(-9) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) * kin.v[0] + T(9) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3]) + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(6) * kin.v[3]) + kin.v[3] * ((T(-15) * kin.v[3]) / T(2) + T(-9) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) * kin.v[0] + T(9) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3]) + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4])));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]);
c[3] = rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + rlog(kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[21] * (abb[2] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[3] * bc<T>[0] * int_to_imaginary<T>(3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(3) + abb[24] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[2] * (-abb[4] + -abb[5] + -abb[6] + -abb[8] + abb[24] * T(-4) + abb[3] * T(3) + abb[12] * T(3))) + abb[22] * (abb[2] * (abb[2] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) + abb[1] * abb[2] * T(2) + abb[2] * abb[19] * T(2) + abb[23] * T(4)) + abb[1] * abb[2] * (abb[4] + abb[5] + abb[6] + abb[8] + abb[3] * T(-3) + abb[12] * T(-3) + abb[24] * T(4)) + abb[2] * abb[19] * (abb[4] + abb[5] + abb[6] + abb[8] + abb[3] * T(-3) + abb[12] * T(-3) + abb[24] * T(4)) + abb[9] * (abb[2] * abb[22] * T(-2) + abb[2] * (-abb[4] + -abb[5] + -abb[6] + -abb[8] + abb[24] * T(-4) + abb[3] * T(3) + abb[12] * T(3)) + abb[20] * (abb[4] + abb[5] + abb[6] + abb[8] + abb[3] * T(-3) + abb[12] * T(-3) + abb[22] * T(2) + abb[24] * T(4))) + abb[23] * (abb[3] * T(-6) + abb[12] * T(-6) + abb[4] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[8] * T(2) + abb[24] * T(8)) + abb[20] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[22] * (abb[1] * T(-2) + abb[2] * T(-2) + abb[19] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2)) + abb[3] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[24] * bc<T>[0] * int_to_imaginary<T>(4) + abb[1] * (-abb[4] + -abb[5] + -abb[6] + -abb[8] + abb[24] * T(-4) + abb[3] * T(3) + abb[12] * T(3)) + abb[2] * (-abb[4] + -abb[5] + -abb[6] + -abb[8] + abb[24] * T(-4) + abb[3] * T(3) + abb[12] * T(3)) + abb[19] * (-abb[4] + -abb[5] + -abb[6] + -abb[8] + abb[24] * T(-4) + abb[3] * T(3) + abb[12] * T(3)) + abb[20] * (abb[3] * T(-6) + abb[12] * T(-6) + abb[4] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[8] * T(2) + abb[22] * T(4) + abb[24] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_180_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl18 = DLog_W_18<T>(kin),dl27 = DLog_W_27<T>(kin),dl9 = DLog_W_9<T>(kin),dl22 = DLog_W_22<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl2 = DLog_W_2<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),spdl21 = SpDLog_f_4_180_W_21<T>(kin),spdl22 = SpDLog_f_4_180_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl18(t), rlog(kin.W[3] / kin_path.W[3]), rlog(-v_path[1]), f_2_1_11(kin_path), f_2_1_15(kin_path), rlog(kin.W[16] / kin_path.W[16]), dl27(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl9(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), dl22(t), rlog(kin.W[1] / kin_path.W[1]), f_2_1_14(kin_path), -rlog(t), dl5(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl1(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl2(t), dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl19(t), dl20(t), dl4(t), dl17(t), dl16(t), dl3(t)}
;

        auto result = f_4_180_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_180_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[118];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[31];
z[4] = abb[36];
z[5] = abb[37];
z[6] = abb[40];
z[7] = abb[41];
z[8] = abb[4];
z[9] = abb[38];
z[10] = abb[5];
z[11] = abb[6];
z[12] = abb[8];
z[13] = abb[12];
z[14] = abb[13];
z[15] = abb[14];
z[16] = abb[15];
z[17] = abb[16];
z[18] = abb[17];
z[19] = abb[33];
z[20] = abb[34];
z[21] = abb[35];
z[22] = abb[22];
z[23] = abb[24];
z[24] = abb[2];
z[25] = abb[9];
z[26] = abb[25];
z[27] = abb[19];
z[28] = abb[20];
z[29] = bc<TR>[0];
z[30] = abb[39];
z[31] = abb[32];
z[32] = abb[10];
z[33] = abb[11];
z[34] = abb[28];
z[35] = abb[23];
z[36] = abb[26];
z[37] = abb[27];
z[38] = abb[29];
z[39] = abb[30];
z[40] = abb[18];
z[41] = bc<TR>[3];
z[42] = bc<TR>[1];
z[43] = bc<TR>[2];
z[44] = bc<TR>[4];
z[45] = bc<TR>[9];
z[46] = z[1] + z[13];
z[47] = z[10] + z[12];
z[48] = z[46] + -z[47];
z[49] = -z[8] + z[11];
z[50] = z[48] + z[49];
z[51] = z[2] * z[50];
z[52] = -z[15] + -z[16] + z[17] + z[18];
z[53] = z[31] * z[52];
z[54] = z[14] * z[52];
z[55] = z[20] * z[52];
z[56] = z[51] + -z[53] + z[54] + T(-3) * z[55];
z[57] = z[10] + -z[13];
z[58] = z[22] / T(2) + z[23];
z[59] = z[8] / T(6);
z[57] = -z[1] + (T(2) * z[11]) / T(3) + z[57] / T(2) + z[58] + -z[59];
z[57] = z[4] * z[57];
z[60] = (T(5) * z[10]) / T(3);
z[61] = T(3) * z[1];
z[62] = T(5) * z[13];
z[63] = (T(-19) * z[12]) / T(3) + z[60] + -z[61] + z[62];
z[64] = z[22] / T(3) + (T(2) * z[23]) / T(3);
z[63] = (T(5) * z[8]) / T(12) + -z[11] / T(12) + z[63] / T(4) + z[64];
z[63] = z[5] * z[63];
z[60] = -z[1] + -z[12] / T(3) + z[13] + z[60];
z[59] = -z[11] / T(6) + -z[59] + z[60] / T(2) + -z[64];
z[59] = z[6] * z[59];
z[60] = z[19] + -z[20] + -z[21] + z[31];
z[64] = -z[4] + z[7];
z[65] = z[9] + -z[30];
z[66] = -z[60] / T(9) + z[64] / T(4) + (T(-3) * z[65]) / T(4);
z[67] = int_to_imaginary<T>(1) * z[29];
z[66] = z[66] * z[67];
z[68] = z[12] + -z[13];
z[69] = z[49] + z[68];
z[69] = z[40] * z[69];
z[65] = -z[5] + z[6] + -z[65];
z[70] = z[43] * z[65];
z[70] = z[69] + -z[70];
z[71] = z[48] + -z[49];
z[72] = z[34] * z[71];
z[62] = z[1] + z[10] + T(-3) * z[12] + z[62];
z[62] = z[62] / T(2);
z[73] = -z[22] + z[62];
z[74] = z[8] / T(2);
z[75] = (T(3) * z[11]) / T(2) + -z[74];
z[76] = z[73] + -z[75];
z[76] = -z[23] + z[76] / T(2);
z[77] = z[30] * z[76];
z[78] = -z[48] / T(2) + z[49] / T(3);
z[78] = z[3] * z[78];
z[79] = T(5) * z[1] + T(-3) * z[10] + z[12] + z[13];
z[79] = z[79] / T(2);
z[80] = z[11] / T(2);
z[81] = (T(3) * z[8]) / T(2) + -z[80];
z[82] = z[79] + -z[81];
z[83] = -z[22] + z[82];
z[83] = -z[23] + z[83] / T(2);
z[84] = z[7] * z[83];
z[85] = -z[1] + z[12];
z[58] = (T(11) * z[8]) / T(6) + (T(-4) * z[11]) / T(3) + -z[13] + z[58] + z[85] / T(2);
z[58] = z[9] * z[58];
z[64] = z[64] + z[65];
z[65] = z[42] * z[64];
z[85] = z[19] * z[52];
z[86] = z[85] / T(2);
z[87] = z[21] * z[52];
z[88] = z[87] / T(2);
z[56] = -z[56] / T(4) + z[57] + z[58] + z[59] + z[63] + z[65] + z[66] + T(3) * z[70] + z[72] + z[77] + z[78] + z[84] + z[86] + z[88];
z[56] = z[29] * z[56];
z[57] = T(3) * z[46];
z[58] = -z[47] + z[57];
z[59] = z[58] / T(2) + -z[74] + -z[80];
z[63] = z[22] + T(2) * z[23];
z[65] = -z[59] + z[63];
z[66] = z[9] * z[65];
z[70] = z[4] * z[65];
z[52] = (T(3) * z[52]) / T(2);
z[77] = z[21] * z[52];
z[70] = z[70] + z[77];
z[78] = z[66] + z[70];
z[84] = z[5] * z[65];
z[89] = -z[47] + z[63];
z[90] = T(2) * z[8] + -z[11];
z[91] = z[89] + z[90];
z[92] = z[3] * z[91];
z[93] = z[55] + z[72];
z[91] = z[6] * z[91];
z[92] = -z[78] + -z[84] + T(2) * z[91] + z[92] + (T(-3) * z[93]) / T(2);
z[94] = z[28] * z[92];
z[68] = -z[1] + z[10] + -z[68];
z[95] = z[49] + z[68];
z[96] = z[26] * z[95];
z[97] = z[55] + z[85];
z[98] = z[96] + -z[97];
z[99] = z[6] * z[65];
z[100] = T(3) * z[68];
z[101] = -z[49] + z[100];
z[102] = z[3] / T(2);
z[103] = z[101] * z[102];
z[104] = T(2) * z[49];
z[105] = z[4] * z[104];
z[104] = z[9] * z[104];
z[98] = z[84] + (T(-3) * z[98]) / T(2) + -z[99] + z[103] + z[104] + z[105];
z[103] = z[0] * z[98];
z[94] = z[94] + z[103];
z[103] = -z[8] + T(2) * z[11];
z[105] = T(3) * z[13];
z[106] = -z[10] + T(2) * z[12] + z[63] + z[103] + -z[105];
z[107] = T(2) * z[5];
z[108] = z[106] * z[107];
z[65] = z[3] * z[65];
z[109] = z[9] * z[106];
z[75] = z[63] + z[75];
z[62] = z[62] + -z[75];
z[110] = z[30] * z[62];
z[52] = z[31] * z[52];
z[52] = z[52] + T(3) * z[110];
z[70] = z[52] + z[65] + -z[70] + (T(3) * z[85]) / T(2) + -z[99] + z[108] + T(2) * z[109];
z[70] = z[24] * z[70];
z[108] = -z[49] + z[68];
z[111] = z[3] * z[108];
z[111] = -z[96] + z[111];
z[112] = z[6] * z[71];
z[113] = z[4] * z[95];
z[114] = z[9] * z[108];
z[112] = -z[72] + z[85] + -z[87] + z[111] + z[112] + z[113] + -z[114];
z[112] = (T(3) * z[112]) / T(2);
z[113] = -(z[37] * z[112]);
z[114] = z[72] + z[97];
z[115] = z[53] + z[114];
z[71] = z[71] * z[102];
z[71] = -z[71] + z[110] + z[115] / T(2);
z[46] = z[46] + z[47];
z[46] = z[46] / T(2);
z[81] = z[46] + -z[63] + -z[81];
z[110] = z[6] * z[81];
z[115] = z[5] + z[9];
z[116] = -(z[62] * z[115]);
z[110] = z[71] + z[110] + z[116];
z[110] = z[39] * z[110];
z[116] = prod_pow(z[42], 2);
z[117] = z[42] * z[43];
z[116] = -z[44] + -z[116] / T(2) + z[117];
z[64] = z[64] * z[116];
z[64] = z[64] + z[110];
z[106] = -(z[5] * z[106]);
z[110] = z[3] * z[49];
z[91] = -z[91] + z[110];
z[110] = z[4] * z[49];
z[104] = -z[91] + -z[104] + z[106] + z[110];
z[104] = T(6) * z[69] + T(2) * z[104];
z[104] = z[27] * z[104];
z[64] = T(3) * z[64] + -z[70] + z[94] + z[104] + z[113];
z[64] = int_to_imaginary<T>(1) * z[64];
z[60] = z[41] * z[60];
z[56] = z[56] + T(3) * z[60] + z[64];
z[56] = z[29] * z[56];
z[47] = -z[47] + -z[57];
z[57] = T(2) * z[22] + T(4) * z[23];
z[60] = (T(5) * z[8]) / T(2);
z[47] = z[47] / T(2) + z[57] + z[60] + -z[80];
z[47] = z[3] * z[47];
z[59] = -z[22] + z[59];
z[59] = -z[23] + z[59] / T(2);
z[64] = z[4] + z[9];
z[64] = z[59] * z[64];
z[104] = z[5] * z[59];
z[106] = z[55] + -z[85];
z[113] = T(3) * z[30];
z[76] = z[76] * z[113];
z[53] = (T(3) * z[53]) / T(4) + z[76];
z[76] = T(3) * z[7];
z[83] = z[76] * z[83];
z[87] = (T(3) * z[87]) / T(4);
z[113] = -z[13] + z[61];
z[113] = T(3) * z[113];
z[116] = -z[10] + T(-13) * z[12] + -z[113];
z[116] = (T(23) * z[8]) / T(2) + (T(-13) * z[11]) / T(2) + T(5) * z[22] + z[116] / T(2);
z[116] = T(5) * z[23] + z[116] / T(2);
z[116] = z[6] * z[116];
z[47] = z[47] + z[53] + z[64] + z[83] + -z[87] + z[104] + (T(-3) * z[106]) / T(4) + z[116];
z[47] = z[28] * z[47];
z[64] = -(z[27] * z[92]);
z[61] = T(2) * z[10] + -z[12] + -z[61] + z[63] + z[90];
z[90] = z[4] + z[6];
z[92] = T(-2) * z[90];
z[92] = z[61] * z[92];
z[82] = -z[63] + z[82];
z[76] = z[76] * z[82];
z[65] = (T(3) * z[55]) / T(2) + -z[65] + z[66] + -z[76] + z[84] + z[92];
z[65] = z[0] * z[65];
z[47] = z[47] + z[64] + z[65] + -z[70];
z[47] = z[28] * z[47];
z[64] = z[27] * z[98];
z[65] = z[54] + -z[85];
z[66] = z[89] + z[103];
z[85] = z[3] + z[107];
z[66] = z[66] * z[85];
z[65] = (T(-3) * z[65]) / T(2) + z[66] + -z[78] + -z[99];
z[78] = (T(3) * z[51]) / T(2) + -z[65];
z[78] = z[24] * z[78];
z[59] = z[6] * z[59];
z[59] = z[59] + -z[104];
z[85] = z[9] * z[49];
z[85] = z[59] + z[85];
z[55] = z[54] + z[55];
z[72] = z[55] + -z[72];
z[72] = -z[72] / T(2) + -z[86] + z[96];
z[68] = z[49] + (T(-3) * z[68]) / T(2);
z[68] = z[3] * z[68];
z[68] = (T(-3) * z[51]) / T(4) + z[68] + (T(3) * z[72]) / T(2) + -z[85] + -z[110];
z[68] = z[25] * z[68];
z[72] = z[4] * z[101];
z[86] = -z[49] + -z[100];
z[86] = z[9] * z[86];
z[72] = z[72] + z[86] + T(-3) * z[93];
z[72] = z[72] / T(2) + -z[77] + -z[84] + -z[91];
z[67] = z[67] * z[72];
z[67] = z[64] + z[67] + z[68] + z[78] + z[94];
z[67] = z[25] * z[67];
z[68] = -z[60] + T(3) * z[73] + -z[80];
z[72] = T(3) * z[23];
z[68] = z[68] / T(2) + -z[72];
z[68] = z[9] * z[68];
z[48] = T(3) * z[48];
z[73] = z[48] + z[49];
z[78] = z[3] / T(4);
z[73] = z[73] * z[78];
z[53] = -z[53] + -z[59] + z[68] + z[73] + -z[110] + (T(-3) * z[114]) / T(4);
z[53] = z[27] * z[53];
z[53] = z[53] + z[70];
z[53] = z[27] * z[53];
z[59] = (T(5) * z[11]) / T(2);
z[68] = z[22] + -z[79];
z[68] = z[59] + T(3) * z[68] + z[74];
z[68] = z[68] / T(2) + z[72];
z[68] = z[4] * z[68];
z[54] = z[54] + -z[97];
z[48] = -z[48] + z[49];
z[48] = z[48] * z[78];
z[48] = z[48] + (T(3) * z[54]) / T(4) + z[68] + z[83] + -z[85] + z[87];
z[48] = z[0] * z[48];
z[49] = z[24] * z[65];
z[48] = z[48] + z[49] + -z[64];
z[48] = z[0] * z[48];
z[49] = -(z[36] * z[112]);
z[64] = T(-7) * z[10] + T(5) * z[12] + z[113];
z[59] = (T(-7) * z[8]) / T(2) + z[59] + -z[63] + z[64] / T(2);
z[59] = -(z[59] * z[90]);
z[64] = -z[1] + z[105];
z[64] = T(-5) * z[10] + T(7) * z[12] + T(-3) * z[64];
z[60] = (T(7) * z[11]) / T(2) + -z[60] + z[63] + z[64] / T(2);
z[60] = z[60] * z[115];
z[57] = z[8] + z[11] + z[57] + -z[58];
z[57] = z[3] * z[57];
z[52] = z[52] + z[57] + z[59] + z[60] + z[76] + -z[77] + (T(-3) * z[106]) / T(2);
z[52] = z[35] * z[52];
z[51] = z[51] + z[54];
z[46] = z[46] + -z[75];
z[46] = z[5] * z[46];
z[54] = z[7] + -z[90];
z[54] = z[54] * z[82];
z[57] = -(z[50] * z[102]);
z[46] = z[46] + z[51] / T(2) + z[54] + z[57] + z[88];
z[46] = z[33] * z[46];
z[51] = -prod_pow(z[27], 2);
z[54] = prod_pow(z[28], 2);
z[51] = z[51] + z[54];
z[51] = z[51] * z[69];
z[46] = z[46] + z[51];
z[51] = z[4] * z[108];
z[54] = z[5] * z[50];
z[51] = z[51] + z[54] + -z[55] + -z[111];
z[51] = (T(3) * z[51]) / T(2) + -z[77];
z[51] = z[32] * z[51];
z[54] = z[5] * z[62];
z[54] = z[54] + -z[71];
z[55] = T(3) * z[38];
z[54] = z[54] * z[55];
z[57] = z[61] * z[90];
z[57] = z[57] + -z[66] + z[109];
z[57] = prod_pow(z[24], 2) * z[57];
z[58] = z[32] * z[95];
z[59] = z[55] * z[62];
z[58] = -z[45] + (T(-3) * z[58]) / T(2) + z[59];
z[58] = z[9] * z[58];
z[55] = -(z[55] * z[81]);
z[55] = (T(85) * z[45]) / T(6) + z[55];
z[55] = z[6] * z[55];
z[59] = z[0] / T(2) + -z[24];
z[59] = z[0] * z[59];
z[59] = -z[32] + z[59];
z[50] = z[2] * z[50] * z[59];
z[59] = -z[4] + (T(-17) * z[5]) / T(3);
z[59] = (T(5) * z[7]) / T(2) + z[30] + (T(5) * z[59]) / T(2);
z[59] = z[45] * z[59];
return T(3) * z[46] + z[47] + z[48] + z[49] + (T(3) * z[50]) / T(2) + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[67];
}



template IntegrandConstructorType<double> f_4_180_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_180_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_180_construct (const Kin<qd_real>&);
#endif

}