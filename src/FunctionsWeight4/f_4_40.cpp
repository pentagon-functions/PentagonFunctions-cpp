#include "f_4_40.h"

namespace PentagonFunctions {

template <typename T> T f_4_40_abbreviated (const std::array<T,40>&);

template <typename T> class SpDLog_f_4_40_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_40_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * T(-4) + (bc<T>[1] * T(-2) + T(2)) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]) + (T(4) * kin.v[0] * kin.v[4]) / T(3) + T(-4) * kin.v[2] * kin.v[4] + T(-4) * kin.v[3] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + bc<T>[1] * kin.v[4] * (T(4) + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(-4) + T(-2) * kin.v[1]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + T(2) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]) + (T(4) * kin.v[0] * kin.v[4]) / T(3) + T(-4) * kin.v[2] * kin.v[4] + T(-4) * kin.v[3] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + rlog(kin.v[0]) * ((T(-2) * kin.v[0] * kin.v[4]) / T(3) + kin.v[1] * ((T(2) * kin.v[0]) / T(3) + bc<T>[1] * T(2) + (bc<T>[1] + T(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3]) + bc<T>[1] * (-kin.v[4] + T(-2)) * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4])) + rlog(-kin.v[1]) * (-(kin.v[0] * kin.v[4]) / T(3) + prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (bc<T>[1] + kin.v[0] / T(3) + -kin.v[2] + -kin.v[3] + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1]) + bc<T>[1] * (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(kin.v[2]) * (-(kin.v[0] * kin.v[4]) / T(3) + prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (bc<T>[1] + kin.v[0] / T(3) + -kin.v[2] + -kin.v[3] + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1]) + bc<T>[1] * (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(kin.v[3]) * (-(kin.v[0] * kin.v[4]) / T(3) + prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (bc<T>[1] + kin.v[0] / T(3) + -kin.v[2] + -kin.v[3] + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1]) + bc<T>[1] * (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(-kin.v[4]) * (-(kin.v[0] * kin.v[4]) / T(3) + prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (bc<T>[1] + kin.v[0] / T(3) + -kin.v[2] + -kin.v[3] + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1]) + bc<T>[1] * (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + bc<T>[1] * ((T(4) * kin.v[0] * kin.v[4]) / T(3) + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + T(2) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]) + T(-4) * kin.v[2] * kin.v[4] + T(-4) * kin.v[3] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + bc<T>[1] * ((-kin.v[1] + T(-2)) * kin.v[1] + kin.v[4] * (T(2) + kin.v[4]))) + bc<T>[0] * int_to_imaginary<T>(1) * ((T(2) * kin.v[0] * kin.v[4]) / T(3) + -prod_pow(kin.v[4], 2) + kin.v[1] * ((T(-2) * kin.v[0]) / T(3) + kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3]) + T(-2) * kin.v[2] * kin.v[4] + T(-2) * kin.v[3] * kin.v[4] + bc<T>[1] * ((-kin.v[1] + T(-2)) * kin.v[1] + kin.v[4] * (T(2) + kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[1] * (-kin.v[0] + bc<T>[1] * T(-3) + ((bc<T>[1] * T(-3)) / T(2) + T(3) / T(2)) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3]) + bc<T>[1] * ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + T(-3) * kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[1]) / T(2) + T(-3)) * kin.v[1] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[1] * (-kin.v[0] + bc<T>[1] * T(-3) + ((bc<T>[1] * T(-3)) / T(2) + T(3) / T(2)) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3]) + bc<T>[1] * ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + T(-3) * kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[1]) / T(2) + T(-3)) * kin.v[1] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4]));
c[2] = (T(-8) + bc<T>[1] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + bc<T>[1] * T(4) * kin.v[4];
c[3] = rlog(kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(2) + bc<T>[1] * T(2) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[1] * T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(-kin.v[1]) * (-(bc<T>[1] * kin.v[4]) + (bc<T>[1] + bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(kin.v[2]) * (-(bc<T>[1] * kin.v[4]) + (bc<T>[1] + bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(kin.v[3]) * (-(bc<T>[1] * kin.v[4]) + (bc<T>[1] + bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[4]) * (-(bc<T>[1] * kin.v[4]) + (bc<T>[1] + bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-6) + bc<T>[1] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + bc<T>[1] * T(3) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-6) + bc<T>[1] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + bc<T>[1] * T(3) * kin.v[4] + T(6) * kin.v[4]) + bc<T>[1] * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4] + bc<T>[1] * (T(-2) * kin.v[1] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[19] * (abb[20] * (abb[21] * T(-4) + abb[3] * abb[16] * T(-2) + abb[4] * abb[16] * T(-2) + abb[16] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[16] * T(2))) + abb[3] * abb[16] * (-abb[1] + -abb[7] + -abb[9] + -abb[10] + abb[22] * T(-4) + abb[8] * T(3) + abb[11] * T(3)) + abb[4] * abb[16] * (-abb[1] + -abb[7] + -abb[9] + -abb[10] + abb[22] * T(-4) + abb[8] * T(3) + abb[11] * T(3)) + abb[16] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[22] * bc<T>[0] * int_to_imaginary<T>(4) + abb[16] * (abb[1] + abb[7] + abb[9] + abb[10] + abb[8] * T(-3) + abb[11] * T(-3) + abb[22] * T(4))) + abb[2] * (abb[16] * abb[20] * T(2) + abb[18] * (-abb[1] + -abb[7] + -abb[9] + -abb[10] + abb[22] * T(-4) + abb[20] * T(-2) + abb[8] * T(3) + abb[11] * T(3)) + abb[16] * (abb[1] + abb[7] + abb[9] + abb[10] + abb[8] * T(-3) + abb[11] * T(-3) + abb[22] * T(4))) + abb[21] * (abb[22] * T(-8) + abb[1] * T(-2) + abb[7] * T(-2) + abb[9] * T(-2) + abb[10] * T(-2) + abb[8] * T(6) + abb[11] * T(6)) + abb[18] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(3) + abb[22] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[20] * (bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2) + abb[4] * T(2) + abb[16] * T(2)) + abb[3] * (abb[1] + abb[7] + abb[9] + abb[10] + abb[8] * T(-3) + abb[11] * T(-3) + abb[22] * T(4)) + abb[4] * (abb[1] + abb[7] + abb[9] + abb[10] + abb[8] * T(-3) + abb[11] * T(-3) + abb[22] * T(4)) + abb[16] * (abb[1] + abb[7] + abb[9] + abb[10] + abb[8] * T(-3) + abb[11] * T(-3) + abb[22] * T(4)) + abb[18] * (abb[22] * T(-8) + abb[20] * T(-4) + abb[1] * T(-2) + abb[7] * T(-2) + abb[9] * T(-2) + abb[10] * T(-2) + abb[8] * T(6) + abb[11] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_40_construct (const Kin<T>& kin) {
    return [&kin, 
            dl18 = DLog_W_18<T>(kin),dl27 = DLog_W_27<T>(kin),dl15 = DLog_W_15<T>(kin),dl14 = DLog_W_14<T>(kin),dl12 = DLog_W_12<T>(kin),dl16 = DLog_W_16<T>(kin),dl30 = DLog_W_30<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),spdl12 = SpDLog_f_4_40_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,40> abbr = 
            {dl18(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), f_2_1_1(kin_path), f_2_1_2(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), dl27(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl15(t), rlog(-v_path[4]), dl14(t), rlog(-v_path[1]), dl12(t), rlog(kin.W[0] / kin_path.W[0]), f_2_1_4(kin_path), -rlog(t), dl16(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl30(t) / kin_path.SqrtDelta, dl1(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl20(t), dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl17(t), dl4(t), dl2(t), dl19(t), dl3(t), dl5(t)}
;

        auto result = f_4_40_abbreviated(abbr);
        result = result + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_40_abbreviated(const std::array<T,40>& abb)
{
using TR = typename T::value_type;
T z[123];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[23];
z[14] = abb[27];
z[15] = abb[30];
z[16] = abb[36];
z[17] = abb[39];
z[18] = abb[16];
z[19] = abb[35];
z[20] = abb[38];
z[21] = abb[18];
z[22] = abb[15];
z[23] = abb[34];
z[24] = abb[37];
z[25] = abb[21];
z[26] = abb[24];
z[27] = abb[25];
z[28] = abb[28];
z[29] = abb[29];
z[30] = abb[12];
z[31] = abb[13];
z[32] = abb[14];
z[33] = abb[26];
z[34] = abb[31];
z[35] = abb[20];
z[36] = abb[22];
z[37] = abb[32];
z[38] = abb[17];
z[39] = abb[33];
z[40] = bc<TR>[1];
z[41] = bc<TR>[3];
z[42] = bc<TR>[5];
z[43] = bc<TR>[2];
z[44] = bc<TR>[4];
z[45] = bc<TR>[7];
z[46] = bc<TR>[8];
z[47] = bc<TR>[9];
z[48] = z[1] + -z[12];
z[49] = z[8] + z[11];
z[50] = -z[9] + z[49];
z[51] = -z[10] + z[50];
z[52] = z[48] + z[51];
z[53] = z[13] * z[52];
z[54] = -z[31] + z[32];
z[55] = z[33] * z[54];
z[53] = -z[53] + z[55];
z[55] = z[9] + -z[11];
z[48] = z[48] + z[55];
z[56] = z[8] + -z[10];
z[57] = z[48] + z[56];
z[58] = z[0] * z[57];
z[59] = -(z[30] * z[54]);
z[58] = z[58] + z[59];
z[59] = z[53] + z[58];
z[60] = z[37] * z[54];
z[61] = -(z[24] * z[40]);
z[61] = z[59] + z[60] + z[61];
z[62] = T(3) * z[1];
z[63] = -z[9] + T(-5) * z[12] + z[62];
z[64] = T(3) * z[10];
z[65] = -z[49] + z[63] + z[64];
z[65] = z[65] / T(2);
z[66] = z[35] + z[65];
z[67] = z[40] / T(2);
z[68] = -z[66] + z[67];
z[68] = -z[36] + z[68] / T(2);
z[68] = z[23] * z[68];
z[69] = z[37] + -z[39];
z[70] = z[23] + -z[24];
z[71] = z[20] + -z[70];
z[72] = (T(-11) * z[69]) / T(90) + -z[71];
z[73] = z[16] + -z[17];
z[74] = z[19] / T(2);
z[72] = z[72] / T(2) + z[73] + z[74];
z[75] = int_to_imaginary<T>(1) * z[7];
z[72] = z[72] * z[75];
z[76] = (T(19) * z[8]) / T(3);
z[63] = (T(-13) * z[10]) / T(3) + -z[11] + z[63] + z[76];
z[63] = z[35] + z[63] / T(2) + -z[67];
z[63] = z[36] + z[63] / T(2);
z[63] = z[20] * z[63];
z[77] = z[1] + z[10];
z[76] = T(-11) * z[9] + (T(25) * z[11]) / T(3) + z[12] + z[76] + (T(-11) * z[77]) / T(3);
z[78] = (T(2) * z[35]) / T(3) + (T(4) * z[36]) / T(3) + (T(7) * z[40]) / T(6);
z[76] = z[76] / T(4) + z[78];
z[76] = z[17] * z[76];
z[79] = (T(-13) * z[1]) / T(3) + -z[9] + (T(-19) * z[10]) / T(3) + T(7) * z[12] + (T(11) * z[49]) / T(3);
z[78] = -z[78] + z[79] / T(4);
z[78] = z[16] * z[78];
z[67] = (T(11) * z[56]) / T(3) + z[67];
z[67] = z[67] * z[74];
z[74] = -z[55] + (T(7) * z[56]) / T(3);
z[79] = z[14] / T(2);
z[74] = z[74] * z[79];
z[80] = -z[19] + z[71];
z[80] = T(13) * z[73] + T(7) * z[80];
z[80] = z[43] * z[80];
z[51] = z[38] * z[51];
z[51] = T(3) * z[51];
z[81] = -z[12] + z[77];
z[82] = -z[8] + z[81];
z[83] = z[22] * z[82];
z[83] = T(3) * z[83];
z[61] = -z[51] + z[61] / T(4) + z[63] + z[67] + z[68] + z[72] + z[74] + z[76] + z[78] + z[80] / T(2) + z[83];
z[61] = z[7] * z[61];
z[63] = z[35] + T(2) * z[36];
z[67] = T(2) * z[8] + z[63] + -z[77];
z[68] = T(3) * z[9];
z[72] = T(2) * z[11] + z[67] + -z[68];
z[74] = z[17] * z[72];
z[76] = T(2) * z[10] + z[63];
z[78] = z[1] + z[49];
z[80] = z[76] + -z[78];
z[84] = z[16] * z[80];
z[85] = z[20] * z[56];
z[86] = z[14] * z[56];
z[87] = T(2) * z[56];
z[88] = z[19] * z[87];
z[89] = -z[51] + z[74] + -z[84] + -z[85] + z[86] + z[88];
z[89] = z[4] * z[89];
z[90] = T(3) * z[12];
z[76] = T(2) * z[1] + -z[49] + z[76] + -z[90];
z[91] = z[16] * z[76];
z[92] = z[19] * z[56];
z[93] = -z[83] + z[92];
z[67] = -z[11] + z[67];
z[94] = z[17] * z[67];
z[87] = z[20] * z[87];
z[95] = z[86] + z[87] + -z[91] + -z[93] + z[94];
z[95] = z[3] * z[95];
z[95] = z[89] + z[95];
z[65] = z[63] + z[65];
z[96] = z[16] + z[20];
z[97] = z[65] * z[96];
z[65] = z[23] * z[65];
z[98] = z[9] + z[11];
z[99] = T(-3) * z[8] + z[12] + z[77];
z[100] = z[98] + z[99];
z[100] = -z[63] + z[100] / T(2);
z[100] = z[17] * z[100];
z[82] = -z[55] + z[82];
z[101] = z[79] * z[82];
z[102] = z[15] * z[82];
z[103] = z[60] + -z[102];
z[104] = z[34] * z[54];
z[105] = z[104] / T(2);
z[97] = -z[65] + z[97] + z[100] + z[101] + z[103] / T(2) + -z[105];
z[97] = T(3) * z[97];
z[100] = z[29] * z[97];
z[64] = z[9] + z[12] + -z[64] + z[78];
z[64] = -z[63] + z[64] / T(2);
z[64] = z[16] * z[64];
z[54] = -(z[39] * z[54]);
z[101] = z[53] + z[54];
z[99] = T(-5) * z[9] + T(3) * z[11] + -z[99];
z[99] = z[63] + z[99] / T(2);
z[106] = z[17] + z[19];
z[107] = z[99] * z[106];
z[108] = z[52] * z[79];
z[99] = z[24] * z[99];
z[64] = z[64] + -z[99] + z[101] / T(2) + z[107] + z[108];
z[64] = T(3) * z[64];
z[101] = -(z[27] * z[64]);
z[107] = z[49] + -z[68] + z[77] + -z[90];
z[108] = z[107] / T(2);
z[109] = z[63] + z[108];
z[110] = z[17] * z[109];
z[111] = z[16] * z[109];
z[87] = z[87] + z[88] + z[110] + -z[111];
z[88] = z[102] + z[104];
z[112] = z[53] + z[88];
z[86] = z[86] + z[87] + (T(3) * z[112]) / T(2);
z[86] = z[2] * z[86];
z[112] = T(3) * z[44];
z[113] = prod_pow(z[40], 2);
z[114] = z[40] * z[43];
z[114] = -z[112] + T(-2) * z[113] + T(3) * z[114];
z[114] = z[73] * z[114];
z[115] = -(z[69] * z[113]);
z[86] = z[86] + T(2) * z[95] + z[100] + z[101] + z[114] + (T(3) * z[115]) / T(20);
z[86] = int_to_imaginary<T>(1) * z[86];
z[95] = z[41] * z[69];
z[61] = z[61] + z[86] + -z[95];
z[61] = z[7] * z[61];
z[86] = z[19] * z[109];
z[100] = z[86] + z[110];
z[101] = z[14] * z[109];
z[110] = z[60] / T(2);
z[114] = -z[65] + z[110];
z[115] = T(2) * z[76];
z[115] = z[20] * z[115];
z[114] = T(-2) * z[91] + z[100] + -z[101] + T(-3) * z[114] + -z[115];
z[115] = -z[3] + z[75];
z[114] = z[114] * z[115];
z[66] = z[36] + z[66] / T(2);
z[66] = z[23] * z[66];
z[115] = -z[53] / T(2);
z[110] = -z[54] + -z[110] + z[115];
z[110] = z[66] + z[99] + z[110] / T(2);
z[108] = z[35] + z[108];
z[108] = z[36] + z[108] / T(2);
z[116] = z[20] * z[108];
z[117] = T(5) * z[8];
z[68] = T(5) * z[11] + -z[68] + z[117];
z[118] = T(7) * z[10];
z[119] = -z[1] + z[68] + z[90] + -z[118];
z[119] = -z[63] + z[119] / T(2);
z[119] = z[16] * z[119];
z[118] = T(9) * z[12] + -z[118];
z[120] = T(9) * z[9];
z[78] = -z[78] + z[118] + z[120];
z[121] = T(2) * z[35] + T(4) * z[36];
z[78] = z[78] / T(4) + -z[121];
z[78] = z[14] * z[78];
z[122] = z[19] * z[72];
z[51] = -z[51] + -z[74] + z[78] + T(3) * z[110] + z[116] + z[119] + -z[122];
z[51] = z[21] * z[51];
z[74] = z[20] * z[109];
z[78] = -(z[14] * z[80]);
z[53] = (T(3) * z[53]) / T(2) + z[74] + z[78] + T(-2) * z[84] + z[100];
z[53] = z[2] * z[53];
z[74] = z[74] + z[111];
z[72] = T(2) * z[72];
z[72] = z[72] * z[106];
z[78] = z[54] / T(2) + -z[99];
z[72] = z[72] + -z[74] + T(3) * z[78] + z[101];
z[78] = z[4] * z[72];
z[51] = z[51] + z[53] + z[78] + z[114];
z[51] = z[21] * z[51];
z[53] = z[14] * z[67];
z[53] = z[53] + T(2) * z[94];
z[67] = -z[53] + z[74] + z[86] + (T(-3) * z[88]) / T(2);
z[74] = -z[2] + z[3];
z[67] = z[67] * z[74];
z[74] = -z[4] + z[21] + z[75];
z[72] = z[72] * z[74];
z[74] = -(z[20] * z[76]);
z[53] = z[53] + z[74] + z[83] + -z[91] + -z[122];
z[53] = z[18] * z[53];
z[53] = z[53] + z[67] + z[72];
z[53] = z[18] * z[53];
z[50] = -z[50] + z[81];
z[67] = z[14] + -z[20];
z[67] = z[50] * z[67];
z[72] = z[17] * z[82];
z[74] = z[19] * z[57];
z[67] = -z[58] + z[67] + z[72] + z[74] + -z[88];
z[67] = z[5] * z[67];
z[59] = -(z[6] * z[59]);
z[59] = z[59] + z[67];
z[64] = -(z[26] * z[64]);
z[62] = z[62] + -z[90];
z[55] = T(3) * z[55] + -z[56];
z[67] = z[55] + z[62];
z[67] = z[67] * z[79];
z[67] = (T(-3) * z[58]) / T(2) + z[67] + z[87];
z[72] = -(z[2] * z[67]);
z[72] = z[72] + -z[89];
z[72] = z[4] * z[72];
z[74] = -(z[28] * z[97]);
z[75] = -(z[73] * z[108]);
z[76] = z[103] + -z[104];
z[66] = z[66] + -z[76] / T(4);
z[55] = z[55] + -z[62];
z[55] = z[14] * z[55];
z[62] = T(-9) * z[1] + -z[10] + T(15) * z[12] + T(3) * z[98] + -z[117];
z[62] = T(-3) * z[35] + z[62] / T(2);
z[62] = T(-3) * z[36] + z[62] / T(2);
z[62] = z[20] * z[62];
z[55] = z[55] / T(4) + z[62] + T(3) * z[66] + z[75] + z[93];
z[55] = z[3] * z[55];
z[62] = -z[2] + z[4];
z[62] = z[62] * z[67];
z[55] = z[55] + z[62];
z[55] = z[3] * z[55];
z[49] = T(-7) * z[49] + T(5) * z[77] + -z[90] + z[120];
z[49] = z[49] / T(2) + -z[63];
z[49] = z[49] * z[106];
z[54] = -z[54] + -z[60];
z[54] = z[54] / T(2) + z[65] + z[99];
z[60] = -z[107] + -z[121];
z[60] = z[14] * z[60];
z[62] = T(-7) * z[1] + z[68] + z[118];
z[62] = z[62] / T(2) + -z[63];
z[62] = z[62] * z[96];
z[49] = z[49] + T(3) * z[54] + z[60] + z[62];
z[49] = z[25] * z[49];
z[54] = -z[58] + -z[102] / T(2) + -z[105] + z[115];
z[48] = (T(3) * z[48]) / T(2) + -z[56];
z[48] = z[14] * z[48];
z[48] = z[48] + (T(3) * z[54]) / T(2) + z[75] + z[85] + z[92];
z[48] = prod_pow(z[2], 2) * z[48];
z[54] = z[112] + z[113] / T(2);
z[54] = z[40] * z[54];
z[56] = T(6) * z[45] + (T(3) * z[46]) / T(2);
z[54] = z[54] + z[56];
z[58] = (T(3) * z[6]) / T(2);
z[57] = z[57] * z[58];
z[57] = -z[54] + z[57];
z[57] = z[20] * z[57];
z[60] = z[112] + (T(2) * z[113]) / T(3);
z[60] = z[40] * z[60];
z[56] = z[56] + z[60];
z[52] = -(z[52] * z[58]);
z[52] = z[52] + -z[56];
z[52] = z[16] * z[52];
z[50] = z[50] * z[58];
z[58] = (T(-31) * z[47]) / T(4) + -z[50] + z[54];
z[58] = z[19] * z[58];
z[54] = z[54] * z[70];
z[56] = z[17] * z[56];
z[60] = T(31) * z[71] + (T(-149) * z[73]) / T(3);
z[60] = z[47] * z[60];
z[50] = z[14] * z[50];
z[62] = z[16] + z[71] + -z[106];
z[62] = prod_pow(z[43], 3) * z[62];
z[63] = z[42] * z[69];
z[65] = z[40] * z[95];
z[63] = (T(12) * z[63]) / T(5) + z[65];
z[63] = int_to_imaginary<T>(1) * z[63];
return z[48] + z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + (T(3) * z[59]) / T(2) + z[60] / T(4) + z[61] + T(2) * z[62] + T(3) * z[63] + z[64] + z[72] + z[74];
}



template IntegrandConstructorType<double> f_4_40_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_40_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_40_construct (const Kin<qd_real>&);
#endif

}