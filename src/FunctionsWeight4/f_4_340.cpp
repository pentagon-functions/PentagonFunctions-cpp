#include "f_4_340.h"

namespace PentagonFunctions {

template <typename T> T f_4_340_abbreviated (const std::array<T,45>&);

template <typename T> class SpDLog_f_4_340_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_340_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4]);
c[1] = kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4]);
c[2] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);
c[3] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[5] * c[0] + abb[6] * c[1] + abb[3] * c[2] + abb[4] * c[3]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(6) + prod_pow(abb[2], 2) * (abb[5] * T(-6) + abb[6] * T(-6) + abb[4] * T(6)) + prod_pow(abb[1], 2) * (abb[3] * T(-6) + abb[4] * T(-6) + abb[5] * T(6) + abb[6] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_340_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_340_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-12) + T(12) * kin.v[0] + T(6) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[0] * (T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[1] = kin.v[1] * (T(-12) + T(12) * kin.v[0] + T(6) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[0] * (T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[2] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[1] * (T(12) + T(-12) * kin.v[0] + T(-6) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[0] * (T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[3] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[1] * (T(12) + T(-12) * kin.v[0] + T(-6) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[0] * (T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[9] * c[0] + abb[5] * c[1] + abb[3] * c[2] + abb[6] * c[3]);
        }

        return abb[7] * (abb[3] * prod_pow(abb[8], 2) * T(6) + prod_pow(abb[8], 2) * (abb[5] * T(-6) + abb[9] * T(-6) + abb[6] * T(6)) + prod_pow(abb[1], 2) * (abb[3] * T(-6) + abb[6] * T(-6) + abb[5] * T(6) + abb[9] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_340_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl22 = DLog_W_22<T>(kin),dl13 = DLog_W_13<T>(kin),dl6 = DLog_W_6<T>(kin),dl8 = DLog_W_8<T>(kin),dl24 = DLog_W_24<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl30 = DLog_W_30<T>(kin),dl16 = DLog_W_16<T>(kin),dl1 = DLog_W_1<T>(kin),dl31 = DLog_W_31<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),spdl23 = SpDLog_f_4_340_W_23<T>(kin),spdl22 = SpDLog_f_4_340_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,45> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl22(t), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_3(kin) - f_1_3_3(kin_path), dl13(t), rlog(v_path[0]), rlog(v_path[2]), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl8(t), dl24(t), dl20(t), f_2_1_8(kin_path), f_2_1_14(kin_path), f_2_1_1(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl3(t), f_2_1_9(kin_path), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl17(t), dl30(t) / kin_path.SqrtDelta, rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[30] / kin_path.W[30]), dl16(t), dl1(t), dl31(t), f_2_2_8(kin_path), dl4(t), dl19(t), dl5(t), dl26(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta}
;

        auto result = f_4_340_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_340_abbreviated(const std::array<T,45>& abb)
{
using TR = typename T::value_type;
T z[102];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[17];
z[3] = abb[28];
z[4] = abb[34];
z[5] = abb[38];
z[6] = abb[4];
z[7] = abb[40];
z[8] = abb[5];
z[9] = abb[6];
z[10] = abb[39];
z[11] = abb[9];
z[12] = abb[30];
z[13] = abb[41];
z[14] = abb[43];
z[15] = abb[44];
z[16] = abb[31];
z[17] = abb[32];
z[18] = abb[33];
z[19] = abb[2];
z[20] = abb[8];
z[21] = abb[14];
z[22] = abb[25];
z[23] = bc<TR>[0];
z[24] = abb[12];
z[25] = abb[23];
z[26] = abb[29];
z[27] = abb[42];
z[28] = abb[16];
z[29] = abb[11];
z[30] = abb[35];
z[31] = abb[13];
z[32] = abb[18];
z[33] = abb[19];
z[34] = abb[24];
z[35] = abb[26];
z[36] = abb[27];
z[37] = abb[10];
z[38] = abb[20];
z[39] = abb[15];
z[40] = abb[21];
z[41] = abb[22];
z[42] = abb[36];
z[43] = abb[37];
z[44] = bc<TR>[1];
z[45] = bc<TR>[3];
z[46] = bc<TR>[5];
z[47] = bc<TR>[2];
z[48] = bc<TR>[4];
z[49] = bc<TR>[7];
z[50] = bc<TR>[8];
z[51] = bc<TR>[9];
z[52] = z[2] + -z[3];
z[52] = z[9] * z[52];
z[53] = z[3] + T(17) * z[25] + T(-13) * z[30];
z[53] = z[6] * z[53];
z[52] = z[52] + z[53];
z[53] = z[12] + z[16] + z[17] + T(-2) * z[18];
z[54] = z[15] * z[53];
z[55] = z[27] * z[53];
z[56] = z[54] + -z[55];
z[57] = z[26] * z[53];
z[58] = z[56] + -z[57];
z[59] = z[6] + z[11];
z[60] = -z[9] + z[59];
z[61] = -z[1] + z[60];
z[61] = z[28] * z[61];
z[62] = z[8] + z[11];
z[63] = z[9] + z[62];
z[63] = z[39] * z[63];
z[64] = z[61] + z[63];
z[65] = T(2) * z[11];
z[66] = z[2] + z[3];
z[67] = T(-3) * z[30] + -z[66];
z[67] = z[65] * z[67];
z[68] = z[1] + z[9];
z[69] = T(2) * z[6] + z[65] + (T(13) * z[68]) / T(2);
z[69] = z[22] * z[69];
z[70] = T(2) * z[5];
z[71] = -z[1] + -z[6] + z[8];
z[71] = z[70] * z[71];
z[72] = T(2) * z[10];
z[73] = -(z[68] * z[72]);
z[74] = z[13] * z[53];
z[75] = (T(5) * z[2]) / T(2) + T(4) * z[3] + (T(3) * z[30]) / T(2);
z[75] = z[1] * z[75];
z[65] = (T(-9) * z[9]) / T(2) + z[65];
z[65] = z[25] * z[65];
z[76] = -z[1] / T(2) + (T(-13) * z[6]) / T(2) + T(2) * z[9] + T(-6) * z[11];
z[76] = z[4] * z[76];
z[77] = T(-5) * z[3] + z[4] + z[30];
z[77] = T(-2) * z[2] + (T(-13) * z[25]) / T(2) + z[77] / T(2);
z[77] = z[8] * z[77];
z[78] = z[13] + -z[15];
z[79] = T(15) * z[14] + T(8) * z[27] + T(-7) * z[78];
z[79] = z[47] * z[79];
z[80] = T(-13) * z[27] + z[78];
z[80] = T(-7) * z[14] + z[80] / T(2);
z[80] = z[44] * z[80];
z[52] = z[52] / T(2) + T(2) * z[58] + T(6) * z[64] + z[65] + z[67] + z[69] + z[71] + z[73] + T(-4) * z[74] + z[75] + z[76] + z[77] + z[79] + z[80];
z[52] = z[23] * z[52];
z[58] = z[5] + z[7] + z[10];
z[64] = T(2) * z[42] + -z[58];
z[65] = z[45] * z[64];
z[52] = z[52] + T(16) * z[65];
z[52] = z[23] * z[52];
z[67] = z[22] * z[60];
z[69] = z[7] * z[60];
z[67] = z[67] + -z[69];
z[60] = z[4] * z[60];
z[60] = z[57] + z[60];
z[71] = z[60] + z[67];
z[73] = -z[1] + z[62];
z[73] = z[5] * z[73];
z[75] = z[8] * z[66];
z[76] = z[73] + -z[75];
z[77] = z[11] * z[66];
z[79] = z[1] * z[66];
z[77] = z[77] + -z[79];
z[80] = -z[56] + -z[71] + z[74] + -z[76] + z[77];
z[80] = z[32] * z[80];
z[81] = z[14] * z[53];
z[54] = z[54] + z[81];
z[82] = z[54] + -z[74];
z[77] = z[77] + -z[82];
z[76] = z[76] + -z[77];
z[83] = z[4] * z[9];
z[84] = z[9] * z[25];
z[57] = -z[57] + z[83] + z[84];
z[83] = -z[57] + -z[76];
z[83] = z[33] * z[83];
z[85] = z[2] + z[30];
z[86] = z[8] * z[85];
z[87] = z[11] * z[85];
z[88] = z[7] * z[62];
z[86] = z[86] + z[87] + -z[88];
z[89] = z[22] * z[59];
z[56] = -z[56] + -z[89];
z[90] = z[6] * z[25];
z[91] = z[11] * z[25];
z[90] = z[90] + z[91];
z[92] = z[5] * z[59];
z[93] = z[56] + z[86] + -z[90] + z[92];
z[93] = z[38] * z[93];
z[55] = z[55] + z[81];
z[81] = z[55] + -z[89];
z[89] = z[81] + -z[90];
z[94] = z[3] + z[30];
z[94] = z[1] * z[94];
z[94] = -z[74] + z[94];
z[95] = z[89] + -z[94];
z[95] = z[34] * z[95];
z[53] = z[43] * z[53];
z[96] = z[1] * z[34];
z[97] = z[9] * z[33];
z[96] = -z[53] + z[96] + z[97];
z[96] = z[10] * z[96];
z[71] = -z[55] + z[71];
z[97] = z[1] * z[10];
z[98] = z[94] + -z[97];
z[99] = -z[71] + z[98];
z[100] = z[35] * z[99];
z[101] = z[34] * z[59];
z[101] = -z[53] + z[101];
z[101] = z[5] * z[101];
z[80] = -z[80] + -z[83] + -z[93] + -z[95] + -z[96] + z[100] + -z[101];
z[83] = -z[6] + z[9];
z[83] = z[3] * z[83];
z[93] = z[2] * z[11];
z[75] = z[61] + -z[69] + z[75] + -z[79] + -z[82] + z[83] + z[93] + z[97];
z[75] = z[19] * z[75];
z[71] = z[21] * z[71];
z[82] = z[20] * z[76];
z[71] = z[71] + z[82];
z[76] = z[0] * z[76];
z[82] = z[29] * z[98];
z[76] = -z[71] + z[76] + z[82];
z[75] = z[75] + T(2) * z[76];
z[75] = z[19] * z[75];
z[76] = -z[3] + z[30];
z[62] = -(z[62] * z[76]);
z[76] = -(z[2] * z[9]);
z[62] = -z[57] + z[62] + z[63] + z[74] + z[76] + -z[79];
z[62] = z[20] * z[62];
z[54] = -z[54] + z[86];
z[74] = z[24] + -z[29];
z[74] = z[54] * z[74];
z[62] = z[62] + T(2) * z[74];
z[62] = z[20] * z[62];
z[74] = z[9] * z[10];
z[76] = z[1] + -z[11];
z[76] = z[4] * z[76];
z[66] = -z[4] + z[66];
z[66] = z[8] * z[66];
z[66] = z[66] + z[69] + T(-2) * z[73] + z[74] + z[76] + z[77];
z[66] = z[0] * z[66];
z[69] = z[57] + -z[74];
z[73] = z[24] * z[69];
z[71] = z[71] + z[73];
z[66] = z[66] + T(2) * z[71];
z[66] = z[0] * z[66];
z[71] = z[11] * z[30];
z[73] = z[8] * z[37];
z[76] = z[1] + z[59];
z[76] = z[31] * z[76];
z[71] = -z[71] + -z[73] + z[76] + z[88] + -z[97];
z[73] = z[81] + -z[91];
z[77] = -z[25] + z[30] + -z[37];
z[77] = z[6] * z[77];
z[77] = -z[71] + z[73] + z[77] + T(2) * z[92];
z[77] = prod_pow(z[29], 2) * z[77];
z[79] = -prod_pow(z[44], 2);
z[79] = T(-4) * z[48] + z[79];
z[82] = z[14] + z[27];
z[79] = z[44] * z[79] * z[82];
z[62] = z[62] + z[66] + z[75] + z[77] + z[79];
z[66] = z[22] * z[68];
z[60] = -z[60] + z[61] + z[66] + -z[76] + z[90] + z[94];
z[61] = T(6) * z[21];
z[66] = z[60] * z[61];
z[68] = z[25] + -z[37];
z[75] = -z[30] + -z[68];
z[75] = z[6] * z[75];
z[71] = z[71] + z[73] + z[75];
z[71] = z[29] * z[71];
z[73] = z[36] * z[99];
z[71] = -z[71] + z[73];
z[73] = z[6] * z[68];
z[63] = z[63] + z[73];
z[73] = -z[68] + -z[85];
z[73] = z[8] * z[73];
z[56] = -z[56] + -z[57] + z[63] + z[73] + -z[87];
z[57] = T(6) * z[24];
z[56] = z[56] * z[57];
z[73] = -z[14] + z[27] + T(2) * z[78];
z[70] = T(-2) * z[7] + T(4) * z[42] + -z[70] + -z[72] + T(3) * z[73];
z[70] = z[44] * z[70];
z[72] = z[14] + -z[78];
z[72] = z[47] * z[72];
z[70] = z[70] + T(6) * z[72];
z[70] = z[44] * z[70];
z[72] = z[27] + z[78];
z[73] = z[48] * z[72];
z[56] = z[56] + z[66] + z[70] + T(-6) * z[71] + T(12) * z[73];
z[59] = -(z[4] * z[59]);
z[55] = z[55] + z[59] + -z[67] + z[74] + -z[84];
z[55] = z[0] * z[55];
z[54] = z[54] + z[69];
z[54] = T(12) * z[54];
z[59] = z[41] * z[54];
z[58] = T(8) * z[42] + T(-4) * z[58] + T(-3) * z[72];
z[58] = prod_pow(z[23], 2) * z[58];
z[55] = T(12) * z[55] + T(2) * z[56] + z[58] + z[59];
z[55] = z[23] * z[55];
z[56] = -(z[46] * z[64]);
z[58] = -(z[44] * z[65]);
z[56] = T(4) * z[56] + z[58];
z[55] = z[55] + T(48) * z[56];
z[55] = int_to_imaginary<T>(1) * z[55];
z[56] = -(z[21] * z[60]);
z[58] = z[89] + z[92];
z[59] = z[29] * z[58];
z[56] = z[56] + T(-2) * z[59];
z[56] = z[56] * z[61];
z[60] = z[8] * z[68];
z[60] = z[60] + -z[63] + z[74] + z[81] + z[88];
z[60] = z[24] * z[60];
z[58] = z[21] * z[58];
z[58] = z[58] + -z[59];
z[58] = T(2) * z[58] + z[60];
z[57] = z[57] * z[58];
z[54] = -(z[40] * z[54]);
z[58] = T(-12) * z[7] + T(24) * z[42];
z[53] = z[53] * z[58];
z[58] = prod_pow(z[47], 3);
z[58] = T(-12) * z[49] + T(-3) * z[50] + T(4) * z[58];
z[59] = T(3) * z[14] + T(2) * z[27] + -z[78];
z[58] = z[58] * z[59];
z[59] = (T(13) * z[14]) / T(2) + T(4) * z[27] + (T(-5) * z[78]) / T(2);
z[59] = z[51] * z[59];
return z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + T(7) * z[59] + T(6) * z[62] + T(-12) * z[80];
}



template IntegrandConstructorType<double> f_4_340_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_340_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_340_construct (const Kin<qd_real>&);
#endif

}