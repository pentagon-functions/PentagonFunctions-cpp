#include "f_4_280.h"

namespace PentagonFunctions {

template <typename T> T f_4_280_abbreviated (const std::array<T,20>&);

template <typename T> class SpDLog_f_4_280_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_280_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[3] * ((T(-3) * kin.v[3]) / T(4) + T(3) + T(-3) * kin.v[4]) + kin.v[2] * ((T(9) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * ((T(9) * kin.v[0]) / T(4) + (T(3) * kin.v[1]) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]);
c[1] = kin.v[2] * (T(3) + T(-3) * kin.v[2] + T(-9) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * (T(3) + T(-6) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * (T(-3) + T(-6) * kin.v[1] + T(9) * kin.v[2] + T(12) * kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * (T(-3) + T(-3) * kin.v[0] + T(-9) * kin.v[1] + T(6) * kin.v[2] + T(9) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[3] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[2] + T(16) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[1] * (-kin.v[1] / T(4) + (T(23) * kin.v[2]) / T(2) + kin.v[3] / T(2) + T(11) + T(-11) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(4) + T(-11) + T(11) * kin.v[4]) + kin.v[2] * ((T(-45) * kin.v[2]) / T(4) + (T(-23) * kin.v[3]) / T(2) + T(-11) + T(11) * kin.v[4]) + kin.v[0] * ((T(-23) * kin.v[1]) / T(2) + (T(45) * kin.v[2]) / T(2) + (T(23) * kin.v[3]) / T(2) + T(11) + (T(-45) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-11) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[2] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(8) + T(4) * kin.v[1] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[3] * (T(-8) + T(4) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[1]) * (kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + T(14) + T(13) * kin.v[2] + T(-14) * kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-14) + T(14) * kin.v[4]) + kin.v[2] * ((T(-27) * kin.v[2]) / T(2) + T(-14) + T(-13) * kin.v[3] + T(14) * kin.v[4]) + kin.v[0] * (T(14) + (T(-27) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-13) * kin.v[1] + T(27) * kin.v[2] + T(13) * kin.v[3] + T(-14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[2] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(8) + T(4) * kin.v[1] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[3] * (T(-8) + T(4) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[3] * (-kin.v[3] / T(2) + T(14) + T(-14) * kin.v[4]) + kin.v[2] * ((T(27) * kin.v[2]) / T(2) + T(14) + T(13) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-14) + T(-13) * kin.v[2] + kin.v[3] + T(14) * kin.v[4]) + kin.v[0] * (T(-14) + (T(27) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(13) * kin.v[1] + T(-27) * kin.v[2] + T(-13) * kin.v[3] + T(14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[2] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-4) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-4) * kin.v[1] + T(8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[3] * (-kin.v[3] + T(28) + T(-28) * kin.v[4]) + kin.v[2] * (T(28) + T(27) * kin.v[2] + T(26) * kin.v[3] + T(-28) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-28) + T(-26) * kin.v[2] + T(2) * kin.v[3] + T(28) * kin.v[4]) + kin.v[0] * (T(-28) + (bc<T>[0] * int_to_imaginary<T>(8) + T(27)) * kin.v[0] + T(26) * kin.v[1] + T(-54) * kin.v[2] + T(-26) * kin.v[3] + T(28) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) + T(-16) * kin.v[2] + T(16) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(16) + T(8) * kin.v[2] + T(-16) * kin.v[4]) + kin.v[3] * (T(16) + T(-8) * kin.v[3] + T(-16) * kin.v[4]) + kin.v[1] * (T(-16) + T(-8) * kin.v[1] + T(16) * kin.v[3] + T(16) * kin.v[4])));
c[2] = T(3) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3];
c[3] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(9)) * kin.v[0] + T(9) * kin.v[1] + T(-9) * kin.v[2] + T(-9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] + T(-8) * kin.v[2] + T(-8) * kin.v[3])) + rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] + T(-8) * kin.v[2] + T(-8) * kin.v[3])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[2] + T(6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-12) + bc<T>[0] * int_to_imaginary<T>(-16)) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[2] + T(12) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) * kin.v[1] + T(16) * kin.v[2] + T(16) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (abb[10] * (abb[2] * abb[7] * T(-8) + abb[3] * T(-6) + abb[2] * (abb[2] * T(-4) + bc<T>[0] * int_to_imaginary<T>(8))) + abb[2] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-16) + abb[2] * (-abb[9] + abb[4] * T(-3) + abb[5] * T(4) + abb[8] * T(8))) + abb[1] * (abb[9] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(16) + abb[1] * (abb[9] + abb[8] * T(-8) + abb[5] * T(-4) + abb[4] * T(3) + abb[10] * T(4)) + abb[10] * (bc<T>[0] * int_to_imaginary<T>(-8) + abb[7] * T(8)) + abb[7] * (abb[8] * T(-16) + abb[5] * T(-8) + abb[9] * T(8))) + abb[3] * (abb[9] * T(-9) + abb[4] * T(3) + abb[5] * T(6) + abb[8] * T(12)) + abb[2] * abb[7] * (abb[9] * T(-8) + abb[5] * T(8) + abb[8] * T(16)) + abb[6] * (abb[2] * abb[10] * T(8) + abb[2] * (abb[8] * T(-16) + abb[5] * T(-8) + abb[9] * T(8)) + abb[1] * (abb[9] * T(-8) + abb[10] * T(-8) + abb[5] * T(8) + abb[8] * T(16))));
    }
};
template <typename T> class SpDLog_f_4_280_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_280_W_7 (const Kin<T>& kin) {
        c[0] = ((T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(3) + T(3) * kin.v[1]) * kin.v[3] + T(3) * kin.v[1] * kin.v[4] + ((T(-3) * kin.v[4]) / T(4) + T(3)) * kin.v[4];
c[1] = kin.v[3] * (T(3) + T(3) * kin.v[1] + (T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[3] + T(-12) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-16) * kin.v[4])) + T(3) * kin.v[1] * kin.v[4] + kin.v[4] * (T(3) + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-8) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((-kin.v[4] + T(28)) * kin.v[4] + T(28) * kin.v[1] * kin.v[4] + kin.v[3] * (T(28) + T(28) * kin.v[1] + (T(-1) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) + T(16) * kin.v[1] + T(-16) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) * kin.v[1] * kin.v[4] + kin.v[4] * (T(16) + T(-8) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((-kin.v[4] / T(2) + T(14)) * kin.v[4] + T(14) * kin.v[1] * kin.v[4] + kin.v[3] * (-kin.v[4] + T(14) + T(14) * kin.v[1] + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-4) * kin.v[4]))) + rlog(-kin.v[1]) * ((-kin.v[4] / T(4) + T(-11)) * kin.v[4] + T(-11) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-8) + T(4) * kin.v[4])) + kin.v[3] * (-kin.v[4] / T(2) + T(-11) + T(-11) * kin.v[1] + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((kin.v[4] / T(2) + T(-14)) * kin.v[4] + T(-14) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-8) + T(4) * kin.v[4])) + kin.v[3] * (T(-14) + T(-14) * kin.v[1] + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[3] + kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[4])));
c[2] = T(-3) * kin.v[3] + T(-3) * kin.v[4];
c[3] = rlog(-kin.v[1]) * ((T(-9) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(16) + T(12)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(16) * kin.v[4] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[11] * (abb[12] * (abb[8] * T(-12) + abb[5] * T(-6) + abb[13] * T(-3) + abb[9] * T(6)) + abb[1] * abb[7] * (abb[8] * T(-16) + abb[5] * T(-8) + abb[9] * T(8)) + abb[10] * (abb[1] * ((abb[1] * T(11)) / T(2) + abb[2] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8)) + abb[1] * abb[7] * T(8) + abb[12] * T(9)) + abb[1] * (abb[9] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(16) + abb[1] * ((abb[13] * T(3)) / T(2) + abb[8] * T(-14) + abb[5] * T(-7) + abb[9] * T(7)) + abb[2] * (abb[9] * T(-8) + abb[5] * T(8) + abb[8] * T(16))) + abb[6] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-16) + abb[6] * (abb[5] + (abb[13] * T(-9)) / T(2) + (abb[10] * T(7)) / T(2) + -abb[9] + abb[8] * T(2)) + abb[10] * (abb[1] * T(-9) + abb[7] * T(-8) + bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * T(8)) + abb[2] * (abb[8] * T(-16) + abb[5] * T(-8) + abb[9] * T(8)) + abb[1] * (abb[9] * T(-6) + abb[13] * T(3) + abb[5] * T(6) + abb[8] * T(12)) + abb[7] * (abb[9] * T(-8) + abb[5] * T(8) + abb[8] * T(16))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_280_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl7 = DLog_W_7<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),spdl21 = SpDLog_f_4_280_W_21<T>(kin),spdl7 = SpDLog_f_4_280_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,20> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), -rlog(t), rlog(kin.W[17] / kin_path.W[17]), rlog(-v_path[1]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[1] / kin_path.W[1]), dl7(t), f_2_1_11(kin_path), -rlog(t), dl18(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl19(t), dl20(t), dl2(t)}
;

        auto result = f_4_280_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_280_abbreviated(const std::array<T,20>& abb)
{
using TR = typename T::value_type;
T z[56];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[14];
z[3] = abb[17];
z[4] = abb[19];
z[5] = abb[5];
z[6] = abb[8];
z[7] = abb[9];
z[8] = abb[10];
z[9] = abb[18];
z[10] = abb[13];
z[11] = abb[2];
z[12] = abb[6];
z[13] = abb[7];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[12];
z[17] = abb[15];
z[18] = abb[16];
z[19] = bc<TR>[9];
z[20] = T(5) * z[0];
z[21] = T(4) * z[12];
z[22] = T(2) * z[11];
z[23] = -z[13] + z[20] + -z[21] + -z[22];
z[23] = z[22] * z[23];
z[24] = T(10) * z[15];
z[25] = T(6) * z[17];
z[26] = z[24] + z[25];
z[27] = T(4) * z[16];
z[28] = prod_pow(z[13], 2);
z[29] = z[27] + z[28];
z[30] = T(3) * z[12];
z[31] = T(4) * z[13];
z[32] = z[30] + z[31];
z[33] = T(2) * z[12];
z[34] = z[32] * z[33];
z[35] = T(3) * z[0];
z[36] = -z[21] + -z[35];
z[36] = z[0] * z[36];
z[23] = z[23] + -z[26] + z[29] + z[34] + z[36];
z[23] = z[4] * z[23];
z[34] = z[0] + -z[12];
z[36] = T(8) * z[11];
z[34] = z[34] * z[36];
z[36] = T(2) * z[13];
z[37] = -z[12] + z[36];
z[38] = z[21] * z[37];
z[39] = T(2) * z[17] + z[28];
z[40] = T(3) * z[39];
z[41] = -z[0] + z[36];
z[41] = z[0] * z[41];
z[24] = T(-16) * z[16] + z[24] + -z[34] + z[38] + -z[40] + -z[41];
z[34] = -z[2] + z[9];
z[24] = -(z[24] * z[34]);
z[38] = -z[0] + z[22];
z[42] = z[13] + z[38];
z[42] = z[22] * z[42];
z[43] = prod_pow(z[12], 2);
z[44] = T(8) * z[13];
z[45] = (T(-11) * z[0]) / T(2) + T(7) * z[12] + -z[44];
z[45] = z[0] * z[45];
z[26] = T(-7) * z[16] + z[26] + -z[28] + z[42] + -z[43] / T(2) + z[45];
z[26] = z[3] * z[26];
z[23] = z[23] + z[24] + z[26];
z[23] = z[10] * z[23];
z[24] = z[33] * z[37];
z[26] = z[25] + z[28];
z[24] = T(-8) * z[16] + z[24] + -z[26];
z[42] = T(3) * z[13] + -z[21];
z[20] = -z[20] + T(-2) * z[42];
z[20] = z[0] * z[20];
z[45] = T(2) * z[0];
z[46] = z[13] + -z[30] + z[45];
z[46] = z[11] + T(2) * z[46];
z[46] = z[22] * z[46];
z[47] = T(2) * z[15];
z[20] = z[20] + -z[24] + z[46] + -z[47];
z[20] = z[2] * z[20];
z[46] = -z[39] + z[47];
z[42] = z[0] + z[42];
z[42] = z[22] * z[42];
z[48] = (T(-7) * z[12]) / T(2) + z[44];
z[48] = z[12] * z[48];
z[44] = (T(-5) * z[0]) / T(2) + T(9) * z[12] + -z[44];
z[44] = z[0] * z[44];
z[42] = T(-9) * z[16] + z[42] + z[44] + T(3) * z[46] + z[48];
z[42] = z[3] * z[42];
z[44] = z[12] + z[13];
z[46] = z[44] + -z[45];
z[46] = z[11] + T(2) * z[46];
z[48] = z[22] * z[46];
z[49] = T(8) * z[15];
z[24] = z[24] + -z[41] + z[48] + z[49];
z[41] = z[9] * z[24];
z[47] = z[26] + z[47];
z[32] = z[12] * z[32];
z[50] = T(2) * z[16];
z[32] = z[32] + z[50];
z[51] = -z[0] + z[11] + -z[13] + z[33];
z[51] = z[22] * z[51];
z[52] = z[12] * z[45];
z[51] = -z[32] + z[47] + z[51] + z[52];
z[51] = z[4] * z[51];
z[20] = z[20] + z[41] + z[42] + z[51];
z[20] = z[8] * z[20];
z[41] = T(3) * z[18];
z[36] = -z[36] + z[41];
z[42] = -z[33] + z[36];
z[51] = -z[22] + z[35] + -z[42];
z[51] = z[2] * z[51];
z[52] = T(4) * z[0];
z[53] = T(-3) * z[11] + -z[21] + z[41] + z[52];
z[53] = z[3] * z[53];
z[38] = z[38] + -z[42];
z[54] = -(z[9] * z[38]);
z[42] = z[11] + -z[42];
z[42] = z[4] * z[42];
z[42] = z[42] + z[51] + z[53] + z[54];
z[42] = z[8] * z[42];
z[36] = -z[22] + z[36];
z[51] = -z[33] + z[35] + -z[36];
z[51] = z[2] * z[51];
z[53] = T(4) * z[11];
z[54] = -z[12] + z[18];
z[54] = z[52] + -z[53] + T(3) * z[54];
z[54] = z[3] * z[54];
z[36] = z[12] + -z[36];
z[36] = z[9] * z[36];
z[38] = -(z[4] * z[38]);
z[36] = z[36] + z[38] + z[51] + z[54];
z[36] = z[7] * z[36];
z[38] = z[0] + z[41];
z[51] = -z[21] + z[38];
z[34] = -(z[34] * z[51]);
z[31] = z[31] + -z[41];
z[41] = z[31] + z[52];
z[51] = -z[11] + z[41];
z[51] = z[3] * z[51];
z[21] = z[11] + -z[21] + -z[31];
z[21] = z[4] * z[21];
z[21] = z[21] + z[34] + z[51];
z[21] = z[10] * z[21];
z[34] = z[38] + -z[53];
z[38] = -z[2] + z[4];
z[34] = -(z[34] * z[38]);
z[41] = -z[12] + z[41];
z[41] = z[3] * z[41];
z[31] = z[12] + -z[31] + -z[53];
z[31] = z[9] * z[31];
z[31] = z[31] + z[34] + z[41];
z[31] = z[1] * z[31];
z[34] = z[12] + -z[13];
z[41] = -z[11] + -z[34] + z[45];
z[51] = z[2] + T(2) * z[3];
z[41] = z[41] * z[51];
z[44] = -z[11] + z[44];
z[44] = z[4] * z[44];
z[54] = -z[11] + z[34];
z[54] = z[9] * z[54];
z[41] = -z[41] + z[44] + -z[54];
z[44] = z[5] + T(2) * z[6];
z[41] = z[41] * z[44];
z[21] = z[21] + z[31] + z[36] + T(2) * z[41] + z[42];
z[21] = int_to_imaginary<T>(1) * z[21];
z[31] = z[2] / T(3) + -z[3];
z[36] = -z[4] + z[9] / T(3) + -z[31];
z[36] = z[10] * z[36];
z[31] = z[4] / T(3) + -z[9] + -z[31];
z[31] = z[1] * z[31];
z[31] = z[31] + z[36];
z[36] = z[9] / T(2);
z[41] = z[4] / T(2);
z[42] = -z[2] / T(2) + -z[3] + z[36] + z[41];
z[44] = z[5] * z[42];
z[54] = -z[4] + -z[9] + z[51];
z[55] = -(z[6] * z[54]);
z[44] = z[44] + z[55];
z[55] = (T(13) * z[2]) / T(6) + z[3];
z[41] = (T(-13) * z[9]) / T(6) + z[41] + z[55];
z[41] = z[8] * z[41];
z[36] = (T(-13) * z[4]) / T(6) + z[36] + z[55];
z[36] = z[7] * z[36];
z[42] = int_to_imaginary<T>(1) * z[14] * z[42];
z[31] = T(2) * z[31] + z[36] + z[41] + -z[42] / T(3) + T(3) * z[44];
z[31] = z[14] * z[31];
z[21] = T(2) * z[21] + z[31];
z[21] = z[14] * z[21];
z[31] = T(3) * z[34] + -z[52];
z[31] = z[31] * z[45];
z[30] = -z[13] + -z[30] + z[52];
z[30] = -z[11] + T(2) * z[30];
z[30] = z[22] * z[30];
z[26] = z[26] + z[30] + z[31] + z[32] + -z[49];
z[26] = z[2] * z[26];
z[30] = T(2) * z[34];
z[31] = -z[11] + z[30];
z[22] = z[22] * z[31];
z[36] = z[12] * z[37];
z[22] = z[22] + -z[36] + z[47] + -z[50];
z[22] = z[9] * z[22];
z[24] = z[4] * z[24];
z[37] = T(3) * z[15] + z[36] + -z[39];
z[39] = T(8) * z[34];
z[41] = -z[0] + z[39];
z[41] = z[0] * z[41];
z[42] = z[11] + -z[39];
z[42] = z[11] * z[42];
z[37] = T(-6) * z[16] + T(3) * z[37] + z[41] + z[42];
z[37] = z[3] * z[37];
z[22] = z[22] + z[24] + z[26] + z[37];
z[22] = z[7] * z[22];
z[24] = z[34] + z[45];
z[24] = z[24] * z[45];
z[26] = z[46] * z[53];
z[34] = T(10) * z[16];
z[24] = T(-16) * z[15] + -z[24] + -z[26] + z[34] + z[40] + T(5) * z[43];
z[24] = z[24] * z[38];
z[25] = -z[25] + z[34];
z[26] = T(-7) * z[0] + z[39];
z[26] = z[0] * z[26];
z[34] = prod_pow(z[11], 2);
z[26] = T(7) * z[15] + -z[25] + z[26] + -z[28] + T(3) * z[34] + z[36];
z[26] = z[3] * z[26];
z[28] = T(4) * z[15] + -z[28];
z[34] = z[28] + z[36];
z[37] = -(z[31] * z[53]);
z[25] = z[25] + -z[34] + z[37];
z[25] = z[9] * z[25];
z[24] = z[24] + z[25] + z[26];
z[24] = z[1] * z[24];
z[25] = -z[0] + z[30];
z[25] = z[25] * z[45];
z[26] = z[11] * z[31];
z[25] = z[25] + -z[26] + -z[27] + z[34];
z[25] = z[25] * z[51];
z[26] = z[15] + z[26] + -z[29] + z[36];
z[26] = z[9] * z[26];
z[25] = -z[25] + z[26];
z[26] = z[33] + z[35];
z[26] = z[0] * z[26];
z[26] = z[26] + T(2) * z[28] + -z[32] + z[48];
z[26] = z[4] * z[26];
z[25] = T(2) * z[25] + z[26];
z[26] = z[6] * z[25];
z[27] = -(z[19] * z[54]);
z[26] = z[26] + (T(16) * z[27]) / T(3);
z[25] = z[5] * z[25];
return z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + T(2) * z[26];
}



template IntegrandConstructorType<double> f_4_280_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_280_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_280_construct (const Kin<qd_real>&);
#endif

}