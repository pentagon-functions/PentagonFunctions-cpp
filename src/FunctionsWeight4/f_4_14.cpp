#include "f_4_14.h"

namespace PentagonFunctions {

template <typename T> T f_4_14_abbreviated (const std::array<T,17>&);



template <typename T> IntegrandConstructorType<T> f_4_14_construct (const Kin<T>& kin) {
    return [&kin, 
            dl13 = DLog_W_13<T>(kin),dl11 = DLog_W_11<T>(kin),dl6 = DLog_W_6<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,17> abbr = 
            {dl13(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[0]), rlog(v_path[2]), f_2_1_1(kin_path), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[3] / kin_path.W[3]), dl11(t), rlog(v_path[3]), f_2_1_2(kin_path), dl6(t), dl18(t), rlog(kin.W[5] / kin_path.W[5]), dl4(t), dl3(t), dl1(t)}
;

        auto result = f_4_14_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_14_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[43];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[12];
z[11] = abb[16];
z[12] = abb[9];
z[13] = abb[14];
z[14] = abb[15];
z[15] = abb[10];
z[16] = abb[13];
z[17] = abb[11];
z[18] = bc<TR>[1];
z[19] = bc<TR>[2];
z[20] = bc<TR>[4];
z[21] = bc<TR>[7];
z[22] = bc<TR>[8];
z[23] = bc<TR>[9];
z[24] = -z[1] + z[8];
z[25] = z[6] + -z[7];
z[26] = z[24] + z[25];
z[26] = z[9] * z[26];
z[27] = z[11] * z[24];
z[28] = z[14] * z[25];
z[29] = z[26] + -z[27] + -z[28];
z[29] = z[12] * z[29];
z[24] = z[24] + -z[25];
z[24] = z[0] * z[24];
z[30] = z[13] * z[25];
z[27] = z[24] + -z[27] + z[30];
z[31] = z[4] * z[27];
z[32] = -z[24] + z[26];
z[30] = z[28] + z[30];
z[33] = -z[30] + z[32];
z[33] = z[2] * z[33];
z[34] = prod_pow(z[18], 2);
z[34] = T(2) * z[20] + z[34];
z[35] = z[10] + -z[11];
z[34] = z[34] * z[35];
z[31] = -z[29] + z[31] + z[33] + z[34];
z[33] = prod_pow(z[3], 2);
z[34] = z[33] * z[35];
z[31] = T(2) * z[31] + -z[34] / T(3);
z[31] = int_to_imaginary<T>(1) * z[3] * z[31];
z[34] = -z[10] + z[14];
z[35] = z[1] + z[8] + T(-2) * z[16];
z[36] = T(2) * z[35];
z[37] = -z[6] + T(3) * z[7] + z[36];
z[34] = z[34] * z[37];
z[38] = z[6] + z[35];
z[39] = T(2) * z[13];
z[40] = -(z[38] * z[39]);
z[41] = -z[6] + -z[7] + T(4) * z[16];
z[42] = T(3) * z[1] + z[8] + -z[41];
z[42] = z[11] * z[42];
z[26] = -z[26] + z[34] + z[40] + z[42];
z[26] = z[15] * z[26];
z[34] = z[10] * z[37];
z[36] = z[6] + z[7] + z[36];
z[36] = z[11] * z[36];
z[34] = z[34] + -z[36];
z[32] = -z[30] + -z[32] + -z[34];
z[32] = z[2] * z[32];
z[25] = z[25] * z[39];
z[25] = z[25] + T(2) * z[28] + z[34];
z[28] = z[4] * z[25];
z[28] = z[28] + z[32];
z[28] = z[2] * z[28];
z[32] = z[2] + -z[4];
z[25] = z[25] * z[32];
z[25] = z[25] + z[29];
z[25] = z[12] * z[25];
z[29] = -z[10] + z[13];
z[29] = z[29] * z[37];
z[32] = T(2) * z[14];
z[32] = -(z[32] * z[38]);
z[34] = z[1] + T(3) * z[8] + -z[41];
z[34] = z[11] * z[34];
z[24] = z[24] + z[29] + z[32] + z[34];
z[24] = z[5] * z[24];
z[29] = z[7] / T(2);
z[32] = -z[6] / T(2) + -z[29] + -z[35];
z[32] = z[11] * z[32];
z[34] = z[10] + z[11];
z[36] = -z[14] + T(2) * z[17];
z[37] = -z[13] + z[36];
z[38] = -z[34] / T(2) + z[37];
z[38] = z[18] * z[38];
z[30] = z[30] + z[32] + z[38];
z[29] = -z[6] / T(6) + z[29] + z[35] / T(3);
z[29] = z[10] * z[29];
z[29] = z[29] + z[30] / T(3);
z[29] = z[29] * z[33];
z[30] = prod_pow(z[19], 2);
z[30] = T(4) * z[30] + z[33];
z[30] = z[19] * z[30];
z[30] = -z[22] + z[30] / T(3);
z[32] = T(2) * z[36] + -z[39];
z[33] = -z[32] + z[34];
z[30] = z[30] * z[33];
z[27] = -(prod_pow(z[4], 2) * z[27]);
z[32] = z[11] + z[32];
z[32] = -z[10] + z[32] / T(3);
z[32] = prod_pow(z[18], 3) * z[32];
z[33] = -z[11] + T(4) * z[36];
z[33] = z[21] * z[33];
z[34] = (T(19) * z[11]) / T(2) + (T(-49) * z[36]) / T(3);
z[34] = z[23] * z[34];
z[35] = T(-8) * z[21] + (T(49) * z[23]) / T(3);
z[35] = z[13] * z[35];
z[36] = T(-6) * z[21] + (T(41) * z[23]) / T(6);
z[36] = z[10] * z[36];
z[37] = -z[10] + z[37];
z[37] = z[18] * z[20] * z[37];
return z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[31] + z[32] + T(2) * z[33] + z[34] + z[35] + z[36] + T(4) * z[37];
}



template IntegrandConstructorType<double> f_4_14_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_14_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_14_construct (const Kin<qd_real>&);
#endif

}