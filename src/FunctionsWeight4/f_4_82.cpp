#include "f_4_82.h"

namespace PentagonFunctions {

template <typename T> T f_4_82_abbreviated (const std::array<T,48>&);

template <typename T> class SpDLog_f_4_82_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_82_W_23 (const Kin<T>& kin) {
        c[0] = T(2) * prod_pow(kin.v[3], 2) + kin.v[0] * ((T(20) * kin.v[2]) / T(3) + (T(-4) * kin.v[3]) / T(3) + (T(-8) * kin.v[4]) / T(3) + (T(-2) / T(3) + bc<T>[1] * T(-2)) * kin.v[0] + T(4) * kin.v[1] + bc<T>[1] * (T(4) + T(-4) * kin.v[1] + T(4) * kin.v[3])) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(-4) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = T(2) * prod_pow(kin.v[3], 2) + kin.v[0] * ((T(-2) * kin.v[0]) / T(3) + (T(20) * kin.v[2]) / T(3) + (T(-4) * kin.v[3]) / T(3) + (T(-8) * kin.v[4]) / T(3) + T(4) * kin.v[1]) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(-4) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-prod_pow(kin.v[4], 2) + kin.v[0] * (-kin.v[0] / T(3) + (T(10) * kin.v[2]) / T(3) + (T(-2) * kin.v[3]) / T(3) + (T(-4) * kin.v[4]) / T(3) + T(2) * kin.v[1]) + prod_pow(kin.v[3], 2) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (T(-3) * kin.v[2] + T(-2) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[1] * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + bc<T>[1] * (kin.v[0] * ((T(-2) * kin.v[0]) / T(3) + (T(20) * kin.v[2]) / T(3) + (T(-4) * kin.v[3]) / T(3) + (T(-8) * kin.v[4]) / T(3) + T(4) * kin.v[1]) + T(2) * prod_pow(kin.v[3], 2) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(-4) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[1] * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * ((T(-5) * kin.v[2]) / T(3) + kin.v[3] / T(3) + (T(2) * kin.v[4]) / T(3) + -kin.v[1] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[1])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * ((T(-5) * kin.v[2]) / T(3) + kin.v[3] / T(3) + (T(2) * kin.v[4]) / T(3) + -kin.v[1] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[1])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(kin.v[3]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * ((T(-5) * kin.v[2]) / T(3) + kin.v[3] / T(3) + (T(2) * kin.v[4]) / T(3) + -kin.v[1] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[1])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[0] * ((T(-5) * kin.v[2]) / T(3) + kin.v[3] / T(3) + (T(2) * kin.v[4]) / T(3) + -kin.v[1] + (bc<T>[1] / T(2) + T(1) / T(6)) * kin.v[0] + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[1])) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[4]) * ((T(3) * prod_pow(kin.v[3], 2)) / T(2) + (T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[0] * (-kin.v[3] + ((bc<T>[1] * T(-3)) / T(2) + T(-1) / T(2)) * kin.v[0] + T(3) * kin.v[1] + T(5) * kin.v[2] + bc<T>[1] * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + T(-2) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(-3) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(3) * prod_pow(kin.v[3], 2)) / T(2) + (T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[0] * (-kin.v[3] + ((bc<T>[1] * T(-3)) / T(2) + T(-1) / T(2)) * kin.v[0] + T(3) * kin.v[1] + T(5) * kin.v[2] + bc<T>[1] * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + T(-2) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(-3) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]))) + rlog(kin.v[0]) * (-prod_pow(kin.v[3], 2) + kin.v[0] * ((T(-10) * kin.v[2]) / T(3) + (T(2) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3) + (bc<T>[1] + T(1) / T(3)) * kin.v[0] + T(-2) * kin.v[1] + bc<T>[1] * (T(-2) + T(2) * kin.v[1] + T(-2) * kin.v[3])) + prod_pow(kin.v[4], 2) + kin.v[2] * (T(3) * kin.v[2] + T(2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + bc<T>[1] * T(4) + T(8)) * kin.v[0] + T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]);
c[3] = rlog(kin.v[0]) * ((T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(kin.v[3]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + bc<T>[1] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[1] * (T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[17] * (abb[13] * (abb[19] * T(-4) + abb[3] * abb[18] * T(-2) + abb[18] * (abb[4] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[18] * T(2))) + abb[3] * abb[18] * (-abb[1] + -abb[7] + -abb[9] + -abb[11] + abb[20] * T(-4) + abb[8] * T(3) + abb[10] * T(3)) + abb[18] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[20] * bc<T>[0] * int_to_imaginary<T>(4) + abb[4] * (-abb[1] + -abb[7] + -abb[9] + -abb[11] + abb[20] * T(-4) + abb[8] * T(3) + abb[10] * T(3)) + abb[18] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[20] * T(4))) + abb[2] * (abb[13] * abb[18] * T(2) + abb[14] * (-abb[1] + -abb[7] + -abb[9] + -abb[11] + abb[20] * T(-4) + abb[13] * T(-2) + abb[8] * T(3) + abb[10] * T(3)) + abb[18] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[20] * T(4))) + abb[19] * (abb[20] * T(-8) + abb[1] * T(-2) + abb[7] * T(-2) + abb[9] * T(-2) + abb[11] * T(-2) + abb[8] * T(6) + abb[10] * T(6)) + abb[14] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3) + abb[20] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[13] * (bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2) + abb[4] * T(2) + abb[18] * T(2)) + abb[3] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[20] * T(4)) + abb[4] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[20] * T(4)) + abb[18] * (abb[1] + abb[7] + abb[9] + abb[11] + abb[8] * T(-3) + abb[10] * T(-3) + abb[20] * T(4)) + abb[14] * (abb[20] * T(-8) + abb[13] * T(-4) + abb[1] * T(-2) + abb[7] * T(-2) + abb[9] * T(-2) + abb[11] * T(-2) + abb[8] * T(6) + abb[10] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_82_construct (const Kin<T>& kin) {
    return [&kin, 
            dl3 = DLog_W_3<T>(kin),dl9 = DLog_W_9<T>(kin),dl11 = DLog_W_11<T>(kin),dl23 = DLog_W_23<T>(kin),dl24 = DLog_W_24<T>(kin),dl6 = DLog_W_6<T>(kin),dl28 = DLog_W_28<T>(kin),dl1 = DLog_W_1<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),spdl23 = SpDLog_f_4_82_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,48> abbr = 
            {dl3(t), rlog(kin.W[16] / kin_path.W[16]), rlog(v_path[0]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_2(kin_path), f_2_1_9(kin_path), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), dl9(t), rlog(kin.W[0] / kin_path.W[0]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(kin.W[1] / kin_path.W[1]), dl11(t), dl23(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), -rlog(t), dl24(t), rlog(kin.W[18] / kin_path.W[18]), dl6(t), dl28(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl1(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl4(t), rlog(kin.W[8] / kin_path.W[8]), dl5(t), dl16(t), dl19(t), dl2(t), dl17(t), rlog(kin.W[23] / kin_path.W[23]), dl20(t), dl18(t)}
;

        auto result = f_4_82_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_82_abbreviated(const std::array<T,48>& abb)
{
using TR = typename T::value_type;
T z[143];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[23];
z[14] = abb[31];
z[15] = abb[38];
z[16] = abb[41];
z[17] = abb[42];
z[18] = abb[43];
z[19] = abb[44];
z[20] = abb[47];
z[21] = abb[14];
z[22] = abb[18];
z[23] = abb[12];
z[24] = abb[46];
z[25] = abb[21];
z[26] = abb[40];
z[27] = abb[19];
z[28] = abb[26];
z[29] = abb[27];
z[30] = abb[32];
z[31] = abb[33];
z[32] = abb[16];
z[33] = abb[13];
z[34] = abb[15];
z[35] = abb[20];
z[36] = abb[22];
z[37] = abb[24];
z[38] = abb[25];
z[39] = abb[28];
z[40] = abb[29];
z[41] = abb[30];
z[42] = abb[35];
z[43] = abb[37];
z[44] = abb[34];
z[45] = abb[36];
z[46] = abb[39];
z[47] = abb[45];
z[48] = bc<TR>[1];
z[49] = bc<TR>[3];
z[50] = bc<TR>[5];
z[51] = bc<TR>[2];
z[52] = bc<TR>[4];
z[53] = bc<TR>[7];
z[54] = bc<TR>[8];
z[55] = bc<TR>[9];
z[56] = -z[9] + z[11];
z[57] = -z[1] + z[12];
z[58] = -z[8] + z[10] + z[56] + z[57];
z[59] = z[0] * z[58];
z[60] = -z[38] + -z[39] + z[40] + z[41];
z[61] = z[43] * z[60];
z[62] = z[42] * z[60];
z[63] = z[45] * z[60];
z[63] = z[59] + -z[61] + -z[62] + -z[63];
z[64] = z[8] + z[12];
z[65] = z[33] / T(2);
z[66] = (T(4) * z[35]) / T(3);
z[67] = (T(13) * z[10]) / T(4) + z[36];
z[67] = (T(5) * z[1]) / T(4) + (T(-7) * z[9]) / T(4) + z[11] / T(4) + -z[47] / T(6) + (T(-11) * z[64]) / T(12) + z[65] + z[66] + z[67] / T(3);
z[67] = z[19] * z[67];
z[68] = z[8] / T(2);
z[69] = z[12] / T(2);
z[70] = z[68] + z[69];
z[71] = z[10] / T(2) + z[70];
z[72] = z[33] + -z[36];
z[73] = z[9] / T(2);
z[74] = z[11] / T(2);
z[75] = z[1] / T(6) + z[71] + z[72] / T(3) + -z[73] + -z[74];
z[75] = z[17] * z[75];
z[76] = (T(5) * z[9]) / T(2) + z[74];
z[77] = z[1] + z[10];
z[78] = (T(3) * z[77]) / T(2);
z[79] = -z[70] + -z[76] + z[78];
z[80] = z[33] + z[79];
z[80] = z[35] + z[80] / T(2);
z[80] = z[24] * z[80];
z[81] = -z[1] + z[34];
z[74] = (T(-4) * z[12]) / T(3) + -z[33] / T(6) + (T(7) * z[36]) / T(6) + -z[68] + z[74] + -z[81];
z[74] = z[14] * z[74];
z[82] = -z[33] + z[68];
z[83] = T(3) * z[10];
z[84] = (T(13) * z[1]) / T(3) + -z[83];
z[76] = (T(7) * z[12]) / T(6) + z[76] + z[82] + z[84] / T(2);
z[84] = (T(7) * z[51]) / T(2);
z[76] = -z[35] + (T(-13) * z[46]) / T(6) + z[76] / T(2) + -z[84];
z[76] = z[15] * z[76];
z[85] = T(3) * z[11];
z[86] = T(3) * z[8] + -z[85];
z[72] = -z[1] + T(3) * z[12] + z[72] + z[86];
z[72] = z[25] * z[72];
z[87] = T(3) * z[9];
z[88] = -z[12] + z[33] + -z[34] + T(3) * z[77] + -z[87];
z[88] = z[23] * z[88];
z[89] = (T(-25) * z[8]) / T(2) + T(-5) * z[33] + (T(11) * z[77]) / T(2);
z[89] = (T(11) * z[11]) / T(2) + (T(-23) * z[12]) / T(6) + -z[73] + z[89] / T(3);
z[66] = z[46] / T(6) + -z[66] + z[89] / T(2);
z[66] = z[16] * z[66];
z[89] = z[20] + z[24];
z[90] = z[18] / T(3);
z[91] = z[89] + z[90];
z[92] = z[16] + -z[19];
z[91] = (T(-7) * z[17]) / T(6) + (T(7) * z[91]) / T(2) + (T(13) * z[92]) / T(3);
z[91] = z[51] * z[91];
z[93] = -z[1] + (T(-11) * z[12]) / T(2) + (T(13) * z[47]) / T(2);
z[93] = z[20] * z[93];
z[63] = -z[63] / T(4) + z[66] + z[67] + z[72] + z[74] + z[75] / T(2) + z[76] + z[80] + -z[88] + z[91] + z[93] / T(3);
z[63] = z[7] * z[63];
z[66] = z[42] + -z[43] + z[44] + -z[45];
z[67] = z[49] * z[66];
z[63] = z[63] + -z[67];
z[63] = z[7] * z[63];
z[74] = T(2) * z[35];
z[75] = z[33] + z[74];
z[76] = T(2) * z[64] + z[75] + -z[77] + -z[85];
z[91] = z[16] * z[76];
z[93] = z[12] + -z[47];
z[94] = T(2) * z[20];
z[93] = z[93] * z[94];
z[95] = z[10] + z[64];
z[96] = -z[36] + -z[74] + z[95];
z[97] = z[19] * z[96];
z[98] = z[12] + -z[36];
z[98] = z[14] * z[98];
z[99] = z[15] * z[57];
z[93] = z[72] + -z[91] + -z[93] + -z[97] + -z[98] + z[99];
z[93] = z[4] * z[93];
z[98] = -z[64] + z[75] + T(2) * z[77] + -z[87];
z[100] = z[19] * z[98];
z[101] = -z[8] + z[34] + z[74] + -z[77];
z[102] = -(z[16] * z[101]);
z[103] = T(2) * z[15];
z[104] = z[1] + -z[46];
z[104] = z[103] * z[104];
z[105] = z[20] * z[57];
z[81] = -(z[14] * z[81]);
z[81] = z[81] + -z[88] + z[100] + z[102] + z[104] + z[105];
z[81] = z[3] * z[81];
z[102] = z[12] + -z[34];
z[102] = z[32] * z[102];
z[104] = -z[2] + z[3];
z[106] = -(z[102] * z[104]);
z[107] = z[1] + -z[36];
z[107] = z[13] * z[107];
z[108] = -z[2] + z[4];
z[109] = z[107] * z[108];
z[81] = z[81] + z[93] + z[106] + z[109];
z[106] = z[98] * z[103];
z[109] = z[77] / T(2);
z[70] = z[70] + z[109];
z[110] = (T(3) * z[9]) / T(2);
z[111] = (T(3) * z[11]) / T(2);
z[112] = z[110] + z[111];
z[113] = z[70] + z[75] + -z[112];
z[114] = z[20] * z[113];
z[115] = (T(3) * z[60]) / T(2);
z[116] = z[44] * z[115];
z[114] = z[114] + -z[116];
z[117] = z[16] * z[113];
z[118] = z[43] * z[115];
z[117] = z[117] + z[118];
z[119] = z[114] + z[117];
z[79] = z[75] + z[79];
z[79] = T(3) * z[79];
z[120] = z[24] * z[79];
z[121] = z[45] * z[115];
z[120] = z[120] + z[121];
z[121] = z[14] * z[113];
z[100] = T(2) * z[100] + z[106] + -z[119] + -z[120] + z[121];
z[100] = z[22] * z[100];
z[106] = (T(3) * z[8]) / T(2);
z[78] = z[78] + z[106];
z[112] = -z[33] + z[112];
z[122] = -z[69] + z[112];
z[123] = T(6) * z[35];
z[124] = T(2) * z[34] + -z[78] + -z[122] + z[123];
z[124] = z[16] * z[124];
z[125] = z[15] + z[19];
z[79] = z[79] * z[125];
z[78] = -z[34] + z[78] + -z[122];
z[126] = z[14] * z[78];
z[120] = z[118] + z[120];
z[127] = z[18] * z[78];
z[115] = z[42] * z[115];
z[79] = -z[79] + z[115] + z[120] + z[124] + -z[126] + z[127];
z[124] = z[31] * z[79];
z[126] = (T(3) * z[10]) / T(2);
z[128] = (T(3) * z[12]) / T(2) + z[106];
z[129] = z[126] + z[128];
z[130] = z[1] / T(2);
z[131] = -z[112] + z[130];
z[123] = T(2) * z[36] + z[123] + -z[129] + z[131];
z[123] = z[19] * z[123];
z[73] = (T(5) * z[11]) / T(2) + z[73] + z[109] + -z[128];
z[132] = z[73] + -z[75];
z[132] = T(3) * z[132];
z[133] = z[16] + z[20];
z[134] = z[132] * z[133];
z[129] = -z[36] + z[129] + z[131];
z[135] = z[14] * z[129];
z[136] = z[17] * z[129];
z[123] = -z[115] + z[123] + z[134] + -z[135] + z[136];
z[116] = -z[116] + z[118] + -z[123];
z[116] = z[29] * z[116];
z[118] = -z[23] + z[25];
z[134] = z[17] / T(3) + -z[90];
z[135] = z[15] / T(2);
z[118] = (T(-11) * z[66]) / T(180) + -z[89] / T(2) + z[92] / T(3) + (T(-5) * z[118]) / T(6) + -z[134] + z[135];
z[137] = prod_pow(z[7], 2);
z[118] = z[118] * z[137];
z[76] = z[76] * z[94];
z[138] = z[15] * z[113];
z[113] = z[19] * z[113];
z[113] = z[113] + -z[115];
z[139] = z[113] + z[138];
z[76] = z[76] + T(2) * z[91] + z[121] + -z[139];
z[91] = z[26] * z[132];
z[91] = z[76] + z[91];
z[121] = -(z[21] * z[91]);
z[94] = z[94] + z[103];
z[94] = z[57] * z[94];
z[94] = z[94] + -z[113] + z[117];
z[103] = -z[34] + z[36] + -z[57];
z[103] = z[14] * z[103];
z[103] = -z[94] + z[103] + -z[127] + z[136];
z[103] = z[2] * z[103];
z[113] = z[29] * z[132];
z[117] = z[137] / T(2);
z[113] = z[113] + z[117];
z[113] = z[26] * z[113];
z[140] = z[17] + -z[18];
z[141] = z[92] + T(3) * z[140];
z[141] = z[52] * z[141];
z[142] = z[2] + -z[29];
z[142] = z[37] * z[60] * z[142];
z[81] = T(2) * z[81] + z[100] + z[103] + z[113] + z[116] + z[118] + z[121] + z[124] + z[141] + (T(3) * z[142]) / T(2);
z[81] = z[7] * z[81];
z[103] = z[92] + -z[140];
z[103] = z[51] * z[103];
z[113] = (T(-3) * z[66]) / T(20) + T(2) * z[140];
z[113] = z[48] * z[113];
z[103] = z[103] + z[113];
z[103] = z[7] * z[103];
z[67] = T(3) * z[67] + z[103];
z[67] = z[48] * z[67];
z[66] = z[50] * z[66];
z[66] = (T(36) * z[66]) / T(5) + z[67] + z[81];
z[66] = int_to_imaginary<T>(1) * z[66];
z[67] = -(z[15] * z[98]);
z[81] = z[136] / T(2);
z[70] = z[70] + -z[112];
z[70] = z[35] + z[70] / T(2);
z[98] = z[16] * z[70];
z[103] = z[20] * z[70];
z[112] = z[9] + z[11];
z[65] = -z[1] / T(4) + z[35] + -z[36] + T(2) * z[47] + -z[65] + (T(-5) * z[95]) / T(4) + (T(3) * z[112]) / T(4);
z[65] = z[19] * z[65];
z[71] = z[36] + -z[71] + z[131];
z[71] = z[71] / T(2) + z[74];
z[71] = z[14] * z[71];
z[74] = (T(3) * z[60]) / T(4);
z[95] = z[44] * z[74];
z[112] = (T(3) * z[61]) / T(4);
z[95] = z[95] + -z[112];
z[62] = (T(3) * z[62]) / T(4);
z[65] = z[62] + z[65] + z[67] + z[71] + z[72] + -z[81] + -z[95] + z[98] + z[103];
z[65] = z[22] * z[65];
z[67] = -(z[14] * z[96]);
z[67] = z[67] + T(-2) * z[97] + -z[119] + z[136] + -z[138];
z[67] = z[2] * z[67];
z[65] = z[65] + z[67];
z[65] = z[22] * z[65];
z[67] = z[19] * z[70];
z[62] = -z[62] + z[67];
z[67] = -z[62] + z[98] + z[105] + z[112];
z[71] = z[45] * z[74];
z[71] = z[71] + T(3) * z[80] + -z[88];
z[72] = z[127] / T(2);
z[74] = -z[34] + z[122];
z[80] = -z[1] + z[83];
z[80] = -z[74] + z[80] / T(2) + z[106];
z[88] = z[14] / T(2);
z[80] = z[80] * z[88];
z[96] = z[1] + T(9) * z[10];
z[96] = (T(-15) * z[9]) / T(2) + T(3) * z[33] + z[96] / T(2) + -z[111] + -z[128];
z[97] = T(2) * z[46];
z[96] = T(3) * z[35] + z[96] / T(2) + z[97];
z[96] = z[15] * z[96];
z[80] = -z[67] + -z[71] + -z[72] + z[80] + z[96];
z[80] = z[3] * z[80];
z[86] = z[57] + -z[83] + z[86] + z[87];
z[86] = z[86] * z[88];
z[88] = (T(3) * z[59]) / T(2);
z[86] = z[86] + z[88] + -z[94];
z[94] = z[86] * z[108];
z[80] = z[80] + z[94] + -z[100];
z[80] = z[3] * z[80];
z[94] = T(4) * z[35];
z[68] = z[68] + z[74] + -z[94] + z[109];
z[68] = z[14] * z[68];
z[74] = z[15] + z[20];
z[70] = z[70] * z[74];
z[74] = (T(9) * z[11]) / T(2) + (T(-7) * z[12]) / T(2) + -z[110];
z[82] = z[74] + (T(13) * z[77]) / T(2) + z[82];
z[82] = T(-5) * z[35] + z[82] / T(2) + -z[97];
z[82] = z[16] * z[82];
z[73] = -z[33] + z[73];
z[73] = -z[35] + z[73] / T(2);
z[73] = T(3) * z[73];
z[96] = -(z[26] * z[73]);
z[62] = z[62] + z[68] + z[70] + z[71] + z[82] + -z[95] + z[96];
z[62] = z[21] * z[62];
z[68] = z[4] * z[91];
z[70] = z[14] + T(2) * z[16];
z[70] = z[70] * z[101];
z[70] = -z[70] + z[114] + -z[127] + z[139];
z[70] = -(z[70] * z[104]);
z[62] = z[62] + z[68] + z[70] + z[100];
z[62] = z[21] * z[62];
z[68] = -(z[20] * z[58]);
z[68] = z[59] + z[68];
z[70] = -(z[16] * z[78]);
z[71] = z[110] + -z[111];
z[78] = z[8] + -z[77];
z[69] = z[34] + z[69] + z[71] + (T(3) * z[78]) / T(2);
z[69] = z[14] * z[69];
z[56] = z[56] + -z[64] + z[77];
z[78] = (T(3) * z[15]) / T(2);
z[82] = z[56] * z[78];
z[68] = (T(3) * z[68]) / T(2) + z[69] + z[70] + z[82] + z[127];
z[68] = z[5] * z[68];
z[69] = -(z[22] * z[76]);
z[70] = -(z[2] * z[86]);
z[69] = z[69] + z[70] + -z[93];
z[69] = z[4] * z[69];
z[70] = -(z[30] * z[79]);
z[76] = -(z[28] * z[123]);
z[79] = -z[36] + z[71] + -z[126] + z[128] + -z[130];
z[79] = z[14] * z[79];
z[56] = z[20] * z[56];
z[56] = z[56] + z[59];
z[59] = z[19] * z[129];
z[58] = -(z[58] * z[78]);
z[56] = (T(3) * z[56]) / T(2) + z[58] + z[59] + z[79] + -z[115] + -z[136];
z[56] = z[6] * z[56];
z[58] = T(-2) * z[33] + -z[64] + -z[77] + z[85] + z[87] + -z[94];
z[58] = z[14] * z[58];
z[59] = (T(9) * z[9]) / T(2) + (T(5) * z[64]) / T(2) + -z[75] + (T(-7) * z[77]) / T(2) + -z[111];
z[59] = z[59] * z[125];
z[64] = (T(-7) * z[8]) / T(2) + z[74] + -z[75] + (T(5) * z[77]) / T(2);
z[64] = z[64] * z[133];
z[58] = z[58] + z[59] + z[64] + -z[115] + z[120];
z[58] = z[27] * z[58];
z[59] = z[34] + -z[36] + -z[83];
z[57] = z[57] + z[59] / T(2) + z[71] + z[106];
z[57] = z[14] * z[57];
z[57] = z[57] + -z[67] + z[72] + -z[81] + z[88] + -z[99];
z[59] = prod_pow(z[2], 2);
z[57] = z[57] * z[59];
z[64] = -(z[4] * z[22]);
z[64] = -z[27] + z[28] + z[64];
z[64] = z[64] * z[132];
z[67] = prod_pow(z[22], 2) * z[73];
z[71] = prod_pow(z[51], 3);
z[71] = T(2) * z[71];
z[72] = T(6) * z[53] + (T(3) * z[54]) / T(2) + (T(-31) * z[55]) / T(4);
z[73] = z[71] + -z[72];
z[74] = -(z[84] * z[137]);
z[64] = z[64] + z[67] + -z[73] + z[74];
z[64] = z[26] * z[64];
z[67] = (T(-13) * z[18]) / T(3) + -z[89];
z[67] = (T(13) * z[17]) / T(6) + z[67] / T(2) + (T(-7) * z[92]) / T(3) + z[135];
z[67] = z[67] * z[117];
z[74] = z[26] + -z[89] + z[90];
z[75] = (T(-2) * z[92]) / T(3);
z[74] = -z[17] / T(6) + z[74] / T(2) + z[75] + z[135];
z[74] = prod_pow(z[48], 2) * z[74];
z[77] = z[15] + -z[89] + -z[92];
z[77] = z[52] * z[77];
z[78] = T(3) * z[52] + z[137] / T(4);
z[78] = z[26] * z[78];
z[67] = z[67] + z[74] + T(3) * z[77] + z[78];
z[67] = z[48] * z[67];
z[74] = z[5] + -z[28];
z[61] = -(z[61] * z[74]);
z[74] = z[6] + z[28];
z[77] = z[5] + -z[27] + -z[74];
z[77] = z[44] * z[60] * z[77];
z[61] = z[61] + z[77];
z[75] = -z[75] + z[89] + -z[134];
z[71] = z[71] * z[75];
z[75] = z[2] + -z[22] / T(2);
z[75] = z[22] * z[75];
z[74] = -z[59] / T(2) + -z[74] + z[75];
z[74] = T(3) * z[74] + z[117];
z[60] = z[37] * z[60] * z[74];
z[74] = -z[53] + -z[54] / T(2) + (T(7) * z[55]) / T(12);
z[74] = -(z[74] * z[140]);
z[75] = T(5) * z[53] + z[54] + (T(7) * z[55]) / T(6);
z[75] = -(z[75] * z[92]);
z[72] = -(z[72] * z[89]);
z[73] = -(z[15] * z[73]);
z[77] = prod_pow(z[3], 2);
z[77] = -z[5] + -z[59] + z[77];
z[77] = z[77] * z[102];
z[78] = -prod_pow(z[4], 2);
z[59] = z[6] + z[59] + z[78];
z[59] = z[59] * z[107];
return z[56] + z[57] + z[58] + z[59] + z[60] / T(2) + (T(3) * z[61]) / T(2) + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[74] + z[75] + z[76] + z[77] + z[80];
}



template IntegrandConstructorType<double> f_4_82_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_82_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_82_construct (const Kin<qd_real>&);
#endif

}