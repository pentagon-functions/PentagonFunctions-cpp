#include "f_4_55.h"

namespace PentagonFunctions {

template <typename T> T f_4_55_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_55_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_55_W_7 (const Kin<T>& kin) {
        c[0] = T(6) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(8) * prod_pow(kin.v[4], 2) + kin.v[3] * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(16) * kin.v[4] + T(12) * kin.v[4]) + rlog(-kin.v[1]) * (((T(-27) * kin.v[4]) / T(2) + T(14)) * kin.v[4] + T(14) * kin.v[1] * kin.v[4] + kin.v[3] * (T(14) + T(14) * kin.v[1] + (T(-27) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-27) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-4) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(27) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + T(-14) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-8) + T(4) * kin.v[4])) + kin.v[3] * (T(-14) + T(-14) * kin.v[1] + (T(27) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[3] + T(27) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[4])));
c[1] = rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * abb[5] * abb[7] * T(-8) + abb[4] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[4] * abb[7] * T(4)) + abb[6] * abb[7] * T(6) + abb[3] * abb[4] * abb[7] * T(8) + abb[1] * (abb[3] * abb[4] * T(-8) + abb[6] * T(-6) + abb[4] * (abb[4] * T(-4) + bc<T>[0] * int_to_imaginary<T>(8)) + abb[4] * abb[5] * T(8)) + abb[2] * (abb[3] * abb[7] * T(-8) + abb[7] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * (abb[7] * T(-4) + abb[1] * T(4)) + abb[5] * abb[7] * T(8) + abb[1] * (abb[5] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[3] * T(8))));
    }
};
template <typename T> class SpDLog_f_4_55_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_55_W_12 (const Kin<T>& kin) {
        c[0] = (T(-9) / T(2) + (T(-27) * kin.v[4]) / T(8)) * kin.v[4] + (T(9) / T(2) + (T(9) * kin.v[1]) / T(8) + (T(9) * kin.v[4]) / T(4)) * kin.v[1];
c[1] = kin.v[4] * (T(-9) / T(2) + T(-9) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-8) * kin.v[4]) + rlog(-kin.v[4]) * (kin.v[1] * (T(12) + T(6) * kin.v[1]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4])) + kin.v[1] * (T(9) / T(2) + (T(-9) * kin.v[1]) / T(2) + (T(27) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[1] * ((T(23) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(8) + T(23) + (T(23) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1]) + ((T(-69) * kin.v[4]) / T(4) + T(-23)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-4) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (T(31) / T(2) + (T(55) * kin.v[4]) / T(4) + bc<T>[0] * int_to_imaginary<T>(8) + (T(7) / T(8) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1]) + (T(-31) / T(2) + (T(-117) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(-8) + T(-4) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * ((T(-23) * kin.v[4]) / T(2) + T(-23) + bc<T>[0] * int_to_imaginary<T>(-8) + (T(-23) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1]) + ((T(69) * kin.v[4]) / T(4) + T(23)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(8) + T(4) * kin.v[4]));
c[2] = (T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[4]) / T(2);
c[3] = rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(9)) * kin.v[1] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[1]) * ((T(-9) * kin.v[4]) / T(2) + (T(9) / T(2) + bc<T>[0] * int_to_imaginary<T>(8)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-9) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(9) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[8] * (abb[3] * abb[5] * (abb[12] * T(-8) + abb[7] * T(8)) + abb[1] * ((abb[10] * T(-9)) / T(2) + abb[3] * abb[5] * T(-8) + abb[5] * ((abb[5] * T(-11)) / T(2) + bc<T>[0] * int_to_imaginary<T>(8) + abb[4] * T(8))) + abb[2] * (abb[12] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[7] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * ((abb[11] * T(-9)) / T(2) + (abb[1] * T(11)) / T(2) + abb[7] * T(-7) + abb[9] * T(6) + abb[12] * T(7)) + abb[1] * (abb[4] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[3] * T(8)) + abb[4] * (abb[12] * T(-8) + abb[7] * T(8)) + abb[3] * (abb[7] * T(-8) + abb[12] * T(8))) + abb[5] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[12] * bc<T>[0] * int_to_imaginary<T>(8) + abb[5] * ((abb[11] * T(9)) / T(2) + abb[12] * T(-7) + abb[9] * T(-6) + abb[7] * T(7)) + abb[4] * (abb[7] * T(-8) + abb[12] * T(8))) + abb[10] * ((abb[11] * T(-9)) / T(2) + abb[12] * T(-9) + abb[7] * T(9)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_55_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl12 = DLog_W_12<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl14 = DLog_W_14<T>(kin),spdl7 = SpDLog_f_4_55_W_7<T>(kin),spdl12 = SpDLog_f_4_55_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,21> abbr = 
            {dl7(t), rlog(kin.W[1] / kin_path.W[1]), rlog(-v_path[1]), rlog(v_path[3]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[4]), f_2_1_11(kin_path), rlog(kin.W[18] / kin_path.W[18]), dl12(t), rlog(kin.W[4] / kin_path.W[4]), f_2_1_4(kin_path), -rlog(t), rlog(kin.W[3] / kin_path.W[3]), dl4(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), rlog(kin.W[13] / kin_path.W[13]), dl19(t), dl2(t), dl5(t), dl14(t)}
;

        auto result = f_4_55_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_55_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[79];
z[0] = abb[1];
z[1] = abb[2];
z[2] = abb[17];
z[3] = abb[18];
z[4] = abb[3];
z[5] = abb[13];
z[6] = abb[19];
z[7] = abb[4];
z[8] = abb[5];
z[9] = bc<TR>[0];
z[10] = abb[6];
z[11] = abb[10];
z[12] = abb[14];
z[13] = abb[15];
z[14] = abb[7];
z[15] = abb[20];
z[16] = abb[9];
z[17] = abb[11];
z[18] = abb[12];
z[19] = abb[16];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = T(5) * z[8];
z[23] = T(3) * z[7];
z[24] = T(3) * z[4];
z[25] = T(2) * z[1];
z[26] = -z[22] + z[23] + -z[24] + z[25];
z[26] = z[18] * z[26];
z[27] = T(5) * z[4];
z[28] = T(3) * z[13];
z[29] = z[1] + z[28];
z[30] = -z[7] + z[22] + z[27] + -z[29];
z[31] = -(z[16] * z[30]);
z[32] = z[7] + -z[13];
z[33] = T(3) * z[19];
z[34] = z[32] * z[33];
z[35] = T(3) * z[8];
z[36] = T(4) * z[1];
z[37] = z[35] + -z[36];
z[38] = T(2) * z[4];
z[39] = -z[7] + z[38];
z[40] = -z[37] + z[39];
z[41] = z[0] * z[40];
z[26] = z[26] + z[31] + -z[34] + z[41];
z[26] = z[5] * z[26];
z[31] = T(4) * z[8];
z[41] = z[29] + -z[31];
z[42] = z[2] + -z[5];
z[41] = z[41] * z[42];
z[43] = T(4) * z[4];
z[44] = -z[7] + -z[28] + z[43];
z[45] = z[31] + z[44];
z[45] = z[6] * z[45];
z[44] = -z[36] + -z[44];
z[44] = z[3] * z[44];
z[41] = z[41] + z[44] + z[45];
z[41] = z[17] * z[41];
z[44] = z[23] + z[31];
z[45] = z[4] + -z[13];
z[45] = -z[36] + z[44] + T(3) * z[45];
z[46] = z[5] * z[14];
z[45] = z[45] * z[46];
z[26] = z[26] + z[41] + z[45];
z[41] = z[4] + T(-2) * z[13];
z[41] = -z[1] + T(3) * z[41] + z[44];
z[41] = z[18] * z[41];
z[44] = -z[4] + -z[32];
z[44] = z[33] * z[44];
z[41] = z[41] + z[44];
z[44] = z[28] + z[38];
z[45] = T(6) * z[7];
z[44] = T(-13) * z[8] + -z[25] + T(3) * z[44] + z[45];
z[44] = z[14] * z[44];
z[47] = -z[1] + z[38];
z[48] = T(2) * z[7];
z[49] = T(14) * z[8] + T(-15) * z[13] + z[47] + z[48];
z[49] = z[16] * z[49];
z[50] = z[4] + z[28];
z[51] = T(7) * z[8];
z[50] = T(2) * z[50] + -z[51];
z[52] = z[1] + -z[48] + -z[50];
z[52] = z[0] * z[52];
z[41] = T(2) * z[41] + z[44] + z[49] + z[52];
z[41] = z[2] * z[41];
z[44] = T(2) * z[18];
z[49] = T(-2) * z[16] + -z[44];
z[30] = z[30] * z[49];
z[49] = T(8) * z[8];
z[52] = T(8) * z[4];
z[29] = T(-4) * z[7] + -z[29] + z[49] + z[52];
z[29] = z[14] * z[29];
z[53] = prod_pow(z[20], 2);
z[54] = z[4] + z[8];
z[55] = z[13] + -z[54];
z[55] = -z[1] + z[7] + T(6) * z[55];
z[55] = z[0] * z[55];
z[29] = z[29] + z[30] + z[34] + (T(9) * z[53]) / T(2) + z[55];
z[29] = z[6] * z[29];
z[30] = T(2) * z[8];
z[28] = z[28] + -z[30] + z[39];
z[28] = z[16] * z[28];
z[34] = -(z[32] * z[44]);
z[32] = z[32] + z[38];
z[32] = z[19] * z[32];
z[39] = T(2) * z[14];
z[55] = z[8] + -z[13] + -z[38];
z[55] = z[39] * z[55];
z[28] = z[28] + z[32] + z[34] + -z[53] / T(2) + z[55];
z[32] = prod_pow(z[9], 2);
z[28] = T(3) * z[28] + (T(-5) * z[32]) / T(4);
z[28] = z[15] * z[28];
z[34] = T(9) * z[8];
z[55] = T(16) * z[1];
z[56] = T(-7) * z[7] + -z[34] + z[52] + z[55];
z[56] = z[0] * z[56];
z[57] = T(7) * z[1];
z[45] = z[45] + -z[50] + -z[57];
z[45] = z[14] * z[45];
z[40] = z[40] * z[44];
z[50] = -z[1] + -z[7] + T(2) * z[54];
z[50] = z[16] * z[50];
z[40] = z[40] + z[45] + z[50] + z[56];
z[40] = z[3] * z[40];
z[45] = z[42] * z[53];
z[50] = -z[2] + -z[3] / T(6) + (T(15) * z[5]) / T(4) + (T(29) * z[6]) / T(12);
z[50] = z[32] * z[50];
z[26] = T(2) * z[26] + z[28] + z[29] + z[40] + z[41] + T(-3) * z[45] + z[50];
z[26] = int_to_imaginary<T>(1) * z[9] * z[26];
z[28] = -z[24] + -z[36] + z[51];
z[28] = -z[23] + T(2) * z[28];
z[28] = z[7] * z[28];
z[29] = z[8] + z[38];
z[40] = -(z[29] * z[31]);
z[41] = T(7) * z[4];
z[45] = -z[35] + z[41];
z[45] = T(2) * z[45] + z[57];
z[45] = z[1] * z[45];
z[50] = T(7) * z[11];
z[53] = T(2) * z[10];
z[56] = prod_pow(z[4], 2);
z[57] = T(3) * z[56];
z[58] = T(6) * z[12];
z[28] = z[28] + z[40] + z[45] + -z[50] + z[53] + -z[57] + -z[58];
z[28] = z[28] * z[46];
z[40] = -z[25] + z[54];
z[45] = z[7] + T(2) * z[40];
z[45] = z[7] * z[45];
z[59] = -z[1] + z[54];
z[55] = z[55] * z[59];
z[59] = T(12) * z[11];
z[60] = T(5) * z[10];
z[61] = T(-14) * z[4] + -z[8];
z[61] = z[8] * z[61];
z[55] = T(-15) * z[12] + -z[45] + z[55] + -z[56] + -z[59] + -z[60] + z[61];
z[55] = z[16] * z[55];
z[61] = z[24] + -z[36];
z[62] = z[8] + z[61];
z[23] = z[23] + T(2) * z[62];
z[23] = z[7] * z[23];
z[62] = z[23] + z[57];
z[63] = T(-6) * z[1] + z[22] + z[41];
z[63] = z[25] * z[63];
z[64] = z[8] + z[52];
z[64] = z[8] * z[64];
z[65] = T(8) * z[10];
z[63] = T(-12) * z[12] + -z[62] + z[63] + -z[64] + -z[65];
z[63] = z[18] * z[63];
z[66] = z[30] + z[41];
z[66] = z[8] * z[66];
z[67] = T(11) * z[11];
z[66] = -z[56] + z[58] + z[66] + z[67];
z[68] = -z[1] + z[30];
z[69] = z[4] + z[48];
z[70] = z[68] + -z[69];
z[70] = z[48] * z[70];
z[71] = T(11) * z[8];
z[72] = T(-8) * z[1] + z[27] + z[71];
z[72] = z[1] * z[72];
z[72] = -z[53] + -z[66] + -z[70] + z[72];
z[72] = z[0] * z[72];
z[71] = T(9) * z[1] + -z[41] + -z[71];
z[71] = z[1] * z[71];
z[73] = T(13) * z[4];
z[74] = -z[8] + z[73];
z[74] = z[8] * z[74];
z[50] = T(-6) * z[10] + T(9) * z[12] + z[50] + -z[62] + z[71] + z[74];
z[50] = z[14] * z[50];
z[62] = z[1] + -z[4];
z[71] = -(z[48] * z[62]);
z[74] = z[1] * z[47];
z[74] = z[10] + T(2) * z[12] + -z[74];
z[71] = z[56] + z[71] + z[74];
z[71] = z[33] * z[71];
z[75] = z[11] * z[18];
z[76] = T(14) * z[75];
z[50] = (T(-95) * z[21]) / T(4) + z[50] + z[55] + z[63] + z[71] + z[72] + -z[76];
z[50] = z[2] * z[50];
z[55] = z[22] + z[38];
z[55] = -z[1] + T(2) * z[55];
z[55] = z[1] * z[55];
z[63] = T(3) * z[12];
z[71] = T(4) * z[56];
z[47] = -z[8] + z[47];
z[47] = -z[7] + T(2) * z[47];
z[47] = z[7] * z[47];
z[47] = z[47] + -z[53] + z[55] + -z[59] + -z[63] + -z[64] + -z[71];
z[47] = z[14] * z[47];
z[53] = -z[1] + T(4) * z[54];
z[53] = z[25] * z[53];
z[54] = z[8] * z[29];
z[55] = z[54] + z[56];
z[45] = -z[45] + -z[53] + T(5) * z[55] + z[58];
z[53] = z[18] * z[45];
z[64] = T(10) * z[11] + z[45];
z[64] = z[16] * z[64];
z[31] = T(5) * z[1] + -z[31] + -z[69];
z[69] = z[7] * z[31];
z[69] = T(2) * z[11] + z[60] + -z[69];
z[27] = -z[27] + -z[30];
z[27] = z[1] * z[27];
z[27] = z[27] + T(3) * z[55] + z[58] + -z[69];
z[27] = z[0] * z[27];
z[55] = z[1] * z[62];
z[72] = z[7] * z[62];
z[77] = -z[12] + z[72];
z[55] = z[55] + -z[77];
z[78] = -z[10] + -z[55];
z[78] = z[33] * z[78];
z[27] = (T(377) * z[21]) / T(24) + z[27] + z[47] + z[53] + z[64] + T(12) * z[75] + z[78];
z[27] = z[6] * z[27];
z[35] = z[29] * z[35];
z[35] = -z[35] + T(2) * z[56];
z[43] = z[8] + z[43];
z[47] = T(3) * z[1];
z[53] = T(-2) * z[43] + -z[47];
z[53] = z[1] * z[53];
z[53] = -z[35] + z[53] + z[65] + -z[70];
z[53] = z[18] * z[53];
z[64] = -z[4] + z[8];
z[64] = z[30] * z[64];
z[70] = z[4] + -z[30] + -z[36];
z[70] = z[1] * z[70];
z[64] = -z[56] + z[64] + z[69] + z[70];
z[64] = z[16] * z[64];
z[69] = z[4] + z[8] / T(2);
z[34] = z[34] * z[69];
z[41] = z[1] + T(5) * z[7] + z[41] + -z[49];
z[41] = z[7] * z[41];
z[49] = (T(-9) * z[1]) / T(2) + T(-16) * z[4] + -z[8];
z[49] = z[1] * z[49];
z[34] = T(16) * z[10] + (T(35) * z[11]) / T(2) + z[34] + z[41] + z[49] + -z[71];
z[34] = z[0] * z[34];
z[37] = z[37] + z[73];
z[37] = z[1] * z[37];
z[41] = z[1] + -z[24] + z[30];
z[41] = z[41] * z[48];
z[37] = z[37] + z[41] + -z[65] + -z[66];
z[37] = z[14] * z[37];
z[41] = prod_pow(z[1], 2);
z[49] = prod_pow(z[7], 2);
z[41] = -z[10] + z[41] + -z[49];
z[41] = z[33] * z[41];
z[34] = (T(32) * z[21]) / T(3) + z[34] + z[37] + z[41] + z[53] + z[64] + T(11) * z[75];
z[34] = z[3] * z[34];
z[37] = -(z[25] * z[43]);
z[41] = z[4] + -z[68];
z[41] = z[7] + T(2) * z[41];
z[41] = z[7] * z[41];
z[35] = -z[35] + z[37] + z[41] + z[60] + z[67];
z[35] = z[0] * z[35];
z[22] = z[22] * z[29];
z[29] = z[4] + z[30];
z[36] = -(z[29] * z[36]);
z[22] = z[22] + -z[23] + z[36] + z[57];
z[22] = z[18] * z[22];
z[23] = z[45] + z[59];
z[23] = z[16] * z[23];
z[36] = z[7] + T(-2) * z[62];
z[36] = z[7] * z[36];
z[36] = z[36] + z[74];
z[33] = z[33] * z[36];
z[22] = (T(105) * z[21]) / T(4) + z[22] + z[23] + z[33] + z[35] + z[76];
z[22] = z[5] * z[22];
z[23] = (T(-5) * z[0]) / T(3) + (T(47) * z[14]) / T(6) + (T(13) * z[16]) / T(6) + (T(17) * z[18]) / T(2) + T(-5) * z[19];
z[23] = z[2] * z[23];
z[33] = z[14] + -z[16];
z[33] = (T(13) * z[0]) / T(2) + T(3) * z[18] + (T(-5) * z[33]) / T(3);
z[33] = z[3] * z[33];
z[23] = z[23] + z[33];
z[33] = (T(3) * z[0]) / T(2) + (T(-27) * z[16]) / T(4) + (T(-25) * z[18]) / T(4) + z[19];
z[33] = z[5] * z[33];
z[35] = T(-5) * z[2] + z[5];
z[35] = z[20] * z[35];
z[36] = (T(-29) * z[0]) / T(3) + (T(67) * z[14]) / T(3) + (T(-77) * z[16]) / T(3) + T(-27) * z[18] + T(21) * z[20];
z[36] = z[6] * z[36];
z[23] = z[23] / T(2) + z[33] + (T(3) * z[35]) / T(2) + z[36] / T(4) + (T(31) * z[46]) / T(12);
z[23] = z[23] * z[32];
z[33] = T(10) * z[10] + -z[58] + -z[71];
z[35] = -z[51] + -z[52];
z[35] = z[8] * z[35];
z[24] = z[1] + z[24];
z[24] = z[24] * z[25];
z[25] = -(z[31] * z[48]);
z[24] = T(4) * z[11] + z[24] + z[25] + z[33] + z[35];
z[24] = z[6] * z[24];
z[25] = -z[48] + z[62];
z[25] = z[25] * z[48];
z[31] = prod_pow(z[8], 2);
z[35] = (T(19) * z[1]) / T(2) + z[38];
z[35] = z[1] * z[35];
z[25] = (T(-17) * z[11]) / T(2) + z[25] + (T(-9) * z[31]) / T(2) + -z[33] + z[35];
z[25] = z[3] * z[25];
z[29] = T(2) * z[29] + -z[47];
z[29] = z[1] * z[29];
z[31] = z[1] + -z[8];
z[31] = z[31] * z[48];
z[29] = z[29] + z[31] + -z[54];
z[29] = T(-8) * z[11] + T(2) * z[29] + -z[60] + -z[63];
z[29] = -(z[29] * z[42]);
z[31] = z[3] + -z[6];
z[31] = (T(-13) * z[31]) / T(4) + (T(-5) * z[42]) / T(3);
z[31] = z[31] * z[32];
z[24] = z[24] + z[25] + T(2) * z[29] + z[31];
z[24] = z[17] * z[24];
z[25] = -z[30] + -z[61];
z[25] = z[1] * z[25];
z[29] = z[8] * z[38];
z[25] = z[25] + z[29] + -z[56] + z[63] + -z[72];
z[25] = z[16] * z[25];
z[29] = z[1] * z[4];
z[29] = z[29] + -z[56] + z[77];
z[29] = z[19] * z[29];
z[30] = z[1] * z[40];
z[31] = -(z[4] * z[8]);
z[30] = -z[12] + z[30] + z[31] + z[56];
z[30] = z[30] * z[39];
z[31] = z[44] * z[55];
z[25] = (T(-7) * z[21]) / T(8) + z[25] + z[29] + z[30] + z[31];
z[29] = T(-7) * z[14] + (T(9) * z[16]) / T(2) + z[18] + (T(5) * z[19]) / T(2) + (T(9) * z[20]) / T(4);
z[29] = z[29] * z[32];
z[25] = T(3) * z[25] + z[29];
z[25] = z[15] * z[25];
return z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[34] + z[50];
}



template IntegrandConstructorType<double> f_4_55_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_55_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_55_construct (const Kin<qd_real>&);
#endif

}