#include "f_4_170.h"

namespace PentagonFunctions {

template <typename T> T f_4_170_abbreviated (const std::array<T,29>&);

template <typename T> class SpDLog_f_4_170_W_10 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_170_W_10 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(4) + T(-2) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (-(prod_pow(abb[2], 2) * abb[3]) + prod_pow(abb[2], 2) * (abb[4] + abb[5] * T(-2)) + prod_pow(abb[1], 2) * (abb[3] + -abb[4] + abb[5] * T(2)));
    }
};
template <typename T> class SpDLog_f_4_170_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_170_W_12 (const Kin<T>& kin) {
        c[0] = -prod_pow(kin.v[4], 2) + kin.v[1] * (-kin.v[1] + T(2) * kin.v[4]) + rlog(kin.v[2]) * ((kin.v[1] / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((-kin.v[1] / T(4) + -kin.v[4] / T(2) + T(-1)) * kin.v[1] + ((T(3) * kin.v[4]) / T(4) + T(1)) * kin.v[4]);
c[1] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-kin.v[4] + kin.v[1]) + rlog(kin.v[2]) * (-kin.v[1] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[6] * (prod_pow(abb[1], 2) * abb[5] + abb[4] * (abb[8] + -prod_pow(abb[1], 2)) + prod_pow(abb[7], 2) * (abb[4] + -abb[5]) + -(abb[5] * abb[8]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_170_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl12 = DLog_W_12<T>(kin),dl5 = DLog_W_5<T>(kin),dl3 = DLog_W_3<T>(kin),dl9 = DLog_W_9<T>(kin),dl4 = DLog_W_4<T>(kin),dl14 = DLog_W_14<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),spdl10 = SpDLog_f_4_170_W_10<T>(kin),spdl12 = SpDLog_f_4_170_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,29> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[16] / kin_path.W[16]), dl12(t), rlog(-v_path[1]), f_2_1_4(kin_path), dl5(t), -rlog(t), dl3(t), f_2_1_7(kin_path), dl9(t), rlog(kin.W[0] / kin_path.W[0]), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(kin.W[15] / kin_path.W[15]), dl4(t), rlog(kin.W[8] / kin_path.W[8]), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl2(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl16(t), dl17(t), dl1(t)}
;

        auto result = f_4_170_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_170_abbreviated(const std::array<T,29>& abb)
{
using TR = typename T::value_type;
T z[53];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[11];
z[3] = abb[23];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[9];
z[7] = abb[10];
z[8] = abb[2];
z[9] = abb[27];
z[10] = abb[7];
z[11] = abb[28];
z[12] = abb[16];
z[13] = abb[26];
z[14] = bc<TR>[0];
z[15] = abb[8];
z[16] = abb[12];
z[17] = abb[13];
z[18] = abb[15];
z[19] = abb[21];
z[20] = abb[22];
z[21] = abb[24];
z[22] = abb[25];
z[23] = abb[14];
z[24] = abb[20];
z[25] = abb[17];
z[26] = abb[18];
z[27] = abb[19];
z[28] = bc<TR>[1];
z[29] = bc<TR>[2];
z[30] = bc<TR>[4];
z[31] = bc<TR>[9];
z[32] = z[23] + z[27];
z[33] = T(2) * z[25];
z[34] = z[1] + z[33];
z[32] = -z[32] / T(2) + z[34];
z[32] = z[13] * z[32];
z[35] = -z[1] + z[4];
z[36] = -z[5] + z[35] / T(2);
z[36] = z[2] * z[36];
z[32] = -z[32] + z[36];
z[37] = z[1] + -z[23];
z[33] = z[33] + z[37];
z[38] = (T(-2) * z[11]) / T(3) + z[17];
z[38] = z[33] * z[38];
z[39] = z[5] + -z[7];
z[39] = z[6] * z[39];
z[40] = -z[23] + z[25];
z[41] = z[24] * z[40];
z[42] = z[39] + T(7) * z[41];
z[43] = T(2) * z[13];
z[44] = -z[3] + z[11] + -z[43];
z[45] = z[28] * z[44];
z[46] = -z[25] + z[27];
z[46] = z[26] * z[46];
z[47] = z[1] + z[23];
z[48] = z[4] / T(2) + z[25] + z[47];
z[48] = -z[5] / T(2) + z[48] / T(3);
z[48] = z[3] * z[48];
z[49] = z[3] + (T(-9) * z[11]) / T(4) + (T(13) * z[13]) / T(4);
z[49] = z[29] * z[49];
z[50] = -z[1] + z[7] / T(3);
z[50] = z[4] / T(3) + -z[5] + z[50] / T(2);
z[50] = z[9] * z[50];
z[32] = -z[32] / T(3) + z[38] + z[42] / T(6) + (T(2) * z[45]) / T(3) + (T(13) * z[46]) / T(6) + z[48] + z[49] + z[50];
z[38] = prod_pow(z[14], 2);
z[32] = z[32] * z[38];
z[42] = z[41] + T(2) * z[46];
z[46] = -z[13] + z[17];
z[46] = z[33] * z[46];
z[40] = z[3] * z[40];
z[46] = -z[40] + z[42] + z[46];
z[46] = z[18] * z[46];
z[48] = z[30] * z[44];
z[46] = -z[46] + z[48];
z[48] = z[11] + -z[13];
z[49] = z[33] * z[48];
z[40] = -z[40] + -z[41] + z[49];
z[49] = -(z[20] * z[40]);
z[44] = z[33] * z[44];
z[50] = z[12] + -z[22];
z[50] = z[44] * z[50];
z[51] = -z[28] + T(2) * z[29];
z[45] = z[45] * z[51];
z[51] = -z[3] + (T(-5) * z[17]) / T(2);
z[51] = z[11] / T(2) + z[51] / T(3);
z[38] = z[38] * z[51];
z[48] = prod_pow(z[29], 2) * z[48];
z[38] = z[38] + z[45] + T(-2) * z[46] + -z[48] / T(2) + z[49] + z[50];
z[45] = int_to_imaginary<T>(1) * z[14];
z[38] = z[38] * z[45];
z[46] = z[3] / T(2);
z[48] = z[4] + -z[23];
z[48] = z[46] * z[48];
z[37] = z[25] + z[37] / T(2);
z[37] = z[11] * z[37];
z[41] = -z[36] + -z[37] + z[41] + z[48];
z[41] = z[10] * z[41];
z[45] = z[40] * z[45];
z[48] = z[12] * z[44];
z[49] = T(2) * z[5];
z[35] = -z[35] + z[49];
z[50] = T(2) * z[9];
z[51] = -z[2] + z[3] + z[50];
z[51] = z[35] * z[51];
z[52] = z[8] * z[51];
z[41] = z[41] + z[45] + z[48] + z[52];
z[41] = z[10] * z[41];
z[45] = -(z[46] * z[47]);
z[47] = -(z[17] * z[33]);
z[37] = z[37] + -z[42] + z[45] + z[47];
z[37] = z[18] * z[37];
z[37] = z[37] + -z[48];
z[37] = z[18] * z[37];
z[42] = -z[3] + z[17];
z[33] = z[33] * z[42];
z[34] = z[27] + -z[34];
z[34] = z[34] * z[43];
z[33] = z[33] + z[34];
z[33] = prod_pow(z[12], 2) * z[33];
z[34] = -(z[19] * z[40]);
z[40] = z[1] + z[4];
z[40] = z[40] * z[46];
z[36] = z[36] + T(-2) * z[39] + z[40];
z[36] = prod_pow(z[0], 2) * z[36];
z[39] = z[3] * z[35];
z[40] = z[1] + -z[7] + z[49];
z[40] = z[40] * z[50];
z[39] = z[39] + z[40];
z[39] = z[8] * z[39];
z[40] = -(z[0] * z[51]);
z[39] = z[39] + z[40];
z[39] = z[8] * z[39];
z[40] = z[16] * z[51];
z[42] = z[21] * z[44];
z[43] = z[2] + -z[9];
z[35] = z[35] * z[43];
z[43] = z[4] + -z[5];
z[43] = z[3] * z[43];
z[35] = z[35] + z[43];
z[35] = z[15] * z[35];
z[43] = (T(-7) * z[3]) / T(3) + (T(91) * z[11]) / T(12) + (T(-125) * z[13]) / T(4);
z[43] = (T(32) * z[9]) / T(3) + z[43] / T(2);
z[43] = z[31] * z[43];
return z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43];
}



template IntegrandConstructorType<double> f_4_170_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_170_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_170_construct (const Kin<qd_real>&);
#endif

}