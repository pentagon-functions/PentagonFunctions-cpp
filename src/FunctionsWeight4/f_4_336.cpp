#include "f_4_336.h"

namespace PentagonFunctions {

template <typename T> T f_4_336_abbreviated (const std::array<T,55>&);

template <typename T> class SpDLog_f_4_336_W_7 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_336_W_7 (const Kin<T>& kin) {
        c[0] = T(-3) * kin.v[1] * kin.v[4] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]);
c[1] = T(-3) * kin.v[1] * kin.v[4] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]);
c[2] = T(-3) * kin.v[1] * kin.v[4] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[1], 2) * ((abb[3] * T(-3)) / T(2) + (abb[4] * T(-3)) / T(2) + (abb[5] * T(-3)) / T(2)) + (prod_pow(abb[2], 2) * abb[3] * T(3)) / T(2) + prod_pow(abb[2], 2) * ((abb[4] * T(3)) / T(2) + (abb[5] * T(3)) / T(2)));
    }
};
template <typename T> class SpDLog_f_4_336_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_336_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (-kin.v[0] / T(2) + (T(-19) * kin.v[4]) / T(2) + T(-7) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4))) * kin.v[1] + kin.v[2] + kin.v[3]) + (kin.v[0] * kin.v[4]) / T(2) + -(kin.v[2] * kin.v[4]) + -(kin.v[3] * kin.v[4]) + bc<T>[0] * (T(-9) / T(2) + (T(-9) * kin.v[4]) / T(4)) * int_to_imaginary<T>(1) * kin.v[4] + kin.v[4] * (T(7) + T(8) * kin.v[4]);
c[1] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2))) * kin.v[1] + T(4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) * kin.v[4];
c[2] = -(kin.v[0] * kin.v[4]) / T(2) + kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + kin.v[4] * (T(-7) + T(-8) * kin.v[4]) + bc<T>[0] * (T(9) / T(2) + (T(9) * kin.v[4]) / T(4)) * int_to_imaginary<T>(1) * kin.v[4] + kin.v[1] * (kin.v[0] / T(2) + (T(19) * kin.v[4]) / T(2) + -kin.v[2] + -kin.v[3] + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + T(7) + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4))) * kin.v[1]);
c[3] = (bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[10] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]);
        }

        return abb[21] * (abb[4] * abb[22] * T(-4) + abb[12] * ((abb[4] * abb[11]) / T(2) + -(abb[2] * abb[4]) / T(2) + (abb[4] * abb[13] * T(-9)) / T(2) + abb[4] * abb[12] * T(-3) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) + abb[4] * abb[8] * abb[12] * T(4) + abb[10] * (abb[8] * abb[12] * T(-4) + abb[12] * (abb[2] / T(2) + -abb[11] / T(2) + (abb[13] * T(9)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[12] * T(3)) + abb[22] * T(4)) + abb[1] * ((abb[2] * abb[4]) / T(2) + abb[1] * (abb[10] / T(2) + -abb[4] / T(2)) + -(abb[4] * abb[11]) / T(2) + (abb[4] * abb[12] * T(7)) / T(2) + (abb[4] * abb[13] * T(9)) / T(2) + abb[4] * abb[8] * T(-4) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[10] * (abb[11] / T(2) + -abb[2] / T(2) + (abb[13] * T(-9)) / T(2) + (abb[12] * T(-7)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[8] * T(4))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_336_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl13 = DLog_W_13<T>(kin),dl6 = DLog_W_6<T>(kin),dl15 = DLog_W_15<T>(kin),dl25 = DLog_W_25<T>(kin),dl12 = DLog_W_12<T>(kin),dl30 = DLog_W_30<T>(kin),dl16 = DLog_W_16<T>(kin),dl31 = DLog_W_31<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl27 = DLog_W_27<T>(kin),spdl7 = SpDLog_f_4_336_W_7<T>(kin),spdl12 = SpDLog_f_4_336_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,55> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl13(t), rlog(v_path[0]), rlog(v_path[2]), dl6(t), f_1_3_1(kin) - f_1_3_1(kin_path), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_9(kin_path), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl25(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl12(t), f_2_1_4(kin_path), dl30(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), f_2_1_7(kin_path), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl16(t), dl31(t), dl20(t), f_2_1_1(kin_path), f_2_1_11(kin_path), dl4(t), dl17(t), dl2(t), dl19(t), dl3(t), dl1(t), dl18(t), dl5(t), dl26(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[17] / kin_path.W[17]), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_336_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_336_abbreviated(const std::array<T,55>& abb)
{
using TR = typename T::value_type;
T z[205];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[35];
z[3] = abb[38];
z[4] = abb[39];
z[5] = abb[40];
z[6] = abb[41];
z[7] = abb[42];
z[8] = abb[44];
z[9] = abb[45];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[10];
z[13] = abb[24];
z[14] = abb[46];
z[15] = abb[53];
z[16] = abb[54];
z[17] = abb[28];
z[18] = abb[29];
z[19] = abb[30];
z[20] = abb[31];
z[21] = abb[32];
z[22] = abb[48];
z[23] = abb[49];
z[24] = abb[50];
z[25] = abb[51];
z[26] = abb[2];
z[27] = abb[18];
z[28] = abb[43];
z[29] = abb[8];
z[30] = abb[11];
z[31] = abb[12];
z[32] = abb[13];
z[33] = bc<TR>[0];
z[34] = abb[15];
z[35] = abb[52];
z[36] = abb[7];
z[37] = abb[9];
z[38] = abb[6];
z[39] = abb[14];
z[40] = abb[16];
z[41] = abb[17];
z[42] = abb[19];
z[43] = abb[20];
z[44] = abb[22];
z[45] = abb[25];
z[46] = abb[26];
z[47] = abb[27];
z[48] = abb[36];
z[49] = abb[37];
z[50] = abb[47];
z[51] = abb[33];
z[52] = abb[23];
z[53] = abb[34];
z[54] = bc<TR>[1];
z[55] = bc<TR>[3];
z[56] = bc<TR>[5];
z[57] = bc<TR>[2];
z[58] = bc<TR>[4];
z[59] = bc<TR>[7];
z[60] = bc<TR>[8];
z[61] = bc<TR>[9];
z[62] = z[9] / T(2);
z[63] = z[4] / T(2);
z[64] = z[62] + -z[63];
z[65] = T(2) * z[27];
z[66] = z[64] + z[65];
z[67] = z[3] / T(2);
z[68] = z[6] / T(2);
z[69] = z[7] / T(2);
z[70] = (T(3) * z[28]) / T(2);
z[71] = z[2] + -z[66] + -z[67] + z[68] + -z[69] + -z[70];
z[71] = z[36] * z[71];
z[72] = z[28] / T(2);
z[73] = z[69] + z[72];
z[74] = -z[3] + z[6];
z[75] = z[2] / T(2);
z[76] = z[5] / T(2);
z[77] = z[27] + -z[64] + -z[73] + -z[74] + -z[75] + z[76];
z[77] = z[0] * z[77];
z[78] = -z[2] + z[5];
z[79] = (T(5) * z[6]) / T(2);
z[80] = -z[27] + z[78] + z[79];
z[81] = (T(3) * z[9]) / T(2);
z[82] = T(3) * z[4];
z[83] = (T(3) * z[8]) / T(2);
z[84] = -z[3] + z[28] + -z[69] + -z[80] + z[81] + -z[82] + -z[83];
z[84] = z[26] * z[84];
z[85] = -z[2] + z[28];
z[86] = z[7] + -z[9];
z[87] = -z[3] + z[86];
z[87] = -z[68] + -z[85] + z[87] / T(2);
z[88] = z[63] + -z[76];
z[89] = -z[87] + -z[88];
z[89] = z[29] * z[89];
z[90] = (T(5) * z[4]) / T(2) + z[83];
z[91] = -z[75] + z[90];
z[92] = z[27] + z[86] + z[91];
z[93] = z[6] / T(4);
z[94] = z[5] / T(4);
z[92] = z[28] + (T(-9) * z[37]) / T(4) + z[92] / T(2) + z[93] + z[94];
z[92] = z[30] * z[92];
z[95] = -z[68] + z[73];
z[96] = -z[27] + z[76];
z[97] = z[95] + -z[96];
z[98] = -(z[31] * z[97]);
z[99] = T(2) * z[26] + -z[29] + z[31];
z[100] = (T(5) * z[36]) / T(2) + z[99];
z[100] = z[37] * z[100];
z[101] = z[36] * z[76];
z[71] = z[71] + z[77] + z[84] + z[89] + z[92] + z[98] + z[100] + z[101];
z[71] = z[30] * z[71];
z[77] = z[6] + -z[28];
z[84] = T(3) * z[5];
z[89] = T(3) * z[7];
z[92] = -z[77] + z[84] + -z[89];
z[92] = z[31] * z[92];
z[98] = z[8] + -z[9];
z[102] = z[98] / T(2);
z[103] = -z[4] + z[72];
z[104] = z[7] + -z[76] + z[102] + -z[103];
z[104] = z[29] * z[104];
z[105] = -z[4] + z[68];
z[106] = z[7] + z[98];
z[107] = -z[28] + z[105] + -z[106] / T(2);
z[107] = z[36] * z[107];
z[108] = z[7] + -z[98];
z[105] = -z[5] + z[105] + z[108] / T(2);
z[105] = z[0] * z[105];
z[108] = z[8] / T(2);
z[109] = z[96] + z[108];
z[110] = -z[62] + z[109];
z[111] = -z[6] + z[103] + -z[110];
z[112] = -(z[26] * z[111]);
z[97] = -z[37] + z[97];
z[97] = z[30] * z[97];
z[113] = z[5] + -z[7];
z[114] = z[32] / T(2);
z[115] = -(z[113] * z[114]);
z[116] = z[36] * z[37];
z[92] = z[92] / T(2) + z[97] + z[104] + z[105] + z[107] + z[112] + z[115] + z[116];
z[97] = T(3) * z[32];
z[92] = z[92] * z[97];
z[104] = z[6] + z[9];
z[105] = (T(7) * z[7]) / T(4);
z[107] = z[4] / T(4);
z[112] = (T(5) * z[3]) / T(2);
z[115] = (T(3) * z[8]) / T(4);
z[117] = (T(3) * z[2]) / T(4) + z[5] + -z[104] + -z[105] + z[107] + z[112] + -z[115];
z[117] = z[0] * z[117];
z[118] = -z[63] + z[83];
z[119] = T(2) * z[9];
z[120] = z[118] + z[119];
z[121] = -z[112] + z[120];
z[122] = (T(3) * z[7]) / T(2);
z[123] = -z[2] + -z[72] + z[79] + z[121] + z[122];
z[123] = z[31] * z[123];
z[117] = z[117] + z[123];
z[117] = z[0] * z[117];
z[87] = z[63] + z[87];
z[87] = z[36] * z[87];
z[123] = (T(5) * z[2]) / T(2);
z[124] = (T(5) * z[28]) / T(2);
z[120] = -z[120] + z[123] + -z[124];
z[125] = (T(3) * z[5]) / T(2);
z[126] = z[3] + z[68] + z[120] + -z[125];
z[126] = z[31] * z[126];
z[127] = -z[2] + z[76];
z[128] = -z[3] + z[95] + -z[127];
z[128] = z[0] * z[128];
z[129] = -z[7] + z[78];
z[130] = -z[4] + z[28];
z[131] = z[9] + z[130];
z[129] = T(7) * z[129] + z[131];
z[129] = z[29] * z[129];
z[87] = z[87] + -z[101] + z[126] + z[128] + z[129] / T(4);
z[87] = z[29] * z[87];
z[126] = T(4) * z[28];
z[128] = (T(3) * z[6]) / T(2);
z[90] = -z[9] + -z[67] + z[90] + z[122] + z[126] + -z[128];
z[90] = z[39] * z[90];
z[129] = T(4) * z[6];
z[132] = z[9] + (T(7) * z[27]) / T(2);
z[91] = -z[70] + z[91] + z[125] + z[129] + -z[132];
z[133] = z[42] * z[91];
z[134] = (T(3) * z[2]) / T(2);
z[135] = z[3] + z[64] + z[124] + -z[128] + z[134];
z[136] = prod_pow(z[36], 2);
z[135] = z[135] * z[136];
z[73] = z[73] + z[80] + z[121];
z[80] = z[0] + -z[31];
z[73] = z[73] * z[80];
z[95] = -z[2] + z[3] + z[65] + z[95];
z[95] = z[36] * z[95];
z[121] = (T(3) * z[26]) / T(2);
z[137] = z[4] + -z[9];
z[138] = -z[27] + z[137];
z[138] = z[121] * z[138];
z[73] = z[73] + z[95] + -z[101] + z[138];
z[73] = z[26] * z[73];
z[95] = z[15] + -z[16];
z[138] = T(2) * z[14];
z[139] = z[35] / T(2);
z[140] = -z[95] + z[138] + -z[139];
z[140] = z[50] * z[140];
z[141] = z[3] + -z[7];
z[120] = z[68] + -z[120] + -z[141];
z[120] = z[36] * z[120];
z[142] = (T(3) * z[31]) / T(2);
z[143] = -(z[77] * z[142]);
z[120] = z[101] + z[120] + z[143];
z[120] = z[31] * z[120];
z[143] = z[8] + z[9];
z[144] = -z[74] + -z[78] + -z[143];
z[144] = z[49] * z[144];
z[145] = -z[74] + z[113] + z[137];
z[145] = z[44] * z[145];
z[146] = z[45] * z[113];
z[147] = z[46] * z[77];
z[147] = z[146] + z[147];
z[148] = prod_pow(z[29], 2);
z[148] = -z[136] + z[148];
z[149] = (T(3) * z[38]) / T(2);
z[148] = z[148] * z[149];
z[99] = z[36] * z[99];
z[149] = z[136] / T(4);
z[99] = (T(7) * z[39]) / T(2) + z[99] + z[149];
z[99] = z[37] * z[99];
z[150] = z[141] + -z[143];
z[151] = -z[85] + z[150];
z[152] = (T(3) * z[48]) / T(2);
z[151] = z[151] * z[152];
z[153] = z[78] + z[130];
z[154] = -z[86] + z[153];
z[154] = (T(7) * z[154]) / T(2);
z[155] = z[40] * z[154];
z[71] = z[71] + z[73] + z[87] + z[90] + z[92] + -z[99] + z[117] + z[120] + z[133] + z[135] / T(2) + z[140] + (T(3) * z[144]) / T(2) + (T(7) * z[145]) / T(2) + T(-3) * z[147] + z[148] + z[151] + z[155];
z[71] = z[1] * z[71];
z[73] = (T(3) * z[51]) / T(4);
z[87] = (T(3) * z[6]) / T(4);
z[90] = -z[27] + z[87];
z[92] = z[73] + -z[90] + z[122];
z[117] = (T(7) * z[8]) / T(2);
z[120] = T(3) * z[9];
z[133] = (T(3) * z[4]) / T(4);
z[135] = -z[70] + z[92] + z[94] + -z[117] + -z[120] + z[133];
z[135] = z[31] * z[135];
z[140] = (T(9) * z[2]) / T(4);
z[144] = -z[87] + z[140];
z[145] = z[27] + z[144];
z[147] = (T(5) * z[4]) / T(4);
z[151] = -z[70] + z[147];
z[155] = T(2) * z[3];
z[156] = (T(9) * z[9]) / T(4) + -z[155];
z[157] = (T(5) * z[5]) / T(2);
z[158] = T(3) * z[8];
z[159] = -z[145] + -z[151] + z[156] + z[157] + z[158];
z[159] = z[0] * z[159];
z[160] = (T(5) * z[28]) / T(4);
z[161] = (T(15) * z[8]) / T(4) + z[160];
z[162] = (T(9) * z[37]) / T(2);
z[163] = (T(5) * z[9]) / T(2);
z[164] = -z[27] + z[94];
z[165] = (T(33) * z[4]) / T(4) + z[144] + -z[161] + -z[162] + -z[163] + z[164];
z[166] = z[30] / T(2);
z[165] = z[165] * z[166];
z[167] = z[65] + -z[83];
z[168] = T(2) * z[4];
z[169] = z[9] + -z[28] + -z[67] + z[167] + -z[168];
z[169] = z[36] * z[169];
z[115] = z[115] + z[164];
z[170] = T(4) * z[4];
z[156] = (T(9) * z[28]) / T(4) + z[115] + -z[156] + -z[170];
z[156] = z[26] * z[156];
z[171] = z[3] + z[8];
z[172] = -z[4] + z[5];
z[173] = z[171] + -z[172];
z[174] = z[29] / T(2);
z[173] = z[173] * z[174];
z[135] = z[100] + -z[101] + z[135] + z[156] + z[159] + z[165] + z[169] + z[173];
z[135] = z[30] * z[135];
z[156] = -z[69] + z[93];
z[159] = z[51] / T(4);
z[107] = z[107] + -z[159];
z[83] = z[28] + -z[37] + z[62] + z[83] + z[107] + z[156] + z[164];
z[83] = z[30] * z[83];
z[164] = z[8] + T(7) * z[9] + -z[89];
z[103] = z[76] + z[93] + -z[103] + z[164] / T(4);
z[103] = z[31] * z[103];
z[164] = T(5) * z[9];
z[165] = z[8] + z[89] + -z[164];
z[169] = z[4] + z[5];
z[165] = -z[93] + z[165] / T(4) + -z[169];
z[165] = z[0] * z[165];
z[173] = z[9] / T(4);
z[175] = z[28] / T(4);
z[176] = z[115] + -z[173] + z[175];
z[177] = -(z[26] * z[176]);
z[178] = T(3) * z[28];
z[179] = z[120] + -z[178];
z[180] = z[6] + -z[8];
z[181] = z[51] + z[82] + z[179] + -z[180];
z[181] = -z[5] + z[181] / T(4);
z[114] = z[114] * z[181];
z[181] = z[5] + z[28] + -z[143];
z[181] = z[174] * z[181];
z[182] = -z[4] + -z[28] + z[86] / T(2);
z[182] = z[36] * z[182];
z[83] = z[83] + z[103] + z[114] + z[116] + z[165] + z[177] + z[181] + z[182];
z[83] = z[83] * z[97];
z[103] = z[76] + z[155];
z[114] = T(2) * z[8] + -z[134];
z[165] = z[114] + z[122];
z[177] = -z[70] + z[81];
z[181] = (T(7) * z[4]) / T(2);
z[182] = -z[103] + z[165] + -z[177] + -z[181];
z[182] = z[31] * z[182];
z[120] = -z[8] + z[120];
z[103] = -z[70] + z[103] + z[120] / T(2) + z[168];
z[103] = z[0] * z[103];
z[120] = z[5] * z[36];
z[168] = -z[4] + -z[171];
z[168] = z[36] * z[168];
z[168] = z[120] + z[168];
z[153] = z[86] + z[153];
z[171] = z[29] * z[153];
z[103] = z[103] + z[168] / T(2) + (T(-3) * z[171]) / T(4) + z[182];
z[103] = z[29] * z[103];
z[168] = T(5) * z[3];
z[171] = T(3) * z[2];
z[182] = -z[6] + -z[84] + -z[158] + -z[164] + z[168] + z[171];
z[183] = (T(3) * z[49]) / T(4);
z[182] = z[182] * z[183];
z[155] = -z[155] + z[177];
z[184] = -z[8] + z[63] + -z[122] + z[134] + -z[155];
z[184] = z[36] * z[184];
z[185] = T(7) * z[4] + z[7] + -z[158] + z[164];
z[186] = (T(3) * z[5]) / T(4);
z[159] = -z[28] + -z[159] + z[185] / T(4) + z[186];
z[142] = z[142] * z[159];
z[142] = -z[101] + z[142] + z[184];
z[142] = z[31] * z[142];
z[159] = -z[76] + z[108];
z[184] = (T(7) * z[3]) / T(2);
z[179] = z[159] + -z[179] + -z[181] + -z[184];
z[179] = z[31] * z[179];
z[181] = T(-9) * z[7] + z[8];
z[181] = (T(9) * z[2]) / T(2) + T(7) * z[3] + z[5] + -z[63] + z[181] / T(2);
z[185] = z[0] / T(2);
z[181] = z[181] * z[185];
z[179] = z[179] + z[181];
z[179] = z[179] * z[185];
z[181] = (T(11) * z[28]) / T(2);
z[112] = z[112] + -z[122] + z[137] + z[181];
z[112] = z[39] * z[112];
z[115] = (T(-7) * z[3]) / T(4) + -z[115] + z[147] + z[177];
z[115] = z[80] * z[115];
z[137] = z[155] + -z[167] + z[170];
z[137] = z[36] * z[137];
z[147] = z[5] + -z[27];
z[155] = z[8] + z[147];
z[155] = z[121] * z[155];
z[101] = z[101] + z[115] + z[137] + -z[155];
z[101] = z[26] * z[101];
z[115] = (T(9) * z[7]) / T(2);
z[137] = -z[8] + (T(15) * z[9]) / T(2) + -z[67] + -z[115] + z[128] + (T(11) * z[169]) / T(2);
z[167] = z[44] / T(2);
z[137] = z[137] * z[167];
z[169] = z[8] + z[28];
z[170] = (T(15) * z[4]) / T(4);
z[144] = (T(13) * z[5]) / T(4) + -z[132] + -z[144] + (T(19) * z[169]) / T(4) + -z[170];
z[169] = -(z[42] * z[144]);
z[158] = z[9] + z[158];
z[177] = -z[51] + z[77];
z[187] = -z[82] + z[158] + z[177];
z[188] = (T(3) * z[46]) / T(4);
z[189] = -(z[187] * z[188]);
z[158] = z[3] + -z[134] + z[158] / T(2);
z[190] = T(2) * z[28];
z[158] = -z[4] + z[158] / T(2) + z[190];
z[158] = z[136] * z[158];
z[150] = -z[2] + -z[28] + -z[150];
z[150] = z[150] * z[152];
z[152] = (T(3) * z[153]) / T(2);
z[153] = -(z[40] * z[152]);
z[191] = z[51] + z[113];
z[192] = -z[4] + z[143] + -z[191];
z[192] = z[45] * z[192];
z[138] = -z[16] / T(2) + -z[138];
z[138] = z[50] * z[138];
z[83] = z[83] + -z[99] + z[101] + z[103] + z[112] + z[135] + z[137] + z[138] + z[142] + z[150] + z[153] + z[158] + z[169] + z[179] + z[182] + z[189] + (T(3) * z[192]) / T(4);
z[83] = z[10] * z[83];
z[101] = (T(25) * z[8]) / T(4);
z[103] = (T(9) * z[4]) / T(4);
z[112] = (T(21) * z[2]) / T(4) + z[103];
z[135] = (T(13) * z[28]) / T(4) + z[101] + -z[112];
z[137] = (T(7) * z[5]) / T(4);
z[90] = -z[9] + -z[90] + z[115] + z[135] + z[137] + -z[162];
z[90] = z[90] * z[166];
z[138] = (T(9) * z[3]) / T(4);
z[142] = z[103] + z[138] + z[171];
z[150] = T(2) * z[6];
z[153] = z[142] + -z[150];
z[158] = (T(5) * z[8]) / T(2);
z[162] = (T(11) * z[9]) / T(4) + z[158];
z[169] = (T(-7) * z[7]) / T(2) + -z[65] + z[153] + -z[160] + -z[162];
z[169] = z[36] * z[169];
z[179] = -z[93] + z[175];
z[182] = -z[27] + z[137];
z[189] = z[34] / T(2);
z[192] = -z[105] + z[179] + z[182] + z[189];
z[192] = z[31] * z[192];
z[140] = z[103] + z[140];
z[193] = T(3) * z[3];
z[194] = T(2) * z[5] + z[190] + -z[193];
z[195] = z[7] + -z[27];
z[196] = (T(-7) * z[6]) / T(4) + z[140] + -z[162] + -z[194] + -z[195];
z[196] = z[0] * z[196];
z[197] = z[150] + -z[193];
z[198] = z[171] + z[197];
z[199] = (T(11) * z[5]) / T(4);
z[195] = z[195] + z[199];
z[200] = (T(15) * z[9]) / T(4);
z[161] = -z[161] + -z[195] + z[198] + z[200];
z[161] = z[26] * z[161];
z[201] = (T(7) * z[28]) / T(4);
z[202] = T(5) * z[8] + (T(11) * z[9]) / T(2);
z[153] = T(-2) * z[7] + z[153] + -z[201] + -z[202] / T(2);
z[203] = -z[5] + z[189];
z[204] = -z[153] + -z[203];
z[204] = z[29] * z[204];
z[90] = z[90] + z[100] + z[120] + z[161] + z[169] + z[192] + z[196] + z[204];
z[90] = z[30] * z[90];
z[100] = (T(13) * z[8]) / T(4);
z[140] = z[100] + z[119] + -z[140];
z[150] = z[150] + z[193];
z[161] = (T(7) * z[34]) / T(2);
z[126] = z[5] + z[126] + -z[161];
z[105] = -z[105] + -z[126] + -z[140] + z[150];
z[105] = z[31] * z[105];
z[169] = T(4) * z[34];
z[192] = z[169] + -z[190];
z[150] = -z[113] + -z[150] + z[171] + -z[192];
z[150] = z[0] * z[150];
z[153] = z[153] + z[189];
z[153] = z[36] * z[153];
z[196] = (T(9) * z[4]) / T(2);
z[122] = (T(21) * z[2]) / T(2) + -z[122] + z[196];
z[204] = -z[122] + z[181] + z[202];
z[204] = -z[34] + z[199] + z[204] / T(2);
z[204] = z[174] * z[204];
z[105] = z[105] + -z[120] + z[150] + z[153] + z[204];
z[105] = z[29] * z[105];
z[100] = z[100] + z[119] + -z[142];
z[129] = z[7] + z[100] + z[129] + z[137] + z[192];
z[129] = z[31] * z[129];
z[142] = (T(3) * z[2]) / T(8);
z[104] = (T(9) * z[4]) / T(8) + (T(-11) * z[7]) / T(8) + (T(-13) * z[8]) / T(8) + -z[104] + z[138] + z[142] + -z[186];
z[104] = z[0] * z[104];
z[104] = z[104] + z[129];
z[104] = z[0] * z[104];
z[100] = z[6] + z[100] + z[190] + z[195];
z[100] = z[80] * z[100];
z[129] = z[7] + z[65] + z[190] + -z[198];
z[129] = z[36] * z[129];
z[138] = z[98] + z[147];
z[121] = z[121] * z[138];
z[100] = z[100] + -z[120] + z[121] + z[129];
z[100] = z[26] * z[100];
z[121] = -z[28] + z[189];
z[129] = (T(11) * z[7]) / T(4) + -z[121] + z[140] + z[197];
z[129] = z[36] * z[129];
z[138] = z[7] / T(4) + -z[34] + -z[77] + -z[94];
z[140] = T(3) * z[31];
z[138] = z[138] * z[140];
z[129] = z[120] + z[129] + z[138];
z[129] = z[31] * z[129];
z[138] = (T(3) * z[28]) / T(4);
z[101] = (T(-21) * z[3]) / T(4) + (T(13) * z[6]) / T(4) + (T(25) * z[7]) / T(4) + -z[9] + z[101] + -z[103] + -z[138];
z[101] = z[39] * z[101];
z[150] = z[74] + -z[78] + -z[98];
z[150] = z[150] * z[183];
z[153] = -z[8] + (T(23) * z[9]) / T(2);
z[183] = (T(21) * z[3]) / T(2) + (T(-23) * z[6]) / T(2) + (T(-11) * z[7]) / T(2) + -z[125] + -z[153] + z[196];
z[167] = z[167] * z[183];
z[135] = (T(25) * z[5]) / T(4) + -z[87] + -z[132] + z[135];
z[183] = z[42] * z[135];
z[122] = (T(23) * z[28]) / T(2) + -z[122] + z[153];
z[122] = z[122] / T(2) + -z[169] + z[199];
z[153] = z[40] * z[122];
z[195] = -z[16] + z[35];
z[197] = T(3) * z[15];
z[195] = T(6) * z[14] + T(-2) * z[195] + -z[197];
z[195] = z[50] * z[195];
z[198] = z[134] + z[196];
z[199] = T(-9) * z[3] + (T(13) * z[6]) / T(2) + T(5) * z[7] + -z[198] + z[202];
z[149] = z[149] * z[199];
z[199] = (T(3) * z[46]) / T(2);
z[199] = -(z[77] * z[199]);
z[202] = z[3] + z[85] + -z[106];
z[202] = z[48] * z[202];
z[90] = z[90] + -z[99] + z[100] + z[101] + z[104] + z[105] + z[129] + (T(-3) * z[146]) / T(2) + z[149] + z[150] + z[153] + z[167] + z[183] + z[195] + z[199] + (T(3) * z[202]) / T(4);
z[90] = z[12] * z[90];
z[91] = -(z[43] * z[91]);
z[99] = -z[27] + z[128];
z[100] = -z[76] + z[99];
z[101] = -z[69] + z[72] + -z[100];
z[101] = z[31] * z[101];
z[64] = z[64] + -z[75] + z[99];
z[64] = z[0] * z[64];
z[75] = T(3) * z[37];
z[104] = (T(5) * z[27]) / T(2) + -z[75];
z[105] = z[79] + -z[104] + -z[178];
z[105] = z[30] * z[105];
z[129] = z[29] + -z[36];
z[146] = T(3) * z[38];
z[129] = z[129] * z[146];
z[66] = (T(-7) * z[2]) / T(2) + z[66] + z[128];
z[66] = z[36] * z[66];
z[146] = T(3) * z[29];
z[131] = z[131] * z[146];
z[149] = T(3) * z[26];
z[111] = -(z[111] * z[149]);
z[150] = -(z[41] * z[154]);
z[113] = z[6] + -z[27] + z[113];
z[113] = z[97] * z[113];
z[153] = T(3) * z[116];
z[64] = z[64] + z[66] + z[91] + z[101] + z[105] + z[111] + z[113] + -z[129] + z[131] + z[150] + -z[153];
z[64] = z[1] * z[64];
z[66] = z[65] + z[178];
z[91] = z[66] + -z[108];
z[101] = (T(5) * z[9]) / T(4);
z[105] = (T(15) * z[2]) / T(4);
z[108] = (T(-9) * z[6]) / T(4) + -z[91] + -z[101] + z[105] + z[170] + z[189];
z[108] = z[36] * z[108];
z[111] = z[84] + z[158];
z[113] = z[171] + z[196];
z[131] = -z[62] + -z[99] + -z[111] + z[113] + -z[169];
z[131] = z[0] * z[131];
z[150] = z[5] + z[8];
z[150] = T(4) * z[150];
z[113] = -z[113] + z[128] + -z[132] + z[150] + z[181];
z[128] = z[43] * z[113];
z[132] = (T(9) * z[7]) / T(4);
z[73] = z[73] + z[100] + -z[132] + -z[133];
z[100] = z[73] + -z[160] + z[161];
z[100] = z[31] * z[100];
z[154] = z[2] + z[4];
z[115] = z[8] + -z[115] + (T(15) * z[154]) / T(2);
z[154] = z[9] + z[28];
z[154] = -z[115] + (T(17) * z[154]) / T(2);
z[167] = (T(17) * z[5]) / T(4);
z[154] = z[154] / T(2) + z[167] + -z[169];
z[170] = z[41] * z[154];
z[178] = -z[28] + z[34];
z[181] = -z[9] + z[178];
z[181] = z[146] * z[181];
z[110] = z[72] + z[110];
z[183] = -(z[110] * z[149]);
z[147] = T(-5) * z[147] + -z[178];
z[147] = z[147] * z[166];
z[100] = z[100] + z[108] + z[128] + z[129] + z[131] + z[147] + z[170] + z[181] + z[183];
z[100] = z[11] * z[100];
z[108] = (T(3) * z[7]) / T(4);
z[128] = (T(11) * z[28]) / T(4);
z[129] = (T(15) * z[6]) / T(4);
z[131] = z[108] + z[128] + -z[129] + -z[161] + -z[182];
z[131] = z[31] * z[131];
z[147] = z[89] + -z[189];
z[112] = -z[112] + z[162];
z[66] = z[66] + z[87] + z[112] + z[147];
z[66] = z[36] * z[66];
z[84] = -z[27] + z[84] + z[112] + z[129] + z[169];
z[84] = z[0] * z[84];
z[104] = -z[72] + -z[104] + -z[147] + z[157];
z[104] = z[30] * z[104];
z[112] = (T(5) * z[5]) / T(4) + -z[27];
z[101] = (T(-5) * z[8]) / T(4) + z[101] + -z[112] + z[175];
z[129] = -(z[101] * z[149]);
z[122] = -(z[41] * z[122]);
z[135] = -(z[43] * z[135]);
z[147] = -z[98] + -z[178];
z[147] = z[146] * z[147];
z[66] = z[66] + z[84] + z[104] + z[122] + z[129] + z[131] + z[135] + z[147] + -z[153];
z[66] = z[12] * z[66];
z[84] = T(6) * z[9] + -z[70] + -z[92] + z[103] + z[158] + z[186];
z[84] = z[31] * z[84];
z[92] = z[9] + z[27];
z[75] = z[75] + -z[82] + (T(5) * z[92]) / T(2) + -z[124] + -z[157];
z[75] = z[30] * z[75];
z[104] = -z[111] + -z[133] + z[145] + -z[200];
z[104] = z[0] * z[104];
z[111] = z[43] * z[144];
z[91] = -z[81] + -z[91] + z[198];
z[91] = z[36] * z[91];
z[122] = -(z[149] * z[176]);
z[129] = z[8] / T(4);
z[131] = (T(3) * z[9]) / T(4) + z[27] + -z[129] + -z[138] + -z[186];
z[131] = z[97] * z[131];
z[135] = z[41] * z[152];
z[75] = z[75] + z[84] + z[91] + z[104] + z[111] + z[122] + z[131] + z[135] + -z[153];
z[75] = z[10] * z[75];
z[84] = -z[18] + z[20];
z[91] = z[19] + z[84];
z[104] = z[22] / T(4) + z[23] + (T(-5) * z[24]) / T(4) + -z[25];
z[111] = z[13] + -z[21];
z[122] = -z[111] / T(2);
z[131] = z[17] / T(4);
z[91] = (T(3) * z[91]) / T(4) + -z[104] + -z[122] + z[131];
z[135] = z[14] * z[91];
z[144] = z[16] * z[91];
z[135] = z[135] + z[144];
z[145] = z[35] * z[91];
z[145] = -z[135] + z[145];
z[145] = z[43] * z[145];
z[147] = -z[84] / T(2);
z[122] = z[17] + -z[19] + -z[122] + z[147];
z[152] = z[14] + z[52];
z[152] = z[122] * z[152];
z[153] = z[35] * z[122];
z[157] = z[4] + -z[98] + z[177];
z[158] = z[11] / T(2);
z[161] = z[157] * z[158];
z[162] = z[16] * z[122];
z[169] = z[12] * z[77];
z[153] = z[152] + -z[153] + -z[161] + -z[162] + z[169];
z[161] = z[10] * z[187];
z[161] = -z[153] + -z[161] / T(2);
z[169] = -(z[1] * z[77]);
z[161] = z[161] / T(2) + z[169];
z[161] = z[47] * z[161];
z[145] = z[145] + z[161];
z[161] = z[0] * z[91];
z[169] = z[17] + z[111];
z[147] = -z[19] / T(4) + z[104] + z[147] + (T(-3) * z[169]) / T(4);
z[169] = z[36] * z[147];
z[170] = -z[161] + z[169];
z[176] = z[31] * z[91];
z[177] = z[170] + z[176];
z[181] = T(-4) * z[58] + z[177];
z[182] = prod_pow(z[57], 2);
z[183] = (T(7) * z[182]) / T(4);
z[186] = (T(-15) * z[54]) / T(2) + T(7) * z[57];
z[186] = z[54] * z[186];
z[181] = T(3) * z[181] + -z[183] + z[186];
z[181] = z[35] * z[181];
z[186] = z[43] * z[91];
z[187] = z[41] * z[147];
z[182] = (T(5) * z[182]) / T(4) + z[187];
z[187] = (T(-11) * z[54]) / T(4) + T(3) * z[57];
z[187] = z[54] * z[187];
z[186] = (T(-9) * z[58]) / T(2) + z[170] + -z[182] + z[186] + z[187];
z[186] = z[186] * z[197];
z[187] = (T(3) * z[34]) / T(2);
z[129] = -z[112] + z[129] + -z[160] + -z[173] + z[187];
z[129] = z[11] * z[129];
z[160] = z[122] / T(2);
z[195] = -(z[16] * z[160]);
z[187] = -z[27] + z[28] + z[125] + -z[187];
z[187] = z[12] * z[187];
z[129] = z[129] + z[187] + z[195];
z[129] = z[97] * z[129];
z[187] = z[7] + z[85];
z[195] = z[8] / T(5) + z[173];
z[196] = -z[53] / T(5) + z[74] / T(10) + z[172] / T(20) + (T(3) * z[187]) / T(20) + z[195];
z[197] = prod_pow(z[54], 2);
z[196] = z[196] * z[197];
z[198] = T(3) * z[91];
z[199] = z[26] * z[198];
z[200] = z[54] / T(2);
z[202] = z[57] + z[200];
z[202] = z[54] * z[202];
z[183] = z[58] + z[183] + z[199] + z[202];
z[183] = z[16] * z[183];
z[199] = z[31] * z[122];
z[170] = -z[170] + z[199] / T(2);
z[202] = T(3) * z[54] + T(-5) * z[57];
z[200] = z[200] * z[202];
z[182] = T(2) * z[58] + z[170] + z[182] + z[200];
z[200] = T(3) * z[14];
z[182] = z[182] * z[200];
z[202] = (T(3) * z[52]) / T(2);
z[204] = z[199] * z[202];
z[64] = z[64] + z[66] + z[75] + z[100] + z[129] + T(3) * z[145] + z[181] + z[182] + z[183] + z[186] + (T(3) * z[196]) / T(2) + z[204];
z[64] = int_to_imaginary<T>(1) * z[64];
z[66] = z[34] / T(3);
z[75] = T(11) * z[27];
z[100] = (T(31) * z[9]) / T(2) + z[75];
z[100] = T(-13) * z[8] + z[100] / T(2);
z[72] = (T(7) * z[2]) / T(4) + (T(-7) * z[5]) / T(2) + -z[66] + z[72] + z[100] / T(3) + z[133] + z[156];
z[72] = z[12] * z[72];
z[100] = z[54] + -z[57];
z[100] = z[91] + -z[100] / T(4);
z[100] = z[15] * z[100];
z[129] = (T(9) * z[54]) / T(4) + (T(-5) * z[57]) / T(6) + z[147];
z[129] = z[14] * z[129];
z[85] = (T(-31) * z[4]) / T(20) + (T(71) * z[5]) / T(20) + (T(53) * z[7]) / T(20) + (T(-31) * z[53]) / T(5) + (T(41) * z[74]) / T(10) + (T(73) * z[85]) / T(20) + T(31) * z[195];
z[85] = -z[16] / T(4) + z[85] / T(3);
z[85] = (T(9) * z[15]) / T(4) + (T(11) * z[35]) / T(4) + z[85] / T(3);
z[85] = int_to_imaginary<T>(1) * z[33] * z[85];
z[72] = z[72] + z[85] + z[100] + z[129];
z[85] = (T(7) * z[9]) / T(2);
z[75] = T(11) * z[8] + -z[75] + -z[85];
z[75] = -z[67] + z[69] + z[75] / T(3);
z[66] = -z[2] + z[66] + z[75] / T(2) + z[93] + z[137] + -z[151];
z[66] = z[66] * z[158];
z[75] = (T(13) * z[54]) / T(12) + (T(3) * z[57]) / T(4) + -z[91];
z[75] = z[16] * z[75];
z[84] = (T(5) * z[19]) / T(4) + (T(-7) * z[54]) / T(12) + -z[57] + z[84] + -z[104] + z[111] / T(4) + -z[131];
z[84] = z[84] * z[139];
z[100] = z[9] + (T(-11) * z[27]) / T(4);
z[93] = (T(-3) * z[3]) / T(8) + (T(13) * z[5]) / T(24) + -z[7] / T(8) + z[28] / T(6) + z[93] + z[100] / T(3) + z[118] + -z[142];
z[93] = z[10] * z[93];
z[92] = -z[8] + (T(11) * z[92]) / T(6);
z[67] = z[2] / T(12) + (T(-17) * z[4]) / T(12) + (T(-5) * z[5]) / T(6) + z[7] / T(3) + -z[67] + (T(-2) * z[77]) / T(3) + z[92] / T(2);
z[67] = z[1] * z[67];
z[66] = z[66] + z[67] + z[72] / T(2) + z[75] + z[84] + z[93];
z[66] = z[33] * z[66];
z[67] = z[4] + (T(-7) * z[6]) / T(2) + T(4) * z[53] + -z[70] + z[134] + -z[150] + -z[164] + z[184];
z[67] = z[55] * z[67];
z[64] = z[64] + z[66] + z[67];
z[64] = z[33] * z[64];
z[66] = (T(3) * z[4]) / T(2);
z[67] = -z[66] + z[99] + z[117] + z[163] + -z[171] + z[194];
z[67] = z[0] * z[67];
z[72] = -z[9] + z[27];
z[75] = -z[72] / T(2) + -z[87] + z[94] + z[103] + -z[114] + -z[128];
z[75] = z[30] * z[75];
z[73] = -z[73] + -z[189] + -z[201];
z[73] = z[31] * z[73];
z[77] = (T(3) * z[3]) / T(4) + -z[8] + -z[133] + z[173];
z[65] = z[65] + z[77] + -z[175];
z[65] = z[36] * z[65];
z[82] = z[82] + -z[193];
z[84] = (T(7) * z[28]) / T(2) + -z[82] + -z[85] + z[109];
z[84] = z[26] * z[84];
z[77] = z[77] + z[175];
z[85] = -z[77] + z[203];
z[85] = z[29] * z[85];
z[65] = z[65] + z[67] + z[73] + z[75] + z[84] + z[85] + -z[120];
z[65] = z[30] * z[65];
z[67] = (T(11) * z[8]) / T(4) + T(4) * z[9] + -z[105] + z[132] + -z[133] + -z[193];
z[73] = z[67] + z[126];
z[73] = z[31] * z[73];
z[75] = z[8] + z[82];
z[82] = z[5] + -z[75] + -z[119] + z[192];
z[82] = z[0] * z[82];
z[84] = z[115] + -z[124] + -z[163];
z[84] = z[34] + z[84] / T(2) + -z[167];
z[84] = z[84] * z[174];
z[77] = z[77] + -z[189];
z[77] = z[36] * z[77];
z[73] = z[73] + z[77] + z[82] + z[84] + z[120];
z[73] = z[29] * z[73];
z[77] = -(z[42] * z[113]);
z[62] = -z[3] + z[8] + z[62] + -z[66] + -z[123];
z[62] = z[28] + z[62] / T(2) + z[87];
z[62] = z[62] * z[136];
z[82] = z[74] + -z[86] + -z[172];
z[82] = z[44] * z[82];
z[62] = z[62] + z[82];
z[67] = -z[67] + z[121];
z[67] = z[36] * z[67];
z[82] = -z[4] + z[5] + z[51] + -z[106];
z[82] = z[82] / T(8) + z[178];
z[82] = z[82] * z[140];
z[67] = z[67] + z[82] + -z[120];
z[67] = z[31] * z[67];
z[82] = (T(3) * z[3]) / T(2);
z[66] = -z[66] + z[82] + -z[119];
z[84] = -z[66] + -z[159] + -z[192];
z[84] = z[31] * z[84];
z[63] = z[3] + -z[9] + z[63] + -z[69] + -z[78];
z[63] = z[0] * z[63];
z[63] = (T(3) * z[63]) / T(2) + z[84];
z[63] = z[0] * z[63];
z[69] = -z[8] + -z[68] + -z[81] + z[82] + -z[127];
z[69] = z[49] * z[69];
z[66] = z[66] + z[109] + z[190];
z[66] = -(z[66] * z[80]);
z[72] = T(-2) * z[72] + z[75] + -z[190];
z[72] = z[36] * z[72];
z[66] = z[66] + z[72] + z[120] + -z[155];
z[66] = z[26] * z[66];
z[72] = z[130] + z[141] + z[180];
z[72] = z[39] * z[72];
z[75] = z[157] * z[188];
z[78] = -z[16] + -z[200];
z[78] = z[50] * z[78];
z[62] = (T(3) * z[62]) / T(2) + z[63] + z[65] + z[66] + z[67] + T(3) * z[69] + (T(9) * z[72]) / T(4) + z[73] + z[75] + z[77] + z[78] + -z[148];
z[62] = z[11] * z[62];
z[63] = -z[161] + z[176];
z[65] = z[63] * z[149];
z[66] = z[161] / T(2) + -z[176];
z[66] = z[0] * z[66];
z[67] = z[136] * z[147];
z[69] = z[67] / T(2);
z[72] = -z[169] + z[199] / T(4);
z[73] = z[31] * z[72];
z[73] = z[59] + -z[66] + -z[69] + z[73];
z[75] = z[147] * z[174];
z[77] = z[31] * z[147];
z[75] = z[75] + -z[77];
z[78] = -z[75] + z[169];
z[78] = z[78] * z[146];
z[80] = -(z[44] * z[198]);
z[81] = z[39] * z[147];
z[81] = T(3) * z[81];
z[82] = z[46] * z[122];
z[84] = (T(3) * z[82]) / T(2);
z[85] = prod_pow(z[57], 3);
z[86] = T(3) * z[58] + z[197];
z[86] = z[54] * z[86];
z[73] = (T(13) * z[60]) / T(8) + (T(-259) * z[61]) / T(12) + -z[65] + T(3) * z[73] + z[78] + z[80] + -z[81] + -z[84] + (T(-13) * z[85]) / T(6) + z[86];
z[73] = z[14] * z[73];
z[78] = prod_pow(z[0], 2) * z[91];
z[80] = z[31] * z[169];
z[69] = -z[59] + -z[69] + -z[78] / T(2) + z[80];
z[78] = z[91] * z[166];
z[80] = -z[78] + z[161];
z[86] = T(3) * z[30];
z[80] = z[80] * z[86];
z[87] = z[49] * z[198];
z[75] = z[75] * z[146];
z[92] = -(z[42] * z[198]);
z[93] = prod_pow(z[54], 3);
z[69] = (T(-3) * z[60]) / T(4) + (T(-785) * z[61]) / T(144) + T(3) * z[69] + z[75] + z[80] + z[85] + -z[87] + z[92] + z[93] / T(2);
z[69] = z[15] * z[69];
z[75] = z[29] * z[147];
z[75] = z[75] + z[170];
z[80] = z[26] * z[91];
z[80] = -z[75] + z[80];
z[80] = z[16] * z[80];
z[91] = z[30] * z[91];
z[91] = z[91] + z[177];
z[91] = z[35] * z[91];
z[68] = z[68] + z[96] + -z[107] + -z[108] + z[138];
z[68] = z[30] * z[68];
z[92] = T(3) * z[34];
z[93] = z[6] + -z[7] + -z[70] + z[92] + z[102] + -z[125];
z[93] = z[31] * z[93];
z[94] = z[0] + (T(3) * z[36]) / T(2);
z[96] = -z[6] + z[106];
z[94] = z[94] * z[96];
z[96] = -z[28] + z[98];
z[99] = -z[34] + z[76] + -z[96] / T(2);
z[99] = z[99] * z[146];
z[93] = z[93] + z[94] + z[99];
z[94] = -(z[26] * z[110]);
z[68] = z[68] + z[93] / T(2) + z[94];
z[68] = z[11] * z[68];
z[93] = z[32] * z[153];
z[76] = -z[76] + z[92];
z[92] = -z[7] + T(5) * z[28];
z[79] = -z[76] + -z[79] + z[92] / T(2);
z[79] = z[31] * z[79];
z[92] = -z[6] + z[98];
z[92] = z[7] + T(-5) * z[92];
z[92] = z[92] * z[185];
z[76] = z[76] + (T(5) * z[96]) / T(2);
z[76] = z[29] * z[76];
z[94] = z[6] + T(-5) * z[106];
z[94] = z[36] * z[94];
z[76] = z[76] + z[79] + z[92] + z[94] / T(2);
z[79] = -(z[26] * z[101]);
z[92] = (T(5) * z[7]) / T(4) + -z[37] + -z[112] + -z[179];
z[92] = z[30] * z[92];
z[76] = z[76] / T(2) + z[79] + z[92] + z[116];
z[76] = z[12] * z[76];
z[79] = z[152] * z[166];
z[68] = z[68] + z[76] + z[79] + z[80] + z[91] + -z[93] / T(4);
z[68] = z[68] * z[97];
z[76] = T(5) * z[59] + (T(3) * z[60]) / T(8) + z[81] + -z[84] + -z[85] / T(2) + z[87];
z[72] = z[72] * z[140];
z[77] = z[77] * z[146];
z[79] = -z[58] + -z[197] / T(6);
z[79] = z[54] * z[79];
z[65] = (T(49) * z[61]) / T(12) + -z[65] + T(-3) * z[66] + z[72] + -z[76] + z[77] + (T(5) * z[79]) / T(2);
z[65] = z[16] * z[65];
z[66] = z[42] * z[135];
z[70] = T(2) * z[53] + -z[70] + -z[74] + z[88] + -z[163] + -z[165];
z[70] = z[54] * z[55] * z[70];
z[72] = -z[8] + z[53];
z[72] = -z[9] + (T(4) * z[72]) / T(5) + (T(-2) * z[74]) / T(5) + -z[172] / T(5) + (T(-3) * z[187]) / T(5);
z[72] = z[56] * z[72];
z[70] = z[70] + T(6) * z[72];
z[70] = int_to_imaginary<T>(1) * z[70];
z[72] = -(z[95] * z[147]);
z[74] = -z[2] + z[143];
z[74] = z[28] + T(5) * z[74] + z[89] + -z[168];
z[74] = z[11] * z[74];
z[72] = z[72] + z[74] / T(4);
z[72] = z[48] * z[72];
z[66] = z[66] + z[70] + z[72];
z[70] = z[135] * z[166];
z[72] = -(z[26] * z[144]);
z[74] = -(z[14] * z[75]);
z[70] = z[70] + z[72] + z[74];
z[70] = z[70] * z[86];
z[63] = -z[63] + -z[78];
z[63] = z[63] * z[86];
z[72] = -z[42] + z[44];
z[72] = z[72] * z[198];
z[74] = -z[58] + z[197] / T(3);
z[74] = z[54] * z[74];
z[63] = (T(-193) * z[61]) / T(72) + z[63] + (T(-3) * z[67]) / T(2) + z[72] + z[74] + -z[76];
z[63] = z[35] * z[63];
z[67] = prod_pow(z[31], 2) * z[160];
z[72] = -(z[30] * z[199]);
z[67] = z[67] + z[72] + -z[82];
z[67] = z[67] * z[202];
z[72] = -z[14] + z[15];
z[72] = z[72] * z[147];
z[74] = -(z[11] * z[154]);
z[72] = T(3) * z[72] + z[74];
z[72] = z[40] * z[72];
z[74] = -z[4] + -z[98] + z[191];
z[74] = z[74] * z[158];
z[74] = z[74] + z[152] + z[162];
z[75] = (T(3) * z[45]) / T(2);
z[74] = z[74] * z[75];
return z[62] + z[63] + z[64] + z[65] + T(3) * z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[74] + z[83] + z[90];
}



template IntegrandConstructorType<double> f_4_336_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_336_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_336_construct (const Kin<qd_real>&);
#endif

}