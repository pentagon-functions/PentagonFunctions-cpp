#include "f_4_284.h"

namespace PentagonFunctions {

template <typename T> T f_4_284_abbreviated (const std::array<T,58>&);

template <typename T> class SpDLog_f_4_284_W_12 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_284_W_12 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[1], 2) * (abb[4] * T(-3) + abb[6] * T(-3) + abb[8] * T(-3) + abb[3] * T(3) + abb[5] * T(3) + abb[7] * T(3)) + prod_pow(abb[2], 2) * (abb[5] * T(-3) + abb[7] * T(-3) + abb[4] * T(3) + abb[6] * T(3) + abb[8] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_284_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_284_W_22 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[9] * (abb[4] * prod_pow(abb[11], 2) * T(-3) + abb[8] * prod_pow(abb[11], 2) * T(3) + prod_pow(abb[10], 2) * (abb[8] * T(-3) + abb[4] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_284_W_10 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_284_W_10 (const Kin<T>& kin) {
        c[0] = T(4) * kin.v[0] * kin.v[2] + kin.v[1] * (T(4) * kin.v[0] + T(-2) * kin.v[1] + T(-8) * kin.v[2] + T(-8) * kin.v[3]) + kin.v[2] * (T(-6) * kin.v[2] + T(-8) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(2) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[4]));
c[1] = kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + T(4) * kin.v[2]) + kin.v[2] * (T(4) * kin.v[2] + T(-8) * kin.v[3]) + kin.v[1] * ((bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[1] + T(12) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(6) * kin.v[2] + T(-4) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(2) * kin.v[2] + T(-4) * kin.v[3]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(kin.v[2]) * (T(4) * kin.v[0] * kin.v[2] + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(-8) * kin.v[3] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(-6) + T(4) * kin.v[0] + (bc<T>[0] * int_to_imaginary<T>(2) + T(1)) * kin.v[1] + T(-2) * kin.v[2] + T(-8) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[2] + T(-4) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (T(2) * kin.v[0] * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[2] * (T(6) + T(-6) * kin.v[2] + T(-4) * kin.v[3] + T(6) * kin.v[4]) + kin.v[1] * (T(6) + T(2) * kin.v[0] + (T(-4) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + T(-10) * kin.v[2] + T(-4) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[2] + T(-2) * kin.v[4]))) + rlog(-kin.v[1]) * (T(-2) * kin.v[0] * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[2] * (T(6) + T(4) * kin.v[3] + T(6) * kin.v[4]) + kin.v[1] * (T(6) + T(-2) * kin.v[0] + (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(4) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(-2) * kin.v[0] * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[2] * (T(6) + T(4) * kin.v[3] + T(6) * kin.v[4]) + kin.v[1] * (T(6) + T(-2) * kin.v[0] + (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(4) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(-2) * kin.v[0] * kin.v[2] + kin.v[2] * (T(3) * kin.v[2] + T(4) * kin.v[3]) + kin.v[1] * (T(-2) * kin.v[0] + kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4])));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[2];
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2]) + rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + T(4) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + T(4) * kin.v[2]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + T(4) * kin.v[2]) + rlog(kin.v[2]) * ((T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[17] * (abb[3] * (abb[22] * T(-4) + abb[18] * (-abb[18] + abb[20] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[21] * T(2))) + abb[1] * (abb[3] * abb[18] * T(-2) + abb[18] * (abb[24] * T(-4) + abb[7] * T(-2) + abb[8] * T(-2) + abb[23] * T(2) + abb[4] * T(4)) + abb[2] * (abb[4] * T(-4) + abb[23] * T(-2) + abb[3] * T(2) + abb[7] * T(2) + abb[8] * T(2) + abb[24] * T(4))) + abb[18] * (abb[23] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[7] * bc<T>[0] * int_to_imaginary<T>(2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[24] * bc<T>[0] * int_to_imaginary<T>(4) + abb[20] * (abb[24] * T(-4) + abb[7] * T(-2) + abb[8] * T(-2) + abb[23] * T(2) + abb[4] * T(4)) + abb[21] * (abb[4] * T(-4) + abb[23] * T(-2) + abb[7] * T(2) + abb[8] * T(2) + abb[24] * T(4)) + abb[18] * (-abb[4] + -abb[7] + abb[23] * T(-5) + abb[8] * T(2) + abb[6] * T(3) + abb[19] * T(3) + abb[24] * T(4))) + abb[2] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[23] * bc<T>[0] * int_to_imaginary<T>(2) + abb[24] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[4] * bc<T>[0] * int_to_imaginary<T>(4) + abb[3] * (abb[21] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[18] * T(2) + abb[20] * T(2)) + abb[21] * (abb[24] * T(-4) + abb[7] * T(-2) + abb[8] * T(-2) + abb[23] * T(2) + abb[4] * T(4)) + abb[18] * (abb[4] * T(-4) + abb[23] * T(-2) + abb[7] * T(2) + abb[8] * T(2) + abb[24] * T(4)) + abb[20] * (abb[4] * T(-4) + abb[23] * T(-2) + abb[7] * T(2) + abb[8] * T(2) + abb[24] * T(4)) + abb[2] * (-abb[3] + -abb[7] + abb[24] * T(-8) + abb[8] * T(-4) + abb[6] * T(-3) + abb[19] * T(-3) + abb[4] * T(5) + abb[23] * T(7))) + abb[22] * (abb[24] * T(-8) + abb[7] * T(-4) + abb[8] * T(-4) + abb[23] * T(4) + abb[4] * T(8)));
    }
};
template <typename T> class SpDLog_f_4_284_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_284_W_21 (const Kin<T>& kin) {
        c[0] = (T(3) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(2)) * kin.v[3] + (T(3) / T(2) + (T(9) * kin.v[2]) / T(8) + (T(3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2)) * kin.v[2] + (T(-3) / T(2) + (T(9) * kin.v[0]) / T(8) + (T(3) * kin.v[1]) / T(4) + (T(-9) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2)) * kin.v[0] + (T(-3) / T(2) + (T(-3) * kin.v[1]) / T(8) + (T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2)) * kin.v[1];
c[1] = bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(-4) * kin.v[2] + T(-8) * kin.v[3]) + kin.v[1] * (T(-4) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3])) + (T(3) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[2] + (T(3) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[3] + kin.v[1] * (T(-3) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(3) * kin.v[3]) + kin.v[0] * (T(-3) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (T(2) + T(-3) * kin.v[2] + T(-8) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * (T(2) + T(-5) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + T(-5) * kin.v[1] + T(8) * kin.v[2] + T(10) * kin.v[3] + T(2) * kin.v[4]) + kin.v[0] * (T(-2) + (T(-3) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-8) * kin.v[1] + T(6) * kin.v[2] + T(8) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]))) + rlog(-kin.v[1]) * (kin.v[1] * (-kin.v[3] / T(2) + kin.v[1] / T(4) + (T(13) * kin.v[2]) / T(2) + T(7) + T(-7) * kin.v[4]) + kin.v[3] * (kin.v[3] / T(4) + T(-7) + T(7) * kin.v[4]) + kin.v[2] * ((T(-27) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(2) + T(-7) + T(7) * kin.v[4]) + kin.v[0] * ((T(-13) * kin.v[1]) / T(2) + (T(27) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + T(7) + (T(-27) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-7) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[1] * (-kin.v[3] / T(2) + kin.v[1] / T(4) + (T(13) * kin.v[2]) / T(2) + T(7) + T(-7) * kin.v[4]) + kin.v[3] * (kin.v[3] / T(4) + T(-7) + T(7) * kin.v[4]) + kin.v[2] * ((T(-27) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(2) + T(-7) + T(7) * kin.v[4]) + kin.v[0] * ((T(-13) * kin.v[1]) / T(2) + (T(27) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + T(7) + (T(-27) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-7) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]))) + rlog(kin.v[2]) * (kin.v[1] * (T(2) + T(5) * kin.v[1] + T(-8) * kin.v[2] + T(-10) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * (T(-2) + T(5) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (T(-2) + T(3) * kin.v[2] + T(8) * kin.v[3] + T(2) * kin.v[4]) + kin.v[0] * (T(2) + (bc<T>[0] * int_to_imaginary<T>(2) + T(3)) * kin.v[0] + T(8) * kin.v[1] + T(-6) * kin.v[2] + T(-8) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[2] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[3] * (-kin.v[3] / T(4) + T(7) + T(-7) * kin.v[4]) + kin.v[2] * ((T(27) * kin.v[2]) / T(4) + (T(13) * kin.v[3]) / T(2) + T(7) + T(-7) * kin.v[4]) + kin.v[1] * (-kin.v[1] / T(4) + (T(-13) * kin.v[2]) / T(2) + kin.v[3] / T(2) + T(-7) + T(7) * kin.v[4]) + kin.v[0] * ((T(13) * kin.v[1]) / T(2) + (T(-27) * kin.v[2]) / T(2) + (T(-13) * kin.v[3]) / T(2) + T(-7) + (T(27) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + T(7) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[2] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-35) / T(2) + (T(-47) * kin.v[1]) / T(8) + (T(-23) * kin.v[2]) / T(4) + (T(47) * kin.v[3]) / T(4) + (T(35) * kin.v[4]) / T(2)) * kin.v[1] + (T(35) / T(2) + (T(93) * kin.v[2]) / T(8) + (T(23) * kin.v[3]) / T(4) + (T(-35) * kin.v[4]) / T(2)) * kin.v[2] + (T(35) / T(2) + (T(-47) * kin.v[3]) / T(8) + (T(-35) * kin.v[4]) / T(2)) * kin.v[3] + kin.v[0] * (T(-35) / T(2) + (T(23) * kin.v[1]) / T(4) + (T(-93) * kin.v[2]) / T(4) + (T(-23) * kin.v[3]) / T(4) + (T(35) * kin.v[4]) / T(2) + (T(93) / T(8) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[2] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4])));
c[2] = (T(3) * kin.v[0]) / T(2) + (T(3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2);
c[3] = bc<T>[0] * rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * int_to_imaginary<T>(1) * (T(4) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3])) + bc<T>[0] * rlog(kin.v[2]) * int_to_imaginary<T>(1) * (T(-4) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[26] * (abb[3] * (abb[11] * abb[20] * T(-4) + abb[27] * T(-3) + abb[11] * (abb[11] * T(-2) + bc<T>[0] * int_to_imaginary<T>(4))) + abb[11] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[23] * bc<T>[0] * int_to_imaginary<T>(4) + abb[29] * bc<T>[0] * int_to_imaginary<T>(4) + abb[11] * (abb[23] + (abb[28] * T(-3)) / T(2) + (abb[8] * T(13)) / T(2) + -abb[4] + abb[29] * T(-2) + abb[19] * T(2))) + abb[27] * ((abb[28] * T(3)) / T(2) + (abb[8] * T(9)) / T(2) + abb[29] * T(-3) + abb[19] * T(3)) + abb[11] * abb[20] * (abb[23] * T(-4) + abb[29] * T(-4) + abb[4] * T(4) + abb[8] * T(4) + abb[19] * T(4)) + abb[1] * (abb[3] * abb[11] * T(4) + abb[21] * (abb[3] * T(-4) + abb[23] * T(-4) + abb[29] * T(-4) + abb[4] * T(4) + abb[8] * T(4) + abb[19] * T(4)) + abb[11] * (abb[4] * T(-4) + abb[8] * T(-4) + abb[19] * T(-4) + abb[23] * T(4) + abb[29] * T(4))) + abb[21] * (abb[23] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[29] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[4] * bc<T>[0] * int_to_imaginary<T>(4) + abb[8] * bc<T>[0] * int_to_imaginary<T>(4) + abb[19] * bc<T>[0] * int_to_imaginary<T>(4) + abb[21] * (abb[4] + (abb[8] * T(-13)) / T(2) + (abb[28] * T(3)) / T(2) + -abb[23] + abb[19] * T(-2) + abb[3] * T(2) + abb[29] * T(2)) + abb[3] * (bc<T>[0] * int_to_imaginary<T>(-4) + abb[20] * T(4)) + abb[20] * (abb[4] * T(-4) + abb[8] * T(-4) + abb[19] * T(-4) + abb[23] * T(4) + abb[29] * T(4))));
    }
};
template <typename T> class SpDLog_f_4_284_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_284_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (prod_pow(kin.v[4], 2) + kin.v[3] * (kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(5) * kin.v[4]) / T(2) + T(6)) * kin.v[4] + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (kin.v[2] / T(2) + T(-6) + T(-4) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[0] * (T(6) + (T(-9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + T(4) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(1) + kin.v[3]) + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4]) + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(-6) + T(2) * kin.v[4])) + rlog(-kin.v[4]) * (((T(5) * kin.v[4]) / T(2) + T(6)) * kin.v[4] + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (kin.v[2] / T(2) + T(-6) + T(-4) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[0] * (T(6) + (T(-9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + T(4) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(1) + kin.v[3]) + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4]) + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(-6) + T(2) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(5) * kin.v[4]) / T(2) + T(6)) * kin.v[4] + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (kin.v[2] / T(2) + T(-6) + T(-4) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[0] * (T(6) + (T(-9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + T(4) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(1) + kin.v[3]) + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4]) + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(-6) + T(2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(-5) * kin.v[4]) / T(2) + T(-6)) * kin.v[4] + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(6) + T(-2) * kin.v[4]) + kin.v[0] * (T(-6) + (T(9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(-4) * kin.v[2] + T(-9) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(6) + T(4) * kin.v[3] + T(3) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (((T(-5) * kin.v[4]) / T(2) + T(-6)) * kin.v[4] + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(6) + T(-2) * kin.v[4]) + kin.v[0] * (T(-6) + (T(9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(-4) * kin.v[2] + T(-9) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(6) + T(4) * kin.v[3] + T(3) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(kin.v[3]) * (((T(-5) * kin.v[4]) / T(2) + T(-6)) * kin.v[4] + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(6) + T(-2) * kin.v[4]) + kin.v[0] * (T(-6) + (T(9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(-4) * kin.v[2] + T(-9) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(6) + T(4) * kin.v[3] + T(3) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4])));
c[1] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(kin.v[3]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4])) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[35] * (abb[10] * (abb[21] * (abb[6] + abb[7] + abb[29] + -abb[19] + -abb[23]) + abb[11] * (abb[19] + abb[23] + -abb[6] + -abb[7] + -abb[29]) + abb[18] * (abb[19] + abb[23] + -abb[6] + -abb[7] + -abb[29]) + abb[20] * (abb[19] + abb[23] + -abb[6] + -abb[7] + -abb[29]) + abb[10] * (abb[5] + abb[19] + abb[23] + -abb[6] + -abb[7] + -abb[29]) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[23] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * (abb[11] + abb[18] + abb[20] + -abb[21] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(1) + abb[29] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[5] * (abb[36] * T(-2) + abb[18] * (abb[21] + -abb[11] + -abb[20] + abb[18] * T(-2) + bc<T>[0] * int_to_imaginary<T>(1))) + abb[36] * (abb[19] * T(-2) + abb[23] * T(-2) + abb[6] * T(2) + abb[7] * T(2) + abb[29] * T(2)) + abb[18] * (abb[11] * (abb[6] + abb[7] + abb[29] + -abb[19] + -abb[23]) + abb[20] * (abb[6] + abb[7] + abb[29] + -abb[19] + -abb[23]) + abb[21] * (abb[19] + abb[23] + -abb[6] + -abb[7] + -abb[29]) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[29] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[19] * bc<T>[0] * int_to_imaginary<T>(1) + abb[23] * bc<T>[0] * int_to_imaginary<T>(1) + abb[18] * (abb[19] * T(-2) + abb[23] * T(-2) + abb[6] * T(2) + abb[7] * T(2) + abb[29] * T(2))));
    }
};
template <typename T> class SpDLog_f_4_284_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_284_W_7 (const Kin<T>& kin) {
        c[0] = (T(3) / T(2) + (T(3) * kin.v[1]) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) * kin.v[1] * kin.v[4]) / T(2) + (T(3) / T(2) + (T(-3) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = kin.v[3] * (T(3) / T(2) + (T(3) * kin.v[1]) / T(2) + (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-8) * kin.v[4])) + (T(3) * kin.v[1] * kin.v[4]) / T(2) + (T(3) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-4) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(21) * kin.v[1] * kin.v[4]) / T(2) + (T(21) / T(2) + (T(39) * kin.v[4]) / T(8)) * kin.v[4] + kin.v[3] * (T(21) / T(2) + (T(21) * kin.v[1]) / T(2) + (T(39) * kin.v[4]) / T(4) + (T(39) / T(8) + bc<T>[0] * int_to_imaginary<T>(-6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(12) + T(12) * kin.v[1] + T(-12) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(12) + T(-6) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((-kin.v[4] / T(4) + T(7)) * kin.v[4] + T(7) * kin.v[1] * kin.v[4] + kin.v[3] * (-kin.v[4] / T(2) + T(7) + T(7) * kin.v[1] + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[1] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(4) + T(-2) * kin.v[4]))) + rlog(-kin.v[4]) * (T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + T(5) * kin.v[4]) + kin.v[3] * (T(-2) + T(-2) * kin.v[1] + (bc<T>[0] * int_to_imaginary<T>(-2) + T(5)) * kin.v[3] + T(10) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[1] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(4) + T(-2) * kin.v[4]))) + rlog(kin.v[3]) * (T(2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(2) + T(-5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4])) + kin.v[3] * (T(2) + T(2) * kin.v[1] + (T(-5) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[3] + T(-10) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[1] + T(4) * kin.v[4]))) + rlog(-kin.v[1]) * ((-kin.v[4] / T(2) + T(-4)) * kin.v[4] + T(-4) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4])) + kin.v[3] * (-kin.v[4] + T(-4) + T(-4) * kin.v[1] + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[1] + T(4) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((kin.v[4] / T(4) + T(-7)) * kin.v[4] + T(-7) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4])) + kin.v[3] * (kin.v[4] / T(2) + T(-7) + T(-7) * kin.v[1] + (T(1) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[1] + T(4) * kin.v[4])));
c[2] = (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2);
c[3] = bc<T>[0] * rlog(kin.v[3]) * int_to_imaginary<T>(1) * (T(-4) * kin.v[3] + T(-4) * kin.v[4]) + bc<T>[0] * rlog(-kin.v[4]) * int_to_imaginary<T>(1) * (T(4) * kin.v[3] + T(4) * kin.v[4]) + rlog(-kin.v[1]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + T(3) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(15) * kin.v[4]) / T(2) + (T(15) / T(2) + bc<T>[0] * int_to_imaginary<T>(12)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[37] * (abb[38] * ((abb[8] * T(-15)) / T(2) + (abb[39] * T(-3)) / T(2) + abb[19] * T(-3) + abb[29] * T(3)) + abb[20] * abb[21] * (abb[8] * T(-12) + abb[6] * T(-4) + abb[19] * T(-4) + abb[5] * T(4) + abb[29] * T(4)) + abb[3] * (abb[21] * (abb[11] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[21] * T(2)) + abb[20] * abb[21] * T(4) + abb[38] * T(6)) + abb[21] * (abb[21] * (abb[6] + (abb[8] * T(-21)) / T(4) + (abb[19] * T(-7)) / T(2) + (abb[39] * T(3)) / T(4) + (abb[29] * T(7)) / T(2) + -abb[5]) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[29] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[6] * bc<T>[0] * int_to_imaginary<T>(4) + abb[19] * bc<T>[0] * int_to_imaginary<T>(4) + abb[8] * bc<T>[0] * int_to_imaginary<T>(12) + abb[11] * (abb[5] * T(-4) + abb[29] * T(-4) + abb[6] * T(4) + abb[19] * T(4) + abb[8] * T(12))) + abb[1] * (abb[6] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[5] * bc<T>[0] * int_to_imaginary<T>(4) + abb[29] * bc<T>[0] * int_to_imaginary<T>(4) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-12) + abb[21] * ((abb[39] * T(3)) / T(2) + (abb[8] * T(15)) / T(2) + abb[29] * T(-3) + abb[19] * T(3)) + abb[1] * (abb[5] + abb[19] / T(2) + -abb[29] / T(2) + (abb[8] * T(-9)) / T(4) + (abb[39] * T(-9)) / T(4) + -abb[6] + abb[3] * T(4)) + abb[3] * (abb[21] * T(-6) + abb[20] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[11] * T(4)) + abb[11] * (abb[8] * T(-12) + abb[6] * T(-4) + abb[19] * T(-4) + abb[5] * T(4) + abb[29] * T(4)) + abb[20] * (abb[5] * T(-4) + abb[29] * T(-4) + abb[6] * T(4) + abb[19] * T(4) + abb[8] * T(12))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_284_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl22 = DLog_W_22<T>(kin),dl31 = DLog_W_31<T>(kin),dl10 = DLog_W_10<T>(kin),dl24 = DLog_W_24<T>(kin),dl21 = DLog_W_21<T>(kin),dl1 = DLog_W_1<T>(kin),dl23 = DLog_W_23<T>(kin),dl7 = DLog_W_7<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl19 = DLog_W_19<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl5 = DLog_W_5<T>(kin),spdl12 = SpDLog_f_4_284_W_12<T>(kin),spdl22 = SpDLog_f_4_284_W_22<T>(kin),spdl10 = SpDLog_f_4_284_W_10<T>(kin),spdl21 = SpDLog_f_4_284_W_21<T>(kin),spdl23 = SpDLog_f_4_284_W_23<T>(kin),spdl7 = SpDLog_f_4_284_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,58> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), dl31(t), f_2_2_7(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl10(t), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[17] / kin_path.W[17]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_7(kin_path), rlog(kin.W[15] / kin_path.W[15]), -rlog(t), dl24(t), dl21(t), f_2_1_15(kin_path), -rlog(t), rlog(kin.W[19] / kin_path.W[19]), dl1(t), f_2_1_10(kin_path), f_2_1_12(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl23(t), f_2_1_8(kin_path), dl7(t), f_2_1_11(kin_path), -rlog(t), dl26(t) / kin_path.SqrtDelta, f_1_3_4(kin) - f_1_3_4(kin_path), f_2_1_14(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_2_1_4(kin_path), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl19(t), dl18(t), dl16(t), dl4(t), dl2(t), dl3(t), dl20(t), dl17(t), dl5(t)}
;

        auto result = f_4_284_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_284_abbreviated(const std::array<T,58>& abb)
{
using TR = typename T::value_type;
T z[246];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[50];
z[3] = abb[52];
z[4] = abb[53];
z[5] = abb[54];
z[6] = abb[55];
z[7] = abb[56];
z[8] = abb[57];
z[9] = abb[4];
z[10] = abb[49];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[7];
z[14] = abb[8];
z[15] = abb[14];
z[16] = abb[40];
z[17] = abb[45];
z[18] = abb[47];
z[19] = abb[48];
z[20] = abb[15];
z[21] = abb[16];
z[22] = abb[19];
z[23] = abb[23];
z[24] = abb[24];
z[25] = abb[29];
z[26] = abb[39];
z[27] = abb[43];
z[28] = abb[2];
z[29] = abb[51];
z[30] = abb[11];
z[31] = abb[18];
z[32] = abb[20];
z[33] = abb[21];
z[34] = bc<TR>[0];
z[35] = abb[46];
z[36] = abb[10];
z[37] = abb[30];
z[38] = abb[22];
z[39] = abb[27];
z[40] = abb[31];
z[41] = abb[32];
z[42] = abb[33];
z[43] = abb[34];
z[44] = abb[38];
z[45] = abb[44];
z[46] = abb[13];
z[47] = abb[36];
z[48] = abb[42];
z[49] = abb[25];
z[50] = abb[41];
z[51] = abb[28];
z[52] = abb[12];
z[53] = bc<TR>[3];
z[54] = bc<TR>[1];
z[55] = bc<TR>[5];
z[56] = bc<TR>[9];
z[57] = bc<TR>[2];
z[58] = bc<TR>[4];
z[59] = (T(7) * z[23]) / T(12);
z[60] = (T(5) * z[1]) / T(2);
z[61] = (T(3) * z[14]) / T(2);
z[62] = (T(5) * z[22]) / T(4);
z[63] = z[11] / T(2);
z[64] = T(2) * z[26];
z[65] = (T(-29) * z[12]) / T(4) + -z[64];
z[65] = (T(7) * z[13]) / T(3) + z[25] / T(6) + z[59] + z[60] + -z[61] + -z[62] + -z[63] + z[65] / T(3);
z[65] = z[2] * z[65];
z[66] = z[22] / T(4);
z[67] = z[25] / T(4);
z[68] = z[66] + -z[67];
z[69] = z[1] / T(2);
z[70] = z[24] + z[69];
z[71] = (T(5) * z[23]) / T(4);
z[72] = (T(3) * z[13]) / T(4);
z[73] = z[11] + -z[12];
z[74] = z[73] / T(4);
z[75] = z[14] + z[68] + -z[70] + z[71] + -z[72] + -z[74];
z[75] = z[5] * z[75];
z[76] = z[1] + z[13];
z[77] = (T(2) * z[9]) / T(3);
z[78] = z[24] / T(3);
z[79] = -z[11] + (T(-35) * z[12]) / T(4) + (T(-7) * z[14]) / T(4);
z[79] = (T(-13) * z[22]) / T(4) + (T(8) * z[23]) / T(3) + (T(19) * z[76]) / T(12) + -z[77] + -z[78] + z[79] / T(3);
z[79] = z[8] * z[79];
z[80] = (T(5) * z[11]) / T(4);
z[81] = z[12] / T(4);
z[82] = z[14] + -z[62] + z[64] + -z[80] + z[81];
z[83] = (T(5) * z[25]) / T(12);
z[84] = z[51] / T(4);
z[77] = (T(-7) * z[1]) / T(12) + (T(2) * z[13]) / T(3) + -z[23] + z[77] + z[82] / T(3) + -z[83] + -z[84];
z[77] = z[6] * z[77];
z[82] = z[23] / T(2);
z[85] = z[13] / T(2);
z[86] = z[82] + -z[85];
z[87] = z[22] / T(2);
z[88] = z[25] / T(2);
z[89] = z[87] + -z[88];
z[90] = T(3) * z[14];
z[91] = -z[1] + z[90];
z[92] = (T(5) * z[73]) / T(2);
z[93] = -z[86] + z[89] + -z[91] + z[92];
z[93] = z[3] * z[93];
z[94] = -z[9] + z[13] / T(3);
z[95] = z[22] + z[90];
z[96] = z[12] + T(7) * z[26];
z[84] = z[1] / T(12) + -z[11] + (T(-5) * z[23]) / T(12) + (T(11) * z[25]) / T(12) + z[84] + -z[94] + -z[95] + z[96] / T(4);
z[84] = z[10] * z[84];
z[96] = -z[14] + z[22];
z[97] = z[12] / T(2);
z[98] = (T(-5) * z[23]) / T(6) + -z[96] / T(3) + z[97];
z[78] = (T(5) * z[1]) / T(12) + z[78] + z[83] + z[94] + z[98] / T(2);
z[78] = z[7] * z[78];
z[83] = (T(11) * z[22]) / T(2);
z[94] = (T(11) * z[13]) / T(2);
z[98] = (T(5) * z[14]) / T(2) + (T(25) * z[23]) / T(2) + (T(-11) * z[25]) / T(2) + z[73] + z[83] + -z[94];
z[98] = z[9] + z[98] / T(2);
z[98] = z[29] * z[98];
z[99] = z[12] + z[22];
z[100] = z[13] + z[23];
z[101] = z[99] + -z[100];
z[101] = z[49] * z[101];
z[102] = T(3) * z[101];
z[103] = -z[8] + z[29];
z[104] = -z[2] + z[7] + z[103];
z[105] = T(2) * z[4];
z[106] = z[3] + z[6] + z[104] + -z[105];
z[107] = z[54] * z[106];
z[108] = z[23] + z[73];
z[109] = -z[22] + z[25];
z[110] = z[13] + z[109];
z[111] = z[108] + z[110];
z[112] = T(2) * z[1];
z[113] = z[111] + -z[112];
z[113] = z[37] * z[113];
z[114] = z[27] + -z[50];
z[115] = -z[114] / T(2);
z[116] = -z[20] + z[21] / T(2);
z[117] = (T(5) * z[15]) / T(6) + -z[115] + -z[116] / T(3);
z[117] = z[35] * z[117];
z[118] = (T(3) * z[50]) / T(4);
z[119] = z[20] + (T(-5) * z[21]) / T(4);
z[119] = (T(7) * z[15]) / T(12) + z[27] / T(4) + -z[118] + z[119] / T(3);
z[119] = z[17] * z[119];
z[120] = z[21] + z[27];
z[121] = z[15] + z[50] + z[120];
z[122] = z[121] / T(2);
z[123] = z[19] * z[122];
z[124] = z[18] * z[122];
z[59] = (T(7) * z[1]) / T(6) + -z[9] / T(3) + (T(-11) * z[11]) / T(12) + (T(5) * z[12]) / T(3) + (T(-5) * z[13]) / T(6) + (T(19) * z[14]) / T(6) + (T(23) * z[22]) / T(12) + z[24] + (T(-4) * z[25]) / T(3) + (T(-7) * z[26]) / T(4) + -z[59];
z[59] = z[4] * z[59];
z[59] = z[59] + z[65] + z[75] + z[77] + z[78] + z[79] + z[84] + z[93] / T(2) + z[98] / T(3) + z[102] + z[107] + z[113] + z[117] + z[119] + -z[123] + z[124];
z[59] = z[34] * z[59];
z[65] = T(-3) * z[17] + T(13) * z[18] + T(4) * z[19] + z[35];
z[65] = z[53] * z[65];
z[59] = z[59] + z[65];
z[59] = z[34] * z[59];
z[65] = (T(7) * z[25]) / T(2);
z[75] = z[65] + z[85];
z[77] = (T(5) * z[22]) / T(2);
z[78] = -z[1] + z[77];
z[79] = T(8) * z[14];
z[84] = (T(-3) * z[11]) / T(2) + (T(5) * z[12]) / T(2) + -z[64] + -z[75] + z[78] + z[79] + -z[82];
z[84] = z[2] * z[84];
z[63] = z[63] + -z[64] + z[97];
z[93] = T(2) * z[9];
z[97] = z[63] + z[88] + -z[93];
z[98] = -z[85] + z[87];
z[113] = (T(3) * z[23]) / T(2);
z[117] = T(2) * z[14];
z[119] = -z[1] + -z[97] + -z[98] + -z[113] + -z[117];
z[119] = z[6] * z[119];
z[125] = -z[93] + z[117];
z[126] = z[73] / T(2);
z[127] = z[113] + -z[126];
z[128] = -z[85] + z[89];
z[129] = z[125] + z[127] + -z[128];
z[130] = z[29] * z[129];
z[131] = T(2) * z[22];
z[132] = T(4) * z[14];
z[133] = z[131] + z[132];
z[134] = -z[12] + z[64];
z[135] = z[133] + -z[134];
z[136] = T(3) * z[23];
z[137] = T(3) * z[1];
z[135] = z[13] + T(5) * z[25] + T(-4) * z[135] + z[136] + z[137];
z[135] = z[10] * z[135];
z[138] = -z[14] + z[73];
z[139] = -z[22] + z[138];
z[140] = z[23] + z[76];
z[141] = z[25] + z[140];
z[139] = -z[93] + T(2) * z[139] + z[141];
z[139] = z[4] * z[139];
z[142] = T(2) * z[13];
z[143] = -z[112] + z[142];
z[144] = T(2) * z[24];
z[145] = z[14] + z[144];
z[146] = -z[23] + z[109] + z[143] + z[145];
z[147] = z[7] * z[146];
z[111] = -z[1] + z[111] / T(2);
z[148] = z[37] * z[111];
z[124] = z[124] + z[148];
z[114] = (T(-3) * z[114]) / T(2);
z[149] = (T(5) * z[15]) / T(2) + -z[114] + -z[116];
z[150] = z[35] * z[149];
z[151] = T(3) * z[124] + z[150];
z[152] = T(2) * z[73];
z[153] = -z[90] + z[152];
z[154] = -z[93] + z[144];
z[155] = z[13] + -z[23];
z[156] = z[1] + z[155];
z[157] = z[154] + z[156];
z[158] = z[153] + -z[157];
z[159] = z[8] * z[158];
z[160] = z[17] * z[149];
z[84] = z[84] + z[119] + -z[130] + -z[135] + -z[139] + -z[147] + z[151] + -z[159] + z[160];
z[119] = z[33] * z[84];
z[135] = z[99] + z[117];
z[139] = -z[11] + T(4) * z[26];
z[161] = -z[135] + z[139];
z[162] = T(5) * z[1];
z[163] = -z[23] + z[25];
z[164] = T(3) * z[13];
z[165] = z[93] + T(2) * z[161] + -z[162] + z[163] + z[164];
z[165] = z[6] * z[165];
z[166] = T(5) * z[14];
z[167] = z[22] + -z[73] + z[166];
z[168] = T(3) * z[25];
z[169] = z[137] + z[168];
z[167] = -z[93] + z[100] + T(2) * z[167] + -z[169];
z[167] = z[10] * z[167];
z[134] = z[25] + -z[90] + -z[131] + T(2) * z[134] + z[143];
z[143] = T(2) * z[2];
z[170] = z[105] + z[143];
z[170] = z[134] * z[170];
z[171] = z[7] * z[158];
z[172] = T(4) * z[15];
z[173] = T(3) * z[27];
z[174] = z[20] + z[21] + z[173];
z[175] = z[172] + z[174];
z[176] = z[19] * z[175];
z[177] = z[171] + z[176];
z[178] = z[17] * z[175];
z[159] = -z[159] + z[165] + z[167] + -z[170] + z[177] + z[178];
z[159] = z[0] * z[159];
z[165] = (T(5) * z[23]) / T(2);
z[75] = T(6) * z[14] + (T(9) * z[22]) / T(2) + z[63] + -z[75] + z[93] + -z[137] + -z[165];
z[75] = z[10] * z[75];
z[167] = z[7] * z[129];
z[170] = T(2) * z[6];
z[178] = z[143] + z[170];
z[179] = z[1] + -z[14];
z[180] = -z[155] + z[179];
z[178] = z[178] * z[180];
z[181] = z[19] * z[149];
z[182] = -z[150] + z[181];
z[75] = z[75] + -z[130] + z[167] + -z[178] + -z[182];
z[75] = z[30] * z[75];
z[130] = z[6] * z[129];
z[130] = z[130] + -z[160];
z[178] = z[130] + z[167];
z[183] = T(3) * z[22];
z[184] = z[108] + z[164] + z[168] + -z[183];
z[184] = -z[93] + z[184] / T(2);
z[184] = z[8] * z[184];
z[129] = z[10] * z[129];
z[109] = z[109] + z[155];
z[185] = -z[14] + z[109];
z[186] = z[143] * z[185];
z[185] = z[29] * z[185];
z[129] = z[129] + -z[178] + z[181] + z[184] + T(-2) * z[185] + -z[186];
z[129] = z[36] * z[129];
z[184] = T(7) * z[23];
z[186] = T(6) * z[22];
z[187] = z[184] + -z[186];
z[188] = T(2) * z[12];
z[189] = z[11] + z[188];
z[190] = T(6) * z[24];
z[191] = T(2) * z[189] + z[190];
z[192] = z[137] + z[164];
z[193] = z[90] + -z[93];
z[194] = z[187] + -z[191] + z[192] + -z[193];
z[194] = z[29] * z[194];
z[195] = z[112] + z[142];
z[196] = T(3) * z[12];
z[197] = -z[117] + z[195] + -z[196];
z[198] = z[136] + -z[144];
z[199] = z[183] + -z[197] + -z[198];
z[200] = T(2) * z[8];
z[201] = z[143] + z[200];
z[201] = z[199] * z[201];
z[202] = z[10] * z[158];
z[203] = z[35] * z[175];
z[177] = -z[177] + -z[194] + -z[201] + z[202] + z[203];
z[177] = z[28] * z[177];
z[75] = z[75] + z[129] + z[159] + z[177];
z[129] = (T(7) * z[23]) / T(4);
z[159] = z[69] + z[129];
z[177] = (T(5) * z[13]) / T(4);
z[194] = z[68] + (T(3) * z[73]) / T(4) + -z[145] + z[159] + -z[177];
z[201] = z[7] * z[194];
z[202] = (T(3) * z[25]) / T(4);
z[204] = z[23] / T(4);
z[205] = (T(-3) * z[22]) / T(4) + z[202] + z[204];
z[206] = z[11] / T(4);
z[207] = -z[26] + z[206];
z[81] = z[81] + z[207];
z[208] = -z[14] + T(-3) * z[81] + -z[112] + z[177] + z[205];
z[208] = z[6] * z[208];
z[209] = (T(5) * z[12]) / T(4);
z[207] = z[207] + z[209];
z[159] = (T(-11) * z[13]) / T(4) + (T(7) * z[22]) / T(4) + z[117] + -z[159] + z[202] + T(3) * z[207];
z[159] = z[2] * z[159];
z[62] = z[62] + z[209];
z[202] = z[13] / T(4);
z[207] = z[62] + -z[202] + -z[204];
z[209] = (T(3) * z[11]) / T(4) + z[26];
z[210] = (T(-7) * z[25]) / T(4) + -z[69] + z[132] + z[207] + -z[209];
z[210] = z[10] * z[210];
z[211] = (T(3) * z[23]) / T(4) + z[67] + -z[202];
z[62] = -z[62] + -z[91] + z[209] + z[211];
z[62] = z[4] * z[62];
z[91] = z[18] * z[121];
z[91] = z[91] / T(4) + z[101] + z[148] / T(2);
z[91] = T(3) * z[91];
z[101] = z[8] * z[199];
z[101] = z[101] + z[185];
z[148] = z[16] + -z[19];
z[185] = z[17] + -z[148];
z[199] = (T(3) * z[121]) / T(4);
z[185] = z[185] * z[199];
z[62] = z[62] + -z[91] + z[101] + z[159] + z[185] + z[201] + z[208] + z[210];
z[62] = z[32] * z[62];
z[134] = -(z[4] * z[134]);
z[159] = -z[22] + z[23] + z[197];
z[159] = z[2] * z[159];
z[185] = -(z[6] * z[180]);
z[135] = z[64] + -z[135];
z[100] = -z[1] + z[25] + z[100] + T(2) * z[135];
z[100] = z[10] * z[100];
z[100] = z[100] + -z[101] + z[102] + z[134] + z[147] + z[159] + z[185];
z[101] = int_to_imaginary<T>(1) * z[34];
z[100] = z[100] * z[101];
z[102] = z[93] + z[132];
z[134] = -z[11] + T(13) * z[12];
z[134] = z[134] / T(2);
z[135] = T(4) * z[24];
z[147] = (T(13) * z[23]) / T(2);
z[159] = T(4) * z[1];
z[185] = (T(3) * z[25]) / T(2);
z[94] = (T(-15) * z[22]) / T(2) + z[94] + -z[102] + -z[134] + -z[135] + z[147] + z[159] + z[185];
z[94] = z[8] * z[94];
z[197] = -z[13] + z[25];
z[201] = T(4) * z[23];
z[208] = z[112] + -z[131] + -z[145] + -z[196] + -z[197] + z[201];
z[208] = z[143] * z[208];
z[209] = T(5) * z[13];
z[210] = -z[166] + z[209];
z[212] = T(2) * z[25];
z[191] = T(8) * z[22] + T(-5) * z[23] + -z[93] + -z[137] + z[191] + -z[210] + -z[212];
z[191] = z[29] * z[191];
z[213] = (T(3) * z[73]) / T(2);
z[214] = z[1] + z[14] + z[85] + z[89] + -z[165] + -z[213];
z[215] = z[144] + z[214];
z[216] = z[7] + -z[10];
z[216] = z[215] * z[216];
z[217] = z[4] * z[158];
z[130] = z[130] + z[217];
z[121] = (T(3) * z[121]) / T(2);
z[217] = z[121] * z[148];
z[94] = z[94] + -z[130] + z[191] + z[203] + z[208] + z[216] + z[217];
z[94] = z[31] * z[94];
z[191] = z[30] + -z[36];
z[149] = z[149] * z[191];
z[203] = z[0] * z[175];
z[175] = z[28] * z[175];
z[149] = z[149] + z[175] + -z[203];
z[203] = z[16] * z[149];
z[208] = (T(3) * z[13]) / T(2);
z[216] = -z[82] + z[208];
z[217] = -z[1] + z[132];
z[97] = z[87] + z[97] + z[216] + z[217];
z[97] = z[30] * z[97];
z[158] = z[28] * z[158];
z[97] = z[97] + z[158];
z[218] = z[4] * z[97];
z[62] = z[62] + -z[75] + z[94] + T(2) * z[100] + z[119] + -z[203] + z[218];
z[62] = z[32] * z[62];
z[94] = prod_pow(z[34], 2);
z[100] = z[43] * z[121];
z[100] = -z[94] / T(3) + z[100] + z[149];
z[100] = z[100] * z[101];
z[119] = T(2) * z[20];
z[149] = T(2) * z[15];
z[218] = -z[21] + z[119] + z[149];
z[219] = T(2) * z[218];
z[220] = z[36] * z[219];
z[221] = -(z[30] * z[218]);
z[220] = z[220] + z[221];
z[220] = z[30] * z[220];
z[149] = z[149] + z[174] / T(2);
z[174] = z[0] * z[149];
z[174] = z[174] + -z[175];
z[174] = z[0] * z[174];
z[114] = (T(13) * z[15]) / T(2) + -z[114] + T(-5) * z[116];
z[175] = -(z[47] * z[114]);
z[221] = prod_pow(z[36], 2);
z[222] = -(z[218] * z[221]);
z[115] = z[15] / T(2) + z[115] + -z[116];
z[116] = T(3) * z[48];
z[223] = -(z[115] * z[116]);
z[224] = prod_pow(z[28], 2);
z[225] = z[149] * z[224];
z[120] = -z[20] + z[120];
z[226] = T(3) * z[45];
z[227] = z[120] * z[226];
z[173] = T(5) * z[20] + -z[21] + z[173];
z[228] = T(8) * z[15] + z[173];
z[229] = z[38] * z[228];
z[121] = -(z[40] * z[121]);
z[230] = z[34] * z[53];
z[100] = z[100] + z[121] + z[174] + z[175] + z[220] + z[222] + z[223] + z[225] + z[227] + z[229] + T(9) * z[230];
z[100] = z[16] * z[100];
z[84] = -(z[84] * z[101]);
z[121] = T(6) * z[9];
z[174] = T(7) * z[14];
z[175] = -z[110] + -z[121] + z[174] + z[201];
z[175] = z[10] * z[175];
z[220] = z[117] + z[128];
z[222] = -z[82] + z[213];
z[223] = z[220] + -z[222];
z[225] = z[2] * z[223];
z[227] = z[18] * z[114];
z[229] = T(2) * z[138];
z[230] = z[8] * z[229];
z[225] = z[225] + -z[227] + z[230];
z[230] = (T(3) * z[22]) / T(2);
z[231] = -z[185] + z[230];
z[232] = -z[208] + z[231];
z[233] = (T(9) * z[23]) / T(2);
z[234] = z[232] + z[233];
z[235] = -z[126] + -z[174] + -z[234];
z[235] = z[5] * z[235];
z[109] = -z[73] + z[109];
z[236] = T(3) * z[9];
z[237] = z[90] + -z[109] + -z[236];
z[237] = z[29] * z[237];
z[238] = z[19] * z[219];
z[239] = z[35] * z[219];
z[240] = z[9] + -z[14];
z[241] = -z[23] + z[240];
z[242] = T(2) * z[241];
z[243] = z[3] * z[242];
z[244] = z[5] * z[9];
z[245] = T(6) * z[244];
z[175] = z[175] + -z[178] + z[225] + z[235] + T(2) * z[237] + -z[238] + z[239] + -z[243] + z[245];
z[175] = z[36] * z[175];
z[178] = T(3) * z[26];
z[235] = (T(7) * z[14]) / T(2);
z[67] = -z[9] + -z[67] + z[69] + -z[178] + z[206] + z[207] + z[235];
z[67] = z[4] * z[67];
z[206] = z[14] + z[126];
z[206] = T(5) * z[206] + z[234];
z[207] = z[5] * z[206];
z[207] = z[207] + z[227];
z[234] = z[19] * z[114];
z[160] = -z[150] + -z[160] + z[207] + z[234];
z[66] = z[66] + z[69] + z[81] + z[211] + -z[240];
z[66] = z[6] * z[66];
z[69] = (T(3) * z[12]) / T(4) + (T(11) * z[22]) / T(4) + (T(-15) * z[25]) / T(4) + z[26] + -z[80] + z[102] + -z[112] + -z[177] + -z[204];
z[69] = z[2] * z[69];
z[68] = z[68] + -z[202];
z[71] = -z[68] + -z[71] + -z[74] + z[240];
z[71] = z[29] * z[71];
z[74] = T(5) * z[12];
z[80] = T(-5) * z[11] + T(19) * z[14] + T(13) * z[22] + -z[26] + -z[74] + -z[168] + -z[184] + z[209];
z[80] = -z[1] + (T(-9) * z[51]) / T(2) + z[80] / T(4) + z[236];
z[80] = z[10] * z[80];
z[81] = -z[9] + -z[24] + z[140];
z[102] = z[81] + z[229];
z[102] = z[8] * z[102];
z[96] = -z[23] + -z[96];
z[96] = -z[1] + z[13] + z[24] + z[88] + z[96] / T(2);
z[96] = z[7] * z[96];
z[177] = T(3) * z[244];
z[66] = z[66] + z[67] + z[69] + z[71] + z[80] + z[96] + z[102] + z[160] / T(2) + -z[177];
z[66] = z[33] * z[66];
z[67] = -z[99] + -z[112] + z[139] + -z[197] + z[240];
z[67] = z[67] * z[170];
z[69] = z[17] * z[219];
z[71] = z[109] / T(2);
z[80] = z[29] * z[71];
z[69] = z[69] + -z[80] + -z[182];
z[80] = -z[11] + z[64];
z[96] = T(2) * z[80] + z[93];
z[99] = -z[14] + -z[22] + -z[96] + z[112] + z[155] + z[168];
z[99] = z[99] * z[143];
z[102] = z[1] + z[88];
z[98] = -z[82] + z[98] + z[102];
z[160] = (T(5) * z[11]) / T(2) + (T(-3) * z[12]) / T(2) + -z[64] + z[98] + -z[117];
z[160] = z[10] * z[160];
z[168] = z[8] * z[138];
z[182] = z[5] * z[138];
z[168] = z[168] + -z[182];
z[67] = z[67] + z[69] + z[99] + z[160] + z[167] + T(2) * z[168] + z[243];
z[67] = z[30] * z[67];
z[99] = T(-6) * z[11] + T(4) * z[12] + (T(11) * z[26]) / T(2);
z[160] = (T(15) * z[14]) / T(2);
z[167] = T(4) * z[25];
z[142] = T(-7) * z[22] + z[99] + -z[142] + z[159] + -z[160] + z[167] + z[201];
z[142] = z[10] * z[142];
z[184] = T(6) * z[73] + z[192];
z[197] = -z[23] + -z[93] + -z[174] + z[184];
z[197] = z[3] * z[197];
z[201] = z[5] * z[229];
z[171] = z[171] + z[197] + -z[201] + -z[238];
z[95] = T(-2) * z[95] + -z[155] + z[169];
z[95] = z[2] * z[95];
z[96] = -z[14] + z[96];
z[202] = -z[96] + z[137] + -z[155];
z[202] = z[4] * z[202];
z[204] = -z[81] + T(-3) * z[138];
z[204] = z[200] * z[204];
z[209] = z[29] * z[242];
z[211] = z[18] * z[228];
z[95] = z[95] + z[142] + z[171] + z[202] + z[204] + -z[209] + -z[211];
z[95] = z[0] * z[95];
z[142] = T(3) * z[73] + -z[132];
z[202] = z[9] + -z[23];
z[195] = z[142] + -z[144] + z[195] + -z[202];
z[195] = z[195] * z[200];
z[198] = z[14] + -z[76] + z[198];
z[200] = z[2] * z[198];
z[200] = z[200] + z[209] + -z[211];
z[142] = -z[24] + z[76] + z[142];
z[204] = T(2) * z[10];
z[142] = z[142] * z[204];
z[142] = z[142] + -z[171] + z[195] + -z[200];
z[142] = z[28] * z[142];
z[171] = z[77] + -z[165];
z[195] = (T(5) * z[25]) / T(2);
z[209] = T(4) * z[9];
z[219] = z[195] + -z[209];
z[79] = z[1] + T(5) * z[63] + z[79] + z[171] + z[208] + z[219];
z[229] = -(z[30] * z[79]);
z[229] = -z[158] + z[229];
z[229] = z[4] * z[229];
z[66] = z[66] + z[67] + z[84] + z[95] + z[142] + z[175] + z[203] + z[229];
z[66] = z[33] * z[66];
z[67] = (T(5) * z[13]) / T(2);
z[83] = -z[14] + (T(23) * z[23]) / T(2) + z[67] + -z[83] + -z[88] + -z[134] + z[137];
z[84] = T(3) * z[24];
z[83] = -z[9] + z[83] / T(2) + -z[84];
z[83] = z[29] * z[83];
z[95] = -(z[10] * z[194]);
z[134] = (T(7) * z[13]) / T(2);
z[175] = -z[11] + z[74];
z[65] = z[14] + -z[65] + -z[78] + z[134] + z[147] + (T(-3) * z[175]) / T(2);
z[65] = z[24] + z[65] / T(2);
z[65] = z[7] * z[65];
z[78] = (T(7) * z[73]) / T(2) + z[86] + z[179] + z[231];
z[78] = -z[9] + z[24] + z[78] / T(2);
z[78] = z[8] * z[78];
z[194] = z[24] + z[214] / T(2);
z[194] = z[2] * z[194];
z[203] = z[17] * z[218];
z[214] = -z[9] + z[110] + -z[138];
z[229] = z[6] * z[214];
z[229] = z[203] + -z[229];
z[148] = -(z[148] * z[199]);
z[118] = (T(5) * z[15]) / T(4) + (T(-7) * z[21]) / T(4) + (T(-3) * z[27]) / T(4) + -z[118] + z[119];
z[118] = z[35] * z[118];
z[119] = z[73] + z[157];
z[199] = -(z[4] * z[119]);
z[65] = z[65] + z[78] + z[83] + z[91] + z[95] + z[118] + z[148] + z[194] + z[199] + z[229];
z[65] = z[31] * z[65];
z[78] = T(2) * z[7];
z[83] = z[78] + z[105];
z[83] = z[83] * z[119];
z[91] = -z[136] + z[192];
z[95] = z[14] + z[91] + z[152];
z[95] = z[5] * z[95];
z[105] = z[10] * z[198];
z[118] = z[14] + z[157];
z[119] = z[8] * z[118];
z[136] = z[5] * z[190];
z[136] = z[136] + -z[245];
z[83] = z[83] + -z[95] + -z[105] + -z[119] + -z[136] + -z[176] + z[200] + -z[243];
z[95] = z[0] + -z[28];
z[83] = -(z[83] * z[95]);
z[105] = z[2] * z[215];
z[119] = z[125] + z[126] + z[128];
z[148] = z[119] + z[165];
z[148] = z[29] * z[148];
z[176] = z[10] + z[78];
z[146] = z[146] * z[176];
z[176] = -z[14] + z[152];
z[194] = z[157] + z[176];
z[194] = z[8] * z[194];
z[105] = z[105] + -z[130] + -z[146] + z[148] + z[151] + z[194];
z[130] = -z[33] + -z[101];
z[105] = z[105] * z[130];
z[130] = -z[125] + z[222] + z[232];
z[130] = z[3] * z[130];
z[78] = z[78] + z[170];
z[78] = z[78] * z[214];
z[146] = z[10] * z[223];
z[69] = -z[69] + z[78] + z[130] + z[146] + z[201] + -z[225];
z[78] = z[36] * z[69];
z[69] = -(z[30] * z[69]);
z[95] = z[95] + -z[191];
z[95] = z[16] * z[95] * z[218];
z[65] = z[65] + z[69] + z[78] + z[83] + T(2) * z[95] + z[105];
z[65] = z[31] * z[65];
z[69] = z[127] + z[128];
z[83] = z[69] + z[90];
z[95] = z[5] * z[83];
z[95] = T(-3) * z[95] + z[181] + z[227];
z[72] = -z[9] + z[72] + (T(9) * z[73]) / T(4) + -z[117] + z[205];
z[72] = z[8] * z[72];
z[68] = z[14] + z[68] + (T(-11) * z[73]) / T(4) + z[93] + -z[129];
z[68] = z[2] * z[68];
z[105] = z[9] + z[126] + -z[220] + -z[233];
z[105] = z[29] * z[105];
z[127] = z[7] * z[214];
z[129] = -z[14] + z[126];
z[128] = (T(-11) * z[23]) / T(2) + -z[128] + T(5) * z[129];
z[128] = z[9] + z[128] / T(2);
z[128] = z[10] * z[128];
z[68] = z[68] + z[72] + -z[95] / T(2) + z[105] + -z[127] + z[128] + -z[130] + z[150] + -z[177] + z[229];
z[68] = z[68] * z[221];
z[72] = T(2) * z[11];
z[95] = -z[9] + -z[22] + -z[72] + z[84] + z[117] + -z[141] + z[178] + z[188];
z[95] = z[4] * z[95];
z[61] = -z[9] + z[61] + z[70] + -z[73] + -z[86];
z[105] = z[7] * z[61];
z[128] = z[172] + z[173] / T(2);
z[128] = z[18] * z[128];
z[129] = z[19] * z[218];
z[105] = z[105] + z[128] + z[129];
z[128] = z[9] + z[161];
z[60] = -z[60] + z[88] + z[128] + z[216];
z[60] = z[6] * z[60];
z[130] = (T(3) * z[1]) / T(2);
z[86] = -z[86] + -z[96] + z[130] + z[185];
z[86] = z[2] * z[86];
z[96] = -z[9] + T(4) * z[11] + -z[14] / T(4) + (T(-5) * z[26]) / T(4) + z[155] + -z[196] + z[231];
z[96] = z[10] * z[96];
z[141] = z[14] + z[156];
z[146] = -z[84] + (T(-3) * z[141]) / T(2);
z[146] = z[5] * z[146];
z[81] = z[81] + T(4) * z[138];
z[81] = z[8] * z[81];
z[138] = z[17] * z[149];
z[60] = z[60] + z[81] + z[86] + z[95] + z[96] + z[105] + z[138] + z[146] + z[177] + -z[197];
z[60] = z[0] * z[60];
z[81] = -z[9] + z[108];
z[86] = -z[81] + -z[112] + z[133] + -z[212];
z[86] = z[10] * z[86];
z[80] = z[14] + z[80];
z[80] = z[22] + T(2) * z[80] + z[93] + -z[169];
z[80] = z[2] * z[80];
z[95] = -z[3] + z[29];
z[95] = z[95] * z[241];
z[96] = -z[1] + z[128] + -z[163];
z[128] = -(z[6] * z[96]);
z[80] = z[80] + z[86] + z[95] + z[128] + -z[168] + -z[203];
z[86] = T(2) * z[30];
z[80] = z[80] * z[86];
z[86] = -(z[86] * z[96]);
z[86] = z[86] + z[158];
z[86] = z[4] * z[86];
z[60] = z[60] + z[80] + z[86] + -z[142];
z[60] = z[0] * z[60];
z[80] = -z[144] + z[162] + z[210];
z[74] = z[72] + -z[74];
z[74] = T(2) * z[74] + z[80] + z[187] + -z[209];
z[74] = z[8] * z[74];
z[86] = z[12] + z[72];
z[86] = T(2) * z[86] + z[186];
z[95] = T(-8) * z[9] + -z[23] + z[86] + z[174] + z[190] + -z[192];
z[95] = z[29] * z[95];
z[128] = -z[23] + z[209];
z[80] = z[80] + -z[86] + z[128];
z[80] = z[2] * z[80];
z[86] = T(4) * z[73];
z[90] = z[86] + -z[90] + z[157];
z[133] = -(z[7] * z[90]);
z[86] = -z[14] + z[86] + z[91];
z[86] = z[5] * z[86];
z[91] = -(z[118] * z[204]);
z[138] = z[35] * z[228];
z[74] = z[74] + z[80] + z[86] + z[91] + z[95] + z[133] + z[136] + z[138] + z[211];
z[74] = z[38] * z[74];
z[80] = z[63] + z[98];
z[86] = -z[2] + z[10];
z[80] = z[80] * z[86];
z[86] = -z[63] + -z[89] + -z[112] + z[216];
z[91] = z[6] * z[86];
z[95] = z[7] * z[111];
z[80] = -z[80] + z[91] + z[95] + z[123] + -z[124];
z[91] = -(z[17] * z[122]);
z[91] = -z[80] + z[91];
z[95] = T(3) * z[43];
z[91] = z[91] * z[95];
z[95] = z[86] * z[95];
z[95] = z[95] + -z[97];
z[95] = z[4] * z[95];
z[97] = z[3] + z[29];
z[98] = z[2] + -z[6];
z[112] = z[7] + -z[8] + z[97] + -z[98];
z[133] = z[18] + z[19];
z[112] = T(3) * z[4] + (T(-2) * z[35]) / T(5) + (T(-3) * z[112]) / T(2) + (T(4) * z[133]) / T(5);
z[112] = prod_pow(z[54], 2) * z[112];
z[97] = (T(-13) * z[4]) / T(12) + z[8] / T(2) + z[10] / T(6) + z[17] / T(9) + (T(-101) * z[18]) / T(135) + (T(-56) * z[19]) / T(135) + (T(13) * z[35]) / T(135) + z[97] / T(4) + (T(-2) * z[98]) / T(3);
z[97] = z[94] * z[97];
z[106] = z[58] * z[106];
z[75] = z[75] + z[91] + z[95] + z[97] + T(-3) * z[106] + z[112];
z[75] = z[34] * z[75];
z[91] = z[35] + T(-2) * z[133];
z[91] = z[55] * z[91];
z[75] = z[75] + (T(96) * z[91]) / T(5);
z[75] = int_to_imaginary<T>(1) * z[75];
z[91] = (T(7) * z[23]) / T(2);
z[95] = -z[1] + z[145];
z[88] = z[85] + -z[88] + z[91] + -z[95] + -z[175] / T(2) + -z[230];
z[88] = z[2] * z[88];
z[97] = z[11] + z[196];
z[97] = z[97] / T(2) + -z[102] + z[145] + z[171] + -z[208];
z[97] = -(z[97] * z[103]);
z[102] = z[113] + -z[208];
z[89] = z[89] + -z[95] + z[102] + z[126];
z[89] = z[7] * z[89];
z[95] = z[10] * z[111];
z[103] = z[35] * z[122];
z[88] = -z[88] + z[89] + -z[95] + z[97] + -z[103] + z[123] + z[124];
z[89] = z[40] * z[88];
z[95] = -(z[16] * z[122]);
z[88] = z[88] + z[95];
z[88] = z[42] * z[88] * z[101];
z[86] = -(z[4] * z[86]);
z[95] = -z[16] + z[17];
z[95] = z[95] * z[122];
z[80] = z[80] + z[86] + z[95];
z[80] = z[41] * z[80];
z[86] = -(z[94] * z[104]);
z[94] = z[101] * z[107];
z[86] = z[86] + z[94];
z[86] = z[57] * z[86];
z[80] = z[80] + z[86] + z[88] + z[89];
z[86] = z[84] + -z[113] + -z[130] + z[183] + z[189] + -z[208] + z[235] + -z[236];
z[86] = z[29] * z[86];
z[88] = z[14] / T(2);
z[84] = z[84] + -z[88] + -z[102] + z[130] + z[152];
z[84] = z[5] * z[84];
z[89] = z[91] + -z[93] + z[152];
z[70] = z[70] + z[85] + -z[88] + -z[89];
z[70] = z[2] * z[70];
z[85] = (T(7) * z[1]) / T(2) + T(-5) * z[24] + z[89] + z[134] + -z[235];
z[85] = z[8] * z[85];
z[88] = z[108] + -z[117];
z[76] = z[9] + z[76] + T(2) * z[88] + -z[135];
z[76] = z[10] * z[76];
z[88] = z[35] * z[149];
z[70] = z[70] + z[76] + z[84] + z[85] + z[86] + z[88] + z[105] + -z[177];
z[70] = z[28] * z[70];
z[76] = z[35] * z[218];
z[84] = z[10] + z[143];
z[81] = z[81] * z[84];
z[84] = -z[176] + z[202];
z[84] = z[8] * z[84];
z[85] = -z[14] + T(-2) * z[23] + -z[73] + z[93];
z[85] = z[29] * z[85];
z[86] = z[3] * z[241];
z[81] = -z[76] + z[81] + z[84] + z[85] + z[86] + -z[182];
z[81] = z[36] * z[81];
z[70] = z[70] + T(2) * z[81];
z[70] = z[28] * z[70];
z[81] = (T(3) * z[51]) / T(2);
z[84] = -z[12] + z[25] + z[81] + z[93] + z[139] + z[155] + -z[160] + -z[183];
z[84] = z[10] * z[84];
z[85] = -z[73] + z[110] + -z[125];
z[85] = z[29] * z[85];
z[86] = T(3) * z[51] + -z[155] + -z[217];
z[86] = z[6] * z[86];
z[88] = z[2] * z[180];
z[76] = -z[76] + z[84] + z[85] + z[86] + z[88] + -z[127] + z[129];
z[76] = z[30] * z[76];
z[76] = z[76] + z[78];
z[76] = z[30] * z[76];
z[67] = z[67] + -z[77];
z[77] = z[67] + -z[92];
z[78] = -z[77] + z[132] + z[147] + -z[195] + -z[209];
z[78] = z[29] * z[78];
z[84] = z[113] + z[219];
z[77] = z[77] + z[84] + z[132];
z[85] = z[6] + z[7];
z[77] = -(z[77] * z[85]);
z[86] = -z[165] + z[209] + z[232];
z[88] = (T(13) * z[73]) / T(2) + -z[86] + -z[132];
z[88] = z[8] * z[88];
z[89] = z[17] + z[35];
z[91] = z[89] * z[114];
z[92] = z[10] * z[109];
z[77] = z[77] + z[78] + z[88] + z[91] + z[92] + -z[227];
z[77] = z[47] * z[77];
z[78] = (T(-9) * z[14]) / T(2) + -z[63];
z[78] = (T(-19) * z[22]) / T(2) + (T(9) * z[25]) / T(2) + T(5) * z[78] + z[81] + z[121] + z[137] + -z[165] + z[208];
z[78] = z[10] * z[78];
z[81] = -z[2] + z[4];
z[79] = z[79] * z[81];
z[63] = z[14] + z[63] + z[87];
z[63] = T(5) * z[63] + z[159] + -z[216] + z[219];
z[63] = z[6] * z[63];
z[81] = z[19] + -z[89];
z[81] = z[81] * z[114];
z[87] = z[121] + -z[206];
z[87] = z[29] * z[87];
z[63] = z[63] + z[78] + z[79] + z[81] + z[87] + z[207] + -z[245];
z[63] = z[39] * z[63];
z[78] = z[83] + -z[93];
z[78] = z[5] * z[78];
z[79] = -z[82] + -z[119];
z[79] = z[29] * z[79];
z[69] = -z[69] + -z[193];
z[69] = z[10] * z[69];
z[81] = z[3] + -z[85];
z[71] = z[71] * z[81];
z[81] = z[17] + z[19];
z[82] = -z[35] + z[81];
z[82] = z[82] * z[115];
z[69] = z[69] + z[71] + z[78] + z[79] + z[82];
z[69] = z[69] * z[116];
z[71] = z[23] + -z[164] + z[209];
z[64] = z[12] + z[64] + -z[72];
z[64] = T(2) * z[64] + z[71] + z[131] + -z[167] + -z[179];
z[64] = z[4] * z[64];
z[72] = T(-7) * z[1] + T(4) * z[161] + -z[163] + z[164] + z[209];
z[72] = -(z[72] * z[98]);
z[78] = z[128] + z[166] + -z[184];
z[79] = z[3] + -z[8];
z[78] = z[78] * z[79];
z[71] = (T(19) * z[14]) / T(2) + T(5) * z[22] + -z[71] + -z[99] + -z[169];
z[71] = z[10] * z[71];
z[79] = z[81] * z[228];
z[64] = z[64] + z[71] + z[72] + z[78] + z[79] + z[211];
z[64] = z[44] * z[64];
z[71] = z[140] + -z[144] + z[153];
z[71] = z[8] * z[71];
z[72] = z[153] + z[156];
z[78] = -z[3] + z[10];
z[72] = z[72] * z[78];
z[78] = z[4] + z[7];
z[78] = z[78] * z[118];
z[79] = -z[141] + -z[154];
z[79] = z[5] * z[79];
z[81] = -(z[19] * z[120]);
z[71] = z[71] + z[72] + z[78] + z[79] + z[81];
z[71] = z[71] * z[226];
z[72] = -z[73] + z[117] + -z[202];
z[78] = -(z[18] * z[72]);
z[72] = -(z[19] * z[72]);
z[79] = z[5] * z[218];
z[81] = z[14] + -z[202];
z[81] = z[35] * z[81];
z[72] = z[72] + z[78] + z[79] + z[81];
z[72] = z[46] * z[72];
z[67] = -z[67] + (T(-11) * z[73]) / T(2) + -z[84] + z[132];
z[67] = z[47] * z[67];
z[73] = z[46] * z[218];
z[78] = T(2) * z[73];
z[67] = z[67] + z[78];
z[67] = z[2] * z[67];
z[61] = z[61] * z[224];
z[79] = -(z[38] * z[90]);
z[81] = -(prod_pow(z[30], 2) * z[96]);
z[61] = (T(7) * z[56]) / T(6) + z[61] + z[79] + z[81];
z[61] = z[4] * z[61];
z[79] = -z[86] + z[132] + -z[213];
z[79] = z[47] * z[79];
z[78] = z[78] + z[79];
z[78] = z[3] * z[78];
z[73] = z[52] * z[73];
z[79] = T(8) * z[5] + T(11) * z[8];
z[79] = (T(-19) * z[2]) / T(3) + -z[3] / T(6) + T(7) * z[6] + (T(85) * z[7]) / T(6) + (T(-32) * z[10]) / T(3) + (T(-101) * z[29]) / T(6) + z[79] / T(3);
z[79] = z[56] * z[79];
return z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + T(2) * z[72] + T(-4) * z[73] + z[74] + z[75] + z[76] + z[77] + z[78] + z[79] + T(3) * z[80] + z[100];
}



template IntegrandConstructorType<double> f_4_284_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_284_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_284_construct (const Kin<qd_real>&);
#endif

}