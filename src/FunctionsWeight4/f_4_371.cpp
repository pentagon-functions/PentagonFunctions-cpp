#include "f_4_371.h"

namespace PentagonFunctions {

template <typename T> T f_4_371_abbreviated (const std::array<T,53>&);

template <typename T> class SpDLog_f_4_371_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_371_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(-14) + T(-14) * kin.v[1] + (T(-5) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-5) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-8) * kin.v[4])) + T(-14) * kin.v[1] * kin.v[4] + ((T(-5) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-4) * kin.v[4]));
c[1] = (T(-6) + bc<T>[0] * int_to_imaginary<T>(8)) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4];
c[2] = kin.v[3] * (T(-14) + T(-14) * kin.v[1] + (T(-5) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-5) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-8) * kin.v[4])) + T(-14) * kin.v[1] * kin.v[4] + ((T(-5) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-4) * kin.v[4]));
c[3] = (T(-6) + bc<T>[0] * int_to_imaginary<T>(8)) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4];
c[4] = kin.v[3] * (T(-14) + T(-14) * kin.v[1] + (T(-5) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-5) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-8) * kin.v[4])) + T(-14) * kin.v[1] * kin.v[4] + ((T(-5) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-4) * kin.v[4]));
c[5] = (T(-6) + bc<T>[0] * int_to_imaginary<T>(8)) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[1] * (t * c[0] + c[1]) + abb[12] * (t * c[2] + c[3]) + abb[8] * (t * c[4] + c[5]);
        }

        return abb[9] * (abb[5] * abb[10] * (abb[8] * T(-8) + abb[12] * T(-8)) + abb[10] * (abb[8] * bc<T>[0] * int_to_imaginary<T>(8) + abb[12] * bc<T>[0] * int_to_imaginary<T>(8) + abb[10] * (abb[8] * T(4) + abb[12] * T(4))) + abb[11] * (abb[8] * T(6) + abb[12] * T(6)) + abb[1] * (abb[5] * abb[10] * T(-8) + abb[10] * (bc<T>[0] * int_to_imaginary<T>(8) + abb[10] * T(4)) + abb[11] * T(6) + abb[6] * abb[10] * T(8)) + abb[6] * abb[10] * (abb[8] * T(8) + abb[12] * T(8)) + abb[3] * (abb[6] * (abb[8] * T(-8) + abb[12] * T(-8)) + abb[3] * (abb[1] * T(-4) + abb[8] * T(-4) + abb[12] * T(-4)) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[1] * (abb[6] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[5] * T(8)) + abb[5] * (abb[8] * T(8) + abb[12] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_371_construct (const Kin<T>& kin) {
    return [&kin, 
            dl13 = DLog_W_13<T>(kin),dl7 = DLog_W_7<T>(kin),dl8 = DLog_W_8<T>(kin),dl26 = DLog_W_26<T>(kin),dl17 = DLog_W_17<T>(kin),dl14 = DLog_W_14<T>(kin),dl19 = DLog_W_19<T>(kin),dl31 = DLog_W_31<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),spdl7 = SpDLog_f_4_371_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,53> abbr = 
            {dl13(t), f_1_3_2(kin) - f_1_3_2(kin_path), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[4]), f_2_1_1(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl7(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl8(t), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), f_1_3_5(kin) - f_1_3_5(kin_path), dl26(t) / kin_path.SqrtDelta, rlog(kin.W[3] / kin_path.W[3]), f_2_1_4(kin_path), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), dl17(t), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl19(t), f_2_1_15(kin_path), dl31(t), f_2_2_2(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[30] / kin_path.W[30]), dl2(t), f_2_1_2(kin_path), dl1(t), dl20(t), dl3(t), dl4(t), dl5(t), dl18(t), dl16(t), dl28(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[19] / kin_path.W[19]), dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta}
;

        auto result = f_4_371_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_371_abbreviated(const std::array<T,53>& abb)
{
using TR = typename T::value_type;
T z[239];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[36];
z[11] = abb[38];
z[12] = abb[39];
z[13] = abb[40];
z[14] = abb[41];
z[15] = abb[42];
z[16] = abb[43];
z[17] = abb[44];
z[18] = abb[14];
z[19] = abb[26];
z[20] = abb[30];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[15];
z[24] = abb[16];
z[25] = abb[20];
z[26] = abb[21];
z[27] = abb[22];
z[28] = abb[28];
z[29] = abb[29];
z[30] = abb[31];
z[31] = abb[33];
z[32] = abb[50];
z[33] = abb[52];
z[34] = abb[37];
z[35] = abb[17];
z[36] = abb[19];
z[37] = abb[51];
z[38] = abb[23];
z[39] = abb[24];
z[40] = abb[34];
z[41] = abb[35];
z[42] = abb[46];
z[43] = abb[47];
z[44] = abb[48];
z[45] = abb[49];
z[46] = abb[12];
z[47] = abb[27];
z[48] = abb[18];
z[49] = abb[25];
z[50] = abb[45];
z[51] = abb[13];
z[52] = abb[32];
z[53] = bc<TR>[3];
z[54] = bc<TR>[1];
z[55] = bc<TR>[5];
z[56] = bc<TR>[2];
z[57] = bc<TR>[4];
z[58] = bc<TR>[9];
z[59] = bc<TR>[7];
z[60] = bc<TR>[8];
z[61] = T(3) * z[6];
z[62] = T(4) * z[18];
z[63] = T(4) * z[5];
z[64] = -z[61] + -z[62] + z[63];
z[65] = z[13] + z[20];
z[66] = -z[10] + z[65];
z[67] = -z[11] + z[66];
z[64] = z[64] * z[67];
z[68] = T(4) * z[13];
z[69] = T(4) * z[14];
z[70] = z[68] + z[69];
z[71] = T(5) * z[15];
z[72] = -z[10] + z[12];
z[73] = T(5) * z[17] + -z[70] + -z[71] + z[72];
z[73] = z[2] * z[73];
z[74] = T(2) * z[15];
z[75] = z[13] + z[74];
z[76] = T(2) * z[17];
z[77] = -z[12] + z[14];
z[78] = -z[10] + -z[75] + z[76] + z[77];
z[79] = T(4) * z[21];
z[78] = z[78] * z[79];
z[80] = -z[10] + z[20];
z[81] = z[14] + z[80];
z[82] = z[17] + z[81];
z[83] = z[11] + z[15];
z[84] = z[82] + -z[83];
z[84] = T(5) * z[84];
z[85] = -(z[29] * z[84]);
z[86] = T(4) * z[4];
z[87] = z[12] + z[14];
z[88] = z[11] + z[87];
z[89] = -z[20] + z[88];
z[89] = z[86] * z[89];
z[90] = -z[10] + z[13];
z[91] = z[19] + z[90];
z[92] = z[12] + -z[83] + z[91];
z[93] = T(3) * z[27];
z[94] = z[92] * z[93];
z[95] = -z[17] + z[65];
z[96] = -z[12] + z[95];
z[97] = -z[11] + z[15];
z[98] = z[96] + z[97];
z[98] = T(8) * z[98];
z[99] = z[24] * z[98];
z[100] = -z[2] + z[4];
z[101] = z[0] * z[100];
z[101] = T(8) * z[101];
z[64] = z[64] + z[73] + z[78] + z[85] + z[89] + z[94] + z[99] + -z[101];
z[64] = z[1] * z[64];
z[73] = T(2) * z[10];
z[78] = T(5) * z[13] + -z[73];
z[85] = T(5) * z[12];
z[89] = T(5) * z[20];
z[94] = T(3) * z[17];
z[99] = T(-4) * z[51] + z[78] + z[85] + z[89] + -z[94];
z[102] = T(13) * z[11];
z[103] = T(11) * z[15];
z[104] = z[99] + z[102] + -z[103];
z[105] = z[24] * z[104];
z[106] = T(2) * z[51];
z[107] = z[66] + -z[106];
z[108] = z[11] + z[107];
z[109] = z[18] * z[108];
z[105] = z[105] + -z[109];
z[110] = -z[17] + z[81] + -z[97];
z[110] = T(3) * z[110];
z[111] = z[29] * z[110];
z[112] = T(4) * z[11];
z[113] = T(3) * z[10];
z[114] = z[112] + z[113];
z[115] = -z[12] + z[94];
z[116] = T(9) * z[15] + -z[114] + z[115];
z[116] = z[2] * z[116];
z[117] = z[15] + z[66];
z[118] = -z[12] + z[106];
z[119] = z[117] + -z[118];
z[120] = T(2) * z[11];
z[121] = -z[119] + -z[120];
z[86] = z[86] * z[121];
z[68] = z[68] + -z[118];
z[121] = z[68] + -z[94];
z[122] = T(4) * z[20];
z[123] = z[121] + z[122];
z[124] = z[15] + z[123];
z[125] = T(7) * z[10];
z[126] = -z[124] + -z[125];
z[126] = z[21] * z[126];
z[127] = -z[73] + z[120];
z[128] = T(2) * z[20];
z[129] = z[13] + z[51] + -z[127] + -z[128];
z[130] = T(2) * z[6];
z[129] = z[129] * z[130];
z[131] = z[13] + -z[15];
z[131] = T(6) * z[131];
z[132] = -(z[27] * z[131]);
z[86] = z[86] + z[101] + z[105] + z[111] + z[116] + z[126] + z[129] + z[132];
z[86] = z[9] * z[86];
z[101] = T(4) * z[12];
z[111] = z[101] + -z[106];
z[116] = -z[13] + z[125];
z[126] = T(3) * z[20];
z[129] = T(8) * z[47];
z[132] = T(3) * z[11];
z[133] = z[69] + -z[111] + z[116] + -z[126] + -z[129] + z[132];
z[133] = z[6] * z[133];
z[134] = z[20] + z[69];
z[135] = -z[10] + z[47];
z[136] = -z[15] + z[134] + T(-3) * z[135];
z[137] = -(z[29] * z[136]);
z[138] = z[14] + z[15];
z[139] = T(2) * z[47];
z[140] = -z[20] + z[139];
z[141] = z[138] + -z[140];
z[142] = T(2) * z[5];
z[143] = z[141] * z[142];
z[144] = T(2) * z[4];
z[145] = -(z[119] * z[144]);
z[137] = z[137] + z[143] + z[145];
z[99] = z[71] + z[99] + -z[132];
z[143] = z[24] * z[99];
z[145] = T(4) * z[15];
z[146] = z[132] + -z[145];
z[107] = -z[101] + -z[107] + z[146];
z[147] = z[18] * z[107];
z[148] = z[69] + z[129];
z[149] = T(-13) * z[15] + -z[121] + -z[125] + z[148];
z[149] = z[21] * z[149];
z[150] = -z[13] + z[72];
z[151] = z[19] + z[97] + z[150];
z[152] = z[93] * z[151];
z[133] = z[133] + T(2) * z[137] + z[143] + z[147] + z[149] + z[152];
z[133] = z[46] * z[133];
z[137] = z[44] + z[45];
z[143] = T(3) * z[137];
z[149] = T(3) * z[42] + T(6) * z[43] + z[143];
z[152] = T(4) * z[41];
z[153] = z[149] + -z[152];
z[154] = z[39] + z[40];
z[155] = T(3) * z[49];
z[156] = -z[154] + z[155];
z[157] = T(4) * z[38];
z[158] = z[153] + z[156] + -z[157];
z[159] = z[18] * z[158];
z[160] = T(5) * z[40];
z[149] = z[149] + z[152] + -z[160];
z[161] = T(5) * z[38] + T(8) * z[39] + -z[149];
z[162] = z[29] * z[161];
z[163] = T(5) * z[39] + -z[155];
z[149] = T(8) * z[38] + -z[149] + z[163];
z[164] = -(z[24] * z[149]);
z[165] = T(2) * z[41];
z[166] = -z[38] + -z[154] + z[165];
z[167] = z[4] * z[166];
z[168] = T(4) * z[167];
z[169] = z[63] * z[166];
z[170] = z[38] + -z[39] + -z[49];
z[61] = z[61] * z[170];
z[171] = prod_pow(z[54], 2);
z[61] = z[61] + -z[159] + z[162] + z[164] + -z[168] + z[169] + T(3) * z[171];
z[61] = z[50] * z[61];
z[162] = z[33] * z[158];
z[164] = z[48] * z[170];
z[169] = z[162] + T(3) * z[164];
z[68] = z[20] + z[68];
z[172] = T(10) * z[10];
z[173] = z[68] + z[172];
z[174] = T(6) * z[47];
z[175] = T(3) * z[19];
z[176] = T(7) * z[15] + z[173] + -z[174] + -z[175];
z[176] = z[46] * z[176];
z[177] = z[50] * z[158];
z[178] = T(3) * z[15];
z[179] = z[175] + -z[178];
z[180] = -z[122] + z[179];
z[181] = -z[11] + z[14];
z[182] = z[101] + -z[180] + z[181];
z[182] = z[1] * z[182];
z[183] = T(3) * z[14];
z[173] = -z[146] + z[173] + -z[183];
z[173] = z[9] * z[173];
z[173] = z[169] + z[173] + z[176] + z[177] + z[182];
z[173] = z[3] * z[173];
z[176] = T(4) * z[39];
z[177] = z[38] + z[40];
z[153] = -z[153] + z[176] + z[177];
z[182] = z[33] * z[153];
z[184] = z[32] * z[161];
z[185] = z[182] + z[184];
z[185] = z[2] * z[185];
z[186] = z[32] * z[149];
z[187] = -z[162] + z[186];
z[187] = z[21] * z[187];
z[188] = z[33] * z[149];
z[186] = z[186] + z[188];
z[188] = -(z[24] * z[186]);
z[189] = -(z[29] * z[184]);
z[190] = T(2) * z[32];
z[191] = T(3) * z[33];
z[192] = -z[190] + -z[191];
z[193] = T(3) * z[50];
z[194] = T(7) * z[37];
z[192] = T(4) * z[192] + z[193] + z[194];
z[195] = T(2) * z[57];
z[192] = z[192] * z[195];
z[196] = T(3) * z[54];
z[197] = z[37] * z[196];
z[198] = z[32] + z[33];
z[199] = z[54] * z[198];
z[200] = -(z[50] * z[196]);
z[199] = -z[197] + T(8) * z[199] + z[200];
z[200] = T(-5) * z[32] + T(-2) * z[33] + z[50];
z[200] = z[56] * z[200];
z[199] = T(2) * z[199] + z[200];
z[199] = z[56] * z[199];
z[200] = z[12] / T(2);
z[201] = z[10] + z[200];
z[202] = z[11] / T(2);
z[203] = z[201] + z[202];
z[204] = (T(-11) * z[14]) / T(2) + (T(391) * z[15]) / T(6) + (T(451) * z[16]) / T(6) + (T(212) * z[17]) / T(3) + T(-31) * z[203];
z[204] = -z[128] + z[204] / T(5);
z[205] = T(4) * z[32];
z[204] = (T(-31) * z[13]) / T(30) + (T(19) * z[33]) / T(2) + (T(-298) * z[52]) / T(45) + -z[194] + z[204] / T(3) + z[205];
z[206] = prod_pow(z[7], 2);
z[204] = z[204] * z[206];
z[207] = z[15] + z[16];
z[203] = (T(-3) * z[14]) / T(2) + (T(26) * z[17]) / T(3) + T(-3) * z[203] + (T(43) * z[207]) / T(6);
z[194] = (T(-3) * z[13]) / T(10) + T(-12) * z[32] + T(-14) * z[33] + (T(-34) * z[52]) / T(15) + z[194] + z[203] / T(5);
z[171] = z[171] * z[194];
z[168] = -(z[33] * z[168]);
z[194] = z[33] * z[170];
z[194] = -z[164] + z[194];
z[203] = z[93] * z[194];
z[61] = z[61] + z[64] + z[86] + z[133] + z[168] + z[171] + z[173] + z[185] + z[187] + z[188] + z[189] + z[192] + z[199] + z[203] + z[204] / T(3);
z[61] = z[7] * z[61];
z[64] = T(4) * z[17];
z[86] = T(3) * z[16];
z[88] = z[13] + T(4) * z[52] + -z[64] + z[73] + -z[86] + z[88] + -z[178];
z[88] = z[53] * z[88] * z[196];
z[133] = z[11] + z[12];
z[168] = z[73] + z[133];
z[171] = z[13] + z[14];
z[173] = z[168] + z[171];
z[173] = T(-52) * z[17] + T(68) * z[52] + T(9) * z[173] + T(-43) * z[207];
z[173] = z[55] * z[173];
z[88] = z[88] + (T(4) * z[173]) / T(5);
z[61] = z[61] + T(2) * z[88];
z[61] = int_to_imaginary<T>(1) * z[61];
z[88] = (T(-5) * z[16]) / T(2) + -z[183] + z[200];
z[173] = T(4) * z[47];
z[185] = (T(3) * z[19]) / T(2);
z[187] = z[173] + z[185];
z[188] = T(8) * z[15];
z[189] = (T(3) * z[13]) / T(2) + z[188];
z[127] = T(6) * z[20] + -z[88] + -z[94] + -z[127] + z[187] + -z[189];
z[127] = z[3] * z[127];
z[192] = T(5) * z[14];
z[196] = z[122] + z[192];
z[199] = T(5) * z[10];
z[188] = z[188] + z[199];
z[203] = T(4) * z[16];
z[204] = z[188] + -z[203];
z[208] = -z[12] + z[13] + z[129];
z[209] = z[112] + -z[196] + z[204] + -z[208];
z[209] = z[21] * z[209];
z[210] = z[114] + z[122];
z[211] = T(3) * z[13];
z[212] = -z[77] + z[129] + -z[210] + z[211];
z[212] = z[6] * z[212];
z[213] = T(2) * z[12];
z[214] = -z[51] + z[213];
z[215] = z[120] + z[214];
z[215] = z[16] + -z[65] + z[178] + T(-2) * z[215];
z[216] = z[18] * z[215];
z[217] = T(2) * z[13];
z[218] = T(2) * z[14];
z[219] = z[217] + z[218];
z[220] = -z[10] + z[17];
z[221] = T(2) * z[16] + -z[219] + T(3) * z[220];
z[221] = z[2] * z[221];
z[222] = -z[14] + -z[16] + z[75] + -z[128];
z[222] = z[142] * z[222];
z[221] = z[221] + z[222];
z[222] = -z[20] + z[118];
z[179] = -z[69] + z[179] + -z[222];
z[223] = -z[112] + -z[179];
z[223] = z[4] * z[223];
z[127] = z[127] + z[209] + z[212] + -z[216] + T(2) * z[221] + z[223];
z[127] = z[3] * z[127];
z[112] = -z[10] + z[94] + -z[112] + z[145] + -z[213];
z[209] = z[112] + -z[219];
z[209] = z[2] * z[209];
z[212] = -z[69] + z[145];
z[221] = -z[122] + z[212];
z[223] = -z[11] + z[17] + z[135];
z[223] = z[221] + T(3) * z[223];
z[224] = -(z[29] * z[223]);
z[140] = -z[11] + z[90] + z[140];
z[225] = z[140] * z[142];
z[181] = z[181] + z[222];
z[181] = z[144] * z[181];
z[181] = z[181] + z[209] + z[224] + z[225];
z[209] = -z[113] + z[211];
z[222] = z[106] + z[209];
z[224] = T(5) * z[11];
z[225] = -z[89] + z[129] + z[222] + -z[224];
z[225] = z[6] * z[225];
z[121] = z[10] + T(-8) * z[20] + z[103] + -z[121] + -z[148];
z[121] = z[21] * z[121];
z[148] = -z[120] + z[174] + z[179];
z[148] = z[3] * z[148];
z[91] = -z[12] + z[91] + -z[97];
z[93] = -(z[91] * z[93]);
z[93] = z[93] + z[105] + z[121] + z[148] + T(2) * z[181] + z[225];
z[105] = int_to_imaginary<T>(1) * z[7];
z[93] = z[93] * z[105];
z[121] = z[11] + z[20];
z[148] = z[16] + -z[17];
z[148] = -z[148] / T(2);
z[174] = z[15] / T(2) + z[148];
z[179] = z[19] / T(2) + T(-3) * z[47];
z[181] = z[14] / T(2);
z[225] = (T(8) * z[51]) / T(3);
z[226] = (T(-7) * z[10]) / T(3) + -z[12];
z[121] = (T(7) * z[13]) / T(6) + (T(-17) * z[121]) / T(6) + z[174] + -z[179] + z[181] + z[225] + z[226] / T(2);
z[121] = z[121] * z[206];
z[111] = T(9) * z[11] + z[65] + z[111] + -z[204];
z[111] = z[4] * z[111];
z[204] = -z[10] + z[16];
z[120] = -z[12] + z[74] + -z[120] + -z[204];
z[226] = T(4) * z[2];
z[120] = z[120] * z[226];
z[215] = z[21] * z[215];
z[226] = T(2) * z[18];
z[108] = z[108] * z[226];
z[108] = -z[108] + z[111] + z[120] + z[215];
z[108] = z[18] * z[108];
z[96] = z[96] + -z[204];
z[111] = T(3) * z[30];
z[96] = z[96] * z[111];
z[120] = T(4) * z[31];
z[215] = z[37] * z[120];
z[96] = -z[96] + z[108] + -z[215];
z[108] = -z[114] + -z[123] + z[178] + z[203];
z[108] = z[4] * z[108];
z[123] = -z[16] + z[74];
z[139] = -z[11] + z[81] + -z[123] + z[139];
z[139] = z[63] * z[139];
z[125] = z[86] + z[115] + -z[125];
z[125] = z[125] / T(2);
z[215] = z[13] / T(2);
z[227] = (T(3) * z[20]) / T(2) + -z[215];
z[228] = z[125] + z[218] + -z[227];
z[228] = z[21] * z[228];
z[139] = -z[108] + z[139] + z[228];
z[139] = z[21] * z[139];
z[140] = z[5] * z[140];
z[77] = z[77] + z[90];
z[228] = z[2] * z[77];
z[228] = -z[140] + z[228];
z[229] = z[11] + z[101];
z[222] = z[20] + -z[69] + -z[222] + z[229];
z[222] = z[4] * z[222];
z[77] = z[21] * z[77];
z[77] = z[77] + -z[109] + z[222] + T(4) * z[228];
z[77] = z[6] * z[77];
z[112] = -z[112] + z[203] + -z[219];
z[222] = prod_pow(z[2], 2);
z[112] = z[112] * z[222];
z[228] = (T(3) * z[17]) / T(2);
z[106] = z[20] / T(2) + z[106] + -z[228];
z[230] = z[106] + z[213];
z[231] = -z[10] / T(2) + -z[13] + z[185];
z[232] = -z[146] + -z[218] + -z[230] + z[231];
z[232] = z[4] * z[232];
z[233] = -z[16] + z[171];
z[234] = z[2] * z[233];
z[235] = -(z[5] * z[233]);
z[235] = z[234] + z[235];
z[232] = z[232] + T(4) * z[235];
z[232] = z[4] * z[232];
z[235] = -(z[23] * z[104]);
z[236] = -z[12] + z[16];
z[81] = -z[15] + z[81] + z[236];
z[81] = z[22] * z[81];
z[140] = -z[140] + T(2) * z[234];
z[140] = z[140] * z[142];
z[138] = z[19] + z[20] + -z[90] + -z[138];
z[234] = T(3) * z[25];
z[138] = z[138] * z[234];
z[237] = T(3) * z[26];
z[91] = z[91] * z[237];
z[233] = z[12] + -z[97] + -z[233];
z[233] = z[8] * z[233];
z[223] = z[28] * z[223];
z[238] = z[34] * z[220];
z[77] = z[77] + T(5) * z[81] + z[91] + z[93] + z[96] + z[112] + z[121] + z[127] + z[138] + z[139] + z[140] + T(-2) * z[223] + z[232] + T(8) * z[233] + z[235] + T(-6) * z[238];
z[77] = z[35] * z[77];
z[81] = -z[64] + z[229];
z[91] = z[86] + -z[113];
z[70] = -z[70] + -z[81] + z[91] + z[178];
z[93] = z[2] * z[70];
z[70] = z[5] * z[70];
z[112] = T(7) * z[11] + z[12] + z[113] + -z[175];
z[64] = (T(-11) * z[13]) / T(2) + (T(-5) * z[15]) / T(2) + z[64] + z[112] / T(2) + -z[128] + -z[218];
z[64] = z[4] * z[64];
z[64] = z[64] + z[70] + -z[93];
z[64] = z[4] * z[64];
z[70] = z[114] + -z[211];
z[112] = -z[70] + -z[87] + z[122];
z[112] = z[4] * z[112];
z[114] = -z[5] + -z[62];
z[114] = z[67] * z[114];
z[121] = T(2) * z[0];
z[127] = z[100] * z[121];
z[138] = z[87] + z[90];
z[138] = z[2] * z[138];
z[90] = -z[87] + z[90];
z[90] = z[79] * z[90];
z[90] = z[90] + z[112] + z[114] + z[127] + z[138];
z[90] = z[6] * z[90];
z[112] = T(4) * z[0];
z[78] = T(-13) * z[12] + -z[78] + z[86] + z[103] + z[112] + -z[192] + -z[224];
z[78] = z[8] * z[78];
z[103] = z[11] + -z[74] + z[213] + z[220];
z[103] = z[2] * z[103];
z[114] = z[74] + z[95] + T(-2) * z[133];
z[114] = z[4] * z[114];
z[103] = z[103] + z[114];
z[76] = -z[76] + -z[91] + z[128] + z[217];
z[91] = -(z[21] * z[76]);
z[114] = -(z[18] * z[67]);
z[91] = z[91] + T(2) * z[103] + z[114];
z[91] = z[91] * z[226];
z[103] = T(-9) * z[12] + -z[199];
z[103] = -z[11] + (T(11) * z[15]) / T(2) + z[86] + z[103] / T(2) + -z[219] + z[228];
z[103] = z[103] * z[222];
z[67] = -(z[67] * z[142]);
z[67] = z[67] + -z[93];
z[67] = z[5] * z[67];
z[93] = z[10] + -z[86] + -z[213] + z[217] + -z[218];
z[93] = z[21] * z[93];
z[82] = z[12] + z[74] + -z[82];
z[82] = z[5] * z[82];
z[95] = z[4] * z[95];
z[82] = z[82] + z[95];
z[82] = T(4) * z[82] + z[93];
z[82] = z[21] * z[82];
z[93] = -z[2] + z[5];
z[95] = z[93] + z[144];
z[95] = z[4] * z[95];
z[114] = z[2] * z[5];
z[95] = z[95] + -z[114] + -z[222];
z[95] = z[95] * z[121];
z[121] = -z[17] + z[171];
z[138] = -z[11] + z[121] + -z[204];
z[139] = T(3) * z[34];
z[138] = z[138] * z[139];
z[95] = z[95] + -z[138];
z[117] = -z[14] + -z[19] + z[117];
z[117] = z[117] * z[234];
z[138] = -(z[120] * z[198]);
z[140] = -z[12] + z[204];
z[140] = T(3) * z[140] + z[221];
z[140] = z[22] * z[140];
z[92] = -(z[92] * z[237]);
z[84] = -(z[28] * z[84]);
z[144] = z[30] * z[204];
z[64] = z[64] + z[67] + z[78] + z[82] + z[84] + z[90] + z[91] + z[92] + z[95] + z[103] + z[117] + z[138] + T(2) * z[140] + T(-6) * z[144];
z[64] = z[1] * z[64];
z[67] = T(-6) * z[14] + (T(-5) * z[17]) / T(2) + T(2) * z[72] + z[86] + -z[126] + -z[185] + z[189] + z[202];
z[67] = z[1] * z[67];
z[78] = T(10) * z[15] + (T(3) * z[16]) / T(2) + -z[113] + -z[187] + -z[192] + -z[200] + z[215];
z[78] = z[46] * z[78];
z[82] = -z[13] + z[80];
z[84] = (T(5) * z[11]) / T(2);
z[88] = z[74] + z[82] + -z[84] + -z[88] + -z[228];
z[88] = z[9] * z[88];
z[90] = T(2) * z[39];
z[91] = T(2) * z[43];
z[92] = z[42] + z[137];
z[103] = z[90] + -z[91] + -z[92] + z[177];
z[103] = z[103] * z[193];
z[137] = (T(3) * z[42]) / T(2) + T(3) * z[43];
z[138] = z[137] + -z[165];
z[140] = z[143] + z[156];
z[140] = T(-2) * z[38] + z[138] + z[140] / T(2);
z[144] = z[33] * z[140];
z[156] = z[137] + z[165];
z[160] = -z[143] + z[160];
z[171] = (T(-5) * z[38]) / T(2) + z[156] + -z[160] / T(2) + -z[176];
z[171] = z[32] * z[171];
z[160] = z[160] + z[163];
z[156] = -z[156] + z[157] + z[160] / T(2);
z[157] = z[37] * z[156];
z[160] = (T(3) * z[164]) / T(2);
z[67] = z[67] + z[78] + z[88] + z[103] + -z[144] + z[157] + z[160] + z[171];
z[67] = z[3] * z[67];
z[69] = z[13] + z[69] + -z[81] + z[89] + -z[188];
z[69] = z[5] * z[69];
z[76] = z[76] * z[226];
z[78] = -z[121] + T(-4) * z[133] + z[178];
z[78] = z[2] * z[78];
z[81] = -z[11] + z[101] + z[134] + -z[209];
z[81] = z[6] * z[81];
z[88] = z[14] + z[180] + z[229];
z[88] = z[4] * z[88];
z[75] = z[17] + z[20] + -z[75] + z[218];
z[75] = z[75] * z[79];
z[69] = z[69] + z[75] + z[76] + z[78] + z[81] + z[88] + -z[127];
z[69] = z[1] * z[69];
z[75] = z[203] + -z[211];
z[76] = -z[10] + z[11];
z[78] = z[15] + z[75] + T(4) * z[76] + z[94] + -z[183];
z[78] = z[2] * z[78];
z[75] = -z[75] + z[76] + -z[126] + -z[145];
z[75] = z[5] * z[75];
z[76] = z[73] + z[224];
z[81] = -z[68] + z[74] + -z[76] + z[183];
z[81] = z[4] * z[81];
z[88] = -z[20] + z[183];
z[101] = z[12] + -z[88] + -z[146] + z[172] + z[217];
z[101] = z[6] * z[101];
z[103] = -z[12] + z[183];
z[121] = -z[13] + -z[103] + -z[203] + z[210];
z[121] = z[21] * z[121];
z[75] = z[75] + z[78] + z[81] + z[101] + z[121] + z[127] + -z[216];
z[75] = z[9] * z[75];
z[78] = T(13) * z[10] + -z[128] + z[145] + z[192] + -z[208];
z[78] = z[6] * z[78];
z[68] = -z[15] + -z[68] + -z[73] + z[175];
z[68] = z[4] * z[68];
z[81] = T(-12) * z[15] + z[129] + z[150] + z[196];
z[81] = z[21] * z[81];
z[101] = z[10] + z[214];
z[65] = z[15] + z[65] + -z[86] + T(2) * z[101];
z[101] = z[18] * z[65];
z[80] = z[80] + -z[212];
z[80] = z[80] * z[142];
z[68] = z[68] + z[78] + z[80] + z[81] + z[101];
z[68] = z[46] * z[68];
z[78] = z[4] * z[158];
z[80] = z[78] + -z[159];
z[81] = -(z[5] * z[161]);
z[101] = -(z[6] * z[153]);
z[79] = z[79] * z[166];
z[79] = z[79] + -z[80] + z[81] + z[101];
z[79] = z[50] * z[79];
z[81] = z[33] * z[166];
z[101] = z[32] * z[166];
z[81] = z[81] + z[101];
z[62] = z[62] * z[81];
z[81] = z[2] * z[153];
z[121] = z[32] * z[81];
z[128] = -(z[4] * z[169]);
z[129] = T(-4) * z[101] + z[162];
z[129] = z[21] * z[129];
z[133] = z[6] * z[162];
z[134] = z[21] + z[93];
z[134] = z[134] * z[166];
z[134] = T(4) * z[134] + z[159];
z[134] = z[37] * z[134];
z[146] = z[63] * z[101];
z[67] = z[62] + z[67] + z[68] + z[69] + z[75] + z[79] + z[121] + z[128] + z[129] + z[133] + z[134] + -z[146];
z[67] = z[3] * z[67];
z[68] = z[76] + -z[88] + -z[118] + z[145] + -z[217];
z[68] = z[4] * z[68];
z[69] = z[103] + -z[145];
z[70] = z[69] + -z[70];
z[70] = z[2] * z[70];
z[69] = z[69] + -z[116] + -z[122];
z[69] = z[21] * z[69];
z[75] = T(3) * z[5] + -z[130];
z[76] = z[11] + z[82];
z[75] = z[75] * z[76];
z[68] = z[68] + z[69] + z[70] + z[75] + -z[109] + -z[127];
z[68] = z[6] * z[68];
z[69] = z[85] + -z[183];
z[70] = T(5) * z[16] + -z[178];
z[73] = z[69] + z[70] + -z[73] + z[102] + -z[112] + -z[211];
z[73] = z[8] * z[73];
z[75] = (T(-7) * z[20]) / T(2) + -z[74] + z[125] + z[215];
z[75] = z[21] * z[75];
z[76] = z[10] + z[16] + z[97];
z[76] = z[63] * z[76];
z[75] = z[75] + z[76] + -z[108];
z[75] = z[21] * z[75];
z[76] = z[10] + z[12];
z[76] = (T(-7) * z[15]) / T(2) + z[16] + z[76] / T(2) + z[132] + -z[228];
z[76] = z[76] * z[222];
z[69] = T(-11) * z[10] + -z[69] + z[70] + -z[126];
z[69] = z[22] * z[69];
z[70] = (T(5) * z[13]) / T(2) + (T(9) * z[15]) / T(2) + -z[84] + -z[106] + -z[201];
z[70] = z[4] * z[70];
z[79] = z[83] + z[204];
z[82] = z[79] * z[93];
z[70] = z[70] + z[82];
z[70] = z[4] * z[70];
z[79] = -(z[79] * z[114]);
z[82] = z[28] * z[110];
z[83] = z[26] * z[131];
z[68] = z[68] + z[69] + z[70] + z[73] + z[75] + z[76] + z[79] + z[82] + z[83] + -z[95] + z[96];
z[68] = z[9] * z[68];
z[69] = z[29] * z[32];
z[70] = -(z[2] * z[198]);
z[73] = z[33] + -z[48];
z[75] = z[27] * z[73];
z[76] = z[6] + -z[29];
z[76] = z[50] * z[76];
z[79] = z[3] * z[48];
z[69] = z[69] + z[70] + z[75] + z[76] + z[79];
z[69] = z[69] * z[105];
z[70] = -(z[2] * z[32]);
z[75] = -(z[4] * z[48]);
z[76] = z[5] + z[6];
z[76] = z[50] * z[76];
z[79] = z[32] + z[48];
z[79] = -z[50] + z[79] / T(2);
z[79] = z[3] * z[79];
z[70] = z[70] + z[75] + z[76] + z[79];
z[70] = z[3] * z[70];
z[75] = z[198] * z[222];
z[76] = prod_pow(z[4], 2);
z[79] = -(z[73] * z[76]);
z[75] = z[75] + z[79];
z[79] = z[114] + -z[222];
z[82] = -(z[4] * z[93]);
z[82] = z[79] + z[82];
z[82] = z[37] * z[82];
z[83] = -z[33] + z[37];
z[83] = z[8] * z[83];
z[85] = z[25] * z[48];
z[73] = -(z[26] * z[73]);
z[88] = z[6] * z[33] * z[100];
z[95] = -z[32] + z[37];
z[96] = -(z[34] * z[95]);
z[97] = z[28] * z[32];
z[102] = -(z[5] * z[6]);
z[102] = -z[25] + -z[28] + z[102];
z[102] = z[50] * z[102];
z[69] = z[69] + z[70] + z[73] + z[75] / T(2) + z[82] + -z[83] + z[85] + z[88] + z[96] + z[97] + z[102];
z[70] = -z[48] + z[95];
z[70] = z[70] * z[206];
z[69] = T(3) * z[69] + z[70] / T(2);
z[69] = z[36] * z[69];
z[70] = z[20] + -z[74] + -z[113] + z[173] + -z[218];
z[70] = z[70] * z[142];
z[73] = z[4] * z[107];
z[75] = -z[87] + -z[116];
z[75] = z[21] * z[75];
z[82] = -z[14] + -z[150];
z[82] = z[82] * z[130];
z[70] = z[70] + -z[73] + z[75] + z[82] + z[147];
z[70] = z[6] * z[70];
z[75] = -z[113] + z[236];
z[71] = z[71] + T(3) * z[75] + -z[89] + -z[192];
z[71] = z[22] * z[71];
z[74] = -z[74] + z[132] + -z[230] + -z[231];
z[74] = z[74] * z[76];
z[75] = -z[10] + -z[86] + z[115];
z[75] = z[75] / T(2) + -z[218] + -z[227];
z[75] = z[21] * z[75];
z[82] = -z[14] + -z[20] + T(-2) * z[135] + z[178];
z[63] = z[63] * z[82];
z[82] = -z[10] + z[124];
z[82] = z[4] * z[82];
z[63] = z[63] + z[75] + z[82];
z[63] = z[21] * z[63];
z[65] = -(z[21] * z[65]);
z[75] = -(z[119] * z[226]);
z[65] = z[65] + -z[73] + z[75];
z[65] = z[18] * z[65];
z[73] = prod_pow(z[5], 2);
z[73] = T(2) * z[73];
z[75] = -(z[73] * z[141]);
z[82] = -(z[151] * z[237]);
z[63] = z[63] + z[65] + z[70] + z[71] + z[74] + z[75] + z[82] + z[117];
z[63] = z[46] * z[63];
z[65] = z[12] + (T(-5) * z[13]) / T(3) + (T(7) * z[14]) / T(3) + (T(13) * z[15]) / T(6) + (T(7) * z[20]) / T(6) + -z[84] + -z[148] + z[179] + z[225];
z[65] = z[46] * z[65];
z[70] = z[143] + (T(-7) * z[154]) / T(3) + z[155];
z[70] = (T(-8) * z[38]) / T(3) + (T(-2) * z[41]) / T(3) + T(-2) * z[54] + z[70] / T(2) + z[137];
z[70] = z[50] * z[70];
z[71] = (T(-13) * z[13]) / T(6) + -z[72] / T(2) + -z[181] + z[225];
z[71] = z[9] * z[71];
z[72] = -(z[32] * z[170]);
z[72] = z[72] + -z[164];
z[74] = z[49] + z[92] + z[154];
z[75] = -z[43] + z[165];
z[82] = z[74] / T(2) + -z[75];
z[84] = z[33] * z[82];
z[85] = (T(-19) * z[10]) / T(3) + z[19];
z[85] = (T(-11) * z[11]) / T(3) + (T(19) * z[13]) / T(6) + z[14] + (T(8) * z[20]) / T(3) + z[85] / T(2) + -z[174];
z[85] = z[1] * z[85];
z[86] = T(-19) * z[32] + T(-5) * z[33];
z[86] = z[54] * z[86];
z[87] = (T(-7) * z[54]) / T(3) + z[170] / T(2);
z[87] = z[37] * z[87];
z[88] = (T(41) * z[32]) / T(2) + T(11) * z[33] + T(2) * z[37];
z[88] = z[50] / T(2) + z[88] / T(3);
z[88] = z[56] * z[88];
z[65] = z[65] + z[70] + z[71] + z[72] / T(2) + z[84] + z[85] + z[86] / T(6) + z[87] + z[88];
z[65] = z[7] * z[65];
z[70] = T(8) * z[52];
z[71] = -z[14] + -z[70] + z[94] + z[123] + z[126] + T(2) * z[168] + z[217];
z[71] = z[53] * z[71];
z[65] = z[65] + T(2) * z[71];
z[65] = z[7] * z[65];
z[71] = z[5] * z[166];
z[71] = z[71] + -z[167];
z[72] = T(3) * z[82];
z[82] = z[21] * z[72];
z[71] = T(4) * z[71] + -z[82];
z[71] = z[21] * z[71];
z[82] = z[22] * z[149];
z[71] = z[71] + -z[82];
z[84] = z[21] * z[158];
z[85] = -(z[166] * z[226]);
z[78] = z[78] + -z[84] + z[85];
z[78] = z[18] * z[78];
z[85] = z[5] * z[153];
z[80] = z[80] + z[85];
z[80] = z[6] * z[80];
z[76] = -(z[76] * z[140]);
z[86] = z[28] * z[161];
z[73] = -(z[73] * z[166]);
z[87] = -(z[170] * z[234]);
z[73] = (T(7) * z[58]) / T(4) + -z[71] + z[73] + z[76] + z[78] + z[80] + z[86] + z[87];
z[73] = z[50] * z[73];
z[76] = -z[81] + z[85];
z[76] = z[4] * z[76];
z[78] = z[2] * z[166];
z[78] = z[78] + -z[167];
z[78] = T(4) * z[78] + -z[84];
z[78] = z[18] * z[78];
z[79] = -(z[79] * z[153]);
z[80] = T(2) * z[60];
z[84] = T(4) * z[59];
z[85] = prod_pow(z[54], 3);
z[71] = (T(401) * z[58]) / T(9) + -z[71] + z[76] + z[78] + z[79] + -z[80] + -z[84] + (T(-7) * z[85]) / T(3);
z[71] = z[37] * z[71];
z[76] = -(z[46] * z[99]);
z[78] = -(z[9] * z[104]);
z[79] = z[50] * z[149];
z[86] = -(z[1] * z[98]);
z[76] = z[76] + z[78] + z[79] + z[86] + z[186];
z[76] = z[23] * z[76];
z[78] = z[33] * z[81];
z[79] = -(z[4] * z[182]);
z[81] = -(z[21] * z[162]);
z[78] = z[78] + z[79] + z[81];
z[78] = z[6] * z[78];
z[79] = -z[38] + -z[40] + z[143];
z[79] = z[79] / T(2) + -z[90] + z[138];
z[79] = z[33] * z[79];
z[81] = z[32] * z[156];
z[81] = -z[79] + z[81] + z[160];
z[81] = z[4] * z[81];
z[86] = -(z[93] * z[101]);
z[81] = z[81] + T(4) * z[86];
z[81] = z[4] * z[81];
z[86] = z[32] * z[158];
z[86] = z[86] + z[162];
z[86] = z[4] * z[86];
z[72] = -(z[32] * z[72]);
z[72] = z[72] + -z[144];
z[72] = z[21] * z[72];
z[72] = z[72] + z[86] + z[146];
z[72] = z[21] * z[72];
z[86] = z[17] + z[207];
z[86] = z[86] * z[120];
z[70] = -(z[31] * z[70]);
z[70] = z[70] + z[86];
z[70] = z[70] * z[166];
z[80] = (T(17) * z[58]) / T(18) + -z[80] + z[82];
z[80] = z[33] * z[80];
z[82] = z[92] + z[177];
z[75] = -z[75] + z[82] / T(2);
z[75] = z[32] * z[75];
z[75] = T(3) * z[75] + z[79];
z[75] = z[75] * z[222];
z[79] = -z[21] + z[100];
z[62] = z[62] * z[79];
z[79] = z[83] * z[161];
z[83] = z[91] + -z[152];
z[82] = z[82] + z[83];
z[82] = -(z[82] * z[95] * z[139]);
z[86] = z[46] * z[136];
z[86] = T(-2) * z[86] + -z[184];
z[86] = z[28] * z[86];
z[74] = z[74] + z[83];
z[83] = z[37] + z[50] + -z[198];
z[74] = z[74] * z[83];
z[66] = z[17] + -z[66] + -z[236];
z[66] = z[46] * z[66];
z[66] = z[66] + z[74];
z[66] = z[66] * z[111];
z[74] = z[2] * z[146];
z[83] = z[164] * z[234];
z[87] = -(z[194] * z[237]);
z[88] = z[33] + -z[205];
z[85] = z[85] * z[88];
z[88] = -z[191] + -z[205];
z[84] = z[84] * z[88];
z[88] = -z[33] + -z[205];
z[88] = z[54] * z[88];
z[88] = z[88] + -z[197];
z[88] = z[88] * z[195];
z[89] = (T(349) * z[58]) / T(36) + T(-4) * z[60];
z[89] = z[32] * z[89];
z[90] = z[33] + z[37] + z[190];
z[90] = prod_pow(z[56], 3) * z[90];
return z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[74] + z[75] + z[76] + z[77] + z[78] + z[79] + z[80] + z[81] + z[82] + z[83] + z[84] + z[85] / T(3) + z[86] + z[87] + z[88] + z[89] + (T(8) * z[90]) / T(3);
}



template IntegrandConstructorType<double> f_4_371_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_371_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_371_construct (const Kin<qd_real>&);
#endif

}