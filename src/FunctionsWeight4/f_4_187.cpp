#include "f_4_187.h"

namespace PentagonFunctions {

template <typename T> T f_4_187_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_187_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_187_W_22 (const Kin<T>& kin) {
        c[0] = (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(2)) * kin.v[0] + (-kin.v[4] / T(4) + T(1) / T(2) + kin.v[3] / T(8)) * kin.v[3] + (-kin.v[2] / T(4) + -kin.v[3] / T(4) + T(-1) / T(2) + kin.v[0] / T(2) + kin.v[1] / T(8) + kin.v[4] / T(4)) * kin.v[1] + (T(-1) / T(2) + (T(-3) * kin.v[2]) / T(8) + kin.v[3] / T(4) + (T(3) * kin.v[4]) / T(4)) * kin.v[2] + (T(1) / T(2) + (T(-3) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = (T(1) / T(2) + (T(-5) * kin.v[3]) / T(2) + (T(-11) * kin.v[4]) / T(2)) * kin.v[3] + (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(2)) * kin.v[0] + kin.v[4] * (T(1) / T(2) + T(-3) * kin.v[4]) + kin.v[1] * (T(-1) / T(2) + kin.v[0] / T(2) + (T(-5) * kin.v[1]) / T(2) + (T(-11) * kin.v[2]) / T(2) + (T(11) * kin.v[4]) / T(2) + T(5) * kin.v[3]) + kin.v[2] * (T(-1) / T(2) + (T(11) * kin.v[3]) / T(2) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(-7) * kin.v[2]) / T(4) + (T(5) * kin.v[3]) / T(2) + (T(7) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + ((T(-3) * kin.v[3]) / T(4) + (T(-5) * kin.v[4]) / T(2) + T(1)) * kin.v[3] + kin.v[1] * ((T(-5) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(5) * kin.v[4]) / T(2) + T(-1) + kin.v[0] + (T(-3) / T(4) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[3])) + ((T(-7) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[3] * (T(4) + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-4) * kin.v[4]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(4) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[3]) + T(4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + rlog(kin.v[3]) * (kin.v[2] * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(4) + T(-4) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[1] + T(4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + T(-4) * kin.v[4]) + kin.v[3] * (T(-4) + T(4) * kin.v[4]) + kin.v[4] * (T(-4) + T(4) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(-11) * kin.v[2]) / T(2) + (T(11) * kin.v[3]) / T(2) + (T(11) * kin.v[4]) / T(2)) * kin.v[0] + (T(11) / T(2) + (T(33) * kin.v[2]) / T(8) + (T(-11) * kin.v[3]) / T(4) + (T(-33) * kin.v[4]) / T(4)) * kin.v[2] + (T(-11) / T(2) + (T(-11) * kin.v[3]) / T(8) + (T(11) * kin.v[4]) / T(4)) * kin.v[3] + kin.v[1] * (T(11) / T(2) + (T(-11) * kin.v[0]) / T(2) + (T(11) * kin.v[2]) / T(4) + (T(11) * kin.v[3]) / T(4) + (T(-11) * kin.v[4]) / T(4) + (T(-11) / T(8) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3])) + (T(-11) / T(2) + (T(33) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4])));
c[2] = kin.v[1] / T(2) + kin.v[2] / T(2) + -kin.v[3] / T(2) + -kin.v[4] / T(2);
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(5) * kin.v[2]) / T(2) + (T(-5) * kin.v[3]) / T(2) + (T(-5) * kin.v[4]) / T(2) + (T(5) / T(2) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) + bc<T>[0] * rlog(kin.v[3]) * int_to_imaginary<T>(1) * (T(2) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[0] * rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * ((abb[3] * abb[10] * T(5)) / T(2) + abb[3] * (-abb[4] / T(2) + abb[9] * T(-3)) + abb[2] * (abb[2] * (abb[9] + abb[4] / T(2) + (abb[10] * T(3)) / T(2) + abb[8] * T(-2)) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2) + abb[9] * bc<T>[0] * int_to_imaginary<T>(2)) + abb[2] * abb[7] * (abb[10] * T(-2) + abb[8] * T(2) + abb[9] * T(2)) + abb[5] * (abb[2] * abb[7] * T(-2) + abb[2] * (bc<T>[0] * int_to_imaginary<T>(-2) + abb[2] * T(2))) + abb[6] * (abb[2] * abb[5] * T(2) + abb[1] * (abb[5] * T(-2) + abb[10] * T(-2) + abb[8] * T(2) + abb[9] * T(2)) + abb[2] * (abb[8] * T(-2) + abb[9] * T(-2) + abb[10] * T(2))) + abb[1] * (abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[10] * bc<T>[0] * int_to_imaginary<T>(2) + abb[5] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[7] * T(2)) + abb[1] * (-abb[4] / T(2) + (abb[10] * T(-3)) / T(2) + -abb[9] + abb[5] * T(-2) + abb[8] * T(2)) + abb[7] * (abb[8] * T(-2) + abb[9] * T(-2) + abb[10] * T(2))));
    }
};
template <typename T> class SpDLog_f_4_187_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_187_W_23 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[3]) * (((T(5) * kin.v[4]) / T(2) + T(2)) * kin.v[4] + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(-2) + T(3) * kin.v[3] + T(-5) * kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-2) + T(-3) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[0] * (-kin.v[3] + T(2) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(-3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(9) * kin.v[4]) / T(2) + T(6)) * kin.v[4] + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(-6) + T(3) * kin.v[3] + T(-9) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(-6) + T(-3) * kin.v[4]) + kin.v[0] * (T(6) + (T(-3) / T(2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-6) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(-5) * kin.v[4]) / T(2) + T(-2)) * kin.v[4] + kin.v[0] * (T(-2) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(2) * kin.v[1] + T(3) * kin.v[2] + kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[1] + T(-2) * kin.v[3]) + T(-3) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(2) + T(3) * kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(2) + T(-3) * kin.v[3] + T(5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(-5) * kin.v[4]) / T(2) + T(-2)) * kin.v[4] + kin.v[0] * (T(-2) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(2) * kin.v[1] + T(3) * kin.v[2] + kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[1] + T(-2) * kin.v[3]) + T(-3) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(2) + T(3) * kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(2) + T(-3) * kin.v[3] + T(5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])));
c[1] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[11] * (abb[12] * (abb[8] * T(-2) + abb[9] * T(-2) + abb[10] * T(2)) + abb[5] * (abb[7] * (abb[2] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) + abb[12] * T(2)) + abb[7] * (abb[10] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2) + abb[9] * bc<T>[0] * int_to_imaginary<T>(2) + abb[7] * abb[10] * T(2) + abb[2] * (abb[10] * T(-2) + abb[8] * T(2) + abb[9] * T(2))) + abb[1] * (abb[1] * abb[10] * T(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[10] * bc<T>[0] * int_to_imaginary<T>(2) + abb[5] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[2] * T(2)) + abb[2] * (abb[8] * T(-2) + abb[9] * T(-2) + abb[10] * T(2))) + abb[6] * (abb[5] * abb[7] * T(2) + abb[1] * (abb[5] * T(-2) + abb[10] * T(-2) + abb[8] * T(2) + abb[9] * T(2)) + abb[7] * (abb[8] * T(-2) + abb[9] * T(-2) + abb[10] * T(2))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_187_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl23 = DLog_W_23<T>(kin),dl9 = DLog_W_9<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),spdl22 = SpDLog_f_4_187_W_22<T>(kin),spdl23 = SpDLog_f_4_187_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,21> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), -rlog(t), rlog(kin.W[3] / kin_path.W[3]), rlog(v_path[3]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[15] / kin_path.W[15]), dl23(t), f_2_1_8(kin_path), dl9(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl20(t), dl4(t), dl16(t), -rlog(t), dl17(t)}
;

        auto result = f_4_187_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_187_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[45];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[16];
z[3] = abb[17];
z[4] = abb[18];
z[5] = abb[20];
z[6] = abb[5];
z[7] = abb[13];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[19];
z[12] = abb[2];
z[13] = abb[6];
z[14] = abb[7];
z[15] = bc<TR>[0];
z[16] = abb[3];
z[17] = abb[12];
z[18] = abb[14];
z[19] = abb[15];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = T(3) * z[10];
z[23] = z[6] + -z[8];
z[24] = -z[9] + z[22] + z[23];
z[24] = z[4] * z[24];
z[25] = z[2] * z[23];
z[26] = z[1] + -z[6];
z[27] = z[5] * z[26];
z[28] = z[1] + -z[8];
z[29] = z[3] * z[28];
z[30] = -z[10] + z[23];
z[31] = z[9] + z[30];
z[31] = z[7] * z[31];
z[24] = z[24] + z[25] + -z[27] + z[29] + -z[31];
z[25] = int_to_imaginary<T>(1) * z[15];
z[27] = -z[13] + z[25];
z[24] = -(z[24] * z[27]);
z[29] = z[9] + -z[10];
z[32] = -z[23] + z[29];
z[33] = z[3] * z[32];
z[34] = z[2] * z[26];
z[35] = z[4] * z[26];
z[33] = z[33] + -z[34] + z[35];
z[33] = z[14] * z[33];
z[35] = T(2) * z[9];
z[36] = z[23] + z[35];
z[36] = z[5] * z[36];
z[37] = z[9] + z[23];
z[38] = T(2) * z[37];
z[39] = z[2] * z[38];
z[36] = z[36] + -z[39];
z[39] = -(z[4] * z[23]);
z[30] = T(3) * z[9] + z[30];
z[30] = z[3] * z[30];
z[30] = z[30] + -z[36] + z[39];
z[30] = z[12] * z[30];
z[24] = z[24] + z[30] + z[33];
z[30] = z[28] + z[35];
z[30] = z[5] * z[30];
z[33] = T(2) * z[6];
z[39] = z[1] + z[8] + -z[33];
z[40] = T(2) * z[10] + -z[39];
z[41] = -z[35] + z[40];
z[41] = z[3] * z[41];
z[42] = (T(-7) * z[1]) / T(2) + T(3) * z[6] + (T(3) * z[10]) / T(2) + T(-2) * z[11] + z[35];
z[42] = z[4] * z[42];
z[43] = z[2] * z[39];
z[30] = z[30] + -z[31] + z[41] + z[42] + z[43];
z[30] = z[0] * z[30];
z[24] = T(2) * z[24] + z[30];
z[24] = z[0] * z[24];
z[30] = -z[1] / T(2) + z[6] + z[9] + z[11];
z[41] = (T(5) * z[10]) / T(2);
z[30] = T(5) * z[20] + z[30] / T(3) + -z[41];
z[30] = z[4] * z[30];
z[42] = (T(5) * z[20]) / T(2);
z[43] = (T(2) * z[1]) / T(3) + -z[6] + -z[42];
z[44] = -z[8] + -z[11] / T(2);
z[44] = (T(2) * z[9]) / T(3) + z[10] / T(6) + -z[43] + z[44] / T(3);
z[44] = z[5] * z[44];
z[41] = z[8] / T(3) + (T(11) * z[9]) / T(6) + -z[41] + z[43];
z[41] = z[3] * z[41];
z[43] = z[1] + z[10];
z[42] = (T(11) * z[6]) / T(6) + T(-2) * z[8] + (T(3) * z[9]) / T(2) + -z[42] + z[43] / T(12);
z[42] = z[2] * z[42];
z[30] = z[30] / T(2) + (T(-8) * z[31]) / T(3) + z[41] + z[42] + z[44];
z[41] = prod_pow(z[15], 2);
z[30] = z[30] * z[41];
z[42] = z[26] + -z[29];
z[42] = z[4] * z[42];
z[39] = -z[35] + z[39];
z[43] = z[5] * z[39];
z[40] = z[3] * z[40];
z[44] = -(z[2] * z[28]);
z[40] = T(3) * z[31] + z[40] + z[42] + z[43] + z[44];
z[40] = z[13] * z[40];
z[42] = z[4] * z[10];
z[37] = z[2] * z[37];
z[43] = z[3] * z[29];
z[37] = -z[31] + z[37] + -z[42] + z[43];
z[37] = z[25] * z[37];
z[43] = z[28] + -z[29];
z[43] = z[4] * z[43];
z[34] = -z[34] + z[43];
z[34] = z[14] * z[34];
z[37] = -z[34] + T(2) * z[37];
z[37] = T(2) * z[37] + z[40];
z[37] = z[13] * z[37];
z[40] = z[3] + -z[5];
z[43] = z[2] + z[40];
z[39] = z[39] * z[43];
z[44] = -z[9] + -z[10] + z[26];
z[44] = z[4] * z[44];
z[31] = z[31] + -z[39] + z[44];
z[31] = T(2) * z[31];
z[39] = -(z[19] * z[31]);
z[44] = -z[2] + z[4];
z[41] = z[41] * z[44];
z[43] = z[4] + -z[43];
z[43] = prod_pow(z[20], 2) * z[43];
z[34] = T(2) * z[34] + z[39] + z[41] / T(2) + z[43];
z[25] = z[25] * z[34];
z[29] = z[4] * z[29];
z[34] = z[3] * z[38];
z[29] = z[29] + -z[34] + z[36];
z[27] = -(z[27] * z[29]);
z[29] = -z[3] + -z[4];
z[29] = z[14] * z[29] * z[32];
z[27] = z[27] + z[29];
z[29] = T(3) * z[1] + -z[10] + -z[33] + -z[35];
z[29] = z[2] * z[29];
z[32] = -z[1] + -z[22];
z[32] = z[9] + z[32] / T(2);
z[32] = z[4] * z[32];
z[29] = z[29] + z[32] + -z[34];
z[29] = z[12] * z[29];
z[27] = T(2) * z[27] + z[29];
z[27] = z[12] * z[27];
z[29] = z[16] * z[35];
z[23] = z[16] * z[23];
z[26] = z[17] * z[26];
z[29] = z[23] + z[26] + z[29];
z[29] = (T(11) * z[21]) / T(4) + T(2) * z[29];
z[29] = -(z[29] * z[40]);
z[22] = -z[22] + -z[28];
z[28] = T(2) * z[17];
z[22] = z[22] * z[28];
z[32] = -z[1] + T(-11) * z[10];
z[32] = z[16] * z[32];
z[28] = T(3) * z[16] + z[28];
z[28] = z[9] * z[28];
z[22] = (T(-23) * z[21]) / T(12) + z[22] + z[28] + z[32] / T(2);
z[22] = z[4] * z[22];
z[28] = -(z[9] * z[16]);
z[23] = T(-2) * z[23] + z[26] + z[28];
z[23] = (T(-25) * z[21]) / T(12) + T(2) * z[23];
z[23] = z[2] * z[23];
z[26] = z[18] * z[31];
z[28] = -z[10] + z[11];
z[28] = z[5] * z[28];
z[28] = z[28] + -z[42];
z[28] = prod_pow(z[14], 2) * z[28];
return z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + T(2) * z[28] + z[29] + z[30] + z[37];
}



template IntegrandConstructorType<double> f_4_187_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_187_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_187_construct (const Kin<qd_real>&);
#endif

}