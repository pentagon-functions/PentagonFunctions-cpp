#include "f_4_183.h"

namespace PentagonFunctions {

template <typename T> T f_4_183_abbreviated (const std::array<T,40>&);

template <typename T> class SpDLog_f_4_183_W_10 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_183_W_10 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[2], 2) * (abb[6] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + prod_pow(abb[1], 2) * (abb[4] * T(-3) + abb[5] * T(-3) + abb[3] * T(3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_183_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_183_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-4) * kin.v[0] + T(2) * kin.v[1] + T(4) * kin.v[2] + T(8) * kin.v[3] + T(-4) * kin.v[4]) + T(4) * kin.v[0] * kin.v[4] + T(-4) * kin.v[2] * kin.v[4] + T(-8) * kin.v[3] * kin.v[4] + T(2) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(-4) + T(-2) * kin.v[1]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = T(-4) * kin.v[2] * kin.v[4] + T(-8) * kin.v[3] * kin.v[4] + T(-10) * prod_pow(kin.v[4], 2) + kin.v[0] * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] * kin.v[4] + T(-4) * kin.v[3] * kin.v[4] + T(-5) * prod_pow(kin.v[4], 2)) + kin.v[1] * ((T(-10) + bc<T>[0] * int_to_imaginary<T>(-5)) * kin.v[1] + T(4) * kin.v[2] + T(8) * kin.v[3] + T(20) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(4) * kin.v[3] + T(10) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-prod_pow(kin.v[4], 2) + T(-2) * kin.v[0] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + T(4) * kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-4) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[1]) * (-prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[2] + kin.v[0] + T(-2) * kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(kin.v[3]) * (-prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[2] + kin.v[0] + T(-2) * kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[2] + kin.v[0] + T(-2) * kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(-kin.v[4]) * (-prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[2] + kin.v[0] + T(-2) * kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(kin.v[2]) * ((T(3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[0] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + T(-6) * kin.v[3] * kin.v[4] + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) * kin.v[0] + T(3) * kin.v[2] + T(6) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[1]) / T(2) + T(-3)) * kin.v[1] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[0] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + T(-6) * kin.v[3] * kin.v[4] + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) * kin.v[0] + T(3) * kin.v[2] + T(6) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[1]) / T(2) + T(-3)) * kin.v[1] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4]));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4];
c[3] = rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[19] * (abb[3] * (-(abb[1] * abb[18]) + abb[20] * T(-2) + abb[1] * (abb[1] + abb[8] + -abb[2] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[1] * abb[18] * (-abb[5] + -abb[6] + -abb[12] + abb[21] * T(-4) + abb[22] * T(-2) + abb[4] * T(3) + abb[11] * T(3)) + abb[1] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(1) + abb[22] * bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[21] * bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * (-abb[5] + -abb[6] + -abb[12] + abb[21] * T(-4) + abb[22] * T(-2) + abb[4] * T(3) + abb[11] * T(3)) + abb[1] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[11] * T(-3) + abb[22] * T(2) + abb[21] * T(4)) + abb[8] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[11] * T(-3) + abb[22] * T(2) + abb[21] * T(4))) + abb[20] * (abb[21] * T(-8) + abb[22] * T(-4) + abb[5] * T(-2) + abb[6] * T(-2) + abb[12] * T(-2) + abb[4] * T(6) + abb[11] * T(6)) + abb[17] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[3] * (abb[1] + abb[2] + abb[18] + -abb[8] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[22] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(3) + abb[21] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[8] * (-abb[5] + -abb[6] + -abb[12] + abb[21] * T(-4) + abb[22] * T(-2) + abb[4] * T(3) + abb[11] * T(3)) + abb[1] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[11] * T(-3) + abb[22] * T(2) + abb[21] * T(4)) + abb[2] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[11] * T(-3) + abb[22] * T(2) + abb[21] * T(4)) + abb[18] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[11] * T(-3) + abb[22] * T(2) + abb[21] * T(4)) + abb[17] * (abb[21] * T(-8) + abb[22] * T(-4) + abb[3] * T(-2) + abb[5] * T(-2) + abb[6] * T(-2) + abb[12] * T(-2) + abb[4] * T(6) + abb[11] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_183_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl18 = DLog_W_18<T>(kin),dl27 = DLog_W_27<T>(kin),dl14 = DLog_W_14<T>(kin),dl12 = DLog_W_12<T>(kin),dl20 = DLog_W_20<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl1 = DLog_W_1<T>(kin),dl16 = DLog_W_16<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),spdl10 = SpDLog_f_4_183_W_10<T>(kin),spdl12 = SpDLog_f_4_183_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,40> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[16] / kin_path.W[16]), dl18(t), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_7(kin_path), f_2_1_8(kin_path), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[3] / kin_path.W[3]), dl27(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl14(t), rlog(-v_path[1]), rlog(v_path[3]), dl12(t), f_2_1_4(kin_path), -rlog(t), rlog(kin.W[15] / kin_path.W[15]), dl20(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl1(t), dl16(t), dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl3(t), dl2(t), dl19(t), dl4(t), dl5(t), dl17(t)}
;

        auto result = f_4_183_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_183_abbreviated(const std::array<T,40>& abb)
{
using TR = typename T::value_type;
T z[118];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[31];
z[4] = abb[34];
z[5] = abb[35];
z[6] = abb[37];
z[7] = abb[38];
z[8] = abb[39];
z[9] = abb[4];
z[10] = abb[5];
z[11] = abb[6];
z[12] = abb[11];
z[13] = abb[12];
z[14] = abb[13];
z[15] = abb[14];
z[16] = abb[15];
z[17] = abb[27];
z[18] = abb[33];
z[19] = abb[21];
z[20] = abb[22];
z[21] = abb[2];
z[22] = abb[8];
z[23] = abb[17];
z[24] = abb[36];
z[25] = abb[18];
z[26] = bc<TR>[0];
z[27] = abb[32];
z[28] = abb[23];
z[29] = abb[26];
z[30] = abb[9];
z[31] = abb[10];
z[32] = abb[30];
z[33] = abb[16];
z[34] = abb[20];
z[35] = abb[24];
z[36] = abb[25];
z[37] = abb[28];
z[38] = abb[29];
z[39] = bc<TR>[3];
z[40] = bc<TR>[1];
z[41] = bc<TR>[2];
z[42] = bc<TR>[4];
z[43] = bc<TR>[9];
z[44] = T(5) * z[11];
z[45] = z[10] + z[13] + -z[44];
z[46] = T(3) * z[41];
z[47] = z[19] / T(3) + z[20] / T(6) + -z[46];
z[48] = z[1] / T(3);
z[45] = z[9] + z[45] / T(6) + -z[47] + -z[48];
z[45] = z[5] * z[45];
z[49] = T(3) * z[9];
z[50] = T(3) * z[12];
z[51] = z[49] + z[50];
z[52] = z[11] + z[13];
z[53] = (T(-5) * z[10]) / T(3) + -z[48] + z[51] + -z[52];
z[53] = -z[20] + z[53] / T(2);
z[53] = -z[19] + z[53] / T(2);
z[53] = z[8] * z[53];
z[54] = z[12] + -z[13];
z[55] = z[9] + -z[11];
z[56] = z[54] + -z[55];
z[57] = z[1] + -z[10];
z[58] = -z[46] + -z[56] / T(2) + (T(-7) * z[57]) / T(3);
z[58] = z[6] * z[58];
z[59] = -z[56] + z[57];
z[60] = z[28] * z[59];
z[61] = z[15] + -z[16];
z[62] = z[29] * z[61];
z[60] = z[60] + -z[62];
z[62] = z[1] + z[11];
z[63] = -z[9] + z[62];
z[64] = -z[10] + -z[54] + z[63];
z[65] = z[2] * z[64];
z[66] = z[14] * z[61];
z[66] = -z[65] + z[66];
z[67] = -z[66] / T(4);
z[68] = T(3) * z[1] + -z[12];
z[69] = z[10] + z[13];
z[70] = T(5) * z[9] + T(-3) * z[11] + -z[68] + z[69];
z[70] = z[70] / T(2);
z[71] = -z[20] + z[70];
z[71] = -z[19] + z[71] / T(2);
z[71] = z[4] * z[71];
z[72] = z[6] + z[7];
z[73] = -z[4] + z[8];
z[74] = z[5] + z[24] + -z[72] + -z[73];
z[75] = z[40] * z[74];
z[76] = -z[12] + z[69];
z[77] = -z[1] + z[76];
z[55] = -z[55] + z[77];
z[78] = z[32] * z[55];
z[48] = -z[10] / T(3) + z[48] + z[54] / T(2);
z[48] = z[3] * z[48];
z[54] = T(7) * z[13];
z[79] = T(2) * z[11];
z[80] = z[54] + -z[79];
z[47] = (T(-2) * z[1]) / T(3) + z[9] / T(2) + (T(5) * z[10]) / T(6) + (T(-5) * z[12]) / T(2) + z[47] + z[80] / T(3);
z[47] = z[7] * z[47];
z[80] = -z[4] + z[8] + T(-3) * z[24];
z[81] = z[18] + -z[27];
z[80] = (T(3) * z[6]) / T(4) + z[80] / T(4) + z[81] / T(9);
z[82] = int_to_imaginary<T>(1) * z[26];
z[80] = z[80] * z[82];
z[77] = z[33] * z[77];
z[77] = T(3) * z[77];
z[83] = z[17] * z[61];
z[84] = z[83] / T(2);
z[85] = z[18] * z[61];
z[46] = z[24] * z[46];
z[45] = z[45] + z[46] + z[47] + z[48] + z[53] + z[58] + z[60] / T(4) + -z[67] + -z[71] + -z[75] + -z[77] + z[78] + z[80] + -z[84] + (T(-3) * z[85]) / T(4);
z[45] = z[26] * z[45];
z[46] = z[63] + -z[76];
z[47] = z[3] * z[46];
z[47] = z[47] + z[60] + -z[83];
z[48] = z[5] * z[55];
z[53] = z[8] * z[59];
z[58] = z[6] * z[46];
z[63] = z[78] + -z[85];
z[48] = -z[47] + z[48] + z[53] + z[58] + -z[63];
z[48] = (T(3) * z[48]) / T(2);
z[53] = -(z[36] * z[48]);
z[58] = z[11] + z[69];
z[68] = z[9] + z[58] + -z[68];
z[76] = T(2) * z[19] + z[20];
z[68] = z[68] / T(2) + -z[76];
z[68] = z[5] * z[68];
z[80] = z[9] + T(-3) * z[10] + z[62];
z[86] = T(-5) * z[12] + T(3) * z[13] + -z[80];
z[86] = z[76] + z[86] / T(2);
z[87] = z[72] * z[86];
z[88] = z[3] / T(2);
z[55] = z[55] * z[88];
z[86] = z[24] * z[86];
z[61] = z[61] / T(2);
z[89] = z[27] * z[61];
z[86] = z[86] + -z[89];
z[55] = z[55] + z[68] + -z[78] / T(2) + -z[84] + -z[86] + z[87];
z[55] = T(3) * z[55];
z[68] = -(z[38] * z[55]);
z[87] = z[51] + -z[62] + -z[69];
z[89] = z[87] / T(2);
z[90] = -z[76] + z[89];
z[91] = z[3] * z[90];
z[92] = z[5] * z[90];
z[93] = (T(3) * z[83]) / T(2);
z[94] = z[92] + -z[93];
z[95] = z[8] * z[90];
z[96] = T(2) * z[10] + -z[62] + z[76];
z[97] = T(2) * z[13] + -z[50] + z[96];
z[98] = T(2) * z[97];
z[98] = -(z[72] * z[98]);
z[99] = z[85] / T(2);
z[100] = z[86] + z[99];
z[98] = z[91] + -z[94] + -z[95] + z[98] + T(3) * z[100];
z[101] = -(z[0] * z[98]);
z[102] = z[6] * z[90];
z[95] = z[95] + z[102];
z[90] = z[7] * z[90];
z[103] = (T(3) * z[63]) / T(2) + z[90];
z[104] = T(2) * z[1] + z[76];
z[105] = -z[58] + z[104];
z[106] = z[3] * z[105];
z[105] = z[5] * z[105];
z[106] = z[95] + z[103] + T(2) * z[105] + z[106];
z[107] = -(z[23] * z[106]);
z[108] = z[3] * z[57];
z[105] = z[105] + z[108];
z[109] = z[7] * z[97];
z[110] = T(2) * z[57];
z[111] = z[6] * z[110];
z[112] = z[8] * z[57];
z[112] = -z[77] + -z[105] + z[109] + -z[111] + z[112];
z[112] = z[25] * z[112];
z[56] = T(-3) * z[56];
z[113] = z[56] + z[57];
z[114] = z[6] * z[113];
z[115] = z[56] + -z[57];
z[116] = z[8] * z[115];
z[117] = z[114] + -z[116];
z[103] = z[103] + z[105] + z[117] / T(2);
z[103] = z[22] * z[103];
z[105] = -(z[41] * z[74]);
z[75] = z[75] / T(2) + z[105];
z[75] = z[40] * z[75];
z[74] = z[42] * z[74];
z[74] = z[74] + z[75];
z[53] = z[53] + z[68] + T(3) * z[74] + z[101] + -z[103] + z[107] + T(2) * z[112];
z[53] = int_to_imaginary<T>(1) * z[53];
z[68] = -(z[39] * z[81]);
z[45] = z[45] + z[53] + T(3) * z[68];
z[45] = z[26] * z[45];
z[53] = z[70] + -z[76];
z[68] = z[5] + z[8];
z[70] = -(z[53] * z[68]);
z[53] = z[4] * z[53];
z[74] = z[64] * z[88];
z[75] = z[12] + z[13] + z[80];
z[75] = z[75] / T(2) + -z[76];
z[75] = z[7] * z[75];
z[61] = z[14] * z[61];
z[61] = z[53] + z[61] + -z[65] / T(2) + z[70] + z[74] + z[75] + z[84] + z[99];
z[61] = z[30] * z[61];
z[65] = -z[20] + z[89];
z[65] = -z[19] + z[65] / T(2);
z[70] = z[8] * z[65];
z[74] = z[6] * z[97];
z[70] = z[70] + z[74];
z[74] = z[85] / T(4);
z[67] = -z[67] + z[71] + z[74];
z[75] = -(z[5] * z[65]);
z[80] = T(5) * z[10];
z[51] = -z[13] + -z[51] + -z[62] + z[80];
z[51] = z[51] / T(4) + z[76];
z[51] = z[3] * z[51];
z[62] = T(5) * z[1] + -z[49];
z[81] = T(7) * z[10] + -z[62];
z[52] = z[50] + T(-5) * z[52] + z[81];
z[52] = z[20] + z[52] / T(2);
z[52] = z[19] + z[52] / T(2);
z[52] = z[7] * z[52];
z[83] = (T(3) * z[83]) / T(4);
z[51] = z[51] + z[52] + T(3) * z[67] + -z[70] + z[75] + z[83];
z[51] = z[0] * z[51];
z[52] = z[23] * z[98];
z[67] = z[25] * z[98];
z[75] = -z[13] + z[96];
z[84] = z[3] + T(2) * z[7];
z[84] = z[75] * z[84];
z[66] = z[66] + z[85];
z[84] = (T(-3) * z[66]) / T(2) + z[84] + z[92] + z[95];
z[84] = z[22] * z[84];
z[51] = z[51] + -z[52] + z[67] + z[84];
z[51] = z[0] * z[51];
z[67] = z[60] + z[66];
z[65] = -(z[7] * z[65]);
z[84] = z[116] / T(4);
z[85] = z[3] / T(4);
z[89] = -(z[85] * z[115]);
z[92] = -z[20] + z[58];
z[92] = -z[1] + -z[19] + z[92] / T(2);
z[92] = z[5] * z[92];
z[65] = z[65] + (T(3) * z[67]) / T(4) + -z[83] + z[84] + z[89] + z[92] + -z[114] / T(4);
z[65] = prod_pow(z[22], 2) * z[65];
z[63] = -z[60] + -z[63];
z[62] = -z[50] + -z[58] + z[62];
z[62] = z[62] / T(4) + z[76];
z[62] = z[5] * z[62];
z[67] = -z[83] + z[109];
z[83] = z[85] * z[113];
z[56] = -z[56] + T(11) * z[57];
z[56] = z[6] * z[56];
z[56] = z[56] / T(4) + z[62] + (T(3) * z[63]) / T(4) + -z[67] + z[77] + z[83] + z[84];
z[56] = z[25] * z[56];
z[52] = -z[52] + z[56] + z[103];
z[52] = z[25] * z[52];
z[56] = z[49] + z[69] + -z[79] + -z[104];
z[62] = T(2) * z[56];
z[62] = -(z[62] * z[68]);
z[62] = T(3) * z[53] + z[62] + z[90] + -z[91] + z[93] + z[102];
z[63] = -z[0] + z[23];
z[62] = z[62] * z[63];
z[63] = z[88] * z[115];
z[69] = z[8] * z[110];
z[60] = (T(3) * z[60]) / T(2) + -z[63] + -z[69] + -z[90] + z[94] + -z[111];
z[63] = -z[22] + z[25] + -z[82];
z[60] = z[60] * z[63];
z[56] = -(z[5] * z[56]);
z[63] = -(z[7] * z[75]);
z[57] = -(z[6] * z[57]);
z[56] = z[56] + z[57] + z[63] + z[69] + z[108];
z[56] = z[21] * z[56];
z[56] = z[56] + z[60] + z[62];
z[56] = z[21] * z[56];
z[57] = -z[71] + z[74] + z[78] / T(4) + z[86];
z[50] = T(5) * z[13] + -z[50] + z[80];
z[60] = T(7) * z[1];
z[49] = -z[11] + z[49] + z[50] + -z[60];
z[49] = z[49] / T(2) + -z[76];
z[49] = z[5] * z[49];
z[60] = T(9) * z[9] + -z[60];
z[62] = T(9) * z[12];
z[58] = -z[58] + z[60] + z[62];
z[63] = T(4) * z[19] + T(2) * z[20];
z[58] = z[58] / T(4) + -z[63];
z[58] = z[3] * z[58];
z[49] = z[49] + T(3) * z[57] + z[58] + -z[67] + -z[70] + -z[77];
z[49] = z[23] * z[49];
z[57] = -(z[22] * z[106]);
z[49] = z[49] + z[57];
z[49] = z[23] * z[49];
z[48] = z[35] * z[48];
z[55] = -(z[37] * z[55]);
z[46] = -(z[8] * z[46]);
z[57] = z[7] * z[64];
z[58] = -(z[6] * z[59]);
z[46] = z[46] + z[47] + z[57] + z[58] + z[66];
z[46] = z[31] * z[46];
z[44] = -z[44] + z[54] + -z[62] + z[81];
z[44] = z[44] / T(2) + z[76];
z[44] = -(z[44] * z[72]);
z[47] = -z[53] + z[100];
z[53] = -z[63] + z[87];
z[53] = z[3] * z[53];
z[50] = T(7) * z[11] + -z[50] + -z[60];
z[50] = z[50] / T(2) + z[76];
z[50] = -(z[50] * z[68]);
z[44] = z[44] + T(3) * z[47] + z[50] + z[53];
z[44] = z[34] * z[44];
z[47] = -z[5] + z[7];
z[47] = z[6] + -z[24] + (T(85) * z[47]) / T(6) + (T(5) * z[73]) / T(2);
z[47] = z[43] * z[47];
return z[44] + z[45] + (T(3) * z[46]) / T(2) + z[47] + z[48] + z[49] + z[51] + z[52] + z[55] + z[56] + T(3) * z[61] + z[65];
}



template IntegrandConstructorType<double> f_4_183_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_183_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_183_construct (const Kin<qd_real>&);
#endif

}