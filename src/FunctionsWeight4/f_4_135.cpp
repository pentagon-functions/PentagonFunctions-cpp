#include "f_4_135.h"

namespace PentagonFunctions {

template <typename T> T f_4_135_abbreviated (const std::array<T,16>&);



template <typename T> IntegrandConstructorType<T> f_4_135_construct (const Kin<T>& kin) {
    return [&kin, 
            dl8 = DLog_W_8<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,16> abbr = 
            {dl8(t), rlog(kin.W[18] / kin_path.W[18]), rlog(v_path[2]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[7] / kin_path.W[7]), dl3(t), f_2_1_14(kin_path), dl16(t), dl19(t), dl20(t)}
;

        auto result = f_4_135_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_135_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[46];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = bc<TR>[0];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = bc<TR>[2];
z[13] = bc<TR>[9];
z[14] = abb[13];
z[15] = abb[15];
z[16] = abb[12];
z[17] = abb[11];
z[18] = abb[14];
z[19] = T(2) * z[2];
z[20] = z[4] + z[19];
z[21] = T(2) * z[3];
z[22] = T(3) * z[7] + -z[20] + -z[21];
z[23] = z[14] * z[22];
z[24] = T(3) * z[18];
z[25] = -z[3] + z[7];
z[26] = z[24] * z[25];
z[27] = z[7] / T(2);
z[28] = z[4] / T(2);
z[29] = -z[2] + z[28];
z[30] = -z[27] + z[29];
z[31] = T(3) * z[0];
z[30] = z[30] * z[31];
z[32] = -z[2] + z[4];
z[33] = z[3] + -z[32];
z[33] = z[17] * z[33];
z[34] = z[2] + z[28];
z[35] = z[3] + (T(-3) * z[7]) / T(2) + z[34];
z[35] = z[15] * z[35];
z[26] = -z[23] + z[26] + z[30] + T(2) * z[33] + z[35];
z[26] = int_to_imaginary<T>(1) * z[26];
z[30] = T(2) * z[0];
z[33] = z[14] + (T(17) * z[17]) / T(2);
z[33] = z[18] + z[33] / T(3);
z[33] = z[15] / T(12) + -z[30] + z[33] / T(2);
z[33] = z[5] * z[33];
z[26] = z[26] + z[33];
z[26] = z[5] * z[26];
z[33] = z[19] + z[28];
z[33] = z[4] * z[33];
z[35] = z[3] / T(2);
z[20] = -z[20] + z[35];
z[20] = z[3] * z[20];
z[36] = prod_pow(z[2], 2);
z[20] = z[20] + z[33] + -z[36];
z[20] = z[17] * z[20];
z[33] = z[3] + z[32];
z[37] = z[21] * z[33];
z[29] = z[4] * z[29];
z[38] = z[36] / T(2);
z[39] = T(3) * z[6] + z[29] + z[38];
z[37] = z[37] + z[39];
z[40] = z[14] * z[37];
z[41] = -z[2] + z[35];
z[41] = z[3] * z[41];
z[42] = z[6] + z[38];
z[41] = z[41] + z[42];
z[43] = -(z[24] * z[41]);
z[44] = -(z[4] * z[34]);
z[44] = z[6] + (T(3) * z[36]) / T(2) + z[44];
z[44] = z[0] * z[44];
z[33] = z[3] * z[33];
z[33] = z[33] + z[39] / T(2);
z[33] = z[15] * z[33];
z[45] = z[17] + -z[18];
z[45] = z[16] * z[45];
z[20] = z[20] + z[26] + z[33] + z[40] + z[43] + (T(3) * z[44]) / T(2) + T(3) * z[45];
z[20] = z[10] * z[20];
z[26] = z[17] * z[22];
z[23] = z[23] + z[26];
z[26] = z[15] + -z[31];
z[22] = z[22] * z[26];
z[22] = z[22] + T(2) * z[23];
z[22] = int_to_imaginary<T>(1) * z[22];
z[23] = z[14] + z[17];
z[26] = -z[15] + z[23];
z[33] = z[5] / T(6);
z[26] = z[26] * z[33];
z[22] = z[22] + z[26];
z[22] = z[5] * z[22];
z[26] = -z[3] + T(2) * z[32];
z[26] = z[21] * z[26];
z[19] = -z[4] + z[19];
z[33] = z[4] * z[19];
z[26] = T(6) * z[6] + z[26] + -z[33] + z[36];
z[26] = z[23] * z[26];
z[21] = z[21] * z[32];
z[21] = z[21] + z[39];
z[21] = z[21] * z[31];
z[31] = z[15] * z[37];
z[21] = z[21] + z[22] + -z[26] + -z[31];
z[22] = -z[8] + -z[9];
z[21] = z[21] * z[22];
z[22] = prod_pow(z[4], 2);
z[26] = T(2) * z[6];
z[22] = z[22] / T(2) + z[26];
z[31] = z[19] + z[35];
z[31] = z[3] * z[31];
z[31] = -z[22] + z[31];
z[31] = z[17] * z[31];
z[19] = z[19] + -z[35];
z[19] = z[3] * z[19];
z[19] = z[19] + -z[22];
z[19] = z[14] * z[19];
z[22] = z[18] * z[41];
z[33] = z[2] + (T(3) * z[4]) / T(2);
z[33] = z[4] * z[33];
z[33] = z[6] + z[33] + (T(-5) * z[36]) / T(2);
z[32] = z[3] * z[32];
z[33] = z[32] + z[33] / T(2);
z[33] = z[0] * z[33];
z[29] = -z[6] + -z[29] + z[38];
z[37] = -z[4] + -z[35];
z[37] = z[3] * z[37];
z[29] = z[16] + z[29] / T(2) + z[37];
z[29] = z[15] * z[29];
z[37] = z[18] + -z[23];
z[37] = z[16] * z[37];
z[19] = z[19] + z[22] + z[29] + z[31] + z[33] + z[37];
z[19] = z[11] * z[19];
z[22] = z[2] * z[4];
z[26] = -z[22] + z[26] + z[32] + z[36];
z[26] = z[0] * z[26];
z[29] = -z[4] + z[35];
z[29] = z[3] * z[29];
z[22] = z[16] + z[22] + z[29] + -z[42];
z[22] = z[14] * z[22];
z[29] = -z[16] + -z[41];
z[29] = z[15] * z[29];
z[22] = z[22] + z[26] + z[29];
z[22] = z[1] * z[22];
z[19] = z[19] + z[22];
z[22] = -z[2] + z[25];
z[22] = z[22] * z[23];
z[26] = T(3) * z[2] + z[3] + -z[27] + -z[28];
z[26] = z[0] * z[26];
z[27] = z[27] + -z[34];
z[27] = z[15] * z[27];
z[28] = -(z[18] * z[25]);
z[22] = T(2) * z[22] + z[26] + z[27] + z[28];
z[22] = z[11] * z[22];
z[26] = -z[4] + z[7];
z[26] = z[14] * z[26];
z[27] = z[3] + z[4] + T(-2) * z[7];
z[27] = z[0] * z[27];
z[25] = z[15] * z[25];
z[25] = z[25] + z[26] + z[27];
z[25] = z[1] * z[25];
z[26] = z[0] + z[15];
z[26] = -z[18] + (T(3) * z[23]) / T(2) + -z[26] / T(4);
z[26] = prod_pow(z[12], 2) * z[26];
z[22] = z[22] + z[25] + z[26];
z[22] = int_to_imaginary<T>(1) * z[22];
z[25] = z[15] / T(2);
z[26] = (T(3) * z[14]) / T(2) + z[25] + -z[30];
z[26] = z[1] * z[26];
z[25] = -z[0] / T(2) + z[23] + -z[25];
z[25] = int_to_imaginary<T>(1) * z[5] * z[25];
z[27] = T(-3) * z[14] + (T(-5) * z[17]) / T(2) + -z[18];
z[27] = T(4) * z[0] + (T(-3) * z[15]) / T(4) + z[27] / T(2);
z[27] = z[11] * z[27];
z[28] = (T(11) * z[0]) / T(4) + (T(-5) * z[15]) / T(4) + -z[18] + -z[23] / T(2);
z[28] = z[12] * z[28];
z[25] = z[25] / T(2) + z[26] + z[27] + (T(3) * z[28]) / T(2);
z[25] = z[5] * z[25];
z[22] = T(3) * z[22] + z[25];
z[22] = z[5] * z[22];
z[23] = (T(-105) * z[0]) / T(4) + (T(325) * z[15]) / T(12) + (T(-23) * z[23]) / T(6) + z[24];
z[23] = z[13] * z[23];
return T(3) * z[19] + z[20] + z[21] + z[22] + z[23] / T(4);
}



template IntegrandConstructorType<double> f_4_135_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_135_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_135_construct (const Kin<qd_real>&);
#endif

}