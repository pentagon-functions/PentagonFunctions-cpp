#include "f_4_85.h"

namespace PentagonFunctions {

template <typename T> T f_4_85_abbreviated (const std::array<T,16>&);

template <typename T> class SpDLog_f_4_85_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_85_W_23 (const Kin<T>& kin) {
        c[0] = ((T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + (T(-3) / T(2) + (T(3) * kin.v[2]) / T(8) + (T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[2] + (T(3) / T(2) + (T(-9) * kin.v[0]) / T(8) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(4) + (T(9) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[0] + (T(-3) / T(2) + (T(-9) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) / T(2) + (T(3) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = kin.v[2] * (T(-3) / T(2) + (T(9) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + T(-9) * kin.v[4]) + kin.v[3] * (T(-3) / T(2) + (T(-15) * kin.v[4]) / T(2) + T(3) * kin.v[3]) + ((T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + (T(3) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[4] + kin.v[0] * (T(3) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(-15) * kin.v[2]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(3) * kin.v[0] + T(-6) * kin.v[3]) + rlog(kin.v[3]) * (kin.v[0] * ((T(-15) * kin.v[0]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[1]) + ((T(21) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-21) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(21) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[1] * (T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(21) / T(2) + (T(-39) * kin.v[0]) / T(8) + (T(-21) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(4) + (T(39) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(4)) * kin.v[0] + ((T(21) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(-21) * kin.v[4]) / T(2)) * kin.v[1] + (T(-21) / T(2) + (T(45) * kin.v[2]) / T(8) + (T(3) * kin.v[3]) / T(4) + (T(-45) * kin.v[4]) / T(4)) * kin.v[2] + (T(-21) / T(2) + (T(-39) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(21) / T(2) + (T(45) * kin.v[4]) / T(8)) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * ((T(15) * kin.v[0]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + T(9) * kin.v[1]) + ((T(-21) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(21) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(15) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[1] * (T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4]));
c[2] = (T(-3) * kin.v[0]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(-3) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(3) * kin.v[0]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * rlog(-kin.v[4] + kin.v[1] + kin.v[2]) + rlog(kin.v[3]) * (T(3) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (prod_pow(abb[2], 2) * ((abb[6] * T(-9)) / T(2) + (abb[7] * T(-3)) / T(4) + (abb[5] * T(21)) / T(4)) + abb[3] * ((abb[5] * T(3)) / T(2) + (abb[7] * T(3)) / T(2) + abb[6] * T(-3)) + abb[4] * ((prod_pow(abb[2], 2) * T(9)) / T(2) + abb[3] * T(3)) + abb[1] * (abb[1] * ((abb[5] * T(-15)) / T(4) + (abb[4] * T(-3)) / T(2) + (abb[6] * T(3)) / T(2) + (abb[7] * T(9)) / T(4)) + abb[2] * abb[4] * T(-3) + abb[2] * ((abb[5] * T(-3)) / T(2) + (abb[7] * T(-3)) / T(2) + abb[6] * T(3))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_85_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl16 = DLog_W_16<T>(kin),dl9 = DLog_W_9<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),spdl23 = SpDLog_f_4_85_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,16> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), -rlog(t), dl16(t), dl9(t), rlog(v_path[3]), dl20(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl17(t), dl4(t)}
;

        auto result = f_4_85_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_85_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[31];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[11];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[5];
z[6] = abb[8];
z[7] = abb[9];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[2];
z[11] = abb[10];
z[12] = bc<TR>[0];
z[13] = abb[3];
z[14] = abb[12];
z[15] = abb[13];
z[16] = bc<TR>[2];
z[17] = bc<TR>[9];
z[18] = z[3] / T(4);
z[19] = z[4] / T(2);
z[20] = (T(3) * z[2]) / T(2);
z[21] = -z[7] + z[20];
z[22] = z[6] + -z[18] + -z[19] + z[21];
z[22] = z[5] * z[22];
z[23] = z[3] / T(2);
z[21] = z[21] + -z[23];
z[19] = z[19] + -z[21];
z[19] = z[8] * z[19];
z[18] = -z[6] + -z[18];
z[18] = z[9] * z[18];
z[24] = z[2] + -z[4] + -z[23];
z[24] = z[1] * z[24];
z[18] = z[18] + z[19] + z[22] + z[24];
z[18] = prod_pow(z[0], 2) * z[18];
z[19] = z[2] + -z[3];
z[22] = -z[4] + z[19];
z[24] = T(3) * z[8];
z[22] = z[22] * z[24];
z[24] = T(2) * z[4];
z[25] = T(-2) * z[2] + T(3) * z[3] + z[24];
z[25] = z[1] * z[25];
z[26] = (T(7) * z[3]) / T(2);
z[27] = T(3) * z[4] + z[26];
z[28] = T(3) * z[2] + -z[27];
z[28] = z[5] * z[28];
z[29] = z[9] * z[23];
z[22] = z[22] + z[25] + -z[28] + -z[29];
z[25] = z[0] * z[22];
z[26] = z[9] * z[26];
z[27] = -(z[5] * z[27]);
z[26] = z[26] + z[27];
z[27] = (T(-3) * z[3]) / T(2) + -z[4];
z[27] = z[1] * z[27];
z[28] = z[4] * z[8];
z[26] = z[26] / T(2) + z[27] + (T(3) * z[28]) / T(2);
z[26] = z[10] * z[26];
z[25] = z[25] + z[26];
z[25] = z[10] * z[25];
z[26] = T(3) * z[19];
z[27] = -z[24] + z[26];
z[28] = -z[5] + z[8];
z[27] = z[27] * z[28];
z[29] = T(2) * z[1];
z[29] = z[19] * z[29];
z[27] = z[27] + -z[29];
z[29] = -(z[10] * z[27]);
z[21] = -z[4] + z[21];
z[21] = z[21] * z[28];
z[30] = -z[19] + z[24];
z[30] = z[1] * z[30];
z[21] = z[21] + z[30];
z[21] = z[11] * z[21];
z[21] = z[21] + z[29];
z[21] = z[11] * z[21];
z[22] = -(z[13] * z[22]);
z[29] = z[14] * z[27];
z[18] = z[18] + z[21] + z[22] + z[25] + z[29];
z[21] = int_to_imaginary<T>(3);
z[22] = z[4] + -z[19] / T(4);
z[22] = z[12] * z[21] * z[22];
z[25] = -z[3] + z[6];
z[25] = z[9] * z[25];
z[20] = (T(7) * z[3]) / T(4) + -z[4] + -z[6] / T(4) + T(-3) * z[7] + z[20];
z[20] = z[5] * z[20];
z[29] = (T(-13) * z[4]) / T(2) + z[19];
z[29] = z[1] * z[29];
z[23] = -z[2] / T(2) + z[7] + -z[23];
z[23] = z[4] + T(3) * z[23];
z[23] = z[8] * z[23];
z[26] = z[16] * z[26];
z[20] = z[20] + z[22] + z[23] + z[25] / T(4) + z[26] + z[29];
z[20] = z[12] * z[20];
z[22] = z[10] + -z[15];
z[22] = z[22] * z[27];
z[23] = z[3] + -z[7];
z[23] = -(z[23] * z[28]);
z[24] = -(z[1] * z[24]);
z[23] = z[23] + z[24];
z[23] = z[11] * z[23];
z[22] = z[22] + T(2) * z[23];
z[21] = z[21] * z[22];
z[20] = z[20] + z[21];
z[20] = z[12] * z[20];
z[19] = T(25) * z[4] + T(-9) * z[19];
z[19] = z[17] * z[19];
return T(3) * z[18] + z[19] + z[20];
}



template IntegrandConstructorType<double> f_4_85_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_85_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_85_construct (const Kin<qd_real>&);
#endif

}