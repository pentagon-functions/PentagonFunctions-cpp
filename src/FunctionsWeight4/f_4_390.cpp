#include "f_4_390.h"

namespace PentagonFunctions {

template <typename T> T f_4_390_abbreviated (const std::array<T,50>&);

template <typename T> class SpDLog_f_4_390_W_7 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_390_W_7 (const Kin<T>& kin) {
        c[0] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[1] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[2] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[1], 2) * (abb[3] * T(-6) + abb[4] * T(-6) + abb[5] * T(-6)) + prod_pow(abb[2], 2) * abb[3] * T(6) + prod_pow(abb[2], 2) * (abb[4] * T(6) + abb[5] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_390_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_390_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4]);
c[1] = kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4]);
c[2] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);
c[3] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[9] * c[0] + abb[3] * c[1] + abb[5] * c[2] + abb[10] * c[3]);
        }

        return abb[6] * (prod_pow(abb[8], 2) * abb[9] * T(-6) + prod_pow(abb[7], 2) * (abb[5] * T(-6) + abb[10] * T(-6) + abb[3] * T(6) + abb[9] * T(6)) + prod_pow(abb[8], 2) * (abb[3] * T(-6) + abb[5] * T(6) + abb[10] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_390_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl23 = DLog_W_23<T>(kin),dl9 = DLog_W_9<T>(kin),dl14 = DLog_W_14<T>(kin),dl25 = DLog_W_25<T>(kin),dl24 = DLog_W_24<T>(kin),dl4 = DLog_W_4<T>(kin),dl29 = DLog_W_29<T>(kin),dl18 = DLog_W_18<T>(kin),dl27 = DLog_W_27<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl31 = DLog_W_31<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),spdl7 = SpDLog_f_4_390_W_7<T>(kin),spdl23 = SpDLog_f_4_390_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,50> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl9(t), rlog(v_path[3]), dl14(t), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl24(t), dl4(t), f_2_1_6(kin_path), f_2_1_8(kin_path), rlog(v_path[0] + v_path[1]), f_2_1_3(kin_path), f_2_1_11(kin_path), rlog(-v_path[1] + v_path[3]), dl29(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[30] / kin_path.W[30]), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl27(t) / kin_path.SqrtDelta, dl2(t), dl16(t), dl17(t), dl19(t), dl20(t), f_2_2_5(kin_path), f_1_3_1(kin_path), f_1_3_3(kin_path), f_1_3_5(kin_path), dl1(t), dl5(t), dl31(t), dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_390_abbreviated(abbr);
        result = result + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_390_abbreviated(const std::array<T,50>& abb)
{
using TR = typename T::value_type;
T z[116];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[39];
z[3] = abb[4];
z[4] = abb[13];
z[5] = abb[17];
z[6] = abb[35];
z[7] = abb[38];
z[8] = abb[45];
z[9] = abb[5];
z[10] = abb[44];
z[11] = abb[10];
z[12] = abb[24];
z[13] = abb[25];
z[14] = abb[28];
z[15] = abb[29];
z[16] = abb[30];
z[17] = abb[49];
z[18] = abb[2];
z[19] = abb[15];
z[20] = abb[31];
z[21] = bc<TR>[0];
z[22] = abb[12];
z[23] = abb[7];
z[24] = abb[36];
z[25] = abb[48];
z[26] = abb[34];
z[27] = abb[14];
z[28] = abb[37];
z[29] = abb[9];
z[30] = abb[47];
z[31] = abb[8];
z[32] = abb[16];
z[33] = abb[19];
z[34] = abb[22];
z[35] = abb[26];
z[36] = abb[27];
z[37] = abb[32];
z[38] = abb[33];
z[39] = abb[21];
z[40] = abb[23];
z[41] = abb[11];
z[42] = abb[18];
z[43] = abb[20];
z[44] = abb[40];
z[45] = abb[41];
z[46] = abb[42];
z[47] = abb[43];
z[48] = abb[46];
z[49] = bc<TR>[3];
z[50] = bc<TR>[1];
z[51] = bc<TR>[2];
z[52] = bc<TR>[4];
z[53] = bc<TR>[9];
z[54] = -z[13] + -z[15] + T(2) * z[16];
z[55] = -(z[25] * z[54]);
z[56] = z[9] * z[24];
z[55] = z[55] + -z[56];
z[56] = z[9] + z[11];
z[57] = z[29] + z[56];
z[57] = z[41] * z[57];
z[58] = -z[3] + z[11];
z[59] = z[4] * z[58];
z[60] = z[57] + z[59];
z[61] = z[14] + -z[54];
z[62] = z[17] * z[61];
z[63] = z[7] * z[9];
z[64] = z[62] + -z[63];
z[65] = z[14] * z[25];
z[65] = -z[64] + z[65];
z[66] = z[5] + z[28];
z[67] = z[29] * z[66];
z[68] = z[5] + z[7];
z[69] = z[3] * z[68];
z[70] = z[30] * z[61];
z[71] = z[6] * z[56];
z[72] = -z[5] + -z[24];
z[72] = z[11] * z[72];
z[72] = z[55] + z[60] + z[65] + -z[67] + z[69] + z[70] + -z[71] + z[72];
z[72] = z[22] * z[72];
z[73] = -(z[12] * z[54]);
z[74] = z[12] * z[14];
z[62] = z[62] + -z[63] + z[73] + z[74];
z[63] = T(2) * z[28];
z[68] = z[2] + -z[63] + -z[68];
z[68] = z[29] * z[68];
z[74] = T(2) * z[70];
z[75] = z[7] + z[28];
z[76] = z[3] * z[75];
z[77] = z[3] + z[9];
z[78] = -z[29] + z[77];
z[79] = z[10] * z[78];
z[80] = z[9] * z[28];
z[68] = -z[62] + z[68] + z[74] + z[76] + -z[79] + z[80];
z[68] = z[31] * z[68];
z[81] = z[8] * z[77];
z[82] = z[5] * z[9];
z[83] = z[81] + -z[82];
z[84] = z[70] + z[80];
z[75] = z[29] * z[75];
z[75] = z[75] + -z[84];
z[85] = T(2) * z[7] + z[66];
z[85] = z[3] * z[85];
z[85] = T(-2) * z[62] + -z[75] + -z[79] + -z[83] + z[85];
z[85] = z[18] * z[85];
z[86] = z[11] * z[24];
z[86] = -z[55] + z[86];
z[65] = z[65] + -z[86];
z[83] = -z[69] + z[83];
z[87] = z[10] * z[56];
z[88] = z[71] + -z[87];
z[89] = z[65] + -z[83] + -z[88];
z[90] = -(z[40] * z[89]);
z[76] = z[75] + -z[76];
z[91] = z[76] + z[79];
z[92] = z[62] + z[91];
z[93] = z[6] + z[20];
z[94] = -z[2] + z[93];
z[94] = z[1] * z[94];
z[95] = z[26] * z[61];
z[94] = z[94] + -z[95];
z[96] = z[92] + z[94];
z[97] = z[36] * z[96];
z[98] = z[20] + z[24];
z[99] = -z[8] + z[98];
z[99] = z[1] * z[99];
z[100] = z[20] * z[56];
z[56] = z[8] * z[56];
z[99] = z[56] + -z[95] + z[99] + -z[100];
z[100] = z[12] + z[25];
z[100] = z[14] * z[100];
z[73] = z[73] + -z[86] + z[100];
z[86] = T(2) * z[73] + -z[88] + z[99];
z[86] = z[23] * z[86];
z[91] = z[65] + -z[91] + z[99];
z[100] = -(z[38] * z[91]);
z[78] = -z[1] + z[78];
z[78] = z[27] * z[78];
z[58] = z[29] + z[58];
z[101] = -z[1] + z[58];
z[101] = z[32] * z[101];
z[102] = z[78] + -z[101];
z[103] = z[20] * z[58];
z[104] = z[102] + z[103];
z[105] = -z[6] + z[24];
z[105] = z[1] * z[105];
z[65] = z[65] + -z[76] + -z[104] + z[105];
z[65] = z[19] * z[65];
z[76] = -z[2] + T(2) * z[48];
z[54] = -(z[54] * z[76]);
z[105] = z[14] * z[76];
z[105] = z[54] + z[105];
z[106] = z[10] * z[61];
z[107] = z[8] * z[61];
z[106] = -z[105] + z[106] + z[107];
z[108] = z[45] + -z[46];
z[108] = T(-2) * z[108];
z[108] = z[106] * z[108];
z[109] = z[8] + z[10];
z[110] = -(z[61] * z[109]);
z[105] = z[105] + z[110];
z[105] = z[47] * z[105];
z[110] = -z[17] + z[25] + z[30];
z[111] = z[52] * z[110];
z[105] = z[105] + z[111];
z[88] = -z[73] + z[88];
z[66] = -z[2] + z[66];
z[66] = z[29] * z[66];
z[66] = z[66] + -z[70];
z[70] = z[66] + z[88];
z[111] = z[43] * z[70];
z[112] = -z[88] + -z[94];
z[112] = z[0] * z[112];
z[113] = z[51] * z[110];
z[114] = z[50] * z[110];
z[115] = T(-2) * z[113] + z[114];
z[115] = z[50] * z[115];
z[65] = z[65] + z[68] + z[72] + z[85] + z[86] + z[90] + z[97] + z[100] + T(2) * z[105] + z[108] + z[111] + z[112] + z[115];
z[65] = int_to_imaginary<T>(1) * z[65];
z[68] = -z[76] + z[109];
z[68] = z[49] * z[68];
z[65] = z[65] + T(8) * z[68];
z[68] = z[25] + T(-4) * z[76];
z[68] = z[14] * z[68];
z[54] = T(-4) * z[54] + z[55] + -z[64] + z[68];
z[54] = T(-2) * z[54] + z[80] + z[95];
z[55] = z[60] + -z[102];
z[58] = -z[58] + T(2) * z[61];
z[58] = z[10] * z[58];
z[58] = -z[58] + z[114];
z[61] = (T(13) * z[5]) / T(2);
z[64] = z[28] / T(2);
z[68] = T(-4) * z[24] + -z[61] + z[64];
z[68] = z[11] * z[68];
z[72] = T(2) * z[2];
z[63] = (T(-9) * z[5]) / T(2) + (T(5) * z[7]) / T(2) + z[24] / T(2) + -z[63] + -z[72];
z[63] = z[29] * z[63];
z[64] = (T(-9) * z[6]) / T(2) + z[7] / T(2) + T(2) * z[20] + (T(13) * z[24]) / T(2) + -z[64] + -z[72];
z[64] = z[1] * z[64];
z[61] = T(4) * z[7] + (T(-5) * z[28]) / T(2) + z[61];
z[61] = z[3] * z[61];
z[72] = -z[3] / T(2) + T(2) * z[9] + (T(5) * z[11]) / T(2);
z[72] = z[6] * z[72];
z[54] = T(-2) * z[54] + T(6) * z[55] + T(-8) * z[58] + z[61] + z[63] + z[64] + z[68] + z[72] + z[74] + (T(-13) * z[103]) / T(2) + T(16) * z[107] + T(18) * z[113];
z[54] = z[21] * z[54];
z[54] = z[54] + T(12) * z[65];
z[54] = z[21] * z[54];
z[55] = -(z[11] * z[28]);
z[58] = z[8] + z[28];
z[58] = z[1] * z[58];
z[55] = z[55] + -z[56] + z[58] + z[67] + z[79] + -z[84] + z[101];
z[55] = prod_pow(z[31], 2) * z[55];
z[58] = -z[62] + z[69];
z[61] = z[6] * z[77];
z[63] = z[1] * z[2];
z[59] = -z[58] + z[59] + z[61] + z[63] + T(2) * z[81] + -z[82] + -z[87];
z[59] = z[0] * z[59];
z[61] = z[62] + z[83];
z[62] = z[22] * z[61];
z[63] = -(z[18] * z[61]);
z[64] = z[23] * z[88];
z[65] = z[19] * z[94];
z[63] = -z[62] + z[63] + -z[64] + z[65];
z[59] = z[59] + T(2) * z[63];
z[59] = z[0] * z[59];
z[63] = -z[2] + -z[24];
z[63] = z[29] * z[63];
z[65] = -(z[1] * z[8]);
z[56] = z[56] + z[57] + z[63] + z[65] + -z[71] + z[73];
z[56] = z[23] * z[56];
z[57] = z[31] * z[66];
z[56] = z[56] + T(-2) * z[57];
z[56] = z[23] * z[56];
z[63] = -z[5] + z[28];
z[63] = z[3] * z[63];
z[65] = -z[7] + -z[93];
z[65] = z[1] * z[65];
z[63] = z[63] + z[65] + -z[75] + -z[78] + -z[82] + z[95];
z[63] = z[18] * z[63];
z[65] = z[19] + -z[31];
z[65] = z[65] * z[92];
z[62] = z[62] + z[65];
z[62] = T(2) * z[62] + z[63];
z[62] = z[18] * z[62];
z[63] = z[5] * z[11];
z[65] = z[2] * z[29];
z[58] = -z[58] + -z[60] + z[63] + z[65] + z[87];
z[58] = z[22] * z[58];
z[57] = z[57] + z[64];
z[57] = T(2) * z[57] + z[58];
z[57] = z[22] * z[57];
z[58] = z[2] + -z[98];
z[58] = z[1] * z[58];
z[58] = z[58] + -z[73] + -z[79] + z[95] + z[104];
z[58] = z[19] * z[58];
z[60] = z[73] + z[99];
z[63] = -z[23] + z[31];
z[63] = z[60] * z[63];
z[58] = z[58] + T(2) * z[63];
z[58] = z[19] * z[58];
z[63] = z[39] * z[89];
z[64] = z[44] * z[106];
z[63] = -z[63] + z[64];
z[55] = z[55] + z[56] + z[57] + z[58] + z[59] + z[62] + T(2) * z[63];
z[56] = z[60] + z[66];
z[56] = z[33] * z[56];
z[57] = z[37] * z[91];
z[58] = z[35] * z[96];
z[59] = z[61] + -z[94];
z[59] = z[34] * z[59];
z[60] = z[42] * z[70];
z[56] = -z[56] + z[57] + z[58] + -z[59] + z[60];
z[57] = z[53] * z[110];
return z[54] + T(6) * z[55] + T(-12) * z[56] + T(-35) * z[57];
}



template IntegrandConstructorType<double> f_4_390_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_390_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_390_construct (const Kin<qd_real>&);
#endif

}