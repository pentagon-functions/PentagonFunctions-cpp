#include "f_4_186.h"

namespace PentagonFunctions {

template <typename T> T f_4_186_abbreviated (const std::array<T,20>&);

template <typename T> class SpDLog_f_4_186_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_186_W_22 (const Kin<T>& kin) {
        c[0] = ((T(9) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(3)) * kin.v[2] + ((T(-3) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(3) + T(-3) * kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[3] + ((T(9) * kin.v[4]) / T(4) + T(-3)) * kin.v[4] + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);
c[1] = kin.v[3] * (T(-3) + T(-6) * kin.v[3] + T(-9) * kin.v[4]) + kin.v[4] * (T(-3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[2] * (T(3) + T(-3) * kin.v[2] + T(9) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(8) * kin.v[3]) + T(-8) * kin.v[4] + kin.v[3] * (T(-8) + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[0] * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4])) + kin.v[1] * (T(3) + T(-3) * kin.v[0] + (T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-9) * kin.v[2] + T(12) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[0] + T(-8) * kin.v[2] + T(16) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(-45) * kin.v[2]) / T(4) + (T(23) * kin.v[3]) / T(2) + (T(45) * kin.v[4]) / T(2) + T(-11)) * kin.v[2] + (-kin.v[3] / T(4) + (T(-23) * kin.v[4]) / T(2) + T(11)) * kin.v[3] + kin.v[1] * ((T(-23) * kin.v[2]) / T(2) + kin.v[3] / T(2) + (T(23) * kin.v[4]) / T(2) + T(-11) + T(11) * kin.v[0] + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[0] + T(-8) * kin.v[3])) + ((T(-45) * kin.v[4]) / T(4) + T(11)) * kin.v[4] + kin.v[0] * (T(11) * kin.v[2] + T(-11) * kin.v[3] + T(-11) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[0] * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(-4) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(-27) * kin.v[4]) / T(2) + T(14)) * kin.v[4] + kin.v[0] * (T(14) * kin.v[2] + T(-14) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(14) + T(-13) * kin.v[4]) + kin.v[1] * (-kin.v[3] + T(-14) + T(14) * kin.v[0] + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(-13) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[0] + T(-8) * kin.v[3]) + T(13) * kin.v[4]) + kin.v[2] * ((T(-27) * kin.v[2]) / T(2) + T(-14) + T(13) * kin.v[3] + T(27) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[0] * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(-4) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]))) + rlog(kin.v[3]) * (((T(27) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + kin.v[2] * ((T(27) * kin.v[2]) / T(2) + T(14) + T(-13) * kin.v[3] + T(-27) * kin.v[4]) + kin.v[1] * (T(14) + T(-14) * kin.v[0] + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(13) * kin.v[2] + kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[0] + T(8) * kin.v[3]) + T(-13) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(-14) + T(13) * kin.v[4]) + kin.v[0] * (T(-14) * kin.v[2] + T(14) * kin.v[3] + T(14) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[4] * (T(-8) + T(4) * kin.v[4]) + kin.v[0] * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (T(28) + T(27) * kin.v[2] + T(-26) * kin.v[3] + T(-54) * kin.v[4]) + kin.v[1] * (T(28) + T(-28) * kin.v[0] + (T(-1) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(26) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) + T(-16) * kin.v[0] + T(16) * kin.v[3]) + T(-26) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(-28) + T(26) * kin.v[4]) + kin.v[4] * (T(-28) + T(27) * kin.v[4]) + kin.v[0] * (T(-28) * kin.v[2] + T(28) * kin.v[3] + T(28) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-16) + T(-8) * kin.v[3]) + kin.v[2] * (T(16) + T(8) * kin.v[2] + T(-16) * kin.v[4]) + kin.v[4] * (T(-16) + T(8) * kin.v[4]) + kin.v[0] * (T(-16) * kin.v[2] + T(16) * kin.v[3] + T(16) * kin.v[4])));
c[2] = T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4];
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(16) + T(12)) * kin.v[1] + T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) * kin.v[2] + T(-16) * kin.v[3] + T(-16) * kin.v[4])) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-9) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-9) * kin.v[2] + T(9) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (abb[10] * (abb[2] * abb[7] * T(-8) + abb[2] * (bc<T>[0] * int_to_imaginary<T>(-8) + abb[2] * T(4)) + abb[3] * T(6)) + abb[2] * abb[7] * (abb[9] * T(-16) + abb[5] * T(8) + abb[8] * T(8)) + abb[2] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-16) + abb[2] * (-abb[5] + abb[8] * T(-4) + abb[4] * T(-3) + abb[9] * T(8))) + abb[3] * (abb[5] * T(-9) + abb[8] * T(-6) + abb[4] * T(3) + abb[9] * T(12)) + abb[6] * (abb[2] * abb[10] * T(8) + abb[1] * (abb[9] * T(-16) + abb[10] * T(-8) + abb[5] * T(8) + abb[8] * T(8)) + abb[2] * (abb[5] * T(-8) + abb[8] * T(-8) + abb[9] * T(16))) + abb[1] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(16) + abb[1] * (abb[5] + abb[9] * T(-8) + abb[10] * T(-4) + abb[4] * T(3) + abb[8] * T(4)) + abb[10] * (bc<T>[0] * int_to_imaginary<T>(8) + abb[7] * T(8)) + abb[7] * (abb[5] * T(-8) + abb[8] * T(-8) + abb[9] * T(16))));
    }
};
template <typename T> class SpDLog_f_4_186_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_186_W_23 (const Kin<T>& kin) {
        c[0] = ((T(9) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-3)) * kin.v[2] + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(3) + T(-3) * kin.v[1]) + ((T(9) * kin.v[4]) / T(4) + T(3)) * kin.v[4];
c[1] = kin.v[0] * (T(3) + (T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-3) * kin.v[1] + T(9) * kin.v[2] + T(12) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[1] + T(8) * kin.v[2] + T(16) * kin.v[3] + T(-8) * kin.v[4])) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[4] * (T(3) + T(-3) * kin.v[4]) + kin.v[2] * (T(-3) + T(-3) * kin.v[2] + T(-9) * kin.v[3] + T(6) * kin.v[4]) + kin.v[3] * (T(-3) + T(-6) * kin.v[3] + T(9) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(-8) + T(-8) * kin.v[3]) + T(8) * kin.v[4] + kin.v[1] * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[3] * (T(-8) + T(-8) * kin.v[3] + T(8) * kin.v[4])) + rlog(kin.v[3]) * (((T(27) * kin.v[4]) / T(2) + T(14)) * kin.v[4] + kin.v[2] * ((T(27) * kin.v[2]) / T(2) + T(-14) + T(13) * kin.v[3] + T(-27) * kin.v[4]) + kin.v[1] * (T(14) * kin.v[2] + T(14) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(-14) + T(-13) * kin.v[4]) + kin.v[0] * (T(14) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-14) * kin.v[1] + T(-13) * kin.v[2] + kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[1] + T(8) * kin.v[3]) + T(13) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(4) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (T(-28) + T(27) * kin.v[2] + T(26) * kin.v[3] + T(-54) * kin.v[4]) + kin.v[1] * (T(28) * kin.v[2] + T(28) * kin.v[3] + T(-28) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(-28) + T(-26) * kin.v[4]) + kin.v[0] * (T(28) + (T(-1) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-28) * kin.v[1] + T(-26) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) + T(-16) * kin.v[1] + T(16) * kin.v[3]) + T(26) * kin.v[4]) + kin.v[4] * (T(28) + T(27) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-16) + T(-8) * kin.v[3]) + kin.v[2] * (T(-16) + T(8) * kin.v[2] + T(-16) * kin.v[4]) + kin.v[1] * (T(16) * kin.v[2] + T(16) * kin.v[3] + T(-16) * kin.v[4]) + kin.v[4] * (T(16) + T(8) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(-45) * kin.v[2]) / T(4) + (T(-23) * kin.v[3]) / T(2) + (T(45) * kin.v[4]) / T(2) + T(11)) * kin.v[2] + (-kin.v[3] / T(4) + (T(23) * kin.v[4]) / T(2) + T(11)) * kin.v[3] + kin.v[0] * ((T(23) * kin.v[2]) / T(2) + kin.v[3] / T(2) + (T(-23) * kin.v[4]) / T(2) + T(-11) + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(11) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(-8) * kin.v[3])) + ((T(-45) * kin.v[4]) / T(4) + T(-11)) * kin.v[4] + kin.v[1] * (T(-11) * kin.v[2] + T(-11) * kin.v[3] + T(11) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(-27) * kin.v[4]) / T(2) + T(-14)) * kin.v[4] + kin.v[0] * (-kin.v[3] + T(-14) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(14) * kin.v[1] + T(13) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(-8) * kin.v[3]) + T(-13) * kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(14) + T(13) * kin.v[4]) + kin.v[1] * (T(-14) * kin.v[2] + T(-14) * kin.v[3] + T(14) * kin.v[4]) + kin.v[2] * ((T(-27) * kin.v[2]) / T(2) + T(14) + T(-13) * kin.v[3] + T(27) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4])));
c[2] = T(-3) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4];
c[3] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-9) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(16) + T(12)) * kin.v[0] + T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-16) * kin.v[2] + T(-16) * kin.v[3] + T(16) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[11] * (abb[10] * (abb[7] * (abb[2] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[7] * T(4)) + abb[12] * T(6)) + abb[7] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-16) + abb[2] * (abb[9] * T(-16) + abb[5] * T(8) + abb[8] * T(8)) + abb[7] * (-abb[8] + abb[5] * T(-4) + abb[13] * T(-3) + abb[9] * T(8))) + abb[12] * (abb[8] * T(-9) + abb[5] * T(-6) + abb[13] * T(3) + abb[9] * T(12)) + abb[1] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(16) + abb[1] * (abb[8] + abb[9] * T(-8) + abb[10] * T(-4) + abb[13] * T(3) + abb[5] * T(4)) + abb[10] * (bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * T(8)) + abb[2] * (abb[5] * T(-8) + abb[8] * T(-8) + abb[9] * T(16))) + abb[6] * (abb[7] * abb[10] * T(8) + abb[1] * (abb[9] * T(-16) + abb[10] * T(-8) + abb[5] * T(8) + abb[8] * T(8)) + abb[7] * (abb[5] * T(-8) + abb[8] * T(-8) + abb[9] * T(16))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_186_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl23 = DLog_W_23<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),spdl22 = SpDLog_f_4_186_W_22<T>(kin),spdl23 = SpDLog_f_4_186_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,20> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), -rlog(t), rlog(kin.W[19] / kin_path.W[19]), rlog(v_path[3]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[3] / kin_path.W[3]), dl23(t), f_2_1_8(kin_path), -rlog(t), dl4(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl16(t), dl20(t), dl17(t)}
;

        auto result = f_4_186_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_186_abbreviated(const std::array<T,20>& abb)
{
using TR = typename T::value_type;
T z[71];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[14];
z[3] = abb[17];
z[4] = abb[18];
z[5] = abb[19];
z[6] = abb[5];
z[7] = abb[8];
z[8] = abb[9];
z[9] = abb[10];
z[10] = abb[13];
z[11] = abb[2];
z[12] = abb[6];
z[13] = abb[7];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[12];
z[17] = abb[15];
z[18] = abb[16];
z[19] = bc<TR>[9];
z[20] = T(2) * z[8] + z[9];
z[21] = T(2) * z[10];
z[22] = z[20] + -z[21];
z[23] = -z[7] + -z[22];
z[24] = z[1] + z[6] + T(2) * z[23];
z[25] = int_to_imaginary<T>(1) * z[14];
z[26] = -z[12] + z[25];
z[24] = z[24] * z[26];
z[27] = T(2) * z[1];
z[28] = -z[6] + z[23] + z[27];
z[29] = z[13] * z[28];
z[30] = T(5) * z[1];
z[31] = -z[6] + z[30];
z[32] = T(4) * z[23] + z[31];
z[33] = z[0] * z[32];
z[24] = z[24] + -z[29] + z[33];
z[33] = T(2) * z[13];
z[24] = z[24] * z[33];
z[34] = (T(-13) * z[7]) / T(6) + (T(2) * z[10]) / T(3);
z[35] = z[8] + z[9] / T(2);
z[35] = T(3) * z[35];
z[36] = z[6] / T(2) + -z[27] + z[34] + z[35];
z[37] = prod_pow(z[14], 2);
z[36] = z[36] * z[37];
z[38] = z[20] + -z[27];
z[39] = z[6] + -z[7];
z[40] = -z[38] + -z[39];
z[41] = z[25] * z[40];
z[42] = z[7] + -z[10];
z[43] = z[0] * z[42];
z[44] = T(-2) * z[41] + -z[43];
z[45] = T(3) * z[10];
z[46] = T(2) * z[20];
z[47] = z[45] + -z[46];
z[48] = z[1] + z[39] + z[47];
z[48] = z[12] * z[48];
z[44] = T(2) * z[44] + z[48];
z[44] = z[12] * z[44];
z[48] = z[12] * z[40];
z[41] = -z[41] + z[48];
z[29] = -z[29] + z[41] + T(-2) * z[43];
z[40] = z[11] * z[40];
z[29] = T(2) * z[29] + z[40];
z[48] = T(2) * z[11];
z[29] = z[29] * z[48];
z[49] = T(2) * z[16];
z[32] = z[32] * z[49];
z[50] = T(5) * z[10];
z[38] = z[6] + T(4) * z[7] + z[38] + -z[50];
z[38] = z[15] * z[38];
z[51] = (T(16) * z[19]) / T(3);
z[38] = z[38] + z[51];
z[52] = T(4) * z[20];
z[31] = T(5) * z[7] + T(-9) * z[10] + -z[31] + z[52];
z[53] = prod_pow(z[0], 2);
z[31] = z[31] * z[53];
z[54] = -z[1] + z[10] + z[39];
z[55] = T(3) * z[18];
z[56] = z[54] * z[55];
z[57] = z[43] + -z[56];
z[58] = z[37] / T(6);
z[57] = T(2) * z[57] + -z[58];
z[57] = z[25] * z[57];
z[24] = z[24] + z[29] + z[31] + -z[32] + z[36] + T(2) * z[38] + z[44] + z[57];
z[24] = z[4] * z[24];
z[29] = z[21] + z[27];
z[31] = T(3) * z[6];
z[36] = T(3) * z[7];
z[38] = z[20] + z[29] + -z[31] + -z[36];
z[38] = z[13] * z[38];
z[44] = T(2) * z[6];
z[27] = -z[27] + z[42] + z[44];
z[57] = T(2) * z[0];
z[27] = z[27] * z[57];
z[27] = z[27] + z[38] + z[41];
z[27] = T(2) * z[27] + z[40];
z[27] = z[27] * z[48];
z[38] = -z[36] + z[52];
z[40] = z[1] + z[10];
z[41] = -z[31] + z[38] + -z[40];
z[59] = z[0] * z[41];
z[60] = z[6] + z[7];
z[61] = z[20] + -z[60];
z[62] = z[25] * z[61];
z[62] = z[59] + T(-2) * z[62];
z[63] = T(3) * z[1];
z[47] = -z[47] + z[60] + -z[63];
z[47] = z[12] * z[47];
z[47] = z[47] + T(2) * z[62];
z[47] = z[12] * z[47];
z[62] = -z[7] + z[50] + -z[52];
z[64] = T(4) * z[6];
z[65] = z[62] + -z[64];
z[66] = z[15] * z[65];
z[67] = z[1] * z[15];
z[51] = -z[51] + z[66] + T(8) * z[67];
z[51] = T(2) * z[51];
z[66] = z[40] + -z[60];
z[55] = z[55] * z[66];
z[59] = z[55] + -z[59];
z[59] = z[58] + T(2) * z[59];
z[59] = z[25] * z[59];
z[22] = -z[22] + z[39];
z[68] = z[12] * z[22];
z[69] = z[22] * z[25];
z[68] = z[68] + -z[69];
z[70] = z[1] + -z[6];
z[42] = T(2) * z[42] + -z[70];
z[42] = z[42] * z[57];
z[42] = z[42] + z[68];
z[22] = z[13] * z[22];
z[42] = z[22] + T(2) * z[42];
z[42] = z[33] * z[42];
z[57] = z[53] * z[66];
z[35] = (T(-2) * z[1]) / T(3) + (T(13) * z[6]) / T(6) + -z[35];
z[34] = -z[34] + z[35];
z[34] = z[34] * z[37];
z[27] = z[27] + z[32] + z[34] + z[42] + z[47] + z[51] + T(9) * z[57] + z[59];
z[27] = z[2] * z[27];
z[32] = -z[40] + z[46] + -z[60];
z[32] = z[0] * z[32];
z[34] = -z[20] + z[40];
z[34] = z[25] * z[34];
z[34] = z[32] + z[34];
z[40] = z[12] * z[41];
z[34] = T(8) * z[34] + z[40];
z[34] = z[12] * z[34];
z[36] = T(-8) * z[20] + z[36] + z[50];
z[36] = z[15] * z[36];
z[36] = (T(32) * z[19]) / T(3) + -z[36] + z[57];
z[29] = T(-3) * z[20] + z[29] + z[60];
z[29] = z[29] * z[37];
z[32] = T(-4) * z[32] + -z[55];
z[32] = T(2) * z[32] + z[37] / T(3);
z[32] = z[25] * z[32];
z[40] = T(4) * z[1] + z[7];
z[41] = z[40] + z[45] + -z[52];
z[41] = z[13] * z[41];
z[42] = -z[7] + z[20];
z[31] = -z[1] + -z[31] + T(4) * z[42];
z[31] = z[26] * z[31];
z[42] = z[0] * z[70];
z[31] = z[31] + -z[42];
z[31] = T(2) * z[31] + z[41];
z[31] = z[13] * z[31];
z[38] = -z[10] + z[38] + -z[64];
z[38] = z[26] * z[38];
z[41] = z[13] * z[61];
z[38] = z[38] + T(4) * z[41] + z[43];
z[41] = z[10] + -z[20];
z[41] = z[6] + T(4) * z[41] + z[63];
z[41] = z[11] * z[41];
z[38] = T(2) * z[38] + z[41];
z[38] = z[11] * z[38];
z[41] = z[6] * z[15];
z[20] = T(10) * z[1] + T(6) * z[6] + T(9) * z[7] + T(7) * z[10] + T(-16) * z[20];
z[20] = z[16] * z[20];
z[20] = z[20] + z[29] + z[31] + z[32] + z[34] + T(-2) * z[36] + z[38] + T(9) * z[41] + T(7) * z[67];
z[20] = z[3] * z[20];
z[29] = -z[10] + z[46];
z[31] = z[29] + -z[40] + z[44];
z[26] = -(z[26] * z[31]);
z[31] = -z[11] + -z[33];
z[28] = z[28] * z[31];
z[31] = T(8) * z[1] + z[65];
z[31] = z[0] * z[31];
z[26] = z[26] + z[28] + z[31];
z[26] = z[26] * z[48];
z[23] = -z[23] + -z[30] + z[64];
z[23] = z[23] * z[49];
z[28] = -z[29] + -z[39] + z[63];
z[28] = z[12] * z[28];
z[29] = z[42] + T(-2) * z[69];
z[28] = z[28] + T(2) * z[29];
z[28] = z[12] * z[28];
z[29] = T(2) * z[42] + z[68];
z[22] = z[22] + T(2) * z[29];
z[22] = z[22] * z[33];
z[29] = T(-9) * z[1] + T(5) * z[6] + -z[62];
z[29] = z[29] * z[53];
z[21] = z[7] / T(2) + -z[21] + -z[35];
z[21] = z[21] * z[37];
z[30] = -z[42] + z[56];
z[30] = T(2) * z[30] + -z[58];
z[25] = z[25] * z[30];
z[21] = z[21] + z[22] + z[23] + z[25] + z[26] + z[28] + z[29] + -z[51];
z[21] = z[5] * z[21];
z[22] = -z[2] + z[3];
z[22] = z[22] * z[66];
z[23] = z[4] + -z[5];
z[23] = z[23] * z[54];
z[22] = z[22] + z[23];
z[22] = z[17] * z[22];
return z[20] + z[21] + T(6) * z[22] + z[24] + z[27];
}



template IntegrandConstructorType<double> f_4_186_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_186_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_186_construct (const Kin<qd_real>&);
#endif

}