#include "f_4_70.h"

namespace PentagonFunctions {

template <typename T> T f_4_70_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_70_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_70_W_23 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[4] / T(2) + (T(-3) * kin.v[0]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + -kin.v[1] + T(1)) * kin.v[0] + (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(4) + T(-1)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[4]) * ((-kin.v[4] / T(2) + (T(-3) * kin.v[0]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + -kin.v[1] + T(1)) * kin.v[0] + (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(4) + T(-1)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * (-kin.v[2] / T(2) + (T(3) * kin.v[0]) / T(4) + (T(-3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-1) + kin.v[1]) + (-kin.v[2] / T(4) + kin.v[3] / T(2) + kin.v[4] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(3) * kin.v[3]) / T(4) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[0] * (-kin.v[2] / T(2) + (T(3) * kin.v[0]) / T(4) + (T(-3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-1) + kin.v[1]) + (-kin.v[2] / T(4) + kin.v[3] / T(2) + kin.v[4] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(3) * kin.v[3]) / T(4) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]));
c[1] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-kin.v[0] + -kin.v[4] + kin.v[2] + kin.v[3]) + rlog(-kin.v[4]) * (-kin.v[0] + -kin.v[4] + kin.v[2] + kin.v[3]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (prod_pow(abb[2], 2) * (abb[5] / T(2) + -abb[6] / T(2) + -abb[7] / T(2)) + abb[4] * (prod_pow(abb[2], 2) / T(2) + -abb[3]) + abb[3] * (abb[6] + abb[7] + -abb[5]) + abb[1] * (abb[2] * abb[4] + abb[1] * ((abb[4] * T(-3)) / T(2) + (abb[5] * T(-3)) / T(2) + (abb[6] * T(3)) / T(2) + (abb[7] * T(3)) / T(2)) + abb[2] * (abb[5] + -abb[6] + -abb[7])));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_70_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl24 = DLog_W_24<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),spdl23 = SpDLog_f_4_70_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), dl24(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl18(t), -rlog(t), dl17(t), dl16(t), dl5(t)}
;

        auto result = f_4_70_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_70_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[41];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[12];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[13];
z[10] = abb[2];
z[11] = abb[9];
z[12] = bc<TR>[0];
z[13] = abb[8];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = T(2) * z[11];
z[20] = int_to_imaginary<T>(1) * z[12];
z[21] = -z[19] + z[20];
z[21] = z[10] * z[21];
z[22] = z[16] * z[20];
z[23] = z[15] + z[22];
z[24] = -z[11] + z[20];
z[25] = T(2) * z[24];
z[26] = z[0] + z[25];
z[27] = z[0] * z[26];
z[28] = -z[11] + T(2) * z[20];
z[28] = z[11] * z[28];
z[29] = prod_pow(z[12], 2);
z[30] = (T(7) * z[29]) / T(6);
z[31] = T(2) * z[14];
z[21] = z[21] + z[23] + -z[27] + -z[28] + -z[30] + -z[31];
z[21] = z[4] * z[21];
z[32] = T(2) * z[15];
z[33] = T(2) * z[22] + z[32];
z[34] = z[0] / T(2);
z[35] = -z[25] + z[34];
z[35] = z[0] * z[35];
z[36] = z[10] / T(2);
z[37] = -z[19] + z[36];
z[38] = -z[0] + z[37];
z[38] = z[10] * z[38];
z[35] = -z[14] + z[33] + z[35] + z[38];
z[35] = z[2] * z[35];
z[24] = z[24] + z[34];
z[24] = z[0] * z[24];
z[38] = -z[11] + z[36];
z[38] = z[10] * z[38];
z[24] = z[14] + -z[23] + z[24] + -z[38];
z[38] = z[3] + z[5];
z[38] = z[24] * z[38];
z[39] = z[23] + z[28];
z[40] = -z[10] + z[20];
z[40] = z[10] * z[40];
z[30] = -z[30] + -z[39] + z[40];
z[30] = z[13] * z[30];
z[21] = z[21] + -z[30] + z[35] + z[38];
z[21] = z[1] * z[21];
z[35] = z[3] + T(-3) * z[5];
z[24] = z[24] * z[35];
z[34] = z[25] + z[34];
z[34] = z[0] * z[34];
z[34] = T(3) * z[14] + z[34];
z[26] = -z[26] + z[36];
z[26] = z[10] * z[26];
z[35] = z[29] / T(3);
z[26] = z[26] + z[34] + z[35];
z[26] = z[2] * z[26];
z[36] = (T(5) * z[29]) / T(6);
z[23] = T(3) * z[23] + -z[28] + -z[36];
z[28] = -z[10] + z[19];
z[38] = T(2) * z[0] + z[20] + z[28];
z[38] = z[10] * z[38];
z[40] = z[0] * z[25];
z[38] = -z[23] + z[38] + z[40];
z[38] = z[4] * z[38];
z[24] = z[24] + z[26] + z[30] + z[38];
z[24] = z[7] * z[24];
z[26] = z[0] + z[20];
z[38] = -z[10] + T(2) * z[26];
z[38] = z[10] * z[38];
z[40] = prod_pow(z[0], 2);
z[31] = z[31] + z[33] + z[35] + -z[38] + z[40];
z[33] = T(2) * z[9];
z[35] = z[31] * z[33];
z[20] = z[17] * z[20];
z[20] = z[20] + (T(5) * z[29]) / T(2);
z[29] = z[17] / T(2);
z[20] = z[20] * z[29];
z[29] = int_to_imaginary<T>(1) * prod_pow(z[12], 3);
z[35] = -z[20] + z[29] / T(6) + z[35];
z[38] = -z[2] + z[4];
z[35] = z[35] * z[38];
z[31] = z[31] * z[38];
z[28] = z[10] * z[28];
z[22] = -z[14] + z[22];
z[22] = T(2) * z[22] + -z[27] + -z[28] + z[32];
z[27] = z[5] * z[22];
z[27] = -z[27] + z[31];
z[28] = -z[26] + -z[37];
z[28] = z[10] * z[28];
z[28] = z[28] + z[34] + -z[36] + -z[39];
z[28] = z[3] * z[28];
z[28] = -z[27] + z[28] + -z[30];
z[28] = z[8] * z[28];
z[19] = (T(-3) * z[10]) / T(2) + z[19] + z[26];
z[19] = z[10] * z[19];
z[25] = (T(3) * z[0]) / T(2) + z[25];
z[25] = z[0] * z[25];
z[19] = z[14] + z[19] + -z[23] + z[25];
z[19] = z[3] * z[19];
z[19] = z[19] + -z[27] + z[30];
z[19] = z[6] * z[19];
z[22] = z[22] * z[33];
z[20] = -z[20] + z[22] + z[29] / T(12);
z[22] = z[3] + -z[5];
z[20] = z[20] * z[22];
z[22] = z[22] + T(21) * z[38];
z[22] = z[18] * z[22];
return z[19] + z[20] + z[21] + z[22] / T(8) + z[24] + z[28] + z[35];
}



template IntegrandConstructorType<double> f_4_70_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_70_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_70_construct (const Kin<qd_real>&);
#endif

}