#include "f_4_328.h"

namespace PentagonFunctions {

template <typename T> T f_4_328_abbreviated (const std::array<T,41>&);

template <typename T> class SpDLog_f_4_328_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_328_W_23 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[1], 2) * (abb[5] * T(-3) + abb[6] * T(-3) + abb[3] * T(3) + abb[4] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_328_W_10 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_328_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-2) * kin.v[1] + T(-8) * kin.v[2] + T(4) * kin.v[3]) + kin.v[2] * (T(-6) * kin.v[2] + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(-2) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));
c[1] = bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(3) * kin.v[2] + T(2) * kin.v[3]) + kin.v[2] * (T(6) * kin.v[2] + T(4) * kin.v[3]) + kin.v[1] * ((bc<T>[0] * int_to_imaginary<T>(5) + T(10)) * kin.v[1] + T(16) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(2) * kin.v[3])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[2] * (T(3) * kin.v[2] + T(-2) * kin.v[3]) + kin.v[1] * (kin.v[1] + T(4) * kin.v[2] + T(-2) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4]))) + rlog(kin.v[2]) * (((T(3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2]))) + rlog(-kin.v[4]) * (((T(3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2]))) + rlog(-kin.v[1]) * (kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(3) * kin.v[3]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-6) * kin.v[2] + T(3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[2] + T(3) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(3) * kin.v[3]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-6) * kin.v[2] + T(3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[2] + T(3) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[1] + T(8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[2];
c[3] = rlog(kin.v[2]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2]) + rlog(-kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2]) + rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[2] + T(6) * kin.v[2]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[2] + T(6) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[27] * (abb[2] * (abb[3] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[30] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(3) + abb[29] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[2] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[29] * T(-4) + abb[30] * T(-2) + abb[6] * T(3)) + abb[9] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[29] * T(-4) + abb[30] * T(-2) + abb[6] * T(3))) + abb[12] * (abb[28] * T(-6) + abb[1] * abb[2] * T(-3) + abb[2] * (bc<T>[0] * int_to_imaginary<T>(3) + abb[2] * T(3) + abb[9] * T(3))) + abb[1] * abb[2] * (abb[3] + abb[4] + abb[5] + abb[8] + abb[6] * T(-3) + abb[30] * T(2) + abb[29] * T(4)) + abb[14] * (abb[2] * abb[12] * T(-3) + abb[15] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[29] * T(-4) + abb[30] * T(-2) + abb[6] * T(3) + abb[12] * T(3)) + abb[2] * (abb[3] + abb[4] + abb[5] + abb[8] + abb[6] * T(-3) + abb[30] * T(2) + abb[29] * T(4))) + abb[28] * (abb[6] * T(-6) + abb[3] * T(2) + abb[4] * T(2) + abb[5] * T(2) + abb[8] * T(2) + abb[30] * T(4) + abb[29] * T(8)) + abb[15] * (abb[3] * bc<T>[0] * int_to_imaginary<T>(1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[30] * bc<T>[0] * int_to_imaginary<T>(2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[29] * bc<T>[0] * int_to_imaginary<T>(4) + abb[12] * (abb[9] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3) + abb[1] * T(3) + abb[2] * T(3)) + abb[1] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[29] * T(-4) + abb[30] * T(-2) + abb[6] * T(3)) + abb[2] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[29] * T(-4) + abb[30] * T(-2) + abb[6] * T(3)) + abb[9] * (abb[3] + abb[4] + abb[5] + abb[8] + abb[6] * T(-3) + abb[30] * T(2) + abb[29] * T(4)) + abb[15] * (abb[6] * T(-6) + abb[12] * T(-6) + abb[3] * T(2) + abb[4] * T(2) + abb[5] * T(2) + abb[8] * T(2) + abb[30] * T(4) + abb[29] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_328_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl4 = DLog_W_4<T>(kin),dl15 = DLog_W_15<T>(kin),dl19 = DLog_W_19<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl1 = DLog_W_1<T>(kin),dl10 = DLog_W_10<T>(kin),dl20 = DLog_W_20<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),spdl23 = SpDLog_f_4_328_W_23<T>(kin),spdl10 = SpDLog_f_4_328_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,41> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), dl4(t), rlog(kin.W[2] / kin_path.W[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_8(kin_path), f_2_1_14(kin_path), rlog(kin.W[1] / kin_path.W[1]), dl15(t), rlog(v_path[2]), rlog(-v_path[4]), dl19(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl28(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl29(t) / kin_path.SqrtDelta, f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl1(t), dl10(t), f_2_1_7(kin_path), -rlog(t), rlog(kin.W[19] / kin_path.W[19]), dl20(t), dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl16(t), dl17(t), dl3(t), dl2(t), dl5(t), dl18(t)}
;

        auto result = f_4_328_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_328_abbreviated(const std::array<T,41>& abb)
{
using TR = typename T::value_type;
T z[122];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[16];
z[4] = abb[31];
z[5] = abb[35];
z[6] = abb[36];
z[7] = abb[37];
z[8] = abb[39];
z[9] = abb[40];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[8];
z[14] = abb[12];
z[15] = abb[19];
z[16] = abb[20];
z[17] = abb[21];
z[18] = abb[22];
z[19] = abb[32];
z[20] = abb[34];
z[21] = abb[29];
z[22] = abb[30];
z[23] = abb[2];
z[24] = abb[9];
z[25] = abb[14];
z[26] = abb[15];
z[27] = bc<TR>[0];
z[28] = abb[23];
z[29] = abb[33];
z[30] = abb[38];
z[31] = abb[10];
z[32] = abb[11];
z[33] = abb[26];
z[34] = abb[13];
z[35] = abb[17];
z[36] = abb[18];
z[37] = abb[24];
z[38] = abb[25];
z[39] = abb[28];
z[40] = bc<TR>[3];
z[41] = bc<TR>[1];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[9];
z[45] = z[1] + -z[11];
z[46] = T(3) * z[13];
z[47] = T(3) * z[14];
z[48] = -z[10] + z[47];
z[49] = -z[12] + (T(-7) * z[45]) / T(3) + z[46] + -z[48];
z[49] = z[4] * z[49];
z[50] = z[10] + z[13];
z[51] = z[12] + z[14];
z[52] = -z[45] + -z[50] + z[51];
z[53] = z[2] * z[52];
z[54] = -z[10] + z[12] + -z[14];
z[55] = z[13] + z[45] + z[54];
z[56] = z[3] * z[55];
z[57] = z[16] + z[17] + z[18];
z[58] = z[15] * z[57];
z[56] = -z[56] + z[58];
z[58] = z[28] * z[57];
z[59] = z[56] + z[58];
z[60] = z[19] * z[57];
z[61] = z[20] * z[57];
z[49] = -z[49] + z[53] + -z[59] + z[60] + T(-3) * z[61];
z[53] = T(5) * z[14];
z[62] = T(3) * z[12];
z[63] = T(5) * z[10];
z[64] = -z[11] + T(-19) * z[13] + z[63];
z[64] = z[53] + -z[62] + z[64] / T(3);
z[65] = (T(5) * z[1]) / T(12) + (T(2) * z[21]) / T(3) + z[22] / T(3);
z[64] = z[64] / T(4) + z[65];
z[64] = z[6] * z[64];
z[66] = z[5] + -z[9];
z[67] = -z[7] + z[30];
z[68] = z[19] + z[20] + z[29];
z[69] = -z[66] / T(4) + (T(3) * z[67]) / T(4) + z[68] / T(9);
z[69] = int_to_imaginary<T>(1) * z[27] * z[69];
z[70] = z[11] + z[13];
z[71] = -z[1] + z[70];
z[72] = -z[14] + z[71];
z[73] = T(3) * z[34];
z[72] = z[72] * z[73];
z[73] = z[29] * z[57];
z[74] = (T(3) * z[73]) / T(4);
z[75] = z[72] + z[74];
z[76] = z[6] + -z[8];
z[77] = z[67] + -z[76];
z[66] = -z[66] + z[77];
z[78] = z[41] * z[66];
z[79] = z[10] + z[70];
z[80] = z[1] + z[51] + -z[79];
z[81] = z[33] * z[80];
z[82] = T(13) * z[10] + z[70];
z[82] = z[14] + -z[62] + z[82] / T(3);
z[65] = -z[65] + z[82] / T(4);
z[65] = z[8] * z[65];
z[82] = T(3) * z[10];
z[83] = T(5) * z[12] + z[14] + z[70] + -z[82];
z[83] = z[83] / T(2);
z[84] = (T(3) * z[1]) / T(2);
z[85] = -z[22] + z[83] + -z[84];
z[85] = -z[21] + z[85] / T(2);
z[86] = z[9] * z[85];
z[87] = z[47] + z[62];
z[88] = -z[50] + z[87];
z[89] = (T(-19) * z[11]) / T(3) + -z[88];
z[89] = (T(25) * z[1]) / T(6) + z[22] + z[89] / T(2);
z[89] = z[21] + z[89] / T(2);
z[89] = z[7] * z[89];
z[88] = (T(5) * z[11]) / T(3) + -z[88];
z[88] = z[1] / T(6) + z[22] + z[88] / T(2);
z[88] = z[21] + z[88] / T(2);
z[88] = z[5] * z[88];
z[53] = z[10] + z[12] + z[53] + T(-3) * z[70];
z[53] = z[53] / T(2);
z[90] = z[1] / T(2);
z[91] = -z[22] + z[53] + z[90];
z[91] = -z[21] + z[91] / T(2);
z[92] = z[30] * z[91];
z[77] = z[42] * z[77];
z[49] = -z[49] / T(4) + z[64] + z[65] + z[69] + z[75] + T(-3) * z[77] + z[78] + (T(5) * z[81]) / T(4) + z[86] + z[88] + z[89] + z[92];
z[49] = z[27] * z[49];
z[64] = T(2) * z[21] + z[22];
z[65] = -z[1] + z[64];
z[69] = -z[10] + -z[47] + z[65] + T(2) * z[70];
z[77] = T(2) * z[6];
z[86] = T(2) * z[7];
z[88] = z[77] + z[86];
z[88] = z[69] * z[88];
z[89] = -z[64] + z[90];
z[53] = z[53] + z[89];
z[92] = z[30] * z[53];
z[93] = T(3) * z[92];
z[94] = -z[79] + z[87];
z[95] = -z[90] + z[94] / T(2);
z[96] = -z[64] + z[95];
z[97] = z[4] * z[96];
z[98] = z[58] + z[61];
z[99] = z[8] * z[96];
z[100] = z[5] * z[96];
z[88] = z[88] + z[93] + -z[97] + (T(-3) * z[98]) / T(2) + z[99] + z[100];
z[101] = z[23] * z[88];
z[82] = -z[62] + z[82];
z[102] = z[46] + -z[47];
z[103] = z[82] + -z[102];
z[104] = z[45] + z[103];
z[105] = z[4] / T(2);
z[106] = z[104] * z[105];
z[86] = z[45] * z[86];
z[107] = z[6] * z[96];
z[57] = (T(3) * z[57]) / T(2);
z[108] = z[29] * z[57];
z[107] = z[107] + -z[108];
z[109] = z[5] * z[45];
z[106] = (T(3) * z[59]) / T(2) + z[86] + -z[99] + -z[106] + z[107] + T(2) * z[109];
z[106] = z[0] * z[106];
z[103] = -z[45] + z[103];
z[110] = z[7] / T(2);
z[111] = z[103] * z[110];
z[112] = z[5] * z[104];
z[113] = (T(3) * z[81]) / T(2);
z[114] = -z[107] + z[113];
z[115] = T(2) * z[1];
z[116] = z[64] + z[115];
z[117] = -z[79] + z[116];
z[118] = z[8] * z[117];
z[119] = z[4] * z[45];
z[120] = z[118] + z[119];
z[121] = z[20] * z[57];
z[111] = -z[111] + z[112] / T(2) + -z[114] + z[120] + -z[121];
z[111] = z[24] * z[111];
z[96] = z[7] * z[96];
z[100] = z[96] + z[100] + -z[121];
z[117] = z[4] * z[117];
z[114] = z[100] + -z[114] + z[117] + T(2) * z[118];
z[117] = z[26] * z[114];
z[101] = -z[101] + -z[106] + z[111] + z[117];
z[106] = z[51] + z[79];
z[84] = z[64] + z[84];
z[106] = -z[84] + z[106] / T(2);
z[106] = z[8] * z[106];
z[111] = z[6] + z[7];
z[53] = z[53] * z[111];
z[58] = -z[58] + z[81];
z[117] = z[80] * z[105];
z[118] = z[73] / T(2);
z[53] = -z[53] + z[58] / T(2) + z[92] + z[106] + -z[117] + z[118];
z[58] = z[38] * z[53];
z[54] = -z[54] + -z[71];
z[71] = z[4] * z[54];
z[59] = -z[59] + z[71];
z[71] = z[8] * z[80];
z[80] = z[5] * z[55];
z[92] = z[7] * z[54];
z[71] = -z[59] + z[61] + -z[71] + z[80] + z[81] + z[92];
z[80] = z[36] * z[71];
z[81] = z[42] * z[66];
z[78] = -z[78] / T(2) + z[81];
z[78] = z[41] * z[78];
z[66] = -(z[43] * z[66]);
z[58] = z[58] + z[66] + z[78] + z[80] / T(2);
z[66] = -(z[6] * z[69]);
z[66] = z[66] + z[72] + z[86] + -z[109] + z[120];
z[66] = z[25] * z[66];
z[58] = T(3) * z[58] + T(2) * z[66] + z[101];
z[58] = int_to_imaginary<T>(1) * z[58];
z[66] = -(z[40] * z[68]);
z[49] = z[49] + z[58] + T(3) * z[66];
z[49] = z[27] * z[49];
z[58] = T(3) * z[21] + (T(3) * z[22]) / T(2);
z[46] = z[11] + -z[46];
z[46] = (T(3) * z[12]) / T(2) + z[46] / T(2) + z[47] + -z[58] + -z[115];
z[46] = z[7] * z[46];
z[66] = T(3) * z[30];
z[66] = z[66] * z[91];
z[68] = -z[56] + -z[61];
z[78] = -z[22] + z[95];
z[78] = -z[21] + z[78] / T(2);
z[80] = z[6] * z[78];
z[81] = z[45] + -z[102];
z[81] = z[81] * z[105];
z[86] = -z[22] + z[79];
z[86] = -z[1] + -z[21] + z[86] / T(2);
z[86] = z[8] * z[86];
z[46] = z[46] + -z[66] + (T(3) * z[68]) / T(4) + -z[75] + z[80] + z[81] + -z[86] + z[112] / T(4) + -z[113];
z[46] = z[25] * z[46];
z[46] = z[46] + -z[101];
z[46] = z[25] * z[46];
z[63] = T(-9) * z[14] + z[62] + -z[63] + T(7) * z[70];
z[68] = (T(5) * z[1]) / T(2);
z[63] = z[63] / T(2) + z[64] + -z[68];
z[63] = z[63] * z[111];
z[75] = T(9) * z[12];
z[47] = T(-7) * z[10] + -z[47] + T(5) * z[70] + z[75];
z[47] = (T(-7) * z[1]) / T(2) + z[47] / T(2) + -z[64];
z[64] = z[5] + z[8];
z[47] = -(z[47] * z[64]);
z[80] = z[83] + -z[84];
z[81] = T(3) * z[9];
z[83] = z[80] * z[81];
z[84] = T(4) * z[21] + T(2) * z[22];
z[91] = z[1] + z[84] + -z[94];
z[91] = z[4] * z[91];
z[92] = -(z[19] * z[57]);
z[47] = z[47] + z[63] + z[83] + z[91] + z[92] + z[93] + -z[108] + -z[121];
z[47] = z[39] * z[47];
z[48] = z[48] + T(-13) * z[70] + -z[75];
z[48] = (T(23) * z[1]) / T(2) + T(5) * z[22] + z[48] / T(2);
z[48] = T(5) * z[21] + z[48] / T(2);
z[48] = z[8] * z[48];
z[63] = -z[79] + -z[87];
z[63] = z[63] / T(2) + z[68] + z[84];
z[63] = z[4] * z[63];
z[68] = z[81] * z[85];
z[68] = (T(-3) * z[60]) / T(4) + z[68];
z[75] = z[5] + z[111];
z[75] = z[75] * z[78];
z[48] = z[48] + (T(-3) * z[61]) / T(4) + z[63] + z[66] + z[68] + z[72] + -z[74] + z[75];
z[48] = z[26] * z[48];
z[63] = z[24] * z[114];
z[48] = z[48] + z[63];
z[48] = z[26] * z[48];
z[63] = -z[10] + z[14];
z[66] = T(2) * z[11];
z[58] = z[58] + -z[62] + (T(-3) * z[63]) / T(2) + z[66] + -z[90];
z[58] = z[5] * z[58];
z[63] = z[7] * z[104];
z[72] = -z[22] + z[50];
z[72] = -z[11] + -z[21] + z[72] / T(2) + z[90];
z[72] = z[6] * z[72];
z[56] = -z[56] / T(2) + z[61];
z[74] = -(z[8] * z[78]);
z[75] = z[45] + z[82];
z[75] = z[75] * z[105];
z[56] = (T(3) * z[56]) / T(2) + z[58] + z[63] / T(4) + z[68] + z[72] + z[74] + z[75];
z[56] = z[0] * z[56];
z[58] = T(2) * z[10] + -z[62] + -z[70] + z[116];
z[58] = z[58] * z[64];
z[62] = z[19] + -z[28];
z[57] = z[57] * z[62];
z[57] = z[57] + T(-2) * z[58] + -z[83] + -z[96] + z[97] + -z[107];
z[57] = z[26] * z[57];
z[62] = -z[50] + z[65] + z[66];
z[63] = z[6] * z[62];
z[64] = -(z[104] * z[110]);
z[65] = z[5] * z[103];
z[63] = z[63] + z[64] + z[65] / T(2) + z[99] + -z[119] + -z[121];
z[63] = z[24] * z[63];
z[56] = z[56] + z[57] + z[63];
z[56] = z[0] * z[56];
z[57] = z[35] * z[71];
z[63] = -(z[6] * z[52]);
z[55] = -(z[7] * z[55]);
z[54] = -(z[5] * z[54]);
z[54] = z[54] + z[55] + z[59] + z[61] + z[63] + z[73];
z[54] = z[32] * z[54];
z[55] = z[0] + -z[24];
z[59] = z[0] + -z[23];
z[59] = z[55] * z[59];
z[59] = z[31] + z[32] + z[59];
z[59] = z[2] * z[52] * z[59];
z[54] = -z[54] + z[57] + -z[59];
z[57] = -(z[26] * z[88]);
z[59] = z[4] + z[77];
z[59] = z[59] * z[62];
z[61] = z[7] * z[69];
z[58] = z[58] + -z[59] + z[61];
z[58] = z[23] * z[58];
z[59] = z[59] + z[99] + z[100];
z[55] = z[55] * z[59];
z[55] = z[55] + z[57] + z[58];
z[55] = z[23] * z[55];
z[53] = z[37] * z[53];
z[57] = -z[8] + z[9];
z[57] = z[57] * z[80];
z[50] = T(-3) * z[11] + z[50] + z[51];
z[50] = z[50] / T(2) + z[89];
z[50] = z[6] * z[50];
z[51] = -(z[52] * z[105]);
z[52] = z[60] + -z[98];
z[50] = z[50] + z[51] + -z[52] / T(2) + z[57] + -z[118];
z[51] = T(3) * z[31];
z[50] = z[50] * z[51];
z[52] = z[105] + -z[110];
z[45] = z[45] * z[52];
z[45] = z[45] + z[72] + -z[86] + -z[109] / T(2);
z[45] = prod_pow(z[24], 2) * z[45];
z[51] = -(z[51] * z[80]);
z[51] = (T(-5) * z[44]) / T(2) + z[51];
z[51] = z[5] * z[51];
z[52] = z[9] + (T(-17) * z[76]) / T(3);
z[52] = (T(5) * z[52]) / T(2) + z[67];
z[52] = z[44] * z[52];
return z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + T(-3) * z[53] + (T(-3) * z[54]) / T(2) + z[55] + z[56];
}



template IntegrandConstructorType<double> f_4_328_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_328_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_328_construct (const Kin<qd_real>&);
#endif

}