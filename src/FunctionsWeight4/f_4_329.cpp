#include "f_4_329.h"

namespace PentagonFunctions {

template <typename T> T f_4_329_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_329_W_7 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_329_W_7 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (T(6) * kin.v[1] * kin.v[4] + kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(6) * kin.v[1] * kin.v[4] + kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4])) + rlog(-kin.v[1]) * (T(-6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[3] * (T(-6) + T(-6) * kin.v[1] + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(-6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[3] * (T(-6) + T(-6) * kin.v[1] + T(3) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(3) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[6] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(-3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_329_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_329_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(2) * kin.v[1] + T(-8) * kin.v[2]) + kin.v[0] * (T(2) * kin.v[0] + T(4) * kin.v[1] + T(-8) * kin.v[2]) + T(-2) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(6) * kin.v[2] + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(4) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[2] * (T(-6) * kin.v[2] + T(-20) * kin.v[3]) + T(-14) * prod_pow(kin.v[3], 2) + kin.v[1] * (T(-10) * kin.v[1] + T(16) * kin.v[2] + T(24) * kin.v[3]) + kin.v[0] * ((T(-10) + bc<T>[0] * int_to_imaginary<T>(-5)) * kin.v[0] + T(-20) * kin.v[1] + T(16) * kin.v[2] + T(24) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-10) * kin.v[1] + T(8) * kin.v[2] + T(12) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-7) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(-3) * kin.v[2] + T(-10) * kin.v[3]) + kin.v[1] * (T(-5) * kin.v[1] + T(8) * kin.v[2] + T(12) * kin.v[3])) + rlog(-kin.v[1]) * (prod_pow(kin.v[3], 2) / T(2) + ((T(-3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (-kin.v[1] / T(2) + T(2) * kin.v[2]) + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[1] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(kin.v[2]) * (prod_pow(kin.v[3], 2) / T(2) + ((T(-3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (-kin.v[1] / T(2) + T(2) * kin.v[2]) + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[1] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (prod_pow(kin.v[3], 2) / T(2) + ((T(-3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (-kin.v[1] / T(2) + T(2) * kin.v[2]) + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[1] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (prod_pow(kin.v[3], 2) / T(2) + ((T(-3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (-kin.v[1] / T(2) + T(2) * kin.v[2]) + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[1] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (kin.v[0] / T(2) + -kin.v[2] + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[4]) * (kin.v[1] * (-kin.v[1] + T(4) * kin.v[2]) + kin.v[0] * (-kin.v[0] + T(-2) * kin.v[1] + T(4) * kin.v[2]) + prod_pow(kin.v[3], 2) + kin.v[2] * (T(-3) * kin.v[2] + T(-2) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-3) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-6) * kin.v[2]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(3) * kin.v[1] + T(-6) * kin.v[2]) + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-3) * prod_pow(kin.v[3], 2)) / T(2) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-6) * kin.v[2]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(3) * kin.v[1] + T(-6) * kin.v[2]) + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[0] + T(8) * kin.v[1] + T(-8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + T(-8) * kin.v[3];
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[1] + T(-3) * kin.v[2] + T(-3) * kin.v[3])) + rlog(-kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(kin.v[2]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[4]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[16] * (abb[3] * (abb[14] * abb[15] + -(abb[9] * abb[15]) + abb[15] * (-abb[15] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[17] * T(2)) + abb[9] * abb[15] * (-abb[5] + -abb[6] + -abb[12] + abb[19] * T(-4) + abb[18] * T(-2) + abb[4] * T(3) + abb[8] * T(3)) + abb[15] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[18] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3) + abb[8] * bc<T>[0] * int_to_imaginary<T>(3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[15] * (-abb[5] + -abb[6] + -abb[12] + abb[19] * T(-4) + abb[18] * T(-2) + abb[4] * T(3) + abb[8] * T(3))) + abb[14] * abb[15] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[8] * T(-3) + abb[18] * T(2) + abb[19] * T(4)) + abb[1] * (abb[3] * abb[15] + abb[2] * (-abb[3] + -abb[5] + -abb[6] + -abb[12] + abb[19] * T(-4) + abb[18] * T(-2) + abb[4] * T(3) + abb[8] * T(3)) + abb[15] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[8] * T(-3) + abb[18] * T(2) + abb[19] * T(4))) + abb[17] * (abb[4] * T(-6) + abb[8] * T(-6) + abb[5] * T(2) + abb[6] * T(2) + abb[12] * T(2) + abb[18] * T(4) + abb[19] * T(8)) + abb[2] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(1) + abb[3] * (abb[9] + -abb[14] + -abb[15] + bc<T>[0] * int_to_imaginary<T>(1)) + abb[18] * bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(4) + abb[14] * (-abb[5] + -abb[6] + -abb[12] + abb[19] * T(-4) + abb[18] * T(-2) + abb[4] * T(3) + abb[8] * T(3)) + abb[15] * (-abb[5] + -abb[6] + -abb[12] + abb[19] * T(-4) + abb[18] * T(-2) + abb[4] * T(3) + abb[8] * T(3)) + abb[9] * (abb[5] + abb[6] + abb[12] + abb[4] * T(-3) + abb[8] * T(-3) + abb[18] * T(2) + abb[19] * T(4)) + abb[2] * (abb[4] * T(-6) + abb[8] * T(-6) + abb[3] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[12] * T(2) + abb[18] * T(4) + abb[19] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_329_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl4 = DLog_W_4<T>(kin),dl8 = DLog_W_8<T>(kin),dl21 = DLog_W_21<T>(kin),dl17 = DLog_W_17<T>(kin),dl26 = DLog_W_26<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),spdl7 = SpDLog_f_4_329_W_7<T>(kin),spdl21 = SpDLog_f_4_329_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,42> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl4(t), rlog(kin.W[15] / kin_path.W[15]), rlog(-v_path[4]), f_2_1_4(kin_path), f_2_1_11(kin_path), rlog(kin.W[2] / kin_path.W[2]), dl8(t), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), dl21(t), f_2_1_15(kin_path), rlog(kin.W[4] / kin_path.W[4]), -rlog(t), dl17(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl26(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl1(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl5(t), dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl18(t), dl20(t), dl16(t), dl3(t), dl19(t), dl2(t)}
;

        auto result = f_4_329_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_329_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[126];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[20];
z[4] = abb[31];
z[5] = abb[36];
z[6] = abb[37];
z[7] = abb[39];
z[8] = abb[40];
z[9] = abb[41];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[8];
z[14] = abb[12];
z[15] = abb[18];
z[16] = abb[19];
z[17] = abb[23];
z[18] = abb[24];
z[19] = abb[25];
z[20] = abb[26];
z[21] = abb[27];
z[22] = abb[33];
z[23] = abb[34];
z[24] = abb[2];
z[25] = abb[9];
z[26] = abb[14];
z[27] = abb[15];
z[28] = bc<TR>[0];
z[29] = abb[35];
z[30] = abb[38];
z[31] = abb[32];
z[32] = abb[10];
z[33] = abb[11];
z[34] = abb[28];
z[35] = abb[17];
z[36] = abb[21];
z[37] = abb[22];
z[38] = abb[29];
z[39] = abb[30];
z[40] = abb[13];
z[41] = bc<TR>[3];
z[42] = bc<TR>[1];
z[43] = bc<TR>[2];
z[44] = bc<TR>[4];
z[45] = bc<TR>[9];
z[46] = -z[18] + -z[19] + z[20] + z[21];
z[47] = z[17] * z[46];
z[48] = z[22] * z[46];
z[49] = z[47] + -z[48];
z[50] = -z[1] + z[10];
z[51] = z[13] + -z[14];
z[52] = z[50] + -z[51];
z[53] = -z[11] + z[12];
z[54] = z[52] + z[53];
z[55] = z[3] * z[54];
z[56] = z[29] * z[46];
z[57] = z[49] + z[55] + -z[56];
z[58] = T(3) * z[10];
z[59] = T(3) * z[13];
z[60] = z[58] + z[59];
z[61] = z[1] + z[14];
z[62] = z[60] + -z[61];
z[63] = z[62] / T(2);
z[64] = z[11] / T(2);
z[65] = z[12] / T(2);
z[66] = z[63] + -z[64] + -z[65];
z[67] = z[15] + T(2) * z[16];
z[68] = z[66] + -z[67];
z[69] = z[8] * z[68];
z[70] = z[6] * z[68];
z[71] = T(2) * z[53];
z[72] = z[9] * z[71];
z[73] = z[7] * z[71];
z[57] = (T(-3) * z[57]) / T(2) + z[69] + -z[70] + z[72] + z[73];
z[57] = z[0] * z[57];
z[72] = z[7] * z[68];
z[69] = z[69] + z[72];
z[72] = z[9] * z[68];
z[74] = -z[61] + z[67];
z[75] = T(2) * z[12];
z[76] = -z[11] + z[75];
z[77] = z[74] + z[76];
z[78] = z[6] * z[77];
z[79] = T(2) * z[78];
z[80] = z[10] + z[13];
z[81] = -z[61] + z[80];
z[82] = z[53] + z[81];
z[83] = z[34] * z[82];
z[84] = -z[56] + z[83];
z[85] = (T(3) * z[84]) / T(2);
z[86] = z[79] + -z[85];
z[72] = z[69] + z[72] + z[86];
z[72] = z[27] * z[72];
z[85] = -z[78] + z[85];
z[85] = z[25] * z[85];
z[57] = z[57] + -z[72] + z[85];
z[72] = z[6] * z[82];
z[85] = z[9] * z[54];
z[49] = z[49] + z[55] + z[72] + -z[83] + -z[85];
z[72] = (T(3) * z[37]) / T(2);
z[72] = z[49] * z[72];
z[85] = T(2) * z[11];
z[87] = -z[12] + z[85];
z[88] = -z[1] + T(2) * z[14] + -z[59] + z[67] + z[87];
z[89] = z[8] * z[88];
z[90] = z[51] + z[53];
z[90] = z[40] * z[90];
z[91] = T(3) * z[90];
z[92] = z[9] * z[53];
z[73] = -z[73] + -z[78] + z[89] + z[91] + z[92];
z[73] = z[26] * z[73];
z[78] = z[61] + z[80];
z[78] = z[78] / T(2);
z[80] = (T(3) * z[12]) / T(2) + -z[64];
z[89] = z[67] + z[80];
z[92] = z[78] + -z[89];
z[92] = z[6] * z[92];
z[93] = z[31] * z[46];
z[94] = z[48] + z[93];
z[84] = z[84] + -z[94];
z[84] = z[84] / T(2) + z[92];
z[92] = T(3) * z[14];
z[95] = T(5) * z[13];
z[96] = z[1] + z[10] + -z[92] + z[95];
z[96] = z[96] / T(2);
z[97] = (T(3) * z[11]) / T(2) + -z[65];
z[98] = z[67] + z[97];
z[99] = z[96] + -z[98];
z[100] = z[8] * z[99];
z[100] = -z[84] + z[100];
z[100] = z[39] * z[100];
z[101] = z[5] + z[6];
z[102] = z[42] / T(2) + -z[43];
z[102] = z[42] * z[102];
z[101] = z[101] * z[102];
z[103] = -z[5] + z[9];
z[104] = z[7] + z[8];
z[105] = z[6] + z[30] + -z[103] + -z[104];
z[105] = z[44] * z[105];
z[100] = z[100] + z[101] + z[105];
z[101] = T(2) * z[24];
z[105] = z[88] * z[101];
z[106] = z[25] * z[68];
z[107] = z[105] + -z[106];
z[108] = T(3) * z[102];
z[109] = z[107] + -z[108];
z[109] = z[8] * z[109];
z[110] = T(3) * z[52];
z[111] = z[53] + z[110];
z[112] = z[25] / T(2);
z[111] = z[111] * z[112];
z[105] = -z[105] + z[111];
z[113] = T(3) * z[99];
z[114] = z[39] * z[113];
z[52] = z[52] + -z[53];
z[115] = (T(3) * z[52]) / T(2);
z[116] = z[37] * z[115];
z[114] = -z[105] + -z[108] + z[114] + z[116];
z[114] = z[7] * z[114];
z[117] = z[24] * z[68];
z[110] = -z[53] + z[110];
z[118] = z[0] / T(2);
z[119] = z[110] * z[118];
z[120] = z[27] * z[77];
z[121] = z[25] * z[53];
z[119] = z[117] + -z[119] + z[120] + z[121];
z[82] = (T(3) * z[82]) / T(2);
z[122] = z[39] * z[82];
z[71] = -(z[26] * z[71]);
z[71] = z[71] + -z[116] + -z[119] + z[122];
z[71] = z[4] * z[71];
z[116] = -z[70] + (T(3) * z[94]) / T(2);
z[122] = -(z[24] * z[116]);
z[112] = z[110] * z[112];
z[112] = z[112] + z[117];
z[108] = -z[108] + z[112];
z[108] = z[9] * z[108];
z[123] = z[24] + -z[39];
z[123] = z[99] * z[123];
z[102] = z[102] + z[123];
z[123] = T(3) * z[30];
z[102] = z[102] * z[123];
z[124] = -z[7] + z[30];
z[125] = z[22] + -z[23] + -z[29] + z[31];
z[124] = z[103] / T(4) + (T(-3) * z[124]) / T(4) + -z[125] / T(9);
z[124] = prod_pow(z[28], 2) * z[124];
z[71] = z[57] + z[71] + z[72] + T(2) * z[73] + T(3) * z[100] + z[102] + z[108] + z[109] + z[114] + z[122] + z[124];
z[71] = int_to_imaginary<T>(1) * z[71];
z[72] = z[50] + (T(7) * z[53]) / T(3) + z[59] + -z[92];
z[72] = z[4] * z[72];
z[73] = -z[53] + z[81];
z[81] = z[2] * z[73];
z[92] = z[47] + z[81];
z[46] = z[23] * z[46];
z[72] = -z[46] + z[55] + T(3) * z[56] + z[72] + T(-5) * z[83] + z[92] + z[94];
z[100] = T(5) * z[1];
z[102] = T(19) * z[14] + -z[100];
z[95] = z[58] + -z[95] + z[102] / T(3);
z[102] = -z[42] + T(3) * z[43];
z[108] = -z[11] / T(12) + (T(5) * z[12]) / T(12) + z[15] / T(3) + (T(2) * z[16]) / T(3) + z[102];
z[95] = z[95] / T(4) + -z[108];
z[95] = z[8] * z[95];
z[109] = T(-3) * z[1] + T(5) * z[10] + z[13] + z[14];
z[109] = z[109] / T(2);
z[80] = -z[15] + -z[80] + z[109];
z[80] = -z[16] + z[80] / T(2);
z[114] = -z[42] + -z[80];
z[114] = z[5] * z[114];
z[122] = T(13) * z[1] + z[14];
z[124] = -z[13] + z[58] + -z[122] / T(3);
z[108] = z[108] + z[124] / T(4);
z[108] = z[6] * z[108];
z[63] = -z[15] + z[63];
z[124] = (T(-5) * z[11]) / T(6) + -z[12] / T(6) + z[63];
z[124] = -z[16] + z[42] + z[124] / T(2);
z[124] = z[9] * z[124];
z[63] = (T(19) * z[11]) / T(6) + (T(-25) * z[12]) / T(6) + z[63];
z[63] = -z[16] + z[63] / T(2) + -z[102];
z[63] = z[7] * z[63];
z[96] = -z[15] + z[96] + -z[97];
z[96] = -z[16] + z[96] / T(2);
z[97] = -z[96] + z[102];
z[97] = z[30] * z[97];
z[63] = z[63] + z[72] / T(4) + z[91] + z[95] + z[97] + z[108] + z[114] + z[124];
z[63] = z[28] * z[63];
z[72] = z[41] * z[125];
z[63] = z[63] + z[71] + T(3) * z[72];
z[63] = z[28] * z[63];
z[71] = -z[1] + z[13];
z[72] = z[15] / T(2);
z[71] = z[10] + z[71] / T(2) + -z[72];
z[95] = T(3) * z[16];
z[71] = z[65] + T(3) * z[71] + -z[85] + -z[95];
z[71] = z[9] * z[71];
z[66] = -z[15] + z[66];
z[66] = -z[16] + z[66] / T(2);
z[85] = z[6] * z[66];
z[97] = z[46] + z[47] + z[48];
z[97] = -z[81] + -z[97] / T(2);
z[80] = z[5] * z[80];
z[97] = -z[80] + z[97] / T(2);
z[102] = (T(3) * z[55]) / T(4);
z[108] = -z[15] + z[61];
z[114] = z[11] + z[16] + -z[65] + -z[108] / T(2);
z[124] = z[8] * z[114];
z[110] = z[110] / T(4);
z[125] = z[7] * z[110];
z[71] = z[71] + z[85] + T(3) * z[97] + -z[102] + z[124] + z[125];
z[71] = z[0] * z[71];
z[89] = -z[89] + z[109];
z[97] = z[5] * z[89];
z[46] = z[46] + z[56];
z[109] = z[46] / T(2) + z[97];
z[76] = T(-2) * z[1] + z[14] + z[58] + -z[67] + -z[76];
z[124] = T(2) * z[76];
z[125] = z[6] * z[124];
z[109] = T(3) * z[109] + -z[125];
z[124] = -(z[9] * z[124]);
z[69] = z[69] + z[109] + z[124];
z[69] = z[27] * z[69];
z[48] = z[48] + z[81];
z[70] = (T(3) * z[48]) / T(2) + -z[70];
z[81] = z[25] * z[70];
z[70] = z[24] * z[70];
z[111] = z[111] + -z[117];
z[111] = z[9] * z[111];
z[74] = z[74] + z[87];
z[87] = z[25] * z[74];
z[124] = -(z[74] * z[101]);
z[124] = -z[87] + z[124];
z[124] = z[8] * z[124];
z[125] = -(z[7] * z[112]);
z[69] = z[69] + z[70] + z[71] + z[81] + z[111] + z[124] + z[125];
z[69] = z[0] * z[69];
z[60] = -z[60] + -z[61];
z[61] = T(2) * z[15] + T(4) * z[16];
z[70] = (T(5) * z[11]) / T(2);
z[60] = z[60] / T(2) + z[61] + -z[65] + z[70];
z[60] = z[24] * z[60];
z[60] = z[60] + z[87];
z[60] = z[24] * z[60];
z[65] = -(z[25] * z[77]);
z[65] = z[65] + z[117] + -z[120];
z[65] = z[27] * z[65];
z[68] = -(z[27] * z[68]);
z[71] = -(z[24] * z[74]);
z[50] = T(3) * z[50] + -z[53];
z[50] = z[50] * z[118];
z[50] = z[50] + z[68] + z[71] + z[121];
z[50] = z[0] * z[50];
z[51] = T(-3) * z[51] + -z[53];
z[51] = z[26] * z[51];
z[51] = z[51] / T(2) + z[119];
z[51] = z[26] * z[51];
z[68] = prod_pow(z[25], 2);
z[53] = z[53] * z[68];
z[53] = z[53] / T(2);
z[71] = -(z[38] * z[82]);
z[61] = z[11] + z[12] + z[61] + -z[62];
z[61] = z[35] * z[61];
z[62] = z[32] * z[115];
z[50] = z[50] + z[51] + -z[53] + z[60] + z[61] + z[62] + z[65] + z[71];
z[50] = z[4] * z[50];
z[51] = -z[10] + z[14];
z[51] = -z[13] + z[51] / T(2) + z[72];
z[51] = T(3) * z[51] + -z[64] + z[75] + z[95];
z[51] = z[7] * z[51];
z[47] = -z[47] + -z[56] + -z[93];
z[47] = z[47] / T(2) + z[83];
z[47] = z[47] / T(2) + -z[90];
z[60] = -(z[8] * z[66]);
z[61] = z[9] * z[110];
z[62] = z[96] * z[123];
z[64] = z[11] + z[108];
z[64] = -z[12] + -z[16] + z[64] / T(2);
z[64] = z[6] * z[64];
z[47] = T(3) * z[47] + z[51] + z[60] + z[61] + z[62] + z[64] + -z[102];
z[47] = z[26] * z[47];
z[51] = -(z[30] * z[113]);
z[51] = z[51] + z[116];
z[51] = z[24] * z[51];
z[60] = -(z[8] * z[107]);
z[61] = -(z[9] * z[112]);
z[62] = z[7] * z[105];
z[47] = z[47] + z[51] + -z[57] + z[60] + z[61] + z[62];
z[47] = z[26] * z[47];
z[51] = -(z[9] * z[52]);
z[57] = z[8] * z[73];
z[54] = z[7] * z[54];
z[51] = z[51] + z[54] + -z[55] + z[56] + z[57] + -z[92];
z[51] = z[32] * z[51];
z[54] = z[4] + -z[7];
z[52] = z[52] * z[54];
z[49] = -z[49] + z[52];
z[49] = z[36] * z[49];
z[49] = z[49] + z[51];
z[51] = z[88] * z[104];
z[52] = -(z[9] * z[76]);
z[51] = z[51] + z[52] + -z[79] + z[91];
z[51] = z[27] * z[51];
z[52] = z[106] + z[117];
z[52] = -(z[52] * z[104]);
z[54] = -(z[24] * z[109]);
z[55] = z[76] * z[101];
z[55] = z[55] + -z[106];
z[55] = z[9] * z[55];
z[56] = -(z[25] * z[86]);
z[51] = z[51] + z[52] + z[54] + z[55] + z[56];
z[51] = z[27] * z[51];
z[52] = z[4] * z[73];
z[48] = -z[46] + -z[48] + z[52];
z[52] = -z[78] + z[98];
z[52] = z[8] * z[52];
z[54] = z[6] + z[9];
z[54] = z[54] * z[89];
z[48] = z[48] / T(2) + z[52] + z[54] + -z[97];
z[48] = z[33] * z[48];
z[52] = z[38] * z[84];
z[48] = z[48] + z[52];
z[46] = z[46] + -z[94];
z[52] = z[46] / T(2) + z[97];
z[54] = T(-7) * z[1] + T(9) * z[10] + T(5) * z[14] + -z[59];
z[54] = (T(-7) * z[12]) / T(2) + z[54] / T(2) + -z[67] + z[70];
z[55] = -(z[6] * z[54]);
z[52] = T(3) * z[52] + z[55];
z[52] = z[35] * z[52];
z[46] = z[46] / T(4) + z[80];
z[46] = T(3) * z[46] + z[85];
z[46] = z[24] * z[46];
z[46] = z[46] + -z[81];
z[46] = z[24] * z[46];
z[55] = z[24] * z[66];
z[55] = z[55] + z[106];
z[55] = z[24] * z[55];
z[53] = z[53] + z[55];
z[54] = -(z[35] * z[54]);
z[54] = z[53] + z[54];
z[54] = z[9] * z[54];
z[55] = z[38] * z[113];
z[56] = T(9) * z[13] + -z[58];
z[57] = T(-7) * z[14] + z[56] + z[100];
z[57] = (T(7) * z[11]) / T(2) + (T(-5) * z[12]) / T(2) + -z[57] / T(2) + z[67];
z[57] = z[35] * z[57];
z[55] = -z[55] + z[57];
z[56] = -z[56] + -z[122];
z[56] = (T(23) * z[11]) / T(2) + (T(-13) * z[12]) / T(2) + T(5) * z[15] + z[56] / T(2);
z[56] = T(5) * z[16] + z[56] / T(2);
z[56] = z[24] * z[56];
z[56] = z[56] + T(2) * z[87];
z[56] = z[24] * z[56];
z[57] = z[68] * z[114];
z[56] = (T(85) * z[45]) / T(6) + z[55] + z[56] + z[57];
z[56] = z[8] * z[56];
z[53] = z[45] + z[53] + z[55];
z[53] = z[7] * z[53];
z[55] = z[35] + z[38];
z[55] = z[55] * z[99];
z[57] = prod_pow(z[24], 2) * z[96];
z[55] = z[55] + z[57];
z[55] = -z[45] + T(3) * z[55];
z[55] = z[30] * z[55];
z[57] = z[64] * z[68];
z[58] = (T(-17) * z[6]) / T(3) + z[103];
z[58] = z[45] * z[58];
return z[46] + z[47] + T(3) * z[48] + (T(3) * z[49]) / T(2) + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + (T(5) * z[58]) / T(2) + z[63] + z[69];
}



template IntegrandConstructorType<double> f_4_329_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_329_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_329_construct (const Kin<qd_real>&);
#endif

}