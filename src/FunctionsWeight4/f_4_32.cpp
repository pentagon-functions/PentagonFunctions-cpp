#include "f_4_32.h"

namespace PentagonFunctions {

template <typename T> T f_4_32_abbreviated (const std::array<T,16>&);

template <typename T> class SpDLog_f_4_32_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_32_W_7 (const Kin<T>& kin) {
        c[0] = (T(3) / T(2) + (T(3) * kin.v[1]) / T(2) + (T(-9) * kin.v[3]) / T(8) + (T(-9) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) * kin.v[1] * kin.v[4]) / T(2) + (T(3) / T(2) + (T(-9) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = (T(3) * kin.v[1] * kin.v[4]) / T(2) + kin.v[4] * (T(3) / T(2) + T(3) * kin.v[4]) + kin.v[3] * (T(3) / T(2) + (T(3) * kin.v[1]) / T(2) + T(3) * kin.v[3] + T(6) * kin.v[4]) + rlog(-kin.v[1]) * ((T(21) * kin.v[1] * kin.v[4]) / T(2) + (T(21) / T(2) + (T(21) * kin.v[1]) / T(2) + (T(-39) * kin.v[3]) / T(8) + (T(-39) * kin.v[4]) / T(4)) * kin.v[3] + (T(21) / T(2) + (T(-39) * kin.v[4]) / T(8)) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (((T(-15) * kin.v[3]) / T(4) + (T(-15) * kin.v[4]) / T(2) + T(9) + T(9) * kin.v[1]) * kin.v[3] + ((T(-15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + T(9) * kin.v[1] * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(15) * kin.v[3]) / T(4) + (T(15) * kin.v[4]) / T(2) + T(-9) + T(-9) * kin.v[1]) * kin.v[3] + ((T(15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + T(-9) * kin.v[1] * kin.v[4]);
c[2] = (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * rlog(-kin.v[1]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (T(3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (abb[4] * ((prod_pow(abb[2], 2) * T(-9)) / T(2) + (abb[3] * T(-3)) / T(2)) + prod_pow(abb[1], 2) * ((abb[5] * T(-3)) / T(2) + (abb[4] * T(9)) / T(2) + abb[7] * T(-3) + abb[6] * T(3)) + abb[3] * ((abb[5] * T(-3)) / T(2) + abb[6] * T(-3) + abb[7] * T(3)) + prod_pow(abb[2], 2) * ((abb[5] * T(3)) / T(2) + abb[6] * T(-3) + abb[7] * T(3)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_32_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl19 = DLog_W_19<T>(kin),dl25 = DLog_W_25<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),spdl7 = SpDLog_f_4_32_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,16> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), rlog(kin.W[1] / kin_path.W[1]), -rlog(t), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), dl19(t), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl20(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t), dl18(t)}
;

        auto result = f_4_32_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_32_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[33];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[11];
z[3] = abb[5];
z[4] = abb[14];
z[5] = abb[6];
z[6] = abb[7];
z[7] = abb[10];
z[8] = abb[15];
z[9] = bc<TR>[0];
z[10] = abb[2];
z[11] = abb[8];
z[12] = abb[9];
z[13] = abb[3];
z[14] = abb[12];
z[15] = abb[13];
z[16] = bc<TR>[2];
z[17] = bc<TR>[9];
z[18] = -z[1] + z[6];
z[19] = T(3) * z[18];
z[20] = T(2) * z[5];
z[21] = z[19] + -z[20];
z[22] = -z[2] + z[4];
z[23] = -(z[21] * z[22]);
z[24] = T(2) * z[8];
z[24] = z[18] * z[24];
z[23] = z[23] + -z[24];
z[24] = z[0] * z[23];
z[25] = -(z[15] * z[23]);
z[26] = prod_pow(z[9], 2);
z[27] = z[8] + z[22] / T(4);
z[27] = z[26] * z[27];
z[25] = z[24] + z[25] + z[27];
z[27] = int_to_imaginary<T>(1) * z[9];
z[25] = z[25] * z[27];
z[28] = -z[18] + z[20];
z[28] = z[8] * z[28];
z[29] = -z[5] + (T(3) * z[18]) / T(2);
z[29] = z[2] * z[29];
z[30] = z[12] * z[18];
z[31] = z[5] + -z[18] / T(2);
z[31] = z[4] * z[31];
z[28] = z[28] + z[29] + -z[30] + z[31];
z[28] = z[7] * z[28];
z[30] = -z[4] + z[12];
z[30] = z[18] * z[30];
z[20] = -(z[8] * z[20]);
z[20] = z[20] + z[30];
z[20] = z[20] * z[27];
z[20] = T(2) * z[20] + -z[24] + z[28];
z[20] = z[7] * z[20];
z[24] = -z[8] + z[12];
z[24] = z[18] * z[24];
z[27] = z[1] * z[11];
z[28] = (T(3) * z[1]) / T(2) + z[5] + -z[6];
z[28] = z[4] * z[28];
z[30] = -z[4] / T(2) + -z[11];
z[30] = z[3] * z[30];
z[24] = z[24] + z[27] + z[28] + z[30];
z[24] = prod_pow(z[10], 2) * z[24];
z[23] = z[14] * z[23];
z[21] = z[13] * z[21];
z[28] = T(3) * z[17];
z[30] = z[21] + -z[28];
z[30] = z[2] * z[30];
z[31] = (T(7) * z[1]) / T(2) + T(-3) * z[6];
z[32] = T(3) * z[5] + z[31];
z[32] = z[13] * z[32];
z[28] = z[28] + z[32];
z[28] = z[4] * z[28];
z[20] = z[20] + z[23] + z[24] + z[25] + z[28] + z[30];
z[23] = -z[4] + z[11];
z[23] = z[3] * z[23];
z[23] = -z[23] + z[27];
z[19] = z[12] * z[19];
z[24] = -z[5] + z[31] / T(2);
z[24] = z[4] * z[24];
z[18] = (T(-13) * z[5]) / T(2) + z[18];
z[18] = z[8] * z[18];
z[22] = z[16] * z[22];
z[18] = z[18] + z[19] + T(-3) * z[22] + -z[23] / T(4) + z[24] + -z[29];
z[18] = z[18] * z[26];
z[19] = T(25) * z[17] + T(-3) * z[21];
z[19] = z[8] * z[19];
z[21] = z[4] * z[6];
z[21] = (T(-3) * z[21]) / T(2) + z[29];
z[22] = prod_pow(z[0], 2);
z[22] = T(3) * z[22];
z[21] = z[21] * z[22];
z[22] = -z[13] + z[22];
z[22] = z[3] * z[4] * z[22];
return z[18] + z[19] + T(3) * z[20] + z[21] + (T(3) * z[22]) / T(2);
}



template IntegrandConstructorType<double> f_4_32_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_32_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_32_construct (const Kin<qd_real>&);
#endif

}