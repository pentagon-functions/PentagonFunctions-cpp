#include "f_4_121.h"

namespace PentagonFunctions {

template <typename T> T f_4_121_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_121_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_121_W_21 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(4) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(4) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * (abb[4] * T(-2) + abb[5] * T(-2)) + prod_pow(abb[2], 2) * abb[3] * T(2) + prod_pow(abb[1], 2) * (abb[3] * T(-2) + abb[4] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_121_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl19 = DLog_W_19<T>(kin),dl25 = DLog_W_25<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),spdl21 = SpDLog_f_4_121_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,17> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), dl19(t), -rlog(t), rlog(kin.W[24] / kin_path.W[24]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl18(t), f_2_1_12(kin_path), f_2_1_15(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl20(t), dl2(t)}
;

        auto result = f_4_121_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_4_121_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[28];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[9];
z[3] = abb[16];
z[4] = abb[4];
z[5] = abb[6];
z[6] = abb[5];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[2];
z[10] = abb[11];
z[11] = abb[15];
z[12] = abb[10];
z[13] = bc<TR>[0];
z[14] = abb[12];
z[15] = abb[13];
z[16] = abb[14];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = T(2) * z[2] + (T(-2) * z[3]) / T(3);
z[20] = z[4] + z[6];
z[21] = -z[1] + z[20];
z[19] = z[19] * z[21];
z[22] = z[3] + -z[10];
z[23] = (T(-5) * z[2]) / T(3) + z[11];
z[23] = z[22] / T(3) + z[23] / T(2);
z[23] = int_to_imaginary<T>(1) * z[13] * z[23];
z[20] = z[7] + z[8] + -z[20];
z[20] = z[5] * z[20];
z[24] = z[1] + -z[6];
z[24] = (T(-3) * z[4]) / T(2) + z[7] / T(6) + (T(4) * z[24]) / T(3);
z[24] = z[11] * z[24];
z[25] = -z[1] + z[4];
z[26] = (T(-3) * z[6]) / T(2) + (T(13) * z[8]) / T(6) + (T(2) * z[25]) / T(3);
z[26] = z[10] * z[26];
z[22] = -z[11] + z[22];
z[27] = z[17] * z[22];
z[19] = z[19] + -z[20] / T(6) + z[23] + z[24] + z[26] + T(-2) * z[27];
z[19] = z[13] * z[19];
z[23] = z[21] * z[22];
z[24] = z[9] + -z[16];
z[24] = z[23] * z[24];
z[26] = z[2] + -z[11];
z[26] = z[21] * z[26];
z[27] = -z[6] + z[8];
z[27] = z[10] * z[27];
z[26] = z[26] + z[27];
z[26] = z[12] * z[26];
z[24] = z[24] + z[26];
z[24] = int_to_imaginary<T>(1) * z[24];
z[19] = z[19] + T(4) * z[24];
z[19] = z[13] * z[19];
z[24] = z[0] + -z[12];
z[24] = z[23] * z[24];
z[26] = -z[4] + z[7];
z[26] = -(z[11] * z[26]);
z[27] = z[10] * z[21];
z[26] = z[26] + z[27];
z[26] = z[9] * z[26];
z[24] = T(2) * z[24] + z[26];
z[24] = z[9] * z[24];
z[23] = T(2) * z[23];
z[26] = z[14] + -z[15];
z[23] = z[23] * z[26];
z[26] = -z[2] + z[3];
z[21] = z[21] * z[26];
z[20] = z[20] + -z[21];
z[20] = prod_pow(z[0], 2) * z[20];
z[25] = -z[8] + -z[25];
z[25] = z[10] * z[25];
z[21] = z[21] + z[25];
z[21] = prod_pow(z[12], 2) * z[21];
z[20] = z[20] + z[21] + z[23] + z[24];
z[21] = z[18] * z[22];
return z[19] + T(2) * z[20] + T(3) * z[21];
}



template IntegrandConstructorType<double> f_4_121_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_121_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_121_construct (const Kin<qd_real>&);
#endif

}