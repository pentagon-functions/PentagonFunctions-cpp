#include "f_4_330.h"

namespace PentagonFunctions {

template <typename T> T f_4_330_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_330_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_330_W_12 (const Kin<T>& kin) {
        c[0] = ((T(-3) * kin.v[1]) / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[1] + (kin.v[4] / T(4) + T(1)) * kin.v[4];
c[1] = kin.v[1] + -kin.v[4];
c[2] = (-kin.v[4] / T(2) + (T(3) * kin.v[1]) / T(4) + T(1)) * kin.v[1] + (-kin.v[4] / T(4) + T(-1)) * kin.v[4];
c[3] = -kin.v[1] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[4] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]);
        }

        return abb[0] * (abb[3] * abb[5] + -(prod_pow(abb[2], 2) * abb[5]) / T(2) + abb[4] * (prod_pow(abb[2], 2) / T(2) + -abb[3]) + abb[1] * (abb[2] * abb[4] + abb[1] * ((abb[4] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2)) + -(abb[2] * abb[5])));
    }
};
template <typename T> class SpDLog_f_4_330_W_7 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_330_W_7 (const Kin<T>& kin) {
        c[0] = T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[3] * (T(-2) + T(-2) * kin.v[1] + kin.v[3] + T(2) * kin.v[4]);
c[1] = T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[3] * (T(-2) + T(-2) * kin.v[1] + kin.v[3] + T(2) * kin.v[4]);
c[2] = T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[3] * (T(-2) + T(-2) * kin.v[1] + kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[8] * c[0] + abb[5] * c[1] + abb[9] * c[2]);
        }

        return abb[6] * (prod_pow(abb[7], 2) * abb[8] + prod_pow(abb[7], 2) * (abb[5] + abb[9]) + prod_pow(abb[1], 2) * (-abb[5] + -abb[8] + -abb[9]));
    }
};
template <typename T> class SpDLog_f_4_330_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_330_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(5) * kin.v[1]) / T(2) + T(4) + T(-2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (T(4) + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[2]) + T(-4) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(-4) + kin.v[3] + T(4) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-4) + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(-2) * kin.v[3];
c[2] = kin.v[1] * ((T(5) * kin.v[1]) / T(2) + T(4) + T(-2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (T(4) + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[2]) + T(-4) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(-4) + kin.v[3] + T(4) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-4) + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(-2) * kin.v[3];
c[4] = kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(4) + T(-4) * kin.v[4]) + kin.v[2] * (kin.v[2] / T(2) + -kin.v[3] + T(4) + T(-4) * kin.v[4]) + kin.v[1] * ((T(-5) * kin.v[1]) / T(2) + T(-4) + T(2) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[0] * (-kin.v[1] + T(-4) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(-2) * kin.v[2] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[5] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]);
c[6] = kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(4) + T(-4) * kin.v[4]) + kin.v[2] * (kin.v[2] / T(2) + -kin.v[3] + T(4) + T(-4) * kin.v[4]) + kin.v[1] * ((T(-5) * kin.v[1]) / T(2) + T(-4) + T(2) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[0] * (-kin.v[1] + T(-4) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(-2) * kin.v[2] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[7] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[8] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]) + abb[12] * (t * c[6] + c[7]);
        }

        return abb[16] * (abb[14] * abb[15] * (abb[9] + abb[12] + -abb[5]) + abb[2] * abb[15] * (abb[5] + -abb[9] + -abb[12]) + abb[1] * (abb[15] * (abb[9] + abb[12] + -abb[5]) + abb[7] * (abb[5] + abb[8] + -abb[9] + -abb[12]) + -(abb[8] * abb[15])) + abb[15] * (abb[15] * (abb[9] + abb[12] + -abb[5]) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[7] * (abb[2] * (abb[9] + abb[12] + -abb[5]) + abb[14] * (abb[5] + -abb[9] + -abb[12]) + abb[15] * (abb[5] + -abb[9] + -abb[12]) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * (abb[14] + abb[15] + -abb[2] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[8] * (abb[2] * abb[15] + -(abb[14] * abb[15]) + abb[17] * T(-2) + abb[15] * (-abb[15] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[17] * (abb[5] * T(-2) + abb[9] * T(2) + abb[12] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_330_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl7 = DLog_W_7<T>(kin),dl4 = DLog_W_4<T>(kin),dl8 = DLog_W_8<T>(kin),dl21 = DLog_W_21<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl15 = DLog_W_15<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl31 = DLog_W_31<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),spdl12 = SpDLog_f_4_330_W_12<T>(kin),spdl7 = SpDLog_f_4_330_W_7<T>(kin),spdl21 = SpDLog_f_4_330_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,46> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_2_1_4(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl7(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl4(t), f_2_1_11(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl8(t), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), dl21(t), f_2_1_15(kin_path), dl17(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl1(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl15(t), dl3(t), dl20(t), dl19(t), dl2(t), dl5(t), dl18(t), dl16(t), dl31(t), dl29(t) / kin_path.SqrtDelta, rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_4_330_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl21(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_330_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[104];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[18];
z[3] = abb[25];
z[4] = abb[28];
z[5] = abb[29];
z[6] = abb[5];
z[7] = abb[10];
z[8] = abb[26];
z[9] = abb[30];
z[10] = abb[8];
z[11] = abb[9];
z[12] = abb[27];
z[13] = abb[12];
z[14] = abb[33];
z[15] = abb[34];
z[16] = abb[35];
z[17] = abb[36];
z[18] = abb[37];
z[19] = abb[38];
z[20] = abb[39];
z[21] = abb[40];
z[22] = abb[41];
z[23] = abb[43];
z[24] = abb[44];
z[25] = abb[2];
z[26] = abb[14];
z[27] = bc<TR>[0];
z[28] = abb[7];
z[29] = abb[15];
z[30] = abb[42];
z[31] = abb[24];
z[32] = abb[21];
z[33] = abb[3];
z[34] = abb[19];
z[35] = abb[20];
z[36] = abb[31];
z[37] = abb[11];
z[38] = abb[13];
z[39] = abb[17];
z[40] = abb[22];
z[41] = abb[23];
z[42] = abb[45];
z[43] = bc<TR>[3];
z[44] = abb[32];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[6] + z[10];
z[50] = T(-7) * z[11] + T(-11) * z[13];
z[50] = z[49] + z[50] / T(3);
z[50] = z[12] * z[50];
z[51] = z[10] / T(3);
z[52] = z[6] / T(3) + z[51];
z[53] = z[11] + z[13] / T(3);
z[54] = z[52] + -z[53];
z[54] = z[7] * z[54];
z[55] = z[11] + z[13];
z[52] = -z[52] + -z[55];
z[52] = z[36] * z[52];
z[56] = z[9] * z[49];
z[57] = z[9] * z[55];
z[50] = z[50] + z[52] + z[54] + z[56] + z[57] / T(3);
z[52] = z[51] + z[53];
z[53] = z[1] / T(3);
z[54] = z[6] / T(2);
z[52] = z[52] / T(2) + -z[53] + z[54];
z[52] = z[2] * z[52];
z[51] = z[11] / T(3) + -z[13] + z[51];
z[51] = z[6] / T(6) + z[51] / T(2) + -z[53];
z[51] = z[5] * z[51];
z[51] = z[51] + z[52];
z[52] = z[24] / T(4);
z[56] = z[8] + z[12];
z[58] = (T(-2) * z[5]) / T(3) + z[9] / T(3) + -z[56];
z[58] = -z[52] + z[58] / T(3);
z[59] = z[23] + z[42];
z[60] = z[3] + z[4];
z[58] = -z[30] / T(2) + z[36] / T(27) + (T(4) * z[44]) / T(27) + z[58] / T(3) + -z[59] / T(12) + -z[60] / T(9);
z[58] = int_to_imaginary<T>(1) * z[27] * z[58];
z[59] = z[16] + z[17];
z[61] = z[19] + z[20] + z[21] + z[22];
z[62] = -z[59] + T(3) * z[61];
z[62] = z[18] + z[62] / T(2);
z[63] = -z[15] + z[62] / T(2);
z[64] = z[30] * z[63];
z[65] = z[6] + z[55];
z[65] = z[38] * z[65];
z[66] = z[64] + z[65];
z[59] = -z[59] / T(3) + z[61];
z[59] = z[18] / T(3) + z[59] / T(2);
z[59] = -z[15] / T(3) + z[59] / T(2);
z[61] = -z[14] + z[23];
z[67] = z[24] + -z[61];
z[67] = z[59] * z[67];
z[59] = z[46] + z[59];
z[59] = z[42] * z[59];
z[68] = z[11] + T(5) * z[13];
z[68] = z[10] + z[68] / T(3);
z[53] = -z[6] / T(12) + z[53] + z[68] / T(4);
z[53] = z[3] * z[53];
z[68] = z[1] + -z[11];
z[69] = z[31] * z[68];
z[70] = T(5) * z[10] + -z[55];
z[71] = -z[6] + -z[70];
z[71] = z[8] * z[71];
z[72] = -z[10] + z[55];
z[73] = -z[6] + z[72] / T(3);
z[73] = z[32] * z[73];
z[74] = -z[14] / T(4) + (T(5) * z[24]) / T(4) + z[30];
z[74] = z[46] * z[74];
z[61] = -z[42] + z[61];
z[75] = -z[30] + -z[61] / T(3);
z[75] = z[45] * z[75];
z[76] = T(3) * z[11];
z[77] = (T(-5) * z[10]) / T(3) + -z[13] + z[76];
z[77] = -z[1] + (T(7) * z[6]) / T(12) + z[77] / T(4);
z[77] = z[4] * z[77];
z[50] = z[50] / T(4) + z[51] / T(2) + z[53] + z[58] + z[59] + z[66] + z[67] + (T(4) * z[69]) / T(3) + z[71] / T(12) + (T(5) * z[73]) / T(4) + z[74] + z[75] + z[77];
z[50] = z[27] * z[50];
z[51] = T(-2) * z[15] + z[62];
z[53] = z[30] * z[51];
z[58] = z[49] + -z[55];
z[59] = z[58] / T(2);
z[62] = z[12] * z[59];
z[67] = z[53] + z[62];
z[71] = T(3) * z[6];
z[73] = -z[71] + z[72];
z[74] = z[32] * z[73];
z[75] = z[74] / T(2);
z[77] = z[5] * z[6];
z[78] = z[67] + z[75] + z[77];
z[79] = z[49] + z[55];
z[80] = z[3] / T(2);
z[81] = z[79] * z[80];
z[82] = z[10] + -z[11];
z[83] = z[13] + z[82];
z[83] = z[83] / T(2);
z[54] = -z[1] + z[54];
z[84] = -z[54] + z[83];
z[85] = z[4] * z[84];
z[86] = z[6] * z[8];
z[81] = z[69] + z[78] + z[81] + -z[85] + z[86];
z[81] = z[25] * z[81];
z[87] = z[24] * z[51];
z[88] = z[10] + z[13];
z[89] = z[76] + z[88];
z[90] = (T(3) * z[6]) / T(2);
z[91] = -z[1] + z[90];
z[89] = z[89] / T(2) + z[91];
z[92] = z[2] * z[89];
z[87] = z[87] + z[92];
z[93] = z[14] * z[51];
z[94] = z[8] * z[59];
z[95] = z[93] + -z[94];
z[96] = z[5] * z[84];
z[67] = z[67] + -z[87] + z[95] + z[96];
z[67] = z[0] * z[67];
z[96] = z[42] * z[51];
z[97] = z[49] + T(3) * z[55];
z[98] = z[36] * z[97];
z[96] = -z[96] + z[98] / T(2);
z[99] = z[5] * z[59];
z[100] = z[12] * z[55];
z[99] = -z[95] + z[96] + -z[99] + T(-2) * z[100];
z[99] = z[28] * z[99];
z[101] = z[29] * z[59];
z[102] = z[28] * z[59];
z[68] = -z[6] + z[68];
z[103] = z[0] * z[68];
z[101] = z[101] + -z[102] + T(2) * z[103];
z[101] = z[4] * z[101];
z[59] = z[3] * z[59];
z[59] = z[59] + z[78] + T(2) * z[86];
z[59] = z[29] * z[59];
z[78] = z[28] * z[55];
z[78] = z[78] + z[103];
z[103] = T(2) * z[3];
z[78] = z[78] * z[103];
z[67] = -z[59] + z[67] + -z[78] + -z[81] + z[99] + -z[101];
z[78] = z[83] + -z[91];
z[78] = z[5] * z[78];
z[81] = z[8] / T(2);
z[83] = z[73] * z[81];
z[75] = -z[69] + -z[75] + z[78] + z[83] + -z[87] + z[93];
z[78] = -z[76] + z[88];
z[78] = z[78] / T(2);
z[83] = T(2) * z[1] + z[78] + -z[90];
z[88] = z[3] * z[83];
z[90] = -(z[4] * z[89]);
z[88] = -z[75] + z[88] + z[90];
z[88] = z[35] * z[88];
z[72] = z[71] + z[72];
z[72] = z[8] * z[72];
z[73] = z[5] * z[73];
z[90] = z[12] * z[97];
z[72] = -z[72] + z[73] + -z[74] + -z[90];
z[73] = z[53] + z[93];
z[90] = z[80] * z[97];
z[72] = z[72] / T(2) + -z[73] + -z[90] + z[96];
z[90] = -(z[41] * z[72]);
z[65] = z[65] + -z[77] + -z[100];
z[77] = -z[69] + z[86];
z[91] = -(z[4] * z[68]);
z[91] = z[65] + -z[77] + z[91];
z[91] = z[26] * z[91];
z[97] = z[45] / T(2) + -z[46];
z[97] = z[45] * z[97];
z[97] = z[47] + z[97];
z[61] = T(3) * z[30] + z[61];
z[61] = z[61] * z[97];
z[97] = -z[14] + z[24];
z[97] = prod_pow(z[46], 2) * z[97];
z[61] = z[61] + z[67] + z[88] + z[90] + T(2) * z[91] + z[97] / T(2);
z[61] = int_to_imaginary<T>(1) * z[61];
z[56] = z[56] + z[60];
z[56] = T(2) * z[5] + -z[9] + -z[36] + T(-4) * z[44] + T(3) * z[56];
z[56] = z[43] * z[56];
z[50] = z[50] + z[56] + z[61];
z[50] = z[27] * z[50];
z[56] = -z[49] + T(5) * z[55];
z[61] = z[12] * z[56];
z[71] = T(3) * z[10] + z[71];
z[88] = z[9] * z[71];
z[57] = z[57] + z[88];
z[61] = z[57] + z[61];
z[61] = z[61] / T(2);
z[88] = z[23] * z[51];
z[90] = z[5] * z[58];
z[70] = T(5) * z[6] + z[70];
z[81] = z[70] * z[81];
z[56] = -(z[56] * z[80]);
z[56] = z[53] + z[56] + -z[61] + z[81] + z[88] + -z[90] + -z[93] + z[96];
z[56] = z[39] * z[56];
z[81] = z[0] * z[58];
z[91] = z[81] + -z[102];
z[93] = z[28] / T(2);
z[91] = z[91] * z[93];
z[71] = z[55] + z[71];
z[93] = z[37] * z[71];
z[70] = z[39] * z[70];
z[70] = -z[70] + z[93];
z[93] = -(z[33] * z[83]);
z[96] = prod_pow(z[0], 2);
z[97] = z[11] + -z[13];
z[97] = -z[1] + -z[10] + z[97] / T(2);
z[97] = z[96] * z[97];
z[99] = z[34] * z[89];
z[100] = -z[0] + z[28];
z[101] = z[29] + T(-2) * z[100];
z[101] = z[29] * z[49] * z[101];
z[70] = -z[70] / T(2) + -z[91] + z[93] + z[97] + z[99] + z[101];
z[70] = z[4] * z[70];
z[93] = z[28] + -z[29];
z[93] = z[58] * z[93];
z[79] = z[0] * z[79];
z[79] = z[79] + z[93];
z[79] = z[4] * z[79];
z[60] = -(z[60] * z[68]);
z[68] = z[11] * z[12];
z[60] = z[60] + -z[68] + -z[77];
z[60] = z[25] * z[60];
z[60] = z[60] + z[79];
z[77] = -z[13] + z[49];
z[79] = -z[76] + z[77];
z[93] = z[7] * z[79];
z[97] = z[93] / T(2);
z[99] = z[5] * z[11];
z[95] = z[95] + z[97] + z[99];
z[99] = z[68] + z[95];
z[99] = z[0] * z[99];
z[95] = T(2) * z[68] + z[95];
z[101] = -(z[28] * z[95]);
z[103] = -(z[0] * z[84]);
z[102] = z[102] + z[103];
z[102] = z[3] * z[102];
z[59] = -z[59] + z[60] / T(2) + z[99] + z[101] + z[102];
z[59] = z[25] * z[59];
z[60] = z[1] + -z[6] + z[13];
z[60] = z[5] * z[60];
z[60] = z[60] + -z[62] + T(-3) * z[69] + -z[74] + z[85] + -z[86];
z[62] = z[24] * z[63];
z[62] = z[62] + z[92] / T(2);
z[69] = z[42] * z[63];
z[69] = -z[69] + z[98] / T(4);
z[74] = -z[10] / T(2) + -z[13] + z[54];
z[74] = z[3] * z[74];
z[60] = z[60] / T(2) + -z[62] + -z[66] + z[69] + z[74];
z[60] = z[26] * z[60];
z[60] = z[60] + -z[67];
z[60] = z[26] * z[60];
z[66] = z[12] * z[79];
z[54] = -z[54] + z[78];
z[54] = z[5] * z[54];
z[54] = z[53] + z[54] + z[66] / T(2) + -z[87] + -z[97];
z[54] = z[33] * z[54];
z[66] = z[34] * z[75];
z[67] = -z[76] + -z[77];
z[67] = z[12] * z[67];
z[74] = z[5] * z[79];
z[71] = -(z[8] * z[71]);
z[67] = z[57] + z[67] + z[71] + z[74] + -z[93];
z[67] = z[67] / T(2) + -z[73];
z[67] = z[37] * z[67];
z[71] = z[1] + z[82];
z[71] = z[5] * z[71];
z[68] = z[57] / T(2) + -z[68] + z[71] + -z[93] + z[94];
z[71] = z[14] * z[63];
z[62] = -z[62] + z[68] / T(2) + -z[71];
z[62] = z[62] * z[96];
z[68] = -z[11] + -z[77];
z[68] = z[5] * z[68];
z[61] = -z[61] + z[68] + z[94];
z[61] = z[61] / T(2) + z[64] + z[69] + -z[71];
z[61] = z[28] * z[61];
z[64] = z[0] * z[95];
z[61] = z[61] + z[64];
z[61] = z[28] * z[61];
z[64] = z[33] * z[89];
z[68] = z[84] * z[96];
z[69] = -(z[34] * z[83]);
z[64] = z[64] + z[68] / T(2) + z[69] + -z[91];
z[64] = z[3] * z[64];
z[68] = z[40] * z[72];
z[69] = z[12] * z[58];
z[57] = z[57] + -z[69] + z[90];
z[49] = z[8] * z[49];
z[49] = T(-2) * z[49] + -z[53] + z[57] / T(2) + -z[88];
z[49] = z[49] * z[100];
z[53] = -(z[28] * z[58]);
z[53] = z[53] + z[81];
z[53] = z[53] * z[80];
z[55] = -(z[3] * z[55]);
z[55] = z[55] + z[65];
z[55] = z[29] * z[55];
z[49] = z[49] + z[53] + z[55];
z[49] = z[29] * z[49];
z[53] = prod_pow(z[28], 2);
z[53] = z[53] + -z[96];
z[53] = z[53] * z[63];
z[51] = -(z[37] * z[51]);
z[51] = z[51] + z[53];
z[51] = z[23] * z[51];
z[52] = (T(-61) * z[14]) / T(12) + -z[30] + -z[52];
z[52] = (T(19) * z[23]) / T(6) + (T(-8) * z[42]) / T(3) + z[52] / T(2);
z[52] = z[48] * z[52];
return z[49] + z[50] + z[51] + z[52] + z[54] + z[56] + z[59] + z[60] + z[61] + z[62] + z[64] + z[66] + z[67] + z[68] + z[70];
}



template IntegrandConstructorType<double> f_4_330_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_330_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_330_construct (const Kin<qd_real>&);
#endif

}