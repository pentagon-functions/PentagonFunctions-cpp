#pragma once
#include "include_helper.hpp"

namespace PentagonFunctions {

template <typename T> IntegrandConstructorType<T> f_4_288_construct (const Kin<T>& kin);


extern template IntegrandConstructorType<double> f_4_288_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
extern template IntegrandConstructorType<dd_real> f_4_288_construct (const Kin<dd_real>&);
extern template IntegrandConstructorType<qd_real> f_4_288_construct (const Kin<qd_real>&);
#endif

}