#include "f_4_51.h"

namespace PentagonFunctions {

template <typename T> T f_4_51_abbreviated (const std::array<T,38>&);

template <typename T> class SpDLog_f_4_51_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_51_W_7 (const Kin<T>& kin) {
        c[0] = T(2) * kin.v[0] * kin.v[4] + T(-6) * kin.v[1] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + ((T(9) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4]) + bc<T>[1] * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4]) + kin.v[3] * ((T(9) * kin.v[4]) / T(2) + T(-9) + T(2) * kin.v[0] + T(-6) * kin.v[1] + T(-3) * kin.v[2] + ((bc<T>[1] * T(3)) / T(2) + T(9) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[3] + bc<T>[1] * (T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]));
c[1] = (T(-9) + bc<T>[1] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[4] + bc<T>[1] * T(-3) * kin.v[4];
c[2] = T(2) * kin.v[0] * kin.v[4] + T(-6) * kin.v[1] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + ((T(9) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4]) + bc<T>[1] * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4]) + kin.v[3] * ((T(9) * kin.v[4]) / T(2) + T(-9) + T(2) * kin.v[0] + T(-6) * kin.v[1] + T(-3) * kin.v[2] + ((bc<T>[1] * T(3)) / T(2) + T(9) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[3] + bc<T>[1] * (T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]));
c[3] = (T(-9) + bc<T>[1] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[4] + bc<T>[1] * T(-3) * kin.v[4];
c[4] = T(2) * kin.v[0] * kin.v[4] + T(-6) * kin.v[1] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + ((T(9) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4]) + bc<T>[1] * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4]) + kin.v[3] * ((T(9) * kin.v[4]) / T(2) + T(-9) + T(2) * kin.v[0] + T(-6) * kin.v[1] + T(-3) * kin.v[2] + ((bc<T>[1] * T(3)) / T(2) + T(9) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[3] + bc<T>[1] * (T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]));
c[5] = (T(-9) + bc<T>[1] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[4] + bc<T>[1] * T(-3) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[5] * (t * c[0] + c[1]) + abb[14] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]);
        }

        return abb[10] * (abb[12] * (abb[9] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[14] * bc<T>[0] * int_to_imaginary<T>(-3)) + abb[2] * abb[12] * (abb[9] * T(3) + abb[14] * T(3)) + abb[3] * abb[12] * (abb[9] * T(3) + abb[14] * T(3)) + abb[1] * (abb[5] * abb[12] * T(-3) + abb[12] * (abb[9] * T(-3) + abb[14] * T(-3)) + abb[11] * (abb[5] * T(3) + abb[9] * T(3) + abb[14] * T(3))) + abb[11] * (abb[2] * (abb[9] * T(-3) + abb[14] * T(-3)) + abb[3] * (abb[9] * T(-3) + abb[14] * T(-3)) + abb[12] * (abb[9] * T(-3) + abb[14] * T(-3)) + abb[9] * bc<T>[0] * int_to_imaginary<T>(3) + abb[14] * bc<T>[0] * int_to_imaginary<T>(3) + abb[5] * (abb[2] * T(-3) + abb[3] * T(-3) + abb[12] * T(-3) + bc<T>[0] * int_to_imaginary<T>(3)) + abb[11] * (abb[5] * T(3) + abb[9] * T(3) + abb[14] * T(3))) + abb[5] * (abb[12] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[2] * abb[12] * T(3) + abb[3] * abb[12] * T(3) + abb[13] * T(9)) + abb[13] * (abb[9] * T(9) + abb[14] * T(9)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_51_construct (const Kin<T>& kin) {
    return [&kin, 
            dl11 = DLog_W_11<T>(kin),dl3 = DLog_W_3<T>(kin),dl7 = DLog_W_7<T>(kin),dl14 = DLog_W_14<T>(kin),dl18 = DLog_W_18<T>(kin),dl1 = DLog_W_1<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl26 = DLog_W_26<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),spdl7 = SpDLog_f_4_51_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,38> abbr = 
            {dl11(t), rlog(v_path[0]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_2(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl3(t), f_2_1_9(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl18(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl1(t), dl4(t), dl19(t), dl26(t) / kin_path.SqrtDelta, rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[19] / kin_path.W[19]), dl17(t), dl16(t), dl2(t), dl5(t), dl20(t), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_51_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_51_abbreviated(const std::array<T,38>& abb)
{
using TR = typename T::value_type;
T z[89];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[5];
z[3] = abb[6];
z[4] = abb[2];
z[5] = abb[3];
z[6] = abb[4];
z[7] = abb[7];
z[8] = abb[21];
z[9] = abb[23];
z[10] = abb[29];
z[11] = abb[30];
z[12] = abb[31];
z[13] = abb[9];
z[14] = abb[24];
z[15] = abb[25];
z[16] = abb[26];
z[17] = abb[27];
z[18] = abb[28];
z[19] = abb[34];
z[20] = abb[36];
z[21] = abb[11];
z[22] = abb[12];
z[23] = abb[18];
z[24] = abb[22];
z[25] = bc<TR>[0];
z[26] = abb[14];
z[27] = abb[37];
z[28] = abb[15];
z[29] = abb[32];
z[30] = abb[33];
z[31] = abb[35];
z[32] = abb[8];
z[33] = abb[13];
z[34] = abb[19];
z[35] = abb[20];
z[36] = abb[16];
z[37] = abb[17];
z[38] = bc<TR>[1];
z[39] = bc<TR>[3];
z[40] = bc<TR>[5];
z[41] = bc<TR>[2];
z[42] = bc<TR>[9];
z[43] = bc<TR>[4];
z[44] = bc<TR>[7];
z[45] = bc<TR>[8];
z[46] = z[1] + -z[5];
z[47] = T(3) * z[4];
z[48] = z[46] * z[47];
z[49] = T(3) * z[32];
z[50] = prod_pow(z[25], 2);
z[51] = z[50] / T(2);
z[48] = -z[48] + z[49] + -z[51];
z[52] = z[5] / T(2);
z[53] = -z[21] + z[52];
z[54] = T(3) * z[5];
z[55] = z[53] * z[54];
z[56] = T(3) * z[34];
z[57] = T(3) * z[33];
z[55] = z[55] + z[56] + z[57];
z[58] = z[21] * z[25];
z[59] = z[25] * z[35];
z[60] = z[58] + -z[59];
z[61] = z[25] * z[37];
z[62] = z[60] + -z[61];
z[63] = int_to_imaginary<T>(3);
z[62] = z[62] * z[63];
z[64] = z[1] / T(2);
z[65] = int_to_imaginary<T>(1) * z[25];
z[66] = -z[5] + z[64] + z[65];
z[67] = z[21] + z[66];
z[68] = T(3) * z[1];
z[67] = z[67] * z[68];
z[62] = z[48] + z[55] + z[62] + z[67];
z[67] = z[19] * z[62];
z[69] = z[22] * z[25];
z[70] = -z[59] + z[69];
z[71] = z[63] * z[70];
z[72] = -z[21] + z[22];
z[47] = z[47] * z[72];
z[73] = z[21] * z[22];
z[47] = -z[47] + T(3) * z[73];
z[52] = -z[22] + z[52];
z[74] = z[52] * z[54];
z[75] = z[68] * z[72];
z[56] = z[47] + z[56] + z[71] + z[74] + z[75];
z[71] = z[56] + -z[57];
z[74] = prod_pow(z[21], 2);
z[76] = T(3) * z[74];
z[77] = z[50] + z[76];
z[77] = z[77] / T(2);
z[78] = T(3) * z[6];
z[49] = z[49] + -z[78];
z[79] = z[49] + -z[71] + z[77];
z[80] = z[27] * z[79];
z[81] = z[64] + -z[65];
z[82] = -z[21] + z[81];
z[82] = z[68] * z[82];
z[83] = -z[50] + z[76];
z[83] = z[83] / T(2);
z[84] = z[37] * z[65];
z[82] = z[78] + z[82] + z[83] + T(3) * z[84];
z[84] = z[20] * z[82];
z[85] = z[60] * z[63];
z[55] = z[55] + z[83] + z[85];
z[83] = z[31] * z[55];
z[81] = -z[22] + z[81];
z[81] = z[1] * z[81];
z[85] = z[35] * z[65];
z[52] = z[5] * z[52];
z[52] = z[34] + z[52];
z[81] = z[32] + -z[52] + z[81] + z[85];
z[81] = T(3) * z[81];
z[85] = z[14] * z[81];
z[86] = z[19] + -z[20];
z[87] = T(3) * z[36];
z[86] = z[86] * z[87];
z[67] = z[67] + z[80] + z[83] + z[84] + z[85] + -z[86];
z[80] = z[15] + z[16] + -z[17] + -z[18];
z[80] = -z[80] / T(2);
z[67] = z[67] * z[80];
z[66] = z[22] + z[66];
z[66] = z[1] * z[66];
z[80] = z[46] + z[72];
z[83] = z[4] * z[80];
z[84] = -z[36] + z[73];
z[66] = -z[6] + z[34] + -z[66] + z[83] + -z[84];
z[83] = z[22] / T(2);
z[85] = -z[5] / T(4) + z[21] + -z[83];
z[85] = z[54] * z[85];
z[86] = -z[61] + z[69];
z[59] = z[59] + z[86];
z[59] = -z[58] + z[59] / T(2);
z[59] = z[59] * z[63];
z[59] = (T(-9) * z[33]) / T(2) + z[50] / T(4) + z[59] + (T(-3) * z[66]) / T(2) + -z[76] + z[85];
z[59] = z[13] * z[59];
z[47] = z[47] + -z[51] + -z[76];
z[54] = -(z[54] * z[72]);
z[66] = -z[58] + z[69];
z[66] = z[63] * z[66];
z[54] = T(-6) * z[33] + z[47] + z[54] + z[66] + z[75];
z[54] = z[26] * z[54];
z[66] = (T(3) * z[74]) / T(2);
z[69] = -z[50] + -z[66] + z[71];
z[69] = z[2] * z[69];
z[70] = -z[61] + -z[70];
z[70] = int_to_imaginary<T>(1) * z[70];
z[52] = z[6] + z[33] + z[51] + -z[52] + z[70] + -z[73];
z[70] = z[1] / T(4) + -z[53] + z[65] / T(2) + -z[83];
z[70] = z[1] * z[70];
z[71] = z[46] + -z[72];
z[75] = z[4] / T(2);
z[76] = -(z[71] * z[75]);
z[83] = z[36] / T(2);
z[52] = z[52] / T(2) + z[70] + z[76] + -z[83];
z[52] = z[3] * z[52];
z[70] = prod_pow(z[38], 2);
z[76] = z[25] * z[70];
z[85] = prod_pow(z[25], 3);
z[76] = T(36) * z[40] + (T(-3) * z[76]) / T(4) + (T(-11) * z[85]) / T(36);
z[88] = int_to_imaginary<T>(1) / T(5);
z[76] = z[76] * z[88];
z[88] = z[38] * z[63];
z[88] = -z[25] + z[88];
z[88] = z[39] * z[88];
z[76] = z[76] + z[88];
z[52] = T(3) * z[52] + z[54] + z[59] + z[69] + z[76];
z[52] = z[12] * z[52];
z[54] = z[62] + -z[87];
z[54] = z[9] * z[54];
z[59] = z[82] + z[87];
z[59] = z[11] * z[59];
z[55] = z[30] * z[55];
z[62] = z[46] * z[68];
z[48] = z[48] + z[62] + z[78];
z[48] = z[7] * z[48];
z[48] = z[48] + z[55];
z[55] = z[10] * z[81];
z[54] = -z[48] + z[54] + -z[55] + z[59];
z[49] = T(-9) * z[33] + z[49] + z[56] + -z[77];
z[49] = z[23] * z[49];
z[55] = int_to_imaginary<T>(1) * z[86];
z[56] = z[4] * z[72];
z[55] = z[33] + -z[55] + z[56] + z[74] + -z[84];
z[56] = T(3) * z[29];
z[56] = z[55] * z[56];
z[49] = z[49] + -z[54] + -z[56];
z[49] = z[13] * z[49];
z[59] = z[41] * z[65];
z[59] = z[51] + z[59];
z[62] = T(3) * z[41];
z[59] = z[59] * z[62];
z[50] = z[38] * z[50];
z[62] = int_to_imaginary<T>(1) * z[85];
z[62] = z[62] / T(2);
z[65] = -z[50] + z[59] + -z[62];
z[65] = z[19] * z[65];
z[49] = z[49] + z[65];
z[65] = -(z[23] * z[79]);
z[54] = -z[54] + z[65];
z[55] = z[29] * z[55];
z[63] = z[63] * z[86];
z[47] = z[47] + z[63] + -z[87];
z[47] = z[28] * z[47];
z[54] = z[47] + z[54] / T(2) + (T(3) * z[55]) / T(2);
z[54] = z[3] * z[54];
z[55] = -z[60] + z[86];
z[55] = int_to_imaginary<T>(1) * z[55];
z[53] = z[5] * z[53];
z[53] = z[34] + z[53] + -z[55] + z[66];
z[55] = -(z[4] * z[71]);
z[60] = z[6] + z[32];
z[63] = z[1] * z[46];
z[55] = z[53] + z[55] + z[60] + z[63] + -z[84];
z[55] = z[3] * z[55];
z[53] = z[53] + -z[60] + -z[73];
z[60] = -(z[46] * z[64]);
z[64] = z[75] * z[80];
z[53] = z[33] + z[53] / T(2) + z[60] + z[64] + z[83];
z[53] = z[13] * z[53];
z[53] = z[53] + z[55] / T(2);
z[55] = z[51] + -z[57];
z[57] = -z[2] + -z[26];
z[57] = z[55] * z[57];
z[53] = T(3) * z[53] + z[57];
z[53] = z[8] * z[53];
z[57] = z[23] * z[55];
z[47] = -z[47] + -z[56] + z[57];
z[47] = z[26] * z[47];
z[55] = z[9] * z[55];
z[48] = -z[48] + z[55];
z[48] = z[2] * z[48];
z[55] = z[58] + -z[61];
z[55] = int_to_imaginary<T>(1) * z[55];
z[56] = z[46] * z[72];
z[57] = z[6] + -z[32];
z[55] = -z[33] + z[36] + -z[55] + z[56] + z[57];
z[56] = z[3] + z[13];
z[55] = z[55] * z[56];
z[56] = -z[33] + -z[57];
z[56] = z[2] * z[56];
z[55] = z[55] / T(2) + z[56];
z[55] = z[24] * z[55];
z[46] = z[4] * z[46];
z[46] = T(2) * z[6] + -z[46] + z[63];
z[56] = z[2] + -z[3];
z[46] = z[0] * z[46] * z[56];
z[46] = z[46] + z[55];
z[55] = prod_pow(z[41], 2);
z[55] = z[51] + T(2) * z[55];
z[55] = z[41] * z[55];
z[56] = z[51] + z[70];
z[56] = z[38] * z[56];
z[56] = z[56] + -z[62];
z[57] = z[38] * z[43];
z[57] = T(3) * z[57];
z[56] = -z[55] + z[56] / T(2) + z[57];
z[56] = z[31] * z[56];
z[51] = z[51] + -z[70];
z[51] = z[38] * z[51];
z[51] = z[51] + -z[59];
z[51] = z[51] / T(2) + -z[57];
z[51] = z[20] * z[51];
z[50] = -z[50] + z[62];
z[50] = z[50] / T(2) + z[55];
z[50] = z[27] * z[50];
z[55] = z[11] + -z[29] + -z[30];
z[55] = z[55] * z[76];
z[57] = -z[27] + z[31];
z[57] = z[45] * z[57];
z[58] = -z[20] + z[31];
z[58] = z[44] * z[58];
z[59] = (T(-87) * z[19]) / T(2) + (T(43) * z[20]) / T(2) + T(47) * z[27] + T(-67) * z[31];
z[59] = z[42] * z[59];
return T(3) * z[46] + z[47] + z[48] + z[49] / T(2) + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + (T(3) * z[57]) / T(2) + T(6) * z[58] + z[59] / T(4) + z[67];
}



template IntegrandConstructorType<double> f_4_51_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_51_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_51_construct (const Kin<qd_real>&);
#endif

}