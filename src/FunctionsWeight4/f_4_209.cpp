#include "f_4_209.h"

namespace PentagonFunctions {

template <typename T> T f_4_209_abbreviated (const std::array<T,20>&);



template <typename T> IntegrandConstructorType<T> f_4_209_construct (const Kin<T>& kin) {
    return [&kin, 
            dl15 = DLog_W_15<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl13 = DLog_W_13<T>(kin),dl1 = DLog_W_1<T>(kin),dl8 = DLog_W_8<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,20> abbr = 
            {dl15(t), rlog(kin.W[0] / kin_path.W[0]), rlog(v_path[2]), rlog(-v_path[4]), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), rlog(kin.W[19] / kin_path.W[19]), dl5(t), rlog(kin.W[12] / kin_path.W[12]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[7] / kin_path.W[7]), rlog(kin.W[4] / kin_path.W[4]), dl20(t), dl3(t), dl13(t), dl1(t), dl8(t)}
;

        auto result = f_4_209_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_209_abbreviated(const std::array<T,20>& abb)
{
using TR = typename T::value_type;
T z[61];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[15];
z[10] = abb[16];
z[11] = abb[18];
z[12] = abb[19];
z[13] = abb[11];
z[14] = abb[9];
z[15] = abb[10];
z[16] = abb[12];
z[17] = abb[13];
z[18] = abb[14];
z[19] = abb[8];
z[20] = bc<TR>[1];
z[21] = bc<TR>[2];
z[22] = bc<TR>[4];
z[23] = bc<TR>[9];
z[24] = abb[17];
z[25] = T(3) * z[18];
z[26] = (T(3) * z[16]) / T(2);
z[27] = z[25] + z[26];
z[28] = T(4) * z[19];
z[29] = -z[1] + z[28];
z[30] = z[17] / T(2);
z[31] = z[7] + z[27] + -z[29] + z[30];
z[31] = z[10] * z[31];
z[32] = T(3) * z[1];
z[33] = z[7] + z[18];
z[34] = T(2) * z[33];
z[35] = z[16] + z[34];
z[36] = T(2) * z[17] + z[28] + z[32] + T(-3) * z[35];
z[36] = z[8] * z[36];
z[37] = T(2) * z[1];
z[38] = z[17] + z[37];
z[39] = T(3) * z[16] + -z[28] + T(4) * z[33] + -z[38];
z[40] = z[11] * z[39];
z[41] = (T(3) * z[17]) / T(2);
z[42] = T(-3) * z[7] + -z[18] + z[26] + z[41];
z[42] = z[12] * z[42];
z[38] = T(2) * z[19] + z[33] + -z[38];
z[43] = T(2) * z[9];
z[38] = z[38] * z[43];
z[31] = z[31] + -z[36] + z[38] + -z[40] + z[42];
z[36] = z[15] * z[31];
z[38] = -z[16] + z[17];
z[42] = -z[34] + z[37] + z[38];
z[44] = T(2) * z[8];
z[43] = z[43] + z[44];
z[43] = z[42] * z[43];
z[45] = -z[29] + z[35];
z[45] = z[10] * z[45];
z[46] = z[1] + z[17];
z[34] = z[34] + -z[46];
z[34] = z[12] * z[34];
z[34] = z[34] + z[40] + z[43] + -z[45];
z[43] = z[4] + z[13];
z[43] = z[34] * z[43];
z[45] = z[9] * z[42];
z[42] = z[8] * z[42];
z[47] = z[42] + z[45];
z[30] = -z[7] + z[30];
z[27] = z[27] + -z[30] + -z[37];
z[27] = z[12] * z[27];
z[37] = z[16] / T(2);
z[48] = z[33] + z[37];
z[49] = z[41] + -z[48];
z[49] = z[10] * z[49];
z[50] = z[1] + -z[7];
z[51] = -(z[0] * z[50]);
z[52] = T(2) * z[51];
z[49] = z[27] + z[47] + z[49] + z[52];
z[49] = z[2] * z[49];
z[40] = -z[40] + z[52];
z[53] = z[6] * z[40];
z[54] = z[6] * z[39];
z[55] = -z[20] + T(2) * z[21];
z[55] = z[20] * z[55];
z[56] = prod_pow(z[21], 2);
z[57] = T(8) * z[22] + T(-4) * z[55] + -z[56] / T(2);
z[54] = z[54] + z[57];
z[54] = z[9] * z[54];
z[58] = T(2) * z[22] + -z[55];
z[59] = z[56] / T(4) + z[58];
z[46] = T(2) * z[7] + -z[28] + z[46];
z[60] = z[6] * z[46];
z[59] = T(3) * z[59] + z[60];
z[59] = z[10] * z[59];
z[28] = z[17] + -z[28] + z[32];
z[32] = -(z[6] * z[28]);
z[32] = z[32] + z[57];
z[32] = z[8] * z[32];
z[55] = T(-4) * z[22] + T(2) * z[55] + -z[56];
z[55] = z[11] * z[55];
z[56] = (T(3) * z[56]) / T(4) + -z[58];
z[56] = z[12] * z[56];
z[57] = prod_pow(z[3], 2);
z[60] = (T(-7) * z[9]) / T(2) + z[11];
z[60] = (T(-7) * z[8]) / T(6) + (T(-3) * z[10]) / T(4) + (T(-5) * z[12]) / T(12) + z[60] / T(3);
z[60] = z[57] * z[60];
z[58] = z[57] + T(-2) * z[58];
z[58] = z[24] * z[58];
z[32] = z[32] + z[36] + z[43] + z[49] + z[53] + z[54] + z[55] + z[56] + T(2) * z[58] + z[59] + z[60];
z[32] = int_to_imaginary<T>(1) * z[3] * z[32];
z[36] = T(2) * z[20];
z[43] = (T(5) * z[1]) / T(6) + (T(-10) * z[19]) / T(3) + (T(9) * z[21]) / T(8) + z[35] / T(3) + -z[36] + z[41];
z[43] = z[10] * z[43];
z[49] = (T(8) * z[20]) / T(3);
z[50] = (T(4) * z[18]) / T(3) + (T(5) * z[21]) / T(4) + (T(-2) * z[38]) / T(3) + -z[49] + (T(-3) * z[50]) / T(2);
z[50] = z[8] * z[50];
z[33] = -z[19] + z[33];
z[33] = z[1] / T(3) + z[17] / T(6) + -z[21] / T(2) + (T(-2) * z[33]) / T(3) + -z[37];
z[33] = (T(4) * z[20]) / T(3) + T(5) * z[33];
z[33] = z[11] * z[33];
z[25] = (T(7) * z[7]) / T(3) + (T(11) * z[16]) / T(6) + (T(5) * z[21]) / T(2) + z[25] + -z[41];
z[25] = (T(-5) * z[1]) / T(3) + z[25] / T(2) + -z[49];
z[25] = z[9] * z[25];
z[49] = (T(5) * z[16]) / T(2) + (T(7) * z[18]) / T(3) + z[21] / T(4) + z[30];
z[49] = -z[1] + (T(2) * z[20]) / T(3) + z[49] / T(2);
z[49] = z[12] * z[49];
z[25] = z[25] + z[33] + z[43] + z[49] + z[50] + (T(5) * z[51]) / T(3);
z[25] = z[25] * z[57];
z[31] = -(z[14] * z[31]);
z[29] = z[29] + -z[41] + -z[48];
z[29] = z[10] * z[29];
z[30] = z[18] + z[30];
z[26] = z[1] + -z[26] + -z[30];
z[26] = z[12] * z[26];
z[26] = z[26] + z[29] + -z[40] + z[47];
z[26] = prod_pow(z[2], 2) * z[26];
z[29] = -(z[2] * z[34]);
z[33] = -z[7] + z[16];
z[33] = z[33] * z[44];
z[38] = T(2) * z[18] + -z[38];
z[38] = z[10] * z[38];
z[33] = z[33] + z[38] + z[45] + z[52];
z[33] = z[4] * z[33];
z[29] = z[29] + z[33];
z[29] = z[4] * z[29];
z[33] = -z[2] + z[4];
z[33] = z[33] * z[34];
z[30] = -z[30] + -z[37];
z[30] = z[10] * z[30];
z[34] = T(3) * z[17] + -z[35];
z[34] = z[9] * z[34];
z[27] = z[27] + z[30] + z[34] + z[42];
z[27] = z[13] * z[27];
z[27] = z[27] + z[33];
z[27] = z[13] * z[27];
z[30] = -(z[9] * z[39]);
z[33] = -(z[10] * z[46]);
z[28] = z[8] * z[28];
z[28] = z[28] + z[30] + z[33] + -z[40];
z[28] = z[5] * z[28];
z[30] = z[36] * z[57];
z[30] = T(-7) * z[23] + z[30];
z[30] = z[24] * z[30];
z[33] = (T(-31) * z[8]) / T(6) + (T(-45) * z[9]) / T(2) + (T(157) * z[10]) / T(12) + (T(91) * z[11]) / T(3) + (T(245) * z[12]) / T(12);
z[33] = z[23] * z[33];
return z[25] + z[26] + z[27] + z[28] + z[29] + (T(4) * z[30]) / T(3) + z[31] + z[32] + z[33] / T(4);
}



template IntegrandConstructorType<double> f_4_209_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_209_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_209_construct (const Kin<qd_real>&);
#endif

}