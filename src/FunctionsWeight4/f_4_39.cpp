#include "f_4_39.h"

namespace PentagonFunctions {

template <typename T> T f_4_39_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_39_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_39_W_7 (const Kin<T>& kin) {
        c[0] = -(kin.v[1] * kin.v[4]) + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + -kin.v[1] + T(-1)) * kin.v[3];
c[1] = kin.v[3] + kin.v[4];
c[2] = -(kin.v[1] * kin.v[4]) + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + -kin.v[1] + T(-1)) * kin.v[3];
c[3] = kin.v[3] + kin.v[4];
c[4] = -(kin.v[1] * kin.v[4]) + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + -kin.v[1] + T(-1)) * kin.v[3];
c[5] = kin.v[3] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[4] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[6] * (t * c[4] + c[5]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * (abb[5] + abb[6]) + abb[4] * (prod_pow(abb[2], 2) + -abb[3]) + abb[3] * (-abb[5] + -abb[6]) + prod_pow(abb[1], 2) * (-abb[4] + -abb[5] + -abb[6]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_39_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl11 = DLog_W_11<T>(kin),dl25 = DLog_W_25<T>(kin),dl14 = DLog_W_14<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl6 = DLog_W_6<T>(kin),dl19 = DLog_W_19<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl1 = DLog_W_1<T>(kin),dl2 = DLog_W_2<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin),dl31 = DLog_W_31<T>(kin),dl29 = DLog_W_29<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),spdl7 = SpDLog_f_4_39_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl11(t), rlog(v_path[0]), rlog(v_path[3]), f_1_3_5(kin) - f_1_3_5(kin_path), dl25(t), f_1_3_1(kin) - f_1_3_1(kin_path), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl5(t), dl16(t), f_2_1_2(kin_path), dl20(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl6(t), f_2_1_9(kin_path), dl19(t), dl18(t), dl4(t), dl1(t), dl2(t), dl3(t), dl17(t), dl31(t), dl29(t) / kin_path.SqrtDelta, rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_39_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_39_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[142];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[17];
z[3] = abb[18];
z[4] = abb[20];
z[5] = abb[25];
z[6] = abb[26];
z[7] = abb[27];
z[8] = abb[28];
z[9] = abb[29];
z[10] = abb[5];
z[11] = abb[14];
z[12] = abb[6];
z[13] = abb[10];
z[14] = abb[12];
z[15] = abb[33];
z[16] = abb[34];
z[17] = abb[35];
z[18] = abb[36];
z[19] = abb[37];
z[20] = abb[38];
z[21] = abb[39];
z[22] = abb[40];
z[23] = abb[41];
z[24] = abb[42];
z[25] = abb[44];
z[26] = abb[2];
z[27] = abb[8];
z[28] = abb[23];
z[29] = abb[9];
z[30] = abb[13];
z[31] = bc<TR>[0];
z[32] = abb[45];
z[33] = abb[11];
z[34] = abb[31];
z[35] = abb[43];
z[36] = abb[3];
z[37] = abb[7];
z[38] = abb[30];
z[39] = abb[15];
z[40] = abb[16];
z[41] = abb[19];
z[42] = abb[21];
z[43] = abb[22];
z[44] = abb[24];
z[45] = bc<TR>[1];
z[46] = bc<TR>[3];
z[47] = bc<TR>[5];
z[48] = abb[32];
z[49] = bc<TR>[2];
z[50] = bc<TR>[4];
z[51] = bc<TR>[7];
z[52] = bc<TR>[9];
z[53] = bc<TR>[8];
z[54] = z[27] / T(2);
z[55] = -z[0] + z[54];
z[56] = T(2) * z[29];
z[57] = T(2) * z[30];
z[58] = z[55] + -z[56] + -z[57];
z[58] = z[9] * z[58];
z[59] = T(2) * z[27];
z[60] = z[0] / T(2);
z[61] = z[59] + -z[60];
z[62] = z[26] / T(2);
z[63] = z[43] / T(2) + -z[56] + -z[61] + -z[62];
z[63] = z[8] * z[63];
z[64] = z[3] * z[27];
z[65] = z[9] * z[62];
z[66] = (T(3) * z[64]) / T(2) + -z[65];
z[67] = (T(3) * z[43]) / T(2);
z[68] = -z[4] + z[9];
z[69] = z[67] * z[68];
z[70] = z[62] + z[67];
z[71] = -z[0] + z[29];
z[72] = -z[27] + z[71];
z[72] = z[70] + T(2) * z[72];
z[72] = z[6] * z[72];
z[73] = z[0] * z[4];
z[74] = z[2] * z[62];
z[75] = -z[0] + z[56];
z[76] = z[11] * z[75];
z[77] = z[33] * z[57];
z[58] = z[58] + z[63] + z[66] + z[69] + z[72] + (T(3) * z[73]) / T(2) + z[74] + z[76] + z[77];
z[58] = z[10] * z[58];
z[63] = -z[26] + z[43];
z[69] = z[6] / T(2);
z[72] = z[63] * z[69];
z[78] = z[4] * z[60];
z[79] = z[43] * z[68];
z[72] = z[72] + z[77] + -z[78] + -z[79] / T(2);
z[78] = z[57] + z[60];
z[80] = -z[56] + -z[70] + z[78];
z[80] = z[8] * z[80];
z[81] = -z[54] + z[56];
z[82] = -(z[9] * z[81]);
z[83] = -z[27] + z[29];
z[84] = z[37] * z[83];
z[84] = T(2) * z[84];
z[85] = (T(3) * z[2]) / T(2);
z[86] = -(z[26] * z[85]);
z[66] = z[66] + -z[72] + z[80] + z[82] + z[84] + z[86];
z[66] = z[1] * z[66];
z[80] = -z[3] + z[8];
z[82] = z[2] + -z[9] + z[80];
z[86] = z[14] / T(2);
z[82] = z[82] * z[86];
z[87] = z[2] / T(2);
z[80] = z[9] + z[80];
z[88] = -z[11] + (T(-3) * z[80]) / T(2) + z[87];
z[88] = z[10] * z[88];
z[89] = T(3) * z[1];
z[90] = z[10] + -z[14];
z[91] = z[89] + -z[90];
z[92] = z[5] / T(2);
z[91] = z[91] * z[92];
z[92] = z[22] + z[23];
z[93] = -z[17] + T(3) * z[92];
z[94] = T(-2) * z[18] + z[20] + z[93] / T(2);
z[95] = z[16] / T(2);
z[96] = z[94] + -z[95];
z[97] = (T(3) * z[19]) / T(2);
z[98] = z[96] + z[97];
z[99] = -z[25] + z[32];
z[98] = z[98] * z[99];
z[100] = z[2] + z[80];
z[101] = (T(3) * z[1]) / T(2);
z[100] = z[100] * z[101];
z[82] = -z[82] + z[88] + z[91] + -z[98] + -z[100];
z[88] = -(z[40] * z[82]);
z[75] = z[59] + z[75];
z[75] = z[8] * z[75];
z[91] = -z[27] + z[56];
z[91] = z[9] * z[91];
z[80] = z[11] + z[80];
z[98] = -(z[40] * z[80]);
z[64] = -z[64] + z[75] + -z[76] + -z[84] + z[91] + z[98];
z[64] = z[13] * z[64];
z[70] = z[57] + z[61] + -z[70];
z[70] = z[8] * z[70];
z[75] = -z[3] + z[9];
z[76] = -(z[54] * z[75]);
z[65] = z[65] + z[70] + -z[72] + -z[74] + z[76];
z[65] = z[14] * z[65];
z[70] = z[25] * z[27];
z[72] = -z[27] + z[43];
z[74] = z[35] * z[72];
z[76] = -z[0] + z[43];
z[84] = z[15] * z[76];
z[63] = z[24] * z[63];
z[91] = -z[0] + z[72];
z[98] = z[32] * z[91];
z[63] = z[63] + -z[70] + z[74] + -z[84] + -z[98];
z[70] = -(z[63] * z[97]);
z[74] = T(2) * z[6];
z[76] = z[74] * z[76];
z[84] = -z[0] + -z[57];
z[84] = z[9] * z[84];
z[73] = z[73] + z[76] + z[77] + z[79] + z[84];
z[73] = z[12] * z[73];
z[76] = z[40] * z[99];
z[63] = -z[63] + z[76];
z[76] = (T(3) * z[21]) / T(2);
z[63] = z[63] * z[76];
z[77] = T(2) * z[26];
z[79] = -z[60] + z[77];
z[79] = z[1] * z[79];
z[61] = -z[57] + z[61];
z[84] = -(z[10] * z[61]);
z[89] = z[89] + z[90];
z[89] = z[89] / T(2);
z[98] = -(z[40] * z[89]);
z[100] = prod_pow(z[45], 2);
z[101] = (T(3) * z[100]) / T(20);
z[102] = -(z[14] * z[60]);
z[79] = z[79] + z[84] + z[98] + -z[101] + z[102];
z[79] = z[7] * z[79];
z[57] = z[57] + -z[60];
z[77] = z[57] + -z[67] + z[77] + z[81];
z[77] = z[1] * z[77];
z[67] = -z[54] + -z[67] + z[78];
z[67] = z[14] * z[67];
z[78] = z[10] / T(2);
z[84] = z[78] * z[91];
z[67] = z[67] + z[77] + z[84] + -z[101];
z[67] = z[5] * z[67];
z[77] = z[27] * z[96];
z[84] = T(3) * z[50] + z[77];
z[91] = z[43] * z[96];
z[98] = z[84] + -z[91];
z[98] = z[35] * z[98];
z[101] = z[0] * z[95];
z[102] = z[0] * z[94];
z[101] = z[101] + -z[102];
z[103] = z[91] + z[101];
z[104] = z[15] * z[103];
z[105] = z[26] * z[96];
z[91] = -z[91] + z[105];
z[91] = z[24] * z[91];
z[103] = -z[77] + z[103];
z[103] = z[32] * z[103];
z[105] = z[25] + z[35];
z[106] = z[6] + z[8];
z[107] = -z[9] / T(5) + T(5) * z[105] + (T(-3) * z[106]) / T(10);
z[107] = -z[32] + z[38] / T(20) + z[48] / T(5) + z[107] / T(2);
z[107] = z[100] * z[107];
z[84] = z[25] * z[84];
z[108] = T(2) * z[32] + -z[105];
z[108] = z[45] * z[108];
z[109] = z[49] * z[99];
z[108] = z[108] + z[109] / T(2);
z[108] = z[49] * z[108];
z[109] = z[1] + z[14];
z[110] = -z[10] + T(3) * z[109];
z[72] = z[72] * z[110];
z[72] = z[72] + z[100] / T(10);
z[111] = z[34] / T(2);
z[72] = z[72] * z[111];
z[112] = z[10] + z[109];
z[112] = z[28] * z[112];
z[113] = z[27] + -z[30];
z[114] = z[112] * z[113];
z[58] = z[58] + z[63] + z[64] + z[65] + z[66] + z[67] + z[70] + z[72] + z[73] + z[79] + z[84] + z[88] + z[91] + z[98] + z[103] + z[104] + z[107] + z[108] + T(2) * z[114];
z[58] = int_to_imaginary<T>(1) * z[58];
z[63] = z[5] + z[7];
z[64] = z[63] + z[106];
z[65] = -z[15] + z[32];
z[66] = z[34] + z[38];
z[67] = z[24] / T(4);
z[65] = (T(-11) * z[9]) / T(90) + (T(11) * z[48]) / T(45) + (T(-11) * z[64]) / T(60) + z[65] / T(4) + (T(11) * z[66]) / T(180) + z[67] + -z[105];
z[65] = int_to_imaginary<T>(1) * z[31] * z[65];
z[70] = z[17] / T(3) + -z[19] + -z[21] + -z[92];
z[70] = z[16] / T(12) + z[18] / T(3) + -z[20] / T(6) + z[70] / T(4);
z[72] = -z[15] + z[24];
z[73] = -z[25] + -z[32] + z[72];
z[70] = z[70] * z[73];
z[73] = z[4] + z[6];
z[73] = z[33] + z[73] / T(12);
z[79] = z[3] / T(4);
z[84] = -z[8] / T(3) + -z[9] / T(4) + -z[73] + z[79];
z[84] = z[1] * z[84];
z[73] = (T(7) * z[8]) / T(6) + -z[73] + -z[75] / T(12);
z[73] = z[14] * z[73];
z[88] = (T(-17) * z[25]) / T(2) + T(-7) * z[35];
z[72] = (T(11) * z[32]) / T(4) + -z[72] + z[88] / T(6);
z[72] = z[49] * z[72];
z[88] = T(3) * z[8];
z[75] = (T(-7) * z[11]) / T(3) + z[75] / T(3) + z[88];
z[75] = z[13] * z[75];
z[79] = -z[4] / T(4) + (T(7) * z[6]) / T(12) + z[7] / T(6) + (T(-4) * z[8]) / T(3) + (T(-13) * z[9]) / T(12) + (T(7) * z[11]) / T(6) + z[33] + z[79];
z[79] = z[10] * z[79];
z[91] = -z[4] / T(6) + z[6] / T(3) + (T(-5) * z[9]) / T(6) + z[33];
z[91] = z[12] * z[91];
z[92] = -z[10] + z[109] / T(3);
z[92] = z[38] * z[92];
z[67] = (T(17) * z[25]) / T(4) + T(4) * z[35] + -z[67];
z[67] = (T(-5) * z[32]) / T(4) + z[67] / T(3);
z[67] = z[45] * z[67];
z[90] = T(13) * z[1] + z[90];
z[90] = z[5] * z[90];
z[65] = z[65] / T(3) + z[67] + z[70] + z[72] + z[73] + z[75] / T(2) + z[79] + z[84] + z[90] / T(12) + z[91] + z[92] / T(4);
z[65] = z[31] * z[65];
z[64] = (T(-2) * z[9]) / T(3) + (T(4) * z[48]) / T(3) + -z[64] + z[66] / T(3);
z[64] = z[46] * z[64];
z[58] = z[58] + z[64] + z[65];
z[58] = z[31] * z[58];
z[64] = -z[27] + z[57];
z[64] = z[27] * z[64];
z[65] = (T(3) * z[36]) / T(2);
z[66] = (T(5) * z[44]) / T(2);
z[67] = z[65] + z[66];
z[70] = T(3) * z[41];
z[72] = prod_pow(z[0], 2);
z[73] = z[72] / T(2);
z[75] = z[70] + z[73];
z[57] = -z[29] + -z[57] + z[59];
z[57] = z[29] * z[57];
z[59] = z[71] + -z[113];
z[79] = z[59] / T(2);
z[84] = -z[26] + -z[79];
z[84] = z[26] * z[84];
z[90] = T(2) * z[0];
z[91] = (T(-3) * z[30]) / T(4) + z[90];
z[91] = z[30] * z[91];
z[57] = z[57] + z[64] + -z[67] + -z[75] / T(2) + z[84] + z[91];
z[57] = z[6] * z[57];
z[84] = z[0] + z[113];
z[84] = -z[29] + z[84] / T(2);
z[84] = z[29] * z[84];
z[91] = z[30] / T(2);
z[92] = z[0] + z[91];
z[98] = z[27] / T(4) + -z[92];
z[98] = z[27] * z[98];
z[84] = z[44] + -z[84] + z[98];
z[70] = z[70] + z[72];
z[98] = z[36] / T(2);
z[103] = z[0] + z[30] / T(4);
z[103] = z[30] * z[103];
z[70] = z[70] / T(2) + z[84] + -z[98] + z[103];
z[70] = z[9] * z[70];
z[103] = -z[0] + z[91];
z[104] = z[30] * z[103];
z[75] = z[75] + -z[104];
z[106] = -z[0] + z[113];
z[107] = z[106] / T(2);
z[108] = z[29] + z[107];
z[108] = z[29] * z[108];
z[75] = z[75] / T(2) + z[108];
z[108] = z[90] + -z[91];
z[114] = z[54] + -z[108];
z[114] = z[27] * z[114];
z[62] = z[62] * z[71];
z[115] = z[44] / T(2);
z[62] = z[62] + z[75] + z[114] + -z[115];
z[62] = z[8] * z[62];
z[114] = z[26] * z[71];
z[116] = z[36] + z[72];
z[117] = z[0] * z[29];
z[114] = z[114] + z[116] + -z[117];
z[87] = z[87] * z[114];
z[118] = prod_pow(z[29], 2);
z[119] = -z[72] + z[118];
z[119] = z[11] * z[119];
z[55] = z[27] * z[55];
z[120] = z[41] + z[73];
z[55] = z[55] + z[120];
z[121] = z[3] * z[55];
z[122] = (T(3) * z[121]) / T(2);
z[123] = z[36] + z[73];
z[124] = z[104] + z[123];
z[125] = z[4] * z[124];
z[79] = z[9] * z[79];
z[126] = z[9] + -z[33];
z[127] = -(z[26] * z[126]);
z[127] = z[79] + z[127];
z[127] = z[26] * z[127];
z[128] = T(3) * z[6];
z[129] = -z[8] + T(-3) * z[68] + -z[128];
z[130] = z[42] / T(2);
z[129] = z[129] * z[130];
z[131] = prod_pow(z[30], 2);
z[132] = z[33] * z[131];
z[57] = z[57] + z[62] + z[70] + -z[87] + -z[119] + -z[122] + (T(3) * z[125]) / T(2) + z[127] + z[129] + -z[132];
z[57] = z[10] * z[57];
z[62] = -z[0] + z[30];
z[70] = z[62] * z[95];
z[70] = z[70] + z[102];
z[127] = z[30] * z[94];
z[77] = -z[77] + z[127];
z[129] = -(z[29] * z[96]);
z[129] = z[70] + -z[77] + z[129];
z[129] = z[26] * z[129];
z[93] = -z[18] + z[20] / T(2) + z[93] / T(4);
z[133] = z[72] * z[93];
z[134] = T(2) * z[51] + -z[133];
z[135] = z[44] * z[96];
z[136] = z[36] * z[94];
z[137] = z[135] + z[136];
z[83] = -(z[83] * z[101]);
z[96] = z[42] * z[96];
z[138] = z[93] * z[131];
z[139] = z[131] / T(2);
z[140] = z[41] + -z[123] + z[139];
z[141] = -(z[95] * z[140]);
z[94] = z[41] * z[94];
z[83] = (T(-5) * z[52]) / T(3) + z[83] + z[94] + z[96] + z[129] + z[134] + -z[137] + z[138] + z[141];
z[83] = z[24] * z[83];
z[129] = (T(3) * z[41]) / T(2);
z[84] = z[72] + z[84] + z[98] + z[129] + -z[131] / T(4);
z[84] = z[9] * z[84];
z[59] = z[26] * z[59];
z[138] = z[0] * z[27];
z[117] = -z[44] + -z[59] + z[117] + -z[138] + z[140];
z[69] = z[69] * z[117];
z[140] = z[6] + -z[68] + z[88];
z[130] = z[130] * z[140];
z[69] = z[69] + z[125] / T(2) + z[130] + z[132];
z[61] = z[26] + z[29] / T(2) + z[61];
z[61] = z[26] * z[61];
z[130] = z[54] + z[108];
z[140] = -(z[27] * z[130]);
z[75] = z[61] + -z[66] + z[75] + z[140];
z[75] = z[8] * z[75];
z[85] = z[85] * z[114];
z[114] = prod_pow(z[27], 2);
z[140] = -z[114] + z[118];
z[140] = z[37] * z[140];
z[141] = z[26] * z[33];
z[79] = z[79] + -z[141];
z[79] = z[26] * z[79];
z[75] = z[69] + z[75] + z[79] + z[84] + z[85] + -z[122] + -z[140];
z[75] = z[1] * z[75];
z[79] = T(5) * z[44];
z[84] = T(-3) * z[27] + z[30];
z[84] = z[27] * z[84];
z[85] = z[29] * z[106];
z[73] = -z[41] + z[73] + -z[79] + z[84] + z[85] + -z[104];
z[61] = z[61] + z[73] / T(2);
z[61] = z[8] * z[61];
z[73] = (T(3) * z[27]) / T(2) + -z[30];
z[73] = z[27] * z[73];
z[73] = z[41] + z[73] + -z[116] + -z[139];
z[84] = -(z[29] * z[107]);
z[73] = z[44] + z[73] / T(2) + z[84];
z[73] = z[9] * z[73];
z[71] = -z[71] + -z[113];
z[71] = z[9] * z[71];
z[71] = z[71] / T(2) + -z[141];
z[71] = z[26] * z[71];
z[61] = z[61] + z[69] + z[71] + z[73] + z[87] + -z[121] / T(2);
z[61] = z[14] * z[61];
z[69] = -(z[91] * z[103]);
z[71] = -z[54] + z[62];
z[73] = z[54] * z[71];
z[84] = -z[29] + z[130];
z[84] = z[29] * z[84];
z[85] = (T(3) * z[42]) / T(2);
z[59] = T(-2) * z[59] + -z[67] + z[69] + -z[72] + z[73] + z[84] + z[85];
z[59] = z[1] * z[59];
z[67] = z[27] * z[71];
z[67] = z[67] + -z[104];
z[69] = z[29] * z[113];
z[71] = z[36] + z[67] + z[69] + -z[79];
z[73] = z[26] * z[113];
z[71] = z[71] / T(2) + T(2) * z[73] + z[85];
z[71] = z[14] * z[71];
z[79] = -z[44] + z[69];
z[67] = -z[36] + -z[42] + z[67] + z[79];
z[78] = z[67] * z[78];
z[59] = z[59] + z[71] + z[78];
z[59] = z[5] * z[59];
z[56] = z[56] * z[113];
z[60] = -z[30] + z[60];
z[60] = z[30] * z[60];
z[56] = z[56] + z[60] + z[64] + -z[66] + z[73] / T(2) + z[98] + -z[129];
z[56] = z[10] * z[56];
z[60] = -z[26] + -z[81] + z[108];
z[60] = z[26] * z[60];
z[64] = z[81] + z[91];
z[64] = z[0] * z[64];
z[60] = z[41] / T(2) + z[60] + z[64] + -z[65] + -z[72] + -z[115];
z[60] = z[1] * z[60];
z[64] = -z[44] + z[73];
z[65] = z[0] * z[30];
z[65] = -z[36] + z[41] + z[64] + z[65] + -z[138];
z[65] = z[65] * z[86];
z[66] = -(z[39] * z[89]);
z[56] = z[56] + z[60] + z[65] + z[66];
z[56] = z[7] * z[56];
z[60] = z[30] * z[93];
z[60] = z[60] + -z[102];
z[60] = z[30] * z[60];
z[60] = z[60] + z[96];
z[65] = z[16] / T(4) + -z[93];
z[66] = z[27] * z[65];
z[70] = z[66] + -z[70] + z[127];
z[70] = z[27] * z[70];
z[71] = z[36] + z[104];
z[71] = z[71] * z[95];
z[72] = z[30] * z[95];
z[72] = z[72] + -z[77];
z[77] = z[29] * z[72];
z[78] = T(4) * z[51];
z[70] = (T(127) * z[52]) / T(24) + -z[60] + z[70] + z[71] + z[77] + -z[78] + -z[137];
z[70] = z[32] * z[70];
z[71] = z[42] + z[117];
z[71] = z[24] * z[71];
z[77] = z[42] + z[124];
z[77] = z[15] * z[77];
z[81] = z[114] + -z[131];
z[64] = -z[42] + -z[64] + z[81] / T(2);
z[84] = z[35] * z[64];
z[55] = z[25] * z[55];
z[67] = z[32] * z[67];
z[55] = z[55] + -z[67] + -z[71] + z[77] + z[84];
z[67] = -(z[55] * z[97]);
z[71] = z[30] * z[92];
z[71] = z[71] + -z[123];
z[71] = z[9] * z[71];
z[77] = prod_pow(z[26], 2);
z[84] = -(z[77] * z[126]);
z[85] = -z[30] + z[90];
z[85] = z[30] * z[85];
z[77] = -z[36] + -z[77] + z[85];
z[77] = z[6] * z[77];
z[68] = -z[68] + -z[74];
z[68] = z[42] * z[68];
z[68] = z[68] + z[71] + z[77] + z[84] + z[125] + -z[132];
z[68] = z[12] * z[68];
z[71] = -(z[39] * z[82]);
z[54] = z[0] + z[54];
z[54] = z[27] * z[54];
z[54] = z[54] + -z[118] + -z[120];
z[54] = z[9] * z[54];
z[74] = -z[27] + z[90];
z[74] = z[27] * z[74];
z[74] = T(-2) * z[41] + z[74] + -z[118];
z[74] = z[8] * z[74];
z[77] = -(z[39] * z[80]);
z[54] = z[54] + z[74] + z[77] + z[119] + z[121] + z[140];
z[54] = z[13] * z[54];
z[74] = z[39] * z[99];
z[55] = -z[55] + z[74];
z[55] = z[55] * z[76];
z[65] = z[65] * z[81];
z[72] = z[26] * z[72];
z[65] = (T(-13) * z[52]) / T(4) + z[65] + z[72] + z[78] + z[96] + -z[135];
z[65] = z[35] * z[65];
z[72] = z[95] * z[120];
z[66] = z[66] + -z[101];
z[66] = z[27] * z[66];
z[66] = (T(-35) * z[52]) / T(24) + z[66] + z[72] + -z[94] + z[134];
z[66] = z[25] * z[66];
z[72] = z[95] * z[124];
z[60] = -z[60] + z[72] + -z[133] + -z[136];
z[60] = z[15] * z[60];
z[72] = T(3) * z[32];
z[74] = -z[72] + z[105];
z[74] = z[53] * z[74];
z[76] = z[27] * z[113];
z[76] = z[41] + z[76] + -z[79];
z[77] = T(3) * z[10] + -z[109];
z[76] = z[38] * z[76] * z[77];
z[74] = z[74] + z[76];
z[64] = z[64] * z[110] * z[111];
z[62] = -(z[62] * z[113]);
z[62] = T(2) * z[44] + z[62] + -z[69] + -z[73];
z[62] = z[62] * z[112];
z[63] = T(2) * z[9] + -z[34] + -z[38] + T(-4) * z[48] + T(3) * z[63] + z[88] + z[128];
z[69] = z[45] * z[46];
z[69] = (T(12) * z[47]) / T(5) + z[69];
z[63] = int_to_imaginary<T>(1) * z[63] * z[69];
z[69] = z[24] + -z[35];
z[69] = -z[25] + (T(-5) * z[32]) / T(2) + z[69] / T(2);
z[69] = z[69] * z[100];
z[72] = z[24] + z[35] + -z[72];
z[72] = z[50] * z[72];
z[69] = z[69] / T(3) + z[72];
z[69] = z[45] * z[69];
z[72] = z[32] + -z[105] / T(3);
z[72] = prod_pow(z[49], 3) * z[72];
return z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + T(2) * z[72] + z[74] / T(2) + z[75] + z[83];
}



template IntegrandConstructorType<double> f_4_39_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_39_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_39_construct (const Kin<qd_real>&);
#endif

}