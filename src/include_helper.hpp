#pragma once

#include "PentagonFunctions_config.h"
#include "Constants.h"
#include "DLogOnPath.h"
#include "FunctionsWeight2.h"
#include "Kin.h"
#include "PolyLog.h"

#include <functional>

namespace PentagonFunctions {

template <typename T> using IntegrandConstructorType = std::function<std::complex<T>(T)>;

} // PentagonFunctions
