#include "Constants.h"

namespace  PentagonFunctions {

template <> double IntegrationTolerance<double> = 1e-9;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
template <> dd_real IntegrationTolerance<dd_real> = ::constants::root_epsilon<dd_real>;
template <> qd_real IntegrationTolerance<qd_real> = ::constants::root_epsilon<qd_real>;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

template <> double SwitchToHPThreshold<double> = 1e-4;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
template <> double SwitchToHPThreshold<dd_real> = to_double(90*IntegrationTolerance<dd_real>);
template <> double SwitchToHPThreshold<qd_real> = to_double(90*IntegrationTolerance<qd_real>);
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
bool always_switch_to_HP_for_sqrt_divergences = true;
#endif

namespace detail {

template <> double VanishingDLogThreshold<double> = 1e-12;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
template <> dd_real VanishingDLogThreshold<dd_real> = 1e-24;
template <> qd_real VanishingDLogThreshold<qd_real> = 1e-58;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

template <> double SpDLogZeroThreshold<double> = 1e-10;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
template <> dd_real SpDLogZeroThreshold<dd_real> = 1e-20;
template <> qd_real SpDLogZeroThreshold<qd_real> = 1e-36;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

template <> int TanhSinh_MaxHalvings<double> = 7;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
template <> int TanhSinh_MaxHalvings<dd_real> = 8;
template <> int TanhSinh_MaxHalvings<qd_real> = 9;
#endif


} // detail
} //  PentagonFunctions
