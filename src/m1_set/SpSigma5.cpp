#include "PentagonFunctions_config.h"

#include "SpSigma5.h"
#include "Constants.h"
#include "PolyLog.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "DLog.h"


namespace PentagonFunctions {
namespace m1_set {

#include "derivatives.incl.hpp"


template <typename T> std::array<T, 2> f_2_30_series_coefficients(const std::array<T, 6>& v) {
    std::array<T, 4> qs;
    {
        T z[5];
        z[0] = -v[2] + v[5];
        z[0] = v[1] * z[0];
        z[1] = v[4] * v[5];
        z[0] = z[0] + -z[1];
        z[1] = v[2] + -v[4];
        z[2] = v[3] * z[1];
        z[3] = z[0] + z[2];
        z[4] = -2 * v[5] + z[1];
        z[4] = v[3] * z[4];
        z[4] = z[0] + z[4];
        z[2] = z[0] + -z[2];
        z[1] = -2 * v[1] + -z[1];
        z[1] = v[3] * z[1];
        z[0] = z[0] + z[1];
        qs = {z[3], z[4], z[2], z[0]};
        for (auto& qi : qs) qi = 1 / qi;
    }

    return {4 * rlog(2 * qs[1] + -2 * qs[2]) * (qs[1] + -qs[2]) +
                4 * rlog(-2 * qs[0] + 2 * qs[1] + -2 * qs[2] + 2 * qs[3]) * (qs[0] + -qs[1] + qs[2] + -qs[3]) +
                rlog(-2 * qs[0] + 2 * qs[3]) * (-4 * qs[0] + 4 * qs[3]),
            (-prod_pow(qs[2], 3) + prod_pow(qs[1], 3)) * (T(4) / T(3)) * rlog(2 * qs[1] + -2 * qs[2]) +
                (-prod_pow(qs[3], 3) + prod_pow(qs[0], 3)) * (T(-4) / T(3)) * rlog(-2 * qs[0] + 2 * qs[3]) +
                (-prod_pow(qs[1], 3) + -prod_pow(qs[3], 3) + prod_pow(qs[0], 3) + prod_pow(qs[2], 3)) * (T(4) / T(3)) *
                    rlog(-2 * qs[0] + 2 * qs[1] + -2 * qs[2] + 2 * qs[3]) +
                (T(2) / T(3)) * (qs[1] + -qs[2]) * (qs[0] + -qs[3]) * (-qs[0] + qs[1] + -qs[2] + qs[3])};
}
template std::array<double, 2> f_2_30_series_coefficients(const std::array<double, 6>& v);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<dd_real, 2> f_2_30_series_coefficients(const std::array<dd_real, 6>& v);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<qd_real, 2> f_2_30_series_coefficients(const std::array<qd_real, 6>& v);
#endif

template <typename T> std::array<T, 2> f_2_31_series_coefficients(const std::array<T, 6>& v) {
    std::array<T, 4> qs;
    {
        T z[9];
        z[0] = -v[0] + v[5];
        z[1] = v[4] * z[0];
        z[2] = -v[2] + v[4];
        z[3] = v[3] * z[2];
        z[4] = z[1] + -z[3];
        z[5] = v[2] + z[0];
        z[6] = -v[1] * z[5];
        z[6] = z[4] + z[6];
        z[7] = v[1] + -v[3];
        z[8] = 2 * v[4];
        z[5] = -z[5] + -2 * z[7] + z[8];
        z[5] = v[1] * z[5];
        z[4] = z[4] + z[5];
        z[2] = 2 * z[0] + z[2];
        z[2] = v[3] * z[2];
        z[5] = -v[2] + z[0];
        z[5] = -v[1] * z[5];
        z[1] = z[1] + z[2] + z[5];
        z[0] = z[0] + z[8];
        z[2] = -2 * v[2] + z[0];
        z[2] = v[4] * z[2];
        z[0] = v[2] + -z[0];
        z[0] = v[1] * z[0];
        z[0] = z[0] + z[2] + z[3];
        qs = {z[6], z[4], z[1], z[0]};
        for (auto& qi : qs) qi = 1 / qi;
    }

    return {-4 * rlog(-2 * qs[0] + -2 * qs[2]) * (qs[0] + qs[2]) + 4 * rlog(2 * qs[1] + -2 * qs[3]) * (qs[1] + -qs[3]) +
                4 * rlog(-2 * qs[0] + 2 * qs[1] + -2 * qs[2] + -2 * qs[3]) * (qs[0] + -qs[1] + qs[2] + qs[3]),
            (prod_pow(qs[0], 3) + prod_pow(qs[2], 3)) * (T(-4) / T(3)) * rlog(-2 * qs[0] + -2 * qs[2]) +
                (-prod_pow(qs[3], 3) + prod_pow(qs[1], 3)) * (T(4) / T(3)) * rlog(2 * qs[1] + -2 * qs[3]) +
                (-prod_pow(qs[1], 3) + prod_pow(qs[0], 3) + prod_pow(qs[2], 3) + prod_pow(qs[3], 3)) * (T(4) / T(3)) *
                    rlog(-2 * qs[0] + 2 * qs[1] + -2 * qs[2] + -2 * qs[3]) +
                (T(-2) / T(3)) * (qs[0] + qs[2]) * (qs[1] + -qs[3]) * (qs[0] + -qs[1] + qs[2] + qs[3])};
}
template std::array<double, 2> f_2_31_series_coefficients(const std::array<double, 6>& v);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<dd_real, 2> f_2_31_series_coefficients(const std::array<dd_real, 6>& v);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<qd_real, 2> f_2_31_series_coefficients(const std::array<qd_real, 6>& v);
#endif

template <typename T> std::array<T, 2> f_2_32_re_series_coefficients(const std::array<T, 6>& v) {
    std::array<T, 4> qs;
    {
        T z[6];
        z[0] = v[0] + -v[2] + v[4];
        z[0] = v[3] * z[0];
        z[1] = -v[2] + v[5];
        z[2] = v[1] * z[1];
        z[2] = z[0] + -z[2];
        z[3] = -v[0] + v[5];
        z[3] = v[4] * z[3];
        z[4] = -z[2] + -z[3];
        z[2] = -z[2] + z[3];
        z[5] = 2 * v[4];
        z[1] = z[1] + z[5];
        z[1] = v[1] * z[1];
        z[0] = -z[0] + z[1];
        z[1] = z[0] + z[3];
        z[3] = -v[0] + 2 * v[2] + -v[5] + -z[5];
        z[3] = v[4] * z[3];
        z[0] = z[0] + z[3];
        qs = {z[4], z[2], z[1], z[0]};
        for (auto& qi : qs) qi = 1 / qi;
    }

    return {rlog(2 * qs[0] + -2 * qs[1]) * (-4 * qs[0] + 4 * qs[1]) +
                4 * rlog(2 * qs[0] + -2 * qs[1] + 2 * qs[2] + -2 * qs[3]) * (qs[0] + -qs[1] + qs[2] + -qs[3]) +
                rlog(2 * qs[2] + -2 * qs[3]) * (-4 * qs[2] + 4 * qs[3]),
            (-prod_pow(qs[1], 3) + prod_pow(qs[0], 3)) * (T(-4) / T(3)) * rlog(2 * qs[0] + -2 * qs[1]) +
                (-prod_pow(qs[3], 3) + prod_pow(qs[2], 3)) * (T(-4) / T(3)) * rlog(2 * qs[2] + -2 * qs[3]) +
                (-prod_pow(qs[1], 3) + -prod_pow(qs[3], 3) + prod_pow(qs[0], 3) + prod_pow(qs[2], 3)) * (T(4) / T(3)) *
                    rlog(2 * qs[0] + -2 * qs[1] + 2 * qs[2] + -2 * qs[3]) +
                (T(2) / T(3)) * (qs[0] + -qs[1]) * (qs[2] + -qs[3]) * (qs[0] + -qs[1] + qs[2] + -qs[3])};
}

template <typename T> std::array<T, 2> f_2_32_im_series_coefficients(const std::array<T, 6>& v) {
    std::array<T, 4> qs;
    {
        T z[6];
        z[0] = v[0] + -v[2] + v[4];
        z[0] = v[3] * z[0];
        z[1] = -v[2] + v[5];
        z[2] = v[1] * z[1];
        z[2] = z[0] + -z[2];
        z[3] = -v[0] + v[5];
        z[3] = v[4] * z[3];
        z[4] = -z[2] + -z[3];
        z[2] = -z[2] + z[3];
        z[5] = 2 * v[4];
        z[1] = z[1] + z[5];
        z[1] = v[1] * z[1];
        z[0] = -z[0] + z[1];
        z[1] = z[0] + z[3];
        z[3] = -v[0] + 2 * v[2] + -v[5] + -z[5];
        z[3] = v[4] * z[3];
        z[0] = z[0] + z[3];
        qs = {z[4], z[2], z[1], z[0]};
        for (auto& qi : qs) qi = 1 / qi;
    }

    return {2 * bc<T>[0] * (-2 * qs[0] + 2 * qs[1] + -2 * qs[2] + 2 * qs[3]), 2 * bc<T>[0] *
                                                                                  (prod_pow(qs[0], 3) * (T(-2) / T(3)) + prod_pow(qs[2], 3) * (T(-2) / T(3)) +
                                                                                   prod_pow(qs[1], 3) * (T(2) / T(3)) + prod_pow(qs[3], 3) * (T(2) / T(3)))};
}

template <typename T> std::array<std::complex<T>,2> f_2_32_series_coefficients(const std::array<T,6>& v) {
    auto re = f_2_32_re_series_coefficients(v);
    auto im = f_2_32_im_series_coefficients(v);
    return {std::complex{re[0],im[0]}, std::complex{re[1], im[1]}};
}
template std::array<std::complex<double>, 2> f_2_32_series_coefficients(const std::array<double, 6>& v);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<std::complex<dd_real>, 2> f_2_32_series_coefficients(const std::array<dd_real, 6>& v);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<std::complex<qd_real>, 2> f_2_32_series_coefficients(const std::array<qd_real, 6>& v);
#endif

template <typename T> std::array<T, 2> f_2_33_series_coefficients(const std::array<T, 6>& v) {
    std::array<T, 4> qs;
    {
        T z[8];
        z[0] = 2 * v[3];
        z[1] = v[5] + -z[0];
        z[2] = v[0] + -v[1];
        z[1] = z[1] * z[2];
        z[3] = -v[3] + v[5];
        z[3] = v[4] * z[3];
        z[4] = -v[3] + z[2];
        z[5] = -v[2] * z[4];
        z[1] = z[1] + z[3] + z[5];
        z[5] = v[2] + -v[4];
        z[6] = 2 * v[5];
        z[4] = -z[4] + 2 * z[5] + -z[6];
        z[4] = v[2] * z[4];
        z[5] = v[5] * z[2];
        z[4] = z[3] + z[4] + z[5];
        z[7] = v[3] + z[2];
        z[7] = -v[2] * z[7];
        z[5] = -z[3] + z[5] + z[7];
        z[2] = -z[2] + z[6];
        z[0] = z[0] + -z[2];
        z[0] = v[5] * z[0];
        z[2] = -v[3] + z[2];
        z[2] = v[2] * z[2];
        z[0] = z[0] + z[2] + -z[3];
        qs = {z[1], z[4], z[5], z[0]};
        for (auto& qi : qs) qi = 1 / qi;
    }

    return {-4 * rlog(-2 * qs[0] + -2 * qs[2]) * (qs[0] + qs[2]) +
                4 * rlog(-2 * qs[0] + -2 * qs[1] + -2 * qs[2] + 2 * qs[3]) * (qs[0] + qs[1] + qs[2] + -qs[3]) +
                rlog(-2 * qs[1] + 2 * qs[3]) * (-4 * qs[1] + 4 * qs[3]),
            (prod_pow(qs[0], 3) + prod_pow(qs[2], 3)) * (T(-4) / T(3)) * rlog(-2 * qs[0] + -2 * qs[2]) +
                (-prod_pow(qs[3], 3) + prod_pow(qs[1], 3)) * (T(-4) / T(3)) * rlog(-2 * qs[1] + 2 * qs[3]) +
                (-prod_pow(qs[3], 3) + prod_pow(qs[0], 3) + prod_pow(qs[1], 3) + prod_pow(qs[2], 3)) * (T(4) / T(3)) *
                    rlog(-2 * qs[0] + -2 * qs[1] + -2 * qs[2] + 2 * qs[3]) +
                (T(2) / T(3)) * (qs[0] + qs[2]) * (qs[1] + -qs[3]) * (qs[0] + qs[1] + qs[2] + -qs[3])};
}
template std::array<double, 2> f_2_33_series_coefficients(const std::array<double, 6>& v);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<dd_real, 2> f_2_33_series_coefficients(const std::array<dd_real, 6>& v);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<qd_real, 2> f_2_33_series_coefficients(const std::array<qd_real, 6>& v);
#endif

template <typename T> std::array<T, 2> f_2_34_series_coefficients(const std::array<T, 6>& v) {
    std::array<T, 4> qs;
    {
        T z[10];
        z[0] = 2 * v[5];
        z[1] = v[1] + -v[3];
        z[2] = 2 * v[0];
        z[3] = z[0] + z[1] + -z[2];
        z[3] = v[2] * z[3];
        z[4] = -v[3] + v[5];
        z[5] = v[4] * z[4];
        z[3] = z[3] + -z[5];
        z[5] = 3 * v[0] + 2 * v[3];
        z[0] = z[0] + -z[5];
        z[0] = v[5] * z[0];
        z[6] = v[3] * z[2];
        z[7] = -v[0] + v[5];
        z[7] = v[1] * z[7];
        z[8] = prod_pow(v[0], 2);
        z[0] = z[0] + -z[3] + z[6] + z[7] + z[8];
        z[9] = 2 * v[1];
        z[2] = -z[2] + z[4] + z[9];
        z[2] = v[4] * z[2];
        z[1] = v[2] * z[1];
        z[1] = -z[1] + z[2];
        z[2] = v[0] * v[5];
        z[2] = z[2] + -z[8];
        z[4] = z[2] + -z[7];
        z[7] = z[1] + -z[4];
        z[3] = z[3] + -z[4];
        z[4] = v[5] + -z[5] + z[9];
        z[4] = v[1] * z[4];
        z[1] = -z[1] + -z[2] + z[4] + z[6];
        qs = {z[0], z[7], z[3], z[1]};
    }

    for (auto& qi : qs) qi = 1 / qi;

    return {-4 * rlog(-2 * qs[0] + -2 * qs[2]) * (qs[0] + qs[2]) + -4 * rlog(-2 * qs[1] + -2 * qs[3]) * (qs[1] + qs[3]) +
                4 * rlog(-2 * qs[0] + -2 * qs[1] + -2 * qs[2] + -2 * qs[3]) * (qs[0] + qs[1] + qs[2] + qs[3]),
            (prod_pow(qs[0], 3) + prod_pow(qs[2], 3)) * (T(-4) / T(3)) * rlog(-2 * qs[0] + -2 * qs[2]) +
                (prod_pow(qs[1], 3) + prod_pow(qs[3], 3)) * (T(-4) / T(3)) * rlog(-2 * qs[1] + -2 * qs[3]) +
                (prod_pow(qs[0], 3) + prod_pow(qs[1], 3) + prod_pow(qs[2], 3) + prod_pow(qs[3], 3)) * (T(4) / T(3)) *
                    rlog(-2 * qs[0] + -2 * qs[1] + -2 * qs[2] + -2 * qs[3]) +
                (T(2) / T(3)) * (qs[0] + qs[2]) * (qs[1] + qs[3]) * (qs[0] + qs[1] + qs[2] + qs[3])};
}
template std::array<double, 2> f_2_34_series_coefficients(const std::array<double, 6>& v);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<dd_real, 2> f_2_34_series_coefficients(const std::array<dd_real, 6>& v);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<qd_real, 2> f_2_34_series_coefficients(const std::array<qd_real, 6>& v);
#endif

template <typename T> std::array<T, 2> f_2_35_series_coefficients(const std::array<T, 6>& v) {
    std::array<T, 4> qs;
    {
        T z[6];
        z[0] = v[0] + v[2] + -v[4];
        z[0] = v[3] * z[0];
        z[1] = v[1] + -v[4];
        z[2] = v[5] * z[1];
        z[2] = z[0] + -z[2];
        z[3] = -v[0] + v[1];
        z[3] = v[2] * z[3];
        z[4] = -z[2] + -z[3];
        z[2] = -z[2] + z[3];
        z[5] = 2 * v[2];
        z[1] = z[1] + z[5];
        z[1] = v[5] * z[1];
        z[0] = -z[0] + z[1];
        z[1] = z[0] + z[3];
        z[3] = -v[0] + -v[1] + 2 * v[4] + -z[5];
        z[3] = v[2] * z[3];
        z[0] = z[0] + z[3];
        qs = {z[4], z[2], z[1], z[0]};
    }

    for (auto& qi : qs) qi = 1 / qi;

    return {rlog(2 * qs[0] + -2 * qs[1]) * (-4 * qs[0] + 4 * qs[1]) +
                4 * rlog(2 * qs[0] + -2 * qs[1] + 2 * qs[2] + -2 * qs[3]) * (qs[0] + -qs[1] + qs[2] + -qs[3]) +
                rlog(2 * qs[2] + -2 * qs[3]) * (-4 * qs[2] + 4 * qs[3]),
            (-prod_pow(qs[1], 3) + prod_pow(qs[0], 3)) * (T(-4) / T(3)) * rlog(2 * qs[0] + -2 * qs[1]) +
                (-prod_pow(qs[3], 3) + prod_pow(qs[2], 3)) * (T(-4) / T(3)) * rlog(2 * qs[2] + -2 * qs[3]) +
                (-prod_pow(qs[1], 3) + -prod_pow(qs[3], 3) + prod_pow(qs[0], 3) + prod_pow(qs[2], 3)) * (T(4) / T(3)) *
                    rlog(2 * qs[0] + -2 * qs[1] + 2 * qs[2] + -2 * qs[3]) +
                (T(2) / T(3)) * (qs[0] + -qs[1]) * (qs[2] + -qs[3]) * (qs[0] + -qs[1] + qs[2] + -qs[3])};
}
template std::array<double, 2> f_2_35_series_coefficients(const std::array<double, 6>& v);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<dd_real, 2> f_2_35_series_coefficients(const std::array<dd_real, 6>& v);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::array<qd_real, 2> f_2_35_series_coefficients(const std::array<qd_real, 6>& v);
#endif

template <typename T>
void SpDLogSigma5_W_200(const Kin<T, KinType::m1>& path_point, const std::array<T, 6>& dv, const std::array<T, 10>& dlr, T& set_re, T& set_im) {

    const auto& w = path_point.W[199];

    if (fabs(w * w) < ::constants::root_epsilon<T>) {
        auto fcoeffs = f_2_31_series_coefficients(path_point.v);

        auto ds = get_sqrt_derivatives(199, path_point.v, dv) / 2;

        if (w.imag() != T(0)) {
            set_im = -fcoeffs.at(0) / w.imag() + fcoeffs.at(1) * w.imag();
            set_im *= ds;
        }
        else {
            set_re = fcoeffs.at(0) / w.real() + fcoeffs.at(1) * w.real();
            set_re *= ds;
        }
    }
    else {
        const auto& k = path_point;
        set_re = dlr[5] * f_2_31_re(k);
        set_im = dlr[5] * f_2_31_im(k);
    }
}

template void SpDLogSigma5_W_200(const Kin<double, KinType::m1>& path_point, const std::array<double, 6>&, const std::array<double, 10>& dlr, double& set_re, double& set_im);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template void SpDLogSigma5_W_200(const Kin<dd_real, KinType::m1>& path_point, const std::array<dd_real, 6>&, const std::array<dd_real, 10>& dlr, dd_real& set_re, dd_real& set_im);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template void SpDLogSigma5_W_200(const Kin<qd_real, KinType::m1>& path_point, const std::array<qd_real, 6>&, const std::array<qd_real, 10>& dlr, qd_real& set_re, qd_real& set_im);
#endif

template <typename T> void SpDLogSigma5_W_202 (const Kin<T, KinType::m1>& path_point, const std::array<T,6>& dv, const std::array<T,10>& dlr, T& set_re, T& set_im) {

	const auto& w = path_point.W[201];

    if (fabs(w*w) < ::constants::root_epsilon<T>) {
        auto fcoeffs = f_2_33_series_coefficients(path_point.v);

        auto ds = get_sqrt_derivatives(201, path_point.v, dv) / 2;

        if (w.imag() != T(0)) {
            set_im = -fcoeffs.at(0) / w.imag() + fcoeffs.at(1) * w.imag();
            set_im *= ds;
        }
        else {
            set_re = fcoeffs.at(0) / w.real() + fcoeffs.at(1) * w.real();
            set_re *= ds;
        }
    }
    else {
        const auto& k = path_point;
        set_re = dlr[7] * f_2_33_re(k);
        set_im = dlr[7] * f_2_33_im(k);
    }
}

template void SpDLogSigma5_W_202 (const Kin<double, KinType::m1>& path_point, const std::array<double,6>&, const std::array<double,10>& dlr, double& set_re, double& set_im);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED                                                           
template void SpDLogSigma5_W_202 (const Kin<dd_real, KinType::m1>& path_point,const std::array<dd_real,6>&, const std::array<dd_real,10>& dlr, dd_real& set_re, dd_real& set_im);
#endif                                                                                                     
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED                                                           
template void SpDLogSigma5_W_202 (const Kin<qd_real, KinType::m1>& path_point,const std::array<qd_real,6>&, const std::array<qd_real,10>& dlr, qd_real& set_re, qd_real& set_im);
#endif



template <typename T> void SpDLogSigma5_W_203 (const Kin<T, KinType::m1>& path_point, const std::array<T,6>& dv, const std::array<T,10>& dlr, T& set_re, T& set_im) {

	const auto& w = path_point.W[202];

    if (fabs(w*w) < ::constants::root_epsilon<T>) {
        auto fcoeffs = f_2_34_series_coefficients(path_point.v);

        auto ds = get_sqrt_derivatives(202, path_point.v, dv) / 2;

        if (w.imag() != T(0)) {
            set_im = -fcoeffs.at(0) / w.imag() + fcoeffs.at(1) * w.imag();
            set_im *= ds;
        }
        else {
            set_re = fcoeffs.at(0) / w.real() + fcoeffs.at(1) * w.real();
            set_re *= ds;
        }
    }
    else {
        const auto& k = path_point;
        set_re = dlr[8] * f_2_34_re(k);
        set_im = dlr[8] * f_2_34_im(k);
    }
}

template void SpDLogSigma5_W_203 (const Kin<double, KinType::m1>& path_point, const std::array<double,6>&, const std::array<double,10>& dlr, double& set_re, double& set_im);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED                                                           
template void SpDLogSigma5_W_203 (const Kin<dd_real, KinType::m1>& path_point,const std::array<dd_real,6>&, const std::array<dd_real,10>& dlr, dd_real& set_re, dd_real& set_im);
#endif                                                                                                     
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED                                                           
template void SpDLogSigma5_W_203 (const Kin<qd_real, KinType::m1>& path_point,const std::array<qd_real,6>&, const std::array<qd_real,10>& dlr, qd_real& set_re, qd_real& set_im);
#endif



template <typename T> void SpDLogSigma5_W_204 (const Kin<T, KinType::m1>& path_point, const std::array<T,6>& dv, const std::array<T,10>& dlr, T& set_re, T& set_im) {

	const auto& w = path_point.W[203];

    if (fabs(w*w) < ::constants::root_epsilon<T>) {
        auto fcoeffs = f_2_35_series_coefficients(path_point.v);

        auto ds = get_sqrt_derivatives(203, path_point.v, dv) / 2;

        if (w.imag() != T(0)) {
            set_im = -fcoeffs.at(0) / w.imag() + fcoeffs.at(1) * w.imag();
            set_im *= ds;
        }
        else {
            set_re = fcoeffs.at(0) / w.real() + fcoeffs.at(1) * w.real();
            set_re *= ds;
        }
    }
    else {
		const auto& k = path_point;
        set_re = dlr[9] * f_2_35_re(k);
        set_im = dlr[9] * f_2_35_im(k);
    }
}

template void SpDLogSigma5_W_204 (const Kin<double, KinType::m1>& path_point,  const std::array<double,6>&, const std::array<double,10>& dlr, double& set_re, double& set_im);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED                                                            
template void SpDLogSigma5_W_204 (const Kin<dd_real, KinType::m1>& path_point, const std::array<dd_real,6>&,const std::array<dd_real,10>& dlr, dd_real& set_re, dd_real& set_im);
#endif                                                                                                      
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED                                                            
template void SpDLogSigma5_W_204 (const Kin<qd_real, KinType::m1>& path_point, const std::array<qd_real,6>&,const std::array<qd_real,10>& dlr, qd_real& set_re, qd_real& set_im);
#endif



template <typename T>
void SpDLogSigma5_W_199(const Kin<T, KinType::m1>& path_point, const std::array<T, 6>& dv, const std::array<T, 10>& dlr, T& set_re, T& set_im) {

    const auto& w = path_point.W[198];

    if (fabs(w * w) < ::constants::root_epsilon<T>) {

        auto fcoeffs = f_2_30_series_coefficients(path_point.v);

        auto ds = get_sqrt_derivatives(198, path_point.v, dv) / 2;

        if (w.imag() != T(0)) {
            set_im = -fcoeffs.at(0) / w.imag() + fcoeffs.at(1) * w.imag();
            set_im *= ds;
        }
        else {
            set_re = fcoeffs.at(0) / w.real() + fcoeffs.at(1) * w.real();
            set_re *= ds;
        }
    }
    else {
        const auto& k = path_point;
        set_re = dlr[4] * f_2_30_re(k);
        set_im = dlr[4] * f_2_30_im(k);
    }
}
template void SpDLogSigma5_W_199 (const Kin<double, KinType::m1>& path_point,  const std::array<double,6>&, const std::array<double,10>& dlr, double& set_re, double& set_im);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template void SpDLogSigma5_W_199 (const Kin<dd_real, KinType::m1>& path_point, const std::array<dd_real,6>&, const std::array<dd_real,10>& dlr, dd_real& set_re, dd_real& set_im);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template void SpDLogSigma5_W_199 (const Kin<qd_real, KinType::m1>& path_point, const std::array<qd_real,6>&, const std::array<qd_real,10>& dlr, qd_real& set_re, qd_real& set_im);
#endif

template <typename T>
void SpDLogSigma5_W_201(const Kin<T, KinType::m1>& path_point, const std::array<T, 6>& dv, const std::array<T, 10>& dlr, T& set_re, T& set_im) {

    const auto& w = path_point.W[200];


    if (fabs(w * w) < ::constants::root_epsilon<T>) {
        auto fcoeffs_re = f_2_32_re_series_coefficients(path_point.v);
        auto fcoeffs_im = f_2_32_im_series_coefficients(path_point.v);

        auto ds = get_sqrt_derivatives(200, path_point.v, dv) / 2;

        if (w.imag() != T(0)) {
            // note signs!
            set_re = fcoeffs_im.at(0) / w.imag() - fcoeffs_re.at(1) * w.imag();
            set_im = -fcoeffs_re.at(0) / w.imag() + fcoeffs_im.at(1) * w.imag();
        }
        else {
            set_re = fcoeffs_re.at(0) / w.real() + fcoeffs_re.at(1) * w.real();
            set_im = fcoeffs_im.at(0) / w.real() + fcoeffs_im.at(1) * w.real();
        }
        set_re *= ds;
        set_im *= ds;
    }
    else {
        const auto& k = path_point;
        set_re = dlr[6] * (4 * prod_pow(m1_set::bc<T>[0], 2) + f_2_32_re(k));
        set_im = dlr[6] * f_2_32_im(k);
    }
    
}
template void SpDLogSigma5_W_201 (const Kin<double, KinType::m1>& path_point, const std::array<double,6>& dv, const std::array<double,10>& dlr, double& set_re, double& set_im);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template void SpDLogSigma5_W_201 (const Kin<dd_real, KinType::m1>& path_point, const std::array<dd_real,6>& dv, const std::array<dd_real,10>& dlr, dd_real& set_re, dd_real& set_im);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template void SpDLogSigma5_W_201 (const Kin<qd_real, KinType::m1>& path_point, const std::array<qd_real,6>& dv, const std::array<qd_real,10>& dlr, qd_real& set_re, qd_real& set_im);
#endif



template <typename T> std::array<std::complex<T>,2> dlog_W_Sigma5_series (int wi, const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	switch (wi) {
		case 161: {
    std::array<T,2> a;

    {
T z[6];
z[0] = k.v[2] * k.v[3];
z[1] = k.v[2] + -k.v[5];
z[2] = k.v[1] * z[1];
z[3] = k.v[3] + -k.v[5];
z[4] = -k.v[4] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = k.v[1] + k.v[3];
z[2] = dv[2] * z[2];
z[1] = dv[1] * z[1];
z[3] = -dv[4] * z[3];
z[4] = k.v[2] + -k.v[4];
z[4] = dv[3] * z[4];
z[5] = -k.v[1] + k.v[4];
z[5] = dv[5] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
a = {z[0], z[1]};
}


    T ds = get_sqrt_derivatives(198, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 162: {
    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[2] * k.v[3];
z[1] = -k.v[0] + k.v[5];
z[2] = -k.v[3] + z[1];
z[3] = -k.v[4] * z[2];
z[1] = k.v[2] + z[1];
z[4] = k.v[1] * z[1];
z[0] = z[0] + z[3] + z[4];
z[3] = -dv[2] * k.v[3];
z[4] = -dv[3] * k.v[2];
z[1] = dv[1] * z[1];
z[5] = -dv[0] + dv[5];
z[6] = dv[3] + -z[5];
z[6] = k.v[4] * z[6];
z[2] = -dv[4] * z[2];
z[5] = dv[2] + z[5];
z[5] = k.v[1] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T ds = get_sqrt_derivatives(199, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 163: {
    std::array<T,2> a;

    {
T z[6];
z[0] = -k.v[4] * k.v[5];
z[1] = -k.v[2] + k.v[5];
z[2] = k.v[1] * z[1];
z[3] = 2 * k.v[1] + k.v[2] + -k.v[4];
z[4] = -k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = -dv[3] * z[3];
z[3] = k.v[1] + -k.v[4];
z[3] = dv[5] * z[3];
z[4] = -k.v[1] + -k.v[3];
z[4] = dv[2] * z[4];
z[5] = k.v[3] + -k.v[5];
z[5] = dv[4] * z[5];
z[1] = -2 * k.v[3] + z[1];
z[1] = dv[1] * z[1];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
a = {z[0], z[1]};
}


    T ds = get_sqrt_derivatives(198, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 164: {
    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[2] + k.v[5];
z[1] = k.v[1] * z[0];
z[2] = -k.v[0] + k.v[2];
z[3] = k.v[3] * z[2];
z[4] = k.v[0] + k.v[3] + -k.v[5];
z[5] = -k.v[4] * z[4];
z[1] = z[1] + z[3] + z[5];
z[3] = -dv[0] + dv[2];
z[3] = k.v[3] * z[3];
z[5] = -dv[2] + dv[5];
z[5] = k.v[1] * z[5];
z[0] = dv[1] * z[0];
z[2] = dv[3] * z[2];
z[6] = -dv[0] + -dv[3] + dv[5];
z[6] = k.v[4] * z[6];
z[4] = -dv[4] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T ds = get_sqrt_derivatives(200, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 165: {
    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[0] + -k.v[5];
z[1] = 2 * k.v[1];
z[2] = z[0] + -z[1];
z[3] = -k.v[2] + z[2];
z[3] = k.v[1] * z[3];
z[2] = k.v[3] + z[2];
z[4] = -k.v[4] * z[2];
z[1] = k.v[2] + z[1];
z[5] = k.v[3] * z[1];
z[3] = z[3] + z[4] + z[5];
z[4] = -dv[0] + dv[5];
z[5] = 2 * dv[1];
z[6] = z[4] + z[5];
z[6] = k.v[4] * z[6];
z[2] = -dv[4] * z[2];
z[4] = -dv[2] + -z[4];
z[4] = k.v[1] * z[4];
z[0] = -4 * k.v[1] + -k.v[2] + z[0];
z[0] = dv[1] * z[0];
z[5] = dv[2] + z[5];
z[5] = k.v[3] * z[5];
z[1] = -k.v[4] + z[1];
z[1] = dv[3] * z[1];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6];
a = {z[3], z[0]};
}


    T ds = get_sqrt_derivatives(199, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 166: {
    std::array<T,2> a;

    {
T z[8];
z[0] = k.v[2] + -k.v[5];
z[1] = k.v[1] * z[0];
z[2] = -k.v[0] + 2 * k.v[1] + k.v[5];
z[3] = -k.v[4] * z[2];
z[4] = k.v[0] + -k.v[2] + k.v[4];
z[5] = k.v[3] * z[4];
z[1] = z[1] + z[3] + z[5];
z[0] = dv[1] * z[0];
z[3] = -dv[5] * k.v[1];
z[5] = -2 * dv[1] + -dv[5];
z[5] = k.v[4] * z[5];
z[4] = dv[3] * z[4];
z[6] = k.v[1] + -k.v[3];
z[6] = dv[2] * z[6];
z[2] = k.v[3] + -z[2];
z[2] = dv[4] * z[2];
z[7] = k.v[3] + k.v[4];
z[7] = dv[0] * z[7];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
a = {z[1], z[0]};
}


    T ds = get_sqrt_derivatives(200, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 167: {
    std::array<T,2> a;

    {
T z[6];
z[0] = k.v[0] + -k.v[1];
z[1] = 2 * k.v[2];
z[2] = z[0] + -z[1];
z[3] = -k.v[3] + z[2];
z[3] = k.v[2] * z[3];
z[2] = k.v[4] + z[2];
z[4] = -k.v[5] * z[2];
z[1] = k.v[3] + z[1];
z[5] = k.v[4] * z[1];
z[3] = z[3] + z[4] + z[5];
z[4] = -dv[0] + dv[1];
z[5] = -k.v[2] + k.v[5];
z[4] = z[4] * z[5];
z[5] = k.v[4] + k.v[5];
z[0] = -4 * k.v[2] + -k.v[3] + z[0] + 2 * z[5];
z[0] = dv[2] * z[0];
z[2] = -dv[5] * z[2];
z[1] = -k.v[5] + z[1];
z[1] = dv[4] * z[1];
z[5] = -k.v[2] + k.v[4];
z[5] = dv[3] * z[5];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5];
a = {z[3], z[0]};
}


    T ds = get_sqrt_derivatives(201, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 168: {
    std::array<T,2> a;

    {
T z[9];
z[0] = -k.v[0] + k.v[1];
z[1] = k.v[0] + -k.v[5];
z[0] = z[0] * z[1];
z[2] = 2 * k.v[0];
z[3] = k.v[1] + -k.v[3] + 2 * k.v[5] + -z[2];
z[4] = -k.v[2] * z[3];
z[5] = k.v[3] + -k.v[5];
z[6] = -k.v[4] * z[5];
z[0] = z[0] + z[4] + z[6];
z[4] = -dv[4] * z[5];
z[5] = dv[5] * k.v[0];
z[2] = k.v[5] + -z[2];
z[2] = dv[0] * z[2];
z[6] = dv[0] + -dv[5];
z[7] = k.v[1] * z[6];
z[3] = -dv[2] * z[3];
z[6] = dv[3] + 2 * z[6];
z[6] = k.v[2] * z[6];
z[1] = -k.v[2] + z[1];
z[1] = dv[1] * z[1];
z[8] = -dv[3] + dv[5];
z[8] = k.v[4] * z[8];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
a = {z[0], z[1]};
}


    T ds = get_sqrt_derivatives(202, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 169: {
    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[0] + -k.v[1];
z[1] = k.v[4] + z[0];
z[2] = -k.v[5] * z[1];
z[3] = k.v[2] * z[0];
z[4] = -k.v[2] + k.v[4] + 2 * z[0];
z[5] = k.v[3] * z[4];
z[2] = z[2] + z[3] + z[5];
z[3] = k.v[2] + -k.v[5];
z[5] = dv[0] + -dv[1];
z[3] = z[3] * z[5];
z[0] = dv[2] * z[0];
z[4] = dv[3] * z[4];
z[5] = -dv[2] + 2 * z[5];
z[5] = k.v[3] * z[5];
z[1] = -dv[5] * z[1];
z[6] = k.v[3] + -k.v[5];
z[6] = dv[4] * z[6];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T ds = get_sqrt_derivatives(201, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 170: {
    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[2] + k.v[5];
z[1] = -k.v[1] * z[0];
z[2] = -k.v[0] + k.v[2];
z[3] = -k.v[3] * z[2];
z[4] = -k.v[0] + k.v[3] + k.v[5];
z[5] = k.v[4] * z[4];
z[1] = z[1] + z[3] + z[5];
z[3] = dv[0] + -dv[2];
z[3] = k.v[3] * z[3];
z[5] = dv[2] + -dv[5];
z[5] = k.v[1] * z[5];
z[0] = -dv[1] * z[0];
z[2] = -dv[3] * z[2];
z[6] = -dv[0] + dv[3] + dv[5];
z[6] = k.v[4] * z[6];
z[4] = dv[4] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T ds = get_sqrt_derivatives(200, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 171: {
    std::array<T,2> a;

    {
T z[9];
z[0] = k.v[3] + k.v[4];
z[0] = 3 * k.v[0] + -k.v[2] + -k.v[5] + 2 * z[0];
z[1] = 2 * k.v[1];
z[2] = z[0] + -z[1];
z[2] = k.v[1] * z[2];
z[3] = -k.v[0] + k.v[5];
z[3] = k.v[0] * z[3];
z[4] = 2 * k.v[0];
z[5] = -k.v[5] + z[4];
z[6] = -k.v[4] * z[5];
z[4] = -k.v[2] + k.v[4] + z[4];
z[7] = -k.v[3] * z[4];
z[2] = z[2] + z[3] + z[6] + z[7];
z[3] = -k.v[3] + z[1] + -z[5];
z[3] = dv[4] * z[3];
z[1] = z[1] + -z[4];
z[1] = dv[3] * z[1];
z[4] = dv[5] * k.v[0];
z[5] = -dv[0] * z[5];
z[6] = 2 * dv[0];
z[7] = dv[5] + -z[6];
z[7] = k.v[4] * z[7];
z[6] = dv[2] + -z[6];
z[6] = k.v[3] * z[6];
z[8] = 3 * dv[0] + -dv[2] + -dv[5];
z[8] = k.v[1] * z[8];
z[0] = -4 * k.v[1] + z[0];
z[0] = dv[1] * z[0];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
a = {z[2], z[0]};
}


    T ds = get_sqrt_derivatives(202, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 172: {
    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[1] + k.v[2];
z[0] = -k.v[0] + -k.v[3] + -k.v[5] + 2 * z[0];
z[1] = 2 * k.v[4];
z[2] = z[0] + -z[1];
z[2] = k.v[4] * z[2];
z[3] = k.v[0] + -k.v[2];
z[4] = -k.v[3] * z[3];
z[5] = -k.v[2] + k.v[5];
z[6] = k.v[1] * z[5];
z[2] = z[2] + z[4] + z[6];
z[4] = z[1] + z[5];
z[4] = dv[1] * z[4];
z[3] = -k.v[4] + -z[3];
z[3] = dv[3] * z[3];
z[1] = -k.v[1] + k.v[3] + z[1];
z[1] = dv[2] * z[1];
z[5] = k.v[1] + -k.v[4];
z[5] = dv[5] * z[5];
z[6] = -k.v[3] + -k.v[4];
z[6] = dv[0] * z[6];
z[0] = -4 * k.v[4] + z[0];
z[0] = dv[4] * z[0];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T ds = get_sqrt_derivatives(200, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 173: {
    std::array<T,2> a;

    {
T z[8];
z[0] = k.v[4] + k.v[5];
z[0] = -k.v[0] + -k.v[1] + -k.v[3] + 2 * z[0];
z[1] = 2 * k.v[2];
z[2] = z[0] + -z[1];
z[2] = k.v[2] * z[2];
z[3] = k.v[1] + -k.v[4];
z[4] = k.v[5] * z[3];
z[5] = -k.v[0] + k.v[4];
z[6] = k.v[3] * z[5];
z[2] = z[2] + z[4] + z[6];
z[3] = z[1] + z[3];
z[3] = dv[5] * z[3];
z[4] = -dv[0] * k.v[3];
z[5] = dv[3] * z[5];
z[6] = dv[1] * k.v[5];
z[7] = -dv[0] + -dv[1] + -dv[3];
z[7] = k.v[2] * z[7];
z[1] = k.v[3] + -k.v[5] + z[1];
z[1] = dv[4] * z[1];
z[0] = -4 * k.v[2] + z[0];
z[0] = dv[2] * z[0];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7];
a = {z[2], z[0]};
}


    T ds = get_sqrt_derivatives(203, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 174: {
    std::array<T,2> a;

    {
T z[9];
z[0] = k.v[2] + k.v[3];
z[0] = 3 * k.v[0] + -k.v[1] + -k.v[4] + 2 * z[0];
z[1] = 2 * k.v[5];
z[2] = z[0] + -z[1];
z[2] = k.v[5] * z[2];
z[3] = prod_pow(k.v[0], 2);
z[4] = 2 * k.v[0];
z[5] = -k.v[4] + z[4];
z[5] = -k.v[3] * z[5];
z[6] = k.v[3] + z[4];
z[6] = -k.v[2] * z[6];
z[7] = k.v[0] + k.v[2];
z[8] = k.v[1] * z[7];
z[2] = z[2] + -z[3] + z[5] + z[6] + z[8];
z[1] = z[1] + -z[4];
z[3] = k.v[1] + -k.v[3] + z[1];
z[3] = dv[2] * z[3];
z[1] = -k.v[2] + k.v[4] + z[1];
z[1] = dv[3] * z[1];
z[4] = -k.v[5] + z[7];
z[4] = dv[1] * z[4];
z[0] = -4 * k.v[5] + z[0];
z[0] = dv[5] * z[0];
z[5] = k.v[3] + -k.v[5];
z[5] = dv[4] * z[5];
z[6] = -k.v[3] + -z[7];
z[6] = k.v[1] + 3 * k.v[5] + 2 * z[6];
z[6] = dv[0] * z[6];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T ds = get_sqrt_derivatives(202, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 175: {
    std::array<T,2> a;

    {
T z[8];
z[0] = k.v[1] + -k.v[4];
z[1] = -k.v[5] * z[0];
z[2] = -k.v[0] + k.v[4];
z[3] = -k.v[3] * z[2];
z[4] = -k.v[0] + k.v[1] + k.v[3];
z[5] = k.v[2] * z[4];
z[1] = z[1] + z[3] + z[5];
z[2] = -dv[3] * z[2];
z[3] = -dv[1] + dv[4];
z[3] = k.v[5] * z[3];
z[5] = -dv[4] * k.v[3];
z[6] = dv[1] + dv[3];
z[6] = k.v[2] * z[6];
z[7] = -k.v[2] + k.v[3];
z[7] = dv[0] * z[7];
z[4] = dv[2] * z[4];
z[0] = -dv[5] * z[0];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
a = {z[1], z[0]};
}


    T ds = get_sqrt_derivatives(203, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 176: {
    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[0] + k.v[5];
z[1] = -k.v[4] * z[0];
z[2] = -k.v[2] + k.v[4] + 2 * z[0];
z[3] = -k.v[3] * z[2];
z[4] = -k.v[2] + z[0];
z[5] = k.v[1] * z[4];
z[1] = z[1] + z[3] + z[5];
z[3] = dv[1] * z[4];
z[0] = -dv[4] * z[0];
z[4] = -dv[0] + dv[5];
z[5] = -k.v[4] * z[4];
z[2] = -dv[3] * z[2];
z[6] = dv[2] + -dv[4] + -2 * z[4];
z[6] = k.v[3] * z[6];
z[4] = -dv[2] + z[4];
z[4] = k.v[1] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T ds = get_sqrt_derivatives(199, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 177: {
    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[0] + k.v[5];
z[1] = k.v[0] + -k.v[1];
z[0] = z[0] * z[1];
z[2] = k.v[3] + -k.v[5] + 2 * z[1];
z[3] = k.v[4] * z[2];
z[4] = -k.v[1] + k.v[3];
z[5] = -k.v[2] * z[4];
z[0] = z[0] + z[3] + z[5];
z[2] = dv[4] * z[2];
z[1] = -k.v[4] + z[1];
z[1] = dv[5] * z[1];
z[3] = -k.v[2] + k.v[4];
z[3] = dv[3] * z[3];
z[5] = 2 * k.v[4] + k.v[5];
z[6] = k.v[0] + k.v[2] + -z[5];
z[6] = dv[1] * z[6];
z[5] = -2 * k.v[0] + k.v[1] + z[5];
z[5] = dv[0] * z[5];
z[4] = -dv[2] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T ds = get_sqrt_derivatives(202, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 178: {
    std::array<T,2> a;

    {
T z[6];
z[0] = k.v[0] + -k.v[5];
z[1] = 2 * k.v[4];
z[2] = z[0] + -z[1];
z[3] = -k.v[3] + z[2];
z[3] = k.v[4] * z[3];
z[2] = k.v[2] + z[2];
z[4] = -k.v[1] * z[2];
z[1] = k.v[3] + z[1];
z[5] = k.v[2] * z[1];
z[3] = z[3] + z[4] + z[5];
z[4] = -dv[0] + dv[5];
z[5] = k.v[1] + -k.v[4];
z[4] = z[4] * z[5];
z[5] = k.v[1] + k.v[2];
z[0] = -k.v[3] + -4 * k.v[4] + z[0] + 2 * z[5];
z[0] = dv[4] * z[0];
z[2] = -dv[1] * z[2];
z[1] = -k.v[1] + z[1];
z[1] = dv[2] * z[1];
z[5] = k.v[2] + -k.v[4];
z[5] = dv[3] * z[5];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5];
a = {z[3], z[0]};
}


    T ds = get_sqrt_derivatives(199, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 179: {
    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[0] + -k.v[1];
z[1] = k.v[2] * z[0];
z[2] = k.v[1] + 2 * k.v[2] + -k.v[4];
z[3] = -k.v[5] * z[2];
z[4] = k.v[0] + k.v[2] + -k.v[4];
z[5] = k.v[3] * z[4];
z[1] = z[1] + z[3] + z[5];
z[0] = dv[2] * z[0];
z[3] = dv[0] + -dv[1];
z[3] = k.v[2] * z[3];
z[5] = -dv[1] + -2 * dv[2] + dv[4];
z[5] = k.v[5] * z[5];
z[4] = dv[3] * z[4];
z[6] = dv[0] + dv[2] + -dv[4];
z[6] = k.v[3] * z[6];
z[2] = -dv[5] * z[2];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T ds = get_sqrt_derivatives(203, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 180: {
    std::array<T,2> a;

    {
T z[6];
z[0] = k.v[0] + -k.v[1];
z[1] = 2 * k.v[5];
z[2] = z[0] + -z[1];
z[3] = -k.v[4] + z[2];
z[3] = k.v[5] * z[3];
z[2] = k.v[3] + z[2];
z[4] = -k.v[2] * z[2];
z[1] = k.v[4] + z[1];
z[5] = k.v[3] * z[1];
z[3] = z[3] + z[4] + z[5];
z[4] = -dv[0] + dv[1];
z[5] = k.v[2] + -k.v[5];
z[4] = z[4] * z[5];
z[5] = k.v[2] + k.v[3];
z[0] = -k.v[4] + -4 * k.v[5] + z[0] + 2 * z[5];
z[0] = dv[5] * z[0];
z[2] = -dv[2] * z[2];
z[1] = -k.v[2] + z[1];
z[1] = dv[3] * z[1];
z[5] = k.v[3] + -k.v[5];
z[5] = dv[4] * z[5];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5];
a = {z[3], z[0]};
}


    T ds = get_sqrt_derivatives(201, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 181: {
    std::array<T,2> a;

    {
T z[8];
z[0] = k.v[1] + -k.v[4];
z[1] = k.v[5] * z[0];
z[2] = -k.v[0] + k.v[4];
z[3] = k.v[3] * z[2];
z[4] = k.v[0] + -k.v[1] + k.v[3];
z[5] = -k.v[2] * z[4];
z[1] = z[1] + z[3] + z[5];
z[2] = dv[3] * z[2];
z[3] = dv[1] + -dv[4];
z[3] = k.v[5] * z[3];
z[5] = dv[4] * k.v[3];
z[6] = dv[1] + -dv[3];
z[6] = k.v[2] * z[6];
z[7] = -k.v[2] + -k.v[3];
z[7] = dv[0] * z[7];
z[4] = -dv[2] * z[4];
z[0] = dv[5] * z[0];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
a = {z[1], z[0]};
}


    T ds = get_sqrt_derivatives(203, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 182: {
    std::array<T,2> a;

    {
T z[6];
z[0] = -k.v[1] * k.v[2];
z[1] = k.v[2] + -k.v[4];
z[2] = k.v[3] * z[1];
z[3] = k.v[1] + -2 * k.v[3] + -k.v[4];
z[4] = k.v[5] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = dv[5] * z[3];
z[3] = -k.v[3] + -k.v[5];
z[3] = dv[4] * z[3];
z[4] = -k.v[1] + k.v[3];
z[4] = dv[2] * z[4];
z[1] = -2 * k.v[5] + z[1];
z[1] = dv[3] * z[1];
z[5] = -k.v[2] + k.v[5];
z[5] = dv[1] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
a = {z[0], z[1]};
}


    T ds = get_sqrt_derivatives(198, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 183: {
    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[3] * k.v[4];
z[1] = k.v[0] + -k.v[1];
z[2] = k.v[3] + z[1];
z[3] = k.v[2] * z[2];
z[1] = -k.v[4] + z[1];
z[4] = -k.v[5] * z[1];
z[0] = z[0] + z[3] + z[4];
z[3] = -dv[4] * k.v[3];
z[4] = -dv[3] * k.v[4];
z[1] = -dv[5] * z[1];
z[2] = dv[2] * z[2];
z[5] = -dv[0] + dv[1];
z[6] = dv[3] + -z[5];
z[6] = k.v[2] * z[6];
z[5] = dv[4] + z[5];
z[5] = k.v[5] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T ds = get_sqrt_derivatives(201, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 184: {
    std::array<T,2> a;

    {
T z[6];
z[0] = -k.v[2] * k.v[3];
z[1] = k.v[2] + -k.v[5];
z[2] = k.v[1] * z[1];
z[3] = k.v[3] + k.v[5];
z[4] = k.v[4] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = k.v[1] + -k.v[3];
z[2] = dv[2] * z[2];
z[1] = dv[1] * z[1];
z[3] = dv[4] * z[3];
z[4] = -k.v[2] + k.v[4];
z[4] = dv[3] * z[4];
z[5] = -k.v[1] + k.v[4];
z[5] = dv[5] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
a = {z[0], z[1]};
}


    T ds = get_sqrt_derivatives(198, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_1(a[0],T{},a[1],ds);
	
    return {coeffs[0], coeffs[1]};
}
case 188: {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[0] * k.v[3];
z[1] = dv[3] * k.v[0];
z[2] = dv[0] * k.v[3];
z[1] = z[1] + z[2];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(198, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_3(a[0], T{}, s1, a[1], ds2, ds1);

    return {coeffs[0]/k.W[197], coeffs[1]/k.W[197]};
}
case 189: {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[1] + -k.v[3] + -k.v[4];
z[1] = -k.v[0] * z[0];
z[2] = -dv[1] + dv[3] + dv[4];
z[2] = k.v[0] * z[2];
z[0] = -dv[0] * z[0];
z[0] = z[0] + z[2];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(199, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_3(a[0], T{}, s1, a[1], ds2, ds1);

    return {coeffs[0]/k.W[197], coeffs[1]/k.W[197]};
}
case 190: {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[0] * k.v[4];
z[1] = dv[4] * k.v[0];
z[2] = dv[0] * k.v[4];
z[1] = z[1] + z[2];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(200, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_3(a[0], T{}, s1, a[1], ds2, ds1);

    return {coeffs[0]/k.W[197], coeffs[1]/k.W[197]};
}
case 191: {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[2] + k.v[3] + -k.v[5];
z[1] = k.v[0] * z[0];
z[2] = dv[2] + dv[3] + -dv[5];
z[2] = k.v[0] * z[2];
z[0] = dv[0] * z[0];
z[0] = z[0] + z[2];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(201, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_3(a[0], T{}, s1, a[1], ds2, ds1);

    return {coeffs[0]/k.W[197], coeffs[1]/k.W[197]};
}
case 192: {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[1] + -k.v[3] + k.v[5];
z[1] = k.v[0] + -z[0];
z[1] = k.v[0] * z[1];
z[2] = -dv[1] + dv[3] + -dv[5];
z[2] = k.v[0] * z[2];
z[0] = 2 * k.v[0] + -z[0];
z[0] = dv[0] * z[0];
z[0] = z[0] + z[2];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(202, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_3(a[0], T{}, s1, a[1], ds2, ds1);

    return {coeffs[0]/k.W[197], coeffs[1]/k.W[197]};
}
case 193: {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[0] * k.v[2];
z[1] = dv[2] * k.v[0];
z[2] = dv[0] * k.v[2];
z[1] = z[1] + z[2];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(203, k.v, dv);

	auto coeffs = get_dlog_sqrt_series_3(a[0], T{}, s1, a[1], ds2, ds1);

    return {coeffs[0]/k.W[197], coeffs[1]/k.W[197]};
}
	}
	throw std::runtime_error("Not implemented");
}

template std::array<std::complex<double>,2> dlog_W_Sigma5_series (int wi, const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
template std::array<std::complex<dd_real>,2> dlog_W_Sigma5_series (int wi, const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
template std::array<std::complex<qd_real>,2> dlog_W_Sigma5_series (int wi, const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED








} // namespace m1_set
} // namespace PentagonFunctions
