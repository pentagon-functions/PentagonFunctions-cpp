#include "SpDLogQ.h"

#include "Constants.h"
#include "Kin.h"
#include "PolyLog.h"

#include "DLog.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> SpDLogQ_W_72 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl, const std::array<T,10>& dlr) {
	using std::abs;
	T x = -(k.v[0] * k.v[3]) + k.v[1] * (k.v[2] + k.v[3]);
     if (abs(x) < detail::SpDLogZeroThreshold<T>) {
		T dx = -((dl[0] + dl[3]) * k.v[0] * k.v[3]) + k.v[1] * ((dl[1] + dl[2]) * k.v[2] + (dl[1] + dl[3]) * k.v[3]);
		std::array<T,2> cre;
		std::array<T,2> cim;

cre[0] = (T(-2) * (-(rlog(k.v[1]) * k.v[0] * k.v[1] * k.v[2]) + -(rlog(T(-1) * k.v[5]) * k.v[0] * k.v[1] * k.v[2]) + -(rlog((T(-1) * k.v[0] * k.v[2]) / (k.v[0] * k.v[5] + T(-1) * k.v[1] * k.v[5]) + T(1)) * k.v[1] * (k.v[1] * k.v[5] + k.v[0] * (k.v[2] + T(-1) * k.v[5]))) + rlog(-(k.v[1] * k.v[2]) / (-k.v[1] + k.v[0])) * k.v[0] * k.v[1] * k.v[2] + rlog(k.v[0]) * k.v[0] * k.v[1] * k.v[2] + rlog(((-k.v[1] + k.v[0]) * k.v[5]) / (k.v[0] * k.v[2])) * k.v[0] * (-k.v[1] + k.v[0]) * k.v[5] + rlog((k.v[0] * k.v[2]) / (-k.v[1] + k.v[0]) + -k.v[5]) * k.v[0] * (k.v[0] * (-k.v[5] + k.v[2]) + k.v[1] * k.v[5]) + rlog(k.v[1]) * k.v[0] * (k.v[0] * (-k.v[5] + k.v[2]) + k.v[1] * k.v[5]) + rlog(-(k.v[1] * k.v[2]) / (-k.v[1] + k.v[0])) * k.v[0] * (-(k.v[1] * k.v[5]) + k.v[0] * (-k.v[2] + k.v[5])) + rlog(k.v[0]) * k.v[0] * (-(k.v[1] * k.v[5]) + k.v[0] * (-k.v[2] + k.v[5])))) / (k.v[0] * k.v[1] * k.v[2] * (k.v[0] * (-k.v[5] + k.v[2]) + k.v[1] * k.v[5]));
cre[1] = rlog(-(k.v[0] * k.v[2]) / (-(k.v[1] * k.v[5]) + k.v[0] * k.v[5]) + T(1)) / (prod_pow(k.v[0], 2) * prod_pow(k.v[2], 2)) + rlog(k.v[0]) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2)) + -rlog(k.v[1]) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2)) + (rlog(-(k.v[1] * k.v[2]) / (-k.v[1] + k.v[0])) + T(-1)) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2)) + -rlog(k.v[0]) / prod_pow(k.v[0] * (-k.v[5] + k.v[2]) + k.v[1] * k.v[5], 2) + rlog(k.v[1]) / prod_pow(k.v[0] * (-k.v[5] + k.v[2]) + k.v[1] * k.v[5], 2) + -rlog((T(-1) * k.v[1] * k.v[2]) / (k.v[0] + T(-1) * k.v[1])) / prod_pow(k.v[0] * (-k.v[5] + k.v[2]) + k.v[1] * k.v[5], 2) + rlog(-k.v[5]) / prod_pow(k.v[0] * (-k.v[5] + k.v[2]) + k.v[1] * k.v[5], 2) + T(1) / (-(prod_pow(k.v[0], 2) * prod_pow(k.v[2], 2)) + -(k.v[0] * k.v[1] * k.v[2] * k.v[5]) + prod_pow(k.v[0], 2) * k.v[2] * k.v[5]) + ((T(2) * k.v[1] * k.v[2]) / (k.v[0] * (-k.v[5] + k.v[2]) + k.v[1] * k.v[5]) + -rlog((k.v[0] * k.v[2]) / (k.v[0] + T(-1) * k.v[1]) + T(-1) * k.v[5])) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2)) + ((-k.v[1] + k.v[0]) * k.v[5] * (-(k.v[1] * k.v[5]) + k.v[0] * (-k.v[2] + k.v[5]) + rlog(((-k.v[1] + k.v[0]) * k.v[5]) / (k.v[0] * k.v[2])) * (-(k.v[1] * k.v[5]) + k.v[0] * (T(-2) * k.v[2] + k.v[5])))) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2) * prod_pow(k.v[0] * (-k.v[5] + k.v[2]) + k.v[1] * k.v[5], 2));
cim[0] = T(0);
cim[1] = T(0);

			
		cre[1] *= x;
		cre[0] += cre[1];
		cre[0] *= dx;

		cim[1] *= x;
		cim[0] += cim[1];
		cim[0] *= dx;

         return {cre[0],cim[0]};
	}

	std::array<T,9> ccc;

	ccc[0] = m1_set::bc<T>[0];
ccc[1] = dlog_W72(k,dl);
ccc[2] = f_1_1(k);
ccc[3] = f_1_2(k);
ccc[4] = f_1_3(k);
ccc[5] = f_1_4(k);
ccc[6] = f_1_8(k);
ccc[7] = f_2_13(k);
ccc[8] = f_2_17(k);


	{
T z[6];
z[0] = ccc[7] + -ccc[8];
z[1] = 2 * ccc[6];
z[2] = -ccc[4] + -z[1];
z[2] = ccc[4] * z[2];
z[1] = ccc[5] + -z[1];
z[1] = ccc[5] * z[1];
z[3] = ccc[4] + ccc[6];
z[4] = -ccc[2] + 2 * z[3];
z[4] = ccc[2] * z[4];
z[5] = prod_pow(ccc[0], 2);
z[3] = -ccc[2] + z[3];
z[3] = -ccc[3] + 2 * z[3];
z[3] = ccc[3] * z[3];
z[0] = 2 * z[0] + z[1] + z[2] + z[3] + z[4] + (T(-1) / T(3)) * z[5];
z[0] = ccc[1] * z[0];
return {z[0], 0};
}
;
}

template std::complex<double> SpDLogQ_W_72 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl, const std::array<double,10>& dlr);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> SpDLogQ_W_72 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl, const std::array<dd_real,10>& dlr);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> SpDLogQ_W_72 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl, const std::array<qd_real,10>& dlr);
#endif


template <typename T> std::complex<T> SpDLogQ_W_74 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl, const std::array<T,10>& dlr) {
	using std::abs;
	T x = k.v[1] * (-k.v[3] + -k.v[4] + k.v[1] + k.v[2]) + k.v[0] * (-k.v[1] + k.v[3] + k.v[4]);
     if (abs(x) < detail::SpDLogZeroThreshold<T>) {
		T dx = dl[2] * k.v[1] * k.v[2] + dl[3] * k.v[0] * k.v[3] + -(dl[3] * k.v[1] * k.v[3]) + dl[1] * k.v[1] * (-k.v[0] + -k.v[3] + -k.v[4] + T(2) * k.v[1] + k.v[2]) + dl[4] * (-k.v[1] + k.v[0]) * k.v[4] + dl[0] * k.v[0] * (-k.v[1] + k.v[3] + k.v[4]);
		std::array<T,2> cre;
		std::array<T,2> cim;

cre[0] = (T(2) * (-(rlog(k.v[1]) * k.v[0] * k.v[1] * k.v[2]) + -(rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) * k.v[0] * k.v[1] * k.v[2]) + -(rlog((T(-1) * (prod_pow(k.v[0], 2) + T(-1) * k.v[0] * (k.v[1] + k.v[4] + k.v[5]) + k.v[1] * (T(-1) * k.v[2] + k.v[4] + k.v[5]))) / (k.v[0] + T(-1) * k.v[1])) * k.v[0] * (prod_pow(k.v[0], 2) + T(-1) * k.v[0] * (k.v[1] + k.v[4] + k.v[5]) + k.v[1] * (T(-1) * k.v[2] + k.v[4] + k.v[5]))) + -(rlog(k.v[1]) * k.v[0] * (prod_pow(k.v[0], 2) + T(-1) * k.v[0] * (k.v[1] + k.v[4] + k.v[5]) + k.v[1] * (T(-1) * k.v[2] + k.v[4] + k.v[5]))) + rlog((k.v[1] * k.v[2]) / (-k.v[0] + k.v[1])) * k.v[0] * k.v[1] * k.v[2] + rlog(k.v[0]) * k.v[0] * k.v[1] * k.v[2] + rlog(((-k.v[1] + k.v[0]) * (-k.v[4] + -k.v[5] + k.v[0] + k.v[2])) / (k.v[0] * k.v[2])) * k.v[0] * (-k.v[1] + k.v[0]) * (-k.v[4] + -k.v[5] + k.v[0] + k.v[2]) + rlog((k.v[1] * k.v[2]) / (-k.v[0] + k.v[1])) * k.v[0] * (-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5])) + rlog(k.v[0]) * k.v[0] * (-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5])) + rlog((k.v[0] * k.v[2]) / ((-k.v[1] + k.v[0]) * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1)) * k.v[1] * (-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5])))) / (k.v[0] * k.v[1] * k.v[2] * (-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5])));
cre[1] = rlog((k.v[0] * k.v[2]) / ((-k.v[1] + k.v[0]) * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1)) / (prod_pow(k.v[0], 2) * prod_pow(k.v[2], 2)) + rlog(k.v[0]) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2)) + -rlog(k.v[1]) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2)) + (rlog((k.v[1] * k.v[2]) / (-k.v[0] + k.v[1])) + T(-1)) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2)) + -rlog(k.v[0]) / prod_pow(-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5]), 2) + rlog(k.v[1]) / prod_pow(-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5]), 2) + -rlog((k.v[1] * k.v[2]) / (T(-1) * k.v[0] + k.v[1])) / prod_pow(-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5]), 2) + rlog(-k.v[0] + -k.v[2] + k.v[4] + k.v[5]) / prod_pow(-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5]), 2) + -(((k.v[0] * k.v[2]) / ((k.v[0] + T(-1) * k.v[1]) * (k.v[0] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5])) + rlog(((k.v[0] + T(-1) * k.v[1]) * (k.v[0] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5])) / (k.v[0] * k.v[2])) + T(-1)) * prod_pow(k.v[0] + T(-1) * k.v[1], 2) * prod_pow(k.v[0] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5], 2)) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2) * prod_pow(-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5]), 2)) + 1 / (k.v[0] * k.v[2] * (-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5]))) + (rlog(((-k.v[1] + k.v[0]) * (-k.v[4] + -k.v[5] + k.v[0] + k.v[2])) / (k.v[0] * k.v[2])) * T(2) * (-k.v[1] + k.v[0]) * (-k.v[4] + -k.v[5] + k.v[0] + k.v[2])) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2) * (-(k.v[0] * (k.v[1] + k.v[4] + k.v[5])) + prod_pow(k.v[0], 2) + k.v[1] * (-k.v[2] + k.v[4] + k.v[5]))) + -((T(2) * k.v[1] * k.v[2]) / (prod_pow(k.v[0], 2) + T(-1) * k.v[0] * (k.v[1] + k.v[4] + k.v[5]) + k.v[1] * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + rlog((T(-1) * (prod_pow(k.v[0], 2) + T(-1) * k.v[0] * (k.v[1] + k.v[4] + k.v[5]) + k.v[1] * (T(-1) * k.v[2] + k.v[4] + k.v[5]))) / (k.v[0] + T(-1) * k.v[1]))) / (prod_pow(k.v[1], 2) * prod_pow(k.v[2], 2));
cim[0] = T(0);
cim[1] = T(0);

			
		cre[1] *= x;
		cre[0] += cre[1];
		cre[0] *= dx;

		cim[1] *= x;
		cim[0] += cim[1];
		cim[0] *= dx;

         return {cre[0],cim[0]};
	}

	std::array<T,9> ccc;

	ccc[0] = m1_set::bc<T>[0];
ccc[1] = dlog_W74(k,dl);
ccc[2] = f_1_1(k);
ccc[3] = f_1_3(k);
ccc[4] = f_1_7(k);
ccc[5] = f_1_9(k);
ccc[6] = f_1_11(k);
ccc[7] = f_2_14(k);
ccc[8] = f_2_18(k);


	{
T z[6];
z[0] = ccc[7] + -ccc[8];
z[1] = prod_pow(ccc[0], 2);
z[2] = 2 * ccc[4];
z[3] = ccc[6] + -z[2];
z[3] = ccc[6] * z[3];
z[2] = -ccc[3] + -z[2];
z[2] = ccc[3] * z[2];
z[4] = ccc[3] + ccc[4];
z[5] = -ccc[2] + 2 * z[4];
z[5] = ccc[2] * z[5];
z[4] = -ccc[2] + z[4];
z[4] = -ccc[5] + 2 * z[4];
z[4] = ccc[5] * z[4];
z[0] = 2 * z[0] + (T(-1) / T(3)) * z[1] + z[2] + z[3] + z[4] + z[5];
z[0] = ccc[1] * z[0];
return {z[0], 0};
}
;
}

template std::complex<double> SpDLogQ_W_74 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl, const std::array<double,10>& dlr);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> SpDLogQ_W_74 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl, const std::array<dd_real,10>& dlr);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> SpDLogQ_W_74 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl, const std::array<qd_real,10>& dlr);
#endif


template <typename T> std::complex<T> SpDLogQ_W_78 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl, const std::array<T,10>& dlr) {
	using std::abs;
	T x = k.v[0] * k.v[2] + (-k.v[4] + k.v[1] + k.v[2]) * (-k.v[5] + k.v[3]);
     if (abs(x) < detail::SpDLogZeroThreshold<T>) {
		T dx = dl[0] * k.v[0] * k.v[2] + (-((dl[3] + dl[4]) * k.v[4]) + dl[1] * k.v[1] + dl[3] * (k.v[1] + k.v[2])) * k.v[3] + dl[2] * k.v[2] * (-k.v[5] + k.v[0] + k.v[3]) + -((dl[1] * k.v[1] + dl[5] * (k.v[1] + k.v[2]) + (dl[4] + dl[5]) * T(-1) * k.v[4]) * k.v[5]);
		std::array<T,2> cre;
		std::array<T,2> cim;

cre[0] = (-rlog((T(-1) * k.v[0] * k.v[2]) / (k.v[1] + k.v[2] + T(-1) * k.v[4]) + k.v[2]) / (k.v[2] * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2])) + rlog(-k.v[5]) / (-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * k.v[5]) + k.v[0] * k.v[2]) + rlog((k.v[0] * k.v[2]) / (-k.v[4] + k.v[1] + k.v[2]) + -k.v[5]) / (k.v[2] * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2])) + rlog(-(k.v[0] * k.v[2]) / ((-k.v[4] + k.v[1] + k.v[2]) * k.v[5]) + T(1)) / (k.v[0] * k.v[2]) + rlog(k.v[0]) / (k.v[2] * (-k.v[1] + -k.v[2] + k.v[0] + k.v[4])) + rlog(k.v[0]) / (-(k.v[0] * k.v[2]) + (-k.v[4] + k.v[1] + k.v[2]) * k.v[5]) + rlog(-(k.v[0] * k.v[2]) / (-k.v[4] + k.v[1] + k.v[2]) + k.v[2]) / (-(k.v[0] * k.v[2]) + (-k.v[4] + k.v[1] + k.v[2]) * k.v[5]) + rlog(-k.v[1] + -k.v[2] + k.v[0] + k.v[4]) / (k.v[2] * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2])) + rlog(-k.v[1] + -k.v[2] + k.v[0] + k.v[4]) / (-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * k.v[5]) + k.v[0] * k.v[2]) + (rlog(((-k.v[4] + k.v[1] + k.v[2]) * k.v[5]) / (k.v[0] * k.v[2])) * (-k.v[4] + k.v[1] + k.v[2]) * k.v[5]) / (k.v[2] * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2]) * (-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * k.v[5]) + k.v[0] * k.v[2]))) * T(2);
cre[1] = rlog(-(k.v[0] * k.v[2]) / ((-k.v[4] + k.v[1] + k.v[2]) * k.v[5]) + T(1)) / (prod_pow(k.v[0], 2) * prod_pow(k.v[2], 2)) + rlog(k.v[0]) / (prod_pow(k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2)) + (rlog(-(k.v[0] * k.v[2]) / (-k.v[4] + k.v[1] + k.v[2]) + k.v[2]) + T(-1)) / (prod_pow(k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2)) + -rlog(k.v[0] + T(-1) * k.v[1] + T(-1) * k.v[2] + k.v[4]) / (prod_pow(k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2)) + -rlog((k.v[0] * k.v[2]) / (k.v[1] + k.v[2] + T(-1) * k.v[4]) + T(-1) * k.v[5]) / (prod_pow(k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2)) + -rlog(k.v[0]) / prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * k.v[5]) + k.v[0] * k.v[2], 2) + rlog(-k.v[1] + -k.v[2] + k.v[0] + k.v[4]) / prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * k.v[5]) + k.v[0] * k.v[2], 2) + rlog(-k.v[5]) / prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * k.v[5]) + k.v[0] * k.v[2], 2) + T(-1) / (k.v[0] * k.v[2] * (-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * k.v[5]) + k.v[0] * k.v[2])) + ((-k.v[4] + k.v[1] + k.v[2]) * k.v[5] * (-(k.v[0] * k.v[2]) + (-k.v[4] + k.v[1] + k.v[2]) * k.v[5] + rlog(((-k.v[4] + k.v[1] + k.v[2]) * k.v[5]) / (k.v[0] * k.v[2])) * (T(-2) * k.v[0] * k.v[2] + (-k.v[4] + k.v[1] + k.v[2]) * k.v[5]))) / (prod_pow(k.v[2], 2) * prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * k.v[5]) + k.v[0] * k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2)) + ((T(-2) * k.v[0] * k.v[2] + T(2) * (-k.v[4] + k.v[1] + k.v[2]) * k.v[5]) / (k.v[2] * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2])) + -rlog((T(-1) * k.v[0] * k.v[2]) / (k.v[1] + k.v[2] + T(-1) * k.v[4]) + k.v[2])) / prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * k.v[5]) + k.v[0] * k.v[2], 2);
cim[0] = T(0);
cim[1] = T(0);

			
		cre[1] *= x;
		cre[0] += cre[1];
		cre[0] *= dx;

		cim[1] *= x;
		cim[0] += cim[1];
		cim[0] *= dx;

         return {cre[0],cim[0]};
	}

	std::array<T,9> ccc;

	ccc[0] = m1_set::bc<T>[0];
ccc[1] = dlog_W78(k,dl);
ccc[2] = f_1_1(k);
ccc[3] = f_1_2(k);
ccc[4] = f_1_4(k);
ccc[5] = f_1_8(k);
ccc[6] = f_1_10(k);
ccc[7] = f_2_16(k);
ccc[8] = f_2_19(k);


	{
T z[6];
z[0] = -ccc[7] + ccc[8];
z[1] = prod_pow(ccc[0], 2);
z[2] = 2 * ccc[3];
z[3] = -ccc[6] + -z[2];
z[3] = ccc[6] * z[3];
z[4] = ccc[3] + ccc[6];
z[5] = -ccc[5] + 2 * z[4];
z[5] = ccc[5] * z[5];
z[4] = -ccc[5] + z[4];
z[4] = -ccc[2] + 2 * z[4];
z[4] = ccc[2] * z[4];
z[2] = ccc[4] + -z[2];
z[2] = ccc[4] * z[2];
z[0] = -2 * z[0] + (T(-1) / T(3)) * z[1] + z[2] + z[3] + z[4] + z[5];
z[0] = ccc[1] * z[0];
return {z[0], 0};
}
;
}

template std::complex<double> SpDLogQ_W_78 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl, const std::array<double,10>& dlr);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> SpDLogQ_W_78 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl, const std::array<dd_real,10>& dlr);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> SpDLogQ_W_78 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl, const std::array<qd_real,10>& dlr);
#endif


template <typename T> std::complex<T> SpDLogQ_W_80 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl, const std::array<T,10>& dlr) {
	using std::abs;
	T x = k.v[0] * (-k.v[1] + k.v[4]) + (-k.v[4] + k.v[1] + k.v[2]) * (-k.v[2] + -k.v[3] + k.v[1] + k.v[5]);
     if (abs(x) < detail::SpDLogZeroThreshold<T>) {
		T dx = dl[2] * T(-2) * prod_pow(k.v[2], 2) + -(dl[2] * k.v[2] * k.v[3]) + -(dl[3] * k.v[2] * k.v[3]) + dl[4] * k.v[0] * k.v[4] + dl[2] * k.v[2] * k.v[4] + dl[4] * k.v[2] * k.v[4] + dl[3] * k.v[3] * k.v[4] + dl[4] * k.v[3] * k.v[4] + dl[0] * k.v[0] * (-k.v[1] + k.v[4]) + (-((dl[4] + dl[5]) * k.v[4]) + (dl[2] + dl[5]) * k.v[2]) * k.v[5] + dl[1] * k.v[1] * (-k.v[0] + -k.v[3] + -k.v[4] + T(2) * k.v[1] + k.v[5]) + -(k.v[1] * (dl[3] * k.v[3] + dl[4] * k.v[4] + dl[5] * T(-1) * k.v[5]));
		std::array<T,2> cre;
		std::array<T,2> cim;

cre[0] = (-rlog((T(-1) * k.v[0] * k.v[2]) / (k.v[1] + k.v[2] + T(-1) * k.v[4]) + k.v[2]) / (k.v[2] * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2])) + -rlog(k.v[0] + T(-1) * k.v[1] + T(-1) * k.v[2] + k.v[4]) / (-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1])) + -rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) / (-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1])) + -(rlog(((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (k.v[0] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5])) / (k.v[0] * k.v[2])) * (k.v[1] + k.v[2] + T(-1) * k.v[4]) * (k.v[0] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5])) / ((-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1])) * k.v[2] * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2])) + rlog(-(k.v[0] * k.v[2]) / ((-k.v[4] + -k.v[5] + k.v[0] + k.v[2]) * (-k.v[4] + k.v[1] + k.v[2])) + T(1)) / (k.v[0] * k.v[2]) + rlog(k.v[0]) / (-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1])) + rlog(k.v[0]) / (k.v[2] * (-k.v[1] + -k.v[2] + k.v[0] + k.v[4])) + rlog(-(k.v[0] * k.v[2]) / (-k.v[4] + k.v[1] + k.v[2]) + k.v[2]) / (-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1])) + rlog(-k.v[1] + -k.v[2] + k.v[0] + k.v[4]) / (k.v[2] * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2])) + rlog(-k.v[2] + (k.v[2] / (-k.v[4] + k.v[1] + k.v[2]) + T(-1)) * k.v[0] + k.v[4] + k.v[5]) / (k.v[2] * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2]))) * T(2);
cre[1] = rlog(-(k.v[0] * k.v[2]) / ((-k.v[4] + -k.v[5] + k.v[0] + k.v[2]) * (-k.v[4] + k.v[1] + k.v[2])) + T(1)) / (prod_pow(k.v[0], 2) * prod_pow(k.v[2], 2)) + rlog(k.v[0]) / (prod_pow(k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2)) + (rlog(-(k.v[0] * k.v[2]) / (-k.v[4] + k.v[1] + k.v[2]) + k.v[2]) + T(-1)) / (prod_pow(k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2)) + -rlog(k.v[0] + T(-1) * k.v[1] + T(-1) * k.v[2] + k.v[4]) / (prod_pow(k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2)) + -rlog(k.v[0]) / prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1]), 2) + -rlog((T(-1) * k.v[0] * k.v[2]) / (k.v[1] + k.v[2] + T(-1) * k.v[4]) + k.v[2]) / prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1]), 2) + rlog(-k.v[1] + -k.v[2] + k.v[0] + k.v[4]) / prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1]), 2) + rlog(-k.v[0] + -k.v[2] + k.v[4] + k.v[5]) / prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1]), 2) + 1 / (k.v[0] * (-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1])) * k.v[2]) + (rlog(((-k.v[4] + -k.v[5] + k.v[0] + k.v[2]) * (-k.v[4] + k.v[1] + k.v[2])) / (k.v[0] * k.v[2])) * T(2) * (-k.v[4] + -k.v[5] + k.v[0] + k.v[2]) * (-k.v[4] + k.v[1] + k.v[2])) / ((-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1])) * prod_pow(k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2)) + (-rlog((k.v[2] / (k.v[1] + k.v[2] + T(-1) * k.v[4]) + T(-1)) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) / prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2) + (T(2) * k.v[2]) / ((-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1])) * (-k.v[0] + -k.v[4] + k.v[1] + k.v[2]))) / prod_pow(k.v[2], 2) + -(((k.v[0] * (T(-1) * k.v[1] + k.v[4]) + (k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) / ((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (k.v[0] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5])) + rlog(((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (k.v[0] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5])) / (k.v[0] * k.v[2]))) * prod_pow(k.v[1] + k.v[2] + T(-1) * k.v[4], 2) * prod_pow(k.v[0] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5], 2)) / (prod_pow(-((k.v[1] + k.v[2] + T(-1) * k.v[4]) * (T(-1) * k.v[2] + k.v[4] + k.v[5])) + k.v[0] * (-k.v[4] + k.v[1]), 2) * prod_pow(k.v[2], 2) * prod_pow(-k.v[1] + -k.v[2] + k.v[0] + k.v[4], 2));
cim[0] = T(0);
cim[1] = T(0);

			
		cre[1] *= x;
		cre[0] += cre[1];
		cre[0] *= dx;

		cim[1] *= x;
		cim[0] += cim[1];
		cim[0] *= dx;

         return {cre[0],cim[0]};
	}

	std::array<T,9> ccc;

	ccc[0] = m1_set::bc<T>[0];
ccc[1] = dlog_W80(k,dl);
ccc[2] = f_1_1(k);
ccc[3] = f_1_7(k);
ccc[4] = f_1_9(k);
ccc[5] = f_1_10(k);
ccc[6] = f_1_11(k);
ccc[7] = f_2_15(k);
ccc[8] = f_2_20(k);


	{
T z[6];
z[0] = ccc[7] + -ccc[8];
z[1] = prod_pow(ccc[0], 2);
z[2] = 2 * ccc[4];
z[3] = ccc[6] + -z[2];
z[3] = ccc[6] * z[3];
z[2] = -ccc[5] + -z[2];
z[2] = ccc[5] * z[2];
z[4] = ccc[4] + ccc[5];
z[5] = -ccc[3] + 2 * z[4];
z[5] = ccc[3] * z[5];
z[4] = -ccc[3] + z[4];
z[4] = -ccc[2] + 2 * z[4];
z[4] = ccc[2] * z[4];
z[0] = 2 * z[0] + (T(-1) / T(3)) * z[1] + z[2] + z[3] + z[4] + z[5];
z[0] = ccc[1] * z[0];
return {z[0], 0};
}
;
}

template std::complex<double> SpDLogQ_W_80 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl, const std::array<double,10>& dlr);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> SpDLogQ_W_80 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl, const std::array<dd_real,10>& dlr);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> SpDLogQ_W_80 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl, const std::array<qd_real,10>& dlr);
#endif


template <typename T> std::complex<T> SpDLogQ_W_82 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl, const std::array<T,10>& dlr) {
	using std::abs;
	T x = k.v[0] * k.v[3] + (-k.v[4] + -k.v[5] + k.v[2]) * (k.v[2] + k.v[3]);
     if (abs(x) < detail::SpDLogZeroThreshold<T>) {
		T dx = (dl[0] + dl[3]) * k.v[0] * k.v[3] + dl[3] * (-k.v[4] + -k.v[5] + k.v[2]) * k.v[3] + dl[2] * k.v[2] * (-k.v[4] + -k.v[5] + T(2) * k.v[2] + k.v[3]) + -((k.v[2] + k.v[3]) * (dl[4] * k.v[4] + dl[5] * k.v[5]));
		std::array<T,2> cre;
		std::array<T,2> cim;

cre[0] = (rlog((k.v[0] * k.v[2]) / (k.v[5] * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1)) * T(-2)) / (k.v[0] * k.v[2]) + ((-rlog(T(-1) * k.v[5]) + -rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) + rlog(k.v[0]) + rlog(k.v[2])) * T(2)) / (k.v[4] * k.v[5] + (-k.v[0] + k.v[5]) * (-k.v[2] + k.v[5]));
cre[1] = (-(k.v[0] * k.v[2] * ((rlog(k.v[0]) + rlog(k.v[2]) + rlog(T(-1) * k.v[5]) * T(-1) + rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) * T(-1)) * k.v[0] * k.v[2] + k.v[4] * k.v[5] + (T(-1) * k.v[0] + k.v[5]) * (T(-1) * k.v[2] + k.v[5]))) / prod_pow(-(k.v[4] * k.v[5]) + (-k.v[5] + k.v[0]) * (-k.v[2] + k.v[5]), 2) + rlog((k.v[0] * k.v[2]) / (k.v[5] * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1))) / (prod_pow(k.v[0], 2) * prod_pow(k.v[2], 2));
cim[0] = (m1_set::bc<T>[0] * T(4)) / (-(k.v[4] * k.v[5]) + (-k.v[5] + k.v[0]) * (-k.v[2] + k.v[5]));
cim[1] = (m1_set::bc<T>[0] * T(2)) / prod_pow(-(k.v[4] * k.v[5]) + (-k.v[5] + k.v[0]) * (-k.v[2] + k.v[5]), 2);

			
		cre[1] *= x;
		cre[0] += cre[1];
		cre[0] *= dx;

		cim[1] *= x;
		cim[0] += cim[1];
		cim[0] *= dx;

         return {cre[0],cim[0]};
	}

	std::array<T,10> ccc;

	ccc[0] = m1_set::bc<T>[0];
ccc[1] = dlog_W82(k,dl);
ccc[2] = f_1_1(k);
ccc[3] = f_1_4(k);
ccc[4] = f_1_5(k);
ccc[5] = f_1_8(k);
ccc[6] = f_1_11(k);
ccc[7] = f_2_13(k);
ccc[8] = f_2_22_im(k);
ccc[9] = f_2_22_re(k);


	{
T z[7];
z[0] = ccc[2] + ccc[4];
z[1] = -ccc[3] + z[0];
z[1] = ccc[5] * z[1];
z[1] = ccc[7] + -ccc[9] + z[1];
z[2] = prod_pow(ccc[4], 2);
z[3] = -ccc[2] + -2 * ccc[4];
z[3] = ccc[2] * z[3];
z[4] = prod_pow(ccc[3], 2);
z[0] = -ccc[5] + z[0];
z[5] = -ccc[6] + 2 * z[0];
z[5] = ccc[6] * z[5];
z[6] = prod_pow(ccc[0], 2);
z[1] = 2 * z[1] + -z[2] + z[3] + z[4] + z[5] + (T(-1) / T(3)) * z[6];
z[1] = ccc[1] * z[1];
z[0] = -ccc[6] + z[0];
z[0] = ccc[0] * z[0];
z[0] = -ccc[8] + 2 * z[0];
z[0] = 2 * ccc[1] * z[0];
return {z[1], z[0]};
}
;
}

template std::complex<double> SpDLogQ_W_82 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl, const std::array<double,10>& dlr);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> SpDLogQ_W_82 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl, const std::array<dd_real,10>& dlr);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> SpDLogQ_W_82 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl, const std::array<qd_real,10>& dlr);
#endif


template <typename T> std::complex<T> SpDLogQ_W_84 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl, const std::array<T,10>& dlr) {
	using std::abs;
	T x = -(k.v[0] * (k.v[2] + k.v[3] + T(-1) * k.v[5])) + (-k.v[5] + k.v[3]) * (-k.v[2] + k.v[4] + k.v[5]);
     if (abs(x) < detail::SpDLogZeroThreshold<T>) {
		T dx = k.v[3] * (-(dl[3] * (k.v[0] + k.v[2])) + (dl[3] + dl[4]) * k.v[4]) + -(dl[2] * k.v[2] * (k.v[0] + k.v[3] + T(-1) * k.v[5])) + -(dl[0] * k.v[0] * (k.v[2] + k.v[3] + T(-1) * k.v[5])) + (-(dl[4] * k.v[4]) + dl[3] * k.v[3] + dl[5] * (-k.v[4] + k.v[0] + k.v[2] + k.v[3])) * k.v[5] + dl[5] * T(-2) * prod_pow(k.v[5], 2);
		std::array<T,2> cre;
		std::array<T,2> cim;

cre[0] = (rlog((k.v[0] * k.v[2]) / (k.v[5] * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1)) * T(-2)) / (k.v[0] * k.v[2]) + ((-rlog(T(-1) * k.v[5]) + -rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) + rlog(k.v[0]) + rlog(k.v[2])) * T(2)) / (k.v[4] * k.v[5] + (-k.v[0] + k.v[5]) * (-k.v[2] + k.v[5]));
cre[1] = (-(k.v[0] * k.v[2] * ((rlog(k.v[0]) + rlog(k.v[2]) + rlog(T(-1) * k.v[5]) * T(-1) + rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) * T(-1)) * k.v[0] * k.v[2] + k.v[4] * k.v[5] + (T(-1) * k.v[0] + k.v[5]) * (T(-1) * k.v[2] + k.v[5]))) / prod_pow(-(k.v[4] * k.v[5]) + (-k.v[5] + k.v[0]) * (-k.v[2] + k.v[5]), 2) + rlog((k.v[0] * k.v[2]) / (k.v[5] * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1))) / (prod_pow(k.v[0], 2) * prod_pow(k.v[2], 2));
cim[0] = (m1_set::bc<T>[0] * T(4)) / (-(k.v[4] * k.v[5]) + (-k.v[5] + k.v[0]) * (-k.v[2] + k.v[5]));
cim[1] = (m1_set::bc<T>[0] * T(2)) / prod_pow(-(k.v[4] * k.v[5]) + (-k.v[5] + k.v[0]) * (-k.v[2] + k.v[5]), 2);

			
		cre[1] *= x;
		cre[0] += cre[1];
		cre[0] *= dx;

		cim[1] *= x;
		cim[0] += cim[1];
		cim[0] *= dx;

         return {cre[0],cim[0]};
	}

	std::array<T,10> ccc;

	ccc[0] = m1_set::bc<T>[0];
ccc[1] = dlog_W84(k,dl);
ccc[2] = f_1_1(k);
ccc[3] = f_1_2(k);
ccc[4] = f_1_4(k);
ccc[5] = f_1_5(k);
ccc[6] = f_1_11(k);
ccc[7] = f_2_16(k);
ccc[8] = f_2_22_im(k);
ccc[9] = f_2_22_re(k);


	{
T z[7];
z[0] = ccc[2] + ccc[5];
z[1] = -ccc[6] + z[0];
z[2] = -ccc[4] + z[1];
z[2] = ccc[3] * z[2];
z[2] = ccc[7] + -ccc[9] + z[2];
z[3] = prod_pow(ccc[2], 2);
z[4] = -2 * ccc[2] + -ccc[5];
z[4] = ccc[5] * z[4];
z[0] = -ccc[6] + 2 * z[0];
z[0] = ccc[6] * z[0];
z[5] = prod_pow(ccc[4], 2);
z[6] = prod_pow(ccc[0], 2);
z[0] = z[0] + 2 * z[2] + -z[3] + z[4] + z[5] + (T(-1) / T(3)) * z[6];
z[0] = ccc[1] * z[0];
z[1] = -ccc[3] + z[1];
z[1] = ccc[0] * z[1];
z[1] = -ccc[8] + 2 * z[1];
z[1] = 2 * ccc[1] * z[1];
return {z[0], z[1]};
}
;
}

template std::complex<double> SpDLogQ_W_84 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl, const std::array<double,10>& dlr);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> SpDLogQ_W_84 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl, const std::array<dd_real,10>& dlr);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> SpDLogQ_W_84 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl, const std::array<qd_real,10>& dlr);
#endif


template <typename T> std::complex<T> SpDLogQ_W_88 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl, const std::array<T,10>& dlr) {
	using std::abs;
	T x = -(k.v[0] * k.v[2]) + (-k.v[3] + -k.v[4] + k.v[1] + k.v[2]) * k.v[5];
     if (abs(x) < detail::SpDLogZeroThreshold<T>) {
		T dx = -((dl[0] + dl[2]) * k.v[0] * k.v[2]) + (dl[1] + dl[5]) * k.v[1] * k.v[5] + (-(dl[3] * k.v[3]) + -(dl[5] * k.v[3]) + -((dl[4] + dl[5]) * k.v[4]) + dl[2] * k.v[2] + dl[5] * k.v[2]) * k.v[5];
		std::array<T,2> cre;
		std::array<T,2> cim;

cre[0] = (rlog((k.v[0] * k.v[2]) / (k.v[5] * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1)) * T(-2)) / (k.v[0] * k.v[2]) + ((-rlog(T(-1) * k.v[5]) + -rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) + rlog(k.v[0]) + rlog(k.v[2])) * T(2)) / (k.v[4] * k.v[5] + (-k.v[0] + k.v[5]) * (-k.v[2] + k.v[5]));
cre[1] = (-(k.v[0] * k.v[2] * ((rlog(k.v[0]) + rlog(k.v[2]) + rlog(T(-1) * k.v[5]) * T(-1) + rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) * T(-1)) * k.v[0] * k.v[2] + k.v[4] * k.v[5] + (T(-1) * k.v[0] + k.v[5]) * (T(-1) * k.v[2] + k.v[5]))) / prod_pow(k.v[4] * k.v[5] + (-k.v[0] + k.v[5]) * (-k.v[2] + k.v[5]), 2) + rlog((k.v[0] * k.v[2]) / (k.v[5] * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1))) / (prod_pow(k.v[0], 2) * prod_pow(k.v[2], 2));
cim[0] = (m1_set::bc<T>[0] * T(4)) / (-(k.v[4] * k.v[5]) + (-k.v[5] + k.v[0]) * (-k.v[2] + k.v[5]));
cim[1] = (m1_set::bc<T>[0] * T(2)) / prod_pow(k.v[4] * k.v[5] + (-k.v[0] + k.v[5]) * (-k.v[2] + k.v[5]), 2);

			
		cre[1] *= x;
		cre[0] += cre[1];
		cre[0] *= dx;

		cim[1] *= x;
		cim[0] += cim[1];
		cim[0] *= dx;

         return {cre[0],cim[0]};
	}

	std::array<T,10> ccc;

	ccc[0] = m1_set::bc<T>[0];
ccc[1] = dlog_W88(k,dl);
ccc[2] = f_1_1(k);
ccc[3] = f_1_4(k);
ccc[4] = f_1_5(k);
ccc[5] = f_1_7(k);
ccc[6] = f_1_11(k);
ccc[7] = f_2_14(k);
ccc[8] = f_2_22_im(k);
ccc[9] = f_2_22_re(k);


	{
T z[7];
z[0] = ccc[2] + ccc[4];
z[1] = -ccc[3] + z[0];
z[2] = -ccc[6] + z[1];
z[2] = ccc[5] * z[2];
z[2] = ccc[7] + -ccc[9] + z[2];
z[3] = prod_pow(ccc[4], 2);
z[4] = -ccc[2] + -2 * ccc[4];
z[4] = ccc[2] * z[4];
z[0] = -ccc[3] + 2 * z[0];
z[0] = ccc[3] * z[0];
z[5] = prod_pow(ccc[6], 2);
z[6] = prod_pow(ccc[0], 2);
z[0] = z[0] + 2 * z[2] + -z[3] + z[4] + z[5] + (T(-1) / T(3)) * z[6];
z[0] = ccc[1] * z[0];
z[1] = -ccc[5] + z[1];
z[1] = ccc[0] * z[1];
z[1] = -ccc[8] + 2 * z[1];
z[1] = 2 * ccc[1] * z[1];
return {z[0], z[1]};
}
;
}

template std::complex<double> SpDLogQ_W_88 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl, const std::array<double,10>& dlr);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> SpDLogQ_W_88 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl, const std::array<dd_real,10>& dlr);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> SpDLogQ_W_88 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl, const std::array<qd_real,10>& dlr);
#endif


template <typename T> std::complex<T> SpDLogQ_W_90 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl, const std::array<T,10>& dlr) {
	using std::abs;
	T x = (-k.v[1] + -k.v[5] + k.v[2] + k.v[3]) * k.v[5] + k.v[0] * (-k.v[2] + k.v[5]);
     if (abs(x) < detail::SpDLogZeroThreshold<T>) {
		T dx = k.v[5] * (-(dl[1] * k.v[1]) + dl[3] * k.v[3] + dl[5] * (-k.v[1] + k.v[0] + k.v[2] + k.v[3] + T(-2) * k.v[5])) + dl[2] * k.v[2] * (-k.v[0] + k.v[5]) + dl[0] * k.v[0] * (-k.v[2] + k.v[5]);
		std::array<T,2> cre;
		std::array<T,2> cim;

cre[0] = (rlog((k.v[0] * k.v[2]) / (k.v[5] * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1)) * T(-2)) / (k.v[0] * k.v[2]) + ((-rlog(T(-1) * k.v[5]) + -rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) + rlog(k.v[0]) + rlog(k.v[2])) * T(2)) / (k.v[4] * k.v[5] + (-k.v[0] + k.v[5]) * (-k.v[2] + k.v[5]));
cre[1] = (-(k.v[0] * k.v[2] * ((rlog(k.v[0]) + rlog(k.v[2]) + rlog(T(-1) * k.v[5]) * T(-1) + rlog(T(-1) * k.v[0] + T(-1) * k.v[2] + k.v[4] + k.v[5]) * T(-1)) * k.v[0] * k.v[2] + k.v[4] * k.v[5] + (T(-1) * k.v[0] + k.v[5]) * (T(-1) * k.v[2] + k.v[5]))) / prod_pow(k.v[4] * k.v[5] + (-k.v[0] + k.v[5]) * (-k.v[2] + k.v[5]), 2) + rlog((k.v[0] * k.v[2]) / (k.v[5] * (-k.v[0] + -k.v[2] + k.v[4] + k.v[5])) + T(1))) / (prod_pow(k.v[0], 2) * prod_pow(k.v[2], 2));
cim[0] = (m1_set::bc<T>[0] * T(4)) / (-(k.v[4] * k.v[5]) + (-k.v[5] + k.v[0]) * (-k.v[2] + k.v[5]));
cim[1] = (m1_set::bc<T>[0] * T(2)) / prod_pow(k.v[4] * k.v[5] + (-k.v[0] + k.v[5]) * (-k.v[2] + k.v[5]), 2);

			
		cre[1] *= x;
		cre[0] += cre[1];
		cre[0] *= dx;

		cim[1] *= x;
		cim[0] += cim[1];
		cim[0] *= dx;

         return {cre[0],cim[0]};
	}

	std::array<T,10> ccc;

	ccc[0] = m1_set::bc<T>[0];
ccc[1] = dlog_W90(k,dl);
ccc[2] = f_1_1(k);
ccc[3] = f_1_4(k);
ccc[4] = f_1_5(k);
ccc[5] = f_1_9(k);
ccc[6] = f_1_11(k);
ccc[7] = f_2_15(k);
ccc[8] = f_2_22_im(k);
ccc[9] = f_2_22_re(k);


	{
T z[7];
z[0] = ccc[2] + ccc[4];
z[1] = -ccc[3] + z[0];
z[2] = -ccc[6] + z[1];
z[2] = ccc[5] * z[2];
z[2] = ccc[7] + -ccc[9] + z[2];
z[3] = prod_pow(ccc[4], 2);
z[4] = -ccc[2] + -2 * ccc[4];
z[4] = ccc[2] * z[4];
z[0] = -ccc[3] + 2 * z[0];
z[0] = ccc[3] * z[0];
z[5] = prod_pow(ccc[6], 2);
z[6] = prod_pow(ccc[0], 2);
z[0] = z[0] + 2 * z[2] + -z[3] + z[4] + z[5] + (T(-1) / T(3)) * z[6];
z[0] = ccc[1] * z[0];
z[1] = -ccc[5] + z[1];
z[1] = ccc[0] * z[1];
z[1] = -ccc[8] + 2 * z[1];
z[1] = 2 * ccc[1] * z[1];
return {z[0], z[1]};
}
;
}

template std::complex<double> SpDLogQ_W_90 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl, const std::array<double,10>& dlr);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> SpDLogQ_W_90 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl, const std::array<dd_real,10>& dlr);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> SpDLogQ_W_90 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl, const std::array<qd_real,10>& dlr);
#endif



} // namespace m1_set
} // namespace PentagonFunctions

