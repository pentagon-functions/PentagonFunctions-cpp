#pragma once

#include "Kin.h"
#include "Constants.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> void SpDLogSigma5_W_199 (const Kin<T, KinType::m1>&, const std::array<T,6>&, const std::array<T,10>&, T& set_re, T& set_im);
template <typename T> void SpDLogSigma5_W_200 (const Kin<T, KinType::m1>&, const std::array<T,6>&, const std::array<T,10>&, T& set_re, T& set_im);
template <typename T> void SpDLogSigma5_W_201 (const Kin<T, KinType::m1>&, const std::array<T,6>&, const std::array<T,10>&, T& set_re, T& set_im);
template <typename T> void SpDLogSigma5_W_202 (const Kin<T, KinType::m1>&, const std::array<T,6>&, const std::array<T,10>&, T& set_re, T& set_im);
template <typename T> void SpDLogSigma5_W_203 (const Kin<T, KinType::m1>&, const std::array<T,6>&, const std::array<T,10>&, T& set_re, T& set_im);
template <typename T> void SpDLogSigma5_W_204 (const Kin<T, KinType::m1>&, const std::array<T,6>&, const std::array<T,10>&, T& set_re, T& set_im);

template <typename T> std::array<T, 2> f_2_30_series_coefficients(const std::array<T, 6>& v);
template <typename T> std::array<T, 2> f_2_31_series_coefficients(const std::array<T, 6>& v);
template <typename T> std::array<std::complex<T>, 2> f_2_32_series_coefficients(const std::array<T, 6>& v);
template <typename T> std::array<T, 2> f_2_33_series_coefficients(const std::array<T, 6>& v);
template <typename T> std::array<T, 2> f_2_34_series_coefficients(const std::array<T, 6>& v);
template <typename T> std::array<T, 2> f_2_35_series_coefficients(const std::array<T, 6>& v);



template <typename T> std::array<std::complex<T>,2> dlog_W_Sigma5_series (int wi, const Kin<T,KinType::m1>& k, const std::array<T,6>& dv);

template <typename T, int si, int wi,  int lo = 0, typename F1>
void SpDLog_Sigma5(const Kin<T, KinType::m1>& k, const std::array<T, 6>& dv, T& set_re, T& set_im, F1 f1) {
    using std::fabs;

    const auto& w = k.W[si];

    if (fabs(w * w) < ::constants::root_epsilon<T>) {
        auto s1 = f1(k.v);
        auto s2 = dlog_W_Sigma5_series(wi, k, dv);

        auto result = prod_pow<lo>(w)*s1[0]*s2[0] + prod_pow<lo+2>(w)*(s1[0]*s2[1] + s1[1]*s2[0]);

        set_re = result.real();
        set_im = result.imag();
    }
}



} // namespace m1_set
} // namespace PentagonFunctions
