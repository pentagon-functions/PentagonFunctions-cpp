/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_174.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_174_abbreviated (const std::array<T,56>& abb) {
T z[100];
z[0] = abb[31] * (T(1) / T(2));
z[1] = abb[29] * (T(1) / T(2));
z[2] = -abb[32] + z[1];
z[3] = abb[28] * (T(1) / T(2));
z[4] = -z[0] + -z[2] + z[3];
z[4] = abb[29] * z[4];
z[5] = prod_pow(abb[32], 2);
z[6] = abb[31] * abb[32];
z[7] = z[5] + z[6];
z[8] = 3 * abb[31];
z[9] = abb[32] + z[8];
z[10] = -z[3] + z[9];
z[10] = z[3] * z[10];
z[11] = abb[33] * (T(1) / T(2));
z[12] = abb[28] + -abb[32];
z[13] = -z[11] * z[12];
z[14] = abb[54] * (T(1) / T(2));
z[15] = abb[36] * (T(1) / T(2));
z[16] = abb[30] * (T(1) / T(4)) + z[3];
z[17] = -abb[32] + z[16];
z[17] = abb[30] * z[17];
z[4] = z[4] + -z[7] + z[10] + z[13] + z[14] + z[15] + z[17];
z[4] = abb[8] * z[4];
z[10] = 2 * abb[28];
z[13] = 2 * abb[32];
z[17] = abb[31] + z[13];
z[18] = -z[10] + z[11] + z[17];
z[18] = abb[33] * z[18];
z[19] = abb[28] + -abb[31];
z[20] = z[13] + -z[19];
z[21] = abb[29] + -abb[30];
z[20] = z[20] * z[21];
z[22] = abb[31] * (T(3) / T(2)) + z[13];
z[22] = abb[31] * z[22];
z[23] = 3 * abb[54];
z[24] = abb[28] + abb[31];
z[24] = abb[28] * z[24];
z[18] = abb[34] + -2 * z[5] + z[15] + z[18] + z[20] + -z[22] + z[23] + z[24];
z[18] = abb[7] * z[18];
z[20] = -abb[33] + 4 * z[12];
z[20] = abb[33] * z[20];
z[24] = abb[31] + -abb[32];
z[25] = 2 * z[24];
z[26] = abb[28] + z[25];
z[26] = abb[28] * z[26];
z[27] = -abb[37] + abb[38];
z[28] = prod_pow(m1_set::bc<T>[0], 2);
z[29] = (T(4) / T(3)) * z[28];
z[30] = abb[31] * z[17];
z[20] = 2 * abb[34] + 4 * abb[54] + z[5] + -z[20] + z[26] + z[27] + z[29] + -z[30];
z[20] = abb[2] * z[20];
z[26] = -z[8] + -z[11] + z[12];
z[26] = z[11] * z[26];
z[31] = prod_pow(abb[31], 2);
z[32] = z[5] + 3 * z[31];
z[33] = abb[30] * (T(1) / T(2));
z[34] = z[9] * z[33];
z[35] = 3 * abb[28];
z[36] = abb[29] * (T(-3) / T(2)) + -z[9] + z[35];
z[36] = z[1] * z[36];
z[37] = 2 * abb[54];
z[38] = -abb[28] + (T(1) / T(2)) * z[9];
z[38] = abb[28] * z[38];
z[26] = z[26] + (T(1) / T(2)) * z[32] + z[34] + z[36] + -z[37] + z[38];
z[26] = abb[0] * z[26];
z[32] = prod_pow(abb[29], 2);
z[34] = 2 * abb[35];
z[36] = prod_pow(abb[28], 2);
z[32] = -z[27] + -z[32] + z[34] + z[36];
z[32] = abb[9] * z[32];
z[38] = (T(1) / T(2)) * z[36];
z[39] = -abb[28] + z[33];
z[39] = abb[30] * z[39];
z[40] = abb[34] + abb[36];
z[41] = -abb[35] + z[27] + -z[38] + -z[39] + -z[40];
z[42] = abb[3] * (T(1) / T(2));
z[41] = z[41] * z[42];
z[32] = z[32] + z[41];
z[42] = abb[28] * z[13];
z[43] = -abb[29] + 2 * z[19];
z[43] = abb[29] * z[43];
z[44] = 2 * z[6];
z[45] = (T(2) / T(3)) * z[28];
z[42] = 2 * abb[53] + -z[5] + z[42] + -z[43] + -z[44] + z[45];
z[42] = abb[11] * z[42];
z[46] = -abb[33] + 2 * z[12];
z[46] = abb[33] * z[46];
z[46] = -z[37] + z[46];
z[47] = 2 * abb[31];
z[48] = abb[28] * z[47];
z[30] = -z[30] + -z[46] + z[48];
z[30] = abb[12] * z[30];
z[48] = -abb[5] + abb[8];
z[49] = abb[4] + z[48];
z[50] = abb[37] * (T(1) / T(2));
z[49] = z[49] * z[50];
z[51] = -abb[31] + z[33];
z[51] = abb[30] * z[51];
z[52] = -abb[31] + z[11];
z[53] = abb[33] * z[52];
z[54] = -abb[54] + z[53];
z[55] = z[51] + -z[54];
z[56] = abb[5] * (T(1) / T(2));
z[55] = z[55] * z[56];
z[30] = -z[30] + z[49] + -z[55];
z[49] = z[30] + -z[42];
z[57] = abb[29] * z[2];
z[58] = abb[33] * z[12];
z[59] = -abb[54] + z[58];
z[60] = z[57] + z[59];
z[61] = (T(3) / T(2)) * z[5];
z[62] = abb[30] * z[12];
z[63] = abb[32] + z[47];
z[64] = abb[28] * z[63];
z[44] = -z[44] + -z[60] + -z[61] + z[62] + z[64];
z[44] = abb[4] * z[44];
z[64] = abb[28] + abb[30];
z[64] = z[24] * z[64];
z[65] = z[2] + -z[19];
z[66] = abb[29] * z[65];
z[67] = -z[12] + z[52];
z[68] = abb[33] * z[67];
z[40] = -abb[35] + -abb[37] + z[40] + z[64] + -z[66] + z[68];
z[64] = abb[14] * (T(1) / T(2));
z[40] = z[40] * z[64];
z[64] = abb[32] + z[19];
z[64] = abb[28] * z[64];
z[64] = -z[6] + z[64];
z[69] = (T(1) / T(2)) * z[64];
z[70] = abb[29] * z[19];
z[71] = z[69] + -z[70];
z[72] = abb[13] * z[71];
z[73] = abb[31] + z[12];
z[73] = abb[28] * z[73];
z[73] = -z[6] + z[73];
z[73] = (T(1) / T(2)) * z[73];
z[74] = -z[59] + z[73];
z[75] = abb[15] * z[74];
z[72] = z[72] + -z[75];
z[75] = abb[0] * (T(1) / T(2));
z[76] = 2 * abb[12];
z[77] = z[75] + -z[76];
z[78] = abb[7] * (T(1) / T(2));
z[79] = abb[4] + -abb[15];
z[80] = abb[13] + z[77] + z[78] + z[79];
z[28] = (T(1) / T(3)) * z[28];
z[80] = z[28] * z[80];
z[81] = -abb[32] + z[33];
z[81] = abb[30] * z[81];
z[81] = abb[36] + z[81];
z[57] = abb[53] + -z[57] + z[81];
z[82] = abb[6] * (T(1) / T(2));
z[57] = z[57] * z[82];
z[83] = abb[0] * (T(3) / T(2));
z[84] = abb[4] + -abb[13];
z[85] = -abb[8] + -z[83] + -z[84];
z[85] = abb[35] * z[85];
z[86] = abb[4] * (T(1) / T(2));
z[87] = -z[78] + z[86];
z[88] = abb[8] * (T(1) / T(2));
z[89] = z[87] + z[88];
z[90] = -abb[0] + -z[89];
z[90] = abb[38] * z[90];
z[91] = abb[16] + abb[17];
z[92] = abb[18] + -abb[19];
z[92] = (T(-1) / T(2)) * z[92];
z[93] = z[91] + z[92];
z[94] = -abb[55] * z[93];
z[95] = abb[15] + z[75];
z[95] = -abb[34] * z[95];
z[96] = 2 * abb[0] + -z[88];
z[97] = 3 * abb[7] + -z[96];
z[98] = abb[4] + abb[13] + z[97];
z[99] = abb[53] * z[98];
z[4] = z[4] + z[18] + z[20] + z[26] + -z[32] + -z[40] + z[44] + z[49] + -z[57] + z[72] + z[80] + z[85] + z[90] + z[94] + z[95] + z[99];
z[4] = abb[48] * z[4];
z[18] = 3 * abb[32];
z[26] = abb[31] + z[18];
z[44] = -z[3] + z[26];
z[44] = z[3] * z[44];
z[80] = abb[32] * (T(-1) / T(2)) + z[3] + -z[52];
z[80] = abb[33] * z[80];
z[85] = -z[1] * z[19];
z[90] = -abb[34] + z[15];
z[94] = abb[31] + abb[32];
z[95] = -abb[31] * z[94];
z[16] = -abb[31] + z[16];
z[16] = abb[30] * z[16];
z[14] = z[14] + z[16] + z[44] + z[80] + z[85] + z[90] + z[95];
z[14] = abb[8] * z[14];
z[16] = -abb[29] + 4 * z[19];
z[16] = abb[29] * z[16];
z[25] = -abb[28] + z[25];
z[25] = abb[28] * z[25];
z[13] = -abb[31] + z[13];
z[13] = abb[31] * z[13];
z[13] = abb[36] + -abb[38] + -4 * abb[53] + z[5] + z[13] + z[16] + z[25] + -z[29] + -z[34];
z[13] = abb[1] * z[13];
z[16] = -z[1] + -z[18] + z[19];
z[16] = z[1] * z[16];
z[25] = 3 * z[5] + z[31];
z[29] = z[26] * z[33];
z[31] = abb[33] * (T(-3) / T(2)) + -z[26] + z[35];
z[31] = z[11] * z[31];
z[34] = -abb[28] + (T(1) / T(2)) * z[26];
z[34] = abb[28] * z[34];
z[16] = z[16] + (T(1) / T(2)) * z[25] + z[29] + z[31] + z[34] + -z[37];
z[16] = abb[0] * z[16];
z[25] = z[1] + -z[10] + z[63];
z[25] = abb[29] * z[25];
z[12] = -z[12] + z[47];
z[29] = -abb[30] + abb[33];
z[12] = z[12] * z[29];
z[31] = -z[47] * z[94];
z[34] = abb[28] + abb[32];
z[34] = abb[28] * z[34];
z[12] = z[12] + z[23] + z[25] + z[31] + z[34] + -z[61];
z[12] = abb[4] * z[12];
z[23] = z[54] + z[70];
z[25] = abb[30] * z[19];
z[17] = abb[28] * z[17];
z[17] = z[17] + -z[22] + -z[23] + z[25] + z[90];
z[17] = abb[7] * z[17];
z[22] = prod_pow(abb[33], 2);
z[22] = -abb[36] + z[22] + -z[36];
z[22] = abb[10] * z[22];
z[22] = z[22] + -z[41];
z[31] = z[40] + -z[57];
z[34] = 2 * abb[10];
z[36] = abb[15] + -z[34] + -z[83];
z[36] = abb[34] * z[36];
z[40] = abb[7] + -abb[13] + abb[15] + z[77] + z[86];
z[40] = z[28] * z[40];
z[41] = -z[75] + z[84];
z[41] = abb[35] * z[41];
z[44] = -abb[0] + abb[10] + z[87] + -z[88];
z[44] = abb[38] * z[44];
z[47] = z[91] + -z[92];
z[54] = -abb[55] * z[47];
z[57] = 3 * abb[4] + abb[7] + -z[96];
z[61] = -abb[13] + z[57];
z[63] = abb[53] * z[61];
z[12] = z[12] + -z[13] + z[14] + z[16] + z[17] + z[22] + z[31] + z[36] + z[40] + z[41] + z[44] + z[49] + z[54] + z[63] + -z[72];
z[12] = abb[47] * z[12];
z[2] = z[2] + -z[8] + z[35];
z[2] = z[1] * z[2];
z[14] = z[24] * z[33];
z[14] = (T(1) / T(2)) * z[5] + -z[14];
z[16] = -z[11] * z[67];
z[17] = abb[32] + -z[0];
z[17] = abb[31] * z[17];
z[33] = (T(1) / T(2)) * z[24];
z[36] = -abb[28] + z[33];
z[36] = abb[28] * z[36];
z[2] = z[2] + z[14] + z[16] + z[17] + z[36];
z[2] = abb[0] * z[2];
z[16] = abb[16] + -abb[17];
z[16] = (T(1) / T(2)) * z[16];
z[17] = abb[55] * z[16];
z[17] = z[17] + -z[31];
z[31] = -abb[31] + z[18];
z[31] = abb[28] * z[31];
z[31] = -z[5] + -3 * z[6] + z[31];
z[31] = (T(1) / T(2)) * z[31] + -z[43];
z[31] = abb[4] * z[31];
z[36] = abb[36] + z[39];
z[39] = z[36] + z[59];
z[40] = z[3] + -z[24];
z[40] = abb[28] * z[40];
z[40] = z[39] + z[40] + -z[70];
z[40] = z[40] * z[88];
z[41] = z[19] * z[94];
z[41] = abb[36] + z[41];
z[41] = z[41] * z[78];
z[43] = -abb[0] + abb[4];
z[44] = z[43] * z[45];
z[49] = -abb[4] + abb[8];
z[54] = abb[5] + z[49];
z[50] = z[50] * z[54];
z[43] = 2 * z[43] + z[88];
z[54] = abb[53] * z[43];
z[63] = -abb[34] * z[75];
z[67] = abb[8] + -z[83];
z[67] = abb[35] * z[67];
z[49] = -abb[7] + -z[49];
z[49] = abb[38] * z[49];
z[2] = z[2] + -z[13] + -z[17] + z[31] + z[32] + z[40] + z[41] + -z[42] + z[44] + (T(1) / T(2)) * z[49] + z[50] + z[54] + z[55] + z[63] + z[67];
z[2] = abb[49] * z[2];
z[13] = -z[18] + z[35] + z[52];
z[13] = z[11] * z[13];
z[18] = -z[1] * z[65];
z[0] = abb[32] + z[0];
z[0] = abb[31] * z[0];
z[31] = -abb[28] + -z[33];
z[31] = abb[28] * z[31];
z[0] = z[0] + z[13] + -z[14] + z[18] + z[31] + -z[37];
z[0] = abb[0] * z[0];
z[13] = z[3] + z[24];
z[13] = abb[28] * z[13];
z[13] = z[13] + z[36] + -z[59] + z[70];
z[13] = abb[34] + (T(1) / T(2)) * z[13];
z[13] = abb[8] * z[13];
z[14] = -abb[31] * z[26];
z[18] = -abb[32] + z[8];
z[18] = abb[28] * z[18];
z[14] = z[14] + z[18];
z[14] = (T(1) / T(2)) * z[14] + -z[15] + -z[46];
z[14] = abb[7] * z[14];
z[15] = z[34] + -z[83];
z[15] = abb[34] * z[15];
z[18] = abb[28] * z[94];
z[7] = -z[7] + z[18];
z[7] = z[7] * z[86];
z[18] = -abb[0] + abb[7] + -abb[12];
z[18] = z[18] * z[45];
z[24] = -abb[10] + -z[89];
z[24] = abb[38] * z[24];
z[31] = -abb[53] * z[88];
z[32] = -abb[35] * z[75];
z[0] = z[0] + z[7] + z[13] + z[14] + z[15] + z[17] + z[18] + z[20] + -z[22] + z[24] + z[30] + z[31] + z[32];
z[0] = abb[50] * z[0];
z[7] = abb[45] * (T(1) / T(2));
z[13] = z[7] * z[19];
z[14] = 2 * abb[52] + abb[42] * (T(1) / T(2));
z[15] = abb[31] * z[14];
z[17] = abb[28] * z[14];
z[18] = z[15] + -z[17];
z[13] = z[13] + z[18];
z[13] = abb[29] * z[13];
z[20] = z[3] * z[26];
z[22] = abb[31] + abb[32] * (T(3) / T(2));
z[22] = abb[31] * z[22];
z[20] = z[20] + -z[22] + z[25];
z[22] = z[20] + -z[23];
z[23] = -abb[34] + z[22];
z[24] = abb[46] * (T(1) / T(2));
z[25] = -abb[44] + z[24];
z[23] = -z[23] * z[25];
z[20] = -z[20] + z[53];
z[20] = z[7] * z[20];
z[26] = abb[43] * (T(1) / T(2));
z[22] = z[22] * z[26];
z[30] = -z[7] + z[14];
z[31] = z[26] + z[30];
z[31] = abb[34] * z[31];
z[32] = abb[54] * z[30];
z[33] = abb[52] + abb[42] * (T(1) / T(4));
z[34] = abb[32] * z[33];
z[35] = 3 * z[34];
z[36] = -z[15] + -z[35];
z[36] = abb[31] * z[36];
z[37] = abb[31] * z[33];
z[35] = z[35] + z[37];
z[35] = abb[28] * z[35];
z[18] = -abb[30] * z[18];
z[40] = -abb[33] * z[33];
z[15] = z[15] + z[40];
z[15] = abb[33] * z[15];
z[15] = z[13] + z[15] + z[18] + z[20] + z[22] + z[23] + -z[31] + z[32] + z[35] + z[36];
z[15] = abb[16] * z[15];
z[3] = z[3] * z[9];
z[3] = -z[3] + z[5] + (T(3) / T(2)) * z[6] + -z[62];
z[9] = z[3] + z[60];
z[18] = -abb[44] + z[24] + -z[26];
z[9] = z[9] * z[18];
z[3] = z[3] + z[58];
z[3] = z[3] * z[7];
z[20] = abb[32] * z[14];
z[17] = -z[17] + z[20];
z[22] = abb[33] * z[17];
z[22] = z[22] + z[32];
z[23] = abb[45] * (T(1) / T(4));
z[24] = z[23] + -z[33];
z[32] = abb[29] * z[24];
z[35] = -abb[32] * z[7];
z[20] = z[20] + z[32] + z[35];
z[20] = abb[29] * z[20];
z[5] = -z[5] * z[14];
z[32] = -z[8] * z[34];
z[8] = z[8] * z[33];
z[8] = z[8] + z[34];
z[8] = abb[28] * z[8];
z[17] = -abb[30] * z[17];
z[3] = z[3] + z[5] + z[8] + z[9] + z[17] + z[20] + z[22] + z[32];
z[3] = abb[17] * z[3];
z[5] = abb[34] + z[74];
z[5] = -z[5] * z[25];
z[8] = -z[34] + z[37];
z[9] = abb[28] * z[33];
z[17] = z[8] + z[9];
z[17] = abb[28] * z[17];
z[20] = z[58] + -z[73];
z[20] = z[7] * z[20];
z[25] = z[26] * z[74];
z[26] = abb[31] * z[34];
z[5] = z[5] + z[17] + z[20] + z[22] + z[25] + -z[26] + z[31];
z[5] = abb[19] * z[5];
z[17] = z[18] + -z[30];
z[20] = abb[8] * z[17];
z[22] = abb[43] * (T(1) / T(4)) + -z[24];
z[24] = -abb[4] + abb[13] + abb[15];
z[24] = z[22] * z[24];
z[25] = abb[46] * (T(-1) / T(4)) + abb[44] * (T(1) / T(2));
z[22] = z[22] + z[25];
z[22] = abb[7] * z[22];
z[31] = -abb[15] + z[84];
z[25] = z[25] * z[31];
z[31] = abb[42] + abb[43] + 2 * abb[44] + -abb[45] + -abb[46] + 4 * abb[52];
z[31] = abb[27] * z[31];
z[32] = abb[26] * abb[51];
z[20] = z[20] + -z[22] + z[24] + -z[25] + -z[31] + (T(-1) / T(4)) * z[32];
z[22] = abb[55] * z[20];
z[6] = -abb[53] + z[6] + -z[27] + -z[38] + z[39] + z[70];
z[24] = abb[51] * (T(1) / T(2));
z[6] = z[6] * z[24];
z[25] = abb[51] * z[28];
z[6] = z[6] + -z[25];
z[25] = abb[21] + abb[23];
z[6] = z[6] * z[25];
z[27] = -z[18] * z[71];
z[8] = -z[8] + z[9];
z[8] = abb[28] * z[8];
z[9] = -z[23] * z[64];
z[8] = z[8] + z[9] + z[13] + -z[26] + z[27];
z[8] = abb[18] * z[8];
z[9] = abb[37] + z[51] + -z[68] + -z[73];
z[9] = abb[24] * z[9];
z[13] = -z[66] + -z[69] + z[81];
z[13] = abb[22] * z[13];
z[26] = -abb[25] * z[74];
z[27] = abb[24] + abb[25];
z[31] = -abb[34] * z[27];
z[9] = z[9] + z[13] + z[26] + z[31];
z[9] = z[9] * z[24];
z[13] = abb[16] * z[17];
z[26] = abb[17] * z[17];
z[13] = z[13] + z[26];
z[31] = abb[18] * z[17];
z[32] = abb[20] * z[24];
z[31] = z[31] + z[32];
z[33] = z[13] + z[31];
z[17] = abb[19] * z[17];
z[27] = -abb[22] + -z[27];
z[27] = z[24] * z[27];
z[27] = -z[17] + z[27] + -z[33];
z[27] = z[27] * z[28];
z[28] = -abb[53] * z[33];
z[35] = -abb[22] * z[24];
z[26] = z[26] + -z[31] + z[35];
z[26] = abb[35] * z[26];
z[31] = -z[32] * z[71];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[8] + z[9] + z[12] + z[15] + z[22] + z[26] + z[27] + z[28] + z[31];
z[2] = abb[41] * z[20];
z[3] = abb[29] + z[29];
z[4] = (T(3) / T(2)) * z[94];
z[5] = z[3] + -z[4];
z[5] = z[5] * z[18];
z[6] = z[4] + -z[29];
z[6] = z[6] * z[7];
z[8] = abb[29] * z[30];
z[9] = z[34] + z[37];
z[12] = abb[30] * z[14];
z[14] = abb[33] * z[14];
z[5] = -z[5] + z[6] + z[8] + -3 * z[9] + -z[12] + z[14];
z[5] = z[5] * z[91];
z[6] = (T(1) / T(2)) * z[94];
z[12] = -abb[29] + z[6];
z[15] = z[12] * z[18];
z[20] = z[23] * z[94];
z[8] = z[8] + -z[9] + z[15] + z[20];
z[8] = abb[18] * z[8];
z[7] = z[7] + z[18];
z[15] = -abb[33] + z[6];
z[7] = z[7] * z[15];
z[7] = z[7] + -z[9] + z[14];
z[7] = abb[19] * z[7];
z[9] = abb[22] + abb[24];
z[14] = -abb[30] + z[6];
z[9] = z[9] * z[14];
z[14] = abb[25] * z[15];
z[9] = z[9] + z[14];
z[9] = z[9] * z[24];
z[14] = z[12] * z[32];
z[5] = z[5] + z[7] + z[8] + z[9] + z[14];
z[5] = m1_set::bc<T>[0] * z[5];
z[1] = abb[30] + -z[1] + -z[11] + z[94];
z[1] = abb[8] * z[1];
z[7] = -abb[32] + z[19];
z[8] = z[7] * z[76];
z[9] = z[29] * z[56];
z[8] = z[8] + z[9];
z[3] = -z[3] + 2 * z[94];
z[11] = abb[4] + abb[7];
z[3] = z[3] * z[11];
z[1] = z[1] + z[3] + z[8];
z[3] = -abb[28] + abb[29];
z[11] = 2 * abb[9];
z[3] = z[3] * z[11];
z[11] = abb[15] * z[15];
z[12] = abb[13] * z[12];
z[11] = z[11] + -z[12];
z[12] = -z[1] + -z[3] + z[11];
z[12] = m1_set::bc<T>[0] * z[12];
z[14] = m1_set::bc<T>[0] * z[7];
z[14] = abb[39] + z[14];
z[15] = 2 * abb[11];
z[14] = z[14] * z[15];
z[15] = m1_set::bc<T>[0] * z[21];
z[15] = abb[39] + z[15];
z[15] = z[15] * z[82];
z[14] = z[14] + z[15];
z[18] = z[56] + z[76];
z[19] = -z[18] + z[79] + z[97];
z[19] = abb[40] * z[19];
z[20] = abb[33] + z[7];
z[20] = m1_set::bc<T>[0] * z[20];
z[20] = 2 * abb[40] + z[20];
z[21] = 2 * abb[2];
z[20] = z[20] * z[21];
z[21] = -abb[41] * z[93];
z[22] = abb[39] * z[98];
z[12] = z[12] + -z[14] + z[19] + z[20] + z[21] + z[22];
z[12] = abb[48] * z[12];
z[19] = abb[29] + -abb[33];
z[19] = z[19] * z[88];
z[4] = -z[4] + z[10];
z[10] = abb[7] * z[4];
z[21] = -abb[28] + abb[33];
z[21] = abb[10] * z[21];
z[21] = 2 * z[21];
z[22] = -2 * abb[33] + z[94];
z[22] = abb[0] * z[22];
z[23] = -abb[4] * z[6];
z[8] = -z[8] + z[10] + -z[19] + z[21] + z[22] + z[23];
z[8] = m1_set::bc<T>[0] * z[8];
z[10] = 2 * abb[7] + -z[18] + -z[96];
z[10] = abb[40] * z[10];
z[16] = abb[41] * z[16];
z[22] = -abb[39] * z[88];
z[8] = z[8] + z[10] + z[15] + z[16] + z[20] + z[22];
z[8] = abb[50] * z[8];
z[4] = abb[4] * z[4];
z[10] = -2 * abb[29] + z[94];
z[10] = abb[0] * z[10];
z[6] = -abb[7] * z[6];
z[3] = z[3] + z[4] + z[6] + z[9] + z[10] + z[19];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = abb[29] + z[7];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = 2 * abb[39] + z[4];
z[6] = 2 * abb[1];
z[4] = z[4] * z[6];
z[4] = z[4] + -z[14];
z[6] = abb[39] * z[43];
z[7] = abb[40] * z[48];
z[3] = z[3] + z[4] + z[6] + (T(-1) / T(2)) * z[7] + -z[16];
z[3] = abb[49] * z[3];
z[1] = -z[1] + -z[11] + -z[21];
z[1] = m1_set::bc<T>[0] * z[1];
z[6] = -abb[41] * z[47];
z[7] = abb[15] + -z[18] + z[57];
z[7] = abb[40] * z[7];
z[9] = abb[39] * z[61];
z[1] = z[1] + z[4] + z[6] + z[7] + z[9];
z[1] = abb[47] * z[1];
z[4] = -abb[25] * z[24];
z[4] = z[4] + -z[13] + -z[17];
z[4] = abb[40] * z[4];
z[6] = -abb[39] * z[33];
z[7] = (T(1) / T(2)) * z[25];
z[9] = -abb[29] + -abb[33] + z[94];
z[9] = m1_set::bc<T>[0] * z[9];
z[9] = -abb[39] + -abb[40] + z[9];
z[7] = abb[51] * z[7] * z[9];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[12];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_174_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("-3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("-3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("-3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("-7.7143564516654021477305372678719799821591283604003080270983573661"),stof<T>("-7.2516632689748497845188292938738884372507784963632055609905540863")}, std::complex<T>{stof<T>("3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("-28.065593408028772133635407567885755680874875087910720549766606817"),stof<T>("-10.134279212974814860169624191733463868627374893137719822959739056")}, std::complex<T>{stof<T>("-28.065593408028772133635407567885755680874875087910720549766606817"),stof<T>("-10.134279212974814860169624191733463868627374893137719822959739056")}, std::complex<T>{stof<T>("5.0640616850712605728445510775568457438497106153224516270917389906"),stof<T>("9.7872971614052357116164436201066045743353814609176417256353631691")}, std::complex<T>{stof<T>("5.0640616850712605728445510775568457438497106153224516270917389906"),stof<T>("9.7872971614052357116164436201066045743353814609176417256353631691")}, std::complex<T>{stof<T>("-0.62875951357982689959604956510979502283569846153696176805265663139"),stof<T>("0.631211800731221965993284197375092772384272312334023096282934671")}, std::complex<T>{stof<T>("-15.428712903330804295461074535743959964318256720800616054196714732"),stof<T>("-14.503326537949699569037658587747776874501556992726411121981108173")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}, rlog(k.W[195].real()/kbase.W[195].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_174_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_174_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-54.465920497743083947987469236870500063587739576331002124048561988"),stof<T>("44.653865674998764678053119695030997310737867420909465989709073009")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), rlog(kend.W[195].real()/k.W[195].real()), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_174_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_174_DLogXconstant_part(base_point<T>, kend);
	value += f_4_174_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_174_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_174_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_174_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_174_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_174_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_174_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
