/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_258.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_258_abbreviated (const std::array<T,53>& abb) {
T z[85];
z[0] = abb[44] + abb[47];
z[1] = abb[43] + abb[45];
z[2] = (T(4) / T(3)) * z[1];
z[3] = abb[42] * (T(1) / T(3));
z[4] = abb[48] * (T(1) / T(2));
z[0] = abb[41] * (T(-7) / T(6)) + abb[40] * (T(5) / T(6)) + (T(1) / T(2)) * z[0] + -z[2] + z[3] + -z[4];
z[0] = abb[6] * z[0];
z[5] = -abb[40] + abb[47];
z[6] = abb[42] + abb[44];
z[7] = -z[4] + -z[5] + (T(1) / T(2)) * z[6];
z[8] = abb[13] * z[7];
z[9] = abb[49] * (T(1) / T(2));
z[10] = abb[21] * z[9];
z[11] = z[8] + z[10];
z[12] = abb[42] + abb[48];
z[13] = -abb[44] + z[12];
z[13] = -abb[41] + (T(1) / T(2)) * z[13];
z[14] = abb[15] * z[13];
z[15] = abb[24] * z[9];
z[15] = z[14] + -z[15];
z[16] = abb[23] * z[9];
z[17] = -z[15] + -z[16];
z[18] = z[11] + -z[17];
z[19] = abb[48] * (T(19) / T(3)) + z[3] + -7 * z[5];
z[20] = abb[46] * (T(13) / T(2));
z[19] = (T(10) / T(3)) * z[1] + (T(1) / T(2)) * z[19] + -z[20];
z[19] = abb[1] * z[19];
z[21] = 3 * abb[44];
z[2] = abb[48] * (T(-13) / T(6)) + abb[42] * (T(5) / T(6)) + -z[2] + z[20] + -z[21];
z[2] = abb[8] * z[2];
z[20] = -z[1] + z[5];
z[22] = -abb[42] + abb[44];
z[23] = z[20] + z[22];
z[23] = abb[12] * z[23];
z[3] = abb[44] + abb[48] + abb[41] * (T(-5) / T(3)) + abb[40] * (T(1) / T(3)) + z[3];
z[3] = -abb[47] + (T(-1) / T(6)) * z[1] + (T(1) / T(2)) * z[3];
z[3] = abb[5] * z[3];
z[24] = z[1] + -z[12];
z[25] = -abb[41] + (T(-1) / T(3)) * z[24];
z[25] = abb[2] * z[25];
z[26] = abb[22] * abb[49];
z[27] = abb[41] + -abb[42];
z[27] = abb[11] * z[27];
z[28] = 3 * z[27];
z[29] = abb[20] * z[9];
z[30] = abb[41] + -abb[48];
z[31] = abb[40] + z[30];
z[32] = abb[0] * z[31];
z[0] = z[0] + z[2] + z[3] + z[18] + z[19] + 2 * z[23] + (T(7) / T(2)) * z[25] + z[26] + z[28] + z[29] + (T(13) / T(6)) * z[32];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[2] = 3 * abb[42] + -abb[44];
z[3] = 2 * abb[40];
z[19] = -2 * abb[47] + z[1] + (T(1) / T(2)) * z[2] + z[3] + -z[4];
z[19] = abb[5] * z[19];
z[25] = abb[23] * abb[49];
z[33] = abb[21] * abb[49];
z[34] = z[25] + z[33];
z[35] = -z[12] + z[21];
z[36] = -z[1] + (T(1) / T(2)) * z[35];
z[37] = abb[7] * z[36];
z[38] = z[8] + (T(1) / T(2)) * z[34] + -z[37];
z[39] = abb[6] * z[36];
z[40] = abb[8] * z[36];
z[41] = z[39] + z[40];
z[42] = 2 * abb[42];
z[43] = -abb[48] + z[1] + z[42];
z[44] = 3 * abb[47];
z[45] = 3 * abb[40] + -z[44];
z[46] = z[43] + z[45];
z[47] = abb[1] * z[46];
z[9] = abb[22] * z[9];
z[19] = -z[9] + z[19] + z[23] + -z[38] + -z[41] + z[47];
z[48] = abb[50] * z[19];
z[49] = 3 * abb[48] + -z[6];
z[49] = z[1] + (T(1) / T(2)) * z[49];
z[50] = 2 * abb[46];
z[51] = z[49] + -z[50];
z[51] = abb[14] * z[51];
z[52] = abb[48] + z[1];
z[53] = -abb[44] + -abb[46] + z[52];
z[53] = abb[10] * z[53];
z[51] = z[10] + z[51] + -z[53];
z[54] = -z[37] + z[39] + -z[51];
z[54] = abb[34] * z[54];
z[55] = -abb[48] + z[6];
z[56] = abb[4] * z[55];
z[57] = (T(1) / T(2)) * z[56];
z[10] = z[10] + -z[17] + z[57];
z[2] = -abb[48] + z[2];
z[2] = z[1] + (T(1) / T(2)) * z[2];
z[2] = abb[5] * z[2];
z[58] = 3 * abb[41];
z[24] = -z[24] + -z[58];
z[59] = abb[2] * z[24];
z[60] = abb[6] * z[13];
z[60] = z[59] + z[60];
z[2] = z[2] + -z[10] + z[60];
z[61] = -abb[51] * z[2];
z[62] = abb[5] * z[7];
z[22] = abb[48] + z[22];
z[22] = -abb[46] + (T(1) / T(2)) * z[22];
z[63] = abb[14] * z[22];
z[62] = z[62] + z[63];
z[64] = -abb[42] + -abb[46] + abb[48] + z[5];
z[65] = abb[1] * z[64];
z[65] = z[8] + -z[16] + -z[62] + z[65];
z[65] = abb[33] * z[65];
z[66] = abb[3] * z[22];
z[15] = z[15] + -z[51] + -z[60] + z[66];
z[15] = abb[32] * z[15];
z[51] = abb[16] + abb[18];
z[7] = z[7] * z[51];
z[51] = abb[44] + z[12];
z[60] = -abb[41] + -abb[47] + (T(1) / T(2)) * z[51];
z[60] = abb[17] * z[60];
z[7] = z[7] + z[60];
z[60] = abb[52] * z[7];
z[67] = -abb[5] * z[55];
z[25] = -z[25] + z[56] + z[67];
z[25] = (T(1) / T(2)) * z[25] + -z[63];
z[25] = abb[35] * z[25];
z[63] = abb[34] * z[36];
z[55] = (T(1) / T(2)) * z[55];
z[67] = -abb[51] * z[55];
z[22] = abb[35] * z[22];
z[22] = z[22] + z[63] + z[67];
z[22] = abb[8] * z[22];
z[63] = -abb[34] + -abb[35] + abb[36];
z[67] = -abb[33] + z[63];
z[68] = prod_pow(abb[26], 2);
z[68] = -z[67] + (T(1) / T(2)) * z[68];
z[68] = z[66] * z[68];
z[15] = z[15] + z[22] + z[25] + -z[48] + z[54] + (T(1) / T(2)) * z[60] + z[61] + z[65] + z[68];
z[8] = z[8] + z[14];
z[14] = abb[9] * z[30];
z[22] = abb[49] * (T(1) / T(4));
z[25] = -abb[24] * z[22];
z[8] = (T(1) / T(2)) * z[8] + z[14] + z[25];
z[25] = (T(1) / T(2)) * z[1];
z[30] = abb[42] + z[25];
z[48] = abb[47] * (T(3) / T(2));
z[54] = abb[41] + z[48];
z[60] = abb[40] * (T(-1) / T(2)) + -z[4] + -z[30] + z[54];
z[60] = abb[5] * z[60];
z[45] = -z[12] + -z[45];
z[61] = abb[46] * (T(3) / T(2));
z[45] = -z[1] + (T(1) / T(2)) * z[45] + z[61];
z[45] = abb[1] * z[45];
z[65] = abb[42] * (T(1) / T(2));
z[68] = -z[25] + z[65];
z[69] = abb[41] * (T(1) / T(2));
z[70] = -abb[40] + z[69];
z[71] = z[4] + -z[68] + z[70];
z[71] = abb[6] * z[71];
z[72] = 3 * abb[46];
z[73] = abb[9] * z[72];
z[73] = 2 * z[32] + -z[73];
z[74] = (T(1) / T(2)) * z[59];
z[61] = abb[48] + -z[61] + -z[68];
z[61] = abb[14] * z[61];
z[68] = abb[49] * (T(3) / T(4));
z[75] = abb[22] * z[68];
z[8] = 3 * z[8] + (T(-1) / T(2)) * z[40] + z[45] + z[60] + z[61] + z[71] + -z[73] + -z[74] + z[75];
z[8] = abb[26] * z[8];
z[3] = z[3] + -z[48] + z[69];
z[45] = abb[48] * (T(5) / T(4)) + -z[3] + (T(-3) / T(4)) * z[6];
z[45] = abb[5] * z[45];
z[60] = z[11] + z[17];
z[61] = abb[20] * z[68];
z[71] = (T(1) / T(2)) * z[32];
z[75] = -z[59] + z[61] + z[71];
z[6] = abb[40] + z[4] + (T(3) / T(2)) * z[6];
z[6] = (T(1) / T(2)) * z[6] + -z[54];
z[6] = abb[6] * z[6];
z[76] = abb[14] * z[36];
z[6] = z[6] + z[45] + -z[47] + (T(3) / T(2)) * z[60] + -z[75] + -z[76];
z[45] = abb[27] + -abb[30];
z[6] = z[6] * z[45];
z[6] = z[6] + z[8];
z[6] = abb[26] * z[6];
z[8] = 13 * abb[42] + -z[21];
z[45] = abb[48] * (T(3) / T(4));
z[60] = 2 * z[1];
z[8] = -4 * abb[40] + abb[47] * (T(9) / T(2)) + (T(-1) / T(4)) * z[8] + z[45] + -z[60] + z[69];
z[8] = abb[5] * z[8];
z[69] = 5 * abb[42] + -z[21];
z[77] = abb[48] * (T(3) / T(2));
z[78] = abb[40] + (T(1) / T(2)) * z[69] + z[77];
z[54] = z[1] + -z[54] + (T(1) / T(2)) * z[78];
z[54] = abb[6] * z[54];
z[18] = (T(3) / T(2)) * z[18];
z[78] = 3 * z[23];
z[79] = abb[49] * (T(3) / T(2));
z[80] = abb[22] * z[79];
z[81] = z[78] + -z[80];
z[8] = z[8] + z[18] + z[40] + -2 * z[47] + -z[54] + z[75] + -z[81];
z[54] = -abb[27] * z[8];
z[75] = abb[40] + abb[42];
z[82] = -abb[41] + z[1];
z[83] = -z[44] + 2 * z[75] + z[82];
z[83] = abb[6] * z[83];
z[84] = abb[8] * z[43];
z[42] = -abb[40] + z[42] + z[82];
z[82] = 2 * abb[5];
z[42] = z[42] * z[82];
z[28] = z[28] + z[32] + z[42] + -z[47] + z[59] + z[83] + z[84];
z[28] = abb[30] * z[28];
z[28] = z[28] + z[54];
z[28] = abb[30] * z[28];
z[24] = abb[6] * z[24];
z[32] = 3 * z[59];
z[36] = abb[5] * z[36];
z[42] = abb[20] * z[79];
z[42] = z[36] + z[42];
z[54] = -z[40] + z[80];
z[17] = 3 * z[17] + z[24] + z[32] + -z[42] + -z[54] + -z[76];
z[24] = abb[26] + -abb[27];
z[17] = z[17] * z[24];
z[24] = z[33] + z[56];
z[27] = z[14] + z[27];
z[33] = abb[46] + (T(-1) / T(2)) * z[49];
z[33] = abb[14] * z[33];
z[49] = abb[41] + z[25] + (T(-1) / T(4)) * z[51];
z[49] = abb[6] * z[49];
z[51] = -abb[9] * abb[46];
z[52] = -abb[46] + (T(1) / T(2)) * z[52];
z[52] = abb[8] * z[52];
z[33] = (T(-1) / T(4)) * z[24] + -z[27] + z[33] + z[49] + z[51] + z[52] + -z[59];
z[33] = abb[31] * z[33];
z[43] = z[43] * z[82];
z[24] = (T(-3) / T(2)) * z[24] + -z[39] + z[43] + z[76] + z[84];
z[24] = abb[30] * z[24];
z[17] = z[17] + -z[24] + 3 * z[33];
z[17] = abb[31] * z[17];
z[30] = z[30] + -z[48];
z[33] = z[30] + -z[70];
z[33] = abb[5] * z[33];
z[4] = abb[40] * (T(3) / T(2)) + -z[4] + z[30];
z[4] = abb[1] * z[4];
z[30] = abb[41] + -z[44] + z[75];
z[30] = z[1] + (T(1) / T(2)) * z[30];
z[30] = abb[6] * z[30];
z[4] = z[4] + (T(3) / T(2)) * z[23] + z[30] + z[33] + -z[40] + -z[71] + -z[74];
z[4] = prod_pow(abb[27], 2) * z[4];
z[11] = 3 * z[11];
z[30] = -z[46] * z[82];
z[30] = z[11] + z[30] + z[41] + -3 * z[47] + -z[76] + -z[81];
z[30] = abb[30] * z[30];
z[33] = z[41] + 3 * z[53];
z[40] = 5 * abb[48];
z[43] = abb[42] + z[21] + -z[40];
z[43] = (T(1) / T(2)) * z[43] + -z[60] + z[72];
z[43] = abb[14] * z[43];
z[44] = abb[5] * z[46];
z[46] = 3 * abb[1];
z[49] = -z[46] * z[64];
z[11] = -z[11] + z[33] + z[43] + z[44] + z[49] + -z[80];
z[11] = abb[26] * z[11];
z[35] = -z[35] + z[60];
z[43] = abb[6] + abb[8];
z[35] = z[35] * z[43];
z[16] = -z[16] + z[37];
z[35] = 3 * z[16] + z[35] + -z[36] + -z[76];
z[35] = abb[27] * z[35];
z[16] = z[16] + -z[23] + z[53] + -z[62];
z[5] = abb[46] * (T(1) / T(2)) + z[5] + -z[25] + -z[65];
z[5] = abb[1] * z[5];
z[5] = z[5] + (T(1) / T(2)) * z[16];
z[5] = abb[28] * z[5];
z[5] = 3 * z[5] + z[11] + z[30] + -z[35];
z[5] = abb[28] * z[5];
z[11] = -z[33] + (T(3) / T(2)) * z[34];
z[16] = -abb[42] + 2 * abb[48] + z[1];
z[23] = z[16] + -z[72];
z[25] = 2 * abb[14];
z[25] = z[23] * z[25];
z[30] = z[25] + z[42] + z[80];
z[33] = z[11] + z[30] + -3 * z[66];
z[33] = abb[26] * z[33];
z[42] = -z[37] + -z[53] + z[57] + z[66];
z[43] = -abb[20] + -abb[22];
z[22] = z[22] * z[43];
z[43] = -abb[42] + z[21];
z[44] = abb[48] + z[43];
z[44] = -abb[46] + (T(1) / T(4)) * z[44];
z[44] = abb[8] * z[44];
z[22] = z[22] + (T(1) / T(2)) * z[42] + z[44];
z[22] = abb[29] * z[22];
z[22] = 3 * z[22] + z[24] + z[33] + z[35];
z[22] = abb[29] * z[22];
z[24] = z[25] + z[36];
z[11] = z[11] + z[24];
z[11] = abb[36] * z[11];
z[25] = abb[51] + z[63];
z[25] = z[25] * z[26];
z[26] = abb[51] + z[67];
z[26] = abb[20] * abb[49] * z[26];
z[25] = z[25] + z[26];
z[13] = abb[19] * z[13];
z[26] = abb[25] * z[68];
z[13] = (T(3) / T(2)) * z[13] + -z[26];
z[26] = abb[52] * z[13];
z[0] = z[0] + z[4] + z[5] + z[6] + z[11] + 3 * z[15] + z[17] + z[22] + (T(3) / T(2)) * z[25] + z[26] + z[28];
z[3] = z[1] + z[3] + -z[45] + (T(1) / T(4)) * z[69];
z[3] = abb[5] * z[3];
z[4] = -abb[40] + (T(-1) / T(2)) * z[43] + z[77];
z[1] = -2 * abb[41] + -z[1] + (T(1) / T(2)) * z[4] + z[48];
z[1] = abb[6] * z[1];
z[1] = z[1] + z[3] + -z[18] + z[47] + -z[54] + 2 * z[59] + -z[61] + z[71];
z[1] = abb[27] * z[1];
z[3] = -abb[30] * z[8];
z[4] = z[10] + 2 * z[27];
z[5] = z[12] + z[21];
z[5] = (T(1) / T(2)) * z[5] + -z[58] + -z[60];
z[5] = abb[6] * z[5];
z[6] = -z[40] + -z[43];
z[8] = 6 * abb[46];
z[6] = (T(1) / T(2)) * z[6] + z[8] + -z[60];
z[6] = abb[8] * z[6];
z[10] = abb[9] * z[8];
z[4] = 3 * z[4] + z[5] + z[6] + z[10] + z[30] + z[32];
z[4] = abb[31] * z[4];
z[5] = z[24] + -6 * z[53];
z[6] = abb[48] + -z[20] + -z[50];
z[6] = z[6] * z[46];
z[6] = z[5] + z[6] + 3 * z[38] + -z[41] + z[78] + z[80];
z[6] = abb[28] * z[6];
z[10] = -abb[5] + abb[6];
z[10] = z[10] * z[31];
z[11] = abb[1] + -abb[14];
z[11] = z[11] * z[23];
z[10] = z[10] + z[11] + -3 * z[14] + -z[59] + z[73];
z[10] = abb[26] * z[10];
z[11] = -z[34] + -z[56];
z[11] = (T(1) / T(2)) * z[11] + z[37];
z[8] = z[8] + -z[16] + -z[21];
z[8] = abb[8] * z[8];
z[5] = -z[5] + z[8] + 3 * z[11] + z[39];
z[5] = abb[29] * z[5];
z[1] = z[1] + z[3] + z[4] + z[5] + z[6] + 2 * z[10];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[37] * z[19];
z[4] = -abb[8] * z[55];
z[2] = -z[2] + z[4] + z[9] + z[29];
z[2] = abb[38] * z[2];
z[4] = abb[39] * z[7];
z[2] = z[2] + z[3] + (T(1) / T(2)) * z[4];
z[3] = abb[39] * z[13];
z[1] = z[1] + 3 * z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_258_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.450078917274434293101657209467872430583744502505932285414849113"),stof<T>("-53.134356214699376742634265603799826706847615954368603431025468477")}, std::complex<T>{stof<T>("-29.812321165349265736397436937642014984677930848764465516646227806"),stof<T>("21.120501999199564205866697115437052890912296680443702455469877917")}, std::complex<T>{stof<T>("6.649017022093725609138036969297300005090047660717341708608668748"),stof<T>("-13.09577886334928700723304252220086849418291639961518254186002122")}, std::complex<T>{stof<T>("-17.615507745935394102840818345096835064136201461388195528260961886"),stof<T>("8.584324298882360639391283163367212198296924741406416600296859453")}, std::complex<T>{stof<T>("-7.687120263558075704086300984559788078432254748391678327741697953"),stof<T>("-14.488192071346316483026338377651072600386143246861793296830825326")}, std::complex<T>{stof<T>("-17.615507745935394102840818345096835064136201461388195528260961886"),stof<T>("8.584324298882360639391283163367212198296924741406416600296859453")}, std::complex<T>{stof<T>("45.329731165513963189616948820715415230753058547167730857428348457"),stof<T>("33.446911998062466965682072841213907965343579306196733408363483264")}, std::complex<T>{stof<T>("-41.024978470615073700210068323774544434220028566552717566617134902"),stof<T>("25.031710826068782179829619406738751342025710693049350032508948328")}, std::complex<T>{stof<T>("5.5477963973201460244185816232478799154516817266589282797765971709"),stof<T>("0.5596011630320834407576285701310278015675444605778966866870027563")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_258_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_258_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("20.169288915866978759965121300858341356942001402230393024982752214"),stof<T>("32.215972030982383726121371183691864231929787980224831705591448018")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,53> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_258_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_258_DLogXconstant_part(base_point<T>, kend);
	value += f_4_258_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_258_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_258_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_258_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_258_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_258_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_258_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
