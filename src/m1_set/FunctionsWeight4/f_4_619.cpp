/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_619.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_619_abbreviated (const std::array<T,65>& abb) {
T z[147];
z[0] = 2 * abb[45];
z[1] = 2 * abb[44];
z[2] = z[0] + -z[1];
z[3] = 5 * abb[54] + -abb[58];
z[4] = abb[47] * (T(3) / T(2));
z[5] = 3 * abb[48];
z[6] = -abb[56] + z[5];
z[7] = 2 * abb[52];
z[8] = abb[49] + abb[50];
z[6] = z[2] + -z[3] + -z[4] + (T(1) / T(2)) * z[6] + z[7] + z[8];
z[6] = abb[4] * z[6];
z[9] = 6 * abb[54];
z[10] = abb[47] + -abb[48];
z[11] = -abb[56] + z[10];
z[12] = z[9] + z[11];
z[13] = 6 * abb[49];
z[14] = 6 * abb[58];
z[15] = 4 * abb[52];
z[16] = -6 * abb[50] + z[12] + -z[13] + z[14] + -z[15];
z[16] = abb[8] * z[16];
z[17] = abb[25] * abb[59];
z[18] = z[16] + -z[17];
z[19] = -abb[44] + abb[45];
z[20] = -abb[58] + z[8];
z[21] = z[19] + -z[20];
z[21] = abb[12] * z[21];
z[22] = 2 * z[21];
z[23] = z[18] + -z[22];
z[24] = 3 * abb[54];
z[25] = 2 * abb[55];
z[11] = (T(1) / T(2)) * z[11] + -z[15] + z[24] + z[25];
z[11] = abb[5] * z[11];
z[26] = abb[59] * (T(1) / T(2));
z[27] = abb[24] + abb[26];
z[28] = z[26] * z[27];
z[28] = -z[11] + z[28];
z[29] = abb[48] + -abb[56];
z[30] = (T(1) / T(2)) * z[29];
z[31] = abb[47] * (T(1) / T(2));
z[32] = z[30] + -z[31];
z[33] = 3 * abb[58];
z[34] = -z[25] + z[33];
z[35] = z[32] + z[34];
z[36] = 2 * abb[49];
z[37] = 2 * abb[50];
z[38] = z[36] + z[37];
z[39] = abb[54] + z[35] + -z[38];
z[40] = abb[7] * z[39];
z[40] = z[28] + z[40];
z[41] = 4 * abb[55];
z[42] = z[15] + -z[41];
z[43] = 2 * abb[54];
z[44] = z[10] + z[43];
z[45] = 2 * abb[58];
z[46] = -abb[56] + z[45];
z[47] = -z[38] + -z[42] + z[44] + z[46];
z[48] = abb[14] * z[47];
z[49] = -abb[47] + abb[51];
z[50] = z[8] + z[49];
z[51] = 4 * abb[44];
z[52] = 5 * abb[45] + -z[24] + z[41] + -z[50] + -z[51];
z[53] = 2 * abb[1];
z[52] = z[52] * z[53];
z[54] = -abb[48] + abb[51];
z[55] = 3 * abb[45];
z[56] = 5 * abb[44] + -z[55];
z[57] = z[25] + 2 * z[54] + -z[56];
z[57] = abb[9] * z[57];
z[58] = z[7] + -z[25];
z[8] = z[8] + z[58];
z[46] = z[8] + -z[46];
z[46] = abb[17] * z[46];
z[59] = -z[46] + z[57];
z[8] = z[8] + -z[44];
z[44] = abb[13] * z[8];
z[60] = abb[58] + z[7] + z[49];
z[61] = 3 * abb[44];
z[62] = 6 * abb[55];
z[60] = abb[45] + 2 * z[60] + -z[61] + -z[62];
z[60] = abb[0] * z[60];
z[39] = -abb[6] * z[39];
z[6] = z[6] + z[23] + z[39] + -z[40] + 5 * z[44] + z[48] + z[52] + -z[59] + z[60];
z[6] = abb[31] * z[6];
z[39] = z[1] + -z[25];
z[52] = abb[45] + -z[24] + -z[39] + z[50];
z[52] = z[52] * z[53];
z[60] = 2 * abb[51];
z[63] = abb[47] + z[29];
z[64] = z[60] + -z[63];
z[65] = -z[1] + z[64];
z[66] = abb[15] * z[65];
z[67] = abb[22] * abb[59];
z[66] = -z[66] + z[67];
z[67] = abb[56] + z[58];
z[68] = -abb[58] + z[67];
z[69] = z[19] + z[68];
z[70] = 2 * abb[0];
z[69] = z[69] * z[70];
z[52] = z[22] + z[52] + z[66] + z[69];
z[69] = abb[23] * abb[59];
z[71] = z[48] + z[69];
z[72] = 2 * z[44];
z[73] = abb[56] + z[10];
z[74] = -z[45] + z[73];
z[75] = z[43] + z[74];
z[76] = abb[7] * z[75];
z[76] = z[72] + z[76];
z[77] = z[71] + z[76];
z[78] = -abb[45] + -abb[54] + z[50];
z[79] = 2 * abb[4];
z[80] = z[78] * z[79];
z[81] = abb[6] * z[75];
z[80] = z[52] + z[77] + z[80] + z[81];
z[82] = abb[33] * z[80];
z[6] = z[6] + z[82];
z[6] = abb[31] * z[6];
z[82] = 2 * abb[46];
z[83] = z[37] + z[82];
z[84] = -abb[57] + z[83];
z[85] = abb[58] + z[19] + z[58] + -z[84];
z[85] = abb[7] * z[85];
z[86] = -abb[52] + abb[57];
z[87] = -abb[45] + abb[51] + z[20] + -z[29] + -z[86];
z[87] = abb[4] * z[87];
z[88] = abb[46] + abb[50];
z[67] = -z[2] + z[67] + -z[88];
z[67] = abb[0] * z[67];
z[89] = abb[1] * z[78];
z[90] = (T(1) / T(2)) * z[63];
z[91] = abb[44] + -abb[51];
z[92] = z[90] + z[91];
z[93] = abb[15] * z[92];
z[94] = abb[22] * z[26];
z[93] = z[93] + z[94];
z[94] = abb[54] + -abb[58];
z[95] = (T(1) / T(2)) * z[73];
z[96] = z[94] + z[95];
z[96] = abb[6] * z[96];
z[97] = -abb[57] + z[95];
z[98] = abb[52] + z[97];
z[99] = abb[16] * z[98];
z[100] = -abb[27] * z[26];
z[11] = -z[11] + z[59] + z[67] + z[85] + z[87] + z[89] + z[93] + z[96] + -z[99] + z[100];
z[11] = abb[29] * z[11];
z[59] = -abb[44] + -abb[51] + z[0];
z[67] = z[7] + z[59];
z[85] = abb[48] + abb[56];
z[87] = 3 * abb[47];
z[96] = -z[85] + z[87];
z[96] = z[67] + -z[84] + (T(1) / T(2)) * z[96];
z[96] = abb[4] * z[96];
z[53] = z[53] * z[78];
z[53] = z[53] + z[93];
z[78] = abb[27] + z[27];
z[100] = z[26] * z[78];
z[99] = z[99] + z[100];
z[100] = z[53] + z[99];
z[101] = abb[52] + z[2];
z[95] = -abb[57] + z[45] + -z[95] + -z[101];
z[95] = abb[7] * z[95];
z[102] = abb[47] + abb[56];
z[103] = abb[52] + z[102];
z[104] = z[59] + z[103];
z[105] = z[83] + -z[104];
z[105] = abb[0] * z[105];
z[95] = -z[71] + z[95] + z[96] + -z[100] + z[105];
z[95] = abb[33] * z[95];
z[47] = abb[4] * z[47];
z[96] = 3 * abb[55] + -z[7] + z[10] + z[94];
z[105] = 4 * abb[14];
z[96] = z[96] * z[105];
z[105] = 2 * z[46];
z[106] = z[81] + -z[105];
z[107] = 8 * abb[52];
z[12] = z[12] + z[41] + -z[107];
z[108] = abb[5] * z[12];
z[27] = abb[59] * z[27];
z[27] = z[27] + -z[108];
z[47] = -z[27] + -z[47] + z[76] + -z[96] + z[106];
z[76] = abb[30] * z[47];
z[109] = 3 * abb[52];
z[110] = 4 * abb[46];
z[111] = z[109] + -z[110];
z[112] = 4 * abb[58];
z[113] = 4 * abb[50];
z[114] = z[41] + z[97] + -z[111] + -z[112] + z[113];
z[114] = abb[7] * z[114];
z[115] = z[71] + z[105];
z[104] = -z[45] + z[104];
z[104] = abb[0] * z[104];
z[100] = z[100] + z[104];
z[116] = abb[48] + -3 * abb[56] + -z[87];
z[116] = abb[57] + z[45] + -z[67] + (T(1) / T(2)) * z[116];
z[116] = abb[4] * z[116];
z[114] = z[22] + z[100] + z[114] + z[115] + z[116];
z[114] = abb[34] * z[114];
z[116] = -z[36] + z[82];
z[117] = -z[7] + -z[74] + z[116];
z[118] = abb[4] * z[117];
z[119] = 2 * abb[57];
z[120] = -z[73] + z[119];
z[121] = -z[7] + z[120];
z[122] = abb[16] * z[121];
z[78] = abb[59] * z[78];
z[78] = z[78] + -z[122];
z[123] = -abb[52] + z[88];
z[124] = abb[55] + -abb[58] + z[123];
z[125] = abb[7] * z[124];
z[106] = z[71] + z[78] + -z[106] + z[118] + 4 * z[125];
z[106] = abb[32] * z[106];
z[80] = -abb[31] * z[80];
z[11] = z[11] + z[76] + z[80] + z[95] + z[106] + z[114];
z[11] = abb[29] * z[11];
z[76] = abb[45] * (T(17) / T(6));
z[80] = 2 * abb[56];
z[95] = abb[47] * (T(13) / T(2)) + z[80];
z[95] = abb[51] * (T(-13) / T(6)) + abb[58] * (T(-11) / T(3)) + abb[44] * (T(-2) / T(3)) + abb[55] * (T(31) / T(6)) + z[76] + (T(5) / T(6)) * z[88] + (T(1) / T(3)) * z[95] + -z[109];
z[95] = abb[0] * z[95];
z[90] = abb[46] + -abb[53] + z[90];
z[90] = abb[3] * z[90];
z[90] = z[66] + z[78] + z[90];
z[114] = 3 * abb[49];
z[118] = z[73] + -z[114];
z[125] = abb[46] * (T(3) / T(2));
z[118] = -abb[57] + abb[52] * (T(-4) / T(3)) + abb[54] * (T(7) / T(3)) + (T(-5) / T(6)) * z[19] + (T(1) / T(2)) * z[118] + z[125];
z[118] = abb[7] * z[118];
z[126] = abb[48] + z[102];
z[127] = -abb[46] + abb[49] * (T(1) / T(3)) + -z[126];
z[128] = abb[54] * (T(8) / T(3));
z[76] = abb[57] + abb[52] * (T(-8) / T(3)) + abb[53] * (T(1) / T(3)) + abb[51] * (T(2) / T(3)) + abb[44] * (T(13) / T(6)) + -z[76] + (T(1) / T(2)) * z[127] + z[128];
z[76] = abb[4] * z[76];
z[127] = z[17] + z[69];
z[129] = -abb[47] + z[85];
z[129] = -abb[54] + abb[52] * (T(2) / T(3)) + z[20] + (T(1) / T(6)) * z[129];
z[129] = abb[8] * z[129];
z[130] = abb[47] + -abb[49];
z[130] = abb[54] + abb[46] * (T(-7) / T(2)) + z[112] + (T(1) / T(2)) * z[130];
z[131] = abb[53] + abb[55];
z[131] = abb[50] * (T(3) / T(2)) + (T(1) / T(6)) * z[131];
z[130] = (T(1) / T(3)) * z[130] + -z[131];
z[130] = abb[6] * z[130];
z[132] = abb[49] + z[49];
z[132] = 31 * abb[44] + 17 * z[132];
z[132] = 7 * abb[54] + abb[55] * (T(-31) / T(2)) + abb[50] * (T(17) / T(2)) + (T(1) / T(2)) * z[132];
z[132] = -8 * abb[45] + (T(1) / T(3)) * z[132];
z[132] = abb[1] * z[132];
z[76] = (T(11) / T(3)) * z[21] + (T(-8) / T(3)) * z[44] + z[76] + (T(1) / T(3)) * z[90] + z[95] + z[118] + (T(5) / T(6)) * z[127] + 5 * z[129] + z[130] + z[132];
z[90] = prod_pow(m1_set::bc<T>[0], 2);
z[76] = z[76] * z[90];
z[95] = abb[56] + z[38];
z[5] = z[5] + z[15];
z[118] = 8 * abb[55] + -z[5] + -z[14] + z[43] + z[87] + z[95];
z[118] = abb[14] * z[118];
z[14] = z[14] + -z[73];
z[127] = 4 * abb[49];
z[129] = z[41] + z[127];
z[130] = z[113] + z[129];
z[132] = z[14] + z[43] + -z[130];
z[133] = abb[6] + abb[7];
z[132] = -z[132] * z[133];
z[8] = 2 * z[8];
z[8] = abb[4] * z[8];
z[8] = z[8] + z[18] + -z[27] + 4 * z[44] + -z[118] + z[132];
z[132] = -abb[31] * z[8];
z[134] = 2 * abb[53];
z[135] = z[126] + -z[134];
z[136] = -z[36] + -z[42] + z[45] + -z[135];
z[136] = abb[4] * z[136];
z[95] = -12 * abb[52] + 16 * abb[55] + z[9] + 5 * z[10] + -z[45] + -z[95];
z[95] = abb[14] * z[95];
z[137] = z[9] + z[110];
z[74] = -z[74] + z[129] + -z[137];
z[74] = abb[6] * z[74];
z[27] = z[27] + z[69];
z[63] = -z[63] + z[134];
z[129] = z[63] + -z[82];
z[138] = abb[3] * z[129];
z[74] = z[27] + z[74] + z[95] + z[105] + z[136] + -z[138];
z[74] = abb[32] * z[74];
z[95] = abb[50] + z[82];
z[85] = (T(1) / T(2)) * z[85];
z[136] = -abb[49] + z[7] + -z[31] + -z[41] + z[85] + z[94] + z[95];
z[136] = abb[4] * z[136];
z[13] = z[13] + z[62];
z[4] = z[4] + -z[134];
z[3] = 6 * abb[46] + z[3] + -z[4] + -z[13] + -z[30] + z[37];
z[3] = abb[6] * z[3];
z[3] = z[3] + -z[40] + -z[44] + -z[46] + -z[96] + z[136];
z[3] = abb[30] * z[3];
z[30] = -abb[47] + abb[53];
z[40] = abb[50] + -abb[54] + z[30];
z[96] = -z[40] * z[79];
z[136] = abb[46] + -abb[49];
z[139] = -abb[54] + abb[55];
z[140] = z[136] + -z[139];
z[140] = abb[6] * z[140];
z[77] = -z[77] + z[96] + z[138] + 4 * z[140];
z[77] = abb[33] * z[77];
z[3] = z[3] + z[74] + z[77] + z[132];
z[3] = abb[30] * z[3];
z[74] = 3 * abb[46];
z[24] = 3 * abb[50] + z[24] + -z[34] + z[74] + -z[109] + -z[120];
z[24] = abb[29] * z[24];
z[34] = z[34] + -z[73];
z[77] = -abb[52] + abb[54];
z[84] = z[77] + z[84];
z[96] = z[34] + -z[84];
z[132] = abb[34] * z[96];
z[77] = z[77] + -z[88];
z[25] = z[10] + z[25];
z[140] = z[25] + z[77];
z[141] = abb[33] * z[140];
z[96] = abb[32] * z[96];
z[132] = z[96] + z[132] + z[141];
z[142] = 2 * abb[47];
z[143] = abb[56] + -z[33] + z[41] + z[142];
z[144] = -2 * abb[48] + z[143];
z[145] = abb[30] * z[144];
z[146] = z[132] + z[145];
z[146] = z[24] + 2 * z[146];
z[146] = abb[29] * z[146];
z[34] = -z[34] + -z[77];
z[34] = abb[32] * z[34];
z[34] = z[34] + -2 * z[141];
z[34] = abb[32] * z[34];
z[77] = -abb[31] + -abb[32];
z[77] = z[77] * z[144];
z[77] = 2 * z[77] + z[145];
z[77] = abb[30] * z[77];
z[141] = prod_pow(abb[31], 2);
z[141] = -2 * abb[61] + z[141];
z[141] = z[141] * z[144];
z[145] = 2 * abb[34];
z[96] = -z[96] * z[145];
z[10] = -5 * abb[56] + -8 * z[10] + z[107];
z[10] = 5 * abb[58] + abb[55] * (T(-16) / T(3)) + abb[57] * (T(5) / T(3)) + (T(1) / T(3)) * z[10] + (T(-7) / T(3)) * z[88] + -z[128];
z[10] = z[10] * z[90];
z[25] = -z[25] + z[84];
z[84] = 2 * abb[60];
z[128] = -z[25] * z[84];
z[10] = z[10] + z[34] + z[77] + z[96] + z[128] + z[141] + z[146];
z[10] = abb[10] * z[10];
z[34] = -z[134] + z[142];
z[77] = 10 * abb[55];
z[96] = 5 * abb[46] + z[34] + z[77] + -z[80] + -z[107] + -z[114];
z[96] = abb[37] * z[96];
z[34] = -abb[49] + -z[15] + z[34] + -z[45] + z[62] + z[74];
z[34] = abb[30] * z[34];
z[62] = z[68] + -z[136];
z[68] = abb[32] + -abb[33];
z[62] = z[62] * z[68];
z[34] = z[34] + 2 * z[62];
z[34] = abb[30] * z[34];
z[62] = -abb[52] + z[30] + -z[80] + -z[95] + z[112];
z[62] = abb[32] * z[62];
z[68] = abb[50] + abb[58];
z[74] = -abb[53] + z[103];
z[80] = -z[68] + z[74] + -z[116];
z[80] = abb[33] * z[80];
z[62] = z[62] + 2 * z[80];
z[62] = abb[32] * z[62];
z[37] = abb[46] + abb[49] + z[37];
z[33] = abb[57] + -z[33] + z[37] + -z[58];
z[58] = abb[32] * z[33];
z[80] = -abb[57] + abb[58] + z[136];
z[80] = abb[33] * z[80];
z[58] = z[58] + z[80];
z[80] = -abb[34] * z[124];
z[80] = -z[58] + z[80];
z[80] = z[80] * z[145];
z[95] = -abb[29] + z[145];
z[95] = z[95] * z[124];
z[95] = z[58] + z[95];
z[103] = 2 * abb[29];
z[95] = z[95] * z[103];
z[107] = -abb[52] + abb[56] + -abb[57] + abb[49] * (T(-1) / T(2)) + -z[31] + -z[112];
z[107] = (T(1) / T(3)) * z[107] + z[125] + z[131];
z[90] = z[90] * z[107];
z[84] = z[33] * z[84];
z[30] = -abb[49] + z[30] + z[123];
z[107] = prod_pow(abb[33], 2);
z[114] = z[30] * z[107];
z[34] = z[34] + z[62] + z[80] + z[84] + z[90] + z[95] + z[96] + z[114];
z[34] = abb[2] * z[34];
z[62] = -abb[61] * z[8];
z[20] = z[20] + z[139];
z[80] = 4 * z[20];
z[84] = abb[7] * z[80];
z[80] = abb[6] * z[80];
z[48] = z[48] + z[80] + z[84];
z[18] = z[18] + z[105];
z[75] = abb[4] * z[75];
z[75] = -z[18] + -z[48] + -z[72] + z[75];
z[84] = abb[30] * z[75];
z[84] = z[84] + z[106];
z[31] = -abb[57] + z[31] + -z[36] + z[82] + z[85];
z[59] = z[31] + z[59];
z[59] = abb[4] * z[59];
z[81] = -z[81] + z[99];
z[90] = -abb[7] * z[98];
z[53] = -z[22] + -z[53] + z[59] + z[81] + z[90] + -z[104];
z[53] = abb[33] * z[53];
z[59] = abb[31] * z[75];
z[75] = z[19] + z[136];
z[90] = abb[7] * z[75];
z[19] = z[19] + z[139];
z[19] = abb[1] * z[19];
z[20] = abb[6] * z[20];
z[95] = abb[0] * z[124];
z[20] = -z[19] + z[20] + z[21] + -z[90] + z[95];
z[20] = z[20] * z[145];
z[20] = z[20] + z[53] + z[59] + -z[84];
z[20] = abb[34] * z[20];
z[53] = -z[71] + z[138];
z[4] = -abb[58] + z[4] + z[36] + z[85] + -z[123];
z[4] = abb[4] * z[4];
z[35] = abb[52] + z[35] + -z[37];
z[35] = abb[7] * z[35];
z[29] = -z[29] + z[87];
z[29] = -abb[53] + (T(1) / T(2)) * z[29] + z[43] + -z[68];
z[29] = abb[6] * z[29];
z[4] = z[4] + -z[28] + z[29] + z[35] + -z[46] + z[53];
z[4] = abb[32] * z[4];
z[28] = -abb[7] * z[117];
z[29] = z[30] * z[79];
z[30] = abb[6] * z[40];
z[28] = z[28] + z[29] + 2 * z[30] + -z[53];
z[28] = abb[33] * z[28];
z[4] = z[4] + z[28];
z[4] = abb[32] * z[4];
z[12] = abb[14] * z[12];
z[12] = z[12] + z[27];
z[28] = abb[47] + -abb[56];
z[15] = abb[51] + z[15] + -z[28];
z[15] = 2 * z[15] + -z[56] + -z[77];
z[15] = abb[0] * z[15];
z[29] = -abb[44] + z[41] + 4 * z[54] + -z[55];
z[29] = abb[9] * z[29];
z[35] = abb[4] * z[65];
z[15] = z[12] + z[15] + 6 * z[19] + z[29] + z[35] + z[66];
z[15] = abb[35] * z[15];
z[0] = -z[0] + z[38] + -z[45] + z[64];
z[0] = abb[4] * z[0];
z[9] = -z[9] + -z[45] + -z[73] + z[130];
z[9] = z[9] * z[133];
z[0] = z[0] + -z[9] + -z[18] + z[52] + z[69];
z[9] = abb[62] * z[0];
z[18] = 2 * abb[10];
z[29] = z[18] * z[144];
z[29] = z[29] + z[47];
z[29] = abb[38] * z[29];
z[35] = z[82] + -z[119] + z[135];
z[35] = abb[4] * z[35];
z[36] = abb[7] * z[121];
z[37] = -abb[6] * z[129];
z[38] = -abb[46] + -z[74] + z[119];
z[40] = 2 * abb[2];
z[38] = z[38] * z[40];
z[35] = z[35] + z[36] + z[37] + z[38] + z[78] + z[138];
z[35] = abb[36] * z[35];
z[36] = -z[42] + z[43] + z[110] + z[113];
z[14] = -z[14] + z[36];
z[14] = abb[7] * z[14];
z[37] = abb[27] * abb[59];
z[37] = z[37] + z[72] + z[108] + -z[122];
z[38] = z[79] * z[140];
z[14] = -z[14] + -z[37] + z[38] + -z[69] + z[118];
z[38] = -abb[60] * z[14];
z[13] = -z[13] + z[63] + z[137];
z[13] = abb[6] * z[13];
z[42] = -abb[4] * z[129];
z[12] = -z[12] + z[13] + z[42] + z[138];
z[12] = abb[37] * z[12];
z[13] = -abb[4] * z[75];
z[42] = abb[45] + -z[49] + -z[123];
z[42] = abb[0] * z[42];
z[13] = z[13] + -z[30] + z[42] + z[89] + z[90];
z[13] = z[13] * z[107];
z[30] = abb[18] + abb[21];
z[30] = z[30] * z[98];
z[42] = -abb[57] + (T(1) / T(2)) * z[126];
z[46] = z[42] + z[91];
z[46] = abb[19] * z[46];
z[47] = abb[20] * z[92];
z[26] = abb[28] * z[26];
z[26] = z[26] + z[30] + -z[46] + z[47];
z[30] = abb[63] * z[26];
z[3] = abb[64] + z[3] + z[4] + z[6] + z[9] + z[10] + z[11] + z[12] + z[13] + z[15] + z[20] + z[29] + z[30] + z[34] + z[35] + z[38] + z[62] + z[76];
z[4] = -6 * abb[45] + 8 * abb[54] + -z[5] + -z[45] + z[51] + z[60] + z[102];
z[4] = abb[4] * z[4];
z[5] = -z[16] + z[17] + 3 * z[21];
z[1] = z[1] + -z[7] + -z[60] + z[143];
z[1] = z[1] * z[70];
z[6] = 2 * z[57];
z[9] = abb[54] + z[39] + z[50] + -z[55];
z[9] = abb[1] * z[9];
z[1] = z[1] + z[4] + 2 * z[5] + z[6] + 6 * z[9] + z[27] + -8 * z[44] + -z[48] + z[66] + -z[105];
z[1] = abb[31] * z[1];
z[2] = -z[2] + z[36] + -z[112] + -z[120];
z[2] = abb[7] * z[2];
z[4] = 4 * z[19] + z[22];
z[5] = -z[73] + z[86] + -z[94];
z[5] = z[5] * z[79];
z[9] = -abb[58] + z[55] + -z[61] + z[88];
z[9] = z[9] * z[70];
z[2] = z[2] + z[4] + z[5] + -z[6] + z[9] + z[37] + z[115];
z[2] = abb[29] * z[2];
z[5] = z[28] + z[41] + z[45] + -z[83] + z[91] + -z[109];
z[5] = abb[0] * z[5];
z[6] = -z[7] + z[31] + z[43] + z[91];
z[6] = abb[4] * z[6];
z[7] = -z[43] + -z[97] + z[101];
z[7] = abb[7] * z[7];
z[4] = -z[4] + z[5] + z[6] + z[7] + -z[72] + z[81] + -z[93];
z[4] = abb[33] * z[4];
z[5] = -4 * abb[54] + abb[57] + z[32] + z[111] + z[127];
z[5] = abb[7] * z[5];
z[6] = z[42] + -z[43] + z[67];
z[6] = abb[4] * z[6];
z[5] = z[5] + z[6] + z[23] + -z[69] + z[72] + z[80] + -z[100];
z[5] = abb[34] * z[5];
z[1] = z[1] + z[2] + z[4] + z[5] + -z[84];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[40] * z[8];
z[0] = abb[41] * z[0];
z[4] = -abb[39] * z[14];
z[5] = -abb[31] * z[144];
z[5] = z[5] + -z[24] + -z[132];
z[5] = m1_set::bc<T>[0] * z[5];
z[6] = -abb[40] * z[144];
z[7] = -abb[39] * z[25];
z[5] = z[5] + z[6] + z[7];
z[5] = z[5] * z[18];
z[6] = abb[42] * z[26];
z[7] = abb[39] * z[33];
z[8] = z[103] + -z[145];
z[8] = z[8] * z[124];
z[8] = z[8] + -z[58];
z[8] = m1_set::bc<T>[0] * z[8];
z[7] = z[7] + z[8];
z[7] = z[7] * z[40];
z[0] = abb[43] + z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_619_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("26.768985058639030131879448466204587843194209849485869459626654332"),stof<T>("-71.09976665845057414018308566493259719417126011945365390520978474")}, std::complex<T>{stof<T>("-42.916313497125379960109039975440756951706763485975498496114291434"),stof<T>("50.097203268269536149451995402961805638110798703170824860421667433")}, std::complex<T>{stof<T>("-7.8551454723131857530691649068406368897196791591473527220537535163"),stof<T>("0.4050122534483317334704660663551477992360172553497047484794015977")}, std::complex<T>{stof<T>("-6.1952705216970842317656340123726600167601908235755293135949545717"),stof<T>("-10.748763138714442913260370660834223831865204776479269736657489719")}, std::complex<T>{stof<T>("-9.952057916789265596463957496863509091752362812914099722892682529"),stof<T>("-10.253800251466595077470719601136567724195256639803559308130627588")}, std::complex<T>{stof<T>("13.061465502593584509857636579806556036551412650418674677640419211"),stof<T>("-9.117988167330287781100243855881446087835255712796969701768699578")}, std::complex<T>{stof<T>("5.2063200302803987567884716729659191468317334912713219555866656948"),stof<T>("-8.7129759138819560476297777895262982885992384574472649532892979805")}, std::complex<T>{stof<T>("16.147328438486349828229591509236169108512553636489629036487637101"),stof<T>("21.002563390181037990731090261970791556060461416282829044788117307")}, std::complex<T>{stof<T>("11.246167422133003109107223535577817204808642390929792727410914612"),stof<T>("71.163691767514521208089033916028741722293713804574897252202436529")}, std::complex<T>{stof<T>("16.839244542299170354640254101143225377603280674039794036914430504"),stof<T>("-46.625841273145552347392710181317769269200435398040620749525899143")}, std::complex<T>{stof<T>("-24.328624569339992099049154152230193507419751075631016355027617159"),stof<T>("-25.032813381616816696485974794408628560763226543209986931203399517")}, std::complex<T>{stof<T>("1.6171009194862126262983661217973796274987872711892859372350642865"),stof<T>("-3.966150087348063883960288851182230484432100681518873173819811858")}, std::complex<T>{stof<T>("0.3770995406714526367745592174686979133639260616043059036115821508"),stof<T>("-7.0856312627672268401954367563287616384305251855206658797846966884")}, std::complex<T>{stof<T>("-1.00524996874097978809576299985933667093425600048938448283835756"),stof<T>("30.513520402711689685045874057871514243327069100966073743551296246")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[43])) - rlog(abs(kbase.W[43])), rlog(k.W[90].real()/kbase.W[90].real()), C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_619_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_619_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(16)) * (-v[3] + v[5]) * (-24 + -5 * v[3] + -7 * v[5] + 4 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((T(-1) / T(4)) * (-v[3] + v[5]) * (-v[3] + v[5] + 2 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[2] = ((-1 + 2 * m1_set::bc<T>[1]) * (T(3) / T(2)) * (-v[3] + v[5])) / tend;
c[3] = (-2 * (1 + m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return t * abb[49] * c[0] + -t * abb[53] * c[1] + abb[49] * c[2] + -abb[53] * c[3] + abb[48] * (t * c[1] + c[3]) + -abb[55] * (t * c[1] + c[3]) + abb[46] * (t * (-c[0] + c[1]) + -c[2] + c[3]);
	}
	{
T z[5];
z[0] = -abb[48] + abb[53] + abb[55];
z[1] = 3 * abb[49];
z[2] = 5 * abb[46] + -2 * z[0] + -z[1];
z[3] = prod_pow(abb[32], 2);
z[4] = prod_pow(abb[30], 2);
z[3] = z[3] + -z[4];
z[2] = z[2] * z[3];
z[0] = abb[46] + -4 * z[0] + z[1];
z[0] = abb[37] * z[0];
z[0] = z[0] + z[2];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_619_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_619_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-34.114692567472980322213382868169567916512369276807794018783148094"),stof<T>("41.870888968994921922672598254108424008015159890570568594610974157")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 43});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,65> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W91(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[43])) - rlog(abs(k.W[43])), rlog(kend.W[90].real()/k.W[90].real()), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}};
abb[43] = SpDLog_f_4_619_W_16_Im(t, path, abb);
abb[64] = SpDLog_f_4_619_W_16_Re(t, path, abb);

                    
            return f_4_619_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_619_DLogXconstant_part(base_point<T>, kend);
	value += f_4_619_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_619_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_619_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_619_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_619_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_619_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_619_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
