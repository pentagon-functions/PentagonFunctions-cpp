/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_290.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_290_abbreviated (const std::array<T,34>& abb) {
T z[56];
z[0] = abb[26] + abb[27];
z[1] = abb[24] + abb[25];
z[2] = -z[0] + z[1];
z[3] = 2 * abb[23];
z[4] = 3 * abb[28];
z[5] = z[3] + -z[4];
z[6] = -abb[19] + abb[22];
z[7] = z[5] + z[6];
z[8] = abb[20] + abb[21];
z[9] = z[2] + z[7] + z[8];
z[9] = abb[0] * z[9];
z[10] = -z[5] + z[6];
z[2] = z[2] + -z[8] + z[10];
z[2] = abb[3] * z[2];
z[11] = -abb[21] + abb[26];
z[12] = 2 * abb[24];
z[13] = -z[7] + 2 * z[11] + -z[12];
z[14] = abb[7] * z[13];
z[15] = abb[20] + abb[26];
z[16] = -z[10] + -z[12] + 2 * z[15];
z[17] = abb[5] * z[16];
z[18] = abb[19] + abb[22];
z[19] = -z[4] + z[18];
z[20] = -abb[20] + abb[27];
z[21] = z[12] + z[19] + 2 * z[20];
z[21] = abb[1] * z[21];
z[22] = abb[25] + abb[26];
z[23] = abb[27] + z[22];
z[24] = abb[24] + z[23];
z[25] = z[18] + z[24];
z[26] = 4 * abb[23];
z[27] = -9 * abb[28] + z[8] + z[25] + z[26];
z[28] = abb[9] * z[27];
z[29] = 6 * abb[28];
z[3] = -z[3] + z[29];
z[25] = z[3] + -z[25];
z[30] = 2 * abb[4];
z[30] = z[25] * z[30];
z[31] = abb[21] + abb[27];
z[32] = -abb[25] + z[31];
z[10] = -z[10] + 2 * z[32];
z[33] = abb[6] * z[10];
z[34] = -abb[25] + z[20];
z[7] = -z[7] + 2 * z[34];
z[35] = abb[8] * z[7];
z[22] = -abb[21] + z[22];
z[19] = z[19] + 2 * z[22];
z[19] = abb[2] * z[19];
z[2] = -z[2] + z[9] + -z[14] + z[17] + z[19] + z[21] + -3 * z[28] + -z[30] + z[33] + -z[35];
z[9] = abb[33] * z[2];
z[36] = -abb[19] + abb[24];
z[37] = 2 * abb[25];
z[38] = z[4] + -z[15] + -z[36] + -z[37];
z[38] = abb[0] * z[38];
z[39] = 2 * abb[19];
z[40] = -z[29] + z[39];
z[41] = abb[22] + -abb[25] + abb[27];
z[42] = abb[21] + z[26];
z[43] = z[40] + z[41] + z[42];
z[43] = abb[6] * z[43];
z[44] = 2 * abb[22];
z[45] = abb[19] + z[44];
z[46] = -z[29] + z[45];
z[42] = abb[24] + -abb[26] + z[42] + z[46];
z[42] = abb[7] * z[42];
z[47] = abb[22] + z[4];
z[48] = abb[20] + 2 * abb[26];
z[49] = -abb[25] + -abb[27] + z[47] + -z[48];
z[49] = abb[3] * z[49];
z[38] = 2 * z[19] + z[28] + z[38] + -z[42] + -z[43] + z[49];
z[38] = prod_pow(abb[11], 2) * z[38];
z[49] = 2 * abb[9];
z[27] = z[27] * z[49];
z[27] = z[27] + z[30];
z[19] = z[19] + -z[27];
z[30] = 2 * abb[20] + abb[21] + -z[3];
z[44] = -abb[19] + z[44];
z[49] = 3 * abb[26] + -z[44];
z[50] = 2 * abb[27];
z[51] = -abb[24] + z[30] + z[49] + z[50];
z[51] = abb[3] * z[51];
z[52] = z[5] + z[39];
z[53] = abb[21] + abb[24];
z[54] = abb[22] + abb[26];
z[55] = -z[52] + z[53] + -z[54];
z[55] = abb[7] * z[55];
z[13] = abb[0] * z[13];
z[13] = -z[13] + z[17] + z[19] + z[51] + z[55];
z[17] = abb[31] * z[13];
z[11] = -z[11] + z[36];
z[11] = abb[7] * z[11];
z[36] = -abb[22] + z[32];
z[36] = abb[6] * z[36];
z[34] = -abb[19] + -z[34];
z[34] = abb[8] * z[34];
z[15] = -abb[24] + z[15];
z[51] = -abb[22] + z[15];
z[51] = abb[5] * z[51];
z[11] = z[11] + z[34] + z[36] + z[51];
z[34] = abb[28] * (T(-3) / T(2)) + (T(1) / T(2)) * z[18];
z[22] = z[22] + z[34];
z[22] = abb[2] * z[22];
z[20] = abb[24] + z[20] + z[34];
z[20] = abb[1] * z[20];
z[20] = z[20] + z[22];
z[22] = z[18] + z[23];
z[23] = -z[8] + -z[22];
z[34] = abb[24] * (T(1) / T(3));
z[23] = abb[23] * (T(-4) / T(3)) + z[4] + (T(1) / T(3)) * z[23] + -z[34];
z[23] = abb[9] * z[23];
z[22] = -2 * abb[28] + abb[23] * (T(2) / T(3)) + (T(1) / T(3)) * z[22] + z[34];
z[22] = abb[4] * z[22];
z[22] = z[22] + z[23];
z[23] = 7 * abb[28];
z[34] = 13 * abb[25] + z[8];
z[34] = 7 * abb[22] + abb[19] * (T(-7) / T(3)) + -5 * z[0] + -z[23] + (T(1) / T(3)) * z[34];
z[36] = abb[23] * (T(8) / T(3));
z[34] = abb[24] * (T(13) / T(6)) + (T(1) / T(2)) * z[34] + z[36];
z[34] = abb[0] * z[34];
z[23] = 7 * abb[19] + -5 * abb[25] + abb[22] * (T(-7) / T(3)) + (T(13) / T(3)) * z[0] + (T(1) / T(3)) * z[8] + -z[23];
z[23] = abb[24] * (T(-5) / T(2)) + (T(1) / T(2)) * z[23] + z[36];
z[23] = abb[3] * z[23];
z[11] = (T(5) / T(2)) * z[11] + 3 * z[20] + 8 * z[22] + z[23] + z[34];
z[11] = prod_pow(m1_set::bc<T>[0], 2) * z[11];
z[20] = -abb[22] + z[39];
z[22] = 3 * abb[25] + -z[20];
z[23] = -abb[27] + z[12] + z[22] + z[30];
z[23] = abb[0] * z[23];
z[5] = z[5] + z[45];
z[30] = z[5] + -z[32];
z[30] = abb[6] * z[30];
z[10] = abb[3] * z[10];
z[10] = z[10] + z[19] + z[23] + -z[30] + -z[35];
z[19] = abb[30] * z[10];
z[23] = 3 * abb[27];
z[30] = 2 * abb[21] + -abb[25] + -z[3] + z[23] + -z[44] + z[48];
z[30] = abb[3] * z[30];
z[27] = z[21] + -z[27];
z[32] = abb[20] + -z[41] + -z[52];
z[32] = abb[8] * z[32];
z[7] = abb[0] * z[7];
z[7] = -z[7] + z[27] + z[30] + z[32] + z[33];
z[30] = abb[32] * z[7];
z[26] = abb[20] + z[26];
z[32] = abb[25] + -abb[27] + z[26] + z[46];
z[32] = abb[8] * z[32];
z[33] = -z[28] + z[32];
z[26] = -abb[24] + z[26] + z[40] + z[54];
z[26] = abb[5] * z[26];
z[34] = -abb[26] + z[47] + -z[50] + -z[53];
z[34] = abb[3] * z[34];
z[12] = -z[4] + z[12];
z[31] = abb[19] + -abb[25] + -z[12] + -z[31];
z[31] = abb[0] * z[31];
z[21] = 2 * z[21] + -z[26] + z[31] + -z[33] + z[34];
z[21] = prod_pow(abb[13], 2) * z[21];
z[31] = abb[21] + abb[25];
z[34] = 3 * abb[24];
z[3] = -abb[20] + abb[26] + z[3] + z[20] + -2 * z[31] + -z[34];
z[3] = abb[0] * z[3];
z[16] = abb[3] * z[16];
z[5] = -z[5] + z[15];
z[5] = abb[5] * z[5];
z[3] = z[3] + -z[5] + z[14] + -z[16] + -z[27];
z[5] = -abb[29] * z[3];
z[14] = abb[4] * z[25];
z[15] = -z[4] + z[8];
z[16] = abb[23] + z[15];
z[1] = -abb[19] + z[1];
z[20] = z[1] + z[16];
z[20] = abb[0] * z[20];
z[25] = -abb[22] + z[0];
z[16] = z[16] + z[25];
z[16] = abb[3] * z[16];
z[16] = -z[14] + z[16] + z[20] + -z[28];
z[16] = abb[12] * z[16];
z[20] = z[26] + z[43];
z[8] = abb[23] + z[8] + -z[29];
z[22] = -z[8] + -z[22] + -z[34];
z[22] = abb[0] * z[22];
z[1] = -abb[23] + z[1];
z[1] = abb[3] * z[1];
z[1] = z[1] + z[14] + z[20] + z[22];
z[1] = abb[10] * z[1];
z[1] = z[1] + 2 * z[16];
z[1] = abb[10] * z[1];
z[8] = -z[8] + -z[23] + -z[49];
z[8] = abb[3] * z[8];
z[16] = -abb[23] + z[25];
z[16] = abb[0] * z[16];
z[8] = z[8] + z[14] + z[16] + z[32] + z[42];
z[8] = prod_pow(abb[12], 2) * z[8];
z[1] = z[1] + z[5] + z[8] + z[9] + z[11] + z[17] + z[19] + z[21] + z[30] + z[38];
z[2] = abb[18] * z[2];
z[5] = abb[16] * z[13];
z[7] = abb[17] * z[7];
z[8] = z[6] + z[12] + z[37];
z[8] = abb[0] * z[8];
z[9] = -z[15] + z[18] + -z[24];
z[11] = abb[3] * z[9];
z[8] = z[8] + z[11] + -z[20] + z[28];
z[8] = abb[10] * z[8];
z[9] = abb[0] * z[9];
z[0] = 2 * z[0] + -z[4] + -z[6];
z[0] = abb[3] * z[0];
z[0] = z[0] + z[9] + -z[33] + -z[42];
z[0] = abb[12] * z[0];
z[0] = z[0] + z[8];
z[0] = m1_set::bc<T>[0] * z[0];
z[4] = abb[15] * z[10];
z[3] = -abb[14] * z[3];
z[0] = 2 * z[0] + z[2] + z[3] + z[4] + z[5] + z[7];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_290_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("14.287050808916868576895900359562792643031938255059196993965157613"),stof<T>("-9.490014446657709032391385949738965204423357251949299301454141377")}, std::complex<T>{stof<T>("15.839830254606910118473915625522640742850827125433561655266293519"),stof<T>("5.724517080325883961316843887146793206757856374984065277392731427")}, std::complex<T>{stof<T>("15.839830254606910118473915625522640742850827125433561655266293519"),stof<T>("5.724517080325883961316843887146793206757856374984065277392731427")}, std::complex<T>{stof<T>("1.616483592786806714756013661995299311233979595995286458501960366"),stof<T>("-28.313663431519988353625562654714154344673355864715314138120392912")}, std::complex<T>{stof<T>("47.583194910917495528599745272603373439967572101921606762999705017"),stof<T>("-26.354643717525929463383260830159533135581000366696482884789071436")}, std::complex<T>{stof<T>("37.366706619853354988123836104091645698758143962349984433057173438"),stof<T>("5.067333188985623080319983712071190893644035275027662266103612672")}, std::complex<T>{stof<T>("37.366706619853354988123836104091645698758143962349984433057173438"),stof<T>("5.067333188985623080319983712071190893644035275027662266103612672")}, std::complex<T>{stof<T>("50.037273835983416850263722801659139030556102621413894968520370685"),stof<T>("23.890982173847902401554160417046380033894033887793677102769864207")}, std::complex<T>{stof<T>("50.037273835983416850263722801659139030556102621413894968520370685"),stof<T>("23.890982173847902401554160417046380033894033887793677102769864207")}, std::complex<T>{stof<T>("-202.48076305013140105048095626753123725392272802852822924686587371"),stof<T>("-3.90550746796139402773632494843705668793560319418728472612660817")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_290_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_290_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("51.46145489601409477637204003193404003024543385602414588468359803"),stof<T>("-200.89940145534890533739206389864569682954080296339444949438606379")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,34> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_290_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_290_DLogXconstant_part(base_point<T>, kend);
	value += f_4_290_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_290_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_290_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_290_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_290_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_290_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_290_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
