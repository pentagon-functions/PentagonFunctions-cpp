/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_36.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_36_abbreviated (const std::array<T,12>& abb) {
T z[6];
z[0] = -abb[9] + abb[10];
z[1] = -abb[3] * z[0];
z[2] = abb[2] * abb[11];
z[1] = z[1] + -z[2];
z[2] = abb[4] * z[0];
z[3] = abb[1] * abb[11];
z[2] = z[2] + -z[3];
z[3] = -z[1] + z[2];
z[3] = abb[5] * z[3];
z[4] = abb[0] * abb[11];
z[5] = z[1] + z[4];
z[5] = abb[6] * z[5];
z[3] = z[3] + z[5];
z[1] = z[1] + -z[4];
z[1] = (T(1) / T(2)) * z[1] + -z[2];
z[1] = abb[7] * z[1];
z[1] = z[1] + (T(1) / T(2)) * z[3];
z[1] = abb[5] * z[1];
z[2] = z[2] + z[4];
z[2] = abb[7] * z[2];
z[2] = z[2] + -z[5];
z[2] = abb[7] * z[2];
z[3] = -abb[0] + abb[2];
z[0] = z[0] * z[3];
z[3] = abb[3] * abb[11];
z[0] = z[0] + -3 * z[3];
z[0] = abb[8] * z[0];
z[0] = z[0] + z[2];
z[0] = (T(1) / T(2)) * z[0] + z[1];
z[0] = (T(1) / T(2)) * z[0];
return {z[0], 0};
}


template <typename T> std::complex<T> f_4_36_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {stof<T>("1.2193658358940369403497795882120816275051541626415601511882206489"), stof<T>("-1.2193658358940369403497795882120816275051541626415601511882206489"), stof<T>("6.931753941494592065707268505023142731001535259108518993644704445")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[117].real()/kbase.W[117].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_36_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_36_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = stof<T>("6.2734268739690245483600412157455172047293809962401506191494911169");
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,12> abb = {dl[0], dl[2], dl[4], dlog_W118(k,dv), dlog_W123(k,dv), f_1_1(k), f_1_5(k), f_1_6(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[117].real()/k.W[117].real())};

                    
            return f_4_36_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_36_DLogXconstant_part(base_point<T>, kend);
	value += f_4_36_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_36_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_36_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_36_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_36_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_36_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_36_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
