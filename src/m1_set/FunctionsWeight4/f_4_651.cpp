/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_651.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_651_abbreviated (const std::array<T,73>& abb) {
T z[169];
z[0] = abb[53] * (T(1) / T(2));
z[1] = 2 * abb[59];
z[2] = z[0] + -z[1];
z[3] = abb[57] * (T(2) / T(3));
z[4] = abb[52] * (T(4) / T(3));
z[5] = z[3] + z[4];
z[6] = abb[51] + -abb[56];
z[7] = 5 * abb[54];
z[8] = abb[58] * (T(3) / T(2));
z[9] = abb[50] * (T(5) / T(2));
z[10] = abb[49] * (T(-7) / T(2)) + abb[55] * (T(9) / T(2)) + -z[2] + -z[5] + (T(-11) / T(3)) * z[6] + -z[7] + -z[8] + -z[9];
z[10] = abb[9] * z[10];
z[11] = abb[53] + -abb[59];
z[12] = abb[54] * (T(5) / T(2));
z[13] = abb[48] * (T(35) / T(12));
z[14] = 4 * abb[56];
z[15] = abb[50] * (T(29) / T(4)) + z[14];
z[5] = abb[58] * (T(-15) / T(4)) + abb[49] * (T(5) / T(2)) + abb[51] * (T(7) / T(3)) + abb[55] * (T(10) / T(3)) + -z[5] + (T(-4) / T(3)) * z[11] + -z[12] + z[13] + (T(1) / T(3)) * z[15];
z[5] = abb[3] * z[5];
z[11] = 2 * abb[56];
z[12] = abb[52] + abb[50] * (T(35) / T(4)) + -z[11] + -z[12];
z[15] = 4 * abb[51];
z[3] = abb[58] * (T(-17) / T(12)) + abb[55] * (T(-5) / T(2)) + abb[49] * (T(11) / T(6)) + z[3] + (T(1) / T(3)) * z[12] + z[13] + z[15];
z[3] = abb[0] * z[3];
z[12] = abb[51] * (T(4) / T(3));
z[13] = abb[49] * (T(11) / T(3));
z[7] = abb[50] * (T(-1) / T(6)) + z[7];
z[4] = abb[55] * (T(-59) / T(12)) + abb[53] * (T(-7) / T(6)) + abb[48] * (T(-5) / T(2)) + abb[58] * (T(29) / T(6)) + z[4] + (T(1) / T(2)) * z[7] + -z[12] + -z[13];
z[4] = abb[13] * z[4];
z[7] = abb[50] + abb[51];
z[16] = 4 * abb[49];
z[17] = abb[52] + abb[57];
z[18] = abb[48] + z[17];
z[19] = 2 * z[7] + z[16] + -z[18];
z[20] = abb[15] * z[19];
z[21] = abb[62] + abb[63];
z[22] = abb[22] * z[21];
z[20] = z[20] + -z[22];
z[23] = -abb[57] + z[6];
z[24] = abb[52] * (T(1) / T(2));
z[23] = abb[49] + (T(1) / T(2)) * z[23] + z[24];
z[23] = abb[5] * z[23];
z[25] = abb[48] + abb[52];
z[26] = -abb[57] + z[25];
z[27] = abb[16] * z[26];
z[28] = z[20] + z[23] + z[27];
z[29] = -abb[56] + z[17];
z[30] = -z[7] + z[29];
z[31] = 2 * abb[58];
z[32] = -abb[53] + z[31];
z[33] = 2 * abb[54];
z[34] = z[30] + -z[32] + z[33];
z[34] = abb[8] * z[34];
z[35] = abb[25] * z[21];
z[36] = abb[23] * z[21];
z[34] = z[34] + -z[35] + -z[36];
z[35] = abb[59] * (T(2) / T(3));
z[36] = 3 * abb[48];
z[37] = 3 * abb[50];
z[38] = abb[52] * (T(-17) / T(6)) + abb[56] * (T(-7) / T(6)) + abb[57] * (T(-5) / T(6)) + abb[49] * (T(4) / T(3)) + abb[51] * (T(25) / T(6)) + z[35] + z[36] + z[37];
z[38] = abb[4] * z[38];
z[39] = abb[57] * (T(1) / T(2));
z[40] = abb[48] + z[39];
z[13] = abb[52] * (T(-5) / T(6)) + abb[59] * (T(1) / T(3)) + (T(-11) / T(6)) * z[6] + -z[13] + z[40];
z[13] = abb[7] * z[13];
z[41] = abb[53] + abb[55];
z[42] = -abb[50] + -8 * abb[54];
z[31] = abb[51] + -abb[59] + abb[48] * (T(4) / T(3)) + abb[49] * (T(8) / T(3)) + -z[31] + (T(5) / T(3)) * z[41] + (T(1) / T(3)) * z[42];
z[31] = abb[17] * z[31];
z[42] = abb[20] + abb[21];
z[43] = abb[18] + abb[19];
z[44] = (T(1) / T(3)) * z[42] + -z[43];
z[45] = abb[60] + abb[61];
z[44] = -z[44] * z[45];
z[46] = abb[24] + abb[26];
z[47] = (T(1) / T(2)) * z[21];
z[48] = -z[46] * z[47];
z[12] = abb[55] * (T(-11) / T(3)) + abb[56] * (T(-7) / T(3)) + abb[48] * (T(-7) / T(4)) + abb[57] * (T(1) / T(3)) + abb[49] * (T(1) / T(6)) + abb[50] * (T(3) / T(4)) + abb[54] * (T(7) / T(2)) + abb[58] * (T(13) / T(4)) + z[12] + -z[35];
z[12] = abb[1] * z[12];
z[35] = -abb[50] + abb[56] * (T(1) / T(2));
z[49] = abb[48] + z[35];
z[50] = abb[51] * (T(1) / T(2));
z[49] = -abb[59] + abb[57] * (T(1) / T(6)) + abb[52] * (T(7) / T(6)) + (T(1) / T(3)) * z[49] + -z[50];
z[49] = abb[6] * z[49];
z[51] = -abb[48] + abb[58];
z[52] = -abb[55] + z[51];
z[53] = abb[2] * z[52];
z[54] = -abb[49] + abb[58];
z[55] = -abb[53] + z[54];
z[56] = abb[14] * z[55];
z[57] = -abb[53] + z[51];
z[58] = abb[10] * z[57];
z[3] = z[3] + z[4] + z[5] + z[10] + z[12] + z[13] + (T(1) / T(3)) * z[28] + z[31] + (T(4) / T(3)) * z[34] + z[38] + z[44] + z[48] + z[49] + (T(1) / T(4)) * z[53] + (T(11) / T(6)) * z[56] + (T(5) / T(2)) * z[58];
z[4] = prod_pow(m1_set::bc<T>[0], 2);
z[3] = z[3] * z[4];
z[5] = abb[50] + -abb[56];
z[5] = (T(1) / T(2)) * z[5];
z[10] = z[5] + z[50];
z[0] = abb[54] + z[0];
z[12] = abb[52] * (T(3) / T(2));
z[13] = abb[59] + z[0] + z[10] + -z[12] + -z[39];
z[13] = abb[3] * z[13];
z[28] = abb[50] + abb[56];
z[28] = (T(1) / T(2)) * z[28] + z[50];
z[31] = z[24] + z[39];
z[0] = abb[58] + -z[0] + z[28] + -z[31];
z[0] = abb[8] * z[0];
z[38] = abb[48] + abb[59];
z[44] = 2 * abb[51];
z[48] = abb[50] + z[44];
z[49] = -abb[56] + z[48];
z[50] = z[16] + -z[38] + z[49];
z[50] = abb[4] * z[50];
z[59] = 2 * abb[49];
z[60] = z[7] + z[59];
z[61] = abb[48] * (T(1) / T(2));
z[62] = z[31] + z[61];
z[63] = z[60] + -z[62];
z[64] = -abb[15] * z[63];
z[65] = (T(1) / T(2)) * z[45];
z[66] = z[42] * z[65];
z[67] = 3 * abb[49];
z[68] = z[49] + z[67];
z[69] = -abb[59] + z[68];
z[70] = 2 * abb[1];
z[71] = z[69] * z[70];
z[72] = abb[23] + abb[25];
z[73] = abb[22] + z[46];
z[74] = z[72] + z[73];
z[74] = z[47] * z[74];
z[75] = 2 * abb[52];
z[76] = abb[57] + z[75];
z[77] = 3 * abb[59];
z[78] = z[76] + -z[77];
z[79] = abb[50] + z[33];
z[80] = -abb[49] + -abb[58] + -z[78] + z[79];
z[80] = abb[9] * z[80];
z[81] = abb[57] + z[6];
z[82] = z[54] + -z[81];
z[82] = abb[0] * z[82];
z[83] = -abb[57] + abb[59];
z[84] = abb[7] * z[83];
z[85] = (T(1) / T(2)) * z[27];
z[13] = z[0] + z[13] + z[50] + -z[58] + z[64] + z[66] + z[71] + z[74] + z[80] + z[82] + z[84] + -z[85];
z[13] = abb[31] * z[13];
z[50] = z[6] + z[59];
z[64] = z[24] + z[50];
z[66] = abb[57] * (T(3) / T(2));
z[74] = -z[1] + -z[61] + z[64] + z[66];
z[74] = abb[7] * z[74];
z[80] = z[1] + -z[17];
z[82] = -z[50] + z[80];
z[86] = abb[17] * z[82];
z[87] = abb[28] * z[21];
z[86] = z[86] + -z[87];
z[88] = z[85] + z[86];
z[89] = z[6] + z[67];
z[90] = z[78] + z[89];
z[91] = 2 * abb[9];
z[92] = z[90] * z[91];
z[93] = abb[1] * z[69];
z[94] = 4 * z[93];
z[92] = z[92] + -z[94];
z[95] = 8 * abb[49];
z[96] = -z[11] + z[95];
z[97] = 5 * abb[51];
z[98] = z[37] + z[96] + z[97];
z[99] = 4 * abb[59];
z[100] = -z[62] + -z[98] + z[99];
z[100] = abb[4] * z[100];
z[101] = 2 * abb[50];
z[102] = 3 * abb[51];
z[103] = -abb[56] + z[101] + z[102];
z[104] = abb[48] + z[103];
z[105] = z[59] + -z[75] + z[104];
z[106] = -abb[3] * z[105];
z[107] = abb[20] + -abb[21];
z[108] = 3 * abb[19];
z[109] = abb[18] + z[107] + z[108];
z[109] = -z[65] * z[109];
z[110] = z[1] + -z[59];
z[111] = -abb[57] + -z[7] + z[110];
z[111] = abb[0] * z[111];
z[74] = z[74] + -z[88] + z[92] + z[100] + z[106] + z[109] + z[111];
z[74] = abb[34] * z[74];
z[100] = abb[48] * (T(3) / T(2));
z[106] = z[24] + -z[39] + -z[50] + z[100];
z[106] = abb[7] * z[106];
z[109] = -z[71] + z[106];
z[111] = z[11] + -z[16];
z[102] = -abb[50] + -z[102] + z[111];
z[112] = -z[1] + z[62] + -z[102];
z[112] = abb[4] * z[112];
z[113] = 3 * abb[18];
z[114] = -abb[19] + z[107] + -z[113];
z[114] = z[65] * z[114];
z[115] = -abb[48] + z[50];
z[116] = abb[3] * z[115];
z[117] = -abb[57] + z[7];
z[118] = 2 * abb[48];
z[119] = -z[117] + -z[118];
z[119] = abb[0] * z[119];
z[88] = z[88] + -z[109] + z[112] + z[114] + z[116] + z[119];
z[88] = abb[35] * z[88];
z[90] = abb[9] * z[90];
z[112] = 2 * abb[4];
z[69] = z[69] * z[112];
z[90] = -z[69] + z[90];
z[114] = 3 * abb[52];
z[119] = -z[1] + z[114];
z[120] = abb[57] + -z[103];
z[121] = -z[59] + z[119] + z[120];
z[122] = abb[3] * z[121];
z[80] = -abb[48] + z[80];
z[123] = abb[7] * z[80];
z[124] = z[86] + z[123];
z[125] = abb[18] + -abb[20];
z[126] = z[45] * z[125];
z[127] = abb[48] + z[6];
z[128] = abb[0] * z[127];
z[126] = z[126] + z[128];
z[90] = -2 * z[90] + 6 * z[93] + -z[122] + z[124] + -z[126];
z[90] = abb[36] * z[90];
z[93] = 2 * abb[57];
z[122] = -z[1] + z[93] + z[115];
z[122] = abb[0] * z[122];
z[128] = abb[3] * z[82];
z[129] = abb[4] * z[80];
z[130] = -z[86] + z[129];
z[131] = abb[19] + -abb[21];
z[132] = z[45] * z[131];
z[122] = z[27] + z[122] + -z[128] + -z[130] + z[132];
z[122] = abb[32] * z[122];
z[128] = abb[36] * z[19];
z[132] = abb[35] * z[63];
z[128] = z[128] + -z[132];
z[63] = abb[34] * z[63];
z[63] = z[63] + z[128];
z[63] = abb[15] * z[63];
z[63] = z[63] + z[122];
z[132] = abb[36] * z[21];
z[133] = abb[35] * z[47];
z[132] = z[132] + -z[133];
z[134] = abb[34] * z[47];
z[135] = z[132] + z[134];
z[73] = -z[73] * z[135];
z[13] = z[13] + z[63] + z[73] + z[74] + z[88] + -z[90];
z[13] = abb[31] * z[13];
z[73] = abb[58] + z[11];
z[74] = 5 * abb[49];
z[88] = -z[33] + -z[48] + z[73] + -z[74] + -z[78];
z[88] = abb[9] * z[88];
z[136] = abb[51] * (T(3) / T(2));
z[137] = -abb[54] + z[136];
z[138] = abb[58] * (T(1) / T(2));
z[5] = -z[5] + -z[62] + z[110] + -z[137] + -z[138];
z[5] = abb[17] * z[5];
z[44] = -z[44] + z[75];
z[138] = -abb[49] + z[138];
z[139] = -abb[54] + abb[50] * (T(3) / T(2));
z[140] = abb[59] + -z[44] + z[61] + -z[138] + z[139];
z[140] = abb[3] * z[140];
z[141] = -abb[48] + z[33];
z[142] = z[37] + -z[141];
z[143] = 6 * abb[51];
z[96] = abb[57] + z[96] + -z[99] + z[142] + z[143];
z[96] = abb[1] * z[96];
z[49] = 2 * z[49];
z[144] = 6 * abb[49] + z[49];
z[145] = z[62] + -z[77] + z[144];
z[145] = abb[4] * z[145];
z[10] = abb[54] + z[10] + -z[138];
z[10] = abb[0] * z[10];
z[146] = abb[28] * z[47];
z[147] = abb[19] * z[65];
z[5] = z[5] + z[10] + z[23] + z[84] + z[88] + z[96] + z[140] + z[145] + -z[146] + z[147];
z[5] = abb[36] * z[5];
z[10] = z[70] * z[83];
z[83] = abb[0] * z[115];
z[88] = abb[19] * z[45];
z[83] = z[83] + z[88];
z[88] = abb[52] + -abb[57];
z[96] = z[50] + z[88];
z[140] = abb[5] * z[96];
z[10] = -z[10] + z[83] + -z[130] + z[140];
z[10] = abb[35] * z[10];
z[5] = z[5] + -z[10];
z[5] = abb[36] * z[5];
z[130] = 3 * abb[58];
z[2] = -abb[49] + abb[55] * (T(-7) / T(2)) + z[2] + z[12] + (T(1) / T(2)) * z[81] + z[130];
z[2] = abb[3] * z[2];
z[35] = abb[52] * (T(-7) / T(2)) + -z[35] + -z[40] + z[77] + z[136];
z[40] = abb[6] * z[35];
z[77] = -abb[53] + z[139];
z[44] = z[44] + -z[77];
z[136] = -abb[48] + z[130];
z[145] = abb[55] * (T(3) / T(2));
z[147] = -z[44] + z[136] + -z[145];
z[148] = -abb[13] * z[147];
z[32] = -abb[55] + z[32];
z[149] = -abb[59] + z[7] + z[32];
z[149] = abb[17] * z[149];
z[84] = z[84] + z[149];
z[72] = z[46] + z[72];
z[72] = z[47] * z[72];
z[149] = 2 * z[56];
z[150] = -abb[52] + abb[55];
z[150] = abb[0] * z[150];
z[151] = -abb[57] + -abb[58] + z[1];
z[152] = abb[1] * z[151];
z[0] = z[0] + z[2] + -z[23] + z[40] + z[72] + z[84] + z[148] + -z[149] + z[150] + z[152];
z[0] = abb[33] * z[0];
z[2] = z[43] * z[45];
z[23] = z[32] + z[78];
z[40] = z[23] * z[91];
z[2] = z[2] + z[40] + -z[129];
z[40] = 2 * abb[55];
z[72] = -abb[57] + z[40] + -z[50] + z[57] + -z[119];
z[72] = abb[3] * z[72];
z[78] = 2 * abb[53];
z[129] = 5 * abb[58] + -z[78];
z[142] = -z[40] + z[129] + z[142];
z[148] = z[15] + -z[99];
z[150] = z[59] + z[142] + z[148];
z[150] = abb[17] * z[150];
z[51] = z[33] + z[51];
z[152] = z[48] + z[59];
z[153] = z[51] + -z[152];
z[154] = abb[0] * z[153];
z[72] = -z[2] + z[34] + z[72] + z[123] + z[150] + z[154];
z[72] = abb[31] * z[72];
z[154] = -z[1] + z[75];
z[155] = abb[48] + abb[49] + z[154];
z[155] = abb[3] * z[155];
z[156] = -abb[59] + z[25];
z[157] = z[112] * z[156];
z[155] = z[155] + z[157];
z[105] = abb[0] * z[105];
z[157] = abb[18] * z[45];
z[105] = z[105] + z[124] + 2 * z[155] + -z[157];
z[124] = 7 * abb[52];
z[155] = 6 * abb[59];
z[158] = z[124] + -z[155];
z[159] = z[118] + z[120] + z[158];
z[160] = abb[6] * z[159];
z[161] = z[21] * z[46];
z[162] = -z[160] + z[161];
z[163] = -z[105] + -z[162];
z[163] = abb[34] * z[163];
z[23] = abb[9] * z[23];
z[164] = -abb[49] + abb[55];
z[165] = -abb[59] + z[75] + -z[164];
z[165] = abb[3] * z[165];
z[166] = -abb[58] + abb[59];
z[166] = abb[1] * z[166];
z[23] = z[23] + z[56] + -z[84] + z[165] + z[166];
z[23] = abb[36] * z[23];
z[0] = z[0] + z[10] + 2 * z[23] + z[72] + z[163];
z[0] = abb[33] * z[0];
z[9] = 3 * abb[54] + abb[58] * (T(-7) / T(2)) + -z[9] + z[41] + -z[67] + -z[100] + -z[148];
z[9] = abb[17] * z[9];
z[10] = 4 * abb[52];
z[23] = z[10] + z[93] + -z[155];
z[11] = -z[11] + z[152];
z[72] = 4 * abb[58];
z[84] = 4 * abb[55];
z[148] = -abb[53] + z[11] + z[23] + z[33] + z[72] + -z[84];
z[148] = abb[9] * z[148];
z[138] = z[61] + z[138];
z[163] = -abb[56] + abb[57];
z[165] = -abb[51] + z[163];
z[77] = abb[55] + z[77] + -z[119] + z[138] + -z[165];
z[77] = abb[3] * z[77];
z[166] = -z[40] + z[51];
z[167] = z[11] + z[166];
z[167] = abb[1] * z[167];
z[82] = abb[7] * z[82];
z[121] = abb[4] * z[121];
z[168] = z[34] + z[58];
z[138] = abb[54] + -abb[55] + abb[50] * (T(1) / T(2)) + z[138];
z[138] = abb[0] * z[138];
z[9] = -z[9] + z[77] + z[82] + -z[121] + z[138] + -z[148] + z[167] + z[168];
z[77] = z[75] + -z[152] + z[166];
z[82] = abb[13] * z[77];
z[121] = z[9] + z[82];
z[121] = abb[68] * z[121];
z[138] = -z[42] + z[108] + z[113];
z[138] = z[65] * z[138];
z[62] = z[62] + z[102];
z[62] = abb[4] * z[62];
z[48] = z[48] + z[163];
z[110] = abb[48] + z[48] + -z[110];
z[110] = abb[0] * z[110];
z[80] = -abb[3] * z[80];
z[62] = z[62] + z[80] + z[85] + z[109] + z[110] + z[138];
z[62] = abb[35] * z[62];
z[80] = abb[32] * z[105];
z[105] = -z[1] + z[16];
z[104] = -z[104] + -z[105];
z[104] = abb[0] * z[104];
z[109] = -abb[48] + abb[49];
z[109] = z[109] * z[112];
z[110] = -abb[3] * z[156];
z[109] = z[109] + z[110];
z[110] = -abb[7] * z[115];
z[71] = z[71] + z[104] + 2 * z[109] + z[110] + z[157];
z[71] = abb[34] * z[71];
z[104] = abb[22] * z[132];
z[62] = z[62] + z[71] + z[80] + z[90] + z[104];
z[62] = abb[34] * z[62];
z[71] = -abb[56] + -z[37];
z[41] = abb[59] + abb[58] * (T(-5) / T(2)) + z[31] + z[41] + -z[61] + (T(1) / T(2)) * z[71] + -z[137];
z[41] = abb[17] * z[41];
z[8] = -abb[54] + -abb[55] + -z[1] + z[8] + z[25] + z[28];
z[8] = abb[0] * z[8];
z[28] = abb[18] * z[65];
z[52] = abb[3] * z[52];
z[71] = -abb[59] + (T(1) / T(2)) * z[18];
z[71] = abb[7] * z[71];
z[8] = z[8] + z[28] + z[41] + (T(-1) / T(2)) * z[52] + -3 * z[53] + z[58] + z[71] + z[146];
z[28] = prod_pow(abb[32], 2);
z[8] = z[8] * z[28];
z[31] = z[7] + -z[31] + z[100];
z[31] = abb[19] * z[31];
z[41] = z[39] + z[61];
z[52] = -z[41] + z[64];
z[52] = abb[18] * z[52];
z[41] = -z[41] + z[60];
z[41] = abb[20] * z[41];
z[24] = z[24] * z[107];
z[53] = abb[4] + abb[7];
z[53] = abb[3] + 2 * abb[30] + (T(1) / T(2)) * z[53];
z[53] = z[45] * z[53];
z[60] = abb[29] * z[47];
z[64] = -abb[48] + abb[57];
z[71] = abb[21] * z[64];
z[80] = abb[16] * z[65];
z[24] = -z[24] + z[31] + z[41] + -z[52] + z[53] + -z[60] + (T(-1) / T(2)) * z[71] + -z[80];
z[31] = abb[69] * z[24];
z[20] = z[20] + -z[161];
z[41] = -abb[20] + z[43];
z[41] = z[41] * z[45];
z[18] = z[18] + -z[144];
z[18] = abb[4] * z[18];
z[52] = -abb[3] * z[96];
z[53] = abb[49] + z[6];
z[60] = 2 * abb[0];
z[71] = z[53] * z[60];
z[80] = abb[57] + z[102];
z[80] = z[70] * z[80];
z[26] = abb[7] * z[26];
z[18] = z[18] + z[20] + z[26] + z[41] + z[52] + z[71] + z[80] + z[140];
z[18] = abb[38] * z[18];
z[41] = z[81] + -z[118] + -z[119];
z[41] = abb[3] * z[41];
z[38] = -z[7] + z[17] + -z[38];
z[38] = z[38] * z[60];
z[52] = -abb[21] + z[43];
z[52] = z[45] * z[52];
z[60] = abb[57] + -z[36] + -z[119];
z[60] = abb[4] * z[60];
z[71] = -abb[27] * z[21];
z[38] = z[27] + z[38] + z[41] + z[52] + z[60] + z[71] + -z[123] + -z[162];
z[38] = abb[37] * z[38];
z[41] = z[16] + z[29] + z[97] + z[142] + -z[155];
z[41] = abb[17] * z[41];
z[41] = z[34] + z[41] + z[87];
z[52] = 5 * abb[55];
z[60] = z[52] + z[74] + -2 * z[79] + -z[129];
z[60] = abb[9] * z[60];
z[64] = 10 * abb[49] + z[49] + z[64] + z[114] + -z[155];
z[64] = abb[4] * z[64];
z[71] = 5 * abb[50] + -z[155];
z[14] = 16 * abb[49] + 10 * abb[51] + -z[14] + z[71] + -z[166];
z[14] = abb[1] * z[14];
z[74] = abb[53] + z[33];
z[11] = z[11] + -z[40] + z[74];
z[11] = abb[3] * z[11];
z[54] = -abb[55] + z[54] + z[127];
z[54] = abb[0] * z[54];
z[79] = abb[19] + abb[20];
z[79] = z[45] * z[79];
z[11] = -z[11] + -z[14] + z[41] + z[54] + z[60] + -z[64] + -z[79];
z[14] = z[11] + z[20] + z[82];
z[14] = abb[64] * z[14];
z[54] = abb[56] + abb[58] + z[7] + -z[33] + -z[93] + z[118];
z[54] = abb[0] * z[54];
z[60] = abb[18] + abb[21];
z[60] = z[45] * z[60];
z[27] = z[27] + 2 * z[58];
z[30] = -abb[58] + z[30] + z[141];
z[30] = abb[17] * z[30];
z[30] = z[30] + z[87];
z[58] = abb[3] * z[57];
z[26] = -z[26] + z[27] + -z[30] + z[34] + -z[54] + z[58] + -z[60];
z[34] = abb[65] * z[26];
z[54] = abb[55] * (T(1) / T(2)) + -z[16] + z[57] + z[139] + -z[158] + -z[165];
z[54] = abb[3] * z[54];
z[57] = abb[4] * z[159];
z[58] = abb[0] * z[77];
z[41] = z[41] + z[54] + -z[57] + z[58] + -z[148];
z[54] = abb[66] * z[41];
z[6] = -z[6] + z[55] + z[88];
z[6] = abb[3] * z[6];
z[55] = -abb[50] + z[33];
z[57] = -abb[48] + z[55] + -z[73] + z[93];
z[57] = abb[1] * z[57];
z[58] = abb[7] * z[96];
z[6] = z[6] + -z[30] + z[56] + z[57] + -z[58] + z[140] + z[168];
z[30] = abb[67] * z[6];
z[57] = -abb[4] * z[115];
z[53] = -z[53] * z[70];
z[53] = z[53] + z[57] + z[83];
z[53] = abb[35] * z[53];
z[53] = z[53] + -z[122];
z[53] = abb[35] * z[53];
z[57] = -abb[34] * z[128];
z[58] = -abb[69] * z[65];
z[57] = z[57] + z[58];
z[57] = abb[15] * z[57];
z[58] = z[28] * z[147];
z[44] = z[44] + -z[118] + -z[145];
z[60] = abb[66] * z[44];
z[58] = z[58] + z[60];
z[58] = abb[13] * z[58];
z[35] = -z[28] * z[35];
z[60] = -abb[32] * abb[34];
z[60] = abb[66] + z[60];
z[60] = z[60] * z[159];
z[35] = z[35] + z[60];
z[35] = abb[6] * z[35];
z[60] = abb[32] * z[21];
z[64] = z[60] + z[133] + -z[134];
z[73] = abb[31] * z[47];
z[73] = -z[64] + z[73];
z[73] = abb[31] * z[73];
z[4] = -abb[65] + (T(-1) / T(3)) * z[4];
z[4] = z[4] * z[21];
z[77] = abb[35] * z[60];
z[79] = -abb[34] * z[133];
z[4] = z[4] + z[73] + z[77] + z[79];
z[4] = abb[27] * z[4];
z[60] = z[60] + z[132];
z[60] = abb[34] * z[60];
z[28] = z[28] * z[47];
z[47] = abb[66] * z[21];
z[28] = -z[28] + -z[47] + z[60];
z[28] = z[28] * z[46];
z[47] = z[117] + -z[154];
z[47] = abb[72] * z[47];
z[0] = abb[70] + abb[71] + z[0] + z[3] + z[4] + z[5] + z[8] + z[13] + z[14] + z[18] + z[28] + z[30] + z[31] + z[34] + z[35] + z[38] + z[47] + z[53] + z[54] + z[57] + z[58] + z[62] + z[121];
z[3] = z[15] + -z[55] + z[93] + -z[111] + -z[136];
z[3] = abb[0] * z[3];
z[4] = z[17] + z[36] + -z[49] + -z[95];
z[4] = abb[4] * z[4];
z[5] = -abb[50] + z[23] + -z[74] + z[130] + -z[164];
z[5] = z[5] * z[91];
z[8] = z[40] + z[99];
z[13] = -abb[50] + z[59];
z[14] = 6 * abb[52] + -z[8] + z[13] + -z[51] + z[93];
z[14] = abb[3] * z[14];
z[17] = z[42] + -z[43];
z[17] = -z[17] * z[45];
z[18] = 3 * abb[57] + z[25] + -z[99];
z[18] = abb[7] * z[18];
z[3] = z[3] + z[4] + z[5] + z[14] + z[17] + z[18] + z[20] + z[27] + -z[94] + -z[150];
z[3] = abb[31] * z[3];
z[4] = 4 * abb[53] + -9 * abb[58] + -z[59] + -z[71] + z[84] + z[141] + -z[143];
z[4] = abb[17] * z[4];
z[5] = z[15] + z[37];
z[10] = -3 * abb[55] + 6 * abb[58] + z[5] + -z[10] + -z[33] + -z[78] + -z[118];
z[10] = abb[13] * z[10];
z[14] = abb[48] + -7 * abb[58] + z[1] + z[16] + z[52];
z[14] = abb[3] * z[14];
z[15] = -z[40] + z[75] + -z[153];
z[15] = abb[0] * z[15];
z[17] = -z[70] * z[151];
z[2] = z[2] + z[4] + z[10] + z[14] + z[15] + z[17] + z[18] + 4 * z[56] + z[140] + -z[162];
z[2] = abb[33] * z[2];
z[4] = z[16] + z[36] + z[75] + -z[99] + z[103];
z[4] = abb[3] * z[4];
z[5] = abb[48] + z[5] + -z[75] + z[105] + z[163];
z[5] = abb[0] * z[5];
z[10] = -z[85] + -2 * z[86];
z[14] = -8 * abb[59] + (T(9) / T(2)) * z[25] + z[39] + z[98];
z[14] = abb[4] * z[14];
z[12] = z[12] + -z[99];
z[15] = abb[57] * (T(-5) / T(2)) + -z[12] + -z[50] + -z[61];
z[15] = abb[7] * z[15];
z[16] = -abb[21] + z[108] + -z[125];
z[16] = z[16] * z[65];
z[4] = z[4] + z[5] + -z[10] + z[14] + z[15] + z[16] + -z[92] + -z[160];
z[4] = abb[34] * z[4];
z[5] = -z[12] + -z[66] + -z[100] + z[102];
z[5] = abb[4] * z[5];
z[12] = -z[13] + z[36] + -z[163];
z[12] = abb[0] * z[12];
z[13] = z[1] + -z[48] + -z[67];
z[13] = z[13] * z[70];
z[14] = -abb[20] + z[113] + -z[131];
z[14] = z[14] * z[65];
z[5] = z[5] + z[10] + z[12] + z[13] + z[14] + z[106] + -z[116] + -z[140];
z[5] = abb[35] * z[5];
z[10] = abb[51] + -z[29] + -z[40] + -z[59] + z[72] + -z[78] + z[101];
z[10] = abb[17] * z[10];
z[12] = -z[23] + -z[32] + -z[89];
z[12] = abb[9] * z[12];
z[12] = z[12] + z[69];
z[8] = z[8] + -z[120] + -z[124];
z[8] = abb[3] * z[8];
z[13] = abb[58] + 3 * z[68] + -z[99];
z[13] = z[13] * z[70];
z[8] = z[8] + z[10] + 2 * z[12] + z[13] + -z[18] + -z[87] + -z[126] + -z[149];
z[8] = abb[36] * z[8];
z[10] = abb[34] * z[21];
z[10] = (T(3) / T(2)) * z[10] + z[132];
z[10] = z[10] * z[46];
z[12] = abb[22] * z[135];
z[2] = z[2] + z[3] + z[4] + z[5] + z[8] + z[10] + z[12] + -z[63];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[43] * z[9];
z[4] = abb[44] * z[24];
z[5] = z[11] + -z[22] + -z[161];
z[5] = abb[39] * z[5];
z[8] = abb[40] * z[26];
z[6] = abb[42] * z[6];
z[9] = abb[13] * z[44];
z[9] = z[9] + z[41] + -z[162];
z[9] = abb[41] * z[9];
z[1] = z[1] + z[7] + -z[76];
z[1] = abb[47] * z[1];
z[7] = abb[39] * z[19];
z[10] = -abb[44] * z[65];
z[7] = z[7] + z[10];
z[7] = abb[15] * z[7];
z[10] = abb[39] + abb[43];
z[10] = z[10] * z[82];
z[11] = -abb[31] * z[21];
z[11] = z[11] + z[64];
z[11] = m1_set::bc<T>[0] * z[11];
z[12] = -abb[40] * z[21];
z[11] = z[11] + z[12];
z[11] = abb[27] * z[11];
z[1] = abb[45] + abb[46] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_651_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-6.29528515329787257381662736342755648630045211758223188305213529"),stof<T>("-64.523801920969199735358964251936598490033742049441984430025219812")}, std::complex<T>{stof<T>("-33.191100308207320131550318266562191383193779680128316816138219234"),stof<T>("-46.221600699162129032767430418644372766945054376522902069174229204")}, std::complex<T>{stof<T>("12.099491000985158508890548363100308502866350863069668498960914948"),stof<T>("-72.650642308036528454707782944359898074474816556365823300717959959")}, std::complex<T>{stof<T>("-9.556190782033000772068973201238137127998521344493552120569657174"),stof<T>("-89.775795015664713550209997856273018164177651713319197598178604883")}, std::complex<T>{stof<T>("10.808913559091387235236103361320073994482499127648662398962360616"),stof<T>("6.4077927811544311565818295087385280728529920676325660171598369699")}, std::complex<T>{stof<T>("-45.936503567357004834645510728662396876855909934067356986017349428"),stof<T>("33.735508803839325404217870049469907446528133334582252098590365979")}, std::complex<T>{stof<T>("5.988687094436881345811310662738584446203671990155887283914284001"),stof<T>("45.889135226615389161817956217069956419934936560811249088292254641")}, std::complex<T>{stof<T>("-6.9020956721917328427791134245095963619886505098679704905266234863"),stof<T>("11.5377331185761566759916865718435094622925464415053162839469128778")}, std::complex<T>{stof<T>("18.661338235799718608053866232969153407763036212485276977573430121"),stof<T>("-5.819414905679509485406763196621858120264633123452250246685482398")}, std::complex<T>{stof<T>("-0.6687458038860240805037139439018829383487291920268642821032152032"),stof<T>("-4.3436871840284605831045519526160562417641996288129021997010243338")}, std::complex<T>{stof<T>("37.665384064594267310429178539118502960545464236064112453467116866"),stof<T>("17.484370262917336362250816970612986793324841172198201351119013293")}, std::complex<T>{stof<T>("-7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("-10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("-10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[81])) - rlog(abs(kbase.W[81])), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_651_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_651_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (6 * v[0] + 2 * v[1] + -4 * (-2 + v[3] + v[4]) + -3 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[49] + abb[51] + -abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = abb[31] + -abb[34];
z[1] = abb[49] + abb[51] + -abb[56];
z[0] = z[0] * z[1];
z[2] = abb[35] * z[1];
z[2] = -z[0] + z[2];
z[2] = abb[35] * z[2];
z[1] = -abb[36] * z[1];
z[0] = z[0] + z[1];
z[0] = abb[36] * z[0];
z[0] = z[0] + z[2];
return 2 * abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_651_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[49] + abb[51] + -abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[35] + -abb[36];
z[1] = abb[49] + abb[51] + -abb[56];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_651_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[2] + v[3]) * (-8 + 8 * v[0] + 4 * v[1] + 3 * v[2] + -v[3] + -4 * v[4] + -2 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (abb[48] + abb[52] + -abb[59]) * (t * c[0] + c[1]);
	}
	{
T z[5];
z[0] = abb[48] + abb[52] + -abb[59];
z[1] = abb[35] * z[0];
z[2] = abb[34] * z[0];
z[3] = -z[1] + z[2];
z[3] = abb[34] * z[3];
z[4] = abb[32] * z[0];
z[1] = z[1] + -z[4];
z[1] = abb[32] * z[1];
z[2] = z[2] + -z[4];
z[2] = abb[31] * z[2];
z[0] = -abb[37] * z[0];
z[0] = z[0] + z[1] + z[2] + z[3];
return 2 * abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_651_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[48] + abb[52] + -abb[59]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = abb[32] + -abb[34];
z[1] = abb[48] + abb[52] + -abb[59];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_651_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-126.507991365947897700912162513529227247739084552934068024746558466"),stof<T>("-5.948048920541609705065113771683172881204344917769751691760698988")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18, 81});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,73> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[81])) - rlog(abs(k.W[81])), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[45] = SpDLog_f_4_651_W_17_Im(t, path, abb);
abb[46] = SpDLog_f_4_651_W_19_Im(t, path, abb);
abb[47] = SpDLogQ_W_82(k,dl,dlr).imag();
abb[70] = SpDLog_f_4_651_W_17_Re(t, path, abb);
abb[71] = SpDLog_f_4_651_W_19_Re(t, path, abb);
abb[72] = SpDLogQ_W_82(k,dl,dlr).real();

                    
            return f_4_651_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_651_DLogXconstant_part(base_point<T>, kend);
	value += f_4_651_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_651_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_651_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_651_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_651_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_651_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_651_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
