/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_628.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_628_abbreviated (const std::array<T,62>& abb) {
T z[94];
z[0] = abb[53] + abb[54] + abb[55];
z[1] = abb[23] + abb[25];
z[1] = z[0] * z[1];
z[2] = (T(3) / T(2)) * z[1];
z[3] = abb[45] + -abb[47];
z[4] = -abb[46] + abb[52];
z[5] = -z[3] + z[4];
z[6] = abb[49] + z[5];
z[7] = abb[16] * z[6];
z[8] = (T(3) / T(2)) * z[0];
z[9] = abb[22] * z[8];
z[10] = -abb[50] + abb[52];
z[11] = abb[0] * z[10];
z[12] = z[2] + -z[7] + z[9] + (T(-13) / T(3)) * z[11];
z[13] = -abb[49] + z[5];
z[14] = abb[8] * z[13];
z[15] = abb[24] * z[0];
z[15] = z[14] + z[15];
z[16] = abb[52] * (T(1) / T(2));
z[17] = abb[49] * (T(1) / T(2));
z[18] = z[16] + -z[17];
z[19] = -abb[46] + z[3];
z[20] = abb[50] + (T(1) / T(2)) * z[19];
z[21] = z[18] + -z[20];
z[22] = abb[14] * z[21];
z[23] = abb[45] * (T(3) / T(2));
z[24] = abb[46] * (T(3) / T(2));
z[25] = z[23] + z[24];
z[26] = -abb[48] + z[25];
z[27] = abb[47] * (T(1) / T(2));
z[28] = z[26] + -z[27];
z[29] = -abb[50] + abb[49] * (T(-5) / T(12)) + abb[52] * (T(3) / T(4)) + (T(1) / T(2)) * z[28];
z[29] = abb[5] * z[29];
z[30] = 3 * abb[46];
z[31] = -3 * abb[52] + abb[49] * (T(11) / T(3)) + -z[3] + z[30];
z[31] = abb[44] * (T(-1) / T(3)) + (T(1) / T(2)) * z[31];
z[32] = abb[6] * (T(1) / T(2));
z[31] = z[31] * z[32];
z[33] = -7 * abb[47] + abb[48] + -abb[52];
z[34] = abb[49] + abb[51];
z[33] = abb[45] + (T(1) / T(6)) * z[33] + (T(1) / T(3)) * z[34];
z[33] = abb[7] * z[33];
z[35] = abb[47] + abb[52];
z[36] = abb[49] + z[35];
z[37] = -abb[48] + z[36];
z[37] = -abb[51] + (T(1) / T(2)) * z[37];
z[38] = abb[44] * (T(1) / T(2));
z[39] = (T(1) / T(3)) * z[37] + -z[38];
z[39] = abb[2] * z[39];
z[40] = -abb[44] + z[4];
z[40] = abb[10] * z[40];
z[41] = 3 * z[40];
z[42] = -abb[49] + z[4];
z[43] = abb[13] * z[42];
z[44] = 3 * z[43];
z[45] = abb[5] * abb[51];
z[46] = abb[26] * z[0];
z[47] = (T(1) / T(2)) * z[46];
z[48] = -abb[49] + z[10];
z[49] = abb[1] * z[48];
z[50] = abb[16] + abb[5] * (T(-5) / T(6)) + abb[0] * (T(13) / T(6));
z[50] = abb[44] * z[50];
z[12] = (T(1) / T(2)) * z[12] + (T(5) / T(4)) * z[15] + -z[22] + z[29] + z[31] + z[33] + z[39] + z[41] + -z[44] + -z[45] + -z[47] + (T(19) / T(6)) * z[49] + z[50];
z[29] = prod_pow(m1_set::bc<T>[0], 2);
z[12] = z[12] * z[29];
z[31] = z[17] + z[27];
z[33] = z[16] + z[31];
z[26] = z[26] + -z[33];
z[39] = -abb[51] + (T(1) / T(2)) * z[26];
z[39] = abb[7] * z[39];
z[50] = abb[22] * z[0];
z[51] = -z[7] + z[50];
z[52] = abb[50] + abb[52];
z[53] = abb[47] + z[30];
z[54] = -abb[48] + -z[52] + z[53];
z[54] = -abb[51] + (T(1) / T(2)) * z[54];
z[54] = abb[9] * z[54];
z[55] = abb[5] * abb[49];
z[55] = -z[49] + z[55];
z[56] = (T(3) / T(2)) * z[22];
z[57] = abb[44] + -abb[49];
z[58] = abb[46] + -abb[50];
z[58] = z[57] + 3 * z[58];
z[58] = z[32] * z[58];
z[46] = (T(3) / T(4)) * z[46];
z[59] = -2 * abb[0] + abb[5] + abb[16] * (T(-3) / T(2));
z[59] = abb[44] * z[59];
z[39] = 2 * z[11] + z[39] + -z[41] + z[46] + (T(-3) / T(4)) * z[51] + 3 * z[54] + -z[55] + -z[56] + z[58] + z[59];
z[39] = abb[28] * z[39];
z[51] = z[1] + z[7];
z[58] = (T(3) / T(2)) * z[51];
z[59] = -z[11] + z[58];
z[60] = (T(3) / T(2)) * z[19];
z[61] = abb[52] * (T(5) / T(2));
z[62] = -abb[50] + z[60] + z[61];
z[63] = abb[49] * (T(3) / T(2));
z[64] = z[62] + -z[63];
z[64] = abb[5] * z[64];
z[64] = -z[9] + -z[59] + z[64];
z[65] = 2 * abb[51];
z[66] = abb[48] + z[65];
z[67] = -z[25] + z[33] + z[66];
z[68] = abb[15] * z[67];
z[69] = abb[21] * z[8];
z[68] = z[68] + -z[69];
z[53] = z[53] + -z[66];
z[52] = z[52] + -z[53];
z[70] = abb[9] * z[52];
z[71] = -z[68] + z[70];
z[72] = -abb[49] + z[19];
z[73] = abb[52] * (T(1) / T(4));
z[72] = abb[44] + abb[50] + (T(3) / T(4)) * z[72] + -z[73];
z[72] = abb[6] * z[72];
z[74] = 2 * z[49];
z[75] = -z[56] + z[74];
z[76] = abb[0] + abb[5];
z[77] = 3 * abb[16];
z[78] = z[76] + -z[77];
z[38] = z[38] * z[78];
z[38] = z[38] + z[46];
z[46] = -z[38] + (T(1) / T(2)) * z[64] + -z[71] + -z[72] + z[75];
z[64] = -abb[31] * z[46];
z[26] = abb[5] * z[26];
z[65] = abb[5] * z[65];
z[26] = z[26] + -z[65];
z[72] = abb[7] * z[67];
z[78] = z[26] + -z[72];
z[79] = abb[26] * z[8];
z[79] = -z[68] + z[79];
z[80] = -z[35] + z[66];
z[81] = 3 * abb[44];
z[82] = -abb[49] + z[80] + z[81];
z[83] = abb[6] * z[82];
z[84] = abb[16] * z[81];
z[58] = -z[58] + -z[78] + -z[79] + -z[83] + z[84];
z[58] = abb[33] * z[58];
z[67] = abb[6] * z[67];
z[53] = -abb[49] + 2 * abb[52] + -z[53];
z[83] = 2 * abb[15];
z[53] = z[53] * z[83];
z[78] = z[2] + z[53] + z[67] + z[78];
z[83] = abb[32] * z[78];
z[39] = z[39] + z[58] + z[64] + z[83];
z[39] = abb[28] * z[39];
z[64] = abb[47] * (T(5) / T(2));
z[83] = abb[46] * (T(9) / T(2));
z[84] = abb[52] * (T(7) / T(2));
z[85] = abb[49] * (T(5) / T(2));
z[23] = -z[23] + z[64] + -z[66] + z[83] + -z[84] + z[85];
z[23] = abb[15] * z[23];
z[23] = z[23] + -z[69];
z[69] = 2 * abb[50];
z[86] = z[63] + z[69];
z[28] = -z[28] + z[61] + -z[86];
z[28] = abb[5] * z[28];
z[87] = 3 * z[22];
z[88] = 4 * z[49] + -z[87];
z[89] = z[1] + z[50];
z[89] = (T(3) / T(2)) * z[89];
z[52] = -abb[6] * z[52];
z[28] = z[23] + z[28] + z[52] + z[65] + -z[70] + z[88] + -z[89];
z[28] = abb[34] * z[28];
z[52] = abb[49] * (T(7) / T(2)) + -z[61];
z[61] = abb[47] * (T(7) / T(2));
z[90] = abb[45] * (T(9) / T(2));
z[24] = z[24] + z[52] + z[61] + -z[90];
z[91] = z[24] + z[66];
z[91] = abb[7] * z[91];
z[24] = abb[48] + z[24];
z[92] = abb[5] * z[24];
z[2] = z[2] + z[92];
z[92] = 3 * abb[45];
z[36] = 2 * abb[48] + 4 * abb[51] + -z[30] + z[36] + -z[92];
z[36] = abb[6] * z[36];
z[23] = z[2] + -z[23] + z[36] + z[65] + z[91];
z[36] = -abb[33] * z[23];
z[91] = 2 * abb[49];
z[92] = 2 * abb[47] + -abb[52] + z[91] + -z[92];
z[93] = abb[48] + z[92];
z[93] = abb[5] * z[93];
z[92] = z[66] + z[92];
z[92] = abb[7] * z[92];
z[93] = z[65] + z[92] + z[93];
z[67] = z[67] + -z[68] + 2 * z[93];
z[93] = abb[31] * z[67];
z[2] = (T(1) / T(2)) * z[2] + z[45];
z[45] = 5 * abb[48];
z[61] = abb[49] * (T(-17) / T(2)) + z[25] + -z[45] + z[61] + z[84];
z[84] = 5 * abb[51];
z[61] = (T(1) / T(2)) * z[61] + -z[84];
z[61] = abb[7] * z[61];
z[25] = -abb[48] + -z[25] + -z[52] + z[64];
z[25] = -abb[51] + (T(1) / T(2)) * z[25];
z[25] = abb[6] * z[25];
z[25] = z[2] + z[25] + -z[44] + z[53] + z[61];
z[25] = abb[32] * z[25];
z[25] = z[25] + z[36] + z[93];
z[25] = abb[32] * z[25];
z[36] = (T(1) / T(2)) * z[1];
z[52] = -z[36] + -z[50];
z[53] = z[62] + -z[85];
z[53] = abb[5] * z[53];
z[52] = -z[49] + 3 * z[52] + z[53];
z[53] = abb[45] + abb[46];
z[53] = abb[49] * (T(-5) / T(4)) + abb[47] * (T(1) / T(4)) + (T(3) / T(4)) * z[53] + -z[66] + z[73];
z[53] = abb[7] * z[53];
z[8] = abb[24] * z[8];
z[8] = z[8] + (T(3) / T(2)) * z[14];
z[14] = abb[50] + z[18];
z[18] = abb[46] + z[3];
z[18] = (T(3) / T(2)) * z[18];
z[61] = z[14] + -z[18];
z[61] = z[32] * z[61];
z[44] = -z[8] + z[44] + (T(1) / T(2)) * z[52] + z[53] + -z[54] + z[61] + z[68];
z[44] = abb[30] * z[44];
z[52] = 2 * abb[5];
z[53] = z[48] * z[52];
z[14] = z[14] + z[60];
z[54] = abb[6] * z[14];
z[53] = z[9] + -z[53] + z[54] + z[71] + z[72] + -z[88];
z[54] = abb[28] + -abb[31];
z[53] = z[53] * z[54];
z[54] = abb[46] * (T(1) / T(2)) + z[16] + -z[63];
z[60] = abb[45] * (T(1) / T(2));
z[27] = z[27] + z[54] + z[60] + -z[66];
z[27] = abb[7] * z[27];
z[61] = abb[6] * z[13];
z[62] = abb[21] * z[0];
z[50] = -z[15] + -z[50] + z[61] + -z[62];
z[31] = abb[52] * (T(-3) / T(2)) + abb[46] * (T(5) / T(2)) + z[31] + z[60];
z[60] = z[31] + -z[66];
z[61] = abb[15] * z[60];
z[27] = z[27] + (T(1) / T(2)) * z[50] + -z[61];
z[50] = 3 * z[27];
z[63] = -abb[32] * z[50];
z[44] = z[44] + z[53] + z[63];
z[44] = abb[30] * z[44];
z[20] = -z[16] + z[20];
z[53] = 3 * z[20];
z[63] = z[53] + z[85];
z[63] = abb[5] * z[63];
z[59] = z[9] + -z[59] + z[63];
z[4] = z[3] + z[4];
z[4] = 3 * z[4];
z[63] = abb[49] + z[4];
z[63] = -abb[44] + (T(1) / T(4)) * z[63];
z[63] = abb[6] * z[63];
z[59] = -z[38] + (T(1) / T(2)) * z[59] + z[63] + z[72] + -z[75];
z[59] = abb[31] * z[59];
z[63] = z[8] + z[89];
z[64] = z[80] + z[91];
z[72] = abb[6] * z[64];
z[64] = abb[7] * z[64];
z[26] = z[26] + z[63] + 2 * z[64] + -z[68] + z[72];
z[26] = abb[32] * z[26];
z[72] = abb[31] * z[82];
z[73] = 3 * z[82];
z[75] = abb[33] * z[73];
z[72] = z[72] + -z[75];
z[75] = abb[28] * z[82];
z[75] = z[72] + z[75];
z[75] = abb[2] * z[75];
z[26] = -z[26] + -z[58] + z[59] + -z[75];
z[46] = abb[28] * z[46];
z[14] = abb[5] * z[14];
z[14] = z[14] + -z[64];
z[48] = -abb[6] * z[48];
z[48] = -z[14] + z[48] + -z[49] + z[63] + z[71];
z[48] = abb[30] * z[48];
z[57] = abb[6] * z[57];
z[57] = z[11] + z[57];
z[58] = -abb[44] * z[76];
z[55] = z[55] + z[57] + z[58];
z[35] = -abb[48] + z[35];
z[34] = -z[34] + (T(1) / T(2)) * z[35];
z[34] = abb[7] * z[34];
z[35] = abb[44] * (T(3) / T(2)) + -z[37];
z[37] = abb[2] * z[35];
z[34] = z[34] + z[37] + (T(1) / T(2)) * z[55];
z[34] = abb[29] * z[34];
z[34] = z[26] + z[34] + z[46] + z[48];
z[34] = abb[29] * z[34];
z[5] = abb[20] * z[5];
z[19] = abb[19] * z[19];
z[5] = z[5] + -z[19];
z[19] = -z[17] + z[20];
z[19] = abb[18] * z[19];
z[20] = -abb[19] + abb[20];
z[20] = z[17] * z[20];
z[37] = abb[27] * (T(1) / T(2));
z[0] = z[0] * z[37];
z[37] = -abb[50] + z[16];
z[37] = abb[19] * z[37];
z[46] = abb[18] + -abb[20];
z[46] = abb[44] * z[46];
z[0] = z[0] + (T(1) / T(2)) * z[5] + z[19] + z[20] + z[37] + z[46];
z[3] = z[3] + z[42];
z[5] = abb[17] * z[3];
z[19] = (T(-3) / T(2)) * z[0] + (T(-3) / T(4)) * z[5];
z[19] = abb[59] * z[19];
z[20] = -z[33] + z[45] + -z[83] + -z[90];
z[20] = (T(1) / T(2)) * z[20] + z[81] + z[84];
z[20] = abb[6] * z[20];
z[24] = abb[51] + (T(1) / T(2)) * z[24];
z[24] = abb[7] * z[24];
z[2] = z[2] + z[20] + z[24] + z[41] + -z[68];
z[20] = prod_pow(abb[33], 2);
z[2] = z[2] * z[20];
z[23] = abb[36] * z[23];
z[24] = -abb[33] * z[67];
z[33] = abb[5] * z[91];
z[37] = abb[0] + -z[52];
z[37] = abb[44] * z[37];
z[33] = z[33] + z[37] + z[49] + -z[57] + z[92];
z[33] = abb[31] * z[33];
z[24] = z[24] + z[33];
z[24] = abb[31] * z[24];
z[13] = abb[7] * z[13];
z[13] = -z[13] + z[15];
z[21] = abb[5] * z[21];
z[3] = z[3] * z[32];
z[3] = -z[3] + (T(1) / T(2)) * z[13] + z[21] + -z[22] + z[36] + z[49];
z[3] = 3 * z[3];
z[13] = abb[57] * z[3];
z[21] = -abb[33] * z[60];
z[22] = -abb[48] + z[31];
z[22] = -abb[51] + (T(1) / T(2)) * z[22];
z[31] = abb[32] * z[22];
z[21] = z[21] + z[31];
z[21] = abb[32] * z[21];
z[31] = abb[32] * z[60];
z[32] = -abb[28] * z[22];
z[32] = z[31] + z[32];
z[32] = abb[28] * z[32];
z[33] = abb[30] * z[22];
z[31] = -z[31] + z[33];
z[31] = abb[30] * z[31];
z[33] = -abb[34] + abb[36] + abb[37] + abb[56] + -abb[58];
z[33] = z[33] * z[60];
z[22] = z[20] * z[22];
z[21] = z[21] + z[22] + z[31] + z[32] + z[33];
z[22] = 3 * abb[4];
z[21] = z[21] * z[22];
z[6] = -abb[44] + (T(1) / T(2)) * z[6];
z[6] = abb[6] * z[6];
z[31] = abb[16] * abb[44];
z[6] = z[6] + z[31] + -z[47];
z[31] = abb[47] * (T(-3) / T(2)) + abb[45] * (T(5) / T(2)) + z[54];
z[32] = -abb[48] + z[31];
z[33] = -abb[5] * z[32];
z[31] = z[31] + -z[66];
z[36] = -abb[7] * z[31];
z[33] = z[6] + z[33] + z[36] + (T(-1) / T(2)) * z[51] + z[65];
z[33] = abb[35] * z[33];
z[36] = z[7] + z[62];
z[6] = -z[6] + (T(1) / T(2)) * z[36] + z[61];
z[36] = abb[56] * z[6];
z[33] = z[33] + -z[36];
z[36] = -abb[58] * z[50];
z[37] = abb[37] * z[78];
z[35] = abb[28] * z[35];
z[35] = z[35] + z[72];
z[35] = abb[28] * z[35];
z[41] = -abb[35] + -abb[56] + z[20];
z[41] = z[41] * z[73];
z[42] = -prod_pow(abb[31], 2) * z[82];
z[35] = z[35] + z[41] + z[42];
z[35] = abb[2] * z[35];
z[41] = -abb[31] * abb[33];
z[42] = abb[31] + -abb[33];
z[42] = abb[32] * z[42];
z[20] = abb[35] + abb[36] + z[20] + z[41] + z[42];
z[20] = z[20] * z[31];
z[31] = abb[51] + (T(-1) / T(2)) * z[32];
z[29] = z[29] * z[31];
z[20] = 3 * z[20] + z[29];
z[20] = abb[3] * z[20];
z[2] = abb[60] + abb[61] + z[2] + z[12] + z[13] + z[19] + z[20] + z[21] + z[23] + z[24] + z[25] + z[28] + 3 * z[33] + z[34] + z[35] + z[36] + z[37] + z[39] + z[44];
z[12] = -abb[44] + -z[16] + -z[18] + z[86];
z[12] = abb[6] * z[12];
z[13] = 2 * z[70];
z[10] = z[10] * z[52];
z[16] = 4 * abb[0] + -z[52] + z[77];
z[16] = abb[44] * z[16];
z[10] = (T(-3) / T(2)) * z[7] + z[10] + -4 * z[11] + z[12] + z[13] + z[16] + 6 * z[40] + z[74] + -z[79];
z[10] = abb[28] * z[10];
z[12] = abb[49] + -abb[52] + z[30] + -z[69];
z[12] = abb[6] * z[12];
z[12] = z[12] + -z[13] + -z[14] + 3 * z[15] + -6 * z[43] + 5 * z[49] + -z[68] + -z[87] + z[89];
z[12] = abb[30] * z[12];
z[1] = -z[1] + z[7];
z[7] = z[17] + z[53];
z[7] = abb[5] * z[7];
z[1] = (T(3) / T(2)) * z[1] + z[7] + -z[9] + -z[11];
z[7] = abb[2] * z[82];
z[4] = -7 * abb[49] + z[4];
z[4] = abb[44] + (T(1) / T(4)) * z[4];
z[4] = abb[6] * z[4];
z[1] = (T(1) / T(2)) * z[1] + z[4] + z[7] + -z[8] + z[38] + -z[49] + z[56] + -z[64];
z[1] = abb[29] * z[1];
z[4] = abb[28] + -abb[30];
z[4] = z[4] * z[22] * z[60];
z[1] = z[1] + z[4] + z[10] + z[12] + z[26];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[39] * z[3];
z[0] = -z[0] + (T(-1) / T(2)) * z[5];
z[0] = abb[41] * z[0];
z[4] = abb[4] * z[60];
z[5] = -z[4] + -z[27];
z[5] = abb[40] * z[5];
z[4] = z[4] + -z[6] + -z[7];
z[4] = abb[38] * z[4];
z[4] = z[4] + z[5];
z[0] = abb[42] + abb[43] + (T(3) / T(2)) * z[0] + z[1] + z[3] + 3 * z[4];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_628_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.842303589037347753716178442501725703371338453839769594880634106"),stof<T>("-15.122073742449383876612057114347449851412820922669855048999498447")}, std::complex<T>{stof<T>("-5.8353936399983307679581584692371563069721225375490980027885774203"),stof<T>("-0.2944280064184412209788379563404994829815088917188172714968998916")}, std::complex<T>{stof<T>("9.6910558069224228983851695190974950420840410831893798120008510992"),stof<T>("5.5052373750092180789417719145809011286940907604500039130053537553")}, std::complex<T>{stof<T>("5.8353936399983307679581584692371563069721225375490980027885774203"),stof<T>("0.2944280064184412209788379563404994829815088917188172714968998916")}, std::complex<T>{stof<T>("-7.627599979258414394697256932890284247236991584379268680010266196"),stof<T>("14.761637334374721982133367462817102553647503900240402103445876479")}, std::complex<T>{stof<T>("-21.160959375218185046798604894489504992692371121408418086891751401"),stof<T>("-5.865673783083879973420461566111248426459407782879456858558975723")}, std::complex<T>{stof<T>("11.4699035682957621484134353753920099506083300382190382748909003019"),stof<T>("0.3604364080746618944786896515303472977653170224294529455536219678")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_628_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_628_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (2 * (7 * v[0] + -11 * v[1] + -v[2] + 3 * v[3] + 4 * v[4] + 6 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -3 * v[5]) + m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (-(4 + 6 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (v[0] + -1 * v[1] + v[2] + v[3] + -1 * v[5])) / tend;


		return (3 * abb[45] + 3 * abb[46] + -abb[47] + -2 * abb[48] + -abb[49] + -4 * abb[51] + -abb[52]) * (4 * t * c[0] + -3 * c[1]) * (T(1) / T(12));
	}
	{
T z[6];
z[0] = abb[34] + -2 * abb[36] + -abb[37];
z[1] = abb[45] + abb[46];
z[1] = -abb[47] + -abb[49] + -abb[52] + 3 * z[1];
z[2] = -2 * abb[48] + -4 * abb[51] + z[1];
z[0] = z[0] * z[2];
z[3] = abb[28] + -abb[29] + abb[31];
z[4] = z[2] * z[3];
z[1] = -abb[48] + -2 * abb[51] + (T(1) / T(2)) * z[1];
z[5] = abb[33] * z[1];
z[4] = z[4] + -5 * z[5];
z[4] = abb[33] * z[4];
z[3] = 2 * abb[33] + -z[3];
z[2] = z[2] * z[3];
z[1] = abb[32] * z[1];
z[1] = z[1] + z[2];
z[1] = abb[32] * z[1];
z[0] = z[0] + z[1] + z[4];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_628_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(8)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (-2 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (3 * abb[45] + 3 * abb[46] + -abb[47] + -2 * abb[48] + -abb[49] + -4 * abb[51] + -abb[52]) * (4 * t * c[0] + c[1]) * (T(-1) / T(4));
	}
	{
T z[2];
z[0] = abb[45] + abb[46];
z[0] = abb[47] + 2 * abb[48] + abb[49] + 4 * abb[51] + abb[52] + -3 * z[0];
z[1] = -abb[32] + abb[33];
return abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_628_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[44] + abb[45] + -abb[47] + -abb[49]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[33], 2);
z[1] = -prod_pow(abb[31], 2);
z[0] = z[0] + z[1];
z[1] = -abb[44] + -abb[45] + abb[47] + abb[49];
return 3 * abb[12] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_628_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_628_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W24(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k), T{0}, T{0}};
abb[42] = SpDLog_f_4_628_W_17_Im(t, path, abb);
abb[43] = SpDLog_f_4_628_W_20_Im(t, path, abb);
abb[60] = SpDLog_f_4_628_W_17_Re(t, path, abb);
abb[61] = SpDLog_f_4_628_W_20_Re(t, path, abb);

                    
            return f_4_628_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_628_DLogXconstant_part(base_point<T>, kend);
	value += f_4_628_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_628_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_628_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_628_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_628_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_628_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_628_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
