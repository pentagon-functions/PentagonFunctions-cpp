/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_491.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_491_abbreviated (const std::array<T,10>& abb) {
T z[3];
z[0] = -abb[0] * abb[3];
z[1] = abb[0] * (T(1) / T(2));
z[2] = abb[4] * z[1];
z[0] = z[0] + z[2];
z[0] = abb[4] * z[0];
z[1] = prod_pow(abb[3], 2) * z[1];
z[2] = abb[0] + -abb[1];
z[2] = abb[5] * z[2];
z[0] = z[0] + z[1] + z[2];
z[1] = abb[7] + -abb[8];
z[0] = z[0] * z[1];
z[0] = abb[9] + z[0];
return {z[0], abb[6]};
}


template <typename T> std::complex<T> f_4_491_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {stof<T>("7.2123414189575657123984289690686999445899177540429932907536293321"), stof<T>("-7.2123414189575657123984289690686999445899177540429932907536293321")};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[7].real()/kbase.W[7].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_491_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_491_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,1> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (prod_pow(v[1] + v[2] + -v[3] + -v[4], 2) * (T(-1) / T(8))) / prod_pow(tend, 2);


		return t * (abb[7] + -abb[8]) * c[0];
	}
	{
T z[3];
z[0] = abb[7] + -abb[8];
z[1] = abb[4] * z[0];
z[0] = (T(1) / T(2)) * z[0];
z[2] = -abb[3] * z[0];
z[1] = z[1] + z[2];
z[1] = abb[3] * z[1];
z[0] = -prod_pow(abb[4], 2) * z[0];
z[0] = z[0] + z[1];
return abb[2] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_491_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_491_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,10> abb = {dlog_W4(k,dl), dlog_W8(k,dl), dlog_W20(k,dl), f_1_7(k), f_1_11(k), f_2_14(k), T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[7].real()/k.W[7].real()), T{0}};
abb[6] = SpDLog_f_4_491_W_20_Im(t, path, abb);
abb[9] = SpDLog_f_4_491_W_20_Re(t, path, abb);

                    
            return f_4_491_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_491_DLogXconstant_part(base_point<T>, kend);
	value += f_4_491_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_491_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_491_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_491_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_491_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_491_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_491_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
