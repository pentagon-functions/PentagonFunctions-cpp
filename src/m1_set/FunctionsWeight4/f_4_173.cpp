/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_173.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_173_abbreviated (const std::array<T,57>& abb) {
T z[96];
z[0] = abb[22] + abb[24];
z[1] = abb[26] + z[0];
z[2] = abb[25] + z[1];
z[3] = abb[21] + abb[23];
z[4] = z[2] + z[3];
z[5] = abb[28] * (T(1) / T(2));
z[4] = z[4] * z[5];
z[6] = abb[25] + abb[26];
z[7] = -z[3] + z[6];
z[8] = abb[32] * (T(1) / T(2));
z[9] = z[7] * z[8];
z[10] = abb[30] * z[0];
z[11] = abb[21] + z[0];
z[12] = abb[23] + z[11];
z[12] = abb[29] * z[12];
z[4] = z[4] + -z[9] + z[10] + -z[12];
z[4] = abb[28] * z[4];
z[9] = abb[21] + abb[26];
z[10] = abb[25] * (T(1) / T(2));
z[13] = abb[23] * (T(1) / T(2));
z[9] = z[0] + (T(1) / T(2)) * z[9] + z[10] + z[13];
z[14] = abb[32] * z[9];
z[5] = z[5] * z[7];
z[7] = -abb[30] + abb[33];
z[15] = -abb[25] * z[7];
z[5] = z[5] + z[12] + -z[14] + z[15];
z[5] = abb[31] * z[5];
z[12] = abb[23] + z[0];
z[15] = abb[25] + z[12];
z[16] = prod_pow(abb[30], 2);
z[17] = (T(1) / T(2)) * z[16];
z[15] = z[15] * z[17];
z[17] = abb[28] + -abb[32];
z[2] = -z[2] * z[17];
z[10] = abb[33] * z[10];
z[2] = z[2] + z[10];
z[2] = abb[33] * z[2];
z[10] = prod_pow(abb[29], 2);
z[13] = z[10] * z[13];
z[18] = -abb[29] + abb[30];
z[19] = -abb[23] * abb[32] * z[18];
z[20] = abb[38] * z[0];
z[21] = abb[55] * z[1];
z[3] = abb[35] * z[3];
z[6] = abb[34] * z[6];
z[22] = abb[27] * (T(1) / T(2));
z[23] = abb[56] * z[22];
z[12] = abb[36] * z[12];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + -z[12] + z[13] + -z[15] + -z[19] + z[20] + z[21] + z[23];
z[3] = prod_pow(m1_set::bc<T>[0], 2);
z[4] = z[3] * z[9];
z[2] = (T(3) / T(2)) * z[2] + z[4];
z[4] = abb[51] + abb[53];
z[5] = z[2] * z[4];
z[6] = abb[54] * z[11];
z[2] = z[2] + (T(3) / T(2)) * z[6];
z[2] = abb[52] * z[2];
z[6] = abb[43] + abb[45];
z[12] = abb[46] + abb[50];
z[13] = abb[47] + z[12];
z[15] = z[6] + -z[13];
z[15] = abb[42] + (T(1) / T(2)) * z[15];
z[19] = abb[14] * z[15];
z[20] = (T(3) / T(2)) * z[19];
z[21] = abb[43] + -abb[47];
z[23] = -abb[45] + z[12];
z[24] = z[21] + z[23];
z[24] = -abb[48] + (T(1) / T(2)) * z[24];
z[25] = abb[16] * z[24];
z[26] = (T(3) / T(2)) * z[25];
z[27] = -abb[47] + abb[50];
z[28] = -abb[48] + z[27];
z[29] = abb[2] * z[28];
z[30] = z[20] + z[26] + -z[29];
z[31] = -abb[46] + abb[48] + abb[50];
z[32] = 3 * abb[43];
z[33] = z[31] + -z[32];
z[34] = abb[49] + abb[44] * (T(1) / T(2));
z[33] = (T(1) / T(2)) * z[33] + z[34];
z[33] = abb[10] * z[33];
z[35] = 3 * abb[42];
z[36] = -z[13] + z[35];
z[37] = z[34] + (T(1) / T(2)) * z[36];
z[37] = abb[1] * z[37];
z[38] = 3 * abb[48];
z[39] = -abb[42] + z[38];
z[40] = -abb[47] + z[32];
z[41] = -z[39] + z[40];
z[41] = abb[4] * z[41];
z[42] = 3 * abb[45];
z[43] = z[32] + z[42];
z[44] = -z[13] + z[43];
z[45] = -z[34] + (T(1) / T(4)) * z[44];
z[45] = abb[8] * z[45];
z[46] = abb[46] + abb[47];
z[47] = 3 * abb[50];
z[48] = -5 * abb[43] + -abb[45] + -z[46] + z[47];
z[49] = z[34] + (T(1) / T(4)) * z[48];
z[50] = 3 * abb[3];
z[51] = z[49] * z[50];
z[52] = abb[42] + abb[48];
z[53] = -abb[50] + z[52];
z[54] = abb[0] * z[53];
z[55] = 2 * z[54];
z[56] = abb[42] + -abb[47];
z[57] = abb[7] * z[56];
z[41] = -z[30] + -3 * z[33] + z[37] + (T(1) / T(2)) * z[41] + z[45] + z[51] + -z[55] + z[57];
z[41] = abb[28] * z[41];
z[45] = abb[44] + 2 * abb[49];
z[36] = z[36] + z[45];
z[51] = abb[1] * z[36];
z[57] = (T(1) / T(2)) * z[54];
z[20] = -z[20] + z[51] + z[57];
z[58] = (T(1) / T(2)) * z[44] + -z[45];
z[59] = abb[15] * z[58];
z[60] = -z[32] + z[45];
z[31] = z[31] + z[60];
z[61] = abb[10] * z[31];
z[59] = z[59] + z[61];
z[62] = 3 * abb[46];
z[63] = -z[42] + z[62];
z[64] = abb[43] + abb[47];
z[65] = 3 * z[64];
z[66] = z[63] + z[65];
z[67] = abb[50] + z[66];
z[67] = -z[52] + (T(1) / T(4)) * z[67];
z[67] = abb[4] * z[67];
z[26] = -z[26] + 2 * z[29];
z[67] = -z[20] + z[26] + -z[59] + z[67];
z[68] = -abb[32] * z[67];
z[69] = abb[8] * z[58];
z[70] = 3 * z[19] + -z[69];
z[36] = abb[4] * z[36];
z[36] = z[36] + -z[70];
z[36] = abb[29] * z[36];
z[71] = abb[29] * z[51];
z[71] = 3 * z[71];
z[72] = 2 * abb[44] + 4 * abb[49];
z[44] = -z[44] + z[72];
z[73] = -z[18] * z[44];
z[74] = abb[12] * z[73];
z[36] = z[36] + z[71] + z[74];
z[48] = z[45] + (T(1) / T(2)) * z[48];
z[50] = z[48] * z[50];
z[75] = abb[4] * z[58];
z[76] = z[50] + -z[69] + z[75];
z[77] = -abb[30] * z[76];
z[78] = abb[29] * z[58];
z[60] = 2 * abb[50] + -z[46] + z[60];
z[79] = abb[30] * z[60];
z[79] = -z[78] + 2 * z[79];
z[79] = abb[15] * z[79];
z[80] = abb[30] * z[58];
z[78] = -z[78] + z[80];
z[80] = 5 * abb[50];
z[66] = z[66] + -z[80];
z[66] = z[52] + (T(1) / T(2)) * z[66];
z[81] = z[8] * z[66];
z[81] = z[78] + z[81];
z[81] = abb[7] * z[81];
z[41] = -z[36] + z[41] + z[68] + z[77] + z[79] + z[81];
z[41] = abb[28] * z[41];
z[40] = z[40] + z[63];
z[68] = z[40] + -z[47];
z[68] = abb[42] + (T(1) / T(4)) * z[68];
z[68] = abb[4] * z[68];
z[20] = z[20] + z[26] + z[68] + z[69];
z[20] = abb[32] * z[20];
z[26] = z[32] + z[63];
z[63] = 5 * abb[47];
z[68] = z[26] + -z[63];
z[77] = z[47] + z[68];
z[39] = -z[39] + (T(1) / T(2)) * z[77];
z[8] = z[8] * z[39];
z[8] = z[8] + z[78];
z[8] = abb[7] * z[8];
z[8] = z[8] + z[20];
z[20] = abb[7] * (T(1) / T(2));
z[39] = -z[20] * z[66];
z[39] = z[39] + z[67];
z[39] = abb[28] * z[39];
z[66] = abb[4] * z[28];
z[66] = z[29] + z[66];
z[67] = z[26] + -z[27];
z[67] = -abb[48] + (T(1) / T(2)) * z[67];
z[77] = abb[7] * z[67];
z[79] = 2 * abb[47];
z[81] = z[45] + z[79];
z[82] = -z[12] + z[81];
z[83] = abb[8] * z[82];
z[83] = z[59] + -z[66] + z[77] + z[83];
z[83] = abb[33] * z[83];
z[84] = abb[4] * z[56];
z[84] = -z[29] + -z[54] + z[84];
z[85] = abb[50] * (T(1) / T(2));
z[86] = abb[46] * (T(1) / T(2));
z[87] = z[85] + z[86];
z[88] = -abb[47] + -z[34] + z[87];
z[88] = abb[8] * z[88];
z[89] = -z[20] * z[56];
z[37] = z[37] + (T(1) / T(2)) * z[84] + z[88] + z[89];
z[37] = abb[31] * z[37];
z[78] = abb[15] * z[78];
z[89] = 2 * abb[8];
z[90] = -abb[4] + -z[89];
z[82] = abb[30] * z[82] * z[90];
z[36] = -z[8] + z[36] + z[37] + z[39] + -z[78] + z[82] + z[83];
z[36] = abb[31] * z[36];
z[37] = -abb[43] + abb[45];
z[39] = (T(-1) / T(2)) * z[37];
z[82] = abb[42] * (T(-7) / T(6)) + abb[47] * (T(5) / T(3)) + z[39] + -z[85] + z[86];
z[82] = abb[4] * z[82];
z[39] = -abb[48] + abb[42] * (T(-5) / T(6)) + abb[47] * (T(1) / T(3)) + z[39] + z[87];
z[39] = abb[7] * z[39];
z[83] = abb[43] + -2 * abb[45] + abb[50] * (T(-13) / T(6)) + abb[46] * (T(5) / T(6)) + abb[49] * (T(7) / T(3)) + abb[44] * (T(7) / T(6)) + abb[47] * (T(10) / T(3));
z[83] = abb[8] * z[83];
z[85] = -abb[42] + (T(1) / T(3)) * z[13];
z[85] = abb[49] * (T(-1) / T(3)) + abb[44] * (T(-1) / T(6)) + (T(1) / T(2)) * z[85];
z[85] = abb[1] * z[85];
z[39] = -z[19] + -z[25] + (T(19) / T(6)) * z[29] + z[39] + (T(13) / T(6)) * z[54] + z[82] + z[83] + 7 * z[85];
z[39] = z[3] * z[39];
z[54] = 3 * abb[47];
z[82] = -z[6] + -z[12] + z[54];
z[82] = z[45] + (T(1) / T(2)) * z[82];
z[82] = abb[8] * z[82];
z[23] = -z[23] + z[64];
z[64] = (T(1) / T(2)) * z[23];
z[83] = abb[4] + -abb[5];
z[83] = z[64] * z[83];
z[85] = abb[3] + -abb[15];
z[85] = z[48] * z[85];
z[86] = abb[52] + abb[53];
z[87] = abb[51] * (T(1) / T(2));
z[90] = (T(-1) / T(2)) * z[86] + -z[87];
z[0] = abb[25] + z[0];
z[0] = z[0] * z[90];
z[0] = z[0] + z[82] + z[83] + z[85];
z[0] = abb[37] * z[0];
z[82] = abb[4] * z[15];
z[19] = -z[19] + z[82];
z[54] = -abb[43] + -5 * abb[45] + -abb[50] + z[54] + z[62];
z[62] = z[45] + (T(1) / T(2)) * z[54];
z[82] = -abb[6] + abb[7] + abb[8];
z[82] = z[62] * z[82];
z[82] = -z[19] + -z[51] + z[82];
z[83] = (T(1) / T(2)) * z[11];
z[85] = z[4] * z[83];
z[85] = -z[82] + z[85];
z[85] = abb[54] * z[85];
z[19] = abb[35] * z[19];
z[90] = -prod_pow(abb[32], 2);
z[3] = -z[3] + z[10] + z[90];
z[90] = -abb[45] + z[46];
z[91] = -abb[42] + z[90];
z[91] = abb[13] * z[91];
z[3] = z[3] * z[91];
z[92] = abb[35] + z[10];
z[92] = z[51] * z[92];
z[93] = -prod_pow(abb[28], 2);
z[93] = z[10] + z[93];
z[94] = -abb[43] + abb[50];
z[95] = -abb[42] + z[94];
z[95] = abb[9] * z[95];
z[93] = z[93] * z[95];
z[0] = z[0] + z[3] + z[19] + z[85] + z[92] + z[93];
z[3] = -2 * abb[46] + abb[50] + z[42] + -z[81];
z[19] = -abb[8] * z[3];
z[19] = z[19] + -z[51] + -z[84];
z[19] = abb[32] * z[19];
z[81] = z[3] * z[89];
z[75] = z[75] + z[81];
z[75] = -z[18] * z[75];
z[81] = 3 * abb[6];
z[84] = z[18] * z[62] * z[81];
z[19] = z[19] + z[75] + z[78] + -z[84];
z[19] = abb[32] * z[19];
z[32] = -9 * abb[45] + z[32] + 7 * z[46] + -z[80];
z[32] = (T(1) / T(2)) * z[32] + z[45];
z[46] = -abb[36] * z[32];
z[75] = -abb[46] + z[42];
z[65] = -z[65] + -z[75] + z[80];
z[78] = 2 * abb[48];
z[65] = z[45] + (T(1) / T(2)) * z[65] + -z[78];
z[65] = abb[34] * z[65];
z[85] = 3 * abb[55];
z[92] = z[24] * z[85];
z[3] = -z[3] * z[18];
z[56] = -abb[32] * z[56];
z[3] = z[3] + z[56];
z[3] = abb[32] * z[3];
z[56] = abb[38] * z[58];
z[3] = 2 * z[3] + z[46] + z[56] + z[65] + z[92];
z[3] = abb[7] * z[3];
z[46] = -z[26] + -z[63] + z[80];
z[46] = -abb[48] + (T(1) / T(2)) * z[46];
z[46] = z[20] * z[46];
z[42] = -z[42] + z[63];
z[56] = 9 * abb[43] + 5 * abb[46] + -7 * abb[50] + z[42];
z[65] = -z[34] + (T(1) / T(4)) * z[56];
z[65] = abb[15] * z[65];
z[80] = -abb[43] + z[27];
z[80] = abb[11] * z[80];
z[33] = z[33] + z[46] + z[65] + (T(-1) / T(2)) * z[66] + 3 * z[80] + -z[88];
z[33] = abb[33] * z[33];
z[46] = abb[4] * z[67];
z[65] = 3 * z[25];
z[66] = 4 * z[29] + -z[65];
z[28] = abb[7] * z[28];
z[28] = -2 * z[28] + -z[46] + z[59] + -z[66] + -z[69];
z[17] = z[17] * z[28];
z[17] = z[17] + z[33];
z[17] = abb[33] * z[17];
z[28] = -abb[4] * z[31];
z[28] = z[28] + z[50] + -z[61] + z[66];
z[28] = abb[34] * z[28];
z[31] = z[13] + -z[37];
z[31] = (T(1) / T(2)) * z[31] + -z[52];
z[31] = abb[17] * z[31];
z[15] = abb[19] * z[15];
z[33] = abb[20] * z[24];
z[15] = -z[15] + z[31] + -z[33];
z[31] = -z[90] + z[94];
z[33] = abb[18] * z[31];
z[33] = (T(3) / T(2)) * z[15] + (T(-3) / T(4)) * z[33];
z[33] = abb[56] * z[33];
z[21] = abb[45] + z[21];
z[21] = (T(1) / T(2)) * z[21] + -z[34];
z[21] = abb[4] * z[21];
z[37] = -abb[43] + -abb[46] + -z[42] + z[47];
z[37] = -z[34] + (T(1) / T(4)) * z[37];
z[37] = abb[8] * z[37];
z[42] = abb[3] * z[49];
z[21] = z[21] + z[37] + z[42] + -z[80];
z[37] = 3 * z[16];
z[21] = z[21] * z[37];
z[42] = -z[45] + (T(1) / T(2)) * z[56];
z[45] = abb[34] + abb[36];
z[42] = z[42] * z[45];
z[45] = abb[35] * z[48];
z[46] = 3 * z[10];
z[48] = -z[46] * z[49];
z[42] = z[42] + -3 * z[45] + z[48];
z[42] = abb[15] * z[42];
z[45] = abb[8] * z[64];
z[31] = (T(1) / T(2)) * z[31];
z[48] = abb[4] * z[31];
z[25] = z[25] + -z[29] + -z[45] + z[48];
z[45] = -z[25] * z[85];
z[48] = abb[15] * z[60];
z[49] = 2 * z[48];
z[52] = z[49] + -z[76];
z[52] = abb[38] * z[52];
z[37] = z[37] + -z[46];
z[37] = z[37] * z[58];
z[56] = -abb[34] + -2 * abb[36] + abb[38];
z[56] = z[44] * z[56];
z[58] = -abb[32] * z[73];
z[37] = z[37] + z[56] + z[58];
z[37] = abb[12] * z[37];
z[56] = prod_pow(abb[33], 2);
z[56] = -z[16] + z[56];
z[56] = z[56] * z[64];
z[58] = -abb[55] * z[23];
z[7] = z[7] * z[23];
z[59] = -abb[31] * z[7];
z[56] = z[56] + z[58] + z[59];
z[58] = abb[5] * (T(3) / T(2));
z[56] = z[56] * z[58];
z[32] = -abb[8] * z[32];
z[44] = -abb[4] * z[44];
z[32] = z[32] + z[44];
z[32] = abb[36] * z[32];
z[10] = -z[10] + z[16];
z[16] = z[34] + (T(1) / T(4)) * z[54];
z[10] = z[10] * z[16];
z[16] = abb[36] * z[62];
z[10] = z[10] + z[16];
z[10] = z[10] * z[81];
z[6] = -z[6] + -z[13];
z[6] = abb[42] + (T(1) / T(4)) * z[6] + z[34];
z[6] = abb[4] * z[6] * z[46];
z[16] = abb[35] + abb[36];
z[16] = z[16] * z[50];
z[0] = 3 * z[0] + z[2] + z[3] + z[5] + z[6] + z[10] + z[16] + z[17] + z[19] + z[21] + z[28] + z[32] + z[33] + z[36] + z[37] + z[39] + z[41] + z[42] + z[45] + z[52] + z[56];
z[2] = -z[12] + -z[43] + z[63];
z[2] = (T(1) / T(2)) * z[2] + z[72];
z[3] = abb[8] * z[2];
z[5] = z[26] + z[27];
z[5] = (T(1) / T(2)) * z[5] + -z[78];
z[5] = abb[4] * z[5];
z[6] = 6 * z[80];
z[5] = -z[3] + z[5] + -z[6] + 5 * z[29] + z[49] + -2 * z[61] + -z[65] + z[77];
z[5] = abb[33] * z[5];
z[10] = abb[4] + -abb[7];
z[10] = z[10] * z[53];
z[10] = z[10] + z[29] + -z[48] + z[51] + z[55] + z[61];
z[10] = abb[28] * z[10];
z[12] = -z[18] * z[48];
z[10] = -z[10] + -z[12] + z[74];
z[12] = z[47] + -z[68];
z[12] = -2 * abb[42] + (T(1) / T(4)) * z[12];
z[12] = abb[4] * z[12];
z[16] = -z[40] + -z[47];
z[16] = abb[42] + (T(1) / T(2)) * z[16] + z[38];
z[16] = z[16] * z[20];
z[3] = z[3] + z[12] + z[16] + z[30] + -2 * z[51] + z[57];
z[3] = abb[31] * z[3];
z[12] = z[13] + z[43];
z[12] = (T(1) / T(2)) * z[12] + -z[35] + -z[72];
z[12] = abb[4] * z[12];
z[12] = z[12] + -z[70] + -6 * z[91];
z[12] = abb[29] * z[12];
z[2] = abb[4] * z[2];
z[13] = -abb[50] + z[79];
z[13] = 2 * z[13] + z[72] + -z[75];
z[13] = z[13] * z[89];
z[2] = z[2] + z[6] + z[13];
z[2] = abb[30] * z[2];
z[6] = abb[28] + -abb[29];
z[6] = z[6] * z[95];
z[2] = z[2] + z[3] + z[5] + 6 * z[6] + -z[8] + -2 * z[10] + z[12] + -z[71] + -z[84];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[7] * z[24];
z[5] = z[1] * z[87];
z[3] = z[3] + z[5] + -z[25];
z[3] = abb[40] * z[3];
z[4] = abb[52] + z[4];
z[4] = z[4] * z[83];
z[4] = z[4] + -z[82];
z[4] = abb[39] * z[4];
z[3] = z[3] + z[4];
z[4] = abb[31] * z[9];
z[5] = abb[33] * z[1];
z[6] = abb[29] * z[11];
z[8] = abb[23] + abb[25];
z[8] = abb[30] * z[8];
z[4] = -z[4] + z[5] + z[6] + z[8] + -z[14];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = abb[41] * z[22];
z[4] = z[4] + z[5];
z[5] = abb[51] * z[4];
z[6] = -abb[18] * z[31];
z[6] = z[6] + z[15];
z[6] = abb[41] * z[6];
z[5] = z[5] + z[6];
z[1] = abb[40] * z[1];
z[1] = z[1] + z[4];
z[4] = (T(3) / T(2)) * z[86];
z[1] = z[1] * z[4];
z[4] = -m1_set::bc<T>[0] * z[7];
z[6] = -abb[40] * z[23];
z[4] = z[4] + z[6];
z[4] = z[4] * z[58];
z[1] = z[1] + z[2] + 3 * z[3] + z[4] + (T(3) / T(2)) * z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_173_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-29.812321165349265736397436937642014984677930848764465516646227806"),stof<T>("21.120501999199564205866697115437052890912296680443702455469877917")}, std::complex<T>{stof<T>("6.6638457612309815136351091535161537510360849826576993189619766952"),stof<T>("-4.3292663793854642962410414458778339783202554025640303972787963686")}, std::complex<T>{stof<T>("8.6358126098310454563038976339963516408630811319422295070512293325"),stof<T>("10.5694942966848531536423757788699940874751195918325011658368221778")}, std::complex<T>{stof<T>("-5.8611628528904734679645278935876502018742341813352511315133635778"),stof<T>("-1.8354233672926516309069811541707324986093303035217831288881022342")}, std::complex<T>{stof<T>("-2.7746497569405719883393697404087014389888469506069783755378657547"),stof<T>("-8.7340709293922015227353946246992615888657892883107180369487199436")}, std::complex<T>{stof<T>("32.586832771024153115841736464661367024747806973898205762818911765"),stof<T>("-10.176453568299202168243448569686147120741720682222187379634005653")}, std::complex<T>{stof<T>("-21.160959375218185046798604894489504992692371121408418086891751401"),stof<T>("-5.865673783083879973420461566111248426459407782879456858558975723")}, std::complex<T>{stof<T>("17.271625219662090912607795267992703281726162263884459014102458665"),stof<T>("21.138988593369706307284751557739988174950239183665002331673644356")}, std::complex<T>{stof<T>("5.8613010041561580768595981069769996007932050068084892608785453734"),stof<T>("-0.374554134215508883980872766880911682695456406389013909999050086")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_173_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_173_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("26.186762785843156371612863720404858726247262629530927245005616939"),stof<T>("-0.66459106497212509293170202386920653165777274837504852969350685")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_173_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_173_DLogXconstant_part(base_point<T>, kend);
	value += f_4_173_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_173_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_173_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_173_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_173_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_173_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_173_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
