/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_527.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_527_abbreviated (const std::array<T,61>& abb) {
T z[110];
z[0] = abb[19] + abb[21];
z[1] = abb[22] + z[0];
z[2] = abb[24] + z[1];
z[3] = abb[27] * (T(1) / T(2));
z[2] = z[2] * z[3];
z[4] = abb[24] * abb[28];
z[5] = abb[32] * z[1];
z[2] = z[2] + -z[4] + -z[5];
z[6] = (T(1) / T(2)) * z[1];
z[7] = abb[30] * z[6];
z[7] = z[2] + z[7];
z[7] = abb[30] * z[7];
z[8] = -abb[22] + abb[24];
z[3] = z[3] * z[8];
z[3] = z[3] + z[5];
z[3] = abb[27] * z[3];
z[5] = abb[20] + abb[22];
z[8] = abb[24] + z[5];
z[9] = abb[28] * (T(1) / T(2));
z[8] = z[8] * z[9];
z[9] = abb[24] * abb[27];
z[8] = z[8] + -z[9];
z[8] = abb[28] * z[8];
z[9] = abb[22] * (T(1) / T(2)) + z[0];
z[10] = prod_pow(abb[32], 2);
z[9] = z[9] * z[10];
z[11] = abb[25] * abb[37];
z[12] = abb[22] * abb[33];
z[13] = abb[59] * z[0];
z[14] = abb[36] * z[0];
z[15] = abb[24] + z[0];
z[16] = abb[57] * z[15];
z[17] = abb[35] * z[5];
z[3] = z[3] + -z[7] + z[8] + -z[9] + (T(1) / T(2)) * z[11] + -z[12] + z[13] + -z[14] + -z[16] + z[17];
z[7] = -abb[24] + z[1];
z[8] = abb[30] * (T(1) / T(2));
z[7] = z[7] * z[8];
z[1] = abb[20] + z[1];
z[8] = abb[31] * z[1];
z[9] = -abb[24] + z[1];
z[11] = abb[29] * z[9];
z[2] = z[2] + z[7] + z[8] + (T(-1) / T(2)) * z[11];
z[7] = 3 * abb[29];
z[2] = z[2] * z[7];
z[7] = abb[27] + abb[31] * (T(1) / T(2));
z[7] = z[0] * z[7];
z[5] = abb[28] * z[5];
z[8] = abb[32] * z[0];
z[5] = z[5] + z[7] + -z[8];
z[7] = 3 * abb[31];
z[5] = z[5] * z[7];
z[7] = prod_pow(m1_set::bc<T>[0], 2);
z[12] = (T(1) / T(2)) * z[9];
z[12] = z[7] * z[12];
z[13] = 3 * abb[58];
z[14] = z[9] * z[13];
z[2] = z[2] + 3 * z[3] + -z[5] + z[12] + z[14];
z[3] = abb[53] + abb[54] + abb[55];
z[5] = (T(1) / T(2)) * z[3];
z[5] = z[2] * z[5];
z[12] = abb[28] + abb[30] + -abb[32];
z[12] = abb[31] * z[12];
z[14] = -abb[27] + abb[28];
z[16] = abb[32] + z[14];
z[17] = -abb[31] + z[16];
z[17] = abb[29] * z[17];
z[18] = abb[27] * abb[32];
z[14] = abb[28] * z[14];
z[19] = abb[30] * z[16];
z[12] = abb[33] + -abb[35] + abb[57] + -abb[59] + z[10] + z[12] + -z[14] + z[17] + -z[18] + -z[19];
z[14] = abb[23] * z[12];
z[2] = z[2] + -3 * z[14];
z[2] = abb[56] * z[2];
z[14] = 3 * abb[47];
z[17] = 3 * abb[48];
z[18] = z[14] + z[17];
z[19] = abb[50] * (T(1) / T(2));
z[20] = abb[46] * (T(1) / T(2));
z[21] = z[19] + z[20];
z[22] = 2 * abb[43] + abb[44];
z[23] = abb[45] * (T(1) / T(2));
z[24] = abb[49] * (T(3) / T(2));
z[25] = -z[18] + z[21] + z[22] + z[23] + z[24];
z[25] = abb[1] * z[25];
z[26] = 2 * abb[50] + -z[14];
z[27] = abb[45] + abb[46];
z[28] = z[22] + z[26] + -z[27];
z[29] = 2 * abb[13];
z[28] = z[28] * z[29];
z[29] = abb[46] * (T(7) / T(2));
z[30] = z[24] + z[29];
z[31] = abb[50] * (T(5) / T(2));
z[32] = z[30] + -z[31];
z[33] = -abb[44] + -z[32];
z[34] = abb[43] + abb[45] * (T(7) / T(4));
z[33] = z[17] + (T(1) / T(2)) * z[33] + -z[34];
z[33] = abb[8] * z[33];
z[35] = abb[50] * (T(7) / T(2));
z[36] = 5 * abb[44];
z[37] = -z[30] + -z[35] + z[36];
z[37] = 5 * abb[43] + abb[45] * (T(17) / T(4)) + (T(1) / T(2)) * z[37];
z[37] = abb[4] * z[37];
z[38] = abb[44] + abb[50];
z[39] = -abb[49] + z[38];
z[39] = abb[43] + (T(1) / T(2)) * z[39];
z[39] = abb[9] * z[39];
z[40] = abb[9] * abb[47];
z[39] = z[39] + (T(-1) / T(2)) * z[40];
z[41] = 3 * z[39];
z[42] = -abb[46] + abb[49];
z[43] = abb[50] + z[42];
z[43] = -abb[47] + -z[23] + (T(1) / T(2)) * z[43];
z[44] = abb[7] * z[43];
z[45] = (T(3) / T(2)) * z[44];
z[46] = abb[46] * (T(5) / T(2)) + -z[24];
z[47] = abb[44] + -z[31] + -z[46];
z[34] = z[34] + (T(1) / T(2)) * z[47];
z[34] = abb[3] * z[34];
z[25] = z[25] + -z[28] + z[33] + z[34] + z[37] + z[41] + z[45];
z[25] = abb[31] * z[25];
z[33] = -abb[49] + z[22];
z[34] = abb[50] + z[33];
z[34] = abb[9] * z[34];
z[34] = z[34] + -z[40];
z[37] = 3 * z[34];
z[40] = 3 * z[44];
z[47] = z[37] + z[40];
z[32] = -6 * abb[48] + abb[45] * (T(7) / T(2)) + z[22] + z[32];
z[48] = abb[4] + abb[8];
z[32] = z[32] * z[48];
z[33] = -abb[48] + z[27] + z[33];
z[49] = 3 * abb[10];
z[50] = z[33] * z[49];
z[51] = abb[45] + -abb[50];
z[52] = abb[46] + abb[49] + z[51];
z[52] = -abb[48] + (T(1) / T(2)) * z[52];
z[53] = abb[5] * z[52];
z[54] = 3 * z[53];
z[55] = z[50] + z[54];
z[56] = 2 * abb[44];
z[27] = 4 * abb[43] + abb[50] + z[27] + z[56];
z[57] = 3 * abb[49];
z[58] = -z[27] + z[57];
z[59] = abb[1] * z[58];
z[60] = 2 * z[59];
z[58] = abb[3] * z[58];
z[61] = 6 * abb[47] + -z[22] + z[46];
z[35] = abb[45] * (T(-5) / T(2)) + z[35] + -z[61];
z[35] = abb[13] * z[35];
z[32] = z[32] + z[35] + -z[47] + -z[55] + -z[58] + -z[60];
z[32] = abb[32] * z[32];
z[35] = z[20] + z[22] + -z[24];
z[62] = z[19] + z[23] + z[35];
z[63] = abb[8] * z[62];
z[64] = abb[4] * z[62];
z[65] = z[63] + z[64];
z[66] = abb[3] * z[62];
z[66] = -z[59] + z[66];
z[28] = z[28] + -z[47] + -z[65] + z[66];
z[47] = -abb[27] * z[28];
z[67] = abb[50] * (T(3) / T(2));
z[68] = -z[23] + z[67];
z[69] = abb[49] * (T(1) / T(2));
z[70] = -z[22] + z[69];
z[71] = 2 * abb[47] + z[20] + -z[68] + z[70];
z[71] = abb[13] * z[71];
z[71] = z[34] + z[44] + z[71];
z[72] = abb[45] * (T(3) / T(2));
z[70] = -z[70] + z[72];
z[21] = -z[21] + z[70];
z[21] = abb[4] * z[21];
z[73] = z[42] + z[51];
z[74] = abb[3] * (T(1) / T(2));
z[75] = z[73] * z[74];
z[76] = abb[6] * z[73];
z[77] = (T(1) / T(2)) * z[76];
z[21] = z[21] + z[71] + z[75] + -z[77];
z[21] = 3 * z[21];
z[75] = -abb[28] * z[21];
z[78] = 2 * abb[8];
z[79] = 2 * abb[4];
z[80] = -z[78] + -z[79];
z[81] = 2 * abb[45];
z[82] = -abb[50] + z[22] + z[81];
z[83] = 2 * abb[46] + -z[17];
z[84] = z[82] + z[83];
z[80] = z[80] * z[84];
z[84] = abb[13] * z[62];
z[55] = z[55] + -z[66] + z[80] + z[84];
z[55] = abb[30] * z[55];
z[25] = z[25] + z[32] + z[47] + z[55] + z[75];
z[25] = abb[31] * z[25];
z[20] = abb[44] + z[20];
z[24] = z[20] + -z[24];
z[32] = abb[50] + z[24];
z[47] = abb[42] * (T(1) / T(4));
z[32] = abb[43] + (T(1) / T(2)) * z[32] + -z[47];
z[32] = abb[0] * z[32];
z[55] = abb[16] + abb[18];
z[75] = abb[52] * z[55];
z[80] = abb[14] * z[51];
z[85] = z[75] + -z[80];
z[86] = abb[51] * z[55];
z[87] = -z[85] + z[86];
z[88] = abb[14] * abb[42];
z[89] = (T(3) / T(4)) * z[88];
z[90] = abb[42] + z[51];
z[91] = abb[2] * z[90];
z[92] = 2 * z[91];
z[93] = -z[32] + (T(3) / T(4)) * z[87] + z[89] + -z[92];
z[94] = abb[45] * (T(3) / T(4));
z[95] = abb[50] * (T(1) / T(4)) + z[35] + z[47] + z[94];
z[95] = abb[3] * z[95];
z[96] = z[19] + z[24];
z[97] = abb[45] * (T(1) / T(4));
z[96] = abb[43] + (T(1) / T(2)) * z[96] + z[97];
z[98] = 3 * abb[12];
z[99] = z[96] * z[98];
z[100] = z[59] + z[99];
z[31] = z[24] + z[31];
z[31] = abb[42] + -abb[43] + (T(-1) / T(2)) * z[31] + z[94];
z[31] = abb[8] * z[31];
z[31] = -z[31] + z[84] + z[93] + z[95] + -z[100];
z[31] = abb[27] * z[31];
z[94] = abb[8] * z[90];
z[95] = z[64] + -z[84] + 2 * z[94];
z[101] = abb[42] * (T(1) / T(2));
z[102] = -abb[50] + -z[35] + z[101];
z[103] = abb[0] * z[102];
z[104] = z[87] + z[88];
z[90] = z[74] * z[90];
z[104] = z[90] + -4 * z[91] + -z[95] + z[103] + (T(3) / T(2)) * z[104];
z[105] = abb[28] * z[104];
z[58] = z[58] + -z[65] + -z[84];
z[65] = z[62] * z[98];
z[60] = z[58] + z[60] + z[65];
z[60] = abb[32] * z[60];
z[31] = z[31] + z[60] + -z[105];
z[65] = abb[50] * (T(-3) / T(4)) + -z[35] + z[47] + -z[97];
z[65] = abb[3] * z[65];
z[98] = -z[24] + z[67];
z[98] = -abb[42] + -abb[43] + abb[45] * (T(-5) / T(4)) + (T(1) / T(2)) * z[98];
z[98] = abb[8] * z[98];
z[65] = -z[64] + z[65] + z[93] + z[98] + z[100];
z[65] = abb[30] * z[65];
z[82] = -abb[46] + z[82];
z[79] = abb[3] + z[79];
z[79] = z[79] * z[82];
z[63] = -z[59] + -z[63] + (T(-3) / T(2)) * z[76] + z[79] + -z[84];
z[79] = abb[31] * z[63];
z[82] = abb[15] + (T(1) / T(2)) * z[55];
z[82] = abb[51] * z[82];
z[93] = (T(1) / T(2)) * z[80];
z[98] = abb[15] * abb[52];
z[77] = (T(-1) / T(2)) * z[75] + -z[77] + z[82] + z[93] + -z[98];
z[82] = abb[1] * z[62];
z[100] = abb[4] * z[96];
z[82] = z[82] + z[91] + z[100];
z[89] = z[82] + -z[89];
z[100] = z[24] + z[67];
z[97] = -abb[43] + z[97];
z[100] = -z[97] + (T(1) / T(2)) * z[100] + -z[101];
z[106] = -abb[8] * z[100];
z[107] = abb[42] * (T(5) / T(4));
z[67] = abb[44] + z[67];
z[108] = -abb[46] + z[67];
z[108] = -z[97] + -z[107] + (T(1) / T(2)) * z[108];
z[108] = abb[3] * z[108];
z[32] = -z[32] + (T(-3) / T(2)) * z[77] + z[89] + z[106] + z[108];
z[32] = abb[29] * z[32];
z[32] = -z[31] + z[32] + z[65] + z[79];
z[32] = abb[29] * z[32];
z[38] = -z[38] + z[46];
z[38] = abb[43] + -z[14] + (T(-1) / T(2)) * z[38] + z[107];
z[38] = abb[0] * z[38];
z[46] = abb[16] + -abb[18];
z[65] = abb[15] + (T(1) / T(2)) * z[46];
z[65] = abb[51] * z[65];
z[65] = z[65] + -z[98];
z[39] = -z[39] + (T(1) / T(4)) * z[80];
z[79] = abb[52] * z[46];
z[106] = -z[39] + (T(1) / T(2)) * z[65] + (T(-1) / T(4)) * z[79];
z[107] = abb[45] + z[35] + z[101];
z[108] = abb[8] * z[107];
z[23] = abb[43] + z[23];
z[24] = z[23] + (T(1) / T(2)) * z[24] + z[47];
z[24] = abb[3] * z[24];
z[24] = z[24] + -z[38] + -z[45] + z[89] + -z[99] + 3 * z[106] + z[108];
z[24] = abb[27] * z[24];
z[24] = z[24] + z[60];
z[24] = abb[27] * z[24];
z[60] = abb[45] + abb[50];
z[20] = abb[43] * (T(-10) / T(3)) + z[18] + (T(-5) / T(3)) * z[20] + (T(-5) / T(6)) * z[60] + -z[69];
z[20] = abb[1] * z[20];
z[60] = abb[46] * (T(-13) / T(2)) + z[36];
z[60] = abb[50] * (T(5) / T(3)) + (T(1) / T(3)) * z[60] + z[69];
z[89] = abb[42] * (T(13) / T(12));
z[106] = abb[43] * (T(5) / T(3));
z[14] = -z[14] + (T(1) / T(2)) * z[60] + z[89] + z[106];
z[14] = abb[0] * z[14];
z[60] = -z[76] + z[87];
z[87] = abb[46] * (T(23) / T(2)) + z[36];
z[87] = z[19] + z[69] + (T(1) / T(3)) * z[87];
z[108] = abb[45] * (T(7) / T(12)) + z[106];
z[87] = abb[42] * (T(-4) / T(3)) + -z[17] + (T(1) / T(2)) * z[87] + z[108];
z[87] = abb[8] * z[87];
z[109] = abb[44] + -abb[46];
z[109] = -z[19] + (T(-5) / T(3)) * z[109];
z[89] = z[89] + -z[108] + (T(1) / T(2)) * z[109];
z[89] = abb[3] * z[89];
z[29] = z[29] + -z[36];
z[29] = abb[50] * (T(7) / T(6)) + (T(1) / T(3)) * z[29] + z[69];
z[29] = abb[45] * (T(-17) / T(12)) + (T(1) / T(2)) * z[29] + -z[106];
z[29] = abb[4] * z[29];
z[36] = (T(1) / T(4)) * z[88];
z[14] = z[14] + z[20] + z[29] + z[36] + (T(1) / T(4)) * z[60] + z[87] + z[89] + (T(1) / T(3)) * z[91];
z[7] = z[7] * z[14];
z[14] = abb[46] + z[56];
z[14] = 8 * abb[43] + 2 * z[14] + -z[17] + z[26] + -z[57] + z[81];
z[14] = abb[1] * z[14];
z[20] = -abb[44] + abb[48] + z[42];
z[20] = (T(1) / T(2)) * z[20] + -z[23];
z[20] = z[20] * z[49];
z[20] = z[20] + (T(3) / T(2)) * z[53];
z[14] = z[14] + z[20] + -z[41] + z[45] + -z[58] + -z[99];
z[10] = z[10] * z[14];
z[14] = -z[30] + -z[67];
z[14] = 2 * abb[42] + (T(1) / T(2)) * z[14] + z[17] + z[97];
z[14] = abb[8] * z[14];
z[17] = abb[3] * z[100];
z[23] = abb[51] + -abb[52];
z[26] = -abb[16] * z[23];
z[14] = z[14] + z[17] + -z[20] + (T(3) / T(2)) * z[26] + z[82] + -z[99] + -z[103];
z[14] = abb[30] * z[14];
z[14] = z[14] + z[31];
z[14] = abb[30] * z[14];
z[17] = abb[12] * z[62];
z[17] = z[17] + -z[66];
z[20] = -z[17] + -z[71];
z[20] = abb[33] * z[20];
z[26] = -2 * abb[48] + abb[46] * (T(3) / T(2)) + -z[19] + z[70];
z[26] = z[26] * z[48];
z[29] = -abb[56] + -z[3];
z[6] = z[6] * z[29];
z[29] = -abb[10] * z[33];
z[6] = z[6] + -z[17] + z[26] + z[29] + -z[53];
z[6] = abb[34] * z[6];
z[6] = z[6] + z[20];
z[17] = -z[75] + z[76] + z[86];
z[17] = (T(-1) / T(4)) * z[17] + -z[36] + -z[39] + (T(1) / T(2)) * z[44];
z[20] = abb[42] * (T(5) / T(2));
z[26] = -z[20] + (T(3) / T(2)) * z[42] + -z[51];
z[26] = z[26] * z[74];
z[17] = 3 * z[17] + z[26] + -z[38] + z[91] + z[95];
z[17] = abb[28] * z[17];
z[26] = abb[27] * z[104];
z[17] = z[17] + z[26];
z[17] = abb[28] * z[17];
z[26] = -abb[36] * z[28];
z[28] = -z[88] + z[94];
z[29] = abb[4] * z[73];
z[30] = abb[15] + z[55];
z[30] = abb[51] * z[30];
z[29] = z[28] + -z[29] + -z[30] + z[76] + z[85] + z[98];
z[29] = (T(1) / T(2)) * z[29] + -z[90] + z[91];
z[13] = -z[13] * z[29];
z[30] = abb[23] * (T(3) / T(2));
z[30] = z[3] * z[30];
z[12] = -z[12] * z[30];
z[31] = (T(-1) / T(4)) * z[55];
z[31] = z[31] * z[51];
z[33] = -abb[15] + abb[17];
z[33] = z[33] * z[96];
z[36] = -abb[8] + 2 * abb[26] + abb[3] * (T(-3) / T(4));
z[36] = z[23] * z[36];
z[38] = -z[47] * z[55];
z[39] = (T(-1) / T(4)) * z[23];
z[39] = abb[14] * z[39];
z[31] = z[31] + z[33] + z[36] + z[38] + z[39];
z[23] = abb[0] * z[23];
z[23] = (T(-9) / T(4)) * z[23] + 3 * z[31];
z[23] = abb[37] * z[23];
z[31] = abb[15] + -abb[18];
z[31] = abb[51] * z[31];
z[33] = abb[18] * abb[52];
z[28] = z[28] + z[31] + z[33] + -z[80] + -z[98];
z[31] = abb[13] * z[43];
z[31] = z[31] + -z[44];
z[33] = -abb[47] + (T(1) / T(2)) * z[42] + z[101];
z[33] = abb[0] * z[33];
z[28] = (T(1) / T(2)) * z[28] + z[31] + -z[33] + z[91];
z[28] = 3 * z[28];
z[33] = -abb[57] * z[28];
z[21] = abb[35] * z[21];
z[36] = z[48] * z[52];
z[38] = abb[47] + abb[48] + -abb[49];
z[38] = abb[1] * z[38];
z[31] = z[31] + z[36] + z[38] + -z[53];
z[31] = 3 * z[31];
z[36] = abb[59] * z[31];
z[2] = abb[60] + (T(1) / T(2)) * z[2] + z[5] + 3 * z[6] + z[7] + z[10] + z[12] + z[13] + z[14] + z[17] + z[21] + z[23] + z[24] + z[25] + z[26] + z[32] + z[33] + z[36];
z[5] = -z[34] + -z[65] + (T(1) / T(2)) * z[79] + z[93];
z[6] = -abb[42] + z[19] + -z[35] + -z[72];
z[6] = abb[8] * z[6];
z[7] = -abb[3] * z[102];
z[10] = (T(3) / T(2)) * z[88] + -z[92];
z[12] = abb[50] + z[20] + -z[61];
z[12] = abb[0] * z[12];
z[5] = 3 * z[5] + z[6] + z[7] + z[10] + z[12] + z[40] + -z[59] + z[84];
z[5] = abb[27] * z[5];
z[6] = z[18] + -z[27];
z[6] = abb[1] * z[6];
z[7] = -z[50] + z[54];
z[6] = 2 * z[6] + -z[7] + z[37] + -z[40] + z[58];
z[6] = abb[32] * z[6];
z[10] = z[10] + z[59] + -z[64] + -z[103];
z[12] = abb[51] * z[46];
z[12] = z[12] + -z[79] + -z[80];
z[13] = abb[3] * z[107];
z[14] = -abb[42] + abb[45] + z[22] + z[83];
z[14] = z[14] * z[78];
z[7] = z[7] + -z[10] + (T(3) / T(2)) * z[12] + z[13] + z[14];
z[7] = abb[30] * z[7];
z[12] = -abb[42] + z[35] + z[68];
z[12] = abb[8] * z[12];
z[13] = abb[46] + z[20] + -z[22] + -z[68];
z[13] = abb[3] * z[13];
z[10] = z[10] + z[12] + z[13] + 3 * z[77];
z[10] = abb[29] * z[10];
z[5] = z[5] + z[6] + z[7] + z[10] + -z[105];
z[5] = m1_set::bc<T>[0] * z[5];
z[6] = abb[39] * z[29];
z[7] = abb[31] * m1_set::bc<T>[0];
z[10] = -z[7] * z[63];
z[12] = abb[24] * abb[30];
z[13] = abb[27] * z[15];
z[4] = z[4] + z[8] + z[11] + z[12] + -z[13];
z[4] = m1_set::bc<T>[0] * z[4];
z[1] = z[1] * z[7];
z[0] = abb[40] * z[0];
z[8] = abb[39] * z[9];
z[9] = abb[38] * z[15];
z[0] = z[0] + -z[1] + z[4] + z[8] + -z[9];
z[1] = (T(3) / T(2)) * z[3];
z[1] = z[0] * z[1];
z[3] = -abb[38] * z[28];
z[4] = m1_set::bc<T>[0] * z[16];
z[4] = -abb[38] + abb[40] + z[4] + -z[7];
z[7] = abb[23] * z[4];
z[0] = z[0] + z[7];
z[0] = abb[56] * z[0];
z[7] = abb[40] * z[31];
z[4] = z[4] * z[30];
z[0] = abb[41] + (T(3) / T(2)) * z[0] + z[1] + z[3] + z[4] + z[5] + -3 * z[6] + z[7] + z[10];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_527_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-12.647469062186067486841910742974404868797376292554273300051175834"),stof<T>("-32.658337759093888618082200129294428401278554834609311333167076193")}, std::complex<T>{stof<T>("-10.293053808775832717721009899336879750883574606940403753481863196"),stof<T>("27.731619358422268971912884190114824923332565285318170185721753227")}, std::complex<T>{stof<T>("-5.146526904387916358860504949668439875441787303470201876740931598"),stof<T>("13.865809679211134485956442095057412461666282642659085092860876613")}, std::complex<T>{stof<T>("2.5950042358985557438643716361737929768100917418995840993366333551"),stof<T>("11.9707155639956360629457743922804315937884199868170465049396740598")}, std::complex<T>{stof<T>("22.27159359148321089997389334597558122403465204867827240790978262"),stof<T>("14.927533426237727127980115252271100110822023273063294500677031204")}, std::complex<T>{stof<T>("27.020867969250850125997294606705381003227719642862572249811799095"),stof<T>("19.452185123951643192620569008360557671995532244014171037750863862")}, std::complex<T>{stof<T>("-15.639725824704900363543386068086837941856187743739759434226724311"),stof<T>("-26.238591946164474130431078670427989972227183207816396208172040982")}, std::complex<T>{stof<T>("-1.4853408623903941775700023282203034067366770014683110969421267106"),stof<T>("-2.5547511592843874835054786769005226002611227079059833853658668355")}, std::complex<T>{stof<T>("-7.7415311402864721027248765858422328522518790453697859760775649528"),stof<T>("1.8950941152154984230106677027769808678778626558420385879212025536")}, std::complex<T>{stof<T>("-4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("-16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_527_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_527_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-3 * m1_set::bc<T>[1] * (v[2] + v[3])) / tend;


		return (abb[45] + abb[47] + -abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[31], 2);
z[1] = -prod_pow(abb[28], 2);
z[0] = z[0] + z[1];
z[1] = -abb[45] + -abb[47] + abb[50];
return 3 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_527_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_527_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("13.4617138864491369806609356118891406511044252740502085718060189675"),stof<T>("-4.3112481116084540729719503268057823180106757399237961327991304953")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,61> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W19(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), T{0}};
abb[41] = SpDLog_f_4_527_W_19_Im(t, path, abb);
abb[60] = SpDLog_f_4_527_W_19_Re(t, path, abb);

                    
            return f_4_527_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_527_DLogXconstant_part(base_point<T>, kend);
	value += f_4_527_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_527_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_527_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_527_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_527_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_527_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_527_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
