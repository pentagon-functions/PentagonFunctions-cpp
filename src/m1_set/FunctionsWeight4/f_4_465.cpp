/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_465.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_465_abbreviated (const std::array<T,64>& abb) {
T z[100];
z[0] = abb[14] * (T(1) / T(4));
z[1] = abb[13] * (T(1) / T(4));
z[2] = -abb[9] + z[1];
z[3] = abb[3] * (T(1) / T(4)) + z[0] + -z[2];
z[4] = abb[1] + abb[6];
z[5] = abb[5] * (T(1) / T(4));
z[6] = -z[3] + z[4] + -z[5];
z[6] = abb[52] * z[6];
z[7] = abb[50] + abb[51];
z[8] = abb[3] * (T(1) / T(2));
z[9] = z[7] * z[8];
z[10] = abb[5] * (T(3) / T(4));
z[3] = -z[3] + -z[10];
z[3] = abb[48] * z[3];
z[11] = -abb[44] + abb[45] + -abb[46] + abb[47];
z[12] = -abb[24] * z[11];
z[13] = (T(1) / T(4)) * z[12];
z[14] = -abb[23] * z[11];
z[15] = (T(1) / T(4)) * z[14];
z[16] = abb[53] * z[4];
z[3] = z[3] + z[6] + z[9] + -z[13] + -z[15] + z[16];
z[3] = abb[32] * z[3];
z[6] = abb[22] * z[11];
z[9] = -abb[20] * z[11];
z[9] = z[6] + -z[9];
z[17] = -abb[19] * z[11];
z[18] = 3 * z[9] + z[14] + -z[17];
z[19] = abb[12] * (T(1) / T(4));
z[5] = abb[1] + z[5] + -z[19];
z[20] = -abb[14] + z[1];
z[21] = abb[6] * (T(3) / T(2));
z[22] = -abb[0] + -z[21];
z[22] = -z[5] + -z[20] + (T(1) / T(2)) * z[22];
z[22] = abb[48] * z[22];
z[23] = 3 * abb[6];
z[24] = abb[0] + z[23];
z[24] = (T(1) / T(4)) * z[24];
z[20] = -abb[1] + -z[20] + -z[24];
z[20] = abb[52] * z[20];
z[18] = z[12] + (T(1) / T(4)) * z[18] + z[20] + z[22];
z[20] = abb[31] * (T(1) / T(2));
z[18] = z[18] * z[20];
z[22] = abb[6] * (T(1) / T(2));
z[25] = abb[0] + z[22];
z[26] = abb[12] * (T(1) / T(2));
z[27] = z[25] + -z[26];
z[28] = abb[14] * (T(1) / T(2));
z[29] = z[8] + z[27] + -z[28];
z[29] = abb[48] * z[29];
z[30] = abb[0] + abb[3];
z[31] = -abb[14] + z[30];
z[32] = abb[52] * (T(1) / T(2));
z[31] = z[31] * z[32];
z[33] = -z[9] + z[17];
z[34] = (T(1) / T(2)) * z[33];
z[35] = (T(1) / T(2)) * z[12];
z[29] = z[29] + z[31] + z[34] + -z[35];
z[31] = abb[29] * (T(1) / T(2));
z[36] = -z[29] * z[31];
z[37] = -z[19] + (T(1) / T(2)) * z[25];
z[38] = abb[9] + z[8];
z[39] = -z[1] + z[10] + z[37] + z[38];
z[39] = abb[48] * z[39];
z[40] = abb[5] * (T(1) / T(2));
z[41] = z[8] + z[40];
z[42] = abb[9] + z[41];
z[23] = abb[0] + -z[23];
z[1] = -abb[1] + -z[1] + (T(1) / T(4)) * z[23] + z[42];
z[1] = abb[52] * z[1];
z[23] = z[14] + z[33];
z[1] = z[1] + (T(1) / T(4)) * z[23] + z[39];
z[1] = abb[27] * z[1];
z[39] = -z[14] + z[33];
z[43] = (T(1) / T(4)) * z[39];
z[37] = z[2] + z[37];
z[44] = -abb[14] + abb[5] * (T(-5) / T(4)) + z[37];
z[44] = abb[48] * z[44];
z[44] = z[43] + z[44];
z[45] = -abb[1] + z[28];
z[46] = z[40] + z[45];
z[47] = -abb[9] + z[24];
z[47] = abb[13] * (T(1) / T(8)) + -z[46] + (T(1) / T(2)) * z[47];
z[47] = abb[52] * z[47];
z[44] = -z[35] + (T(1) / T(2)) * z[44] + z[47];
z[44] = abb[28] * z[44];
z[47] = (T(1) / T(2)) * z[9];
z[48] = abb[1] + z[22];
z[49] = abb[9] + z[40] + -z[48];
z[49] = abb[52] * z[49];
z[50] = abb[9] + abb[5] * (T(3) / T(2)) + z[48];
z[50] = abb[48] * z[50];
z[49] = -z[47] + z[49] + z[50];
z[50] = abb[30] * (T(1) / T(2));
z[49] = z[49] * z[50];
z[51] = -abb[48] + abb[52];
z[52] = abb[30] * z[51];
z[53] = -abb[31] * z[51];
z[53] = z[52] + z[53];
z[53] = abb[7] * z[53];
z[1] = z[1] + z[3] + z[18] + z[36] + z[44] + z[49] + (T(1) / T(4)) * z[53];
z[1] = (T(1) / T(8)) * z[1];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[48] + abb[52];
z[18] = abb[6] + abb[13];
z[36] = -abb[8] + -abb[14] + z[18];
z[36] = z[3] * z[36];
z[44] = -abb[21] * z[11];
z[33] = z[33] + z[44];
z[36] = -z[14] + z[33] + z[36];
z[49] = abb[35] * z[36];
z[53] = -abb[0] + abb[12];
z[54] = -abb[5] + z[53];
z[55] = z[20] * z[54];
z[56] = z[26] + z[40];
z[57] = abb[0] * (T(1) / T(2));
z[58] = -abb[6] + -z[56] + z[57];
z[58] = abb[28] * z[58];
z[53] = abb[6] + z[53];
z[59] = abb[29] * z[53];
z[55] = z[55] + z[58] + z[59];
z[59] = -abb[27] * z[54];
z[59] = z[55] + z[59];
z[59] = m1_set::bc<T>[0] * z[59];
z[60] = abb[5] + -abb[12];
z[61] = -abb[36] * z[60];
z[62] = abb[0] * abb[36];
z[61] = z[61] + -z[62];
z[63] = -abb[16] + abb[17];
z[64] = (T(1) / T(2)) * z[63];
z[65] = -abb[40] * z[64];
z[59] = z[59] + -z[61] + z[65];
z[59] = abb[49] * z[59];
z[65] = abb[37] + abb[39];
z[6] = z[6] * z[65];
z[66] = -abb[39] * z[11];
z[67] = -abb[37] * z[11];
z[66] = z[66] + z[67];
z[68] = abb[36] * z[11];
z[68] = -z[66] + z[68];
z[68] = abb[21] * z[68];
z[69] = abb[35] + -abb[36];
z[69] = -z[11] * z[69];
z[67] = z[67] + z[69];
z[67] = abb[24] * z[67];
z[6] = -z[6] + z[49] + -z[59] + -z[67] + -z[68];
z[49] = abb[36] + abb[37];
z[59] = abb[8] * (T(1) / T(2));
z[49] = z[49] * z[59];
z[67] = -abb[36] + abb[37];
z[67] = z[28] * z[67];
z[65] = z[8] * z[65];
z[49] = z[49] + -z[65] + z[67];
z[65] = -z[22] + z[59];
z[67] = -abb[9] + z[65];
z[67] = abb[39] * z[67];
z[68] = abb[36] + -abb[39];
z[68] = z[40] * z[68];
z[69] = -abb[9] + -z[22];
z[69] = abb[37] * z[69];
z[70] = -abb[36] * z[26];
z[62] = z[49] + z[62] + z[67] + z[68] + z[69] + z[70];
z[62] = abb[43] * (T(-1) / T(32)) + (T(1) / T(16)) * z[62];
z[62] = abb[48] * z[62];
z[67] = abb[0] * (T(1) / T(4));
z[5] = z[5] + z[67];
z[5] = abb[31] * z[5];
z[68] = z[31] * z[53];
z[69] = (T(1) / T(2)) * z[58];
z[70] = abb[27] * (T(1) / T(2));
z[71] = z[54] * z[70];
z[72] = -abb[1] + abb[3];
z[73] = abb[30] * z[72];
z[5] = -z[5] + z[68] + z[69] + -z[71] + -z[73];
z[5] = m1_set::bc<T>[0] * z[5];
z[68] = abb[40] * z[63];
z[73] = abb[38] * z[72];
z[5] = z[5] + (T(-1) / T(2)) * z[61] + (T(-1) / T(4)) * z[68] + -z[73];
z[61] = abb[51] * (T(1) / T(16));
z[68] = abb[50] * (T(1) / T(16));
z[73] = z[61] + z[68];
z[5] = z[5] * z[73];
z[73] = z[8] + -z[40];
z[74] = z[45] + -z[59];
z[75] = z[73] + z[74];
z[75] = abb[48] * z[75];
z[74] = z[41] + z[74];
z[74] = abb[52] * z[74];
z[76] = z[17] + z[44];
z[76] = (T(1) / T(2)) * z[76];
z[74] = z[35] + z[74] + z[75] + z[76];
z[75] = abb[7] * z[51];
z[77] = (T(-1) / T(16)) * z[74] + (T(1) / T(32)) * z[75];
z[77] = abb[38] * z[77];
z[78] = z[22] + z[59];
z[79] = -abb[1] + z[40];
z[80] = -abb[9] + z[78] + -z[79];
z[80] = abb[39] * z[80];
z[81] = abb[36] * z[57];
z[82] = -abb[9] + z[22];
z[82] = abb[37] * z[82];
z[49] = z[49] + z[80] + z[81] + z[82];
z[49] = abb[52] * z[49];
z[80] = abb[15] + abb[18];
z[81] = z[63] + z[80];
z[82] = -abb[48] * z[81];
z[83] = -abb[25] * z[11];
z[84] = -abb[52] * z[80];
z[82] = z[82] + z[83] + z[84];
z[82] = abb[40] * z[82];
z[84] = abb[15] + -abb[17];
z[85] = abb[29] * z[84];
z[86] = -abb[15] + z[64];
z[86] = abb[28] * z[86];
z[85] = z[85] + z[86];
z[87] = abb[27] + -z[20];
z[88] = abb[16] + abb[17];
z[87] = z[87] * z[88];
z[87] = z[85] + z[87];
z[87] = m1_set::bc<T>[0] * z[87];
z[89] = abb[36] * z[88];
z[90] = abb[0] + abb[5];
z[91] = abb[12] + z[90];
z[92] = abb[40] * z[91];
z[87] = z[87] + z[89] + (T(1) / T(2)) * z[92];
z[89] = -abb[26] * abb[40];
z[87] = (T(1) / T(2)) * z[87] + z[89];
z[89] = abb[54] * (T(1) / T(16));
z[87] = z[87] * z[89];
z[92] = abb[19] + abb[20];
z[92] = (T(-1) / T(32)) * z[92];
z[66] = z[66] * z[92];
z[92] = abb[28] * z[4];
z[93] = abb[30] * z[4];
z[94] = z[92] + -z[93];
z[95] = abb[27] * z[4];
z[95] = -z[94] + z[95];
z[96] = -m1_set::bc<T>[0] * z[95];
z[97] = abb[39] * z[4];
z[97] = abb[43] * (T(1) / T(2)) + z[97];
z[96] = z[96] + (T(1) / T(2)) * z[97];
z[96] = abb[53] * z[96];
z[97] = abb[28] * z[51];
z[98] = -z[52] + z[97];
z[98] = m1_set::bc<T>[0] * z[98];
z[99] = -abb[37] * z[51];
z[98] = z[98] + z[99];
z[99] = abb[4] * (T(1) / T(32));
z[98] = z[98] * z[99];
z[1] = abb[61] + abb[62] + z[1] + z[5] + (T(-1) / T(32)) * z[6] + (T(1) / T(16)) * z[49] + z[62] + z[66] + z[77] + (T(1) / T(64)) * z[82] + z[87] + (T(1) / T(8)) * z[96] + z[98];
z[5] = -z[26] + z[59];
z[6] = abb[13] * (T(1) / T(2));
z[26] = z[5] + -z[6] + z[38] + z[90];
z[26] = abb[48] * z[26];
z[49] = -z[6] + z[42];
z[62] = -z[4] + z[49] + z[57] + z[59];
z[62] = abb[52] * z[62];
z[66] = z[14] + -z[44];
z[26] = z[26] + z[62] + (T(1) / T(2)) * z[66];
z[26] = z[26] * z[70];
z[10] = -z[10] + -z[28] + z[37];
z[10] = abb[48] * z[10];
z[2] = z[2] + z[24] + -z[46];
z[2] = abb[52] * z[2];
z[2] = z[2] + z[10] + -z[35] + z[43];
z[2] = abb[28] * z[2];
z[10] = z[42] + -z[59];
z[24] = z[10] + -z[48];
z[24] = abb[52] * z[24];
z[10] = z[10] + z[22];
z[10] = abb[48] * z[10];
z[33] = (T(1) / T(2)) * z[33];
z[10] = z[10] + z[24] + z[33];
z[24] = abb[30] * z[10];
z[37] = abb[14] + -z[6] + -z[27] + -z[40];
z[37] = abb[48] * z[37];
z[18] = -abb[0] + -z[18];
z[18] = abb[14] + (T(1) / T(2)) * z[18];
z[18] = abb[52] * z[18];
z[18] = z[12] + z[18] + z[37] + (T(-1) / T(2)) * z[39];
z[18] = z[18] * z[20];
z[37] = -abb[29] * z[29];
z[2] = z[2] + z[18] + z[24] + z[26] + z[37];
z[2] = abb[27] * z[2];
z[18] = -z[22] + z[45];
z[24] = abb[5] + abb[9];
z[26] = z[18] + z[24];
z[26] = abb[48] * z[26];
z[26] = z[16] + -z[26] + -z[47];
z[37] = (T(-1) / T(2)) * z[7];
z[39] = abb[1] + abb[3];
z[37] = z[37] * z[39];
z[21] = abb[9] + -z[21];
z[0] = -abb[1] + z[0] + (T(1) / T(2)) * z[21];
z[0] = abb[52] * z[0];
z[0] = z[0] + z[13] + (T(-1) / T(2)) * z[26] + z[37] + (T(-1) / T(4)) * z[75];
z[0] = abb[32] * z[0];
z[13] = z[28] + z[49];
z[21] = -z[4] + z[13];
z[21] = abb[52] * z[21];
z[13] = abb[48] * z[13];
z[11] = (T(-1) / T(2)) * z[11];
z[11] = abb[23] * z[11];
z[11] = z[11] + z[13] + z[21] + z[35];
z[13] = -abb[27] + abb[28];
z[11] = z[11] * z[13];
z[3] = z[3] * z[18];
z[3] = z[3] + z[35] + z[47];
z[13] = -abb[31] * z[3];
z[18] = abb[3] * abb[30];
z[21] = abb[1] * abb[31];
z[18] = z[18] + z[21];
z[18] = z[7] * z[18];
z[21] = -abb[48] * z[24];
z[24] = -abb[9] + z[4];
z[24] = abb[52] * z[24];
z[21] = z[21] + z[24];
z[21] = abb[30] * z[21];
z[24] = z[20] * z[75];
z[26] = abb[53] * z[95];
z[0] = z[0] + z[11] + z[13] + z[18] + z[21] + z[24] + z[26];
z[0] = abb[32] * z[0];
z[4] = -z[4] * z[70];
z[4] = z[4] + z[94];
z[4] = abb[27] * z[4];
z[11] = -abb[0] + abb[2];
z[13] = -z[11] + z[72];
z[18] = prod_pow(abb[28], 2);
z[21] = (T(1) / T(2)) * z[18];
z[13] = z[13] * z[21];
z[24] = z[92] + (T(-1) / T(2)) * z[93];
z[24] = abb[30] * z[24];
z[26] = -abb[28] + z[31];
z[30] = -abb[2] + abb[6] + z[30];
z[37] = abb[29] * z[26] * z[30];
z[42] = abb[0] + -abb[6];
z[42] = (T(1) / T(2)) * z[42];
z[43] = -abb[1] + z[8];
z[45] = abb[2] * (T(-1) / T(2)) + z[42] + z[43];
z[47] = prod_pow(m1_set::bc<T>[0], 2);
z[49] = (T(1) / T(3)) * z[47];
z[45] = z[45] * z[49];
z[4] = z[4] + z[13] + z[24] + z[37] + z[45];
z[4] = abb[53] * z[4];
z[11] = z[11] + z[22] + z[56];
z[13] = abb[49] + abb[51];
z[13] = z[11] * z[13];
z[22] = -z[27] + -z[41];
z[22] = abb[48] * z[22];
z[24] = (T(1) / T(2)) * z[51];
z[27] = abb[4] * z[24];
z[8] = -abb[52] * z[8];
z[30] = abb[53] * z[30];
z[8] = z[8] + z[13] + z[22] + z[27] + z[30] + -z[34];
z[8] = abb[34] * z[8];
z[13] = -z[28] + z[38];
z[22] = z[13] + -z[65];
z[22] = abb[48] * z[22];
z[13] = z[13] + -z[78];
z[13] = abb[52] * z[13];
z[13] = z[13] + z[22] + z[27] + z[33] + -z[35];
z[13] = abb[57] * z[13];
z[22] = -z[6] + z[48];
z[27] = -z[22] + -z[73];
z[27] = abb[48] * z[27];
z[22] = -z[22] + -z[41];
z[22] = abb[52] * z[22];
z[30] = -z[7] * z[39];
z[14] = z[9] + -z[14];
z[33] = (T(1) / T(2)) * z[75];
z[14] = (T(1) / T(2)) * z[14] + z[22] + z[27] + z[30] + -z[33];
z[14] = abb[33] * z[14];
z[22] = abb[5] + -z[43] + z[59];
z[22] = abb[48] * z[22];
z[27] = -abb[3] + abb[8];
z[27] = z[27] * z[32];
z[22] = z[22] + z[27] + -z[76];
z[22] = z[22] * z[50];
z[27] = -abb[52] * z[46];
z[30] = abb[5] + abb[14];
z[30] = abb[48] * z[30];
z[27] = z[27] + (T(-1) / T(2)) * z[30] + -z[35];
z[27] = abb[28] * z[27];
z[22] = z[22] + z[27];
z[22] = abb[30] * z[22];
z[6] = -abb[3] + z[6];
z[25] = z[6] + -z[25] + z[56];
z[25] = abb[48] * z[25];
z[6] = z[6] + -z[42];
z[6] = abb[52] * z[6];
z[6] = z[6] + (T(-1) / T(2)) * z[23] + z[25];
z[23] = abb[28] * (T(1) / T(2));
z[6] = z[6] * z[23];
z[3] = abb[30] * z[3];
z[3] = z[3] + z[6];
z[3] = abb[31] * z[3];
z[6] = abb[31] * z[29];
z[25] = -abb[0] + abb[14];
z[27] = abb[52] * z[25];
z[27] = z[12] + z[27] + z[30];
z[23] = abb[29] * (T(-1) / T(4)) + z[23];
z[23] = z[23] * z[27];
z[6] = z[6] + z[23];
z[6] = abb[29] * z[6];
z[23] = -abb[0] + abb[5];
z[23] = abb[2] + (T(1) / T(2)) * z[23];
z[27] = -z[23] * z[26];
z[29] = z[20] * z[53];
z[27] = z[27] + z[29];
z[27] = abb[29] * z[27];
z[29] = z[55] + -z[71];
z[29] = z[29] * z[70];
z[27] = z[27] + -z[29];
z[23] = z[18] * z[23];
z[29] = abb[56] * z[54];
z[30] = abb[60] * z[64];
z[23] = z[23] + z[29] + z[30];
z[29] = -abb[31] * z[58];
z[29] = z[23] + z[29];
z[30] = -abb[2] + z[60];
z[32] = abb[0] + abb[6] * (T(1) / T(3));
z[30] = (T(-1) / T(3)) * z[30] + (T(-1) / T(2)) * z[32];
z[32] = (T(1) / T(2)) * z[47];
z[34] = z[30] * z[32];
z[29] = -z[27] + (T(1) / T(2)) * z[29] + z[34];
z[29] = abb[49] * z[29];
z[7] = z[7] * z[72];
z[7] = z[7] + -z[33] + z[74];
z[7] = abb[58] * z[7];
z[10] = z[10] + -z[16];
z[10] = abb[59] * z[10];
z[5] = -abb[0] + -z[5] + z[28] + -z[40];
z[5] = abb[56] * z[5];
z[16] = abb[60] * z[81];
z[5] = z[5] + (T(1) / T(4)) * z[16];
z[5] = abb[48] * z[5];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[10] + z[13] + z[14] + z[22] + z[29];
z[2] = -z[20] + z[70];
z[2] = z[2] * z[88];
z[2] = z[2] + z[85];
z[2] = abb[27] * z[2];
z[3] = abb[60] * (T(1) / T(2));
z[4] = -z[3] * z[91];
z[5] = abb[16] * z[26];
z[6] = -abb[31] * z[84];
z[5] = z[5] + z[6];
z[5] = abb[29] * z[5];
z[6] = abb[15] * (T(-1) / T(2)) + -z[88];
z[6] = z[6] * z[49];
z[7] = -abb[56] * z[88];
z[8] = abb[16] * z[21];
z[10] = -abb[31] * z[86];
z[13] = abb[15] + -z[63];
z[13] = abb[34] * z[13];
z[2] = z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[10] + z[13];
z[4] = abb[26] * abb[60];
z[2] = (T(1) / T(2)) * z[2] + z[4];
z[2] = z[2] * z[89];
z[4] = abb[3] * (T(1) / T(6)) + abb[13] * (T(1) / T(12));
z[5] = abb[1] * (T(1) / T(4));
z[6] = abb[5] * (T(1) / T(8));
z[7] = abb[9] + -z[67];
z[7] = -abb[8] + (T(1) / T(2)) * z[7];
z[7] = z[4] + -z[5] + z[6] + (T(1) / T(3)) * z[7];
z[7] = abb[52] * z[7];
z[8] = (T(-5) / T(8)) * z[9] + -z[15] + z[44];
z[9] = -abb[0] + abb[9] + abb[6] * (T(5) / T(4));
z[5] = -abb[8] + -z[5] + (T(1) / T(2)) * z[9] + z[19];
z[4] = z[4] + (T(1) / T(3)) * z[5] + -z[6];
z[4] = abb[48] * z[4];
z[4] = z[4] + z[7] + (T(1) / T(3)) * z[8] + (T(1) / T(4)) * z[17] + (T(-1) / T(24)) * z[75];
z[4] = z[4] * z[47];
z[5] = prod_pow(abb[30], 2);
z[6] = z[5] * z[72];
z[6] = z[6] + -z[23];
z[7] = -z[30] + (T(-1) / T(3)) * z[72];
z[7] = z[7] * z[32];
z[8] = abb[1] * abb[30];
z[8] = z[8] + z[69];
z[8] = abb[31] * z[8];
z[6] = (T(1) / T(2)) * z[6] + z[7] + z[8] + z[27];
z[7] = -z[6] * z[61];
z[8] = abb[34] * z[11];
z[6] = -z[6] + z[8];
z[6] = z[6] * z[68];
z[8] = -abb[30] * z[24];
z[8] = z[8] + z[97];
z[8] = abb[30] * z[8];
z[9] = z[31] * z[51];
z[9] = z[9] + -z[97];
z[9] = abb[29] * z[9];
z[10] = z[47] * z[51];
z[8] = z[8] + z[9] + (T(1) / T(6)) * z[10];
z[8] = z[8] * z[99];
z[9] = -z[12] + z[36];
z[9] = abb[55] * z[9];
z[5] = z[5] * z[24];
z[10] = -abb[31] * z[52];
z[5] = z[5] + z[10];
z[5] = abb[7] * z[5];
z[10] = abb[48] + -abb[53];
z[10] = abb[63] * z[10];
z[5] = z[5] + z[9] + z[10];
z[3] = z[3] * z[80];
z[9] = z[57] + z[79];
z[9] = z[9] * z[18];
z[10] = -abb[8] + z[25];
z[10] = abb[56] * z[10];
z[3] = z[3] + z[9] + z[10];
z[3] = (T(1) / T(32)) * z[3];
z[3] = abb[52] * z[3];
z[9] = z[12] + z[44];
z[9] = (T(1) / T(32)) * z[9];
z[9] = abb[56] * z[9];
z[10] = abb[60] * z[83];
z[0] = abb[41] + abb[42] + (T(1) / T(16)) * z[0] + z[2] + z[3] + (T(1) / T(8)) * z[4] + (T(1) / T(32)) * z[5] + z[6] + z[7] + z[8] + z[9] + (T(-1) / T(64)) * z[10];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_465_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.020898306371438252515741060746933841823397787250839508815725475101"),stof<T>("0.13574022450088939322201724851925175755513123840040319374065701043")}, std::complex<T>{stof<T>("0.1351135996422727134439098190793225223541996862176824857816791106"),stof<T>("0.30254326910386149651592487300032369275821954141726113707824483379")}, std::complex<T>{stof<T>("0.24780643431348467770013527172102095898841715112460425594970456891"),stof<T>("0.46403429348042305992986401397481012610314291936457940631858096974")}, std::complex<T>{stof<T>("0.24780643431348467770013527172102095898841715112460425594970456891"),stof<T>("0.46403429348042305992986401397481012610314291936457940631858096974")}, std::complex<T>{stof<T>("0.053288561085580309456759226098342009758950786597486899487371394669"),stof<T>("0.061842275467620006176704672420265680252001586638264831807137015426")}, std::complex<T>{stof<T>("-0.12914315315118745546968416203968801705472160933480442966735225988"),stof<T>("-0.41731877743565414418514426699660811792460362213415030181661970505")}, std::complex<T>{stof<T>("0.052678935884231730299906118451924879625719695309925263090767030428"),stof<T>("0.119129763596743450607089913422324455774389719436512833921156798303")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_465_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_465_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(128)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (-v[3] + v[5])) / tend;


		return (abb[49] + abb[50] + abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[49] + abb[50] + abb[51];
z[1] = abb[28] + -abb[29];
return abb[10] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_465_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(128)) * (-v[3] + v[5]) * (-6 * v[0] + 2 * v[1] + -4 * (2 + v[3]) + m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(-1) / T(32)) * (-v[3] + v[5])) / tend;


		return (abb[49] + abb[50] + abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = abb[27] + abb[29] + abb[28] * (T(-1) / T(2));
z[0] = abb[28] * z[0];
z[1] = abb[27] + abb[29] * (T(1) / T(2));
z[1] = abb[29] * z[1];
z[2] = abb[28] + -abb[29];
z[2] = abb[31] * z[2];
z[0] = z[0] + -z[1] + -z[2];
z[0] = -abb[34] + (T(1) / T(2)) * z[0];
z[1] = abb[49] + abb[50] + abb[51];
return abb[10] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_465_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[50] + abb[51] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[50] + abb[51] + abb[52];
z[1] = abb[31] + -abb[32];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_465_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + -2 * v[0] + v[1] + v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[50] + abb[51] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + abb[32];
z[1] = -abb[31] + abb[32];
z[0] = z[0] * z[1];
z[0] = abb[33] + (T(1) / T(2)) * z[0];
z[1] = abb[50] + abb[51] + abb[52];
return abb[11] * (T(1) / T(8)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_465_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.36305763688590465716802049969219397096264130247739573151632483067"),stof<T>("0.23018706837600162262874729035368137348782582526734162686533743354")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,64> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W16(k,dl), dlog_W20(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[41] = SpDLog_f_4_465_W_16_Im(t, path, abb);
abb[42] = SpDLog_f_4_465_W_20_Im(t, path, abb);
abb[43] = SpDLogQ_W_84(k,dl,dlr).imag();
abb[61] = SpDLog_f_4_465_W_16_Re(t, path, abb);
abb[62] = SpDLog_f_4_465_W_20_Re(t, path, abb);
abb[63] = SpDLogQ_W_84(k,dl,dlr).real();

                    
            return f_4_465_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_465_DLogXconstant_part(base_point<T>, kend);
	value += f_4_465_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_465_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_465_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_465_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_465_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_465_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_465_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
