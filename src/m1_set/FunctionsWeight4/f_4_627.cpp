/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_627.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_627_abbreviated (const std::array<T,63>& abb) {
T z[122];
z[0] = 4 * abb[29];
z[1] = 4 * abb[32];
z[2] = abb[33] * (T(1) / T(2));
z[3] = abb[31] + z[0] + -z[1] + z[2];
z[3] = abb[33] * z[3];
z[4] = 2 * abb[31];
z[5] = -abb[32] + z[4];
z[5] = abb[32] * z[5];
z[6] = -abb[31] + abb[32];
z[7] = 2 * z[6];
z[8] = -abb[29] + z[7];
z[9] = abb[29] * z[8];
z[10] = 3 * abb[59];
z[11] = prod_pow(abb[31], 2);
z[12] = (T(1) / T(2)) * z[11];
z[13] = prod_pow(m1_set::bc<T>[0], 2);
z[3] = -4 * abb[35] + z[3] + z[5] + z[9] + -z[10] + z[12] + (T(-19) / T(6)) * z[13];
z[3] = abb[1] * z[3];
z[5] = 2 * abb[29];
z[9] = (T(1) / T(2)) * z[6];
z[14] = z[5] + -z[9];
z[14] = abb[29] * z[14];
z[15] = abb[31] * (T(1) / T(2));
z[16] = -abb[32] + z[15];
z[16] = abb[32] * z[16];
z[14] = z[12] + z[14] + z[16];
z[14] = abb[0] * z[14];
z[17] = z[3] + -z[14];
z[18] = 2 * abb[32];
z[19] = z[5] + -z[18];
z[20] = abb[33] * (T(5) / T(4));
z[21] = -z[15] + -z[19] + z[20];
z[21] = abb[33] * z[21];
z[22] = abb[29] * (T(1) / T(2));
z[23] = -z[15] + z[22];
z[24] = z[18] + z[23];
z[25] = abb[30] * (T(5) / T(4));
z[26] = abb[34] * (T(5) / T(2));
z[27] = -z[24] + -z[25] + z[26];
z[27] = abb[30] * z[27];
z[24] = abb[34] * (T(5) / T(4)) + -z[24];
z[24] = abb[34] * z[24];
z[28] = abb[36] * (T(5) / T(2));
z[24] = z[24] + z[28];
z[29] = abb[32] * z[15];
z[30] = abb[37] + z[29];
z[31] = -abb[59] + z[30];
z[32] = 3 * z[31];
z[33] = abb[29] * z[6];
z[34] = -z[32] + (T(-5) / T(2)) * z[33];
z[35] = abb[38] * (T(1) / T(2));
z[27] = abb[35] * (T(5) / T(2)) + z[21] + -z[24] + z[27] + (T(1) / T(2)) * z[34] + -z[35];
z[27] = abb[7] * z[27];
z[34] = abb[30] * (T(7) / T(4));
z[36] = abb[33] * (T(3) / T(2));
z[37] = z[34] + -z[36];
z[38] = -z[7] + -z[22] + z[26] + z[37];
z[38] = abb[30] * z[38];
z[39] = abb[32] * (T(1) / T(2));
z[40] = abb[31] + z[39];
z[41] = abb[33] * (T(1) / T(4)) + z[22] + -z[40];
z[41] = abb[33] * z[41];
z[42] = abb[58] * (T(3) / T(2));
z[41] = z[41] + -z[42];
z[10] = z[10] + -z[11];
z[10] = (T(1) / T(2)) * z[10];
z[43] = -z[10] + z[41];
z[44] = prod_pow(abb[29], 2);
z[45] = z[35] + (T(1) / T(4)) * z[44];
z[46] = z[43] + -z[45];
z[47] = abb[37] * (T(3) / T(2));
z[16] = z[16] + -z[24] + z[38] + z[46] + -z[47];
z[16] = abb[5] * z[16];
z[24] = -abb[34] + z[22];
z[25] = z[24] + z[25] + -z[36] + z[40];
z[25] = abb[30] * z[25];
z[38] = abb[29] + -abb[31];
z[40] = abb[34] * (T(1) / T(4)) + -z[38] + z[39];
z[40] = abb[34] * z[40];
z[48] = abb[57] * (T(3) / T(2));
z[40] = -z[35] + z[40] + -z[48];
z[49] = z[6] * z[22];
z[32] = z[32] + -z[49];
z[50] = -abb[35] + abb[36];
z[32] = z[25] + (T(1) / T(2)) * z[32] + -z[40] + z[41] + z[50];
z[32] = abb[4] * z[32];
z[41] = abb[31] + abb[32];
z[51] = abb[32] * z[41];
z[52] = 3 * abb[37];
z[51] = -z[12] + z[51] + z[52];
z[53] = z[6] + z[22];
z[53] = abb[29] * z[53];
z[54] = -abb[34] + z[38];
z[55] = 3 * abb[34];
z[56] = z[54] * z[55];
z[57] = 3 * abb[57];
z[53] = -z[51] + z[53] + -z[56] + -z[57];
z[56] = (T(1) / T(6)) * z[13];
z[58] = -z[53] + z[56];
z[58] = abb[2] * z[58];
z[0] = z[0] + -z[9];
z[59] = abb[33] * (T(9) / T(2));
z[60] = 4 * abb[30];
z[61] = abb[34] * (T(7) / T(2));
z[62] = z[0] + -z[59] + z[60] + -z[61];
z[62] = abb[30] * z[62];
z[63] = -abb[29] + abb[34];
z[64] = -z[6] + z[63];
z[65] = abb[34] * (T(1) / T(2));
z[66] = z[64] * z[65];
z[67] = -abb[29] + abb[33];
z[68] = z[6] + z[67];
z[68] = abb[33] * z[68];
z[33] = z[33] + -z[68];
z[69] = -7 * abb[35] + -9 * abb[58] + -z[33];
z[70] = abb[36] * (T(7) / T(2));
z[62] = 4 * abb[38] + abb[57] * (T(9) / T(2)) + z[62] + -z[66] + (T(1) / T(2)) * z[69] + z[70];
z[62] = abb[16] * z[62];
z[69] = -abb[29] + z[6];
z[71] = z[22] * z[69];
z[72] = abb[29] + -abb[32];
z[73] = abb[33] * z[72];
z[29] = -abb[59] + z[29];
z[74] = -abb[35] + z[29];
z[71] = z[71] + z[73] + z[74];
z[71] = -z[13] + 3 * z[71];
z[73] = abb[15] * (T(1) / T(2));
z[75] = z[71] * z[73];
z[76] = -abb[30] + abb[33];
z[77] = -abb[31] + abb[33];
z[76] = z[76] * z[77];
z[76] = abb[58] + abb[59] + -z[76];
z[76] = (T(5) / T(2)) * z[13] + 3 * z[76];
z[78] = abb[8] * z[76];
z[78] = (T(1) / T(2)) * z[78];
z[79] = z[75] + z[78];
z[80] = prod_pow(abb[30], 2);
z[81] = prod_pow(abb[33], 2);
z[80] = z[80] + -z[81];
z[82] = 3 * abb[13];
z[83] = z[80] * z[82];
z[83] = -z[79] + z[83];
z[84] = prod_pow(abb[34], 2);
z[85] = z[13] + -z[44] + z[84];
z[86] = 3 * abb[10];
z[85] = z[85] * z[86];
z[84] = -z[44] + z[81] + z[84];
z[86] = abb[34] + z[67];
z[87] = abb[30] * (T(1) / T(2));
z[88] = z[86] + -z[87];
z[88] = abb[30] * z[88];
z[89] = abb[38] + -abb[58];
z[88] = z[88] + -z[89];
z[90] = z[50] + -z[88];
z[91] = abb[57] + (T(1) / T(2)) * z[84] + z[90];
z[91] = abb[6] * z[91];
z[92] = abb[30] + -abb[34];
z[93] = -abb[32] + abb[34];
z[94] = -z[92] * z[93];
z[94] = abb[36] + abb[37] + z[94];
z[95] = (T(1) / T(2)) * z[13];
z[96] = 3 * z[94] + -z[95];
z[96] = abb[3] * z[96];
z[97] = (T(1) / T(2)) * z[96];
z[98] = -z[2] + z[69];
z[98] = abb[33] * z[98];
z[99] = abb[29] * (T(3) / T(2)) + -z[6];
z[99] = abb[29] * z[99];
z[98] = abb[35] + z[98] + z[99];
z[99] = abb[9] * z[98];
z[100] = -abb[17] + abb[19];
z[101] = abb[18] + abb[20] + z[100];
z[102] = abb[60] * (T(3) / T(4));
z[103] = -z[101] * z[102];
z[104] = abb[0] * (T(13) / T(6));
z[105] = abb[7] * (T(1) / T(4));
z[106] = -abb[13] + z[105];
z[106] = abb[4] * (T(-3) / T(4)) + abb[5] * (T(-1) / T(6)) + -z[104] + 3 * z[106];
z[106] = z[13] * z[106];
z[16] = z[16] + -z[17] + z[27] + z[32] + z[58] + z[62] + -z[83] + z[85] + (T(-9) / T(2)) * z[91] + z[97] + -z[99] + z[103] + z[106];
z[16] = abb[53] * z[16];
z[27] = z[18] * z[41];
z[27] = 6 * abb[37] + -z[11] + z[27];
z[32] = z[18] + -z[38];
z[62] = -abb[34] + 2 * z[32];
z[62] = abb[34] * z[62];
z[103] = 3 * abb[33];
z[8] = -abb[34] + z[8] + z[103];
z[106] = -5 * abb[30] + 2 * z[8];
z[106] = abb[30] * z[106];
z[107] = 2 * abb[36];
z[108] = 2 * abb[38];
z[106] = 6 * abb[58] + z[27] + -z[44] + -z[62] + -2 * z[68] + z[106] + z[107] + -z[108];
z[106] = abb[5] * z[106];
z[109] = 2 * abb[30];
z[110] = z[5] + z[109];
z[103] = abb[34] + z[6] + z[103] + -z[110];
z[103] = abb[30] * z[103];
z[64] = abb[34] * z[64];
z[64] = z[33] + z[64];
z[111] = 3 * abb[58];
z[57] = z[50] + z[57] + -z[64] + -z[103] + z[108] + -z[111];
z[57] = abb[16] * z[57];
z[103] = abb[57] + -abb[58];
z[112] = 2 * abb[35];
z[113] = -abb[30] + 2 * z[86];
z[113] = abb[30] * z[113];
z[84] = -z[84] + -2 * z[103] + -z[107] + -z[108] + z[112] + z[113];
z[84] = abb[6] * z[84];
z[113] = -abb[35] + abb[38];
z[114] = -abb[36] + -z[52] + z[113];
z[115] = -abb[34] + z[32];
z[116] = abb[30] + 2 * z[115];
z[116] = abb[30] * z[116];
z[62] = -z[62] + -2 * z[114] + z[116];
z[62] = abb[7] * z[62];
z[116] = abb[34] * z[54];
z[116] = abb[57] + z[116];
z[117] = abb[29] + z[7];
z[117] = abb[29] * z[117];
z[27] = (T(-1) / T(3)) * z[13] + -z[27] + -6 * z[116] + z[117];
z[27] = abb[2] * z[27];
z[107] = z[107] + z[113];
z[116] = abb[29] + z[6];
z[117] = 5 * abb[34] + -2 * z[116];
z[117] = abb[34] * z[117];
z[118] = 2 * abb[34] + -z[116];
z[119] = -abb[30] + -2 * z[118];
z[119] = abb[30] * z[119];
z[117] = 2 * z[107] + z[117] + z[119];
z[117] = abb[4] * z[117];
z[119] = 3 * abb[29];
z[7] = z[7] + -z[119];
z[7] = abb[29] * z[7];
z[69] = abb[33] + -2 * z[69];
z[69] = abb[33] * z[69];
z[7] = z[7] + z[69] + -z[112];
z[7] = abb[9] * z[7];
z[69] = abb[5] * (T(1) / T(3));
z[120] = -abb[7] + z[69];
z[121] = z[13] * z[120];
z[94] = z[13] + -6 * z[94];
z[94] = abb[3] * z[94];
z[7] = z[7] + z[27] + 2 * z[57] + z[62] + 3 * z[84] + z[94] + z[106] + z[117] + z[121];
z[7] = abb[52] * z[7];
z[27] = z[32] + -z[65];
z[27] = abb[34] * z[27];
z[8] = abb[30] * (T(-5) / T(2)) + z[8];
z[8] = abb[30] * z[8];
z[32] = (T(1) / T(2)) * z[44];
z[8] = abb[36] + -abb[38] + z[8] + -z[27] + -z[32] + z[51] + -z[68] + z[111];
z[8] = abb[5] * z[8];
z[44] = z[87] + z[115];
z[44] = abb[30] * z[44];
z[27] = -z[27] + z[44] + -z[114];
z[27] = abb[7] * z[27];
z[44] = z[26] + -z[116];
z[44] = abb[34] * z[44];
z[51] = -z[87] + -z[118];
z[51] = abb[30] * z[51];
z[44] = z[44] + z[51] + z[107];
z[44] = abb[4] * z[44];
z[51] = z[95] * z[120];
z[8] = z[8] + z[27] + z[44] + z[51] + z[57] + -z[58] + -3 * z[91] + -z[96] + -z[99];
z[8] = abb[47] * z[8];
z[27] = z[2] + -z[72];
z[44] = abb[33] * z[27];
z[51] = -z[38] + z[65];
z[51] = abb[34] * z[51];
z[57] = -abb[31] * abb[32];
z[32] = abb[36] + -abb[37] + abb[59] + z[32] + z[44] + z[51] + z[57] + -z[88];
z[32] = abb[5] * z[32];
z[57] = -abb[32] + z[67];
z[62] = abb[30] * (T(1) / T(4));
z[57] = abb[34] + (T(1) / T(2)) * z[57] + -z[62];
z[57] = abb[30] * z[57];
z[68] = abb[34] * (T(3) / T(2));
z[84] = -abb[32] + z[68];
z[84] = z[65] * z[84];
z[57] = abb[57] * (T(-1) / T(2)) + z[57] + -z[84];
z[72] = -z[2] + -z[72];
z[72] = abb[33] * z[72];
z[84] = abb[29] + -z[9];
z[84] = abb[29] * z[84];
z[31] = -z[31] + z[72] + z[84];
z[72] = abb[58] * (T(1) / T(2));
z[31] = (T(1) / T(2)) * z[31] + -z[35] + -z[50] + z[57] + z[72];
z[31] = abb[4] * z[31];
z[84] = abb[33] * (T(5) / T(2)) + z[9] + z[68] + -z[110];
z[84] = abb[30] * z[84];
z[88] = 3 * abb[35] + 5 * abb[58] + z[33];
z[94] = abb[36] * (T(3) / T(2));
z[84] = abb[57] * (T(-5) / T(2)) + z[66] + z[84] + (T(1) / T(2)) * z[88] + -z[94] + -z[108];
z[84] = abb[16] * z[84];
z[51] = -z[30] + z[49] + z[51];
z[54] = z[54] + z[87];
z[54] = abb[30] * z[54];
z[88] = -abb[31] + z[2];
z[88] = abb[33] * z[88];
z[54] = abb[38] + abb[59] + z[50] + z[51] + z[54] + -z[88];
z[106] = abb[7] * (T(1) / T(2));
z[54] = z[54] * z[106];
z[80] = abb[13] * z[80];
z[107] = -abb[17] + abb[20];
z[114] = -abb[18] + -abb[19] + z[107];
z[115] = abb[60] * (T(1) / T(4));
z[117] = -z[114] * z[115];
z[118] = abb[4] * (T(1) / T(4));
z[105] = abb[13] + z[105] + z[118];
z[105] = z[13] * z[105];
z[31] = z[31] + (T(1) / T(2)) * z[32] + z[54] + z[80] + z[84] + z[99] + z[105] + z[117];
z[31] = 3 * z[31] + z[75] + -z[78] + -z[85] + (T(15) / T(2)) * z[91] + z[97];
z[31] = abb[49] * z[31];
z[26] = -z[9] + z[26] + z[36] + -z[110];
z[26] = abb[30] * z[26];
z[32] = 5 * abb[35] + -z[33] + z[111];
z[26] = z[26] + -z[28] + (T(1) / T(2)) * z[32] + -z[48] + -z[66] + -z[108];
z[26] = abb[16] * z[26];
z[28] = (T(3) / T(2)) * z[91];
z[26] = z[26] + z[28] + z[58] + (T(-3) / T(2)) * z[96];
z[32] = -z[34] + z[61];
z[33] = abb[31] + z[18];
z[33] = -z[22] + -z[32] + 2 * z[33] + -z[36];
z[33] = abb[30] * z[33];
z[34] = z[15] + z[18];
z[34] = abb[32] * z[34];
z[1] = z[1] + -z[23];
z[36] = abb[34] * (T(7) / T(4)) + -z[1];
z[36] = abb[34] * z[36];
z[48] = abb[37] * (T(9) / T(2)) + z[36] + z[70];
z[54] = z[34] + z[48];
z[33] = z[33] + z[46] + z[54];
z[33] = abb[5] * z[33];
z[44] = -abb[37] + z[29] + -z[44] + z[49];
z[40] = -abb[36] + z[40];
z[25] = abb[35] + z[25] + -z[40] + -z[42] + (T(-3) / T(2)) * z[44];
z[25] = abb[4] * z[25];
z[1] = -z[1] + z[32];
z[1] = abb[30] * z[1];
z[32] = -z[49] + z[88];
z[42] = -z[29] + -z[32] + z[52];
z[42] = 7 * abb[36] + 3 * z[42] + -z[113];
z[36] = -z[1] + z[36] + (T(1) / T(2)) * z[42];
z[36] = abb[7] * z[36];
z[42] = -abb[18] + z[100];
z[46] = abb[20] + z[42];
z[52] = -z[46] * z[102];
z[58] = abb[5] * (T(-7) / T(3)) + abb[4] * (T(1) / T(2)) + -z[106];
z[58] = z[58] * z[95];
z[25] = z[25] + z[26] + z[33] + z[36] + z[52] + z[58] + z[79] + z[99];
z[25] = abb[50] * z[25];
z[33] = abb[31] * (T(5) / T(4)) + z[18];
z[33] = abb[32] * z[33];
z[36] = -abb[29] + (T(3) / T(4)) * z[6];
z[36] = abb[29] * z[36];
z[1] = abb[35] * (T(-3) / T(2)) + -z[1] + -z[10] + -z[21] + z[33] + -z[35] + z[36] + z[48];
z[1] = abb[7] * z[1];
z[4] = z[4] + -z[39];
z[10] = -z[4] + z[24] + -z[37];
z[10] = abb[30] * z[10];
z[21] = -abb[29] + (T(-3) / T(2)) * z[6];
z[21] = z[21] * z[22];
z[24] = abb[32] + abb[31] * (T(1) / T(4));
z[24] = abb[32] * z[24];
z[10] = z[10] + z[21] + z[24] + -z[40] + -z[43] + z[47];
z[10] = abb[4] * z[10];
z[21] = abb[30] * (T(-17) / T(4)) + 4 * z[6] + -z[22] + z[59] + -z[61];
z[21] = abb[30] * z[21];
z[20] = z[4] + -z[20] + z[22];
z[20] = abb[33] * z[20];
z[11] = abb[59] * (T(3) / T(2)) + abb[58] * (T(9) / T(2)) + -z[11] + z[20] + z[21] + -z[45] + z[54];
z[11] = abb[5] * z[11];
z[20] = -abb[20] + z[42];
z[21] = -z[20] * z[102];
z[24] = abb[7] * (T(-5) / T(12)) + abb[4] * (T(11) / T(12)) + z[69] + z[82];
z[24] = z[13] * z[24];
z[1] = z[1] + z[3] + z[10] + z[11] + z[21] + z[24] + z[26] + z[83];
z[1] = abb[48] * z[1];
z[3] = abb[4] * z[53];
z[10] = 3 * z[53] + -z[95];
z[10] = abb[2] * z[10];
z[11] = abb[29] + z[9];
z[11] = abb[29] * z[11];
z[11] = z[11] + -z[12] + -z[34];
z[11] = abb[7] * z[11];
z[12] = abb[60] * (T(3) / T(2));
z[21] = z[12] * z[100];
z[24] = 13 * abb[0] + -abb[4] + -5 * abb[7];
z[24] = z[24] * z[56];
z[3] = z[3] + z[10] + z[11] + -z[14] + z[21] + z[24] + -z[85];
z[3] = abb[45] * z[3];
z[10] = -abb[31] + z[19];
z[11] = -z[2] + z[10];
z[11] = abb[33] * z[11];
z[11] = z[11] + 3 * z[29] + z[49] + -z[112];
z[11] = abb[7] * z[11];
z[14] = -abb[15] * z[71];
z[19] = z[12] * z[107];
z[21] = -abb[4] * z[98];
z[24] = -abb[7] + z[104];
z[24] = z[13] * z[24];
z[11] = z[11] + z[14] + z[17] + z[19] + z[21] + z[24] + -z[99];
z[11] = abb[51] * z[11];
z[14] = -z[2] + -z[18] + z[22] + z[62] + z[68];
z[14] = abb[30] * z[14];
z[17] = -z[18] + z[23];
z[18] = abb[34] * (T(3) / T(4)) + z[17];
z[18] = abb[34] * z[18];
z[19] = z[2] * z[27];
z[21] = -abb[32] + -z[15];
z[21] = abb[32] * z[21];
z[14] = abb[37] * (T(-5) / T(2)) + abb[59] * (T(1) / T(2)) + z[14] + -z[18] + z[19] + z[21] + z[45] + -z[72] + -z[94];
z[14] = abb[5] * z[14];
z[19] = -3 * abb[36] + -5 * abb[37] + abb[38] + z[32] + z[74];
z[17] = abb[30] * (T(-3) / T(4)) + z[17] + z[68];
z[17] = abb[30] * z[17];
z[17] = z[17] + -z[18] + (T(1) / T(2)) * z[19];
z[17] = abb[7] * z[17];
z[18] = z[44] + -z[89];
z[18] = -abb[36] + (T(1) / T(2)) * z[18] + z[57];
z[18] = abb[4] * z[18];
z[19] = z[46] * z[115];
z[14] = z[14] + z[17] + z[18] + z[19];
z[17] = -abb[33] + abb[34];
z[6] = -z[6] + z[17];
z[18] = abb[30] * z[6];
z[18] = z[18] + -z[50] + -z[64] + z[103];
z[19] = abb[16] * (T(3) / T(2));
z[21] = -z[18] * z[19];
z[23] = abb[5] + abb[7] * (T(3) / T(4)) + -z[118];
z[23] = z[13] * z[23];
z[14] = 3 * z[14] + z[21] + z[23] + z[28] + -z[79] + (T(5) / T(2)) * z[96];
z[14] = abb[46] * z[14];
z[18] = abb[26] * z[18];
z[21] = abb[27] * z[71];
z[23] = z[51] + (T(1) / T(2)) * z[81] + z[90];
z[23] = -z[13] + 3 * z[23];
z[24] = abb[23] + abb[25];
z[23] = z[23] * z[24];
z[22] = z[22] * z[116];
z[24] = abb[34] * z[38];
z[22] = abb[57] + -z[22] + z[24] + z[30];
z[13] = z[13] + 3 * z[22];
z[22] = abb[21] * z[13];
z[24] = abb[22] + abb[24];
z[26] = -z[24] * z[76];
z[12] = abb[28] * z[12];
z[12] = -z[12] + -3 * z[18] + z[21] + -z[22] + z[23] + z[26];
z[18] = abb[54] + abb[55] + abb[56];
z[21] = (T(1) / T(2)) * z[18];
z[12] = z[12] * z[21];
z[21] = abb[46] + -abb[48] + abb[49] + -abb[50] + -abb[53];
z[21] = abb[45] + (T(1) / T(2)) * z[21];
z[13] = abb[14] * z[13] * z[21];
z[1] = abb[61] + abb[62] + z[1] + z[3] + z[7] + z[8] + z[11] + z[12] + z[13] + z[14] + z[16] + z[25] + z[31];
z[2] = -z[2] + z[5] + (T(-3) / T(4)) * z[41] + -z[65] + z[87];
z[2] = abb[7] * z[2];
z[3] = -z[39] + z[65] + z[77] + -z[109];
z[3] = abb[5] * z[3];
z[0] = abb[0] * z[0];
z[7] = (T(3) / T(2)) * z[41];
z[8] = -abb[29] + z[7];
z[8] = -abb[33] + (T(1) / T(2)) * z[8] + z[92];
z[8] = abb[4] * z[8];
z[11] = abb[13] * abb[33];
z[12] = 6 * z[11];
z[2] = -z[0] + z[2] + -z[3] + z[8] + -z[12];
z[2] = m1_set::bc<T>[0] * z[2];
z[8] = 5 * abb[33] + z[10];
z[8] = m1_set::bc<T>[0] * z[8];
z[8] = 3 * abb[41] + z[8];
z[8] = abb[1] * z[8];
z[10] = -abb[33] + z[15] + z[87];
z[10] = m1_set::bc<T>[0] * z[10];
z[13] = abb[40] + abb[41];
z[10] = z[10] + (T(-1) / T(2)) * z[13];
z[14] = abb[8] * z[10];
z[15] = 3 * z[14];
z[16] = (T(1) / T(2)) * z[41];
z[22] = -abb[33] + z[16];
z[23] = m1_set::bc<T>[0] * z[22];
z[23] = -abb[41] + z[23];
z[25] = abb[15] * z[23];
z[26] = -z[15] + (T(3) / T(2)) * z[25];
z[27] = z[8] + z[26];
z[28] = abb[7] * abb[41];
z[29] = abb[4] * abb[39];
z[30] = z[28] + z[29];
z[31] = abb[5] * z[13];
z[31] = -z[30] + z[31];
z[32] = abb[42] * (T(1) / T(2));
z[33] = -z[32] * z[101];
z[13] = abb[4] * z[13];
z[33] = -z[13] + -z[31] + z[33];
z[34] = z[55] + -z[116];
z[35] = m1_set::bc<T>[0] * z[34];
z[35] = 3 * abb[39] + -z[35];
z[35] = abb[2] * z[35];
z[36] = m1_set::bc<T>[0] * z[67];
z[37] = abb[39] + -abb[40];
z[38] = z[36] + -z[37];
z[38] = abb[6] * z[38];
z[39] = -abb[30] + z[86];
z[39] = m1_set::bc<T>[0] * z[39];
z[40] = 9 * z[37] + -z[39];
z[41] = abb[16] * (T(1) / T(2));
z[40] = z[40] * z[41];
z[42] = abb[10] * m1_set::bc<T>[0];
z[43] = abb[29] * z[42];
z[43] = 6 * z[43];
z[36] = abb[9] * z[36];
z[44] = 2 * z[36];
z[2] = z[2] + z[27] + (T(3) / T(2)) * z[33] + z[35] + (T(9) / T(2)) * z[38] + z[40] + z[43] + -z[44];
z[2] = abb[53] * z[2];
z[33] = -abb[30] + z[17];
z[40] = z[16] + z[33];
z[40] = abb[7] * z[40];
z[45] = abb[5] * z[93];
z[47] = -abb[29] + z[16];
z[48] = abb[4] * z[47];
z[40] = z[40] + z[45] + z[48];
z[40] = m1_set::bc<T>[0] * z[40];
z[46] = z[32] * z[46];
z[48] = abb[4] + -abb[5];
z[49] = abb[40] + -abb[41];
z[48] = z[48] * z[49];
z[30] = -z[25] + -z[30] + -z[38] + z[40] + z[46] + z[48];
z[40] = -z[37] + z[39];
z[48] = z[40] * z[41];
z[14] = z[14] + (T(1) / T(2)) * z[30] + z[48];
z[14] = abb[46] * z[14];
z[30] = z[9] + -z[63];
z[30] = m1_set::bc<T>[0] * z[30];
z[30] = abb[39] + z[30];
z[21] = abb[14] * z[21] * z[30];
z[14] = z[14] + z[21];
z[21] = abb[21] * z[30];
z[30] = -z[9] + z[17];
z[30] = m1_set::bc<T>[0] * z[30];
z[30] = -abb[40] + z[30];
z[48] = abb[23] * z[30];
z[30] = abb[25] * z[30];
z[40] = abb[26] * z[40];
z[50] = -abb[27] * z[23];
z[51] = abb[28] * z[32];
z[21] = z[21] + -z[30] + -z[40] + -z[48] + z[50] + z[51];
z[10] = -z[10] * z[24];
z[10] = z[10] + (T(1) / T(2)) * z[21];
z[18] = -3 * z[18];
z[10] = z[10] * z[18];
z[18] = z[28] + -z[29];
z[21] = -z[32] * z[114];
z[24] = -abb[5] * z[49];
z[21] = z[13] + z[18] + z[21] + z[24];
z[24] = -z[22] + -z[92];
z[24] = z[24] * z[106];
z[16] = -abb[29] + -z[16];
z[16] = abb[33] + (T(1) / T(2)) * z[16];
z[16] = abb[4] * z[16];
z[11] = 2 * z[11] + z[16] + z[24] + (T(1) / T(2)) * z[45];
z[11] = m1_set::bc<T>[0] * z[11];
z[16] = z[23] * z[73];
z[23] = -z[5] * z[42];
z[11] = z[11] + z[16] + (T(1) / T(2)) * z[21] + z[23] + (T(-5) / T(2)) * z[38];
z[16] = -5 * z[37] + z[39];
z[16] = z[16] * z[19];
z[11] = 3 * z[11] + z[15] + z[16] + 6 * z[36];
z[11] = abb[49] * z[11];
z[15] = -3 * z[22] + z[92];
z[15] = z[15] * z[106];
z[16] = (T(-3) / T(2)) * z[47] + z[92];
z[16] = abb[4] * z[16];
z[3] = -z[3] + z[15] + z[16];
z[3] = m1_set::bc<T>[0] * z[3];
z[15] = 3 * z[37];
z[16] = z[15] + z[39];
z[16] = z[16] * z[41];
z[16] = z[16] + (T(3) / T(2)) * z[38];
z[19] = -abb[4] * z[49];
z[19] = z[19] + -z[31] + -z[46];
z[21] = z[35] + z[44];
z[3] = z[3] + -z[16] + (T(3) / T(2)) * z[19] + z[21] + z[26];
z[3] = abb[50] * z[3];
z[19] = -z[20] * z[32];
z[20] = 3 * abb[40];
z[22] = abb[41] + z[20];
z[22] = abb[5] * z[22];
z[13] = z[13] + -z[18] + z[19] + z[22];
z[4] = 2 * abb[33] + -z[4] + -z[60] + -z[65];
z[4] = abb[5] * z[4];
z[18] = -7 * abb[31] + abb[32];
z[18] = (T(1) / T(2)) * z[18] + z[119];
z[17] = -z[17] + (T(1) / T(2)) * z[18] + -z[109];
z[17] = abb[4] * z[17];
z[18] = abb[31] + 5 * abb[32];
z[18] = (T(1) / T(2)) * z[18] + -z[33];
z[18] = z[18] * z[106];
z[4] = z[4] + z[12] + z[17] + z[18];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = z[4] + (T(3) / T(2)) * z[13] + -z[16] + -z[27] + z[35];
z[4] = abb[48] * z[4];
z[6] = z[6] + z[109];
z[6] = abb[5] * z[6];
z[12] = abb[4] + -abb[7];
z[12] = z[12] * z[92];
z[6] = z[6] + z[12];
z[6] = m1_set::bc<T>[0] * z[6];
z[12] = z[15] + -z[39];
z[12] = abb[16] * z[12];
z[13] = abb[5] * z[20];
z[6] = z[6] + -z[12] + -z[13] + z[21] + -3 * z[38];
z[12] = -abb[47] + -2 * abb[52];
z[6] = z[6] * z[12];
z[9] = -z[5] + -z[9];
z[9] = abb[7] * z[9];
z[12] = abb[4] * z[34];
z[9] = z[0] + z[9] + z[12];
z[9] = m1_set::bc<T>[0] * z[9];
z[12] = z[32] * z[100];
z[12] = -z[12] + z[29] + z[35];
z[9] = z[9] + -3 * z[12] + -z[43];
z[9] = abb[45] * z[9];
z[12] = z[32] * z[107];
z[12] = -z[12] + z[25] + z[28];
z[5] = -abb[33] + -z[5] + z[7];
z[5] = abb[7] * z[5];
z[7] = abb[4] * z[67];
z[0] = z[0] + z[5] + -2 * z[7];
z[0] = m1_set::bc<T>[0] * z[0];
z[0] = z[0] + -z[8] + -3 * z[12] + -z[44];
z[0] = abb[51] * z[0];
z[0] = abb[43] + abb[44] + z[0] + z[2] + z[3] + z[4] + z[6] + z[9] + z[10] + z[11] + 3 * z[14];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_627_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.842303589037347753716178442501725703371338453839769594880634106"),stof<T>("-15.122073742449383876612057114347449851412820922669855048999498447")}, std::complex<T>{stof<T>("-5.8353936399983307679581584692371563069721225375490980027885774203"),stof<T>("-0.2944280064184412209788379563404994829815088917188172714968998916")}, std::complex<T>{stof<T>("-7.627599979258414394697256932890284247236991584379268680010266196"),stof<T>("14.761637334374721982133367462817102553647503900240402103445876479")}, std::complex<T>{stof<T>("9.6910558069224228983851695190974950420840410831893798120008510992"),stof<T>("5.5052373750092180789417719145809011286940907604500039130053537553")}, std::complex<T>{stof<T>("5.8353936399983307679581584692371563069721225375490980027885774203"),stof<T>("0.2944280064184412209788379563404994829815088917188172714968998916")}, std::complex<T>{stof<T>("-21.160959375218185046798604894489504992692371121408418086891751401"),stof<T>("-5.865673783083879973420461566111248426459407782879456858558975723")}, std::complex<T>{stof<T>("11.4699035682957621484134353753920099506083300382190382748909003019"),stof<T>("0.3604364080746618944786896515303472977653170224294529455536219678")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_627_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_627_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[45] + abb[46] + -abb[48] + -abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[34], 2);
z[1] = -prod_pow(abb[32], 2);
z[0] = z[0] + z[1];
z[1] = -abb[45] + -abb[46] + abb[48] + abb[50];
return 3 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_627_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_627_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (-4 * v[0] + 11 * v[1] + 7 * v[2] + -3 * v[3] + -7 * v[4] + 6 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) + -m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]))) / prod_pow(tend, 2);
c[1] = (-(4 + 6 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (v[1] + v[2] + -1 * v[3] + -1 * v[4])) / tend;


		return (3 * abb[46] + -2 * abb[47] + -abb[48] + 3 * abb[49] + -abb[50] + -4 * abb[52] + -abb[53]) * (4 * t * c[0] + -3 * c[1]) * (T(1) / T(12));
	}
	{
T z[6];
z[0] = -abb[35] + 2 * abb[36];
z[1] = abb[38] + z[0];
z[2] = abb[29] + -abb[31] + abb[32];
z[3] = 2 * abb[30] + z[2];
z[4] = -5 * abb[34] + 2 * z[3];
z[4] = abb[34] * z[4];
z[5] = -abb[30] + 2 * z[2];
z[5] = abb[30] * z[5];
z[4] = 2 * z[1] + -z[4] + z[5];
z[5] = abb[47] + 2 * abb[52];
z[4] = z[4] * z[5];
z[3] = abb[34] * (T(5) / T(2)) + -z[3];
z[3] = abb[34] * z[3];
z[2] = abb[30] * (T(-1) / T(2)) + z[2];
z[2] = abb[30] * z[2];
z[2] = z[2] + z[3];
z[1] = -z[1] + -z[2];
z[1] = abb[49] * z[1];
z[0] = abb[38] + z[0] + z[2];
z[2] = -abb[46] * z[0];
z[1] = z[1] + z[2];
z[2] = abb[48] + abb[50] + abb[53];
z[0] = z[0] * z[2];
z[0] = z[0] + 3 * z[1] + z[4];
return abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_627_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (3 * abb[46] + -2 * abb[47] + -abb[48] + 3 * abb[49] + -abb[50] + -4 * abb[52] + -abb[53]) * (2 * t * c[0] + c[1]) * (T(-1) / T(2));
	}
	{
T z[2];
z[0] = abb[46] + abb[49];
z[0] = -2 * abb[47] + -abb[48] + -abb[50] + -4 * abb[52] + -abb[53] + 3 * z[0];
z[1] = abb[30] + -abb[34];
return abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_627_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}, T{0}};
abb[43] = SpDLog_f_4_627_W_17_Im(t, path, abb);
abb[44] = SpDLog_f_4_627_W_20_Im(t, path, abb);
abb[61] = SpDLog_f_4_627_W_17_Re(t, path, abb);
abb[62] = SpDLog_f_4_627_W_20_Re(t, path, abb);

                    
            return f_4_627_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_627_DLogXconstant_part(base_point<T>, kend);
	value += f_4_627_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_627_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_627_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_627_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_627_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_627_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_627_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
