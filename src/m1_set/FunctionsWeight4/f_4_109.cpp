/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_109.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_109_abbreviated (const std::array<T,29>& abb) {
T z[41];
z[0] = abb[22] * (T(5) / T(4));
z[1] = abb[18] + -abb[20];
z[2] = (T(1) / T(4)) * z[1];
z[3] = -abb[23] + z[2];
z[4] = abb[19] * (T(3) / T(2));
z[5] = z[0] + z[3] + -z[4];
z[5] = abb[0] * z[5];
z[6] = abb[22] * (T(1) / T(2));
z[7] = z[4] + -z[6];
z[8] = -abb[23] + z[1];
z[9] = abb[21] * (T(3) / T(2));
z[10] = z[7] + -z[8] + -z[9];
z[11] = abb[6] * z[10];
z[12] = 2 * z[8];
z[13] = -5 * abb[19] + abb[21] + 3 * abb[22] + z[12];
z[14] = abb[1] * z[13];
z[5] = z[5] + -z[11] + -z[14];
z[15] = abb[21] * (T(1) / T(2));
z[16] = 2 * abb[19];
z[17] = abb[22] * (T(7) / T(4)) + z[3] + -z[15] + -z[16];
z[17] = abb[3] * z[17];
z[7] = z[1] + -z[7];
z[7] = abb[2] * z[7];
z[18] = abb[24] + abb[26];
z[19] = abb[25] + (T(1) / T(2)) * z[18];
z[20] = abb[8] * z[19];
z[21] = -abb[19] + abb[22];
z[22] = -abb[21] + z[21];
z[23] = 2 * z[22];
z[24] = abb[5] * z[23];
z[25] = abb[2] * abb[21];
z[7] = -z[5] + z[7] + z[17] + -z[20] + -z[24] + (T(1) / T(2)) * z[25];
z[7] = abb[11] * z[7];
z[17] = abb[22] * (T(1) / T(4));
z[26] = abb[19] * (T(1) / T(2));
z[27] = 2 * abb[21];
z[28] = -abb[23] + (T(3) / T(4)) * z[1] + -z[17] + -z[26] + z[27];
z[28] = abb[0] * z[28];
z[29] = 3 * abb[19];
z[30] = 3 * abb[21];
z[31] = -abb[22] + -z[12] + z[29] + -z[30];
z[32] = abb[5] * z[31];
z[33] = -abb[23] + z[9];
z[2] = -z[2] + z[17] + -z[33];
z[17] = abb[3] * z[2];
z[34] = -abb[19] + z[1];
z[34] = abb[2] * z[34];
z[34] = z[25] + z[34];
z[22] = abb[1] * z[22];
z[17] = z[11] + z[17] + z[22] + z[28] + -z[32] + (T(-3) / T(2)) * z[34];
z[17] = abb[13] * z[17];
z[7] = z[7] + z[17];
z[7] = abb[13] * z[7];
z[17] = abb[22] * (T(3) / T(4));
z[3] = -z[3] + z[17] + -z[26] + -z[27];
z[3] = abb[0] * z[3];
z[28] = abb[2] * z[21];
z[28] = -z[25] + z[28];
z[28] = -z[20] + (T(1) / T(2)) * z[28];
z[35] = (T(5) / T(4)) * z[1];
z[0] = -abb[23] + abb[21] * (T(7) / T(2)) + -z[0] + z[35];
z[0] = abb[3] * z[0];
z[36] = -abb[19] + 5 * abb[21] + -abb[22] + z[12];
z[36] = abb[5] * z[36];
z[37] = -abb[1] * z[23];
z[0] = z[0] + z[3] + -z[11] + -z[28] + -z[36] + z[37];
z[0] = abb[13] * z[0];
z[3] = z[16] + -z[17] + -z[33] + -z[35];
z[3] = abb[3] * z[3];
z[3] = z[3] + z[5] + z[28];
z[3] = abb[11] * z[3];
z[5] = (T(1) / T(2)) * z[1];
z[17] = -abb[19] + z[5] + z[6];
z[28] = -abb[0] * z[17];
z[33] = -abb[22] + z[1];
z[35] = abb[2] * z[33];
z[20] = z[20] + z[22] + z[25] + z[28] + (T(1) / T(2)) * z[35];
z[20] = abb[14] * z[20];
z[0] = z[0] + z[3] + z[20];
z[0] = abb[14] * z[0];
z[3] = 2 * abb[23];
z[20] = z[3] + -z[9];
z[4] = -abb[22] + z[4] + -z[5] + z[20];
z[4] = abb[8] * z[4];
z[5] = abb[9] * z[10];
z[2] = abb[7] * z[2];
z[22] = abb[2] + -abb[10];
z[18] = 2 * abb[25] + z[18];
z[22] = z[18] * z[22];
z[28] = abb[0] * (T(1) / T(2)) + abb[3] * (T(3) / T(2));
z[28] = z[19] * z[28];
z[2] = z[2] + z[4] + z[5] + z[22] + z[28];
z[2] = abb[15] * z[2];
z[4] = 2 * abb[22] + -z[3];
z[5] = abb[19] * (T(7) / T(2));
z[9] = (T(3) / T(2)) * z[1] + z[4] + -z[5] + z[9];
z[9] = abb[1] * z[9];
z[22] = -abb[23] + z[21];
z[28] = abb[0] * z[22];
z[11] = -z[11] + z[28];
z[10] = -abb[3] * z[10];
z[17] = abb[4] * z[17];
z[9] = z[9] + z[10] + -z[11] + z[17];
z[9] = prod_pow(abb[11], 2) * z[9];
z[10] = 4 * abb[22];
z[28] = 4 * abb[23] + -z[30];
z[30] = 7 * abb[19];
z[37] = 3 * z[1] + z[10] + -z[28] + -z[30];
z[37] = abb[1] * z[37];
z[16] = -abb[22] + -z[1] + z[16];
z[38] = abb[4] * z[16];
z[37] = z[37] + -z[38];
z[39] = abb[3] * z[31];
z[31] = abb[6] * z[31];
z[40] = 2 * abb[0];
z[22] = z[22] * z[40];
z[22] = z[22] + -z[31];
z[39] = -z[22] + z[37] + -z[39];
z[40] = -abb[27] * z[39];
z[5] = abb[22] * (T(5) / T(2)) + -z[5] + z[8] + -z[15];
z[5] = abb[3] * z[5];
z[10] = abb[19] * (T(-15) / T(2)) + (T(7) / T(2)) * z[1] + z[10] + -z[20];
z[10] = abb[1] * z[10];
z[5] = z[5] + z[10] + -z[11] + -3 * z[17] + -z[24];
z[5] = abb[12] * z[5];
z[10] = z[22] + z[24];
z[11] = -11 * abb[19] + 6 * abb[22] + 5 * z[1] + -z[28];
z[11] = abb[1] * z[11];
z[13] = abb[3] * z[13];
z[11] = -z[10] + z[11] + z[13] + z[38];
z[13] = -abb[11] * z[11];
z[17] = abb[3] * z[23];
z[17] = z[17] + z[31] + z[36];
z[12] = abb[21] + -5 * abb[22] + -z[12] + z[30];
z[12] = abb[1] * z[12];
z[20] = abb[0] * z[23];
z[12] = z[12] + -z[17] + z[20];
z[20] = abb[13] + -abb[14];
z[20] = z[12] * z[20];
z[5] = z[5] + z[13] + z[20];
z[5] = abb[12] * z[5];
z[13] = abb[21] * (T(5) / T(2)) + -z[6] + z[8] + -z[26];
z[13] = abb[5] * z[13];
z[3] = abb[21] + -z[3] + z[21];
z[3] = abb[0] * z[3];
z[3] = z[3] + z[13] + z[38];
z[13] = abb[22] * (T(1) / T(6)) + (T(1) / T(3)) * z[8] + z[15] + -z[26];
z[13] = abb[6] * z[13];
z[15] = -abb[19] + abb[21];
z[6] = abb[23] + (T(-2) / T(3)) * z[1] + -z[6] + (T(-7) / T(6)) * z[15];
z[6] = abb[1] * z[6];
z[8] = abb[19] + -z[8] + -z[27];
z[8] = abb[3] * z[8];
z[3] = (T(1) / T(3)) * z[3] + z[6] + (T(2) / T(3)) * z[8] + z[13];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[6] = -abb[11] + abb[13];
z[6] = abb[7] * z[6];
z[8] = 3 * abb[13] + abb[14];
z[8] = z[6] * z[8] * z[19];
z[13] = abb[28] * z[12];
z[0] = z[0] + z[2] + z[3] + z[5] + z[7] + (T(1) / T(2)) * z[8] + z[9] + z[13] + z[40];
z[2] = z[27] + z[33];
z[2] = abb[0] * z[2];
z[2] = z[2] + z[14];
z[3] = abb[3] * z[16];
z[3] = -z[2] + z[3] + -z[31] + z[32] + 2 * z[34];
z[3] = abb[13] * z[3];
z[1] = -abb[21] + -z[1] + -z[4] + z[29];
z[1] = abb[3] * z[1];
z[4] = abb[2] * z[16];
z[5] = abb[8] * z[18];
z[1] = z[1] + z[4] + z[5] + z[10] + -z[37];
z[1] = abb[11] * z[1];
z[2] = z[2] + -z[5] + z[17] + -2 * z[25] + -z[35];
z[2] = abb[14] * z[2];
z[4] = abb[12] * z[11];
z[5] = -z[6] * z[18];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[39];
z[3] = abb[17] * z[12];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_109_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-4.3080130294798100297032187444824170648041567248677161988047028117"),stof<T>("-6.667834103490437016712743283272233831533282238691846164464730301")}, std::complex<T>{stof<T>("5.1685047525126802049137448023017476974723701710487284699223535932"),stof<T>("8.8697518978216205340721591248574844202357208555319408500955593207")}, std::complex<T>{stof<T>("4.3080130294798100297032187444824170648041567248677161988047028117"),stof<T>("6.667834103490437016712743283272233831533282238691846164464730301")}, std::complex<T>{stof<T>("-19.515523587473482564954655745381223709656874673017669308926928238"),stof<T>("-13.899594857300568574446238187513152637824582497307720674592411335")}, std::complex<T>{stof<T>("-0.8604917230328701752105260578193306326682134461810122711176507815"),stof<T>("-2.2019177943311835173594158415852505887024386168400946856308290197")}, std::complex<T>{stof<T>("16.068002281026542710461963058718137277520931394330965381239876208"),stof<T>("9.433678548141315075092910745826169394993738875455969195758510053")}, std::complex<T>{stof<T>("-0.94645826884586151334932252788511187146196813235744440321885439"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-1.89291653769172302669864505577022374292393626471488880643770878"),stof<T>("15.748931692217380713457583612152853300165062016478596541063558734")}, std::complex<T>{stof<T>("-0.94645826884586151334932252788511187146196813235744440321885439"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[45].real()/kbase.W[45].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_109_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_109_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("17.477038949176712724826996413913032050783717973894922013598557238"),stof<T>("-15.828075831849186499478378227964838234362390539565122199534003401")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[45].real()/k.W[45].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_109_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_109_DLogXconstant_part(base_point<T>, kend);
	value += f_4_109_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_109_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_109_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_109_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_109_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_109_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_109_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
