/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_123.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_123_abbreviated (const std::array<T,56>& abb) {
T z[69];
z[0] = abb[31] * m1_set::bc<T>[0];
z[1] = abb[30] * m1_set::bc<T>[0];
z[2] = -abb[40] + z[1];
z[3] = z[0] + -z[2];
z[3] = abb[1] * z[3];
z[4] = abb[18] + abb[20];
z[5] = abb[42] * (T(1) / T(4));
z[6] = z[4] * z[5];
z[7] = abb[31] + -abb[32];
z[8] = m1_set::bc<T>[0] * z[7];
z[8] = abb[40] + z[8];
z[9] = abb[14] * z[8];
z[3] = z[3] + -z[6] + z[9];
z[6] = abb[33] * (T(1) / T(2));
z[10] = -abb[31] + z[6];
z[10] = m1_set::bc<T>[0] * z[10];
z[11] = abb[30] * (T(1) / T(2));
z[12] = m1_set::bc<T>[0] * z[11];
z[13] = -abb[40] + z[12];
z[10] = z[10] + z[13];
z[14] = abb[15] * z[10];
z[15] = z[6] + z[7];
z[16] = m1_set::bc<T>[0] * z[15];
z[16] = -z[13] + z[16];
z[17] = abb[6] * z[16];
z[17] = -z[14] + z[17];
z[18] = abb[34] * m1_set::bc<T>[0];
z[19] = abb[41] + z[18];
z[12] = z[12] + -z[19];
z[20] = m1_set::bc<T>[0] * (T(1) / T(2));
z[21] = abb[33] + -z[7];
z[20] = z[20] * z[21];
z[20] = abb[40] * (T(-1) / T(2)) + z[12] + z[20];
z[20] = abb[5] * z[20];
z[21] = abb[33] * m1_set::bc<T>[0];
z[22] = -z[1] + z[21];
z[23] = abb[0] * (T(1) / T(4));
z[22] = z[22] * z[23];
z[21] = -z[19] + z[21];
z[24] = abb[13] * z[21];
z[25] = abb[8] * z[16];
z[17] = -z[3] + (T(1) / T(2)) * z[17] + z[20] + z[22] + -z[24] + z[25];
z[17] = abb[48] * z[17];
z[20] = abb[32] * m1_set::bc<T>[0];
z[20] = z[19] + -z[20];
z[20] = abb[4] * z[20];
z[25] = m1_set::bc<T>[0] * z[6];
z[25] = z[12] + z[25];
z[26] = abb[17] * z[25];
z[27] = abb[42] * (T(1) / T(2));
z[28] = abb[21] * z[27];
z[29] = abb[7] * z[8];
z[20] = z[20] + z[26] + -z[28] + -z[29];
z[26] = abb[33] * (T(3) / T(2));
z[28] = z[7] + z[26];
z[29] = m1_set::bc<T>[0] * z[28];
z[13] = -z[13] + -z[19] + z[29];
z[13] = abb[6] * z[13];
z[13] = z[13] + -z[14] + -z[20];
z[14] = abb[2] * z[21];
z[29] = abb[19] * z[5];
z[14] = z[14] + -z[24] + z[29];
z[24] = abb[31] + -abb[33];
z[24] = m1_set::bc<T>[0] * z[24];
z[2] = -z[2] + z[19] + z[24];
z[24] = abb[8] * (T(1) / T(2));
z[29] = z[2] * z[24];
z[26] = -abb[31] + -abb[32] + z[26];
z[26] = m1_set::bc<T>[0] * z[26];
z[1] = -abb[40] + (T(3) / T(2)) * z[1] + -z[19] + z[26];
z[26] = abb[5] * (T(1) / T(2));
z[1] = z[1] * z[26];
z[1] = z[1] + -z[3] + (T(1) / T(2)) * z[13] + z[14] + z[29];
z[1] = abb[49] * z[1];
z[3] = abb[18] * z[16];
z[8] = abb[19] * z[8];
z[3] = z[3] + z[8];
z[8] = abb[6] + abb[15];
z[13] = abb[0] + z[8];
z[5] = z[5] * z[13];
z[13] = abb[20] * (T(1) / T(2));
z[10] = -z[10] * z[13];
z[16] = -abb[28] * abb[42];
z[29] = -abb[8] * z[27];
z[3] = (T(1) / T(2)) * z[3] + z[5] + z[10] + z[16] + z[29];
z[3] = abb[52] * z[3];
z[5] = -z[6] + z[7];
z[10] = m1_set::bc<T>[0] * z[5];
z[10] = abb[40] + z[10] + -z[12];
z[12] = abb[23] + abb[25];
z[10] = z[10] * z[12];
z[2] = abb[24] * z[2];
z[12] = -abb[22] + abb[26];
z[12] = z[12] * z[25];
z[16] = abb[27] * z[27];
z[2] = z[2] + z[10] + z[12] + -z[16];
z[10] = abb[43] + -abb[44] + abb[45];
z[12] = abb[46] * (T(1) / T(2));
z[16] = (T(-1) / T(2)) * z[10] + z[12];
z[2] = z[2] * z[16];
z[16] = abb[6] * z[21];
z[16] = z[16] + -z[20];
z[14] = -z[14] + (T(-1) / T(2)) * z[16] + z[22];
z[0] = abb[40] + z[0] + -z[19];
z[0] = z[0] * z[24];
z[16] = z[0] + z[14];
z[16] = abb[51] * z[16];
z[19] = z[25] * z[26];
z[14] = -z[14] + z[19];
z[0] = -z[0] + z[14];
z[0] = abb[47] * z[0];
z[18] = abb[40] + abb[41] + z[18];
z[19] = abb[31] * (T(1) / T(2));
z[20] = -abb[32] + z[19];
z[21] = m1_set::bc<T>[0] * z[20];
z[18] = (T(1) / T(2)) * z[18] + z[21];
z[18] = abb[8] * z[18];
z[9] = -z[9] + z[14] + z[18];
z[9] = abb[50] * z[9];
z[14] = -abb[51] * z[25];
z[18] = abb[42] * abb[52];
z[14] = z[14] + z[18];
z[14] = z[14] * z[26];
z[0] = z[0] + z[1] + z[2] + z[3] + z[9] + z[14] + z[16] + z[17];
z[0] = (T(1) / T(16)) * z[0];
z[1] = abb[31] * (T(3) / T(4));
z[2] = -abb[32] + z[6];
z[3] = abb[29] * (T(1) / T(2));
z[9] = z[1] + z[2] + -z[3];
z[9] = abb[31] * z[9];
z[14] = -abb[29] + -abb[32];
z[14] = z[6] * z[14];
z[16] = abb[34] * (T(1) / T(2));
z[17] = -abb[33] + z[16];
z[18] = -abb[29] + abb[30];
z[21] = z[17] + z[18];
z[22] = z[16] * z[21];
z[25] = -abb[29] + z[7];
z[27] = z[11] * z[25];
z[29] = abb[37] + abb[38];
z[30] = abb[36] * (T(1) / T(2));
z[31] = abb[35] * (T(1) / T(2));
z[32] = prod_pow(abb[32], 2);
z[33] = (T(1) / T(2)) * z[32];
z[9] = -abb[39] + z[9] + z[14] + -z[22] + -z[27] + (T(1) / T(2)) * z[29] + z[30] + z[31] + z[33];
z[14] = -abb[16] * z[9];
z[22] = abb[29] + abb[33];
z[27] = z[3] * z[22];
z[22] = z[11] * z[22];
z[29] = -abb[38] + abb[39];
z[34] = abb[54] + z[29];
z[35] = abb[34] * z[18];
z[36] = prod_pow(m1_set::bc<T>[0], 2);
z[37] = -z[22] + z[27] + z[34] + z[35] + (T(1) / T(2)) * z[36];
z[37] = z[26] * z[37];
z[15] = -z[3] + z[15];
z[38] = abb[30] * z[15];
z[39] = abb[29] + -abb[33];
z[19] = z[19] + -z[39];
z[40] = abb[31] * z[19];
z[41] = z[3] * z[39];
z[41] = abb[36] + z[41];
z[42] = abb[33] * z[2];
z[43] = (T(1) / T(6)) * z[36];
z[38] = z[38] + -z[40] + -z[41] + -z[42] + -z[43];
z[44] = abb[0] * (T(1) / T(2));
z[38] = z[38] * z[44];
z[44] = prod_pow(abb[33], 2);
z[44] = (T(1) / T(2)) * z[44];
z[45] = -abb[54] + z[44];
z[46] = abb[34] * z[17];
z[47] = -abb[35] + z[29] + -z[43] + z[45] + z[46];
z[47] = abb[2] * z[47];
z[48] = (T(1) / T(3)) * z[36];
z[49] = abb[54] + z[48];
z[50] = z[46] + -z[49];
z[44] = z[44] + z[50];
z[51] = abb[13] * z[44];
z[47] = z[47] + -z[51];
z[37] = -z[37] + z[38] + z[47];
z[49] = z[35] + z[49];
z[22] = z[22] + -z[49];
z[27] = abb[35] + -z[22] + z[27];
z[52] = abb[17] * z[27];
z[53] = prod_pow(abb[31], 2);
z[53] = (T(1) / T(2)) * z[53];
z[54] = -abb[37] + -z[33] + z[53];
z[54] = abb[7] * z[54];
z[55] = abb[37] + -abb[39];
z[56] = abb[38] + z[33] + z[55];
z[57] = abb[36] + z[56];
z[58] = -abb[32] + z[3];
z[59] = abb[29] * z[58];
z[60] = z[57] + z[59];
z[60] = abb[3] * z[60];
z[61] = abb[32] * abb[33];
z[62] = -abb[54] + -z[33] + z[61];
z[63] = -abb[38] + z[62];
z[63] = abb[4] * z[63];
z[52] = -z[52] + z[54] + z[60] + -z[63];
z[54] = abb[7] * z[7] * z[11];
z[60] = -abb[4] * z[17];
z[63] = z[16] * z[60];
z[64] = abb[11] * abb[38];
z[65] = abb[3] * z[31];
z[54] = (T(-1) / T(2)) * z[52] + z[54] + -z[63] + z[64] + -z[65];
z[20] = abb[31] * z[20];
z[63] = -abb[36] + z[20] + -z[59];
z[63] = abb[9] * z[63];
z[64] = abb[53] * (T(1) / T(2));
z[65] = abb[7] * z[64];
z[63] = -z[37] + -z[54] + z[63] + -z[65];
z[32] = z[32] + -z[45] + z[55];
z[17] = -z[16] * z[17];
z[17] = z[17] + z[20] + z[31] + (T(1) / T(2)) * z[32] + z[43];
z[17] = abb[6] * z[17];
z[31] = -abb[33] + z[58];
z[31] = z[3] * z[31];
z[32] = -abb[54] + z[56];
z[31] = abb[36] + z[31] + (T(1) / T(2)) * z[32];
z[16] = z[16] * z[18];
z[18] = z[16] + z[64];
z[15] = abb[31] * z[15];
z[45] = -z[3] + z[7];
z[45] = -abb[30] * z[45];
z[15] = z[15] + -z[18] + z[31] + z[43] + z[45];
z[15] = abb[8] * z[15];
z[45] = abb[30] * z[7];
z[43] = -z[43] + z[45];
z[65] = abb[31] * z[7];
z[65] = -abb[53] + -z[43] + z[65];
z[65] = abb[14] * z[65];
z[66] = abb[19] + abb[21];
z[67] = (T(1) / T(4)) * z[66];
z[68] = -abb[55] * z[67];
z[14] = z[14] + z[15] + z[17] + z[63] + -z[65] + z[68];
z[14] = abb[50] * z[14];
z[15] = abb[35] + z[55];
z[17] = -z[15] + z[44];
z[44] = abb[6] * z[17];
z[45] = abb[53] + z[45];
z[45] = abb[7] * z[45];
z[55] = -abb[3] * abb[35];
z[60] = -abb[34] * z[60];
z[44] = z[44] + z[45] + -z[52] + z[55] + z[60];
z[45] = abb[29] * z[11];
z[16] = -z[16] + z[45] + z[64];
z[45] = abb[31] * z[39];
z[52] = abb[33] + z[58];
z[52] = abb[29] * z[52];
z[52] = abb[54] + z[45] + z[52] + z[56];
z[52] = abb[35] + -z[16] + (T(1) / T(2)) * z[52];
z[52] = abb[8] * z[52];
z[55] = abb[10] * z[15];
z[37] = z[37] + (T(1) / T(2)) * z[44] + z[52] + z[55];
z[37] = abb[51] * z[37];
z[5] = -z[3] + z[5];
z[5] = abb[30] * z[5];
z[44] = abb[36] + -abb[53] + z[53];
z[52] = abb[32] + z[6];
z[52] = abb[29] * z[52];
z[5] = z[5] + z[29] + -z[44] + z[49] + z[52];
z[5] = abb[25] * z[5];
z[22] = -z[22] + z[52] + -z[57];
z[22] = abb[22] * z[22];
z[29] = z[11] * z[39];
z[29] = z[29] + -z[45];
z[49] = abb[53] + z[29];
z[52] = abb[29] * z[2];
z[52] = z[49] + -z[52];
z[53] = -z[15] + z[52];
z[56] = -z[50] + z[53] + -z[61];
z[56] = abb[23] * z[56];
z[27] = abb[26] * z[27];
z[5] = z[5] + z[22] + -z[27] + z[56];
z[22] = abb[32] + z[3];
z[27] = abb[29] * z[22];
z[27] = z[27] + -z[32] + -z[45];
z[6] = abb[30] * z[6];
z[6] = -z[6] + z[18] + (T(1) / T(2)) * z[27] + z[48];
z[18] = abb[24] * z[6];
z[5] = (T(1) / T(2)) * z[5] + z[18];
z[18] = z[5] * z[10];
z[16] = z[16] + z[31] + (T(-1) / T(2)) * z[45];
z[16] = abb[8] * z[16];
z[20] = z[20] + z[33];
z[27] = abb[12] * z[20];
z[31] = abb[6] * (T(1) / T(2));
z[17] = -z[17] * z[31];
z[16] = z[16] + z[17] + z[27] + z[63];
z[16] = abb[47] * z[16];
z[17] = -z[43] + z[44] + z[59];
z[17] = abb[19] * z[17];
z[32] = z[42] + z[52];
z[33] = -abb[18] * z[32];
z[17] = z[17] + z[33];
z[33] = z[41] + z[48];
z[41] = -z[33] + -z[49];
z[13] = z[13] * z[41];
z[13] = z[13] + (T(1) / T(2)) * z[17];
z[13] = abb[52] * z[13];
z[8] = abb[28] + (T(-1) / T(4)) * z[8] + -z[23] + z[24] + -z[26];
z[8] = abb[52] * z[8];
z[17] = -abb[47] + abb[51];
z[17] = z[17] * z[67];
z[23] = abb[27] * (T(1) / T(4));
z[10] = -z[10] * z[23];
z[8] = z[8] + z[10] + z[17];
z[8] = abb[55] * z[8];
z[8] = z[8] + z[13] + z[14] + z[16] + z[18] + z[37];
z[10] = -abb[47] * z[9];
z[13] = -abb[34] * z[21];
z[14] = -abb[29] * abb[33];
z[16] = -abb[30] * z[25];
z[13] = -abb[35] + abb[36] + -abb[37] + abb[38] + z[13] + z[14] + z[16] + z[40] + -z[61];
z[13] = abb[51] * z[13];
z[14] = -z[15] + -z[20];
z[14] = abb[48] * z[14];
z[10] = z[10] + (T(1) / T(2)) * z[13] + z[14];
z[13] = abb[16] * (T(1) / T(2));
z[10] = z[10] * z[13];
z[9] = -z[9] * z[13];
z[13] = z[29] + z[33];
z[14] = abb[15] * z[13];
z[15] = abb[32] + -abb[33];
z[15] = abb[33] * z[15];
z[15] = z[15] + -z[50] + -z[53];
z[15] = z[15] * z[31];
z[16] = -abb[7] + -abb[15];
z[16] = z[16] * z[64];
z[14] = (T(-1) / T(2)) * z[14] + z[15] + z[16] + -z[47] + -z[54];
z[15] = -abb[33] + z[22];
z[16] = abb[29] * z[15];
z[16] = abb[36] + z[16] + z[42];
z[17] = abb[32] * (T(1) / T(2)) + -z[19];
z[17] = abb[31] * z[17];
z[7] = -z[7] + z[39];
z[7] = z[7] * z[11];
z[7] = z[7] + z[64];
z[11] = -z[7] + (T(-1) / T(2)) * z[16] + z[17] + (T(-1) / T(4)) * z[36];
z[11] = abb[1] * z[11];
z[11] = z[11] + (T(-1) / T(2)) * z[27] + (T(1) / T(2)) * z[65];
z[6] = -z[6] * z[24];
z[1] = -abb[32] + z[1] + -z[39];
z[1] = abb[31] * z[1];
z[2] = abb[29] + -z[2];
z[2] = abb[29] * z[2];
z[2] = abb[36] + z[2] + z[34];
z[2] = z[1] + (T(1) / T(2)) * z[2];
z[16] = z[3] + -z[28];
z[16] = abb[30] * z[16];
z[16] = abb[53] + z[16] + z[35];
z[2] = (T(1) / T(2)) * z[2] + (T(1) / T(4)) * z[16] + z[48];
z[2] = abb[5] * z[2];
z[16] = -z[4] + -z[66];
z[17] = abb[55] * (T(1) / T(8));
z[16] = z[16] * z[17];
z[2] = z[2] + z[6] + z[9] + -z[11] + (T(1) / T(2)) * z[14] + z[16];
z[2] = abb[49] * z[2];
z[3] = z[3] * z[15];
z[1] = z[1] + z[3] + z[7] + z[30] + (T(5) / T(12)) * z[36] + -z[46] + -z[62];
z[1] = z[1] * z[26];
z[3] = -abb[53] + -z[13];
z[3] = abb[15] * z[3];
z[6] = -abb[6] * z[32];
z[3] = z[3] + z[6];
z[3] = (T(1) / T(2)) * z[3] + z[38] + z[51] + z[55];
z[6] = -z[42] + -z[53];
z[6] = z[6] * z[24];
z[4] = -z[4] * z[17];
z[1] = z[1] + (T(1) / T(2)) * z[3] + z[4] + z[6] + -z[11];
z[1] = abb[48] * z[1];
z[3] = abb[55] * z[23];
z[3] = z[3] + -z[5];
z[3] = z[3] * z[12];
z[1] = z[1] + z[2] + z[3] + (T(1) / T(2)) * z[8] + z[10];
z[1] = (T(1) / T(8)) * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_123_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.36813472680370106153428655606201240311053498886403753685408638535"),stof<T>("-0.0893823334400750971016757222276308508916324177160810151412019251")}, std::complex<T>{stof<T>("-0.31935761537108200263418637367372447756092390279539149128999424678"),stof<T>("-0.0782676175874082845274551552359817736180068193959733119391516899")}, std::complex<T>{stof<T>("-0.74201942046551508413928863172573184438203149039584953997042159767"),stof<T>("-0.239300485380256874911974132104265884203365260508724706707923318")}, std::complex<T>{stof<T>("-0.33268789271474433825794687153262863030490766914840443961897461061"),stof<T>("-0.0782676175874082845274551552359817736180068193959733119391516899")}, std::complex<T>{stof<T>("0.35352611360417871083052009615230804847780643118374766429767337313"),stof<T>("0.0893823334400750971016757222276308508916324177160810151412019251")}, std::complex<T>{stof<T>("0.34913764372941623396282949923612243757713194499727521855068316693"),stof<T>("0.10954301550664013236867633762511965301720890386360536038993468996")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_123_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_123_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.05228749863635302844347072576415118845455338957185586304271570779"),stof<T>("0.17378114512730445888008483338244308103428996613747556767710587947")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_123_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_123_DLogXconstant_part(base_point<T>, kend);
	value += f_4_123_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_123_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_123_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_123_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_123_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_123_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_123_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
