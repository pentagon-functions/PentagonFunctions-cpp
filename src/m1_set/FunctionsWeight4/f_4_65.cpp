/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_65.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_65_abbreviated (const std::array<T,55>& abb) {
T z[110];
z[0] = abb[28] * abb[50];
z[1] = abb[28] * abb[48];
z[2] = abb[28] * abb[49];
z[3] = z[0] + z[1] + -z[2];
z[4] = abb[4] * z[3];
z[4] = (T(1) / T(2)) * z[4];
z[5] = abb[49] + -abb[50];
z[6] = -abb[48] + z[5];
z[7] = -abb[28] + abb[29];
z[8] = -abb[27] + z[7];
z[9] = abb[13] * z[6] * z[8];
z[10] = abb[27] + -abb[32];
z[11] = abb[49] * z[10];
z[12] = abb[50] * z[10];
z[11] = z[11] + -z[12];
z[13] = abb[47] * z[10];
z[14] = z[11] + z[13];
z[15] = abb[46] * z[10];
z[16] = z[14] + z[15];
z[17] = abb[2] * z[16];
z[9] = z[4] + (T(-1) / T(2)) * z[9] + z[17];
z[17] = abb[29] * (T(1) / T(2));
z[18] = -z[10] + z[17];
z[19] = abb[28] * (T(1) / T(2));
z[20] = z[18] + -z[19];
z[20] = z[5] * z[20];
z[21] = abb[48] * z[7];
z[22] = abb[46] * (T(1) / T(2));
z[23] = -abb[27] * z[22];
z[20] = -z[13] + z[20] + (T(-1) / T(2)) * z[21] + z[23];
z[20] = abb[8] * z[20];
z[23] = z[10] * z[22];
z[23] = z[14] + z[23];
z[23] = abb[15] * z[23];
z[24] = -abb[27] + z[17];
z[25] = abb[32] * (T(1) / T(2));
z[26] = z[24] + z[25];
z[27] = abb[48] + abb[50];
z[26] = z[26] * z[27];
z[28] = abb[49] * (T(1) / T(2));
z[29] = abb[29] + -abb[32];
z[30] = z[28] * z[29];
z[26] = z[26] + z[30];
z[26] = abb[6] * z[26];
z[30] = abb[7] * (T(1) / T(2));
z[31] = -z[6] * z[29] * z[30];
z[32] = -abb[42] + abb[45];
z[33] = abb[41] + abb[43];
z[34] = abb[44] + z[32] + -z[33];
z[35] = abb[22] * (T(1) / T(2));
z[8] = z[8] * z[34] * z[35];
z[36] = abb[48] + -abb[50];
z[36] = (T(1) / T(2)) * z[36];
z[37] = abb[47] + z[28];
z[38] = z[36] + z[37];
z[39] = -abb[27] + abb[29];
z[40] = abb[5] * z[38] * z[39];
z[41] = abb[46] + abb[49];
z[42] = -z[27] + z[41];
z[43] = abb[0] * (T(1) / T(2));
z[42] = abb[27] * z[42] * z[43];
z[44] = abb[50] * z[7];
z[44] = z[21] + z[44];
z[44] = abb[10] * z[44];
z[45] = abb[47] * z[7];
z[21] = z[21] + z[45];
z[21] = abb[1] * z[21];
z[45] = abb[19] * z[10];
z[46] = -abb[16] * abb[27];
z[46] = z[45] + z[46];
z[47] = abb[51] * (T(1) / T(2));
z[46] = z[46] * z[47];
z[8] = z[8] + -z[9] + z[20] + z[21] + z[23] + z[26] + z[31] + z[40] + z[42] + z[44] + z[46];
z[20] = (T(1) / T(2)) * z[34];
z[21] = abb[24] * z[20];
z[26] = abb[47] + z[5];
z[31] = z[22] + z[26];
z[40] = abb[15] * z[31];
z[26] = abb[46] + z[26];
z[42] = abb[2] * z[26];
z[44] = z[40] + -z[42];
z[21] = z[21] + -z[44];
z[46] = abb[17] + abb[19];
z[48] = z[46] * z[47];
z[49] = abb[46] + z[6];
z[50] = z[43] * z[49];
z[51] = (T(1) / T(2)) * z[6];
z[52] = abb[13] * z[51];
z[53] = z[50] + -z[52];
z[54] = abb[3] * z[22];
z[55] = abb[6] * z[51];
z[54] = z[54] + z[55];
z[55] = abb[20] * z[20];
z[56] = abb[8] * z[26];
z[48] = -z[21] + z[48] + z[53] + z[54] + z[55] + -z[56];
z[56] = abb[30] * (T(1) / T(2));
z[57] = -z[48] * z[56];
z[58] = abb[22] * z[20];
z[59] = abb[4] * z[51];
z[54] = -z[54] + -z[58] + z[59];
z[31] = abb[8] * z[31];
z[21] = z[21] + z[31];
z[31] = abb[16] + z[46];
z[31] = z[31] * z[47];
z[31] = z[21] + -z[31] + z[54] + -z[55];
z[60] = (T(1) / T(2)) * z[31];
z[61] = abb[31] * z[60];
z[62] = -z[10] * z[34];
z[63] = abb[24] * z[62];
z[61] = -z[61] + (T(1) / T(4)) * z[63];
z[64] = abb[47] + abb[48];
z[65] = abb[49] + z[64];
z[65] = abb[12] * z[65];
z[66] = abb[27] + abb[28];
z[67] = -abb[29] + (T(1) / T(2)) * z[66];
z[67] = z[65] * z[67];
z[8] = (T(1) / T(2)) * z[8] + z[57] + z[61] + z[67];
z[8] = m1_set::bc<T>[0] * z[8];
z[36] = abb[47] + z[36];
z[57] = z[28] + z[36];
z[67] = abb[5] * z[57];
z[58] = z[58] + z[67];
z[68] = abb[16] + -abb[19];
z[68] = z[47] * z[68];
z[69] = abb[6] * z[27];
z[21] = z[21] + -z[53] + z[58] + -z[65] + z[68] + z[69];
z[21] = (T(1) / T(2)) * z[21];
z[53] = -abb[38] * z[21];
z[68] = -abb[7] + abb[8];
z[68] = z[51] * z[68];
z[69] = abb[49] + z[27];
z[70] = abb[6] * (T(1) / T(2));
z[69] = z[69] * z[70];
z[71] = abb[1] * z[64];
z[72] = abb[10] * z[27];
z[73] = z[71] + z[72];
z[52] = z[52] + z[58] + z[68] + z[69] + z[73];
z[52] = (T(1) / T(2)) * z[52] + -z[65];
z[58] = abb[39] * z[52];
z[68] = abb[23] * z[20];
z[31] = -z[31] + z[68];
z[31] = abb[40] * z[31];
z[68] = -z[29] * z[34];
z[74] = abb[30] * z[34];
z[68] = z[68] + z[74];
z[75] = -abb[31] * z[34];
z[75] = z[68] + z[75];
z[75] = m1_set::bc<T>[0] * z[75];
z[76] = abb[39] * z[34];
z[76] = -z[75] + z[76];
z[77] = abb[23] * (T(1) / T(4));
z[76] = z[76] * z[77];
z[78] = -abb[39] + -abb[40];
z[78] = -z[34] * z[78];
z[75] = -z[75] + z[78];
z[78] = abb[21] * (T(1) / T(4));
z[75] = z[75] * z[78];
z[8] = z[8] + (T(1) / T(2)) * z[31] + z[53] + z[58] + z[75] + z[76];
z[8] = (T(1) / T(8)) * z[8];
z[31] = abb[46] + abb[49] * (T(3) / T(2)) + z[36];
z[31] = abb[13] * z[31];
z[31] = z[31] + -z[67];
z[36] = abb[9] * z[41];
z[51] = -abb[3] * z[51];
z[42] = -z[31] + z[36] + z[42] + z[51] + z[55] + z[59] + z[69];
z[42] = abb[35] * z[42];
z[51] = abb[28] + abb[32];
z[53] = -z[19] * z[51];
z[58] = abb[27] * (T(1) / T(2));
z[59] = z[10] * z[58];
z[69] = -abb[28] + z[10];
z[75] = -z[17] * z[69];
z[53] = -abb[36] + z[53] + z[59] + z[75];
z[53] = abb[48] * z[53];
z[59] = prod_pow(abb[27], 2);
z[24] = abb[29] * z[24];
z[75] = prod_pow(abb[28], 2);
z[75] = abb[36] + (T(1) / T(2)) * z[75];
z[59] = z[24] + (T(1) / T(2)) * z[59] + -z[75];
z[76] = abb[46] + abb[47];
z[79] = z[59] * z[76];
z[80] = abb[27] + abb[32];
z[81] = -z[7] + z[80];
z[81] = abb[29] * z[81];
z[82] = abb[28] * abb[32];
z[83] = -abb[27] * abb[32];
z[83] = z[81] + -z[82] + z[83];
z[84] = abb[50] * (T(1) / T(2));
z[83] = z[83] * z[84];
z[85] = abb[28] + -abb[32];
z[86] = -z[19] * z[85];
z[87] = z[58] * z[80];
z[51] = -3 * abb[27] + -z[51];
z[51] = abb[29] + (T(1) / T(2)) * z[51];
z[51] = abb[29] * z[51];
z[51] = -abb[36] + z[51] + z[86] + z[87];
z[51] = abb[49] * z[51];
z[51] = z[51] + z[53] + z[79] + z[83];
z[51] = abb[13] * z[51];
z[53] = -z[28] + z[84];
z[79] = prod_pow(abb[32], 2);
z[83] = (T(1) / T(2)) * z[79];
z[84] = z[75] + -z[83];
z[86] = abb[27] * z[25];
z[86] = z[84] + z[86];
z[53] = z[53] * z[86];
z[86] = abb[27] * z[85];
z[87] = abb[34] + z[79];
z[86] = -z[82] + z[86] + z[87];
z[88] = z[22] * z[86];
z[89] = z[75] + z[83];
z[90] = -abb[27] + -z[25];
z[90] = abb[27] * z[90];
z[90] = z[89] + z[90];
z[90] = -z[24] + (T(1) / T(2)) * z[90];
z[90] = abb[48] * z[90];
z[91] = abb[27] * z[80];
z[92] = -z[79] + z[91];
z[24] = z[24] + (T(1) / T(2)) * z[92];
z[92] = -abb[47] * z[24];
z[53] = z[53] + z[88] + z[90] + z[92];
z[53] = abb[3] * z[53];
z[49] = abb[0] * z[49];
z[44] = -z[44] + -z[49] + z[54];
z[54] = abb[51] * (T(1) / T(4));
z[88] = abb[16] + -z[46];
z[88] = z[54] * z[88];
z[90] = abb[20] * (T(1) / T(4));
z[92] = -z[34] * z[90];
z[44] = (T(1) / T(2)) * z[44] + z[65] + -z[73] + z[88] + z[92];
z[73] = -abb[24] * z[34];
z[88] = z[22] + (T(1) / T(3)) * z[64];
z[92] = abb[8] * (T(1) / T(2));
z[88] = z[88] * z[92];
z[44] = (T(1) / T(3)) * z[44] + (T(-1) / T(12)) * z[73] + z[88];
z[88] = prod_pow(m1_set::bc<T>[0], 2);
z[44] = z[44] * z[88];
z[81] = z[81] + -z[89];
z[89] = -abb[34] + z[81];
z[93] = -abb[28] + z[58];
z[94] = -abb[32] + z[93];
z[94] = abb[27] * z[94];
z[94] = -z[89] + -z[94];
z[94] = z[27] * z[94];
z[95] = -abb[32] + -z[93];
z[95] = abb[27] * z[95];
z[96] = -abb[28] + z[80];
z[96] = abb[29] * z[96];
z[95] = -abb[34] + z[84] + z[95] + z[96];
z[95] = abb[49] * z[95];
z[94] = z[94] + z[95];
z[24] = -abb[46] * z[24];
z[24] = z[24] + (T(1) / T(2)) * z[94];
z[24] = abb[6] * z[24];
z[94] = z[79] + z[91];
z[95] = -z[17] + z[80];
z[95] = abb[29] * z[95];
z[94] = (T(1) / T(2)) * z[94] + -z[95];
z[96] = abb[34] + z[94];
z[97] = abb[47] + z[22];
z[98] = z[96] * z[97];
z[94] = abb[48] * z[94];
z[99] = abb[34] * abb[48];
z[94] = z[94] + z[98] + z[99];
z[94] = abb[14] * z[94];
z[98] = abb[28] + z[25];
z[98] = abb[27] * z[98];
z[100] = -z[81] + z[98];
z[100] = -z[33] * z[100];
z[101] = -z[89] + z[98];
z[32] = abb[44] + z[32];
z[101] = z[32] * z[101];
z[102] = abb[34] * z[33];
z[100] = z[100] + z[101] + -z[102];
z[100] = z[35] * z[100];
z[101] = z[5] + z[97];
z[103] = abb[15] * (T(1) / T(2));
z[104] = abb[27] * z[10];
z[101] = z[101] * z[103] * z[104];
z[105] = -abb[28] + z[17];
z[105] = abb[29] * z[105];
z[106] = z[75] + z[105];
z[93] = abb[27] * z[93];
z[93] = z[93] + -z[106];
z[38] = -abb[5] * z[38] * z[93];
z[93] = abb[47] + -abb[50];
z[107] = z[41] + z[93];
z[107] = abb[2] * z[107];
z[108] = -abb[32] + z[58];
z[108] = abb[27] * z[108];
z[84] = -z[84] + z[108];
z[84] = -z[84] * z[107];
z[108] = z[82] + -z[83];
z[105] = -abb[34] + z[105] + z[108];
z[109] = -abb[49] + z[27];
z[30] = z[30] * z[105] * z[109];
z[105] = -abb[3] + abb[14];
z[97] = abb[48] + z[97];
z[105] = z[97] * z[105];
z[41] = -abb[6] * z[41];
z[31] = z[31] + z[41] + z[105];
z[31] = abb[33] * z[31];
z[41] = abb[27] * (T(3) / T(2));
z[105] = abb[32] * z[41];
z[105] = -abb[33] + z[95] + -z[105] + -z[108];
z[105] = z[50] * z[105];
z[59] = -abb[33] + -z[59];
z[59] = z[36] * z[59];
z[108] = abb[29] * abb[32];
z[87] = -z[87] + z[108];
z[87] = z[76] * z[87];
z[108] = -z[79] + z[108];
z[108] = abb[48] * z[108];
z[87] = z[87] + -z[99] + z[108];
z[87] = abb[11] * z[87];
z[72] = z[72] * z[106];
z[24] = z[24] + z[30] + z[31] + z[38] + z[42] + z[44] + z[51] + z[53] + z[59] + z[72] + z[84] + z[87] + z[94] + z[100] + z[101] + z[105];
z[30] = abb[3] * (T(1) / T(2));
z[31] = abb[46] + -z[6];
z[31] = z[30] * z[31];
z[38] = abb[6] * abb[49];
z[31] = z[31] + z[36] + z[38] + z[40] + z[67];
z[36] = abb[4] * z[6];
z[36] = z[36] + z[49] + z[73];
z[38] = -abb[49] + -z[22] + (T(-1) / T(2)) * z[93];
z[38] = abb[13] * z[38];
z[40] = z[46] * z[54];
z[26] = -z[26] * z[92];
z[26] = z[26] + (T(1) / T(2)) * z[31] + (T(1) / T(4)) * z[36] + z[38] + z[40] + z[55];
z[26] = abb[30] * z[26];
z[31] = abb[28] + z[10];
z[36] = -z[31] * z[34];
z[38] = abb[20] * z[36];
z[38] = -z[38] + z[63];
z[3] = -z[3] + -z[15];
z[3] = z[3] * z[30];
z[30] = -abb[50] * z[31];
z[40] = -abb[48] * z[69];
z[30] = z[30] + z[40];
z[40] = 3 * abb[28] + z[10];
z[40] = z[28] * z[40];
z[42] = abb[28] * z[76];
z[30] = (T(1) / T(2)) * z[30] + z[40] + z[42];
z[30] = abb[13] * z[30];
z[40] = abb[48] * z[10];
z[11] = -z[11] + -z[15] + z[40];
z[11] = z[11] * z[43];
z[27] = z[27] * z[69];
z[31] = -abb[49] * z[31];
z[27] = z[27] + z[31];
z[27] = z[27] * z[70];
z[0] = z[0] + -z[1];
z[1] = -abb[28] * z[37];
z[0] = (T(1) / T(2)) * z[0] + z[1];
z[0] = abb[5] * z[0];
z[1] = z[69] * z[107];
z[31] = -abb[28] * abb[46];
z[2] = -z[2] + z[31];
z[2] = abb[9] * z[2];
z[31] = abb[17] * z[10];
z[37] = -z[31] + -z[45];
z[37] = z[37] * z[47];
z[16] = abb[8] * z[16];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[11] + z[16] + -z[23] + z[26] + z[27] + z[30] + z[37] + (T(-1) / T(2)) * z[38];
z[0] = z[0] * z[56];
z[1] = -abb[30] * z[48];
z[2] = -z[12] + z[40];
z[3] = z[10] * z[28];
z[2] = (T(1) / T(2)) * z[2] + z[3] + z[13] + z[15];
z[2] = abb[3] * z[2];
z[3] = -z[6] * z[39];
z[3] = z[3] + z[15];
z[3] = abb[6] * z[3];
z[2] = z[2] + z[3] + z[23];
z[3] = -z[7] + z[25] + z[58];
z[4] = -abb[16] * z[3];
z[6] = -abb[29] + (T(1) / T(2)) * z[80];
z[10] = -abb[18] * z[6];
z[4] = z[4] + z[10] + z[31] + (T(1) / T(2)) * z[45];
z[4] = z[4] * z[47];
z[10] = z[32] + -z[33];
z[3] = -z[3] * z[10] * z[35];
z[7] = -z[7] + -z[25] + z[41];
z[11] = z[7] * z[50];
z[7] = -abb[46] * z[7];
z[7] = z[7] + -z[14];
z[7] = z[7] * z[92];
z[12] = -z[22] + -z[64];
z[6] = abb[14] * z[6] * z[12];
z[12] = -z[39] * z[71];
z[13] = -z[62] * z[90];
z[14] = -abb[46] + -z[64];
z[14] = abb[11] * z[14] * z[29];
z[1] = z[1] + (T(1) / T(2)) * z[2] + z[3] + z[4] + z[6] + z[7] + -z[9] + z[11] + z[12] + z[13] + z[14] + z[61];
z[2] = abb[31] * (T(1) / T(2));
z[1] = z[1] * z[2];
z[2] = -abb[25] * z[20];
z[3] = abb[17] * z[57];
z[4] = abb[19] * z[5];
z[6] = abb[16] * abb[48];
z[7] = abb[16] + abb[19];
z[9] = abb[47] * z[7];
z[7] = abb[17] + (T(1) / T(2)) * z[7];
z[7] = abb[46] * z[7];
z[11] = abb[18] * z[97];
z[2] = z[2] + z[3] + z[4] + z[6] + z[7] + z[9] + z[11];
z[3] = abb[0] + abb[14];
z[4] = abb[3] + -abb[6] + z[103];
z[3] = -abb[26] + abb[8] * (T(3) / T(8)) + (T(1) / T(8)) * z[3] + (T(1) / T(4)) * z[4];
z[3] = abb[51] * z[3];
z[2] = (T(1) / T(4)) * z[2] + z[3];
z[2] = abb[37] * z[2];
z[3] = z[36] + z[74];
z[3] = abb[30] * z[3];
z[4] = abb[28] + z[58];
z[6] = abb[27] * z[4];
z[7] = z[6] + -z[81];
z[7] = -z[7] * z[33];
z[6] = z[6] + -z[89];
z[6] = z[6] * z[32];
z[9] = abb[31] * z[20];
z[11] = z[9] + -z[68];
z[11] = abb[31] * z[11];
z[12] = -abb[35] + abb[53] + abb[54] + (T(1) / T(6)) * z[88];
z[13] = -z[12] * z[34];
z[6] = z[3] + z[6] + z[7] + z[11] + z[13] + -z[102];
z[6] = z[6] * z[78];
z[7] = abb[52] * z[21];
z[11] = abb[34] + z[83] + -z[95] + z[98];
z[11] = abb[16] * z[11];
z[13] = -abb[33] + z[86];
z[13] = abb[17] * z[13];
z[14] = abb[33] + z[96];
z[14] = abb[18] * z[14];
z[15] = z[45] * z[58];
z[11] = z[11] + z[13] + z[14] + z[15];
z[11] = z[11] * z[54];
z[13] = abb[32] * (T(3) / T(2)) + -z[66];
z[13] = z[13] * z[58];
z[14] = -z[17] * z[18];
z[15] = abb[34] * (T(1) / T(2));
z[13] = z[13] + z[14] + -z[15] + (T(-1) / T(4)) * z[79] + z[82];
z[13] = abb[46] * z[13];
z[14] = z[104] + -z[106];
z[5] = -z[5] * z[14];
z[14] = -abb[48] * z[106];
z[16] = -abb[47] * z[104];
z[5] = z[5] + z[14] + z[16];
z[5] = (T(1) / T(2)) * z[5] + z[13];
z[5] = z[5] * z[92];
z[13] = -abb[53] * z[52];
z[14] = abb[54] * z[60];
z[9] = z[9] + (T(-1) / T(2)) * z[62] + -z[74];
z[9] = abb[31] * z[9];
z[16] = -abb[28] + z[25];
z[16] = abb[27] * z[16];
z[17] = z[16] + -z[106];
z[10] = -z[10] * z[17];
z[12] = -abb[33] + -z[12];
z[12] = z[12] * z[34];
z[3] = z[3] + z[9] + z[10] + z[12];
z[3] = z[3] * z[77];
z[9] = -z[75] + z[91];
z[10] = -abb[27] + abb[29] * (T(1) / T(4)) + (T(1) / T(2)) * z[85];
z[10] = abb[29] * z[10];
z[9] = (T(1) / T(2)) * z[9] + z[10];
z[10] = -abb[48] * z[9];
z[9] = -z[9] + -z[15];
z[9] = abb[47] * z[9];
z[12] = -abb[33] * z[64];
z[9] = z[9] + z[10] + z[12] + (T(-1) / T(2)) * z[99];
z[9] = abb[1] * z[9];
z[10] = -abb[32] + z[19];
z[10] = abb[28] * z[10];
z[10] = abb[36] + z[10] + -z[16] + z[83];
z[12] = -z[10] * z[33];
z[10] = abb[34] + z[10];
z[10] = z[10] * z[32];
z[15] = -abb[33] * z[34];
z[10] = z[10] + z[12] + z[15] + -z[102];
z[10] = z[10] * z[90];
z[12] = abb[27] * abb[28];
z[12] = z[12] + z[75];
z[4] = abb[29] * (T(3) / T(4)) + -z[4];
z[4] = abb[29] * z[4];
z[4] = z[4] + (T(1) / T(2)) * z[12];
z[4] = -z[4] * z[65];
z[12] = z[73] * z[104];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[9] + z[10] + z[11] + (T(1) / T(8)) * z[12] + z[13] + z[14] + (T(1) / T(2)) * z[24];
z[0] = (T(1) / T(8)) * z[0];
return {z[8], z[0]};
}


template <typename T> std::complex<T> f_4_65_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.09382579361082897682366377363861259116751075385895296090785212793"),stof<T>("0.12832150841338710834857793158407761055448379905059110604202612514")}, std::complex<T>{stof<T>("0.10198647717095071663959355714702994394167512172568796675986275992"),stof<T>("0.12102525889729807149284139805493756552266918831067577564555943721")}, std::complex<T>{stof<T>("0.098430801155425983306243512074946629640624871321852072834017440721"),stof<T>("0.098781766478966211316103650373724145600679262734623516910986311868")}, std::complex<T>{stof<T>("0.09967689319174666382051443906712154606640149297235772544416572387"),stof<T>("0.11414779797419843910718037329991085058498641774411145324386241959")}, std::complex<T>{stof<T>("-0.0035556760155247333333500450720833143010502504038358939258453192"),stof<T>("-0.022243492418331860176737747681213419921989925576052258734573125343")}, std::complex<T>{stof<T>("0.080610197645943882040336503662042835406726712312518261807545420308"),stof<T>("0.108063422522188153157371941282165206955170803724137639419870031113")}};
	
	std::vector<C> intdlogs = {rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_65_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_65_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.1048986139193034620440683042478487250690525310754507657177961592"),stof<T>("-0.10270139769601509863597051635081703607726541005692490421616384826")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k)};

                    
            return f_4_65_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_65_DLogXconstant_part(base_point<T>, kend);
	value += f_4_65_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_65_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_65_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_65_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_65_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_65_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_65_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
