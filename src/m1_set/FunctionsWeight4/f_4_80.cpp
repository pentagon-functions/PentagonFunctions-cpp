/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_80.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_80_abbreviated (const std::array<T,55>& abb) {
T z[116];
z[0] = abb[30] * (T(1) / T(2));
z[1] = -abb[31] + z[0];
z[1] = abb[30] * z[1];
z[2] = prod_pow(abb[31], 2);
z[3] = (T(1) / T(2)) * z[2];
z[4] = z[1] + z[3];
z[5] = abb[27] * (T(1) / T(2));
z[6] = -abb[26] + abb[29];
z[7] = abb[28] + z[6];
z[8] = z[5] + -z[7];
z[8] = abb[27] * z[8];
z[9] = abb[30] + -abb[31];
z[10] = -abb[26] + z[9];
z[11] = abb[28] + z[10];
z[11] = abb[28] * z[11];
z[12] = -abb[29] + z[9];
z[13] = abb[26] + z[12];
z[14] = abb[29] * z[13];
z[15] = prod_pow(abb[26], 2);
z[16] = (T(1) / T(2)) * z[15];
z[8] = z[4] + z[8] + z[11] + -z[14] + z[16];
z[8] = abb[21] * z[8];
z[17] = abb[26] * z[9];
z[18] = abb[30] * z[9];
z[19] = z[2] + -z[17] + z[18];
z[19] = abb[22] * z[19];
z[20] = -abb[25] * abb[36];
z[19] = z[19] + z[20];
z[20] = abb[22] + abb[23];
z[20] = z[5] * z[20];
z[21] = abb[26] + -abb[30];
z[22] = -abb[28] + z[21];
z[22] = abb[22] * z[22];
z[23] = -abb[23] * z[7];
z[20] = z[20] + z[22] + z[23];
z[20] = abb[27] * z[20];
z[22] = abb[26] + z[9];
z[23] = abb[26] * z[22];
z[24] = -z[18] + -z[23];
z[25] = abb[29] * (T(1) / T(2));
z[22] = z[22] + -z[25];
z[22] = abb[29] * z[22];
z[22] = z[22] + (T(1) / T(2)) * z[24];
z[22] = abb[24] * z[22];
z[17] = z[17] + z[18];
z[18] = z[2] + z[17];
z[5] = -abb[31] + z[5] + -z[6];
z[5] = abb[27] * z[5];
z[5] = abb[35] + z[5] + -z[14] + (T(1) / T(2)) * z[18];
z[5] = abb[20] * z[5];
z[18] = abb[35] + -abb[52];
z[24] = abb[21] + abb[23];
z[26] = abb[22] + z[24];
z[18] = z[18] * z[26];
z[27] = prod_pow(abb[28], 2);
z[17] = z[17] + z[27];
z[14] = -z[14] + (T(1) / T(2)) * z[17];
z[14] = abb[23] * z[14];
z[11] = abb[22] * z[11];
z[17] = abb[20] + abb[21] + abb[22];
z[17] = abb[33] * z[17];
z[28] = abb[20] + z[24];
z[28] = abb[34] * z[28];
z[29] = -abb[20] + -abb[23];
z[29] = abb[32] * z[29];
z[30] = abb[22] + abb[24];
z[31] = abb[51] * z[30];
z[32] = abb[20] + -abb[24] + z[26];
z[33] = -abb[53] * z[32];
z[5] = z[5] + z[8] + z[11] + z[14] + z[17] + z[18] + (T(1) / T(2)) * z[19] + z[20] + z[22] + z[28] + z[29] + z[31] + z[33];
z[8] = prod_pow(m1_set::bc<T>[0], 2);
z[11] = z[8] * z[32];
z[5] = 3 * z[5] + (T(-1) / T(2)) * z[11];
z[5] = abb[50] * z[5];
z[11] = abb[27] * (T(3) / T(2));
z[14] = 3 * abb[29];
z[17] = z[11] + -z[14];
z[18] = 2 * abb[26];
z[19] = z[9] + z[18];
z[20] = z[17] + z[19];
z[20] = abb[27] * z[20];
z[22] = abb[28] * (T(3) / T(2));
z[28] = z[10] + z[22];
z[29] = abb[28] * z[28];
z[31] = abb[26] * (T(1) / T(2));
z[33] = z[9] + z[31];
z[33] = abb[26] * z[33];
z[34] = abb[29] * (T(3) / T(2));
z[35] = z[19] + -z[34];
z[35] = abb[29] * z[35];
z[36] = 2 * abb[35];
z[20] = -z[20] + z[29] + -z[33] + z[35] + -z[36];
z[20] = abb[13] * z[20];
z[33] = -abb[26] + -2 * z[9] + z[34];
z[33] = abb[29] * z[33];
z[35] = 2 * abb[31];
z[37] = -abb[30] + z[35];
z[37] = abb[30] * z[37];
z[33] = -z[3] + z[16] + z[33] + -z[37];
z[33] = abb[8] * z[33];
z[38] = z[9] + -z[31];
z[38] = abb[26] * z[38];
z[39] = 3 * abb[28];
z[28] = z[28] * z[39];
z[28] = z[4] + z[28] + -z[38];
z[28] = abb[1] * z[28];
z[38] = abb[29] * z[10];
z[1] = -z[1] + z[2] + z[16] + z[38];
z[16] = abb[3] + abb[6];
z[1] = z[1] * z[16];
z[17] = abb[26] + 2 * abb[30] + abb[31] + z[17];
z[17] = z[16] * z[17];
z[38] = abb[30] + z[35];
z[40] = -abb[26] + z[38];
z[40] = abb[8] * z[40];
z[41] = abb[29] + -abb[30];
z[42] = abb[4] * z[41];
z[17] = z[17] + z[40] + 3 * z[42];
z[17] = abb[27] * z[17];
z[40] = z[12] * z[14];
z[37] = -z[2] + z[37];
z[23] = z[23] + -z[37] + -z[40];
z[23] = abb[2] * z[23];
z[29] = -abb[35] + -z[29];
z[40] = -abb[8] + z[16];
z[29] = z[29] * z[40];
z[42] = abb[29] * z[12];
z[43] = abb[30] * abb[31];
z[42] = z[42] + z[43];
z[44] = 3 * abb[12];
z[42] = z[42] * z[44];
z[45] = abb[1] + -abb[8];
z[46] = 3 * abb[33];
z[45] = z[45] * z[46];
z[47] = prod_pow(abb[30], 2);
z[48] = prod_pow(abb[29], 2);
z[49] = -z[47] + z[48];
z[49] = abb[4] * z[49];
z[1] = -z[1] + -z[17] + z[20] + z[23] + -z[28] + z[29] + z[33] + z[42] + -z[45] + (T(3) / T(2)) * z[49];
z[17] = -abb[43] * z[1];
z[20] = -abb[4] + z[16];
z[23] = abb[13] + z[20];
z[28] = 3 * abb[34];
z[29] = z[23] * z[28];
z[1] = -z[1] + z[29];
z[1] = abb[45] * z[1];
z[29] = -abb[4] + abb[2] * (T(5) / T(3));
z[29] = abb[1] * (T(-13) / T(6)) + abb[8] * (T(-5) / T(3)) + abb[12] * (T(7) / T(2)) + (T(-23) / T(6)) * z[16] + (T(1) / T(2)) * z[29];
z[33] = abb[43] + abb[45];
z[29] = z[29] * z[33];
z[42] = abb[42] + -abb[48];
z[45] = -abb[44] + abb[46];
z[49] = z[42] + z[45];
z[50] = abb[12] * z[49];
z[51] = 3 * abb[44];
z[52] = abb[46] + abb[49];
z[53] = z[51] + -z[52];
z[54] = abb[4] * z[53];
z[55] = -abb[46] + abb[49];
z[56] = -abb[44] + z[55];
z[57] = (T(1) / T(2)) * z[56];
z[58] = -z[42] + z[57];
z[59] = abb[15] * z[58];
z[60] = abb[41] * (T(13) / T(3));
z[61] = abb[44] + abb[49];
z[62] = abb[46] * (T(-43) / T(3)) + -z[61];
z[62] = z[60] + (T(1) / T(2)) * z[62];
z[63] = 3 * abb[48];
z[62] = abb[42] * (T(-5) / T(6)) + (T(1) / T(2)) * z[62] + z[63];
z[62] = abb[8] * z[62];
z[64] = abb[46] * (T(23) / T(3));
z[65] = abb[49] + -z[51] + -z[64];
z[60] = 13 * abb[48] + abb[42] * (T(-13) / T(3)) + -z[60] + (T(1) / T(2)) * z[65];
z[65] = abb[3] * (T(1) / T(2));
z[60] = z[60] * z[65];
z[65] = abb[6] * (T(1) / T(2));
z[64] = 13 * abb[44] + abb[49] * (T(-23) / T(3)) + -z[64];
z[64] = 5 * abb[47] + (T(1) / T(2)) * z[64];
z[64] = z[64] * z[65];
z[66] = -abb[49] + abb[46] * (T(1) / T(2));
z[67] = abb[47] * (T(-1) / T(2)) + (T(-1) / T(3)) * z[66];
z[67] = abb[2] * z[67];
z[68] = -abb[41] + (T(1) / T(3)) * z[52];
z[68] = abb[1] * z[68];
z[69] = abb[41] + -abb[49];
z[70] = abb[42] + z[69];
z[71] = abb[0] * z[70];
z[29] = z[29] + (T(7) / T(2)) * z[50] + (T(1) / T(4)) * z[54] + (T(1) / T(2)) * z[59] + z[60] + z[62] + z[64] + 5 * z[67] + (T(13) / T(2)) * z[68] + (T(13) / T(6)) * z[71];
z[8] = z[8] * z[29];
z[29] = z[0] * z[53];
z[60] = 2 * abb[46];
z[62] = -abb[49] + z[60];
z[35] = z[35] * z[62];
z[64] = (T(1) / T(2)) * z[53];
z[67] = abb[26] * z[64];
z[68] = -z[29] + z[35] + z[67];
z[68] = abb[8] * z[68];
z[71] = z[34] * z[53];
z[72] = abb[30] * z[53];
z[67] = z[67] + -z[71] + z[72];
z[71] = -abb[31] * z[64];
z[71] = -z[67] + z[71];
z[71] = abb[3] * z[71];
z[73] = abb[31] * z[62];
z[67] = -z[67] + z[73];
z[67] = abb[6] * z[67];
z[74] = z[0] * z[54];
z[75] = abb[7] * z[57];
z[76] = abb[31] * z[75];
z[74] = z[74] + z[76];
z[76] = abb[4] * z[64];
z[77] = abb[44] + z[55];
z[77] = -abb[47] + (T(1) / T(2)) * z[77];
z[78] = abb[5] * z[77];
z[79] = z[76] + -z[78];
z[80] = -abb[29] * z[79];
z[81] = -abb[26] * z[78];
z[80] = z[74] + z[80] + z[81];
z[81] = abb[8] * z[57];
z[81] = z[78] + z[81];
z[82] = abb[6] * z[77];
z[82] = -z[75] + z[81] + z[82];
z[83] = z[39] * z[82];
z[84] = abb[3] * z[64];
z[85] = -abb[44] + z[52];
z[86] = (T(1) / T(2)) * z[85];
z[87] = -abb[47] + z[86];
z[87] = abb[6] * z[87];
z[81] = -z[81] + -z[84] + z[87];
z[11] = z[11] * z[81];
z[11] = z[11] + z[67] + z[68] + z[71] + 3 * z[80] + z[83];
z[11] = abb[27] * z[11];
z[67] = abb[6] * z[64];
z[67] = z[67] + z[84];
z[68] = z[67] + -z[76];
z[71] = 3 * abb[46];
z[61] = -z[61] + z[71];
z[76] = 2 * abb[42];
z[80] = 2 * abb[48] + (T(-1) / T(2)) * z[61] + -z[76];
z[80] = abb[8] * z[80];
z[20] = abb[2] + abb[8] + -abb[12] + z[20];
z[20] = z[20] * z[33];
z[81] = 3 * abb[42] + -z[63];
z[83] = z[62] + z[81];
z[84] = abb[2] * z[83];
z[20] = -z[20] + z[50] + -z[59] + z[68] + z[80] + -z[84];
z[50] = abb[53] * z[20];
z[80] = abb[8] * z[58];
z[84] = abb[47] + z[42] + -z[55];
z[87] = abb[2] * z[84];
z[88] = abb[13] * z[77];
z[59] = z[59] + -z[78] + -z[80] + z[87] + z[88];
z[80] = abb[51] * z[59];
z[87] = 3 * abb[41];
z[89] = -z[52] + z[87];
z[90] = abb[1] * z[89];
z[86] = -abb[41] + z[86];
z[91] = abb[3] * z[86];
z[92] = abb[14] * z[86];
z[90] = z[90] + -z[91] + z[92];
z[91] = 3 * abb[49];
z[93] = abb[44] + abb[46];
z[94] = z[91] + -z[93];
z[94] = -2 * abb[47] + (T(1) / T(2)) * z[94];
z[95] = abb[13] * z[94];
z[95] = -z[78] + z[95];
z[96] = -abb[1] + abb[13];
z[96] = -z[33] * z[96];
z[96] = z[90] + -z[95] + z[96];
z[96] = abb[32] * z[96];
z[69] = abb[47] + z[69];
z[69] = abb[9] * z[69];
z[97] = z[69] + (T(1) / T(2)) * z[78];
z[98] = z[15] * z[97];
z[27] = z[2] + -z[27];
z[99] = abb[41] + -abb[46];
z[27] = abb[11] * z[27] * z[99];
z[27] = z[27] + -z[50] + z[80] + z[96] + z[98];
z[50] = z[9] * z[63];
z[80] = 3 * z[93];
z[93] = 5 * abb[49];
z[96] = z[80] + -z[93];
z[96] = -z[9] * z[96];
z[98] = abb[41] * z[9];
z[96] = z[50] + (T(1) / T(2)) * z[96] + -z[98];
z[100] = abb[49] * (T(1) / T(2));
z[101] = abb[48] * (T(3) / T(2));
z[102] = abb[42] * (T(-1) / T(2)) + z[99] + -z[100] + z[101];
z[102] = abb[26] * z[102];
z[103] = abb[42] * z[9];
z[104] = 2 * z[103];
z[96] = (T(1) / T(2)) * z[96] + z[102] + -z[104];
z[96] = abb[26] * z[96];
z[102] = abb[30] * z[62];
z[50] = -z[50] + -z[73] + z[102] + 3 * z[103];
z[73] = abb[26] * z[83];
z[100] = -abb[46] + abb[42] * (T(-3) / T(2)) + z[100] + z[101];
z[100] = z[14] * z[100];
z[73] = 2 * z[50] + z[73] + z[100];
z[73] = abb[29] * z[73];
z[100] = z[51] + z[91];
z[105] = 13 * abb[46] + -z[100];
z[105] = abb[31] * z[105];
z[106] = 5 * abb[46];
z[107] = -z[51] + z[106];
z[108] = z[91] + -z[107];
z[108] = abb[30] * z[108];
z[105] = z[105] + z[108];
z[105] = abb[30] * z[105];
z[108] = 7 * abb[46] + z[100];
z[108] = z[2] * z[108];
z[105] = z[105] + z[108];
z[108] = abb[30] + abb[31];
z[109] = z[0] * z[108];
z[110] = 2 * z[2] + z[109];
z[111] = -abb[41] * z[110];
z[112] = abb[30] + -3 * abb[31];
z[101] = abb[30] * z[101] * z[112];
z[112] = z[37] * z[76];
z[73] = z[73] + z[96] + z[101] + (T(1) / T(4)) * z[105] + z[111] + z[112];
z[73] = abb[8] * z[73];
z[80] = abb[49] + z[80];
z[96] = z[9] * z[80];
z[101] = abb[48] * z[9];
z[55] = abb[41] + z[55];
z[55] = -abb[42] + (T(1) / T(2)) * z[55];
z[55] = abb[26] * z[55];
z[55] = z[55] + (T(1) / T(4)) * z[96] + -z[98] + (T(-3) / T(2)) * z[101] + (T(1) / T(2)) * z[103];
z[55] = abb[26] * z[55];
z[96] = z[51] + -z[91] + z[106];
z[96] = z[2] * z[96];
z[105] = z[91] + z[107];
z[105] = abb[31] * z[105];
z[91] = 9 * abb[44] + -z[91];
z[106] = -abb[46] + z[91];
z[106] = abb[30] * z[106];
z[105] = z[105] + z[106];
z[105] = abb[30] * z[105];
z[96] = z[96] + z[105];
z[105] = abb[31] * z[53];
z[106] = -z[72] + z[105];
z[111] = abb[26] * z[53];
z[111] = z[106] + z[111];
z[112] = z[25] * z[111];
z[113] = abb[41] * z[4];
z[114] = -z[2] + -z[109];
z[114] = z[63] * z[114];
z[110] = abb[42] * z[110];
z[55] = z[55] + (T(1) / T(4)) * z[96] + z[110] + z[112] + z[113] + z[114];
z[55] = abb[3] * z[55];
z[96] = (T(1) / T(2)) * z[106];
z[106] = 3 * abb[47];
z[110] = 2 * abb[49] + -z[106];
z[112] = -abb[46] + z[110];
z[113] = z[18] * z[112];
z[94] = -z[14] * z[94];
z[114] = -z[39] * z[77];
z[66] = abb[47] * (T(3) / T(2)) + z[66];
z[115] = abb[27] * z[66];
z[94] = z[94] + z[96] + z[113] + z[114] + -3 * z[115];
z[94] = abb[27] * z[94];
z[93] = -abb[46] + -z[51] + z[93];
z[93] = (T(1) / T(2)) * z[93] + -z[106];
z[113] = -abb[26] * z[93];
z[114] = -abb[44] + -abb[47] + abb[49];
z[34] = z[34] * z[114];
z[34] = z[34] + -z[96] + z[113];
z[34] = abb[29] * z[34];
z[66] = -abb[26] * z[66];
z[66] = z[66] + z[96];
z[66] = abb[26] * z[66];
z[96] = -z[39] * z[114];
z[96] = z[96] + -z[111];
z[96] = abb[28] * z[96];
z[113] = 3 * abb[52];
z[77] = -z[77] * z[113];
z[36] = z[36] * z[112];
z[34] = z[34] + z[36] + z[66] + z[77] + z[94] + (T(1) / T(2)) * z[96];
z[34] = abb[13] * z[34];
z[36] = z[52] + z[81] + -z[106];
z[66] = -z[31] * z[36];
z[66] = -z[50] + z[66];
z[66] = abb[26] * z[66];
z[77] = abb[26] * z[84];
z[42] = abb[47] + abb[49] + -5 * z[42] + -z[71];
z[42] = z[25] * z[42];
z[42] = z[42] + z[50] + z[77];
z[42] = z[14] * z[42];
z[37] = z[37] * z[81];
z[62] = -z[2] * z[62];
z[35] = z[35] + -z[102];
z[35] = abb[30] * z[35];
z[35] = z[35] + z[37] + z[42] + z[62] + z[66];
z[35] = abb[2] * z[35];
z[37] = abb[3] * z[89];
z[42] = z[37] + z[92];
z[62] = abb[8] * (T(1) / T(2));
z[66] = -z[45] * z[62];
z[71] = z[65] * z[114];
z[77] = abb[7] * z[56];
z[42] = (T(1) / T(2)) * z[42] + z[66] + z[71] + (T(1) / T(4)) * z[77] + -z[97];
z[42] = z[39] * z[42];
z[66] = -z[62] + z[65];
z[66] = z[66] * z[111];
z[71] = z[9] * z[85];
z[81] = abb[26] * z[86];
z[71] = (T(-1) / T(2)) * z[71] + z[81] + z[98];
z[81] = abb[14] * z[71];
z[85] = z[9] * z[87];
z[92] = abb[30] * z[52];
z[94] = abb[31] * z[52];
z[85] = -z[85] + z[92] + -z[94];
z[92] = abb[26] * z[89];
z[92] = z[85] + z[92];
z[96] = -abb[3] * z[92];
z[42] = z[42] + z[66] + -3 * z[81] + z[96];
z[42] = abb[28] * z[42];
z[31] = z[31] * z[89];
z[31] = z[31] + z[85];
z[31] = abb[26] * z[31];
z[22] = z[22] * z[89];
z[22] = z[22] + -z[92];
z[22] = z[22] * z[39];
z[66] = -z[3] * z[52];
z[81] = -z[0] * z[52];
z[81] = z[81] + z[94];
z[81] = abb[30] * z[81];
z[4] = z[4] * z[87];
z[4] = z[4] + z[22] + z[31] + z[66] + z[81];
z[4] = abb[1] * z[4];
z[22] = z[101] + -z[103];
z[31] = abb[26] * z[58];
z[56] = -z[9] * z[56];
z[31] = -z[22] + -z[31] + (T(1) / T(2)) * z[56];
z[66] = abb[26] * z[31];
z[81] = -abb[30] * z[22];
z[56] = z[0] * z[56];
z[56] = z[56] + z[66] + z[81];
z[25] = -z[25] * z[58];
z[25] = z[25] + -z[31];
z[25] = abb[29] * z[25];
z[25] = z[25] + (T(1) / T(2)) * z[56];
z[56] = 3 * abb[15];
z[25] = z[25] * z[56];
z[29] = z[29] + -z[105];
z[29] = abb[30] * z[29];
z[66] = abb[29] * z[111];
z[81] = -abb[49] + z[107];
z[3] = z[3] * z[81];
z[15] = -z[15] * z[64];
z[3] = z[3] + z[15] + z[29] + z[66];
z[3] = z[3] * z[65];
z[9] = -z[9] * z[45];
z[15] = abb[29] * z[49];
z[9] = z[9] + z[15] + z[22];
z[9] = abb[29] * z[9];
z[15] = -z[43] * z[49];
z[9] = z[9] + z[15];
z[9] = z[9] * z[44];
z[15] = -z[61] * z[62];
z[22] = abb[6] * z[57];
z[15] = z[15] + z[22] + -z[75] + z[90];
z[15] = z[15] * z[46];
z[22] = abb[31] * abb[49];
z[29] = abb[30] * abb[49];
z[45] = z[22] + -z[29] + z[98] + z[103];
z[18] = z[18] * z[70];
z[46] = -z[18] + (T(-1) / T(2)) * z[45];
z[46] = abb[26] * z[46];
z[57] = -z[2] + z[109];
z[61] = -abb[41] + -abb[42];
z[57] = z[57] * z[61];
z[22] = z[22] + z[29];
z[22] = z[0] * z[22];
z[29] = -abb[49] * z[2];
z[22] = z[22] + z[29] + z[46] + z[57];
z[22] = abb[0] * z[22];
z[29] = abb[26] * z[71];
z[43] = -z[2] + z[43];
z[43] = -z[43] * z[86];
z[29] = z[29] + z[43];
z[29] = abb[14] * z[29];
z[43] = z[48] * z[79];
z[29] = z[29] + z[43];
z[43] = z[47] * z[54];
z[2] = z[2] * z[77];
z[2] = z[2] + z[43];
z[43] = abb[44] + z[52];
z[43] = -abb[41] + -abb[48] + (T(1) / T(2)) * z[43];
z[43] = abb[16] * z[43];
z[46] = abb[18] * z[86];
z[43] = z[43] + z[46];
z[46] = abb[17] + abb[19];
z[46] = (T(-3) / T(2)) * z[46];
z[46] = z[46] * z[58];
z[43] = (T(3) / T(2)) * z[43] + z[46];
z[43] = abb[36] * z[43];
z[46] = z[82] * z[113];
z[47] = abb[8] * z[64];
z[47] = z[47] + -z[67] + -3 * z[78];
z[47] = abb[35] * z[47];
z[23] = abb[43] * z[23];
z[23] = z[23] + -z[68] + z[95];
z[23] = z[23] * z[28];
z[1] = abb[54] + z[1] + (T(-3) / T(4)) * z[2] + z[3] + z[4] + (T(1) / T(2)) * z[5] + z[8] + z[9] + z[11] + z[15] + z[17] + z[22] + z[23] + z[25] + 3 * z[27] + (T(3) / T(2)) * z[29] + z[34] + z[35] + z[42] + z[43] + z[46] + z[47] + z[55] + z[73];
z[2] = z[44] * z[108];
z[3] = 3 * abb[4];
z[4] = 2 * abb[8] + -z[3] + 4 * z[16];
z[4] = abb[27] * z[4];
z[5] = -abb[27] + z[7];
z[7] = 2 * abb[13];
z[5] = z[5] * z[7];
z[7] = z[10] + z[39];
z[8] = 2 * abb[1];
z[7] = z[7] * z[8];
z[9] = abb[2] * z[19];
z[10] = -abb[29] + z[38];
z[10] = z[10] * z[16];
z[11] = abb[28] * z[40];
z[15] = 4 * abb[31] + z[41];
z[15] = abb[8] * z[15];
z[3] = abb[30] * z[3];
z[3] = -z[2] + z[3] + z[4] + -z[5] + z[7] + -z[9] + z[10] + 2 * z[11] + z[15];
z[3] = -z[3] * z[33];
z[4] = 2 * abb[41];
z[5] = abb[42] + -z[4] + -z[63] + (T(1) / T(2)) * z[80];
z[5] = abb[26] * z[5];
z[7] = abb[46] + -z[100];
z[7] = z[0] * z[7];
z[4] = z[4] * z[38];
z[9] = -abb[29] * z[83];
z[10] = abb[31] * abb[46];
z[11] = 6 * abb[48];
z[15] = abb[31] * z[11];
z[4] = z[4] + z[5] + z[7] + z[9] + -8 * z[10] + z[15] + z[104];
z[4] = abb[8] * z[4];
z[5] = -abb[46] + -z[91];
z[0] = z[0] * z[5];
z[5] = z[11] * z[108];
z[7] = -z[38] * z[76];
z[9] = -abb[29] * z[64];
z[0] = z[0] + z[5] + z[7] + z[9] + -4 * z[10] + z[18] + -2 * z[98];
z[0] = abb[3] * z[0];
z[5] = 2 * z[69] + z[78];
z[7] = z[5] + -z[75];
z[9] = z[62] * z[81];
z[11] = -abb[6] * z[93];
z[7] = 3 * z[7] + z[9] + z[11] + -2 * z[37];
z[7] = abb[28] * z[7];
z[9] = abb[26] * z[36];
z[11] = -z[14] * z[84];
z[9] = z[9] + z[11] + z[50];
z[9] = abb[2] * z[9];
z[11] = -abb[29] * z[58];
z[11] = z[11] + -z[31];
z[11] = z[11] * z[56];
z[14] = -abb[31] * z[81];
z[15] = -abb[29] * z[53];
z[14] = z[14] + z[15] + z[72];
z[14] = z[14] * z[65];
z[15] = -abb[8] * z[81];
z[15] = z[15] + -3 * z[54];
z[16] = z[51] + -z[60] + -z[110];
z[16] = abb[6] * z[16];
z[17] = 2 * abb[3];
z[17] = z[17] * z[53];
z[15] = (T(1) / T(2)) * z[15] + z[16] + z[17];
z[15] = abb[27] * z[15];
z[16] = z[18] + z[45];
z[16] = abb[0] * z[16];
z[5] = -abb[26] * z[5];
z[5] = z[5] + z[74];
z[2] = z[2] * z[49];
z[17] = -z[39] * z[89];
z[17] = z[17] + z[92];
z[8] = z[8] * z[17];
z[17] = abb[27] + -abb[28];
z[6] = z[6] + -z[17];
z[6] = abb[13] * z[6] * z[93];
z[18] = -abb[31] * abb[41];
z[19] = abb[28] * z[99];
z[10] = z[10] + z[18] + z[19];
z[10] = abb[11] * z[10];
z[0] = z[0] + z[2] + z[3] + z[4] + 3 * z[5] + z[6] + z[7] + z[8] + z[9] + 6 * z[10] + z[11] + z[14] + z[15] + 2 * z[16];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[39] * z[20];
z[3] = abb[37] * z[59];
z[4] = z[82] + -z[88];
z[4] = abb[38] * z[4];
z[2] = z[2] + z[3] + z[4];
z[3] = abb[28] + z[12];
z[3] = -z[3] * z[24];
z[4] = z[17] + z[21];
z[4] = abb[22] * z[4];
z[5] = abb[24] * z[13];
z[6] = abb[20] * z[41];
z[3] = z[3] + z[4] + z[5] + z[6];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = abb[37] * z[30];
z[5] = -abb[38] * z[26];
z[6] = -abb[39] * z[32];
z[3] = z[3] + z[4] + z[5] + z[6];
z[3] = abb[50] * z[3];
z[0] = abb[40] + z[0] + 3 * z[2] + (T(3) / T(2)) * z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_80_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-36.682310515106630392833272475757325351289707818356945272902581058"),stof<T>("52.741337696243548012391414832183606836297750117548411587622738085")}, std::complex<T>{stof<T>("-26.198296627818665715852294092733382933427874830835336739337282745"),stof<T>("8.503853713337012963867488862109638201182081099726697992227678228")}, std::complex<T>{stof<T>("-49.524956236407311254082823283016140611008461205871124774759687584"),stof<T>("73.841783123950370409383066140526278607337617840009044329562912174")}, std::complex<T>{stof<T>("-25.29960182007458360275951304817094766106727341782048314949503923"),stof<T>("-0.231393115198248609905564510504544725433921351183129945062375423")}, std::complex<T>{stof<T>("-49.524956236407311254082823283016140611008461205871124774759687584"),stof<T>("73.841783123950370409383066140526278607337617840009044329562912174")}, std::complex<T>{stof<T>("-34.994242349480808660486572442622549822969684600108166592620316202"),stof<T>("26.641826692748775297875689373194511221230188847269298308405341226")}, std::complex<T>{stof<T>("78.871258056120432773457608788556736642252678241932957572828799883"),stof<T>("-76.572862457047181862460952427279532632895456463743946810263047842")}, std::complex<T>{stof<T>("22.151596628180127799237021635363734563250931212593987090763209676"),stof<T>("-5.541381265041952900884038064851839450190321124808665566465167137")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_80_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_80_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(32)) * (-v[3] + v[5]) * (4 * (12 + 2 * v[0] + -2 * v[1] + v[3] + 3 * v[5] + -m1_set::bc<T>[1] * (4 + v[3] + v[5])) + -m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((-2 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(-3) / T(4)) * (-v[3] + v[5])) / tend;


		return (abb[43] + -abb[44] + abb[45] + -abb[47] + abb[49]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[27] + -abb[29];
z[1] = abb[26] + abb[27];
z[0] = z[0] * z[1];
z[0] = -abb[32] + abb[34] + abb[35] + z[0];
z[1] = -abb[43] + abb[44] + -abb[45] + abb[47] + -abb[49];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_80_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(3) / T(8)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(3) / T(2)) * (-v[3] + v[5])) / tend;


		return (abb[43] + -abb[44] + abb[45] + -abb[47] + abb[49]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[43] + -abb[44] + abb[45] + -abb[47] + abb[49];
z[1] = abb[27] + -abb[29];
return 3 * abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_80_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("134.00016935824535130876180219353654991258256745266805246001734843"),stof<T>("57.62304878618258613068434794966887603493917368650055524885138918")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W16(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[27].real()/k.W[27].real()), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[40] = SpDLog_f_4_80_W_16_Im(t, path, abb);
abb[54] = SpDLog_f_4_80_W_16_Re(t, path, abb);

                    
            return f_4_80_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_80_DLogXconstant_part(base_point<T>, kend);
	value += f_4_80_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_80_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_80_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_80_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_80_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_80_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_80_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
