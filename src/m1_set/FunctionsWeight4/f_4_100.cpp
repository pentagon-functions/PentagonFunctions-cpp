/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_100.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_100_abbreviated (const std::array<T,60>& abb) {
T z[81];
z[0] = abb[29] * m1_set::bc<T>[0];
z[1] = abb[30] * m1_set::bc<T>[0];
z[2] = z[0] + -z[1];
z[3] = abb[31] * m1_set::bc<T>[0];
z[4] = abb[28] * m1_set::bc<T>[0];
z[5] = z[2] + -z[3] + z[4];
z[5] = abb[14] * z[5];
z[6] = abb[32] * m1_set::bc<T>[0];
z[7] = z[4] + -z[6];
z[8] = z[2] + z[7];
z[9] = abb[8] * z[8];
z[10] = z[1] + -z[4];
z[11] = abb[5] * z[10];
z[9] = z[9] + z[11];
z[11] = abb[33] * m1_set::bc<T>[0];
z[12] = z[1] + -z[11];
z[13] = abb[40] + z[12];
z[14] = abb[7] * z[13];
z[14] = -z[5] + z[9] + -z[14];
z[15] = z[3] + -z[6];
z[16] = abb[28] * (T(1) / T(2));
z[17] = -m1_set::bc<T>[0] * z[16];
z[17] = (T(1) / T(2)) * z[11] + z[15] + z[17];
z[17] = abb[3] * z[17];
z[18] = abb[40] + abb[41];
z[19] = -z[12] + -z[15] + z[18];
z[20] = abb[6] * (T(1) / T(2));
z[19] = z[19] * z[20];
z[10] = abb[9] * z[10];
z[21] = abb[11] * z[12];
z[22] = abb[40] * (T(1) / T(2));
z[23] = abb[5] + -abb[14];
z[24] = abb[8] + z[23];
z[24] = z[22] * z[24];
z[25] = abb[2] + abb[3] + abb[8];
z[26] = -abb[41] * z[25];
z[14] = -z[10] + (T(1) / T(2)) * z[14] + z[17] + z[19] + z[21] + z[24] + z[26];
z[17] = abb[41] + -z[0] + z[6];
z[19] = abb[4] * (T(1) / T(4));
z[17] = z[17] * z[19];
z[24] = -z[6] + z[11];
z[26] = z[3] + z[24];
z[27] = -z[4] + z[26];
z[28] = -abb[41] + (T(1) / T(2)) * z[27];
z[29] = abb[12] * z[28];
z[14] = (T(1) / T(2)) * z[14] + -z[17] + -z[29];
z[14] = abb[50] * z[14];
z[5] = -z[5] + -z[9];
z[9] = abb[3] * (T(1) / T(2));
z[30] = abb[8] * (T(1) / T(2));
z[31] = abb[2] + z[9] + z[30];
z[32] = abb[41] * z[31];
z[33] = abb[14] * (T(1) / T(2));
z[34] = abb[5] * (T(1) / T(2));
z[35] = -abb[13] + z[33] + z[34];
z[36] = z[30] + -z[35];
z[36] = abb[40] * z[36];
z[5] = (T(-1) / T(2)) * z[5] + -z[21] + z[32] + -z[36];
z[24] = z[0] + z[4] + z[24];
z[22] = -z[1] + -z[22] + (T(1) / T(2)) * z[24];
z[22] = abb[1] * z[22];
z[24] = -abb[41] + z[27];
z[36] = abb[16] * (T(1) / T(4));
z[37] = z[24] * z[36];
z[22] = z[22] + -z[29] + z[37];
z[12] = -z[12] + (T(1) / T(2)) * z[15];
z[12] = z[9] * z[12];
z[1] = z[1] + z[18];
z[37] = z[1] + -z[26];
z[38] = abb[6] * (T(1) / T(4));
z[39] = z[37] * z[38];
z[40] = abb[7] * (T(1) / T(4));
z[13] = -z[13] * z[40];
z[5] = (T(-1) / T(2)) * z[5] + z[12] + z[13] + -z[17] + z[22] + z[39];
z[5] = abb[51] * z[5];
z[3] = z[3] + z[11];
z[12] = z[3] + z[4] + -3 * z[6];
z[2] = z[2] + (T(1) / T(2)) * z[12];
z[12] = z[2] * z[9];
z[8] = abb[40] + -z[8];
z[8] = z[8] * z[20];
z[13] = -abb[13] + abb[14];
z[17] = abb[40] * z[13];
z[21] = -z[17] + z[21];
z[39] = 3 * abb[3] + abb[8];
z[39] = (T(1) / T(4)) * z[39];
z[41] = -abb[41] * z[39];
z[8] = z[8] + z[12] + (T(1) / T(2)) * z[21] + z[22] + z[41];
z[8] = abb[52] * z[8];
z[12] = z[9] * z[27];
z[21] = abb[16] * (T(1) / T(2));
z[22] = z[21] * z[24];
z[12] = z[12] + z[22] + -z[32];
z[10] = -z[10] + z[12] + -z[17];
z[10] = (T(1) / T(2)) * z[10] + -z[29];
z[10] = abb[53] * z[10];
z[5] = z[5] + z[8] + z[10] + z[14];
z[8] = -z[4] + z[11] + -z[15];
z[8] = abb[41] + (T(1) / T(2)) * z[8];
z[8] = abb[50] * z[8];
z[10] = abb[51] * z[37];
z[2] = -z[2] + z[18];
z[2] = abb[52] * z[2];
z[14] = -z[4] + -z[26];
z[14] = z[1] + (T(1) / T(2)) * z[14];
z[14] = abb[53] * z[14];
z[17] = abb[54] * z[28];
z[18] = abb[52] + abb[53];
z[22] = -abb[50] + -abb[54] + -z[18];
z[22] = abb[39] * z[22];
z[2] = z[2] + z[8] + z[10] + z[14] + z[17] + (T(1) / T(2)) * z[22];
z[8] = abb[0] * (T(1) / T(16));
z[2] = z[2] * z[8];
z[10] = -abb[8] + abb[16];
z[10] = abb[54] * z[10];
z[14] = abb[50] * z[23];
z[10] = z[10] + -z[14];
z[14] = -abb[44] + abb[45] + abb[47] + -abb[49];
z[17] = abb[46] + -abb[48];
z[14] = abb[43] + (T(1) / T(2)) * z[14] + (T(1) / T(2)) * z[17];
z[17] = abb[23] * z[14];
z[22] = z[13] + z[30];
z[26] = -z[21] + z[22];
z[18] = z[18] * z[26];
z[26] = z[30] + z[35];
z[27] = -z[21] + z[26];
z[27] = abb[51] * z[27];
z[10] = (T(1) / T(2)) * z[10] + -z[17] + z[18] + z[27];
z[10] = abb[39] * z[10];
z[0] = -z[0] + z[1] + -z[7];
z[0] = z[0] * z[17];
z[7] = abb[41] + -z[15];
z[7] = abb[21] * z[7] * z[14];
z[0] = z[0] + z[7] + z[10];
z[7] = (T(-1) / T(16)) * z[12] + (T(1) / T(8)) * z[29];
z[7] = abb[54] * z[7];
z[3] = z[3] + -z[4] + -z[6];
z[3] = abb[18] * z[3];
z[6] = abb[20] * z[24];
z[10] = abb[17] + abb[18];
z[12] = -abb[41] * z[10];
z[17] = abb[17] + -abb[20];
z[17] = abb[39] * z[17];
z[3] = z[3] + z[6] + z[12] + z[17];
z[3] = abb[55] * z[3];
z[6] = z[11] + z[15];
z[1] = -z[1] + z[6];
z[11] = abb[22] + abb[24];
z[11] = (T(-1) / T(16)) * z[11];
z[1] = z[1] * z[11];
z[4] = -abb[39] + -abb[41] + -z[4] + z[6];
z[6] = abb[25] * (T(1) / T(16));
z[4] = z[4] * z[6];
z[1] = z[1] + z[4];
z[1] = z[1] * z[14];
z[0] = abb[59] + (T(1) / T(16)) * z[0] + z[1] + z[2] + (T(1) / T(32)) * z[3] + (T(1) / T(8)) * z[5] + z[7];
z[1] = abb[33] * z[14];
z[2] = abb[32] * z[14];
z[3] = abb[28] * z[14];
z[4] = -z[1] + z[2] + -z[3];
z[5] = abb[30] * z[14];
z[7] = abb[29] * z[14];
z[5] = z[4] + z[5] + -z[7];
z[5] = abb[30] * z[5];
z[11] = abb[33] * (T(1) / T(2));
z[4] = -z[4] * z[11];
z[12] = -abb[37] + abb[58];
z[15] = prod_pow(m1_set::bc<T>[0], 2);
z[17] = (T(1) / T(6)) * z[15];
z[18] = z[12] + z[17];
z[24] = prod_pow(abb[32], 2);
z[24] = (T(1) / T(2)) * z[24];
z[27] = abb[35] + abb[56] + -z[18] + z[24];
z[27] = z[14] * z[27];
z[28] = abb[29] * (T(1) / T(2));
z[29] = z[14] * z[28];
z[3] = z[3] + z[29];
z[29] = -z[2] + z[3];
z[29] = abb[29] * z[29];
z[32] = -abb[57] * z[14];
z[35] = abb[32] * (T(1) / T(2));
z[37] = z[14] * z[35];
z[41] = -abb[28] * z[37];
z[4] = z[4] + z[5] + z[27] + z[29] + z[32] + z[41];
z[4] = abb[23] * z[4];
z[27] = abb[31] * z[14];
z[3] = z[3] + -z[27];
z[29] = abb[29] * z[3];
z[37] = -z[27] + z[37];
z[41] = abb[32] * z[37];
z[29] = z[29] + z[32] + z[41];
z[32] = z[11] * z[14];
z[2] = -z[2] + z[27] + z[32];
z[2] = abb[33] * z[2];
z[42] = prod_pow(abb[31], 2);
z[43] = abb[36] + z[42];
z[18] = -z[18] + z[43];
z[44] = abb[35] + z[18];
z[44] = z[14] * z[44];
z[45] = z[14] * z[16];
z[27] = -z[27] + z[45];
z[27] = abb[28] * z[27];
z[2] = z[2] + z[5] + z[27] + z[29] + z[44];
z[2] = abb[22] * z[2];
z[5] = abb[28] * z[37];
z[18] = -abb[34] + z[18];
z[27] = abb[35] + z[18];
z[27] = z[14] * z[27];
z[37] = z[37] + z[45];
z[32] = z[32] + -z[37];
z[32] = abb[33] * z[32];
z[1] = -z[1] + z[3];
z[1] = abb[29] * z[1];
z[1] = z[1] + z[5] + z[27] + z[32] + z[41];
z[1] = abb[21] * z[1];
z[3] = z[14] * z[18];
z[18] = -abb[33] * z[37];
z[27] = abb[30] * (T(1) / T(2));
z[32] = z[14] * z[27];
z[7] = -z[7] + z[32];
z[7] = abb[30] * z[7];
z[3] = z[3] + z[5] + z[7] + z[18] + z[29];
z[3] = abb[24] * z[3];
z[5] = -abb[31] + z[35];
z[7] = z[5] + z[16];
z[18] = -abb[28] + abb[33];
z[29] = z[7] * z[18];
z[32] = (T(1) / T(3)) * z[15];
z[44] = z[32] + -z[42];
z[45] = (T(1) / T(2)) * z[44];
z[46] = abb[32] * z[5];
z[47] = z[45] + -z[46];
z[29] = abb[56] + z[29] + z[47];
z[48] = abb[58] + z[29];
z[49] = z[21] * z[48];
z[31] = abb[58] * z[31];
z[49] = z[31] + z[49];
z[50] = -abb[31] + abb[32];
z[51] = abb[33] * z[50];
z[52] = abb[28] * z[50];
z[47] = z[47] + z[51] + -z[52];
z[51] = -abb[3] * z[47];
z[53] = abb[28] + -abb[32];
z[54] = abb[28] * z[53];
z[55] = abb[33] * z[53];
z[54] = z[54] + -z[55];
z[54] = abb[8] * z[54];
z[56] = abb[17] * abb[38];
z[56] = -z[54] + z[56];
z[57] = abb[8] * abb[56];
z[51] = z[51] + (T(1) / T(2)) * z[56] + z[57];
z[56] = abb[38] * (T(1) / T(4));
z[57] = abb[20] * z[56];
z[51] = -z[49] + (T(1) / T(2)) * z[51] + z[57];
z[51] = abb[54] * z[51];
z[57] = abb[38] * (T(1) / T(2));
z[58] = -abb[26] * z[14] * z[57];
z[1] = z[1] + z[2] + z[3] + z[4] + z[51] + z[58];
z[2] = abb[36] + -abb[57];
z[3] = z[2] + z[42];
z[4] = -abb[33] + z[50];
z[4] = abb[29] * z[4];
z[51] = abb[31] * abb[32];
z[58] = abb[28] + -abb[31];
z[59] = abb[33] * z[58];
z[60] = abb[33] + z[53];
z[61] = abb[29] + -abb[30] + z[60];
z[62] = abb[30] * z[61];
z[4] = abb[34] + -z[3] + -z[4] + z[51] + -z[52] + z[59] + -z[62];
z[33] = z[4] * z[33];
z[17] = -abb[35] + z[17];
z[51] = -z[17] + z[24];
z[52] = -abb[57] + z[51];
z[59] = z[28] + z[53];
z[59] = abb[29] * z[59];
z[59] = z[59] + -z[62];
z[63] = abb[33] + -z[53];
z[63] = z[11] * z[63];
z[64] = abb[32] * (T(3) / T(2));
z[65] = abb[28] + -z[64];
z[65] = abb[28] * z[65];
z[63] = z[52] + z[59] + z[63] + z[65];
z[63] = z[30] * z[63];
z[65] = abb[29] * abb[33];
z[55] = z[55] + -z[62] + z[65];
z[55] = abb[11] * z[55];
z[66] = z[28] + z[58];
z[67] = abb[29] * z[66];
z[2] = z[2] + z[67];
z[67] = -abb[29] + z[27];
z[68] = abb[30] * z[67];
z[69] = -abb[34] + z[2] + z[68];
z[70] = prod_pow(abb[28], 2);
z[71] = (T(1) / T(2)) * z[70];
z[72] = (T(1) / T(2)) * z[42];
z[73] = z[69] + -z[71] + z[72];
z[74] = -z[34] * z[73];
z[75] = -z[13] + z[30] + -z[34];
z[75] = abb[37] * z[75];
z[76] = -z[10] * z[56];
z[26] = -abb[56] * z[26];
z[77] = abb[13] * abb[57];
z[26] = z[26] + z[31] + z[33] + z[55] + z[63] + z[74] + z[75] + z[76] + -z[77];
z[17] = z[17] + -z[46];
z[31] = -abb[31] + z[16];
z[33] = abb[28] * z[31];
z[33] = z[17] + -z[33];
z[63] = z[11] + -z[50];
z[63] = abb[33] * z[63];
z[2] = z[2] + -z[12] + -z[33] + z[42] + -z[62] + z[63];
z[38] = z[2] * z[38];
z[63] = z[28] + z[60];
z[63] = abb[29] * z[63];
z[74] = z[11] + z[53];
z[74] = abb[33] * z[74];
z[75] = -abb[32] + z[16];
z[76] = abb[28] * z[75];
z[52] = z[52] + z[76];
z[63] = z[52] + z[63] + z[74];
z[76] = abb[37] * (T(1) / T(2));
z[62] = -z[62] + z[76];
z[63] = z[62] + (T(1) / T(2)) * z[63];
z[63] = abb[1] * z[63];
z[36] = z[36] * z[48];
z[78] = abb[58] + (T(1) / T(2)) * z[47];
z[78] = abb[12] * z[78];
z[79] = abb[38] * (T(1) / T(8));
z[80] = abb[20] * z[79];
z[80] = z[78] + z[80];
z[36] = -z[36] + z[63] + z[80];
z[63] = z[27] + -z[60];
z[63] = abb[30] * z[63];
z[53] = z[16] * z[53];
z[60] = z[11] * z[60];
z[53] = abb[34] + abb[35] + z[53] + z[60] + z[63];
z[60] = abb[15] * (T(1) / T(4));
z[53] = z[53] * z[60];
z[60] = z[32] + z[42];
z[50] = abb[29] * z[50];
z[24] = -abb[36] + -abb[58] + z[24] + -z[50] + (T(-1) / T(2)) * z[60];
z[19] = z[19] * z[24];
z[24] = prod_pow(abb[33], 2);
z[24] = (T(1) / T(2)) * z[24];
z[50] = -abb[35] + -abb[57] + -z[24] + z[65] + z[68];
z[40] = z[40] * z[50];
z[50] = abb[19] * z[79];
z[19] = z[19] + z[40] + z[50] + z[53];
z[40] = -abb[33] + -z[66];
z[40] = abb[29] * z[40];
z[50] = -abb[28] * z[5];
z[53] = -abb[31] + abb[28] * (T(-3) / T(2)) + abb[32] * (T(5) / T(2)) + -z[11];
z[53] = abb[33] * z[53];
z[17] = abb[34] + z[17] + z[40] + -z[43] + z[50] + z[53];
z[17] = (T(1) / T(2)) * z[17] + -z[62];
z[17] = z[9] * z[17];
z[17] = z[17] + -z[19] + (T(1) / T(2)) * z[26] + -z[36] + z[38];
z[17] = abb[51] * z[17];
z[4] = abb[14] * z[4];
z[26] = -abb[57] + -z[51] + -z[59] + z[71] + -z[74];
z[26] = abb[8] * z[26];
z[38] = abb[5] * z[73];
z[40] = -abb[17] + abb[18] * (T(-1) / T(2));
z[40] = abb[38] * z[40];
z[4] = z[4] + z[26] + z[38] + z[40];
z[26] = abb[35] + abb[36];
z[38] = abb[34] + -z[26] + z[32] + (T(-3) / T(2)) * z[42];
z[40] = -abb[31] + abb[32] * (T(3) / T(4));
z[43] = abb[33] * (T(1) / T(4));
z[50] = abb[28] * (T(1) / T(4)) + z[40] + -z[43];
z[50] = abb[33] * z[50];
z[51] = -abb[33] + z[66];
z[53] = -z[28] * z[51];
z[40] = -abb[28] * z[40];
z[38] = (T(1) / T(2)) * z[38] + z[40] + -z[46] + z[50] + z[53] + -z[76];
z[38] = abb[3] * z[38];
z[34] = -abb[14] + z[30] + z[34];
z[34] = abb[37] * z[34];
z[40] = abb[56] * (T(1) / T(2));
z[23] = z[23] * z[40];
z[25] = abb[58] * z[25];
z[4] = (T(1) / T(2)) * z[4] + z[23] + z[25] + z[34] + z[38] + z[55];
z[23] = z[28] * z[66];
z[25] = z[27] * z[67];
z[28] = abb[36] + z[72];
z[23] = -abb[34] + z[23] + z[25] + (T(1) / T(2)) * z[28] + (T(-1) / T(4)) * z[70] + z[76];
z[23] = abb[9] * z[23];
z[3] = -abb[58] + z[3] + -z[33];
z[25] = abb[31] + abb[32];
z[25] = -abb[28] + (T(1) / T(2)) * z[25] + z[43];
z[25] = abb[33] * z[25];
z[27] = z[27] * z[61];
z[28] = -abb[33] + abb[29] * (T(1) / T(4)) + (T(1) / T(2)) * z[58];
z[28] = abb[29] * z[28];
z[3] = (T(1) / T(2)) * z[3] + z[25] + z[27] + z[28] + z[76];
z[3] = z[3] * z[20];
z[3] = z[3] + (T(1) / T(2)) * z[4] + -z[19] + -z[23] + -z[78];
z[3] = abb[50] * z[3];
z[4] = abb[56] * z[22];
z[19] = abb[14] * abb[57];
z[13] = abb[37] * z[13];
z[4] = -z[4] + -z[13] + z[19] + (T(1) / T(4)) * z[54] + -z[77];
z[13] = abb[18] + abb[17] * (T(1) / T(2));
z[19] = -z[13] * z[57];
z[19] = z[4] + z[19] + z[55];
z[22] = -z[15] + z[42];
z[25] = -abb[31] + z[64];
z[27] = z[25] * z[35];
z[22] = abb[35] + (T(1) / T(4)) * z[22] + z[27] + z[59];
z[27] = abb[31] * (T(-1) / T(2)) + -z[11] + -z[75];
z[27] = abb[33] * z[27];
z[28] = abb[31] * z[16];
z[27] = -z[22] + z[27] + z[28];
z[27] = z[9] * z[27];
z[24] = abb[37] + z[24] + z[52] + z[59];
z[20] = z[20] * z[24];
z[24] = abb[58] * z[39];
z[19] = (T(1) / T(2)) * z[19] + z[20] + z[24] + z[27] + -z[36];
z[19] = abb[52] * z[19];
z[9] = z[9] * z[47];
z[20] = -abb[17] * z[56];
z[4] = z[4] + z[9] + z[20] + z[49];
z[4] = (T(1) / T(2)) * z[4] + -z[23] + -z[80];
z[4] = abb[53] * z[4];
z[9] = abb[54] * z[78];
z[1] = (T(1) / T(2)) * z[1] + z[3] + z[4] + z[9] + z[17] + z[19];
z[2] = abb[51] * z[2];
z[3] = z[5] + -z[16];
z[3] = z[3] * z[16];
z[4] = z[5] * z[35];
z[5] = -z[12] + z[40];
z[3] = z[3] + z[4] + z[5];
z[4] = -z[32] + 3 * z[42];
z[9] = abb[33] + -z[7];
z[9] = z[9] * z[11];
z[12] = abb[29] * z[51];
z[4] = -abb[34] + z[3] + (T(1) / T(4)) * z[4] + z[9] + z[12] + z[26];
z[4] = abb[50] * z[4];
z[9] = z[31] + -z[35];
z[9] = z[9] * z[16];
z[12] = abb[33] + z[16] + -z[25];
z[12] = z[11] * z[12];
z[5] = -abb[57] + z[5] + z[9] + z[12] + z[22];
z[5] = abb[52] * z[5];
z[7] = -z[7] * z[11];
z[3] = z[3] + z[7] + (T(-1) / T(12)) * z[15] + (T(3) / T(4)) * z[42] + z[69];
z[3] = abb[53] * z[3];
z[7] = abb[58] + (T(1) / T(2)) * z[29];
z[7] = abb[54] * z[7];
z[9] = -abb[55] * z[56];
z[2] = z[2] + z[3] + z[4] + z[5] + z[7] + z[9];
z[2] = z[2] * z[8];
z[3] = abb[17] * z[16];
z[4] = abb[32] * z[13];
z[5] = abb[18] * abb[31];
z[3] = -z[3] + z[4] + -z[5];
z[3] = z[3] * z[18];
z[4] = abb[20] * z[48];
z[7] = -z[21] + -z[30];
z[7] = abb[38] * z[7];
z[8] = abb[18] * z[44];
z[9] = -abb[18] * z[35];
z[5] = z[5] + z[9];
z[5] = abb[32] * z[5];
z[9] = -abb[17] * abb[56];
z[10] = abb[58] * z[10];
z[3] = z[3] + z[4] + z[5] + z[7] + (T(1) / T(2)) * z[8] + z[9] + z[10];
z[4] = abb[27] * abb[38];
z[3] = (T(1) / T(2)) * z[3] + z[4];
z[3] = abb[55] * z[3];
z[4] = abb[56] + abb[58] + z[45];
z[4] = z[4] * z[14];
z[5] = z[18] * z[37];
z[4] = z[4] + z[5] + -z[41];
z[4] = z[4] * z[6];
z[1] = abb[42] + (T(1) / T(8)) * z[1] + z[2] + (T(1) / T(16)) * z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_100_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.06955710257811329262123070672638209599179161739081484706907015291"),stof<T>("0.40012788526738570884592048184052148541211716790954687641792762423")}, std::complex<T>{stof<T>("-0.052723199244031805204772305669985527841628211953459922470166215258"),stof<T>("0.112024350255097547706401579906632280020304470788641997581606156281")}, std::complex<T>{stof<T>("0.0427802290817057885374239026824539953270740463452897187726221166"),stof<T>("0.26794842810340937909538814468734169242255837066297445670922101667")}, std::complex<T>{stof<T>("-0.02293628033349495792116472822387146523165965363048331073310087278"),stof<T>("0.17900325458638821838408776851877993073444340166981196291345575059")}, std::complex<T>{stof<T>("0.016435191501797186144418708983212813151046729127317117556625088764"),stof<T>("-0.030373567780978282109358666898988592548664251477348641537935749809")}, std::complex<T>{stof<T>("-0.12593375930407590941161915200899992428250791545343682933411608509"),stof<T>("0.13801363516870842049040280265772312585990829028334043153424807411")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_100_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_100_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_100_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(512)) * (-v[3] + v[5]) * (-24 * v[0] + 8 * v[1] + 6 * v[3] + -6 * v[5] + m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[2] * (T(3) / T(64)) * (-v[3] + v[5])) / tend;


		return (abb[50] + abb[51] + abb[53]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[50] + abb[51] + abb[53];
z[1] = -abb[34] + abb[37];
return abb[10] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_100_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.00897161861057647797021460969078765113333079215822420734776380894"),stof<T>("0.21201960730796774152001785807014792714958978032196034346572313048")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W16(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[42] = SpDLog_f_4_100_W_16_Im(t, path, abb);
abb[59] = SpDLog_f_4_100_W_16_Re(t, path, abb);

                    
            return f_4_100_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_100_DLogXconstant_part(base_point<T>, kend);
	value += f_4_100_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_100_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_100_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_100_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_100_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_100_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_100_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
