/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_467.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_467_abbreviated (const std::array<T,69>& abb) {
T z[84];
z[0] = abb[31] * (T(1) / T(2));
z[1] = -abb[29] + z[0];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[38] * (T(1) / T(2));
z[3] = abb[37] * (T(1) / T(2));
z[4] = abb[34] * (T(1) / T(2));
z[5] = m1_set::bc<T>[0] * z[4];
z[6] = z[1] + -z[2] + -z[3] + z[5];
z[6] = abb[0] * z[6];
z[7] = abb[32] * m1_set::bc<T>[0];
z[8] = abb[40] + z[7];
z[9] = abb[34] * m1_set::bc<T>[0];
z[10] = z[8] + -z[9];
z[10] = abb[14] * z[10];
z[11] = abb[38] + abb[40];
z[12] = abb[39] + abb[41];
z[13] = abb[37] + z[12];
z[14] = z[11] + z[13];
z[15] = abb[8] * z[14];
z[16] = z[10] + -z[15];
z[7] = z[7] + -z[9] + -z[12];
z[7] = -abb[37] + (T(1) / T(2)) * z[7];
z[7] = abb[9] * z[7];
z[17] = abb[31] + -abb[32];
z[18] = m1_set::bc<T>[0] * z[17];
z[19] = -abb[39] + z[18];
z[20] = abb[13] * (T(1) / T(2));
z[19] = z[19] * z[20];
z[7] = z[7] + z[19];
z[19] = abb[41] * (T(1) / T(2));
z[21] = abb[29] + -abb[30];
z[22] = -abb[32] + -z[21];
z[22] = m1_set::bc<T>[0] * z[22];
z[22] = z[9] + z[19] + z[22];
z[22] = abb[5] * z[22];
z[23] = -abb[29] + z[17];
z[24] = m1_set::bc<T>[0] * z[23];
z[24] = z[19] + z[24];
z[25] = abb[33] * m1_set::bc<T>[0];
z[26] = z[24] + z[25];
z[26] = abb[6] * z[26];
z[27] = -z[8] + -z[9];
z[27] = z[25] + (T(1) / T(2)) * z[27];
z[27] = abb[1] * z[27];
z[28] = abb[32] * (T(1) / T(2));
z[29] = -abb[30] + z[0];
z[30] = -z[28] + -z[29];
z[30] = m1_set::bc<T>[0] * z[30];
z[31] = abb[39] * (T(1) / T(2));
z[30] = z[30] + -z[31];
z[30] = abb[2] * z[30];
z[18] = abb[40] + abb[41] + z[18];
z[18] = abb[38] + (T(1) / T(2)) * z[18];
z[18] = abb[10] * z[18];
z[32] = abb[46] * (T(1) / T(4));
z[6] = abb[45] * (T(1) / T(4)) + z[6] + -z[7] + (T(1) / T(2)) * z[16] + z[18] + z[22] + z[26] + z[27] + z[30] + z[32];
z[6] = abb[58] * z[6];
z[16] = abb[29] + abb[32];
z[22] = abb[31] * (T(3) / T(2));
z[26] = -z[16] + z[22];
z[26] = m1_set::bc<T>[0] * z[26];
z[27] = abb[40] * (T(1) / T(2));
z[30] = z[5] + z[27];
z[33] = abb[39] + -abb[41];
z[26] = z[26] + z[30] + (T(-1) / T(2)) * z[33];
z[33] = abb[53] + abb[55];
z[26] = -z[26] * z[33];
z[12] = (T(1) / T(2)) * z[12];
z[1] = z[1] + z[12] + z[30];
z[34] = -abb[51] + abb[57];
z[1] = z[1] * z[34];
z[1] = z[1] + z[26];
z[24] = z[9] + z[24];
z[24] = abb[58] * z[24];
z[1] = (T(1) / T(2)) * z[1] + z[24];
z[1] = abb[3] * z[1];
z[1] = z[1] + z[6];
z[6] = abb[30] * (T(1) / T(2));
z[24] = -abb[31] + z[6];
z[26] = abb[29] + z[24];
z[26] = m1_set::bc<T>[0] * z[26];
z[35] = (T(1) / T(2)) * z[25];
z[36] = -abb[40] + -z[13] + z[26] + -z[35];
z[36] = abb[22] * z[36];
z[37] = -z[6] + z[17];
z[38] = -abb[29] + z[37];
z[38] = m1_set::bc<T>[0] * z[38];
z[13] = z[13] + (T(3) / T(2)) * z[25] + z[38];
z[38] = abb[25] * z[13];
z[39] = -z[9] + z[35];
z[40] = -abb[29] + z[6];
z[41] = m1_set::bc<T>[0] * z[40];
z[42] = -abb[37] + z[41];
z[43] = z[39] + -z[42];
z[44] = abb[26] * z[43];
z[14] = abb[24] * z[14];
z[14] = -z[14] + z[36] + -z[38] + z[44];
z[2] = -z[2] + z[3] + z[31];
z[3] = m1_set::bc<T>[0] * z[29];
z[3] = z[2] + z[3] + z[25] + -z[30];
z[30] = abb[27] * z[3];
z[31] = (T(1) / T(2)) * z[14] + z[30];
z[36] = abb[50] * z[31];
z[38] = (T(1) / T(2)) * z[13];
z[44] = abb[23] * z[38];
z[31] = -z[31] + z[44];
z[44] = abb[47] * z[31];
z[45] = abb[42] * (T(1) / T(4));
z[46] = abb[28] * z[45];
z[31] = -z[31] + z[46];
z[31] = abb[49] * z[31];
z[31] = -z[31] + z[36] + z[44];
z[22] = -abb[30] + z[22];
z[22] = m1_set::bc<T>[0] * z[22];
z[2] = -z[2] + (T(-3) / T(2)) * z[9] + z[22] + z[25] + z[27];
z[2] = abb[17] * z[2];
z[22] = 3 * z[16];
z[36] = abb[30] * (T(5) / T(2)) + -z[22];
z[36] = m1_set::bc<T>[0] * z[36];
z[36] = abb[41] + 3 * z[9] + -z[11] + z[35] + z[36];
z[36] = abb[5] * z[36];
z[8] = z[8] + -z[25];
z[44] = abb[7] * z[8];
z[46] = abb[30] + -abb[32];
z[47] = m1_set::bc<T>[0] * z[46];
z[47] = -abb[39] + z[47];
z[48] = abb[4] * z[47];
z[44] = z[44] + z[48];
z[36] = -z[15] + z[36] + z[44];
z[38] = abb[6] * z[38];
z[48] = -abb[38] + z[35];
z[26] = z[26] + -z[48];
z[49] = abb[0] * z[26];
z[36] = (T(1) / T(2)) * z[36] + z[38] + -z[49];
z[2] = z[2] + z[36];
z[38] = -abb[30] + abb[31];
z[50] = m1_set::bc<T>[0] * (T(1) / T(2));
z[50] = abb[2] * z[38] * z[50];
z[50] = z[7] + z[50];
z[51] = abb[16] * z[43];
z[52] = (T(1) / T(4)) * z[51];
z[53] = abb[1] * (T(1) / T(2));
z[8] = z[8] * z[53];
z[2] = (T(1) / T(2)) * z[2] + -z[8] + z[32] + -z[50] + -z[52];
z[54] = (T(1) / T(4)) * z[33];
z[2] = -z[2] * z[54];
z[14] = (T(-1) / T(16)) * z[14] + (T(-1) / T(8)) * z[30];
z[14] = abb[48] * z[14];
z[3] = abb[17] * z[3];
z[30] = abb[45] * (T(-1) / T(2)) + z[3] + -z[36];
z[36] = abb[29] + (T(-1) / T(2)) * z[46];
z[36] = m1_set::bc<T>[0] * z[36];
z[12] = -z[9] + -z[12] + z[36];
z[12] = abb[9] * z[12];
z[8] = z[8] + z[12] + (T(1) / T(2)) * z[30];
z[8] = (T(1) / T(4)) * z[8] + (T(-1) / T(16)) * z[51];
z[8] = abb[51] * z[8];
z[12] = abb[30] * (T(3) / T(2)) + -z[16];
z[16] = m1_set::bc<T>[0] * z[12];
z[16] = abb[41] + z[11] + z[16] + -z[39];
z[16] = abb[5] * z[16];
z[15] = -z[15] + z[16] + -z[44];
z[16] = 3 * abb[31] + z[6] + -z[22];
z[16] = m1_set::bc<T>[0] * z[16];
z[22] = abb[37] + abb[39];
z[16] = abb[41] + z[16] + -z[22] + (T(5) / T(2)) * z[25];
z[30] = abb[6] * (T(1) / T(2));
z[16] = z[16] * z[30];
z[36] = abb[0] * z[43];
z[3] = z[3] + (T(1) / T(2)) * z[15] + z[16] + -z[36];
z[15] = abb[29] + -abb[31];
z[16] = z[15] + z[28];
z[16] = m1_set::bc<T>[0] * z[16];
z[16] = -z[16] + z[19] + z[27] + z[35];
z[16] = abb[10] * z[16];
z[19] = abb[2] * (T(1) / T(2));
z[27] = z[19] * z[47];
z[3] = (T(1) / T(2)) * z[3] + z[16] + z[27] + z[32] + z[52];
z[27] = abb[57] * (T(1) / T(4));
z[3] = z[3] * z[27];
z[28] = abb[31] * m1_set::bc<T>[0];
z[11] = -z[9] + z[11] + -z[22] + z[28];
z[11] = abb[17] * z[11];
z[10] = -z[10] + z[11];
z[11] = -z[41] + -z[48];
z[11] = abb[5] * z[11];
z[11] = -z[10] + z[11] + z[49];
z[22] = abb[19] * z[45];
z[11] = (T(1) / T(2)) * z[11] + z[16] + z[22] + z[50];
z[16] = abb[52] * (T(1) / T(4));
z[11] = z[11] * z[16];
z[7] = z[7] + (T(-1) / T(2)) * z[10] + z[18];
z[10] = abb[54] * (T(1) / T(4));
z[7] = z[7] * z[10];
z[18] = z[35] + z[42];
z[18] = abb[6] * z[18];
z[18] = z[18] + -z[36];
z[5] = -z[5] + -z[42];
z[5] = abb[9] * z[5];
z[9] = -z[9] + z[25];
z[9] = z[9] * z[53];
z[22] = -abb[18] * z[45];
z[5] = z[5] + z[9] + (T(1) / T(2)) * z[18] + z[22];
z[9] = abb[56] * (T(1) / T(4));
z[5] = z[5] * z[9];
z[18] = abb[23] * (T(1) / T(16));
z[22] = abb[48] + abb[50];
z[18] = z[18] * z[22];
z[13] = z[13] * z[18];
z[25] = z[33] + -z[34];
z[28] = abb[15] * (T(1) / T(16));
z[25] = z[25] * z[28];
z[26] = -z[25] * z[26];
z[28] = abb[18] + -abb[19];
z[32] = -abb[20] + -abb[21] + z[28];
z[32] = abb[57] * z[32];
z[28] = abb[20] + z[28];
z[35] = abb[21] + z[28];
z[35] = abb[51] * z[35];
z[32] = z[32] + z[35];
z[28] = -abb[21] + z[28];
z[35] = -z[28] * z[33];
z[36] = abb[47] + -z[22];
z[36] = abb[28] * z[36];
z[35] = -z[32] + z[35] + z[36];
z[35] = abb[42] * z[35];
z[1] = abb[65] + abb[66] + (T(1) / T(4)) * z[1] + z[2] + z[3] + z[5] + z[7] + z[8] + z[11] + z[13] + z[14] + z[26] + (T(-1) / T(8)) * z[31] + (T(1) / T(32)) * z[35];
z[2] = abb[31] * z[29];
z[3] = abb[30] * abb[32];
z[2] = z[2] + z[3];
z[5] = -abb[63] + z[2];
z[7] = abb[35] + abb[36];
z[8] = prod_pow(m1_set::bc<T>[0], 2);
z[11] = -z[7] + (T(5) / T(6)) * z[8];
z[13] = abb[33] * (T(1) / T(2));
z[14] = z[13] * z[21];
z[26] = abb[34] * z[21];
z[29] = abb[60] + abb[62];
z[31] = -abb[32] + z[6];
z[31] = abb[29] * z[31];
z[31] = z[5] + -z[11] + -z[14] + z[26] + -z[29] + z[31];
z[35] = abb[5] * (T(1) / T(4));
z[31] = z[31] * z[35];
z[36] = prod_pow(abb[32], 2);
z[39] = abb[63] + z[36];
z[41] = abb[59] + abb[61];
z[42] = abb[35] + -abb[36] + -z[39] + z[41];
z[43] = prod_pow(abb[34], 2);
z[44] = z[8] + z[43];
z[40] = z[17] + z[40];
z[45] = abb[29] * (T(1) / T(2));
z[40] = z[40] * z[45];
z[47] = -abb[34] + abb[29] * (T(3) / T(2)) + -z[6] + -z[17];
z[47] = z[13] * z[47];
z[0] = -abb[32] + z[0];
z[0] = abb[31] * z[0];
z[40] = -z[0] + z[40] + (T(1) / T(2)) * z[42] + (T(1) / T(4)) * z[44] + z[47];
z[30] = z[30] * z[40];
z[40] = z[4] + -z[21];
z[40] = abb[34] * z[40];
z[42] = abb[29] * z[38];
z[42] = -z[2] + z[42];
z[44] = z[29] + -z[41];
z[23] = abb[34] + z[23];
z[47] = abb[33] * z[23];
z[48] = z[40] + z[42] + z[44] + -z[47];
z[49] = (T(1) / T(4)) * z[48];
z[50] = abb[17] * z[49];
z[51] = z[21] * z[45];
z[52] = (T(1) / T(3)) * z[8];
z[51] = z[51] + -z[52];
z[55] = -abb[59] + z[51];
z[56] = -abb[35] + z[14];
z[26] = -z[26] + z[55] + z[56];
z[57] = (T(1) / T(4)) * z[26];
z[58] = abb[16] * z[57];
z[59] = (T(1) / T(6)) * z[8];
z[60] = (T(1) / T(2)) * z[36];
z[61] = -abb[62] + z[60];
z[62] = z[59] + -z[61];
z[63] = -abb[32] + abb[34];
z[64] = abb[33] * z[63];
z[43] = (T(1) / T(2)) * z[43];
z[64] = abb[35] + z[43] + z[62] + -z[64];
z[65] = abb[7] * z[64];
z[65] = (T(1) / T(4)) * z[65];
z[58] = z[58] + z[65];
z[17] = z[17] + -z[45];
z[66] = abb[29] * z[17];
z[67] = -z[0] + z[66];
z[68] = abb[33] * z[15];
z[69] = abb[62] + abb[63];
z[68] = z[52] + -z[67] + -z[68] + z[69];
z[70] = abb[10] * (T(1) / T(2));
z[71] = -z[68] * z[70];
z[72] = -abb[32] + z[45];
z[73] = abb[29] * z[72];
z[74] = abb[63] + z[41];
z[75] = -abb[60] + z[61] + z[73] + -z[74];
z[75] = -z[52] + (T(1) / T(4)) * z[75];
z[76] = abb[8] * z[75];
z[2] = abb[36] + z[2];
z[77] = -abb[61] + z[60];
z[78] = -z[59] + z[77];
z[79] = z[2] + -z[78];
z[80] = abb[4] * z[79];
z[81] = -abb[68] + -z[80];
z[82] = abb[0] * (T(1) / T(2));
z[83] = -z[26] * z[82];
z[19] = z[19] * z[79];
z[19] = z[19] + z[30] + z[31] + z[50] + z[58] + z[71] + -z[76] + (T(1) / T(4)) * z[81] + z[83];
z[19] = z[19] * z[27];
z[27] = abb[34] + z[37] + -z[45];
z[27] = abb[33] * z[27];
z[30] = abb[29] * z[37];
z[30] = z[30] + -z[74];
z[11] = z[11] + z[27] + -z[30] + -z[43];
z[27] = (T(1) / T(4)) * z[11];
z[31] = abb[6] * z[27];
z[37] = z[53] * z[64];
z[24] = z[24] + z[45];
z[53] = abb[29] * z[24];
z[24] = abb[33] * z[24];
z[24] = abb[36] + z[24];
z[53] = -abb[60] + -z[24] + -z[52] + z[53];
z[64] = z[53] * z[82];
z[31] = -z[31] + z[37] + -z[64] + -z[76];
z[37] = -z[45] + z[46];
z[37] = abb[29] * z[37];
z[46] = -abb[32] + z[4];
z[64] = -z[21] + z[46];
z[64] = abb[34] * z[64];
z[71] = abb[61] + abb[63];
z[74] = -z[37] + z[52] + z[64] + z[71];
z[74] = abb[9] * z[74];
z[79] = -abb[67] + z[80];
z[12] = abb[29] * z[12];
z[81] = (T(1) / T(2)) * z[8];
z[12] = z[12] + z[29] + z[81];
z[2] = abb[35] + -z[2] + -z[12] + z[39];
z[29] = -z[6] + z[45];
z[39] = z[29] + -z[46];
z[39] = abb[34] * z[39];
z[83] = abb[33] * z[21];
z[39] = z[39] + (T(1) / T(4)) * z[83];
z[2] = (T(1) / T(2)) * z[2] + -z[39];
z[83] = abb[5] * (T(1) / T(2));
z[2] = z[2] * z[83];
z[2] = z[2] + -z[31] + z[50] + z[65] + (T(1) / T(2)) * z[74] + (T(-1) / T(4)) * z[79];
z[2] = abb[51] * z[2];
z[8] = -z[8] + z[24] + z[30] + z[61];
z[8] = abb[22] * z[8];
z[24] = abb[25] * z[11];
z[8] = z[8] + -z[24];
z[24] = abb[24] * z[75];
z[30] = abb[26] * z[57];
z[8] = (T(1) / T(4)) * z[8] + z[24] + -z[30];
z[24] = abb[48] * z[8];
z[30] = abb[27] * z[49];
z[8] = z[8] + -z[30];
z[30] = abb[50] * z[8];
z[2] = z[2] + z[24] + z[30];
z[3] = abb[60] + abb[62] + z[3] + -z[41];
z[24] = abb[32] + abb[34] * (T(-3) / T(4)) + z[29];
z[24] = abb[34] * z[24];
z[23] = z[13] * z[23];
z[29] = z[38] * z[45];
z[6] = abb[32] + abb[31] * (T(-3) / T(4)) + z[6];
z[6] = abb[31] * z[6];
z[3] = (T(1) / T(2)) * z[3] + -z[6] + z[23] + z[24] + -z[29];
z[6] = abb[17] * (T(1) / T(2));
z[3] = z[3] * z[6];
z[6] = z[4] * z[46];
z[23] = z[45] * z[72];
z[6] = abb[59] + z[6] + -z[23] + (T(1) / T(2)) * z[71] + z[81];
z[6] = abb[9] * z[6];
z[23] = z[0] + -z[52] + z[77];
z[20] = z[20] * z[23];
z[20] = -z[6] + z[20];
z[13] = z[13] * z[38];
z[24] = abb[36] + -z[13] + z[29];
z[24] = abb[2] * z[24];
z[24] = -z[20] + z[24];
z[5] = abb[35] + z[5];
z[12] = abb[36] + -z[5] + -z[12] + z[36];
z[12] = (T(1) / T(2)) * z[12] + -z[39];
z[12] = z[12] * z[83];
z[29] = -abb[68] + z[80];
z[30] = abb[64] * (T(1) / T(8));
z[28] = z[28] * z[30];
z[3] = z[3] + z[12] + z[24] + z[28] + (T(-1) / T(4)) * z[29] + -z[31] + z[58];
z[3] = z[3] * z[54];
z[12] = -abb[63] + z[67];
z[28] = abb[36] + z[12] + z[43] + -z[47] + -z[59] + -z[60];
z[28] = abb[6] * z[28];
z[29] = z[21] + -z[63];
z[29] = abb[33] * z[29];
z[29] = -abb[35] + z[29] + z[40] + z[62];
z[29] = abb[1] * z[29];
z[31] = abb[34] * z[46];
z[36] = z[31] + -z[52] + z[61];
z[36] = abb[14] * z[36];
z[38] = abb[33] * z[38];
z[39] = -abb[36] + z[38] + -z[42] + -z[78];
z[39] = abb[2] * z[39];
z[23] = abb[13] * z[23];
z[23] = z[23] + z[28] + z[29] + z[36] + z[39];
z[28] = z[60] + z[64];
z[5] = z[5] + -z[28] + z[37] + -z[59];
z[5] = z[5] * z[35];
z[4] = z[4] * z[21];
z[13] = z[4] + -z[13];
z[15] = -abb[29] * z[15];
z[15] = abb[59] + abb[60] + z[7] + z[15];
z[15] = z[13] + (T(1) / T(2)) * z[15] + z[52];
z[15] = z[15] * z[82];
z[21] = -z[0] + -z[69] + z[73];
z[21] = -abb[60] + (T(1) / T(2)) * z[21] + -z[81];
z[29] = z[21] * z[70];
z[35] = abb[67] + abb[68];
z[5] = z[5] + (T(-1) / T(2)) * z[6] + z[15] + (T(1) / T(4)) * z[23] + z[29] + (T(-1) / T(8)) * z[35] + -z[76];
z[5] = abb[58] * z[5];
z[6] = abb[36] + z[69] + z[77];
z[15] = z[17] * z[45];
z[6] = abb[35] * (T(3) / T(2)) + -z[0] + (T(-1) / T(2)) * z[6] + z[13] + z[15];
z[6] = -z[6] * z[33];
z[15] = z[7] + z[61] + z[66] + -z[71];
z[13] = z[13] + (T(1) / T(2)) * z[15] + -z[52];
z[13] = z[13] * z[34];
z[7] = z[7] + z[12] + -z[28] + -z[38];
z[7] = abb[58] * z[7];
z[6] = z[6] + z[7] + z[13];
z[6] = abb[3] * z[6];
z[0] = -z[0] + z[31] + -z[44];
z[0] = abb[17] * z[0];
z[0] = z[0] + -z[36];
z[7] = -abb[10] * z[68];
z[12] = abb[36] + -abb[60] + -z[14] + z[51];
z[12] = abb[5] * z[12];
z[13] = abb[0] * z[53];
z[7] = -z[0] + z[7] + z[12] + z[13];
z[12] = abb[64] * (T(1) / T(4));
z[13] = -abb[19] * z[12];
z[7] = (T(1) / T(2)) * z[7] + z[13] + z[24];
z[7] = z[7] * z[16];
z[13] = abb[23] * z[27];
z[14] = abb[28] * z[30];
z[8] = -z[8] + z[13] + -z[14];
z[13] = abb[47] + abb[49];
z[13] = (T(1) / T(4)) * z[13];
z[8] = z[8] * z[13];
z[13] = abb[10] * z[21];
z[0] = (T(-1) / T(2)) * z[0] + z[13] + -z[20];
z[0] = z[0] * z[10];
z[10] = -z[55] + z[56];
z[10] = abb[6] * z[10];
z[13] = -abb[0] * z[26];
z[10] = z[10] + z[13];
z[13] = -z[4] + z[55];
z[13] = abb[9] * z[13];
z[4] = -z[4] + z[56];
z[4] = abb[1] * z[4];
z[12] = abb[18] * z[12];
z[4] = z[4] + (T(1) / T(2)) * z[10] + z[12] + z[13];
z[4] = z[4] * z[9];
z[9] = abb[27] * abb[48] * z[48];
z[10] = abb[16] * abb[51] * z[26];
z[9] = z[9] + z[10];
z[10] = -z[11] * z[18];
z[11] = -z[25] * z[53];
z[12] = abb[28] * z[22];
z[12] = z[12] + z[32];
z[12] = abb[64] * z[12];
z[0] = abb[43] + abb[44] + z[0] + (T(1) / T(4)) * z[2] + z[3] + z[4] + (T(1) / T(2)) * z[5] + (T(1) / T(8)) * z[6] + z[7] + z[8] + (T(-1) / T(16)) * z[9] + z[10] + z[11] + (T(1) / T(32)) * z[12] + z[19];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_467_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("-0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("-0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.0417966127428765050314821214938676836467955745016790176314509502"),stof<T>("0.27148044900177878644403449703850351511026247680080638748131402086")}, std::complex<T>{stof<T>("0.0883298241095633926946418692511844408412068741973559873146708585"),stof<T>("0.44009601786745975038172927061295343418807866948773457276764121692")}, std::complex<T>{stof<T>("0.15258225933979579846437091687265167261413254825078213605058390699"),stof<T>("0.56468626539569294860296510654471602956976011454291979071394015152")}, std::complex<T>{stof<T>("0.15258225933979579846437091687265167261413254825078213605058390699"),stof<T>("0.56468626539569294860296510654471602956976011454291979071394015152")}, std::complex<T>{stof<T>("-0.0883298241095633926946418692511844408412068741973559873146708585"),stof<T>("-0.44009601786745975038172927061295343418807866948773457276764121692")}, std::complex<T>{stof<T>("-0.0417966127428765050314821214938676836467955745016790176314509502"),stof<T>("-0.27148044900177878644403449703850351511026247680080638748131402086")}, std::complex<T>{stof<T>("-0.30516451867959159692874183374530334522826509650156427210116781398"),stof<T>("-1.12937253079138589720593021308943205913952022908583958142788030304")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_467_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_467_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(64)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(16)) * (-v[3] + v[5])) / tend;


		return (abb[52] + abb[53] + abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[52] + abb[53] + abb[55];
z[1] = abb[30] + -abb[31];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_467_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(64)) * (-v[3] + v[5]) * (-8 + -6 * v[0] + 2 * v[1] + -3 * v[3] + -v[5] + m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(-1) / T(16)) * (-v[3] + v[5])) / tend;


		return (abb[52] + abb[53] + abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + abb[31];
z[0] = (T(1) / T(2)) * z[0];
z[1] = -abb[29] + abb[33];
z[0] = z[0] * z[1];
z[0] = -abb[36] + z[0];
z[1] = abb[52] + abb[53] + abb[55];
return abb[11] * (T(1) / T(4)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_467_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(64)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(16)) * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[53] + abb[55] + abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[53] + -abb[55] + -abb[56];
z[1] = abb[33] + -abb[34];
return abb[12] * m1_set::bc<T>[0] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_467_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(64)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + -8 * v[0] + v[1] + -3 * v[2] + -3 * v[3] + 3 * v[4] + m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[53] + abb[55] + abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[33] + abb[34];
z[0] = (T(1) / T(2)) * z[0];
z[1] = -abb[29] + abb[30];
z[0] = z[0] * z[1];
z[0] = -abb[35] + z[0];
z[1] = -abb[53] + -abb[55] + -abb[56];
return abb[12] * (T(1) / T(4)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_467_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,69> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W20(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}, T{0}, T{0}};
abb[43] = SpDLog_f_4_467_W_16_Im(t, path, abb);
abb[44] = SpDLog_f_4_467_W_20_Im(t, path, abb);
abb[45] = SpDLogQ_W_84(k,dl,dlr).imag();
abb[46] = SpDLogQ_W_88(k,dl,dlr).imag();
abb[65] = SpDLog_f_4_467_W_16_Re(t, path, abb);
abb[66] = SpDLog_f_4_467_W_20_Re(t, path, abb);
abb[67] = SpDLogQ_W_84(k,dl,dlr).real();
abb[68] = SpDLogQ_W_88(k,dl,dlr).real();

                    
            return f_4_467_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_467_DLogXconstant_part(base_point<T>, kend);
	value += f_4_467_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_467_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_467_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_467_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_467_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_467_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_467_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
