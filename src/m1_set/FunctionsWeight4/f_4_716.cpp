/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_716.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_716_abbreviated (const std::array<T,39>& abb) {
T z[107];
z[0] = 7 * abb[6];
z[1] = 4 * abb[4];
z[2] = abb[0] + abb[7];
z[3] = -z[0] + -z[1] + z[2];
z[3] = abb[27] * z[3];
z[4] = 3 * abb[0];
z[5] = 2 * abb[3];
z[0] = 5 * abb[4] + -abb[7] + z[0] + -z[4] + z[5];
z[6] = 4 * abb[32];
z[0] = z[0] * z[6];
z[7] = abb[25] + abb[31];
z[8] = abb[27] + z[7];
z[9] = abb[30] + -3 * abb[32] + z[8];
z[10] = 8 * abb[9];
z[10] = z[9] * z[10];
z[11] = abb[31] * (T(1) / T(3));
z[12] = -abb[0] + abb[4];
z[13] = -38 * abb[6] + 14 * abb[7] + -17 * z[12];
z[13] = z[11] * z[13];
z[14] = 2 * abb[31];
z[15] = 2 * abb[27];
z[16] = z[14] + z[15];
z[17] = -abb[25] + -z[16];
z[17] = z[5] * z[17];
z[18] = 4 * abb[7];
z[19] = 22 * abb[0] + -25 * abb[4] + -14 * abb[6];
z[19] = -4 * abb[3] + abb[8] * (T(-8) / T(3)) + z[18] + (T(1) / T(3)) * z[19];
z[19] = abb[30] * z[19];
z[20] = 2 * abb[29];
z[21] = abb[0] + abb[7] * (T(-8) / T(3)) + abb[4] * (T(-2) / T(3)) + abb[6] * (T(1) / T(3)) + abb[8] * (T(4) / T(3)) + z[5];
z[21] = z[20] * z[21];
z[22] = 2 * abb[4];
z[23] = 9 * abb[0] + abb[6] * (T(-4) / T(3)) + abb[7] * (T(2) / T(3)) + -z[5] + z[22];
z[23] = abb[28] * z[23];
z[24] = abb[34] + abb[35];
z[25] = abb[11] * z[24];
z[26] = 4 * z[25];
z[27] = abb[10] + abb[11];
z[28] = abb[33] * z[27];
z[29] = abb[10] * z[24];
z[30] = abb[0] + abb[4];
z[31] = -12 * abb[6] + abb[7] * (T(22) / T(3)) + (T(-23) / T(3)) * z[30];
z[31] = abb[25] * z[31];
z[32] = abb[30] + -z[7];
z[32] = abb[1] * z[32];
z[11] = abb[25] + abb[29] * (T(-4) / T(3)) + abb[28] * (T(-1) / T(3)) + z[11];
z[11] = abb[2] * z[11];
z[33] = -abb[0] + -abb[1];
z[33] = abb[26] * z[33];
z[0] = z[0] + (T(4) / T(3)) * z[3] + z[10] + z[11] + z[13] + z[17] + z[19] + z[21] + z[23] + z[26] + 2 * z[28] + (T(16) / T(3)) * z[29] + z[31] + (T(13) / T(3)) * z[32] + (T(26) / T(3)) * z[33];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[3] = 3 * abb[7];
z[11] = 4 * abb[8];
z[13] = z[3] + -z[11];
z[17] = abb[0] + abb[3];
z[19] = 3 * abb[4];
z[21] = 2 * abb[6];
z[23] = z[13] + z[17] + -z[19] + -z[21];
z[23] = abb[28] * z[23];
z[31] = 2 * abb[8];
z[32] = 7 * abb[7] + -z[31];
z[33] = -abb[4] + z[5];
z[34] = 3 * abb[6];
z[35] = -z[4] + -z[32] + -z[33] + -z[34];
z[35] = z[6] * z[35];
z[36] = 4 * abb[6];
z[37] = z[5] + z[36];
z[32] = 8 * abb[0] + -z[19] + z[32] + z[37];
z[32] = abb[30] * z[32];
z[38] = abb[4] + z[21];
z[39] = -z[5] + z[38];
z[40] = 2 * abb[0];
z[41] = z[13] + -z[39] + z[40];
z[41] = z[20] * z[41];
z[42] = abb[28] + z[20];
z[43] = -z[7] + z[42];
z[43] = abb[2] * z[43];
z[44] = abb[27] + -abb[31];
z[45] = abb[25] + z[44];
z[45] = abb[5] * z[45];
z[46] = z[43] + z[45];
z[46] = 2 * z[46];
z[47] = 8 * abb[7];
z[48] = -z[11] + z[47];
z[49] = -abb[0] + z[1];
z[50] = z[48] + -z[49];
z[51] = 8 * abb[6];
z[52] = z[50] + z[51];
z[52] = abb[25] * z[52];
z[53] = 6 * abb[7];
z[54] = z[21] + z[53];
z[55] = -abb[4] + z[54];
z[55] = z[14] * z[55];
z[56] = -abb[4] + abb[8];
z[57] = z[34] + -z[56];
z[58] = abb[0] + z[3] + z[57];
z[58] = z[15] * z[58];
z[59] = 3 * abb[25];
z[60] = z[16] + z[59];
z[60] = abb[3] * z[60];
z[61] = abb[11] * abb[33];
z[23] = -z[10] + z[23] + -2 * z[25] + z[32] + z[35] + z[41] + z[46] + z[52] + z[55] + z[58] + z[60] + z[61];
z[23] = abb[15] * z[23];
z[32] = 5 * abb[0];
z[35] = z[32] + -z[51];
z[1] = -8 * abb[8] + z[1] + -z[35] + z[47];
z[1] = abb[25] * z[1];
z[41] = -3 * z[30] + z[36] + z[48];
z[41] = z[14] * z[41];
z[48] = -z[12] + z[31] + -z[36];
z[52] = 2 * abb[7];
z[55] = -z[48] + z[52];
z[55] = z[15] * z[55];
z[58] = 3 * abb[3];
z[60] = z[4] + z[58];
z[62] = -z[11] + -z[22] + z[60];
z[62] = abb[28] * z[62];
z[63] = abb[12] * z[24];
z[64] = 4 * z[63];
z[65] = 2 * z[29];
z[66] = z[64] + z[65];
z[67] = z[61] + z[66];
z[68] = abb[27] + abb[31];
z[69] = -z[59] + -4 * z[68];
z[69] = abb[3] * z[69];
z[70] = abb[7] + z[40];
z[71] = -z[56] + z[70];
z[72] = 4 * abb[30];
z[71] = z[71] * z[72];
z[73] = abb[8] + z[52];
z[58] = abb[0] + z[58] + -z[73];
z[74] = 4 * abb[29];
z[58] = z[58] * z[74];
z[75] = 16 * abb[32];
z[76] = abb[6] + abb[7];
z[77] = abb[8] + -z[76];
z[77] = z[75] * z[77];
z[1] = z[1] + z[26] + z[41] + z[55] + z[58] + z[62] + z[67] + z[69] + z[71] + z[77];
z[1] = abb[14] * z[1];
z[26] = z[17] + z[22];
z[26] = abb[28] * z[26];
z[41] = abb[3] * abb[25];
z[26] = z[26] + -z[41] + -z[61];
z[41] = -abb[8] + z[52];
z[55] = -abb[3] + z[41];
z[58] = -abb[4] + z[40];
z[61] = z[55] + z[58];
z[61] = z[61] * z[72];
z[55] = abb[0] + z[55];
z[55] = z[55] * z[74];
z[55] = z[26] + -z[55] + z[61];
z[61] = -z[12] + z[18];
z[62] = -z[31] + z[61];
z[62] = -z[16] * z[62];
z[50] = -abb[25] * z[50];
z[69] = -z[12] + z[41];
z[71] = 8 * abb[32];
z[77] = z[69] * z[71];
z[50] = z[50] + -z[55] + z[62] + -z[66] + z[77];
z[50] = abb[18] * z[50];
z[62] = abb[3] * z[8];
z[62] = -z[25] + z[62];
z[66] = -abb[3] + z[56];
z[77] = abb[30] * z[66];
z[45] = -z[45] + z[62] + z[77];
z[77] = abb[3] + -abb[8];
z[78] = z[42] * z[77];
z[78] = z[43] + -z[45] + z[78];
z[79] = z[12] + z[21];
z[80] = -abb[8] + z[79];
z[81] = 2 * abb[32];
z[82] = z[80] * z[81];
z[83] = abb[27] * z[80];
z[82] = z[82] + -z[83];
z[84] = -abb[8] + z[21];
z[85] = abb[0] + -z[22] + -z[84];
z[85] = abb[25] * z[85];
z[86] = -abb[0] + z[21];
z[87] = -abb[8] + -z[86];
z[87] = abb[31] * z[87];
z[85] = -z[78] + z[82] + z[85] + z[87];
z[85] = abb[17] * z[85];
z[1] = z[1] + z[23] + z[50] + 4 * z[85];
z[1] = abb[15] * z[1];
z[23] = abb[3] * z[68];
z[50] = -abb[6] + z[40];
z[50] = abb[28] * z[50];
z[23] = -z[23] + z[25] + z[28] + 2 * z[50];
z[28] = -z[11] + z[36] + -z[58];
z[28] = abb[31] * z[28];
z[50] = -abb[8] + z[40];
z[85] = abb[6] + z[50];
z[85] = abb[25] * z[85];
z[85] = z[63] + z[85];
z[87] = 2 * abb[25];
z[88] = -z[44] + z[87];
z[88] = abb[5] * z[88];
z[89] = z[22] + z[77];
z[89] = abb[30] * z[89];
z[90] = abb[6] + -z[33] + -z[50];
z[90] = z[20] * z[90];
z[91] = -z[6] * z[80];
z[92] = abb[30] + -abb[31];
z[93] = abb[1] * z[92];
z[94] = abb[27] * z[84];
z[95] = 4 * abb[0];
z[96] = abb[1] + -3 * abb[5] + z[95];
z[96] = abb[26] * z[96];
z[28] = -z[23] + z[28] + -z[29] + 2 * z[85] + z[88] + z[89] + z[90] + z[91] + -z[93] + z[94] + z[96];
z[28] = abb[14] * z[28];
z[7] = z[7] * z[80];
z[85] = z[29] + -z[63];
z[7] = z[7] + -z[82] + -z[85];
z[7] = abb[17] * z[7];
z[28] = -2 * z[7] + z[28];
z[28] = abb[14] * z[28];
z[82] = z[5] + z[52];
z[88] = z[22] + z[34];
z[89] = -abb[0] + z[31];
z[90] = -z[82] + z[88] + z[89];
z[90] = abb[29] * z[90];
z[91] = z[30] + z[34];
z[94] = z[3] + z[5];
z[96] = z[91] + z[94];
z[97] = z[81] * z[96];
z[98] = abb[7] + -z[38] + z[77];
z[98] = abb[28] * z[98];
z[99] = abb[3] + z[2] + z[88];
z[99] = abb[30] * z[99];
z[100] = abb[8] + z[2];
z[101] = z[38] + z[100];
z[101] = abb[27] * z[101];
z[102] = z[22] + z[36];
z[103] = z[2] + z[102];
z[103] = abb[25] * z[103];
z[104] = z[62] + -z[63];
z[105] = abb[31] * z[76];
z[9] = abb[9] * z[9];
z[106] = 4 * z[9];
z[43] = z[43] + -z[90] + -z[97] + z[98] + z[99] + z[101] + z[103] + z[104] + 3 * z[105] + -z[106];
z[90] = abb[36] * z[43];
z[97] = -abb[4] + z[31];
z[98] = abb[31] * z[97];
z[99] = abb[4] * abb[25];
z[78] = z[78] + z[85] + z[98] + z[99];
z[78] = abb[20] * z[78];
z[78] = z[78] + z[90];
z[85] = 3 * abb[8];
z[90] = -abb[0] + z[19] + z[51] + z[52] + -z[85];
z[90] = abb[25] * z[90];
z[30] = -6 * abb[6] + -z[5] + -z[30] + -z[53] + z[85];
z[30] = z[30] * z[81];
z[82] = -z[79] + z[82] + -z[85];
z[42] = z[42] * z[82];
z[82] = -abb[0] + z[36] + z[53] + z[56];
z[82] = abb[31] * z[82];
z[99] = z[2] + z[21] + -z[66];
z[101] = 2 * abb[30];
z[99] = z[99] * z[101];
z[57] = abb[7] + z[57];
z[57] = z[15] * z[57];
z[30] = z[30] + z[42] + z[46] + z[57] + z[82] + z[90] + z[99] + -z[106];
z[30] = abb[21] * z[30];
z[42] = abb[8] + z[88];
z[46] = abb[27] * z[42];
z[46] = -z[29] + z[46] + z[104];
z[57] = z[18] + z[86];
z[57] = abb[31] * z[57];
z[82] = -abb[0] + z[102];
z[82] = abb[25] * z[82];
z[57] = z[46] + z[57] + z[82];
z[13] = z[13] + z[95];
z[82] = z[13] + -z[39];
z[82] = z[20] * z[82];
z[86] = z[6] * z[96];
z[88] = -z[31] + -z[39] + z[70];
z[90] = abb[28] * z[88];
z[96] = -abb[4] + abb[7] + -z[11] + -z[37];
z[96] = abb[30] * z[96];
z[57] = z[10] + -2 * z[57] + -z[82] + z[86] + -z[90] + z[96];
z[82] = abb[37] * z[57];
z[96] = z[38] + -z[40];
z[96] = abb[25] * z[96];
z[99] = abb[3] + -abb[4] + -abb[6];
z[99] = z[20] * z[99];
z[103] = abb[3] + -z[79];
z[103] = abb[28] * z[103];
z[104] = -abb[28] + abb[31] + z[20] + -z[59];
z[104] = abb[2] * z[104];
z[45] = -z[45] + z[83] + z[96] + z[98] + z[99] + z[103] + z[104];
z[45] = prod_pow(abb[17], 2) * z[45];
z[28] = z[28] + z[30] + z[45] + 2 * z[78] + z[82];
z[30] = z[16] * z[48];
z[45] = z[11] + -z[49] + -z[51];
z[45] = abb[25] * z[45];
z[48] = abb[0] + z[77];
z[49] = z[48] * z[74];
z[66] = z[66] * z[72];
z[78] = z[71] * z[80];
z[26] = z[26] + z[30] + z[45] + z[49] + -z[64] + z[65] + z[66] + z[78];
z[26] = abb[14] * z[26];
z[30] = abb[25] + -abb[28];
z[30] = z[12] * z[30];
z[45] = abb[10] * abb[33];
z[30] = z[30] + z[45];
z[30] = abb[18] * z[30];
z[26] = 4 * z[7] + z[26] + 2 * z[30];
z[26] = abb[18] * z[26];
z[30] = abb[5] * z[44];
z[45] = abb[1] + abb[5];
z[45] = abb[26] * z[45];
z[30] = -z[30] + -z[45] + z[63] + z[93];
z[64] = z[40] + -z[97];
z[64] = abb[31] * z[64];
z[58] = abb[25] * z[58];
z[66] = -abb[27] * z[12];
z[77] = abb[30] * z[77];
z[48] = -abb[28] * z[48];
z[48] = z[30] + z[48] + -z[49] + z[58] + z[62] + z[64] + z[66] + z[77];
z[48] = abb[19] * z[48];
z[49] = -z[36] + -z[52] + z[56];
z[49] = abb[25] * z[49];
z[53] = abb[4] + -z[53] + -z[84];
z[53] = abb[31] * z[53];
z[58] = -z[52] + -z[91];
z[58] = abb[27] * z[58];
z[49] = z[49] + z[53] + z[58] + -z[62];
z[53] = z[20] * z[88];
z[53] = -z[10] + z[53];
z[58] = -abb[4] + -z[36] + -z[94] + -z[95];
z[58] = abb[30] * z[58];
z[50] = 5 * abb[7] + z[5] + z[34] + z[50];
z[50] = z[6] * z[50];
z[49] = 2 * z[49] + z[50] + -z[53] + z[58] + -z[90];
z[49] = abb[15] * z[49];
z[2] = -abb[8] + z[2];
z[50] = z[2] * z[20];
z[50] = -z[29] + z[50];
z[58] = -abb[25] + z[81];
z[58] = z[58] * z[69];
z[61] = -z[61] + z[85];
z[61] = abb[31] * z[61];
z[64] = -abb[7] + z[12];
z[66] = abb[27] * z[64];
z[70] = abb[8] + -z[70];
z[70] = abb[30] * z[70];
z[30] = -z[30] + z[50] + z[58] + z[61] + z[66] + z[70];
z[30] = abb[14] * z[30];
z[58] = z[8] + -z[81];
z[58] = z[58] * z[69];
z[2] = z[2] * z[101];
z[2] = z[2] + -z[50] + z[58] + z[63];
z[2] = abb[18] * z[2];
z[2] = z[2] + z[30];
z[2] = 2 * z[2] + z[49];
z[30] = abb[0] + z[102];
z[30] = abb[25] * z[30];
z[49] = z[54] + -z[89];
z[49] = abb[31] * z[49];
z[34] = abb[4] + z[34] + z[100];
z[34] = abb[27] * z[34];
z[44] = -z[44] + -z[87];
z[44] = abb[5] * z[44];
z[50] = z[87] + z[92];
z[50] = abb[1] * z[50];
z[30] = z[30] + z[34] + z[44] + z[45] + z[49] + z[50] + z[62];
z[34] = abb[7] + z[39];
z[39] = abb[28] * z[34];
z[37] = z[19] + z[37];
z[44] = -abb[7] + z[31] + z[37];
z[44] = abb[30] * z[44];
z[34] = -z[20] * z[34];
z[10] = -z[10] + 2 * z[30] + z[34] + -z[39] + z[44] + -z[86];
z[10] = abb[16] * z[10];
z[2] = 2 * z[2] + z[10];
z[2] = abb[16] * z[2];
z[10] = z[12] * z[24];
z[8] = abb[10] * z[8];
z[8] = -z[8] + z[10];
z[10] = -4 * abb[13] + z[22] + z[60];
z[10] = abb[33] * z[10];
z[22] = abb[12] + z[27];
z[22] = z[22] * z[72];
z[24] = abb[11] + abb[12];
z[24] = z[24] * z[74];
z[27] = 3 * abb[28] + -z[59];
z[27] = abb[11] * z[27];
z[8] = 2 * z[8] + -z[10] + z[22] + -z[24] + z[27];
z[10] = -abb[38] * z[8];
z[0] = z[0] + z[1] + z[2] + z[10] + z[26] + 2 * z[28] + 4 * z[48];
z[0] = 2 * z[0];
z[1] = -z[4] + -z[21] + z[52] + z[56];
z[1] = abb[25] * z[1];
z[2] = z[4] + z[18];
z[10] = abb[8] + z[2] + -z[102];
z[10] = abb[31] * z[10];
z[21] = z[5] + -z[12] + -z[76];
z[20] = z[20] * z[21];
z[21] = -abb[1] + abb[5] + -z[40];
z[21] = abb[26] * z[21];
z[21] = z[21] + z[93];
z[22] = abb[7] + -z[80];
z[22] = abb[27] * z[22];
z[24] = 3 * z[12] + z[36];
z[26] = z[24] + -z[73];
z[26] = z[26] * z[81];
z[27] = abb[5] * z[87];
z[12] = -abb[3] + abb[7] + -2 * z[12];
z[12] = abb[30] * z[12];
z[1] = z[1] + z[10] + z[12] + z[20] + 2 * z[21] + z[22] + z[23] + z[26] + -z[27] + -z[63] + z[65];
z[1] = abb[14] * z[1];
z[1] = z[1] + z[7];
z[7] = z[11] + z[32] + z[51];
z[7] = abb[25] * z[7];
z[10] = z[38] + z[52];
z[4] = z[4] + z[10] + z[31];
z[4] = z[4] * z[15];
z[11] = abb[7] + z[31] + z[33] + z[36];
z[11] = z[11] * z[101];
z[12] = z[17] + -z[36] + z[52];
z[12] = abb[28] * z[12];
z[2] = abb[4] + z[2] + z[31];
z[2] = z[2] * z[14];
z[3] = -abb[3] + -abb[8] + z[3] + -z[79];
z[3] = z[3] * z[74];
z[14] = -abb[6] + -abb[8] + -z[40] + -z[94];
z[14] = z[14] * z[71];
z[15] = 7 * abb[25] + 8 * z[68];
z[15] = abb[3] * z[15];
z[2] = z[2] + z[3] + z[4] + z[7] + -16 * z[9] + z[11] + z[12] + z[14] + z[15] + -8 * z[25] + -z[67];
z[2] = abb[15] * z[2];
z[3] = -z[19] + -z[36] + z[41];
z[3] = abb[25] * z[3];
z[4] = -abb[8] + -z[10];
z[4] = abb[31] * z[4];
z[3] = z[3] + z[4] + z[27] + -z[46];
z[4] = z[13] + -z[37];
z[4] = abb[30] * z[4];
z[7] = abb[1] * abb[25];
z[7] = z[7] + z[45];
z[5] = abb[7] + z[5] + z[42];
z[5] = z[5] * z[6];
z[3] = 2 * z[3] + z[4] + z[5] + -4 * z[7] + z[39] + -z[53];
z[3] = abb[16] * z[3];
z[4] = -z[18] + z[24];
z[4] = z[4] * z[16];
z[5] = 8 * abb[4] + -z[35] + -z[47];
z[5] = abb[25] * z[5];
z[6] = -abb[6] + -z[64];
z[6] = z[6] * z[75];
z[4] = z[4] + z[5] + z[6] + -6 * z[29] + -z[55];
z[4] = abb[18] * z[4];
z[1] = 4 * z[1] + z[2] + 2 * z[3] + z[4];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[22] * z[43];
z[3] = abb[23] * z[57];
z[4] = -abb[24] * z[8];
z[1] = z[1] + 4 * z[2] + 2 * z[3] + z[4];
z[1] = 2 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_716_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("184.59701950267346798585441479195217662138146356808560951682975081"),stof<T>("239.80092275748920226503509253047515224208029854097392904410292705")}, std::complex<T>{stof<T>("-61.04356881355948280729575040891834460164172653029667444672396576"),stof<T>("310.8643371952468942889795055873055137324787101789556224207677071")}, std::complex<T>{stof<T>("113.81402412622069756930498277975278170901715590597345613481363758"),stof<T>("-100.7273826369310837619078755930088399820812678101708229714828605")}, std::complex<T>{stof<T>("-159.71848989214608423451723097730599760752650603094143754619738365"),stof<T>("-239.01219824878978828755513445965119124602596697077952819499725381")}, std::complex<T>{stof<T>("-55.981081322799019208639005474829241127248003856658516087449246882"),stof<T>("-25.620487089528904825109455965593657013001794545422230398443896226")}, std::complex<T>{stof<T>("4.56745670295813050481405847363743422194985809879158646086070954"),stof<T>("-415.51625574132423572612278288948722963328824407563807625133898568")}, std::complex<T>{stof<T>("68.13470547093420845059975259249174738344504617477185084804584856"),stof<T>("-139.22660244732493584220738936091720009443504075646343359147592095")}, std::complex<T>{stof<T>("-100.60661801771934845868193049851908584354998543391942344277196114"),stof<T>("548.98522805482844115327074434794131287337655971971950939978923857")}, std::complex<T>{stof<T>("37.237750483933568717930266059851400269885744102587696183423612107"),stof<T>("-54.318795126461373211707472408357397756266512262009501857913420847")}, std::complex<T>{stof<T>("115.62569373563779910527655210486707388642848124061625145027161581"),stof<T>("-108.11066495425534745776941889956021115989000493009439046011185339")}, std::complex<T>{stof<T>("115.62569373563779910527655210486707388642848124061625145027161581"),stof<T>("-108.11066495425534745776941889956021115989000493009439046011185339")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[66].real()/kbase.W[66].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_716_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_716_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("602.11065999088512433460142870338496112072546632715760040760124999"),stof<T>("-432.81553595718476015486600337203220390331614475871995493803040458")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,39> abb = {dl[0], dl[1], dl[5], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W67(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_2_3(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[66].real()/k.W[66].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), f_2_4_re(k), f_2_6_re(k), f_2_24_re(k)};

                    
            return f_4_716_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_716_DLogXconstant_part(base_point<T>, kend);
	value += f_4_716_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_716_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_716_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_716_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_716_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_716_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_716_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
