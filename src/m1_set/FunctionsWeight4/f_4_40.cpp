/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_40.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_40_abbreviated (const std::array<T,16>& abb) {
T z[15];
z[0] = 2 * abb[11];
z[1] = -abb[12] + abb[13] + z[0];
z[2] = abb[5] * z[1];
z[3] = -abb[11] + abb[14];
z[3] = abb[0] * z[3];
z[4] = -abb[12] + abb[13] * (T(-7) / T(6)) + abb[11] * (T(-1) / T(6));
z[4] = abb[3] * z[4];
z[5] = abb[11] + abb[13] * (T(1) / T(2));
z[6] = abb[14] * (T(1) / T(2)) + -z[5];
z[6] = abb[1] * z[6];
z[4] = -z[2] + (T(-13) / T(6)) * z[3] + z[4] + (T(1) / T(3)) * z[6];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[6] = -abb[11] + abb[12];
z[7] = abb[4] * z[6];
z[5] = abb[12] * (T(-1) / T(2)) + z[5];
z[5] = abb[2] * z[5];
z[5] = z[5] + z[7];
z[8] = prod_pow(abb[8], 2);
z[9] = prod_pow(abb[7], 2);
z[10] = z[8] + -z[9];
z[5] = z[5] * z[10];
z[10] = 2 * abb[1];
z[0] = abb[13] + z[0];
z[11] = -abb[14] + z[0];
z[11] = z[10] * z[11];
z[0] = -abb[12] + z[0];
z[12] = abb[3] * z[0];
z[11] = -z[2] + z[11] + z[12];
z[11] = abb[6] * z[11];
z[12] = -abb[7] + abb[8];
z[1] = z[1] * z[12];
z[10] = -abb[2] + abb[3] + z[10];
z[13] = z[1] * z[10];
z[11] = z[11] + z[13];
z[11] = abb[6] * z[11];
z[13] = -abb[1] + abb[2];
z[13] = z[0] * z[13];
z[14] = abb[3] + abb[4];
z[6] = z[6] * z[14];
z[6] = z[6] + z[13];
z[6] = abb[9] * z[6];
z[13] = z[2] + 2 * z[3];
z[13] = z[9] * z[13];
z[8] = z[8] + z[9];
z[9] = abb[12] + abb[13];
z[9] = abb[3] * z[9];
z[8] = z[8] * z[9];
z[0] = z[0] * z[10];
z[10] = -abb[15] * z[0];
z[4] = z[4] + z[5] + z[6] + (T(1) / T(2)) * z[8] + z[10] + z[11] + z[13];
z[5] = -z[7] * z[12];
z[2] = -abb[7] * z[2];
z[2] = z[2] + z[5];
z[5] = abb[7] + abb[8];
z[5] = -z[5] * z[9];
z[1] = -abb[2] * z[1];
z[3] = abb[7] * z[3];
z[1] = z[1] + 2 * z[2] + -4 * z[3] + z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[0] = -abb[10] * z[0];
z[0] = z[0] + z[1];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_40_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("3.6829813829162597425126257087210567251836075161072208182775494951"),stof<T>("-11.9791235274951074822690799539702078075941909156745399809233309644")}, std::complex<T>{stof<T>("-1.8851066797865090121045382553432042033443533330780297410706893654"),stof<T>("-4.2652683322429439786853419176585110096129822091780198741212434611")}, std::complex<T>{stof<T>("-19.892863152671605965999716894711289190566145705205083238003708664"),stof<T>("2.010688063418513109653840556751340815251191929326263598038320412")}, std::complex<T>{stof<T>("-21.690737855801356696407804348089141712405399888234274315210568794"),stof<T>("18.255079923156564570608262428380059632458365054178823453082894837")}};
	
	std::vector<C> intdlogs = {rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[26].real()/kbase.W[26].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_40_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_40_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-5.4964366298235505513183220296284724901068060406509121251377469976"),stof<T>("11.3816813901908646538128467122095886100821096564933159719895461214")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,16> abb = {dl[1], dl[3], dlog_W10(k,dl), dl[4], dlog_W21(k,dl), dlog_W27(k,dl), f_1_2(k), f_1_3(k), f_1_6(k), f_2_5(k), f_2_6_im(k), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[26].real()/k.W[26].real()), f_2_6_re(k)};

                    
            return f_4_40_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_40_DLogXconstant_part(base_point<T>, kend);
	value += f_4_40_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_40_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_40_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_40_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_40_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_40_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_40_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
