/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_207.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_207_abbreviated (const std::array<T,9>& abb) {
T z[8];
z[0] = abb[1] + abb[2];
z[1] = abb[6] + abb[7];
z[0] = z[0] * z[1];
z[2] = abb[8] * z[0];
z[3] = 4 * abb[2] + abb[1] * (T(-5) / T(2));
z[3] = z[1] * z[3];
z[4] = -abb[6] + abb[7];
z[4] = abb[0] * z[4];
z[3] = z[3] + (T(13) / T(2)) * z[4];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[5] = -abb[1] + 3 * abb[2];
z[1] = z[1] * z[5];
z[5] = (T(-1) / T(2)) * z[1] + -2 * z[4];
z[5] = prod_pow(abb[4], 2) * z[5];
z[6] = abb[3] * z[0];
z[7] = abb[4] + abb[3] * (T(1) / T(2));
z[7] = z[6] * z[7];
z[2] = z[2] + (T(1) / T(3)) * z[3] + z[5] + z[7];
z[1] = z[1] + 4 * z[4];
z[1] = abb[4] * z[1];
z[1] = z[1] + -z[6];
z[1] = m1_set::bc<T>[0] * z[1];
z[0] = abb[5] * z[0];
z[0] = z[0] + z[1];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_207_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.783380739531793706202640249219795159768385958230961139356235713"),stof<T>("18.689655014825286707415423873817743885363767154876205942963075529")}, std::complex<T>{stof<T>("33.17924610273263426341173016377833664797775631866535066591290487"),stof<T>("-17.820504831487842433801100982942375379552962953481440963202714146")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[25].real()/kbase.W[25].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_207_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_207_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("5.908138768689258773260419648517811704182935976389948343781602254"),stof<T>("-17.795923471226649136110521121229075300075712274980973961296511351")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,9> abb = {dlog_W3(k,dl), dlog_W7(k,dl), dlog_W26(k,dl), f_1_8(k), f_1_10(k), f_2_11_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[25].real()/k.W[25].real()), f_2_11_re(k)};

                    
            return f_4_207_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_207_DLogXconstant_part(base_point<T>, kend);
	value += f_4_207_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_207_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_207_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_207_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_207_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_207_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_207_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
