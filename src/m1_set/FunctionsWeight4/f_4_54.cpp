/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_54.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_54_abbreviated (const std::array<T,55>& abb) {
T z[82];
z[0] = -abb[43] + abb[44];
z[1] = abb[41] + z[0];
z[2] = abb[32] * z[1];
z[3] = -abb[30] * z[0];
z[4] = z[2] + -z[3];
z[5] = abb[27] * z[0];
z[6] = abb[31] * z[0];
z[7] = abb[27] + -abb[30];
z[8] = abb[31] + z[7];
z[8] = abb[41] * z[8];
z[9] = -abb[32] + z[7];
z[10] = abb[31] + z[9];
z[11] = -abb[42] * z[10];
z[8] = -z[4] + z[5] + z[6] + z[8] + z[11];
z[8] = abb[24] * z[8];
z[11] = -abb[30] + abb[31];
z[12] = abb[41] * z[11];
z[12] = z[3] + z[12];
z[13] = abb[29] * z[1];
z[14] = z[6] + z[13];
z[15] = z[12] + z[14];
z[16] = -abb[29] + abb[32];
z[17] = -z[11] + z[16];
z[18] = abb[42] * z[17];
z[2] = -z[2] + z[15] + z[18];
z[18] = -abb[21] + -abb[23];
z[2] = z[2] * z[18];
z[18] = -abb[27] + abb[31];
z[19] = abb[41] * z[18];
z[19] = -z[5] + z[19];
z[14] = z[14] + z[19];
z[20] = abb[28] * z[1];
z[21] = abb[29] + z[18];
z[22] = -abb[28] + z[21];
z[23] = abb[42] * z[22];
z[20] = -z[14] + z[20] + z[23];
z[20] = abb[22] * z[20];
z[23] = abb[42] * z[11];
z[12] = -z[6] + -z[12] + z[23];
z[12] = abb[20] * z[12];
z[23] = abb[17] * abb[51];
z[24] = abb[19] * abb[51];
z[25] = -z[23] + -z[24];
z[25] = z[10] * z[25];
z[26] = abb[49] + -abb[50];
z[27] = -z[10] * z[26];
z[28] = abb[0] + -abb[14];
z[29] = -z[27] * z[28];
z[30] = -abb[45] + abb[47];
z[30] = abb[7] * z[30];
z[31] = z[16] * z[30];
z[2] = z[2] + z[8] + z[12] + z[20] + z[25] + z[29] + z[31];
z[8] = -abb[29] + z[7];
z[12] = abb[28] + z[8];
z[20] = abb[12] * (T(1) / T(2));
z[12] = z[12] * z[20];
z[25] = abb[14] * (T(1) / T(2));
z[29] = z[10] * z[25];
z[31] = abb[6] * (T(1) / T(2));
z[32] = z[17] * z[31];
z[33] = abb[8] * (T(1) / T(2));
z[34] = z[22] * z[33];
z[35] = abb[0] * z[10];
z[12] = z[12] + z[29] + z[32] + -z[34] + -z[35];
z[29] = abb[28] + -abb[31];
z[32] = abb[4] * (T(1) / T(2));
z[29] = z[29] * z[32];
z[34] = abb[5] * (T(1) / T(2));
z[36] = abb[9] + -z[34];
z[37] = -abb[27] + abb[29];
z[36] = z[36] * z[37];
z[38] = abb[10] * z[16];
z[39] = abb[11] * z[10];
z[38] = z[38] + -z[39];
z[39] = (T(-1) / T(2)) * z[11] + z[16];
z[39] = abb[3] * z[39];
z[36] = -z[12] + z[29] + z[36] + -z[38] + z[39];
z[36] = abb[47] * z[36];
z[39] = -z[34] * z[37];
z[22] = abb[15] * z[22];
z[12] = -z[12] + -z[22] + -z[29] + z[39];
z[12] = abb[45] * z[12];
z[29] = abb[14] * z[10];
z[29] = z[29] + -z[35];
z[29] = (T(1) / T(2)) * z[29] + z[38];
z[35] = abb[3] * (T(1) / T(2));
z[10] = -z[10] * z[35];
z[10] = z[10] + -z[29];
z[10] = abb[46] * z[10];
z[38] = abb[32] * (T(1) / T(2));
z[39] = -abb[29] + z[38];
z[40] = abb[27] + abb[30];
z[41] = -3 * abb[31] + z[40];
z[41] = abb[28] + z[39] + (T(1) / T(2)) * z[41];
z[41] = abb[3] * z[41];
z[22] = z[22] + -z[29] + z[41];
z[22] = abb[48] * z[22];
z[29] = -abb[11] * z[27];
z[41] = abb[45] * z[11];
z[27] = z[27] + z[41];
z[27] = z[27] * z[35];
z[41] = abb[9] * abb[45];
z[37] = z[37] * z[41];
z[2] = (T(1) / T(2)) * z[2] + z[10] + z[12] + z[22] + z[27] + z[29] + z[36] + z[37];
z[2] = m1_set::bc<T>[0] * z[2];
z[10] = -abb[42] + z[1];
z[12] = abb[21] * z[10];
z[22] = abb[22] * z[10];
z[27] = z[12] + z[22];
z[29] = abb[23] * z[10];
z[36] = -abb[5] + abb[6];
z[37] = abb[12] + z[36];
z[42] = -abb[8] + z[37];
z[42] = abb[45] * z[42];
z[37] = abb[8] + z[37];
z[37] = abb[47] * z[37];
z[37] = -z[27] + -z[29] + -z[30] + z[37] + z[42];
z[42] = abb[46] + abb[48];
z[43] = abb[6] * z[42];
z[37] = (T(1) / T(2)) * z[37] + z[43];
z[37] = abb[39] * z[37];
z[43] = abb[0] * z[26];
z[44] = -z[24] + z[43];
z[45] = abb[14] * z[26];
z[46] = abb[8] * z[26];
z[47] = abb[16] * abb[51];
z[22] = z[22] + z[44] + -z[45] + z[46] + z[47];
z[46] = -abb[0] + z[25];
z[47] = z[20] + -z[33] + -z[34] + z[46];
z[48] = abb[45] + abb[47];
z[47] = -z[47] * z[48];
z[49] = abb[46] * (T(1) / T(2));
z[50] = abb[48] * (T(1) / T(2)) + z[49];
z[51] = abb[8] + z[28];
z[50] = z[50] * z[51];
z[51] = (T(1) / T(2)) * z[10];
z[51] = abb[24] * z[51];
z[22] = (T(1) / T(2)) * z[22] + z[47] + z[50] + z[51];
z[22] = abb[38] * z[22];
z[2] = z[2] + z[22] + z[37];
z[22] = abb[20] * z[10];
z[29] = z[22] + z[29];
z[37] = z[27] + z[29] + z[45];
z[45] = z[24] + z[37];
z[47] = abb[16] + abb[17];
z[50] = abb[51] * (T(1) / T(2));
z[47] = z[47] * z[50];
z[50] = z[26] * z[33];
z[43] = -z[43] + (T(1) / T(2)) * z[45] + z[47] + z[50];
z[45] = -z[31] + z[46];
z[47] = z[33] + z[45];
z[50] = abb[2] * (T(1) / T(2));
z[51] = abb[3] * (T(1) / T(4));
z[52] = -abb[11] + z[50] + z[51];
z[47] = abb[4] * (T(1) / T(4)) + (T(1) / T(2)) * z[47] + z[52];
z[47] = abb[47] * z[47];
z[46] = z[33] + z[46];
z[46] = (T(1) / T(2)) * z[46] + z[52];
z[46] = abb[46] * z[46];
z[52] = abb[6] + -abb[15] + z[25] + z[33];
z[52] = abb[11] + abb[3] * (T(-3) / T(4)) + (T(-1) / T(2)) * z[52];
z[52] = abb[48] * z[52];
z[50] = z[26] * z[50];
z[45] = -z[33] + z[45];
z[53] = abb[15] + -z[32] + z[45];
z[54] = abb[45] * (T(1) / T(2));
z[55] = z[53] * z[54];
z[56] = -abb[45] + z[26];
z[51] = z[51] * z[56];
z[57] = abb[24] * z[10];
z[58] = abb[11] * z[26];
z[43] = (T(-1) / T(2)) * z[43] + -z[46] + -z[47] + -z[50] + -z[51] + z[52] + -z[55] + (T(1) / T(4)) * z[57] + z[58];
z[46] = abb[40] * z[43];
z[47] = (T(1) / T(2)) * z[18];
z[50] = -z[39] + z[47];
z[51] = abb[28] * (T(1) / T(2));
z[52] = z[50] + -z[51];
z[55] = abb[47] + abb[48];
z[52] = -z[52] * z[55];
z[59] = z[49] + z[54];
z[59] = z[16] * z[59];
z[52] = z[52] + z[59];
z[52] = m1_set::bc<T>[0] * z[52];
z[59] = -z[42] + -z[48];
z[59] = abb[39] * z[59];
z[52] = z[52] + (T(1) / T(2)) * z[59];
z[52] = abb[1] * z[52];
z[2] = (T(1) / T(2)) * z[2] + z[46] + z[52];
z[2] = (T(1) / T(4)) * z[2];
z[46] = abb[31] * (T(1) / T(2));
z[52] = -abb[30] + z[46];
z[59] = abb[31] * z[52];
z[60] = abb[27] * (T(1) / T(2));
z[61] = -abb[30] + z[60];
z[62] = abb[27] * z[61];
z[63] = prod_pow(abb[30], 2);
z[59] = z[59] + z[62] + z[63];
z[64] = -z[11] + z[39];
z[64] = abb[32] * z[64];
z[65] = z[8] + z[51];
z[65] = abb[28] * z[65];
z[66] = -abb[36] + abb[53];
z[67] = z[65] + -z[66];
z[68] = abb[29] * z[21];
z[64] = z[59] + z[64] + z[67] + z[68];
z[69] = abb[42] * z[64];
z[70] = z[1] * z[51];
z[71] = abb[41] * z[7];
z[5] = z[5] + z[70] + z[71];
z[71] = z[3] + z[5] + -z[13];
z[71] = abb[28] * z[71];
z[72] = -z[0] * z[63];
z[71] = z[71] + -z[72];
z[73] = abb[29] * z[14];
z[74] = abb[53] * z[1];
z[75] = abb[36] * z[1];
z[73] = z[73] + -z[74] + z[75];
z[59] = -abb[41] * z[59];
z[74] = z[1] * z[38];
z[15] = z[15] + -z[74];
z[15] = abb[32] * z[15];
z[76] = z[0] * z[60];
z[76] = z[3] + z[76];
z[77] = abb[27] * z[76];
z[0] = z[0] * z[46];
z[78] = -z[0] + -z[3];
z[78] = abb[31] * z[78];
z[15] = z[15] + z[59] + z[69] + -z[71] + -z[73] + -z[77] + z[78];
z[15] = abb[21] * z[15];
z[59] = abb[27] * abb[30];
z[69] = z[59] + -z[63];
z[78] = abb[33] + z[69];
z[61] = z[46] + z[61];
z[79] = abb[31] * z[61];
z[80] = z[78] + -z[79];
z[9] = z[9] + z[51];
z[9] = abb[28] * z[9];
z[81] = -z[38] + z[61];
z[81] = abb[32] * z[81];
z[9] = -abb[36] + -z[9] + z[80] + z[81];
z[81] = -abb[42] * z[9];
z[69] = z[69] + -z[79];
z[69] = abb[41] * z[69];
z[3] = abb[27] * z[3];
z[3] = -z[3] + z[69];
z[0] = z[0] + z[76];
z[69] = abb[31] * z[0];
z[76] = abb[41] * z[61];
z[0] = z[0] + z[76];
z[76] = z[0] + -z[74];
z[76] = abb[32] * z[76];
z[4] = z[4] + -z[5];
z[4] = abb[28] * z[4];
z[5] = abb[33] * z[1];
z[4] = z[3] + z[4] + z[5] + -z[69] + z[72] + -z[75] + z[76] + z[81];
z[4] = abb[20] * z[4];
z[5] = z[21] + -z[51];
z[5] = abb[28] * z[5];
z[21] = z[5] + z[66] + -z[68];
z[51] = abb[32] * z[50];
z[66] = z[18] * z[46];
z[66] = -z[21] + -z[51] + z[66];
z[66] = abb[42] * z[66];
z[6] = z[6] + z[19];
z[13] = (T(1) / T(2)) * z[6] + z[13] + -z[74];
z[13] = abb[32] * z[13];
z[14] = z[14] + -z[70];
z[14] = abb[28] * z[14];
z[6] = -abb[31] * z[6];
z[19] = abb[52] * z[10];
z[6] = (T(1) / T(2)) * z[6] + z[13] + z[14] + -z[19] + z[66] + -z[73];
z[6] = abb[22] * z[6];
z[0] = abb[32] * z[0];
z[0] = z[0] + -z[69];
z[13] = abb[32] * z[61];
z[14] = prod_pow(abb[29], 2);
z[61] = (T(1) / T(2)) * z[14];
z[66] = -z[13] + z[61] + z[67] + -z[80];
z[66] = abb[42] * z[66];
z[61] = -abb[53] + z[61];
z[69] = abb[33] + -abb[36] + -z[61];
z[1] = z[1] * z[69];
z[1] = z[0] + z[1] + z[3] + z[66] + -z[71];
z[1] = abb[23] * z[1];
z[3] = z[7] + z[46];
z[3] = abb[31] * z[3];
z[66] = abb[32] * z[11];
z[69] = (T(1) / T(2)) * z[63];
z[3] = -z[3] + z[59] + z[66] + -z[69];
z[59] = abb[17] * z[3];
z[66] = abb[27] * abb[31];
z[70] = prod_pow(abb[27], 2);
z[71] = z[66] + -z[70];
z[73] = abb[32] * z[18];
z[73] = -z[71] + z[73];
z[74] = -abb[52] + (T(1) / T(2)) * z[73];
z[75] = abb[16] * z[74];
z[59] = z[59] + z[75];
z[59] = abb[51] * z[59];
z[62] = z[62] + z[69] + z[79];
z[13] = -z[13] + z[62];
z[75] = z[13] * z[26];
z[76] = abb[52] * z[26];
z[75] = z[75] + -z[76];
z[75] = z[28] * z[75];
z[79] = -abb[52] + z[13];
z[24] = -z[24] * z[79];
z[73] = z[26] * z[73];
z[73] = (T(1) / T(2)) * z[73] + -z[76];
z[73] = abb[8] * z[73];
z[1] = z[1] + z[4] + z[6] + z[15] + z[24] + z[59] + z[73] + z[75];
z[4] = abb[28] * z[17];
z[6] = abb[31] * z[7];
z[8] = abb[32] * z[8];
z[15] = abb[52] + -abb[53];
z[4] = -z[4] + z[6] + -z[8] + -z[15] + -z[68] + -z[78];
z[4] = z[4] * z[20];
z[6] = z[31] * z[64];
z[8] = z[14] + z[63] + -z[70];
z[8] = -abb[33] + abb[52] + (T(1) / T(2)) * z[8] + z[67];
z[8] = z[8] * z[34];
z[17] = abb[29] * (T(1) / T(2)) + z[18];
z[17] = abb[29] * z[17];
z[20] = -abb[33] + -z[17] + z[51] + (T(1) / T(2)) * z[71];
z[24] = abb[13] * (T(1) / T(2));
z[20] = z[20] * z[24];
z[24] = z[25] * z[79];
z[25] = abb[0] * z[79];
z[4] = z[4] + -z[6] + z[8] + -z[20] + z[24] + -z[25];
z[6] = 3 * abb[27] + -abb[31];
z[6] = z[6] * z[46];
z[6] = z[6] + -z[70];
z[8] = abb[29] * z[18];
z[8] = abb[36] + -z[5] + -z[6] + z[8] + -z[15] + z[51];
z[8] = z[8] * z[33];
z[15] = abb[36] + z[17];
z[5] = z[5] + -z[15];
z[18] = -abb[27] + z[46];
z[18] = abb[31] * z[18];
z[18] = z[18] + (T(1) / T(2)) * z[70];
z[20] = z[5] + -z[18];
z[20] = abb[15] * z[20];
z[24] = prod_pow(abb[31], 2);
z[24] = z[24] + -z[63];
z[34] = abb[28] * z[11];
z[24] = (T(1) / T(2)) * z[24] + -z[34];
z[24] = z[24] * z[32];
z[8] = -z[4] + z[8] + z[20] + z[24];
z[8] = abb[45] * z[8];
z[34] = -z[52] + z[60];
z[34] = abb[31] * z[34];
z[46] = abb[27] * z[7];
z[34] = -abb[33] + -abb[36] + z[34] + -z[46] + -z[63];
z[51] = abb[31] * (T(3) / T(2));
z[52] = -abb[30] + z[38] + z[51] + -z[60];
z[52] = z[38] * z[52];
z[59] = abb[28] * (T(1) / T(4));
z[7] = (T(-1) / T(2)) * z[7] + -z[39] + -z[59];
z[7] = abb[28] * z[7];
z[7] = z[7] + -z[17] + (T(1) / T(2)) * z[34] + z[52];
z[7] = abb[3] * z[7];
z[17] = abb[11] * z[3];
z[34] = abb[32] * z[16];
z[16] = abb[28] * z[16];
z[34] = -z[16] + z[34];
z[34] = abb[10] * z[34];
z[17] = z[17] + z[34];
z[34] = z[39] + z[47];
z[34] = abb[32] * z[34];
z[6] = -abb[52] + -z[6] + -z[21] + z[34];
z[6] = z[6] * z[33];
z[21] = z[63] + z[70];
z[15] = z[15] + (T(1) / T(2)) * z[21] + z[65] + -z[66];
z[21] = abb[9] * z[15];
z[34] = abb[3] + -abb[4];
z[36] = -abb[12] + z[36];
z[47] = -z[34] + z[36];
z[47] = abb[9] + (T(1) / T(2)) * z[47];
z[47] = abb[35] * z[47];
z[4] = -z[4] + z[6] + z[7] + -z[17] + z[21] + -z[24] + z[47];
z[4] = abb[47] * z[4];
z[6] = z[3] * z[26];
z[7] = -abb[45] * z[9];
z[7] = z[6] + z[7];
z[7] = z[7] * z[35];
z[6] = -abb[11] * z[6];
z[9] = z[15] * z[41];
z[15] = abb[16] + abb[19];
z[21] = -z[15] * z[26];
z[24] = -abb[0] + -abb[8] + -abb[14];
z[24] = abb[51] * z[24];
z[10] = abb[25] * z[10];
z[10] = z[10] + z[21] + z[24];
z[21] = (T(-1) / T(4)) * z[48];
z[24] = -abb[17] + abb[18] + z[15];
z[21] = z[21] * z[24];
z[24] = (T(-1) / T(4)) * z[42];
z[15] = z[15] * z[24];
z[24] = abb[26] * abb[51];
z[10] = (T(1) / T(4)) * z[10] + z[15] + z[21] + z[24];
z[10] = abb[37] * z[10];
z[15] = z[34] + z[36];
z[15] = abb[45] * z[15];
z[12] = -z[12] + z[15] + -z[29];
z[12] = (T(1) / T(2)) * z[12] + z[41];
z[12] = abb[35] * z[12];
z[1] = (T(1) / T(2)) * z[1] + z[4] + z[6] + z[7] + z[8] + z[9] + z[10] + z[12];
z[4] = -abb[54] * z[43];
z[6] = abb[41] * z[62];
z[7] = -abb[42] * z[13];
z[0] = -z[0] + z[6] + z[7] + -z[19] + (T(-1) / T(2)) * z[72] + z[77];
z[0] = abb[24] * z[0];
z[6] = z[23] + z[37] + -z[44];
z[7] = -abb[11] + z[35];
z[8] = z[7] + z[32] + z[45];
z[8] = abb[47] * z[8];
z[9] = abb[45] * z[53];
z[10] = z[35] * z[56];
z[12] = (T(1) / T(2)) * z[28];
z[7] = z[7] + -z[12];
z[7] = abb[46] * z[7];
z[6] = (T(1) / T(2)) * z[6] + z[7] + z[8] + z[9] + z[10] + -z[58];
z[7] = -abb[11] + -abb[15] + -z[12];
z[7] = (T(1) / T(3)) * z[7] + z[35];
z[7] = abb[48] * z[7];
z[6] = (T(1) / T(3)) * z[6] + z[7] + (T(-1) / T(6)) * z[57];
z[7] = prod_pow(m1_set::bc<T>[0], 2);
z[6] = z[6] * z[7];
z[8] = prod_pow(abb[32], 2);
z[9] = z[8] + -z[14];
z[9] = abb[53] + (T(1) / T(2)) * z[9] + -z[16];
z[10] = abb[34] + z[9];
z[12] = z[10] * z[30];
z[0] = z[0] + z[6] + z[12];
z[6] = abb[36] + -abb[53] + (T(-1) / T(2)) * z[8] + z[18] + z[68];
z[8] = -z[50] + z[59];
z[8] = abb[28] * z[8];
z[6] = (T(1) / T(2)) * z[6] + z[8];
z[8] = -abb[47] * z[6];
z[12] = abb[34] * (T(1) / T(2));
z[6] = -z[6] + z[12];
z[6] = abb[48] * z[6];
z[9] = z[9] * z[54];
z[12] = z[12] * z[48];
z[10] = z[10] * z[49];
z[7] = z[7] * z[55];
z[6] = z[6] + (T(1) / T(12)) * z[7] + z[8] + z[9] + z[10] + z[12];
z[6] = abb[1] * z[6];
z[7] = abb[14] * z[79];
z[7] = z[7] + -z[25];
z[8] = abb[32] * z[39];
z[8] = z[8] + z[61];
z[8] = abb[6] * z[8];
z[9] = z[33] * z[74];
z[7] = (T(1) / T(2)) * z[7] + -z[8] + -z[9] + z[17];
z[3] = z[3] * z[35];
z[3] = z[3] + -z[7];
z[8] = -abb[10] + z[31];
z[8] = abb[34] * z[8];
z[3] = (T(1) / T(2)) * z[3] + z[8];
z[3] = abb[46] * z[3];
z[9] = z[40] + -z[51];
z[9] = abb[31] * z[9];
z[9] = z[9] + -z[46] + -z[69];
z[10] = z[11] * z[38];
z[5] = z[5] + (T(1) / T(2)) * z[9] + z[10];
z[5] = abb[3] * z[5];
z[5] = z[5] + -z[7] + -z[20];
z[5] = (T(1) / T(2)) * z[5] + z[8];
z[5] = abb[48] * z[5];
z[7] = -abb[3] + -abb[6] + abb[13];
z[8] = -abb[8] + -z[7];
z[8] = abb[45] * z[8];
z[8] = z[8] + -z[22] + -z[27];
z[7] = abb[8] + -z[7];
z[7] = -abb[10] + (T(1) / T(4)) * z[7];
z[7] = abb[47] * z[7];
z[7] = z[7] + (T(1) / T(4)) * z[8];
z[7] = abb[34] * z[7];
z[0] = (T(1) / T(4)) * z[0] + (T(1) / T(2)) * z[1] + z[3] + z[4] + z[5] + z[6] + z[7];
z[0] = (T(1) / T(4)) * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_54_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.21048062489256053764196837293710010686895007511718671115592204823"),stof<T>("-0.04194482283851053381871003559965396676814852410640021842251567236")}, std::complex<T>{stof<T>("-0.19365856240504483447876012912750631441931085818364299720749858094"),stof<T>("0.24288053846791031698319522830392157622660335713265409024552433505")}, std::complex<T>{stof<T>("-0.025644232831919409048356041537461223244007822388515458342944911798"),stof<T>("-0.073210673100624777136655043426318116330949358807642647587827689006")}, std::complex<T>{stof<T>("-0.15544607849632252103623350723968708096710118774860114818442850773"),stof<T>("-0.27679292663235168124861761364236671935592401363901717350041320928")}, std::complex<T>{stof<T>("-0.032870383003594372288837417966425626302093458254634235113250177528"),stof<T>("0.060747135561956564218717333797977185097328502954697283075871499618")}, std::complex<T>{stof<T>("0.032870383003594372288837417966425626302093458254634235113250177528"),stof<T>("-0.060747135561956564218717333797977185097328502954697283075871499618")}, std::complex<T>{stof<T>("-0.25186751860815181882323830401799984856501583090687365866823217018"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_54_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_54_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.29125453957040087051919869849396333399652537439058339158447977451"),stof<T>("0.03068996749074059118317976479273059248876755809690744889799906302")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W70(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k)};

                    
            return f_4_54_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_54_DLogXconstant_part(base_point<T>, kend);
	value += f_4_54_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_54_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_54_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_54_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_54_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_54_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_54_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
