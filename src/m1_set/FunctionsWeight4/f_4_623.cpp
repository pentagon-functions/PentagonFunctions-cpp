/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_623.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_623_abbreviated (const std::array<T,64>& abb) {
T z[92];
z[0] = abb[46] + abb[47];
z[1] = (T(5) / T(3)) * z[0];
z[2] = abb[49] * (T(1) / T(6));
z[3] = abb[52] * (T(3) / T(2));
z[4] = abb[48] * (T(1) / T(2));
z[5] = abb[50] + abb[44] * (T(-1) / T(3)) + abb[45] * (T(5) / T(3)) + -z[1] + z[2] + -z[3] + z[4];
z[5] = abb[4] * z[5];
z[6] = abb[22] + abb[24];
z[7] = abb[53] + abb[54] + -abb[55];
z[6] = z[6] * z[7];
z[8] = (T(3) / T(2)) * z[6];
z[9] = abb[52] * (T(1) / T(2));
z[10] = abb[49] * (T(1) / T(2));
z[11] = z[9] + z[10];
z[12] = z[0] + z[11];
z[13] = abb[48] * (T(3) / T(2));
z[14] = -z[12] + z[13];
z[15] = abb[6] * z[14];
z[16] = abb[49] + abb[52];
z[17] = -z[0] + z[16];
z[17] = -abb[44] + (T(1) / T(3)) * z[17];
z[17] = abb[1] * z[17];
z[5] = z[5] + -z[8] + z[15] + z[17];
z[17] = (T(1) / T(2)) * z[0];
z[18] = abb[51] * (T(3) / T(2));
z[19] = -abb[52] + z[10] + -z[17] + z[18];
z[19] = abb[15] * z[19];
z[20] = abb[45] + -abb[50];
z[21] = z[10] + z[20];
z[22] = z[4] + -z[9] + z[21];
z[23] = abb[14] * z[22];
z[24] = (T(1) / T(2)) * z[7];
z[25] = abb[25] * z[24];
z[25] = z[23] + z[25];
z[26] = 3 * abb[48];
z[1] = -z[1] + -z[26];
z[27] = 3 * abb[51];
z[1] = abb[52] * (T(-2) / T(3)) + (T(1) / T(2)) * z[1] + -z[2] + z[27];
z[1] = abb[3] * z[1];
z[28] = (T(3) / T(2)) * z[20];
z[2] = abb[51] * (T(-5) / T(2)) + abb[52] * (T(7) / T(6)) + (T(4) / T(3)) * z[0] + z[2] + z[28];
z[2] = abb[2] * z[2];
z[29] = abb[51] + -abb[52];
z[30] = abb[48] + -z[0] + z[29];
z[30] = abb[12] * z[30];
z[31] = (T(1) / T(2)) * z[30];
z[32] = abb[21] + abb[23];
z[32] = -2 * z[32];
z[32] = z[7] * z[32];
z[11] = -abb[44] + z[11];
z[33] = -z[4] + z[11];
z[34] = abb[13] * z[33];
z[35] = 2 * abb[51];
z[36] = abb[49] + -abb[52];
z[37] = -abb[48] + z[36];
z[38] = z[35] + z[37];
z[38] = abb[8] * z[38];
z[39] = abb[52] * (T(3) / T(4));
z[40] = -abb[50] + abb[49] * (T(-13) / T(12)) + abb[44] * (T(-5) / T(6)) + abb[45] * (T(1) / T(6)) + abb[48] * (T(5) / T(4)) + (T(-7) / T(6)) * z[0] + z[39];
z[40] = abb[7] * z[40];
z[41] = abb[48] + z[36];
z[42] = abb[5] * z[41];
z[43] = (T(1) / T(4)) * z[42];
z[29] = abb[44] + z[29];
z[29] = abb[9] * z[29];
z[44] = 3 * z[29];
z[45] = abb[20] * z[24];
z[46] = abb[44] + abb[45];
z[47] = -abb[52] + z[46];
z[47] = abb[0] * z[47];
z[1] = z[1] + z[2] + (T(1) / T(2)) * z[5] + -z[19] + -z[25] + z[31] + z[32] + -z[34] + 2 * z[38] + z[40] + -z[43] + -z[44] + -z[45] + (T(13) / T(6)) * z[47];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[2] = -abb[52] + z[0];
z[5] = 3 * abb[44] + -abb[49] + z[2];
z[32] = abb[1] * z[5];
z[8] = z[8] + z[32];
z[38] = abb[3] * z[14];
z[40] = (T(3) / T(2)) * z[7];
z[48] = abb[21] * z[40];
z[49] = -z[8] + z[38] + -z[48];
z[50] = (T(3) / T(4)) * z[7];
z[51] = abb[25] * z[50];
z[51] = (T(3) / T(2)) * z[23] + z[51];
z[12] = -z[12] + z[18] + -z[28];
z[12] = abb[2] * z[12];
z[18] = z[9] + z[17];
z[28] = abb[44] * (T(1) / T(2));
z[52] = -abb[45] + z[28];
z[53] = -z[10] + z[18] + z[52];
z[53] = abb[4] * z[53];
z[54] = (T(3) / T(2)) * z[34];
z[55] = 3 * abb[50];
z[56] = -abb[45] + z[55];
z[57] = abb[44] + (T(1) / T(2)) * z[56];
z[18] = -abb[49] + -z[18] + z[57];
z[18] = abb[7] * z[18];
z[58] = abb[20] * z[50];
z[37] = abb[51] + (T(1) / T(2)) * z[37];
z[59] = abb[8] * z[37];
z[50] = abb[23] * z[50];
z[12] = z[12] + z[18] + -z[19] + z[44] + -2 * z[47] + (T(-1) / T(2)) * z[49] + z[50] + z[51] + z[53] + z[54] + z[58] + (T(-3) / T(2)) * z[59];
z[12] = abb[28] * z[12];
z[18] = 2 * z[0];
z[13] = -z[13] + z[18];
z[19] = abb[52] * (T(5) / T(2)) + -z[10] + z[13] + -z[27];
z[19] = abb[15] * z[19];
z[44] = abb[4] * z[14];
z[49] = z[38] + z[44];
z[50] = 3 * z[30];
z[53] = -z[49] + z[50];
z[23] = 3 * z[23];
z[60] = abb[51] + z[20] + z[36];
z[60] = abb[2] * z[60];
z[61] = abb[26] * z[7];
z[62] = (T(3) / T(2)) * z[61];
z[19] = z[19] + z[23] + z[53] + -3 * z[60] + z[62];
z[19] = abb[29] * z[19];
z[63] = (T(1) / T(2)) * z[6];
z[64] = z[32] + z[34] + z[63];
z[65] = abb[15] * z[14];
z[62] = -z[62] + z[65];
z[5] = abb[4] * z[5];
z[5] = z[5] + -z[38] + z[62] + 3 * z[64];
z[5] = abb[33] * z[5];
z[65] = abb[33] * z[7];
z[66] = abb[32] * z[24];
z[65] = z[65] + -z[66];
z[66] = abb[20] * (T(3) / T(2));
z[65] = z[65] * z[66];
z[5] = z[5] + z[65];
z[65] = abb[25] * z[40];
z[66] = abb[29] * z[65];
z[19] = z[5] + z[19] + z[66];
z[67] = -abb[49] + 2 * abb[52] + z[0];
z[68] = -z[27] + z[67];
z[68] = abb[15] * z[68];
z[69] = 2 * z[68];
z[48] = -z[48] + 3 * z[59] + z[69];
z[70] = abb[23] * z[40];
z[53] = -z[48] + -z[53] + z[70];
z[70] = -abb[30] * z[53];
z[71] = 2 * abb[49];
z[72] = z[2] + z[71];
z[73] = 3 * z[20];
z[74] = z[72] + z[73];
z[75] = abb[2] * z[74];
z[76] = z[32] + z[75];
z[77] = (T(1) / T(2)) * z[47];
z[54] = z[54] + z[77];
z[78] = -z[51] + z[54] + z[76];
z[6] = (T(3) / T(4)) * z[6];
z[79] = z[6] + z[78];
z[80] = abb[48] * (T(3) / T(4));
z[81] = abb[49] * (T(3) / T(4)) + z[80];
z[82] = abb[52] * (T(1) / T(4));
z[83] = -z[57] + z[81] + z[82];
z[83] = abb[4] * z[83];
z[83] = -z[62] + -z[79] + z[83];
z[84] = -abb[32] * z[83];
z[85] = abb[30] + -abb[33];
z[86] = z[14] * z[85];
z[74] = abb[29] * z[74];
z[86] = z[74] + z[86];
z[87] = abb[50] * (T(3) / T(2));
z[88] = 2 * abb[45];
z[89] = -z[28] + z[87] + -z[88];
z[90] = abb[52] * (T(5) / T(4));
z[81] = -z[81] + z[89] + z[90];
z[91] = -abb[32] * z[81];
z[91] = z[86] + z[91];
z[91] = abb[7] * z[91];
z[81] = abb[7] * z[81];
z[81] = -z[58] + z[81] + z[83];
z[81] = abb[31] * z[81];
z[12] = z[12] + -z[19] + z[70] + z[81] + z[84] + z[91];
z[12] = abb[28] * z[12];
z[70] = abb[49] * (T(5) / T(4)) + z[0] + z[39] + -z[57] + -z[80];
z[70] = abb[4] * z[70];
z[51] = -z[6] + -z[32] + -z[38] + -z[51] + -z[54] + z[70] + 2 * z[75];
z[51] = abb[32] * z[51];
z[54] = z[46] + -z[55];
z[54] = z[0] + z[10] + (T(1) / T(2)) * z[54];
z[54] = abb[4] * z[54];
z[52] = abb[49] + z[17] + -z[52] + -z[87];
z[52] = abb[7] * z[52];
z[2] = z[2] + z[73];
z[2] = abb[49] + (T(1) / T(2)) * z[2];
z[2] = abb[2] * z[2];
z[2] = z[2] + (T(1) / T(2)) * z[32] + -z[38] + z[52] + z[54] + -z[77];
z[2] = abb[31] * z[2];
z[52] = z[15] + -z[63];
z[54] = 3 * z[52];
z[16] = z[16] + z[18];
z[63] = z[16] + -z[26];
z[70] = abb[4] * z[63];
z[77] = abb[3] * z[63];
z[70] = z[54] + -z[62] + z[70] + z[77];
z[81] = -abb[29] + abb[30];
z[70] = z[70] * z[81];
z[28] = 4 * abb[45] + abb[50] * (T(-9) / T(2)) + abb[49] * (T(13) / T(4)) + z[18] + -z[28] + -z[39] + -z[80];
z[28] = abb[32] * z[28];
z[39] = abb[29] + -z[85];
z[39] = z[14] * z[39];
z[28] = z[28] + z[39];
z[28] = abb[7] * z[28];
z[2] = z[2] + z[5] + z[28] + z[51] + z[70];
z[2] = abb[31] * z[2];
z[5] = z[23] + z[49] + -z[62] + -3 * z[75];
z[5] = abb[29] * z[5];
z[28] = abb[3] * z[72];
z[39] = (T(3) / T(2)) * z[42];
z[51] = -z[28] + z[39];
z[62] = -z[44] + -z[51] + z[62];
z[62] = z[62] * z[85];
z[70] = z[0] + z[71];
z[71] = -abb[44] + z[88];
z[55] = -z[55] + z[70] + z[71];
z[55] = abb[4] * z[55];
z[28] = z[28] + z[47] + z[55] + -z[76];
z[28] = abb[32] * z[28];
z[5] = z[5] + z[28] + z[62] + z[66];
z[5] = abb[32] * z[5];
z[28] = z[72] * z[85];
z[46] = -z[46] + z[70];
z[46] = abb[32] * z[46];
z[28] = z[28] + z[46] + -z[74];
z[28] = abb[32] * z[28];
z[46] = (T(3) / T(2)) * z[22];
z[55] = prod_pow(abb[29], 2);
z[62] = -z[46] * z[55];
z[66] = abb[60] * z[14];
z[70] = -z[0] + z[4];
z[72] = abb[49] * (T(-3) / T(2)) + z[9] + z[70];
z[74] = 2 * z[20] + -z[72];
z[76] = 3 * abb[34];
z[74] = z[74] * z[76];
z[72] = abb[35] * z[72];
z[81] = abb[59] * z[41];
z[28] = 2 * z[28] + z[62] + z[66] + -3 * z[72] + z[74] + (T(3) / T(2)) * z[81];
z[28] = abb[7] * z[28];
z[62] = abb[21] * z[24];
z[72] = z[52] + z[59] + -z[62];
z[36] = z[26] + -z[36];
z[36] = -abb[51] + (T(1) / T(4)) * z[36];
z[36] = abb[3] * z[36];
z[74] = abb[23] * z[7];
z[31] = z[31] + z[36] + z[43] + (T(-1) / T(2)) * z[72] + (T(1) / T(4)) * z[74];
z[31] = prod_pow(abb[30], 2) * z[31];
z[36] = abb[15] * z[37];
z[43] = abb[26] * z[24];
z[36] = z[36] + z[43];
z[72] = -z[30] + z[36] + z[52];
z[21] = abb[51] * (T(1) / T(2)) + -z[17] + -z[21];
z[21] = abb[2] * z[21];
z[21] = z[21] + (T(1) / T(2)) * z[72];
z[21] = z[21] * z[55];
z[55] = -z[59] + z[62];
z[37] = abb[3] * z[37];
z[59] = -z[36] + z[37] + -z[55];
z[59] = abb[59] * z[59];
z[21] = z[21] + z[31] + z[59];
z[31] = abb[61] * z[10];
z[9] = abb[61] * z[9];
z[59] = z[9] + z[31];
z[62] = -abb[50] + z[4];
z[72] = -abb[44] + z[62];
z[72] = abb[61] * z[72];
z[72] = z[59] + z[72];
z[72] = abb[16] * z[72];
z[83] = abb[44] + z[4];
z[83] = abb[61] * z[83];
z[59] = -z[59] + z[83];
z[59] = abb[18] * z[59];
z[83] = -abb[59] + -abb[60];
z[74] = z[74] * z[83];
z[41] = (T(1) / T(2)) * z[41];
z[83] = prod_pow(abb[33], 2);
z[84] = -z[41] * z[83];
z[81] = -z[81] + z[84];
z[81] = abb[5] * z[81];
z[59] = z[59] + z[72] + z[74] + z[81];
z[10] = z[3] + -z[10];
z[70] = z[10] + -z[70];
z[72] = abb[51] + (T(-1) / T(2)) * z[70];
z[72] = abb[15] * z[72];
z[74] = abb[52] + z[0];
z[74] = -abb[51] + (T(1) / T(2)) * z[74];
z[74] = abb[3] * z[74];
z[32] = -z[29] + z[32] + (T(-1) / T(4)) * z[61] + z[72] + z[74];
z[72] = 3 * z[83];
z[32] = z[32] * z[72];
z[74] = abb[7] * z[22];
z[81] = abb[23] * z[24];
z[55] = z[55] + z[81];
z[36] = z[36] + z[55];
z[60] = -z[25] + -z[36] + z[60] + z[74];
z[60] = 3 * z[60];
z[74] = abb[57] * z[60];
z[49] = z[49] + -z[52];
z[35] = -z[35] + z[70];
z[35] = abb[15] * z[35];
z[35] = -z[30] + -z[35] + -z[43] + z[55];
z[43] = z[35] + z[49];
z[43] = 3 * z[43];
z[52] = -abb[58] * z[43];
z[38] = -z[38] + z[48] + z[50];
z[38] = abb[60] * z[38];
z[25] = -z[25] + -z[49] + z[75];
z[25] = z[25] * z[76];
z[48] = abb[4] * z[33];
z[48] = z[48] + -z[64];
z[35] = -z[35] + -z[45] + z[48];
z[35] = 3 * z[35];
z[45] = abb[56] * z[35];
z[4] = z[4] + z[20];
z[4] = abb[61] * z[4];
z[4] = z[4] + -z[9] + z[31];
z[9] = abb[17] + -abb[19];
z[9] = (T(3) / T(2)) * z[9];
z[4] = z[4] * z[9];
z[9] = (T(1) / T(2)) * z[42];
z[20] = abb[3] * z[41];
z[20] = -z[9] + z[20] + z[48];
z[31] = abb[20] * z[40];
z[20] = 3 * z[20] + -z[31];
z[20] = abb[35] * z[20];
z[40] = abb[49] * (T(1) / T(4));
z[17] = abb[44] + abb[48] * (T(-1) / T(4)) + z[17] + -z[40] + -z[82];
z[17] = z[17] * z[72];
z[17] = z[17] + -z[66];
z[17] = abb[4] * z[17];
z[7] = -abb[27] * abb[61] * z[7];
z[1] = abb[62] + abb[63] + z[1] + z[2] + z[4] + z[5] + (T(3) / T(4)) * z[7] + z[12] + z[17] + z[20] + 3 * z[21] + z[25] + z[28] + z[32] + z[38] + z[45] + z[52] + (T(3) / T(2)) * z[59] + z[74];
z[2] = z[34] + z[68];
z[4] = z[16] + -z[27] + z[73];
z[4] = abb[2] * z[4];
z[5] = -z[10] + -z[13] + z[71];
z[5] = abb[4] * z[5];
z[3] = -2 * abb[44] + abb[49] * (T(5) / T(2)) + z[3] + z[13] + -z[56];
z[3] = abb[7] * z[3];
z[2] = -3 * z[2] + z[3] + z[4] + z[5] + -z[8] + -z[23] + -6 * z[29] + -z[31] + 4 * z[47] + -z[50] + -z[65] + -z[77];
z[2] = abb[28] * z[2];
z[3] = abb[48] * (T(9) / T(4));
z[4] = abb[49] * (T(-7) / T(4)) + z[3] + -z[18] + z[57] + -z[90];
z[4] = abb[4] * z[4];
z[0] = -z[0] + z[3] + z[40];
z[3] = abb[52] * (T(-7) / T(4)) + z[0] + -z[89];
z[3] = abb[7] * z[3];
z[5] = z[6] + -z[15];
z[6] = abb[15] * z[63];
z[6] = z[6] + 3 * z[61];
z[3] = z[3] + z[4] + 3 * z[5] + -z[6] + z[58] + -z[77] + z[78];
z[3] = abb[31] * z[3];
z[4] = 6 * abb[51] + -z[26] + -z[67];
z[4] = abb[3] * z[4];
z[4] = z[4] + -6 * z[30] + -z[39] + z[44] + z[54] + -z[69];
z[4] = abb[30] * z[4];
z[0] = z[0] + -z[57] + -z[82];
z[0] = abb[4] * z[0];
z[0] = z[0] + z[6] + z[51] + -z[79];
z[0] = abb[32] * z[0];
z[5] = abb[49] * (T(-19) / T(4)) + abb[52] * (T(13) / T(4)) + -z[18] + -z[80] + z[89];
z[5] = abb[32] * z[5];
z[5] = z[5] + -z[86];
z[5] = abb[7] * z[5];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[19];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[38] * z[43];
z[3] = abb[37] * z[60];
z[4] = -abb[19] * z[22];
z[5] = z[11] + z[62];
z[5] = abb[16] * z[5];
z[6] = -abb[18] * z[33];
z[7] = -abb[27] * z[24];
z[4] = z[4] + z[5] + z[6] + z[7];
z[5] = abb[17] * z[46];
z[4] = (T(3) / T(2)) * z[4] + z[5];
z[4] = abb[41] * z[4];
z[5] = abb[36] * z[35];
z[6] = abb[7] * z[14];
z[6] = z[6] + -z[53];
z[6] = abb[40] * z[6];
z[7] = abb[7] * z[41];
z[7] = z[7] + -z[9] + -z[36] + z[37];
z[7] = abb[39] * z[7];
z[0] = abb[42] + abb[43] + z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + 3 * z[7];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_623_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.842303589037347753716178442501725703371338453839769594880634106"),stof<T>("-15.122073742449383876612057114347449851412820922669855048999498447")}, std::complex<T>{stof<T>("-25.574899553340639407108411114306672003636284064046785281202285788"),stof<T>("-28.102645388630594562804646197061075364821905261319253398516520148")}, std::complex<T>{stof<T>("2.6839523787930476751568555039135095976803968152661836106764841582"),stof<T>("9.0919584727082030491411070293242072619099732739702616849568244715")}, std::complex<T>{stof<T>("2.6839523787930476751568555039135095976803968152661836106764841582"),stof<T>("9.0919584727082030491411070293242072619099732739702616849568244715")}, std::complex<T>{stof<T>("-12.1371035290945741652266690414334509822825674060545516881928466627"),stof<T>("0.0888827129369301617539068542816990864875531811551856400898352636")}, std::complex<T>{stof<T>("2.424030856902938820802202570692558006174986576563953068994389074"),stof<T>("20.520678711206664342152845385697853535847425093237616012383059314")}, std::complex<T>{stof<T>("30.925825592714709289107393276103936093361705962293153222155919102"),stof<T>("15.228450699034538768309313555481365438949009702791305351896596809")}, std::complex<T>{stof<T>("4.1022251109274566080708313757226772948767486925420001365627291908"),stof<T>("3.6933535039509225836003187579738035774753691034025007215732636044")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_623_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_623_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[44] + -abb[49]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[32], 2);
z[1] = -prod_pow(abb[33], 2);
z[0] = z[0] + z[1];
z[1] = abb[44] + -abb[49];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_623_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_623_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(16)) * (v[2] + v[3]) * (24 + 4 * v[1] + -3 * v[2] + -7 * v[3] + -4 * v[4] + 4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 8 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1]) * (T(-3) / T(2)) * (v[2] + v[3])) / tend;


		return (abb[45] + abb[46] + abb[47] + -abb[48] + abb[49] + -abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[29], 2);
z[1] = -prod_pow(abb[31], 2);
z[0] = z[0] + z[1];
z[1] = abb[29] + -abb[31];
z[1] = abb[32] * z[1];
z[0] = -abb[34] + (T(1) / T(2)) * z[0] + z[1];
z[1] = abb[45] + abb[46] + abb[47] + -abb[48] + abb[49] + -abb[50];
return 3 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_623_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_623_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.663520514862896713432643040624959344618227012637414280328929803"),stof<T>("-51.448641858424355831654255257271394480942083378565447635601733442")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,64> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}};
abb[42] = SpDLog_f_4_623_W_17_Im(t, path, abb);
abb[43] = SpDLog_f_4_623_W_19_Im(t, path, abb);
abb[62] = SpDLog_f_4_623_W_17_Re(t, path, abb);
abb[63] = SpDLog_f_4_623_W_19_Re(t, path, abb);

                    
            return f_4_623_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_623_DLogXconstant_part(base_point<T>, kend);
	value += f_4_623_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_623_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_623_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_623_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_623_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_623_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_623_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
