/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_657.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_657_abbreviated (const std::array<T,70>& abb) {
T z[139];
z[0] = 4 * abb[58];
z[1] = 8 * abb[48] + -z[0];
z[2] = abb[52] * (T(5) / T(2));
z[3] = abb[47] * (T(1) / T(2));
z[4] = -abb[53] + z[3];
z[5] = abb[56] * (T(1) / T(2));
z[6] = 2 * abb[57];
z[7] = 6 * abb[51] + -3 * abb[55] + z[1] + z[2] + z[4] + -z[5] + z[6];
z[7] = abb[1] * z[7];
z[8] = 2 * abb[55];
z[9] = 2 * abb[53];
z[10] = z[8] + -z[9];
z[11] = 2 * abb[49];
z[12] = abb[57] + z[11];
z[13] = 3 * abb[58];
z[14] = z[12] + -z[13];
z[15] = 5 * abb[48];
z[16] = -abb[52] + abb[56];
z[17] = 2 * abb[51];
z[18] = z[10] + -z[14] + -z[15] + z[16] + -z[17];
z[18] = abb[9] * z[18];
z[19] = 3 * abb[49];
z[20] = -abb[47] + z[19];
z[21] = 3 * abb[52];
z[22] = 3 * abb[51];
z[23] = -abb[55] + abb[57];
z[24] = abb[48] + -z[20] + z[21] + z[22] + -z[23];
z[25] = abb[50] * (T(1) / T(2));
z[26] = abb[53] + z[25];
z[24] = abb[58] + (T(1) / T(2)) * z[24] + -z[26];
z[24] = abb[7] * z[24];
z[27] = 2 * abb[58];
z[28] = abb[49] + abb[57];
z[29] = z[27] + -z[28];
z[30] = abb[51] + -abb[55];
z[31] = 2 * abb[48];
z[32] = z[30] + z[31];
z[33] = z[29] + -z[32];
z[34] = abb[16] * z[33];
z[35] = abb[61] + abb[62];
z[36] = abb[22] * z[35];
z[34] = z[34] + z[36];
z[37] = abb[48] + z[30];
z[38] = 2 * z[37];
z[39] = abb[14] * z[38];
z[40] = -z[34] + z[39];
z[41] = abb[51] * (T(1) / T(2));
z[42] = abb[48] + z[41];
z[43] = abb[55] * (T(1) / T(2));
z[44] = z[42] + -z[43];
z[45] = abb[49] * (T(1) / T(2));
z[46] = abb[57] * (T(1) / T(2));
z[47] = z[45] + z[46];
z[48] = -abb[58] + z[44] + z[47];
z[48] = abb[6] * z[48];
z[49] = abb[47] + -abb[56];
z[50] = abb[50] + z[49];
z[51] = abb[10] * z[50];
z[48] = z[48] + (T(1) / T(2)) * z[51];
z[52] = -abb[52] + abb[55];
z[53] = 3 * abb[48];
z[54] = z[52] + -z[53];
z[55] = 4 * abb[51];
z[56] = 2 * z[54] + -z[55];
z[57] = z[3] + z[47];
z[58] = -z[13] + -z[56] + z[57];
z[58] = abb[5] * z[58];
z[42] = abb[53] + -z[5] + z[42] + (T(-1) / T(2)) * z[52];
z[42] = abb[0] * z[42];
z[59] = -abb[55] + z[28];
z[60] = -abb[52] + z[59];
z[60] = abb[53] + -abb[56] + -z[41] + (T(1) / T(2)) * z[60];
z[60] = abb[4] * z[60];
z[61] = (T(1) / T(2)) * z[35];
z[62] = abb[26] * z[61];
z[63] = abb[24] * z[61];
z[62] = z[62] + z[63];
z[64] = abb[49] + -abb[57];
z[65] = z[32] + z[64];
z[66] = abb[8] * z[65];
z[67] = abb[25] * z[35];
z[66] = z[66] + z[67];
z[68] = abb[23] * z[35];
z[69] = z[66] + z[68];
z[70] = -abb[48] + abb[56];
z[71] = abb[13] * z[70];
z[72] = -abb[59] + abb[60];
z[73] = -abb[19] * z[72];
z[74] = z[71] + z[73];
z[75] = abb[4] + -abb[13];
z[76] = z[25] * z[75];
z[7] = z[7] + z[18] + z[24] + -z[40] + z[42] + -z[48] + z[58] + z[60] + z[62] + z[69] + (T(1) / T(2)) * z[74] + z[76];
z[7] = abb[32] * z[7];
z[18] = abb[47] + z[28];
z[24] = abb[52] + z[31];
z[42] = z[17] + -z[18] + 2 * z[24];
z[42] = abb[15] * z[42];
z[58] = abb[20] * z[72];
z[42] = z[42] + -z[58];
z[58] = z[42] + -z[68];
z[74] = z[19] + -z[27];
z[76] = 2 * abb[52];
z[77] = -abb[55] + z[22] + z[76];
z[78] = -abb[57] + z[77];
z[79] = -z[31] + z[74] + -z[78];
z[80] = abb[7] * z[79];
z[81] = z[30] + z[53];
z[82] = z[14] + z[81];
z[83] = 2 * abb[9];
z[82] = z[82] * z[83];
z[54] = -z[17] + z[54];
z[84] = abb[58] + z[54];
z[85] = 4 * abb[5];
z[86] = z[84] * z[85];
z[87] = abb[1] * z[84];
z[29] = -abb[47] + z[29];
z[88] = abb[6] * z[29];
z[89] = abb[18] * z[72];
z[88] = z[88] + z[89];
z[90] = abb[47] + z[30];
z[91] = abb[0] * z[90];
z[80] = z[40] + z[58] + z[80] + z[82] + z[86] + 6 * z[87] + -z[88] + z[91];
z[86] = abb[30] * z[80];
z[91] = -2 * abb[50] + 5 * abb[56];
z[92] = 2 * abb[54];
z[93] = -z[9] + z[21] + z[91] + -z[92];
z[94] = -abb[47] + z[27];
z[95] = -z[22] + z[59] + -z[93] + z[94];
z[95] = abb[16] * z[95];
z[96] = z[6] + -z[8];
z[97] = -abb[52] + z[9];
z[98] = z[96] + z[97];
z[99] = -z[27] + -z[49] + z[98];
z[99] = abb[1] * z[99];
z[99] = -z[71] + z[99];
z[100] = abb[6] * z[33];
z[101] = -z[51] + z[69];
z[102] = abb[51] + abb[52];
z[103] = -z[9] + z[102];
z[59] = -z[59] + z[103];
z[104] = 2 * abb[56];
z[105] = z[59] + z[104];
z[105] = abb[4] * z[105];
z[106] = z[101] + -z[105];
z[107] = abb[57] + z[74] + -z[92];
z[81] = abb[50] + -abb[56] + z[81] + z[107];
z[81] = abb[7] * z[81];
z[108] = z[14] + z[104];
z[109] = abb[50] + abb[54];
z[110] = -z[108] + z[109];
z[110] = z[83] * z[110];
z[111] = -abb[4] + -abb[13];
z[111] = abb[50] * z[111];
z[81] = -z[36] + z[81] + z[95] + -z[99] + -z[100] + -z[106] + -z[110] + z[111];
z[81] = abb[34] * z[81];
z[7] = z[7] + z[81] + z[86];
z[7] = abb[32] * z[7];
z[81] = abb[27] * z[35];
z[86] = abb[21] * z[72];
z[95] = abb[47] + abb[49];
z[111] = -abb[57] + z[95];
z[112] = abb[17] * z[111];
z[81] = z[81] + z[86] + z[112];
z[86] = abb[13] * abb[50];
z[113] = z[42] + z[51] + z[71] + -z[81] + -z[86];
z[114] = abb[47] + -abb[52];
z[115] = abb[53] * (T(3) / T(2)) + abb[56] * (T(7) / T(12));
z[116] = abb[51] * (T(2) / T(3));
z[117] = abb[58] * (T(1) / T(3));
z[118] = abb[57] * (T(-5) / T(3)) + abb[55] * (T(-2) / T(3)) + abb[48] * (T(13) / T(6)) + (T(-3) / T(4)) * z[114] + z[115] + z[116] + z[117];
z[118] = abb[1] * z[118];
z[119] = abb[47] * (T(3) / T(2));
z[120] = abb[48] + -z[52];
z[120] = abb[49] * (T(1) / T(6)) + abb[51] * (T(4) / T(3)) + -z[46] + -z[117] + z[119] + (T(2) / T(3)) * z[120];
z[120] = abb[5] * z[120];
z[121] = abb[48] + abb[57];
z[122] = abb[49] + -abb[55];
z[115] = abb[47] * (T(-1) / T(12)) + abb[52] * (T(1) / T(4)) + -z[115] + z[116] + (T(2) / T(3)) * z[121] + (T(-1) / T(3)) * z[122];
z[115] = abb[0] * z[115];
z[121] = z[45] + -z[46];
z[123] = -z[44] + -z[121];
z[123] = abb[8] * z[123];
z[124] = abb[51] + -abb[55] + z[31] + z[46];
z[117] = abb[47] * (T(-5) / T(6)) + -z[45] + z[117] + (T(1) / T(3)) * z[124];
z[117] = abb[6] * z[117];
z[122] = -abb[58] + z[31] + z[122];
z[124] = abb[54] + z[49];
z[116] = z[116] + (T(2) / T(3)) * z[122] + (T(1) / T(4)) * z[124];
z[116] = abb[7] * z[116];
z[46] = abb[49] * (T(7) / T(2)) + z[46];
z[43] = z[43] + z[46] + z[114];
z[114] = abb[58] + z[41] + (T(-1) / T(3)) * z[43];
z[114] = abb[3] * z[114];
z[67] = z[67] + -z[89];
z[122] = abb[2] * z[124];
z[124] = abb[23] * z[61];
z[67] = -z[62] + (T(-5) / T(6)) * z[67] + (T(-1) / T(2)) * z[73] + (T(-1) / T(3)) * z[113] + z[114] + z[115] + z[116] + z[117] + z[118] + z[120] + (T(-1) / T(4)) * z[122] + (T(5) / T(3)) * z[123] + -z[124];
z[67] = prod_pow(m1_set::bc<T>[0], 2) * z[67];
z[113] = 5 * abb[51];
z[1] = -z[1] + z[8] + -z[21] + -z[57] + -z[113];
z[1] = abb[5] * z[1];
z[21] = abb[27] * z[61];
z[114] = (T(1) / T(2)) * z[72];
z[115] = abb[21] * z[114];
z[21] = z[21] + (T(1) / T(2)) * z[112] + z[115];
z[112] = abb[24] * z[35];
z[35] = abb[26] * z[35];
z[112] = z[35] + z[112];
z[115] = 3 * z[73];
z[116] = z[89] + -z[112] + -z[115];
z[117] = z[3] + -z[32];
z[45] = abb[57] * (T(3) / T(2)) + -z[27] + z[45] + -z[117];
z[45] = abb[6] * z[45];
z[118] = abb[47] + z[31];
z[120] = -z[11] + z[77] + z[118];
z[123] = -abb[7] * z[120];
z[125] = abb[51] + z[24];
z[126] = -z[57] + z[125];
z[127] = abb[15] * z[126];
z[128] = abb[20] * z[114];
z[127] = -z[127] + z[128];
z[128] = z[124] + z[127];
z[125] = -abb[57] + z[27] + -z[125];
z[125] = abb[0] * z[125];
z[1] = z[1] + -z[21] + -z[34] + z[45] + z[82] + 4 * z[87] + (T(1) / T(2)) * z[116] + z[123] + z[125] + -z[128];
z[1] = abb[30] * z[1];
z[45] = -abb[32] * z[80];
z[80] = 6 * abb[58];
z[82] = 7 * abb[49] + -z[80];
z[87] = 2 * abb[47];
z[78] = z[78] + -z[82] + -z[87];
z[116] = abb[3] * z[78];
z[123] = z[88] + z[116];
z[120] = abb[0] * z[120];
z[125] = -abb[48] + -z[11] + z[94];
z[129] = 2 * abb[7];
z[125] = z[125] * z[129];
z[95] = -abb[58] + z[95];
z[130] = z[85] * z[95];
z[120] = z[34] + z[120] + z[123] + -z[125] + z[130];
z[125] = -abb[34] + abb[35];
z[120] = z[120] * z[125];
z[125] = -z[32] + z[119] + z[121];
z[125] = abb[6] * z[125];
z[125] = z[39] + (T(-3) / T(2)) * z[89] + z[125];
z[130] = 2 * abb[1];
z[84] = z[84] * z[130];
z[128] = -z[84] + z[128];
z[131] = z[125] + -z[128];
z[132] = z[17] + z[24];
z[133] = -z[23] + z[94] + -z[132];
z[133] = abb[0] * z[133];
z[134] = 4 * abb[48];
z[22] = -abb[52] + z[8] + -z[22] + -z[134];
z[135] = z[22] + z[57];
z[135] = abb[5] * z[135];
z[115] = z[35] + z[115];
z[115] = (T(1) / T(2)) * z[115] + z[135];
z[135] = z[21] + z[63];
z[136] = abb[7] * z[29];
z[133] = z[115] + z[131] + -z[133] + z[135] + -z[136];
z[136] = abb[31] * z[133];
z[77] = -z[77] + z[94] + -z[134];
z[77] = abb[0] * z[77];
z[94] = -abb[47] + z[32];
z[137] = abb[6] * z[94];
z[137] = z[89] + z[137];
z[138] = abb[47] + -abb[48];
z[85] = -z[85] * z[138];
z[95] = -z[95] * z[129];
z[77] = z[77] + -z[84] + z[85] + z[95] + -z[137];
z[77] = abb[33] * z[77];
z[1] = z[1] + z[45] + z[77] + z[120] + z[136];
z[1] = abb[33] * z[1];
z[45] = -abb[55] + z[18] + -z[80] + z[93] + z[113] + z[134];
z[45] = abb[16] * z[45];
z[45] = -z[36] + z[45];
z[77] = z[15] + -z[52];
z[20] = abb[57] + z[20] + z[55] + 2 * z[77] + -z[80];
z[20] = abb[5] * z[20];
z[77] = -z[9] + z[49];
z[85] = 16 * abb[48] + 10 * abb[51] + 5 * abb[52] + -4 * abb[55] + z[77] + -z[80] + z[92];
z[85] = abb[1] * z[85];
z[15] = 4 * abb[53] + -5 * abb[54] + -z[15] + z[76] + z[91];
z[15] = abb[9] * z[15];
z[10] = z[10] + -z[132];
z[76] = z[10] + z[92];
z[91] = abb[50] + -z[76];
z[91] = abb[7] * z[91];
z[95] = abb[4] * abb[50];
z[113] = z[95] + -z[105];
z[120] = z[73] + z[112];
z[70] = abb[54] + -z[70] + -z[90];
z[70] = abb[0] * z[70];
z[15] = z[15] + z[20] + -z[45] + -z[58] + z[70] + z[85] + z[91] + -z[113] + z[120];
z[15] = abb[36] * z[15];
z[20] = abb[47] + abb[58];
z[17] = z[17] + -z[20] + -z[52] + z[134];
z[17] = abb[5] * z[17];
z[16] = -z[9] + z[16];
z[14] = -abb[48] + -z[14] + -z[16];
z[14] = abb[9] * z[14];
z[52] = -z[51] + (T(1) / T(2)) * z[95];
z[58] = abb[52] + -abb[57] + -z[19] + z[30];
z[26] = abb[58] + z[26] + (T(1) / T(2)) * z[58];
z[26] = abb[7] * z[26];
z[58] = -abb[57] + abb[58];
z[70] = abb[6] * z[58];
z[85] = -abb[56] + z[37];
z[90] = -abb[57] + -z[85];
z[90] = abb[0] * z[90];
z[14] = z[14] + z[17] + -z[21] + z[26] + -z[52] + -z[60] + z[70] + z[90] + z[128];
z[14] = prod_pow(abb[30], 2) * z[14];
z[0] = z[0] + -z[55];
z[17] = z[0] + -z[93] + -z[118];
z[17] = abb[16] * z[17];
z[26] = z[77] + z[132];
z[55] = abb[0] * z[26];
z[29] = abb[5] * z[29];
z[29] = z[29] + -z[120];
z[70] = -z[32] + -z[50] + -z[107];
z[70] = abb[7] * z[70];
z[70] = -z[17] + z[29] + -z[55] + z[70] + z[88] + z[110] + z[113];
z[70] = abb[30] * z[70];
z[88] = abb[52] * (T(1) / T(2));
z[90] = abb[53] + z[88];
z[91] = z[25] + -z[37] + -z[90] + z[92] + -z[108];
z[91] = abb[9] * z[91];
z[43] = abb[51] * (T(-3) / T(2)) + -z[13] + z[43];
z[43] = abb[5] * z[43];
z[43] = z[43] + -z[60] + z[62] + -z[91];
z[41] = abb[53] * (T(1) / T(2)) + z[25] + -z[41];
z[60] = abb[52] * (T(3) / T(2));
z[23] = -z[23] + z[60];
z[19] = -7 * abb[48] + -z[19] + z[23];
z[62] = 4 * abb[56];
z[19] = abb[58] + abb[54] * (T(-13) / T(4)) + -z[3] + (T(1) / T(2)) * z[19] + -z[41] + z[62];
z[19] = abb[7] * z[19];
z[91] = abb[4] + 3 * abb[13];
z[25] = z[25] * z[91];
z[88] = z[4] + z[88];
z[91] = -abb[55] + abb[56] * (T(-3) / T(2)) + z[27] + -z[88];
z[91] = abb[1] * z[91];
z[5] = -abb[48] + z[5];
z[88] = -abb[51] + z[5] + -z[88];
z[88] = abb[0] * z[88];
z[17] = -z[17] + z[19] + z[25] + -z[43] + -z[48] + (T(-3) / T(2)) * z[71] + z[88] + z[91];
z[17] = abb[34] * z[17];
z[17] = z[17] + z[70];
z[17] = abb[34] * z[17];
z[19] = -abb[50] + z[132];
z[25] = -abb[49] + abb[54];
z[25] = z[9] + z[19] + -4 * z[25] + z[62] + -z[80] + z[96];
z[25] = abb[9] * z[25];
z[25] = z[25] + z[112] + -z[113];
z[0] = -3 * abb[53] + abb[56] * (T(7) / T(2)) + -z[0] + z[2] + z[53] + -z[109] + z[119];
z[0] = abb[16] * z[0];
z[2] = abb[51] + z[23];
z[4] = -abb[50] + abb[54] + z[2] + z[4] + z[5] + -z[74];
z[4] = abb[7] * z[4];
z[23] = -z[49] + -z[76];
z[23] = abb[1] * z[23];
z[48] = -abb[5] * z[79];
z[5] = -abb[54] + z[3] + z[5] + z[90];
z[5] = abb[0] * z[5];
z[0] = z[0] + z[4] + z[5] + z[23] + -z[25] + z[48] + -z[51] + z[100];
z[0] = abb[39] * z[0];
z[4] = -z[11] + z[92];
z[3] = z[3] + -z[4] + -z[27] + -z[44] + z[104];
z[3] = abb[0] * z[3];
z[5] = abb[55] + z[60];
z[5] = abb[54] * (T(3) / T(4)) + (T(1) / T(2)) * z[5] + z[13] + -z[31] + -z[41] + -z[46];
z[5] = abb[7] * z[5];
z[11] = -abb[58] + (T(1) / T(2)) * z[18];
z[11] = abb[6] * z[11];
z[3] = z[3] + z[5] + z[11] + -z[34] + -z[43] + z[52] + (T(-1) / T(2)) * z[89] + -z[116] + 3 * z[122];
z[3] = abb[35] * z[3];
z[2] = -abb[53] + abb[54] * (T(1) / T(2)) + z[2] + -z[50] + -z[82] + -z[134];
z[2] = abb[7] * z[2];
z[4] = z[4] + z[26];
z[4] = abb[0] * z[4];
z[5] = abb[5] * z[78];
z[2] = -z[2] + z[4] + -z[5] + z[25] + -z[45] + z[116];
z[4] = abb[34] * z[2];
z[5] = z[29] + -z[34];
z[11] = abb[7] * z[33];
z[13] = z[6] + z[94];
z[23] = z[13] + -z[27];
z[23] = abb[0] * z[23];
z[11] = -z[5] + -z[11] + z[23] + z[81];
z[23] = abb[30] * z[11];
z[3] = z[3] + z[4] + z[23];
z[3] = abb[35] * z[3];
z[2] = -abb[38] * z[2];
z[4] = -z[22] + -z[27] + z[57];
z[4] = abb[5] * z[4];
z[23] = abb[7] * z[94];
z[25] = z[35] + -z[73];
z[26] = abb[57] + -z[87] + -z[102];
z[26] = abb[0] * z[26];
z[4] = z[4] + z[23] + (T(1) / T(2)) * z[25] + z[26] + z[34] + -z[131] + z[135];
z[4] = abb[30] * z[4];
z[25] = z[58] * z[130];
z[26] = abb[0] * z[94];
z[25] = -z[5] + -z[25] + z[26] + z[69];
z[29] = -abb[32] + abb[34];
z[25] = z[25] * z[29];
z[11] = abb[35] * z[11];
z[29] = -z[37] * z[130];
z[31] = -abb[5] * z[94];
z[26] = z[26] + z[29] + z[31] + z[39] + z[73];
z[26] = abb[31] * z[26];
z[4] = z[4] + -z[11] + z[25] + z[26];
z[4] = abb[31] * z[4];
z[22] = abb[57] + z[22];
z[22] = z[22] * z[130];
z[18] = z[18] + z[56];
z[18] = abb[5] * z[18];
z[25] = abb[7] * z[65];
z[26] = abb[6] * z[111];
z[26] = z[26] + -z[89];
z[29] = abb[0] * z[38];
z[18] = z[18] + z[22] + -z[25] + z[26] + z[29] + z[42] + z[66] + z[120];
z[22] = -abb[64] * z[18];
z[25] = -z[47] + z[102] + z[119];
z[25] = abb[19] * z[25];
z[29] = z[117] + -z[121];
z[29] = abb[18] * z[29];
z[31] = -abb[5] + -abb[6] + abb[15] + abb[17];
z[31] = z[31] * z[114];
z[33] = abb[7] + 2 * abb[29];
z[33] = z[33] * z[72];
z[34] = abb[20] * z[126];
z[35] = abb[28] * z[61];
z[37] = abb[21] * z[111];
z[25] = z[25] + z[29] + z[31] + -z[33] + -z[34] + z[35] + (T(1) / T(2)) * z[37];
z[29] = abb[66] * z[25];
z[20] = -z[20] + z[28] + -z[102];
z[20] = abb[0] * z[20];
z[28] = -abb[57] + z[74];
z[30] = -z[28] + z[30] + -z[87];
z[30] = abb[7] * z[30];
z[28] = -3 * abb[47] + -z[28];
z[28] = abb[5] * z[28];
z[20] = 2 * z[20] + z[28] + z[30] + z[81] + z[120] + -z[123];
z[20] = abb[37] * z[20];
z[28] = abb[50] + -z[64] + z[85];
z[28] = abb[7] * z[28];
z[30] = abb[47] + abb[56];
z[31] = -z[30] + z[98];
z[31] = abb[1] * z[31];
z[33] = abb[6] * z[65];
z[34] = abb[50] * z[75];
z[34] = z[34] + z[71];
z[30] = z[30] + z[59];
z[30] = abb[16] * z[30];
z[30] = z[30] + z[36];
z[28] = -z[28] + z[30] + z[31] + -z[33] + z[34] + z[106];
z[31] = -abb[65] * z[28];
z[26] = z[26] + 2 * z[51] + -z[81] + -z[113];
z[6] = abb[55] + abb[56] + -z[6] + z[87] + z[103];
z[6] = abb[0] * z[6];
z[33] = abb[7] * z[50];
z[6] = z[6] + z[26] + -z[30] + z[33];
z[30] = -abb[63] * z[6];
z[12] = z[12] + -z[27] + -z[102];
z[33] = -abb[69] * z[12];
z[0] = abb[67] + abb[68] + z[0] + z[1] + z[2] + z[3] + z[4] + z[7] + z[14] + z[15] + z[17] + z[20] + z[22] + z[29] + z[30] + z[31] + z[33] + z[67];
z[1] = z[5] + z[137];
z[2] = z[16] + -z[32];
z[2] = z[2] * z[83];
z[3] = -4 * abb[57] + z[24] + z[27] + -z[77];
z[3] = abb[1] * z[3];
z[4] = z[10] + -z[49];
z[4] = abb[0] * z[4];
z[5] = abb[50] + -z[8] + z[97] + -z[138];
z[5] = abb[7] * z[5];
z[3] = z[1] + -z[2] + z[3] + z[4] + z[5] + -z[34] + z[39] + -z[42] + z[51] + -2 * z[66] + -z[68] + z[105];
z[3] = abb[32] * z[3];
z[4] = abb[57] + z[54];
z[4] = z[4] * z[130];
z[5] = abb[47] + -abb[55] + z[132];
z[7] = -abb[57] + z[5];
z[7] = abb[0] * z[7];
z[4] = z[4] + z[7] + -z[21] + -z[23] + z[63] + z[66] + z[115] + z[124] + z[125] + -z[127];
z[4] = abb[31] * z[4];
z[5] = -abb[58] + z[5];
z[5] = abb[5] * z[5];
z[7] = z[13] + -z[104];
z[7] = abb[0] * z[7];
z[8] = -z[9] + z[19];
z[8] = abb[7] * z[8];
z[2] = z[2] + 2 * z[5] + z[7] + z[8] + -z[26] + -z[40] + -z[84];
z[2] = abb[30] * z[2];
z[5] = abb[7] * z[138];
z[1] = -z[1] + z[5] + z[55] + z[86] + z[99] + z[101];
z[1] = abb[34] * z[1];
z[5] = abb[33] * z[133];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + -z[11];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[41] * z[18];
z[3] = -abb[42] * z[28];
z[4] = abb[43] * z[25];
z[5] = -abb[40] * z[6];
z[6] = -abb[46] * z[12];
z[1] = abb[44] + abb[45] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_657_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("4.0549405544182681858021109701137329747769965591317530237318657023"),stof<T>("5.2105367781938766172750286354578781008753444151395946877934623755")}, std::complex<T>{stof<T>("-15.927884452211109625321039725382567525874898971354366178509680538"),stof<T>("-2.770327363474794988029681283490635497436961693904857615219119947")}, std::complex<T>{stof<T>("1.197783340108982842113156859196858997201643252906002650661903574"),stof<T>("-10.531710897712234205811330326594131389396102116440914079046112629")}, std::complex<T>{stof<T>("1.1048384237308970223656392223276463223785147286541254668707201231"),stof<T>("1.4639623029700601432425161240033904706239233795937915111856691633")}, std::complex<T>{stof<T>("-11.142246098401870014504996874073717719946404962646231662376745148"),stof<T>("8.983070217804028219509322590022969875115110214350503866977932479")}, std::complex<T>{stof<T>("-3.0333195632308512894927279627196221507316554311365096099336933851"),stof<T>("2.5248877916572274723048389645350136470954512437313557734568534154")}, std::complex<T>{stof<T>("14.723031882494874666277338608196022999331801735450616364936254692"),stof<T>("12.219091889099924597567083692411917054300198342654057289205969846")}, std::complex<T>{stof<T>("0.7474105939235813918735996072560840695488486637844138699749244169"),stof<T>("-12.5677283706967630459880254716939147551697581419461767381240639864")}, std::complex<T>{stof<T>("4.1843893662613707477229743317377905246010223899295916125993394271"),stof<T>("1.0028794179071829898056514172515855326762007416075746582541814245")}, std::complex<T>{stof<T>("-1.8488106530094033717902282135306562051351095078410308024113523606"),stof<T>("0.4401574990783764726823055843217097560116973162601166337086091277")}, std::complex<T>{stof<T>("-1.6048809138609959509188142370120174883144154325121793177869173784"),stof<T>("5.0998238860993871277760182364824319829888294352480755024407994981")}, std::complex<T>{stof<T>("-1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(abs(k.W[73])) - rlog(abs(kbase.W[73])), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_657_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_657_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (24 + 5 * v[0] + 7 * v[1] + 5 * v[2] + -7 * v[3] + -12 * v[4] + -4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -5 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((T(-1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (-30 * v[0] + 14 * v[1] + -6 * v[2] + -6 * v[3] + 6 * v[5] + m1_set::bc<T>[2] * (21 * v[0] + -v[1] + -3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[2] = ((T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (2 * (-48 + 3 * v[0] + -19 * v[1] + -9 * v[2] + 15 * v[3] + 24 * v[4] + 12 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 9 * v[5]) + m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[3] = ((-1 + 2 * m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[4] = (m1_set::bc<T>[2] * (T(-3) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[5] = ((4 * m1_set::bc<T>[1] + -m1_set::bc<T>[2]) * (T(-3) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return abb[50] * c[3] + abb[53] * (c[3] + -c[4]) + abb[49] * (2 * c[3] + -c[4]) + abb[48] * c[4] + abb[51] * (-2 * c[3] + c[4]) + abb[56] * c[5] + -abb[47] * (2 * c[3] + c[5]) + abb[52] * (-3 * c[3] + c[4]) * (T(1) / T(2)) + abb[54] * (-3 * c[3] + c[4] + -2 * c[5]) * (T(1) / T(2)) + t * (abb[50] * c[0] + abb[53] * (c[0] + -c[1]) + abb[49] * (2 * c[0] + -c[1]) + abb[48] * c[1] + abb[51] * (-2 * c[0] + c[1]) + abb[56] * c[2] + -abb[47] * (2 * c[0] + c[2]) + abb[52] * (-3 * c[0] + c[1]) * (T(1) / T(2)) + abb[54] * (-3 * c[0] + c[1] + -2 * c[2]) * (T(1) / T(2)));
	}
	{
T z[6];
z[0] = -abb[50] + -abb[53] + abb[52] * (T(3) / T(2));
z[1] = abb[49] + -abb[51];
z[2] = 2 * abb[47];
z[3] = 3 * abb[56] + abb[54] * (T(-9) / T(4)) + (T(1) / T(2)) * z[0] + -z[1] + -z[2];
z[3] = prod_pow(abb[35], 2) * z[3];
z[4] = -2 * z[1];
z[2] = abb[54] * (T(3) / T(2)) + z[0] + z[2] + z[4];
z[5] = -abb[38] * z[2];
z[2] = abb[35] * z[2];
z[0] = abb[54] * (T(1) / T(2)) + -z[0];
z[0] = -abb[56] + (T(1) / T(2)) * z[0] + z[1];
z[0] = abb[34] * z[0];
z[0] = 3 * z[0] + z[2];
z[0] = abb[34] * z[0];
z[1] = abb[48] + -abb[53] + abb[54];
z[1] = abb[47] + abb[52] + -abb[56] + 2 * z[1] + z[4];
z[2] = abb[36] + -abb[39];
z[1] = z[1] * z[2];
z[0] = z[0] + z[1] + z[3] + z[5];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_657_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_657_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + 4 * v[0] + 3 * v[1] + -v[2] + v[3] + v[4] + 2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[47] + abb[49] + -abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[30] + -abb[31];
z[1] = abb[33] + z[0];
z[1] = abb[33] * z[1];
z[0] = abb[35] + z[0];
z[0] = abb[35] * z[0];
z[0] = -abb[37] + -z[0] + z[1];
z[1] = abb[47] + abb[49] + -abb[58];
return 2 * abb[12] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_657_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[47] + abb[49] + -abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[33] + abb[35];
z[1] = abb[47] + abb[49] + -abb[58];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_657_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("1.641716277584588732796610950561090164051020069258474987822853549"),stof<T>("-15.888915923535740023822939603066782080337307132266650919022404062")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19, 73});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,70> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(abs(kend.W[73])) - rlog(abs(k.W[73])), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[44] = SpDLog_f_4_657_W_17_Im(t, path, abb);
abb[45] = SpDLog_f_4_657_W_20_Im(t, path, abb);
abb[46] = SpDLogQ_W_74(k,dl,dlr).imag();
abb[67] = SpDLog_f_4_657_W_17_Re(t, path, abb);
abb[68] = SpDLog_f_4_657_W_20_Re(t, path, abb);
abb[69] = SpDLogQ_W_74(k,dl,dlr).real();

                    
            return f_4_657_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_657_DLogXconstant_part(base_point<T>, kend);
	value += f_4_657_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_657_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_657_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_657_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_657_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_657_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_657_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
