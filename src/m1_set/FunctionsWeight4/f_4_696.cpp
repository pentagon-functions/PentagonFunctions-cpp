/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_696.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_696_abbreviated (const std::array<T,74>& abb) {
T z[139];
z[0] = abb[53] + -abb[54] + -abb[55];
z[1] = abb[56] + abb[58];
z[2] = -2 * abb[57] + z[0] + z[1];
z[3] = abb[26] * z[2];
z[4] = abb[27] * z[2];
z[5] = z[3] + -z[4];
z[6] = abb[60] + abb[63];
z[7] = -abb[64] + z[6];
z[8] = abb[61] + abb[62];
z[9] = z[7] + z[8];
z[9] = abb[12] * z[9];
z[10] = 2 * z[9];
z[11] = abb[62] + abb[64];
z[11] = abb[10] * z[11];
z[12] = 2 * z[11];
z[13] = z[10] + -z[12];
z[14] = abb[19] * abb[65];
z[15] = 2 * z[14];
z[16] = -abb[62] + z[7];
z[17] = abb[0] * z[16];
z[18] = 2 * z[17];
z[19] = -z[13] + z[15] + z[18];
z[20] = 2 * abb[13];
z[21] = abb[63] + z[8];
z[20] = z[20] * z[21];
z[22] = 2 * abb[61];
z[23] = abb[62] + z[6];
z[24] = z[22] + z[23];
z[25] = -abb[64] + z[24];
z[26] = 2 * abb[3];
z[25] = z[25] * z[26];
z[27] = 3 * abb[60];
z[28] = 2 * abb[62];
z[29] = abb[61] + z[28];
z[30] = z[27] + z[29];
z[31] = abb[63] + z[30];
z[31] = abb[6] * z[31];
z[32] = abb[24] * z[2];
z[33] = abb[61] + z[6];
z[34] = abb[7] * z[33];
z[35] = abb[1] * z[8];
z[36] = 4 * z[35];
z[37] = abb[60] + abb[62];
z[38] = abb[11] * z[37];
z[39] = abb[17] * z[6];
z[40] = -4 * abb[11] + -abb[17];
z[40] = abb[61] * z[40];
z[25] = z[5] + z[19] + -z[20] + z[25] + z[31] + z[32] + z[34] + z[36] + -4 * z[38] + -z[39] + z[40];
z[25] = abb[35] * z[25];
z[31] = -abb[57] + abb[59] + z[0];
z[32] = -abb[24] * z[31];
z[40] = -abb[26] * z[31];
z[41] = z[32] + z[40];
z[42] = 2 * abb[59] + z[0] + -z[1];
z[43] = abb[25] * z[42];
z[44] = z[41] + -z[43];
z[45] = abb[15] * z[8];
z[46] = -abb[57] + -abb[59] + z[1];
z[47] = abb[22] * z[46];
z[45] = z[45] + z[47];
z[48] = abb[23] * z[46];
z[49] = z[45] + z[48];
z[50] = abb[4] * z[8];
z[51] = -z[49] + z[50];
z[52] = z[20] + z[51];
z[53] = abb[11] * abb[61];
z[38] = z[38] + z[53];
z[53] = abb[3] * z[8];
z[54] = abb[9] * z[6];
z[55] = -z[38] + z[53] + z[54];
z[56] = abb[61] + abb[63];
z[57] = -z[27] + z[28] + -z[56];
z[57] = abb[6] * z[57];
z[58] = 3 * abb[61];
z[59] = abb[63] + -z[27] + z[58];
z[59] = abb[8] * z[59];
z[60] = abb[5] * z[23];
z[61] = 4 * abb[61] + 3 * z[23];
z[62] = abb[14] * z[61];
z[34] = -z[34] + -8 * z[35] + z[44] + -z[52] + -4 * z[55] + z[57] + z[59] + z[60] + z[62];
z[34] = abb[32] * z[34];
z[57] = abb[17] * abb[61];
z[39] = z[39] + z[57];
z[57] = z[4] + z[39];
z[19] = z[19] + -z[57];
z[59] = 4 * z[54];
z[62] = abb[6] * z[28];
z[63] = z[36] + z[59] + -z[62];
z[64] = abb[14] * z[24];
z[65] = -z[43] + z[48] + z[64];
z[66] = 2 * abb[60];
z[67] = z[8] + z[66];
z[68] = 2 * abb[63];
z[69] = z[67] + z[68];
z[70] = abb[4] * z[69];
z[71] = abb[26] * z[46];
z[72] = z[45] + z[71];
z[73] = abb[24] * z[46];
z[74] = z[72] + z[73];
z[26] = -z[7] * z[26];
z[75] = -abb[61] + z[6];
z[76] = abb[8] * z[75];
z[77] = -abb[62] + z[6];
z[78] = abb[5] * z[77];
z[26] = -z[19] + z[26] + z[63] + -z[65] + -z[70] + -z[74] + z[76] + -z[78];
z[26] = abb[30] * z[26];
z[79] = abb[61] + -abb[62];
z[80] = z[66] + -z[79];
z[80] = abb[8] * z[80];
z[81] = 2 * z[35];
z[82] = z[20] + z[81];
z[83] = z[6] + z[8];
z[84] = abb[14] * z[83];
z[85] = 2 * z[84];
z[86] = abb[5] * z[79];
z[87] = z[85] + -z[86];
z[88] = abb[4] * z[83];
z[89] = 2 * z[88];
z[90] = abb[25] * z[46];
z[48] = z[48] + z[90];
z[91] = abb[3] * z[28];
z[62] = -z[48] + -z[62] + -z[74] + z[80] + z[82] + -z[87] + z[89] + z[91];
z[62] = abb[36] * z[62];
z[91] = abb[23] * z[42];
z[44] = z[44] + -z[91];
z[92] = abb[7] * z[8];
z[93] = z[44] + z[92];
z[94] = 2 * abb[64];
z[27] = -4 * abb[62] + -3 * abb[63] + -z[27] + -z[58] + z[94];
z[27] = abb[3] * z[27];
z[95] = abb[6] * z[77];
z[95] = z[36] + z[95];
z[76] = z[76] + z[95];
z[96] = abb[4] * z[24];
z[19] = -z[19] + z[27] + -z[76] + z[87] + z[93] + -z[96];
z[19] = abb[34] * z[19];
z[27] = -abb[60] + z[22];
z[87] = abb[62] + abb[63] + z[27];
z[87] = abb[8] * z[87];
z[87] = z[64] + z[87];
z[97] = abb[3] * abb[62];
z[98] = abb[5] * abb[62];
z[99] = z[97] + -z[98];
z[100] = abb[6] * abb[62];
z[101] = z[99] + -z[100];
z[102] = -abb[25] * z[31];
z[82] = z[82] + -z[87] + z[96] + 2 * z[101] + -z[102];
z[82] = abb[31] * z[82];
z[103] = abb[21] * abb[65];
z[104] = abb[17] * abb[64];
z[103] = z[103] + -z[104];
z[104] = z[39] + z[103];
z[105] = abb[27] * z[46];
z[105] = z[104] + z[105];
z[106] = abb[61] + z[7];
z[107] = abb[3] * z[106];
z[107] = -z[92] + z[107];
z[108] = -z[105] + z[107];
z[109] = z[14] + z[17];
z[110] = z[74] + z[108] + z[109];
z[111] = abb[37] * z[110];
z[112] = -abb[27] * z[31];
z[112] = z[103] + z[112];
z[113] = -z[13] + z[109] + z[112];
z[114] = -abb[64] + 2 * z[83];
z[114] = abb[3] * z[114];
z[115] = -abb[23] * z[31];
z[116] = z[41] + z[115];
z[117] = abb[6] * z[23];
z[118] = -z[116] + z[117];
z[119] = abb[14] * z[23];
z[120] = z[118] + -z[119];
z[121] = z[113] + z[114] + z[120];
z[121] = abb[33] * z[121];
z[19] = z[19] + z[25] + z[26] + z[34] + z[62] + z[82] + z[111] + z[121];
z[19] = m1_set::bc<T>[0] * z[19];
z[25] = abb[18] * abb[65];
z[12] = -z[12] + z[25];
z[26] = z[60] + z[102];
z[34] = abb[8] * abb[64];
z[62] = -z[12] + z[17] + z[26] + z[34] + z[112] + -z[119];
z[82] = -abb[46] * z[62];
z[122] = z[7] + z[29];
z[122] = abb[8] * z[122];
z[10] = -z[10] + z[25];
z[122] = -z[10] + z[17] + z[48] + -z[50] + -z[105] + -z[122];
z[123] = -z[74] + -z[122];
z[123] = abb[45] * z[123];
z[124] = -z[84] + z[88];
z[125] = -z[33] + z[94];
z[125] = abb[8] * z[125];
z[13] = z[13] + -z[99] + -z[100] + -z[124] + z[125];
z[99] = abb[2] * z[106];
z[106] = -z[13] + 2 * z[99];
z[106] = abb[51] * z[106];
z[34] = z[14] + z[34];
z[125] = -abb[64] + z[37] + z[56];
z[126] = 2 * abb[12];
z[125] = z[125] * z[126];
z[103] = -z[25] + -z[34] + -z[103] + z[125];
z[103] = abb[48] * z[103];
z[125] = z[6] + z[29];
z[126] = abb[64] + z[125];
z[126] = abb[8] * z[126];
z[127] = abb[5] * z[8];
z[126] = z[12] + z[14] + z[126] + z[127];
z[104] = z[90] + z[104] + -z[107] + -z[126];
z[104] = abb[50] * z[104];
z[107] = z[20] + -z[81] + -z[119];
z[127] = abb[7] * z[24];
z[128] = z[107] + -z[127];
z[129] = abb[8] * z[24];
z[130] = -abb[63] + z[37];
z[130] = abb[6] * z[130];
z[129] = z[26] + z[128] + z[129] + z[130];
z[131] = z[40] + z[129];
z[131] = abb[47] * z[131];
z[132] = -abb[6] + abb[8];
z[132] = z[79] * z[132];
z[132] = -z[81] + z[90] + z[92] + z[132];
z[51] = -z[51] + z[132];
z[51] = abb[49] * z[51];
z[133] = -abb[73] * z[125];
z[134] = -abb[48] * z[31];
z[133] = z[133] + z[134];
z[133] = abb[25] * z[133];
z[135] = abb[4] + -abb[6];
z[37] = abb[63] + z[37];
z[37] = abb[48] * z[37];
z[135] = z[37] * z[135];
z[136] = -abb[48] * abb[61];
z[37] = -z[37] + z[136];
z[136] = abb[48] * abb[64];
z[37] = 2 * z[37] + z[136];
z[37] = abb[3] * z[37];
z[136] = abb[50] * z[46];
z[137] = -z[134] + z[136];
z[137] = abb[27] * z[137];
z[138] = abb[62] * abb[73];
z[134] = -z[134] + z[138];
z[136] = -z[134] + z[136];
z[136] = abb[23] * z[136];
z[138] = -abb[26] * z[134];
z[31] = -abb[47] * z[31];
z[31] = z[31] + -z[134];
z[31] = abb[24] * z[31];
z[19] = z[19] + z[31] + z[37] + z[51] + z[82] + z[103] + z[104] + z[106] + z[123] + z[131] + z[133] + z[135] + z[136] + z[137] + z[138];
z[19] = 4 * z[19];
z[31] = z[40] + z[115];
z[37] = 2 * z[54];
z[51] = -z[37] + z[78];
z[54] = abb[16] * z[75];
z[78] = abb[3] * z[33];
z[78] = -z[31] + z[45] + -z[51] + z[54] + -z[70] + z[78] + -z[119];
z[78] = abb[38] * z[78];
z[30] = -abb[63] + z[30];
z[30] = abb[6] * z[30];
z[82] = z[6] + z[28];
z[103] = z[58] + z[82];
z[103] = abb[7] * z[103];
z[104] = abb[60] + abb[61] + -abb[63];
z[104] = abb[8] * z[104];
z[106] = abb[3] * z[75];
z[30] = z[30] + -6 * z[38] + z[81] + -z[91] + z[103] + z[104] + -z[106];
z[43] = -z[43] + z[54];
z[103] = z[30] + z[43];
z[103] = abb[39] * z[103];
z[82] = z[22] + z[82];
z[82] = abb[3] * z[82];
z[106] = abb[8] * z[8];
z[106] = z[102] + z[106];
z[123] = -z[81] + z[84];
z[131] = abb[6] * z[6];
z[131] = -z[123] + z[131];
z[133] = abb[5] * z[6];
z[82] = -z[37] + -z[82] + z[88] + z[106] + z[116] + -z[131] + z[133];
z[116] = abb[42] * z[82];
z[134] = -abb[4] * z[23];
z[34] = z[10] + z[34] + -z[102] + z[112] + z[114] + z[118] + z[134];
z[34] = abb[69] * z[34];
z[72] = z[72] + z[122];
z[72] = abb[66] * z[72];
z[62] = abb[67] * z[62];
z[13] = abb[72] * z[13];
z[102] = abb[8] * z[23];
z[100] = -z[48] + -z[100] + z[102] + -z[123];
z[102] = z[37] + -z[92];
z[112] = abb[3] * abb[61];
z[114] = abb[4] * z[6];
z[98] = z[98] + z[100] + z[102] + z[112] + -z[114];
z[112] = abb[43] * z[98];
z[118] = -z[45] + z[50] + -z[132];
z[118] = abb[70] * z[118];
z[48] = -z[48] + z[108] + z[126];
z[48] = abb[71] * z[48];
z[108] = -z[41] + -z[129];
z[108] = abb[68] * z[108];
z[51] = z[51] + z[96];
z[24] = abb[3] * z[24];
z[24] = z[24] + -z[51] + z[120];
z[96] = abb[41] * z[24];
z[70] = z[70] + z[86];
z[86] = abb[3] * z[79];
z[86] = -z[70] + z[74] + z[86] + z[102];
z[120] = abb[40] * z[86];
z[122] = abb[38] + abb[66];
z[122] = z[46] * z[122];
z[123] = -abb[39] * z[42];
z[126] = abb[52] * abb[62];
z[122] = z[122] + z[123] + -z[126];
z[122] = abb[24] * z[122];
z[123] = -abb[25] * abb[52] * z[125];
z[46] = -abb[70] * z[46];
z[46] = z[46] + -z[126];
z[46] = abb[23] * z[46];
z[126] = -abb[26] * z[126];
z[13] = -z[13] + -z[34] + -z[46] + -z[48] + -z[62] + -z[72] + -z[78] + z[96] + -z[103] + -z[108] + z[112] + -z[116] + -z[118] + z[120] + -z[122] + -z[123] + -z[126];
z[34] = 3 * abb[62];
z[46] = -abb[61] + z[34] + -z[66] + -z[68];
z[46] = abb[6] * z[46];
z[48] = 2 * z[8];
z[62] = abb[15] * z[48];
z[47] = 2 * z[47] + z[62];
z[38] = 2 * z[38];
z[62] = 2 * abb[8];
z[27] = z[27] * z[62];
z[0] = abb[57] + -3 * abb[59] + -2 * z[0] + z[1];
z[0] = abb[24] * z[0];
z[1] = -abb[7] * z[77];
z[0] = z[0] + z[1] + z[27] + -10 * z[35] + z[38] + z[40] + z[46] + z[47] + -z[50] + -4 * z[53] + z[54] + -z[59] + z[60] + 2 * z[65];
z[0] = abb[32] * z[0];
z[1] = 2 * z[55];
z[27] = z[66] + z[79];
z[27] = abb[6] * z[27];
z[27] = z[1] + z[27] + z[36] + z[52] + z[80] + -z[85] + -z[90] + -z[92];
z[27] = abb[36] * z[27];
z[1] = z[1] + -z[26] + -z[41] + -z[87] + z[95] + z[127];
z[1] = abb[31] * z[1];
z[26] = abb[24] * z[42];
z[26] = z[26] + -z[43];
z[36] = z[26] + -z[49];
z[40] = z[36] + z[38] + -z[92] + -z[104] + -z[107] + -z[130];
z[40] = abb[35] * z[40];
z[41] = abb[3] * z[48];
z[36] = z[36] + z[41] + -z[64] + z[76] + z[102];
z[41] = abb[30] * z[36];
z[1] = z[1] + z[27] + z[40] + z[41];
z[0] = z[0] + 2 * z[1];
z[0] = abb[32] * z[0];
z[1] = -z[7] + -z[22] + -z[28];
z[1] = abb[3] * z[1];
z[7] = -z[92] + z[131];
z[27] = -abb[64] + z[8];
z[27] = abb[8] * z[27];
z[1] = z[1] + -z[7] + z[12] + -z[14] + -z[18] + z[27] + z[32] + z[54] + z[57] + -z[71] + -z[114] + -z[133];
z[1] = abb[30] * z[1];
z[12] = 2 * z[121];
z[1] = z[1] + z[12];
z[1] = abb[30] * z[1];
z[2] = abb[23] * z[2];
z[18] = z[47] + 2 * z[73];
z[2] = z[2] + z[3] + 4 * z[11] + 3 * z[17] + z[18] + -z[57];
z[3] = -4 * z[9] + 3 * z[14];
z[27] = -3 * abb[64] + 4 * z[6];
z[8] = -z[8] + -z[27];
z[8] = abb[3] * z[8];
z[40] = z[75] + z[94];
z[40] = abb[8] * z[40];
z[28] = abb[5] * z[28];
z[41] = -abb[14] * z[22];
z[8] = -z[2] + -z[3] + z[8] + z[28] + z[40] + z[41] + -z[43] + z[63] + -z[89];
z[8] = abb[30] * z[8];
z[17] = -z[17] + z[39];
z[28] = -3 * z[83] + z[94];
z[28] = abb[3] * z[28];
z[39] = -abb[4] + -abb[5];
z[39] = abb[61] * z[39];
z[40] = -abb[64] + -z[23];
z[40] = abb[8] * z[40];
z[4] = z[4] + -z[7] + -z[10] + -z[15] + z[17] + z[28] + z[39] + z[40] + z[44];
z[4] = abb[34] * z[4];
z[7] = abb[8] * z[125];
z[7] = z[7] + 2 * z[25] + -z[43];
z[10] = -z[32] + z[117];
z[10] = -2 * z[10];
z[15] = 5 * abb[61] + z[27] + z[34];
z[15] = abb[3] * z[15];
z[25] = abb[7] * z[48];
z[3] = z[3] + z[5] + z[7] + -z[10] + z[15] + -z[17] + -z[25] + z[91];
z[3] = abb[35] * z[3];
z[5] = abb[5] * abb[61];
z[5] = z[5] + -z[74] + z[88] + z[97] + z[100];
z[5] = abb[36] * z[5];
z[15] = abb[32] * z[36];
z[5] = z[5] + -z[15] + z[111];
z[15] = abb[4] + -abb[14];
z[15] = abb[61] * z[15];
z[15] = z[15] + z[81] + z[101] + -z[106];
z[17] = 2 * abb[31];
z[15] = z[15] * z[17];
z[3] = z[3] + z[4] + 2 * z[5] + z[8] + z[12] + z[15];
z[3] = abb[34] * z[3];
z[4] = abb[5] * z[29];
z[5] = -abb[61] + -2 * z[23];
z[5] = abb[4] * z[5];
z[4] = -z[4] + -z[5] + z[57] + -z[84] + z[93];
z[5] = z[9] + -z[11];
z[8] = -abb[60] + -8 * abb[63];
z[8] = abb[64] * (T(10) / T(3)) + (T(1) / T(3)) * z[8] + -z[58];
z[8] = z[8] * z[62];
z[9] = abb[64] * (T(-2) / T(3)) + abb[62] * (T(1) / T(3)) + z[33];
z[9] = abb[3] * z[9];
z[6] = -6 * abb[62] + (T(1) / T(3)) * z[6];
z[6] = abb[6] * z[6];
z[11] = abb[13] * z[21];
z[4] = (T(-1) / T(3)) * z[4] + 6 * z[5] + z[6] + z[8] + z[9] + (T(14) / T(3)) * z[11] + (T(4) / T(3)) * z[35] + (T(2) / T(3)) * z[109];
z[5] = prod_pow(m1_set::bc<T>[0], 2);
z[4] = z[4] * z[5];
z[6] = abb[30] * z[82];
z[8] = abb[33] * z[24];
z[6] = z[6] + z[8];
z[8] = abb[31] * z[82];
z[6] = 2 * z[6] + z[8];
z[6] = abb[31] * z[6];
z[8] = -z[16] + -z[22];
z[8] = abb[3] * z[8];
z[8] = z[8] + -z[18] + z[25] + -z[37] + z[70] + -2 * z[71] + z[105] + -z[109];
z[8] = abb[37] * z[8];
z[9] = abb[30] + -abb[35];
z[9] = z[9] * z[110];
z[11] = abb[36] * z[86];
z[9] = z[9] + z[11];
z[8] = z[8] + 2 * z[9];
z[8] = abb[37] * z[8];
z[9] = -z[26] + z[30];
z[9] = abb[35] * z[9];
z[11] = -abb[64] + z[69];
z[15] = abb[3] * z[11];
z[16] = 2 * z[119];
z[2] = z[2] + -z[7] + z[14] + z[15] + -z[16];
z[2] = abb[30] * z[2];
z[7] = abb[3] * z[23];
z[7] = z[7] + z[38] + -z[115] + z[128];
z[7] = z[7] * z[17];
z[2] = z[2] + z[7] + z[9] + -z[12];
z[2] = abb[35] * z[2];
z[7] = abb[28] * z[42];
z[9] = -abb[60] + -z[56] + z[94];
z[9] = abb[21] * z[9];
z[12] = -abb[18] * z[125];
z[11] = -abb[19] * z[11];
z[14] = -abb[0] + -abb[3] + -2 * abb[17] + 8 * abb[29] + -z[62];
z[14] = abb[65] * z[14];
z[15] = abb[20] * z[75];
z[7] = z[7] + z[9] + z[11] + z[12] + z[14] + z[15];
z[7] = abb[44] * z[7];
z[9] = abb[64] + -z[61];
z[9] = abb[3] * z[9];
z[9] = z[9] + z[10] + z[16] + 2 * z[31] + z[51] + -z[113];
z[9] = prod_pow(abb[33], 2) * z[9];
z[10] = -abb[30] * z[98];
z[11] = -abb[6] * z[67];
z[11] = z[11] + z[38] + z[45] + -z[53] + -z[81];
z[11] = abb[35] * z[11];
z[12] = -abb[60] + z[56];
z[12] = abb[8] * z[12];
z[12] = z[12] + -z[20] + -z[101] + -z[124];
z[12] = abb[31] * z[12];
z[10] = z[10] + z[11] + z[12];
z[11] = -abb[36] * z[98];
z[10] = 2 * z[10] + z[11];
z[10] = abb[36] * z[10];
z[5] = -abb[72] + (T(-5) / T(3)) * z[5];
z[5] = z[5] * z[99];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + 4 * z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + -2 * z[13];
z[0] = 2 * z[0];
return {z[19], z[0]};
}


template <typename T> std::complex<T> f_4_696_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("-12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("24.868723824175411697649691909283439733663559733331259852651735776"),stof<T>("61.853923650294623273177281241428735261702087835361376970798803989")}, std::complex<T>{stof<T>("11.3490400813821118878519245809897379499177073529474069186036510587"),stof<T>("5.3422663012062552210335102931708464582582251319146652442346114126")}, std::complex<T>{stof<T>("104.622377029514873099286803637880724222635172276139885411146588596"),stof<T>("14.076926827520740306946172564005013825634795875809231625705708537")}, std::complex<T>{stof<T>("13.410365629505712410908358187884871427155500825183520480789768615"),stof<T>("22.488865810016666914521174577880645041077791437494176369188107556")}, std::complex<T>{stof<T>("25.717377079093476004935563626283305900062106057101915024970501794"),stof<T>("1.621278223372334221714645070576243103741431147346828003280808728")}, std::complex<T>{stof<T>("-28.764267386167936168123426742923129359233536971631414979444392175"),stof<T>("35.331490603189355645543117480377120220136522312535150472767506973")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_696_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_696_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.468767921412444390913041726294227503092108287975746296968868452"),stof<T>("67.736248468690538042086193888703719615872133654612685166765616267")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,74> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_27_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_27_re(k)};

                    
            return f_4_696_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_696_DLogXconstant_part(base_point<T>, kend);
	value += f_4_696_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_696_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_696_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_696_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_696_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_696_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_696_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
