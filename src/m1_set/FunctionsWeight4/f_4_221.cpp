/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_221.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_221_abbreviated (const std::array<T,65>& abb) {
T z[133];
z[0] = 6 * abb[55];
z[1] = -abb[53] + z[0];
z[2] = 4 * abb[45];
z[3] = -abb[54] + z[2];
z[4] = z[1] + -z[3];
z[5] = 3 * abb[49];
z[6] = 5 * abb[52] + z[5];
z[7] = 2 * abb[46];
z[8] = z[6] + -z[7];
z[9] = 5 * abb[47];
z[10] = 2 * abb[51];
z[11] = 2 * abb[50];
z[12] = abb[44] + abb[48];
z[13] = -z[4] + z[8] + z[9] + -z[10] + -z[11] + z[12];
z[13] = abb[16] * z[13];
z[14] = abb[49] + -abb[54];
z[15] = 5 * abb[45];
z[16] = z[14] + z[15];
z[17] = 3 * abb[48];
z[18] = 4 * abb[47];
z[16] = -abb[44] + -z[1] + 2 * z[16] + z[17] + z[18];
z[16] = abb[6] * z[16];
z[19] = 2 * abb[45];
z[20] = abb[49] + z[19];
z[21] = abb[53] + z[12];
z[22] = 2 * abb[47];
z[20] = 2 * z[20] + -z[21] + z[22];
z[20] = abb[17] * z[20];
z[23] = abb[45] + -abb[52];
z[24] = 2 * abb[49];
z[25] = 4 * abb[50] + -z[7] + -5 * z[23] + z[24];
z[25] = abb[10] * z[25];
z[26] = abb[44] + -abb[52];
z[27] = z[10] + z[26];
z[28] = 16 * abb[45] + 10 * abb[47] + 5 * abb[49] + -4 * abb[54] + -z[0] + -z[11] + z[27];
z[28] = abb[2] * z[28];
z[29] = -abb[56] + abb[57];
z[30] = abb[22] * z[29];
z[31] = abb[58] + abb[59] + abb[60] + abb[61];
z[32] = abb[27] * z[31];
z[30] = z[30] + z[32];
z[33] = abb[47] + abb[49];
z[34] = -abb[53] + z[33];
z[35] = -abb[48] + z[34];
z[36] = -abb[54] + z[11];
z[37] = z[35] + -z[36];
z[38] = 2 * abb[52];
z[39] = z[37] + z[38];
z[39] = abb[3] * z[39];
z[40] = abb[3] + -abb[8];
z[40] = abb[46] * z[40];
z[39] = z[39] + -z[40];
z[41] = abb[19] * z[29];
z[42] = abb[24] * z[31];
z[43] = z[41] + -z[42];
z[44] = -abb[45] + abb[54];
z[45] = abb[49] + z[22];
z[46] = -2 * z[44] + z[45];
z[46] = abb[8] * z[46];
z[47] = abb[47] + -abb[54];
z[48] = abb[44] + z[47];
z[49] = abb[51] + z[23] + -z[48];
z[49] = abb[0] * z[49];
z[50] = abb[8] * z[11];
z[51] = 2 * abb[8];
z[52] = -5 * abb[10] + -z[51];
z[52] = abb[51] * z[52];
z[16] = -z[13] + z[16] + -z[20] + z[25] + z[28] + z[30] + z[39] + -z[43] + z[46] + z[49] + z[50] + z[52];
z[16] = abb[36] * z[16];
z[25] = 3 * abb[55];
z[28] = -abb[53] + z[25];
z[46] = 2 * abb[48];
z[49] = z[28] + -z[46];
z[52] = 3 * abb[45];
z[53] = -z[47] + z[49] + -z[52];
z[54] = 2 * abb[10];
z[53] = z[53] * z[54];
z[55] = z[19] + z[47];
z[56] = abb[48] + z[55];
z[57] = 2 * abb[55];
z[58] = -abb[53] + z[57];
z[59] = z[56] + -z[58];
z[60] = abb[16] * z[59];
z[53] = z[53] + -z[60];
z[61] = abb[48] * (T(1) / T(2));
z[62] = abb[53] * (T(1) / T(2));
z[63] = z[61] + z[62];
z[64] = abb[44] * (T(1) / T(2));
z[65] = z[63] + z[64];
z[66] = -abb[49] + z[65];
z[67] = abb[47] + z[19];
z[68] = z[66] + -z[67];
z[69] = abb[17] * z[68];
z[70] = (T(1) / T(2)) * z[29];
z[71] = abb[22] * z[70];
z[32] = (T(1) / T(2)) * z[32] + z[69] + z[71];
z[69] = abb[23] * z[31];
z[71] = -z[32] + (T(1) / T(2)) * z[69];
z[72] = -z[33] + z[46] + -z[58];
z[73] = 2 * abb[18];
z[73] = z[72] * z[73];
z[74] = 4 * abb[55];
z[5] = 2 * z[3] + z[5] + -z[74];
z[9] = -z[5] + -z[9] + -z[65];
z[9] = abb[6] * z[9];
z[75] = 3 * abb[47];
z[24] = z[24] + z[75];
z[76] = -abb[54] + z[19];
z[77] = abb[44] + z[76];
z[78] = z[24] + -z[46] + z[77];
z[79] = -abb[8] * z[78];
z[80] = z[55] + z[61] + -z[64];
z[81] = abb[53] * (T(3) / T(2)) + -z[57] + z[80];
z[81] = abb[5] * z[81];
z[82] = abb[26] * z[31];
z[83] = -z[42] + z[82];
z[83] = (T(1) / T(2)) * z[83];
z[84] = abb[20] * z[70];
z[85] = -abb[55] + z[12];
z[86] = abb[14] * z[85];
z[87] = 2 * z[86];
z[84] = z[84] + z[87];
z[88] = -abb[55] + z[45];
z[89] = -abb[54] + z[52];
z[90] = z[88] + z[89];
z[91] = abb[2] * z[90];
z[92] = abb[21] * z[70];
z[93] = -abb[53] + z[12];
z[94] = abb[15] * z[93];
z[92] = -z[92] + (T(1) / T(2)) * z[94];
z[95] = (T(3) / T(2)) * z[41] + -z[92];
z[67] = abb[49] + z[67];
z[96] = z[58] + -z[67];
z[96] = abb[0] * z[96];
z[9] = z[9] + -z[53] + z[71] + -z[73] + z[79] + z[81] + z[83] + z[84] + -4 * z[91] + z[95] + z[96];
z[9] = abb[30] * z[9];
z[79] = -z[12] + z[58];
z[81] = abb[5] * z[79];
z[96] = abb[20] * z[29];
z[97] = -z[82] + z[96];
z[81] = z[73] + z[81] + z[97];
z[98] = abb[25] * z[31];
z[99] = z[81] + -z[98];
z[20] = -z[20] + z[30] + -z[69];
z[30] = z[17] + -z[58];
z[100] = -z[24] + z[30] + -z[76];
z[101] = abb[8] * z[100];
z[102] = 4 * abb[6];
z[103] = z[90] * z[102];
z[48] = abb[0] * z[48];
z[104] = abb[45] + z[47];
z[105] = 2 * z[104];
z[106] = abb[13] * z[105];
z[48] = z[20] + -z[48] + z[53] + 6 * z[91] + z[99] + -z[101] + z[103] + -z[106];
z[48] = abb[35] * z[48];
z[53] = z[61] + -z[62];
z[91] = abb[44] * (T(3) / T(2));
z[101] = z[53] + -z[55] + z[91];
z[101] = abb[5] * z[101];
z[103] = 2 * abb[2];
z[90] = z[90] * z[103];
z[107] = z[90] + -z[106];
z[71] = z[71] + (T(-3) / T(2)) * z[96] + z[101] + -z[107];
z[101] = z[45] + -z[58] + z[77];
z[101] = abb[0] * z[101];
z[75] = z[75] + 2 * z[76];
z[66] = -z[66] + z[75];
z[66] = abb[6] * z[66];
z[76] = abb[8] * z[79];
z[108] = z[87] + -z[98];
z[109] = (T(1) / T(2)) * z[42];
z[66] = z[66] + -z[71] + z[76] + (T(-1) / T(2)) * z[82] + z[95] + -z[101] + z[108] + -z[109];
z[76] = -abb[34] * z[66];
z[24] = -abb[54] + z[24];
z[95] = 7 * abb[48];
z[101] = 2 * abb[44];
z[1] = -z[1] + -z[24] + z[95] + z[101];
z[110] = abb[7] * z[1];
z[81] = -z[81] + z[110];
z[78] = abb[0] * z[78];
z[111] = -abb[44] + z[57];
z[112] = abb[45] + z[46];
z[113] = z[111] + -z[112];
z[51] = z[51] * z[113];
z[102] = z[85] * z[102];
z[51] = -z[51] + -z[60] + z[78] + -z[81] + z[102];
z[78] = abb[32] + -abb[33];
z[51] = z[51] * z[78];
z[2] = -z[2] + -z[24] + z[111];
z[2] = abb[0] * z[2];
z[24] = 2 * abb[6];
z[102] = -abb[44] + abb[45];
z[102] = z[24] * z[102];
z[85] = -abb[8] * z[85];
z[85] = z[85] + z[102];
z[102] = -abb[44] + z[55];
z[111] = -abb[5] * z[102];
z[2] = z[2] + 2 * z[85] + z[87] + z[90] + -z[96] + z[111];
z[2] = abb[31] * z[2];
z[2] = z[2] + z[9] + z[48] + z[51] + z[76];
z[2] = abb[31] * z[2];
z[9] = -z[57] + z[65];
z[51] = abb[52] * (T(1) / T(2));
z[76] = abb[49] * (T(1) / T(2));
z[85] = z[51] + z[76];
z[111] = abb[47] * (T(3) / T(2));
z[113] = abb[54] * (T(1) / T(2));
z[114] = z[111] + -z[113];
z[115] = abb[50] + -z[9] + -z[19] + -z[85] + -z[114];
z[115] = abb[16] * z[115];
z[72] = abb[18] * z[72];
z[116] = -abb[53] + abb[55];
z[117] = abb[5] * z[116];
z[118] = z[72] + z[117];
z[89] = abb[49] + z[89];
z[89] = z[18] + 2 * z[89];
z[119] = -z[25] + z[65] + z[89];
z[119] = abb[6] * z[119];
z[120] = abb[49] * (T(3) / T(2));
z[121] = z[64] + z[120];
z[22] = z[22] + -z[46];
z[122] = abb[45] + abb[55] + z[22] + -z[51] + z[121];
z[122] = abb[8] * z[122];
z[123] = 2 * abb[54] + -z[11];
z[124] = -z[45] + z[123];
z[15] = abb[52] + -z[15] + z[49] + z[124];
z[15] = abb[10] * z[15];
z[5] = abb[44] + 6 * abb[47] + abb[53] + z[5] + -z[11];
z[5] = abb[2] * z[5];
z[125] = abb[47] * (T(1) / T(2));
z[126] = -z[113] + z[125];
z[127] = abb[45] + z[126];
z[128] = abb[50] + -z[51] + z[76] + z[127];
z[128] = abb[0] * z[128];
z[129] = abb[19] * z[70];
z[130] = abb[8] * abb[50];
z[5] = z[5] + z[15] + -z[106] + z[109] + z[115] + z[118] + z[119] + z[122] + z[128] + -z[129] + -z[130];
z[5] = abb[35] * z[5];
z[15] = abb[51] + abb[55];
z[109] = abb[46] + z[15] + -z[33] + -z[38];
z[109] = abb[16] * z[109];
z[115] = abb[46] + z[49];
z[119] = -z[38] + z[115];
z[122] = -abb[10] * z[119];
z[23] = abb[46] + z[23];
z[23] = abb[11] * z[23];
z[112] = -abb[55] + z[112];
z[112] = abb[8] * z[112];
z[128] = abb[8] + abb[10];
z[131] = -abb[51] * z[128];
z[132] = -abb[52] + abb[55];
z[132] = abb[2] * z[132];
z[112] = -z[23] + z[109] + z[112] + -z[118] + z[122] + z[131] + z[132];
z[112] = abb[33] * z[112];
z[5] = z[5] + 2 * z[112];
z[5] = abb[35] * z[5];
z[112] = -abb[50] + z[125];
z[76] = abb[52] + z[76];
z[113] = -z[63] + z[76] + z[112] + z[113];
z[113] = abb[3] * z[113];
z[40] = (T(-1) / T(2)) * z[40] + z[113] + z[118];
z[113] = abb[48] * (T(3) / T(2));
z[14] = -abb[53] + z[14];
z[14] = abb[55] + (T(1) / T(2)) * z[14] + -z[113] + z[125];
z[14] = abb[8] * z[14];
z[3] = -abb[44] + z[3] + z[88];
z[3] = abb[6] * z[3];
z[118] = abb[49] + z[11];
z[49] = -abb[45] + -abb[52] + z[49] + z[118];
z[49] = abb[10] * z[49];
z[122] = abb[46] + z[26];
z[122] = abb[9] * z[122];
z[125] = -abb[52] + abb[53] + z[104];
z[131] = -abb[0] * z[125];
z[3] = z[3] + z[14] + z[32] + z[40] + z[49] + z[90] + -z[92] + (T(-1) / T(2)) * z[98] + z[122] + z[130] + z[131];
z[3] = abb[30] * z[3];
z[14] = abb[49] + z[75];
z[9] = z[9] + z[14];
z[9] = abb[6] * z[9];
z[32] = abb[8] * z[102];
z[34] = -z[34] + -z[101];
z[34] = abb[0] * z[34];
z[9] = z[9] + z[32] + z[34] + -z[60] + -z[71] + -z[83] + z[92] + z[129];
z[9] = abb[34] * z[9];
z[32] = 2 * abb[53];
z[34] = z[32] + -z[57] + z[102];
z[34] = abb[0] * z[34];
z[49] = abb[6] * z[79];
z[49] = z[43] + z[49];
z[60] = z[49] + z[60];
z[71] = abb[21] * z[29];
z[71] = z[71] + -z[94];
z[75] = abb[8] * z[59];
z[34] = z[34] + -z[60] + -z[71] + z[75] + -z[108];
z[34] = abb[32] * z[34];
z[75] = -z[18] + z[74];
z[83] = z[11] + -z[19];
z[90] = z[10] + z[83];
z[92] = -abb[44] + z[90];
z[8] = -z[8] + z[75] + z[92];
z[8] = abb[16] * z[8];
z[49] = z[8] + -z[49];
z[94] = z[49] + -z[99];
z[10] = z[10] * z[128];
z[108] = -z[10] + z[94];
z[128] = z[26] + z[30];
z[129] = -z[55] + -z[128];
z[129] = abb[8] * z[129];
z[39] = z[39] + z[69];
z[119] = z[54] * z[119];
z[83] = -z[45] + z[83];
z[131] = -z[26] + z[83];
z[131] = abb[0] * z[131];
z[119] = -z[39] + -z[108] + z[119] + z[129] + z[131];
z[119] = abb[33] * z[119];
z[3] = z[3] + z[9] + z[34] + -z[48] + z[119];
z[3] = abb[30] * z[3];
z[0] = -z[0] + z[32] + z[45];
z[9] = -z[38] + z[44];
z[48] = 4 * abb[48];
z[11] = z[11] + z[48];
z[9] = -abb[46] + z[0] + -2 * z[9] + z[11];
z[9] = abb[10] * z[9];
z[9] = z[9] + z[39] + z[42] + z[130];
z[30] = abb[47] + -z[30] + z[44] + z[51] + z[121];
z[30] = abb[8] * z[30];
z[100] = -abb[6] * z[100];
z[119] = z[19] + -z[27] + -z[124];
z[119] = abb[2] * z[119];
z[52] = -abb[46] + -3 * abb[50] + -abb[51] + abb[49] * (T(5) / T(2)) + abb[52] * (T(7) / T(2)) + z[52] + -z[75] + z[91];
z[52] = abb[16] * z[52];
z[75] = z[82] + z[98];
z[59] = -abb[5] * z[59];
z[85] = -abb[45] + abb[50] + -abb[51] + z[64] + z[85];
z[85] = abb[0] * z[85];
z[121] = 4 * abb[10];
z[124] = abb[8] + z[121];
z[124] = abb[51] * z[124];
z[30] = -z[9] + z[30] + z[52] + z[59] + z[73] + -z[75] + z[85] + z[100] + z[119] + -z[122] + z[124];
z[30] = abb[40] * z[30];
z[21] = -z[21] + z[89];
z[21] = abb[6] * z[21];
z[14] = -abb[53] + z[14];
z[14] = z[14] * z[103];
z[52] = -abb[53] + z[56];
z[56] = abb[4] + -abb[8];
z[56] = z[52] * z[56];
z[59] = abb[0] * z[105];
z[85] = abb[5] * z[93];
z[14] = z[14] + z[20] + z[21] + z[43] + -z[56] + -z[59] + -z[85] + z[97] + -z[98];
z[21] = abb[63] * z[14];
z[56] = 3 * abb[52];
z[59] = z[56] + -z[57];
z[89] = -abb[45] + z[59] + z[62] + z[113] + z[126];
z[89] = abb[8] * z[89];
z[69] = z[69] + z[98];
z[97] = 2 * z[23];
z[100] = abb[8] * abb[51];
z[58] = -abb[52] + z[58];
z[58] = abb[2] * z[58];
z[105] = -abb[48] + abb[51];
z[105] = abb[0] * z[105];
z[40] = z[40] + z[58] + (T(1) / T(2)) * z[69] + z[89] + z[97] + (T(-7) / T(2)) * z[100] + z[105] + -z[109];
z[58] = prod_pow(abb[33], 2);
z[40] = z[40] * z[58];
z[69] = abb[46] + abb[50];
z[6] = abb[53] + -abb[54] + -z[6];
z[6] = (T(1) / T(2)) * z[6] + z[15] + z[61] + -z[64] + z[69] + -z[111];
z[6] = abb[16] * z[6];
z[15] = abb[49] + abb[54] + z[56];
z[12] = -abb[51] + z[12] + (T(1) / T(2)) * z[15] + -z[57] + z[112];
z[12] = abb[0] * z[12];
z[15] = abb[44] + -abb[49] + abb[48] * (T(7) / T(2)) + -z[25] + z[62] + -z[114];
z[25] = abb[7] * z[15];
z[61] = abb[8] * z[26];
z[61] = z[61] + z[82];
z[64] = z[61] + z[100];
z[65] = -abb[55] + z[65];
z[65] = abb[5] * z[65];
z[89] = abb[51] + z[26];
z[89] = abb[1] * z[89];
z[6] = z[6] + z[12] + z[25] + (T(1) / T(2)) * z[64] + z[65] + -z[72] + -z[84] + 3 * z[89];
z[12] = prod_pow(abb[32], 2);
z[6] = z[6] * z[12];
z[25] = abb[6] * z[1];
z[4] = -abb[47] + -z[4] + z[26] + z[95] + -z[120];
z[4] = abb[8] * z[4];
z[27] = z[27] + -z[46] + -z[83];
z[64] = abb[0] * z[27];
z[65] = abb[8] * (T(-1) / T(2)) + -z[121];
z[65] = abb[51] * z[65];
z[4] = z[4] + z[9] + -z[13] + z[25] + z[64] + z[65] + z[98] + -z[110];
z[4] = abb[38] * z[4];
z[9] = z[20] + z[41] + -z[42] + z[71] + -z[82] + -z[85] + z[96] + -2 * z[98];
z[13] = abb[47] * (T(2) / T(3));
z[25] = abb[50] * (T(13) / T(6));
z[42] = abb[55] * (T(11) / T(3));
z[51] = abb[45] * (T(1) / T(3)) + z[51];
z[51] = abb[44] * (T(-13) / T(12)) + abb[54] * (T(-4) / T(3)) + abb[49] * (T(5) / T(12)) + z[13] + z[25] + -z[42] + (T(13) / T(2)) * z[51];
z[51] = abb[2] * z[51];
z[64] = abb[45] + abb[54];
z[42] = abb[52] * (T(-13) / T(4)) + abb[53] * (T(-2) / T(3)) + abb[44] * (T(1) / T(4)) + z[42];
z[13] = abb[49] * (T(1) / T(4)) + z[13] + -z[25] + z[42] + (T(2) / T(3)) * z[64];
z[13] = abb[0] * z[13];
z[25] = z[89] + -z[100];
z[44] = abb[49] + -z[44];
z[44] = -abb[48] + -abb[53] + z[18] + 2 * z[44];
z[44] = abb[44] + (T(1) / T(3)) * z[44];
z[44] = abb[6] * z[44];
z[42] = -z[17] + z[42];
z[42] = abb[8] * z[42];
z[9] = (T(1) / T(3)) * z[9] + z[13] + (T(-13) / T(4)) * z[25] + z[42] + z[44] + z[51] + (T(11) / T(3)) * z[86];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[13] = -abb[53] + z[17] + -z[57];
z[17] = z[13] + -z[47] + z[101];
z[17] = abb[8] * z[17];
z[25] = abb[44] + abb[55] + z[35];
z[35] = 2 * abb[0];
z[25] = z[25] * z[35];
z[13] = 3 * abb[44] + z[13];
z[13] = abb[6] * z[13];
z[13] = z[13] + z[17] + z[25] + z[43] + z[87] + z[99];
z[17] = z[13] + z[71];
z[17] = abb[62] * z[17];
z[25] = abb[0] * z[102];
z[35] = -abb[6] * z[102];
z[42] = -z[103] * z[104];
z[35] = z[25] + z[35] + -z[41] + z[42] + z[106];
z[35] = abb[34] * z[35];
z[41] = z[103] * z[116];
z[25] = -z[25] + z[41] + z[60];
z[41] = -abb[33] + abb[35];
z[25] = z[25] * z[41];
z[25] = z[25] + -z[34] + z[35];
z[25] = abb[34] * z[25];
z[34] = abb[44] + abb[52];
z[35] = z[34] + z[37];
z[35] = abb[16] * z[35];
z[35] = z[35] + -z[39];
z[34] = abb[49] + -z[32] + z[34] + z[123];
z[34] = abb[2] * z[34];
z[37] = abb[5] * z[52];
z[39] = -abb[48] + z[125];
z[39] = abb[8] * z[39];
z[23] = z[23] + z[34] + -z[35] + z[37] + z[39] + z[75];
z[23] = abb[39] * z[23];
z[32] = abb[52] + -z[32] + z[33] + -z[36] + z[101];
z[32] = abb[0] * z[32];
z[32] = z[32] + -z[35] + z[61] + z[71];
z[32] = abb[37] * z[32];
z[31] = abb[28] * z[31];
z[34] = abb[6] * z[29];
z[35] = abb[21] * z[93];
z[31] = -z[31] + z[34] + z[35];
z[33] = z[33] + -z[63] + z[91];
z[33] = abb[19] * z[33];
z[34] = abb[8] + 2 * abb[29];
z[34] = z[29] * z[34];
z[35] = abb[22] * z[68];
z[37] = abb[15] * z[70];
z[31] = (T(1) / T(2)) * z[31] + -z[33] + z[34] + -z[35] + -z[37];
z[33] = abb[17] * z[70];
z[33] = -z[31] + z[33];
z[33] = abb[64] * z[33];
z[22] = -z[22] + z[69];
z[34] = abb[51] * (T(3) / T(2));
z[35] = -z[22] + z[34] + z[101] + z[120];
z[35] = abb[38] * z[35];
z[22] = abb[44] + z[22] + z[34] + -3 * z[76];
z[34] = -z[12] + z[58];
z[22] = z[22] * z[34];
z[34] = abb[36] + -abb[40];
z[27] = z[27] * z[34];
z[22] = z[22] + z[27] + z[35];
z[22] = abb[12] * z[22];
z[1] = -abb[62] * z[1];
z[15] = -z[15] * z[58];
z[1] = z[1] + z[15];
z[1] = abb[7] * z[1];
z[15] = z[53] + z[127];
z[27] = prod_pow(abb[35], 2);
z[27] = z[27] + -z[58];
z[15] = z[15] * z[27];
z[27] = z[41] * z[52];
z[34] = -abb[34] * z[27];
z[35] = -abb[39] * z[52];
z[15] = z[15] + z[34] + z[35];
z[15] = abb[4] * z[15];
z[34] = -z[62] + z[80];
z[35] = -abb[64] * z[34];
z[29] = -abb[37] * z[29];
z[29] = z[29] + z[35];
z[29] = abb[20] * z[29];
z[35] = abb[37] * z[93];
z[37] = -abb[64] * z[70];
z[35] = z[35] + z[37];
z[35] = abb[5] * z[35];
z[12] = 2 * abb[37] + abb[39] + -z[12];
z[12] = z[12] * z[122];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[9] + z[12] + z[15] + z[16] + z[17] + z[21] + z[22] + z[23] + z[25] + z[29] + z[30] + z[32] + z[33] + z[35] + z[40];
z[2] = -z[55] + z[56] + -z[115] + -z[118];
z[2] = z[2] * z[54];
z[2] = z[2] + -z[50];
z[3] = abb[44] + -z[59] + -z[83];
z[3] = abb[0] * z[3];
z[4] = z[77] + z[88];
z[4] = z[4] * z[24];
z[5] = z[26] + z[45];
z[6] = z[5] + z[46];
z[9] = z[6] + z[19];
z[9] = abb[8] * z[9];
z[12] = 2 * z[122];
z[3] = z[2] + z[3] + z[4] + z[8] + z[9] + -z[10] + -z[12] + -z[73] + z[87] + z[107] + -2 * z[117];
z[3] = abb[30] * z[3];
z[4] = -abb[31] + -abb[34];
z[4] = z[4] * z[66];
z[5] = -z[5] + -z[19] + z[123];
z[5] = abb[0] * z[5];
z[8] = -abb[54] + -z[67] + -z[128];
z[8] = abb[8] * z[8];
z[9] = abb[45] + -abb[53] + -z[26] + z[36];
z[9] = z[9] * z[103];
z[2] = -z[2] + z[5] + z[8] + z[9] + z[20] + z[97] + z[106] + -z[108];
z[2] = abb[35] * z[2];
z[5] = abb[44] + -7 * abb[52] + z[19] + -z[48] + z[74];
z[5] = abb[8] * z[5];
z[6] = z[6] + -z[90];
z[6] = abb[0] * z[6];
z[8] = -z[28] + z[38];
z[8] = z[8] * z[103];
z[5] = z[5] + z[6] + z[8] + z[49] + z[81] + -z[97] + 7 * z[100];
z[5] = abb[33] * z[5];
z[6] = z[71] + -z[110];
z[0] = -z[0] + -z[46] + -z[56] + z[92];
z[0] = abb[0] * z[0];
z[8] = abb[52] + -z[55] + z[79];
z[8] = abb[8] * z[8];
z[9] = -z[86] + z[89];
z[0] = z[0] + z[6] + z[8] + -6 * z[9] + z[12] + -z[94] + -z[100];
z[0] = abb[32] * z[0];
z[8] = abb[49] + -abb[51] + z[38];
z[7] = z[7] + -3 * z[8] + z[11] + -z[18] + z[101];
z[7] = abb[12] * z[7] * z[78];
z[8] = -abb[4] * z[27];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[7] + z[8];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[42] * z[14];
z[3] = z[6] + z[13];
z[3] = abb[41] * z[3];
z[4] = -abb[20] * z[34];
z[5] = -abb[5] + abb[17];
z[5] = z[5] * z[70];
z[4] = z[4] + z[5] + -z[31];
z[4] = abb[43] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_221_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.532805985296568558258419582201622705850006466615638653357974654"),stof<T>("28.062415966599068695583676699101002397539170265371126850982119055")}, std::complex<T>{stof<T>("-5.314358362566202221213433441950675357221404212869091682279619172"),stof<T>("-14.373241672832695586894414240062031359724011170323441565412025148")}, std::complex<T>{stof<T>("1.9594798130662163873989005553265251712627320171770225697128488332"),stof<T>("-4.1250878519596201870296527229978940872860288119180415307706102621")}, std::complex<T>{stof<T>("-0.129803804334678893203096132894538656179959869957744067954357991"),stof<T>("2.1465620572231758874228673563315264380987650849661997888578009263")}, std::complex<T>{stof<T>("-8.0372246038784373492790827841108266250910412317974933502374951302"),stof<T>("-6.6070098446596179841001461936864855331647478330222009177818013592")}, std::complex<T>{stof<T>("-1.5436644586460723025905989959005095631341155528533368917803892472"),stof<T>("-2.9317513597099414601712158769909011948659456181210215549736171654")}, std::complex<T>{stof<T>("18.144754066858149068905201000318787262198335924913404592168093821"),stof<T>("19.655691398047286199212022087388755513629744924392089337995959235")}, std::complex<T>{stof<T>("20.969660676523338494718863497830297021770986891178098711337749017"),stof<T>("25.206461292872376852567409896382725415033116273142749500238321767")}, std::complex<T>{stof<T>("8.0336745904168767669017214194211034946976743012417427290800703294"),stof<T>("3.5386620197414545874055401696019045534328421152535663193122884676")}, std::complex<T>{stof<T>("2.9218224286660744113546553976309690196603579241088665283397350529"),stof<T>("6.6126383472427798164134010891832985216066551560037079983624411642")}, std::complex<T>{stof<T>("-10.486237687740467943840103363165364538053323645352295119910078167"),stof<T>("-14.906159115956760447200094277016805389779583165283266012829397709")}, std::complex<T>{stof<T>("-24.303748446147996408504459090197558519166594349054900296452518855"),stof<T>("-21.693912603527431625085336361492647674755891281338759814387022612")}, std::complex<T>{stof<T>("7.7143564516654021477305372678719799821591283604003080270983573661"),stof<T>("7.2516632689748497845188292938738884372507784963632055609905540863")}, std::complex<T>{stof<T>("-7.7143564516654021477305372678719799821591283604003080270983573661"),stof<T>("-7.2516632689748497845188292938738884372507784963632055609905540863")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[78].real()/kbase.W[78].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_221_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_221_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-8.186415904781475370060485544048617112045245055520208162264429074"),stof<T>("-31.255760586685788672783464567668658030623360093831403754431504096")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,65> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W79(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[78].real()/k.W[78].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_221_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_221_DLogXconstant_part(base_point<T>, kend);
	value += f_4_221_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_221_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_221_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_221_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_221_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_221_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_221_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
