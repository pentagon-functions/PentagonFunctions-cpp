/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_767.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_767_abbreviated (const std::array<T,38>& abb) {
T z[64];
z[0] = 2 * abb[27];
z[1] = -abb[24] + abb[26];
z[2] = -abb[29] + z[0] + z[1];
z[3] = abb[1] * z[2];
z[4] = 2 * abb[28];
z[5] = -abb[29] + z[4];
z[6] = -z[1] + z[5];
z[7] = abb[2] * z[6];
z[8] = z[3] + z[7];
z[9] = 2 * abb[25] + -6 * abb[30];
z[10] = abb[24] + abb[26];
z[11] = z[9] + z[10];
z[12] = 3 * abb[28];
z[13] = 3 * abb[27];
z[14] = z[11] + z[12] + z[13];
z[15] = abb[7] * z[14];
z[15] = z[8] + z[15];
z[16] = 4 * abb[28];
z[17] = 3 * abb[29];
z[18] = z[0] + -z[1] + z[16] + -z[17];
z[19] = -abb[4] * z[18];
z[20] = 4 * abb[27];
z[21] = z[1] + z[4] + -z[17] + z[20];
z[22] = -abb[0] * z[21];
z[23] = abb[27] + abb[28];
z[24] = -abb[29] + z[23];
z[25] = 2 * abb[3];
z[26] = z[24] * z[25];
z[23] = 2 * abb[29] + z[11] + z[23];
z[27] = 2 * abb[5];
z[28] = z[23] * z[27];
z[29] = -abb[31] + -2 * abb[32] + abb[33];
z[30] = 2 * z[29];
z[31] = abb[10] * z[30];
z[32] = abb[11] * z[30];
z[33] = abb[9] * z[29];
z[15] = 2 * z[15] + z[19] + z[22] + -z[26] + -z[28] + z[31] + -z[32] + z[33];
z[15] = abb[13] * z[15];
z[19] = abb[3] * z[18];
z[22] = z[0] + z[9];
z[34] = abb[24] + z[4];
z[35] = abb[26] + abb[29];
z[36] = z[22] + z[34] + z[35];
z[37] = abb[4] * z[36];
z[37] = z[32] + z[37];
z[38] = -z[27] * z[36];
z[39] = z[11] + z[17];
z[40] = abb[7] * z[39];
z[41] = 2 * abb[26];
z[42] = z[13] + z[41];
z[43] = -abb[25] + 3 * abb[30];
z[44] = -abb[24] + z[42] + -z[43];
z[45] = 2 * abb[0];
z[44] = z[44] * z[45];
z[46] = abb[10] * z[29];
z[38] = z[8] + z[19] + -z[33] + -z[37] + z[38] + z[40] + z[44] + -z[46];
z[38] = abb[12] * z[38];
z[44] = z[9] + z[17];
z[47] = 3 * abb[26];
z[48] = z[0] + -z[34] + z[44] + z[47];
z[48] = abb[7] * z[48];
z[36] = abb[0] * z[36];
z[49] = z[6] * z[25];
z[50] = abb[4] * z[39];
z[30] = abb[9] * z[30];
z[51] = z[30] + -z[32];
z[36] = 4 * z[7] + -z[36] + z[48] + z[49] + z[50] + z[51];
z[36] = abb[15] * z[36];
z[49] = 3 * abb[24];
z[0] = abb[26] + z[0];
z[4] = -z[0] + z[4] + z[44] + z[49];
z[4] = abb[7] * z[4];
z[4] = 4 * z[3] + z[4] + -z[30] + -z[37];
z[30] = abb[0] * z[39];
z[39] = -z[28] + z[30];
z[44] = z[2] * z[25];
z[44] = z[4] + z[39] + z[44];
z[44] = abb[14] * z[44];
z[44] = z[36] + z[44];
z[15] = z[15] + z[38] + -z[44];
z[15] = abb[12] * z[15];
z[38] = 5 * abb[24];
z[50] = 6 * abb[25] + -18 * abb[30];
z[52] = 5 * abb[29] + z[50];
z[53] = 6 * abb[28];
z[54] = z[0] + z[38] + z[52] + z[53];
z[55] = abb[5] * z[54];
z[55] = z[31] + z[55];
z[10] = 3 * z[10] + z[16] + z[20] + z[52];
z[10] = abb[8] * z[10];
z[52] = 2 * z[10];
z[56] = -z[52] + z[55];
z[18] = abb[7] * z[18];
z[57] = -abb[24] + z[47];
z[58] = abb[29] + z[9];
z[20] = z[20] + z[57] + z[58];
z[20] = abb[0] * z[20];
z[59] = 5 * abb[28];
z[60] = z[9] + z[59];
z[13] = 2 * abb[24] + -abb[29] + z[13] + z[60];
z[13] = z[13] * z[25];
z[61] = -abb[4] * z[23];
z[51] = -5 * z[7] + -z[13] + 2 * z[18] + -z[20] + -z[51] + -z[56] + z[61];
z[51] = abb[18] * z[51];
z[61] = abb[11] * z[29];
z[62] = z[8] + -z[61];
z[63] = -abb[28] + z[43];
z[0] = -z[0] + z[63];
z[0] = abb[0] * z[0];
z[63] = -abb[24] + -abb[29] + z[63];
z[63] = abb[4] * z[63];
z[0] = z[0] + z[62] + -z[63];
z[26] = z[26] + z[40];
z[0] = 2 * z[0] + z[26] + z[33];
z[0] = abb[13] * z[0];
z[40] = -abb[27] + z[43];
z[34] = z[34] + -z[40];
z[34] = abb[4] * z[34];
z[40] = -z[35] + z[40];
z[40] = abb[0] * z[40];
z[34] = z[34] + z[40] + -z[62];
z[26] = -z[26] + z[28] + z[33] + 2 * z[34];
z[26] = abb[12] * z[26];
z[28] = abb[4] * z[6];
z[2] = abb[0] * z[2];
z[34] = -z[2] + z[8] + -z[28];
z[34] = abb[16] * z[34];
z[26] = -z[0] + z[26] + z[34] + z[44];
z[26] = abb[16] * z[26];
z[34] = -abb[26] + z[49];
z[40] = z[16] + z[34] + z[58];
z[40] = abb[4] * z[40];
z[23] = abb[0] * z[23];
z[23] = 5 * z[3] + z[23] + -z[32] + z[40] + -z[52];
z[23] = abb[17] * z[23];
z[22] = z[22] + -z[35] + z[49] + z[53];
z[22] = abb[4] * z[22];
z[32] = 6 * abb[27];
z[11] = z[11] + -z[17] + z[32] + z[53];
z[35] = abb[7] * z[11];
z[40] = 4 * abb[3];
z[43] = z[24] * z[40];
z[39] = z[22] + -z[31] + -z[35] + -z[39] + z[43];
z[39] = abb[13] * z[39];
z[44] = z[24] * z[27];
z[49] = -abb[3] * z[6];
z[44] = -z[2] + z[44] + z[46] + z[49];
z[44] = abb[14] * z[44];
z[39] = z[39] + z[44];
z[39] = abb[14] * z[39];
z[15] = z[15] + z[23] + z[26] + z[39] + z[51];
z[8] = -z[8] + z[10];
z[5] = -z[5] + -z[9] + -z[32] + -z[57];
z[5] = abb[0] * z[5];
z[23] = -z[27] * z[54];
z[11] = -z[11] * z[25];
z[5] = z[5] + 2 * z[8] + z[11] + -z[22] + z[23] + 3 * z[35];
z[5] = abb[19] * z[5];
z[8] = -z[3] + z[10] + z[35];
z[10] = -abb[27] + 7 * abb[28] + z[9] + z[38] + -z[47];
z[10] = abb[4] * z[10];
z[11] = -abb[24] + -5 * abb[26] + -8 * abb[27] + -z[16] + -z[17] + -z[50];
z[11] = abb[0] * z[11];
z[8] = -3 * z[7] + 2 * z[8] + z[10] + z[11] + -z[13] + -z[55];
z[8] = abb[13] * z[8];
z[10] = z[7] + z[56];
z[11] = z[14] * z[25];
z[13] = abb[28] + z[42] + z[58];
z[16] = z[13] * z[45];
z[22] = abb[4] * z[24];
z[11] = z[10] + z[11] + z[16] + z[22] + -z[35];
z[11] = abb[15] * z[11];
z[8] = z[8] + 2 * z[11];
z[8] = abb[13] * z[8];
z[11] = -abb[27] + -z[34] + -z[60];
z[11] = abb[4] * z[11];
z[13] = -z[13] * z[25];
z[10] = -z[10] + z[11] + z[13] + -z[30];
z[10] = prod_pow(abb[15], 2) * z[10];
z[5] = z[5] + z[8] + z[10];
z[8] = abb[3] * z[21];
z[10] = abb[7] * z[21];
z[11] = z[28] + -z[33];
z[13] = z[46] + z[61];
z[8] = 2 * z[3] + z[8] + -z[10] + z[11] + -z[13];
z[8] = 16 * z[8];
z[16] = abb[35] * z[8];
z[13] = z[2] + 2 * z[7] + -z[13] + -z[18] + z[19];
z[13] = 16 * z[13];
z[18] = -abb[34] * z[13];
z[19] = abb[3] + -abb[7];
z[19] = (T(16) / T(3)) * z[19];
z[1] = abb[27] + -abb[28] + z[1];
z[19] = z[1] * z[19];
z[19] = -7 * z[2] + (T(29) / T(3)) * z[3] + -z[7] + z[19] + (T(-5) / T(3)) * z[28] + (T(-8) / T(3)) * z[33];
z[19] = prod_pow(m1_set::bc<T>[0], 2) * z[19];
z[12] = 5 * abb[27] + -abb[29] + z[9] + z[12] + z[41];
z[12] = abb[3] * z[12];
z[10] = z[10] + -z[12] + -z[46];
z[12] = 6 * abb[24] + -abb[27] + 7 * abb[29] + z[50] + z[59];
z[12] = abb[5] * z[12];
z[10] = -16 * z[10] + 8 * z[12];
z[10] = abb[17] * z[10];
z[12] = -abb[10] + abb[11];
z[21] = 2 * z[1];
z[12] = z[12] * z[21];
z[22] = abb[0] + -abb[4];
z[22] = z[22] * z[29];
z[23] = abb[9] * z[6];
z[12] = z[12] + -z[22] + z[23];
z[12] = 8 * z[12];
z[22] = -abb[36] * z[12];
z[5] = abb[37] + 4 * z[5] + z[10] + 8 * z[15] + z[16] + z[18] + 2 * z[19] + z[22];
z[3] = z[3] + -z[7];
z[3] = 2 * z[3];
z[7] = z[14] * z[27];
z[7] = z[7] + z[31];
z[9] = -7 * abb[26] + -12 * abb[27] + -z[9] + z[17] + z[38];
z[9] = abb[0] * z[9];
z[6] = -z[6] * z[40];
z[6] = z[3] + z[6] + z[7] + z[9] + z[37] + -z[48];
z[6] = abb[12] * z[6];
z[4] = z[4] + -z[7] + z[20] + z[43];
z[4] = abb[14] * z[4];
z[1] = -z[1] * z[25];
z[7] = abb[7] * z[21];
z[1] = z[1] + z[2] + -z[3] + z[7] + -z[11];
z[1] = abb[16] * z[1];
z[0] = -z[0] + z[1] + z[4] + z[6] + z[36];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[20] * z[13];
z[2] = abb[21] * z[8];
z[3] = -abb[22] * z[12];
z[0] = abb[23] + 8 * z[0] + z[1] + z[2] + z[3];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_4_767_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-125.934588131562867027987201068199528416981152218300294786977756903"),stof<T>("-34.158397358460124271203705157856657604278286740724799878152553044")}, std::complex<T>{stof<T>("40.038800735197709089112287581300450629783529997147228482560616218"),stof<T>("-72.218749300377565748216694797228676819607635696610748333009253288")}, std::complex<T>{stof<T>("165.97338886676057611709948864949997904676468221544752326953837312"),stof<T>("-38.06035194191744147701298963937201921532934895588594845485670024")}, std::complex<T>{stof<T>("291.90797699832344314508668971769950746374583443374781805651613002"),stof<T>("-3.9019545834573172058092844815153616110510622151611485767041472")}, std::complex<T>{stof<T>("-85.895787396365157938874913486899077787197622221153066304417140684"),stof<T>("-106.377146658837690019420399955085334423885922437335548211161806332")}, std::complex<T>{stof<T>("-120.11640220559312726733686274390135188935058999144168544768184866"),stof<T>("216.65624790113269724465008439168603045882290708983224499902775986")}, std::complex<T>{stof<T>("54.150756200242028690121984092745941244694211628465848930159105146"),stof<T>("21.596086681272167350715119075337558373714802429811697023888125087")}, std::complex<T>{stof<T>("108.301512400484057380243968185491882489388423256931697860318210292"),stof<T>("43.192173362544334701430238150675116747429604859623394047776250175")}, std::complex<T>{stof<T>("-54.150756200242028690121984092745941244694211628465848930159105146"),stof<T>("-21.596086681272167350715119075337558373714802429811697023888125087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[59].real()/kbase.W[59].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_767_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_767_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (-v[3] + v[5]) * (32 + -12 * (2 + m1_set::bc<T>[2]) * v[0] + 4 * (2 + m1_set::bc<T>[2]) * v[1] + -6 * v[3] + 22 * v[5] + -3 * m1_set::bc<T>[2] * (8 + v[3] + 3 * v[5]))) / prod_pow(tend, 2);
c[1] = ((T(1) / T(2)) * (-v[3] + v[5]) * (8 * (3 * v[0] + -v[1] + 5 * (2 + v[5])) + -3 * m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(2)) * (-v[3] + v[5]) * (40 + -32 * m1_set::bc<T>[1] + -v[3] + -8 * m1_set::bc<T>[1] * v[3] + 21 * v[5] + -8 * m1_set::bc<T>[1] * v[5] + -m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[3] = (6 * m1_set::bc<T>[2] * (v[3] + -v[5])) / tend;
c[4] = (-12 * (-2 + 3 * m1_set::bc<T>[2]) * (-v[3] + v[5])) / tend;
c[5] = (-4 * (-5 + 4 * m1_set::bc<T>[1] + 9 * m1_set::bc<T>[2]) * (-v[3] + v[5])) / tend;


		return t * (abb[24] * c[0] + abb[25] * c[1] + -3 * abb[30] * c[1] + abb[26] * (-c[0] + c[1]) + abb[29] * (c[0] + c[1] + -c[2]) + abb[28] * c[2] + abb[27] * (-2 * c[0] + c[1] + c[2])) + abb[24] * c[3] + abb[25] * c[4] + -3 * abb[30] * c[4] + abb[26] * (-c[3] + c[4]) + abb[27] * c[5] + -abb[29] * (c[3] + -2 * c[4] + c[5]) + abb[28] * (2 * c[3] + -c[4] + c[5]);
	}
	{
T z[6];
z[0] = 3 * abb[25] + abb[28];
z[1] = 18 * abb[30];
z[0] = abb[24] + 5 * abb[26] + 6 * abb[27] + 2 * z[0] + -z[1];
z[2] = abb[17] + -abb[19];
z[0] = z[0] * z[2];
z[3] = abb[25] + abb[26];
z[1] = -z[1] + 6 * z[3];
z[3] = -5 * abb[27] + abb[28] + -z[1];
z[3] = abb[18] * z[3];
z[2] = -7 * abb[18] + 5 * z[2];
z[2] = abb[29] * z[2];
z[0] = z[0] + z[2] + z[3];
z[2] = 2 * abb[25] + abb[28];
z[3] = -abb[24] + -abb[26] + -abb[27] + -2 * abb[29] + 6 * abb[30] + -z[2];
z[4] = abb[12] + -abb[16];
z[3] = z[3] * z[4];
z[4] = 11 * abb[29];
z[2] = -4 * abb[24] + -10 * abb[26] + -13 * abb[27] + 42 * abb[30] + -7 * z[2] + -z[4];
z[2] = abb[13] * z[2];
z[2] = z[2] + 4 * z[3];
z[2] = abb[13] * z[2];
z[5] = 2 * abb[24] + 10 * abb[25] + 8 * abb[26] + 7 * abb[27] + abb[28] + -30 * abb[30] + z[4];
z[5] = abb[13] * z[5];
z[3] = -2 * z[3] + z[5];
z[1] = -abb[27] + 5 * abb[28] + -z[1] + -z[4];
z[1] = abb[15] * z[1];
z[1] = z[1] + 2 * z[3];
z[1] = abb[15] * z[1];
z[0] = 2 * z[0] + z[1] + z[2];
return 4 * abb[6] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_767_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (2 * m1_set::bc<T>[0] * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (8 * m1_set::bc<T>[0] * (-v[3] + v[5])) / tend;


		return (abb[24] + 2 * abb[25] + abb[26] + abb[27] + abb[28] + 2 * abb[29] + -6 * abb[30]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[25] + abb[29];
z[0] = abb[24] + abb[26] + abb[27] + abb[28] + -6 * abb[30] + 2 * z[0];
z[1] = abb[13] + -abb[15];
return 16 * abb[6] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_767_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("91.644058808541862660899329202980337040404451777336878251363872215"),stof<T>("45.855323522092799986241253727861356024870009159400494559808716944")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,38> abb = {dl[0], dl[1], dl[5], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W16(k,dl), dlog_W28(k,dl), dlog_W60(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_2_3(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_24_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[59].real()/k.W[59].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), f_2_4_re(k), f_2_6_re(k), f_2_24_re(k), T{0}};
abb[23] = SpDLog_f_4_767_W_16_Im(t, path, abb);
abb[37] = SpDLog_f_4_767_W_16_Re(t, path, abb);

                    
            return f_4_767_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_767_DLogXconstant_part(base_point<T>, kend);
	value += f_4_767_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_767_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_767_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_767_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_767_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_767_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_767_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
