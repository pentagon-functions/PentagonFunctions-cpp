/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_652.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_652_abbreviated (const std::array<T,71>& abb) {
T z[176];
z[0] = 4 * abb[51];
z[1] = 4 * abb[47];
z[2] = z[0] + z[1];
z[3] = 4 * abb[52];
z[4] = abb[46] + abb[48];
z[5] = -abb[56] + z[2] + -z[3] + z[4];
z[5] = abb[14] * z[5];
z[6] = abb[19] * abb[59];
z[7] = abb[60] + abb[61];
z[8] = abb[25] * z[7];
z[6] = z[6] + z[8];
z[8] = abb[19] * abb[58];
z[5] = -z[5] + z[6] + -z[8];
z[9] = abb[20] * z[7];
z[10] = abb[58] + -abb[59];
z[11] = abb[18] * z[10];
z[12] = -abb[48] + abb[56];
z[13] = -abb[46] + z[12];
z[14] = abb[13] * z[13];
z[9] = z[9] + -z[11] + -z[14];
z[11] = abb[49] * (T(1) / T(2));
z[14] = abb[55] * (T(1) / T(2));
z[15] = z[11] + z[14];
z[16] = -abb[53] + z[15];
z[17] = abb[48] * (T(1) / T(2));
z[18] = abb[56] * (T(1) / T(2));
z[19] = z[17] + -z[18];
z[20] = z[16] + z[19];
z[20] = abb[6] * z[20];
z[21] = abb[7] + -abb[12];
z[22] = 2 * abb[9];
z[23] = abb[0] + -abb[3] + -11 * z[21] + -z[22];
z[23] = abb[50] * z[23];
z[23] = z[5] + z[9] + z[20] + -z[23];
z[24] = 5 * abb[51];
z[25] = 5 * abb[47];
z[26] = z[24] + z[25];
z[27] = abb[55] + abb[57];
z[28] = abb[54] * (T(5) / T(3));
z[29] = 3 * abb[49];
z[30] = -29 * abb[52] + abb[48] * (T(53) / T(2));
z[27] = abb[56] * (T(-15) / T(4)) + abb[53] * (T(-1) / T(3)) + abb[46] * (T(35) / T(12)) + z[26] + (T(4) / T(3)) * z[27] + -z[28] + -z[29] + (T(1) / T(6)) * z[30];
z[27] = abb[3] * z[27];
z[30] = abb[47] + abb[51];
z[31] = abb[54] * (T(11) / T(3));
z[32] = abb[46] * (T(5) / T(2));
z[33] = abb[52] * (T(1) / T(6)) + 6 * z[12] + (T(-11) / T(3)) * z[30] + -z[31] + -z[32];
z[33] = abb[12] * z[33];
z[34] = 2 * abb[57];
z[35] = -abb[56] + z[34];
z[36] = abb[55] * (T(7) / T(3));
z[37] = 5 * abb[52];
z[38] = abb[47] * (T(-49) / T(6)) + abb[51] * (T(-29) / T(3)) + abb[48] * (T(-29) / T(6)) + abb[54] * (T(9) / T(2)) + -z[32] + z[35] + z[36] + z[37];
z[38] = abb[9] * z[38];
z[39] = abb[46] * (T(1) / T(2));
z[40] = z[17] + z[39];
z[41] = abb[51] + -abb[52];
z[42] = z[40] + z[41];
z[43] = -11 * abb[49] + 35 * z[42];
z[44] = 5 * abb[54];
z[45] = 11 * abb[47];
z[46] = abb[56] * (T(3) / T(2));
z[43] = (T(1) / T(3)) * z[43] + -z[44] + z[45] + -z[46];
z[43] = abb[0] * z[43];
z[47] = abb[57] * (T(2) / T(3));
z[48] = 3 * abb[52];
z[49] = abb[48] * (T(23) / T(6)) + -z[48];
z[31] = abb[46] * (T(-7) / T(12)) + abb[53] * (T(-1) / T(6)) + abb[51] * (T(19) / T(3)) + abb[56] * (T(29) / T(12)) + abb[47] * (T(31) / T(6)) + -z[31] + -z[36] + -z[47] + (T(1) / T(2)) * z[49];
z[31] = abb[2] * z[31];
z[36] = abb[23] * z[7];
z[49] = abb[21] * z[7];
z[50] = z[36] + z[49];
z[51] = -abb[49] + abb[55];
z[52] = 2 * abb[52];
z[53] = z[51] + -z[52];
z[54] = z[12] + z[53];
z[55] = abb[8] * z[54];
z[55] = z[50] + z[55];
z[56] = 2 * abb[48];
z[57] = 2 * abb[46];
z[58] = -z[41] + z[56] + z[57];
z[59] = 5 * abb[49];
z[58] = 2 * z[58] + z[59];
z[60] = 2 * abb[56];
z[28] = -abb[57] + abb[47] * (T(-2) / T(3)) + z[28] + (T(1) / T(3)) * z[58] + -z[60];
z[28] = abb[15] * z[28];
z[58] = 6 * abb[52];
z[61] = 2 * abb[49];
z[62] = z[58] + z[61];
z[47] = abb[56] * (T(-17) / T(6)) + abb[51] * (T(15) / T(2)) + abb[47] * (T(19) / T(3)) + abb[46] * (T(29) / T(6)) + abb[48] * (T(35) / T(6)) + -z[14] + z[47] + -z[62];
z[47] = abb[7] * z[47];
z[63] = abb[56] * (T(5) / T(2));
z[64] = 5 * abb[46];
z[65] = -17 * abb[48] + -23 * abb[51] + abb[53] + -z[64];
z[65] = abb[47] * (T(-23) / T(6)) + abb[57] * (T(1) / T(3)) + abb[55] * (T(11) / T(6)) + -z[61] + z[63] + (T(1) / T(6)) * z[65];
z[65] = abb[4] * z[65];
z[66] = abb[22] + abb[24];
z[66] = z[7] * z[66];
z[67] = (T(1) / T(2)) * z[66];
z[68] = abb[16] * z[10];
z[69] = abb[17] * z[10];
z[70] = abb[1] * z[13];
z[23] = (T(-1) / T(3)) * z[23] + z[27] + z[28] + z[31] + z[33] + z[38] + (T(1) / T(2)) * z[43] + z[47] + (T(-4) / T(3)) * z[55] + z[65] + -z[67] + -z[68] + -z[69] + (T(17) / T(12)) * z[70];
z[27] = prod_pow(m1_set::bc<T>[0], 2);
z[23] = z[23] * z[27];
z[28] = z[3] + z[34];
z[31] = 10 * abb[47];
z[33] = 8 * abb[51];
z[38] = z[31] + z[33];
z[43] = 3 * abb[48];
z[47] = 2 * abb[55];
z[65] = z[43] + -z[47];
z[71] = -abb[46] + abb[56];
z[72] = z[65] + -z[71];
z[73] = z[28] + -z[38] + -z[72];
z[74] = abb[2] * z[73];
z[75] = 2 * abb[51];
z[76] = abb[48] + z[75];
z[77] = z[25] + z[76];
z[78] = 3 * abb[57];
z[79] = z[47] + z[78];
z[80] = -z[52] + -z[71] + -z[77] + z[79];
z[80] = abb[9] * z[80];
z[81] = 2 * abb[47];
z[82] = z[75] + z[81];
z[83] = -abb[52] + z[82];
z[84] = abb[57] + -z[15] + z[19] + z[83];
z[84] = abb[3] * z[84];
z[85] = z[6] + z[66];
z[36] = z[36] + z[49] + z[85];
z[49] = z[11] + -z[14];
z[86] = abb[52] + z[49];
z[87] = z[19] + z[86];
z[87] = abb[8] * z[87];
z[88] = (T(-1) / T(2)) * z[9];
z[89] = -z[52] + z[82];
z[90] = -z[18] + z[40] + z[89];
z[90] = abb[14] * z[90];
z[8] = (T(1) / T(2)) * z[8] + z[90];
z[90] = 8 * abb[47];
z[91] = 3 * abb[51];
z[92] = abb[48] + z[91];
z[93] = -abb[52] + z[92];
z[93] = -abb[56] + -abb[57] + -z[47] + z[90] + 2 * z[93];
z[93] = abb[7] * z[93];
z[94] = abb[3] + abb[9];
z[95] = abb[50] * z[94];
z[96] = abb[47] + -abb[49];
z[97] = abb[0] * z[96];
z[98] = -abb[57] + z[4];
z[99] = -abb[4] * z[98];
z[36] = -z[8] + (T(1) / T(2)) * z[36] + z[70] + -z[74] + z[80] + z[84] + -z[87] + -z[88] + z[93] + -z[95] + z[97] + z[99];
z[36] = abb[29] * z[36];
z[80] = 2 * z[70];
z[84] = -z[80] + z[88];
z[88] = -z[8] + z[84];
z[93] = z[6] + -z[66];
z[93] = z[88] + (T(1) / T(2)) * z[93];
z[97] = 2 * abb[2];
z[99] = z[73] * z[97];
z[100] = (T(3) / T(2)) * z[68];
z[101] = z[93] + -z[99] + -z[100];
z[102] = abb[48] + abb[51];
z[103] = -abb[46] + z[34];
z[104] = z[51] + -z[81] + -2 * z[102] + z[103];
z[105] = abb[15] * z[104];
z[106] = abb[26] * z[7];
z[105] = z[105] + z[106];
z[107] = 6 * abb[47];
z[108] = abb[49] + abb[55];
z[109] = abb[52] + -z[76];
z[109] = z[103] + -z[107] + z[108] + 2 * z[109];
z[109] = abb[0] * z[109];
z[110] = -abb[56] + z[43] + z[57];
z[111] = 6 * abb[51];
z[112] = z[107] + z[111];
z[113] = -z[3] + -z[108] + z[110] + z[112];
z[114] = -abb[3] * z[113];
z[115] = 12 * abb[51];
z[116] = 16 * abb[47] + z[115];
z[58] = abb[49] + 3 * abb[55] + z[58] + -z[116];
z[117] = 4 * abb[57];
z[118] = abb[48] * (T(-11) / T(2)) + -z[32] + z[46] + z[58] + z[117];
z[118] = abb[7] * z[118];
z[119] = -z[51] + z[82];
z[120] = -z[39] + z[119];
z[121] = z[17] + -z[34] + z[46] + z[120];
z[121] = abb[4] * z[121];
z[122] = 7 * abb[47];
z[123] = 2 * z[76] + -z[79] + z[122];
z[123] = z[22] * z[123];
z[124] = 2 * abb[50];
z[125] = z[94] * z[124];
z[126] = z[123] + z[125];
z[127] = (T(1) / T(2)) * z[69];
z[109] = -z[101] + -z[105] + z[109] + z[114] + z[118] + z[121] + z[126] + z[127];
z[109] = abb[33] * z[109];
z[114] = -z[4] + z[117];
z[118] = 2 * abb[54];
z[121] = z[114] + z[118];
z[128] = 5 * abb[56];
z[129] = z[112] + z[128];
z[130] = z[62] + z[121] + -z[129];
z[130] = abb[15] * z[130];
z[131] = z[104] + z[118];
z[131] = abb[3] * z[131];
z[132] = 3 * abb[56];
z[133] = -abb[54] + z[132];
z[134] = -z[78] + z[133];
z[135] = z[22] * z[134];
z[136] = -z[4] + z[35];
z[137] = abb[4] * z[136];
z[137] = z[69] + z[137];
z[138] = abb[7] * z[136];
z[139] = -z[13] + z[89];
z[140] = -abb[0] * z[139];
z[125] = -z[55] + z[68] + -z[125] + -z[130] + z[131] + -z[135] + z[137] + z[138] + z[140];
z[125] = abb[31] * z[125];
z[131] = z[2] + -z[51] + -z[52];
z[140] = z[18] + -z[34];
z[141] = abb[48] * (T(3) / T(2));
z[142] = z[39] + z[141];
z[143] = z[131] + z[140] + z[142];
z[143] = abb[7] * z[143];
z[142] = -z[46] + z[119] + z[142];
z[142] = abb[4] * z[142];
z[144] = -z[52] + z[75];
z[145] = abb[47] + abb[49];
z[146] = z[144] + z[145];
z[98] = z[98] + z[146];
z[98] = z[97] * z[98];
z[147] = (T(3) / T(2)) * z[69];
z[98] = z[98] + z[147];
z[148] = abb[46] + z[53];
z[148] = abb[0] * z[148];
z[149] = z[12] + -z[119];
z[150] = -abb[3] * z[149];
z[151] = (T(1) / T(2)) * z[68];
z[93] = z[93] + z[98] + z[105] + z[142] + z[143] + -z[148] + z[150] + z[151];
z[93] = abb[32] * z[93];
z[142] = 2 * abb[7];
z[73] = z[73] * z[142];
z[143] = -z[12] + z[75];
z[150] = z[1] + -z[108] + z[143];
z[150] = abb[0] * z[150];
z[73] = z[5] + -z[73] + z[150];
z[137] = z[105] + z[137];
z[56] = abb[46] + -z[48] + z[56];
z[24] = z[24] + z[56];
z[24] = abb[49] + 2 * z[24] + z[45];
z[45] = -abb[56] + z[24] + -z[79];
z[45] = z[45] * z[97];
z[150] = -abb[48] + z[52];
z[152] = -z[91] + z[150];
z[152] = -z[107] + 2 * z[152];
z[153] = z[60] + z[108];
z[154] = -abb[46] + -z[34] + z[152] + z[153];
z[155] = abb[3] * z[154];
z[45] = z[45] + z[73] + -z[126] + z[137] + -z[155];
z[126] = -abb[30] * z[45];
z[155] = abb[3] * z[104];
z[156] = z[66] + z[68];
z[138] = z[138] + z[156];
z[157] = abb[4] * z[13];
z[158] = -z[105] + z[138] + 2 * z[157];
z[159] = z[9] + 4 * z[70];
z[160] = abb[48] + -z[35] + z[119];
z[160] = abb[0] * z[160];
z[155] = -z[155] + -z[158] + -z[159] + z[160];
z[155] = abb[34] * z[155];
z[36] = z[36] + z[93] + z[109] + z[125] + z[126] + z[155];
z[36] = abb[29] * z[36];
z[83] = -z[18] + -z[49] + -z[83] + z[103] + -z[141];
z[83] = abb[15] * z[83];
z[93] = abb[48] * (T(7) / T(2));
z[103] = -z[18] + z[93];
z[109] = abb[46] * (T(3) / T(2));
z[38] = -z[3] + z[38] + -z[79] + z[103] + z[109];
z[38] = abb[7] * z[38];
z[79] = 3 * abb[47] + -abb[52];
z[15] = -z[15] + z[39] + z[79] + z[143];
z[15] = abb[0] * z[15];
z[72] = -z[52] + z[72];
z[125] = -9 * abb[47] + -z[72] + z[78] + -z[111];
z[125] = abb[9] * z[125];
z[126] = -abb[57] + z[60];
z[56] = -abb[53] + z[2] + z[56] + -z[126];
z[56] = abb[3] * z[56];
z[24] = z[24] + -z[47] + -z[117];
z[143] = abb[2] * z[24];
z[160] = abb[56] + -abb[57];
z[161] = abb[4] * z[160];
z[162] = z[95] + z[161];
z[163] = (T(1) / T(2)) * z[106];
z[15] = z[15] + -z[20] + z[38] + z[56] + -z[67] + z[83] + z[125] + z[143] + -z[151] + -z[162] + z[163];
z[15] = abb[30] * z[15];
z[38] = abb[33] * z[45];
z[45] = z[61] + z[82];
z[56] = 2 * abb[53];
z[67] = z[45] + -z[56];
z[83] = abb[46] + z[43];
z[125] = z[67] + z[83];
z[143] = z[125] + -z[132];
z[143] = abb[3] * z[143];
z[164] = z[97] * z[160];
z[149] = abb[0] * z[149];
z[143] = z[143] + -z[149] + -z[158] + z[164];
z[143] = abb[32] * z[143];
z[158] = z[12] + z[56] + -z[108];
z[165] = abb[6] * z[158];
z[166] = abb[32] * z[165];
z[143] = z[143] + z[166];
z[41] = 2 * z[41] + z[81];
z[167] = abb[49] + abb[54];
z[126] = z[41] + z[126] + -z[167];
z[126] = abb[15] * z[126];
z[134] = abb[9] * z[134];
z[168] = abb[2] * z[160];
z[160] = -abb[54] + z[160];
z[160] = abb[3] * z[160];
z[134] = -z[126] + z[134] + z[160] + z[162] + -z[168];
z[134] = abb[31] * z[134];
z[15] = z[15] + z[38] + 2 * z[134] + -z[143];
z[15] = abb[30] * z[15];
z[38] = 7 * abb[48];
z[134] = z[38] + -z[132];
z[160] = 4 * abb[55];
z[162] = 6 * abb[57];
z[169] = z[160] + z[162];
z[170] = 10 * abb[52];
z[171] = 3 * abb[46];
z[172] = 24 * abb[47] + 18 * abb[51] + z[118] + z[134] + -z[169] + -z[170] + z[171];
z[172] = abb[2] * z[172];
z[173] = 5 * abb[48];
z[71] = 18 * abb[47] + -z[3] + -z[71] + z[115] + -z[169] + z[173];
z[71] = abb[7] * z[71];
z[90] = z[90] + -z[108];
z[33] = z[33] + z[90] + -z[118];
z[115] = z[57] + -z[162];
z[150] = -z[33] + -z[115] + -z[128] + 3 * z[150];
z[150] = abb[15] * z[150];
z[150] = z[106] + z[150];
z[169] = -abb[46] + z[52];
z[174] = z[76] + z[169];
z[44] = 13 * abb[47] + -7 * abb[56] + z[44] + -z[160] + 2 * z[174];
z[44] = abb[9] * z[44];
z[153] = abb[54] + z[77] + -z[153];
z[153] = abb[0] * z[153];
z[72] = z[72] + z[112] + -z[118];
z[72] = abb[3] * z[72];
z[112] = z[118] + z[139];
z[174] = abb[12] * z[112];
z[174] = z[55] + z[174];
z[175] = z[21] * z[124];
z[44] = z[5] + -z[44] + -z[68] + z[71] + z[72] + z[150] + z[153] + z[172] + z[174] + z[175];
z[71] = -abb[63] * z[44];
z[72] = z[17] + z[48];
z[153] = abb[54] + abb[55];
z[30] = -z[30] + -z[39] + z[61] + z[72] + z[140] + -z[153];
z[30] = abb[3] * z[30];
z[140] = -z[117] + z[141];
z[141] = abb[56] * (T(7) / T(2));
z[26] = -z[26] + z[37] + -z[109] + -z[140] + -z[141] + z[167];
z[26] = abb[15] * z[26];
z[37] = abb[4] * z[104];
z[104] = 4 * abb[54];
z[172] = z[104] + z[162];
z[65] = -z[65] + -z[129] + z[169] + z[172];
z[65] = abb[9] * z[65];
z[65] = z[65] + z[70];
z[42] = abb[47] + z[18] + z[42] + -z[167];
z[42] = abb[0] * z[42];
z[79] = z[79] + z[92] + -z[153];
z[79] = z[79] * z[97];
z[92] = abb[7] * z[154];
z[21] = z[21] + z[94];
z[21] = z[21] * z[124];
z[21] = z[21] + z[26] + z[30] + -z[37] + -z[42] + -z[65] + -z[79] + z[92] + z[174];
z[26] = -abb[66] * z[21];
z[30] = z[89] + -z[136];
z[30] = abb[0] * z[30];
z[37] = -z[19] + z[39] + -z[131];
z[37] = abb[7] * z[37];
z[42] = z[19] + z[109];
z[79] = z[42] + -z[119];
z[79] = abb[4] * z[79];
z[92] = -abb[3] * z[136];
z[8] = z[8] + z[30] + z[37] + z[79] + z[84] + (T(-1) / T(2)) * z[85] + z[92] + -z[98] + -z[100];
z[8] = abb[33] * z[8];
z[30] = z[13] + z[47] + -z[56] + -z[75] + -z[145];
z[30] = abb[2] * z[30];
z[37] = -abb[46] + z[119];
z[79] = -abb[7] * z[37];
z[84] = abb[46] + -abb[51];
z[85] = abb[53] + z[84] + -z[145];
z[85] = abb[4] * z[85];
z[92] = -abb[3] * z[13];
z[30] = z[30] + -z[68] + z[70] + z[79] + 2 * z[85] + z[92] + -z[149];
z[30] = abb[32] * z[30];
z[8] = z[8] + z[30] + -z[155];
z[8] = abb[32] * z[8];
z[16] = abb[54] + -z[16] + -z[17] + -z[34] + z[63];
z[16] = abb[3] * z[16];
z[17] = z[50] + z[66];
z[30] = 4 * abb[56];
z[50] = -abb[46] + z[30];
z[68] = 4 * abb[48] + z[48] + -z[50];
z[68] = abb[12] * z[68];
z[79] = abb[0] + abb[3];
z[85] = abb[50] * z[79];
z[85] = -z[68] + z[85];
z[92] = abb[0] * abb[54];
z[16] = z[16] + (T(1) / T(2)) * z[17] + z[20] + -z[85] + -z[87] + z[92] + z[126] + -z[161] + -z[164];
z[16] = abb[31] * z[16];
z[17] = z[75] + z[83];
z[17] = -3 * z[17] + z[61] + -z[107] + z[117] + z[132];
z[17] = abb[3] * z[17];
z[20] = abb[0] * z[113];
z[75] = abb[56] + z[34];
z[83] = 3 * z[4];
z[87] = z[75] + -z[83];
z[92] = z[87] * z[142];
z[79] = z[79] * z[124];
z[17] = z[17] + -z[20] + z[79] + z[92] + -z[137];
z[17] = abb[33] * z[17];
z[16] = z[16] + z[17] + z[143];
z[16] = abb[31] * z[16];
z[20] = -abb[55] + z[29];
z[2] = -z[2] + z[20] + z[34] + -z[57] + -z[134];
z[2] = abb[3] * z[2];
z[38] = -abb[56] + z[3] + z[38] + z[51] + z[115];
z[51] = abb[5] * z[38];
z[92] = -5 * z[4] + z[34] + z[132];
z[92] = abb[7] * z[92];
z[94] = z[69] + z[156];
z[98] = abb[49] + -abb[57] + -z[139];
z[100] = 2 * abb[0];
z[98] = z[98] * z[100];
z[107] = -abb[4] * z[87];
z[2] = z[2] + z[51] + z[79] + z[92] + -z[94] + z[98] + z[107] + -z[159];
z[2] = abb[36] * z[2];
z[33] = abb[46] + 11 * abb[48] + z[33] + z[48] + -z[132] + -z[162];
z[33] = abb[3] * z[33];
z[48] = -z[12] + z[48] + -z[57];
z[48] = abb[12] * z[48];
z[51] = abb[0] + -abb[9];
z[51] = z[51] * z[124];
z[79] = abb[0] * z[112];
z[33] = z[33] + -z[48] + -z[51] + z[55] + -z[65] + z[79] + z[150];
z[48] = -abb[7] * z[38];
z[48] = -z[33] + z[48];
z[48] = abb[65] * z[48];
z[14] = abb[54] + abb[57] + abb[49] * (T(3) / T(2)) + -z[14] + -z[63] + z[72] + -z[82];
z[14] = abb[15] * z[14];
z[51] = -abb[54] + -z[34] + z[39] + z[60] + -z[86];
z[51] = abb[0] * z[51];
z[65] = -abb[57] + (T(-1) / T(2)) * z[4] + z[46];
z[65] = abb[4] * z[65];
z[72] = -abb[3] * abb[54];
z[14] = z[14] + z[51] + z[65] + z[72] + -z[80] + z[85] + -z[127] + -z[163];
z[14] = abb[34] * z[14];
z[14] = z[14] + -z[17];
z[14] = abb[34] * z[14];
z[17] = -z[13] + z[61];
z[0] = -z[0] + z[3] + -z[17] + -z[81];
z[0] = abb[7] * z[0];
z[51] = -abb[53] + z[52] + -z[57] + -z[102];
z[25] = z[25] + 2 * z[51] + -z[59] + z[60];
z[25] = abb[2] * z[25];
z[5] = z[5] + z[94];
z[51] = abb[3] * z[158];
z[57] = -z[12] + -z[67] + z[171];
z[57] = abb[4] * z[57];
z[59] = -z[96] * z[100];
z[0] = z[0] + -z[5] + z[25] + -z[51] + z[57] + z[59] + z[165];
z[0] = abb[35] * z[0];
z[25] = abb[4] * z[158];
z[54] = abb[15] * z[54];
z[54] = z[54] + -z[66] + z[106];
z[57] = -abb[52] + -abb[53] + abb[55];
z[57] = z[57] * z[97];
z[25] = z[25] + z[51] + -z[54] + z[55] + z[57] + -z[165];
z[51] = -abb[64] * z[25];
z[28] = z[28] + -z[90] + -z[110] + -z[111];
z[28] = abb[0] * z[28];
z[57] = abb[3] * z[87];
z[59] = -abb[49] + z[1] + -3 * z[84];
z[59] = z[59] * z[142];
z[37] = -abb[4] * z[37];
z[28] = z[28] + z[37] + z[57] + z[59] + -z[69] + -z[70] + -z[74];
z[28] = prod_pow(abb[33], 2) * z[28];
z[18] = z[18] + -z[40] + -z[81] + -z[144];
z[18] = abb[19] * z[18];
z[19] = z[19] + z[120];
z[19] = abb[17] * z[19];
z[37] = z[42] + z[53];
z[37] = abb[16] * z[37];
z[7] = abb[27] * z[7];
z[13] = abb[18] * z[13];
z[7] = z[7] + -z[13];
z[13] = abb[4] + abb[7] + -abb[13];
z[40] = (T(1) / T(2)) * z[10];
z[13] = z[13] * z[40];
z[40] = abb[3] + 2 * abb[28] + abb[14] * (T(-1) / T(2));
z[10] = z[10] * z[40];
z[7] = (T(1) / T(2)) * z[7] + z[10] + z[13] + z[18] + z[19] + -z[37];
z[10] = -abb[67] * z[7];
z[9] = z[9] + z[80];
z[13] = z[9] + -z[54] + z[55] + -z[69] + z[148] + z[157];
z[18] = -abb[62] * z[13];
z[19] = abb[33] * z[38];
z[37] = abb[46] + z[52];
z[40] = -z[37] + z[49] + z[78] + -z[103];
z[42] = -abb[34] * z[40];
z[42] = -z[19] + z[42];
z[42] = abb[34] * z[42];
z[40] = abb[31] * z[40];
z[40] = z[19] + z[40];
z[40] = abb[31] * z[40];
z[49] = abb[65] * z[38];
z[11] = -z[11] + z[37] + z[93];
z[37] = abb[55] + -abb[56];
z[11] = -abb[57] + (T(1) / T(3)) * z[11] + (T(1) / T(6)) * z[37];
z[11] = z[11] * z[27];
z[11] = z[11] + z[40] + z[42] + z[49];
z[11] = abb[5] * z[11];
z[27] = -abb[49] + -abb[50] + z[35] + z[41];
z[35] = abb[70] * z[27];
z[0] = abb[68] + abb[69] + z[0] + z[2] + z[8] + z[10] + z[11] + z[14] + z[15] + z[16] + z[18] + z[23] + z[26] + z[28] + z[35] + z[36] + z[48] + z[51] + z[71];
z[2] = 2 * z[105];
z[8] = z[20] + -z[50];
z[1] = -z[1] + -z[8] + z[56] + -4 * z[102];
z[1] = abb[3] * z[1];
z[10] = abb[48] * (T(-5) / T(2)) + -z[46] + -z[109] + z[117] + -z[131];
z[10] = abb[7] * z[10];
z[11] = -z[32] + -z[93] + -z[119] + z[141];
z[11] = abb[4] * z[11];
z[6] = -z[6] + 3 * z[66];
z[14] = -z[47] + z[169];
z[12] = z[12] + -z[14] + -z[45];
z[12] = abb[0] * z[12];
z[15] = z[136] + -z[146];
z[15] = z[15] * z[97];
z[1] = z[1] + -z[2] + (T(1) / T(2)) * z[6] + z[10] + z[11] + z[12] + z[15] + -z[88] + -z[147] + z[151];
z[1] = abb[32] * z[1];
z[6] = 10 * abb[51];
z[4] = 4 * abb[49] + -9 * abb[56] + -z[4] + -z[6] + -z[31] + z[170] + z[172];
z[4] = abb[15] * z[4];
z[10] = z[34] + -z[104] + z[125] + -z[128];
z[10] = abb[3] * z[10];
z[11] = z[114] + -z[132];
z[11] = abb[4] * z[11];
z[11] = z[11] + z[69];
z[12] = abb[0] + 2 * abb[3] + abb[9];
z[12] = z[12] * z[124];
z[15] = -z[118] + z[139];
z[15] = abb[0] * z[15];
z[4] = z[4] + z[10] + -z[11] + z[12] + z[15] + -2 * z[68] + z[135] + -z[138] + z[165] + 4 * z[168];
z[4] = abb[31] * z[4];
z[3] = abb[46] + z[3] + -z[43] + -z[116] + z[132] + z[160];
z[3] = abb[7] * z[3];
z[10] = -abb[54] + z[14] + z[30] + z[77] + -z[162];
z[10] = z[10] * z[22];
z[14] = z[17] + z[144];
z[14] = abb[0] * z[14];
z[15] = abb[56] + z[61] + -z[89] + -z[121];
z[15] = abb[3] * z[15];
z[16] = abb[56] + z[83] + -z[117];
z[16] = abb[4] * z[16];
z[17] = 4 * z[95];
z[3] = z[3] + -z[5] + -z[9] + z[10] + z[14] + z[15] + z[16] + z[17] + z[99] + z[130];
z[3] = abb[29] * z[3];
z[5] = 12 * abb[47];
z[6] = z[5] + z[6] + -z[47] + -z[62] + -z[75] + z[171] + z[173];
z[6] = abb[0] * z[6];
z[9] = -abb[52] + z[43] + z[91];
z[5] = -abb[55] + z[5] + 4 * z[9] + -z[29] + -z[30] + z[64] + -z[117];
z[5] = abb[3] * z[5];
z[9] = -z[39] + -z[63] + -z[119] + -z[140];
z[9] = abb[4] * z[9];
z[10] = -8 * abb[57] + abb[46] * (T(17) / T(2)) + abb[48] * (T(23) / T(2)) + -z[58] + -z[141];
z[10] = abb[7] * z[10];
z[2] = z[2] + z[5] + z[6] + z[9] + z[10] + -z[12] + z[101] + -z[123] + z[127];
z[2] = abb[33] * z[2];
z[5] = -z[50] + -z[108] + z[117] + z[118] + -z[152];
z[5] = abb[3] * z[5];
z[6] = -abb[48] + abb[51] + -z[52];
z[6] = 2 * z[6] + -z[8] + z[81] + -z[118];
z[6] = abb[15] * z[6];
z[8] = -z[76] + z[78];
z[8] = 2 * z[8] + z[47] + -z[122] + -z[133];
z[8] = z[8] * z[22];
z[9] = z[24] * z[97];
z[5] = z[5] + z[6] + z[8] + z[9] + z[11] + -z[17] + z[73] + z[106];
z[5] = abb[30] * z[5];
z[6] = abb[31] * z[38];
z[6] = z[6] + -z[19];
z[6] = abb[5] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + -z[155] + -z[166];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[41] * z[21];
z[3] = -abb[38] * z[44];
z[4] = abb[5] + -abb[7];
z[4] = z[4] * z[38];
z[4] = z[4] + -z[33];
z[4] = abb[40] * z[4];
z[5] = -abb[42] * z[7];
z[6] = -abb[39] * z[25];
z[7] = -abb[37] * z[13];
z[8] = abb[45] * z[27];
z[1] = abb[43] + abb[44] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_652_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-6.29528515329787257381662736342755648630045211758223188305213529"),stof<T>("-64.523801920969199735358964251936598490033742049441984430025219812")}, std::complex<T>{stof<T>("-4.648776280346461502406939077331480510113580387668771380436434008"),stof<T>("-136.276986099657012331846668350753443922702679935297084115128749386")}, std::complex<T>{stof<T>("-27.415864449953606298527193528579593308032105533185937704666545853"),stof<T>("-73.958233534702477581305841182487921112774988866373525826317962181")}, std::complex<T>{stof<T>("-27.510702998400240794246281185631817624479037092496168783581563618"),stof<T>("64.943091245506897517445422704619919536885390033747554316354706972")}, std::complex<T>{stof<T>("19.385093949657884334397719018576290600527223645225120141947376151"),stof<T>("-9.250839060752725233430095639563205829678077469076768531712087619")}, std::complex<T>{stof<T>("-14.16965834473212103186906576390162217030267791479195000423127705"),stof<T>("-141.87476503824746926384801129284200570441698809440718458676092042")}, std::complex<T>{stof<T>("-24.1989820019703170177810967262006170057327017261393369979218299"),stof<T>("145.30128461607305690941556588871979614894963311273164660143591992")}, std::complex<T>{stof<T>("-20.126853833625761274242277192687764210513436985270529121195400339"),stof<T>("66.930758053291214191362429759842574081003883826678419908731223757")}, std::complex<T>{stof<T>("17.248061050046704324939596324950202354303726225327581180538696386"),stof<T>("-12.860951191558865491514431526429113067273891835256003410967741868")}, std::complex<T>{stof<T>("35.224067915157534534999243869156076198719343464051598276657774354"),stof<T>("8.449390016586249184603375265897530320208679339511484699695970869")}, std::complex<T>{stof<T>("-7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("-20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("-10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[89])) - rlog(abs(kbase.W[89])), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_652_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_652_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (8 + -7 * v[0] + 3 * v[1] + 5 * v[2] + v[3] + -4 * v[4] + -2 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -5 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[46] + abb[48] + -abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[29] + -abb[32];
z[1] = abb[33] + z[0];
z[1] = abb[33] * z[1];
z[0] = abb[34] + z[0];
z[0] = abb[34] * z[0];
z[0] = -abb[36] + -z[0] + z[1];
z[1] = abb[46] + abb[48] + -abb[57];
return 2 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_652_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[46] + abb[48] + -abb[57]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = -abb[33] + abb[34];
z[1] = abb[46] + abb[48] + -abb[57];
return 2 * abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_652_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(16)) * (v[2] + v[3]) * (-56 + -16 * v[0] + -8 * v[1] + 5 * v[2] + 13 * v[3] + 8 * v[4] + -28 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(4)) * (v[2] + v[3]) * (v[2] + v[3] + -2 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]))) / prod_pow(tend, 2);
c[2] = ((T(9) / T(2)) * (v[2] + v[3])) / tend;
c[3] = (2 * (1 + m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return abb[47] * (t * c[0] + c[2]) + (abb[51] + abb[53] + -abb[55]) * (t * c[1] + c[3]) + abb[49] * (-t * c[0] + t * c[1] + -c[2] + c[3]);
	}
	{
T z[4];
z[0] = abb[47] + -abb[49];
z[1] = abb[29] + -abb[33];
z[0] = z[0] * z[1];
z[0] = 2 * z[0];
z[1] = abb[51] + abb[53] + -abb[55];
z[2] = abb[47] + abb[49] + 2 * z[1];
z[3] = abb[32] * z[2];
z[3] = z[0] + z[3];
z[3] = abb[32] * z[3];
z[2] = -abb[30] * z[2];
z[0] = -z[0] + z[2];
z[0] = abb[30] * z[0];
z[1] = -9 * abb[47] + 5 * abb[49] + -4 * z[1];
z[1] = abb[35] * z[1];
z[0] = z[0] + z[1] + z[3];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_652_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[47] + -abb[49]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = -abb[47] + abb[49];
z[1] = -abb[30] + abb[32];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_652_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-128.359126217558993190511369650359832656916261901341062719953161917"),stof<T>("53.337117937343598340315315912830717205057569506616477246186819865")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18, 89});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,71> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[89])) - rlog(abs(k.W[89])), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[43] = SpDLog_f_4_652_W_17_Im(t, path, abb);
abb[44] = SpDLog_f_4_652_W_19_Im(t, path, abb);
abb[45] = SpDLogQ_W_90(k,dl,dlr).imag();
abb[68] = SpDLog_f_4_652_W_17_Re(t, path, abb);
abb[69] = SpDLog_f_4_652_W_19_Re(t, path, abb);
abb[70] = SpDLogQ_W_90(k,dl,dlr).real();

                    
            return f_4_652_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_652_DLogXconstant_part(base_point<T>, kend);
	value += f_4_652_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_652_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_652_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_652_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_652_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_652_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_652_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
