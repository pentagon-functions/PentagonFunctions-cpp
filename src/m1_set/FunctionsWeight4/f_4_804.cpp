/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_804.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_804_abbreviated (const std::array<T,86>& abb) {
T z[156];
z[0] = abb[27] + abb[29];
z[1] = abb[62] + -abb[64];
z[2] = abb[65] + z[1];
z[3] = -abb[60] + z[2];
z[4] = z[0] * z[3];
z[5] = abb[67] + abb[69];
z[6] = -abb[71] + z[5];
z[7] = abb[68] + abb[70];
z[8] = -abb[73] + z[7];
z[9] = z[6] + -z[8];
z[10] = abb[5] * z[9];
z[11] = abb[15] * z[6];
z[12] = abb[61] + z[1];
z[13] = abb[31] * z[12];
z[14] = z[11] + -z[13];
z[15] = z[6] + z[8];
z[16] = abb[6] * z[15];
z[17] = 2 * z[15];
z[18] = abb[9] * z[17];
z[19] = 2 * z[8];
z[20] = abb[7] * z[19];
z[21] = abb[4] * z[8];
z[22] = abb[3] * z[8];
z[4] = z[4] + z[10] + z[14] + -z[16] + -z[18] + z[20] + z[21] + z[22];
z[23] = 2 * abb[38];
z[4] = z[4] * z[23];
z[24] = 2 * abb[72];
z[25] = abb[71] + abb[73];
z[26] = z[24] + z[25];
z[27] = z[5] + z[7];
z[28] = z[26] + z[27];
z[29] = abb[6] * z[28];
z[30] = abb[12] * z[15];
z[31] = abb[25] * z[3];
z[30] = z[30] + -z[31];
z[32] = abb[3] * z[15];
z[33] = z[29] + z[30] + z[32];
z[34] = abb[60] + -abb[65];
z[35] = 2 * abb[61] + z[1];
z[36] = z[34] + z[35];
z[37] = abb[30] + z[0];
z[36] = z[36] * z[37];
z[37] = 2 * abb[5];
z[38] = z[6] * z[37];
z[39] = abb[72] + z[25];
z[40] = 2 * abb[0];
z[41] = z[39] * z[40];
z[42] = abb[9] * z[15];
z[43] = -abb[14] * z[15];
z[44] = abb[61] + -abb[65];
z[45] = abb[60] + z[44];
z[46] = abb[28] * z[45];
z[36] = z[20] + z[33] + z[36] + z[38] + -z[41] + -4 * z[42] + z[43] + 2 * z[46];
z[36] = abb[39] * z[36];
z[43] = z[6] + z[19];
z[47] = abb[7] * z[43];
z[48] = abb[2] * z[6];
z[49] = -z[14] + z[48];
z[50] = abb[3] * z[6];
z[50] = z[49] + z[50];
z[51] = abb[5] * z[6];
z[47] = -z[18] + z[47] + -z[50] + z[51];
z[52] = abb[37] * z[47];
z[53] = abb[7] * z[17];
z[54] = abb[72] + abb[73] + z[5];
z[54] = abb[10] * z[54];
z[55] = 4 * z[54];
z[33] = z[33] + -z[53] + -z[55];
z[53] = abb[14] * z[9];
z[56] = abb[30] * z[3];
z[53] = -z[53] + z[56];
z[56] = z[33] + z[41] + -z[53];
z[56] = abb[40] * z[56];
z[57] = -z[18] + z[32];
z[58] = abb[7] * z[9];
z[59] = -z[57] + z[58];
z[60] = z[16] + z[59];
z[61] = -z[10] + -z[30] + z[60];
z[62] = 2 * abb[35];
z[63] = z[61] * z[62];
z[64] = abb[14] * z[6];
z[65] = abb[30] * z[12];
z[64] = z[41] + z[64] + -z[65];
z[65] = 2 * z[54];
z[66] = -z[14] + z[65];
z[67] = abb[6] * z[6];
z[67] = z[66] + z[67];
z[68] = abb[7] * z[6];
z[69] = abb[28] * z[12];
z[70] = -z[64] + z[67] + z[68] + z[69];
z[70] = 2 * z[70];
z[71] = abb[34] * z[70];
z[50] = -z[50] + z[64] + -z[65];
z[64] = 2 * abb[36];
z[50] = z[50] * z[64];
z[64] = abb[37] * z[12];
z[72] = abb[40] * z[3];
z[73] = abb[35] * z[3];
z[74] = z[64] + z[72] + -z[73];
z[75] = 2 * abb[28];
z[76] = -z[74] * z[75];
z[77] = abb[73] + z[7] + z[24];
z[78] = 2 * z[5];
z[79] = z[77] + z[78];
z[79] = abb[6] * z[79];
z[65] = -z[65] + z[79];
z[79] = abb[7] * z[8];
z[79] = z[22] + z[65] + -z[79];
z[80] = z[21] + z[79];
z[81] = abb[14] * z[8];
z[82] = abb[30] * z[45];
z[81] = z[80] + -z[81] + z[82];
z[46] = z[46] + z[81];
z[82] = 2 * abb[41];
z[83] = -z[46] * z[82];
z[84] = -abb[39] + abb[41];
z[85] = -z[45] * z[84];
z[86] = abb[34] * z[12];
z[74] = -z[74] + z[85] + z[86];
z[85] = 2 * abb[26];
z[74] = z[74] * z[85];
z[86] = 2 * z[64] + z[72];
z[86] = -z[0] * z[86];
z[4] = z[4] + z[36] + z[50] + -2 * z[52] + z[56] + z[63] + z[71] + z[74] + z[76] + z[83] + z[86];
z[4] = m1_set::bc<T>[0] * z[4];
z[36] = abb[52] * z[61];
z[52] = abb[53] * z[81];
z[36] = -z[36] + z[52];
z[52] = -abb[29] * z[12];
z[52] = -z[47] + z[52] + -z[69];
z[56] = 2 * z[12];
z[63] = -abb[27] * z[56];
z[52] = 2 * z[52] + z[63];
z[52] = abb[51] * z[52];
z[28] = abb[18] * z[28];
z[63] = abb[19] + abb[22];
z[63] = z[15] * z[63];
z[69] = abb[23] * z[9];
z[71] = abb[33] * z[3];
z[28] = z[28] + z[63] + z[69] + -z[71];
z[63] = -abb[54] * z[28];
z[69] = abb[50] * z[70];
z[71] = abb[52] * z[3];
z[74] = abb[53] * z[45];
z[71] = z[71] + -z[74];
z[74] = abb[50] + -abb[51];
z[74] = z[12] * z[74];
z[74] = z[71] + z[74];
z[74] = z[74] * z[85];
z[76] = abb[74] + z[15];
z[81] = -abb[55] + -abb[56];
z[81] = z[76] * z[81];
z[71] = z[71] * z[75];
z[75] = 3 * abb[74];
z[83] = z[9] + -z[75];
z[83] = abb[58] * z[83];
z[85] = 2 * abb[63];
z[86] = -abb[66] + z[85];
z[87] = z[2] + z[86];
z[88] = abb[85] * z[87];
z[89] = -abb[74] + z[15];
z[89] = abb[57] * z[89];
z[4] = z[4] + -2 * z[36] + z[52] + z[63] + z[69] + z[71] + z[74] + z[81] + z[83] + z[88] + z[89];
z[36] = 2 * abb[68];
z[52] = 2 * abb[74];
z[63] = z[36] + z[52];
z[69] = -abb[70] + abb[73];
z[71] = -z[5] + z[69];
z[74] = z[63] + -z[71];
z[74] = abb[0] * z[74];
z[81] = abb[13] * abb[68];
z[83] = abb[21] * abb[75];
z[81] = z[81] + z[83];
z[83] = 2 * z[81];
z[88] = abb[13] * z[5];
z[89] = z[83] + z[88];
z[90] = abb[74] + z[8];
z[91] = 2 * z[90];
z[92] = abb[1] * z[91];
z[93] = z[16] + z[92];
z[94] = -abb[74] + z[6];
z[95] = 2 * abb[8];
z[94] = z[94] * z[95];
z[96] = -z[74] + z[89] + z[93] + z[94];
z[97] = -abb[71] + z[78];
z[98] = z[36] + -z[69] + z[97];
z[99] = abb[74] + z[98];
z[99] = abb[2] * z[99];
z[100] = abb[5] * z[15];
z[101] = abb[20] * abb[75];
z[102] = abb[68] + z[5];
z[103] = abb[11] * z[102];
z[104] = 2 * z[103];
z[105] = z[101] + z[104];
z[106] = abb[5] + -abb[13];
z[107] = abb[3] + abb[7];
z[108] = -abb[6] + -2 * z[106] + z[107];
z[108] = abb[74] * z[108];
z[109] = abb[60] + z[86];
z[110] = z[12] + z[109];
z[111] = -abb[28] * z[110];
z[112] = -4 * abb[63] + 2 * abb[66];
z[113] = -abb[60] + z[112];
z[114] = -z[2] + z[113];
z[115] = abb[27] * z[114];
z[116] = abb[13] * abb[71];
z[117] = -abb[29] * z[45];
z[68] = z[21] + -z[22] + -z[68] + -z[96] + z[99] + z[100] + z[105] + -z[108] + z[111] + z[115] + -z[116] + z[117];
z[68] = abb[38] * z[68];
z[99] = abb[68] + abb[74];
z[111] = 3 * z[5];
z[99] = 3 * abb[70] + 4 * z[99] + z[111];
z[115] = 3 * abb[73];
z[117] = 2 * abb[71];
z[118] = -z[99] + z[115] + z[117];
z[118] = abb[0] * z[118];
z[119] = abb[14] * z[91];
z[85] = -abb[65] + abb[66] + -z[85];
z[120] = abb[61] + z[85];
z[121] = 2 * z[120];
z[122] = abb[30] * z[121];
z[119] = z[119] + -z[122];
z[122] = -abb[3] + abb[7];
z[123] = -abb[13] + z[122];
z[124] = z[52] * z[123];
z[125] = z[119] + z[124];
z[126] = 4 * abb[8];
z[127] = abb[74] + z[102];
z[128] = z[126] * z[127];
z[118] = z[118] + z[125] + z[128];
z[128] = 2 * abb[60];
z[129] = z[12] + z[112] + -z[128];
z[130] = -abb[28] * z[129];
z[131] = abb[17] * abb[75];
z[132] = z[116] + z[131];
z[103] = 4 * z[103] + -z[132];
z[133] = -abb[6] * z[17];
z[134] = -z[69] + -z[97];
z[134] = abb[7] * z[134];
z[5] = abb[71] + z[5];
z[135] = -z[5] + -z[36];
z[135] = abb[2] * z[135];
z[136] = -abb[61] + 2 * abb[65] + z[1];
z[112] = -z[112] + z[136];
z[137] = abb[29] * z[112];
z[89] = z[14] + -z[89] + -2 * z[101] + z[103] + z[118] + z[130] + z[133] + z[134] + z[135] + z[137];
z[89] = abb[34] * z[89];
z[130] = abb[3] * z[9];
z[133] = z[18] + z[130];
z[134] = 3 * abb[68] + z[111];
z[135] = 2 * abb[73];
z[137] = 2 * abb[70] + -z[117] + z[134] + -z[135];
z[137] = abb[7] * z[137];
z[138] = -z[78] + -z[90] + z[117];
z[138] = abb[2] * z[138];
z[139] = abb[5] * z[19];
z[140] = -z[108] + z[139];
z[14] = z[14] + z[96] + -z[103] + -z[133] + z[137] + z[138] + -z[140];
z[14] = abb[37] * z[14];
z[96] = abb[5] * z[8];
z[103] = -z[11] + z[13] + z[96];
z[107] = -abb[6] + z[37] + -z[107];
z[107] = abb[74] * z[107];
z[137] = z[6] + 3 * z[8];
z[138] = abb[7] * z[137];
z[141] = z[107] + -z[138];
z[142] = z[40] * z[76];
z[142] = -z[94] + z[142];
z[143] = -z[6] + z[90];
z[144] = abb[2] * z[143];
z[103] = -z[93] + -2 * z[103] + z[119] + -z[133] + -z[141] + -z[142] + z[144];
z[133] = -abb[36] * z[103];
z[132] = z[81] + z[132];
z[144] = -z[104] + z[132];
z[145] = -z[96] + z[144];
z[146] = z[57] + z[94];
z[147] = abb[2] * z[76];
z[93] = -z[93] + z[147];
z[108] = z[93] + -z[108];
z[69] = abb[71] + z[69];
z[134] = z[69] + -z[134];
z[134] = abb[7] * z[134];
z[148] = abb[68] + abb[71];
z[149] = abb[74] + z[148];
z[149] = z[40] * z[149];
z[134] = z[108] + z[134] + -2 * z[145] + -z[146] + z[149];
z[145] = -abb[35] * z[134];
z[38] = z[38] + -z[59] + z[108] + z[142];
z[38] = abb[40] * z[38];
z[59] = abb[35] * z[87];
z[108] = abb[36] * z[87];
z[59] = z[59] + -z[108];
z[149] = -abb[61] + -2 * z[1] + z[85];
z[149] = abb[37] * z[149];
z[150] = abb[40] * z[87];
z[149] = -z[59] + z[149] + -z[150];
z[151] = -abb[28] * z[149];
z[152] = abb[37] * z[87];
z[153] = abb[40] * z[109];
z[154] = z[152] + z[153];
z[59] = z[59] + z[154];
z[155] = 2 * abb[27];
z[59] = z[59] * z[155];
z[64] = z[64] + -2 * z[108];
z[64] = abb[29] * z[64];
z[14] = z[14] + -z[38] + z[59] + z[64] + z[68] + z[89] + z[133] + z[145] + z[151];
z[14] = abb[38] * z[14];
z[59] = z[9] + -z[52];
z[59] = abb[14] * z[59];
z[64] = abb[30] * z[114];
z[59] = z[59] + z[64];
z[64] = -abb[6] * z[39];
z[64] = -z[32] + z[64] + z[96];
z[68] = -z[92] + z[147];
z[27] = abb[74] + z[27];
z[89] = abb[72] + z[27];
z[96] = z[40] * z[89];
z[58] = z[18] + -z[30] + z[58] + z[59] + 2 * z[64] + -z[68] + -z[94] + z[96] + z[107];
z[58] = abb[40] * z[58];
z[64] = abb[5] * z[17];
z[18] = z[18] + -z[64] + z[119];
z[26] = z[26] + z[27];
z[26] = z[26] * z[40];
z[26] = z[26] + -z[55];
z[27] = abb[71] + z[7];
z[64] = 4 * abb[72] + z[27] + z[111] + z[115];
z[64] = abb[6] * z[64];
z[5] = z[5] + z[24];
z[7] = 2 * z[7];
z[94] = -z[5] + -z[7] + -z[75];
z[94] = z[94] * z[95];
z[115] = abb[3] * z[137];
z[133] = abb[3] + abb[6];
z[37] = -abb[7] + -z[37] + 3 * z[133];
z[37] = abb[74] * z[37];
z[75] = z[75] + z[137];
z[75] = abb[2] * z[75];
z[137] = abb[1] * z[90];
z[37] = z[18] + z[26] + z[37] + z[64] + z[75] + z[94] + z[115] + -6 * z[137] + -z[138];
z[37] = abb[36] * z[37];
z[7] = 3 * abb[72] + z[7] + z[25] + z[52] + z[78];
z[7] = z[7] * z[40];
z[25] = z[59] + -z[124];
z[40] = z[89] * z[126];
z[7] = z[7] + z[25] + -z[40];
z[59] = abb[71] + -z[77] + -z[111];
z[59] = abb[6] * z[59];
z[64] = -abb[29] * z[114];
z[75] = abb[28] * z[121];
z[20] = -z[7] + z[20] + -z[30] + 2 * z[48] + z[55] + z[59] + z[64] + -z[75] + z[130];
z[20] = abb[34] * z[20];
z[55] = abb[7] * z[15];
z[18] = -z[18] + -z[32] + z[55] + -z[93] + z[107] + z[142];
z[18] = abb[37] * z[18];
z[55] = abb[12] * z[17];
z[31] = -2 * z[31] + z[55] + -z[60] + -z[68] + -z[140] + -z[142];
z[31] = abb[35] * z[31];
z[55] = abb[2] * z[90];
z[55] = z[55] + -z[92];
z[59] = abb[14] * z[90];
z[60] = abb[30] * z[120];
z[59] = z[59] + -z[60];
z[60] = abb[29] * z[109];
z[64] = abb[6] + -abb[13];
z[64] = abb[74] * z[64];
z[64] = -z[21] + z[55] + z[59] + z[60] + z[64];
z[23] = z[23] * z[64];
z[2] = -z[2] + z[86] + z[128];
z[64] = -abb[35] + abb[40];
z[2] = z[2] * z[64];
z[35] = z[35] + z[85];
z[35] = abb[37] * z[35];
z[2] = z[2] + -z[35] + z[108];
z[35] = -abb[28] * z[2];
z[64] = -abb[6] + z[122];
z[64] = abb[74] * z[64];
z[64] = z[64] + -z[79];
z[68] = z[89] * z[95];
z[68] = z[68] + -z[96];
z[77] = abb[28] * z[120];
z[55] = -z[55] + z[64] + z[68] + -z[77];
z[55] = abb[39] * z[55];
z[79] = abb[36] + -abb[37];
z[79] = z[79] * z[121];
z[85] = abb[40] * z[114];
z[79] = z[79] + -z[85];
z[86] = -abb[29] * z[79];
z[90] = z[62] * z[109];
z[92] = abb[34] * z[3];
z[79] = -z[79] + z[90] + z[92];
z[79] = abb[27] * z[79];
z[18] = z[18] + z[20] + z[23] + z[31] + z[35] + z[37] + z[55] + z[58] + z[79] + z[86];
z[18] = abb[39] * z[18];
z[20] = abb[16] * z[127];
z[23] = 4 * z[20] + -z[105];
z[31] = z[8] + z[52];
z[31] = abb[14] * z[31];
z[24] = -abb[73] + z[24] + z[99];
z[24] = abb[0] * z[24];
z[35] = z[44] + z[113];
z[37] = abb[29] * z[35];
z[44] = -abb[6] + -abb[13];
z[44] = z[44] * z[52];
z[55] = -z[52] + -z[102];
z[55] = abb[2] * z[55];
z[58] = -abb[30] * z[35];
z[79] = abb[70] + abb[72];
z[86] = z[79] * z[95];
z[90] = -abb[6] * z[8];
z[44] = z[23] + -z[24] + z[31] + -z[37] + z[44] + z[55] + z[58] + -z[81] + z[86] + -z[88] + z[90];
z[44] = abb[42] * z[44];
z[55] = -abb[5] + z[133];
z[55] = z[52] * z[55];
z[27] = abb[72] + z[27] + z[52];
z[27] = z[27] * z[95];
z[5] = z[5] + z[135];
z[5] = abb[6] * z[5];
z[58] = abb[5] * z[43];
z[86] = 4 * z[137];
z[5] = z[5] + -z[27] + z[55] + -z[58] + -z[86];
z[27] = z[6] + -z[19] + -z[52];
z[27] = abb[14] * z[27];
z[55] = abb[3] * z[43];
z[43] = -z[43] + -z[52];
z[43] = abb[2] * z[43];
z[58] = -abb[30] * z[112];
z[27] = -z[5] + z[27] + z[43] + -z[55] + z[58];
z[27] = abb[46] * z[27];
z[43] = z[88] + -z[131];
z[58] = z[43] + z[81];
z[88] = abb[7] * z[102];
z[22] = -z[22] + -z[58] + z[88];
z[88] = abb[2] * z[102];
z[88] = z[21] + z[22] + z[88] + z[101];
z[88] = abb[45] * z[88];
z[27] = z[27] + z[44] + z[88];
z[44] = z[150] + z[152];
z[88] = -abb[35] + abb[36];
z[88] = z[44] * z[88];
z[90] = abb[45] * z[45];
z[94] = abb[77] * z[12];
z[90] = z[90] + -z[94];
z[99] = abb[47] * z[120];
z[99] = z[90] + z[99];
z[102] = prod_pow(abb[37], 2);
z[105] = z[12] * z[102];
z[88] = -z[88] + 2 * z[99] + z[105];
z[99] = -abb[34] * z[129];
z[107] = -abb[38] * z[110];
z[99] = z[99] + z[107] + -z[149];
z[99] = abb[38] * z[99];
z[107] = -abb[34] * z[121];
z[108] = -abb[39] * z[120];
z[2] = -z[2] + z[107] + z[108];
z[2] = abb[39] * z[2];
z[107] = abb[37] * z[112];
z[108] = 2 * z[73];
z[110] = -z[107] + z[108] + -2 * z[150];
z[111] = -abb[34] * z[114];
z[111] = z[110] + z[111];
z[111] = abb[34] * z[111];
z[84] = -abb[37] + abb[38] + -z[84];
z[84] = z[84] * z[120];
z[113] = abb[34] * z[35];
z[84] = z[84] + z[113] + z[153];
z[82] = z[82] * z[84];
z[84] = 2 * abb[79];
z[45] = z[45] * z[84];
z[113] = 2 * abb[48];
z[115] = z[109] * z[113];
z[121] = 2 * abb[44];
z[126] = z[114] * z[121];
z[56] = -abb[76] * z[56];
z[129] = 2 * z[3];
z[130] = -abb[78] * z[129];
z[2] = z[2] + z[45] + z[56] + z[82] + -z[88] + z[99] + z[111] + z[115] + z[126] + z[130];
z[2] = abb[26] * z[2];
z[45] = abb[2] * z[148];
z[45] = z[30] + z[45] + z[101];
z[43] = z[43] + z[83] + z[116];
z[56] = z[63] + z[78] + z[79];
z[56] = z[56] * z[95];
z[37] = -z[37] + z[56];
z[56] = abb[0] * z[89];
z[63] = -abb[28] * z[114];
z[78] = abb[68] + z[97];
z[79] = abb[7] * z[78];
z[25] = -z[25] + z[37] + -z[43] + z[45] + -z[55] + -4 * z[56] + z[63] + z[67] + z[79];
z[25] = abb[34] * z[25];
z[56] = -abb[7] * z[98];
z[17] = abb[3] * z[17];
z[17] = z[17] + z[43] + z[49] + z[56] + -z[118];
z[17] = abb[37] * z[17];
z[7] = z[7] + z[33];
z[7] = abb[40] * z[7];
z[33] = z[71] + z[117];
z[33] = abb[0] * z[33];
z[43] = z[33] + -z[144];
z[16] = z[16] + z[79];
z[49] = z[16] + -z[30] + -z[43];
z[49] = z[49] * z[62];
z[56] = abb[28] * z[110];
z[62] = z[85] + -z[107];
z[62] = abb[29] * z[62];
z[7] = z[7] + z[17] + z[25] + z[49] + z[50] + z[56] + z[62];
z[7] = abb[34] * z[7];
z[17] = abb[27] + abb[28];
z[17] = z[17] * z[114];
z[25] = -abb[6] + -abb[7] + z[106];
z[25] = z[25] * z[52];
z[49] = -z[52] + -z[148];
z[49] = abb[2] * z[49];
z[50] = -z[78] * z[95];
z[16] = -z[16] + z[17] + z[23] + z[25] + z[49] + z[50] + z[100] + -z[132];
z[16] = z[16] * z[121];
z[17] = z[24] + -z[37];
z[23] = abb[28] + abb[30];
z[23] = z[23] * z[35];
z[23] = z[17] + -z[22] + z[23] + -z[31] + -z[124];
z[23] = abb[34] * z[23];
z[24] = z[95] * z[127];
z[22] = z[22] + z[24] + -z[74];
z[25] = abb[74] * z[123];
z[31] = z[25] + z[59];
z[35] = z[22] + z[31];
z[35] = abb[37] * z[35];
z[37] = abb[30] * z[109];
z[49] = abb[14] * abb[74];
z[25] = z[25] + z[37] + z[49] + z[68];
z[25] = abb[40] * z[25];
z[31] = z[31] + -z[77];
z[37] = abb[29] * z[120];
z[22] = -z[22] + -z[31] + z[37];
z[22] = abb[38] * z[22];
z[31] = -z[31] + -z[60] + -z[68] + z[80];
z[31] = abb[39] * z[31];
z[37] = abb[37] * z[120];
z[37] = z[37] + -z[153];
z[49] = -abb[28] + -abb[29];
z[37] = z[37] * z[49];
z[22] = z[22] + z[23] + z[25] + z[31] + z[35] + z[37];
z[23] = -z[36] + z[71];
z[23] = abb[7] * z[23];
z[19] = abb[3] * z[19];
z[17] = -z[17] + -z[19] + -z[21] + -z[23] + -z[58] + -z[65] + -z[75] + z[125];
z[17] = abb[41] * z[17];
z[17] = z[17] + 2 * z[22];
z[17] = abb[41] * z[17];
z[21] = abb[37] * z[103];
z[22] = abb[30] * z[87];
z[25] = abb[14] * z[143];
z[22] = z[22] + z[25];
z[22] = 2 * z[22] + -z[26] + z[93] + -z[139] + -z[141] + z[146];
z[22] = abb[40] * z[22];
z[25] = abb[2] * z[91];
z[5] = -z[5] + -z[19] + -z[25] + -z[41] + z[66] + -z[119];
z[5] = abb[36] * z[5];
z[5] = z[5] + z[21] + z[22];
z[5] = abb[36] * z[5];
z[19] = abb[37] * z[134];
z[10] = -z[10] + z[23] + z[43] + -z[57];
z[10] = abb[35] * z[10];
z[10] = z[10] + z[19] + z[38];
z[10] = abb[35] * z[10];
z[11] = -z[11] + z[13] + -2 * z[29] + z[48] + -z[51] + 8 * z[54] + -z[55];
z[13] = z[30] + -z[42] + -z[53];
z[1] = abb[61] * (T(1) / T(3)) + -z[1] + (T(4) / T(3)) * z[34];
z[19] = abb[26] + abb[28];
z[1] = z[1] * z[19];
z[19] = z[128] + -z[136];
z[0] = (T(1) / T(3)) * z[0];
z[0] = z[0] * z[19];
z[8] = -z[6] + (T(-2) / T(3)) * z[8];
z[8] = abb[7] * z[8];
z[19] = abb[0] * z[39];
z[0] = z[0] + z[1] + z[8] + (T(-1) / T(3)) * z[11] + (T(2) / T(3)) * z[13] + (T(4) / T(3)) * z[19];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = -z[32] + z[33] + z[45] + -z[81] + -z[116];
z[8] = 2 * abb[43];
z[1] = z[1] * z[8];
z[8] = abb[36] * z[112];
z[8] = -z[8] + 2 * z[44];
z[8] = abb[36] * z[8];
z[8] = z[8] + -z[105];
z[11] = -z[72] + -z[92] + z[108];
z[11] = abb[34] * z[11];
z[13] = abb[46] * z[112];
z[19] = -z[13] + z[94];
z[21] = -z[73] + -2 * z[154];
z[21] = abb[35] * z[21];
z[22] = -abb[43] * z[129];
z[11] = z[8] + z[11] + 2 * z[19] + z[21] + z[22];
z[11] = abb[27] * z[11];
z[19] = abb[2] + abb[6] + z[122];
z[19] = abb[74] * z[19];
z[21] = abb[28] * z[109];
z[19] = z[19] + -2 * z[20] + z[21] + z[24] + z[104];
z[19] = z[19] * z[113];
z[20] = abb[80] * z[28];
z[21] = 2 * abb[77] + -z[102];
z[21] = z[21] * z[47];
z[22] = z[64] + -z[96];
z[22] = 2 * z[22] + -z[25] + z[40] + z[86];
z[22] = abb[47] * z[22];
z[23] = abb[20] + abb[21] + -abb[24];
z[6] = z[6] * z[23];
z[12] = abb[32] * z[12];
z[23] = -abb[0] + abb[7];
z[23] = abb[75] * z[23];
z[24] = abb[17] * z[69];
z[6] = z[6] + z[12] + z[23] + z[24];
z[6] = abb[49] * z[6];
z[12] = -abb[28] * z[88];
z[13] = -z[13] + -z[90];
z[8] = z[8] + 2 * z[13];
z[8] = abb[29] * z[8];
z[13] = -abb[76] * z[70];
z[3] = -abb[28] * z[3];
z[3] = z[3] + -z[61];
z[23] = 2 * abb[78];
z[3] = z[3] * z[23];
z[23] = z[46] * z[84];
z[24] = abb[82] + -abb[83];
z[15] = z[15] * z[24];
z[24] = abb[59] * z[87];
z[9] = -abb[84] * z[9];
z[25] = abb[81] * z[76];
z[26] = abb[82] + abb[83] + 3 * abb[84];
z[26] = abb[74] * z[26];
z[0] = z[0] + z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + 2 * z[27];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_804_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("2.524847202924887863973136789500371089537089249336092385131738684")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("5.1156137591021159324521833062370106254853630667671669020404408748"),stof<T>("-8.1590970221738957400031467760741852677013306533114953925001605429")}, std::complex<T>{T(0),stof<T>("-4.8838051494255641203781278846932830704884750971992815173794895611")}, std::complex<T>{stof<T>("-5.1156137591021159324521833062370106254853630667671669020404408748"),stof<T>("8.1590970221738957400031467760741852677013306533114953925001605429")}, std::complex<T>{stof<T>("2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("-4.9667497776376699241622007318470126247813267979357331438214834646")}, std::complex<T>{T(0),stof<T>("2.4419025747127820601890639423466415352442375485996407586897447806")}, std::complex<T>{stof<T>("-2.4939003900426225558292045134710842137374617248643126052941063816"),stof<T>("3.0920666745614964014334366177485018354415681810202102422555504278")}, std::complex<T>{stof<T>("-17.040385928760769488196743423687018007180412614622192395855445486"),stof<T>("-5.290163050565290899345303063427136709837723615997776667436138894")}, std::complex<T>{stof<T>("-2.4939003900426225558292045134710842137374617248643126052941063816"),stof<T>("3.0920666745614964014334366177485018354415681810202102422555504278")}, std::complex<T>{stof<T>("-14.337465154743132696112718342242759003832236793458855653146413676"),stof<T>("-5.290163050565290899345303063427136709837723615997776667436138894")}, std::complex<T>{stof<T>("-13.842052801351284171667853128630002617836076455602366060010560602"),stof<T>("-9.889730798974770082178024667461046681760135504376391620493800699")}, std::complex<T>{stof<T>("-13.633032417376269935413032560656827828225362359303341922595635174"),stof<T>("-6.797664124413273680744588049712544846318567323356181378238250271")}, std::complex<T>{stof<T>("0.7044327373668627606996857815859311756068744341555137305507785022"),stof<T>("-1.5075010738479827813992849862854081364808437073584047108021113766")}, stof<T>("-11.0940356532336271997514588314622372152655935607305737954051615464"), stof<T>("-4.8774633435761477613991183528483265100206166505662406047528825957")};
	
	std::vector<C> intdlogs = {rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[99].real()/kbase.W[99].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[159]) - log_iphi_im(kbase.W[159]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_804_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_804_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-7.1765302386471297128623954798335858204546452489373033659790159279"),stof<T>("5.0812086329262811649309628951172101118924354693940436858887864507")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({198});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,86> abb = {dl[0], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W21(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W61(k,dl), dlog_W118(k,dv), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_21(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[99].real()/k.W[99].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[159]) - log_iphi_im(k.W[159])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_7_re(k), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W162(k,dv) * f_2_30(k);
abb[81] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,198,161>(k, dv, abb[81], abb[55], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W164(k,dv) * f_2_30(k);
abb[82] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,198,163>(k, dv, abb[82], abb[56], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W183(k,dv) * f_2_30(k);
abb[83] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,198,182>(k, dv, abb[83], abb[57], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W185(k,dv) * f_2_30(k);
abb[84] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,198,184>(k, dv, abb[84], abb[58], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W189(k,dv) * f_2_30(k);
abb[85] = c.real();
abb[59] = c.imag();
SpDLog_Sigma5<T,198,188>(k, dv, abb[85], abb[59], f_2_30_series_coefficients<T>);
}

                    
            return f_4_804_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_804_DLogXconstant_part(base_point<T>, kend);
	value += f_4_804_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_804_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_804_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_804_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_804_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_804_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_804_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
