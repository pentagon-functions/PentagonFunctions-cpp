/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_137.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_137_abbreviated (const std::array<T,59>& abb) {
T z[123];
z[0] = abb[43] + abb[49];
z[1] = abb[46] * (T(1) / T(12));
z[2] = abb[51] * (T(1) / T(3));
z[3] = 3 * abb[47];
z[4] = 3 * abb[45];
z[5] = -11 * abb[44] + abb[50] + z[4];
z[5] = abb[48] * (T(13) / T(3)) + (T(1) / T(2)) * z[5];
z[0] = abb[41] * (T(-13) / T(6)) + abb[42] * (T(-5) / T(6)) + (T(-1) / T(12)) * z[0] + -z[1] + -z[2] + z[3] + (T(1) / T(2)) * z[5];
z[0] = abb[8] * z[0];
z[5] = abb[49] * (T(1) / T(4));
z[6] = 3 * abb[50];
z[7] = abb[44] + abb[45] * (T(-5) / T(3)) + abb[43] * (T(7) / T(3)) + -z[6];
z[1] = abb[42] * (T(1) / T(3)) + -z[1] + z[2] + z[5] + (T(1) / T(4)) * z[7];
z[1] = abb[3] * z[1];
z[7] = abb[47] + abb[50];
z[8] = abb[45] * (T(1) / T(3));
z[9] = abb[51] * (T(2) / T(3));
z[10] = abb[49] * (T(1) / T(2));
z[7] = abb[42] * (T(-7) / T(6)) + abb[46] * (T(-5) / T(6)) + abb[43] * (T(-1) / T(6)) + (T(1) / T(2)) * z[7] + z[8] + -z[9] + z[10];
z[7] = abb[2] * z[7];
z[11] = abb[44] + abb[50];
z[12] = -abb[45] + z[11];
z[13] = abb[43] * (T(1) / T(2));
z[14] = abb[47] + z[13];
z[12] = (T(1) / T(2)) * z[12] + -z[14];
z[15] = abb[46] * (T(1) / T(2));
z[16] = z[10] + -z[15];
z[17] = abb[42] + z[12] + -z[16];
z[18] = abb[14] * z[17];
z[19] = abb[43] + -abb[50];
z[20] = abb[44] + -abb[45];
z[21] = z[19] + z[20];
z[22] = -abb[49] + z[21];
z[22] = abb[42] + z[15] + (T(1) / T(2)) * z[22];
z[23] = abb[5] * z[22];
z[24] = abb[52] + -abb[53] + -abb[54] + abb[55];
z[25] = -abb[21] * z[24];
z[26] = (T(1) / T(2)) * z[25];
z[27] = -z[18] + z[23] + z[26];
z[8] = abb[43] * (T(-1) / T(3)) + -z[8] + z[11];
z[28] = abb[46] + abb[49];
z[8] = (T(1) / T(2)) * z[8] + -z[9] + (T(-1) / T(6)) * z[28];
z[8] = abb[15] * z[8];
z[9] = abb[46] * (T(3) / T(4));
z[29] = 3 * abb[44];
z[30] = abb[50] + abb[43] * (T(-5) / T(3)) + abb[45] * (T(7) / T(3)) + -z[29];
z[2] = abb[49] * (T(-7) / T(12)) + abb[42] * (T(1) / T(6)) + z[2] + z[9] + (T(1) / T(4)) * z[30];
z[2] = abb[7] * z[2];
z[30] = abb[41] + -abb[48];
z[31] = abb[45] + z[30];
z[32] = abb[46] + -abb[49];
z[33] = z[31] + z[32];
z[34] = abb[1] * z[33];
z[35] = abb[42] + z[32];
z[36] = abb[0] * z[35];
z[37] = -z[34] + z[36];
z[38] = abb[23] + abb[25];
z[38] = (T(-1) / T(4)) * z[38];
z[38] = z[24] * z[38];
z[20] = abb[47] + -z[20] + z[32];
z[20] = abb[11] * z[20];
z[39] = 3 * z[20];
z[40] = abb[46] * (T(7) / T(2));
z[41] = abb[47] + z[11];
z[42] = (T(1) / T(2)) * z[41];
z[43] = -3 * abb[49] + abb[51] + abb[42] * (T(7) / T(2)) + z[40] + -z[42];
z[43] = abb[9] * z[43];
z[0] = z[0] + z[1] + z[2] + z[7] + z[8] + (T(1) / T(2)) * z[27] + (T(-13) / T(6)) * z[37] + z[38] + -z[39] + z[43];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = 2 * abb[51];
z[2] = z[1] + z[10] + z[15];
z[7] = z[6] + z[29];
z[8] = abb[43] + abb[45];
z[27] = z[7] + -z[8];
z[38] = (T(1) / T(2)) * z[27];
z[43] = -z[2] + z[38];
z[44] = abb[12] * z[43];
z[45] = (T(3) / T(2)) * z[24];
z[46] = abb[22] * z[45];
z[47] = z[44] + z[46];
z[48] = abb[25] * z[45];
z[48] = -z[47] + z[48];
z[49] = 3 * z[18] + z[48];
z[50] = -z[1] + z[3];
z[51] = z[8] + z[50];
z[52] = 4 * abb[42];
z[53] = 2 * abb[49];
z[54] = 3 * abb[46] + -z[51] + z[52] + -z[53];
z[54] = abb[2] * z[54];
z[55] = 3 * z[21];
z[56] = -abb[49] + z[55];
z[57] = 2 * abb[42];
z[56] = z[15] + (T(1) / T(2)) * z[56] + z[57];
z[56] = abb[3] * z[56];
z[54] = -z[49] + z[54] + z[56];
z[54] = abb[32] * z[54];
z[56] = 2 * abb[43];
z[58] = abb[49] + z[1];
z[59] = -z[6] + z[56] + z[58];
z[60] = abb[42] + -abb[45];
z[61] = z[59] + z[60];
z[61] = abb[2] * z[61];
z[62] = 2 * abb[3];
z[63] = z[35] * z[62];
z[45] = abb[23] * z[45];
z[64] = (T(3) / T(2)) * z[25] + -z[45];
z[65] = -z[47] + -z[61] + -z[63] + z[64];
z[65] = abb[31] * z[65];
z[66] = abb[30] * z[43];
z[67] = abb[28] * z[43];
z[68] = 2 * abb[31];
z[69] = z[35] * z[68];
z[55] = abb[49] + z[55];
z[55] = abb[42] + -z[15] + (T(1) / T(2)) * z[55];
z[70] = abb[32] * z[55];
z[69] = z[66] + -z[67] + -z[69] + -z[70];
z[69] = abb[7] * z[69];
z[54] = z[54] + z[65] + z[69];
z[65] = abb[47] * (T(3) / T(2));
z[69] = abb[42] * (T(1) / T(2));
z[70] = z[65] + -z[69];
z[71] = z[13] + z[30];
z[72] = abb[50] * (T(3) / T(2));
z[73] = z[2] + -z[70] + z[71] + -z[72];
z[73] = abb[0] * z[73];
z[74] = 2 * z[34];
z[73] = z[73] + z[74];
z[75] = (T(1) / T(2)) * z[24];
z[76] = abb[25] * z[75];
z[76] = z[18] + z[76];
z[77] = abb[23] * z[75];
z[77] = -z[26] + z[77];
z[78] = z[76] + z[77];
z[79] = (T(3) / T(2)) * z[78];
z[80] = -abb[44] + -abb[45] + abb[50];
z[80] = (T(1) / T(2)) * z[80];
z[71] = -z[16] + z[71] + -z[80];
z[81] = abb[13] * z[71];
z[82] = (T(3) / T(2)) * z[81];
z[83] = 5 * abb[45];
z[84] = -z[6] + z[29] + -z[83];
z[85] = abb[49] * (T(3) / T(2));
z[86] = abb[43] * (T(3) / T(2)) + z[85];
z[87] = abb[46] * (T(3) / T(2));
z[88] = z[86] + -z[87];
z[84] = abb[42] + -z[30] + (T(1) / T(2)) * z[84] + z[88];
z[89] = abb[3] * (T(1) / T(2));
z[84] = z[84] * z[89];
z[90] = 2 * abb[46];
z[91] = -abb[49] + z[90];
z[92] = 3 * abb[42] + -z[51] + z[91];
z[93] = abb[2] * z[92];
z[84] = -z[73] + -z[79] + z[82] + z[84] + z[93];
z[84] = abb[30] * z[84];
z[94] = abb[45] + z[7];
z[95] = abb[49] * (T(3) / T(4));
z[70] = abb[43] * (T(3) / T(4)) + -z[9] + -z[30] + z[70] + (T(-1) / T(4)) * z[94] + z[95];
z[70] = abb[8] * z[70];
z[55] = abb[7] * z[55];
z[19] = abb[47] + z[19];
z[96] = 3 * z[19];
z[97] = -z[35] + z[96];
z[98] = abb[2] * z[97];
z[55] = z[55] + z[98];
z[21] = z[21] + -z[32];
z[32] = abb[3] * z[21];
z[37] = (T(-3) / T(4)) * z[32] + -z[37] + (T(1) / T(2)) * z[55] + z[70] + z[79];
z[37] = abb[29] * z[37];
z[59] = z[30] + z[59];
z[59] = abb[0] * z[59];
z[59] = 4 * z[34] + z[59] + -3 * z[81];
z[33] = z[33] * z[62];
z[33] = z[33] + -z[64];
z[70] = z[33] + z[47] + z[59];
z[79] = abb[28] * z[70];
z[26] = z[18] + z[26];
z[98] = (T(3) / T(4)) * z[24];
z[99] = abb[23] * z[98];
z[100] = -z[82] + z[99];
z[101] = abb[25] * z[98];
z[47] = -z[47] + z[101];
z[102] = (T(-3) / T(2)) * z[26] + -z[47] + z[100];
z[103] = z[74] + z[93];
z[104] = -abb[48] + z[13];
z[80] = -z[80] + z[104];
z[105] = abb[41] + z[80];
z[106] = abb[46] * (T(5) / T(2));
z[107] = abb[49] * (T(5) / T(2));
z[105] = -abb[42] + -3 * z[105] + -z[106] + z[107];
z[89] = z[89] * z[105];
z[105] = abb[0] * (T(1) / T(2));
z[97] = -z[97] * z[105];
z[89] = z[89] + z[97] + -z[102] + -z[103];
z[89] = abb[27] * z[89];
z[11] = abb[45] + z[11];
z[97] = (T(3) / T(2)) * z[11];
z[104] = z[3] + z[97] + z[104];
z[108] = abb[41] * (T(1) / T(2));
z[109] = -z[57] + z[108];
z[110] = abb[46] * (T(5) / T(4));
z[104] = -z[1] + z[5] + (T(1) / T(2)) * z[104] + z[109] + -z[110];
z[104] = abb[27] * z[104];
z[111] = abb[31] * z[43];
z[112] = abb[32] * z[92];
z[111] = -z[67] + z[111] + z[112];
z[7] = -z[7] + z[83];
z[7] = (T(1) / T(2)) * z[7];
z[83] = abb[48] + -z[3] + -z[7] + -z[86];
z[83] = z[9] + (T(1) / T(2)) * z[83] + -z[109];
z[83] = abb[30] * z[83];
z[83] = z[83] + z[104] + z[111];
z[83] = abb[8] * z[83];
z[86] = abb[43] + z[29];
z[104] = -abb[45] + z[86];
z[112] = abb[46] + z[1];
z[113] = z[30] + z[104] + -z[112];
z[114] = abb[30] * z[113];
z[115] = abb[28] * z[113];
z[114] = z[114] + -z[115];
z[114] = abb[10] * z[114];
z[37] = z[37] + z[54] + z[79] + z[83] + z[84] + z[89] + -z[114];
z[37] = abb[29] * z[37];
z[83] = abb[51] + z[15];
z[84] = abb[41] * (T(3) / T(2));
z[89] = abb[48] * (T(-3) / T(2)) + z[8] + -z[69] + -z[72] + z[83] + z[84];
z[89] = abb[3] * z[89];
z[116] = abb[15] * z[43];
z[18] = (T(3) / T(2)) * z[18] + z[116];
z[82] = -z[34] + z[82];
z[12] = z[12] + z[16];
z[16] = abb[4] * z[12];
z[117] = (T(3) / T(2)) * z[16];
z[118] = abb[51] + z[10];
z[119] = z[42] + -z[118];
z[120] = 3 * abb[9];
z[119] = z[119] * z[120];
z[96] = z[35] + z[96];
z[96] = z[96] * z[105];
z[89] = z[18] + -z[82] + z[89] + -z[93] + z[96] + z[101] + -z[117] + -z[119];
z[96] = prod_pow(abb[27], 2);
z[89] = z[89] * z[96];
z[101] = abb[48] + -z[6];
z[56] = z[3] + z[56] + (T(1) / T(2)) * z[101] + -z[108] + -z[118];
z[56] = abb[0] * z[56];
z[101] = -z[117] + z[119];
z[4] = abb[43] + 5 * abb[44] + abb[50] + -z[4];
z[105] = -abb[51] + (T(1) / T(4)) * z[4] + z[5] + -z[9];
z[105] = 3 * z[105];
z[119] = abb[6] * z[105];
z[98] = abb[22] * z[98];
z[56] = z[33] + z[39] + z[44] + z[56] + -z[82] + z[98] + -z[101] + z[119];
z[56] = abb[28] * z[56];
z[70] = -abb[27] * z[70];
z[56] = z[56] + z[70];
z[56] = abb[28] * z[56];
z[70] = -abb[48] + -abb[50];
z[70] = -abb[47] + abb[51] + (T(1) / T(2)) * z[70];
z[70] = -z[10] + z[57] + 3 * z[70] + z[84] + z[90];
z[70] = abb[0] * z[70];
z[25] = (T(3) / T(4)) * z[25] + -z[116];
z[82] = -abb[51] + (T(1) / T(4)) * z[27];
z[84] = abb[42] + -z[82] + -z[95] + z[110];
z[84] = abb[3] * z[84];
z[70] = -z[25] + z[34] + z[70] + z[84] + -z[93] + z[98] + z[100] + -z[119];
z[70] = abb[30] * z[70];
z[84] = abb[49] * (T(5) / T(4));
z[90] = z[84] + -z[108];
z[95] = abb[43] * (T(5) / T(2));
z[98] = abb[48] + z[95];
z[100] = abb[45] + z[6];
z[110] = abb[44] + z[100];
z[110] = -z[98] + (T(3) / T(2)) * z[110];
z[116] = abb[46] * (T(1) / T(4));
z[110] = -z[1] + -z[69] + -z[90] + (T(1) / T(2)) * z[110] + z[116];
z[110] = abb[3] * z[110];
z[119] = 4 * abb[51];
z[27] = -z[27] + z[28] + z[119];
z[27] = abb[15] * z[27];
z[28] = abb[42] + -z[41] + z[112];
z[28] = z[28] * z[120];
z[73] = z[27] + -z[28] + z[73] + 2 * z[93] + z[102] + z[110];
z[73] = abb[27] * z[73];
z[102] = z[1] + -z[10] + z[87];
z[4] = (T(1) / T(2)) * z[4] + -z[102];
z[110] = abb[6] * z[4];
z[112] = 3 * z[110];
z[44] = z[44] + z[112];
z[121] = abb[3] * z[43];
z[121] = z[27] + z[121];
z[122] = z[44] + z[121];
z[122] = abb[31] * z[122];
z[70] = z[70] + z[73] + -z[79] + z[122];
z[70] = abb[30] * z[70];
z[38] = -z[1] + z[38] + z[57] + z[87] + -z[107];
z[73] = abb[3] * z[38];
z[79] = 3 * z[16];
z[41] = -z[41] + z[58];
z[58] = z[41] * z[120];
z[58] = z[58] + z[79];
z[7] = z[7] + z[106];
z[87] = -6 * abb[47] + abb[49] * (T(7) / T(2)) + z[1] + -z[7] + -z[95];
z[87] = abb[12] * z[87];
z[61] = z[27] + z[46] + -z[58] + z[61] + z[73] + z[87];
z[61] = abb[31] * z[61];
z[73] = abb[46] + -abb[47];
z[87] = 2 * abb[45];
z[72] = abb[42] * (T(-13) / T(2)) + z[13] + z[72] + (T(-9) / T(2)) * z[73] + z[87] + z[107] + -z[119];
z[72] = abb[2] * z[72];
z[60] = abb[44] * (T(-3) / T(2)) + -z[13] + -z[60] + z[118];
z[60] = abb[3] * z[60];
z[73] = -abb[46] + -abb[51] + z[10];
z[42] = abb[42] + -z[42] + -z[73];
z[42] = z[42] * z[120];
z[18] = z[18] + z[42] + z[47] + z[60] + z[72] + z[117];
z[18] = abb[32] * z[18];
z[42] = z[28] + z[49] + -3 * z[93] + -z[121];
z[47] = -abb[27] + abb[30];
z[42] = z[42] * z[47];
z[18] = z[18] + z[42] + z[61];
z[18] = abb[32] * z[18];
z[42] = z[1] + -z[86] + z[87] + z[91];
z[42] = z[42] * z[68];
z[42] = z[42] + z[67];
z[49] = -9 * abb[47] + -z[97] + -z[98];
z[49] = abb[46] * (T(13) / T(4)) + (T(1) / T(2)) * z[49] + z[52] + -z[90] + z[119];
z[49] = abb[27] * z[49];
z[52] = -2 * z[30];
z[60] = z[52] + z[57];
z[6] = 9 * abb[44] + -z[6];
z[61] = abb[45] + z[6];
z[61] = -abb[51] + abb[46] * (T(-7) / T(4)) + abb[43] * (T(5) / T(4)) + -z[60] + (T(1) / T(4)) * z[61] + z[84];
z[61] = abb[30] * z[61];
z[49] = z[42] + z[49] + z[61];
z[49] = abb[30] * z[49];
z[61] = abb[27] * z[43];
z[67] = z[61] + -z[67];
z[67] = abb[28] * z[67];
z[43] = abb[36] * z[43];
z[43] = -z[43] + z[67];
z[65] = abb[45] + abb[48] * (T(-1) / T(2)) + z[13] + z[65] + z[73] + z[109];
z[65] = z[65] * z[96];
z[67] = 3 * abb[28];
z[68] = z[4] * z[67];
z[68] = -z[61] + z[68];
z[72] = -abb[31] * z[105];
z[72] = z[68] + z[72];
z[72] = abb[31] * z[72];
z[11] = (T(1) / T(2)) * z[11];
z[86] = 2 * abb[47] + z[11] + z[13];
z[87] = -z[57] + z[86] + -z[102];
z[90] = 3 * abb[56];
z[95] = -z[87] * z[90];
z[92] = -z[47] * z[92];
z[8] = z[3] + z[8];
z[8] = abb[42] * (T(-3) / T(2)) + (T(1) / T(2)) * z[8] + z[73];
z[8] = abb[32] * z[8];
z[8] = 3 * z[8] + 2 * z[92];
z[8] = abb[32] * z[8];
z[8] = z[8] + z[43] + z[49] + z[65] + z[72] + z[95];
z[8] = abb[8] * z[8];
z[7] = z[1] + z[7] + z[13] + -z[52] + -z[85];
z[7] = abb[3] * z[7];
z[6] = -5 * abb[43] + 7 * abb[45] + -z[6];
z[6] = z[1] + (T(1) / T(2)) * z[6] + z[40] + -z[107];
z[40] = abb[7] + abb[8];
z[6] = -z[6] * z[40];
z[49] = abb[10] * z[113];
z[6] = z[6] + z[7] + -z[27] + z[46] + z[49] + z[59] + -z[64] + -z[112];
z[6] = abb[34] * z[6];
z[7] = abb[48] + z[10] + -z[11] + z[14];
z[7] = abb[16] * z[7];
z[11] = -abb[18] * z[71];
z[14] = -abb[19] * z[17];
z[10] = abb[42] + -z[10] + -z[80];
z[10] = abb[17] * z[10];
z[27] = -abb[16] + abb[17];
z[27] = z[15] * z[27];
z[46] = -abb[26] * z[75];
z[49] = -abb[16] + -abb[17];
z[49] = abb[41] * z[49];
z[7] = z[7] + z[10] + z[11] + z[14] + z[27] + z[46] + z[49];
z[7] = abb[37] * z[7];
z[9] = -abb[42] + -z[9] + -z[82] + z[84];
z[9] = abb[3] * z[9];
z[10] = -abb[46] + -z[51] + z[53];
z[11] = 2 * abb[12];
z[10] = z[10] * z[11];
z[11] = abb[42] + abb[49] + -z[51];
z[11] = abb[2] * z[11];
z[9] = z[9] + -z[10] + z[11] + -z[25] + -z[39] + z[99] + -z[101];
z[9] = abb[31] * z[9];
z[10] = -z[10] + z[58] + -z[64] + -z[121];
z[11] = abb[27] * z[10];
z[1] = -z[1] + z[15] + -z[85] + z[86];
z[1] = abb[12] * z[1];
z[1] = z[1] + z[16] + z[77];
z[14] = abb[9] * z[41];
z[14] = z[1] + z[14] + z[110];
z[15] = -z[14] * z[67];
z[9] = z[9] + z[11] + z[15];
z[9] = abb[31] * z[9];
z[11] = abb[31] * z[38];
z[15] = abb[43] + z[69] + (T(-1) / T(2)) * z[100] + z[118];
z[15] = abb[32] * z[15];
z[11] = z[11] + z[15] + z[61] + -z[66];
z[11] = abb[32] * z[11];
z[15] = abb[49] * (T(-7) / T(4)) + abb[46] * (T(9) / T(4)) + z[57] + -z[82];
z[15] = abb[31] * z[15];
z[15] = z[15] + z[68];
z[15] = abb[31] * z[15];
z[5] = z[5] + -z[82] + z[116];
z[25] = abb[30] * z[5];
z[25] = z[25] + z[42];
z[25] = abb[30] * z[25];
z[5] = z[5] * z[96];
z[5] = z[5] + z[11] + z[15] + z[25] + z[43];
z[5] = abb[7] * z[5];
z[11] = abb[8] * z[17];
z[15] = z[19] + -z[35];
z[15] = abb[2] * z[15];
z[17] = abb[7] * z[22];
z[11] = z[11] + -z[15] + -z[17] + z[23] + (T(1) / T(2)) * z[32] + -z[78];
z[11] = 3 * z[11];
z[15] = abb[58] * z[11];
z[12] = abb[12] * z[12];
z[17] = abb[22] * z[75];
z[12] = z[12] + -z[16] + z[17];
z[16] = -abb[20] + -abb[24];
z[16] = z[16] * z[75];
z[25] = -abb[3] * z[22];
z[21] = abb[7] * z[21];
z[27] = -abb[2] * z[19];
z[16] = -z[12] + z[16] + (T(1) / T(2)) * z[21] + z[23] + z[25] + z[27];
z[16] = abb[35] * z[16];
z[21] = abb[3] * z[71];
z[19] = abb[0] * z[19];
z[12] = z[12] + z[19] + z[21] + z[34] + -z[81];
z[12] = abb[33] * z[12];
z[4] = z[4] * z[40];
z[4] = z[4] + -z[14];
z[14] = abb[57] * z[4];
z[12] = z[12] + z[14] + z[16];
z[10] = abb[36] * z[10];
z[14] = abb[27] * z[113];
z[16] = -abb[48] + z[104];
z[16] = (T(1) / T(2)) * z[16] + -z[83] + z[108];
z[19] = abb[30] * z[16];
z[19] = z[14] + 3 * z[19] + -z[115];
z[19] = abb[30] * z[19];
z[16] = -abb[28] * z[16];
z[14] = -z[14] + z[16];
z[14] = abb[28] * z[14];
z[14] = z[14] + z[19];
z[14] = abb[10] * z[14];
z[16] = abb[27] * z[24];
z[19] = abb[31] * z[24];
z[16] = z[16] + z[19];
z[21] = abb[28] * z[24];
z[23] = z[16] + -z[21];
z[25] = -abb[28] + abb[30];
z[23] = z[23] * z[25];
z[25] = abb[32] * z[24];
z[16] = z[16] + -z[25];
z[27] = abb[30] * z[24];
z[27] = -z[16] + z[27];
z[34] = abb[32] * z[27];
z[38] = abb[33] + abb[56] + -abb[57];
z[38] = z[24] * z[38];
z[16] = -z[16] + z[21];
z[21] = abb[29] * z[16];
z[21] = z[21] + z[23] + -z[34] + z[38];
z[23] = abb[20] * (T(3) / T(2));
z[21] = z[21] * z[23];
z[17] = z[17] + -z[76];
z[38] = abb[9] * z[35];
z[1] = z[1] + z[17] + -z[38] + z[93];
z[38] = z[1] * z[90];
z[39] = abb[31] * z[22];
z[40] = (T(1) / T(2)) * z[22];
z[40] = abb[32] * z[40];
z[40] = -z[39] + z[40];
z[40] = abb[32] * z[40];
z[22] = abb[29] * z[22];
z[41] = (T(-1) / T(2)) * z[22] + z[39];
z[41] = abb[29] * z[41];
z[40] = z[40] + z[41];
z[41] = 3 * abb[5];
z[40] = z[40] * z[41];
z[42] = z[47] * z[75];
z[19] = -z[19] + z[25] + z[42];
z[19] = abb[29] * z[19];
z[25] = abb[27] * abb[30];
z[25] = z[25] + -z[96];
z[25] = z[25] * z[75];
z[42] = abb[56] * z[24];
z[19] = z[19] + z[25] + -z[34] + z[42];
z[25] = abb[24] * (T(3) / T(2));
z[19] = z[19] * z[25];
z[0] = z[0] + z[5] + z[6] + (T(3) / T(2)) * z[7] + z[8] + z[9] + z[10] + 3 * z[12] + z[14] + z[15] + z[18] + z[19] + z[21] + z[37] + z[38] + z[40] + z[56] + z[70] + z[89];
z[5] = z[35] * z[120];
z[6] = 2 * z[36];
z[5] = z[5] + -z[6] + -3 * z[26] + z[45] + -z[48] + z[63] + z[79] + z[103];
z[5] = abb[27] * z[5];
z[7] = abb[43] + -z[30] + z[50];
z[8] = z[7] + -z[57] + -z[91];
z[9] = 2 * abb[0];
z[8] = z[8] * z[9];
z[10] = -abb[42] + z[31];
z[10] = z[10] * z[62];
z[8] = z[8] + z[10] + -3 * z[17] + z[28] + z[74] + -z[93] + z[112];
z[8] = abb[30] * z[8];
z[7] = abb[49] + -z[7];
z[7] = z[7] * z[9];
z[7] = z[7] + -6 * z[20] + -z[33] + -z[44] + -z[58] + z[74];
z[7] = abb[28] * z[7];
z[3] = -abb[42] + z[3] + z[52];
z[9] = -z[3] + -z[88] + (T(1) / T(2)) * z[94];
z[9] = abb[8] * z[9];
z[6] = z[6] + z[9] + (T(3) / T(2)) * z[32] + -z[55] + -z[74] + -3 * z[78];
z[6] = abb[29] * z[6];
z[2] = z[2] + z[13] + z[60] + -z[97];
z[2] = abb[27] * z[2];
z[3] = abb[45] + z[3] + -z[29];
z[3] = abb[30] * z[3];
z[2] = z[2] + 2 * z[3] + -z[111];
z[2] = abb[8] * z[2];
z[3] = z[22] + -z[39];
z[3] = z[3] * z[41];
z[9] = -z[25] * z[27];
z[2] = z[2] + z[3] + z[5] + z[6] + z[7] + z[8] + z[9] + -z[54] + -2 * z[114];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[40] * z[11];
z[4] = abb[39] * z[4];
z[5] = -abb[8] * z[87];
z[6] = abb[24] * z[75];
z[1] = z[1] + z[5] + z[6];
z[1] = abb[38] * z[1];
z[1] = z[1] + z[4];
z[4] = abb[38] + -abb[39];
z[4] = z[4] * z[24];
z[5] = -m1_set::bc<T>[0] * z[16];
z[4] = z[4] + z[5];
z[4] = z[4] * z[23];
z[1] = 3 * z[1] + z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_137_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-24.536387955142672865451267298611106797937889449949832114388524824"),stof<T>("-19.074924415149657966039011612127756414201695810829806141398734184")}, std::complex<T>{stof<T>("-3.23895802702897259590442901953597545681436802423294922798010256"),stof<T>("37.829815963077187565642504416975882230382881435494741200674329002")}, std::complex<T>{stof<T>("1.0978400858822731583841342481425144531668667160790521128934473712"),stof<T>("-8.9955186541143739080626206894123653425404699660104868076640906612")}, std::complex<T>{stof<T>("-5.421569305254586120209323749229981786920542745143044923173978778"),stof<T>("-2.0547241624318958528565933393634676347451041717558226302035443197")}, std::complex<T>{stof<T>("-6.302479430545530087268814107703917561903324149653201917295686214"),stof<T>("-18.189022525477983978346734424253540974595004440128415625141688194")}, std::complex<T>{stof<T>("-9.541437457574502683173243127239893018717692173886151145275788774"),stof<T>("19.640793437599203587295769992722341255787876995366325575532640809")}, std::complex<T>{stof<T>("32.144087829821972594539716880726911138315454571529267583905145812"),stof<T>("-9.278439037202921785534659653027402098072191396363528405353543")}, std::complex<T>{stof<T>("24.536387955142672865451267298611106797937889449949832114388524824"),stof<T>("19.074924415149657966039011612127756414201695810829806141398734184")}, std::complex<T>{stof<T>("-3.270901761768053974799886314437314430396330381267434128643071057"),stof<T>("-18.471971164838981722131453841233089060649464194311893461586142479")}, std::complex<T>{stof<T>("11.7144991334602834995889951935346929959471558390745331610254124601"),stof<T>("7.826696381354152042898304537923113147402057164956054693717592332")}, std::complex<T>{stof<T>("-25.624678438685113315946258883354414898228045110307170547837719663"),stof<T>("2.337644545520443730328632302978504390276825602108864227892996658")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[86].real()/kbase.W[86].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_137_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_137_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("12.052736040430011002423329966953710393340516777041299346899913236"),stof<T>("64.591807426328764178638793827595488233895930153473749188711738427")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W87(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[86].real()/k.W[86].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k)};

                    
            return f_4_137_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_137_DLogXconstant_part(base_point<T>, kend);
	value += f_4_137_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_137_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_137_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_137_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_137_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_137_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_137_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
