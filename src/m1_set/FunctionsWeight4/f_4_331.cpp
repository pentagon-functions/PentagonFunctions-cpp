/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_331.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_331_abbreviated (const std::array<T,28>& abb) {
T z[40];
z[0] = abb[11] + -abb[14];
z[1] = abb[12] + 5 * z[0];
z[2] = 3 * abb[13];
z[1] = 2 * z[1] + -z[2];
z[1] = abb[13] * z[1];
z[3] = 2 * abb[14];
z[4] = -abb[11] + z[3];
z[5] = 3 * abb[11];
z[6] = z[4] * z[5];
z[7] = abb[15] + abb[26];
z[8] = -abb[12] + 6 * z[0];
z[8] = abb[12] * z[8];
z[9] = prod_pow(abb[14], 2);
z[10] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = -z[1] + -z[6] + 8 * z[7] + z[8] + 3 * z[9] + (T(8) / T(3)) * z[10];
z[1] = abb[1] * z[1];
z[6] = abb[12] + z[0];
z[6] = -z[2] + 2 * z[6];
z[6] = abb[13] * z[6];
z[7] = -abb[12] + 2 * z[0];
z[7] = abb[12] * z[7];
z[7] = z[6] + -z[7] + z[10];
z[7] = abb[5] * z[7];
z[1] = z[1] + z[7];
z[8] = abb[11] * (T(1) / T(2));
z[11] = -abb[14] + z[8];
z[12] = z[5] * z[11];
z[13] = 4 * abb[15];
z[12] = z[12] + z[13];
z[14] = 3 * abb[14];
z[15] = abb[12] * (T(1) / T(2));
z[16] = -4 * abb[11] + z[14] + z[15];
z[16] = abb[12] * z[16];
z[17] = 3 * z[0];
z[18] = abb[12] + z[17];
z[2] = -z[2] + 2 * z[18];
z[2] = abb[13] * z[2];
z[18] = 4 * abb[26];
z[2] = z[2] + (T(-5) / T(6)) * z[10] + -z[12] + z[16] + -z[18];
z[2] = abb[2] * z[2];
z[16] = 2 * abb[26];
z[19] = (T(7) / T(6)) * z[10] + z[16];
z[20] = 2 * abb[13];
z[21] = z[0] * z[20];
z[22] = 2 * abb[15];
z[23] = z[21] + -z[22];
z[24] = -z[3] + z[8];
z[24] = abb[11] * z[24];
z[25] = (T(3) / T(2)) * z[9];
z[26] = abb[12] * z[0];
z[24] = z[19] + -z[23] + z[24] + z[25] + z[26];
z[24] = abb[3] * z[24];
z[27] = abb[11] * z[0];
z[23] = (T(2) / T(3)) * z[10] + -z[23] + z[27];
z[4] = -z[4] + -z[15];
z[4] = abb[12] * z[4];
z[4] = z[4] + z[16] + z[23] + z[25];
z[4] = abb[0] * z[4];
z[27] = 2 * abb[12];
z[28] = abb[11] + z[3] + -z[27];
z[28] = abb[11] * z[28];
z[6] = z[6] + z[28];
z[6] = abb[4] * z[6];
z[23] = z[23] + z[26];
z[28] = z[16] + z[23];
z[28] = abb[6] * z[28];
z[2] = -z[1] + z[2] + z[4] + -z[6] + z[24] + z[28];
z[2] = abb[24] * z[2];
z[4] = abb[18] + 2 * abb[19] + abb[20];
z[24] = 7 * abb[25];
z[29] = 2 * z[4] + -z[24];
z[30] = 2 * abb[2];
z[29] = z[29] * z[30];
z[31] = 2 * abb[25];
z[32] = z[4] + -z[31];
z[33] = abb[6] * z[32];
z[4] = -2 * abb[21] + 20 * abb[25] + -7 * z[4];
z[4] = abb[10] * z[4];
z[24] = 4 * abb[19] + -z[24];
z[34] = abb[20] * (T(7) / T(4)) + abb[18] * (T(9) / T(4)) + z[24];
z[34] = abb[3] * z[34];
z[24] = abb[18] * (T(7) / T(4)) + abb[20] * (T(9) / T(4)) + z[24];
z[24] = abb[0] * z[24];
z[30] = abb[0] + abb[3] + z[30];
z[30] = abb[21] * z[30];
z[35] = 2 * abb[22] + abb[24];
z[35] = abb[9] * z[35];
z[4] = z[4] + z[24] + z[29] + z[30] + z[33] + z[34] + -z[35];
z[24] = -abb[27] * z[4];
z[29] = 4 * abb[14];
z[30] = abb[12] + -z[5] + z[29];
z[30] = abb[12] * z[30];
z[17] = -abb[13] + z[17];
z[17] = z[17] * z[20];
z[33] = 2 * z[10];
z[12] = -6 * abb[26] + (T(-7) / T(2)) * z[9] + -z[12] + z[17] + z[30] + -z[33];
z[12] = abb[2] * z[12];
z[17] = -abb[13] + z[0];
z[17] = z[17] * z[20];
z[17] = (T(3) / T(2)) * z[10] + -z[13] + -z[16] + z[17];
z[30] = abb[14] * (T(1) / T(4));
z[34] = abb[11] + -z[30];
z[34] = abb[11] * z[34];
z[35] = abb[11] + -5 * abb[14];
z[35] = -abb[12] + (T(1) / T(4)) * z[35];
z[35] = abb[12] * z[35];
z[34] = (T(5) / T(4)) * z[9] + -z[17] + z[34] + z[35];
z[34] = abb[0] * z[34];
z[35] = abb[11] + abb[14] * (T(7) / T(2));
z[35] = abb[11] * z[35];
z[36] = (T(9) / T(2)) * z[9];
z[35] = z[35] + -z[36];
z[37] = z[10] + (T(-1) / T(2)) * z[26] + -z[35];
z[37] = abb[3] * z[37];
z[1] = -z[1] + z[12] + 2 * z[28] + z[34] + (T(1) / T(2)) * z[37];
z[1] = abb[22] * z[1];
z[12] = abb[11] + abb[14];
z[28] = -z[8] * z[12];
z[34] = abb[12] + (T(-1) / T(2)) * z[0];
z[34] = abb[12] * z[34];
z[37] = -abb[12] + z[0];
z[38] = -abb[13] + 2 * z[37];
z[38] = abb[13] * z[38];
z[18] = z[9] + (T(1) / T(3)) * z[10] + -z[18] + -z[22] + z[28] + z[34] + z[38];
z[18] = abb[3] * z[18];
z[27] = -abb[13] + z[27];
z[28] = abb[13] * z[27];
z[22] = -z[22] + z[28];
z[34] = abb[14] + z[15];
z[34] = abb[12] * z[34];
z[11] = abb[11] * z[11];
z[11] = z[11] + -z[19] + -z[22] + z[34];
z[11] = abb[2] * z[11];
z[19] = -abb[12] + z[12];
z[19] = z[15] * z[19];
z[38] = abb[14] * (T(1) / T(2));
z[39] = abb[11] + -z[38];
z[39] = abb[11] * z[39];
z[17] = -z[9] + -z[17] + z[19] + z[39];
z[17] = abb[0] * z[17];
z[6] = z[6] + -z[7] + z[11] + z[17] + z[18];
z[6] = abb[23] * z[6];
z[7] = -z[20] * z[27];
z[11] = abb[14] + abb[11] * (T(5) / T(2));
z[11] = abb[11] * z[11];
z[7] = z[7] + (T(-23) / T(6)) * z[10] + z[11] + z[13] + -z[34];
z[7] = abb[19] * z[7];
z[11] = abb[14] + z[5];
z[11] = z[8] * z[11];
z[13] = -z[12] * z[15];
z[11] = (T(-13) / T(6)) * z[10] + z[11] + z[13] + -z[22];
z[11] = abb[20] * z[11];
z[13] = z[15] * z[37];
z[15] = abb[11] + z[38];
z[15] = abb[11] * z[15];
z[13] = (T(-5) / T(3)) * z[10] + z[13] + z[15] + -z[22];
z[13] = abb[18] * z[13];
z[15] = -abb[11] * z[12];
z[15] = -abb[15] + (T(4) / T(3)) * z[10] + z[15];
z[3] = abb[12] + z[3];
z[3] = abb[12] * z[3];
z[3] = z[3] + 2 * z[15] + z[28];
z[3] = z[3] * z[31];
z[15] = z[16] * z[32];
z[16] = abb[14] + z[8];
z[16] = abb[11] * z[16];
z[16] = (T(-1) / T(2)) * z[10] + z[16];
z[17] = z[16] + -z[34];
z[17] = abb[21] * z[17];
z[18] = abb[23] + abb[22] * (T(1) / T(2));
z[19] = abb[27] * z[18];
z[3] = z[3] + z[7] + z[11] + z[13] + -z[15] + z[17] + z[19];
z[3] = abb[8] * z[3];
z[7] = abb[13] * z[0];
z[7] = 4 * z[7] + -3 * z[26];
z[8] = z[8] + z[29];
z[8] = abb[11] * z[8];
z[8] = z[7] + z[8] + (T(-9) / T(2)) * z[10] + -z[36];
z[8] = abb[19] * z[8];
z[11] = abb[11] * abb[14];
z[11] = -z[9] + z[11];
z[11] = (T(9) / T(4)) * z[11] + z[21] + (T(-5) / T(4)) * z[26] + -z[33];
z[11] = abb[18] * z[11];
z[13] = 2 * abb[11];
z[17] = -7 * abb[14] + -z[13];
z[17] = abb[11] * z[17];
z[7] = -z[7] + 9 * z[9] + 6 * z[10] + z[17];
z[7] = abb[25] * z[7];
z[9] = -5 * z[10] + (T(-7) / T(2)) * z[26] + z[35];
z[9] = (T(1) / T(2)) * z[9] + z[21];
z[9] = abb[20] * z[9];
z[10] = z[16] + -z[25];
z[10] = abb[21] * z[10];
z[16] = 2 * abb[24] + abb[23] * (T(1) / T(2)) + abb[22] * (T(7) / T(4));
z[17] = abb[27] * z[16];
z[7] = z[7] + z[8] + z[9] + z[10] + z[11] + -z[15] + z[17];
z[7] = abb[7] * z[7];
z[8] = -z[23] * z[32];
z[8] = z[8] + -z[15];
z[8] = abb[9] * z[8];
z[1] = z[1] + z[2] + z[3] + z[6] + z[7] + z[8] + z[24];
z[2] = -abb[17] * z[4];
z[0] = m1_set::bc<T>[0] * z[0];
z[3] = abb[12] * m1_set::bc<T>[0];
z[4] = z[0] + -z[3];
z[6] = 2 * abb[16];
z[7] = abb[13] * m1_set::bc<T>[0];
z[8] = z[4] + z[6] + z[7];
z[8] = abb[1] * z[8];
z[9] = m1_set::bc<T>[0] * z[20];
z[10] = z[0] + -z[9];
z[10] = abb[5] * z[10];
z[8] = 2 * z[8] + -z[10];
z[8] = 2 * z[8];
z[11] = 4 * abb[16];
z[15] = 2 * z[3];
z[17] = -3 * z[0] + -z[11] + z[15];
z[17] = abb[2] * z[17];
z[19] = -z[3] + z[9];
z[20] = abb[11] * m1_set::bc<T>[0];
z[21] = z[6] + z[19] + z[20];
z[21] = abb[3] * z[21];
z[22] = -z[7] + z[20];
z[23] = 4 * abb[4];
z[22] = z[22] * z[23];
z[23] = abb[14] * m1_set::bc<T>[0];
z[19] = z[19] + -z[23];
z[24] = z[6] + z[19];
z[25] = abb[6] * z[24];
z[24] = abb[0] * z[24];
z[17] = -z[8] + z[17] + z[21] + z[22] + z[24] + z[25];
z[17] = abb[24] * z[17];
z[21] = -z[6] + z[9];
z[24] = -z[3] + -7 * z[23];
z[24] = -z[21] + (T(1) / T(4)) * z[24];
z[24] = abb[0] * z[24];
z[4] = -6 * abb[16] + -3 * z[4] + -z[9];
z[4] = abb[2] * z[4];
z[26] = abb[11] + abb[14] * (T(7) / T(4));
z[26] = m1_set::bc<T>[0] * z[26];
z[26] = (T(1) / T(4)) * z[3] + z[26];
z[26] = abb[3] * z[26];
z[4] = z[4] + -z[8] + z[24] + 2 * z[25] + z[26];
z[4] = abb[22] * z[4];
z[0] = -z[0] + -z[6] + -z[9] + z[15];
z[0] = abb[2] * z[0];
z[8] = -z[3] + -3 * z[23];
z[8] = (T(1) / T(2)) * z[8] + -z[21];
z[8] = abb[0] * z[8];
z[15] = (T(5) / T(2)) * z[3];
z[21] = -abb[11] + abb[14] * (T(5) / T(2));
z[21] = m1_set::bc<T>[0] * z[21];
z[11] = -z[11] + z[15] + z[21];
z[11] = abb[3] * z[11];
z[0] = z[0] + z[8] + 2 * z[10] + z[11] + -z[22];
z[0] = abb[23] * z[0];
z[8] = 4 * z[7];
z[10] = -5 * abb[11] + -abb[14];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = 4 * z[3] + -z[8] + z[10];
z[10] = abb[19] * z[10];
z[11] = -z[5] + -z[38];
z[11] = m1_set::bc<T>[0] * z[11];
z[11] = -z[9] + z[11] + z[15];
z[11] = abb[20] * z[11];
z[15] = -z[13] + -z[38];
z[15] = m1_set::bc<T>[0] * z[15];
z[15] = (T(3) / T(2)) * z[3] + -z[9] + z[15];
z[15] = abb[18] * z[15];
z[21] = abb[14] + z[13];
z[21] = m1_set::bc<T>[0] * z[21];
z[7] = -z[3] + z[7] + z[21];
z[7] = abb[25] * z[7];
z[6] = z[6] * z[32];
z[12] = abb[21] * m1_set::bc<T>[0] * z[12];
z[12] = z[6] + z[12];
z[18] = abb[17] * z[18];
z[7] = 4 * z[7] + z[10] + z[11] + -z[12] + z[15] + z[18];
z[7] = abb[8] * z[7];
z[5] = -z[5] + z[30];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = (T(7) / T(4)) * z[3] + z[5] + -z[9];
z[5] = abb[20] * z[5];
z[10] = -z[13] + -z[30];
z[10] = m1_set::bc<T>[0] * z[10];
z[9] = (T(5) / T(4)) * z[3] + -z[9] + z[10];
z[9] = abb[18] * z[9];
z[3] = 3 * z[3] + -z[8];
z[8] = z[3] + -5 * z[20];
z[8] = abb[19] * z[8];
z[10] = 8 * abb[11] + z[14];
z[10] = m1_set::bc<T>[0] * z[10];
z[3] = -z[3] + z[10];
z[3] = abb[25] * z[3];
z[10] = abb[17] * z[16];
z[3] = z[3] + z[5] + z[8] + z[9] + z[10] + -z[12];
z[3] = abb[7] * z[3];
z[5] = -z[19] * z[32];
z[5] = z[5] + -z[6];
z[5] = abb[9] * z[5];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[7] + z[17];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_331_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-11.827154332694712068781633699458444006742784028044442410670148784"),stof<T>("17.862547939024860428584230575418782634774820954401407503932057527")}, std::complex<T>{stof<T>("-29.18870339260922318507673990257276946749065402023784275791480152"),stof<T>("43.156890146480435302988897303164377166221354792865329943617609801")}, std::complex<T>{stof<T>("-17.361549059914511116295106203114325460747869992193400347244652736"),stof<T>("25.294342207455574874404666727745594531446533838463922439685552273")}, std::complex<T>{stof<T>("-3.7750428937635931319690940113069686134663500614979661092813990745"),stof<T>("6.147904513184628857106431949762537542394915181439860528724860466")}, std::complex<T>{stof<T>("2.895585631478921413457167452578190592329694022991161521201813387"),stof<T>("42.803067818362375040480766428177239881807258032513788786921151547")}, std::complex<T>{stof<T>("-0.602615376919195616233522304180790492957868935481005655067886603"),stof<T>("33.599798774146552605845413919878426962879417443352380429430877033")}, std::complex<T>{stof<T>("-9.2735157649391712965431096355795455219145893367523581218082854753"),stof<T>("-4.4863056395529831676652452887626277027336385478323168548790511213")}, std::complex<T>{stof<T>("40.513832073900002580984021936493675307889704204731741085758998743"),stof<T>("-61.600603686034321874308193152451989793406100337184911529792191198")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), rlog(k.W[196].real()/kbase.W[196].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_331_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_331_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("51.32229004228195186994131846883018191658493268321992025875967181"),stof<T>("-17.663143407427509107915991934863979635228730646227713881148991654")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dl[1], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_2_3(k), f_2_6_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), rlog(kend.W[196].real()/k.W[196].real()), f_2_6_re(k), f_2_24_re(k)};

                    
            return f_4_331_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_331_DLogXconstant_part(base_point<T>, kend);
	value += f_4_331_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_331_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_331_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_331_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_331_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_331_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_331_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
