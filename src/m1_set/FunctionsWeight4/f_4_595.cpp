/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_595.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_595_abbreviated (const std::array<T,29>& abb) {
T z[34];
z[0] = prod_pow(abb[12], 2);
z[1] = prod_pow(abb[9], 2);
z[0] = z[0] + -z[1];
z[1] = abb[0] + abb[4];
z[1] = -abb[5] + (T(1) / T(2)) * z[1];
z[1] = z[0] * z[1];
z[2] = -abb[0] + abb[1];
z[3] = prod_pow(m1_set::bc<T>[0], 2);
z[3] = (T(1) / T(12)) * z[3];
z[4] = z[2] * z[3];
z[5] = 2 * abb[1];
z[6] = abb[0] + z[5];
z[7] = 5 * abb[5];
z[8] = z[6] + -z[7];
z[9] = abb[3] + abb[4] + -3 * abb[8] + -z[8];
z[9] = abb[15] * z[9];
z[10] = 4 * abb[8];
z[11] = -abb[4] + z[10];
z[7] = -2 * abb[0] + abb[3] + z[7] + -z[11];
z[12] = abb[14] * z[7];
z[13] = prod_pow(abb[10], 2);
z[14] = abb[3] * (T(1) / T(2));
z[15] = abb[4] * (T(1) / T(2)) + z[14];
z[16] = abb[5] * (T(5) / T(2)) + z[15];
z[17] = -3 * abb[0] + z[16];
z[17] = z[13] * z[17];
z[18] = 2 * abb[3];
z[11] = z[11] + -z[18];
z[5] = abb[5] * (T(-11) / T(2)) + abb[0] * (T(3) / T(2)) + z[5] + z[11];
z[5] = abb[13] * z[5];
z[19] = -abb[4] + abb[8];
z[20] = -abb[0] + z[19];
z[21] = -abb[26] * z[20];
z[1] = z[1] + z[4] + z[5] + z[9] + z[12] + z[17] + z[21];
z[1] = abb[23] * z[1];
z[4] = (T(1) / T(2)) * z[0];
z[5] = -abb[4] + abb[5];
z[4] = z[4] * z[5];
z[9] = -abb[2] + abb[3];
z[12] = -z[3] * z[9];
z[14] = abb[2] + z[14];
z[17] = abb[0] * (T(1) / T(2));
z[21] = -abb[4] + -abb[5] + abb[8] * (T(3) / T(2)) + -z[14] + z[17];
z[21] = abb[15] * z[21];
z[22] = -abb[2] + z[19];
z[23] = -abb[0] + abb[5];
z[24] = z[22] + -z[23];
z[25] = abb[14] * z[24];
z[26] = abb[5] * (T(1) / T(2));
z[15] = z[15] + z[26];
z[27] = abb[2] * (T(1) / T(2));
z[28] = abb[0] + -z[15] + z[27];
z[28] = z[13] * z[28];
z[29] = abb[26] * z[22];
z[4] = z[4] + z[12] + z[21] + z[25] + z[28] + z[29];
z[4] = abb[19] * z[4];
z[12] = -abb[26] + abb[27];
z[21] = abb[7] + -abb[8];
z[25] = abb[0] + z[21];
z[12] = z[12] * z[25];
z[0] = z[0] + z[13];
z[28] = (T(1) / T(2)) * z[23];
z[0] = z[0] * z[28];
z[29] = -z[21] + z[23];
z[29] = abb[15] * z[29];
z[30] = abb[9] + -abb[12];
z[31] = -abb[10] + z[30];
z[31] = z[23] * z[31];
z[28] = abb[11] * z[28];
z[28] = z[28] + z[31];
z[28] = abb[11] * z[28];
z[31] = abb[14] * z[23];
z[32] = -abb[0] + 2 * abb[5];
z[33] = -abb[13] * z[32];
z[0] = z[0] + z[12] + z[28] + z[29] + z[31] + z[33];
z[0] = abb[22] * z[0];
z[12] = z[13] * z[23];
z[28] = abb[3] + -abb[8];
z[29] = z[28] + z[32];
z[31] = abb[14] * z[29];
z[12] = z[12] + z[31];
z[31] = abb[11] + z[30];
z[33] = abb[5] + z[28];
z[31] = z[31] * z[33];
z[29] = abb[10] * z[29];
z[29] = -z[29] + z[31];
z[31] = 2 * abb[11];
z[29] = z[29] * z[31];
z[6] = -4 * abb[5] + z[6] + -z[28];
z[6] = abb[15] * z[6];
z[8] = 2 * abb[8] + z[8] + -z[18];
z[8] = abb[13] * z[8];
z[6] = z[6] + -z[8] + -2 * z[12] + -z[29];
z[8] = abb[20] + abb[24];
z[6] = z[6] * z[8];
z[8] = -abb[0] + abb[8];
z[8] = -abb[5] + abb[7] + (T(-1) / T(2)) * z[8] + z[14];
z[8] = abb[15] * z[8];
z[9] = -z[2] + z[9];
z[3] = z[3] * z[9];
z[9] = abb[2] + z[23];
z[12] = (T(1) / T(2)) * z[13];
z[9] = z[9] * z[12];
z[12] = abb[2] + z[21];
z[13] = abb[26] * z[12];
z[14] = abb[3] + abb[8];
z[21] = -abb[2] + abb[5] + -z[14];
z[29] = abb[14] * z[21];
z[3] = -z[3] + -z[8] + z[9] + -z[13] + z[29];
z[8] = -abb[25] * z[3];
z[9] = z[17] + -z[26];
z[13] = -abb[1] + abb[8] + z[9] + z[18] + z[27];
z[17] = abb[25] * z[13];
z[15] = abb[8] + -z[15] + -z[27];
z[15] = abb[19] * z[15];
z[10] = abb[1] + -z[10] + z[16];
z[10] = abb[23] * z[10];
z[10] = z[10] + z[15] + z[17];
z[10] = abb[11] * z[10];
z[15] = abb[4] + z[28];
z[16] = -z[15] * z[30];
z[17] = -abb[10] * z[24];
z[16] = z[16] + z[17];
z[16] = abb[19] * z[16];
z[11] = 3 * abb[5] + -z[11];
z[11] = z[11] * z[30];
z[7] = -abb[10] * z[7];
z[7] = z[7] + z[11];
z[7] = abb[23] * z[7];
z[11] = -z[14] + z[32];
z[14] = z[11] * z[30];
z[17] = abb[10] * z[21];
z[14] = z[14] + -z[17];
z[17] = -abb[25] * z[14];
z[7] = z[7] + z[10] + z[16] + z[17];
z[7] = abb[11] * z[7];
z[10] = -abb[11] * z[13];
z[10] = z[10] + z[14];
z[10] = abb[11] * z[10];
z[13] = -abb[13] * z[11];
z[2] = -abb[7] + z[2] + -z[28];
z[14] = -abb[27] * z[2];
z[3] = z[3] + z[10] + z[13] + z[14];
z[3] = abb[21] * z[3];
z[10] = abb[25] * z[11];
z[9] = -z[9] + z[15];
z[9] = abb[19] * z[9];
z[9] = z[9] + z[10];
z[9] = abb[13] * z[9];
z[10] = abb[25] * z[2];
z[11] = -abb[1] + z[19];
z[11] = abb[23] * z[11];
z[13] = abb[19] * z[15];
z[10] = z[10] + z[11] + z[13];
z[11] = abb[27] * z[10];
z[0] = abb[28] + z[0] + z[1] + z[3] + z[4] + z[6] + z[7] + z[8] + z[9] + z[11];
z[1] = abb[22] * z[25];
z[3] = abb[25] * z[12];
z[4] = abb[19] * z[22];
z[6] = -abb[23] * z[20];
z[3] = -z[1] + z[3] + z[4] + z[6];
z[3] = abb[16] * z[3];
z[4] = -abb[19] * z[5];
z[5] = -abb[4] + z[32];
z[5] = abb[23] * z[5];
z[6] = -abb[22] * z[23];
z[4] = z[4] + z[5] + z[6];
z[4] = -m1_set::bc<T>[0] * z[4] * z[30];
z[1] = z[1] + z[10];
z[1] = abb[17] * z[1];
z[2] = -abb[17] * z[2];
z[5] = -abb[16] * z[12];
z[2] = z[2] + z[5];
z[2] = abb[21] * z[2];
z[1] = abb[18] + z[1] + z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_595_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("1.14016149693474912995296418286682575963298866662145464232264644222"),stof<T>("-0.55672287705019939752797847344117014904747887801658190442675284488")}, std::complex<T>{stof<T>("0.1040174213071643484279363078505384332898067920506590908236125443"),stof<T>("-2.3780749888865589803956656259166614109724664030794131420083200126")}, std::complex<T>{stof<T>("1.0361440756275847815250278750162873263431818745707955514990338979"),stof<T>("1.8213521118363595828676871524754912619249875250628312375815671677")}, std::complex<T>{stof<T>("-1.14016149693474912995296418286682575963298866662145464232264644222"),stof<T>("0.55672287705019939752797847344117014904747887801658190442675284488")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[30].real()/kbase.W[30].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_595_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_595_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[2] + v[3]) * (-24 + 7 * v[2] + 7 * v[3] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -12 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (v[2] + v[3]) * (-2 * (8 * v[0] + 4 * v[1] + 7 * v[2] + 3 * v[3] + -4 * v[4]) + m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(32)) * (v[2] + v[3]) * (-2 * (8 * v[0] + 4 * v[1] + 5 * v[2] + v[3] + -4 * v[4]) + 8 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[3] = ((-1 + 2 * m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[2] + v[3])) / tend;
c[4] = (m1_set::bc<T>[2] * (T(3) / T(2)) * (v[2] + v[3])) / tend;
c[5] = ((1 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(1) / T(2)) * (v[2] + v[3])) / tend;


		return abb[21] * c[3] + -abb[25] * c[3] + abb[20] * c[4] + abb[24] * c[4] + abb[23] * c[5] + abb[22] * c[4] * (T(-1) / T(2)) + t * (abb[21] * c[0] + (abb[23] + -abb[25]) * c[0] + abb[20] * c[1] + (abb[19] + -abb[23]) * c[2] + (abb[22] + abb[23] + -2 * abb[24]) * c[1] * (T(-1) / T(2))) + abb[19] * (c[3] + -c[5] + c[4] * (T(-1) / T(2)));
	}
	{
T z[6];
z[0] = -abb[21] + abb[25];
z[1] = abb[23] + z[0];
z[2] = abb[11] * z[1];
z[3] = -5 * abb[23] + z[0];
z[3] = abb[10] * z[3];
z[2] = z[2] + (T(1) / T(2)) * z[3];
z[2] = abb[10] * z[2];
z[1] = -abb[14] * z[1];
z[3] = abb[20] + -abb[23] + abb[24];
z[3] = -abb[22] + 2 * z[3];
z[4] = abb[15] * z[3];
z[0] = abb[23] + -z[0];
z[0] = prod_pow(abb[11], 2) * z[0];
z[5] = abb[10] + -abb[11];
z[5] = abb[10] * z[5];
z[5] = abb[14] + z[5];
z[5] = abb[15] + 2 * z[5];
z[5] = abb[19] * z[5];
z[3] = -abb[19] + -z[3];
z[3] = abb[13] * z[3];
z[0] = (T(3) / T(2)) * z[0] + z[1] + z[2] + z[3] + z[4] + z[5];
return abb[6] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_595_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_595_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W19(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), f_1_1(k), f_1_4(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[30].real()/k.W[30].real()), f_2_4_re(k), f_2_11_re(k), T{0}};
abb[18] = SpDLog_f_4_595_W_19_Im(t, path, abb);
abb[28] = SpDLog_f_4_595_W_19_Re(t, path, abb);

                    
            return f_4_595_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_595_DLogXconstant_part(base_point<T>, kend);
	value += f_4_595_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_595_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_595_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_595_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_595_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_595_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_595_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
