/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_146.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_146_abbreviated (const std::array<T,29>& abb) {
T z[39];
z[0] = 2 * abb[19] + abb[21];
z[1] = -abb[24] + z[0];
z[2] = abb[23] * (T(1) / T(2));
z[3] = abb[20] * (T(1) / T(2));
z[4] = z[1] + z[2] + -z[3];
z[4] = abb[5] * z[4];
z[5] = 2 * abb[22];
z[6] = abb[20] + z[5];
z[7] = -abb[18] + z[6];
z[8] = -z[0] + z[7];
z[8] = abb[0] * z[8];
z[9] = 2 * z[0];
z[10] = abb[20] + abb[22];
z[11] = -abb[24] + z[9] + -z[10];
z[12] = abb[6] * z[11];
z[6] = -z[6] + -z[9];
z[13] = 13 * abb[18];
z[6] = 4 * z[6] + z[13];
z[14] = 3 * abb[23];
z[6] = abb[24] + (T(1) / T(3)) * z[6] + z[14];
z[6] = abb[2] * z[6];
z[15] = -abb[22] + z[3] + -z[9];
z[2] = abb[24] + -z[2] + (T(1) / T(3)) * z[15];
z[2] = abb[1] * z[2];
z[13] = -8 * abb[20] + 5 * abb[22] + -z[0] + -z[13];
z[15] = 3 * abb[24];
z[13] = (T(1) / T(3)) * z[13] + z[15];
z[13] = abb[3] * z[13];
z[5] = -2 * abb[20] + 5 * z[0] + -z[5];
z[5] = -abb[24] + (T(1) / T(3)) * z[5];
z[5] = abb[4] * z[5];
z[2] = z[2] + 5 * z[4] + 4 * z[5] + z[6] + (T(13) / T(3)) * z[8] + -z[12] + z[13];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[4] = abb[20] + z[9];
z[5] = 2 * z[4] + -z[14] + -z[15];
z[6] = -abb[22] + z[5];
z[6] = abb[2] * z[6];
z[13] = 9 * abb[24];
z[16] = 4 * z[0];
z[17] = abb[20] + -z[16];
z[14] = 7 * abb[22] + z[13] + z[14] + 4 * z[17];
z[17] = 2 * abb[1];
z[14] = z[14] * z[17];
z[13] = 10 * z[0] + -z[10] + -z[13];
z[13] = abb[4] * z[13];
z[18] = 2 * abb[24] + -z[9];
z[19] = -abb[20] + abb[23];
z[20] = z[18] + -z[19];
z[20] = abb[5] * z[20];
z[21] = 3 * z[20];
z[22] = 3 * abb[6];
z[22] = z[11] * z[22];
z[23] = -z[15] + z[16];
z[24] = -z[10] + z[23];
z[25] = 4 * abb[3];
z[26] = -z[24] * z[25];
z[6] = z[6] + z[13] + z[14] + -z[21] + z[22] + z[26];
z[6] = abb[14] * z[6];
z[13] = 4 * abb[1];
z[13] = z[13] * z[24];
z[13] = z[13] + -z[22];
z[14] = 2 * abb[3];
z[26] = z[14] * z[24];
z[27] = z[9] + -z[15];
z[10] = z[10] + z[27];
z[28] = abb[2] * z[10];
z[10] = abb[4] * z[10];
z[26] = -z[10] + z[13] + z[26] + -z[28];
z[28] = 2 * z[26];
z[29] = -abb[13] * z[28];
z[6] = z[6] + z[29];
z[6] = abb[14] * z[6];
z[29] = 3 * abb[22];
z[30] = abb[24] * (T(3) / T(2));
z[31] = abb[18] * (T(1) / T(2));
z[16] = abb[20] * (T(5) / T(2)) + -z[16] + z[29] + z[30] + -z[31];
z[16] = abb[3] * z[16];
z[17] = z[17] * z[24];
z[21] = -z[17] + z[21];
z[3] = abb[22] + z[3] + -z[31];
z[9] = z[3] + -z[9] + z[30];
z[9] = abb[0] * z[9];
z[24] = abb[25] + -abb[26];
z[30] = abb[7] * z[24];
z[9] = z[9] + (T(3) / T(2)) * z[30];
z[32] = -abb[18] + z[5];
z[32] = abb[2] * z[32];
z[16] = z[9] + z[16] + z[21] + z[22] + z[32];
z[16] = abb[12] * z[16];
z[32] = abb[14] * z[28];
z[33] = 8 * z[0];
z[31] = abb[24] * (T(-9) / T(2)) + abb[20] * (T(-7) / T(2)) + -z[29] + -z[31] + z[33];
z[31] = abb[3] * z[31];
z[4] = z[4] + -z[15];
z[15] = -abb[18] + -z[4];
z[15] = abb[2] * z[15];
z[15] = z[9] + z[13] + z[15] + z[31];
z[15] = abb[13] * z[15];
z[15] = z[15] + z[16] + z[32];
z[15] = abb[12] * z[15];
z[16] = -abb[20] + z[0];
z[16] = 2 * z[16];
z[29] = abb[18] + z[16] + -z[29];
z[29] = abb[2] * z[29];
z[31] = abb[4] * z[11];
z[34] = abb[18] + abb[20];
z[35] = -z[23] + z[34];
z[36] = abb[3] * z[35];
z[37] = 3 * z[24];
z[37] = abb[8] * z[37];
z[38] = z[17] + z[37];
z[30] = 3 * z[30];
z[8] = 4 * z[8] + -z[22] + z[29] + z[30] + 3 * z[31] + -z[36] + z[38];
z[8] = abb[11] * z[8];
z[27] = z[7] + z[27];
z[27] = abb[2] * z[27];
z[29] = abb[24] * (T(-15) / T(2)) + -z[3] + z[33];
z[29] = abb[3] * z[29];
z[9] = z[9] + 2 * z[10] + -z[13] + z[27] + -z[29];
z[13] = abb[12] + -abb[13];
z[9] = z[9] * z[13];
z[9] = -z[8] + z[9] + z[32];
z[9] = abb[11] * z[9];
z[13] = z[18] + z[19];
z[13] = abb[2] * z[13];
z[18] = z[11] * z[14];
z[12] = 2 * z[12] + -z[13] + -z[17] + -z[18] + z[20];
z[12] = 3 * z[12];
z[13] = -abb[28] * z[12];
z[7] = -z[7] + z[23];
z[18] = abb[0] * z[7];
z[19] = -abb[2] * z[35];
z[0] = -z[0] + z[34];
z[0] = z[0] * z[25];
z[0] = z[0] + -z[17] + z[18] + z[19] + z[37];
z[0] = prod_pow(abb[13], 2) * z[0];
z[17] = abb[27] * z[28];
z[19] = abb[0] + abb[3];
z[19] = abb[2] + -2 * abb[10] + (T(3) / T(2)) * z[19];
z[19] = z[19] * z[24];
z[3] = abb[24] * (T(-1) / T(2)) + z[3];
z[3] = abb[7] * z[3];
z[11] = -abb[9] * z[11];
z[1] = abb[8] * z[1];
z[1] = 2 * z[1] + z[3] + z[11] + z[19];
z[1] = abb[15] * z[1];
z[0] = z[0] + 3 * z[1] + z[2] + z[6] + z[9] + z[13] + z[15] + z[17];
z[1] = abb[14] * z[26];
z[2] = -z[10] + z[18] + -z[22];
z[3] = 2 * abb[18] + -abb[22];
z[5] = z[3] + -z[5];
z[5] = abb[2] * z[5];
z[6] = abb[3] * z[7];
z[5] = z[2] + z[5] + z[6] + -z[21] + -z[30];
z[5] = abb[12] * z[5];
z[3] = -z[3] + -z[4];
z[3] = z[3] * z[14];
z[4] = abb[18] + abb[22] + -z[16];
z[4] = abb[2] * z[4];
z[2] = -z[2] + z[3] + z[4] + -z[38];
z[2] = abb[13] * z[2];
z[1] = -z[1] + z[2] + z[5] + z[8];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[16] * z[26];
z[1] = z[1] + z[2];
z[2] = -abb[17] * z[12];
z[1] = 2 * z[1] + z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_146_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-18.959443965275373902985288861958725493858885815901869383366351011"),stof<T>("15.58728128082497075093557636648036379123427287038040098584118758")}, std::complex<T>{stof<T>("7.29066661917076727356158130168775612123402945215260583749819591"),stof<T>("207.17350519872560019844394479365795599609582026535228560125707756")}, std::complex<T>{stof<T>("-60.131996987868912823259793315265656989122730483489743877265890411"),stof<T>("-61.050357396397359974557623481929038171749421762292890907765600958")}, std::complex<T>{stof<T>("3.645333309585383636780790650843878060617014726076302918749097957"),stof<T>("103.586752599362800099221972396828977998047910132676142800628538781")}, std::complex<T>{stof<T>("14.470992220823802754968917857785885663283930959861970428652489076"),stof<T>("-102.770320302245361239538219678772280499614144373765980554258950138")}, std::complex<T>{stof<T>("55.643545243417341675243422311092817158547775627449844922552028476"),stof<T>("-26.132681625023030514045019830362878536630449741092688660652161599")}, std::complex<T>{stof<T>("0.843118434866187511235580353328961769957940129963596035964763978"),stof<T>("-16.403713577942409610619329084537061289668038629290563232210776224")}, std::complex<T>{stof<T>("-0.466587119561924118219320910239394343139074717472046424179137927"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}, std::complex<T>{stof<T>("0.466587119561924118219320910239394343139074717472046424179137927"),stof<T>("-47.246795076652142140372750836458559900495186049435789623190676203")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_146_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_146_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("107.843456416863119401093077894541821529440754104644881943746560112"),stof<T>("-49.7680708994789113061447116959432323102822341359427172762299492")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_146_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_146_DLogXconstant_part(base_point<T>, kend);
	value += f_4_146_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_146_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_146_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_146_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_146_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_146_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_146_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
