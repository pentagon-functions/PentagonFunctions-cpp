/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_21.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_21_abbreviated (const std::array<T,14>& abb) {
T z[14];
z[0] = abb[1] + -abb[2];
z[1] = abb[10] + abb[12];
z[2] = abb[11] + (T(1) / T(2)) * z[1];
z[3] = z[0] * z[2];
z[4] = -abb[3] + -abb[4];
z[5] = abb[13] * (T(3) / T(2));
z[4] = z[4] * z[5];
z[6] = abb[11] + -abb[12];
z[6] = abb[0] * z[6];
z[3] = z[3] + z[4] + 2 * z[6];
z[3] = prod_pow(abb[6], 2) * z[3];
z[4] = -abb[0] + abb[1];
z[7] = z[2] * z[4];
z[8] = -abb[10] + abb[11];
z[8] = abb[2] * z[8];
z[9] = abb[4] * z[5];
z[7] = z[7] + 2 * z[8] + z[9];
z[7] = abb[8] * z[7];
z[9] = abb[0] + abb[2];
z[10] = z[2] * z[9];
z[11] = abb[3] * z[5];
z[10] = z[10] + z[11];
z[11] = abb[1] * z[2];
z[10] = (T(1) / T(2)) * z[10] + z[11];
z[12] = abb[6] * z[10];
z[7] = z[7] + -z[12];
z[7] = abb[8] * z[7];
z[13] = abb[7] + abb[8];
z[10] = z[10] * z[13];
z[10] = z[10] + z[12];
z[10] = abb[7] * z[10];
z[5] = z[5] * z[9];
z[2] = abb[3] * z[2];
z[2] = z[2] + z[5];
z[5] = -abb[5] + abb[1] * (T(1) / T(2));
z[5] = abb[13] * z[5];
z[2] = (T(1) / T(2)) * z[2] + z[5];
z[2] = abb[9] * z[2];
z[5] = -z[6] + -z[8];
z[5] = (T(1) / T(2)) * z[5] + -z[11];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[2] = 3 * z[2] + z[3] + (T(13) / T(3)) * z[5] + z[7] + z[10];
z[1] = 2 * abb[11] + z[1];
z[0] = -z[0] * z[1];
z[3] = abb[3] * abb[13];
z[3] = 3 * z[3];
z[5] = abb[4] * abb[13];
z[5] = 3 * z[5];
z[0] = z[0] + z[3] + z[5] + -4 * z[6];
z[0] = abb[6] * z[0];
z[4] = -z[1] * z[4];
z[4] = z[4] + -z[5] + -4 * z[8];
z[4] = abb[8] * z[4];
z[5] = -2 * abb[1] + -z[9];
z[1] = z[1] * z[5];
z[1] = z[1] + -z[3];
z[1] = abb[7] * z[1];
z[0] = z[0] + z[1] + z[4];
z[0] = m1_set::bc<T>[0] * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_21_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.890692132822189806957914625707096031009369800433595394575290467"),stof<T>("18.044923259837364770762474372677647439570604036380722585533187963")}, std::complex<T>{stof<T>("-52.672961428301767057605314695823353708824803411097620833491517139"),stof<T>("68.069008743710168898593463565169894431713375364017248818035369614")}, std::complex<T>{stof<T>("-20.808716632072529557502826949675730271378380393531556083494895506"),stof<T>("-5.746774532483955638521704095274591823155660828628573636748856479")}, std::complex<T>{stof<T>("-0.233293559780962059109660455119697171569537358736023212089568963"),stof<T>("23.623397538326071070186375418229279950247593024717894811595338102")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[117].real()/kbase.W[117].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_21_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_21_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("64.610977201373750386875858208707023697306686082714940497224559317"),stof<T>("66.303880913063390592875221780646070270477029572486007604665088608")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,14> abb = {dl[0], dl[2], dl[4], dlog_W118(k,dv), dlog_W123(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[117].real()/k.W[117].real())};

                    
            return f_4_21_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_21_DLogXconstant_part(base_point<T>, kend);
	value += f_4_21_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_21_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_21_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_21_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_21_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_21_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_21_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
