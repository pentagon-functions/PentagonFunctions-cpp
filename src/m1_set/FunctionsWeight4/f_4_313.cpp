/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_313.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_313_abbreviated (const std::array<T,54>& abb) {
T z[93];
z[0] = abb[43] + abb[49];
z[1] = abb[44] + abb[46];
z[2] = abb[48] * (T(1) / T(2));
z[3] = abb[41] + -abb[47];
z[4] = (T(1) / T(6)) * z[0] + (T(1) / T(3)) * z[1] + -z[2] + (T(1) / T(2)) * z[3];
z[4] = abb[1] * z[4];
z[5] = abb[43] * (T(23) / T(12)) + (T(4) / T(3)) * z[1];
z[6] = abb[49] * (T(1) / T(4));
z[7] = abb[47] + abb[41] * (T(-1) / T(3));
z[7] = abb[45] * (T(-13) / T(4)) + abb[42] * (T(1) / T(3)) + -z[5] + z[6] + (T(13) / T(2)) * z[7];
z[7] = abb[8] * z[7];
z[8] = abb[45] * (T(1) / T(4));
z[5] = abb[49] * (T(7) / T(12)) + z[2] + -z[5] + z[8];
z[5] = abb[4] * z[5];
z[9] = abb[41] * (T(13) / T(2)) + -5 * z[1];
z[8] = abb[43] * (T(-43) / T(12)) + abb[42] * (T(13) / T(6)) + -z[6] + -z[8] + (T(1) / T(3)) * z[9];
z[8] = abb[3] * z[8];
z[9] = -abb[45] + abb[49];
z[10] = -abb[48] + z[1];
z[11] = z[9] + z[10];
z[12] = -abb[12] * z[11];
z[13] = 3 * z[12];
z[14] = abb[49] * (T(1) / T(2));
z[15] = -abb[42] + z[14];
z[16] = abb[43] + -abb[45];
z[17] = (T(1) / T(2)) * z[16];
z[18] = z[15] + z[17];
z[19] = abb[15] * z[18];
z[20] = abb[50] * (T(1) / T(4));
z[21] = abb[20] + abb[22];
z[22] = abb[23] + z[21];
z[23] = abb[21] + z[22];
z[24] = z[20] * z[23];
z[25] = -abb[42] + abb[49];
z[26] = -abb[48] + z[25];
z[26] = abb[9] * z[26];
z[27] = 3 * z[26];
z[28] = abb[42] + -abb[43];
z[28] = abb[11] * z[28];
z[29] = 3 * z[28];
z[30] = abb[25] * z[20];
z[9] = -abb[43] + z[9];
z[31] = abb[6] * z[9];
z[25] = -abb[41] + z[25];
z[32] = abb[0] * z[25];
z[33] = -abb[43] + z[1];
z[34] = -abb[49] + z[33];
z[34] = abb[42] + (T(1) / T(3)) * z[34];
z[34] = abb[2] * z[34];
z[4] = 13 * z[4] + z[5] + z[7] + z[8] + z[13] + (T(-1) / T(2)) * z[19] + z[24] + z[27] + -z[29] + -z[30] + (T(1) / T(4)) * z[31] + (T(-13) / T(6)) * z[32] + z[34];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[5] = (T(1) / T(2)) * z[1];
z[7] = abb[42] * (T(1) / T(2));
z[8] = abb[43] * (T(1) / T(2));
z[24] = -abb[41] + z[5] + z[7] + -z[8] + z[14];
z[24] = abb[8] * z[24];
z[34] = abb[45] * (T(1) / T(2));
z[35] = z[8] + z[34];
z[36] = z[3] + -z[14] + z[35];
z[37] = abb[13] * z[36];
z[38] = (T(3) / T(2)) * z[37];
z[39] = 3 * abb[48];
z[33] = -z[33] + z[39];
z[40] = -abb[49] + (T(1) / T(2)) * z[33];
z[40] = abb[14] * z[40];
z[41] = abb[50] * (T(3) / T(4));
z[42] = abb[25] * z[41];
z[42] = (T(3) / T(2)) * z[19] + z[42];
z[43] = -abb[49] + z[1];
z[44] = 3 * abb[42] + -abb[43] + z[43];
z[45] = abb[2] * z[44];
z[46] = (T(1) / T(2)) * z[45];
z[47] = z[42] + z[46];
z[17] = abb[48] + -z[14] + z[17];
z[48] = abb[7] * z[17];
z[49] = z[8] + z[14];
z[50] = z[1] + z[49];
z[51] = abb[45] * (T(3) / T(2));
z[52] = -z[50] + z[51];
z[53] = abb[4] * z[52];
z[54] = -abb[48] + z[3];
z[50] = -z[50] + (T(-3) / T(2)) * z[54];
z[50] = abb[1] * z[50];
z[54] = 3 * abb[47];
z[55] = -abb[41] + z[54];
z[56] = -z[1] + z[55];
z[15] = -abb[43] + -z[15] + (T(1) / T(2)) * z[56];
z[15] = abb[3] * z[15];
z[56] = 2 * z[32];
z[57] = -abb[23] * z[41];
z[15] = z[15] + z[24] + -z[27] + z[38] + -z[40] + z[47] + (T(-3) / T(2)) * z[48] + z[50] + (T(-1) / T(2)) * z[53] + z[56] + z[57];
z[15] = prod_pow(abb[27], 2) * z[15];
z[24] = abb[8] * z[52];
z[27] = z[24] + z[53];
z[50] = abb[5] * z[52];
z[57] = -z[27] + z[50];
z[16] = z[1] + z[16];
z[58] = z[3] + z[16];
z[58] = abb[10] * z[58];
z[59] = 2 * abb[43];
z[43] = z[43] + z[59];
z[60] = 3 * z[3];
z[61] = z[43] + z[60];
z[62] = abb[1] * z[61];
z[63] = abb[50] * (T(1) / T(2));
z[64] = -z[22] * z[63];
z[65] = abb[43] * (T(3) / T(2));
z[66] = -z[14] + z[65];
z[67] = z[1] + z[66];
z[68] = -z[34] + z[67];
z[69] = 2 * z[3];
z[70] = -z[68] + -z[69];
z[70] = abb[3] * z[70];
z[64] = z[37] + -z[57] + z[58] + -z[62] + z[64] + z[70];
z[64] = abb[34] * z[64];
z[70] = -abb[3] * z[36];
z[71] = abb[14] * z[17];
z[72] = -abb[43] + -abb[48] + abb[49] + -z[3];
z[72] = abb[1] * z[72];
z[73] = abb[24] * z[63];
z[74] = -abb[23] * z[63];
z[70] = z[37] + -z[48] + z[70] + z[71] + z[72] + -z[73] + z[74];
z[70] = abb[33] * z[70];
z[71] = z[23] * z[63];
z[68] = abb[3] * z[68];
z[74] = (T(1) / T(2)) * z[9];
z[75] = abb[4] * z[74];
z[76] = (T(1) / T(2)) * z[31];
z[68] = z[68] + z[71] + -z[75] + z[76];
z[71] = abb[8] * z[18];
z[71] = -z[19] + -z[45] + z[71];
z[75] = z[68] + z[71];
z[75] = abb[52] * z[75];
z[77] = z[12] + z[48];
z[78] = z[21] * z[63];
z[78] = z[73] + -z[77] + z[78];
z[79] = -z[57] + z[78];
z[79] = abb[53] * z[79];
z[80] = abb[49] * (T(3) / T(2));
z[81] = z[1] + z[80];
z[35] = -2 * abb[48] + -z[35] + z[81];
z[82] = abb[14] * z[35];
z[78] = z[78] + -z[82];
z[82] = abb[25] * z[63];
z[71] = z[71] + -z[82];
z[82] = z[71] + -z[78];
z[83] = abb[51] * z[82];
z[84] = abb[16] + -abb[18];
z[36] = z[36] * z[84];
z[84] = -abb[42] + -abb[47] + z[34] + z[49];
z[84] = abb[17] * z[84];
z[85] = abb[26] * z[63];
z[36] = z[36] + z[84] + z[85];
z[36] = (T(1) / T(2)) * z[36];
z[36] = abb[37] * z[36];
z[84] = abb[21] + abb[23];
z[63] = z[63] * z[84];
z[63] = z[48] + z[63] + z[76];
z[74] = abb[3] * z[74];
z[74] = z[63] + z[73] + -z[74];
z[76] = abb[4] * z[17];
z[76] = z[74] + z[76];
z[76] = abb[35] * z[76];
z[35] = -abb[53] * z[35];
z[84] = -abb[35] * z[17];
z[35] = z[35] + z[84];
z[35] = abb[14] * z[35];
z[35] = z[35] + z[36] + z[64] + z[70] + z[75] + z[76] + z[79] + z[83];
z[36] = 2 * abb[41];
z[64] = z[7] + z[36];
z[70] = abb[45] * (T(3) / T(4));
z[75] = z[64] + z[70];
z[76] = abb[43] * (T(5) / T(4)) + z[1];
z[79] = abb[49] * (T(3) / T(4));
z[83] = -z[54] + z[75] + z[76] + -z[79];
z[83] = abb[8] * z[83];
z[84] = z[70] + z[79];
z[5] = -2 * abb[42] + abb[43] * (T(7) / T(4)) + z[5] + -z[36] + z[84];
z[5] = abb[3] * z[5];
z[70] = -z[70] + z[76];
z[76] = -z[6] + z[70];
z[76] = abb[4] * z[76];
z[85] = -z[23] * z[41];
z[5] = z[5] + z[29] + (T(-3) / T(4)) * z[31] + -z[32] + z[47] + -z[62] + z[76] + z[83] + z[85];
z[5] = abb[29] * z[5];
z[29] = -z[51] + z[55] + -z[65];
z[6] = abb[42] + -z[6] + (T(1) / T(2)) * z[29];
z[6] = abb[8] * z[6];
z[29] = z[22] * z[41];
z[29] = z[29] + -z[38];
z[41] = abb[47] * (T(3) / T(2));
z[47] = abb[49] * (T(-5) / T(4)) + abb[43] * (T(3) / T(4)) + -z[41] + z[75];
z[47] = abb[3] * z[47];
z[65] = (T(1) / T(2)) * z[32];
z[42] = -z[42] + z[65];
z[75] = abb[14] * z[52];
z[76] = abb[50] * (T(3) / T(2));
z[83] = abb[24] * z[76];
z[75] = z[75] + z[83];
z[6] = z[6] + z[29] + -z[42] + z[45] + z[47] + z[62] + z[75];
z[6] = abb[27] * z[6];
z[47] = abb[25] * z[76];
z[85] = 3 * z[19] + z[47];
z[86] = z[75] + z[85];
z[52] = abb[3] * z[52];
z[44] = abb[8] * z[44];
z[87] = z[44] + 3 * z[45] + z[52] + -z[53] + z[86];
z[88] = abb[28] * z[87];
z[6] = z[6] + -z[88];
z[5] = z[5] + z[6];
z[5] = abb[29] * z[5];
z[89] = 2 * z[1];
z[90] = 4 * abb[41];
z[7] = abb[47] * (T(-9) / T(2)) + abb[43] * (T(13) / T(4)) + -z[7] + -z[84] + z[89] + z[90];
z[7] = abb[3] * z[7];
z[84] = -abb[42] + (T(-1) / T(2)) * z[55] + z[70] + z[79];
z[84] = abb[8] * z[84];
z[91] = -z[45] + 2 * z[62];
z[92] = 3 * z[58];
z[7] = z[7] + z[29] + z[42] + -z[53] + z[84] + z[91] + -z[92];
z[7] = abb[29] * z[7];
z[29] = -abb[42] + z[80];
z[42] = abb[45] * (T(9) / T(2)) + -z[1] + -z[8] + -z[29] + -z[55];
z[42] = abb[8] * z[42];
z[22] = z[22] * z[76];
z[42] = -z[22] + z[42] + z[53];
z[41] = z[41] + -z[64] + -z[70] + z[79];
z[41] = abb[3] * z[41];
z[38] = z[38] + z[41] + (T(1) / T(2)) * z[42] + z[46] + (T(-3) / T(2)) * z[50] + -z[62] + z[65];
z[38] = abb[30] * z[38];
z[6] = -z[6] + z[7] + z[38];
z[6] = abb[30] * z[6];
z[7] = z[21] * z[76];
z[38] = -z[7] + 3 * z[77];
z[33] = 2 * abb[49] + -z[33];
z[33] = 2 * z[33];
z[41] = abb[14] * z[33];
z[41] = -z[27] + z[38] + z[41] + z[52];
z[42] = abb[27] * z[41];
z[46] = 2 * abb[3];
z[64] = abb[4] + z[46];
z[43] = z[43] * z[64];
z[23] = z[23] * z[76];
z[23] = z[23] + (T(3) / T(2)) * z[31];
z[31] = z[23] + -z[24] + z[43] + z[75];
z[43] = abb[29] * z[31];
z[64] = 3 * abb[45];
z[0] = z[0] + -z[64] + z[89];
z[65] = abb[4] + abb[8];
z[0] = z[0] * z[65];
z[65] = 3 * z[50];
z[0] = z[0] + -z[52] + z[65] + -z[75];
z[0] = abb[30] * z[0];
z[34] = z[10] + -z[34] + z[49];
z[34] = abb[4] * z[34];
z[24] = z[24] + -z[34] + -z[48];
z[20] = -z[20] * z[21];
z[9] = abb[3] * z[9];
z[9] = (T(-1) / T(4)) * z[9] + z[12] + z[20] + (T(-1) / T(2)) * z[24] + -z[40];
z[20] = 3 * abb[31];
z[9] = z[9] * z[20];
z[24] = -abb[4] + abb[14];
z[17] = z[17] * z[24];
z[17] = z[17] + -z[74];
z[24] = 3 * abb[28];
z[17] = z[17] * z[24];
z[0] = z[0] + z[9] + z[17] + z[42] + z[43];
z[0] = abb[31] * z[0];
z[9] = -z[22] + 3 * z[37];
z[17] = -z[51] + z[89];
z[8] = abb[49] * (T(-5) / T(2)) + z[8] + -z[17] + z[39];
z[8] = abb[14] * z[8];
z[22] = abb[3] * z[61];
z[8] = z[8] + -z[9] + -z[13] + z[22] + z[27] + -3 * z[72] + z[83];
z[8] = abb[27] * z[8];
z[13] = abb[23] * (T(-1) / T(2)) + -z[21];
z[13] = abb[50] * z[13];
z[13] = z[13] + z[37] + z[48] + z[50];
z[2] = z[2] + (T(-5) / T(2)) * z[3] + -z[67];
z[2] = abb[1] * z[2];
z[3] = abb[14] * z[11];
z[3] = (T(1) / T(2)) * z[3];
z[21] = -z[1] + -z[60];
z[21] = -abb[43] + z[14] + (T(1) / T(2)) * z[21];
z[21] = abb[3] * z[21];
z[2] = z[2] + z[3] + (T(1) / T(2)) * z[13] + z[21] + z[58] + -z[73];
z[2] = abb[32] * z[2];
z[13] = z[46] * z[61];
z[13] = z[13] + -z[27] + z[75];
z[9] = z[9] + -z[13] + -3 * z[62] + z[92];
z[21] = abb[29] + -abb[30];
z[9] = z[9] * z[21];
z[21] = -z[57] + z[78];
z[20] = z[20] * z[21];
z[2] = 3 * z[2] + z[8] + z[9] + z[20];
z[2] = abb[32] * z[2];
z[8] = abb[4] * z[11];
z[9] = -abb[3] * z[16];
z[11] = abb[24] * abb[50];
z[8] = z[8] + z[9] + z[11] + z[19] + z[44] + z[63];
z[3] = -z[3] + (T(1) / T(2)) * z[8] + z[26] + -z[28] + z[30] + (T(3) / T(2)) * z[45];
z[3] = z[3] * z[24];
z[8] = -abb[27] * z[87];
z[3] = z[3] + z[8];
z[3] = abb[28] * z[3];
z[8] = abb[36] * z[41];
z[9] = abb[19] * abb[37] * z[18];
z[11] = -abb[52] * z[47];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[8] + (T(3) / T(2)) * z[9] + z[11] + z[15] + 3 * z[35];
z[2] = -abb[42] + z[36] + -z[51] + z[66];
z[2] = abb[8] * z[2];
z[3] = abb[1] * z[33];
z[4] = z[25] * z[46];
z[2] = z[2] + z[3] + z[4] + 6 * z[26] + -4 * z[32] + z[38] + -z[45] + -z[86];
z[2] = abb[27] * z[2];
z[3] = z[36] + -z[54];
z[4] = -z[1] + -z[3];
z[5] = abb[43] * (T(5) / T(2));
z[4] = 2 * z[4] + -z[5] + z[29] + -z[51];
z[4] = abb[8] * z[4];
z[6] = z[56] + -z[85] + z[91];
z[5] = -z[5] + z[14] + -z[17];
z[5] = abb[4] * z[5];
z[8] = 4 * abb[42] + abb[43] * (T(-7) / T(2)) + -z[51] + -z[81] + z[90];
z[8] = abb[3] * z[8];
z[4] = z[4] + z[5] + z[6] + z[8] + z[23] + -6 * z[28];
z[4] = abb[29] * z[4];
z[5] = 6 * z[58] + z[65];
z[1] = -abb[42] + z[1] + z[59];
z[8] = -z[1] + 2 * z[55] + -z[64];
z[8] = abb[8] * z[8];
z[1] = -z[1] + -z[3];
z[1] = z[1] * z[46];
z[1] = z[1] + z[5] + -z[6] + z[8] + z[53];
z[1] = abb[30] * z[1];
z[3] = z[12] + -z[48];
z[6] = abb[43] + z[10] + z[69];
z[6] = abb[1] * z[6];
z[3] = 3 * z[3] + -z[5] + 6 * z[6] + z[7] + z[13];
z[3] = abb[32] * z[3];
z[5] = -abb[31] * z[31];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[88];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[40] * z[21];
z[3] = abb[38] * z[82];
z[4] = z[68] + z[71];
z[4] = abb[39] * z[4];
z[2] = z[2] + z[3] + z[4];
z[1] = z[1] + 3 * z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_313_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.22705487121682531585184216070836765633796551931412506268836417"),stof<T>("-83.781197666787351751956231260651738209094290445805215079939375928")}, std::complex<T>{stof<T>("-6.82070293910296763206809565657268841048751017346517091363243205"),stof<T>("-48.273888092845965450964722256267332118280718923943817868695212391")}, std::complex<T>{stof<T>("-6.487778227773556684859704839822798395323266192295462149335665204"),stof<T>("50.985310504584355545386129673624559246764242442805024950460033881")}, std::complex<T>{stof<T>("-7.184124888314354108986160328487616470637305501138732813068445575"),stof<T>("18.6213156475939604861182307099586211964636631965117156096363888")}, std::complex<T>{stof<T>("-9.254686010775785804542390477783609725658270984679616371327311634"),stof<T>("-15.55469254458090349004541917808688501560192144786321451017521826")}, std::complex<T>{stof<T>("-7.184124888314354108986160328487616470637305501138732813068445575"),stof<T>("18.6213156475939604861182307099586211964636631965117156096363888")}, std::complex<T>{stof<T>("38.780699012426523743089114106068010223173683305746504857014498087"),stof<T>("15.712188923077182853362767695481843777380915081787966261904006313")}, std::complex<T>{stof<T>("-43.693299263115379353354045628413022018389543203864180985206756727"),stof<T>("49.092492404841541510823828740696764182871493937718239190702631764")}, std::complex<T>{stof<T>("6.124356278562170207941640167907870335173470864621900249899651679"),stof<T>("15.90989323585557039169682329260139406798013967765050852787156731")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_313_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_313_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("41.335149241672406577830469226200241270730304505594626944805218864"),stof<T>("-4.110441131842011108727343692266235889087615146785555234126705769")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,54> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W18(k,dl), dlog_W22(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k)};

                    
            return f_4_313_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_313_DLogXconstant_part(base_point<T>, kend);
	value += f_4_313_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_313_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_313_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_313_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_313_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_313_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_313_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
