/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_240.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_240_abbreviated (const std::array<T,28>& abb) {
T z[31];
z[0] = abb[23] + abb[24] + abb[25];
z[1] = abb[8] * z[0];
z[2] = (T(1) / T(2)) * z[1];
z[3] = abb[7] * z[0];
z[4] = z[2] + (T(1) / T(2)) * z[3];
z[5] = abb[20] * (T(1) / T(2));
z[6] = abb[18] + z[5];
z[7] = abb[19] + abb[22];
z[8] = -z[6] + (T(1) / T(2)) * z[7];
z[9] = -abb[2] * z[8];
z[10] = abb[19] + -abb[21];
z[11] = abb[18] + abb[20];
z[12] = z[10] + z[11];
z[13] = abb[3] * z[12];
z[14] = abb[21] + abb[22];
z[15] = 3 * abb[18] + -z[14];
z[16] = abb[20] + (T(1) / T(2)) * z[15];
z[17] = abb[6] * z[16];
z[11] = -abb[22] + z[11];
z[18] = abb[4] * z[11];
z[6] = abb[21] * (T(-1) / T(2)) + z[6];
z[6] = abb[1] * z[6];
z[6] = z[4] + z[6] + z[9] + (T(1) / T(2)) * z[13] + -z[17] + (T(3) / T(2)) * z[18];
z[6] = abb[11] * z[6];
z[9] = abb[2] * (T(1) / T(2));
z[12] = z[9] * z[12];
z[13] = abb[0] * (T(1) / T(2));
z[13] = z[8] * z[13];
z[12] = (T(1) / T(4)) * z[3] + z[12] + z[13];
z[13] = abb[19] * (T(1) / T(2));
z[19] = abb[21] + abb[22] * (T(3) / T(2)) + z[13];
z[20] = 2 * abb[18];
z[19] = abb[20] * (T(-5) / T(4)) + (T(1) / T(2)) * z[19] + -z[20];
z[19] = abb[3] * z[19];
z[15] = 2 * abb[20] + z[15];
z[21] = abb[1] * z[15];
z[19] = z[12] + z[17] + z[19] + -z[21];
z[22] = abb[12] + -abb[13];
z[22] = z[19] * z[22];
z[23] = 4 * abb[18];
z[24] = 3 * abb[20] + -abb[21] + -2 * abb[22] + z[23];
z[24] = abb[1] * z[24];
z[25] = abb[3] * z[15];
z[26] = abb[6] * z[15];
z[24] = z[18] + z[24] + z[25] + -z[26];
z[27] = -abb[14] * z[24];
z[6] = z[6] + z[22] + z[27];
z[6] = abb[11] * z[6];
z[22] = abb[22] * (T(1) / T(2));
z[27] = abb[21] + z[22];
z[28] = abb[20] * (T(3) / T(2)) + -z[27];
z[29] = -z[13] + -z[20] + -z[28];
z[29] = abb[2] * z[29];
z[8] = abb[0] * z[8];
z[30] = abb[18] + -abb[19];
z[30] = abb[3] * z[30];
z[2] = -z[2] + z[8] + z[21] + z[29] + 2 * z[30];
z[2] = abb[12] * z[2];
z[8] = abb[5] * z[15];
z[15] = -z[8] + 3 * z[21] + 2 * z[25] + -z[26];
z[25] = abb[14] * z[15];
z[2] = z[2] + z[25];
z[2] = abb[12] * z[2];
z[25] = 3 * abb[21] + abb[22] * (T(5) / T(2)) + -z[13];
z[23] = abb[20] * (T(-11) / T(4)) + -z[23] + (T(1) / T(2)) * z[25];
z[23] = abb[3] * z[23];
z[17] = z[8] + -z[12] + z[17] + -2 * z[21] + z[23];
z[17] = abb[12] * z[17];
z[13] = -z[13] + z[27];
z[13] = -abb[18] + abb[20] * (T(-3) / T(4)) + (T(1) / T(2)) * z[13];
z[13] = abb[3] * z[13];
z[21] = abb[5] * z[16];
z[23] = -abb[1] * z[16];
z[12] = -z[12] + z[13] + z[21] + z[23];
z[12] = abb[13] * z[12];
z[12] = z[12] + z[17];
z[12] = abb[13] * z[12];
z[13] = abb[0] + abb[3];
z[23] = (T(3) / T(4)) * z[0];
z[13] = z[13] * z[23];
z[23] = abb[8] + abb[9];
z[23] = z[16] * z[23];
z[9] = abb[10] + -z[9];
z[0] = z[0] * z[9];
z[5] = z[5] + z[22];
z[9] = -abb[21] + abb[19] * (T(3) / T(2)) + z[5];
z[22] = abb[7] * (T(1) / T(2));
z[9] = z[9] * z[22];
z[0] = -z[0] + z[9] + z[13] + z[23];
z[9] = -abb[27] * z[0];
z[13] = 2 * abb[5] + -abb[6];
z[14] = abb[18] + abb[20] * (T(2) / T(3)) + (T(-1) / T(3)) * z[14];
z[13] = z[13] * z[14];
z[5] = abb[18] * (T(-1) / T(2)) + -z[5] + -z[10];
z[5] = abb[3] * z[5];
z[10] = -abb[22] + abb[18] * (T(-4) / T(3)) + abb[20] * (T(-1) / T(6)) + abb[21] * (T(7) / T(6));
z[10] = abb[1] * z[10];
z[14] = abb[22] + abb[21] * (T(-1) / T(3));
z[14] = abb[18] * (T(-5) / T(6)) + abb[20] * (T(-1) / T(3)) + abb[19] * (T(2) / T(3)) + (T(1) / T(2)) * z[14];
z[14] = abb[2] * z[14];
z[4] = -z[4] + (T(1) / T(3)) * z[5] + z[10] + z[13] + z[14];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[5] = abb[18] * (T(5) / T(2)) + z[28];
z[5] = abb[1] * z[5];
z[10] = abb[3] * z[16];
z[5] = z[5] + z[10] + (T(-1) / T(2)) * z[18] + -z[21];
z[5] = prod_pow(abb[14], 2) * z[5];
z[10] = abb[15] * z[24];
z[13] = abb[26] * z[15];
z[2] = z[2] + z[4] + z[5] + z[6] + z[9] + z[10] + z[12] + z[13];
z[4] = -abb[18] + abb[21] + -abb[22];
z[4] = abb[1] * z[4];
z[4] = z[4] + z[8] + 2 * z[18] + -z[26];
z[4] = abb[14] * z[4];
z[5] = abb[13] * z[19];
z[6] = abb[20] + -z[7] + z[20];
z[7] = -abb[2] + abb[3];
z[6] = z[6] * z[7];
z[7] = abb[1] * z[11];
z[7] = z[7] + -z[18];
z[1] = -z[1] + -z[3] + z[6] + 2 * z[7];
z[1] = abb[11] * z[1];
z[1] = z[1] + z[4] + z[5] + z[17];
z[1] = m1_set::bc<T>[0] * z[1];
z[0] = -abb[17] * z[0];
z[3] = abb[16] * z[15];
z[0] = z[0] + z[1] + z[3];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_240_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.819632815579498942189688408353079355378528686218847979574353393"),stof<T>("14.276314065293514956511827405386637490287777181999150628429505089")}, std::complex<T>{stof<T>("-12.3283672886785364847232001551944048439285359397906020651212025211"),stof<T>("-1.1320407441903722082871918205716230148170823948741651600313691976")}, std::complex<T>{stof<T>("-6.4730072807196775268428657889559693879003884371585441891565109468"),stof<T>("4.800369712413548688281429115902097361462124556108535457561819782")}, std::complex<T>{stof<T>("13.674992823538357900070022774591514811406676188850905855539044967"),stof<T>("-8.343903608689594059943206468912917114008570231016450010836316109")}, std::complex<T>{stof<T>("-7.2019855428186803732271569856355454235062877516923616663825340205"),stof<T>("3.5435338962760453716617773530108197525464456749079145532744963274")}, std::complex<T>{stof<T>("-4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("-4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("-4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("6.7898493908076716514634340510446747195333140327511877322391776059")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_240_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_240_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("8.4398028656071492016532352079163994483074608864738259312953233156"),stof<T>("-0.906723019227952867052876679613530455814338445057657061206383308")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dlog_W3(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_240_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_240_DLogXconstant_part(base_point<T>, kend);
	value += f_4_240_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_240_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_240_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_240_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_240_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_240_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_240_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
