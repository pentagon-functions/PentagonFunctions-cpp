/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_209.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_209_abbreviated (const std::array<T,27>& abb) {
T z[34];
z[0] = -abb[20] + abb[22];
z[1] = 5 * z[0];
z[2] = abb[18] + z[1];
z[3] = 3 * abb[19];
z[2] = (T(1) / T(2)) * z[2] + -z[3];
z[2] = -abb[21] + (T(1) / T(2)) * z[2];
z[2] = abb[0] * z[2];
z[4] = 2 * abb[18];
z[5] = 2 * abb[21];
z[6] = z[4] + -z[5];
z[7] = 3 * z[0];
z[8] = -5 * abb[19] + z[6] + z[7];
z[9] = abb[1] * z[8];
z[10] = -abb[18] + abb[21];
z[11] = abb[19] * (T(-3) / T(2)) + (T(1) / T(2)) * z[0] + -z[10];
z[12] = abb[6] * z[11];
z[2] = z[2] + -z[9] + z[12];
z[9] = 2 * z[0] + -z[3];
z[13] = z[9] + -z[10];
z[13] = abb[2] * z[13];
z[14] = abb[23] + -abb[24];
z[15] = abb[8] * (T(1) / T(2));
z[15] = -z[14] * z[15];
z[16] = abb[18] + z[0];
z[16] = -abb[19] + (T(1) / T(2)) * z[16];
z[17] = abb[3] * z[16];
z[18] = -abb[19] + z[0];
z[19] = abb[5] * z[18];
z[20] = 2 * z[19];
z[13] = z[2] + -z[13] + z[15] + (T(-1) / T(2)) * z[17] + z[20];
z[13] = abb[12] * z[13];
z[21] = abb[7] * z[14];
z[22] = abb[12] * z[21];
z[13] = z[13] + (T(-1) / T(4)) * z[22];
z[22] = -z[0] + z[3] + -z[6];
z[23] = abb[6] * z[22];
z[8] = abb[2] * z[8];
z[24] = 2 * z[18];
z[25] = abb[0] * z[24];
z[8] = z[8] + -z[20] + -z[25];
z[25] = 7 * abb[19] + -z[1] + -z[6];
z[26] = abb[1] * z[25];
z[26] = -z[8] + -z[23] + z[26];
z[27] = abb[13] * z[26];
z[27] = z[13] + -z[27];
z[10] = abb[19] * (T(5) / T(2)) + (T(-3) / T(2)) * z[0] + z[10];
z[10] = abb[1] * z[10];
z[28] = -abb[18] + z[0];
z[29] = z[5] + -z[28];
z[30] = abb[4] * z[29];
z[31] = -abb[2] * z[11];
z[32] = abb[0] * z[28];
z[31] = z[10] + z[12] + z[30] + z[31] + z[32];
z[31] = abb[11] * z[31];
z[21] = (T(3) / T(4)) * z[21];
z[32] = -z[15] + (T(3) / T(2)) * z[17] + z[21];
z[4] = z[0] + z[4];
z[3] = -abb[21] + -z[3] + z[4];
z[3] = abb[2] * z[3];
z[2] = -z[2] + z[3] + -z[32];
z[2] = abb[14] * z[2];
z[2] = z[2] + z[27] + z[31];
z[2] = abb[11] * z[2];
z[3] = -abb[18] + abb[19];
z[31] = abb[2] * z[3];
z[17] = z[17] + z[31];
z[31] = (T(3) / T(2)) * z[16];
z[33] = -abb[21] + z[31];
z[33] = abb[0] * z[33];
z[10] = z[10] + (T(3) / T(2)) * z[17] + z[21] + z[33];
z[10] = abb[14] * z[10];
z[10] = z[10] + -z[27];
z[10] = abb[14] * z[10];
z[17] = abb[21] * (T(2) / T(3));
z[21] = abb[18] * (T(-2) / T(3)) + abb[19] * (T(5) / T(3)) + -z[0] + z[17];
z[21] = abb[2] * z[21];
z[4] = -abb[19] + (T(1) / T(3)) * z[4] + -z[17];
z[4] = abb[6] * z[4];
z[17] = abb[21] * (T(-13) / T(2)) + z[24];
z[17] = abb[0] * z[17];
z[1] = abb[18] * (T(-17) / T(2)) + -z[1];
z[1] = abb[19] * (T(9) / T(2)) + abb[21] * (T(17) / T(6)) + (T(1) / T(3)) * z[1];
z[1] = abb[1] * z[1];
z[1] = z[1] + z[4] + (T(1) / T(3)) * z[17] + (T(2) / T(3)) * z[19] + z[21];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[4] = -z[3] + z[5];
z[4] = abb[0] * z[4];
z[0] = -abb[19] + -z[0] + z[6];
z[0] = abb[1] * z[0];
z[6] = -abb[2] * z[18];
z[0] = z[0] + z[4] + z[6] + z[19];
z[4] = prod_pow(abb[13], 2);
z[0] = z[0] * z[4];
z[6] = -abb[0] + abb[2];
z[6] = z[6] * z[16];
z[16] = abb[1] * z[18];
z[6] = z[6] + -z[15] + z[16] + -z[19];
z[6] = prod_pow(abb[12], 2) * z[6];
z[15] = abb[2] + -abb[10] + abb[0] * (T(1) / T(4)) + abb[3] * (T(3) / T(4));
z[14] = -z[14] * z[15];
z[11] = abb[9] * z[11];
z[15] = -abb[21] + (T(1) / T(2)) * z[28];
z[15] = abb[8] * z[15];
z[16] = abb[7] * z[31];
z[11] = -z[11] + z[14] + z[15] + -z[16];
z[14] = -abb[26] * z[11];
z[15] = abb[2] * z[22];
z[9] = abb[18] + z[9];
z[5] = z[5] + z[9];
z[5] = abb[0] * z[5];
z[5] = z[5] + z[15];
z[5] = abb[15] * z[5];
z[8] = -abb[25] * z[8];
z[15] = -abb[15] + -abb[25];
z[15] = z[15] * z[23];
z[9] = 4 * abb[21] + -z[9];
z[9] = abb[15] * z[9];
z[4] = -z[4] * z[29];
z[4] = z[4] + z[9];
z[4] = abb[4] * z[4];
z[9] = abb[25] * z[25];
z[16] = abb[15] * z[18];
z[9] = z[9] + -3 * z[16];
z[9] = abb[1] * z[9];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[8] + z[9] + z[10] + z[14] + z[15];
z[1] = -abb[21] + z[24];
z[1] = abb[2] * z[1];
z[2] = abb[18] + -z[7];
z[2] = abb[19] + (T(1) / T(2)) * z[2];
z[2] = -abb[21] + (T(1) / T(2)) * z[2];
z[2] = abb[0] * z[2];
z[4] = abb[1] * z[24];
z[1] = z[1] + z[2] + z[4] + -z[12] + -z[20] + z[32];
z[1] = abb[14] * z[1];
z[2] = abb[0] * z[29];
z[2] = -z[2] + z[30];
z[4] = 3 * abb[1] + abb[2];
z[4] = z[4] * z[22];
z[2] = 2 * z[2] + z[4] + -z[23];
z[2] = abb[13] * z[2];
z[4] = -abb[1] + -abb[2];
z[4] = z[4] * z[18];
z[3] = -abb[0] * z[3];
z[3] = z[3] + z[4] + z[19] + -z[30];
z[3] = abb[11] * z[3];
z[1] = z[1] + z[2] + 2 * z[3] + -z[13];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[17] * z[11];
z[3] = abb[16] * z[26];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_209_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-13.276195570559416486478966186296973222490612857687288039718866913"),stof<T>("35.314153730281515935942008304622660630931389626139620138638242022")}, std::complex<T>{stof<T>("13.103992331776114223711603037798741967028832637211032979468063628"),stof<T>("-28.976701269069971943585298287524752747198502022727122404951789515")}, std::complex<T>{stof<T>("-0.172203238783302262767363148498231255461780220476255060250803285"),stof<T>("6.3374524612115439923567100170979078837328876034124977336864525063")}, std::complex<T>{stof<T>("16.147328438486349828229591509236169108512553636489629036487637101"),stof<T>("21.002563390181037990731090261970791556060461416282829044788117307")}, std::complex<T>{stof<T>("0.172203238783302262767363148498231255461780220476255060250803285"),stof<T>("-6.3374524612115439923567100170979078837328876034124977336864525063")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}, std::complex<T>{stof<T>("0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("-0.64194487762304279435700210128213717713839885131132720351431713993")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_209_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_209_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("22.227297927436354128698602673496422427038429717452235048571112114"),stof<T>("-29.100131925637271942526338882661493358283090296529561198973606645")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dl[1], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_2_3(k), f_2_6_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), f_2_6_re(k), f_2_24_re(k)};

                    
            return f_4_209_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_209_DLogXconstant_part(base_point<T>, kend);
	value += f_4_209_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_209_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_209_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_209_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_209_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_209_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_209_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
