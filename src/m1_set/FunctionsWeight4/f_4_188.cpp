/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_188.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_188_abbreviated (const std::array<T,17>& abb) {
T z[21];
z[0] = abb[13] * (T(1) / T(2));
z[1] = abb[11] * (T(1) / T(2)) + z[0];
z[2] = 2 * abb[15];
z[3] = abb[12] + abb[14] + -z[2];
z[4] = z[1] + z[3];
z[4] = abb[1] * z[4];
z[5] = -abb[12] + abb[14];
z[6] = -abb[11] + abb[13];
z[7] = z[5] + z[6];
z[7] = abb[4] * z[7];
z[8] = z[5] + -z[6];
z[8] = abb[5] * z[8];
z[9] = abb[11] + z[3];
z[10] = abb[3] * z[9];
z[11] = -z[4] + (T(-1) / T(2)) * z[7] + z[8] + z[10];
z[11] = abb[8] * z[11];
z[12] = 2 * z[3];
z[13] = abb[11] + abb[13];
z[14] = z[12] + z[13];
z[15] = abb[1] * z[14];
z[10] = -z[7] + -2 * z[10] + z[15];
z[10] = abb[6] * z[10];
z[10] = z[10] + z[11];
z[10] = abb[8] * z[10];
z[11] = abb[1] * z[6];
z[6] = abb[3] * z[6];
z[11] = z[6] + z[11];
z[15] = -abb[0] * z[14];
z[11] = 2 * z[11] + z[15];
z[15] = -abb[6] + abb[8];
z[11] = z[11] * z[15];
z[16] = abb[0] * z[5];
z[17] = -z[6] + -z[8] + z[16];
z[17] = abb[7] * z[17];
z[12] = 3 * abb[11] + -abb[13] + z[12];
z[18] = abb[2] * z[12];
z[15] = z[15] * z[18];
z[11] = z[11] + z[15] + z[17];
z[11] = abb[7] * z[11];
z[17] = abb[13] * (T(-5) / T(2)) + abb[11] * (T(7) / T(2)) + z[3];
z[17] = abb[3] * z[17];
z[9] = -abb[1] * z[9];
z[9] = (T(-5) / T(2)) * z[8] + z[9] + (T(7) / T(2)) * z[16] + z[17];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[16] = abb[13] + z[3];
z[17] = abb[3] * z[16];
z[4] = -z[4] + (T(3) / T(2)) * z[7] + z[17];
z[19] = prod_pow(abb[6], 2);
z[4] = z[4] * z[19];
z[20] = -abb[1] * z[12];
z[17] = z[7] + 2 * z[17] + z[20];
z[17] = abb[9] * z[17];
z[20] = -prod_pow(abb[8], 2);
z[20] = z[19] + z[20];
z[0] = abb[11] * (T(3) / T(2)) + -z[0] + z[3];
z[0] = z[0] * z[20];
z[3] = abb[9] * z[12];
z[0] = z[0] + z[3];
z[0] = abb[2] * z[0];
z[1] = abb[12] * (T(1) / T(2)) + abb[14] * (T(3) / T(2)) + z[1] + -z[2];
z[2] = -z[1] * z[19];
z[1] = abb[8] * z[1];
z[3] = -abb[6] * z[5];
z[1] = z[1] + z[3];
z[1] = abb[8] * z[1];
z[3] = -abb[11] + -abb[13] + 4 * abb[15];
z[5] = -3 * abb[12] + -abb[14] + z[3];
z[5] = abb[9] * z[5];
z[1] = z[1] + z[2] + z[5];
z[1] = abb[0] * z[1];
z[2] = 2 * abb[1];
z[2] = z[2] * z[16];
z[3] = abb[12] + 3 * abb[14] + -z[3];
z[3] = abb[0] * z[3];
z[2] = -z[2] + z[3] + z[8];
z[3] = abb[2] + -abb[3];
z[3] = z[3] * z[12];
z[3] = -z[2] + z[3];
z[3] = abb[16] * z[3];
z[0] = z[0] + z[1] + z[3] + z[4] + (T(1) / T(3)) * z[9] + z[10] + z[11] + z[17];
z[1] = z[7] + -z[8];
z[1] = abb[8] * z[1];
z[3] = -z[6] + -z[7];
z[3] = abb[6] * z[3];
z[1] = z[1] + z[3];
z[3] = abb[14] + -abb[15];
z[3] = 4 * z[3] + z[13];
z[3] = abb[6] * z[3];
z[4] = -abb[8] * z[14];
z[3] = z[3] + z[4];
z[3] = abb[0] * z[3];
z[1] = 2 * z[1] + z[3] + z[15];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[3] * z[12];
z[2] = -z[2] + z[3] + z[18];
z[2] = abb[10] * z[2];
z[1] = z[1] + z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_188_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("11.1231464561236586273124563303981169811699648158847388817903589943"),stof<T>("-5.9079601393747193953923148060160025755193369358308241695272798262")}, std::complex<T>{stof<T>("7.962799379729440207310842399981307364863146460690968827796623753"),stof<T>("9.9372348957886210955919134463176886769053300606467173500580183881")}, std::complex<T>{stof<T>("-0.8093865748586307518508933486579257601277954756740860729800273434"),stof<T>("-4.9830764590960427948879363300327806475954145604462590432757470355")}, std::complex<T>{stof<T>("12.664720382800615543612283563499075077221192219730336789824039549"),stof<T>("-31.719308092730145476152415718415255123134833053200883775664072112")}, std::complex<T>{stof<T>("-20.627519762530055750923125963480382442084338680421305617620663302"),stof<T>("21.782073196941524380560502272097566446229502992554166425606053723")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[22].real()/kbase.W[22].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_188_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_188_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-23.482325476159740874439499764333047122605783938716380492818823115"),stof<T>("-13.136301231887298085660883363344361919220847759396564063396254535")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,17> abb = {dlog_W3(k,dl), dlog_W7(k,dl), dlog_W8(k,dl), dl[4], dlog_W18(k,dl), dlog_W26(k,dl), f_1_6(k), f_1_8(k), f_1_10(k), f_2_8(k), f_2_11_im(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[22].real()/k.W[22].real()), f_2_11_re(k)};

                    
            return f_4_188_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_188_DLogXconstant_part(base_point<T>, kend);
	value += f_4_188_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_188_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_188_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_188_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_188_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_188_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_188_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
