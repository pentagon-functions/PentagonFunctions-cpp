/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_713.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_713_abbreviated (const std::array<T,60>& abb) {
T z[82];
z[0] = abb[47] + -abb[50];
z[1] = abb[46] + -abb[51];
z[2] = z[0] + -z[1];
z[3] = abb[19] * z[2];
z[4] = abb[17] * z[2];
z[5] = z[3] + z[4];
z[6] = abb[16] * z[2];
z[7] = z[5] + z[6];
z[8] = abb[0] + -abb[11];
z[9] = -abb[1] + abb[10];
z[10] = z[8] + z[9];
z[11] = abb[53] * z[10];
z[12] = abb[48] + -abb[49];
z[13] = z[0] + z[12];
z[14] = abb[21] * z[13];
z[15] = z[1] + z[12];
z[16] = abb[20] * z[15];
z[11] = -z[7] + z[11] + z[14] + z[16];
z[11] = abb[26] * z[11];
z[17] = abb[16] * z[15];
z[18] = z[16] + z[17];
z[19] = abb[18] * z[15];
z[20] = z[18] + z[19];
z[21] = abb[19] * z[15];
z[22] = z[20] + z[21];
z[23] = abb[17] * z[15];
z[24] = z[22] + z[23];
z[24] = abb[29] * z[24];
z[10] = abb[26] * z[10];
z[25] = abb[3] + abb[5];
z[26] = -abb[10] + z[25];
z[27] = abb[29] * z[26];
z[28] = z[10] + z[27];
z[28] = abb[55] * z[28];
z[29] = abb[18] * z[2];
z[30] = abb[24] * z[29];
z[31] = abb[29] * abb[53];
z[26] = z[26] * z[31];
z[24] = z[11] + -z[24] + z[26] + z[28] + -z[30];
z[26] = -abb[3] + abb[8];
z[28] = -z[9] + z[26];
z[30] = abb[26] * z[28];
z[32] = abb[1] + abb[5] + -abb[8];
z[32] = abb[24] * z[32];
z[27] = -z[27] + z[30] + z[32];
z[26] = -abb[2] + z[26];
z[32] = -abb[4] + abb[5];
z[33] = z[26] + -z[32];
z[34] = abb[25] * z[33];
z[35] = abb[2] + -abb[4];
z[36] = -abb[5] + z[9];
z[37] = z[35] + z[36];
z[38] = abb[30] * z[37];
z[39] = -abb[1] + abb[3];
z[40] = z[35] + z[39];
z[41] = abb[28] * z[40];
z[42] = 2 * z[39];
z[43] = abb[27] * z[42];
z[34] = -z[27] + -z[34] + z[38] + -z[41] + -z[43];
z[38] = abb[52] + abb[54];
z[34] = z[34] * z[38];
z[41] = abb[53] + abb[55];
z[43] = 2 * abb[5];
z[44] = abb[1] + abb[2] + 2 * abb[4] + -abb[11] + -z[43];
z[44] = z[41] * z[44];
z[44] = -z[5] + z[14] + z[44];
z[44] = abb[28] * z[44];
z[45] = 2 * z[32];
z[46] = abb[53] * z[45];
z[47] = abb[55] * z[45];
z[5] = z[5] + z[29] + z[46] + z[47];
z[46] = abb[25] * z[5];
z[48] = abb[18] * z[13];
z[49] = z[14] + z[48];
z[18] = z[18] + z[49];
z[50] = abb[10] + -abb[11] + z[32];
z[51] = z[41] * z[50];
z[51] = z[18] + z[51];
z[52] = -abb[2] + abb[8];
z[53] = -abb[4] + abb[10];
z[54] = z[52] + -z[53];
z[55] = z[38] * z[54];
z[55] = -z[51] + z[55];
z[55] = abb[23] * z[55];
z[56] = abb[16] * z[13];
z[48] = z[48] + z[56];
z[56] = -z[14] + z[48];
z[57] = abb[19] * z[13];
z[57] = z[56] + z[57];
z[58] = abb[17] * z[13];
z[58] = z[57] + z[58];
z[59] = abb[0] + -abb[1];
z[60] = -abb[11] + z[25] + z[59];
z[61] = z[41] * z[60];
z[61] = -z[58] + z[61];
z[62] = abb[27] * z[61];
z[63] = -abb[2] + z[9] + z[32];
z[64] = z[41] * z[63];
z[64] = z[20] + z[64];
z[64] = abb[30] * z[64];
z[34] = z[24] + z[34] + z[44] + z[46] + z[55] + -z[62] + z[64];
z[34] = m1_set::bc<T>[0] * z[34];
z[44] = abb[43] * z[33];
z[46] = abb[45] * z[37];
z[55] = abb[42] * z[54];
z[64] = abb[44] * z[42];
z[44] = z[44] + -z[46] + -z[55] + z[64];
z[44] = -z[38] * z[44];
z[46] = -abb[42] * z[51];
z[51] = z[4] + z[29];
z[55] = z[3] + z[51];
z[55] = abb[43] * z[55];
z[64] = abb[45] * z[20];
z[61] = -abb[44] * z[61];
z[65] = abb[45] * z[63];
z[45] = abb[43] * z[45];
z[45] = z[45] + z[65];
z[45] = z[41] * z[45];
z[34] = z[34] + z[44] + z[45] + z[46] + z[55] + z[61] + z[64];
z[34] = 4 * z[34];
z[44] = abb[0] + -abb[3];
z[45] = 2 * abb[7];
z[46] = -abb[9] + z[45];
z[55] = -z[32] + z[44] + z[46];
z[55] = abb[33] * z[55];
z[61] = abb[17] + abb[19];
z[64] = abb[16] + abb[18];
z[65] = z[61] + 2 * z[64];
z[65] = abb[41] * z[65];
z[66] = 2 * abb[6];
z[67] = -abb[9] + z[66];
z[52] = -abb[0] + z[52] + z[67];
z[52] = abb[32] * z[52];
z[68] = z[45] + -z[66];
z[69] = z[35] + z[68];
z[70] = -z[39] + z[69];
z[70] = abb[39] * z[70];
z[71] = abb[59] * z[37];
z[26] = z[26] + -z[59];
z[72] = abb[36] * z[26];
z[54] = abb[56] * z[54];
z[73] = -abb[0] + z[46] + z[53];
z[73] = abb[31] * z[73];
z[74] = -abb[4] + z[9];
z[75] = abb[3] + z[74];
z[75] = abb[35] * z[75];
z[76] = abb[14] * abb[40];
z[42] = abb[58] * z[42];
z[77] = abb[0] * abb[35];
z[42] = -z[42] + z[52] + z[54] + z[55] + z[65] + z[70] + z[71] + -z[72] + -z[73] + z[75] + -z[76] + -z[77];
z[52] = 2 * abb[8] + -z[35];
z[54] = -2 * abb[3] + -z[43] + z[46] + z[52];
z[54] = abb[25] * z[54];
z[55] = abb[24] * z[33];
z[46] = abb[8] + -z[25] + z[46];
z[65] = abb[27] + -abb[28];
z[70] = z[46] * z[65];
z[55] = z[55] + -z[70];
z[54] = z[54] + -2 * z[55];
z[54] = abb[25] * z[54];
z[55] = 2 * abb[1];
z[70] = 2 * abb[10];
z[71] = z[55] + -z[70];
z[72] = -z[67] + z[71];
z[75] = -z[35] + z[43] + z[72];
z[75] = abb[30] * z[75];
z[37] = abb[29] * z[37];
z[36] = z[36] + z[67];
z[76] = z[36] * z[65];
z[37] = z[37] + -z[76];
z[37] = 2 * z[37] + z[75];
z[37] = abb[30] * z[37];
z[75] = abb[24] * z[26];
z[76] = z[44] + -z[74];
z[76] = abb[29] * z[76];
z[78] = -abb[6] + abb[7];
z[78] = abb[28] * z[78];
z[75] = -z[30] + z[75] + z[76] + z[78];
z[78] = 2 * abb[28];
z[75] = z[75] * z[78];
z[52] = -3 * z[39] + z[52] + -z[70];
z[52] = abb[26] * z[52];
z[52] = z[52] + -2 * z[76];
z[52] = abb[26] * z[52];
z[26] = abb[26] * z[26];
z[40] = abb[29] * z[40];
z[26] = z[26] + z[40];
z[40] = -z[32] + z[59];
z[76] = abb[24] * z[40];
z[26] = 2 * z[26] + z[76];
z[26] = abb[24] * z[26];
z[68] = -z[39] + z[68];
z[68] = abb[28] * z[68];
z[78] = abb[27] * z[39];
z[27] = z[27] + -z[68] + z[78];
z[68] = 2 * abb[27];
z[27] = z[27] * z[68];
z[68] = prod_pow(abb[29], 2);
z[68] = 2 * abb[38] + z[68];
z[25] = -abb[0] + -abb[2] + z[25];
z[78] = z[25] * z[68];
z[79] = 2 * abb[57];
z[33] = z[33] * z[79];
z[80] = 2 * abb[37];
z[81] = z[40] * z[80];
z[26] = z[26] + z[27] + -z[33] + z[37] + 2 * z[42] + -z[52] + z[54] + z[75] + z[78] + z[81];
z[26] = -z[26] * z[38];
z[17] = z[17] + z[19];
z[19] = abb[2] + z[67];
z[27] = -abb[5] + z[19] + z[59];
z[33] = abb[55] + z[38];
z[27] = z[27] * z[33];
z[33] = -abb[5] + abb[9];
z[37] = abb[2] + -z[33] + z[59];
z[37] = abb[53] * z[37];
z[27] = z[17] + z[21] + z[27] + z[37];
z[27] = abb[34] * z[27];
z[18] = abb[56] * z[18];
z[37] = z[6] + z[29];
z[37] = abb[33] * z[37];
z[42] = -abb[58] * z[56];
z[52] = -abb[59] * z[20];
z[15] = abb[35] * z[15];
z[54] = abb[58] * z[13];
z[15] = z[15] + z[54];
z[54] = abb[32] + -abb[36];
z[56] = -z[2] * z[54];
z[56] = -z[15] + z[56];
z[56] = abb[19] * z[56];
z[67] = z[16] + z[21];
z[75] = abb[31] * z[67];
z[78] = abb[33] + abb[36];
z[2] = z[2] * z[78];
z[2] = z[2] + -z[15];
z[2] = abb[17] * z[2];
z[15] = -z[6] * z[54];
z[54] = -abb[35] * z[16];
z[2] = z[2] + z[15] + z[18] + z[27] + z[37] + z[42] + z[52] + z[54] + z[56] + z[75];
z[15] = z[40] * z[41];
z[15] = -z[7] + z[15] + -z[29];
z[18] = abb[24] * z[15];
z[27] = -z[9] + z[33];
z[27] = abb[53] * z[27];
z[37] = abb[55] * z[36];
z[22] = -z[22] + z[27] + -z[37];
z[27] = abb[30] * z[22];
z[37] = abb[29] * z[17];
z[40] = -abb[5] + abb[7];
z[42] = 2 * abb[53];
z[52] = z[40] * z[42];
z[51] = -z[51] + z[52];
z[52] = abb[25] * z[51];
z[54] = abb[55] * z[10];
z[27] = z[11] + -z[18] + z[27] + z[37] + z[52] + z[54];
z[28] = z[28] + z[69];
z[28] = abb[28] * z[28];
z[36] = abb[30] * z[36];
z[37] = abb[25] * z[46];
z[46] = abb[8] + -abb[10];
z[46] = abb[27] * z[46];
z[25] = abb[29] * z[25];
z[25] = z[25] + z[28] + z[30] + z[36] + -z[37] + -z[46] + z[76];
z[28] = -2 * z[38];
z[25] = z[25] * z[28];
z[28] = z[0] + -2 * z[1] + -z[12];
z[30] = abb[16] + abb[19];
z[28] = z[28] * z[30];
z[28] = 2 * z[16] + -z[28] + z[49];
z[30] = 4 * abb[7] + abb[9];
z[9] = abb[4] + z[9];
z[9] = 2 * z[9];
z[36] = 3 * abb[5];
z[37] = z[8] + z[9] + -z[30] + z[36];
z[37] = abb[53] * z[37];
z[46] = 4 * abb[6];
z[49] = abb[0] + abb[11];
z[9] = -abb[9] + z[9] + -z[36] + z[46] + -z[49];
z[9] = abb[55] * z[9];
z[9] = z[9] + z[28] + z[37];
z[9] = abb[28] * z[9];
z[36] = z[33] + z[49] + -z[70];
z[36] = abb[53] * z[36];
z[37] = abb[5] + abb[9];
z[52] = -z[8] + z[37] + -z[70];
z[52] = abb[55] * z[52];
z[28] = -z[28] + z[36] + z[52];
z[28] = abb[27] * z[28];
z[36] = z[4] + z[21];
z[33] = -z[8] + -z[33];
z[33] = abb[53] * z[33];
z[52] = -z[37] + z[49];
z[52] = abb[55] * z[52];
z[33] = -z[14] + z[33] + z[36] + z[52];
z[33] = abb[23] * z[33];
z[9] = z[9] + z[25] + 2 * z[27] + z[28] + z[33];
z[9] = abb[23] * z[9];
z[25] = abb[59] * z[63];
z[27] = abb[58] * z[60];
z[28] = -abb[3] + z[74];
z[28] = abb[35] * z[28];
z[33] = abb[56] * z[50];
z[50] = abb[41] * z[64];
z[52] = 2 * z[59];
z[54] = abb[36] * z[52];
z[25] = z[25] + -z[27] + z[28] + -z[33] + z[50] + z[54] + z[77];
z[27] = -abb[4] + z[45];
z[28] = abb[5] + -z[27] + z[59];
z[28] = abb[39] * z[28];
z[33] = abb[33] * z[40];
z[28] = -z[25] + z[28] + -2 * z[33] + z[73];
z[28] = z[28] * z[42];
z[33] = -abb[0] + -abb[1] + -z[32] + z[66];
z[33] = abb[39] * z[33];
z[40] = abb[0] + -abb[6];
z[40] = abb[32] * z[40];
z[45] = abb[0] + -abb[9] + z[53];
z[45] = abb[31] * z[45];
z[25] = -z[25] + z[33] + 2 * z[40] + z[45];
z[33] = z[44] + z[74];
z[40] = abb[29] * z[33];
z[44] = abb[3] + abb[4] + abb[11] + -4 * z[59] + -z[70];
z[45] = abb[26] * z[44];
z[40] = 2 * z[40] + z[45];
z[40] = abb[26] * z[40];
z[25] = 2 * z[25] + z[40];
z[25] = abb[55] * z[25];
z[40] = abb[29] * z[63];
z[10] = -z[10] + z[40];
z[10] = abb[55] * z[10];
z[32] = z[32] + z[59];
z[40] = z[32] * z[41];
z[40] = -z[6] + z[40];
z[40] = abb[24] * z[40];
z[45] = abb[29] * z[16];
z[50] = z[31] * z[63];
z[10] = z[10] + -z[11] + z[40] + z[45] + z[50];
z[11] = abb[2] + -abb[9] + -z[27] + z[43];
z[11] = abb[53] * z[11];
z[27] = -abb[4] + z[43];
z[19] = z[19] + -z[27];
z[19] = abb[55] * z[19];
z[11] = z[11] + z[19] + z[36] + z[48];
z[11] = abb[28] * z[11];
z[10] = 2 * z[10] + z[11];
z[10] = abb[28] * z[10];
z[8] = z[8] + z[37] + -z[46] + z[55];
z[8] = abb[55] * z[8];
z[11] = -5 * abb[5] + z[30] + -z[49] + z[55];
z[11] = abb[53] * z[11];
z[4] = 2 * z[4];
z[8] = -z[4] + z[8] + z[11] + -z[57];
z[8] = abb[28] * z[8];
z[8] = z[8] + 2 * z[24] + -z[62];
z[8] = abb[27] * z[8];
z[11] = -z[22] * z[65];
z[19] = -abb[29] * abb[55];
z[19] = z[19] + -z[31];
z[19] = z[19] * z[63];
z[22] = -abb[29] * z[20];
z[11] = z[11] + z[19] + z[22];
z[19] = -abb[2] + -abb[9] + z[27] + -z[71];
z[19] = abb[53] * z[19];
z[22] = -abb[2] + -abb[4] + -z[72];
z[22] = abb[55] * z[22];
z[19] = z[19] + 2 * z[20] + z[21] + z[22];
z[19] = abb[30] * z[19];
z[11] = 2 * z[11] + z[19];
z[11] = abb[30] * z[11];
z[0] = -2 * z[0] + 3 * z[1] + z[12];
z[0] = -z[0] * z[61];
z[1] = z[6] + -z[16];
z[6] = abb[53] * z[44];
z[0] = z[0] + 2 * z[1] + z[6] + -z[14];
z[0] = abb[26] * z[0];
z[1] = z[23] + z[67];
z[1] = abb[29] * z[1];
z[6] = z[31] * z[33];
z[1] = z[1] + z[6];
z[0] = z[0] + 2 * z[1];
z[0] = abb[26] * z[0];
z[1] = z[42] * z[59];
z[1] = z[1] + -z[7];
z[1] = abb[26] * z[1];
z[6] = -z[31] * z[32];
z[7] = -abb[29] * z[32];
z[12] = abb[26] * z[52];
z[7] = z[7] + z[12];
z[7] = abb[55] * z[7];
z[1] = z[1] + z[6] + z[7];
z[1] = 2 * z[1] + -z[18];
z[1] = abb[24] * z[1];
z[6] = -abb[7] + z[27];
z[6] = z[6] * z[42];
z[3] = z[3] + z[4] + z[6] + 2 * z[29] + z[47];
z[3] = abb[25] * z[3];
z[4] = -abb[24] * z[5];
z[6] = -z[51] * z[65];
z[4] = z[4] + z[6];
z[3] = z[3] + 2 * z[4];
z[3] = abb[25] * z[3];
z[4] = -abb[22] * z[13];
z[6] = abb[12] + abb[13];
z[7] = abb[14] + z[6];
z[7] = abb[53] * z[7];
z[6] = abb[14] + -z[6];
z[6] = abb[55] * z[6];
z[12] = abb[15] * z[41];
z[4] = z[4] + z[6] + z[7] + z[12];
z[4] = abb[40] * z[4];
z[6] = -z[15] * z[80];
z[5] = -z[5] * z[79];
z[7] = z[17] * z[68];
z[12] = 2 * abb[0] + abb[3] + -abb[11] + z[27] + -z[55];
z[12] = z[12] * z[41];
z[12] = z[12] + -z[58];
z[13] = (T(-1) / T(3)) * z[35] + -z[39];
z[13] = -z[13] * z[38];
z[12] = (T(1) / T(3)) * z[12] + z[13];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[0] = z[0] + z[1] + 2 * z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[25] + z[26] + z[28];
z[0] = 2 * z[0];
return {z[34], z[0]};
}


template <typename T> std::complex<T> f_4_713_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("-12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("16.537165260607952306513862350575882959560418493699168394850268043"),stof<T>("10.877054322280359479773219977897823112402103767095470461713939626")}, std::complex<T>{stof<T>("14.828489720484507046897453457450770565450591257440239914391065281"),stof<T>("10.877054322280359479773219977897823112402103767095470461713939626")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_713_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_713_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-16.318242825496372264672583411332668116429845160004445986730124149"),stof<T>("21.397581756938163177290323382231810987414296529853350017825968654")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W18(k,dl), dlog_W21(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_21(k), f_2_23(k), f_2_28_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_11_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_11_re(k)};

                    
            return f_4_713_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_713_DLogXconstant_part(base_point<T>, kend);
	value += f_4_713_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_713_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_713_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_713_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_713_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_713_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_713_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
