/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_122.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_122_abbreviated (const std::array<T,58>& abb) {
T z[75];
z[0] = -abb[31] + abb[33];
z[0] = abb[4] * z[0];
z[1] = abb[30] + -abb[31];
z[2] = -abb[7] * z[1];
z[0] = z[0] + z[2];
z[3] = abb[32] * (T(1) / T(2));
z[4] = abb[29] * (T(1) / T(2));
z[5] = -abb[30] + z[3] + z[4];
z[6] = abb[14] * (T(1) / T(2));
z[5] = z[5] * z[6];
z[7] = -abb[32] + abb[33];
z[8] = abb[2] * z[7];
z[9] = -abb[28] + abb[32];
z[10] = abb[29] + z[9];
z[11] = -abb[31] + z[10];
z[11] = abb[13] * z[11];
z[0] = (T(-1) / T(2)) * z[0] + z[5] + -z[8] + -z[11];
z[5] = abb[52] * z[0];
z[8] = abb[33] * (T(1) / T(2));
z[12] = abb[32] * (T(5) / T(4)) + -z[8];
z[13] = abb[31] * (T(1) / T(2));
z[14] = abb[28] + z[13];
z[15] = abb[30] * (T(1) / T(2));
z[16] = abb[29] * (T(5) / T(4)) + z[12] + -z[14] + -z[15];
z[16] = abb[5] * z[16];
z[17] = abb[29] + -abb[30];
z[17] = abb[1] * z[17];
z[0] = z[0] + z[16] + z[17];
z[0] = abb[51] * z[0];
z[16] = -abb[49] + abb[53];
z[18] = -abb[33] + z[10];
z[19] = -z[16] * z[18];
z[20] = abb[32] * (T(3) / T(2));
z[21] = -abb[28] + z[20];
z[22] = abb[29] * (T(3) / T(2)) + -z[1] + z[21];
z[22] = -abb[33] + (T(1) / T(2)) * z[22];
z[22] = abb[52] * z[22];
z[19] = (T(1) / T(2)) * z[19] + z[22];
z[19] = abb[5] * z[19];
z[22] = abb[29] + abb[32];
z[23] = abb[28] * (T(1) / T(2));
z[24] = abb[33] * (T(-3) / T(2)) + -z[1] + z[22] + -z[23];
z[24] = abb[5] * z[24];
z[11] = -z[11] + z[17] + z[24];
z[11] = abb[50] * z[11];
z[17] = abb[2] * z[16];
z[7] = z[7] * z[17];
z[0] = z[0] + z[5] + z[7] + z[11] + z[19];
z[0] = m1_set::bc<T>[0] * z[0];
z[5] = abb[21] + abb[24];
z[7] = -abb[25] + z[5];
z[11] = abb[22] + abb[23];
z[19] = z[7] + z[11];
z[24] = abb[33] * z[19];
z[25] = abb[22] * (T(1) / T(2));
z[26] = abb[23] + (T(1) / T(2)) * z[7] + z[25];
z[27] = abb[29] * z[26];
z[28] = abb[32] * z[26];
z[29] = -abb[24] * z[1];
z[28] = z[28] + z[29];
z[29] = abb[22] * z[1];
z[30] = abb[23] * abb[30];
z[24] = z[24] + -z[27] + -z[28] + z[29] + z[30];
z[24] = m1_set::bc<T>[0] * z[24];
z[27] = abb[40] * z[19];
z[29] = abb[24] + z[11];
z[31] = abb[39] * z[29];
z[32] = abb[41] * (T(1) / T(2));
z[33] = abb[26] * z[32];
z[27] = -z[24] + -z[27] + -z[31] + z[33];
z[31] = abb[42] + abb[45] * (T(1) / T(2));
z[33] = -z[27] * z[31];
z[34] = abb[47] * (T(1) / T(2));
z[24] = -z[24] * z[34];
z[35] = abb[4] * (T(1) / T(2));
z[36] = abb[2] + z[35];
z[37] = abb[5] * (T(1) / T(2));
z[38] = -z[36] + -z[37];
z[38] = abb[51] * z[38];
z[39] = -z[19] * z[34];
z[40] = (T(1) / T(2)) * z[16];
z[41] = -abb[52] + z[40];
z[42] = abb[50] * (T(-3) / T(2)) + z[41];
z[42] = abb[5] * z[42];
z[36] = -abb[52] * z[36];
z[17] = z[17] + z[36] + z[38] + z[39] + z[42];
z[17] = abb[40] * z[17];
z[36] = -abb[7] + abb[14];
z[38] = -abb[1] + (T(-1) / T(2)) * z[36] + -z[37];
z[38] = abb[51] * z[38];
z[39] = -z[29] * z[34];
z[36] = -abb[5] + -z[36];
z[36] = abb[52] * z[36];
z[42] = -abb[1] + -abb[5];
z[42] = abb[50] * z[42];
z[36] = (T(1) / T(2)) * z[36] + z[38] + z[39] + z[42];
z[36] = abb[39] * z[36];
z[38] = abb[50] * (T(1) / T(2));
z[39] = -z[38] + z[40];
z[40] = -abb[33] + z[4];
z[21] = z[21] + z[40];
z[42] = -z[21] * z[39];
z[43] = z[14] + -z[15];
z[44] = abb[29] * (T(1) / T(4));
z[12] = z[12] + -z[43] + z[44];
z[12] = abb[51] * z[12];
z[40] = abb[32] + z[40];
z[45] = -abb[28] + -z[1];
z[45] = z[40] + (T(1) / T(2)) * z[45];
z[45] = abb[52] * z[45];
z[12] = z[12] + z[42] + z[45];
z[12] = m1_set::bc<T>[0] * z[12];
z[42] = abb[51] * (T(1) / T(2));
z[41] = -z[38] + z[41] + -z[42];
z[45] = abb[40] * z[41];
z[46] = abb[51] + -abb[52];
z[47] = abb[39] * z[46];
z[12] = z[12] + z[45] + (T(1) / T(2)) * z[47];
z[12] = abb[6] * z[12];
z[45] = 3 * abb[32];
z[47] = abb[29] + z[45];
z[47] = -abb[33] + (T(1) / T(4)) * z[47];
z[47] = z[16] * z[47];
z[48] = abb[33] + z[1];
z[49] = abb[32] * (T(-3) / T(4)) + -z[44] + z[48];
z[49] = abb[52] * z[49];
z[50] = abb[32] * (T(1) / T(4));
z[48] = abb[29] * (T(-3) / T(4)) + z[48] + -z[50];
z[48] = abb[50] * z[48];
z[51] = -abb[30] + -abb[33] + z[22];
z[52] = -abb[51] * z[51];
z[47] = z[47] + z[48] + z[49] + z[52];
z[47] = m1_set::bc<T>[0] * z[47];
z[48] = -abb[50] + z[16];
z[49] = abb[51] + abb[52];
z[52] = z[48] + -z[49];
z[53] = abb[40] * z[52];
z[54] = abb[50] + z[49];
z[54] = abb[39] * z[54];
z[47] = z[47] + -z[53] + z[54];
z[47] = abb[0] * z[47];
z[54] = -abb[28] + z[3];
z[55] = -abb[31] + z[15];
z[56] = z[4] + z[8] + z[54] + z[55];
z[56] = abb[52] * z[56];
z[51] = -z[42] * z[51];
z[57] = z[1] + z[9];
z[58] = abb[50] * z[57];
z[51] = z[51] + z[56] + z[58];
z[51] = m1_set::bc<T>[0] * z[51];
z[56] = (T(1) / T(2)) * z[49];
z[58] = abb[50] + z[56];
z[59] = abb[39] * z[58];
z[60] = abb[40] * z[56];
z[51] = z[51] + z[59] + z[60];
z[51] = abb[8] * z[51];
z[41] = abb[17] * z[41];
z[59] = z[32] * z[41];
z[60] = abb[26] * abb[47];
z[48] = -abb[51] + z[48];
z[61] = abb[20] * z[48];
z[60] = z[60] + z[61];
z[61] = (T(1) / T(4)) * z[60];
z[61] = abb[41] * z[61];
z[22] = -abb[33] + (T(1) / T(2)) * z[22];
z[22] = m1_set::bc<T>[0] * z[22];
z[22] = -abb[40] + z[22];
z[62] = -abb[16] * z[22] * z[48];
z[0] = z[0] + z[12] + z[17] + z[24] + z[33] + z[36] + z[47] + z[51] + z[59] + z[61] + (T(1) / T(2)) * z[62];
z[12] = (T(1) / T(4)) * z[27];
z[17] = -abb[44] + abb[46];
z[24] = abb[43] + -abb[48];
z[27] = -z[17] + -z[24];
z[12] = z[12] * z[27];
z[21] = m1_set::bc<T>[0] * z[21];
z[21] = -abb[40] + z[21];
z[21] = abb[17] * z[21];
z[27] = abb[0] + abb[6];
z[33] = -abb[41] * z[27];
z[22] = abb[20] * z[22];
z[36] = -abb[16] * z[32];
z[21] = z[21] + z[22] + (T(1) / T(2)) * z[33] + z[36];
z[22] = abb[27] * z[32];
z[21] = (T(1) / T(4)) * z[21] + z[22];
z[21] = abb[54] * z[21];
z[22] = -z[23] + z[40];
z[32] = -abb[52] + z[48];
z[22] = m1_set::bc<T>[0] * z[22] * z[32];
z[22] = z[22] + -z[53];
z[22] = abb[12] * z[22];
z[32] = -abb[41] * z[58];
z[18] = m1_set::bc<T>[0] * z[18];
z[18] = -abb[40] + z[18];
z[18] = abb[54] * z[18];
z[18] = z[18] + z[32];
z[32] = abb[18] * (T(1) / T(4));
z[18] = z[18] * z[32];
z[33] = abb[19] * abb[41] * z[49];
z[0] = (T(1) / T(2)) * z[0] + z[12] + z[18] + z[21] + z[22] + (T(-1) / T(8)) * z[33];
z[0] = (T(1) / T(8)) * z[0];
z[12] = abb[25] * abb[28];
z[18] = abb[28] * z[5];
z[12] = z[12] + -z[18];
z[7] = abb[23] + z[7];
z[7] = abb[29] * z[7];
z[21] = abb[23] * abb[28];
z[22] = abb[22] * abb[32];
z[33] = abb[33] * z[25];
z[7] = z[7] + z[12] + -z[21] + z[22] + -z[33];
z[7] = abb[33] * z[7];
z[21] = -z[1] + z[23];
z[22] = abb[22] * z[21];
z[12] = (T(1) / T(2)) * z[12];
z[22] = z[12] + z[22] + -z[30];
z[22] = abb[32] * z[22];
z[25] = abb[28] * z[25];
z[12] = z[12] + z[25] + -z[28];
z[12] = abb[29] * z[12];
z[25] = -abb[28] + z[13];
z[25] = abb[31] * z[25];
z[28] = abb[37] + -abb[38];
z[25] = z[25] + z[28];
z[30] = abb[28] * abb[30];
z[33] = prod_pow(abb[28], 2);
z[36] = (T(1) / T(2)) * z[33];
z[40] = z[30] + -z[36];
z[47] = z[25] + z[40];
z[49] = abb[23] * z[47];
z[51] = abb[28] * abb[31];
z[53] = abb[38] + z[51];
z[30] = -z[30] + z[53];
z[59] = -abb[34] + z[30];
z[59] = abb[22] * z[59];
z[13] = abb[21] * z[13];
z[13] = z[13] + -z[18];
z[13] = abb[31] * z[13];
z[18] = -abb[35] + abb[38];
z[61] = abb[37] + -z[18];
z[5] = z[5] * z[61];
z[19] = abb[56] * z[19];
z[11] = abb[21] + z[11];
z[11] = abb[36] * z[11];
z[61] = abb[34] + z[36];
z[62] = abb[25] * z[61];
z[29] = abb[55] * z[29];
z[63] = prod_pow(abb[30], 2);
z[64] = (T(1) / T(2)) * z[63];
z[65] = abb[24] * z[64];
z[5] = -z[5] + z[7] + -z[11] + z[12] + -z[13] + z[19] + -z[22] + z[29] + -z[49] + z[59] + -z[62] + -z[65];
z[7] = prod_pow(m1_set::bc<T>[0], 2);
z[11] = (T(1) / T(3)) * z[7];
z[12] = z[11] * z[26];
z[5] = (T(1) / T(2)) * z[5] + z[12];
z[12] = abb[57] * (T(1) / T(4));
z[13] = abb[26] * z[12];
z[13] = -z[5] + z[13];
z[19] = (T(1) / T(2)) * z[24];
z[13] = z[13] * z[19];
z[19] = (T(-1) / T(2)) * z[17] + -z[31] + z[34];
z[5] = z[5] * z[19];
z[19] = -abb[32] + z[8];
z[22] = -abb[28] + abb[29];
z[24] = z[19] + z[22];
z[24] = z[8] * z[24];
z[26] = abb[30] * (T(3) / T(4));
z[29] = -abb[31] + -z[23] + z[26];
z[29] = abb[30] * z[29];
z[31] = z[3] + -z[4];
z[34] = -abb[28] + z[1];
z[31] = z[31] * z[34];
z[49] = prod_pow(abb[31], 2);
z[59] = (T(1) / T(2)) * z[49];
z[62] = -abb[38] + z[59];
z[65] = abb[34] + abb[35];
z[66] = abb[37] * (T(1) / T(2));
z[67] = abb[36] * (T(1) / T(2));
z[24] = z[24] + -z[29] + -z[31] + -z[62] + (T(-1) / T(2)) * z[65] + -z[66] + -z[67];
z[24] = abb[15] * z[24];
z[29] = abb[29] * z[57];
z[31] = abb[30] * z[34];
z[65] = abb[30] * abb[32];
z[29] = -z[29] + z[31] + z[65];
z[29] = abb[13] * z[29];
z[31] = z[49] + -z[63];
z[31] = abb[36] + (T(1) / T(2)) * z[31];
z[31] = abb[7] * z[31];
z[2] = abb[29] * z[2];
z[2] = -z[2] + z[31];
z[31] = abb[55] * (T(1) / T(2));
z[49] = abb[7] * z[31];
z[2] = (T(-1) / T(2)) * z[2] + z[24] + -z[29] + -z[49];
z[24] = abb[31] * abb[32];
z[49] = abb[33] * z[19];
z[49] = -abb[56] + z[49];
z[24] = abb[37] + -z[24] + -z[49] + z[59];
z[35] = z[24] * z[35];
z[59] = z[4] * z[9];
z[63] = -abb[30] + z[23];
z[63] = abb[32] * z[63];
z[63] = abb[35] + abb[55] + -z[40] + -z[59] + -z[63];
z[6] = z[6] * z[63];
z[68] = abb[30] * z[55];
z[62] = abb[34] + abb[36] + z[62] + z[68];
z[68] = abb[11] * z[62];
z[18] = abb[10] * z[18];
z[6] = z[2] + z[6] + -z[18] + z[35] + z[68];
z[35] = abb[28] + abb[31];
z[69] = abb[30] + (T(-1) / T(2)) * z[35] + z[50];
z[69] = abb[32] * z[69];
z[70] = z[36] + z[53];
z[57] = z[4] * z[57];
z[43] = abb[30] * z[43];
z[71] = -z[31] + z[67];
z[43] = z[43] + z[57] + -z[69] + (T(-1) / T(2)) * z[70] + z[71];
z[43] = abb[1] * z[43];
z[57] = z[8] * z[22];
z[57] = abb[56] * (T(1) / T(2)) + z[57];
z[28] = -abb[35] + -z[28] + z[51];
z[26] = z[26] + -z[35];
z[26] = abb[30] * z[26];
z[51] = abb[28] * (T(3) / T(2)) + -z[1];
z[69] = abb[32] * (T(-5) / T(2)) + z[51];
z[69] = z[4] * z[69];
z[70] = abb[28] * (T(1) / T(4));
z[72] = abb[30] + z[70];
z[72] = abb[32] * z[72];
z[26] = z[26] + (T(1) / T(2)) * z[28] + z[31] + z[57] + z[69] + z[72];
z[26] = z[26] * z[37];
z[69] = prod_pow(abb[32], 2);
z[69] = z[49] + (T(1) / T(2)) * z[69];
z[72] = abb[2] * (T(1) / T(2));
z[69] = z[69] * z[72];
z[6] = (T(1) / T(2)) * z[6] + z[26] + -z[43] + -z[69];
z[6] = abb[51] * z[6];
z[26] = -abb[36] + abb[55];
z[30] = z[26] + z[30];
z[10] = -z[8] + z[10];
z[8] = z[8] * z[10];
z[8] = abb[56] + z[8];
z[70] = z[1] + -z[70];
z[72] = -z[50] + -z[70];
z[72] = abb[32] * z[72];
z[45] = -abb[28] + z[45];
z[45] = z[44] * z[45];
z[73] = abb[34] + -z[36];
z[45] = -z[8] + -z[30] + z[45] + z[72] + (T(1) / T(2)) * z[73];
z[45] = abb[50] * z[45];
z[72] = z[9] * z[50];
z[8] = -z[8] + (T(1) / T(2)) * z[61] + z[72];
z[72] = abb[35] + abb[36] + z[25];
z[73] = abb[28] + abb[32];
z[44] = z[44] * z[73];
z[44] = z[8] + z[44] + z[72];
z[44] = abb[49] * z[44];
z[9] = z[3] * z[9];
z[73] = z[4] * z[73];
z[73] = -z[61] + z[73];
z[10] = abb[33] * z[10];
z[74] = -z[9] + z[10] + -z[73];
z[74] = abb[56] + (T(1) / T(2)) * z[74];
z[74] = abb[53] * z[74];
z[28] = abb[55] + z[28] + -z[64];
z[50] = z[50] + -z[70];
z[50] = abb[29] * z[50];
z[8] = z[8] + -z[28] + z[50];
z[8] = abb[52] * z[8];
z[22] = abb[33] * z[22];
z[22] = abb[56] + z[22];
z[50] = abb[29] * abb[32];
z[64] = z[22] + -z[50];
z[47] = z[26] + -z[47] + z[64] + z[65];
z[65] = -abb[51] * z[47];
z[8] = z[8] + z[44] + z[45] + z[65] + z[74];
z[44] = -abb[52] + z[16];
z[45] = abb[50] * (T(-5) / T(24)) + abb[51] * (T(-1) / T(3)) + (T(1) / T(8)) * z[44];
z[45] = z[7] * z[45];
z[8] = (T(1) / T(2)) * z[8] + z[45];
z[8] = abb[0] * z[8];
z[24] = abb[4] * z[24];
z[45] = abb[14] * z[63];
z[24] = z[24] + z[45];
z[15] = z[15] * z[55];
z[45] = abb[31] * z[23];
z[15] = abb[35] + abb[38] * (T(-1) / T(2)) + -z[15] + (T(1) / T(4)) * z[33] + -z[45] + z[66];
z[15] = abb[9] * z[15];
z[15] = z[15] + (T(1) / T(2)) * z[18];
z[2] = (T(1) / T(2)) * z[2] + -z[15] + (T(1) / T(4)) * z[24] + -z[69];
z[2] = abb[52] * z[2];
z[4] = z[4] + -z[19] + -z[23];
z[4] = abb[33] * z[4];
z[14] = abb[31] * z[14];
z[18] = -abb[28] + z[55];
z[18] = abb[30] * z[18];
z[19] = abb[32] * z[1];
z[24] = -abb[32] + z[23];
z[24] = abb[29] * z[24];
z[4] = abb[55] + abb[56] * (T(3) / T(2)) + z[4] + z[14] + z[18] + z[19] + z[24];
z[4] = z[4] * z[37];
z[14] = abb[15] * z[62];
z[14] = z[14] + -z[68];
z[18] = -z[14] + -z[29];
z[4] = z[4] + (T(1) / T(2)) * z[18] + -z[43];
z[4] = abb[50] * z[4];
z[18] = z[49] + z[59];
z[19] = abb[31] * z[35];
z[19] = -abb[38] + z[19] + -z[36];
z[24] = -z[23] + z[55];
z[24] = abb[30] * z[24];
z[1] = z[1] + -z[3];
z[1] = z[1] * z[3];
z[1] = z[1] + -z[18] + (T(1) / T(2)) * z[19] + z[24] + z[31] + z[67];
z[1] = abb[52] * z[1];
z[19] = -abb[32] + z[51];
z[19] = abb[32] * z[19];
z[19] = -abb[34] + -z[18] + z[19] + -z[30] + -z[33];
z[19] = z[19] * z[42];
z[9] = z[9] + z[18] + z[61];
z[18] = z[9] * z[39];
z[1] = z[1] + z[18] + z[19];
z[18] = 7 * abb[52] + -5 * z[16];
z[18] = abb[51] + abb[50] * (T(5) / T(8)) + (T(1) / T(8)) * z[18];
z[18] = z[11] * z[18];
z[1] = (T(1) / T(2)) * z[1] + z[18];
z[1] = abb[6] * z[1];
z[18] = z[3] + -z[21];
z[18] = abb[30] * z[18];
z[19] = z[25] + -z[36];
z[3] = z[3] + z[34];
z[24] = -abb[29] * z[3];
z[18] = z[18] + (T(1) / T(2)) * z[19] + z[24] + -z[57] + z[71];
z[18] = abb[52] * z[18];
z[19] = -z[42] * z[47];
z[3] = -abb[32] * z[3];
z[3] = z[3] + -z[26] + z[40] + -z[53];
z[3] = abb[50] * z[3];
z[3] = z[3] + z[18] + z[19];
z[18] = abb[52] + -z[42];
z[18] = abb[50] * (T(1) / T(4)) + (T(1) / T(3)) * z[18];
z[18] = z[7] * z[18];
z[3] = (T(1) / T(2)) * z[3] + z[18];
z[3] = abb[8] * z[3];
z[18] = abb[16] + z[27];
z[18] = abb[54] * z[18];
z[18] = -z[18] + z[60];
z[17] = abb[45] + z[17];
z[19] = abb[26] * (T(1) / T(2));
z[17] = z[17] * z[19];
z[19] = abb[26] * abb[42];
z[24] = abb[19] * z[56];
z[25] = abb[18] * z[58];
z[17] = z[17] + (T(-1) / T(2)) * z[18] + z[19] + z[24] + z[25] + -z[41];
z[12] = z[12] * z[17];
z[17] = abb[32] * z[23];
z[11] = z[11] + z[17] + z[22] + -z[73];
z[17] = -abb[16] * z[11] * z[48];
z[18] = z[61] + z[72];
z[18] = -abb[3] * z[18] * z[46];
z[17] = z[17] + z[18];
z[11] = abb[20] * z[11];
z[9] = (T(5) / T(6)) * z[7] + -z[9];
z[9] = abb[17] * z[9];
z[9] = z[9] + z[11];
z[11] = abb[28] * abb[32];
z[11] = z[11] + z[64];
z[18] = z[7] + z[11];
z[18] = z[18] * z[32];
z[19] = abb[27] * abb[57];
z[9] = (T(1) / T(4)) * z[9] + z[18] + (T(-1) / T(2)) * z[19];
z[9] = abb[54] * z[9];
z[14] = (T(-1) / T(2)) * z[14] + -z[15];
z[14] = abb[49] * z[14];
z[15] = abb[2] * (T(1) / T(6));
z[18] = -z[15] * z[16];
z[15] = -abb[13] + abb[14] * (T(1) / T(6)) + z[15];
z[19] = abb[52] * z[15];
z[18] = z[18] + z[19];
z[19] = abb[1] * (T(1) / T(2));
z[15] = abb[5] * (T(7) / T(6)) + z[15] + z[19];
z[15] = z[15] * z[42];
z[19] = -abb[13] + abb[5] * (T(5) / T(6)) + z[19];
z[19] = z[19] * z[38];
z[23] = abb[52] * (T(1) / T(3)) + (T(-1) / T(4)) * z[16];
z[23] = abb[5] * z[23];
z[15] = z[15] + (T(1) / T(2)) * z[18] + z[19] + z[23];
z[15] = z[7] * z[15];
z[18] = -z[20] + -z[21];
z[18] = abb[29] * z[18];
z[19] = abb[28] * z[20];
z[18] = z[18] + z[19] + z[28];
z[18] = (T(1) / T(2)) * z[18] + z[22];
z[18] = abb[52] * z[18];
z[11] = -z[11] * z[16];
z[11] = (T(1) / T(2)) * z[11] + z[18];
z[11] = z[11] * z[37];
z[18] = abb[32] * z[54];
z[10] = -z[10] + z[18] + z[50];
z[10] = -abb[56] + (T(1) / T(2)) * z[10];
z[18] = abb[50] + abb[51] + -z[44];
z[10] = z[10] * z[18];
z[7] = z[7] * z[52];
z[7] = (T(7) / T(12)) * z[7] + z[10];
z[7] = abb[12] * z[7];
z[10] = z[16] * z[69];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + (T(1) / T(4)) * z[17];
z[1] = (T(1) / T(8)) * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_122_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.33544708924584890950927681887506465303997874222600823778300662167"),stof<T>("0.41738475196625233894669460176927356036780264854083947588076907006")}, std::complex<T>{stof<T>("-0.15141742748738769130413518055511426876063303160107946405028209885"),stof<T>("0.34744782487440725069106668990182744449854462167345904266326193174")}, std::complex<T>{stof<T>("-0.63588084814289589128506421583579554634173920945833963226935551111"),stof<T>("0.77423368543239799718476651588954293831550487200511627533601867233")}, std::complex<T>{stof<T>("-0.16474770483105002692789567841401842150461679795409241237926246268"),stof<T>("0.34744782487440725069106668990182744449854462167345904266326193174")}, std::complex<T>{stof<T>("0.28007517883941167000130541744931950050807039688426705566768705025"),stof<T>("-0.41738475196625233894669460176927356036780264854083947588076907006")}, std::complex<T>{stof<T>("0.22603409504498470222028439746846583230548262315657703361890322032"),stof<T>("-0.3091934776858435751821130762385186588467190235677404549136720148")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_122_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_122_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.16831371701021864110403270801378589945759898580185247280772667097"),stof<T>("0.09750852466673457076103761154255776833667972949689576507471239559")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_122_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_122_DLogXconstant_part(base_point<T>, kend);
	value += f_4_122_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_122_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_122_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_122_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_122_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_122_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_122_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
