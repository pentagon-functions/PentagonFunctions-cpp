/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_117.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_117_abbreviated (const std::array<T,60>& abb) {
T z[96];
z[0] = abb[34] * m1_set::bc<T>[0];
z[0] = abb[41] + z[0];
z[1] = abb[31] * m1_set::bc<T>[0];
z[1] = -z[0] + z[1];
z[1] = abb[5] * z[1];
z[2] = abb[33] * (T(1) / T(2));
z[3] = -abb[30] + z[2];
z[4] = abb[32] * (T(1) / T(2));
z[5] = z[3] + z[4];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = -abb[40] + z[5];
z[6] = abb[14] * z[5];
z[7] = abb[30] + -abb[31];
z[8] = m1_set::bc<T>[0] * z[7];
z[8] = abb[40] + z[8];
z[8] = abb[6] * z[8];
z[1] = z[1] + z[6] + z[8];
z[1] = (T(1) / T(2)) * z[1];
z[6] = -abb[29] + abb[32] * (T(3) / T(2));
z[8] = abb[30] * (T(1) / T(2));
z[9] = -abb[31] + z[2];
z[10] = -z[6] + -z[8] + -z[9];
z[10] = m1_set::bc<T>[0] * z[10];
z[11] = abb[40] * (T(1) / T(2));
z[12] = abb[34] * (T(1) / T(2));
z[13] = m1_set::bc<T>[0] * z[12];
z[13] = abb[41] * (T(1) / T(2)) + z[13];
z[14] = z[11] + -z[13];
z[10] = z[10] + -z[14];
z[10] = abb[8] * z[10];
z[15] = -abb[29] + abb[30] + -3 * abb[31] + z[2] + z[4];
z[16] = m1_set::bc<T>[0] * (T(1) / T(2));
z[15] = z[15] * z[16];
z[15] = z[0] + z[11] + z[15];
z[15] = abb[4] * z[15];
z[17] = abb[32] * m1_set::bc<T>[0];
z[17] = -z[0] + z[17];
z[17] = abb[2] * z[17];
z[18] = abb[18] + -abb[19];
z[19] = abb[42] * (T(1) / T(4));
z[20] = z[18] * z[19];
z[21] = -abb[29] + z[7];
z[22] = abb[32] + z[21];
z[22] = m1_set::bc<T>[0] * z[22];
z[22] = abb[40] + z[22];
z[23] = abb[7] * (T(1) / T(2));
z[24] = z[22] * z[23];
z[25] = abb[0] * m1_set::bc<T>[0];
z[26] = abb[33] * (T(1) / T(4));
z[27] = -abb[31] + z[26];
z[28] = abb[32] * (T(-3) / T(4)) + -z[27];
z[28] = z[25] * z[28];
z[10] = z[1] + z[10] + z[15] + z[17] + z[20] + z[24] + z[28];
z[15] = abb[54] * (T(1) / T(2));
z[10] = z[10] * z[15];
z[20] = abb[29] * (T(1) / T(2));
z[24] = z[7] + -z[20];
z[28] = abb[32] * (T(1) / T(4));
z[29] = z[24] + -z[26] + z[28];
z[29] = m1_set::bc<T>[0] * z[29];
z[29] = abb[40] + z[13] + z[29];
z[29] = abb[7] * z[29];
z[30] = -abb[17] + abb[20];
z[31] = -z[19] * z[30];
z[32] = z[4] + z[9];
z[33] = -z[20] + z[32];
z[33] = m1_set::bc<T>[0] * z[33];
z[33] = z[13] + z[33];
z[33] = abb[4] * z[33];
z[34] = abb[32] + abb[33];
z[34] = z[16] * z[34];
z[34] = -z[0] + z[34];
z[35] = abb[16] * z[34];
z[36] = abb[33] * (T(3) / T(4));
z[37] = abb[31] + -z[28] + -z[36];
z[25] = z[25] * z[37];
z[37] = -abb[30] + abb[33];
z[38] = m1_set::bc<T>[0] * z[37];
z[38] = -abb[40] + z[38];
z[38] = abb[1] * z[38];
z[39] = abb[29] + -abb[33];
z[40] = -abb[32] + z[39];
z[41] = abb[31] + z[40];
z[41] = m1_set::bc<T>[0] * z[41];
z[42] = abb[8] * z[41];
z[25] = z[25] + z[29] + z[31] + z[33] + (T(1) / T(2)) * z[35] + z[38] + z[42];
z[25] = abb[52] * z[25];
z[29] = z[2] + z[6];
z[29] = m1_set::bc<T>[0] * z[29];
z[29] = -z[0] + z[29];
z[31] = -abb[17] * z[29];
z[33] = -abb[20] * z[34];
z[35] = m1_set::bc<T>[0] * z[40];
z[35] = z[0] + z[35];
z[42] = abb[18] * z[35];
z[31] = z[31] + z[33] + z[42];
z[31] = abb[56] * z[31];
z[33] = -abb[44] + abb[47] + -abb[48] + -abb[49];
z[42] = -z[4] * z[33];
z[43] = -z[2] * z[33];
z[42] = z[42] + z[43];
z[44] = -abb[31] * z[33];
z[45] = z[42] + -z[44];
z[46] = abb[23] + abb[25];
z[45] = m1_set::bc<T>[0] * z[45] * z[46];
z[47] = -abb[30] * z[33];
z[42] = z[42] + -z[47];
z[42] = m1_set::bc<T>[0] * z[42];
z[48] = abb[40] * z[33];
z[42] = z[42] + z[48];
z[42] = abb[21] * z[42];
z[48] = z[0] * z[33];
z[49] = -abb[32] * z[33];
z[50] = -abb[33] * z[33];
z[51] = z[49] + z[50];
z[16] = z[16] * z[51];
z[16] = z[16] + z[48];
z[16] = abb[26] * z[16];
z[16] = z[16] + z[31] + z[42] + z[45];
z[31] = abb[16] * (T(1) / T(4));
z[42] = abb[28] + -z[31];
z[42] = abb[56] * z[42];
z[45] = -abb[27] * z[33];
z[42] = z[42] + (T(1) / T(4)) * z[45];
z[45] = -abb[42] * z[42];
z[16] = (T(1) / T(2)) * z[16] + z[25] + z[45];
z[25] = m1_set::bc<T>[0] * z[32] * z[46];
z[45] = abb[40] + z[0];
z[46] = abb[32] + z[37];
z[46] = m1_set::bc<T>[0] * z[46];
z[46] = -z[45] + z[46];
z[48] = abb[22] + abb[24];
z[51] = z[46] * z[48];
z[52] = abb[26] * z[34];
z[5] = abb[21] * z[5];
z[53] = abb[27] * (T(1) / T(2));
z[54] = abb[42] * z[53];
z[5] = z[5] + z[25] + z[51] + z[52] + -z[54];
z[25] = abb[45] + abb[46];
z[51] = abb[43] * (T(-1) / T(2)) + (T(-1) / T(4)) * z[25];
z[5] = z[5] * z[51];
z[51] = (T(1) / T(2)) * z[46];
z[52] = abb[8] * z[51];
z[41] = abb[12] * z[41];
z[1] = z[1] + z[38] + z[41] + -z[52];
z[38] = z[1] + z[17];
z[41] = abb[30] + abb[31];
z[52] = abb[33] * (T(5) / T(2)) + -z[41];
z[54] = -abb[29] + abb[32] * (T(5) / T(4));
z[52] = (T(1) / T(2)) * z[52] + z[54];
z[52] = m1_set::bc<T>[0] * z[52];
z[11] = -z[11] + -z[13] + z[52];
z[13] = abb[4] * (T(1) / T(2));
z[11] = z[11] * z[13];
z[52] = -abb[32] + (T(1) / T(2)) * z[39];
z[52] = m1_set::bc<T>[0] * z[52];
z[52] = z[0] + z[52];
z[52] = abb[13] * z[52];
z[34] = z[31] * z[34];
z[34] = z[34] + z[52];
z[52] = abb[17] + abb[20];
z[55] = abb[18] + abb[19];
z[56] = z[52] + z[55];
z[57] = abb[42] * (T(1) / T(8));
z[58] = z[56] * z[57];
z[11] = z[11] + z[34] + (T(1) / T(2)) * z[38] + -z[58];
z[38] = abb[51] * z[11];
z[57] = -z[52] * z[57];
z[58] = abb[4] * (T(1) / T(4));
z[35] = -z[35] * z[58];
z[17] = (T(1) / T(2)) * z[17] + z[34] + z[35] + z[57];
z[17] = abb[55] * z[17];
z[34] = z[2] + z[7];
z[35] = (T(1) / T(2)) * z[34] + z[54];
z[35] = m1_set::bc<T>[0] * z[35];
z[14] = z[14] + z[35];
z[35] = z[14] * z[23];
z[51] = -abb[0] * z[51];
z[11] = z[11] + z[35] + z[51];
z[11] = abb[53] * z[11];
z[35] = -z[19] * z[55];
z[6] = abb[33] * (T(3) / T(2)) + z[6] + -z[41];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = -abb[40] + z[6];
z[6] = z[6] * z[13];
z[1] = z[1] + z[6] + z[35];
z[6] = abb[50] * (T(1) / T(2));
z[1] = z[1] * z[6];
z[14] = abb[51] * z[14];
z[35] = abb[55] * (T(1) / T(2));
z[29] = z[29] * z[35];
z[22] = z[6] * z[22];
z[19] = abb[56] * z[19];
z[14] = z[14] + z[19] + z[22] + z[29];
z[14] = z[14] * z[23];
z[22] = -abb[51] * z[46];
z[29] = -3 * abb[32] + -abb[33];
z[29] = m1_set::bc<T>[0] * z[29];
z[0] = z[0] + (T(1) / T(4)) * z[29];
z[0] = abb[55] * z[0];
z[29] = -abb[30] + z[36];
z[36] = -z[28] + -z[29];
z[36] = m1_set::bc<T>[0] * z[36];
z[36] = abb[40] + z[36];
z[36] = abb[50] * z[36];
z[0] = z[0] + z[19] + z[22] + z[36];
z[19] = abb[0] * (T(1) / T(2));
z[0] = z[0] * z[19];
z[22] = z[47] + -z[50];
z[36] = z[22] + -z[49];
z[36] = m1_set::bc<T>[0] * z[36];
z[41] = -z[33] * z[45];
z[36] = z[36] + z[41];
z[41] = (T(-1) / T(4)) * z[48];
z[36] = z[36] * z[41];
z[0] = z[0] + z[1] + z[5] + z[10] + z[11] + z[14] + (T(1) / T(2)) * z[16] + z[17] + z[36] + z[38];
z[0] = (T(1) / T(8)) * z[0];
z[1] = z[20] * z[37];
z[5] = z[4] * z[21];
z[10] = z[2] * z[7];
z[11] = -abb[31] + abb[30] * (T(3) / T(4));
z[11] = abb[30] * z[11];
z[10] = -z[10] + z[11];
z[11] = -abb[32] + z[12];
z[14] = z[11] + -z[39];
z[16] = z[12] * z[14];
z[17] = prod_pow(abb[31], 2);
z[36] = abb[38] + z[17];
z[38] = abb[36] * (T(1) / T(2));
z[41] = abb[35] * (T(1) / T(2));
z[45] = abb[37] * (T(1) / T(2));
z[1] = z[1] + z[5] + z[10] + -z[16] + (T(1) / T(2)) * z[36] + z[38] + z[41] + z[45];
z[1] = abb[15] * z[1];
z[5] = abb[31] * abb[32];
z[46] = abb[34] * z[11];
z[51] = -abb[58] + z[46];
z[54] = (T(1) / T(2)) * z[17];
z[57] = abb[38] + z[54];
z[5] = z[5] + z[51] + -z[57];
z[5] = abb[5] * z[5];
z[59] = z[3] + z[20];
z[60] = abb[29] * z[59];
z[61] = abb[32] * z[59];
z[62] = prod_pow(m1_set::bc<T>[0], 2);
z[63] = (T(1) / T(3)) * z[62];
z[60] = abb[36] + z[60] + -z[61] + z[63];
z[64] = abb[57] + z[60];
z[65] = abb[14] * z[64];
z[66] = prod_pow(abb[30], 2);
z[67] = -z[17] + z[66];
z[68] = abb[33] * z[7];
z[67] = (T(1) / T(2)) * z[67] + -z[68];
z[69] = -abb[37] + -abb[57] + z[67];
z[69] = abb[6] * z[69];
z[5] = -z[5] + z[65] + z[69];
z[24] = abb[29] * z[24];
z[24] = -abb[57] + z[24];
z[65] = -abb[58] + z[57];
z[69] = abb[32] * z[37];
z[70] = z[24] + z[65] + z[69];
z[71] = abb[39] * (T(1) / T(2));
z[45] = z[45] + -z[71];
z[72] = z[12] * z[39];
z[70] = z[45] + -z[63] + (T(1) / T(2)) * z[70] + z[72];
z[73] = abb[8] * z[70];
z[74] = abb[30] * z[7];
z[74] = -z[68] + z[74];
z[37] = abb[29] * z[37];
z[75] = z[37] + z[74];
z[69] = z[62] + -z[69] + z[75];
z[76] = abb[12] * z[69];
z[73] = z[73] + -z[76];
z[76] = prod_pow(abb[32], 2);
z[76] = z[51] + (T(-1) / T(6)) * z[62] + (T(1) / T(2)) * z[76];
z[76] = abb[2] * z[76];
z[77] = -abb[31] + z[20];
z[77] = abb[29] * z[77];
z[78] = abb[35] + abb[37];
z[79] = -abb[36] + abb[39] + -z[57] + -z[77] + -z[78];
z[79] = abb[3] * z[79];
z[80] = (T(1) / T(2)) * z[79];
z[81] = abb[10] * abb[36];
z[81] = -z[76] + z[80] + z[81];
z[82] = -abb[31] + z[8];
z[82] = abb[30] * z[82];
z[54] = z[54] + z[82];
z[83] = z[54] + z[78];
z[83] = abb[11] * z[83];
z[84] = abb[10] + abb[11] + -abb[15];
z[84] = abb[39] * z[84];
z[1] = -z[1] + (T(1) / T(2)) * z[5] + z[73] + z[81] + z[83] + -z[84];
z[84] = abb[31] * (T(1) / T(2));
z[85] = abb[29] * (T(1) / T(4));
z[3] = z[3] + z[84] + z[85];
z[3] = abb[29] * z[3];
z[59] = -z[28] + z[59] + z[84];
z[59] = abb[32] * z[59];
z[86] = (T(1) / T(4)) * z[62];
z[87] = abb[57] * (T(1) / T(2));
z[3] = z[3] + -z[45] + -z[59] + (T(1) / T(2)) * z[74] + z[86] + z[87];
z[3] = abb[1] * z[3];
z[59] = z[4] * z[39];
z[74] = z[20] * z[39];
z[74] = abb[35] + z[74];
z[88] = z[63] + z[74];
z[89] = abb[34] * z[39];
z[89] = -abb[58] + z[89];
z[59] = z[59] + z[88] + -z[89];
z[31] = z[31] * z[59];
z[90] = z[3] + z[31];
z[84] = z[29] + z[84];
z[84] = abb[29] * z[84];
z[10] = abb[38] * (T(-1) / T(2)) + z[10] + -z[38] + z[84] + z[87];
z[84] = abb[58] * (T(1) / T(2));
z[91] = -z[72] + z[84];
z[92] = -abb[30] + abb[33] * (T(5) / T(4)) + -z[85];
z[92] = abb[32] * z[92];
z[92] = -z[10] + (T(-7) / T(6)) * z[62] + -z[71] + -z[91] + z[92];
z[92] = z[13] * z[92];
z[93] = z[4] + -z[39];
z[93] = z[4] * z[93];
z[94] = z[11] + z[39];
z[94] = z[12] * z[94];
z[94] = -abb[58] + z[94];
z[93] = (T(-7) / T(12)) * z[62] + z[93] + z[94];
z[93] = abb[13] * z[93];
z[95] = abb[59] * (T(1) / T(8));
z[56] = z[56] * z[95];
z[1] = (T(-1) / T(2)) * z[1] + -z[56] + -z[90] + z[92] + -z[93];
z[56] = -abb[51] * z[1];
z[68] = (T(1) / T(2)) * z[66] + -z[68];
z[92] = abb[29] * z[9];
z[36] = -z[36] + z[68] + z[92];
z[27] = z[27] + -z[85];
z[92] = -abb[32] * z[27];
z[36] = (T(1) / T(2)) * z[36] + -z[38] + z[51] + z[63] + z[71] + -z[87] + z[92];
z[36] = abb[4] * z[36];
z[14] = abb[34] * z[14];
z[38] = abb[32] * z[21];
z[37] = abb[36] + abb[38] + -z[14] + z[37] + z[38] + z[68] + -z[78];
z[37] = abb[15] * z[37];
z[5] = -z[5] + z[37];
z[5] = (T(1) / T(2)) * z[5];
z[37] = -z[24] + z[65];
z[8] = -abb[29] + z[8] + z[32];
z[8] = abb[32] * z[8];
z[32] = (T(5) / T(6)) * z[62];
z[8] = z[8] + -z[32] + (T(1) / T(2)) * z[37] + -z[45] + z[72];
z[8] = abb[8] * z[8];
z[37] = z[39] * z[85];
z[37] = z[37] + z[86];
z[16] = z[16] + z[41];
z[27] = z[27] + z[28];
z[27] = abb[32] * z[27];
z[27] = -z[16] + z[27] + -z[37] + z[57];
z[27] = abb[0] * z[27];
z[38] = z[4] + z[21];
z[38] = abb[32] * z[38];
z[65] = (T(1) / T(2)) * z[62];
z[24] = -abb[37] + abb[39] + -z[24] + z[38] + -z[65];
z[38] = -z[23] * z[24];
z[68] = -abb[10] * abb[39];
z[18] = abb[59] * z[18];
z[8] = -z[5] + z[8] + (T(-1) / T(4)) * z[18] + z[27] + z[36] + z[38] + z[68] + z[81];
z[8] = z[8] * z[15];
z[15] = abb[0] * z[70];
z[18] = -abb[32] + abb[29] * (T(3) / T(2)) + -z[34];
z[18] = abb[32] * z[18];
z[2] = z[2] + z[21];
z[2] = abb[29] * z[2];
z[2] = -abb[35] + abb[37] + -abb[57] + abb[58] + z[2] + z[18] + -z[46];
z[18] = abb[39] * (T(1) / T(4));
z[2] = (T(1) / T(4)) * z[2] + -z[18] + z[63];
z[21] = abb[7] * z[2];
z[1] = -z[1] + z[15] + z[21];
z[1] = abb[53] * z[1];
z[15] = z[7] + -z[26] + -z[85];
z[15] = abb[29] * z[15];
z[21] = abb[29] * (T(3) / T(4)) + z[26];
z[7] = -z[7] + z[21] + -z[28];
z[7] = abb[32] * z[7];
z[11] = z[11] * z[12];
z[7] = -abb[57] + z[7] + z[11] + z[15] + -z[41] + -z[54] + (T(1) / T(12)) * z[62] + -z[84];
z[7] = z[7] * z[23];
z[11] = -abb[8] * z[69];
z[15] = -abb[11] * abb[39];
z[11] = z[11] + z[15] + z[79] + z[83];
z[15] = -z[61] + z[65] + z[75] + -z[91];
z[13] = z[13] * z[15];
z[15] = z[29] + z[85];
z[23] = z[15] + -z[28];
z[23] = abb[32] * z[23];
z[21] = -abb[30] + z[21];
z[21] = abb[29] * z[21];
z[16] = -z[16] + -z[21] + z[23] + (T(-5) / T(12)) * z[62];
z[21] = -abb[37] + z[67];
z[23] = -abb[36] + z[16] + -z[21];
z[19] = z[19] * z[23];
z[23] = -abb[38] + -z[77] + z[82];
z[23] = -abb[36] + (T(1) / T(2)) * z[23] + z[71];
z[23] = abb[9] * z[23];
z[26] = z[30] * z[95];
z[7] = z[7] + (T(1) / T(2)) * z[11] + z[13] + z[19] + -z[23] + z[26] + z[90];
z[7] = abb[52] * z[7];
z[11] = -z[20] * z[33];
z[13] = z[11] + -z[43] + z[44];
z[13] = abb[32] * z[13];
z[19] = -abb[29] * z[33];
z[19] = z[19] + -z[50];
z[26] = -z[12] * z[33];
z[26] = -z[19] + z[26] + -z[49];
z[26] = abb[34] * z[26];
z[27] = abb[35] + -abb[38];
z[27] = -z[27] * z[33];
z[17] = -z[17] * z[33];
z[29] = (T(1) / T(2)) * z[17];
z[30] = -z[33] * z[63];
z[34] = z[19] * z[20];
z[13] = z[13] + z[26] + z[27] + -z[29] + z[30] + z[34];
z[13] = abb[25] * z[13];
z[26] = z[4] * z[40];
z[26] = -z[26] + -z[32] + z[51] + z[74];
z[27] = abb[17] * z[26];
z[32] = -abb[20] * z[59];
z[34] = abb[32] * z[39];
z[34] = z[34] + z[62] + -z[89];
z[36] = -abb[18] * z[34];
z[27] = z[27] + z[32] + z[36];
z[27] = abb[56] * z[27];
z[11] = z[11] + -z[47];
z[32] = z[11] + z[43];
z[36] = -abb[29] + abb[32];
z[32] = z[32] * z[36];
z[32] = -z[30] + z[32];
z[36] = -z[33] * z[66];
z[17] = -z[17] + z[36];
z[36] = abb[36] + -abb[37];
z[36] = -z[33] * z[36];
z[38] = z[44] + -z[47];
z[38] = abb[33] * z[38];
z[17] = (T(1) / T(2)) * z[17] + -z[32] + z[36] + z[38];
z[17] = abb[23] * z[17];
z[4] = -abb[34] + z[4] + z[20];
z[4] = z[4] * z[19];
z[36] = abb[35] + abb[58];
z[36] = -z[33] * z[36];
z[4] = z[4] + z[30] + z[36];
z[4] = abb[26] * z[4];
z[36] = abb[36] + abb[57];
z[38] = -z[33] * z[36];
z[32] = -z[32] + z[38];
z[32] = abb[21] * z[32];
z[4] = z[4] + z[13] + z[17] + z[27] + z[32];
z[13] = abb[59] * z[42];
z[4] = (T(1) / T(2)) * z[4] + z[13];
z[9] = z[9] + -z[20];
z[9] = abb[32] * z[9];
z[9] = z[9] + -z[14] + z[57] + -z[88];
z[9] = abb[25] * z[9];
z[13] = z[21] + z[60];
z[13] = abb[23] * z[13];
z[14] = abb[26] * z[59];
z[17] = abb[21] * z[64];
z[20] = abb[59] * z[53];
z[9] = z[9] + -z[13] + -z[14] + -z[17] + -z[20];
z[13] = z[48] * z[70];
z[9] = (T(1) / T(2)) * z[9] + z[13];
z[13] = abb[43] + (T(1) / T(2)) * z[25];
z[9] = z[9] * z[13];
z[13] = z[34] * z[58];
z[14] = z[52] * z[95];
z[13] = z[13] + z[14] + z[31] + (T(-1) / T(2)) * z[76] + z[93];
z[13] = abb[55] * z[13];
z[5] = -z[5] + z[73] + z[80];
z[14] = -abb[32] * z[15];
z[10] = z[10] + z[14];
z[10] = (T(1) / T(2)) * z[10] + z[18] + z[63];
z[10] = abb[4] * z[10];
z[14] = z[55] * z[95];
z[3] = z[3] + (T(1) / T(2)) * z[5] + z[10] + z[14] + -z[23];
z[3] = abb[50] * z[3];
z[5] = abb[51] * z[70];
z[10] = -z[28] * z[40];
z[10] = z[10] + -z[37] + -z[41] + z[94];
z[10] = z[10] * z[35];
z[14] = z[16] + -z[36];
z[6] = z[6] * z[14];
z[14] = abb[56] * z[95];
z[5] = z[5] + z[6] + z[10] + -z[14];
z[5] = abb[0] * z[5];
z[6] = -abb[38] + abb[57] + abb[58];
z[6] = -z[6] * z[33];
z[10] = z[11] + z[44];
z[10] = abb[29] * z[10];
z[11] = abb[32] * z[22];
z[6] = z[6] + z[10] + z[11] + -z[29];
z[10] = z[33] * z[45];
z[11] = z[12] * z[19];
z[6] = (T(1) / T(2)) * z[6] + z[10] + -z[11] + z[30];
z[10] = (T(1) / T(2)) * z[48];
z[6] = z[6] * z[10];
z[10] = abb[55] * z[26];
z[11] = abb[50] * z[24];
z[10] = z[10] + z[11];
z[2] = abb[51] * z[2];
z[2] = z[2] + (T(-1) / T(4)) * z[10] + -z[14];
z[2] = abb[7] * z[2];
z[1] = z[1] + z[2] + z[3] + (T(1) / T(2)) * z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[13] + z[56];
z[1] = (T(1) / T(8)) * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_117_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.30043375889704698177578739696073089330176046723233139448634888944"),stof<T>("0.35684893346614565823807191412026937794770222346427679945524960227")}, std::complex<T>{stof<T>("-0.63588084814289589128506421583579554634173920945833963226935551111"),stof<T>("0.77423368543239799718476651588954293831550487200511627533601867233")}, std::complex<T>{stof<T>("-0.47113314331184586435716853742177712483712241150424721989009304843"),stof<T>("0.42678586055799074649369982598771549381696025033165723267275674059")}, std::complex<T>{stof<T>("-0.63588084814289589128506421583579554634173920945833963226935551111"),stof<T>("0.77423368543239799718476651588954293831550487200511627533601867233")}, std::complex<T>{stof<T>("-0.48446342065550819998092903528068127758110617785726016821907341226"),stof<T>("0.42678586055799074649369982598771549381696025033165723267275674059")}, std::complex<T>{stof<T>("-0.28007517883941167000130541744931950050807039688426705566768705025"),stof<T>("0.41738475196625233894669460176927356036780264854083947588076907006")}, std::complex<T>{stof<T>("-0.22603409504498470222028439746846583230548262315657703361890322032"),stof<T>("0.3091934776858435751821130762385186588467190235677404549136720148")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_117_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_117_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.16831371701021864110403270801378589945759898580185247280772667097"),stof<T>("-0.09750852466673457076103761154255776833667972949689576507471239559")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_117_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_117_DLogXconstant_part(base_point<T>, kend);
	value += f_4_117_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_117_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_117_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_117_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_117_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_117_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_117_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
