/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_654.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_654_abbreviated (const std::array<T,71>& abb) {
T z[143];
z[0] = abb[23] + abb[25];
z[1] = -abb[27] + z[0];
z[2] = abb[24] + abb[26];
z[3] = z[1] + z[2];
z[4] = abb[35] * z[3];
z[5] = abb[31] * z[2];
z[6] = abb[33] * z[1];
z[7] = (T(1) / T(2)) * z[2];
z[8] = abb[32] * z[7];
z[9] = abb[27] * abb[34];
z[4] = -z[4] + z[5] + z[6] + z[8] + z[9];
z[4] = abb[32] * z[4];
z[5] = abb[31] + -abb[33];
z[6] = abb[22] + z[2];
z[8] = abb[27] + z[6];
z[10] = z[5] * z[8];
z[11] = abb[27] + z[7];
z[11] = abb[36] * z[11];
z[12] = abb[27] + z[2];
z[13] = abb[32] * z[12];
z[9] = z[9] + z[10] + -z[11] + z[13];
z[9] = abb[36] * z[9];
z[10] = abb[33] + -abb[34];
z[11] = abb[28] + z[6];
z[13] = (T(1) / T(2)) * z[11];
z[14] = abb[27] + z[13];
z[10] = z[10] * z[14];
z[14] = abb[22] + -abb[28];
z[15] = abb[31] * (T(1) / T(2));
z[14] = z[14] * z[15];
z[15] = abb[27] + abb[28];
z[15] = abb[35] * z[15];
z[10] = z[10] + -z[14] + -z[15];
z[10] = abb[31] * z[10];
z[11] = (T(5) / T(2)) * z[0] + z[11];
z[14] = prod_pow(m1_set::bc<T>[0], 2);
z[16] = (T(1) / T(3)) * z[14];
z[11] = z[11] * z[16];
z[16] = -abb[28] + z[6];
z[17] = abb[34] * (T(1) / T(2));
z[16] = z[16] * z[17];
z[17] = abb[35] * z[1];
z[17] = z[16] + z[17];
z[17] = abb[33] * z[17];
z[7] = z[1] + z[7];
z[18] = prod_pow(abb[35], 2);
z[7] = z[7] * z[18];
z[19] = abb[65] * z[3];
z[20] = abb[28] + z[12];
z[20] = abb[37] * z[20];
z[21] = abb[40] * z[2];
z[6] = abb[39] * z[6];
z[12] = abb[38] * z[12];
z[22] = abb[22] + abb[27];
z[23] = abb[64] * z[22];
z[15] = abb[34] * z[15];
z[24] = abb[29] * (T(1) / T(2));
z[25] = abb[67] * z[24];
z[26] = abb[28] + z[0];
z[27] = abb[66] * z[26];
z[4] = z[4] + -z[6] + z[7] + -z[9] + -z[10] + -z[11] + z[12] + -z[15] + -z[17] + -z[19] + -z[20] + z[21] + -z[23] + -z[25] + -z[27];
z[6] = abb[62] + abb[63];
z[4] = -z[4] * z[6];
z[7] = 2 * abb[52];
z[9] = 3 * abb[51];
z[10] = z[7] + -z[9];
z[11] = 5 * abb[53];
z[12] = 4 * abb[49];
z[15] = abb[56] + -abb[57];
z[17] = abb[50] + z[15];
z[19] = -z[10] + z[11] + z[12] + z[17];
z[19] = abb[17] * z[19];
z[20] = abb[50] + abb[56];
z[21] = abb[48] + z[20];
z[23] = 2 * abb[51];
z[25] = z[21] + -z[23];
z[27] = 2 * abb[53];
z[28] = -z[12] + z[25] + -z[27];
z[28] = abb[16] * z[28];
z[29] = -abb[60] + abb[61];
z[30] = abb[21] * z[29];
z[31] = z[28] + z[30];
z[32] = 3 * abb[50];
z[33] = abb[56] + z[32];
z[34] = -abb[57] + z[27];
z[35] = 2 * z[34];
z[36] = -abb[48] + 10 * abb[49] + z[23] + z[33] + z[35];
z[36] = abb[7] * z[36];
z[37] = 8 * abb[49];
z[38] = 2 * abb[57];
z[11] = z[11] + z[37] + -z[38];
z[39] = abb[48] + 5 * abb[51] + 2 * z[11];
z[39] = abb[1] * z[39];
z[40] = 5 * abb[49];
z[7] = -z[7] + z[23] + -z[40];
z[7] = abb[9] * z[7];
z[41] = abb[51] + abb[52];
z[42] = abb[53] + -abb[57];
z[43] = abb[49] + z[42];
z[44] = 2 * z[43];
z[45] = z[41] + z[44];
z[45] = abb[5] * z[45];
z[46] = 5 * abb[9];
z[47] = 5 * abb[17];
z[48] = -abb[1] + z[46] + -z[47];
z[48] = abb[58] * z[48];
z[49] = abb[1] + -abb[17];
z[50] = abb[7] + z[49];
z[51] = 6 * abb[59];
z[52] = -z[50] * z[51];
z[53] = abb[5] + -abb[17];
z[54] = -abb[1] + z[53];
z[46] = -z[46] + -2 * z[54];
z[46] = abb[55] * z[46];
z[55] = 2 * abb[9];
z[56] = abb[5] + z[55];
z[57] = -z[49] + z[56];
z[58] = 2 * abb[54];
z[57] = z[57] * z[58];
z[59] = abb[49] + abb[55];
z[60] = abb[48] + z[42];
z[61] = -abb[58] + z[59] + -z[60];
z[61] = abb[0] * z[61];
z[62] = abb[17] * abb[48];
z[63] = abb[18] * z[29];
z[7] = z[7] + -z[19] + z[31] + z[36] + z[39] + z[45] + z[46] + z[48] + z[52] + z[57] + z[61] + -z[62] + -z[63];
z[7] = abb[37] * z[7];
z[36] = abb[20] * z[29];
z[39] = z[30] + z[36];
z[45] = abb[49] + abb[52];
z[46] = abb[13] * z[45];
z[48] = abb[48] + abb[52];
z[52] = -abb[58] + z[48];
z[52] = abb[10] * z[52];
z[57] = abb[48] + abb[50];
z[61] = -abb[56] + z[57];
z[64] = abb[15] * z[61];
z[65] = 2 * abb[5];
z[66] = -abb[4] + z[65];
z[67] = abb[1] + -abb[7] + -z[66];
z[67] = abb[59] * z[67];
z[28] = -z[28] + -z[39] + -z[46] + z[52] + -z[64] + -z[67];
z[67] = abb[51] * (T(3) / T(4));
z[68] = abb[54] * (T(3) / T(2));
z[69] = 2 * z[42];
z[70] = -5 * abb[56] + abb[49] * (T(13) / T(2)) + z[69];
z[70] = abb[48] * (T(-3) / T(4)) + z[67] + z[68] + (T(1) / T(3)) * z[70];
z[70] = abb[1] * z[70];
z[71] = abb[51] + z[34];
z[72] = abb[49] + z[71];
z[73] = abb[48] * (T(3) / T(2));
z[74] = abb[56] * (T(1) / T(2));
z[72] = abb[50] * (T(1) / T(6)) + (T(2) / T(3)) * z[72] + z[73] + -z[74];
z[72] = abb[7] * z[72];
z[75] = 2 * abb[49];
z[76] = z[42] + z[75];
z[77] = abb[50] + z[76];
z[77] = abb[48] * (T(1) / T(4)) + (T(2) / T(3)) * z[77];
z[77] = abb[5] * z[77];
z[78] = z[74] + z[76];
z[79] = abb[50] * (T(1) / T(2));
z[78] = abb[48] * (T(-5) / T(6)) + (T(1) / T(3)) * z[78] + -z[79];
z[78] = abb[4] * z[78];
z[80] = abb[49] + z[79];
z[81] = abb[56] + -z[42];
z[81] = -z[80] + (T(1) / T(2)) * z[81];
z[81] = abb[8] * z[81];
z[82] = z[27] + z[75];
z[83] = 2 * abb[56];
z[84] = -abb[50] + abb[57] + z[82] + z[83];
z[68] = abb[58] * (T(-7) / T(12)) + abb[48] * (T(-1) / T(12)) + abb[51] * (T(1) / T(4)) + -z[68] + (T(1) / T(3)) * z[84];
z[68] = abb[0] * z[68];
z[84] = abb[48] + -abb[51];
z[85] = abb[56] + abb[57];
z[85] = abb[53] + abb[50] * (T(-7) / T(3)) + (T(-1) / T(3)) * z[85];
z[85] = abb[59] + (T(-1) / T(3)) * z[84] + (T(1) / T(2)) * z[85];
z[85] = abb[3] * z[85];
z[86] = abb[2] * abb[48];
z[87] = (T(1) / T(2)) * z[63];
z[88] = abb[19] * z[29];
z[89] = -abb[2] + abb[5];
z[89] = (T(1) / T(4)) * z[89];
z[90] = abb[13] * (T(-1) / T(3)) + abb[1] * (T(7) / T(12)) + -z[89];
z[90] = abb[58] * z[90];
z[89] = abb[55] * z[89];
z[28] = (T(-1) / T(3)) * z[28] + z[68] + z[70] + z[72] + z[77] + z[78] + (T(5) / T(3)) * z[81] + z[85] + (T(-1) / T(4)) * z[86] + z[87] + (T(5) / T(6)) * z[88] + z[89] + z[90];
z[14] = z[14] * z[28];
z[28] = abb[48] + abb[49];
z[68] = 3 * abb[53];
z[70] = -abb[52] + z[9] + -z[15] + z[28] + -z[32] + z[68];
z[72] = abb[5] * (T(1) / T(2));
z[70] = z[70] * z[72];
z[77] = abb[51] * (T(5) / T(2));
z[78] = abb[48] * (T(1) / T(2));
z[34] = 3 * z[34] + z[37] + z[77] + z[78] + z[83];
z[34] = abb[1] * z[34];
z[37] = z[74] + z[79];
z[81] = z[37] + z[78];
z[35] = 6 * abb[49] + z[35];
z[85] = z[23] + z[35] + z[81];
z[85] = abb[7] * z[85];
z[89] = 2 * abb[50];
z[90] = abb[56] + z[89];
z[40] = -abb[51] + -z[40] + -z[69] + -z[90];
z[40] = abb[9] * z[40];
z[69] = abb[56] + z[42];
z[80] = (T(1) / T(2)) * z[69] + z[80];
z[80] = abb[4] * z[80];
z[80] = (T(1) / T(2)) * z[52] + z[80];
z[91] = z[20] + z[76];
z[92] = abb[17] * z[91];
z[93] = abb[14] * z[44];
z[94] = z[92] + z[93];
z[95] = abb[50] + -abb[56];
z[96] = z[76] + z[95];
z[97] = abb[8] * z[96];
z[98] = 3 * abb[7];
z[99] = 2 * abb[17];
z[100] = z[98] + -z[99];
z[101] = 3 * abb[9];
z[102] = abb[4] + z[101];
z[103] = abb[5] + z[102];
z[104] = -4 * abb[1] + -z[100] + z[103];
z[104] = abb[59] * z[104];
z[105] = abb[1] + -abb[13];
z[106] = abb[9] + (T(-1) / T(2)) * z[105];
z[106] = abb[58] * z[106];
z[107] = abb[1] + z[56];
z[108] = -abb[54] * z[107];
z[34] = z[34] + z[40] + (T(-1) / T(2)) * z[46] + z[70] + -z[80] + z[85] + -z[87] + -z[94] + z[97] + z[104] + z[106] + z[108];
z[34] = z[18] * z[34];
z[40] = -abb[53] + z[15];
z[70] = abb[51] * (T(3) / T(2)) + -z[40];
z[85] = -z[32] + -z[45] + z[70] + z[78];
z[85] = abb[5] * z[85];
z[104] = -abb[57] + z[68];
z[23] = z[23] + z[104];
z[106] = -abb[56] + z[23];
z[108] = -z[32] + z[75] + z[106];
z[109] = abb[7] * z[108];
z[110] = 4 * abb[9];
z[111] = 2 * abb[1];
z[112] = z[53] + z[110] + -z[111];
z[112] = abb[55] * z[112];
z[113] = abb[7] + -z[99] + z[103];
z[114] = 2 * abb[59];
z[113] = z[113] * z[114];
z[115] = abb[4] * z[91];
z[116] = z[43] + z[90];
z[117] = -abb[51] + abb[52];
z[118] = 2 * z[116] + -z[117];
z[118] = abb[9] * z[118];
z[119] = 3 * abb[49];
z[77] = -abb[52] + 4 * abb[53] + z[77] + z[119];
z[77] = abb[17] * z[77];
z[120] = z[44] + -z[84];
z[120] = abb[1] * z[120];
z[121] = abb[5] + 7 * abb[17];
z[121] = abb[1] + -z[110] + (T(1) / T(2)) * z[121];
z[121] = abb[58] * z[121];
z[122] = -3 * abb[17] + -z[56] + z[111];
z[122] = abb[54] * z[122];
z[77] = -z[52] + (T(3) / T(2)) * z[62] + z[77] + z[85] + z[109] + z[112] + z[113] + -z[115] + -z[118] + z[120] + z[121] + z[122];
z[77] = abb[40] * z[77];
z[85] = abb[52] * (T(1) / T(2));
z[109] = abb[51] * (T(1) / T(2));
z[112] = z[85] + -z[109] + -z[116];
z[112] = abb[9] * z[112];
z[116] = 7 * abb[50];
z[104] = abb[56] + -z[104] + z[116];
z[104] = z[84] + (T(1) / T(2)) * z[104];
z[104] = abb[7] * z[104];
z[104] = z[104] + -z[112];
z[112] = abb[1] + abb[13];
z[112] = 4 * abb[5] + z[47] + -z[55] + (T(-3) / T(2)) * z[112];
z[112] = abb[58] * z[112];
z[98] = -4 * abb[17] + z[98] + z[103] + z[111];
z[98] = abb[59] * z[98];
z[120] = z[78] + z[109];
z[121] = abb[58] * (T(1) / T(2));
z[122] = abb[54] + z[121];
z[123] = abb[49] + abb[53];
z[124] = -z[120] + z[122] + -z[123];
z[124] = abb[0] * z[124];
z[27] = abb[49] + z[27];
z[27] = -z[10] + 2 * z[27];
z[125] = abb[17] * z[27];
z[70] = -z[48] + z[70];
z[126] = -7 * abb[49] + -z[32] + z[70];
z[126] = z[72] * z[126];
z[127] = -abb[57] + -z[120];
z[127] = abb[1] * z[127];
z[128] = abb[5] * (T(-13) / T(4)) + z[55] + -z[99];
z[128] = abb[55] * z[128];
z[129] = abb[9] + z[72];
z[130] = abb[1] + -z[99] + -z[129];
z[130] = abb[54] * z[130];
z[80] = (T(3) / T(2)) * z[46] + z[62] + -z[80] + z[98] + -z[104] + z[112] + z[124] + z[125] + z[126] + z[127] + z[128] + z[130];
z[80] = abb[32] * z[80];
z[98] = abb[9] + z[53];
z[112] = 2 * abb[55];
z[98] = z[98] * z[112];
z[124] = -abb[52] + z[90];
z[124] = z[55] * z[124];
z[98] = z[98] + -z[124];
z[124] = abb[48] + abb[51];
z[125] = 2 * z[15] + -z[124];
z[125] = abb[1] * z[125];
z[125] = -z[52] + z[97] + z[125];
z[126] = z[49] * z[58];
z[126] = z[125] + z[126];
z[10] = z[10] + z[17] + -z[68];
z[10] = abb[17] * z[10];
z[127] = z[42] + z[119];
z[128] = abb[52] + z[33] + z[127];
z[128] = abb[5] * z[128];
z[47] = abb[5] + z[47] + -z[110];
z[130] = -z[47] + -z[105];
z[130] = abb[58] * z[130];
z[54] = -z[54] + -z[102];
z[54] = z[54] * z[114];
z[10] = z[10] + -z[46] + z[54] + -z[62] + -z[98] + z[115] + -z[126] + z[128] + z[130];
z[10] = abb[35] * z[10];
z[54] = -z[33] + -z[48] + -z[76];
z[54] = abb[5] * z[54];
z[115] = -abb[58] + z[124];
z[128] = -z[58] + z[115];
z[130] = 2 * z[123];
z[131] = z[128] + z[130];
z[131] = abb[0] * z[131];
z[27] = z[27] + -z[58];
z[27] = abb[17] * z[27];
z[47] = abb[58] * z[47];
z[47] = z[47] + z[62];
z[132] = abb[4] * z[21];
z[133] = -z[88] + z[132];
z[134] = abb[7] * z[21];
z[134] = -z[63] + z[134];
z[27] = z[27] + z[47] + z[54] + z[98] + z[113] + -z[131] + -z[133] + -z[134];
z[27] = abb[31] * z[27];
z[54] = abb[48] + z[75];
z[98] = z[23] + z[54] + -z[89];
z[113] = abb[0] * z[98];
z[135] = 2 * abb[48];
z[106] = z[106] + -z[116] + -z[135];
z[136] = z[51] + z[106];
z[136] = abb[3] * z[136];
z[137] = -z[92] + z[136];
z[28] = z[28] + z[89];
z[28] = z[28] * z[65];
z[138] = 2 * abb[7];
z[139] = -abb[17] + z[138];
z[66] = z[66] + z[139];
z[66] = z[66] * z[114];
z[140] = 4 * abb[7];
z[141] = z[57] * z[140];
z[28] = z[28] + -z[66] + z[113] + -z[133] + z[137] + z[141];
z[28] = abb[34] * z[28];
z[66] = -z[92] + z[134];
z[50] = z[50] * z[114];
z[113] = z[50] + -z[66];
z[134] = abb[56] * z[111];
z[134] = -z[97] + z[113] + -z[134];
z[141] = -abb[48] + z[76];
z[142] = abb[0] * z[141];
z[142] = -z[134] + z[142];
z[142] = abb[33] * z[142];
z[10] = z[10] + z[27] + -z[28] + z[80] + z[142];
z[10] = abb[32] * z[10];
z[27] = z[89] + -z[114];
z[80] = abb[49] + (T(1) / T(2)) * z[42];
z[89] = 2 * abb[58];
z[142] = z[27] + z[78] + -z[80] + z[89] + -z[112];
z[142] = abb[0] * z[142];
z[40] = abb[50] * (T(-7) / T(2)) + (T(-1) / T(2)) * z[40] + z[67] + -z[75] + -z[85];
z[40] = abb[5] * z[40];
z[67] = -abb[4] + 3 * abb[5] + z[100] + z[101];
z[67] = abb[59] * z[67];
z[85] = abb[2] + abb[5] * (T(1) / T(4));
z[85] = z[55] + 3 * z[85];
z[85] = abb[55] * z[85];
z[100] = (T(1) / T(2)) * z[88];
z[101] = -3 * abb[2] + -z[55];
z[101] = abb[58] * z[101];
z[129] = -abb[54] * z[129];
z[40] = z[40] + -z[52] + z[67] + z[85] + 3 * z[86] + -z[100] + z[101] + -z[104] + z[129] + (T(1) / T(2)) * z[132] + -z[137] + z[142];
z[40] = abb[36] * z[40];
z[67] = z[72] + -z[99] + z[110];
z[67] = abb[55] * z[67];
z[70] = -z[12] + z[70] + -z[116];
z[70] = abb[5] * z[70];
z[72] = abb[7] * z[106];
z[19] = z[19] + z[47] + z[67] + z[70] + z[72] + -z[118];
z[47] = -abb[50] + z[123];
z[47] = 2 * z[47] + z[112];
z[67] = z[47] + z[128];
z[67] = abb[0] * z[67];
z[53] = abb[7] + z[53];
z[70] = abb[9] + z[53];
z[51] = -z[51] * z[70];
z[72] = z[56] + z[99];
z[85] = abb[54] * z[72];
z[51] = -z[19] + z[51] + z[67] + z[85] + z[136];
z[51] = abb[32] * z[51];
z[67] = z[83] + z[141];
z[85] = z[67] + -z[114];
z[85] = abb[0] * z[85];
z[86] = abb[5] * z[91];
z[53] = z[53] * z[114];
z[91] = z[36] + z[64];
z[53] = -z[53] + z[66] + z[85] + z[86] + z[91];
z[5] = z[5] * z[53];
z[5] = z[5] + z[28] + z[40] + z[51];
z[5] = abb[36] * z[5];
z[28] = abb[51] + abb[53];
z[40] = z[28] + z[75];
z[51] = z[40] + -z[81];
z[51] = abb[16] * z[51];
z[64] = (T(1) / T(2)) * z[64];
z[39] = (T(1) / T(2)) * z[39] + z[64];
z[66] = z[39] + -z[51];
z[85] = -abb[5] * z[98];
z[9] = -z[9] + -z[11] + -z[81];
z[9] = abb[7] * z[9];
z[11] = -z[102] + z[111] + z[139];
z[11] = z[11] * z[114];
z[86] = z[90] + z[127];
z[86] = z[55] * z[86];
z[98] = z[71] + z[119];
z[101] = abb[1] * z[98];
z[102] = -z[76] + z[78];
z[104] = abb[56] * (T(3) / T(2)) + z[79] + -z[102];
z[104] = abb[4] * z[104];
z[40] = -abb[56] + -z[40] + z[114];
z[40] = abb[0] * z[40];
z[106] = (T(3) / T(2)) * z[63];
z[9] = z[9] + z[11] + z[40] + -z[66] + z[85] + z[86] + z[92] + z[100] + -4 * z[101] + z[104] + z[106];
z[9] = abb[34] * z[9];
z[11] = z[98] * z[111];
z[40] = -z[11] + z[51];
z[33] = -z[33] + z[41] + z[42];
z[33] = abb[5] * z[33];
z[30] = z[30] + -z[36];
z[33] = z[30] + z[33];
z[36] = -abb[48] + z[12] + z[71];
z[36] = abb[7] * z[36];
z[41] = -abb[7] + z[103] + -z[111];
z[41] = abb[59] * z[41];
z[42] = -abb[49] + abb[51] + -abb[58] + -z[90];
z[42] = abb[9] * z[42];
z[51] = -abb[4] * abb[56];
z[85] = abb[54] * z[56];
z[90] = -abb[56] + abb[58] + -z[43];
z[90] = abb[0] * z[90];
z[33] = (T(1) / T(2)) * z[33] + z[36] + -z[40] + z[41] + z[42] + z[51] + z[52] + -z[64] + z[85] + z[90];
z[33] = abb[31] * z[33];
z[36] = abb[5] * z[108];
z[41] = 3 * abb[1] + -z[103] + z[139];
z[41] = z[41] * z[114];
z[42] = z[98] * z[140];
z[36] = z[31] + z[36] + -z[41] + z[42] + -z[86] + -z[94] + 6 * z[101] + -z[133];
z[36] = abb[35] * z[36];
z[41] = abb[0] * abb[35];
z[42] = z[41] * z[60];
z[36] = z[36] + -z[42];
z[42] = z[74] + -z[79];
z[51] = z[42] + -z[73] + z[76];
z[51] = abb[4] * z[51];
z[51] = z[51] + (T(3) / T(2)) * z[88];
z[40] = -z[40] + z[51];
z[50] = z[50] + z[94];
z[60] = -abb[51] + -z[12] + z[38] + -z[68];
z[68] = -z[60] + z[81];
z[68] = abb[7] * z[68];
z[74] = abb[5] * z[141];
z[79] = abb[56] + -z[28];
z[85] = z[79] + -z[135];
z[85] = abb[0] * z[85];
z[39] = z[39] + z[40] + -z[50] + z[68] + z[74] + z[85] + z[87];
z[39] = abb[33] * z[39];
z[9] = z[9] + z[33] + -z[36] + z[39];
z[9] = abb[31] * z[9];
z[12] = -abb[48] + -z[12] + -z[23] + z[114];
z[12] = abb[0] * z[12];
z[23] = abb[1] + -abb[5];
z[23] = z[23] * z[114];
z[33] = abb[4] * z[141];
z[33] = z[33] + z[88];
z[39] = -z[57] * z[65];
z[57] = abb[48] + -abb[49];
z[65] = -z[57] * z[140];
z[12] = z[11] + z[12] + -z[23] + -z[33] + z[39] + z[65];
z[12] = abb[34] * z[12];
z[12] = z[12] + z[36];
z[12] = abb[34] * z[12];
z[36] = abb[56] + z[60];
z[36] = z[36] * z[111];
z[25] = -z[25] + z[35];
z[25] = abb[7] * z[25];
z[35] = abb[5] * z[96];
z[39] = abb[4] * z[61];
z[39] = z[39] + -z[88];
z[61] = abb[0] * z[44];
z[25] = z[25] + z[31] + z[35] + -z[36] + -z[39] + -z[61] + z[63] + -z[97];
z[35] = abb[66] * z[25];
z[19] = abb[38] * z[19];
z[36] = z[42] + z[102];
z[36] = abb[19] * z[36];
z[42] = abb[4] + abb[7] + -abb[15] + -abb[16];
z[61] = (T(1) / T(2)) * z[29];
z[42] = z[42] * z[61];
z[61] = abb[5] + 2 * abb[30];
z[29] = z[29] * z[61];
z[37] = -z[28] + z[37];
z[61] = -z[37] + z[73];
z[61] = abb[18] * z[61];
z[37] = z[37] + -z[75];
z[37] = abb[21] * z[37];
z[65] = abb[20] + abb[21];
z[65] = z[65] * z[78];
z[68] = abb[20] * z[95];
z[29] = z[29] + -z[36] + -z[37] + z[42] + -z[61] + -z[65] + (T(-1) / T(2)) * z[68];
z[36] = -abb[67] * z[29];
z[37] = abb[57] + abb[58] + z[28] + -z[83] + z[135];
z[42] = -abb[64] * z[37];
z[61] = abb[54] + z[80] + z[109] + -z[121];
z[61] = z[18] * z[61];
z[59] = -z[59] + z[120] + z[122];
z[59] = abb[40] * z[59];
z[20] = -abb[48] + z[20] + -z[28];
z[20] = 2 * z[20] + -z[114];
z[20] = abb[39] * z[20];
z[47] = -z[47] + -z[115];
z[47] = abb[38] * z[47];
z[65] = abb[38] + abb[64];
z[68] = z[58] * z[65];
z[20] = z[20] + z[42] + z[47] + z[59] + z[61] + z[68];
z[20] = abb[0] * z[20];
z[15] = -z[15] + -z[82] + z[114] + -z[124];
z[15] = abb[0] * z[15];
z[42] = z[60] + z[81];
z[42] = abb[7] * z[42];
z[42] = z[42] + z[93] + -z[106];
z[21] = abb[5] * z[21];
z[15] = z[15] + -z[21] + -z[23] + (T(1) / T(2)) * z[30] + z[40] + -z[42] + -z[64];
z[15] = abb[34] * z[15];
z[21] = abb[0] + -abb[7];
z[21] = z[21] * z[141];
z[23] = -z[43] * z[111];
z[21] = z[21] + z[23] + -z[63] + z[93];
z[21] = abb[33] * z[21];
z[23] = abb[35] * z[134];
z[30] = -z[41] * z[141];
z[21] = -z[15] + z[21] + z[23] + z[30];
z[21] = abb[33] * z[21];
z[23] = -abb[52] + -z[43] + z[95];
z[23] = abb[5] * z[23];
z[30] = abb[4] * z[96];
z[17] = z[17] + -z[28];
z[28] = abb[17] * z[17];
z[28] = z[28] + -z[62];
z[40] = -abb[5] + -abb[13] + z[49];
z[40] = abb[58] * z[40];
z[23] = z[23] + -z[28] + -z[30] + -z[40] + -z[46] + z[126];
z[30] = -abb[65] * z[23];
z[17] = abb[52] + z[17];
z[40] = -z[17] + -z[58] + z[89];
z[43] = abb[37] + -abb[40] + abb[65] + -z[65];
z[43] = z[40] * z[43];
z[47] = -prod_pow(abb[31], 2);
z[18] = z[18] + z[47];
z[17] = abb[54] + -abb[58] + (T(1) / T(2)) * z[17];
z[18] = z[17] * z[18];
z[47] = abb[31] + -abb[35];
z[47] = z[40] * z[47];
z[49] = abb[32] * z[17];
z[49] = -z[47] + z[49];
z[49] = abb[32] * z[49];
z[59] = abb[32] * z[40];
z[17] = abb[36] * z[17];
z[17] = z[17] + z[59];
z[17] = abb[36] * z[17];
z[17] = z[17] + z[18] + z[43] + z[49];
z[17] = abb[6] * z[17];
z[18] = -z[32] + z[69] + -z[135];
z[18] = abb[5] * z[18];
z[32] = -3 * abb[48] + abb[56] + -z[32];
z[32] = abb[7] * z[32];
z[18] = z[18] + z[32] + -z[63] + z[91] + z[133];
z[18] = abb[39] * z[18];
z[32] = z[39] + 2 * z[52] + -z[91];
z[39] = abb[5] + abb[17];
z[39] = abb[58] * z[39];
z[43] = abb[5] * z[48];
z[28] = z[28] + z[32] + -z[39] + z[43];
z[39] = -abb[64] * z[28];
z[43] = abb[38] + abb[39];
z[43] = -z[43] * z[136];
z[48] = -abb[4] + abb[5] + abb[7];
z[48] = abb[39] * z[48];
z[49] = abb[38] * z[70];
z[48] = z[48] + 3 * z[49];
z[48] = z[48] * z[114];
z[49] = -abb[38] * z[72];
z[59] = -abb[64] * z[99];
z[49] = z[49] + z[59];
z[49] = abb[54] * z[49];
z[27] = z[27] + z[79];
z[59] = -abb[70] * z[27];
z[4] = abb[68] + abb[69] + z[4] + z[5] + z[7] + z[9] + z[10] + z[12] + z[14] + z[17] + z[18] + z[19] + z[20] + z[21] + z[30] + z[34] + z[35] + z[36] + z[39] + z[43] + z[48] + z[49] + z[59] + z[77];
z[5] = z[33] + z[113];
z[7] = -z[38] + z[45] + -z[124];
z[7] = abb[5] * z[7];
z[9] = abb[49] + -z[83];
z[9] = 2 * z[9] + -z[84];
z[9] = abb[1] * z[9];
z[10] = abb[51] + z[76];
z[12] = z[10] * z[55];
z[14] = -z[55] + z[105];
z[14] = abb[58] * z[14];
z[17] = z[58] * z[107];
z[7] = z[5] + z[7] + z[9] + z[12] + z[14] + z[17] + z[31] + z[46] + z[52] + z[93] + -2 * z[97];
z[7] = abb[35] * z[7];
z[9] = abb[58] + -z[10];
z[9] = z[9] * z[55];
z[10] = -z[117] + z[130];
z[10] = abb[5] * z[10];
z[12] = z[54] + z[71];
z[14] = z[12] * z[138];
z[17] = -z[56] * z[58];
z[18] = z[67] + -z[89];
z[18] = abb[0] * z[18];
z[9] = z[9] + z[10] + z[11] + z[14] + z[17] + z[18] + -z[32] + -z[50];
z[9] = abb[31] * z[9];
z[10] = abb[56] + -z[98];
z[10] = z[10] * z[111];
z[11] = -abb[56] + z[12];
z[11] = abb[0] * z[11];
z[10] = z[10] + z[11] + z[42] + -z[51] + -z[66] + -z[74] + z[97];
z[10] = abb[33] * z[10];
z[11] = abb[5] * z[57];
z[12] = abb[58] * z[105];
z[14] = abb[1] * z[58];
z[5] = -z[5] + z[11] + z[12] + z[14] + z[46] + z[125] + z[131];
z[5] = abb[32] * z[5];
z[11] = -z[44] + -z[58] + -z[115];
z[11] = z[11] * z[41];
z[5] = z[5] + z[7] + z[9] + z[10] + z[11] + -z[15];
z[5] = m1_set::bc<T>[0] * z[5];
z[7] = abb[43] * z[25];
z[9] = -abb[44] * z[29];
z[2] = -abb[27] + abb[28] + 2 * z[0] + z[2];
z[2] = abb[35] * z[2];
z[0] = z[0] + z[13];
z[0] = abb[33] * z[0];
z[1] = abb[32] * z[1];
z[10] = abb[31] * z[22];
z[0] = z[0] + z[1] + -z[2] + -z[10] + -z[16];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[42] * z[3];
z[2] = abb[36] * m1_set::bc<T>[0];
z[3] = z[2] * z[8];
z[8] = abb[41] * z[22];
z[10] = abb[44] * z[24];
z[11] = abb[43] * z[26];
z[0] = z[0] + -z[1] + z[3] + -z[8] + -z[10] + -z[11];
z[0] = -z[0] * z[6];
z[1] = -abb[42] * z[23];
z[2] = -z[2] * z[53];
z[3] = -z[37] + z[58];
z[3] = abb[0] * z[3];
z[6] = -abb[17] * z[58];
z[3] = z[3] + z[6] + -z[28];
z[3] = abb[41] * z[3];
z[6] = -abb[41] + abb[42];
z[6] = z[6] * z[40];
z[8] = -m1_set::bc<T>[0] * z[47];
z[6] = z[6] + z[8];
z[6] = abb[6] * z[6];
z[8] = -abb[47] * z[27];
z[0] = abb[45] + abb[46] + z[0] + z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + z[8] + z[9];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_654_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("4.0549405544182681858021109701137329747769965591317530237318657023"),stof<T>("5.2105367781938766172750286354578781008753444151395946877934623755")}, std::complex<T>{stof<T>("-15.927884452211109625321039725382567525874898971354366178509680538"),stof<T>("-2.770327363474794988029681283490635497436961693904857615219119947")}, std::complex<T>{stof<T>("1.197783340108982842113156859196858997201643252906002650661903574"),stof<T>("-10.531710897712234205811330326594131389396102116440914079046112629")}, std::complex<T>{stof<T>("-3.0333195632308512894927279627196221507316554311365096099336933851"),stof<T>("2.5248877916572274723048389645350136470954512437313557734568534154")}, std::complex<T>{stof<T>("1.1048384237308970223656392223276463223785147286541254668707201231"),stof<T>("1.4639623029700601432425161240033904706239233795937915111856691633")}, std::complex<T>{stof<T>("-11.142246098401870014504996874073717719946404962646231662376745148"),stof<T>("8.983070217804028219509322590022969875115110214350503866977932479")}, std::complex<T>{stof<T>("14.723031882494874666277338608196022999331801735450616364936254692"),stof<T>("12.219091889099924597567083692411917054300198342654057289205969846")}, std::complex<T>{stof<T>("-1.8488106530094033717902282135306562051351095078410308024113523606"),stof<T>("0.4401574990783764726823055843217097560116973162601166337086091277")}, std::complex<T>{stof<T>("0.7474105939235813918735996072560840695488486637844138699749244169"),stof<T>("-12.5677283706967630459880254716939147551697581419461767381240639864")}, std::complex<T>{stof<T>("4.1843893662613707477229743317377905246010223899295916125993394271"),stof<T>("1.0028794179071829898056514172515855326762007416075746582541814245")}, std::complex<T>{stof<T>("-1.6048809138609959509188142370120174883144154325121793177869173784"),stof<T>("5.0998238860993871277760182364824319829888294352480755024407994981")}, std::complex<T>{stof<T>("-1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(abs(k.W[79])) - rlog(abs(kbase.W[79])), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_654_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_654_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (8 + -7 * v[0] + 3 * v[1] + 5 * v[2] + v[3] + -4 * v[4] + -2 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -5 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[48] + abb[50] + -abb[59]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[31] + -abb[33];
z[1] = abb[34] + z[0];
z[1] = abb[34] * z[1];
z[0] = abb[36] + z[0];
z[0] = abb[36] * z[0];
z[0] = -abb[39] + -z[0] + z[1];
z[1] = abb[48] + abb[50] + -abb[59];
return 2 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_654_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[48] + abb[50] + -abb[59]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = -abb[34] + abb[36];
z[1] = abb[48] + abb[50] + -abb[59];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_654_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (-24 + -12 * v[0] + 7 * v[1] + -5 * v[2] + -7 * v[3] + 5 * v[4] + 4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 12 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (8 * v[0] + 7 * v[1] + 7 * v[2] + -3 * v[3] + -7 * v[4]) + -m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (96 + 32 * v[0] + -38 * v[1] + 10 * v[2] + 30 * v[3] + -10 * v[4] + m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + -24 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -48 * v[5])) / prod_pow(tend, 2);
c[3] = ((-1 + 2 * m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[4] = (m1_set::bc<T>[2] * (T(3) / T(2)) * (-v[1] + -v[2] + v[3] + v[4])) / tend;
c[5] = ((4 * m1_set::bc<T>[1] + -m1_set::bc<T>[2]) * (T(-3) / T(4)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return abb[52] * c[3] + abb[54] * (c[3] + -c[4]) + abb[50] * (2 * c[3] + -c[4]) + abb[49] * c[4] + abb[53] * (-2 * c[3] + c[4]) + abb[58] * c[5] + -abb[48] * (2 * c[3] + c[5]) + abb[51] * (-3 * c[3] + c[4]) * (T(1) / T(2)) + abb[55] * (-3 * c[3] + c[4] + -2 * c[5]) * (T(1) / T(2)) + t * (abb[52] * c[0] + abb[54] * (c[0] + -c[1]) + abb[50] * (2 * c[0] + -c[1]) + abb[49] * c[1] + abb[53] * (-2 * c[0] + c[1]) + abb[58] * c[2] + -abb[48] * (2 * c[0] + c[2]) + abb[51] * (-3 * c[0] + c[1]) * (T(1) / T(2)) + abb[55] * (-3 * c[0] + c[1] + -2 * c[2]) * (T(1) / T(2)));
	}
	{
T z[6];
z[0] = abb[52] + abb[54];
z[1] = 2 * abb[48];
z[2] = abb[50] + -abb[53];
z[3] = abb[51] * (T(-3) / T(2)) + z[0] + -z[1] + 2 * z[2];
z[4] = abb[38] * z[3];
z[3] = abb[55] * (T(3) / T(2)) + -z[3];
z[3] = abb[36] * z[3];
z[0] = abb[51] * (T(-3) / T(4)) + (T(1) / T(2)) * z[0] + z[2];
z[5] = -abb[58] + abb[55] * (T(1) / T(4)) + z[0];
z[5] = abb[32] * z[5];
z[3] = z[3] + 3 * z[5];
z[3] = abb[32] * z[3];
z[5] = -abb[37] + abb[40];
z[2] = -abb[49] + abb[54] + z[2];
z[2] = -abb[48] + -abb[51] + abb[58] + 2 * z[2];
z[2] = z[2] * z[5];
z[0] = 3 * abb[58] + -z[0] + -z[1];
z[1] = prod_pow(abb[36], 2);
z[0] = z[0] * z[1];
z[1] = -abb[38] + (T(-3) / T(2)) * z[1];
z[1] = (T(3) / T(2)) * z[1] + -2 * z[5];
z[1] = abb[55] * z[1];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4];
return abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_654_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_654_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("1.641716277584588732796610950561090164051020069258474987822853549"),stof<T>("-15.888915923535740023822939603066782080337307132266650919022404062")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19, 79});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,71> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(abs(kend.W[79])) - rlog(abs(k.W[79])), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[45] = SpDLog_f_4_654_W_17_Im(t, path, abb);
abb[46] = SpDLog_f_4_654_W_20_Im(t, path, abb);
abb[47] = SpDLogQ_W_80(k,dl,dlr).imag();
abb[68] = SpDLog_f_4_654_W_17_Re(t, path, abb);
abb[69] = SpDLog_f_4_654_W_20_Re(t, path, abb);
abb[70] = SpDLogQ_W_80(k,dl,dlr).real();

                    
            return f_4_654_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_654_DLogXconstant_part(base_point<T>, kend);
	value += f_4_654_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_654_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_654_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_654_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_654_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_654_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_654_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
