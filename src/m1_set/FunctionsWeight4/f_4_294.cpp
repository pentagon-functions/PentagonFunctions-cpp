/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_294.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_294_abbreviated (const std::array<T,34>& abb) {
T z[13];
z[0] = -abb[23] + abb[28];
z[1] = abb[4] + -abb[9];
z[1] = z[0] * z[1];
z[2] = -abb[12] * z[1];
z[3] = abb[4] + -abb[5] + -abb[6];
z[3] = z[0] * z[3];
z[4] = abb[24] + -abb[28];
z[5] = abb[19] + -abb[25] + -z[4];
z[5] = abb[0] * z[5];
z[3] = z[3] + z[5];
z[3] = abb[10] * z[3];
z[2] = 2 * z[2] + z[3];
z[2] = abb[10] * z[2];
z[3] = abb[29] + abb[30] + abb[31] + abb[32] + abb[33];
z[3] = 2 * z[3];
z[3] = -z[1] * z[3];
z[6] = abb[7] + -abb[9];
z[7] = abb[6] + z[6];
z[7] = z[0] * z[7];
z[8] = abb[26] + -abb[28];
z[9] = -abb[21] + abb[25] + z[8];
z[9] = abb[2] * z[9];
z[7] = z[7] + z[9];
z[7] = prod_pow(abb[11], 2) * z[7];
z[10] = abb[4] + -abb[7] + -abb[8];
z[10] = z[0] * z[10];
z[8] = -abb[22] + abb[27] + z[8];
z[8] = abb[3] * z[8];
z[10] = -z[8] + z[10];
z[10] = prod_pow(abb[12], 2) * z[10];
z[11] = abb[5] + -abb[9];
z[12] = abb[8] + z[11];
z[12] = z[0] * z[12];
z[4] = -abb[20] + abb[27] + z[4];
z[4] = abb[1] * z[4];
z[12] = z[4] + z[12];
z[12] = prod_pow(abb[13], 2) * z[12];
z[2] = z[2] + z[3] + z[7] + z[10] + z[12];
z[3] = abb[5] + abb[7];
z[7] = abb[6] + abb[8];
z[3] = -32 * abb[4] + 8 * abb[9] + 12 * z[3] + 12 * z[7];
z[3] = z[0] * z[3];
z[7] = -z[5] + z[8];
z[3] = z[3] + -z[4] + 13 * z[7] + -z[9];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[2] = 12 * z[2] + z[3];
z[3] = abb[8] + z[6];
z[3] = z[0] * z[3];
z[3] = z[3] + z[8];
z[3] = abb[12] * z[3];
z[4] = abb[6] + z[11];
z[0] = z[0] * z[4];
z[0] = z[0] + -z[5];
z[0] = abb[10] * z[0];
z[0] = z[0] + z[3];
z[0] = m1_set::bc<T>[0] * z[0];
z[3] = -abb[14] + -abb[15] + -abb[16] + -abb[17] + -abb[18];
z[1] = z[1] * z[3];
z[0] = z[0] + z[1];
z[0] = 24 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_294_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("38.465820901107017132791621168366399704479561354895964217352689771"),stof<T>("155.03138340149910087738157533550697601112644282942553847072269052")}, stof<T>("-32.956831045205618506819556344859225809804288248102477311593405295"), stof<T>("-32.956831045205618506819556344859225809804288248102477311593405295"), std::complex<T>{stof<T>("-49.13623052107978003135019350258992704579306033015248487229617689"),stof<T>("136.91873149445314554478650620636100777405312451143889591022078336")}, std::complex<T>{stof<T>("-102.24030369920257264368541891449389035307383457873764231237732759"),stof<T>("310.70852504071095904647011948005737510300719599672670811994808846")}, std::complex<T>{stof<T>("-5.50898985590139862597206482350717389467527310679348690575928448"),stof<T>("-155.03138340149910087738157533550697601112644282942553847072269052")}, std::complex<T>{stof<T>("-5.50898985590139862597206482350717389467527310679348690575928448"),stof<T>("-155.03138340149910087738157533550697601112644282942553847072269052")}, std::complex<T>{stof<T>("82.09306156628539853816974984744915285559734857825496218388958218"),stof<T>("-136.91873149445314554478650620636100777405312451143889591022078336")}, std::complex<T>{stof<T>("82.09306156628539853816974984744915285559734857825496218388958218"),stof<T>("-136.91873149445314554478650620636100777405312451143889591022078336")}, std::complex<T>{stof<T>("25.656231988818572731487733890551911392151759107276167034247029881"),stof<T>("-18.758410144758712624302037938189391317827628655862273739004614584")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_294_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_294_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("14.9579874494508173018169059777765430758278712020567774841100619"),stof<T>("-288.57306021245802127390417347318757460016465554144575279896873921")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,34> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_294_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_294_DLogXconstant_part(base_point<T>, kend);
	value += f_4_294_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_294_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_294_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_294_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_294_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_294_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_294_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
