/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_444.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_444_abbreviated (const std::array<T,59>& abb) {
T z[77];
z[0] = -abb[30] + abb[35];
z[1] = abb[31] + -abb[32];
z[2] = z[0] + -z[1];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = -abb[41] + z[2];
z[4] = abb[0] * (T(1) / T(2));
z[5] = z[3] * z[4];
z[6] = -abb[32] + abb[34];
z[6] = m1_set::bc<T>[0] * z[6];
z[7] = abb[11] * z[6];
z[8] = -abb[33] + abb[34];
z[9] = abb[13] * m1_set::bc<T>[0] * z[8];
z[5] = z[5] + -z[7] + z[9];
z[7] = -abb[30] + abb[34];
z[7] = m1_set::bc<T>[0] * z[7];
z[10] = -abb[41] + abb[42];
z[11] = z[7] + z[10];
z[12] = abb[6] * (T(1) / T(2));
z[13] = -z[11] * z[12];
z[14] = abb[7] * (T(1) / T(2));
z[15] = -abb[31] + abb[33];
z[15] = m1_set::bc<T>[0] * z[15];
z[15] = -abb[43] + z[15];
z[15] = z[14] * z[15];
z[16] = abb[8] * (T(1) / T(2));
z[17] = abb[30] + abb[33];
z[18] = -abb[34] + z[17];
z[19] = -abb[31] + z[18];
z[19] = m1_set::bc<T>[0] * z[19];
z[20] = abb[42] + -z[19];
z[20] = z[16] * z[20];
z[21] = -abb[35] + z[18];
z[21] = m1_set::bc<T>[0] * z[21];
z[22] = z[10] + -3 * z[21];
z[23] = abb[16] * (T(1) / T(2));
z[22] = z[22] * z[23];
z[24] = abb[30] + abb[32];
z[25] = -abb[34] + (T(1) / T(2)) * z[24];
z[25] = abb[3] * m1_set::bc<T>[0] * z[25];
z[13] = -z[5] + z[13] + z[15] + z[20] + z[22] + z[25];
z[15] = -z[1] + z[17];
z[15] = -abb[34] + (T(1) / T(2)) * z[15];
z[15] = m1_set::bc<T>[0] * z[15];
z[20] = abb[42] * (T(1) / T(2));
z[15] = z[15] + -z[20];
z[22] = abb[1] * z[15];
z[13] = (T(1) / T(2)) * z[13] + z[22];
z[13] = abb[51] * z[13];
z[25] = abb[32] * (T(1) / T(2));
z[26] = abb[31] * (T(1) / T(2));
z[27] = z[25] + -z[26];
z[28] = abb[30] * (T(1) / T(2));
z[29] = abb[35] * (T(1) / T(2));
z[30] = z[8] + -z[27] + -z[28] + z[29];
z[30] = m1_set::bc<T>[0] * z[30];
z[31] = abb[43] * (T(1) / T(2));
z[30] = z[30] + z[31];
z[32] = abb[48] + abb[50];
z[33] = abb[3] * z[32];
z[30] = -z[30] * z[33];
z[5] = -z[5] * z[32];
z[34] = -abb[49] * z[9];
z[5] = z[5] + z[30] + z[34];
z[30] = abb[17] * (T(1) / T(4));
z[34] = z[30] * z[32];
z[35] = -abb[43] + z[3];
z[36] = z[34] * z[35];
z[37] = abb[49] + z[32];
z[23] = -z[21] * z[23] * z[37];
z[38] = abb[14] * z[32];
z[39] = m1_set::bc<T>[0] * z[1];
z[39] = abb[43] + (T(1) / T(2)) * z[39];
z[40] = z[38] * z[39];
z[41] = abb[8] * (T(1) / T(4));
z[42] = z[32] * z[41];
z[43] = -abb[41] + abb[43];
z[43] = -z[42] * z[43];
z[44] = abb[49] * (T(1) / T(2));
z[7] = abb[9] * z[7];
z[45] = -z[7] * z[44];
z[46] = abb[1] * z[32];
z[15] = z[15] * z[46];
z[5] = (T(1) / T(2)) * z[5] + z[13] + z[15] + z[23] + z[36] + z[40] + z[43] + z[45];
z[13] = abb[23] + abb[25];
z[11] = z[11] * z[13];
z[13] = abb[43] + z[10] + -z[19];
z[15] = abb[22] + abb[24];
z[13] = z[13] * z[15];
z[10] = -z[10] + z[21];
z[10] = abb[26] * z[10];
z[19] = -abb[27] * z[35];
z[10] = z[10] + z[11] + z[13] + z[19];
z[11] = abb[45] + abb[46] + -abb[47];
z[11] = (T(1) / T(32)) * z[11];
z[10] = z[10] * z[11];
z[13] = abb[0] * z[3];
z[2] = -abb[43] + z[2];
z[19] = abb[3] * z[2];
z[19] = z[13] + z[19];
z[23] = abb[41] * (T(1) / T(2));
z[36] = m1_set::bc<T>[0] * z[0];
z[31] = z[23] + -z[31] + -z[36];
z[31] = z[16] * z[31];
z[35] = z[30] * z[35];
z[40] = z[0] + z[27];
z[40] = m1_set::bc<T>[0] * z[40];
z[23] = -z[23] + z[40];
z[23] = abb[2] * z[23];
z[39] = abb[14] * z[39];
z[19] = (T(1) / T(4)) * z[19] + -z[23] + z[31] + z[35] + z[39];
z[31] = abb[53] * (T(1) / T(8));
z[19] = z[19] * z[31];
z[35] = -abb[3] + abb[8];
z[21] = z[21] * z[35];
z[7] = -z[7] + z[9] + z[13] + z[21];
z[9] = -abb[35] + z[1];
z[13] = abb[33] + -z[9];
z[13] = -abb[34] + (T(1) / T(2)) * z[13];
z[13] = m1_set::bc<T>[0] * z[13];
z[13] = z[13] + -z[20];
z[13] = abb[5] * z[13];
z[7] = (T(1) / T(2)) * z[7] + z[13] + -z[22] + -z[23];
z[13] = abb[52] * (T(1) / T(8));
z[7] = z[7] * z[13];
z[3] = -abb[21] * z[3];
z[2] = abb[19] * z[2];
z[20] = abb[18] * abb[41];
z[21] = -abb[18] + abb[21];
z[22] = abb[43] * z[21];
z[2] = z[2] + z[3] + z[20] + z[22];
z[2] = abb[54] * z[2];
z[3] = abb[31] + -abb[35] + z[8];
z[3] = m1_set::bc<T>[0] * z[3];
z[3] = abb[42] + z[3];
z[3] = z[3] * z[32];
z[8] = abb[34] + z[9];
z[8] = m1_set::bc<T>[0] * z[8];
z[8] = abb[42] + abb[43] + z[8];
z[8] = abb[51] * z[8];
z[3] = z[3] + (T(1) / T(2)) * z[8];
z[3] = abb[5] * z[3];
z[8] = abb[4] * (T(1) / T(32));
z[6] = -abb[42] + -z[6];
z[6] = abb[51] * z[6] * z[8];
z[20] = -abb[51] + abb[52] + abb[53] + -z[37];
z[22] = abb[10] * (T(1) / T(16));
z[20] = z[20] * z[22];
z[22] = z[20] * z[36];
z[2] = abb[58] + (T(1) / T(32)) * z[2] + (T(1) / T(16)) * z[3] + (T(1) / T(8)) * z[5] + z[6] + z[7] + z[10] + z[19] + z[22];
z[3] = z[24] + -z[29];
z[3] = abb[35] * z[3];
z[5] = -abb[35] + z[26];
z[6] = -abb[30] + abb[32];
z[7] = -z[5] + (T(1) / T(2)) * z[6];
z[7] = abb[31] * z[7];
z[10] = abb[30] * z[25];
z[19] = prod_pow(abb[30], 2);
z[22] = (T(1) / T(2)) * z[19];
z[23] = prod_pow(abb[33], 2);
z[3] = -z[3] + z[7] + z[10] + z[22] + -z[23];
z[36] = abb[57] * (T(1) / T(2));
z[39] = prod_pow(m1_set::bc<T>[0], 2);
z[40] = (T(1) / T(12)) * z[39];
z[43] = abb[34] * (T(1) / T(2));
z[45] = -abb[33] + z[43];
z[45] = abb[34] * z[45];
z[3] = (T(1) / T(2)) * z[3] + z[36] + z[40] + -z[45];
z[3] = z[3] * z[33];
z[6] = z[6] + z[29];
z[6] = abb[35] * z[6];
z[33] = -abb[55] + z[6];
z[47] = (T(1) / T(6)) * z[39];
z[48] = z[33] + -z[47];
z[49] = abb[31] * z[0];
z[50] = abb[30] * abb[32];
z[49] = -z[22] + -z[48] + z[49] + z[50];
z[4] = z[4] * z[49];
z[51] = (T(1) / T(2)) * z[39];
z[52] = (T(1) / T(2)) * z[23];
z[53] = -z[45] + z[51] + -z[52];
z[53] = abb[13] * z[53];
z[54] = -abb[33] + z[25];
z[54] = abb[32] * z[54];
z[55] = -z[45] + z[54];
z[56] = abb[11] * z[55];
z[4] = z[4] + -z[53] + z[56];
z[56] = -z[4] * z[32];
z[57] = abb[49] * z[53];
z[3] = z[3] + z[56] + z[57];
z[18] = -z[1] + z[18];
z[18] = abb[34] * z[18];
z[56] = prod_pow(abb[32], 2);
z[56] = -abb[56] + (T(1) / T(2)) * z[56];
z[57] = abb[33] + z[28];
z[58] = abb[30] * z[57];
z[59] = -z[18] + z[56] + z[58];
z[60] = -abb[35] + z[17];
z[61] = -abb[32] + z[60];
z[61] = abb[35] * z[61];
z[61] = -abb[38] + z[61];
z[62] = abb[57] + z[47];
z[63] = -abb[37] + z[62];
z[52] = abb[39] + z[52];
z[5] = -abb[32] + z[5];
z[5] = abb[31] * z[5];
z[5] = z[5] + z[52] + z[59] + -z[61] + -z[63];
z[5] = abb[51] * z[5];
z[64] = abb[31] * abb[33];
z[64] = -z[52] + z[64];
z[65] = -abb[30] + z[29];
z[66] = abb[35] * z[65];
z[59] = z[59] + -z[64] + z[66];
z[67] = abb[37] * (T(1) / T(2));
z[39] = (T(1) / T(3)) * z[39];
z[59] = -z[39] + (T(1) / T(2)) * z[59] + z[67];
z[59] = z[32] * z[59];
z[5] = (T(1) / T(4)) * z[5] + z[59];
z[5] = abb[5] * z[5];
z[59] = abb[32] * abb[35];
z[9] = abb[31] * z[9];
z[9] = z[9] + z[39] + z[59];
z[9] = -abb[57] + (T(1) / T(2)) * z[9];
z[38] = z[9] * z[38];
z[59] = -z[19] + z[50];
z[59] = (T(1) / T(2)) * z[59];
z[7] = z[7] + z[59];
z[68] = z[7] + z[62];
z[33] = -z[33] + z[68];
z[34] = z[33] * z[34];
z[1] = -abb[30] + z[1];
z[69] = abb[31] * z[1];
z[50] = z[50] + z[69];
z[50] = abb[55] + (T(1) / T(2)) * z[50];
z[70] = -abb[57] + z[50];
z[42] = -z[42] * z[70];
z[70] = abb[32] * abb[33];
z[70] = -z[64] + z[70];
z[71] = abb[30] * abb[33];
z[72] = z[70] + z[71];
z[27] = z[27] + z[57];
z[57] = abb[34] * (T(3) / T(4)) + -z[27];
z[57] = abb[34] * z[57];
z[73] = abb[56] * (T(1) / T(2));
z[57] = z[57] + -z[73];
z[40] = -z[40] + z[57] + (T(1) / T(2)) * z[72];
z[46] = -z[40] * z[46];
z[67] = -abb[11] * z[32] * z[67];
z[28] = -abb[33] + z[28];
z[28] = abb[30] * z[28];
z[28] = abb[36] + z[28];
z[72] = -z[28] + z[45];
z[29] = -abb[33] + z[29];
z[29] = abb[35] * z[29];
z[74] = abb[38] + z[29];
z[75] = z[72] + z[74];
z[76] = z[52] + z[75];
z[76] = abb[9] * z[76];
z[44] = -z[44] * z[76];
z[3] = (T(1) / T(2)) * z[3] + z[5] + z[34] + z[38] + z[42] + z[44] + z[46] + z[67];
z[5] = z[19] + -z[23];
z[17] = abb[32] * z[17];
z[17] = abb[36] + z[17];
z[23] = -z[5] + z[17];
z[34] = -z[26] * z[60];
z[38] = abb[38] * (T(1) / T(2));
z[42] = abb[55] * (T(1) / T(2));
z[44] = 3 * abb[30] + -abb[32] + abb[33];
z[44] = -abb[35] + (T(1) / T(2)) * z[44];
z[44] = abb[35] * z[44];
z[46] = -3 * abb[33] + z[1];
z[46] = abb[34] + (T(1) / T(2)) * z[46];
z[46] = abb[34] * z[46];
z[23] = (T(1) / T(2)) * z[23] + z[34] + -z[38] + z[42] + z[44] + z[46] + -z[73];
z[23] = abb[16] * z[23];
z[34] = z[24] * z[26];
z[44] = -abb[32] * z[24];
z[44] = -z[19] + z[44];
z[1] = -z[1] + -z[43];
z[1] = abb[34] * z[1];
z[1] = -abb[36] + z[1] + z[34] + (T(1) / T(2)) * z[44];
z[43] = abb[15] * (T(1) / T(2));
z[1] = z[1] * z[43];
z[28] = z[28] + -z[29] + -z[52] + z[54];
z[28] = (T(1) / T(2)) * z[28] + -z[38] + -z[45];
z[28] = abb[3] * z[28];
z[44] = -z[18] + z[71];
z[46] = abb[37] + z[44] + -z[47] + z[56] + -z[64];
z[46] = z[16] * z[46];
z[47] = -abb[55] + abb[56];
z[52] = z[47] + -z[52];
z[29] = -z[29] + z[52] + -z[72];
z[12] = z[12] * z[29];
z[29] = abb[33] + -z[26];
z[29] = abb[31] * z[29];
z[29] = z[29] + z[62] + z[74];
z[14] = z[14] * z[29];
z[29] = -abb[6] * z[38];
z[38] = -abb[11] + abb[3] * (T(1) / T(2)) + -z[43];
z[38] = abb[37] * z[38];
z[1] = z[1] + -z[4] + z[12] + z[14] + z[23] + z[28] + z[29] + z[38] + z[46];
z[4] = abb[1] * z[40];
z[1] = (T(1) / T(2)) * z[1] + -z[4];
z[12] = abb[37] + abb[56] + z[55];
z[8] = z[8] * z[12];
z[1] = (T(1) / T(8)) * z[1] + z[8];
z[1] = abb[51] * z[1];
z[8] = abb[31] * z[60];
z[8] = z[8] + -z[17] + z[18] + z[47] + -z[61];
z[8] = abb[26] * z[8];
z[12] = z[24] * z[25];
z[12] = z[12] + z[44] + -z[52];
z[14] = abb[37] + z[12] + -z[34] + z[74];
z[14] = abb[25] * z[14];
z[17] = abb[31] * z[27];
z[12] = -z[12] + z[17] + z[63];
z[12] = -z[12] * z[15];
z[7] = z[7] + -z[48];
z[15] = -abb[57] + -z[7];
z[15] = abb[27] * z[15];
z[17] = -z[52] + z[75];
z[17] = abb[23] * z[17];
z[18] = abb[28] * abb[40];
z[8] = z[8] + z[12] + z[14] + z[15] + z[17] + (T(-1) / T(2)) * z[18];
z[8] = z[8] * z[11];
z[11] = z[25] + z[65];
z[11] = abb[35] * z[11];
z[0] = z[0] * z[26];
z[0] = -z[0] + z[11] + -z[39] + -z[42] + -z[59];
z[0] = abb[2] * z[0];
z[11] = z[51] + -z[66];
z[10] = -z[10] + z[19];
z[10] = (T(1) / T(2)) * z[10] + -z[11] + z[36] + -z[42] + (T(-1) / T(4)) * z[69];
z[10] = z[10] * z[16];
z[12] = abb[0] * z[49];
z[6] = -z[6] + z[68];
z[14] = abb[3] * z[6];
z[14] = z[12] + z[14];
z[9] = abb[14] * z[9];
z[15] = z[30] * z[33];
z[16] = -abb[19] + z[21];
z[17] = abb[40] * z[16];
z[9] = z[0] + z[9] + z[10] + (T(1) / T(4)) * z[14] + z[15] + (T(1) / T(8)) * z[17];
z[9] = z[9] * z[31];
z[5] = (T(1) / T(2)) * z[5] + -z[45] + z[66];
z[10] = z[5] * z[35];
z[10] = z[10] + z[12] + -z[53] + -z[76];
z[12] = -z[58] + -z[66] + -z[70];
z[12] = (T(1) / T(2)) * z[12] + z[39] + -z[57];
z[12] = abb[5] * z[12];
z[0] = z[0] + z[4] + (T(1) / T(2)) * z[10] + z[12];
z[0] = z[0] * z[13];
z[4] = -abb[21] * z[7];
z[6] = abb[19] * z[6];
z[7] = -abb[18] * z[50];
z[10] = -abb[57] * z[21];
z[4] = z[4] + z[6] + z[7] + z[10];
z[6] = abb[29] + abb[3] * (T(-1) / T(4)) + -z[30] + -z[41];
z[6] = abb[40] * z[6];
z[4] = (T(1) / T(2)) * z[4] + z[6];
z[4] = abb[54] * z[4];
z[5] = -abb[16] * z[5] * z[37];
z[4] = z[4] + z[5];
z[5] = z[11] + -z[22];
z[5] = z[5] * z[20];
z[6] = z[16] * z[32];
z[7] = abb[20] * abb[51];
z[6] = z[6] + z[7];
z[6] = abb[40] * z[6];
z[0] = abb[44] + z[0] + z[1] + (T(1) / T(8)) * z[3] + (T(1) / T(16)) * z[4] + z[5] + (T(1) / T(64)) * z[6] + z[8] + z[9];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_444_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.33701468064558185446866412470019830319937022914251869924624946374"),stof<T>("-0.14561875400977267489976611882910214047667878027641329937096211061")}, std::complex<T>{stof<T>("0.15585669667485479635021532445261665812868809914622417177226469251"),stof<T>("-0.0091097422486795369492200358745308101337777565600924021283795574")}, std::complex<T>{stof<T>("0.33701468064558185446866412470019830319937022914251869924624946374"),stof<T>("-0.14561875400977267489976611882910214047667878027641329937096211061")}, std::complex<T>{stof<T>("0.13613951574430168564359624868426622630872501971132014618253301624"),stof<T>("-0.07608864657997020762690622448667846084791668744126236746022915172")}, std::complex<T>{stof<T>("-0.20636411204104329246113791402030153420516809761363269667554353924"),stof<T>("0.3057165809321707237797521704678084597823187860405509182532481123")}, std::complex<T>{stof<T>("-0.08118023641783776518816272675970876612021329015729065179285492045"),stof<T>("0.051353341293645892176786036393003526554956333792846185870425911794")}, std::complex<T>{stof<T>("-0.09878707965036109190184511942008702383650409213743360019279322877"),stof<T>("0.13801363516870842049040280265772312585990829028334043153424807411")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_444_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_444_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[49] + abb[51] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[49] + abb[51] + abb[52];
z[1] = abb[33] + -abb[35];
return abb[12] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_444_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(512)) * (v[1] + v[2] + -v[3] + -v[4]) * (m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + 4 * (-4 + -3 * v[1] + -3 * v[2] + v[3] + 3 * v[4] + m1_set::bc<T>[1] * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])))) / prod_pow(tend, 2);
c[1] = ((2 + 2 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(-1) / T(64)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[49] + abb[51] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + -abb[33] + abb[35];
z[0] = abb[35] * z[0];
z[1] = abb[30] * abb[33];
z[0] = -abb[36] + abb[38] + abb[39] + z[0] + z[1];
z[1] = abb[49] + abb[51] + abb[52];
return abb[12] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_444_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.14873367333180748396883801988726134710133084534528226533555621794"),stof<T>("0.291845677891083424916745273505581526316684254359053906837983272")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W18(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), T{0}, rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k), T{0}};
abb[44] = SpDLog_f_4_444_W_20_Im(t, path, abb);
abb[58] = SpDLog_f_4_444_W_20_Re(t, path, abb);

                    
            return f_4_444_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_444_DLogXconstant_part(base_point<T>, kend);
	value += f_4_444_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_444_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_444_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_444_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_444_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_444_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_444_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
