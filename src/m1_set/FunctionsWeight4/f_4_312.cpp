/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_312.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_312_abbreviated (const std::array<T,27>& abb) {
T z[45];
z[0] = abb[19] + abb[22];
z[1] = abb[23] * (T(5) / T(2));
z[2] = abb[21] * (T(1) / T(2));
z[3] = abb[20] + z[0] + -z[1] + z[2];
z[3] = abb[8] * z[3];
z[4] = -abb[23] + z[0];
z[5] = abb[20] * (T(1) / T(2));
z[6] = z[4] + z[5];
z[7] = -abb[18] + abb[24];
z[8] = (T(1) / T(2)) * z[7];
z[9] = z[6] + -z[8];
z[9] = abb[0] * z[9];
z[10] = -z[2] + z[6];
z[11] = abb[6] * z[10];
z[12] = -abb[21] + z[7];
z[13] = abb[5] * z[12];
z[14] = abb[20] + -abb[23];
z[15] = -abb[4] * z[14];
z[16] = z[13] + z[15];
z[17] = (T(1) / T(2)) * z[16];
z[18] = z[0] + z[5];
z[19] = abb[23] * (T(3) / T(2));
z[20] = -z[18] + z[19];
z[21] = -z[12] + z[20];
z[21] = abb[3] * z[21];
z[22] = z[2] + -z[7];
z[23] = (T(5) / T(2)) * z[0];
z[24] = abb[20] * (T(-3) / T(4)) + abb[23] * (T(15) / T(4)) + z[22] + -z[23];
z[24] = abb[7] * z[24];
z[25] = -abb[20] + z[7];
z[26] = abb[2] * z[25];
z[27] = (T(1) / T(2)) * z[26];
z[28] = abb[1] * z[4];
z[3] = z[3] + -z[9] + z[11] + z[17] + (T(-1) / T(2)) * z[21] + z[24] + z[27] + z[28];
z[3] = abb[11] * z[3];
z[24] = 2 * abb[21];
z[28] = abb[23] * (T(11) / T(2)) + -5 * z[0] + -z[5] + -2 * z[7] + z[24];
z[28] = abb[7] * z[28];
z[29] = 2 * abb[23];
z[30] = 2 * abb[19];
z[31] = 2 * abb[22] + z[30];
z[32] = z[25] + z[29] + -z[31];
z[32] = abb[0] * z[32];
z[33] = 4 * abb[23];
z[34] = abb[20] + z[7];
z[35] = -z[31] + z[33] + -z[34];
z[35] = abb[8] * z[35];
z[32] = z[32] + -z[35];
z[36] = 2 * z[4];
z[37] = abb[20] + -abb[21] + z[36];
z[38] = abb[6] * z[37];
z[39] = abb[1] * z[36];
z[38] = z[38] + z[39];
z[39] = z[15] + z[38];
z[21] = -z[21] + z[28] + z[32] + z[39];
z[28] = -abb[12] * z[21];
z[40] = abb[22] + z[7];
z[41] = 3 * abb[23];
z[30] = -abb[21] + z[30] + 2 * z[40] + -z[41];
z[40] = abb[7] * z[30];
z[42] = abb[3] * z[12];
z[32] = z[15] + z[32] + -z[40] + z[42];
z[32] = abb[9] * z[32];
z[3] = z[3] + z[28] + z[32];
z[3] = abb[11] * z[3];
z[28] = abb[20] + -5 * abb[23] + 4 * z[0] + z[12];
z[28] = abb[7] * z[28];
z[32] = -z[0] + z[8];
z[41] = abb[21] + abb[20] * (T(3) / T(2)) + -z[32] + -z[41];
z[41] = abb[8] * z[41];
z[6] = -abb[21] + z[6] + z[8];
z[6] = abb[3] * z[6];
z[6] = z[6] + -z[9] + z[16] + z[26] + -z[28] + z[38] + z[41];
z[6] = abb[15] * z[6];
z[9] = abb[23] * (T(1) / T(2));
z[41] = abb[21] + z[9] + -z[18];
z[41] = abb[3] * z[41];
z[43] = z[13] + -z[15] + z[41];
z[19] = -z[0] + z[19];
z[22] = z[19] + z[22];
z[22] = abb[8] * z[22];
z[44] = abb[23] * (T(7) / T(4));
z[23] = abb[20] * (T(1) / T(4)) + abb[21] * (T(3) / T(2)) + -z[7] + -z[23] + z[44];
z[23] = abb[7] * z[23];
z[19] = z[8] + -z[19];
z[19] = abb[1] * z[19];
z[4] = -abb[0] * z[4];
z[4] = z[4] + z[11] + z[19] + -z[22] + z[23] + (T(-1) / T(2)) * z[43];
z[4] = abb[12] * z[4];
z[11] = -abb[8] * z[30];
z[19] = -abb[23] + z[7];
z[23] = abb[1] * z[19];
z[30] = abb[0] * z[36];
z[11] = z[11] + z[13] + -z[23] + z[30] + z[40];
z[11] = abb[9] * z[11];
z[4] = z[4] + z[11];
z[4] = abb[12] * z[4];
z[11] = abb[21] + (T(7) / T(2)) * z[7];
z[13] = z[0] + -z[5] + -z[11] + z[33];
z[13] = abb[7] * z[13];
z[18] = (T(1) / T(4)) * z[7] + z[18] + -z[44];
z[18] = abb[3] * z[18];
z[11] = abb[20] + abb[23] * (T(-7) / T(2)) + z[11] + -z[31];
z[11] = abb[6] * z[11];
z[30] = 7 * abb[23] + -z[5] + (T(-13) / T(2)) * z[7];
z[30] = abb[0] * z[30];
z[33] = abb[23] * (T(-5) / T(4)) + (T(13) / T(4)) * z[7] + -z[31];
z[33] = abb[1] * z[33];
z[11] = z[11] + z[13] + z[18] + (T(-1) / T(4)) * z[26] + (T(1) / T(2)) * z[30] + z[33];
z[11] = prod_pow(m1_set::bc<T>[0], 2) * z[11];
z[13] = abb[23] + z[7];
z[18] = z[13] + -z[31];
z[18] = abb[0] * z[18];
z[30] = abb[3] * z[37];
z[15] = z[15] + z[30];
z[13] = 2 * abb[20] + -z[13];
z[13] = abb[6] * z[13];
z[13] = -z[13] + -z[15] + -z[18] + z[28] + z[35];
z[18] = -abb[25] * z[13];
z[28] = -abb[26] * z[21];
z[29] = z[29] + -z[34];
z[29] = abb[6] * z[29];
z[17] = z[17] + z[22] + -z[29];
z[10] = -abb[3] * z[10];
z[7] = -z[0] + z[7];
z[7] = abb[0] * z[7];
z[7] = z[7] + z[10] + -z[17] + (T(-1) / T(2)) * z[23];
z[7] = prod_pow(abb[9], 2) * z[7];
z[10] = z[12] + -z[14];
z[10] = abb[8] * z[10];
z[10] = z[10] + -z[16];
z[12] = -abb[0] * z[25];
z[12] = z[10] + z[12] + z[23] + -z[42];
z[12] = abb[13] * z[12];
z[2] = -z[2] + z[8] + -z[20];
z[2] = abb[3] * z[2];
z[8] = -z[5] + -z[32];
z[8] = abb[0] * z[8];
z[2] = z[2] + z[8] + z[17] + -z[27];
z[2] = abb[10] * z[2];
z[8] = abb[21] + -abb[23];
z[8] = abb[3] * z[8];
z[8] = z[8] + z[10];
z[10] = z[8] + -z[26];
z[10] = abb[11] * z[10];
z[2] = z[2] + z[10];
z[2] = abb[10] * z[2];
z[8] = -abb[14] * z[8];
z[10] = abb[14] + abb[25];
z[10] = z[10] * z[26];
z[2] = z[2] + z[3] + z[4] + z[6] + z[7] + z[8] + z[10] + (T(1) / T(3)) * z[11] + z[12] + z[18] + z[28];
z[3] = -abb[0] + abb[1];
z[3] = 2 * z[3];
z[3] = z[3] * z[19];
z[3] = z[3] + z[15] + -2 * z[29] + -z[40];
z[3] = abb[9] * z[3];
z[0] = 3 * z[0];
z[4] = z[0] + -z[5] + -z[9] + -z[24];
z[4] = abb[7] * z[4];
z[4] = z[4] + -z[39] + z[41];
z[4] = abb[12] * z[4];
z[0] = abb[21] + -z[0] + z[1] + -z[5];
z[0] = abb[7] * z[0];
z[1] = -abb[3] * z[20];
z[0] = z[0] + z[1] + z[38];
z[0] = abb[11] * z[0];
z[0] = z[0] + z[3] + z[4];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -z[13] + z[26];
z[1] = abb[16] * z[1];
z[3] = -abb[17] * z[21];
z[0] = z[0] + z[1] + z[3];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_312_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-4.171975357091260252043523469487017582450893835303851356552847152"),stof<T>("-3.3419335874053662471503572110893861234362777435976292153505038481")}, std::complex<T>{stof<T>("-16.900508084290603617881854206630805896274362030943143607781593844"),stof<T>("5.028924956508673002287073092664459925396764709552659096640782361")}, std::complex<T>{stof<T>("6.7182789718561576069294429130509825412330651296114964771935896886"),stof<T>("2.9059201366585011814269374309669465557484230049023527858895057752")}, std::complex<T>{stof<T>("-1.5379394748195508877946303039206117082456198810963493042931264748"),stof<T>("-1.470468888893865262887082009899158890607943874688947112148628708")}, std::complex<T>{stof<T>("-16.900508084290603617881854206630805896274362030943143607781593844"),stof<T>("5.028924956508673002287073092664459925396764709552659096640782361")}, std::complex<T>{stof<T>("7.5481932301627366467035181280134174808360229471241450783282834787"),stof<T>("-9.8063097916786751679772857248216337139735215833636939857321632763")}, std::complex<T>{stof<T>("4.171975357091260252043523469487017582450893835303851356552847152"),stof<T>("3.3419335874053662471503572110893861234362777435976292153505038481")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_312_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_312_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-6.061265055815116469636576514050325251149881957035015158162865845"),stof<T>("21.468764258814714415653422853394464599019839121395236435035740092")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), f_1_1(k), f_1_4(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), f_2_4_re(k), f_2_11_re(k)};

                    
            return f_4_312_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_312_DLogXconstant_part(base_point<T>, kend);
	value += f_4_312_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_312_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_312_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_312_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_312_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_312_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_312_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
