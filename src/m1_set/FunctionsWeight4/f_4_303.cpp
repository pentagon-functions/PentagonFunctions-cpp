/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_303.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_303_abbreviated (const std::array<T,27>& abb) {
T z[38];
z[0] = -abb[20] + abb[22];
z[1] = 2 * abb[21];
z[2] = -z[0] + z[1];
z[3] = abb[18] + z[2];
z[3] = abb[4] * z[3];
z[4] = (T(1) / T(2)) * z[0];
z[5] = abb[18] + z[4];
z[6] = abb[19] * (T(3) / T(2));
z[7] = z[5] + -z[6];
z[8] = -abb[21] + z[7];
z[9] = abb[6] * z[8];
z[10] = 2 * abb[5];
z[11] = -abb[19] + z[0];
z[10] = z[10] * z[11];
z[12] = abb[1] * abb[19];
z[13] = (T(5) / T(2)) * z[12];
z[14] = -abb[1] * z[0];
z[15] = abb[1] * abb[18];
z[16] = -abb[18] + abb[19] * (T(7) / T(2)) + (T(-5) / T(2)) * z[0];
z[16] = abb[3] * z[16];
z[17] = abb[18] + z[0];
z[18] = 2 * abb[19] + -z[17];
z[19] = -abb[0] * z[18];
z[20] = 2 * abb[0];
z[21] = -abb[1] + abb[3] + z[20];
z[21] = abb[21] * z[21];
z[16] = -z[3] + z[9] + z[10] + z[13] + (T(7) / T(2)) * z[14] + z[15] + z[16] + z[19] + z[21];
z[16] = prod_pow(abb[14], 2) * z[16];
z[19] = 3 * abb[19];
z[21] = 2 * abb[18];
z[22] = z[19] + -z[21];
z[2] = z[2] + z[22];
z[2] = abb[6] * z[2];
z[23] = abb[0] + abb[3];
z[23] = z[1] * z[23];
z[24] = z[12] + z[14];
z[25] = 2 * z[0];
z[26] = -abb[18] + z[19] + -z[25];
z[27] = 4 * abb[21] + z[26];
z[27] = abb[4] * z[27];
z[22] = -z[0] + z[22];
z[22] = abb[3] * z[22];
z[28] = abb[0] * z[26];
z[23] = -z[2] + z[22] + z[23] + 3 * z[24] + z[27] + -z[28];
z[24] = -abb[25] * z[23];
z[27] = abb[18] + 5 * z[0];
z[19] = -z[19] + (T(1) / T(2)) * z[27];
z[27] = abb[0] * (T(1) / T(2));
z[19] = z[19] * z[27];
z[27] = -abb[0] + 2 * abb[1] + abb[3];
z[27] = abb[21] * z[27];
z[19] = z[9] + z[19] + z[27];
z[26] = abb[3] * z[26];
z[27] = 5 * z[12] + 3 * z[14];
z[26] = z[26] + z[27];
z[28] = abb[1] * z[21];
z[10] = z[10] + -z[28];
z[17] = -abb[19] + (T(1) / T(2)) * z[17];
z[29] = abb[2] * z[17];
z[30] = abb[23] + abb[24];
z[31] = abb[7] * z[30];
z[32] = (T(1) / T(2)) * z[31];
z[33] = abb[8] * z[30];
z[34] = -z[10] + -z[19] + -z[26] + (T(1) / T(2)) * z[29] + z[32] + (T(1) / T(4)) * z[33];
z[34] = abb[12] * z[34];
z[35] = abb[1] + abb[3];
z[1] = z[1] * z[35];
z[1] = z[1] + -z[2];
z[2] = z[1] + z[10];
z[10] = 5 * abb[19] + -3 * z[0] + -z[21];
z[10] = abb[3] * z[10];
z[21] = z[11] * z[20];
z[10] = z[2] + z[10] + 7 * z[12] + 5 * z[14] + z[21];
z[21] = abb[14] * z[10];
z[34] = z[21] + z[34];
z[29] = (T(3) / T(2)) * z[29] + (T(3) / T(4)) * z[33];
z[19] = -z[19] + -z[22] + -z[27] + z[28] + -z[29] + -z[32];
z[19] = abb[11] * z[19];
z[13] = z[13] + (T(3) / T(2)) * z[14] + -z[15];
z[22] = -abb[18] + abb[19];
z[22] = abb[3] * z[22];
z[36] = (T(3) / T(2)) * z[17];
z[36] = abb[0] * z[36];
z[37] = -abb[0] + abb[1];
z[37] = abb[21] * z[37];
z[22] = z[13] + (T(3) / T(2)) * z[22] + z[29] + z[36] + z[37];
z[22] = abb[13] * z[22];
z[19] = z[19] + z[22] + z[34];
z[19] = abb[13] * z[19];
z[22] = abb[18] * (T(7) / T(2));
z[29] = abb[19] * (T(-5) / T(2)) + -z[0] + z[22];
z[29] = abb[0] * z[29];
z[36] = abb[5] * z[11];
z[29] = z[29] + z[36];
z[14] = (T(-1) / T(3)) * z[14] + z[15];
z[15] = abb[19] * (T(1) / T(2));
z[4] = abb[18] + -z[4] + -z[15];
z[36] = abb[3] * (T(1) / T(3));
z[4] = z[4] * z[36];
z[5] = -abb[21] + z[5];
z[5] = (T(-1) / T(3)) * z[5] + z[15];
z[5] = abb[6] * z[5];
z[22] = -z[22] + z[25];
z[15] = abb[21] * (T(-4) / T(3)) + z[15] + (T(1) / T(3)) * z[22];
z[15] = abb[4] * z[15];
z[22] = abb[1] * (T(-1) / T(2)) + abb[0] * (T(1) / T(6)) + -z[36];
z[22] = abb[21] * z[22];
z[4] = z[4] + z[5] + (T(-2) / T(3)) * z[12] + (T(1) / T(2)) * z[14] + z[15] + z[22] + (T(1) / T(3)) * z[29];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[5] = abb[21] * z[35];
z[5] = z[5] + z[9] + z[13];
z[7] = -abb[3] * z[7];
z[9] = -abb[18] + z[0];
z[12] = abb[0] * z[9];
z[7] = z[3] + z[5] + z[7] + z[12];
z[7] = abb[11] * z[7];
z[7] = z[7] + -z[34];
z[7] = abb[11] * z[7];
z[0] = abb[18] * (T(-1) / T(2)) + -z[0] + z[6];
z[0] = abb[3] * z[0];
z[0] = z[0] + z[5] + (T(1) / T(2)) * z[12] + z[32];
z[0] = abb[12] * z[0];
z[0] = z[0] + -z[21];
z[0] = abb[12] * z[0];
z[5] = -abb[26] * z[10];
z[6] = abb[3] + -abb[10] + abb[0] * (T(1) / T(4));
z[6] = z[6] * z[30];
z[13] = abb[8] * z[17];
z[14] = abb[2] * z[30];
z[13] = z[13] + (T(1) / T(2)) * z[14];
z[8] = -abb[9] * z[8];
z[14] = abb[21] + (T(-1) / T(2)) * z[9];
z[14] = abb[7] * z[14];
z[6] = z[6] + z[8] + (T(3) / T(2)) * z[13] + z[14];
z[6] = abb[15] * z[6];
z[0] = z[0] + z[4] + z[5] + z[6] + z[7] + z[16] + z[19] + z[24];
z[4] = -abb[16] * z[23];
z[1] = -z[1] + -z[12] + -z[26] + z[28] + -z[31];
z[1] = abb[12] * z[1];
z[5] = abb[2] * z[18];
z[2] = z[2] + z[5] + -z[33];
z[5] = -z[9] * z[20];
z[3] = -z[2] + -2 * z[3] + z[5] + -z[26] + z[31];
z[3] = abb[11] * z[3];
z[5] = abb[3] * z[11];
z[2] = z[2] + -3 * z[5] + z[12] + z[27];
z[2] = abb[13] * z[2];
z[1] = z[1] + z[2] + z[3] + z[21];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[17] * z[10];
z[1] = z[1] + z[2] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_303_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-10.5310788947465738925997198593769860843356285156687147469774000497"),stof<T>("1.4588598612491342812118147996542339667044971325437066488445343151")}, std::complex<T>{stof<T>("10.53621819428656309846082257230952671766659978586591822473931155"),stof<T>("-3.4270936220792020731818444221714519030298827434077062112535346285")}, std::complex<T>{stof<T>("0.0051392995399892058611027129325406333309712701972034777619115003"),stof<T>("-1.9682337608300677919700296225172179363253856108639995624090003133")}, std::complex<T>{stof<T>("-26.880210816872341450909897200596016269818166498766283936203828411"),stof<T>("-4.65572360152186764833772650702743528282549532619367129579613118")}, std::complex<T>{stof<T>("-0.0051392995399892058611027129325406333309712701972034777619115003"),stof<T>("1.9682337608300677919700296225172179363253856108639995624090003133")}, std::complex<T>{stof<T>("-1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_303_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_303_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-36.869322902457852185574564728002899894329581299997821189718565005"),stof<T>("27.766363206743743975979840957242261308202491038358400957529679802")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_303_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_303_DLogXconstant_part(base_point<T>, kend);
	value += f_4_303_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_303_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_303_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_303_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_303_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_303_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_303_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
