/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_162.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_162_abbreviated (const std::array<T,16>& abb) {
T z[15];
z[0] = abb[7] + abb[8];
z[1] = abb[6] + z[0];
z[2] = abb[6] * z[1];
z[3] = (T(1) / T(4)) * z[2];
z[4] = abb[8] * (T(1) / T(2));
z[5] = abb[7] + z[4];
z[5] = abb[7] * z[5];
z[6] = prod_pow(abb[8], 2);
z[7] = prod_pow(m1_set::bc<T>[0], 2);
z[8] = z[3] + (T(-1) / T(2)) * z[5] + -2 * z[6] + (T(-1) / T(12)) * z[7];
z[8] = abb[2] * z[8];
z[9] = abb[6] * (T(1) / T(2));
z[1] = z[1] * z[9];
z[9] = z[1] + (T(-2) / T(3)) * z[7];
z[10] = abb[7] + -abb[8];
z[10] = abb[7] * z[10];
z[10] = z[6] + z[10];
z[11] = z[9] + (T(1) / T(2)) * z[10];
z[11] = abb[0] * z[11];
z[12] = z[1] + (T(-1) / T(2)) * z[7];
z[13] = abb[7] * z[4];
z[13] = z[6] + -z[12] + z[13];
z[14] = abb[1] * z[13];
z[8] = z[8] + z[11] + (T(-1) / T(2)) * z[14];
z[8] = abb[11] * z[8];
z[14] = z[6] + (T(-1) / T(3)) * z[7];
z[1] = z[1] + -z[5] + 2 * z[14];
z[1] = abb[2] * z[1];
z[14] = 2 * abb[7];
z[4] = -z[4] + z[14];
z[4] = abb[7] * z[4];
z[4] = z[4] + -z[6] + z[9];
z[4] = abb[1] * z[4];
z[2] = z[2] + (T(-4) / T(3)) * z[7] + z[10];
z[2] = abb[0] * z[2];
z[1] = z[1] + z[2] + z[4];
z[1] = abb[10] * z[1];
z[2] = -z[6] + (T(-1) / T(6)) * z[7];
z[4] = abb[8] * (T(-1) / T(4)) + -z[14];
z[4] = abb[7] * z[4];
z[2] = (T(1) / T(2)) * z[2] + z[3] + z[4];
z[2] = abb[1] * z[2];
z[3] = -z[5] + z[12];
z[4] = abb[2] * z[3];
z[2] = z[2] + (T(1) / T(2)) * z[4] + z[11];
z[2] = abb[12] * z[2];
z[4] = abb[4] * z[13];
z[3] = abb[3] * z[3];
z[3] = -z[3] + z[4];
z[4] = abb[13] + abb[14];
z[3] = -z[3] * z[4];
z[5] = abb[1] + abb[2];
z[6] = -abb[5] + abb[0] * (T(1) / T(2)) + (T(3) / T(4)) * z[5];
z[6] = z[4] * z[6];
z[7] = abb[3] + abb[4];
z[9] = abb[11] * (T(1) / T(4));
z[10] = abb[10] * (T(-1) / T(2)) + abb[12] * (T(-1) / T(4)) + -z[9];
z[10] = z[7] * z[10];
z[10] = -z[6] + z[10];
z[10] = abb[15] * z[10];
z[1] = z[1] + z[2] + (T(3) / T(2)) * z[3] + z[8] + 3 * z[10];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[6] * m1_set::bc<T>[0];
z[3] = (T(1) / T(2)) * z[0] + z[2];
z[4] = -z[3] * z[4] * z[7];
z[5] = z[3] * z[5];
z[3] = abb[0] * z[3];
z[3] = z[3] + (T(1) / T(2)) * z[5];
z[8] = -abb[11] * z[3];
z[9] = -z[7] * z[9];
z[6] = -z[6] + z[9];
z[6] = abb[9] * z[6];
z[7] = abb[9] * z[7];
z[3] = -z[3] + (T(-3) / T(4)) * z[7];
z[3] = abb[12] * z[3];
z[0] = -z[0] + -2 * z[2];
z[0] = abb[0] * z[0];
z[0] = z[0] + -z[5] + (T(-3) / T(2)) * z[7];
z[0] = abb[10] * z[0];
z[0] = z[0] + z[3] + (T(3) / T(2)) * z[4] + 3 * z[6] + z[8];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_162_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.162273066096953499292856997926669629182883935648798084600840359"),stof<T>("14.633300484517895006119187001685607389125752937927857609262389682")}, std::complex<T>{stof<T>("-12.566746881450349585252947280583076751192871383858558146164888611"),stof<T>("7.316650242258947503059593500842803694562876468963928804631194841")}, std::complex<T>{stof<T>("-12.566746881450349585252947280583076751192871383858558146164888611"),stof<T>("7.316650242258947503059593500842803694562876468963928804631194841")}, std::complex<T>{stof<T>("-5.6625643406453896979536410169604529201995250922469491639220986118"),stof<T>("9.221856769776943285659647924643806313592372772159790793087290699")}, std::complex<T>{stof<T>("-5.6625643406453896979536410169604529201995250922469491639220986118"),stof<T>("9.221856769776943285659647924643806313592372772159790793087290699")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_162_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_162_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("9.049908959211975710103564171440123557607626823606545456360578054"),stof<T>("-15.110600536661042078713527327565168833126323868433986670665659408")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,16> abb = {dl[0], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), f_2_25_re(k)};

                    
            return f_4_162_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_162_DLogXconstant_part(base_point<T>, kend);
	value += f_4_162_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_162_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_162_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_162_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_162_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_162_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_162_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
