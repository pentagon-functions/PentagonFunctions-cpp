/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_151.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_151_abbreviated (const std::array<T,28>& abb) {
T z[28];
z[0] = abb[19] * (T(1) / T(2));
z[1] = abb[20] + -abb[22];
z[2] = abb[18] + -z[0] + (T(1) / T(2)) * z[1];
z[3] = abb[2] * z[2];
z[4] = abb[23] + abb[24] + -abb[25];
z[5] = abb[8] * z[4];
z[6] = (T(1) / T(2)) * z[5];
z[7] = abb[21] + abb[22];
z[8] = -abb[20] + (T(1) / T(2)) * z[7];
z[9] = abb[18] * (T(-3) / T(2)) + z[8];
z[10] = abb[6] * z[9];
z[11] = abb[7] * z[4];
z[12] = abb[18] + z[1];
z[13] = abb[4] * z[12];
z[14] = abb[20] + -abb[21];
z[15] = abb[18] + (T(1) / T(2)) * z[14];
z[15] = abb[1] * z[15];
z[16] = abb[18] + abb[19] + z[14];
z[17] = (T(1) / T(2)) * z[16];
z[17] = abb[3] * z[17];
z[3] = z[3] + z[6] + z[10] + (T(1) / T(2)) * z[11] + (T(3) / T(2)) * z[13] + z[15] + z[17];
z[3] = prod_pow(abb[11], 2) * z[3];
z[15] = abb[22] * (T(1) / T(2));
z[17] = 2 * abb[18];
z[0] = abb[21] + abb[20] * (T(-3) / T(2)) + -z[0] + z[15] + -z[17];
z[0] = abb[2] * z[0];
z[2] = abb[0] * z[2];
z[18] = 2 * abb[3];
z[19] = abb[18] + -abb[19];
z[19] = z[18] * z[19];
z[7] = 2 * abb[20] + -z[7];
z[20] = 3 * abb[18] + z[7];
z[21] = abb[1] * z[20];
z[0] = z[0] + -z[2] + -z[6] + z[19] + z[21];
z[0] = abb[13] * z[0];
z[6] = abb[21] + abb[20] * (T(-5) / T(2)) + abb[22] * (T(3) / T(2));
z[19] = abb[19] * (T(1) / T(4));
z[6] = (T(1) / T(2)) * z[6] + -z[17] + z[19];
z[6] = abb[3] * z[6];
z[22] = abb[2] * z[16];
z[2] = z[2] + -z[22];
z[2] = (T(-1) / T(2)) * z[2] + (T(1) / T(4)) * z[11];
z[6] = z[2] + z[6] + -z[10] + -z[21];
z[23] = abb[11] * z[6];
z[24] = 4 * abb[18];
z[25] = 3 * abb[21] + abb[20] * (T(-11) / T(2)) + abb[22] * (T(5) / T(2));
z[19] = -z[19] + -z[24] + (T(1) / T(2)) * z[25];
z[19] = abb[3] * z[19];
z[25] = abb[5] * z[20];
z[10] = z[10] + -z[25];
z[2] = -z[2] + -z[10] + z[19] + -2 * z[21];
z[2] = abb[12] * z[2];
z[0] = z[0] + z[2] + z[23];
z[0] = abb[13] * z[0];
z[2] = -7 * abb[5] + abb[6];
z[8] = abb[18] * (T(1) / T(2)) + (T(-1) / T(3)) * z[8];
z[2] = z[2] * z[8];
z[8] = abb[1] * z[12];
z[12] = abb[18] * (T(5) / T(6)) + abb[19] * (T(13) / T(6)) + z[7];
z[12] = abb[3] * z[12];
z[2] = z[2] + (T(5) / T(6)) * z[8] + z[12] + (T(-4) / T(3)) * z[13] + (T(13) / T(6)) * z[22];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[6] = -abb[12] * z[6];
z[6] = z[6] + -z[23];
z[6] = abb[12] * z[6];
z[8] = 3 * abb[20] + z[24];
z[12] = 2 * abb[22];
z[19] = -abb[21] + z[8] + -z[12];
z[19] = abb[1] * z[19];
z[23] = abb[3] * z[20];
z[24] = abb[6] * z[20];
z[19] = z[13] + z[19] + z[23] + -z[24];
z[23] = -abb[11] * z[19];
z[12] = 7 * abb[18] + abb[21] * (T(-5) / T(2)) + abb[20] * (T(9) / T(2)) + -z[12];
z[12] = abb[1] * z[12];
z[26] = abb[3] * z[9];
z[10] = z[10] + z[12] + (T(-1) / T(2)) * z[13] + -3 * z[26];
z[10] = abb[14] * z[10];
z[12] = z[18] * z[20];
z[20] = z[24] + z[25];
z[12] = z[12] + -z[20] + 3 * z[21];
z[26] = -abb[12] + abb[13];
z[26] = z[12] * z[26];
z[10] = z[10] + z[23] + z[26];
z[10] = abb[14] * z[10];
z[23] = -abb[26] * z[19];
z[26] = -abb[27] * z[12];
z[27] = abb[8] + -abb[9];
z[9] = z[9] * z[27];
z[27] = abb[0] + abb[3];
z[27] = abb[10] + abb[2] * (T(-1) / T(2)) + (T(-3) / T(4)) * z[27];
z[4] = z[4] * z[27];
z[15] = abb[21] + abb[19] * (T(-3) / T(2)) + abb[20] * (T(-1) / T(2)) + -z[15];
z[15] = abb[7] * z[15];
z[4] = z[4] + z[9] + (T(1) / T(2)) * z[15];
z[4] = abb[15] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[6] + z[10] + z[23] + z[26];
z[2] = -z[14] + -z[17];
z[2] = abb[1] * z[2];
z[1] = -abb[19] + z[1] + z[17];
z[3] = -abb[2] * z[1];
z[4] = -abb[3] * z[16];
z[2] = z[2] + z[3] + z[4] + -z[5] + -z[11] + -3 * z[13] + z[24];
z[2] = abb[11] * z[2];
z[3] = abb[0] * z[1];
z[3] = z[3] + z[21];
z[1] = -abb[3] * z[1];
z[1] = z[1] + -z[3] + z[11] + 2 * z[22] + z[24] + -z[25];
z[1] = abb[12] * z[1];
z[4] = abb[18] + 2 * abb[19] + z[7];
z[4] = z[4] * z[18];
z[6] = abb[19] + -2 * abb[21] + -abb[22] + z[8];
z[6] = abb[2] * z[6];
z[3] = z[3] + z[4] + z[5] + z[6] + -z[20];
z[3] = abb[13] * z[3];
z[4] = abb[14] * z[19];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[19];
z[3] = -abb[17] * z[12];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_151_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("9.106003321762473658794660234463274284907291865535393050546718865"),stof<T>("-31.783190042406910703169750734971255981007200977261337679902550179")}, std::complex<T>{stof<T>("44.686185096598243211033028135603090005606276802531386155780851556"),stof<T>("-22.138655475694424244654642548608827584493555249503976297876662586")}, std::complex<T>{stof<T>("27.501769066320572612008485440547452076429342587422460015384637127"),stof<T>("-28.397558032418940993670742474486906021202270738850665041024863484")}, std::complex<T>{stof<T>("-26.290419352040144257819202929518912214084226080644319190942933294"),stof<T>("25.524287485682393954153650809093177544298485487914648936754349281")}, std::complex<T>{stof<T>("-1.2113497142804283541892825110285398623451165067781408244417038325"),stof<T>("2.8732705467365470395170916653937284769037852509360161042705142033")}, std::complex<T>{stof<T>("0.0777645199269873530365534850398990571898457862453410706965229878"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("0.0777645199269873530365534850398990571898457862453410706965229878"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-0.0777645199269873530365534850398990571898457862453410706965229878"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_151_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_151_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-43.217331886924722512974283501811867721515331255912683185274246782"),stof<T>("-38.529595657140733799085947506758195875129491340095888541719576694")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_151_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_151_DLogXconstant_part(base_point<T>, kend);
	value += f_4_151_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_151_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_151_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_151_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_151_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_151_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_151_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
