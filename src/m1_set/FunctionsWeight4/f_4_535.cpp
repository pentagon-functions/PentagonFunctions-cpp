/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_535.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_535_abbreviated (const std::array<T,64>& abb) {
T z[149];
z[0] = 2 * abb[49];
z[1] = 2 * abb[50];
z[2] = z[0] + z[1];
z[3] = 5 * abb[54];
z[4] = 2 * abb[47];
z[5] = 2 * abb[44];
z[6] = 2 * abb[52];
z[7] = abb[45] + abb[48] + abb[51] + z[2] + -z[3] + -z[4] + -z[5] + z[6];
z[7] = abb[3] * z[7];
z[8] = 4 * abb[44];
z[9] = 8 * abb[55];
z[10] = 2 * abb[45];
z[11] = 2 * abb[51];
z[12] = 6 * abb[52] + abb[56] + abb[58] + -z[4] + -z[8] + -z[9] + z[10] + z[11];
z[12] = abb[0] * z[12];
z[13] = -abb[47] + abb[48];
z[14] = abb[56] + z[13];
z[15] = (T(1) / T(2)) * z[14];
z[16] = 3 * abb[54];
z[17] = z[15] + -z[16];
z[18] = 3 * abb[58];
z[19] = 3 * abb[50] + -z[18];
z[20] = 3 * abb[49];
z[21] = z[6] + z[17] + z[19] + z[20];
z[21] = abb[6] * z[21];
z[22] = -abb[56] + z[18];
z[23] = 2 * abb[48];
z[24] = 4 * abb[55];
z[4] = z[4] + -z[22] + -z[23] + z[24];
z[25] = abb[10] * z[4];
z[26] = 4 * abb[52];
z[27] = 2 * abb[55];
z[17] = z[17] + z[26] + -z[27];
z[17] = abb[4] * z[17];
z[25] = -z[17] + z[25];
z[28] = 2 * abb[54];
z[29] = z[13] + -z[28];
z[30] = -z[24] + z[26];
z[31] = 2 * abb[58];
z[32] = -abb[56] + z[31];
z[33] = z[2] + z[29] + z[30] + -z[32];
z[34] = abb[14] * z[33];
z[35] = abb[48] + -abb[56];
z[36] = abb[47] + z[35];
z[37] = (T(1) / T(2)) * z[36];
z[38] = -abb[51] + z[37];
z[39] = abb[44] + z[38];
z[40] = abb[15] * z[39];
z[41] = -z[34] + z[40];
z[42] = 3 * abb[45];
z[43] = -5 * abb[44] + z[11] + z[42];
z[23] = -z[23] + z[27] + z[43];
z[23] = abb[9] * z[23];
z[44] = -z[6] + z[27];
z[45] = abb[49] + abb[50];
z[46] = -z[44] + z[45];
z[29] = z[29] + z[46];
z[47] = abb[13] * z[29];
z[48] = -abb[56] + z[13];
z[49] = -z[28] + z[48];
z[50] = z[31] + z[49];
z[51] = abb[8] * z[50];
z[52] = abb[23] + abb[25];
z[53] = abb[22] + z[52];
z[53] = abb[59] * z[53];
z[54] = z[51] + z[53];
z[55] = abb[44] + -abb[45];
z[56] = -abb[58] + z[45];
z[57] = z[55] + z[56];
z[57] = abb[12] * z[57];
z[58] = abb[7] * z[50];
z[59] = -abb[47] + abb[51];
z[60] = z[45] + z[59];
z[61] = 10 * abb[55];
z[62] = -10 * abb[44] + 11 * abb[45] + -9 * abb[54] + -z[60] + z[61];
z[62] = abb[1] * z[62];
z[63] = abb[24] * abb[59];
z[7] = z[7] + z[12] + -z[21] + -z[23] + z[25] + z[41] + 5 * z[47] + -z[54] + z[57] + -z[58] + z[62] + (T(-1) / T(2)) * z[63];
z[7] = abb[30] * z[7];
z[12] = abb[56] + z[2];
z[62] = 3 * abb[47];
z[64] = 6 * abb[58];
z[9] = -3 * abb[48] + z[9] + z[12] + -z[26] + z[28] + z[62] + -z[64];
z[65] = abb[14] * z[9];
z[66] = 6 * abb[54];
z[67] = -z[14] + z[66];
z[68] = 6 * abb[49];
z[69] = -6 * abb[50] + -z[26] + z[64] + z[67] + -z[68];
z[69] = abb[6] * z[69];
z[70] = 2 * abb[10];
z[71] = z[4] * z[70];
z[72] = 8 * abb[52];
z[67] = z[24] + z[67] + -z[72];
z[73] = abb[4] * z[67];
z[71] = z[71] + z[73];
z[74] = z[69] + z[71];
z[75] = 4 * abb[50];
z[76] = z[24] + z[75];
z[77] = 4 * abb[49];
z[78] = z[76] + z[77];
z[79] = -z[28] + -z[48] + -z[64] + z[78];
z[80] = abb[7] + abb[8];
z[79] = -z[79] * z[80];
z[81] = 2 * abb[3];
z[29] = -z[29] * z[81];
z[29] = z[29] + -4 * z[47] + z[53] + z[65] + -z[74] + z[79];
z[79] = abb[33] * z[29];
z[82] = z[5] + z[16] + -z[27];
z[83] = -abb[45] + -z[60] + z[82];
z[84] = 2 * abb[1];
z[83] = z[83] * z[84];
z[85] = abb[56] + -z[44];
z[86] = -abb[58] + -z[55] + z[85];
z[87] = 2 * abb[0];
z[86] = z[86] * z[87];
z[88] = z[11] + -z[36];
z[89] = -z[5] + z[88];
z[90] = abb[15] * z[89];
z[90] = z[63] + z[90];
z[91] = 2 * z[57];
z[83] = -z[83] + z[86] + -z[90] + -z[91];
z[86] = 2 * z[47];
z[51] = z[51] + -z[86];
z[92] = z[34] + z[51] + z[53];
z[93] = -abb[45] + z[45];
z[94] = -abb[54] + z[59] + z[93];
z[95] = z[81] * z[94];
z[95] = -z[58] + z[83] + -z[92] + z[95];
z[96] = -abb[29] + abb[31] + -abb[32];
z[95] = z[95] * z[96];
z[7] = z[7] + z[79] + z[95];
z[7] = abb[30] * z[7];
z[79] = abb[49] + abb[57];
z[95] = abb[46] + z[1] + -z[18] + z[44] + z[79];
z[96] = -abb[8] * z[95];
z[97] = abb[46] + -abb[53];
z[37] = z[37] + z[97];
z[37] = abb[5] * z[37];
z[32] = z[32] + -z[46];
z[32] = abb[17] * z[32];
z[46] = -z[32] + z[37];
z[98] = 2 * abb[46];
z[99] = -z[0] + z[98];
z[100] = abb[52] + abb[57];
z[101] = -abb[47] + abb[53];
z[102] = abb[50] + z[101];
z[103] = -abb[58] + -z[99] + z[100] + -z[102];
z[103] = abb[3] * z[103];
z[104] = -abb[52] + abb[54];
z[105] = -z[27] + z[104];
z[106] = z[18] + z[48];
z[107] = abb[46] + abb[50];
z[108] = -z[105] + -z[106] + z[107];
z[108] = abb[10] * z[108];
z[109] = (T(1) / T(2)) * z[48];
z[110] = abb[57] + z[109];
z[111] = -abb[52] + z[110];
z[112] = abb[16] * z[111];
z[113] = abb[27] * abb[59];
z[114] = -z[112] + (T(1) / T(2)) * z[113];
z[115] = abb[26] * abb[59];
z[116] = z[63] + z[115];
z[117] = -abb[58] + -z[49] + -z[107];
z[117] = abb[7] * z[117];
z[118] = abb[22] * abb[59];
z[96] = -z[17] + z[34] + -z[46] + z[96] + z[103] + z[108] + -z[114] + z[116] + z[117] + z[118];
z[96] = abb[34] * z[96];
z[103] = 4 * abb[46];
z[108] = z[66] + z[103];
z[117] = z[31] + z[48];
z[77] = z[24] + z[77] + -z[108] + z[117];
z[77] = abb[7] * z[77];
z[12] = -12 * abb[52] + 16 * abb[55] + -z[12] + -5 * z[13] + -z[31] + z[66];
z[12] = abb[14] * z[12];
z[119] = abb[47] + abb[56];
z[120] = abb[48] + z[119];
z[121] = 2 * abb[53];
z[122] = z[120] + -z[121];
z[123] = -z[0] + -z[30] + z[31] + -z[122];
z[123] = abb[3] * z[123];
z[124] = -z[98] + z[121];
z[125] = -z[36] + z[124];
z[126] = abb[5] * z[125];
z[127] = z[116] + z[126];
z[128] = z[118] + z[127];
z[129] = 2 * z[32];
z[12] = z[12] + -z[71] + z[77] + z[123] + -z[128] + -z[129];
z[12] = abb[33] * z[12];
z[77] = z[1] + z[98];
z[123] = -abb[57] + z[77];
z[130] = z[27] + z[104];
z[106] = z[106] + -z[123] + -z[130];
z[106] = z[70] * z[106];
z[131] = z[106] + z[113];
z[132] = z[34] + z[118];
z[133] = 2 * abb[57];
z[134] = z[48] + z[133];
z[135] = -z[6] + z[134];
z[136] = abb[16] * z[135];
z[116] = z[116] + z[136];
z[137] = z[116] + z[132];
z[117] = -z[6] + z[99] + z[117];
z[117] = abb[3] * z[117];
z[138] = -z[58] + z[129];
z[139] = 4 * abb[8];
z[140] = -abb[52] + z[107];
z[141] = abb[55] + -abb[58] + z[140];
z[142] = z[139] * z[141];
z[117] = z[117] + z[131] + -z[137] + -z[138] + z[142];
z[142] = abb[29] + -abb[32];
z[117] = z[117] * z[142];
z[12] = z[12] + z[96] + z[117];
z[12] = abb[34] * z[12];
z[96] = -abb[54] + abb[58];
z[117] = z[96] + z[109];
z[117] = abb[7] * z[117];
z[143] = abb[1] * z[94];
z[117] = -z[32] + -z[40] + z[117] + -z[143];
z[144] = 3 * abb[52];
z[145] = -z[16] + -z[27] + z[144];
z[146] = 3 * abb[46];
z[19] = z[19] + -z[134] + -z[145] + z[146];
z[19] = abb[10] * z[19];
z[44] = abb[58] + -z[44] + -z[55] + -z[123];
z[44] = abb[8] * z[44];
z[5] = -z[5] + z[10];
z[134] = -z[5] + z[85] + -z[107];
z[134] = abb[0] * z[134];
z[93] = -abb[58] + z[93];
z[147] = abb[51] + abb[52] + -abb[57] + -z[35] + z[93];
z[147] = abb[3] * z[147];
z[148] = (T(1) / T(2)) * z[115];
z[17] = z[17] + z[19] + z[23] + z[44] + -z[114] + -z[117] + z[134] + z[147] + z[148];
z[17] = abb[29] * z[17];
z[19] = z[84] * z[94];
z[44] = abb[22] + (T(1) / T(2)) * z[52];
z[44] = abb[59] * z[44];
z[41] = -z[19] + -z[41] + z[44] + z[63] + -z[114] + z[148];
z[44] = 4 * abb[58];
z[76] = -z[44] + z[76] + z[103] + -z[110] + -z[144];
z[76] = abb[8] * z[76];
z[84] = abb[52] + z[119];
z[94] = -abb[44] + -abb[51] + z[10];
z[110] = z[84] + z[94];
z[134] = -z[31] + z[110];
z[134] = abb[0] * z[134];
z[91] = -z[91] + z[134];
z[134] = z[6] + z[94];
z[119] = abb[48] + -3 * z[119];
z[119] = abb[57] + z[31] + (T(1) / T(2)) * z[119] + -z[134];
z[119] = abb[3] * z[119];
z[76] = -z[41] + z[76] + z[91] + z[106] + z[119] + -z[129];
z[76] = abb[32] * z[76];
z[17] = z[17] + z[76];
z[17] = abb[29] * z[17];
z[76] = (T(1) / T(2)) * z[120];
z[106] = -abb[57] + z[76];
z[94] = z[94] + z[99] + z[106];
z[94] = abb[3] * z[94];
z[52] = abb[59] * z[52];
z[99] = z[52] + -z[115];
z[115] = abb[8] * z[111];
z[19] = -z[19] + -z[40] + z[58] + -z[91] + z[94] + (T(1) / T(2)) * z[99] + z[114] + z[115];
z[19] = abb[32] * z[19];
z[40] = abb[53] + z[55] + -z[76] + z[79] + -z[98];
z[40] = abb[3] * z[40];
z[76] = abb[46] + -abb[49];
z[79] = -z[55] + z[76] + -z[111];
z[79] = abb[8] * z[79];
z[91] = abb[54] + -z[107] + -z[109];
z[91] = abb[7] * z[91];
z[59] = abb[45] + -z[59] + -z[140];
z[59] = abb[0] * z[59];
z[94] = (T(1) / T(2)) * z[52];
z[37] = z[37] + z[40] + z[59] + z[79] + z[91] + -z[94] + -z[114] + z[143];
z[37] = abb[31] * z[37];
z[40] = -z[5] + z[31] + -z[100] + z[109];
z[40] = abb[8] * z[40];
z[59] = z[13] + z[107] + -z[130];
z[79] = z[59] * z[70];
z[91] = -abb[48] + -abb[56] + z[62];
z[91] = (T(1) / T(2)) * z[91] + -z[123] + z[134];
z[91] = abb[3] * z[91];
z[99] = z[77] + -z[110];
z[99] = abb[0] * z[99];
z[40] = z[40] + z[41] + -z[79] + z[91] + z[99];
z[40] = abb[29] * z[40];
z[41] = -abb[54] + z[102];
z[41] = z[41] * z[81];
z[91] = -abb[54] + abb[55];
z[99] = -z[76] + z[91];
z[100] = 4 * abb[7];
z[99] = z[99] * z[100];
z[41] = z[41] + -z[92] + z[99] + -z[127];
z[41] = abb[33] * z[41];
z[99] = z[49] + z[77];
z[99] = abb[7] * z[99];
z[79] = z[79] + z[99] + z[113];
z[0] = z[0] + z[133];
z[99] = z[0] + z[6] + -z[103];
z[14] = -z[1] + -z[14] + z[99];
z[14] = abb[3] * z[14];
z[102] = abb[58] + z[76];
z[110] = -abb[57] + z[102];
z[114] = abb[8] * z[110];
z[14] = z[14] + -z[79] + 2 * z[114] + z[137];
z[14] = abb[34] * z[14];
z[14] = z[14] + z[41];
z[19] = -z[14] + z[19] + z[37] + z[40];
z[19] = abb[31] * z[19];
z[37] = abb[58] + -z[10] + -z[77] + z[82] + -z[109];
z[37] = abb[8] * z[37];
z[22] = -z[22] + -z[55] + z[77];
z[22] = abb[0] * z[22];
z[40] = -z[52] + -z[63];
z[38] = -z[38] + z[93];
z[38] = abb[3] * z[38];
z[21] = z[21] + z[22] + z[37] + z[38] + (T(1) / T(2)) * z[40] + -3 * z[57] + -z[117];
z[22] = prod_pow(abb[32], 2);
z[21] = z[21] * z[22];
z[37] = -abb[48] + abb[51];
z[37] = -abb[44] + z[24] + 4 * z[37] + -z[42];
z[37] = abb[9] * z[37];
z[38] = abb[14] * z[67];
z[26] = -abb[47] + abb[56] + z[26];
z[40] = 2 * z[26] + z[43] + -z[61];
z[40] = abb[0] * z[40];
z[41] = abb[3] * z[89];
z[42] = -z[55] + z[91];
z[43] = abb[1] * z[42];
z[37] = z[37] + z[38] + z[40] + z[41] + 6 * z[43] + -z[73] + -z[90] + -z[118];
z[37] = abb[35] * z[37];
z[38] = -abb[49] + abb[50] + z[98];
z[15] = z[6] + z[15] + -z[24] + z[38] + -z[96];
z[15] = abb[3] * z[15];
z[40] = 6 * abb[55];
z[41] = z[40] + z[68] + -z[121];
z[43] = -z[35] + -z[62];
z[1] = 6 * abb[46] + -abb[58] + z[1] + z[3] + -z[41] + (T(1) / T(2)) * z[43];
z[1] = abb[7] * z[1];
z[18] = -abb[54] + z[2] + -z[18] + z[27] + -z[109];
z[18] = abb[8] * z[18];
z[27] = z[6] + z[13];
z[43] = 3 * abb[55] + -z[27] + -z[96];
z[62] = 4 * abb[14];
z[43] = z[43] * z[62];
z[1] = z[1] + z[15] + z[18] + z[25] + z[32] + -z[43] + -z[47] + -z[94];
z[1] = abb[33] * z[1];
z[15] = z[69] + -z[129];
z[18] = z[100] + z[139];
z[25] = z[56] + z[91];
z[18] = z[18] * z[25];
z[25] = abb[3] * z[50];
z[18] = z[15] + z[18] + z[25] + z[86] + -z[132];
z[18] = abb[32] * z[18];
z[25] = abb[3] * z[33];
z[25] = z[25] + -z[43] + -z[51] + -z[52] + z[138];
z[32] = z[25] + z[71];
z[32] = abb[29] * z[32];
z[1] = z[1] + z[18] + z[32];
z[1] = abb[33] * z[1];
z[18] = 2 * abb[60];
z[32] = z[18] * z[95];
z[33] = z[95] * z[142];
z[43] = z[85] + -z[102];
z[43] = abb[33] * z[43];
z[33] = z[33] + z[43];
z[44] = -abb[56] + z[44] + -z[107] + -z[133];
z[44] = abb[34] * z[44];
z[33] = 2 * z[33] + z[44];
z[33] = abb[34] * z[33];
z[44] = abb[50] + abb[58] + -z[0] + z[146];
z[44] = abb[34] * z[44];
z[50] = -z[110] * z[142];
z[43] = z[43] + z[44] + z[50];
z[38] = abb[56] + z[38] + -z[133];
z[38] = abb[31] * z[38];
z[44] = z[38] + -2 * z[43];
z[44] = abb[31] * z[44];
z[26] = -abb[53] + -z[26];
z[20] = 5 * abb[46] + -z[20] + 2 * z[26] + z[61];
z[20] = abb[37] * z[20];
z[26] = -abb[58] + -z[6] + -z[101];
z[26] = -abb[49] + 2 * z[26] + z[40] + z[146];
z[26] = prod_pow(abb[33], 2) * z[26];
z[50] = abb[49] * (T(5) / T(6));
z[51] = abb[47] * (T(1) / T(2));
z[56] = -abb[56] + z[51];
z[56] = abb[57] + abb[46] * (T(-13) / T(6)) + abb[55] * (T(-11) / T(6)) + abb[50] * (T(-3) / T(2)) + abb[53] * (T(-1) / T(6)) + abb[58] * (T(2) / T(3)) + z[6] + z[50] + (T(1) / T(3)) * z[56];
z[61] = prod_pow(m1_set::bc<T>[0], 2);
z[56] = z[56] * z[61];
z[62] = -abb[29] + 2 * abb[32];
z[62] = abb[29] * z[62];
z[22] = z[22] + -z[62];
z[22] = -2 * z[22];
z[22] = z[22] * z[141];
z[20] = z[20] + z[22] + z[26] + z[32] + z[33] + z[44] + z[56];
z[20] = abb[2] * z[20];
z[22] = -z[31] + z[48] + -z[66] + z[78];
z[22] = -z[22] * z[80];
z[2] = z[2] + -z[10] + -z[31] + z[88];
z[2] = abb[3] * z[2];
z[2] = z[2] + -z[15] + z[22] + -z[52] + z[83];
z[2] = abb[36] * z[2];
z[15] = abb[50] + abb[55];
z[22] = abb[54] * (T(-8) / T(3)) + abb[58] * (T(1) / T(3)) + abb[57] * (T(2) / T(3)) + (T(-13) / T(6)) * z[55];
z[15] = abb[46] * (T(-3) / T(2)) + (T(-2) / T(3)) * z[15] + z[22] + (T(1) / T(6)) * z[48] + z[50] + z[144];
z[15] = abb[8] * z[15];
z[26] = 8 * z[47];
z[32] = z[26] + z[46] + -z[94] + z[112];
z[33] = abb[50] + -abb[53];
z[44] = abb[49] * (T(1) / T(2));
z[46] = abb[47] + abb[48];
z[46] = abb[56] + (T(1) / T(3)) * z[46];
z[22] = abb[52] * (T(-8) / T(3)) + abb[46] * (T(7) / T(6)) + -z[22] + (T(1) / T(3)) * z[33] + -z[44] + (T(1) / T(2)) * z[46];
z[22] = abb[3] * z[22];
z[33] = abb[50] + abb[51];
z[33] = (T(1) / T(2)) * z[33] + -z[51];
z[10] = abb[54] + -z[10] + z[33] + z[44];
z[44] = abb[55] * (T(1) / T(2));
z[10] = abb[44] * (T(1) / T(2)) + (T(1) / T(3)) * z[10] + -z[44];
z[10] = abb[1] * z[10];
z[46] = abb[46] * (T(1) / T(2));
z[33] = -abb[52] + abb[45] * (T(1) / T(2)) + -z[33] + -z[46];
z[33] = (T(1) / T(3)) * z[33] + z[44];
z[33] = abb[0] * z[33];
z[10] = z[10] + z[33];
z[33] = abb[53] + z[35];
z[3] = -abb[47] + -abb[58] + abb[50] * (T(5) / T(2)) + abb[49] * (T(7) / T(2)) + -z[3] + (T(1) / T(2)) * z[33];
z[3] = abb[55] * (T(7) / T(6)) + (T(1) / T(3)) * z[3] + -z[46];
z[3] = abb[7] * z[3];
z[33] = -abb[54] + z[13];
z[35] = abb[56] + -abb[57] + 8 * z[33] + z[72];
z[35] = -abb[58] + abb[55] * (T(-16) / T(3)) + (T(1) / T(3)) * z[35] + (T(11) / T(3)) * z[107];
z[35] = abb[10] * z[35];
z[3] = z[3] + 13 * z[10] + z[15] + z[22] + (T(-1) / T(3)) * z[32] + z[35] + (T(1) / T(6)) * z[113];
z[3] = z[3] * z[61];
z[10] = abb[61] * z[29];
z[9] = -abb[60] * z[9];
z[15] = -abb[37] * z[67];
z[9] = z[9] + z[15];
z[9] = abb[14] * z[9];
z[15] = z[120] + -z[124] + -z[133];
z[15] = abb[3] * z[15];
z[22] = z[84] + z[97] + -z[133];
z[32] = 2 * abb[2];
z[22] = z[22] * z[32];
z[35] = z[52] + z[126] + -z[136];
z[44] = abb[8] * z[135];
z[46] = abb[7] * z[125];
z[15] = z[15] + -z[22] + z[35] + z[44] + -z[46] + z[113];
z[22] = abb[62] * z[15];
z[30] = z[30] + z[49] + z[64] + -z[75] + -z[103];
z[30] = abb[8] * z[30];
z[30] = z[30] + z[53] + -z[86] + z[116];
z[44] = -z[30] + z[113];
z[44] = abb[60] * z[44];
z[25] = abb[38] * z[25];
z[36] = -z[36] + -z[41] + z[108];
z[36] = abb[7] * z[36];
z[36] = z[36] + z[128];
z[36] = abb[37] * z[36];
z[41] = -abb[18] + -abb[21];
z[41] = z[41] * z[111];
z[39] = abb[20] * z[39];
z[46] = abb[28] * abb[59];
z[47] = -abb[44] + abb[51] + -z[106];
z[47] = abb[19] * z[47];
z[39] = z[39] + z[41] + (T(1) / T(2)) * z[46] + z[47];
z[39] = abb[39] * z[39];
z[4] = abb[38] * z[4];
z[41] = z[13] + z[105] + z[123];
z[46] = -abb[60] * z[41];
z[4] = z[4] + z[46];
z[4] = z[4] * z[70];
z[18] = z[18] * z[59];
z[46] = -abb[37] * z[125];
z[18] = z[18] + z[46];
z[18] = abb[3] * z[18];
z[46] = abb[37] + abb[38] + abb[60];
z[46] = z[46] * z[73];
z[1] = abb[63] + z[1] + z[2] + z[3] + z[4] + z[7] + z[9] + z[10] + z[12] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[25] + z[36] + z[37] + z[39] + z[44] + z[46];
z[2] = abb[47] + -z[6];
z[2] = 3 * abb[44] + -abb[45] + 2 * z[2] + -z[11] + -z[31] + z[40];
z[2] = z[2] * z[87];
z[3] = -5 * abb[45] + z[8] + z[16] + -z[24] + z[60];
z[4] = 4 * abb[1];
z[3] = z[3] * z[4];
z[6] = 4 * z[57] + -z[58];
z[7] = 2 * z[23];
z[5] = 4 * abb[54] + -z[5] + -z[27] + -z[45];
z[5] = z[5] * z[81];
z[2] = z[2] + z[3] + z[5] + -z[6] + z[7] + -z[26] + z[34] + z[54] + -z[74];
z[2] = abb[30] * z[2];
z[3] = z[4] * z[42];
z[4] = z[3] + z[86];
z[5] = -z[55] + -z[104];
z[5] = z[5] * z[139];
z[8] = -z[76] + -z[104];
z[8] = z[8] * z[81];
z[9] = 4 * abb[0];
z[10] = -z[9] * z[141];
z[5] = z[4] + z[5] + z[6] + z[8] + z[10] + z[69] + z[116] + -z[131];
z[5] = abb[32] * z[5];
z[6] = z[73] + z[113];
z[8] = abb[57] + z[13] + z[145];
z[8] = z[8] * z[70];
z[10] = z[33] + z[140];
z[10] = z[10] * z[81];
z[11] = abb[0] * z[55];
z[3] = z[3] + z[6] + -z[7] + z[8] + z[10] + -6 * z[11] + -z[92] + -z[116];
z[3] = abb[29] * z[3];
z[0] = z[0] + z[49] + -z[98];
z[0] = abb[8] * z[0];
z[7] = z[28] + -z[99] + z[122];
z[7] = abb[3] * z[7];
z[8] = -abb[52] + abb[55];
z[8] = z[8] * z[9];
z[0] = z[0] + -z[4] + z[7] + z[8] + z[35] + z[79];
z[0] = abb[31] * z[0];
z[0] = z[0] + z[2] + z[3] + z[5] + z[14];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = z[59] * z[81];
z[3] = -z[41] * z[70];
z[2] = z[2] + z[3] + z[6] + -z[30] + -z[65];
z[2] = abb[40] * z[2];
z[3] = abb[41] * z[29];
z[4] = abb[42] * z[15];
z[5] = abb[40] * z[95];
z[6] = -z[38] + z[43];
z[6] = m1_set::bc<T>[0] * z[6];
z[5] = z[5] + z[6];
z[5] = z[5] * z[32];
z[0] = abb[43] + z[0] + z[2] + z[3] + z[4] + z[5];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_535_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("31.244583647361897648253408046726507638691732259922391291107062059"),stof<T>("-101.010687180429909005768940119884056250348315736316851507191568748")}, std::complex<T>{stof<T>("-57.26459218269924773497311995206891302868227301629667500362077607"),stof<T>("75.372515878242231993092766293039117342717031394772064798568368511")}, std::complex<T>{stof<T>("-9.410711739752162158929059958834209655609836892541888897246020822"),stof<T>("29.14911243269471205368091779820484953088819231406418174784744487")}, std::complex<T>{stof<T>("-7.980284789846829140891103032742586755453996011196002441224467241"),stof<T>("-18.70344260646602047792186135604564952139725645427407981166808747")}, std::complex<T>{stof<T>("-18.03972374549052094582860887259981863453654474517828127128924677"),stof<T>("-6.934728695721656534754312470799289386234027887270706896955112767")}, std::complex<T>{stof<T>("-15.532908392857661095280658559274947084285905971025127236833297768"),stof<T>("-11.245203273529609545397136177881869551921586682481371382148952786")}, std::complex<T>{stof<T>("-24.94362013260982325420971851810915673989574286356701613407931859"),stof<T>("17.903909159165102508283781620322979978966605631582810365698492083")}, std::complex<T>{stof<T>("26.020008535337350086719711905342405389990540756374283712513714011"),stof<T>("25.638171302187677012676173826844938907631284341544786708623200238")}, std::complex<T>{stof<T>("-10.760873059092412523257527299680710715846047484198644793814259346"),stof<T>("88.060744790987371173847250801054768061962963479275110587109594253")}, std::complex<T>{stof<T>("63.301908878788412641598797524714208892981284069846555517884539182"),stof<T>("-63.539658901733851567275641798393917143682286505912596354835844172")}, std::complex<T>{stof<T>("-42.481596864052308313403764385176266298052687851665631894005500307"),stof<T>("-36.289799799997883549739157887907211053443905540365887146986724783")}, std::complex<T>{stof<T>("-7.8096660889735223330593301290231286639647705593830221073502749734"),stof<T>("-5.2177975875694346952655363586154087649347047766373697958117282213")}, std::complex<T>{stof<T>("15.108892572745061412213892796520457261086464323838870134816341142"),stof<T>("8.354975580390796395232248397316582697642181201437503469674320267")}, std::complex<T>{stof<T>("25.624678438685113315946258883354414898228045110307170547837719663"),stof<T>("-2.337644545520443730328632302978504390276825602108864227892996658")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[41].real()/kbase.W[41].real()), rlog(k.W[86].real()/kbase.W[86].real()), C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_535_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_535_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (v[0] + -v[1] + v[2] + v[3] + 2 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -v[5])) / prod_pow(tend, 2);
c[1] = ((T(3) / T(16)) * (24 + 7 * v[0] + 5 * v[1] + 7 * v[2] + -5 * v[3] + -12 * v[4] + -4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -7 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[2] = (-2 * (1 + m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[3] = ((1 + 10 * m1_set::bc<T>[1]) * (T(-1) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * ((-abb[48] + abb[53] + abb[55]) * c[0] + abb[49] * c[1] + -abb[46] * (c[0] + c[1])) + abb[48] * c[2] + -abb[53] * c[2] + -abb[55] * c[2] + abb[49] * (c[2] + -c[3]) + abb[46] * c[3];
	}
	{
T z[5];
z[0] = -abb[48] + abb[53] + abb[55];
z[1] = 3 * abb[49];
z[2] = 5 * abb[46] + -2 * z[0] + -z[1];
z[3] = prod_pow(abb[34], 2);
z[4] = prod_pow(abb[33], 2);
z[3] = z[3] + -z[4];
z[2] = z[2] * z[3];
z[0] = abb[46] + -4 * z[0] + z[1];
z[0] = abb[37] * z[0];
z[0] = z[0] + z[2];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_535_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_535_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-42.262116433983970198763959731297750214480266592009802071972232474"),stof<T>("47.770749013124607519631741254067233723112320083978534953974679991")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,64> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W87(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[41].real()/k.W[41].real()), rlog(kend.W[86].real()/k.W[86].real()), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k), T{0}};
abb[43] = SpDLog_f_4_535_W_17_Im(t, path, abb);
abb[63] = SpDLog_f_4_535_W_17_Re(t, path, abb);

                    
            return f_4_535_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_535_DLogXconstant_part(base_point<T>, kend);
	value += f_4_535_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_535_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_535_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_535_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_535_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_535_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_535_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
