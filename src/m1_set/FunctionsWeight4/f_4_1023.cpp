/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_1023.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_1023_abbreviated (const std::array<T,84>& abb) {
T z[120];
z[0] = 2 * abb[64];
z[1] = 2 * abb[63];
z[2] = abb[65] + abb[66];
z[3] = z[0] + z[1] + -z[2];
z[4] = 2 * abb[0];
z[5] = z[3] * z[4];
z[6] = abb[25] * abb[67];
z[7] = abb[63] + abb[66];
z[8] = abb[13] * z[7];
z[9] = z[6] + -z[8];
z[10] = -abb[65] + abb[66];
z[11] = abb[63] + abb[64];
z[12] = z[10] + -z[11];
z[13] = abb[9] * z[12];
z[14] = abb[64] + abb[65];
z[15] = abb[12] * z[14];
z[16] = z[13] + -z[15];
z[17] = -abb[63] + abb[66];
z[18] = abb[14] * z[17];
z[19] = z[16] + z[18];
z[20] = z[10] + z[11];
z[21] = abb[6] * z[20];
z[22] = abb[64] + -abb[65];
z[23] = abb[11] * z[22];
z[24] = z[21] + z[23];
z[25] = 2 * abb[66];
z[26] = 5 * abb[64];
z[27] = -abb[63] + z[25] + -z[26];
z[27] = abb[10] * z[27];
z[28] = -abb[64] + abb[66];
z[29] = abb[3] * z[28];
z[30] = 3 * z[29];
z[31] = -abb[63] + abb[65];
z[32] = abb[2] * z[31];
z[33] = 3 * z[32];
z[34] = -abb[66] + z[11];
z[34] = abb[16] * z[34];
z[35] = 4 * z[34];
z[36] = abb[64] + abb[66];
z[37] = abb[4] * z[36];
z[38] = abb[63] + abb[65];
z[39] = abb[1] * z[38];
z[40] = 5 * abb[63];
z[41] = -abb[64] + 2 * abb[65] + -z[40];
z[41] = abb[5] * z[41];
z[42] = abb[27] * abb[67];
z[5] = z[5] + z[9] + z[19] + -z[24] + z[27] + z[30] + z[33] + z[35] + z[37] + z[39] + z[41] + z[42];
z[5] = abb[36] * z[5];
z[27] = z[2] + z[11];
z[41] = abb[8] * z[27];
z[42] = (T(1) / T(2)) * z[41];
z[43] = z[2] + -z[11];
z[44] = abb[7] * z[43];
z[45] = -z[42] + (T(1) / T(2)) * z[44];
z[46] = abb[67] * (T(1) / T(2));
z[47] = abb[26] * z[46];
z[48] = z[45] + z[47];
z[49] = -z[28] + z[31];
z[50] = abb[18] * z[49];
z[51] = abb[29] * abb[67];
z[50] = -z[50] + z[51];
z[51] = -abb[27] + abb[28];
z[51] = z[46] * z[51];
z[52] = z[48] + z[50] + z[51];
z[53] = z[18] + z[39];
z[54] = -abb[65] + z[28];
z[55] = 3 * abb[63];
z[56] = z[54] + -z[55];
z[57] = abb[5] * (T(1) / T(2));
z[58] = z[56] * z[57];
z[58] = -z[29] + z[58];
z[59] = abb[0] * z[43];
z[60] = 3 * abb[65];
z[61] = abb[64] + z[17] + z[60];
z[62] = abb[10] * (T(1) / T(2));
z[63] = -z[61] * z[62];
z[64] = 2 * z[32];
z[65] = abb[25] * z[46];
z[16] = -z[16] + z[52] + z[53] + z[58] + -z[59] + z[63] + z[64] + -z[65];
z[16] = abb[37] * z[16];
z[63] = abb[66] + z[22] + z[55];
z[66] = z[57] * z[63];
z[24] = z[24] + z[37];
z[51] = -z[24] + -z[51] + z[66];
z[47] = z[47] + z[65];
z[45] = z[45] + z[47];
z[66] = z[17] + z[22];
z[67] = abb[17] * z[66];
z[68] = z[8] + z[67];
z[69] = abb[0] * z[27];
z[70] = abb[63] + z[60];
z[71] = z[28] + z[70];
z[72] = z[62] * z[71];
z[73] = 2 * z[39];
z[72] = -z[32] + z[45] + -z[51] + -z[68] + z[69] + z[72] + -z[73];
z[72] = abb[32] * z[72];
z[74] = 4 * z[29] + -z[50];
z[75] = 2 * z[18];
z[76] = -z[67] + z[75];
z[77] = z[74] + -z[76];
z[78] = -z[41] + z[44];
z[79] = abb[26] * abb[67];
z[80] = z[78] + z[79];
z[81] = z[6] + z[80];
z[82] = 2 * abb[27] + abb[28];
z[82] = abb[67] * z[82];
z[83] = z[81] + z[82];
z[84] = 4 * z[32];
z[85] = z[83] + -z[84];
z[86] = abb[0] * z[11];
z[87] = 2 * z[86];
z[88] = 2 * z[23];
z[89] = z[87] + -z[88];
z[90] = 2 * z[8];
z[35] = z[35] + -z[90];
z[91] = 2 * abb[5];
z[92] = z[38] * z[91];
z[93] = z[0] + -z[31];
z[25] = -z[25] + z[93];
z[94] = 2 * abb[10];
z[25] = z[25] * z[94];
z[95] = 2 * z[15];
z[25] = z[25] + -z[35] + -z[77] + -z[85] + -z[89] + z[92] + -z[95];
z[25] = abb[31] * z[25];
z[3] = abb[0] * z[3];
z[92] = 2 * z[34];
z[96] = 2 * z[29];
z[97] = z[92] + z[96];
z[98] = z[0] + -z[10];
z[98] = abb[10] * z[98];
z[99] = abb[5] * z[1];
z[98] = -z[3] + z[8] + -z[32] + -z[53] + -z[97] + z[98] + z[99];
z[98] = abb[35] * z[98];
z[99] = z[8] + z[45];
z[100] = z[21] + -z[50];
z[101] = abb[27] + abb[28];
z[102] = z[46] * z[101];
z[103] = z[99] + z[100] + z[102];
z[104] = -z[23] + z[37];
z[105] = z[103] + -z[104];
z[106] = 3 * abb[64];
z[107] = -z[2] + z[55] + z[106];
z[107] = abb[0] * z[107];
z[108] = 7 * abb[63] + -5 * abb[65] + z[36];
z[108] = z[57] * z[108];
z[109] = -abb[66] + z[31];
z[26] = z[26] + z[109];
z[26] = z[26] * z[62];
z[26] = z[26] + -5 * z[32] + z[95] + z[105] + -z[107] + z[108];
z[26] = abb[38] * z[26];
z[108] = z[19] + z[65];
z[52] = -z[39] + z[52] + -z[108];
z[110] = 7 * abb[64] + -5 * abb[66] + z[38];
z[110] = z[62] * z[110];
z[40] = z[40] + z[54];
z[40] = z[40] * z[57];
z[35] = 5 * z[29] + z[35] + -z[40] + -z[52] + z[107] + -z[110];
z[40] = abb[39] * z[35];
z[54] = z[39] + z[102];
z[17] = abb[65] + -z[17] + z[106];
z[107] = z[17] * z[62];
z[48] = -z[48] + -z[54] + z[107];
z[71] = z[57] * z[71];
z[71] = -z[29] + z[59] + z[71];
z[107] = -z[67] + z[88];
z[108] = z[48] + z[71] + z[107] + z[108];
z[110] = -abb[33] * z[108];
z[111] = 3 * abb[66];
z[112] = abb[64] + z[111];
z[113] = z[31] + z[112];
z[114] = z[62] * z[113];
z[114] = -z[32] + z[59] + z[114];
z[51] = z[51] + -z[76] + -z[99] + z[114];
z[99] = abb[34] * z[51];
z[115] = -abb[65] + z[11];
z[116] = abb[38] * z[115];
z[117] = abb[36] * z[115];
z[118] = z[116] + -z[117];
z[119] = 4 * abb[15];
z[118] = z[118] * z[119];
z[5] = z[5] + z[16] + z[25] + z[26] + z[40] + z[72] + z[98] + z[99] + z[110] + -z[118];
z[5] = abb[35] * z[5];
z[16] = z[44] + z[79];
z[25] = z[16] + -z[41];
z[40] = -z[32] + z[39];
z[44] = -z[8] + z[29];
z[55] = -abb[64] + -z[55];
z[55] = abb[5] * z[55];
z[72] = abb[27] + 2 * abb[28];
z[72] = abb[67] * z[72];
z[79] = -abb[63] + -z[106];
z[79] = abb[10] * z[79];
z[19] = z[6] + -z[19] + z[24] + 2 * z[25] + z[40] + -z[44] + z[55] + z[72] + z[79] + z[87];
z[19] = abb[32] * z[19];
z[24] = z[57] * z[61];
z[24] = z[24] + z[69] + -z[95];
z[16] = (T(3) / T(2)) * z[16];
z[41] = -z[16] + (T(3) / T(2)) * z[41];
z[55] = -3 * z[31] + z[112];
z[55] = z[55] * z[62];
z[6] = (T(3) / T(2)) * z[6];
z[61] = abb[67] * z[101];
z[72] = (T(3) / T(2)) * z[61];
z[33] = -z[6] + -3 * z[8] + z[24] + z[33] + z[41] + z[55] + -z[72] + -z[100] + -z[104];
z[33] = abb[38] * z[33];
z[55] = z[57] + -z[62];
z[79] = -z[49] * z[55];
z[98] = z[29] + z[86];
z[99] = -z[8] + z[98];
z[101] = -z[15] + z[32];
z[47] = z[47] + (T(1) / T(2)) * z[78] + z[79] + -z[99] + -z[101] + z[102];
z[47] = abb[37] * z[47];
z[78] = -z[106] + z[109];
z[79] = z[62] * z[78];
z[32] = -z[32] + z[79];
z[79] = abb[63] + z[111];
z[22] = -z[22] + z[79];
z[102] = -z[22] * z[57];
z[102] = z[32] + -z[59] + z[96] + z[102] + z[103] + z[104];
z[102] = abb[36] * z[102];
z[19] = z[19] + z[33] + z[47] + z[102];
z[19] = abb[37] * z[19];
z[33] = -z[15] + z[67];
z[47] = z[57] * z[113];
z[102] = 2 * z[37];
z[47] = -z[13] + -z[18] + -z[29] + z[33] + z[47] + -z[48] + -z[65] + z[69] + -z[102];
z[47] = abb[36] * z[47];
z[48] = -z[55] * z[66];
z[55] = -z[18] + z[23];
z[45] = z[37] + z[45] + z[48] + z[54] + z[55] + -z[86];
z[45] = abb[32] * z[45];
z[24] = -z[24] + -z[32] + -z[105];
z[24] = abb[38] * z[24];
z[24] = z[24] + z[45] + z[47];
z[24] = abb[32] * z[24];
z[32] = z[23] + z[37] + -z[42];
z[37] = -z[60] + z[79] + z[106];
z[37] = z[37] * z[57];
z[42] = abb[27] + 3 * abb[28];
z[42] = z[42] * z[46];
z[6] = -z[6] + -z[16] + -z[21] + -3 * z[32] + z[37] + -z[42] + z[68] + z[75] + -z[114];
z[6] = abb[32] * z[6];
z[16] = -abb[36] + abb[37];
z[16] = z[16] * z[51];
z[32] = abb[5] * z[20];
z[37] = abb[28] * abb[67];
z[21] = z[21] + z[37];
z[32] = z[21] + -z[32] + z[81] + z[102] + z[107];
z[45] = abb[34] * z[32];
z[6] = z[6] + z[16] + z[45];
z[6] = abb[34] * z[6];
z[16] = z[87] + -z[95];
z[45] = z[16] + z[84];
z[47] = -z[28] + -2 * z[31];
z[47] = z[47] * z[91];
z[48] = z[36] * z[94];
z[47] = -z[37] + -z[45] + z[47] + z[48] + z[77] + -z[81] + -z[88] + -z[90];
z[47] = abb[36] * z[47];
z[48] = -z[8] + -z[15] + z[55];
z[51] = abb[5] * z[38];
z[36] = abb[10] * z[36];
z[36] = z[36] + z[48] + z[51] + z[59];
z[36] = abb[32] * z[36];
z[31] = abb[5] * z[31];
z[51] = abb[10] * z[28];
z[48] = -z[31] + -z[48] + -z[51] + -z[69];
z[48] = abb[37] * z[48];
z[36] = z[36] + z[48];
z[48] = abb[5] * z[12];
z[48] = -z[48] + -z[95] + z[96];
z[54] = z[61] + z[81];
z[55] = z[54] + z[90];
z[59] = z[55] + -z[64];
z[20] = abb[10] * z[20];
z[20] = z[20] + -z[59];
z[60] = -z[20] + -z[48];
z[60] = abb[31] * z[60];
z[68] = z[86] + z[101];
z[77] = abb[10] * abb[64];
z[79] = z[31] + z[68] + -z[77];
z[79] = abb[38] * z[79];
z[36] = 2 * z[36] + z[47] + z[60] + 4 * z[79] + z[118];
z[36] = abb[31] * z[36];
z[41] = z[13] + z[41] + -z[65];
z[22] = z[22] * z[62];
z[22] = z[22] + z[69] + -z[90];
z[47] = z[70] + z[106] + -z[111];
z[47] = z[47] * z[57];
z[30] = -3 * z[15] + z[22] + z[30] + z[41] + -z[42] + z[47] + -z[50] + -z[53];
z[30] = abb[37] * z[30];
z[42] = z[80] + z[90];
z[37] = -z[13] + z[37];
z[47] = -abb[5] * z[49];
z[53] = -z[28] * z[94];
z[47] = -z[16] + z[37] + z[42] + z[47] + z[53] + -z[74] + -z[92];
z[47] = abb[39] * z[47];
z[35] = -abb[36] * z[35];
z[22] = -z[22] + -z[52] + -z[58];
z[22] = abb[32] * z[22];
z[52] = abb[5] * abb[63];
z[51] = z[34] + z[51] + -z[52] + z[99];
z[53] = 4 * abb[31];
z[57] = z[51] * z[53];
z[22] = z[22] + z[30] + z[35] + z[47] + z[57];
z[22] = abb[39] * z[22];
z[30] = z[50] + -z[67];
z[9] = -z[9] + -z[25] + z[30] + -z[52] + -z[82];
z[25] = z[1] + -z[28];
z[25] = abb[20] * z[25];
z[28] = abb[64] + -z[1];
z[28] = -abb[66] + (T(1) / T(3)) * z[28];
z[28] = abb[10] * z[28];
z[9] = (T(1) / T(3)) * z[9] + (T(4) / T(3)) * z[25] + z[28] + (T(-7) / T(3)) * z[34] + (T(-2) / T(3)) * z[40] + z[75] + -z[98];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[28] = abb[43] * z[32];
z[32] = z[37] + -z[48] + z[50] + z[80];
z[32] = abb[42] * z[32];
z[35] = abb[19] * z[93];
z[23] = -z[23] + z[35];
z[35] = abb[15] * z[115];
z[14] = -abb[5] * z[14];
z[14] = z[14] + z[23] + -2 * z[35] + -z[68];
z[14] = abb[46] * z[14];
z[37] = prod_pow(abb[38], 2);
z[40] = -z[37] * z[115];
z[47] = 2 * z[116] + -z[117];
z[47] = abb[36] * z[47];
z[40] = z[40] + z[47];
z[40] = abb[15] * z[40];
z[9] = z[9] + z[14] + z[28] + z[32] + z[40];
z[1] = z[1] + z[10];
z[1] = abb[5] * z[1];
z[0] = abb[10] * z[0];
z[0] = z[0] + z[1] + -z[3] + z[15] + -z[29] + -z[64] + -z[104];
z[0] = abb[36] * z[0];
z[0] = z[0] + -z[26];
z[0] = abb[36] * z[0];
z[1] = -z[18] + z[39];
z[3] = abb[64] + 3 * z[38] + -z[111];
z[3] = z[3] * z[62];
z[1] = -3 * z[1] + z[3] + -z[33] + z[41] + -z[71] + -z[72] + -z[88];
z[1] = abb[32] * z[1];
z[3] = abb[10] * z[12];
z[3] = z[3] + -z[13] + z[61] + z[73] + -z[76] + z[80];
z[10] = abb[33] * z[3];
z[12] = abb[36] + abb[37];
z[12] = z[12] * z[108];
z[1] = z[1] + z[10] + z[12];
z[1] = abb[33] * z[1];
z[10] = -z[18] + z[34];
z[12] = z[10] + -z[25];
z[14] = abb[10] * abb[63];
z[14] = z[14] + z[86];
z[15] = -z[12] + -z[14];
z[15] = abb[68] * z[15];
z[23] = z[23] + -z[35];
z[26] = abb[5] * abb[64];
z[26] = -z[23] + z[26] + z[86];
z[26] = abb[40] * z[26];
z[28] = abb[10] * z[11];
z[28] = z[28] + z[52];
z[12] = -z[12] + -z[28];
z[12] = abb[71] * z[12];
z[11] = abb[5] * z[11];
z[11] = z[11] + -z[23] + z[77];
z[11] = abb[41] * z[11];
z[11] = z[11] + z[12] + z[15] + z[26];
z[12] = -abb[5] + abb[10];
z[15] = z[12] * z[66];
z[15] = z[15] + z[54] + z[73] + -z[75] + -z[89] + z[102];
z[15] = abb[44] * z[15];
z[3] = 2 * z[3];
z[23] = -abb[69] * z[3];
z[26] = abb[10] * z[49];
z[26] = z[26] + -2 * z[31] + -z[45] + z[55] + z[100];
z[26] = z[26] * z[37];
z[29] = abb[22] * z[43];
z[31] = abb[21] * z[27];
z[32] = abb[24] * z[49];
z[33] = abb[23] * z[66];
z[35] = abb[30] * abb[67];
z[29] = -z[29] + z[31] + -z[32] + -z[33] + z[35];
z[31] = abb[73] * z[29];
z[32] = abb[74] * z[56];
z[33] = abb[76] * z[63];
z[35] = abb[81] * z[78];
z[32] = z[32] + -z[33] + z[35];
z[33] = -z[18] + -z[25] + z[92] + z[99];
z[35] = -z[7] * z[94];
z[35] = -2 * z[33] + z[35];
z[35] = abb[72] * z[35];
z[20] = z[20] + -z[100];
z[37] = 2 * z[20];
z[38] = abb[70] * z[37];
z[12] = z[12] * z[49];
z[12] = z[12] + -z[16] + z[59] + -z[96];
z[12] = abb[45] * z[12];
z[16] = -abb[61] + -abb[62];
z[16] = z[16] * z[46];
z[40] = abb[77] + abb[78];
z[40] = (T(1) / T(2)) * z[40];
z[40] = z[27] * z[40];
z[41] = (T(1) / T(2)) * z[43];
z[45] = -abb[75] + -abb[80];
z[45] = z[41] * z[45];
z[17] = (T(1) / T(2)) * z[17];
z[46] = abb[79] * z[17];
z[0] = z[0] + z[1] + z[5] + z[6] + 2 * z[9] + 4 * z[11] + z[12] + z[15] + z[16] + z[19] + z[22] + z[23] + z[24] + z[26] + z[31] + (T(-1) / T(2)) * z[32] + z[35] + z[36] + z[38] + z[40] + z[45] + z[46];
z[1] = z[8] + z[39];
z[2] = z[2] * z[4];
z[4] = -z[27] * z[94];
z[1] = 4 * z[1] + -z[2] + z[4] + -z[30] + z[83];
z[1] = abb[32] * z[1];
z[4] = -abb[33] * z[3];
z[5] = -abb[66] * z[94];
z[5] = z[5] + -z[13] + z[21] + z[42] + -z[97];
z[5] = abb[36] * z[5];
z[6] = abb[38] * z[20];
z[5] = z[5] + z[6];
z[6] = z[43] * z[94];
z[2] = z[2] + z[6] + -4 * z[18] + -z[30] + z[85];
z[2] = abb[37] * z[2];
z[6] = abb[35] + -abb[39];
z[6] = 4 * z[6];
z[6] = z[6] * z[51];
z[8] = abb[10] * abb[66];
z[8] = z[8] + z[34] + z[44];
z[8] = z[8] * z[53];
z[1] = z[1] + z[2] + z[4] + 2 * z[5] + z[6] + z[8];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[56] + abb[57];
z[2] = z[2] * z[27];
z[4] = -abb[54] * z[43];
z[5] = abb[55] * z[63];
z[6] = -abb[60] * z[78];
z[8] = -abb[53] * z[56];
z[9] = abb[82] + abb[83];
z[9] = abb[67] * z[9];
z[2] = z[2] + z[4] + z[5] + z[6] + z[8] + z[9];
z[3] = -abb[48] * z[3];
z[4] = abb[52] * z[29];
z[5] = -z[10] + -z[28];
z[5] = abb[50] * z[5];
z[6] = -z[10] + -z[14];
z[6] = abb[47] * z[6];
z[8] = abb[47] + abb[50];
z[8] = z[8] * z[25];
z[5] = z[5] + z[6] + z[8];
z[6] = -abb[10] * z[7];
z[6] = z[6] + -z[33];
z[6] = abb[51] * z[6];
z[7] = abb[49] * z[37];
z[8] = -abb[59] * z[41];
z[9] = abb[58] * z[17];
z[1] = z[1] + (T(1) / T(2)) * z[2] + z[3] + z[4] + 4 * z[5] + 2 * z[6] + z[7] + z[8] + z[9];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_1023_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("35.754665871700723108794116484294208571246700813899137004967334256"),stof<T>("16.491633548675635874539296260905778385781835158327393429015731683")}, std::complex<T>{stof<T>("15.299172975284955523706137583204596817602776227268947722199118695"),stof<T>("7.253446518536916494954193164126049482150122995964867019835692216")}, stof<T>("-1.4913792718499973244846009065923632007163434473081581903679203258"), std::complex<T>{stof<T>("-1.07220200943060609760639042808394648759216621138800026996545093607"),stof<T>("0.66074942683782383613524747436354166550955942458559860959311808426")}, std::complex<T>{stof<T>("2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("-4.9667497776376699241622007318470126247813267979357331438214834646")}};
	
	std::vector<C> intdlogs = {rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_1023_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_1023_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("13.600584507896593966863618621101654072166295179926694175228125532"),stof<T>("10.990644009996832900333512828444708263012833906081216199880230083")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({199, 201});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,84> abb = {dl[0], dl[1], dlog_W3(k,dl), dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W58(k,dl), dlog_W69(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_3(k), f_2_8(k), f_2_14(k), f_2_16(k), f_2_17(k), f_2_20(k), f_2_21(k), f_2_4_im(k), f_2_6_im(k), f_2_10_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_6_re(k), f_2_10_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W163(k,dv) * f_2_31(k);
abb[74] = c.real();
abb[53] = c.imag();
SpDLog_Sigma5<T,199,162>(k, dv, abb[74], abb[53], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W166(k,dv) * f_2_31(k);
abb[75] = c.real();
abb[54] = c.imag();
SpDLog_Sigma5<T,199,165>(k, dv, abb[75], abb[54], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W168(k,dv) * f_2_33(k);
abb[76] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,201,167>(k, dv, abb[76], abb[55], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W170(k,dv) * f_2_33(k);
abb[77] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,201,169>(k, dv, abb[77], abb[56], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W177(k,dv) * f_2_31(k);
abb[78] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,199,176>(k, dv, abb[78], abb[57], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W179(k,dv) * f_2_31(k);
abb[79] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,199,178>(k, dv, abb[79], abb[58], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W181(k,dv) * f_2_33(k);
abb[80] = c.real();
abb[59] = c.imag();
SpDLog_Sigma5<T,201,180>(k, dv, abb[80], abb[59], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W184(k,dv) * f_2_33(k);
abb[81] = c.real();
abb[60] = c.imag();
SpDLog_Sigma5<T,201,183>(k, dv, abb[81], abb[60], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W190(k,dv) * f_2_31(k);
abb[82] = c.real();
abb[61] = c.imag();
SpDLog_Sigma5<T,199,189>(k, dv, abb[82], abb[61], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W192(k,dv) * f_2_33(k);
abb[83] = c.real();
abb[62] = c.imag();
SpDLog_Sigma5<T,201,191>(k, dv, abb[83], abb[62], f_2_33_series_coefficients<T>);
}

                    
            return f_4_1023_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_1023_DLogXconstant_part(base_point<T>, kend);
	value += f_4_1023_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_1023_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_1023_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_1023_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_1023_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_1023_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_1023_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
