/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_341.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_341_abbreviated (const std::array<T,27>& abb) {
T z[68];
z[0] = 2 * abb[20];
z[1] = 5 * abb[19] + z[0];
z[1] = 2 * z[1];
z[2] = 3 * abb[18];
z[3] = 3 * abb[21];
z[4] = z[1] + -z[2] + -z[3];
z[5] = abb[22] + abb[23];
z[6] = abb[24] + z[5];
z[7] = -z[4] + z[6];
z[8] = abb[6] * z[7];
z[9] = abb[9] * abb[26];
z[8] = z[8] + z[9];
z[9] = 2 * abb[19];
z[10] = abb[20] + z[9];
z[11] = 4 * z[10];
z[12] = abb[24] * (T(5) / T(2)) + -z[11];
z[13] = abb[23] + z[2];
z[14] = 2 * abb[22];
z[15] = -abb[21] + -z[12] + -z[13] + z[14];
z[15] = abb[2] * z[15];
z[16] = abb[24] + z[14];
z[17] = z[4] + -z[16];
z[17] = abb[4] * z[17];
z[18] = abb[18] + abb[21];
z[11] = z[11] + -z[18];
z[19] = 3 * abb[24];
z[20] = z[11] + -z[19];
z[21] = 2 * abb[1];
z[22] = z[20] * z[21];
z[23] = 5 * abb[22];
z[24] = 2 * z[10];
z[25] = -abb[24] + z[24];
z[26] = -abb[18] + -z[25];
z[26] = abb[21] + z[23] + 2 * z[26];
z[26] = abb[0] * z[26];
z[27] = 3 * abb[22];
z[9] = abb[24] * (T(3) / T(2)) + z[9];
z[28] = -z[9] + z[27];
z[29] = -abb[21] + -abb[23] + -z[28];
z[29] = abb[3] * z[29];
z[30] = abb[8] * abb[26];
z[31] = (T(1) / T(2)) * z[30];
z[32] = abb[7] * abb[26];
z[33] = (T(3) / T(2)) * z[32];
z[34] = abb[7] + abb[8];
z[35] = abb[25] * z[34];
z[15] = z[8] + z[15] + z[17] + z[22] + z[26] + z[29] + -z[31] + -z[33] + z[35];
z[15] = abb[11] * z[15];
z[17] = 2 * abb[18];
z[26] = 8 * z[10];
z[29] = 2 * abb[21];
z[35] = 5 * abb[24] + z[5] + z[17] + -z[26] + z[29];
z[36] = z[21] * z[35];
z[37] = z[8] + -z[32];
z[38] = abb[21] + abb[22];
z[39] = abb[19] + abb[20];
z[40] = -abb[24] + z[39];
z[40] = 2 * z[40];
z[41] = z[38] + z[40];
z[41] = abb[3] * z[41];
z[42] = abb[18] + abb[23];
z[40] = z[40] + z[42];
z[40] = abb[0] * z[40];
z[43] = 2 * abb[2];
z[44] = z[20] * z[43];
z[19] = -z[19] + z[24];
z[45] = z[19] + z[38];
z[46] = abb[4] * z[45];
z[19] = z[19] + z[42];
z[47] = abb[5] * z[19];
z[36] = -z[36] + z[37] + -z[40] + -z[41] + z[44] + -z[46] + -z[47];
z[40] = 2 * abb[14];
z[40] = z[36] * z[40];
z[41] = 4 * abb[1];
z[44] = z[20] * z[41];
z[44] = z[8] + z[44];
z[48] = 3 * z[30] + z[33];
z[46] = 2 * z[46];
z[49] = 2 * abb[25];
z[34] = z[34] * z[49];
z[50] = 2 * z[20];
z[51] = -abb[22] + abb[23];
z[52] = -z[50] + z[51];
z[52] = abb[2] * z[52];
z[53] = z[28] + z[29];
z[53] = abb[0] * z[53];
z[54] = 3 * abb[19];
z[0] = z[0] + z[54];
z[55] = abb[24] * (T(9) / T(2));
z[0] = 2 * z[0] + -z[55];
z[56] = abb[23] + z[14];
z[57] = z[0] + z[56];
z[57] = abb[3] * z[57];
z[52] = z[34] + -z[44] + z[46] + -z[48] + z[52] + z[53] + z[57];
z[52] = abb[12] * z[52];
z[15] = z[15] + -z[40] + z[52];
z[15] = abb[11] * z[15];
z[52] = 2 * abb[12];
z[36] = z[36] * z[52];
z[53] = 4 * abb[18];
z[57] = 4 * abb[21];
z[58] = 7 * abb[24];
z[59] = 5 * abb[23];
z[60] = 16 * z[10] + -z[23] + -z[53] + -z[57] + -z[58] + -z[59];
z[60] = z[21] * z[60];
z[35] = -z[35] * z[43];
z[61] = 9 * abb[19] + 4 * abb[20];
z[58] = -z[58] + 2 * z[61];
z[61] = abb[18] + z[3] + -z[58];
z[61] = abb[5] * z[61];
z[62] = 2 * z[39];
z[63] = abb[18] + z[62];
z[64] = abb[24] + -z[63];
z[64] = z[5] + 2 * z[64];
z[64] = abb[0] * z[64];
z[58] = abb[21] + z[2] + -z[58];
z[58] = abb[4] * z[58];
z[62] = abb[21] + z[62];
z[65] = abb[24] + -z[62];
z[65] = z[5] + 2 * z[65];
z[65] = abb[3] * z[65];
z[35] = z[35] + z[37] + z[58] + z[60] + z[61] + z[64] + z[65];
z[35] = abb[14] * z[35];
z[35] = z[35] + z[36];
z[35] = abb[14] * z[35];
z[36] = abb[22] + z[3];
z[58] = 2 * abb[23];
z[12] = -abb[18] + -z[12] + -z[36] + z[58];
z[12] = abb[2] * z[12];
z[4] = -abb[24] + z[4] + -z[58];
z[4] = abb[5] * z[4];
z[60] = -abb[18] + z[29];
z[61] = 2 * z[25];
z[64] = z[59] + -z[60] + -z[61];
z[64] = abb[3] * z[64];
z[65] = 3 * abb[23];
z[9] = -z[9] + z[65];
z[66] = -abb[18] + -abb[22] + -z[9];
z[66] = abb[0] * z[66];
z[67] = -abb[8] * abb[25];
z[4] = z[4] + z[12] + z[22] + z[31] + z[37] + z[64] + z[66] + z[67];
z[4] = abb[13] * z[4];
z[12] = -z[44] + 2 * z[47];
z[31] = -z[50] + -z[51];
z[31] = abb[2] * z[31];
z[44] = abb[22] + z[58];
z[0] = z[0] + z[44];
z[0] = abb[0] * z[0];
z[17] = z[9] + z[17];
z[17] = abb[3] * z[17];
z[47] = abb[8] * z[49];
z[0] = z[0] + z[12] + z[17] + z[31] + -z[47] + z[48];
z[0] = abb[12] * z[0];
z[17] = 9 * abb[24];
z[26] = z[5] + -z[17] + z[26];
z[26] = abb[2] * z[26];
z[28] = -z[28] + -z[58];
z[28] = abb[0] * z[28];
z[9] = -z[9] + -z[14];
z[9] = abb[3] * z[9];
z[9] = z[9] + -z[12] + z[26] + z[28] + -z[33] + -z[46];
z[9] = abb[11] * z[9];
z[0] = z[0] + z[4] + z[9] + -z[40];
z[0] = abb[13] * z[0];
z[4] = -abb[9] * z[7];
z[1] = abb[23] + -z[1] + z[55];
z[1] = abb[7] * z[1];
z[7] = abb[0] + abb[3];
z[9] = -abb[2] + abb[10];
z[9] = -z[7] + 2 * z[9];
z[9] = z[9] * z[49];
z[12] = 3 * abb[2] + -abb[6];
z[12] = abb[26] * z[12];
z[14] = abb[8] * z[51];
z[1] = z[1] + z[4] + z[9] + z[12] + z[14];
z[1] = abb[17] * z[1];
z[4] = 2 * abb[24];
z[9] = 7 * abb[19] + 3 * abb[20] + -z[4];
z[12] = -abb[18] + z[9];
z[12] = 2 * z[12];
z[14] = z[12] + -z[36];
z[14] = abb[2] * z[14];
z[14] = z[14] + -z[30] + z[37];
z[26] = abb[20] + z[54];
z[28] = -z[4] + z[26];
z[31] = -abb[18] + -z[28];
z[31] = -z[3] + 2 * z[31] + z[65];
z[31] = abb[5] * z[31];
z[33] = 15 * abb[19] + 7 * abb[20];
z[17] = -z[17] + 2 * z[33];
z[33] = -4 * abb[23] + z[17] + -z[36] + -z[53];
z[33] = z[21] * z[33];
z[14] = 2 * z[14] + z[31] + z[33];
z[14] = abb[16] * z[14];
z[31] = abb[7] * abb[25];
z[33] = z[31] + -z[32];
z[7] = -z[7] * z[20];
z[10] = z[10] + -z[18];
z[20] = abb[2] * z[10];
z[7] = z[7] + 4 * z[20] + z[22] + -3 * z[33];
z[7] = prod_pow(abb[12], 2) * z[7];
z[13] = -4 * abb[22] + -z[13] + z[17] + -z[57];
z[13] = abb[1] * z[13];
z[17] = z[2] + z[29];
z[9] = 2 * z[9] + -z[17];
z[20] = -abb[23] + z[9];
z[20] = abb[2] * z[20];
z[22] = abb[3] * z[45];
z[8] = z[8] + z[13] + z[20] + -z[22] + z[30];
z[13] = -z[17] + z[27] + -2 * z[28];
z[13] = abb[4] * z[13];
z[4] = -z[4] + 3 * z[39];
z[4] = 2 * z[4];
z[2] = abb[22] + -z[2] + -z[4];
z[2] = abb[0] * z[2];
z[2] = z[2] + 2 * z[8] + z[13];
z[2] = abb[15] * z[2];
z[8] = abb[18] * (T(13) / T(2));
z[13] = abb[21] * (T(13) / T(2));
z[17] = 28 * abb[19] + 13 * abb[20] + -abb[24];
z[20] = z[8] + z[13] + -2 * z[17];
z[22] = abb[22] * (T(13) / T(2));
z[27] = abb[23] * (T(13) / T(2));
z[20] = (T(1) / T(3)) * z[20] + z[22] + z[27];
z[20] = abb[1] * z[20];
z[28] = 13 * abb[21];
z[8] = z[8] + z[17] + -z[28];
z[8] = (T(1) / T(3)) * z[8] + -z[22];
z[8] = abb[0] * z[8];
z[22] = 13 * abb[18];
z[28] = -26 * abb[19] + -11 * abb[20] + z[22] + z[28];
z[28] = abb[2] * z[28];
z[28] = z[28] + -z[32];
z[8] = z[8] + z[20] + (T(1) / T(3)) * z[28];
z[20] = prod_pow(m1_set::bc<T>[0], 2);
z[8] = z[8] * z[20];
z[13] = z[13] + z[17] + -z[22];
z[13] = (T(1) / T(3)) * z[13] + -z[27];
z[13] = z[13] * z[20];
z[4] = abb[23] + -z[3] + -z[4];
z[4] = abb[16] * z[4];
z[17] = abb[17] * abb[26];
z[17] = (T(3) / T(2)) * z[17];
z[4] = z[4] + z[13] + z[17];
z[4] = abb[3] * z[4];
z[13] = abb[16] * z[19];
z[13] = -2 * z[13] + z[17];
z[13] = abb[0] * z[13];
z[0] = z[0] + z[1] + z[2] + z[4] + z[7] + z[8] + z[13] + z[14] + z[15] + z[35];
z[1] = -z[5] + -z[18] + z[61];
z[2] = -z[1] * z[43];
z[4] = -z[11] + z[16] + z[58];
z[4] = z[4] * z[41];
z[3] = -abb[23] + -z[3] + z[12];
z[3] = abb[5] * z[3];
z[5] = -abb[22] + z[9];
z[5] = abb[4] * z[5];
z[7] = -z[44] + z[63];
z[7] = abb[0] * z[7];
z[2] = z[2] + z[3] + z[4] + z[5] + z[7];
z[2] = m1_set::bc<T>[0] * z[2];
z[4] = -z[56] + z[62];
z[7] = abb[3] * m1_set::bc<T>[0];
z[4] = z[4] * z[7];
z[2] = z[2] + z[4];
z[2] = abb[14] * z[2];
z[4] = abb[1] * z[1];
z[4] = z[4] + z[32];
z[8] = z[25] + -z[38];
z[9] = abb[2] * z[8];
z[9] = z[4] + z[9] + z[30];
z[11] = -z[23] + 2 * z[26] + -z[60];
z[11] = abb[0] * z[11];
z[5] = -z[5] + 2 * z[9] + z[11] + -z[34];
z[5] = m1_set::bc<T>[0] * z[5];
z[6] = -z[6] + z[24];
z[6] = 2 * z[6];
z[9] = -z[6] * z[7];
z[5] = z[5] + z[9];
z[5] = abb[11] * z[5];
z[1] = z[1] * z[21];
z[9] = z[25] + -z[42];
z[11] = abb[2] * z[9];
z[11] = z[11] + -z[30];
z[6] = -abb[0] * z[6];
z[1] = z[1] + -z[3] + z[6] + 2 * z[11] + z[47];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[18] + z[26];
z[3] = abb[21] + 2 * z[3] + -z[59];
z[3] = z[3] * z[7];
z[1] = z[1] + z[3];
z[1] = abb[13] * z[1];
z[3] = -z[10] * z[43];
z[6] = abb[0] * z[8];
z[3] = z[3] + -z[4] + z[6] + z[31];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = z[7] * z[9];
z[3] = z[3] + z[4];
z[3] = z[3] * z[52];
z[1] = z[1] + z[2] + z[3] + z[5];
z[1] = 2 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_341_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-27.889403285087090801069980359840300133101474350624794681469490382"),stof<T>("-83.196400767844090906085274414735652250291181611233973436221401978")}, std::complex<T>{stof<T>("-72.82259149892357832230631600522619241414666033586994373781921442"),stof<T>("147.09821813557976877576959808236507505806231605827848685447006468")}, std::complex<T>{stof<T>("-51.485043156627675525378910142189396073097600436838178394929161717"),stof<T>("48.152360661795769158351087595667449725253277076219544555100510636")}, std::complex<T>{stof<T>("81.676400206767532035408011094789515680798742834561082087615104311"),stof<T>("-5.334667649647456319790451842255117641966227657540240624281705879")}, std::complex<T>{stof<T>("62.33087954390197187021557766297567441691197399731788961165602121"),stof<T>("4.011486417002503095075024048738249631372191753874525630165082433")}, std::complex<T>{stof<T>("-30.722961452271725654200051441313820896957881183938102022158180716"),stof<T>("-47.192666622593818842210453926689595238404321502839753834012979621")}, std::complex<T>{stof<T>("-6.2861617571935545640236204756924383267523504807230656289121200018"),stof<T>("-7.6929847562484529013792209737114797793863885202695222950595298166")}, std::complex<T>{stof<T>("-1.091630268192701708001828241307879024172744121136462991877465036"),stof<T>("-31.497863384434761426915167224305706600330124032957193082127117469")}, std::complex<T>{stof<T>("-1.586469303213321861709221708068931424263939161469902928506673507"),stof<T>("31.497863384434761426915167224305706600330124032957193082127117469")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_341_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_341_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("49.694277564353185052973548822507349488143457106271160765044407911"),stof<T>("83.653732130240188447931571566428040223531033874664911795469827396")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dlog_W3(k,dl), dl[2], dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_341_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_341_DLogXconstant_part(base_point<T>, kend);
	value += f_4_341_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_341_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_341_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_341_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_341_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_341_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_341_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
