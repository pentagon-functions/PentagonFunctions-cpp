/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_314.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_314_abbreviated (const std::array<T,29>& abb) {
T z[51];
z[0] = abb[20] + -abb[23];
z[1] = 2 * abb[21];
z[2] = 2 * abb[24] + -z[1];
z[3] = abb[18] + -abb[22];
z[4] = 4 * abb[19];
z[5] = z[0] + z[2] + z[3] + -z[4];
z[6] = abb[6] * z[5];
z[7] = abb[25] + -abb[26];
z[8] = abb[9] * z[7];
z[6] = z[6] + z[8];
z[8] = abb[8] * z[7];
z[9] = z[6] + -z[8];
z[10] = abb[21] + -abb[24];
z[11] = abb[23] + z[10];
z[12] = 3 * abb[19];
z[13] = 2 * abb[20];
z[14] = -z[11] + -z[12] + z[13];
z[15] = 2 * abb[5];
z[14] = z[14] * z[15];
z[16] = 4 * abb[18];
z[17] = 4 * abb[20];
z[18] = abb[22] + abb[23];
z[19] = 16 * abb[19] + 8 * abb[21] + -5 * abb[24] + -z[16] + -z[17] + z[18];
z[20] = abb[3] * z[19];
z[21] = abb[22] + z[10];
z[22] = 2 * abb[18];
z[23] = -z[12] + -z[21] + z[22];
z[24] = 2 * abb[4];
z[23] = z[23] * z[24];
z[25] = 4 * abb[21];
z[26] = 6 * abb[19] + -abb[24] + z[25];
z[27] = 3 * abb[20];
z[28] = -abb[18] + 2 * abb[22] + -z[26] + z[27];
z[28] = abb[0] * z[28];
z[29] = abb[18] + abb[20];
z[30] = 32 * abb[19] + 16 * abb[21] + -7 * abb[24] + -z[18] + -8 * z[29];
z[30] = abb[1] * z[30];
z[31] = 3 * abb[18];
z[26] = -abb[20] + 2 * abb[23] + -z[26] + z[31];
z[26] = abb[2] * z[26];
z[14] = z[9] + z[14] + z[20] + z[23] + z[26] + z[28] + z[30];
z[14] = prod_pow(abb[14], 2) * z[14];
z[20] = -abb[18] + abb[19];
z[23] = abb[4] * z[20];
z[26] = z[9] + -z[23];
z[13] = z[13] + -z[18];
z[28] = 3 * abb[24];
z[25] = -8 * abb[19] + z[13] + z[22] + -z[25] + z[28];
z[30] = abb[3] * z[25];
z[19] = abb[1] * z[19];
z[32] = -abb[24] + z[1];
z[33] = z[12] + z[32];
z[34] = -abb[20] + z[33];
z[34] = abb[0] * z[34];
z[33] = -abb[18] + z[33];
z[33] = abb[2] * z[33];
z[35] = abb[19] + -abb[20];
z[36] = abb[5] * z[35];
z[19] = z[19] + z[26] + -z[30] + -z[33] + -z[34] + -z[36];
z[33] = 2 * abb[14];
z[33] = z[19] * z[33];
z[25] = abb[1] * z[25];
z[34] = (T(3) / T(2)) * z[8];
z[30] = -z[6] + 2 * z[25] + z[30] + z[34];
z[37] = 2 * abb[19];
z[38] = z[1] + z[37];
z[39] = abb[18] * (T(1) / T(2));
z[40] = abb[24] * (T(3) / T(2));
z[41] = -abb[23] + z[39] + z[40];
z[42] = abb[20] * (T(1) / T(2));
z[43] = z[38] + -z[41] + z[42];
z[43] = abb[2] * z[43];
z[43] = 2 * z[36] + z[43];
z[40] = -abb[22] + z[40] + z[42];
z[1] = z[1] + z[4];
z[44] = abb[18] * (T(3) / T(2)) + -z[1] + z[40];
z[44] = abb[0] * z[44];
z[45] = z[30] + z[43] + -z[44];
z[45] = abb[13] * z[45];
z[38] = z[38] + z[39] + -z[40];
z[38] = abb[0] * z[38];
z[30] = 2 * z[23] + z[30] + z[38];
z[38] = -z[30] + -z[43];
z[38] = abb[11] * z[38];
z[4] = -z[4] + z[29];
z[40] = z[2] + z[4] + -z[18];
z[40] = abb[3] * z[40];
z[40] = z[25] + z[40];
z[43] = -abb[20] + z[11] + z[37];
z[15] = z[15] * z[43];
z[4] = z[4] + -z[32];
z[32] = abb[2] * z[4];
z[43] = abb[7] * z[7];
z[15] = z[15] + z[32] + -z[40] + -z[43];
z[32] = z[9] + z[15];
z[32] = abb[12] * z[32];
z[32] = z[32] + -z[33] + z[38] + z[45];
z[32] = abb[12] * z[32];
z[38] = abb[22] * (T(1) / T(2));
z[45] = abb[24] * (T(1) / T(3));
z[46] = abb[18] * (T(1) / T(6));
z[47] = abb[20] * (T(1) / T(6));
z[48] = abb[23] * (T(-1) / T(2)) + abb[21] * (T(1) / T(3)) + abb[19] * (T(2) / T(3)) + -z[38] + z[45] + -z[46] + -z[47];
z[48] = abb[1] * z[48];
z[49] = abb[21] * (T(1) / T(2));
z[50] = abb[19] * (T(11) / T(6)) + z[49];
z[38] = -abb[24] + z[38];
z[38] = abb[18] * (T(-7) / T(6)) + (T(1) / T(3)) * z[38] + -z[47] + z[50];
z[38] = abb[0] * z[38];
z[47] = abb[19] + -abb[22] + -abb[23] + -abb[24] + z[49];
z[39] = z[39] + z[42] + (T(1) / T(3)) * z[47];
z[39] = abb[3] * z[39];
z[42] = abb[20] * (T(-7) / T(6)) + abb[23] * (T(1) / T(6)) + -z[45] + -z[46] + z[50];
z[42] = abb[2] * z[42];
z[45] = abb[19] * (T(7) / T(2));
z[11] = 4 * z[11];
z[46] = abb[20] * (T(13) / T(2)) + -z[11];
z[46] = -z[45] + (T(1) / T(3)) * z[46];
z[46] = abb[5] * z[46];
z[21] = 4 * z[21];
z[47] = abb[18] * (T(13) / T(2)) + -z[21];
z[45] = -z[45] + (T(1) / T(3)) * z[47];
z[45] = abb[4] * z[45];
z[9] = (T(-1) / T(3)) * z[9] + z[38] + z[39] + z[42] + z[45] + z[46] + z[48];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[38] = abb[20] * (T(3) / T(2)) + -z[1] + z[41];
z[39] = abb[2] * z[38];
z[30] = z[30] + -z[39];
z[30] = abb[13] * z[30];
z[10] = -z[3] + z[10] + z[37];
z[10] = z[10] * z[24];
z[4] = abb[0] * z[4];
z[4] = z[4] + z[10] + -z[40] + z[43];
z[10] = z[4] + z[6];
z[10] = abb[11] * z[10];
z[10] = z[10] + z[30] + -z[33];
z[10] = abb[11] * z[10];
z[24] = abb[0] * z[35];
z[8] = -z[6] + z[8] + z[24];
z[24] = 12 * abb[19] + 5 * abb[21] + -z[28];
z[30] = -abb[22] + z[17] + -z[24] + z[31];
z[31] = 2 * abb[1];
z[30] = z[30] * z[31];
z[40] = 3 * abb[21] + -abb[24];
z[41] = -abb[23] + -z[22] + z[40];
z[42] = 9 * abb[19];
z[41] = abb[20] + 2 * z[41] + z[42];
z[41] = abb[2] * z[41];
z[2] = 5 * abb[19] + -z[2];
z[13] = -abb[18] + z[2] + -z[13];
z[45] = 2 * abb[3];
z[13] = z[13] * z[45];
z[11] = abb[20] + z[11] + z[12];
z[11] = abb[5] * z[11];
z[43] = 2 * z[43];
z[8] = -2 * z[8] + z[11] + z[13] + -z[30] + -z[41] + -z[43];
z[11] = -abb[28] * z[8];
z[13] = abb[21] + -z[29] + z[37];
z[13] = z[13] * z[45];
z[13] = z[13] + -z[25] + -z[34] + z[39] + z[44];
z[13] = abb[13] * z[13];
z[13] = z[13] + z[33];
z[13] = abb[13] * z[13];
z[29] = abb[2] * z[20];
z[29] = -z[6] + z[29];
z[30] = -abb[22] + z[40];
z[17] = abb[18] + -z[17] + 2 * z[30] + z[42];
z[17] = abb[0] * z[17];
z[16] = -abb[23] + z[16] + -z[24] + z[27];
z[16] = z[16] * z[31];
z[2] = -abb[20] + z[2] + z[18] + -z[22];
z[2] = z[2] * z[45];
z[12] = abb[18] + z[12] + z[21];
z[12] = abb[4] * z[12];
z[2] = z[2] + z[12] + -z[16] + -z[17] + -2 * z[29] + z[43];
z[12] = -abb[27] * z[2];
z[5] = -abb[9] * z[5];
z[16] = abb[8] * z[38];
z[17] = abb[0] + abb[2];
z[17] = -3 * abb[3] + -abb[6] + 6 * abb[10] + (T(-3) / T(2)) * z[17];
z[7] = z[7] * z[17];
z[0] = z[0] + -z[3];
z[0] = abb[7] * z[0];
z[0] = z[0] + z[5] + z[7] + z[16];
z[0] = abb[15] * z[0];
z[0] = z[0] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[32];
z[3] = abb[14] * z[19];
z[5] = abb[2] * z[35];
z[5] = z[5] + z[6] + -z[36];
z[1] = z[1] + z[18] + -z[28];
z[1] = abb[3] * z[1];
z[6] = abb[0] * z[20];
z[1] = z[1] + z[5] + z[6] + -z[23] + -z[25];
z[1] = abb[13] * z[1];
z[4] = -z[4] + -z[5];
z[4] = abb[11] * z[4];
z[5] = -z[6] + -z[15] + -z[26];
z[5] = abb[12] * z[5];
z[1] = z[1] + z[3] + z[4] + z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[2];
z[3] = -abb[17] * z[8];
z[1] = 2 * z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_314_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("7.499370481393428529613643864909206776407606765853323308413651831"),stof<T>("30.925185260231911121207769558381019047413615432257003277117091769")}, std::complex<T>{stof<T>("42.216715266864030124334445014411318774298620193757042889861416277"),stof<T>("-56.431647100018905478157875709729794450535405067495488775056624084")}, std::complex<T>{stof<T>("3.128120631044279442516169200329558015994916039926816086680606328"),stof<T>("39.184038852243588931132430335434875260433171862298276964317354233")}, std::complex<T>{stof<T>("23.730277588438377338269960790123016598083898396478660062078145007"),stof<T>("-15.281833559914695864347906795049705272452430732263689371594978874")}, std::complex<T>{stof<T>("-26.880210816872341450909897200596016269818166498766283936203828411"),stof<T>("-4.65572360152186764833772650702743528282549532619367129579613118")}, std::complex<T>{stof<T>("-28.047142876499479648595544777175144846893072748278675484952211386"),stof<T>("-13.006971778289777983726806804056996473558405155221492303361916016")}, std::complex<T>{stof<T>("1.0415736570350415015205919490605387750050481237538885681001826802"),stof<T>("1.9925591836775255289411045634991659495362591271776860663901126389")}, std::complex<T>{stof<T>("-4.5968061348999354552581905392268209704183843451340513010916107703"),stof<T>("-6.9160590414200418020718042420585732451309314383448089228716819912")}, std::complex<T>{stof<T>("4.5968061348999354552581905392268209704183843451340513010916107703"),stof<T>("6.9160590414200418020718042420585732451309314383448089228716819912")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_314_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_314_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("42.701588759907823751466344002883850371580380878801311685890182373"),stof<T>("-40.466838422179643029303309779121212607863054988428644863630012494")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_314_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_314_DLogXconstant_part(base_point<T>, kend);
	value += f_4_314_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_314_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_314_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_314_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_314_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_314_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_314_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
