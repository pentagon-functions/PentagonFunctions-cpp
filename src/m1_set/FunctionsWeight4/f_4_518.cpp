/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_518.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_518_abbreviated (const std::array<T,57>& abb) {
T z[90];
z[0] = abb[45] + abb[46];
z[1] = abb[44] * (T(1) / T(2));
z[2] = -abb[43] + -5 * z[0] + z[1];
z[3] = abb[50] * (T(3) / T(2));
z[4] = abb[47] * (T(1) / T(2));
z[2] = abb[48] + abb[42] * (T(5) / T(3)) + (T(1) / T(3)) * z[2] + -z[3] + z[4];
z[2] = abb[7] * z[2];
z[5] = abb[22] * abb[51];
z[6] = abb[25] * abb[51];
z[7] = z[5] + z[6];
z[8] = abb[44] + abb[50];
z[9] = -z[0] + z[8];
z[9] = -abb[43] + (T(1) / T(3)) * z[9];
z[9] = abb[2] * z[9];
z[10] = abb[21] * abb[51];
z[2] = z[2] + z[7] + z[9] + (T(5) / T(2)) * z[10];
z[9] = 2 * abb[44];
z[11] = abb[50] * (T(1) / T(2));
z[12] = (T(-5) / T(2)) * z[0] + -z[9] + -z[11];
z[13] = abb[49] * (T(1) / T(2));
z[12] = z[4] + (T(1) / T(3)) * z[12] + z[13];
z[12] = abb[6] * z[12];
z[14] = abb[42] + -abb[48];
z[15] = -abb[44] + abb[50];
z[16] = -abb[47] + z[15];
z[17] = (T(1) / T(2)) * z[16];
z[18] = -z[14] + z[17];
z[19] = abb[14] * z[18];
z[20] = abb[51] * (T(1) / T(2));
z[21] = abb[20] * z[20];
z[21] = -z[19] + z[21];
z[22] = z[0] + z[14];
z[23] = abb[44] + -abb[47] + z[22];
z[23] = abb[11] * z[23];
z[24] = z[1] + z[11];
z[25] = z[0] + z[24];
z[26] = abb[47] * (T(3) / T(2));
z[27] = -z[25] + z[26];
z[28] = abb[8] * z[27];
z[29] = -z[4] + z[24];
z[30] = -abb[43] + z[29];
z[31] = abb[15] * z[30];
z[32] = abb[50] * (T(3) / T(4));
z[33] = abb[47] * (T(3) / T(4));
z[34] = z[32] + z[33];
z[35] = 2 * z[0];
z[36] = abb[44] * (T(-7) / T(4)) + abb[43] * (T(-5) / T(2)) + -z[35];
z[36] = -abb[48] + abb[42] * (T(1) / T(6)) + z[34] + (T(1) / T(3)) * z[36];
z[36] = abb[4] * z[36];
z[37] = abb[50] * (T(19) / T(2)) + 10 * z[0] + z[1];
z[37] = abb[49] * (T(-13) / T(2)) + (T(7) / T(2)) * z[14] + (T(1) / T(3)) * z[37];
z[37] = abb[1] * z[37];
z[38] = -abb[43] + abb[50];
z[39] = -abb[49] + z[38];
z[39] = abb[9] * z[39];
z[40] = 3 * z[39];
z[41] = abb[23] * abb[51];
z[42] = abb[3] * z[16];
z[43] = -abb[49] + z[0];
z[44] = -abb[47] + abb[50] + z[43];
z[45] = abb[12] * z[44];
z[46] = 3 * z[45];
z[47] = abb[24] * z[20];
z[38] = -abb[42] + z[38];
z[48] = abb[0] * z[38];
z[2] = (T(1) / T(2)) * z[2] + z[12] + z[21] + -2 * z[23] + (T(-5) / T(2)) * z[28] + -z[31] + z[36] + z[37] + z[40] + (T(5) / T(4)) * z[41] + (T(-1) / T(4)) * z[42] + -z[46] + z[47] + (T(-13) / T(6)) * z[48];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[12] = abb[42] * (T(3) / T(2));
z[36] = abb[49] * (T(3) / T(2));
z[37] = abb[48] * (T(3) / T(2));
z[25] = -z[12] + -z[25] + z[36] + z[37];
z[25] = abb[1] * z[25];
z[49] = (T(1) / T(2)) * z[0];
z[50] = abb[42] * (T(1) / T(2)) + -z[37];
z[51] = -abb[43] + z[50];
z[52] = -abb[44] + -z[11] + -z[49] + -z[51];
z[52] = abb[4] * z[52];
z[53] = -abb[44] + z[0];
z[36] = -abb[50] + z[36] + (T(-1) / T(2)) * z[53];
z[36] = abb[13] * z[36];
z[54] = abb[51] * (T(3) / T(4));
z[55] = abb[20] * z[54];
z[55] = (T(3) / T(2)) * z[19] + -z[55];
z[56] = (T(3) / T(2)) * z[31];
z[57] = abb[6] * z[27];
z[58] = abb[43] + z[0] + z[15];
z[58] = -abb[42] + (T(1) / T(2)) * z[58];
z[58] = abb[7] * z[58];
z[59] = -abb[50] + z[0];
z[60] = 3 * abb[43] + -abb[44] + z[59];
z[61] = abb[2] * z[60];
z[62] = (T(1) / T(2)) * z[61];
z[63] = abb[47] + z[15];
z[63] = -abb[49] + (T(1) / T(2)) * z[63];
z[64] = abb[5] * z[63];
z[65] = -abb[25] * z[54];
z[25] = z[25] + -z[36] + -z[40] + 2 * z[48] + z[52] + -z[55] + z[56] + (T(-1) / T(2)) * z[57] + z[58] + z[62] + (T(3) / T(2)) * z[64] + z[65];
z[25] = abb[27] * z[25];
z[40] = abb[7] * z[27];
z[52] = z[40] + z[57];
z[58] = 3 * abb[49];
z[53] = 2 * abb[50] + z[53] + -z[58];
z[53] = 2 * z[53];
z[65] = abb[13] * z[53];
z[66] = 3 * z[64];
z[67] = z[46] + z[66];
z[68] = abb[4] * z[27];
z[69] = abb[51] * (T(3) / T(2));
z[70] = abb[22] * z[69];
z[71] = abb[24] * z[69];
z[65] = -z[52] + z[65] + -z[67] + z[68] + -z[70] + -z[71];
z[70] = abb[28] * z[65];
z[54] = abb[24] * z[54];
z[72] = (T(1) / T(2)) * z[48];
z[54] = z[54] + -z[56] + -z[61] + z[72];
z[56] = abb[22] * z[20];
z[73] = z[10] + z[56];
z[74] = abb[25] * z[20];
z[74] = z[73] + z[74];
z[59] = z[9] + z[59];
z[75] = 3 * abb[48];
z[76] = -3 * abb[42] + -z[59] + z[75];
z[77] = abb[1] * z[76];
z[74] = -z[54] + z[55] + (T(-3) / T(2)) * z[74] + -z[77];
z[78] = abb[50] * (T(5) / T(2)) + -z[26];
z[79] = abb[44] * (T(3) / T(2));
z[80] = -abb[43] + z[75] + z[78] + -z[79];
z[81] = 2 * abb[42];
z[80] = (T(1) / T(2)) * z[80] + -z[81];
z[80] = abb[4] * z[80];
z[82] = abb[50] * (T(1) / T(4));
z[83] = abb[44] * (T(3) / T(4)) + z[33] + z[51] + z[82];
z[83] = abb[7] * z[83];
z[27] = abb[13] * z[27];
z[80] = -z[27] + -z[74] + z[80] + z[83];
z[83] = -abb[31] * z[80];
z[25] = z[25] + z[70] + z[83];
z[25] = abb[27] * z[25];
z[70] = abb[44] * (T(5) / T(4)) + z[0];
z[33] = z[32] + -z[33] + z[51] + z[70];
z[33] = abb[7] * z[33];
z[83] = abb[43] * (T(1) / T(2));
z[84] = -4 * abb[42] + abb[44] * (T(-13) / T(4)) + abb[48] * (T(9) / T(2)) + z[34] + -z[35] + z[83];
z[84] = abb[4] * z[84];
z[85] = 3 * z[23];
z[33] = (T(-3) / T(4)) * z[7] + -z[33] + -z[54] + -z[55] + z[57] + 2 * z[77] + z[84] + z[85];
z[33] = abb[31] * z[33];
z[54] = z[27] + z[68];
z[55] = z[7] + z[10];
z[68] = 3 * z[31];
z[60] = abb[7] * z[60];
z[55] = z[54] + (T(-3) / T(2)) * z[55] + -z[57] + z[60] + 3 * z[61] + z[68] + -z[71];
z[60] = abb[30] * z[55];
z[8] = 3 * abb[47] + -z[8] + -z[35];
z[84] = abb[7] * z[8];
z[86] = abb[6] * z[8];
z[54] = z[54] + z[84] + z[86];
z[84] = abb[23] * z[69];
z[84] = 3 * z[28] + -z[84];
z[87] = z[54] + -z[84];
z[88] = abb[28] * z[87];
z[33] = z[33] + -z[60] + z[88];
z[50] = z[0] + z[1] + z[50] + z[83];
z[50] = abb[7] * z[50];
z[60] = -abb[44] + z[37];
z[12] = -z[11] + z[12] + z[49] + -z[60];
z[12] = abb[1] * z[12];
z[88] = -abb[43] + z[0];
z[60] = abb[42] + -z[60] + (T(1) / T(2)) * z[88];
z[60] = abb[4] * z[60];
z[12] = z[12] + (T(-3) / T(2)) * z[23] + z[50] + -z[57] + z[60] + z[62] + z[72];
z[12] = abb[32] * z[12];
z[50] = abb[27] * z[80];
z[57] = abb[29] * z[87];
z[12] = z[12] + -z[33] + z[50] + z[57];
z[12] = abb[32] * z[12];
z[50] = abb[7] * z[30];
z[31] = -z[31] + z[50] + -z[61];
z[11] = -z[11] + z[79];
z[50] = -z[0] + z[4];
z[57] = z[11] + -z[50];
z[57] = abb[4] * z[57];
z[60] = abb[3] * z[17];
z[62] = -abb[6] * z[17];
z[7] = (T(1) / T(2)) * z[7] + z[31] + z[47] + z[57] + z[60] + z[62];
z[7] = abb[34] * z[7];
z[57] = abb[23] * z[20];
z[57] = -z[28] + z[52] + z[57];
z[11] = z[11] + z[81];
z[62] = -2 * abb[48] + z[11] + -z[50];
z[62] = abb[4] * z[62];
z[72] = abb[21] * z[20];
z[62] = z[21] + z[23] + z[57] + -z[62] + z[72] + z[77];
z[72] = abb[54] * z[62];
z[79] = z[47] + z[64];
z[5] = z[5] + z[10];
z[80] = abb[13] * z[63];
z[80] = (T(1) / T(2)) * z[5] + z[79] + -z[80];
z[87] = abb[4] * z[18];
z[15] = abb[49] + z[14] + -z[15];
z[89] = -abb[1] * z[15];
z[21] = z[21] + z[80] + z[87] + z[89];
z[21] = abb[33] * z[21];
z[89] = 2 * abb[49];
z[3] = -z[1] + z[3] + -z[50] + -z[89];
z[3] = abb[13] * z[3];
z[3] = z[3] + -z[45];
z[6] = z[6] + z[10];
z[31] = z[3] + (T(1) / T(2)) * z[6] + z[31] + -z[64];
z[50] = abb[52] * z[31];
z[3] = -z[3] + z[56] + z[57] + z[79];
z[57] = -abb[53] * z[3];
z[7] = z[7] + z[21] + z[50] + -z[57] + z[72];
z[10] = (T(3) / T(2)) * z[10];
z[21] = z[10] + -z[27];
z[50] = abb[20] * z[69];
z[19] = 3 * z[19] + -z[50];
z[50] = 2 * abb[4];
z[57] = z[50] * z[76];
z[57] = -z[19] + z[21] + z[52] + z[57] + 3 * z[77] + z[85];
z[57] = abb[31] * z[57];
z[44] = abb[13] * z[44];
z[23] = z[23] + -z[41] + z[44] + -z[47] + -z[52] + -z[56] + z[87];
z[0] = abb[44] + z[0];
z[0] = (T(-1) / T(2)) * z[0] + z[13] + -z[14];
z[0] = abb[1] * z[0];
z[13] = (T(1) / T(2)) * z[64];
z[0] = z[0] + -z[13] + (T(1) / T(2)) * z[23] + z[28];
z[0] = abb[29] * z[0];
z[1] = z[1] + -z[35] + z[58] + -z[78];
z[1] = abb[13] * z[1];
z[14] = z[19] + z[46];
z[19] = -abb[4] * z[76];
z[23] = 3 * abb[1];
z[15] = z[15] * z[23];
z[1] = z[1] + -z[10] + z[14] + z[15] + z[19] + z[52];
z[1] = abb[27] * z[1];
z[10] = 3 * abb[28];
z[15] = z[3] * z[10];
z[0] = 3 * z[0] + z[1] + z[15] + z[57];
z[0] = abb[29] * z[0];
z[1] = -abb[27] * z[55];
z[15] = abb[24] * abb[51];
z[16] = abb[4] * z[16];
z[15] = z[15] + z[16];
z[16] = z[29] + z[43];
z[19] = abb[6] * (T(1) / T(2));
z[16] = z[16] * z[19];
z[13] = z[13] + (T(1) / T(4)) * z[15] + -z[16];
z[15] = abb[44] * (T(1) / T(4));
z[16] = abb[43] + abb[47] * (T(-1) / T(4)) + -z[15] + z[49] + -z[82];
z[16] = abb[7] * z[16];
z[19] = z[44] + z[73];
z[16] = -z[13] + z[16] + (T(-1) / T(2)) * z[19] + z[39] + z[60] + z[61];
z[16] = abb[30] * z[16];
z[19] = z[50] * z[59];
z[28] = abb[6] * z[59];
z[29] = (T(3) / T(2)) * z[42];
z[19] = z[19] + -z[21] + z[28] + z[29] + -z[40];
z[19] = abb[31] * z[19];
z[17] = abb[4] * z[17];
z[21] = abb[6] * z[63];
z[17] = z[17] + z[21] + z[80];
z[21] = z[17] + -z[60];
z[21] = z[10] * z[21];
z[1] = z[1] + 3 * z[16] + -z[19] + z[21];
z[1] = abb[30] * z[1];
z[9] = z[9] + z[88];
z[16] = z[9] + -z[75] + z[81];
z[16] = abb[7] * z[16];
z[9] = -abb[42] + z[9];
z[9] = z[9] * z[50];
z[9] = z[9] + z[16] + z[28] + -z[48] + -z[61] + z[77];
z[9] = prod_pow(abb[31], 2) * z[9];
z[16] = abb[36] * z[65];
z[21] = -z[40] + -z[56];
z[13] = -z[13] + (T(1) / T(2)) * z[21] + -z[36] + -z[45];
z[10] = z[10] * z[13];
z[10] = z[10] + z[19];
z[10] = abb[28] * z[10];
z[4] = -abb[43] + -abb[48] + z[4] + z[24];
z[4] = abb[17] * z[4];
z[13] = abb[16] + abb[18];
z[13] = z[13] * z[18];
z[18] = abb[19] * z[30];
z[19] = abb[26] * z[20];
z[4] = -z[4] + z[13] + z[18] + -z[19];
z[13] = abb[55] * z[4];
z[17] = -3 * z[17] + z[29];
z[17] = abb[35] * z[17];
z[0] = abb[56] + z[0] + z[1] + z[2] + 3 * z[7] + z[9] + z[10] + z[12] + (T(-3) / T(2)) * z[13] + z[16] + z[17] + z[25];
z[1] = -abb[43] + z[11] + -z[26];
z[1] = abb[7] * z[1];
z[2] = z[38] * z[50];
z[7] = abb[1] * z[53];
z[1] = z[1] + z[2] + (T(3) / T(2)) * z[6] + z[7] + -z[27] + 6 * z[39] + -4 * z[48] + -z[61] + -z[67] + -z[68];
z[1] = abb[27] * z[1];
z[2] = abb[8] * z[8];
z[2] = -z[2] + z[41];
z[6] = abb[50] + z[22] + -z[89];
z[6] = z[6] * z[23];
z[2] = 3 * z[2] + (T(3) / T(2)) * z[5] + z[6] + -z[14] + z[54] + z[66] + z[71] + -z[85];
z[2] = abb[29] * z[2];
z[5] = -z[34] + -z[37] + z[70] + z[81] + z[83];
z[5] = abb[4] * z[5];
z[6] = abb[47] * (T(-15) / T(4)) + z[15] + z[32] + z[35] + -z[51];
z[6] = abb[7] * z[6];
z[5] = z[5] + z[6] + z[74] + z[84] + -z[86];
z[5] = abb[32] * z[5];
z[1] = z[1] + z[2] + z[5] + -z[33];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[39] * z[62];
z[3] = abb[38] * z[3];
z[4] = abb[40] * z[4];
z[5] = abb[37] * z[31];
z[2] = z[2] + z[3] + (T(-1) / T(2)) * z[4] + z[5];
z[1] = abb[41] + z[1] + 3 * z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_518_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.450078917274434293101657209467872430583744502505932285414849113"),stof<T>("-53.134356214699376742634265603799826706847615954368603431025468477")}, std::complex<T>{stof<T>("-3.842303589037347753716178442501725703371338453839769594880634106"),stof<T>("-15.122073742449383876612057114347449851412820922669855048999498447")}, std::complex<T>{stof<T>("5.955619561115181136183194534859822576938147794007982362233604841"),stof<T>("-2.6802180037284676978423523679772994022041376239944463996023347556")}, std::complex<T>{stof<T>("10.196413297685457616930521855257939299806246635619880033109209743"),stof<T>("-16.440645940048484040486324987198986131844974154008383607881331246")}, std::complex<T>{stof<T>("10.196413297685457616930521855257939299806246635619880033109209743"),stof<T>("-16.440645940048484040486324987198986131844974154008383607881331246")}, std::complex<T>{stof<T>("50.988688193162936909068429124561912295980143336839418967543593764"),stof<T>("-5.910846882291830441500942051562465807976598915046520025025282327")}, std::complex<T>{stof<T>("-41.024978470615073700210068323774544434220028566552717566617134902"),stof<T>("25.031710826068782179829619406738751342025710693049350032508948328")}, std::complex<T>{stof<T>("-43.693299263115379353354045628413022018389543203864180985206756727"),stof<T>("49.092492404841541510823828740696764182871493937718239190702631764")}, std::complex<T>{stof<T>("8.0830973256076242344635057628998424262394372954516672657562390083"),stof<T>("1.3616458061293675339680844951257631217719843926559178407205019567")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_518_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_518_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(-3) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[2] + v[3])) / tend;


		return (abb[43] + -abb[44]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -prod_pow(abb[30], 2);
z[1] = prod_pow(abb[31], 2);
z[0] = z[0] + z[1];
z[1] = abb[43] + -abb[44];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_518_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_518_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-6.194762672576488621916568983164697542571121639990908593947942353"),stof<T>("46.288936587992042781314864263340869758586774024164777721917736206")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W19(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}};
abb[41] = SpDLog_f_4_518_W_19_Im(t, path, abb);
abb[56] = SpDLog_f_4_518_W_19_Re(t, path, abb);

                    
            return f_4_518_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_518_DLogXconstant_part(base_point<T>, kend);
	value += f_4_518_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_518_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_518_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_518_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_518_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_518_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_518_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
