/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_6.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_6_abbreviated (const std::array<T,17>& abb) {
T z[12];
z[0] = abb[11] + abb[13];
z[1] = -abb[12] + z[0];
z[2] = abb[3] * z[1];
z[3] = abb[5] * z[1];
z[4] = abb[13] + -abb[15];
z[4] = abb[1] * z[4];
z[2] = -z[2] + z[3] + -z[4];
z[2] = prod_pow(abb[6], 2) * z[2];
z[5] = -abb[1] + abb[2] + -abb[3];
z[6] = abb[9] + abb[16];
z[6] = -z[1] * z[5] * z[6];
z[7] = -abb[2] + abb[4];
z[8] = abb[5] + z[7];
z[8] = z[1] * z[8];
z[0] = abb[14] + abb[15] + -z[0];
z[0] = abb[0] * z[0];
z[8] = z[0] + z[8];
z[8] = abb[7] * z[8];
z[5] = z[1] * z[5];
z[9] = 2 * abb[6];
z[9] = z[5] * z[9];
z[10] = -z[8] + -z[9];
z[10] = abb[7] * z[10];
z[1] = z[1] * z[7];
z[7] = -abb[12] + abb[13];
z[11] = abb[14] + z[7];
z[11] = abb[3] * z[11];
z[1] = z[1] + z[11];
z[1] = abb[8] * z[1];
z[9] = z[1] + z[9];
z[9] = abb[8] * z[9];
z[2] = z[2] + 2 * z[6] + z[9] + z[10];
z[6] = abb[14] * (T(-13) / T(6)) + abb[11] * (T(1) / T(6)) + -2 * z[7];
z[6] = abb[3] * z[6];
z[0] = (T(13) / T(6)) * z[0] + 2 * z[3] + (T(1) / T(6)) * z[4] + z[6];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[0] = z[0] + 2 * z[2];
z[1] = -z[1] + z[8];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[10] * z[5];
z[1] = z[1] + z[2];
z[1] = 4 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_6_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("8.4682404971162099089140022069314870872748233086889533753573189457"),stof<T>("9.620600119370669569825579755556749038565448556475945047630399758")}, std::complex<T>{stof<T>("5.6222654359935863214769242164443351869283148125763426365424174878"),stof<T>("6.5876136958573652638365985848577205214314408911412515492807577678")}, std::complex<T>{stof<T>("-27.313003291794943017884728564533476899333714700810616951752986282"),stof<T>("11.66746622729919930677166384352233911102692416303757190380213707")}, std::complex<T>{stof<T>("-14.090505933109796230390926423375822274203138121265296011899736434"),stof<T>("-16.208213815228034833662178340414469559996889447617196596911157526")}, std::complex<T>{stof<T>("21.690737855801356696407804348089141712405399888234274315210568794"),stof<T>("-18.255079923156564570608262428380059632458365054178823453082894837")}};
	
	std::vector<C> intdlogs = {rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[26].real()/kbase.W[26].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_6_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_6_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-9.8620179418024667668645985009418330346419086224926656524554029069"),stof<T>("3.4380399736722134916605243965917542254830795316136619725949648286")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,17> abb = {dl[1], dl[3], dlog_W10(k,dl), dl[4], dlog_W21(k,dl), dlog_W27(k,dl), f_1_2(k), f_1_3(k), f_1_6(k), f_2_5(k), f_2_6_im(k), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[26].real()/k.W[26].real()), f_2_6_re(k)};

                    
            return f_4_6_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_6_DLogXconstant_part(base_point<T>, kend);
	value += f_4_6_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_6_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_6_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_6_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_6_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_6_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_6_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
