/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_383.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_383_abbreviated (const std::array<T,35>& abb) {
T z[42];
z[0] = abb[9] + abb[10];
z[1] = abb[30] + abb[31];
z[2] = (T(1) / T(4)) * z[1];
z[2] = z[0] * z[2];
z[3] = abb[24] + abb[29];
z[4] = abb[26] * (T(1) / T(2));
z[5] = abb[25] + z[4];
z[3] = abb[27] + (T(5) / T(2)) * z[3] + -7 * z[5];
z[3] = abb[4] * z[3];
z[6] = abb[26] * (T(1) / T(4));
z[7] = abb[24] * (T(1) / T(4));
z[8] = abb[25] * (T(-2) / T(3)) + abb[28] * (T(-1) / T(6)) + abb[29] * (T(5) / T(12)) + -z[6] + z[7];
z[8] = abb[3] * z[8];
z[9] = abb[25] + abb[26];
z[10] = -abb[24] + abb[27];
z[11] = abb[29] * (T(13) / T(2)) + 2 * z[9] + (T(-17) / T(2)) * z[10];
z[11] = abb[0] * z[11];
z[12] = abb[26] * (T(2) / T(3));
z[13] = abb[29] * (T(1) / T(3));
z[14] = abb[25] + abb[27] * (T(-1) / T(3)) + z[12] + -z[13];
z[14] = abb[8] * z[14];
z[15] = 2 * abb[25];
z[13] = abb[26] * (T(-7) / T(6)) + abb[27] * (T(5) / T(6)) + z[13] + -z[15];
z[13] = abb[2] * z[13];
z[16] = abb[24] + -abb[29];
z[17] = abb[28] + z[16];
z[17] = abb[1] * z[17];
z[18] = 2 * abb[29];
z[19] = abb[26] + abb[27] + -z[18];
z[20] = abb[7] * z[19];
z[12] = abb[29] * (T(-11) / T(6)) + abb[25] * (T(-1) / T(2)) + abb[27] * (T(7) / T(6)) + z[12];
z[12] = abb[6] * z[12];
z[3] = z[2] + (T(1) / T(6)) * z[3] + z[8] + (T(1) / T(3)) * z[11] + z[12] + z[13] + z[14] + (T(13) / T(6)) * z[17] + -z[20];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[8] = abb[24] * (T(1) / T(2));
z[11] = abb[27] * (T(1) / T(2));
z[12] = abb[29] * (T(3) / T(2)) + z[8] + -z[9] + -z[11];
z[12] = abb[4] * z[12];
z[13] = abb[29] * (T(1) / T(2));
z[14] = -z[5] + z[8] + z[13];
z[21] = -abb[0] * z[14];
z[4] = -abb[29] + z[4] + z[11];
z[22] = abb[6] * z[4];
z[23] = 2 * z[19];
z[24] = abb[8] * z[23];
z[25] = (T(1) / T(2)) * z[1];
z[26] = abb[9] * z[25];
z[27] = abb[25] + abb[28] + -abb[29];
z[27] = abb[3] * z[27];
z[28] = -abb[2] * z[19];
z[12] = z[12] + -z[20] + z[21] + -z[22] + z[24] + z[26] + 2 * z[27] + z[28];
z[12] = prod_pow(abb[15], 2) * z[12];
z[21] = 4 * abb[25];
z[26] = abb[29] * (T(5) / T(4));
z[27] = abb[27] * (T(3) / T(2));
z[28] = abb[26] * (T(-11) / T(4)) + -z[7] + -z[21] + z[26] + z[27];
z[28] = abb[4] * z[28];
z[29] = abb[25] * (T(3) / T(2));
z[30] = abb[26] + -z[11] + -z[13] + z[29];
z[31] = abb[8] * z[30];
z[32] = abb[3] * z[14];
z[2] = z[2] + (T(1) / T(2)) * z[32];
z[33] = -z[9] + z[10];
z[34] = abb[0] * (T(1) / T(2));
z[33] = z[33] * z[34];
z[34] = 3 * abb[25];
z[35] = -2 * abb[26] + abb[27] + abb[29] + -z[34];
z[36] = abb[2] * z[35];
z[37] = 2 * z[36];
z[38] = abb[6] * z[35];
z[28] = -z[2] + z[28] + z[31] + z[33] + z[37] + -z[38];
z[28] = abb[16] * z[28];
z[7] = -z[7] + z[15];
z[6] = z[6] + z[7] + z[26] + -z[27];
z[6] = abb[4] * z[6];
z[2] = z[2] + z[36];
z[26] = abb[29] * (T(7) / T(2));
z[29] = abb[26] + abb[27] * (T(5) / T(2)) + -z[26] + -z[29];
z[29] = abb[8] * z[29];
z[39] = abb[6] * z[19];
z[6] = -z[2] + z[6] + z[29] + z[33] + -z[39];
z[6] = abb[15] * z[6];
z[2] = z[2] + z[31];
z[29] = -abb[26] + abb[29];
z[29] = z[7] + (T(-7) / T(4)) * z[29];
z[29] = abb[4] * z[29];
z[27] = -abb[29] + abb[24] * (T(-3) / T(2)) + (T(-1) / T(2)) * z[9] + z[27];
z[27] = abb[0] * z[27];
z[16] = abb[26] + z[16];
z[16] = abb[5] * z[16];
z[27] = -z[2] + -z[16] + -z[22] + z[27] + z[29];
z[27] = abb[13] * z[27];
z[6] = z[6] + z[27] + z[28];
z[6] = abb[13] * z[6];
z[4] = -abb[4] * z[4];
z[10] = -abb[29] + z[10];
z[10] = abb[0] * z[10];
z[4] = z[4] + z[10] + z[16] + -2 * z[17] + z[20] + -z[22];
z[4] = abb[17] * z[4];
z[20] = abb[4] * z[19];
z[22] = z[20] + z[39];
z[24] = z[22] + -z[24];
z[27] = abb[15] * z[24];
z[4] = z[4] + z[27];
z[4] = abb[17] * z[4];
z[15] = -abb[27] + z[15];
z[13] = abb[26] * (T(-3) / T(2)) + -z[8] + z[13] + -z[15];
z[13] = abb[0] * z[13];
z[29] = 2 * abb[4];
z[31] = -abb[24] + abb[25];
z[31] = z[29] * z[31];
z[40] = abb[10] * z[25];
z[13] = z[13] + z[31] + z[32] + -z[36] + z[40];
z[13] = abb[16] * z[13];
z[7] = abb[29] * (T(-3) / T(4)) + abb[26] * (T(5) / T(4)) + z[7] + -z[11];
z[7] = abb[4] * z[7];
z[2] = -z[2] + z[7] + z[33];
z[2] = abb[15] * z[2];
z[7] = -z[2] + z[13];
z[7] = abb[16] * z[7];
z[13] = 3 * abb[24];
z[31] = abb[26] + abb[29];
z[33] = z[13] + z[31];
z[33] = -abb[27] + (T(1) / T(2)) * z[33];
z[33] = abb[9] * z[33];
z[40] = abb[3] + abb[4];
z[41] = (T(3) / T(2)) * z[1];
z[40] = z[40] * z[41];
z[41] = abb[0] * z[1];
z[33] = z[33] + z[40] + z[41];
z[14] = abb[10] * z[14];
z[30] = abb[11] * z[30];
z[1] = abb[12] * z[1];
z[1] = -z[1] + (T(3) / T(2)) * z[14] + z[30] + (T(1) / T(2)) * z[33];
z[14] = abb[34] * z[1];
z[30] = -2 * abb[27] + z[31] + z[34];
z[30] = abb[6] * z[30];
z[31] = -6 * abb[25] + -5 * abb[26] + abb[27] + 4 * abb[29];
z[31] = abb[4] * z[31];
z[33] = 4 * abb[26] + abb[27] + -5 * abb[29] + z[34];
z[33] = abb[8] * z[33];
z[34] = 3 * z[36];
z[30] = z[30] + z[31] + z[33] + z[34];
z[31] = abb[32] * z[30];
z[19] = -abb[8] * z[19];
z[19] = z[10] + -z[16] + z[19] + z[20];
z[19] = abb[18] * z[19];
z[20] = z[29] * z[35];
z[29] = abb[8] * z[35];
z[20] = z[20] + -z[29] + z[34] + -z[38];
z[33] = abb[13] + -abb[16];
z[33] = z[20] * z[33];
z[5] = z[5] + -z[11];
z[11] = abb[4] + -abb[6];
z[5] = z[5] * z[11];
z[11] = abb[26] + z[15];
z[11] = abb[2] * z[11];
z[5] = z[5] + z[11];
z[5] = abb[14] * z[5];
z[5] = 3 * z[5] + z[27] + z[33];
z[5] = abb[14] * z[5];
z[11] = -abb[20] * z[22];
z[15] = -abb[29] + z[9];
z[27] = 3 * abb[19];
z[15] = z[15] * z[27];
z[27] = -abb[2] + -abb[4];
z[27] = z[15] * z[27];
z[33] = abb[20] * z[23];
z[15] = z[15] + z[33];
z[15] = abb[8] * z[15];
z[33] = abb[33] * z[24];
z[3] = z[3] + z[4] + z[5] + z[6] + z[7] + z[11] + z[12] + z[14] + z[15] + z[19] + z[27] + z[31] + z[33];
z[4] = abb[26] * (T(-7) / T(2)) + z[8] + -z[21] + z[26];
z[4] = abb[4] * z[4];
z[5] = -3 * abb[27] + z[9] + z[13] + z[18];
z[5] = abb[0] * z[5];
z[0] = z[0] * z[25];
z[6] = 2 * z[16];
z[0] = z[0] + z[4] + z[5] + z[6] + -z[29] + z[32] + z[37] + z[39];
z[0] = abb[13] * z[0];
z[4] = -abb[7] * z[23];
z[4] = z[4] + -z[6] + -2 * z[10] + 4 * z[17] + z[22];
z[4] = abb[17] * z[4];
z[5] = -abb[14] * z[20];
z[0] = z[0] + -z[2] + z[4] + z[5] + -z[28];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[23] * z[1];
z[2] = abb[21] * z[30];
z[4] = abb[22] * z[24];
z[0] = z[0] + z[1] + z[2] + z[4];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_383_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("13.89080176549358099314059959235238098129596881720881596598448881"),stof<T>("-60.437637728212557938425205334794277264659936456790018877794028093")}, std::complex<T>{stof<T>("23.879381808358947358200499996971327984902276670437873584039445088"),stof<T>("-4.964525490228577077160159552622320638799579908595529274791218172")}, std::complex<T>{stof<T>("0.2015533229792085425028727201302613426726795509405163333223935846"),stof<T>("-9.5031409798842799019678613114784397898708661711707674171535287782")}, std::complex<T>{stof<T>("-15.877892395071963112430422521104305911120166048471898901490971519"),stof<T>("37.643942315400290543009241147558098481130285140035957282348822649")}, std::complex<T>{stof<T>("21.690737855801356696407804348089141712405399888234274315210568794"),stof<T>("-18.255079923156564570608262428380059632458365054178823453082894837")}, std::complex<T>{stof<T>("-6.0143987837086021264802545471150971439579133907028917470419908594"),stof<T>("-9.8857214123594460704331174076995990588010539146863664121123990331")}, std::complex<T>{stof<T>("1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("-3.073952256592314428553215974881268771197457590719930264362430233")}, std::complex<T>{stof<T>("1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("-3.073952256592314428553215974881268771197457590719930264362430233")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_383_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_383_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-61.050504075050102766174370205160398789496951453334777072294893951"),stof<T>("26.095116799996252388387906399154123963584972728326104579889433891")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,35> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_383_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_383_DLogXconstant_part(base_point<T>, kend);
	value += f_4_383_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_383_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_383_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_383_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_383_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_383_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_383_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
