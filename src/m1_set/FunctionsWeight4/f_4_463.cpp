/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_463.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_463_abbreviated (const std::array<T,60>& abb) {
T z[69];
z[0] = abb[43] + -abb[44] + abb[45] + -abb[46];
z[1] = abb[39] * z[0];
z[2] = abb[38] * z[0];
z[3] = z[1] + z[2];
z[4] = abb[31] * z[0];
z[5] = abb[30] * z[0];
z[6] = z[4] + z[5];
z[7] = abb[32] * z[0];
z[6] = (T(1) / T(2)) * z[6] + -z[7];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = (T(-1) / T(2)) * z[3] + z[6];
z[6] = abb[21] * z[6];
z[8] = -abb[28] + abb[29];
z[9] = m1_set::bc<T>[0] * z[8];
z[10] = abb[38] + -abb[39];
z[11] = z[9] + -z[10];
z[11] = abb[48] * z[11];
z[12] = z[9] + z[10];
z[12] = abb[51] * z[12];
z[11] = z[11] + z[12];
z[12] = -abb[49] * z[10];
z[11] = (T(1) / T(2)) * z[11] + z[12];
z[11] = abb[4] * z[11];
z[12] = abb[30] * (T(1) / T(2));
z[13] = z[0] * z[12];
z[14] = abb[28] * (T(1) / T(2));
z[15] = z[0] * z[14];
z[16] = abb[29] * z[0];
z[15] = z[15] + -z[16];
z[13] = z[13] + z[15];
z[17] = abb[27] * z[0];
z[18] = z[4] + z[13] + z[17];
z[18] = -z[7] + (T(1) / T(2)) * z[18];
z[18] = m1_set::bc<T>[0] * z[18];
z[19] = abb[37] * z[0];
z[3] = -z[3] + z[19];
z[3] = (T(1) / T(2)) * z[3] + z[18];
z[3] = abb[19] * z[3];
z[18] = abb[38] + abb[39];
z[20] = abb[30] + abb[31];
z[21] = -abb[32] + (T(1) / T(2)) * z[20];
z[21] = m1_set::bc<T>[0] * z[21];
z[18] = (T(1) / T(2)) * z[18] + -z[21];
z[22] = -abb[48] + abb[51];
z[23] = abb[8] * z[22];
z[24] = z[18] * z[23];
z[3] = z[3] + z[6] + z[11] + z[24];
z[6] = -z[16] + z[17];
z[4] = z[4] + -z[7];
z[11] = z[4] + z[6];
z[11] = m1_set::bc<T>[0] * z[11];
z[1] = -z[1] + z[11] + z[19];
z[1] = abb[24] * z[1];
z[11] = abb[28] * z[0];
z[19] = -z[5] + -z[11];
z[7] = z[7] + (T(1) / T(2)) * z[19];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = z[2] + z[7];
z[7] = abb[23] * z[7];
z[19] = abb[48] + abb[51];
z[24] = abb[14] * z[19];
z[25] = abb[28] + abb[30];
z[25] = -abb[32] + (T(1) / T(2)) * z[25];
z[25] = m1_set::bc<T>[0] * z[25];
z[25] = -abb[38] + z[25];
z[25] = z[24] * z[25];
z[26] = z[10] * z[22];
z[27] = abb[40] * (T(1) / T(2));
z[28] = abb[53] * z[27];
z[26] = z[26] + z[28];
z[26] = abb[5] * z[26];
z[1] = z[1] + z[7] + z[25] + z[26];
z[7] = abb[1] * z[18];
z[18] = abb[38] + -z[21];
z[18] = abb[11] * z[18];
z[7] = z[7] + -z[18];
z[21] = -abb[27] + z[12];
z[25] = -abb[29] + z[14];
z[26] = z[21] + -z[25];
z[26] = m1_set::bc<T>[0] * z[26];
z[26] = -abb[37] + z[26];
z[28] = abb[0] * (T(1) / T(4));
z[29] = z[26] * z[28];
z[30] = z[7] + z[29];
z[14] = z[14] + z[21];
z[14] = m1_set::bc<T>[0] * z[14];
z[14] = -abb[37] + z[14];
z[21] = abb[5] * (T(1) / T(4));
z[31] = z[14] * z[21];
z[32] = abb[4] * (T(1) / T(2));
z[10] = z[10] * z[32];
z[33] = abb[12] * (T(1) / T(4));
z[34] = z[26] * z[33];
z[35] = -abb[16] + abb[17];
z[36] = abb[40] * z[35];
z[10] = z[10] + z[30] + z[31] + -z[34] + (T(1) / T(8)) * z[36];
z[31] = abb[50] * z[10];
z[30] = abb[51] * z[30];
z[18] = z[18] + z[29];
z[18] = abb[48] * z[18];
z[7] = abb[49] * z[7];
z[7] = z[7] + -z[18] + -z[30] + z[31];
z[18] = abb[6] * (T(1) / T(4));
z[29] = z[9] * z[18];
z[10] = -z[10] + z[29];
z[29] = abb[47] * (T(1) / T(8));
z[10] = z[10] * z[29];
z[14] = -abb[16] * z[14];
z[26] = -abb[17] * z[26];
z[30] = abb[0] + abb[12];
z[27] = z[27] * z[30];
z[31] = abb[15] * z[9];
z[14] = z[14] + z[26] + z[27] + z[31];
z[26] = abb[26] * abb[40];
z[14] = (T(1) / T(32)) * z[14] + (T(-1) / T(16)) * z[26];
z[14] = abb[53] * z[14];
z[26] = -abb[29] + abb[28] * (T(3) / T(4));
z[27] = abb[32] * (T(1) / T(2));
z[31] = abb[31] * (T(1) / T(2));
z[34] = abb[30] * (T(-1) / T(4)) + -z[26] + z[27] + -z[31];
z[34] = m1_set::bc<T>[0] * z[34];
z[34] = abb[38] * (T(1) / T(2)) + z[34];
z[34] = abb[48] * z[34];
z[36] = -abb[31] + abb[32];
z[37] = abb[28] + -abb[30];
z[37] = z[36] + (T(1) / T(2)) * z[37];
z[37] = m1_set::bc<T>[0] * z[37];
z[37] = abb[38] + z[37];
z[38] = abb[51] * (T(1) / T(2));
z[37] = z[37] * z[38];
z[39] = abb[50] * (T(1) / T(2));
z[9] = z[9] * z[39];
z[9] = z[9] + z[34] + z[37];
z[34] = abb[6] * (T(1) / T(16));
z[9] = z[9] * z[34];
z[37] = -abb[27] + abb[32];
z[37] = m1_set::bc<T>[0] * z[37];
z[40] = abb[37] + -abb[39];
z[37] = z[37] + -z[40];
z[37] = abb[7] * z[37];
z[41] = abb[27] + -abb[29];
z[36] = z[36] + -z[41];
z[36] = m1_set::bc<T>[0] * z[36];
z[36] = z[36] + -z[40];
z[40] = abb[13] * (T(1) / T(32));
z[36] = -z[36] * z[40];
z[36] = z[36] + (T(1) / T(32)) * z[37];
z[36] = z[19] * z[36];
z[13] = z[4] + z[13];
z[13] = m1_set::bc<T>[0] * z[13];
z[2] = -z[2] + z[13];
z[13] = abb[20] + abb[22];
z[37] = (T(1) / T(32)) * z[13];
z[2] = z[2] * z[37];
z[37] = abb[15] + -abb[18];
z[37] = z[19] * z[37];
z[42] = abb[25] * z[0];
z[37] = z[37] + z[42];
z[42] = abb[40] * z[37];
z[43] = -abb[48] + abb[52];
z[43] = (T(-1) / T(32)) * z[43];
z[43] = abb[42] * z[43];
z[1] = abb[58] + (T(1) / T(32)) * z[1] + z[2] + (T(1) / T(16)) * z[3] + (T(-1) / T(8)) * z[7] + z[9] + z[10] + z[14] + z[36] + (T(1) / T(64)) * z[42] + z[43];
z[2] = z[4] + z[5];
z[3] = z[11] + -z[17];
z[4] = z[2] + -z[3];
z[4] = abb[32] * z[4];
z[7] = z[11] + -z[16];
z[9] = z[7] * z[41];
z[10] = -abb[54] + abb[56];
z[11] = abb[34] + z[10];
z[11] = z[0] * z[11];
z[14] = -abb[30] * z[6];
z[7] = -z[5] + z[7];
z[7] = abb[31] * z[7];
z[4] = z[4] + z[7] + z[9] + z[11] + z[14];
z[4] = abb[24] * z[4];
z[7] = abb[27] * (T(1) / T(2));
z[9] = z[0] * z[7];
z[9] = z[9] + z[15];
z[11] = abb[30] * z[9];
z[9] = abb[27] * z[9];
z[2] = abb[32] * z[2];
z[14] = abb[31] * z[5];
z[2] = -z[2] + z[14];
z[14] = abb[55] + abb[56];
z[16] = prod_pow(m1_set::bc<T>[0], 2);
z[17] = -abb[35] + (T(1) / T(2)) * z[16];
z[36] = -abb[54] + z[14] + z[17];
z[36] = z[0] * z[36];
z[9] = -z[2] + z[9] + -z[11] + z[36];
z[9] = abb[19] * z[9];
z[36] = abb[27] + -abb[28];
z[42] = z[7] * z[36];
z[43] = z[12] * z[36];
z[44] = -abb[28] + abb[29] * (T(1) / T(2));
z[44] = abb[29] * z[44];
z[45] = prod_pow(abb[28], 2);
z[46] = (T(1) / T(2)) * z[45];
z[47] = z[44] + z[46];
z[48] = abb[35] + z[47];
z[49] = (T(1) / T(3)) * z[16];
z[50] = abb[54] + z[49];
z[42] = z[42] + -z[43] + z[48] + -z[50];
z[51] = abb[16] * z[42];
z[52] = abb[27] * z[8];
z[53] = abb[30] * z[8];
z[54] = (T(1) / T(6)) * z[16];
z[55] = -abb[35] + z[54];
z[56] = z[52] + -z[53] + -z[55];
z[57] = abb[15] * z[56];
z[25] = z[7] + z[25];
z[58] = abb[27] + -abb[30];
z[25] = z[25] * z[58];
z[50] = abb[35] + -z[25] + z[50];
z[58] = -abb[17] * z[50];
z[51] = z[51] + z[57] + z[58];
z[51] = abb[53] * z[51];
z[57] = z[31] + z[41];
z[58] = abb[31] * z[57];
z[59] = prod_pow(abb[29], 2);
z[59] = abb[34] + (T(1) / T(2)) * z[59];
z[60] = abb[36] + z[59];
z[58] = z[58] + z[60];
z[61] = -abb[31] + z[27];
z[62] = abb[32] * z[61];
z[63] = prod_pow(abb[27], 2);
z[62] = -z[10] + z[58] + z[62] + (T(-1) / T(2)) * z[63];
z[62] = z[19] * z[62];
z[63] = abb[33] * z[19];
z[62] = z[62] + -z[63];
z[62] = abb[7] * z[62];
z[14] = z[14] + (T(5) / T(6)) * z[16];
z[64] = z[0] * z[14];
z[2] = -z[2] + z[64];
z[2] = abb[21] * z[2];
z[64] = abb[27] + abb[30];
z[64] = -abb[32] + (T(1) / T(2)) * z[64];
z[3] = z[3] * z[64];
z[65] = abb[55] + z[49];
z[66] = -z[0] * z[65];
z[3] = z[3] + z[66];
z[3] = abb[23] * z[3];
z[66] = abb[31] * z[8];
z[67] = abb[29] * z[8];
z[67] = abb[34] + z[67];
z[66] = -z[55] + -z[66] + z[67];
z[22] = -abb[3] * z[22] * z[66];
z[64] = z[36] * z[64];
z[64] = z[64] + z[65];
z[64] = z[24] * z[64];
z[66] = -abb[23] + -abb[24];
z[66] = z[0] * z[66];
z[24] = z[24] + z[66];
z[24] = abb[33] * z[24];
z[2] = z[2] + z[3] + z[4] + z[9] + z[22] + z[24] + z[51] + z[62] + z[64];
z[3] = abb[55] * (T(1) / T(2));
z[4] = abb[36] * (T(1) / T(2));
z[9] = z[3] + -z[4];
z[22] = -abb[28] + z[7];
z[24] = -z[7] * z[22];
z[51] = z[31] + z[36];
z[62] = -z[31] * z[51];
z[61] = -z[36] + z[61];
z[64] = -z[27] * z[61];
z[66] = abb[56] * (T(1) / T(2));
z[24] = abb[35] + z[9] + (T(-1) / T(12)) * z[16] + z[24] + z[44] + (T(1) / T(4)) * z[45] + z[62] + z[64] + -z[66];
z[24] = abb[48] * z[24];
z[45] = abb[31] * z[51];
z[61] = abb[32] * z[61];
z[22] = abb[27] * z[22];
z[45] = abb[36] + z[22] + z[45] + z[61];
z[62] = z[45] + z[46];
z[64] = -abb[55] + abb[56];
z[68] = -z[54] + -z[62] + z[64];
z[38] = z[38] * z[68];
z[24] = z[24] + z[38];
z[24] = abb[5] * z[24];
z[38] = z[49] + z[62];
z[38] = abb[1] * z[38];
z[48] = z[48] + z[54];
z[62] = abb[2] * z[48];
z[68] = -abb[0] * z[48];
z[38] = abb[59] * (T(1) / T(2)) + z[38] + z[62] + z[68];
z[38] = abb[52] * z[38];
z[24] = z[24] + z[38];
z[21] = z[21] * z[42];
z[38] = abb[54] + -z[25] + z[47];
z[16] = (T(1) / T(4)) * z[16];
z[38] = abb[35] + z[16] + (T(1) / T(2)) * z[38];
z[42] = abb[0] * (T(1) / T(2));
z[38] = z[38] * z[42];
z[20] = -abb[32] + z[20];
z[42] = abb[32] * z[20];
z[68] = abb[30] * abb[31];
z[42] = z[42] + -z[68];
z[14] = z[14] + z[42];
z[68] = abb[1] * z[14];
z[42] = (T(1) / T(2)) * z[42] + z[65];
z[42] = abb[11] * z[42];
z[65] = -z[42] + (T(1) / T(2)) * z[68];
z[33] = z[33] * z[50];
z[50] = z[54] + z[64];
z[68] = z[32] * z[50];
z[62] = (T(1) / T(2)) * z[62];
z[21] = -z[21] + -z[33] + z[38] + -z[62] + -z[65] + z[68];
z[33] = -abb[50] * z[21];
z[38] = z[0] * z[31];
z[6] = -z[5] + z[6] + z[38];
z[6] = abb[31] * z[6];
z[17] = abb[33] + -abb[55] + -z[17] + -z[60];
z[17] = z[0] * z[17];
z[0] = z[0] * z[27];
z[0] = z[0] + -z[5];
z[0] = abb[32] * z[0];
z[5] = abb[27] * z[15];
z[0] = z[0] + -z[5] + -z[6] + z[11] + z[17];
z[5] = (T(-1) / T(4)) * z[13];
z[0] = z[0] * z[5];
z[5] = abb[30] + z[51];
z[5] = abb[31] * z[5];
z[5] = z[5] + z[22] + z[46];
z[6] = -abb[31] + abb[32] * (T(3) / T(4));
z[11] = -abb[30] + -z[36];
z[11] = z[6] + (T(1) / T(2)) * z[11];
z[11] = abb[32] * z[11];
z[5] = (T(1) / T(2)) * z[5] + -z[9] + z[11] + -z[16] + -z[66];
z[5] = abb[1] * z[5];
z[11] = abb[54] + -z[25] + -z[47] + z[54];
z[11] = z[11] * z[28];
z[13] = z[58] + z[61];
z[15] = z[13] + z[22];
z[16] = abb[9] * (T(1) / T(2));
z[15] = z[15] * z[16];
z[11] = z[11] + -z[15];
z[5] = z[5] + z[11] + z[42];
z[5] = abb[51] * z[5];
z[11] = abb[59] * (T(-1) / T(4)) + z[11] + -z[42] + z[62];
z[11] = abb[48] * z[11];
z[7] = -abb[29] + z[7];
z[7] = abb[27] * z[7];
z[7] = z[7] + z[13] + z[53];
z[13] = -abb[35] + z[49];
z[15] = -z[7] + -z[13] + -z[64];
z[15] = abb[48] * z[15];
z[7] = abb[35] + -z[7] + z[64];
z[7] = abb[51] * z[7];
z[7] = z[7] + z[15];
z[15] = -abb[49] * z[50];
z[16] = -abb[52] * z[48];
z[7] = (T(1) / T(2)) * z[7] + z[15] + z[16];
z[7] = z[7] * z[32];
z[15] = abb[49] * z[65];
z[0] = z[0] + (T(1) / T(4)) * z[2] + z[5] + z[7] + z[11] + z[15] + (T(1) / T(2)) * z[24] + z[33];
z[2] = abb[30] + -z[57];
z[2] = z[2] * z[31];
z[5] = -abb[30] + z[27];
z[5] = z[5] * z[27];
z[7] = -abb[27] * z[26];
z[11] = abb[27] * (T(1) / T(4)) + z[26];
z[11] = abb[30] * z[11];
z[2] = z[2] + -z[3] + -z[4] + z[5] + z[7] + z[11] + -z[13] + (T(-1) / T(2)) * z[59];
z[2] = abb[48] * z[2];
z[3] = abb[30] + z[57];
z[3] = abb[31] * z[3];
z[4] = abb[27] + abb[28] * (T(-3) / T(2));
z[4] = abb[27] * z[4];
z[3] = z[3] + z[4] + z[43] + z[59];
z[4] = z[6] + -z[12] + -z[36];
z[4] = abb[32] * z[4];
z[3] = (T(1) / T(2)) * z[3] + z[4] + -z[9] + -z[54];
z[3] = abb[51] * z[3];
z[4] = -z[44] + z[45] + z[55];
z[4] = abb[52] * z[4];
z[5] = z[39] * z[56];
z[2] = z[2] + z[3] + z[4] + z[5] + (T(1) / T(2)) * z[63];
z[2] = z[2] * z[34];
z[3] = z[18] * z[56];
z[3] = z[3] + -z[21];
z[3] = z[3] * z[29];
z[4] = z[20] + z[36];
z[4] = abb[32] * z[4];
z[5] = abb[30] + z[8];
z[5] = abb[31] * z[5];
z[6] = abb[30] * z[41];
z[4] = -z[4] + z[5] + z[6] + -z[10] + z[52] + -z[67];
z[4] = -z[4] * z[19];
z[4] = z[4] + -z[63];
z[4] = z[4] * z[40];
z[5] = -abb[5] + -z[30];
z[5] = abb[53] * z[5];
z[6] = abb[50] * z[35];
z[7] = abb[47] * z[35];
z[5] = z[5] + z[6] + z[7] + -z[37];
z[6] = abb[26] * abb[53];
z[5] = (T(1) / T(4)) * z[5] + z[6];
z[5] = abb[57] * z[5];
z[6] = -z[14] * z[23];
z[0] = abb[41] + (T(1) / T(8)) * z[0] + z[2] + z[3] + z[4] + (T(1) / T(16)) * z[5] + (T(1) / T(32)) * z[6];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_463_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.26309090279216278001407381946305763106540864060601367103901342582"),stof<T>("-0.38068040225873842530728415778143086555260350644490332516158241055")}, std::complex<T>{stof<T>("0.20531556988561892464562918779022462465493646848598145846365866456"),stof<T>("-0.36692548041253916053596210827137743567723796531177468035818837531")}, std::complex<T>{stof<T>("0.12797730314989006657016400038373510871120895438833118525733431523"),stof<T>("-0.68322367136259992182320903078175455831082304786216446223982724434")}, std::complex<T>{stof<T>("0.26309090279216278001407381946305763106540864060601367103901342582"),stof<T>("-0.38068040225873842530728415778143086555260350644490332516158241055")}, std::complex<T>{stof<T>("0.049219051904339527132869290794184266029198584138564081607028599467"),stof<T>("0.051564436705231106375680085075364259633975315306124760191391390894")}, stof<T>("0.19445478182325857965854926342930396208173717667651950271598702203"), std::complex<T>{stof<T>("0.052678935884231730299906118451924879625719695309925263090767030428"),stof<T>("0.119129763596743450607089913422324455774389719436512833921156798303")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_463_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_463_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(128)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (-v[3] + v[5])) / tend;


		return (abb[47] + abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[47] + abb[48] + abb[50];
z[1] = abb[28] + -abb[29];
return abb[10] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_463_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(128)) * (-v[3] + v[5]) * (-6 * v[0] + 2 * v[1] + -4 * (2 + v[3]) + m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(-1) / T(32)) * (-v[3] + v[5])) / tend;


		return (abb[47] + abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = abb[27] + abb[29] + abb[28] * (T(-1) / T(2));
z[0] = abb[28] * z[0];
z[1] = abb[27] + abb[29] * (T(1) / T(2));
z[1] = abb[29] * z[1];
z[2] = abb[28] + -abb[29];
z[2] = abb[30] * z[2];
z[0] = z[0] + -z[1] + -z[2];
z[0] = -abb[35] + (T(1) / T(2)) * z[0];
z[1] = abb[47] + abb[48] + abb[50];
return abb[10] * (T(1) / T(8)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_463_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.28255429200904087779361517960953856897985361988895684852379685137"),stof<T>("-0.02533597840639754950538030421562829449461506953229206289013496404")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W16(k,dl), dlog_W23(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_4_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), T{0}, T{0}};
abb[41] = SpDLog_f_4_463_W_16_Im(t, path, abb);
abb[42] = SpDLogQ_W_78(k,dl,dlr).imag();
abb[58] = SpDLog_f_4_463_W_16_Re(t, path, abb);
abb[59] = SpDLogQ_W_78(k,dl,dlr).real();

                    
            return f_4_463_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_463_DLogXconstant_part(base_point<T>, kend);
	value += f_4_463_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_463_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_463_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_463_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_463_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_463_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_463_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
