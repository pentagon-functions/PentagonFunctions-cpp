/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_612.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_612_abbreviated (const std::array<T,64>& abb) {
T z[102];
z[0] = abb[44] + abb[50];
z[1] = abb[47] + abb[48];
z[2] = abb[46] * (T(11) / T(6));
z[3] = 3 * abb[49];
z[0] = abb[43] * (T(-22) / T(3)) + abb[45] * (T(-11) / T(3)) + (T(-11) / T(6)) * z[0] + (T(5) / T(2)) * z[1] + -z[2] + z[3];
z[0] = abb[2] * z[0];
z[4] = 2 * abb[47];
z[5] = -abb[46] + abb[49];
z[6] = -abb[44] + abb[50];
z[7] = -z[4] + z[5] + z[6];
z[7] = abb[8] * z[7];
z[8] = abb[53] + abb[54] + abb[55] + -abb[56];
z[9] = abb[21] * z[8];
z[7] = -z[7] + z[9];
z[10] = abb[44] * (T(1) / T(2));
z[11] = abb[46] * (T(1) / T(2));
z[12] = z[10] + z[11];
z[13] = abb[50] * (T(1) / T(2));
z[14] = z[12] + z[13];
z[15] = abb[49] * (T(3) / T(2));
z[16] = 2 * abb[43] + abb[45];
z[17] = z[15] + -z[16];
z[18] = z[14] + -z[17];
z[19] = abb[12] * z[18];
z[20] = abb[49] * (T(1) / T(2));
z[21] = 5 * abb[45] + abb[46] * (T(-13) / T(2));
z[21] = abb[42] * (T(13) / T(6)) + z[20] + (T(1) / T(3)) * z[21];
z[22] = abb[43] * (T(5) / T(3));
z[23] = 3 * abb[47];
z[21] = abb[50] * (T(5) / T(6)) + (T(1) / T(2)) * z[21] + z[22] + -z[23];
z[21] = abb[0] * z[21];
z[24] = abb[50] * (T(3) / T(4));
z[25] = -abb[48] + abb[45] * (T(1) / T(3));
z[25] = abb[46] * (T(1) / T(3)) + (T(1) / T(2)) * z[25];
z[22] = abb[42] * (T(-1) / T(12)) + abb[44] * (T(19) / T(12)) + z[22] + -z[24] + 5 * z[25];
z[22] = abb[6] * z[22];
z[12] = z[12] + z[20];
z[25] = -abb[48] + z[12] + -z[13];
z[26] = abb[4] * z[25];
z[27] = -abb[42] + z[6];
z[28] = abb[13] * z[27];
z[29] = z[26] + -z[28];
z[30] = z[5] + -z[6];
z[31] = abb[7] * z[30];
z[32] = abb[44] + abb[46];
z[33] = z[23] + z[32];
z[34] = -abb[45] + z[33];
z[34] = -abb[43] + -abb[50] + (T(1) / T(2)) * z[34];
z[34] = abb[14] * z[34];
z[35] = abb[20] + abb[22];
z[36] = (T(5) / T(4)) * z[35];
z[36] = -z[8] * z[36];
z[37] = (T(-1) / T(2)) * z[8];
z[38] = abb[23] * z[37];
z[39] = abb[51] + -abb[52];
z[40] = (T(1) / T(2)) * z[39];
z[41] = abb[18] * z[40];
z[38] = z[38] + z[41];
z[41] = -abb[42] + -11 * abb[45] + abb[46] * (T(7) / T(2));
z[42] = abb[50] * (T(3) / T(2));
z[41] = abb[44] * (T(-31) / T(6)) + abb[49] * (T(5) / T(2)) + (T(1) / T(3)) * z[41] + z[42];
z[41] = abb[43] * (T(-11) / T(3)) + (T(1) / T(2)) * z[41];
z[41] = abb[5] * z[41];
z[43] = -abb[47] + -abb[49] + abb[50];
z[44] = abb[45] + z[43];
z[44] = abb[43] + (T(1) / T(2)) * z[44];
z[44] = abb[9] * z[44];
z[6] = -abb[47] + z[6];
z[6] = abb[11] * z[6];
z[45] = 3 * z[6];
z[46] = abb[15] * z[39];
z[47] = (T(-3) / T(2)) * z[8];
z[48] = abb[19] * z[47];
z[49] = abb[1] * z[27];
z[2] = -abb[49] + abb[44] * (T(-11) / T(3)) + abb[43] * (T(-8) / T(3)) + abb[45] * (T(-4) / T(3)) + abb[48] * (T(1) / T(2)) + abb[50] * (T(7) / T(3)) + z[2];
z[2] = abb[3] * z[2];
z[50] = -abb[16] * z[39];
z[0] = z[0] + z[2] + -2 * z[7] + z[19] + z[21] + z[22] + (T(1) / T(2)) * z[29] + (T(-1) / T(4)) * z[31] + z[34] + z[36] + -z[38] + z[41] + z[44] + -z[45] + (T(-5) / T(4)) * z[46] + z[48] + (T(1) / T(6)) * z[49] + z[50];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[2] = abb[46] * (T(5) / T(2)) + -z[15];
z[7] = abb[42] * (T(5) / T(2)) + -z[2];
z[21] = abb[45] + z[7];
z[22] = -abb[43] + z[23];
z[21] = z[13] + (T(1) / T(2)) * z[21] + -z[22];
z[21] = abb[0] * z[21];
z[29] = abb[47] + (T(-1) / T(2)) * z[5] + z[10] + -z[13];
z[36] = abb[8] * z[29];
z[41] = (T(3) / T(2)) * z[36];
z[50] = abb[2] * z[18];
z[15] = abb[45] + z[11] + -z[15];
z[10] = z[10] + z[13] + z[15];
z[51] = abb[43] + (T(1) / T(2)) * z[10];
z[52] = 3 * abb[12];
z[53] = z[51] * z[52];
z[54] = abb[3] * z[51];
z[55] = z[9] + z[28];
z[56] = (T(-3) / T(4)) * z[8];
z[57] = abb[23] * z[56];
z[58] = (T(3) / T(4)) * z[39];
z[59] = abb[18] * z[58];
z[57] = z[57] + z[59];
z[59] = abb[42] * (T(1) / T(2));
z[60] = abb[44] + z[59];
z[61] = z[15] + z[60];
z[61] = abb[43] + (T(1) / T(2)) * z[61];
z[62] = abb[5] * z[61];
z[17] = -z[11] + z[17];
z[60] = -z[17] + z[60];
z[63] = abb[6] * z[60];
z[44] = 3 * z[44];
z[64] = (T(3) / T(2)) * z[39];
z[65] = abb[15] * z[64];
z[66] = abb[16] * z[58];
z[54] = -z[21] + z[41] + z[44] + -z[49] + z[50] + -z[53] + z[54] + (T(3) / T(4)) * z[55] + z[57] + z[62] + z[63] + z[65] + z[66];
z[54] = abb[27] * z[54];
z[62] = abb[6] * z[18];
z[63] = z[35] * z[47];
z[62] = z[62] + z[63];
z[67] = abb[3] * z[18];
z[68] = z[48] + z[62] + z[67];
z[52] = z[18] * z[52];
z[69] = abb[14] * z[18];
z[70] = abb[24] * z[47];
z[69] = -z[69] + z[70];
z[70] = -4 * abb[43] + -2 * abb[45] + -abb[50] + z[3] + -z[32];
z[71] = abb[5] * z[70];
z[72] = abb[2] * z[70];
z[73] = z[52] + -z[68] + z[69] + z[71] + 2 * z[72];
z[73] = abb[29] * z[73];
z[74] = -abb[50] + z[59];
z[75] = -z[15] + z[74];
z[75] = -abb[43] + (T(1) / T(2)) * z[75];
z[75] = abb[0] * z[75];
z[76] = 2 * z[49];
z[57] = (T(3) / T(4)) * z[28] + z[57] + -z[66] + -z[75] + -z[76];
z[66] = abb[19] * z[56];
z[53] = -z[53] + z[66];
z[77] = -z[53] + z[57];
z[78] = z[35] * z[56];
z[79] = z[77] + -z[78];
z[80] = abb[42] * (T(1) / T(4));
z[81] = abb[44] * (T(3) / T(4));
z[82] = abb[50] * (T(1) / T(4)) + -z[17] + z[80] + z[81];
z[82] = abb[5] * z[82];
z[83] = abb[43] + (T(1) / T(2)) * z[15];
z[84] = abb[50] * (T(5) / T(4));
z[81] = abb[42] + z[81] + -z[83] + -z[84];
z[81] = abb[6] * z[81];
z[81] = z[69] + z[72] + z[79] + z[81] + -z[82];
z[82] = -abb[28] * z[81];
z[54] = z[54] + z[73] + z[82];
z[54] = abb[27] * z[54];
z[10] = abb[17] * z[10];
z[82] = abb[13] * z[40];
z[10] = z[10] + -z[82];
z[82] = abb[15] * z[51];
z[85] = abb[0] + abb[5];
z[85] = z[58] * z[85];
z[86] = abb[17] * abb[43];
z[87] = -abb[25] * z[8];
z[88] = abb[26] * z[39];
z[89] = (T(1) / T(4)) * z[27];
z[90] = abb[18] * z[89];
z[10] = (T(1) / T(2)) * z[10] + -z[82] + -z[85] + z[86] + (T(-1) / T(4)) * z[87] + 2 * z[88] + -z[90];
z[82] = abb[62] * z[10];
z[85] = -abb[23] + -z[35];
z[85] = -z[8] * z[85];
z[86] = abb[3] * z[30];
z[87] = abb[5] * z[27];
z[88] = -abb[18] * z[39];
z[85] = -z[28] + z[46] + z[85] + z[86] + -z[87] + z[88];
z[85] = abb[33] * z[85];
z[86] = abb[33] * z[39];
z[88] = abb[62] * z[27];
z[86] = z[86] + (T(1) / T(2)) * z[88];
z[86] = abb[16] * z[86];
z[85] = z[85] + z[86];
z[18] = abb[5] * z[18];
z[18] = z[18] + -z[72];
z[86] = -z[18] + z[19];
z[20] = abb[44] * (T(3) / T(2)) + -z[13] + z[16] + -z[20];
z[88] = 2 * abb[48];
z[90] = abb[46] * (T(-3) / T(2)) + -z[20] + z[88];
z[91] = abb[3] + abb[6];
z[90] = z[90] * z[91];
z[92] = z[35] * z[37];
z[93] = -z[26] + z[92];
z[94] = abb[19] * z[37];
z[90] = z[86] + z[90] + -z[93] + -z[94];
z[90] = abb[34] * z[90];
z[4] = z[4] + z[12] + -z[16] + -z[42];
z[4] = abb[14] * z[4];
z[12] = z[16] + z[43];
z[12] = abb[9] * z[12];
z[42] = (T(1) / T(2)) * z[9];
z[37] = abb[24] * z[37];
z[4] = z[4] + z[12] + z[37] + -z[42];
z[43] = z[4] + -z[36];
z[86] = z[43] + z[86];
z[95] = abb[58] * z[86];
z[11] = -z[11] + z[20];
z[11] = abb[3] * z[11];
z[20] = abb[5] * (T(1) / T(2));
z[30] = z[20] * z[30];
z[11] = z[11] + z[30];
z[4] = z[4] + z[11];
z[4] = abb[60] * z[4];
z[96] = abb[42] + z[5];
z[96] = -abb[47] + (T(1) / T(2)) * z[96];
z[96] = abb[0] * z[96];
z[40] = abb[15] * z[40];
z[38] = -z[38] + -z[40] + (T(-1) / T(2)) * z[55] + z[96];
z[29] = abb[14] * z[29];
z[29] = z[29] + -z[37] + z[92];
z[37] = z[29] + z[38];
z[37] = abb[57] * z[37];
z[29] = z[29] + z[94];
z[40] = abb[3] * z[25];
z[40] = -z[29] + z[40];
z[1] = -abb[49] + z[1];
z[1] = abb[2] * z[1];
z[42] = z[1] + -z[26] + z[40] + z[42];
z[55] = abb[6] * z[25];
z[92] = -z[36] + -z[42] + -z[55];
z[92] = abb[59] * z[92];
z[96] = -abb[19] * z[8];
z[96] = -z[36] + (T(1) / T(2)) * z[96];
z[97] = abb[57] + abb[60];
z[96] = z[96] * z[97];
z[97] = abb[33] + abb[57];
z[97] = z[27] * z[97];
z[98] = abb[1] * z[97];
z[99] = -abb[62] * z[39];
z[97] = (T(1) / T(2)) * z[97] + z[99];
z[97] = abb[6] * z[97];
z[4] = z[4] + z[37] + z[82] + (T(1) / T(2)) * z[85] + z[90] + z[92] + z[95] + z[96] + z[97] + z[98];
z[37] = abb[48] * (T(3) / T(2));
z[82] = abb[47] * (T(3) / T(2));
z[14] = z[14] + z[16] + -z[37] + -z[82];
z[14] = abb[2] * z[14];
z[85] = abb[46] * (T(1) / T(4)) + -z[16];
z[90] = abb[49] * (T(3) / T(4));
z[92] = abb[50] * (T(7) / T(4));
z[37] = abb[44] * (T(11) / T(4)) + -z[37] + -z[85] + z[90] + -z[92];
z[37] = abb[3] * z[37];
z[95] = 3 * abb[48];
z[96] = -abb[45] + z[95];
z[97] = -abb[43] + z[13];
z[32] = -z[32] + (T(1) / T(2)) * z[96] + z[97];
z[96] = abb[6] * z[32];
z[45] = (T(3) / T(4)) * z[31] + z[45];
z[98] = (T(3) / T(2)) * z[26];
z[99] = -abb[45] + abb[46];
z[97] = -abb[44] + z[97] + (T(1) / T(2)) * z[99];
z[100] = -abb[5] * z[97];
z[34] = (T(3) / T(4)) * z[9] + z[14] + z[34] + z[37] + z[41] + z[45] + -z[66] + z[96] + -z[98] + z[100];
z[34] = abb[30] * z[34];
z[37] = 2 * abb[50];
z[41] = 2 * abb[46] + -z[95];
z[66] = 2 * abb[44];
z[3] = 8 * abb[43] + 4 * abb[45] + -z[3] + -z[23] + z[37] + z[41] + z[66];
z[3] = abb[2] * z[3];
z[23] = z[16] + -z[33] + z[37];
z[23] = abb[14] * z[23];
z[33] = -abb[50] + z[16] + z[66];
z[37] = z[33] + z[41];
z[66] = abb[6] * z[37];
z[95] = 3 * z[12];
z[37] = abb[3] * z[37];
z[3] = z[3] + z[23] + z[37] + z[66] + -z[71] + -z[95];
z[3] = abb[29] * z[3];
z[71] = 2 * z[23];
z[9] = (T(3) / T(2)) * z[9] + z[18] + 3 * z[36] + -z[68] + z[71] + -z[95];
z[68] = -abb[27] * z[9];
z[95] = (T(3) / T(2)) * z[31];
z[96] = -abb[46] + z[33];
z[100] = abb[3] * z[96];
z[101] = -z[95] + 2 * z[100];
z[96] = abb[5] * z[96];
z[62] = -z[62] + z[69] + -z[72] + z[96] + z[101];
z[62] = abb[31] * z[62];
z[26] = 3 * z[26] + -2 * z[37];
z[18] = z[18] + -z[26] + 2 * z[66] + z[69];
z[66] = -abb[28] * z[18];
z[3] = z[3] + z[34] + z[62] + z[66] + z[68];
z[3] = abb[30] * z[3];
z[34] = abb[44] * (T(1) / T(4));
z[62] = z[17] + -z[24] + -z[34] + z[80];
z[62] = abb[5] * z[62];
z[66] = abb[44] * (T(5) / T(4));
z[24] = -abb[42] + z[24] + -z[66] + -z[83];
z[24] = abb[6] * z[24];
z[24] = z[24] + -z[53] + -z[57] + z[62] + -z[67] + z[72] + -z[78];
z[24] = abb[28] * z[24];
z[53] = abb[27] * z[81];
z[57] = abb[3] * z[97];
z[58] = abb[15] * z[58];
z[57] = z[57] + z[58];
z[58] = -abb[42] + abb[44];
z[62] = z[58] + -z[99];
z[62] = abb[43] + (T(1) / T(2)) * z[62];
z[62] = abb[5] * z[62];
z[61] = -abb[6] * z[61];
z[68] = (T(1) / T(2)) * z[49];
z[50] = z[50] + -z[57] + z[61] + z[62] + z[68] + z[75];
z[50] = abb[31] * z[50];
z[24] = z[24] + z[50] + z[53] + -z[73];
z[24] = abb[31] * z[24];
z[2] = abb[45] + -z[2];
z[2] = (T(1) / T(2)) * z[2] + -z[22] + -z[66] + z[92];
z[2] = abb[14] * z[2];
z[22] = z[20] * z[27];
z[50] = abb[24] * z[56];
z[27] = abb[6] * z[27];
z[2] = z[2] + -z[21] + z[22] + (T(-5) / T(4)) * z[27] + -z[45] + z[50] + z[57] + z[68] + -z[78];
z[2] = abb[32] * z[2];
z[21] = (T(1) / T(2)) * z[27] + z[49];
z[17] = z[17] + z[74];
z[17] = abb[0] * z[17];
z[45] = z[17] + z[21] + z[63] + -z[65] + -z[69] + z[87] + z[95] + -z[100];
z[45] = abb[31] * z[45];
z[47] = abb[23] * z[47];
z[53] = abb[18] * z[64];
z[28] = (T(3) / T(2)) * z[28] + z[47] + z[53];
z[47] = abb[16] * z[64];
z[17] = z[17] + z[47];
z[27] = z[17] + -z[22] + 2 * z[27] + -z[28] + 4 * z[49] + -z[67] + -z[69];
z[53] = abb[27] + -abb[28];
z[53] = z[27] * z[53];
z[2] = z[2] + z[45] + z[53];
z[2] = abb[32] * z[2];
z[45] = -z[34] + -z[82] + z[84] + -z[85] + -z[90];
z[45] = abb[14] * z[45];
z[32] = z[32] * z[91];
z[51] = abb[5] * z[51];
z[14] = z[14] + z[32] + -z[44] + z[45] + -z[50] + z[51] + z[98];
z[14] = prod_pow(abb[29], 2) * z[14];
z[32] = -z[16] + -z[41] + -z[58];
z[32] = abb[6] * z[32];
z[41] = -abb[5] * z[60];
z[17] = -z[17] + 2 * z[32] + -z[37] + z[41] + -z[49] + z[72];
z[17] = abb[28] * z[17];
z[18] = abb[29] * z[18];
z[17] = z[17] + z[18];
z[17] = abb[28] * z[17];
z[18] = -abb[61] * z[9];
z[32] = -abb[33] + -abb[60];
z[32] = z[31] * z[32];
z[0] = abb[63] + z[0] + z[2] + z[3] + 3 * z[4] + z[14] + z[17] + z[18] + z[24] + (T(3) / T(2)) * z[32] + z[54];
z[2] = abb[44] * (T(5) / T(2));
z[3] = -abb[42] + -6 * abb[43] + -z[2] + -z[13] + -3 * z[15];
z[3] = abb[6] * z[3];
z[4] = -6 * abb[47] + abb[50] + z[7] + z[16];
z[4] = abb[0] * z[4];
z[7] = abb[3] * z[70];
z[3] = z[3] + z[4] + z[7] + -6 * z[12] + z[22] + -z[28] + -3 * z[46] + -z[47] + -z[48] + z[52] + -z[63] + z[71] + z[76];
z[3] = abb[27] * z[3];
z[4] = -z[12] + z[55];
z[7] = (T(1) / T(2)) * z[31];
z[12] = abb[48] + -z[33];
z[12] = abb[3] * z[12];
z[6] = z[1] + z[4] + -2 * z[6] + -z[7] + z[12] + z[23] + -z[30] + -z[93];
z[6] = abb[30] * z[6];
z[1] = z[1] + -z[4] + -z[19] + -z[40];
z[1] = abb[29] * z[1];
z[1] = z[1] + z[6];
z[4] = 3 * abb[43];
z[6] = -abb[42] + z[4] + (T(3) / T(2)) * z[15] + -z[34] + z[92];
z[6] = abb[6] * z[6];
z[2] = abb[50] * (T(5) / T(2)) + -z[2] + -3 * z[5] + z[59];
z[2] = z[2] * z[20];
z[5] = abb[14] * z[70];
z[12] = 3 * abb[24];
z[13] = -z[12] + (T(9) / T(4)) * z[35];
z[13] = -z[8] * z[13];
z[2] = z[2] + -z[5] + z[6] + z[13] + -z[77] + -z[101];
z[2] = abb[31] * z[2];
z[6] = -abb[32] * z[27];
z[8] = -z[8] * z[12];
z[12] = abb[45] * (T(1) / T(2)) + abb[49] * (T(1) / T(4)) + abb[46] * (T(5) / T(4)) + -z[88];
z[4] = abb[42] + abb[50] * (T(-13) / T(4)) + abb[44] * (T(19) / T(4)) + z[4] + 3 * z[12];
z[4] = abb[6] * z[4];
z[4] = z[4] + z[5] + z[8] + -z[26] + z[79] + (T(1) / T(4)) * z[87];
z[4] = abb[28] * z[4];
z[1] = 3 * z[1] + z[2] + z[3] + z[4] + z[6];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[16] * z[89];
z[2] = z[2] + z[10];
z[2] = abb[40] * z[2];
z[3] = abb[36] * z[86];
z[4] = -abb[37] * z[42];
z[5] = z[21] + z[29] + z[38];
z[5] = abb[35] * z[5];
z[6] = -z[7] + z[11] + z[43] + z[94];
z[6] = abb[38] * z[6];
z[7] = -abb[35] + -abb[37];
z[7] = z[7] * z[36];
z[8] = -abb[37] * z[25];
z[10] = -abb[40] * z[39];
z[8] = z[8] + z[10];
z[8] = abb[6] * z[8];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
z[3] = -abb[39] * z[9];
z[1] = abb[41] + z[1] + 3 * z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_612_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.5456475103687967511626615423861988683086098975062501467963607855"),stof<T>("-11.5033015991443592773318823824912092128823182683640941530054341614")}, std::complex<T>{stof<T>("3.402800050253254215055999418538193991107424795644827539591794253"),stof<T>("53.005038414406225442918449250355425377080589699495969496998979947")}, std::complex<T>{stof<T>("4.506327512280110241669155214187873596628702543494398012230264319"),stof<T>("37.078855698803254118411860117702453011443658874134524866902840008")}, std::complex<T>{stof<T>("1.701400025126627107527999709269096995553712397822413769795897126"),stof<T>("26.502519207203112721459224625177712688540294849747984748499489973")}, std::complex<T>{stof<T>("-9.802134862913920151869041703399913166617528186632045364256450569"),stof<T>("-21.181965232726981999077966400784476980680922130742873291916936564")}, std::complex<T>{stof<T>("-5.4515598653916402665652246560949376972339281434538109750257225907"),stof<T>("0.8976728580175186752065514742314725551047601620077609794918476323")}, std::complex<T>{stof<T>("8.1007348377872930443410419941308161710638157888096315944605534422"),stof<T>("-5.3205539744761307223812582243932357078593727190051114565825534096")}, std::complex<T>{stof<T>("-2.8049274871534831341411555049187766010749901456719842424343671925"),stof<T>("-10.5763364916001413969526354925247403229033640243865401184033500348")}, std::complex<T>{stof<T>("-3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("-14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[125].real()/kbase.W[125].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_612_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_612_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(8)) * (-v[3] + v[5]) * (-24 + 4 * v[2] + -5 * v[3] + -3 * v[5] + 4 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (3 * (-1 + 2 * m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return (2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[49]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[28] + abb[29];
z[1] = -abb[28] + -abb[30];
z[0] = z[0] * z[1];
z[0] = abb[34] + z[0];
z[1] = 2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[49];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_612_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-3) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return (2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[49]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -2 * abb[43] + -abb[44] + -abb[45] + -abb[46] + abb[48] + abb[49];
z[1] = abb[28] + -abb[29];
return 3 * abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_612_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("60.151579686058178482824320303096999919365554354263710139575613413"),stof<T>("31.89417336517168893789946242170821194401905998580203096991505474")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,64> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W16(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[125].real()/k.W[125].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}};
abb[41] = SpDLog_f_4_612_W_16_Im(t, path, abb);
abb[63] = SpDLog_f_4_612_W_16_Re(t, path, abb);

                    
            return f_4_612_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_612_DLogXconstant_part(base_point<T>, kend);
	value += f_4_612_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_612_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_612_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_612_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_612_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_612_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_612_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
