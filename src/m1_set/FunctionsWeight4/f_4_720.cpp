/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_720.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_720_abbreviated (const std::array<T,39>& abb) {
T z[114];
z[0] = 3 * abb[18];
z[1] = 2 * abb[14];
z[2] = z[0] + -z[1];
z[3] = 3 * abb[16];
z[4] = -2 * z[2] + z[3];
z[4] = abb[16] * z[4];
z[5] = 4 * abb[15];
z[6] = abb[14] + -abb[17];
z[7] = abb[16] + z[6];
z[8] = -z[5] * z[7];
z[9] = 6 * abb[21];
z[10] = prod_pow(abb[18], 2);
z[11] = 3 * z[10];
z[12] = abb[14] * z[6];
z[4] = z[4] + z[8] + z[9] + z[11] + 4 * z[12];
z[4] = abb[6] * z[4];
z[8] = abb[18] * z[6];
z[13] = z[8] + -z[12];
z[14] = abb[16] + -abb[17];
z[15] = z[1] + z[14];
z[16] = abb[18] + -z[15];
z[16] = abb[16] * z[16];
z[17] = abb[15] * z[7];
z[16] = z[13] + z[16] + z[17];
z[17] = 3 * abb[21];
z[16] = 2 * z[16] + -z[17];
z[16] = abb[8] * z[16];
z[18] = 2 * abb[15];
z[19] = z[7] * z[18];
z[20] = z[1] * z[6];
z[19] = z[19] + -z[20];
z[21] = 2 * abb[18];
z[22] = -abb[17] + -z[21];
z[22] = z[3] + 2 * z[22];
z[22] = abb[16] * z[22];
z[23] = 2 * z[6];
z[24] = -abb[18] + z[23];
z[25] = abb[18] * z[24];
z[22] = abb[21] + z[19] + z[22] + -z[25];
z[22] = abb[0] * z[22];
z[26] = 4 * z[6];
z[27] = z[0] + -z[26];
z[27] = abb[18] * z[27];
z[28] = 7 * abb[16];
z[29] = -5 * abb[18] + z[23];
z[29] = z[28] + 2 * z[29];
z[29] = abb[16] * z[29];
z[27] = z[9] + z[27] + z[29];
z[27] = abb[7] * z[27];
z[29] = abb[18] + z[23];
z[29] = abb[18] * z[29];
z[30] = 2 * abb[17];
z[31] = -abb[16] + z[30];
z[31] = abb[16] * z[31];
z[19] = abb[21] + -z[19] + z[29] + z[31];
z[19] = abb[3] * z[19];
z[31] = -abb[16] + z[21];
z[32] = abb[16] * z[31];
z[33] = -z[10] + z[32];
z[34] = -abb[21] + z[33];
z[35] = -abb[9] * z[34];
z[36] = 2 * abb[4];
z[34] = -z[34] * z[36];
z[37] = abb[0] + 2 * abb[9];
z[37] = -5 * abb[3] + -7 * abb[6] + abb[7] + -z[36] + 3 * z[37];
z[38] = prod_pow(m1_set::bc<T>[0], 2);
z[37] = z[37] * z[38];
z[4] = z[4] + z[16] + z[19] + z[22] + z[27] + z[34] + -6 * z[35] + z[37];
z[4] = abb[32] * z[4];
z[16] = 3 * abb[0];
z[19] = 2 * abb[3];
z[22] = 7 * abb[8];
z[27] = -5 * abb[4] + -z[16] + -z[19] + z[22];
z[27] = abb[31] * z[27];
z[34] = 4 * abb[29];
z[37] = abb[27] + z[34];
z[39] = abb[0] + -abb[8];
z[40] = abb[4] + z[39];
z[37] = z[37] * z[40];
z[40] = abb[33] + abb[35];
z[41] = abb[34] + z[40];
z[42] = -abb[10] + abb[12];
z[42] = -z[41] * z[42];
z[43] = -abb[1] + -abb[8];
z[43] = abb[0] + z[19] + 2 * z[43];
z[43] = abb[26] * z[43];
z[44] = 3 * abb[8];
z[45] = -abb[0] + abb[4] + z[19] + -z[44];
z[45] = abb[28] * z[45];
z[46] = -abb[0] * abb[25];
z[47] = -abb[25] + abb[26];
z[47] = abb[5] * z[47];
z[27] = z[27] + z[37] + z[42] + z[43] + z[45] + z[46] + z[47];
z[27] = abb[19] * z[27];
z[37] = 2 * abb[16];
z[42] = -abb[15] + z[37];
z[43] = abb[15] * z[42];
z[45] = 2 * abb[20];
z[46] = z[43] + -z[45];
z[47] = prod_pow(abb[16], 2);
z[48] = 2 * abb[21];
z[47] = -z[46] + z[47] + z[48];
z[49] = prod_pow(abb[14], 2);
z[50] = -z[10] + z[49];
z[51] = z[47] + 2 * z[50];
z[52] = -abb[28] + abb[31];
z[51] = z[51] * z[52];
z[47] = -z[47] + -z[50];
z[47] = abb[26] * z[47];
z[52] = -abb[25] * z[50];
z[47] = z[47] + z[51] + z[52];
z[47] = abb[5] * z[47];
z[4] = z[4] + z[27] + z[47];
z[27] = abb[0] + 10 * abb[2] + 7 * abb[4];
z[47] = 6 * abb[6];
z[51] = 4 * abb[9];
z[19] = 9 * abb[8] + z[19] + -z[27] + z[47] + -z[51];
z[19] = abb[31] * z[19];
z[52] = 3 * abb[4];
z[53] = -z[51] + z[52];
z[54] = 2 * abb[7];
z[55] = abb[0] + z[54];
z[56] = 6 * abb[3] + -z[22] + z[47] + z[53] + z[55];
z[56] = abb[28] * z[56];
z[57] = 4 * abb[7];
z[27] = -z[22] + z[27] + -z[51] + z[57];
z[27] = abb[27] * z[27];
z[58] = 2 * abb[8];
z[59] = abb[9] + z[58];
z[55] = 5 * abb[2] + 4 * abb[4] + z[55] + -2 * z[59];
z[59] = 2 * abb[29];
z[55] = z[55] * z[59];
z[60] = abb[3] + abb[6] + -abb[8];
z[61] = 6 * abb[30];
z[60] = z[60] * z[61];
z[61] = abb[10] + abb[12];
z[62] = z[41] * z[61];
z[63] = 2 * abb[26];
z[64] = abb[7] + z[39];
z[65] = z[63] * z[64];
z[36] = 6 * abb[9] + -z[36];
z[66] = 3 * abb[6];
z[67] = abb[0] + abb[3] + 3 * abb[7] + -z[36] + z[66];
z[68] = 2 * abb[32];
z[67] = z[67] * z[68];
z[19] = z[19] + z[27] + z[55] + z[56] + -z[60] + -z[62] + z[65] + -z[67];
z[19] = 4 * z[19];
z[27] = -abb[36] * z[19];
z[55] = -abb[18] + -z[6];
z[55] = z[21] * z[55];
z[56] = 4 * abb[18];
z[60] = -abb[14] + -7 * abb[17] + -z[37] + z[56];
z[60] = abb[16] * z[60];
z[62] = 3 * z[6];
z[65] = 4 * abb[16];
z[68] = z[62] + -z[65];
z[69] = -z[18] * z[68];
z[70] = 14 * abb[20];
z[71] = 4 * abb[21];
z[72] = prod_pow(abb[17], 2);
z[73] = 2 * z[72];
z[74] = 3 * abb[14];
z[75] = -abb[17] + z[74];
z[76] = abb[14] * z[75];
z[55] = z[55] + z[60] + z[69] + -z[70] + -z[71] + -z[73] + z[76];
z[55] = abb[3] * z[55];
z[60] = abb[17] + -z[56] + z[65] + z[74];
z[60] = abb[16] * z[60];
z[68] = abb[15] * z[68];
z[69] = 7 * abb[20] + -z[8];
z[9] = z[9] + -z[12] + z[60] + z[68] + z[69];
z[9] = abb[8] * z[9];
z[9] = z[9] + 2 * z[35];
z[60] = abb[14] + z[31];
z[60] = z[37] * z[60];
z[68] = 3 * abb[15];
z[76] = 2 * z[7];
z[77] = z[68] + -z[76];
z[77] = abb[15] * z[77];
z[78] = 2 * z[10];
z[79] = -abb[14] + -z[30];
z[79] = abb[14] * z[79];
z[60] = z[60] + -z[71] + z[77] + -z[78] + z[79];
z[77] = 2 * abb[6];
z[60] = z[60] * z[77];
z[79] = 6 * abb[18];
z[80] = -z[65] + z[79];
z[81] = 7 * abb[14];
z[82] = 5 * abb[17];
z[83] = z[80] + -z[81] + -z[82];
z[83] = abb[16] * z[83];
z[84] = z[74] + z[82];
z[84] = abb[14] * z[84];
z[43] = -10 * abb[20] + -z[11] + 5 * z[43] + -z[71] + z[83] + z[84];
z[43] = abb[4] * z[43];
z[83] = abb[16] + -abb[18];
z[84] = -z[37] * z[83];
z[85] = -abb[15] + z[76];
z[85] = abb[15] * z[85];
z[84] = z[25] + z[73] + z[84] + -z[85];
z[84] = abb[0] * z[84];
z[86] = -abb[18] + z[6];
z[87] = -z[3] + -2 * z[86];
z[87] = abb[16] * z[87];
z[29] = z[29] + -z[48] + z[87];
z[29] = z[29] * z[54];
z[87] = -abb[9] + abb[8] * (T(2) / T(3));
z[88] = abb[6] * (T(10) / T(3));
z[89] = abb[3] + abb[7] * (T(-8) / T(3)) + abb[0] * (T(-1) / T(3)) + 2 * z[87] + z[88];
z[90] = 2 * z[38];
z[89] = z[89] * z[90];
z[9] = 2 * z[9] + z[29] + z[43] + z[55] + z[60] + z[84] + z[89];
z[29] = 2 * abb[28];
z[9] = z[9] * z[29];
z[43] = 5 * abb[14];
z[55] = 3 * abb[17];
z[60] = z[43] + z[55] + -z[80];
z[60] = abb[16] * z[60];
z[80] = -z[81] + z[82];
z[80] = abb[14] * z[80];
z[26] = -z[3] + z[26];
z[26] = -abb[15] + 2 * z[26];
z[84] = abb[15] * z[26];
z[89] = 8 * abb[21];
z[60] = z[11] + z[60] + z[70] + z[80] + z[84] + z[89];
z[60] = abb[4] * z[60];
z[70] = abb[17] + z[3];
z[2] = z[2] + -z[70];
z[2] = z[2] * z[37];
z[80] = -z[6] + z[37];
z[84] = z[68] * z[80];
z[91] = abb[20] + abb[21];
z[2] = z[2] + -z[8] + z[20] + z[84] + -9 * z[91];
z[2] = abb[8] * z[2];
z[8] = z[5] * z[6];
z[84] = 4 * abb[20];
z[8] = z[8] + z[84];
z[91] = -abb[16] + z[23];
z[91] = abb[16] * z[91];
z[92] = -abb[14] + z[30];
z[92] = abb[14] * z[92];
z[91] = -abb[21] + -z[8] + z[72] + z[91] + -z[92];
z[93] = 2 * abb[2];
z[94] = z[91] * z[93];
z[95] = abb[1] * z[10];
z[96] = z[35] + z[95];
z[96] = 2 * z[96];
z[2] = z[2] + -z[94] + z[96];
z[81] = -z[55] + -z[56] + z[81];
z[81] = abb[16] * z[81];
z[97] = z[6] * z[21];
z[97] = -z[45] + z[97];
z[98] = abb[14] + abb[17];
z[99] = abb[14] * z[98];
z[100] = -z[73] + z[99];
z[101] = z[6] + z[37];
z[102] = -z[18] * z[101];
z[81] = z[48] + z[81] + -z[97] + -z[100] + z[102];
z[81] = abb[3] * z[81];
z[76] = abb[15] + z[76];
z[76] = abb[15] * z[76];
z[25] = -z[25] + z[76];
z[49] = -2 * z[49] + z[72];
z[76] = -abb[18] + z[1];
z[102] = z[37] * z[76];
z[49] = -z[25] + z[48] + 2 * z[49] + z[102];
z[49] = abb[0] * z[49];
z[102] = -z[6] + z[21];
z[102] = z[57] * z[83] * z[102];
z[103] = abb[1] * (T(13) / T(3));
z[104] = z[51] + z[103];
z[105] = abb[2] * (T(1) / T(3));
z[88] = abb[8] * (T(-4) / T(3)) + abb[4] * (T(8) / T(3)) + abb[7] * (T(10) / T(3)) + abb[0] * (T(11) / T(3)) + abb[3] * (T(13) / T(3)) + z[88] + -z[104] + z[105];
z[88] = z[38] * z[88];
z[33] = z[33] + -z[48];
z[33] = z[33] * z[77];
z[2] = 2 * z[2] + z[33] + z[49] + z[60] + z[81] + z[88] + z[102];
z[33] = 2 * abb[31];
z[2] = z[2] * z[33];
z[49] = 2 * abb[0] + abb[3];
z[22] = -z[22] + z[49] + z[51] + z[52] + -z[54] + -z[77];
z[22] = abb[31] * z[22];
z[60] = 3 * abb[3];
z[81] = 4 * abb[6] + -z[44] + z[53] + z[60];
z[81] = abb[28] * z[81];
z[49] = -abb[8] + z[49] + z[53] + z[57];
z[49] = abb[27] * z[49];
z[39] = -abb[1] + z[39];
z[53] = abb[3] + abb[7];
z[39] = 2 * z[39] + z[53];
z[39] = abb[26] * z[39];
z[64] = abb[4] + -abb[9] + z[64];
z[34] = z[34] * z[64];
z[64] = abb[11] + z[61];
z[41] = z[41] * z[64];
z[53] = abb[30] * z[53];
z[22] = z[22] + -z[34] + -z[39] + z[41] + -z[49] + -z[53] + z[67] + -z[81];
z[22] = 4 * z[22];
z[34] = -abb[37] * z[22];
z[39] = abb[16] * z[6];
z[41] = z[39] + z[100];
z[49] = z[6] * z[18];
z[53] = z[41] + z[49] + -z[97];
z[53] = abb[11] * z[53];
z[67] = abb[10] * abb[14];
z[81] = abb[10] * z[37];
z[88] = z[67] + -z[81];
z[97] = abb[10] * z[21];
z[102] = abb[10] * abb[17];
z[97] = z[97] + -z[102];
z[106] = z[88] + z[97];
z[106] = abb[16] * z[106];
z[107] = abb[15] * z[6];
z[107] = abb[20] + z[107];
z[13] = z[13] + -z[39] + z[107];
z[108] = 2 * abb[12];
z[109] = z[13] * z[108];
z[110] = abb[10] * abb[15];
z[81] = -z[81] + z[110];
z[110] = abb[15] * z[81];
z[67] = z[67] + z[102];
z[67] = abb[14] * z[67];
z[111] = abb[10] * z[10];
z[112] = 2 * abb[10];
z[113] = abb[20] * z[112];
z[53] = -z[53] + -z[67] + -z[106] + z[109] + -z[110] + z[111] + -z[113];
z[53] = z[40] * z[53];
z[39] = z[12] + z[39] + -z[45] + -z[49];
z[39] = z[39] * z[44];
z[45] = -abb[14] * z[37];
z[45] = z[45] + z[85] + z[92];
z[45] = z[45] * z[66];
z[49] = -abb[14] * abb[17];
z[66] = abb[16] * z[98];
z[46] = -z[46] + z[49] + z[66];
z[46] = z[46] * z[52];
z[49] = 4 * z[95];
z[39] = z[39] + z[45] + z[46] + -z[49];
z[45] = 5 * abb[16];
z[46] = abb[18] + -z[62];
z[46] = z[45] + 2 * z[46];
z[46] = abb[16] * z[46];
z[46] = z[10] + -6 * z[12] + z[46] + 12 * z[107];
z[46] = abb[3] * z[46];
z[58] = abb[3] + abb[7] * (T(4) / T(3)) + -z[58] + z[77] + z[103];
z[58] = z[58] * z[90];
z[62] = abb[16] + z[21];
z[62] = abb[16] * z[62];
z[11] = -z[11] + z[62];
z[11] = abb[7] * z[11];
z[11] = z[11] + 2 * z[39] + z[46] + z[58];
z[11] = abb[30] * z[11];
z[11] = z[11] + z[53];
z[39] = z[6] * z[56];
z[8] = -z[8] + z[39] + -z[41];
z[8] = abb[11] * z[8];
z[39] = -abb[10] * z[56];
z[39] = z[39] + -z[88] + z[102];
z[39] = abb[16] * z[39];
z[41] = 4 * abb[12];
z[13] = z[13] * z[41];
z[46] = z[10] * z[112];
z[53] = -z[18] * z[81];
z[58] = -abb[10] * z[84];
z[8] = z[8] + z[13] + z[39] + z[46] + z[53] + z[58] + -z[67];
z[8] = abb[34] * z[8];
z[13] = z[43] + -z[55];
z[39] = abb[14] * z[13];
z[43] = -z[56] * z[86];
z[46] = 13 * abb[14] + -z[65] + -z[82];
z[46] = abb[16] * z[46];
z[53] = -z[5] * z[101];
z[43] = z[39] + z[43] + z[46] + z[53] + z[71] + -z[73] + z[84];
z[43] = abb[3] * z[43];
z[46] = abb[17] + z[83];
z[46] = z[37] * z[46];
z[53] = 5 * z[6] + -z[37];
z[53] = abb[15] * z[53];
z[20] = z[17] + -z[20] + z[46] + z[53] + z[69];
z[20] = abb[8] * z[20];
z[20] = z[20] + z[94] + z[96];
z[46] = -abb[16] + z[79];
z[53] = abb[14] + z[46] + z[55];
z[53] = abb[16] * z[53];
z[25] = -z[25] + z[39] + -z[48] + z[53] + -3 * z[72];
z[25] = abb[0] * z[25];
z[20] = 2 * z[20] + z[25];
z[25] = 12 * abb[18];
z[53] = -6 * abb[16] + -13 * abb[17] + z[25] + -z[74];
z[53] = abb[16] * z[53];
z[26] = -z[18] * z[26];
z[26] = -28 * abb[20] + -6 * z[10] + z[26] + z[39] + z[53] + -z[89];
z[26] = abb[4] * z[26];
z[23] = -z[0] + z[23];
z[23] = abb[18] * z[23];
z[39] = z[0] + -z[6];
z[39] = -z[3] + 2 * z[39];
z[39] = abb[16] * z[39];
z[23] = z[23] + z[39] + -z[71];
z[23] = z[23] * z[57];
z[39] = 4 * abb[8] + abb[0] * (T(-20) / T(3)) + -z[104] + -z[105];
z[39] = abb[3] * (T(-17) / T(3)) + abb[4] * (T(11) / T(3)) + 2 * z[39] + z[57];
z[39] = z[38] * z[39];
z[20] = 2 * z[20] + z[23] + z[26] + z[39] + z[43];
z[20] = abb[27] * z[20];
z[23] = -z[21] + z[37] + z[98];
z[23] = abb[16] * z[23];
z[26] = -abb[16] + z[6];
z[26] = z[18] * z[26];
z[26] = -z[12] + z[26];
z[23] = z[17] + z[23] + z[26] + z[84];
z[23] = abb[8] * z[23];
z[39] = abb[2] * z[91];
z[43] = z[10] + z[48] + z[84];
z[53] = abb[14] + abb[16];
z[58] = -abb[17] + z[21] + -z[53];
z[58] = abb[16] * z[58];
z[26] = -z[26] + -z[43] + z[58];
z[26] = abb[4] * z[26];
z[58] = -z[7] + z[21];
z[58] = abb[16] * z[58];
z[62] = -abb[17] + z[1];
z[65] = abb[14] * z[62];
z[58] = -abb[21] + z[58] + z[65] + -z[72];
z[58] = abb[0] * z[58];
z[65] = -abb[16] * z[83];
z[65] = -abb[21] + z[65];
z[54] = z[54] * z[65];
z[23] = z[23] + z[26] + z[35] + z[39] + z[54] + z[58];
z[26] = -9 * abb[0] + abb[4] * (T(4) / T(3)) + 4 * z[87] + -z[105];
z[26] = z[26] * z[38];
z[23] = 4 * z[23] + z[26];
z[23] = z[23] * z[59];
z[26] = -abb[0] + 2 * abb[13];
z[26] = -2 * z[26] + z[52] + z[60];
z[35] = abb[34] + 2 * z[40];
z[26] = z[26] * z[35];
z[35] = abb[10] + 3 * abb[11];
z[39] = z[35] + z[41];
z[39] = z[29] * z[39];
z[35] = z[35] + -z[108];
z[35] = z[33] * z[35];
z[54] = abb[10] + abb[11];
z[58] = 3 * abb[25];
z[54] = z[54] * z[58];
z[58] = 8 * abb[29];
z[59] = z[58] * z[61];
z[60] = 9 * abb[10] + abb[11] + 12 * abb[12];
z[60] = abb[27] * z[60];
z[61] = abb[30] * z[64];
z[26] = z[26] + z[35] + z[39] + -z[54] + z[59] + z[60] + -12 * z[61];
z[35] = abb[38] * z[26];
z[32] = -z[10] + -z[32] + z[48];
z[32] = abb[1] * z[32];
z[39] = -abb[14] + abb[18];
z[48] = -abb[16] + 2 * z[39];
z[48] = abb[16] * z[48];
z[54] = prod_pow(abb[15], 2);
z[48] = -abb[21] + z[48] + z[50] + z[54];
z[48] = abb[0] * z[48];
z[50] = -abb[15] + -abb[18] + z[53];
z[53] = abb[16] * z[50];
z[53] = abb[20] + z[53];
z[17] = z[17] + 2 * z[53];
z[17] = abb[8] * z[17];
z[17] = z[17] + z[32] + z[48];
z[32] = z[3] + -z[21];
z[48] = -abb[16] * z[32];
z[10] = z[10] + z[48] + -z[71];
z[10] = abb[7] * z[10];
z[48] = -abb[16] + z[5] + -2 * z[76];
z[48] = abb[16] * z[48];
z[43] = -z[43] + z[48];
z[43] = abb[3] * z[43];
z[48] = abb[3] + -abb[7];
z[53] = 3 * abb[1] + (T(2) / T(3)) * z[48];
z[53] = z[38] * z[53];
z[10] = z[10] + 2 * z[17] + z[43] + z[53];
z[10] = z[10] * z[63];
z[17] = abb[16] * z[7];
z[12] = -z[12] + z[17] + z[72] + -z[78];
z[12] = abb[0] * z[12];
z[12] = z[12] + -z[49];
z[17] = -abb[16] * z[80];
z[17] = z[17] + z[99];
z[17] = abb[4] * z[17];
z[43] = 8 * abb[16] + z[6];
z[43] = abb[16] * z[43];
z[43] = z[43] + z[100];
z[43] = abb[3] * z[43];
z[12] = 2 * z[12] + z[17] + z[43];
z[12] = abb[25] * z[12];
z[16] = z[16] + z[103];
z[16] = -abb[4] + abb[3] * (T(-5) / T(3)) + 2 * z[16];
z[16] = abb[25] * z[16];
z[17] = -abb[11] * z[40];
z[43] = -abb[10] + abb[11] * (T(-7) / T(3));
z[43] = abb[34] * z[43];
z[16] = z[16] + (T(4) / T(3)) * z[17] + z[43];
z[16] = z[16] * z[38];
z[2] = z[2] + 4 * z[4] + z[8] + z[9] + z[10] + 2 * z[11] + z[12] + z[16] + z[20] + z[23] + z[27] + z[34] + z[35];
z[2] = 2 * z[2];
z[4] = -abb[18] + z[37];
z[8] = 6 * abb[15];
z[9] = z[4] + -z[8] + 3 * z[62];
z[9] = abb[3] * z[9];
z[10] = z[15] + -z[18];
z[11] = -z[10] * z[44];
z[12] = -abb[15] + z[7];
z[15] = z[12] * z[47];
z[16] = -z[14] * z[52];
z[17] = abb[1] * abb[18];
z[20] = 8 * z[17];
z[23] = -abb[16] + z[0];
z[27] = abb[7] * z[23];
z[9] = z[9] + z[11] + z[15] + z[16] + z[20] + z[27];
z[9] = abb[30] * z[9];
z[11] = abb[16] + abb[18];
z[15] = z[11] * z[48];
z[16] = abb[1] * z[11];
z[27] = abb[0] * z[39];
z[16] = z[16] + z[27];
z[15] = z[15] + 2 * z[16];
z[15] = abb[26] * z[15];
z[9] = z[9] + z[15];
z[15] = z[18] + -z[23] + -2 * z[62];
z[15] = abb[6] * z[15];
z[16] = -z[36] * z[83];
z[23] = z[30] + -z[74];
z[27] = -z[23] + z[42];
z[27] = abb[0] * z[27];
z[24] = z[3] + z[24];
z[24] = abb[7] * z[24];
z[30] = abb[15] + -z[21] + z[23];
z[30] = abb[3] * z[30];
z[34] = abb[8] * z[50];
z[15] = z[15] + z[16] + z[24] + z[27] + z[30] + z[34];
z[15] = abb[32] * z[15];
z[16] = abb[8] * z[10];
z[24] = z[7] + -z[18];
z[27] = -z[24] * z[93];
z[30] = 4 * abb[14];
z[34] = -z[14] + -z[30];
z[34] = abb[0] * z[34];
z[10] = -z[10] + z[21];
z[10] = abb[4] * z[10];
z[35] = abb[9] * z[83];
z[36] = -abb[7] * z[37];
z[10] = z[10] + z[16] + z[27] + z[34] + 2 * z[35] + z[36];
z[10] = z[10] * z[58];
z[4] = -z[4] + z[68] + -z[75];
z[4] = abb[8] * z[4];
z[16] = -z[17] + z[35];
z[16] = 4 * z[16];
z[17] = abb[2] * z[24];
z[17] = 4 * z[17];
z[14] = abb[15] + -z[14] + z[74];
z[14] = abb[0] * z[14];
z[24] = -z[77] * z[83];
z[4] = z[4] + z[14] + z[16] + z[17] + z[24];
z[14] = -abb[17] + z[18] + z[30] + -z[32];
z[14] = abb[3] * z[14];
z[24] = 14 * abb[14] + -8 * abb[15] + -z[46] + -z[82];
z[24] = abb[4] * z[24];
z[0] = z[0] + -z[7];
z[0] = z[0] * z[57];
z[0] = z[0] + 2 * z[4] + z[14] + z[24];
z[0] = z[0] * z[33];
z[4] = z[18] + -z[31] + z[55];
z[4] = abb[11] * z[4];
z[7] = z[50] * z[108];
z[3] = abb[10] * z[3];
z[14] = abb[10] * z[1];
z[3] = z[3] + z[4] + z[7] + z[14] + -z[97];
z[3] = z[3] * z[40];
z[4] = z[5] + -z[56];
z[5] = abb[16] + -z[1] + z[4] + z[82];
z[5] = abb[11] * z[5];
z[7] = z[45] + -z[56];
z[7] = abb[10] * z[7];
z[24] = z[41] * z[50];
z[5] = z[5] + z[7] + z[14] + z[24] + z[102];
z[5] = abb[34] * z[5];
z[7] = -5 * abb[15] + abb[18] + z[13] + z[37];
z[7] = abb[8] * z[7];
z[7] = z[7] + z[16] + -z[17];
z[13] = -12 * abb[14] + z[18] + -z[28] + z[82];
z[13] = abb[0] * z[13];
z[7] = 2 * z[7] + z[13];
z[13] = -10 * abb[14] + 16 * abb[15] + -9 * abb[16] + z[25] + z[55];
z[13] = abb[4] * z[13];
z[14] = z[21] + -z[101];
z[14] = abb[7] * z[14];
z[16] = 6 * abb[14];
z[4] = -13 * abb[16] + -abb[17] + z[4] + -z[16];
z[4] = abb[3] * z[4];
z[4] = z[4] + 2 * z[7] + z[13] + 8 * z[14];
z[4] = abb[27] * z[4];
z[7] = z[11] + -z[23] + -z[68];
z[7] = abb[8] * z[7];
z[11] = z[51] * z[83];
z[13] = abb[15] + -z[32] + z[98];
z[13] = z[13] * z[77];
z[12] = -abb[0] * z[12];
z[7] = z[7] + z[11] + z[12] + z[13];
z[8] = z[8] + -z[30] + -z[70] + z[79];
z[8] = abb[3] * z[8];
z[11] = abb[16] + -z[16] + z[79] + -z[82];
z[11] = abb[4] * z[11];
z[6] = -z[6] + -z[21];
z[6] = z[6] * z[57];
z[6] = z[6] + 2 * z[7] + z[8] + z[11];
z[6] = z[6] * z[29];
z[7] = -abb[3] + -abb[4];
z[1] = abb[16] + abb[17] + z[1];
z[1] = z[1] * z[7];
z[7] = -abb[16] + z[56] + z[62];
z[7] = abb[0] * z[7];
z[7] = z[7] + z[20];
z[1] = z[1] + 2 * z[7];
z[1] = abb[25] * z[1];
z[7] = -abb[25] + -abb[26] + -z[29] + z[33];
z[7] = abb[5] * z[7] * z[39];
z[0] = z[0] + z[1] + 2 * z[3] + z[4] + z[5] + z[6] + 8 * z[7] + 4 * z[9] + z[10] + 8 * z[15];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[22] * z[19];
z[3] = -abb[23] * z[22];
z[4] = abb[24] * z[26];
z[0] = z[0] + z[1] + z[3] + z[4];
z[0] = 2 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_720_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("85.82822959517301995078194075540569081690467905248014305106918761"),stof<T>("-366.43469794955730345844814666486935900427346815089729014825310729")}, std::complex<T>{stof<T>("110.10022083314024766107863395918839928035576921437018988378153515"),stof<T>("-201.55549125586254250097952164134001672582754044864321697892421243")}, std::complex<T>{stof<T>("173.28531951184321132758307380122608417438317857644266069379167973"),stof<T>("741.48101368149162546049126484522143386514981737989553922726430652")}, std::complex<T>{stof<T>("129.24214816167475341246306725634623608784322671798466612882339287"),stof<T>("105.67307208760204353649949872290045993861017624707909667707826658")}, std::complex<T>{stof<T>("232.96858838789866024827924884856856513693471670213487501427687795"),stof<T>("697.14558027296734551342310485526773159391813362132062019248635007")}, std::complex<T>{stof<T>("-95.58074714905174317993131182440130980196755179602134784244911595"),stof<T>("-421.11027799944506457987834430361124538383088297247305522142308112")}, std::complex<T>{stof<T>("-101.802638596379944832199093468805884082538326508694811416119877059"),stof<T>("24.712815041801377212817677209845363688255429195941320046025989862")}, std::complex<T>{stof<T>("100.60661801771934845868193049851908584354998543391942344277196114"),stof<T>("-548.98522805482844115327074434794131287337655971971950939978923857")}, std::complex<T>{stof<T>("-41.150192767770661669416019985164273346656993035440859083424391595"),stof<T>("-0.526925298667398965645525917154584352643019593924613255714988305")}, std::complex<T>{stof<T>("-56.250364342825034197292396030392147800522393281432723520549987893"),stof<T>("24.064692754071116462780201881895565816936641131834828859184453559")}, std::complex<T>{stof<T>("-41.150192767770661669416019985164273346656993035440859083424391595"),stof<T>("-0.526925298667398965645525917154584352643019593924613255714988305")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[67].real()/kbase.W[67].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_720_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_720_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-1039.96353304791445303363901989106385512185192189961098538542513085"),stof<T>("-535.02395615171459008659550711935831086151703790219947436538848007")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,39> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W68(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[67].real()/k.W[67].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_720_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_720_DLogXconstant_part(base_point<T>, kend);
	value += f_4_720_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_720_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_720_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_720_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_720_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_720_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_720_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
