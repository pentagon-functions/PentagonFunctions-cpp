/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_578.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_578_abbreviated (const std::array<T,31>& abb) {
T z[40];
z[0] = 5 * abb[23];
z[1] = abb[19] + z[0];
z[2] = abb[21] * (T(1) / T(2));
z[3] = 3 * abb[20];
z[1] = (T(1) / T(2)) * z[1] + -z[2] + -z[3];
z[1] = -abb[24] + (T(1) / T(2)) * z[1];
z[1] = abb[0] * z[1];
z[4] = 2 * abb[24];
z[5] = 2 * abb[21] + z[4];
z[6] = 2 * abb[19];
z[7] = z[5] + -z[6];
z[8] = 3 * abb[23];
z[9] = 5 * abb[20] + -abb[22] + z[7] + -z[8];
z[10] = abb[1] * z[9];
z[11] = abb[20] * (T(3) / T(2));
z[12] = abb[23] * (T(1) / T(2)) + -z[11];
z[13] = abb[22] * (T(3) / T(2));
z[14] = z[12] + z[13];
z[15] = abb[19] + z[14];
z[15] = abb[6] * z[15];
z[16] = abb[21] + abb[24];
z[17] = abb[6] * z[16];
z[15] = -z[15] + z[17];
z[1] = z[1] + z[10] + -z[15];
z[10] = abb[19] + 7 * abb[23];
z[10] = -abb[22] + (T(1) / T(2)) * z[10];
z[18] = 2 * abb[20];
z[19] = abb[24] + z[18];
z[20] = abb[21] * (T(1) / T(4));
z[10] = (T(1) / T(2)) * z[10] + -z[19] + -z[20];
z[10] = abb[2] * z[10];
z[21] = -abb[19] + abb[21];
z[22] = abb[22] * (T(1) / T(2));
z[12] = z[12] + -z[21] + z[22];
z[12] = abb[3] * z[12];
z[23] = abb[26] + -abb[27];
z[24] = abb[25] + (T(1) / T(2)) * z[23];
z[25] = abb[8] * z[24];
z[26] = (T(3) / T(2)) * z[25];
z[27] = abb[7] * z[24];
z[10] = -z[1] + z[10] + z[12] + -z[26] + -z[27];
z[10] = abb[13] * z[10];
z[6] = abb[23] + z[6];
z[12] = 3 * abb[22];
z[3] = z[3] + z[5] + -z[6] + -z[12];
z[5] = abb[6] * z[3];
z[28] = abb[20] + -abb[23];
z[29] = abb[24] + z[28];
z[30] = 2 * abb[0];
z[31] = z[29] * z[30];
z[31] = z[5] + z[31];
z[32] = 4 * abb[24] + -z[12];
z[33] = 5 * abb[19];
z[34] = 11 * abb[20] + 5 * abb[21] + -6 * abb[23] + z[32] + -z[33];
z[34] = abb[1] * z[34];
z[9] = abb[2] * z[9];
z[18] = -abb[23] + z[18] + z[21];
z[18] = abb[4] * z[18];
z[9] = z[9] + -z[18] + -z[31] + z[34];
z[9] = abb[14] * z[9];
z[25] = (T(1) / T(2)) * z[25] + z[27];
z[1] = z[1] + -z[25];
z[1] = abb[12] * z[1];
z[33] = z[8] + z[33];
z[33] = (T(1) / T(2)) * z[33];
z[34] = z[12] + z[33];
z[19] = abb[21] * (T(-5) / T(4)) + -z[19] + (T(1) / T(2)) * z[34];
z[34] = abb[2] * abb[12];
z[19] = z[19] * z[34];
z[28] = abb[22] + z[28];
z[35] = abb[12] * z[28];
z[36] = abb[3] * z[35];
z[36] = (T(1) / T(2)) * z[36];
z[1] = z[1] + z[9] + z[10] + -z[19] + -z[36];
z[9] = abb[19] + abb[23];
z[10] = -abb[20] + -z[2] + (T(1) / T(2)) * z[9];
z[19] = abb[4] * z[10];
z[4] = -z[4] + z[13];
z[13] = 2 * abb[23] + abb[20] * (T(-7) / T(2)) + z[4] + (T(-3) / T(2)) * z[21];
z[13] = abb[1] * z[13];
z[37] = abb[24] + z[21];
z[14] = -z[14] + z[37];
z[38] = -abb[2] * z[14];
z[29] = abb[0] * z[29];
z[13] = z[13] + z[15] + z[19] + z[29] + z[38];
z[13] = abb[11] * z[13];
z[13] = z[1] + z[13];
z[13] = abb[11] * z[13];
z[17] = z[17] + z[29];
z[29] = abb[20] + -abb[22];
z[6] = (T(1) / T(3)) * z[6] + -z[29];
z[38] = abb[6] * z[6];
z[39] = -abb[23] + abb[24];
z[39] = -abb[22] + abb[20] * (T(7) / T(3)) + z[21] + (T(4) / T(3)) * z[39];
z[39] = abb[1] * z[39];
z[6] = -z[6] + (T(2) / T(3)) * z[16];
z[6] = abb[2] * z[6];
z[6] = z[6] + (T(-2) / T(3)) * z[17] + (T(1) / T(3)) * z[18] + z[38] + z[39];
z[6] = prod_pow(m1_set::bc<T>[0], 2) * z[6];
z[4] = -abb[23] + abb[19] * (T(-1) / T(2)) + z[2] + -z[4] + z[11];
z[4] = abb[7] * z[4];
z[16] = abb[19] + -abb[23];
z[2] = -z[2] + (T(1) / T(2)) * z[16];
z[12] = z[2] + z[12];
z[12] = -abb[24] + (T(1) / T(2)) * z[12];
z[12] = abb[8] * z[12];
z[17] = abb[0] * (T(1) / T(2)) + abb[2] * (T(3) / T(2));
z[17] = z[17] * z[24];
z[24] = abb[3] + -abb[10];
z[23] = 2 * abb[25] + z[23];
z[23] = z[23] * z[24];
z[14] = abb[9] * z[14];
z[4] = z[4] + -z[12] + -z[14] + z[17] + z[23];
z[12] = abb[29] * z[4];
z[14] = 7 * abb[20];
z[0] = abb[22] + -z[0] + z[7] + z[14];
z[0] = abb[1] * z[0];
z[0] = z[0] + -z[5];
z[5] = z[28] * z[30];
z[5] = -z[0] + z[5];
z[5] = abb[12] * z[5];
z[7] = 2 * abb[22] + -abb[24];
z[17] = abb[21] * (T(5) / T(2));
z[23] = -4 * abb[20] + z[7] + -z[17] + z[33];
z[23] = abb[1] * z[23];
z[22] = abb[23] * (T(-3) / T(2)) + abb[20] * (T(5) / T(2)) + -z[22] + z[37];
z[24] = -abb[2] * z[22];
z[33] = -abb[22] + abb[24];
z[37] = abb[0] * z[33];
z[19] = -3 * z[19] + z[23] + z[24] + z[37];
z[19] = abb[14] * z[19];
z[23] = 2 * abb[2];
z[24] = -z[23] * z[35];
z[5] = z[5] + z[19] + z[24];
z[5] = abb[14] * z[5];
z[9] = -abb[21] + z[9];
z[9] = (T(3) / T(4)) * z[9] + -z[11] + -z[33];
z[9] = abb[0] * z[9];
z[11] = abb[1] * z[22];
z[19] = -abb[19] + -z[8];
z[19] = -abb[22] + (T(1) / T(2)) * z[19];
z[19] = abb[20] + abb[24] + (T(1) / T(2)) * z[19] + z[20];
z[19] = abb[2] * z[19];
z[22] = z[21] + z[29];
z[22] = abb[3] * z[22];
z[9] = z[9] + z[11] + z[19] + (T(3) / T(2)) * z[22] + z[26];
z[9] = abb[13] * z[9];
z[8] = -abb[19] + z[8];
z[7] = abb[20] * (T(-1) / T(2)) + -z[7] + (T(1) / T(4)) * z[8] + z[20];
z[7] = abb[0] * z[7];
z[8] = abb[1] * z[28];
z[7] = z[7] + 2 * z[8] + -z[15] + z[25];
z[7] = abb[12] * z[7];
z[11] = 7 * abb[22] + (T(5) / T(2)) * z[16] + -z[17];
z[11] = -abb[24] + (T(1) / T(2)) * z[11];
z[11] = z[11] * z[34];
z[7] = z[7] + z[9] + z[11] + z[36];
z[7] = abb[13] * z[7];
z[3] = -abb[2] * z[3];
z[9] = -4 * abb[23] + z[14] + 3 * z[21] + z[32];
z[9] = abb[1] * z[9];
z[3] = -z[3] + z[9] + z[18] + -z[31];
z[9] = abb[28] * z[3];
z[10] = -abb[0] * z[10];
z[2] = abb[22] + z[2];
z[2] = abb[3] * z[2];
z[2] = z[2] + -z[8] + z[10] + z[27];
z[2] = prod_pow(abb[12], 2) * z[2];
z[8] = z[23] + -z[30];
z[8] = z[8] * z[28];
z[0] = z[0] + z[8];
z[0] = abb[15] * z[0];
z[0] = abb[30] + z[0] + z[2] + z[5] + z[6] + z[7] + z[9] + z[12] + z[13];
z[2] = abb[17] * z[4];
z[4] = abb[11] * z[3];
z[1] = -z[1] + z[4];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[16] * z[3];
z[1] = abb[18] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_578_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("1.6936675648640577315088984629966780825934093920957707473579124142"),stof<T>("-0.1373777706822379521961513516345162108958095317255918202397306975")}, std::complex<T>{stof<T>("2.9795724260916326824432717770985865750487014510108330902035943004"),stof<T>("9.302099203911671622919389552922617024242573525527527182105618056")}, std::complex<T>{stof<T>("-1.6936675648640577315088984629966780825934093920957707473579124142"),stof<T>("0.1373777706822379521961513516345162108958095317255918202397306975")}, std::complex<T>{stof<T>("-4.6732399909556904139521702400952646576421108431066038375615067145"),stof<T>("-9.1647214332294336707232382012881008133467639938019353618658873585")}, std::complex<T>{stof<T>("6.3669075558197481454610687030919427402355202352023745849194191287"),stof<T>("9.027343662547195718527086849653584602450954462076343541626156661")}, std::complex<T>{stof<T>("-1.7593518334562059155443784923489128405387359026509918272931048777"),stof<T>("1.2838897552460855887140042025642743542767977026226544070286342799")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}, std::complex<T>{stof<T>("0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("-0.64194487762304279435700210128213717713839885131132720351431713993")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[53].real()/kbase.W[53].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_578_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_578_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (8 + v[0] + -v[1] + -3 * v[2] + -3 * v[3] + 3 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 + -23 * v[0] + -9 * v[1] + -11 * v[2] + 5 * v[3] + 16 * v[4] + 8 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 11 * v[5])) / prod_pow(tend, 2);
c[2] = (v[0] + -v[1] + v[2] + v[3] + -v[5]) / tend;
c[3] = ((1 + 4 * m1_set::bc<T>[1]) * (T(-1) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * (abb[19] * c[0] + -(abb[21] + -3 * abb[22] + abb[23] + abb[24]) * c[0] + (abb[20] + abb[22] + -abb[23]) * c[1]) + abb[19] * c[2] + -abb[21] * c[2] + -abb[24] * c[2] + abb[20] * c[3] + -abb[23] * (c[2] + c[3]) + abb[22] * (3 * c[2] + c[3]);
	}
	{
T z[8];
z[0] = 2 * abb[11];
z[1] = -abb[13] + abb[14];
z[0] = z[0] * z[1];
z[2] = prod_pow(abb[13], 2);
z[3] = prod_pow(abb[14], 2);
z[2] = z[2] + -z[3];
z[3] = abb[12] * z[1];
z[4] = -z[0] + (T(1) / T(2)) * z[2] + 5 * z[3];
z[4] = abb[22] * z[4];
z[5] = 2 * abb[12];
z[1] = z[1] * z[5];
z[1] = z[1] + z[2];
z[5] = -abb[19] + abb[24];
z[6] = -z[1] * z[5];
z[7] = z[0] + (T(3) / T(2)) * z[2] + -z[3];
z[7] = abb[23] * z[7];
z[0] = -z[0] + (T(-5) / T(2)) * z[2] + -z[3];
z[0] = abb[20] * z[0];
z[1] = 2 * abb[15] + -z[1];
z[1] = abb[21] * z[1];
z[2] = abb[20] + -5 * abb[22] + abb[23] + 2 * z[5];
z[2] = abb[15] * z[2];
z[0] = z[0] + z[1] + z[2] + z[4] + z[6] + z[7];
return abb[5] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_578_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[20] + abb[22] + -abb[23]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = -abb[13] + abb[14];
z[1] = abb[20] + abb[22] + -abb[23];
return 2 * abb[5] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_578_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-5.3327076838268161126063737713208932148492932876997077151064121283"),stof<T>("-4.0844501200541244403007478007050454601030019292544973806803433009")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W29(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_15(k), f_2_2_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[53].real()/k.W[53].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), f_2_2_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_578_W_17_Im(t, path, abb);
abb[30] = SpDLog_f_4_578_W_17_Re(t, path, abb);

                    
            return f_4_578_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_578_DLogXconstant_part(base_point<T>, kend);
	value += f_4_578_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_578_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_578_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_578_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_578_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_578_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_578_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
