/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_597.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_597_abbreviated (const std::array<T,66>& abb) {
T z[157];
z[0] = abb[48] + abb[55];
z[1] = abb[49] * (T(1) / T(6));
z[2] = abb[45] * (T(1) / T(2));
z[3] = abb[53] * (T(2) / T(3));
z[4] = abb[54] * (T(1) / T(6));
z[5] = abb[56] * (T(1) / T(6));
z[6] = 2 * abb[58];
z[0] = abb[47] * (T(-3) / T(2)) + abb[50] * (T(-1) / T(3)) + (T(-7) / T(6)) * z[0] + z[1] + -z[2] + z[3] + z[4] + -z[5] + z[6];
z[0] = abb[4] * z[0];
z[7] = abb[50] * (T(1) / T(2));
z[8] = abb[49] * (T(1) / T(2));
z[9] = z[7] + -z[8];
z[10] = 2 * abb[47];
z[11] = -abb[58] + z[10];
z[12] = abb[45] * (T(13) / T(2));
z[13] = 2 * abb[55];
z[14] = abb[46] * (T(13) / T(2)) + -z[9] + z[11] + z[12] + z[13];
z[14] = -abb[53] + abb[44] * (T(-13) / T(6)) + abb[48] * (T(-3) / T(2)) + -z[5] + (T(1) / T(3)) * z[14];
z[14] = abb[3] * z[14];
z[15] = abb[49] + abb[50];
z[15] = (T(1) / T(2)) * z[15];
z[16] = abb[56] * (T(1) / T(2));
z[17] = abb[52] + z[16];
z[18] = -abb[44] + -z[15] + z[17];
z[19] = abb[16] * z[18];
z[20] = 2 * abb[51];
z[21] = -z[13] + z[20];
z[22] = abb[47] + abb[48];
z[23] = z[21] + z[22];
z[24] = -abb[56] + z[6];
z[25] = z[23] + -z[24];
z[25] = abb[17] * z[25];
z[26] = z[19] + z[25];
z[27] = -abb[53] + z[22];
z[1] = abb[58] + abb[51] * (T(-2) / T(3)) + abb[50] * (T(1) / T(6)) + -z[1] + -z[5] + -z[27];
z[1] = abb[6] * z[1];
z[28] = 13 * abb[47] + abb[50];
z[29] = abb[55] + -z[28];
z[12] = -abb[56] + 8 * abb[58] + -z[12] + (T(1) / T(2)) * z[29];
z[29] = abb[51] * (T(1) / T(3));
z[30] = 3 * abb[46];
z[12] = abb[44] * (T(-19) / T(6)) + abb[52] * (T(1) / T(6)) + (T(1) / T(3)) * z[12] + -z[29] + z[30];
z[12] = abb[0] * z[12];
z[28] = abb[55] + (T(1) / T(3)) * z[28];
z[4] = abb[48] * (T(-10) / T(3)) + abb[58] * (T(-7) / T(3)) + abb[45] * (T(17) / T(3)) + -z[4] + (T(1) / T(2)) * z[28] + -z[29];
z[4] = abb[1] * z[4];
z[28] = abb[47] + -abb[50];
z[29] = -abb[52] + abb[55] * (T(7) / T(3)) + -z[28];
z[31] = abb[48] * (T(1) / T(2));
z[3] = abb[44] * (T(-7) / T(6)) + abb[46] * (T(5) / T(3)) + -z[3] + (T(1) / T(2)) * z[29] + -z[31];
z[3] = abb[2] * z[3];
z[7] = z[7] + z[8];
z[2] = -abb[47] + -abb[52] + abb[58] + z[2] + z[7];
z[2] = abb[44] * (T(-1) / T(6)) + abb[46] * (T(1) / T(2)) + (T(1) / T(3)) * z[2] + -z[5] + -z[31];
z[2] = abb[8] * z[2];
z[5] = abb[59] + abb[60] + -abb[61];
z[29] = abb[22] * z[5];
z[31] = abb[24] * z[5];
z[32] = z[29] + z[31];
z[33] = abb[25] * z[5];
z[34] = z[32] + z[33];
z[35] = abb[27] * z[5];
z[36] = z[34] + -z[35];
z[37] = abb[23] * z[5];
z[38] = z[36] + z[37];
z[39] = -abb[46] + z[22];
z[40] = -abb[58] + z[39];
z[41] = abb[44] + z[40];
z[41] = abb[12] * z[41];
z[42] = abb[49] + -abb[55];
z[43] = 4 * z[42];
z[44] = abb[45] * (T(-29) / T(2)) + -z[43];
z[44] = abb[54] * (T(4) / T(3)) + abb[48] * (T(7) / T(2)) + (T(1) / T(3)) * z[44];
z[44] = abb[13] * z[44];
z[45] = -abb[52] + z[42];
z[45] = abb[46] * (T(-7) / T(2)) + abb[44] * (T(29) / T(6)) + (T(4) / T(3)) * z[45];
z[45] = abb[10] * z[45];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[12] + z[14] + (T(1) / T(3)) * z[26] + (T(1) / T(6)) * z[38] + (T(8) / T(3)) * z[41] + z[44] + z[45];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = z[9] + -z[16];
z[2] = 3 * abb[53];
z[3] = z[1] + z[2];
z[4] = 3 * abb[47];
z[12] = 3 * abb[48];
z[14] = 3 * abb[58];
z[26] = -z[3] + z[4] + z[12] + -z[14] + z[20];
z[26] = abb[6] * z[26];
z[26] = z[26] + (T(-1) / T(2)) * z[37];
z[38] = 4 * abb[51];
z[44] = 4 * abb[55];
z[45] = z[38] + -z[44];
z[46] = 2 * abb[48];
z[47] = z[10] + z[46];
z[48] = abb[49] + -abb[50];
z[49] = 2 * abb[53];
z[50] = -z[48] + z[49];
z[51] = -z[24] + z[45] + z[47] + -z[50];
z[52] = abb[15] * z[51];
z[53] = abb[26] * z[5];
z[54] = z[52] + -z[53];
z[55] = -z[24] + z[50];
z[56] = abb[3] * z[55];
z[57] = -z[54] + z[56];
z[58] = abb[4] * z[55];
z[59] = z[57] + z[58];
z[60] = 4 * abb[44];
z[61] = 2 * abb[46];
z[62] = 2 * abb[52];
z[63] = -abb[50] + -z[44];
z[63] = 6 * abb[51] + abb[56] + abb[58] + -z[60] + z[61] + z[62] + 2 * z[63];
z[63] = abb[0] * z[63];
z[64] = z[20] + z[46];
z[65] = 5 * abb[53];
z[66] = 2 * abb[44];
z[67] = abb[46] + abb[49] + abb[52] + 2 * z[28] + z[64] + -z[65] + -z[66];
z[67] = abb[8] * z[67];
z[68] = z[33] + z[35];
z[69] = 5 * abb[44] + -z[30] + -z[62];
z[42] = 2 * z[42];
z[70] = z[42] + z[69];
z[70] = abb[10] * z[70];
z[71] = -abb[50] + abb[52];
z[22] = z[22] + z[71];
z[72] = abb[44] + -abb[55];
z[72] = 11 * abb[46] + -9 * abb[53] + -z[22] + -10 * z[72];
z[72] = abb[2] * z[72];
z[73] = -abb[49] + abb[50] + z[13];
z[74] = -abb[56] + z[14];
z[73] = 2 * z[73] + -z[74];
z[75] = abb[9] * z[73];
z[63] = -z[19] + -z[26] + z[41] + z[59] + z[63] + z[67] + (T(1) / T(2)) * z[68] + z[70] + z[72] + z[75];
z[63] = abb[30] * z[63];
z[67] = z[2] + -z[13] + z[66];
z[22] = abb[46] + z[22] + -z[67];
z[72] = 2 * abb[2];
z[22] = z[22] * z[72];
z[75] = abb[56] + -abb[58];
z[76] = z[21] + z[75];
z[77] = abb[44] + -abb[46];
z[78] = z[76] + -z[77];
z[79] = 2 * abb[0];
z[78] = z[78] * z[79];
z[80] = -abb[49] + abb[56];
z[81] = -abb[50] + z[80];
z[82] = z[62] + z[81];
z[83] = -z[66] + z[82];
z[84] = abb[16] * z[83];
z[84] = -z[35] + z[84];
z[85] = 2 * z[41];
z[86] = z[84] + z[85];
z[78] = z[78] + -z[86];
z[87] = -z[22] + -z[57] + -z[78];
z[88] = abb[29] + abb[31];
z[87] = z[87] * z[88];
z[88] = abb[56] + z[47];
z[89] = 6 * abb[58];
z[90] = 8 * abb[55] + -z[38] + -3 * z[48] + z[49] + z[88] + -z[89];
z[90] = abb[15] * z[90];
z[90] = -z[33] + -z[53] + z[90];
z[91] = 6 * abb[53];
z[92] = abb[56] + z[48];
z[93] = z[91] + -z[92];
z[94] = 6 * abb[48];
z[95] = -6 * abb[47] + -z[38] + z[89] + z[93] + -z[94];
z[95] = abb[6] * z[95];
z[96] = 2 * abb[9];
z[97] = z[73] * z[96];
z[98] = z[90] + -z[95] + -z[97];
z[99] = -abb[56] + z[48];
z[89] = z[89] + z[99];
z[100] = 4 * abb[47];
z[101] = z[44] + z[100];
z[102] = 4 * abb[48];
z[103] = z[101] + z[102];
z[104] = z[49] + z[89] + -z[103];
z[105] = abb[3] * z[104];
z[105] = z[98] + z[105];
z[105] = abb[33] * z[105];
z[39] = -abb[53] + z[39] + z[71];
z[106] = abb[31] * z[39];
z[107] = abb[29] * z[39];
z[108] = z[106] + z[107];
z[23] = z[23] + -z[50];
z[50] = -abb[33] * z[23];
z[50] = z[50] + -z[108];
z[109] = 2 * abb[8];
z[50] = z[50] * z[109];
z[110] = abb[33] * z[104];
z[111] = abb[29] * z[55];
z[112] = abb[31] * z[55];
z[110] = z[110] + -z[111] + -z[112];
z[110] = abb[4] * z[110];
z[113] = abb[33] * z[37];
z[50] = z[50] + z[63] + z[87] + z[105] + z[110] + -z[113];
z[50] = abb[30] * z[50];
z[53] = z[32] + z[53];
z[52] = z[52] + -z[53];
z[63] = -z[13] + z[14];
z[87] = -abb[51] + abb[53];
z[105] = abb[45] + abb[47];
z[110] = -z[63] + -z[87] + -z[99] + z[105];
z[110] = abb[9] * z[110];
z[114] = -abb[45] + abb[54];
z[15] = -z[15] + z[16] + z[114];
z[15] = abb[5] * z[15];
z[9] = z[9] + z[16];
z[115] = abb[51] + z[9];
z[116] = -abb[57] + z[115];
z[117] = abb[14] * z[116];
z[118] = z[25] + z[117];
z[119] = abb[47] + abb[55];
z[119] = -z[20] + 2 * z[119];
z[120] = abb[48] + abb[57];
z[121] = abb[45] + -z[14] + z[119] + z[120];
z[122] = -abb[3] * z[121];
z[123] = 4 * abb[58];
z[124] = 2 * abb[57];
z[125] = -abb[56] + -z[105] + z[123] + -z[124];
z[125] = abb[1] * z[125];
z[126] = 2 * abb[54];
z[127] = 5 * abb[45] + -z[12] + -z[126];
z[42] = z[42] + z[127];
z[42] = abb[13] * z[42];
z[128] = 2 * abb[45];
z[129] = -z[46] + z[128];
z[28] = abb[54] + z[28];
z[130] = abb[51] + abb[57] + -abb[58] + -z[28] + -z[129];
z[130] = abb[8] * z[130];
z[131] = -z[49] + z[99];
z[132] = -abb[58] + -z[105] + -z[131];
z[132] = abb[4] * z[132];
z[110] = z[15] + (T(-1) / T(2)) * z[33] + z[42] + z[52] + z[110] + -z[118] + z[122] + z[125] + z[130] + z[132];
z[110] = abb[34] * z[110];
z[122] = -z[34] + z[54];
z[125] = 2 * abb[1];
z[121] = z[121] * z[125];
z[130] = z[99] + z[124];
z[132] = -z[20] + z[130];
z[133] = abb[14] * z[132];
z[121] = z[121] + -z[133];
z[134] = -abb[51] + z[105];
z[135] = -abb[55] + abb[58];
z[136] = z[134] + -z[135];
z[137] = 4 * z[136];
z[138] = abb[3] * z[137];
z[139] = 2 * z[25];
z[138] = z[138] + z[139];
z[140] = z[121] + -z[122] + z[138];
z[141] = abb[29] + -abb[31];
z[140] = z[140] * z[141];
z[88] = -12 * abb[51] + 16 * abb[55] + -z[6] + -5 * z[48] + -z[88] + z[91];
z[88] = abb[15] * z[88];
z[142] = -abb[45] + abb[48];
z[143] = z[76] + z[142];
z[143] = z[125] * z[143];
z[81] = z[81] + z[126];
z[144] = z[81] + -z[128];
z[145] = abb[5] * z[144];
z[143] = z[143] + -z[145];
z[88] = z[53] + z[88] + z[139] + z[143];
z[88] = abb[33] * z[88];
z[24] = z[24] + z[48];
z[146] = -z[20] + z[24] + z[129];
z[146] = z[141] * z[146];
z[147] = abb[50] + abb[56];
z[148] = -z[6] + z[147];
z[149] = abb[49] + z[148];
z[150] = z[46] + -z[126] + z[149];
z[151] = -z[45] + -z[150];
z[151] = abb[33] * z[151];
z[146] = z[146] + z[151];
z[146] = abb[8] * z[146];
z[151] = 4 * abb[45];
z[152] = z[91] + z[151];
z[24] = z[24] + z[44] + z[102] + -z[152];
z[24] = abb[33] * z[24];
z[24] = z[24] + -z[111] + z[112];
z[24] = abb[4] * z[24];
z[102] = z[10] + z[128];
z[111] = -abb[57] + z[102];
z[153] = z[13] + z[87];
z[14] = -z[14] + -z[99] + z[111] + z[153];
z[141] = -z[14] * z[141];
z[154] = abb[33] * z[73];
z[141] = z[141] + -z[154];
z[141] = z[96] * z[141];
z[24] = z[24] + z[88] + z[110] + z[140] + z[141] + z[146];
z[24] = abb[34] * z[24];
z[88] = z[7] + z[16];
z[110] = abb[54] + z[77] + -z[88] + z[120] + -z[128];
z[110] = abb[8] * z[110];
z[140] = -z[77] + z[116] + -z[142];
z[140] = abb[3] * z[140];
z[71] = abb[46] + -z[71] + -z[134];
z[71] = abb[0] * z[71];
z[134] = z[124] + -z[128];
z[141] = -abb[47] + abb[48];
z[146] = abb[56] + -z[134] + -z[141];
z[146] = abb[1] * z[146];
z[155] = abb[2] * z[39];
z[156] = abb[53] + z[9] + -z[105];
z[156] = abb[4] * z[156];
z[15] = -z[15] + (T(-1) / T(2)) * z[34] + z[71] + z[110] + -z[117] + z[140] + z[146] + z[155] + z[156];
z[15] = abb[32] * z[15];
z[29] = z[29] + z[31] + z[68];
z[29] = -z[19] + (T(1) / T(2)) * z[29] + -z[54];
z[31] = -abb[45] + -abb[58] + z[120];
z[68] = z[31] * z[125];
z[68] = z[68] + z[117];
z[71] = -z[48] + -z[105] + z[153];
z[110] = z[71] * z[96];
z[66] = z[61] + -z[66];
z[115] = -abb[57] + z[6] + -z[66] + -z[115];
z[115] = abb[3] * z[115];
z[120] = -abb[44] + z[61];
z[140] = -abb[52] + z[120];
z[146] = abb[51] + z[140];
z[153] = z[102] + -z[146] + -z[147];
z[153] = abb[0] * z[153];
z[115] = -z[29] + -z[68] + z[110] + z[115] + z[153];
z[115] = abb[29] * z[115];
z[10] = z[10] + z[151];
z[64] = z[10] + -z[64] + z[92] + -z[124];
z[64] = abb[8] * z[64];
z[92] = 3 * abb[45];
z[153] = -abb[47] + -abb[58] + z[46] + -z[92] + z[124];
z[153] = z[125] * z[153];
z[131] = z[102] + z[131];
z[131] = abb[4] * z[131];
z[31] = abb[3] * z[31];
z[31] = 2 * z[31] + z[64] + -z[110] + -z[122] + z[131] + -z[133] + z[153];
z[31] = abb[34] * z[31];
z[64] = z[146] + z[148];
z[64] = abb[0] * z[64];
z[36] = (T(1) / T(2)) * z[36];
z[110] = -abb[3] * z[116];
z[68] = z[19] + z[36] + -z[64] + z[68] + z[85] + z[110];
z[68] = abb[31] * z[68];
z[22] = z[22] + z[78];
z[78] = z[39] * z[109];
z[59] = z[22] + z[59] + z[78];
z[59] = abb[30] * z[59];
z[78] = z[72] * z[108];
z[59] = z[59] + -z[78];
z[57] = -z[57] + -z[143];
z[57] = abb[33] * z[57];
z[78] = abb[50] * (T(3) / T(2));
z[108] = -z[8] + z[20] + z[78];
z[110] = -z[17] + z[108] + -z[111] + z[120];
z[110] = abb[29] * z[110];
z[88] = -abb[57] + z[88];
z[120] = z[88] + z[129] + z[140];
z[120] = abb[31] * z[120];
z[28] = abb[53] + -z[28];
z[28] = abb[33] * z[28];
z[28] = 2 * z[28] + z[110] + z[120];
z[28] = abb[8] * z[28];
z[110] = abb[53] + -abb[55];
z[120] = z[110] + -z[142];
z[120] = abb[33] * z[120];
z[120] = -z[112] + 4 * z[120];
z[120] = abb[4] * z[120];
z[15] = z[15] + z[28] + z[31] + z[57] + z[59] + z[68] + z[115] + z[120];
z[15] = abb[32] * z[15];
z[28] = z[101] + z[151];
z[31] = 3 * abb[51];
z[57] = -abb[57] + z[9] + z[28] + -z[31] + -z[123];
z[57] = abb[3] * z[57];
z[68] = abb[1] * z[137];
z[29] = z[29] + z[57] + z[64] + z[68] + -z[85] + z[117] + z[139];
z[29] = abb[31] * z[29];
z[57] = z[125] * z[136];
z[19] = z[19] + z[57];
z[57] = abb[57] + abb[58] + -z[77] + -z[119] + -z[128];
z[57] = abb[3] * z[57];
z[21] = abb[56] + z[21] + -z[66] + -z[105];
z[21] = abb[0] * z[21];
z[33] = -z[33] + z[35];
z[21] = -z[19] + z[21] + (T(1) / T(2)) * z[33] + z[57] + -z[70] + -z[118];
z[21] = abb[29] * z[21];
z[33] = 3 * abb[55];
z[35] = abb[53] + -abb[58];
z[57] = -z[20] + z[33] + z[35] + -z[48];
z[64] = 4 * abb[15];
z[57] = z[57] * z[64];
z[56] = z[32] + -z[56] + z[57];
z[64] = -abb[33] * z[56];
z[85] = abb[33] * z[139];
z[21] = z[21] + z[29] + z[64] + -z[85];
z[21] = abb[29] * z[21];
z[29] = -abb[53] + z[9] + z[47] + -z[63];
z[29] = abb[3] * z[29];
z[33] = abb[50] + -abb[58] + z[33];
z[33] = -abb[48] + 2 * z[33] + -z[38] + z[92] + -z[126];
z[33] = abb[1] * z[33];
z[29] = -z[25] + z[29] + (T(-1) / T(2)) * z[32] + z[33] + -z[42] + -z[57];
z[32] = prod_pow(abb[33], 2);
z[29] = z[29] * z[32];
z[33] = abb[58] + z[9] + -z[61] + z[67] + -z[102];
z[33] = abb[3] * z[33];
z[57] = -z[74] + z[102];
z[64] = z[57] + -z[77];
z[67] = abb[0] * z[64];
z[19] = -z[19] + -z[25] + z[26] + z[33] + -z[36] + -3 * z[41] + z[67];
z[19] = abb[31] * z[19];
z[25] = z[34] + z[95];
z[26] = z[27] + -z[135];
z[26] = 4 * z[26];
z[27] = abb[3] * z[26];
z[27] = z[25] + z[27] + -z[54];
z[27] = abb[33] * z[27];
z[27] = z[27] + z[85] + z[113];
z[19] = z[19] + z[27];
z[19] = abb[31] * z[19];
z[33] = 6 * abb[55] + z[94];
z[8] = 6 * abb[45] + -z[8] + z[11] + z[16] + -z[33] + z[65] + -z[78] + z[126];
z[8] = z[8] * z[32];
z[11] = abb[33] * z[26];
z[16] = z[9] + z[35];
z[26] = abb[31] * z[16];
z[26] = z[11] + z[26];
z[26] = abb[31] * z[26];
z[36] = abb[33] * z[55];
z[16] = abb[29] * z[16];
z[16] = z[16] + z[36];
z[16] = abb[29] * z[16];
z[54] = abb[36] * z[144];
z[8] = z[8] + z[16] + z[26] + z[54];
z[8] = abb[4] * z[8];
z[16] = abb[50] + 5 * abb[55] + -abb[56];
z[26] = 8 * abb[51];
z[16] = 2 * z[16] + -z[26];
z[54] = z[16] + z[69];
z[54] = abb[0] * z[54];
z[26] = z[26] + -z[44] + -z[93];
z[55] = abb[15] * z[26];
z[53] = -z[53] + z[55];
z[30] = abb[44] + -4 * abb[52] + z[30] + z[43];
z[30] = abb[10] * z[30];
z[55] = z[77] + z[110];
z[65] = 6 * abb[2];
z[55] = z[55] * z[65];
z[65] = abb[8] * z[83];
z[30] = z[30] + z[53] + z[54] + z[55] + -z[65] + z[84];
z[54] = abb[62] * z[30];
z[45] = z[45] + -z[49] + z[89] + -z[100] + -z[151];
z[45] = abb[3] * z[45];
z[48] = -z[13] + z[48] + z[87] + z[111];
z[48] = z[48] * z[96];
z[55] = z[71] * z[109];
z[45] = z[45] + z[48] + z[55] + z[90] + -z[121];
z[45] = abb[35] * z[45];
z[33] = -z[33] + z[81] + z[152];
z[33] = abb[4] * z[33];
z[16] = z[16] + z[127];
z[16] = abb[1] * z[16];
z[12] = abb[45] + -4 * abb[54] + z[12] + z[43];
z[12] = abb[13] * z[12];
z[43] = abb[8] * z[144];
z[12] = z[12] + z[16] + z[33] + -z[43] + z[53] + z[145];
z[16] = -abb[64] * z[12];
z[33] = abb[33] * z[26];
z[3] = z[3] + z[13] + -z[38];
z[13] = -abb[29] * z[3];
z[13] = z[13] + -z[33];
z[13] = abb[29] * z[13];
z[38] = abb[30] * z[3];
z[38] = z[33] + z[38];
z[38] = abb[30] * z[38];
z[43] = abb[34] * z[3];
z[33] = z[33] + z[43];
z[33] = abb[34] * z[33];
z[43] = abb[35] + -abb[37] + -abb[38] + -abb[62] + abb[64];
z[43] = z[26] * z[43];
z[3] = z[3] * z[32];
z[3] = z[3] + z[13] + z[33] + z[38] + z[43];
z[3] = abb[7] * z[3];
z[13] = abb[51] + -z[114] + -z[124] + z[147];
z[13] = z[13] * z[125];
z[33] = -abb[3] * z[132];
z[13] = z[13] + z[33] + -z[34] + z[133] + -z[145];
z[13] = abb[36] * z[13];
z[1] = -z[1] + z[20] + z[35] + -z[44] + z[128] + -z[141];
z[1] = z[1] * z[32];
z[33] = abb[33] * z[51];
z[34] = abb[57] + abb[56] * (T(-3) / T(2)) + z[6] + -z[108] + -z[140];
z[34] = abb[31] * z[34];
z[35] = abb[51] + -abb[57];
z[38] = abb[52] + z[35] + z[40] + z[80];
z[38] = abb[29] * z[38];
z[33] = z[33] + z[34] + z[38];
z[33] = abb[29] * z[33];
z[34] = -abb[49] + z[126] + z[134] + -z[147];
z[34] = abb[36] * z[34];
z[7] = -z[7] + z[17] + z[40];
z[7] = abb[31] * z[7];
z[7] = z[7] + -z[36];
z[7] = abb[31] * z[7];
z[1] = z[1] + z[7] + z[33] + z[34];
z[1] = abb[8] * z[1];
z[2] = z[2] + z[4] + -z[31] + -z[63] + z[92] + -z[130];
z[2] = abb[29] * z[2];
z[4] = -abb[31] * z[14];
z[4] = z[4] + z[154];
z[2] = z[2] + 2 * z[4];
z[2] = abb[29] * z[2];
z[4] = z[32] * z[73];
z[2] = z[2] + z[4];
z[2] = abb[9] * z[2];
z[4] = z[6] + -z[47] + z[61] + -z[82];
z[7] = abb[8] * z[4];
z[14] = z[25] + z[37] + z[139];
z[6] = z[6] + z[91] + -z[99];
z[17] = z[6] + -z[103];
z[25] = abb[3] + abb[4];
z[17] = z[17] * z[25];
z[7] = z[7] + z[14] + -z[17] + -z[22];
z[17] = abb[63] * z[7];
z[22] = abb[8] * z[51];
z[22] = z[22] + -z[56] + z[58] + z[97] + -z[139];
z[22] = abb[38] * z[22];
z[25] = -z[25] * z[104];
z[23] = z[23] * z[109];
z[23] = z[23] + z[25] + z[37] + -z[98];
z[23] = abb[37] * z[23];
z[9] = z[9] + z[35];
z[9] = abb[20] * z[9];
z[25] = abb[19] * z[116];
z[18] = -abb[21] * z[18];
z[31] = -abb[44] + abb[52] + -z[88];
z[31] = abb[18] * z[31];
z[5] = abb[28] * z[5];
z[5] = (T(1) / T(2)) * z[5] + z[9] + z[18] + z[25] + z[31];
z[5] = abb[39] * z[5];
z[9] = 2 * z[106] + z[107];
z[9] = abb[29] * z[9];
z[18] = prod_pow(abb[31], 2) * z[39];
z[9] = z[9] + z[18];
z[9] = abb[2] * z[9];
z[0] = abb[65] + z[0] + z[1] + z[2] + z[3] + z[5] + z[8] + z[9] + z[13] + z[15] + z[16] + z[17] + z[19] + z[21] + z[22] + z[23] + z[24] + z[29] + z[45] + z[50] + z[54];
z[1] = z[58] + z[145];
z[2] = z[75] + -z[77];
z[2] = z[2] * z[79];
z[3] = -z[75] + -z[142];
z[3] = z[3] * z[125];
z[5] = z[39] * z[72];
z[8] = -abb[46] + abb[48] + abb[52] + -abb[54];
z[8] = z[8] * z[109];
z[2] = z[1] + z[2] + z[3] + z[5] + z[8] + -z[86];
z[2] = abb[32] * z[2];
z[3] = z[20] + -z[49] + z[57];
z[3] = z[3] * z[96];
z[3] = z[3] + z[52] + -z[138];
z[5] = z[66] + -z[76];
z[5] = z[5] * z[79];
z[5] = z[3] + z[5] + -z[68] + 2 * z[70] + z[86];
z[5] = abb[29] * z[5];
z[6] = 4 * abb[46] + -z[6] + z[28] + -z[60];
z[6] = abb[3] * z[6];
z[8] = -z[64] * z[79];
z[6] = z[6] + z[8] + z[14] + 6 * z[41] + z[68] + z[84];
z[6] = abb[31] * z[6];
z[8] = z[10] + -z[46] + -z[74];
z[8] = z[8] * z[125];
z[9] = -abb[8] * z[150];
z[1] = -z[1] + -z[3] + z[8] + z[9] + -2 * z[42];
z[1] = abb[34] * z[1];
z[3] = abb[31] * z[4];
z[4] = z[61] + -z[62] + z[149];
z[4] = abb[29] * z[4];
z[3] = z[3] + z[4] + z[36];
z[3] = abb[8] * z[3];
z[4] = -z[11] + -z[112];
z[4] = abb[4] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + -z[27] + z[59];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[40] * z[30];
z[3] = -abb[42] * z[12];
z[4] = abb[41] * z[7];
z[5] = -abb[29] + abb[34];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = -abb[40] + abb[42] + z[5];
z[5] = abb[7] * z[5] * z[26];
z[1] = abb[43] + z[1] + z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_597_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("44.23713195026187218143881542947823139797227645809737307457557201"),stof<T>("49.502518072288631026120604341123315498076331506729782953237808943")}, std::complex<T>{stof<T>("10.624334149109474899015143382588251795531220022564749627778395759"),stof<T>("-65.997105283720751541947529653951386511906066061300445624224606473")}, std::complex<T>{stof<T>("-17.3569211333895307305289182288822151281541099593310891383717436"),stof<T>("-44.846794470766763377782877834095880215250836180536111657441677762")}, std::complex<T>{stof<T>("37.850914001008348630011643986486117162459806765911487901008950891"),stof<T>("-9.085999239260718923919559126710554407860939987189425975425467942")}, std::complex<T>{stof<T>("-4.245631642495581397110755505521075020049942636079332116292261972"),stof<T>("37.167899974669224478950882713679889553150186654647579881421082561")}, std::complex<T>{stof<T>("-9.6828187376176999207974413199552490311688575092476137340277730479"),stof<T>("-9.197316798543106472178986495581096598325966055993558603818360175")}, std::complex<T>{stof<T>("5.0908180600955862436000824111323249140084946285878272807087843549"),stof<T>("-5.8901656697258340185603748109524106697434780372762098677635646154")}, std::complex<T>{stof<T>("-7.98428230247421031110172843502801663774093850048728549049301454"),stof<T>("36.182301262038450442165442601245304117603097762223614328378193531")}, std::complex<T>{stof<T>("-26.880210816872341450909897200596016269818166498766283936203828411"),stof<T>("-4.65572360152186764833772650702743528282549532619367129579613118")}, std::complex<T>{stof<T>("31.472211494394455128107256109418940386978529379426070389522817104"),stof<T>("19.743206069790808139077087813560942550894939419463439767378055971")}, std::complex<T>{stof<T>("22.757919100187496475499252166115590582918290638322726505229571943"),stof<T>("-32.875150133221177988546830916616618189020609743506265592323397971")}, std::complex<T>{stof<T>("3.3676899982809686022714939669936017099032012018749873784947527717"),stof<T>("-0.6711171654161685410993605695944634682903763857024034947602507578")}, stof<T>("-8.1467639364098125700497265545092086142014652425291793387481963645"), std::complex<T>{stof<T>("-38.162658122975090905833493810102835172170037353845123221464291653"),stof<T>("15.647282074402721483579294507257428545894794410168039337949283315")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("-1.5961736222681129079204730221135863214600019276878811243393385392")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[36].real()/kbase.W[36].real()), rlog(k.W[75].real()/kbase.W[75].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_597_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_597_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(112)) * (v[2] + v[3]) * (56 * (-4 * m1_set::bc<T>[1] + -2 * m1_set::bc<T>[4] + m1_set::bc<T>[2] * (3 + v[0])) + -42 * v[2] + -42 * v[3] + 40 * v[4] + 56 * (m1_set::bc<T>[1] * (v[2] + v[3]) + v[5] + -2 * m1_set::bc<T>[1] * v[5]) + 7 * (4 * (-2 + m1_set::bc<T>[2]) * v[1] + 4 * m1_set::bc<T>[4] * (v[2] + v[3] + -2 * v[5]) + m1_set::bc<T>[2] * (v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5])))) / prod_pow(tend, 2);
c[1] = ((4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[2] + v[3])) / tend;


		return (abb[47] + abb[48] + abb[49] + -abb[50] + 2 * abb[51] + -2 * abb[53] + -2 * abb[55]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[5];
z[0] = abb[35] + -2 * abb[37] + -abb[38];
z[1] = abb[51] + -abb[55];
z[1] = -abb[47] + -abb[48] + -abb[49] + abb[50] + 2 * abb[53] + -2 * z[1];
z[0] = z[0] * z[1];
z[2] = abb[30] * z[1];
z[3] = abb[33] * z[1];
z[4] = -z[2] + z[3];
z[4] = abb[32] * z[4];
z[0] = z[0] + z[4];
z[4] = abb[29] + abb[31];
z[1] = z[1] * z[4];
z[4] = -2 * z[1] + z[3];
z[4] = abb[33] * z[4];
z[1] = z[1] + 2 * z[3];
z[1] = 2 * z[1] + -5 * z[2];
z[1] = abb[30] * z[1];
z[0] = 2 * z[0] + z[1] + z[4];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_597_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[47] + abb[48] + abb[49] + -abb[50] + 2 * abb[51] + -2 * abb[53] + -2 * abb[55]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = abb[53] + abb[55];
z[0] = -abb[47] + -abb[48] + -abb[49] + abb[50] + -2 * abb[51] + 2 * z[0];
z[1] = -abb[30] + abb[33];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_597_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-34.088696593189916341299168418629364323798529415304722665947423388"),stof<T>("-51.493598445318449833808303002488252352723527170284346934832118752")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,66> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W76(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[36].real()/k.W[36].real()), rlog(kend.W[75].real()/k.W[75].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), T{0}};
abb[43] = SpDLog_f_4_597_W_19_Im(t, path, abb);
abb[65] = SpDLog_f_4_597_W_19_Re(t, path, abb);

                    
            return f_4_597_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_597_DLogXconstant_part(base_point<T>, kend);
	value += f_4_597_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_597_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_597_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_597_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_597_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_597_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_597_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
