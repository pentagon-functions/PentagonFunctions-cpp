/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_72.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_72_abbreviated (const std::array<T,30>& abb) {
T z[41];
z[0] = abb[23] + 2 * abb[24];
z[1] = 2 * abb[22];
z[2] = z[0] + -z[1];
z[3] = -abb[20] + abb[26];
z[4] = 2 * abb[25];
z[5] = z[2] + -z[3] + z[4];
z[6] = abb[8] * z[5];
z[7] = 2 * abb[26];
z[8] = -z[4] + z[7];
z[9] = abb[22] + -z[0];
z[10] = 2 * abb[21];
z[11] = z[8] + z[9] + -z[10];
z[12] = abb[6] * z[11];
z[13] = 2 * abb[20];
z[8] = z[8] + -z[9] + -z[13];
z[8] = abb[3] * z[8];
z[14] = -abb[25] + z[3];
z[15] = abb[1] * z[14];
z[16] = 2 * z[15];
z[17] = z[8] + z[16];
z[1] = -abb[25] + z[1];
z[18] = abb[20] * (T(1) / T(2));
z[19] = -abb[21] + abb[26] * (T(1) / T(2)) + z[18];
z[20] = abb[24] + abb[23] * (T(1) / T(2));
z[21] = -z[1] + z[19] + z[20];
z[21] = abb[0] * z[21];
z[22] = abb[21] + abb[25];
z[23] = -abb[22] + z[20];
z[24] = abb[26] * (T(-3) / T(2)) + z[18] + z[22] + -z[23];
z[24] = abb[9] * z[24];
z[25] = abb[20] + abb[26];
z[26] = -z[10] + z[25];
z[0] = z[0] + -z[26];
z[0] = abb[2] * z[0];
z[27] = -abb[25] + z[19] + -z[23];
z[27] = abb[4] * z[27];
z[28] = -abb[22] + z[3];
z[29] = abb[5] * z[28];
z[21] = z[0] + z[6] + z[12] + z[17] + z[21] + 3 * z[24] + z[27] + -z[29];
z[21] = abb[16] * z[21];
z[24] = z[19] + z[23];
z[24] = abb[0] * z[24];
z[27] = abb[22] * (T(1) / T(2)) + -z[20];
z[14] = -z[14] + z[27];
z[14] = abb[3] * z[14];
z[24] = -z[14] + z[24] + (T(-1) / T(2)) * z[29];
z[10] = z[4] + z[10];
z[30] = abb[20] + -3 * abb[26] + -z[2] + z[10];
z[30] = abb[9] * z[30];
z[31] = abb[26] + -z[22];
z[32] = z[27] + z[31];
z[32] = abb[6] * z[32];
z[19] = z[19] + -z[20];
z[19] = abb[2] * z[19];
z[33] = abb[25] * (T(1) / T(2));
z[34] = z[23] + z[33];
z[35] = abb[20] + -abb[21] + -z[34];
z[35] = abb[4] * z[35];
z[36] = abb[8] * z[34];
z[35] = z[15] + -z[19] + z[24] + z[30] + z[32] + (T(1) / T(2)) * z[35] + (T(3) / T(2)) * z[36];
z[35] = abb[11] * z[35];
z[2] = z[2] + z[26];
z[2] = abb[0] * z[2];
z[10] = abb[22] + -z[10] + z[25];
z[10] = abb[4] * z[10];
z[10] = z[2] + z[8] + z[10] + z[30];
z[10] = abb[10] * z[10];
z[0] = z[0] + z[8] + z[30];
z[25] = -abb[21] + abb[26];
z[4] = abb[22] + -z[4] + z[25];
z[36] = abb[4] * z[4];
z[37] = -abb[22] + z[25];
z[38] = 2 * abb[0];
z[39] = z[37] * z[38];
z[36] = z[0] + -z[29] + z[36] + z[39];
z[40] = -abb[13] * z[36];
z[10] = z[10] + z[35] + z[40];
z[10] = abb[11] * z[10];
z[34] = abb[4] * z[34];
z[7] = -z[7] + z[13];
z[13] = abb[25] * (T(7) / T(4)) + z[7] + (T(-1) / T(2)) * z[23];
z[13] = abb[8] * z[13];
z[35] = -abb[0] * z[4];
z[13] = z[13] + -z[14] + z[16] + -z[29] + z[32] + (T(-1) / T(2)) * z[34] + z[35];
z[13] = abb[12] * z[13];
z[17] = z[17] + z[30];
z[7] = abb[25] * (T(5) / T(2)) + z[7] + z[23];
z[7] = abb[8] * z[7];
z[7] = z[7] + z[12];
z[5] = abb[0] * z[5];
z[26] = abb[25] * (T(-3) / T(2)) + -z[23] + z[26];
z[26] = abb[4] * z[26];
z[5] = z[5] + z[7] + z[17] + z[26];
z[26] = -abb[11] * z[5];
z[13] = z[13] + z[26];
z[13] = abb[12] * z[13];
z[11] = abb[4] * z[11];
z[26] = 2 * z[9] + z[31];
z[26] = abb[6] * z[26];
z[1] = z[1] + -z[25];
z[1] = abb[0] * z[1];
z[0] = z[0] + -z[1] + z[6] + z[11] + z[26];
z[1] = abb[27] * z[0];
z[6] = -z[9] + z[31];
z[6] = abb[6] * z[6];
z[11] = abb[21] * (T(-1) / T(2)) + z[18] + z[27];
z[11] = abb[4] * z[11];
z[11] = -z[6] + z[11] + z[19] + z[24];
z[11] = prod_pow(abb[13], 2) * z[11];
z[24] = -abb[28] * z[5];
z[25] = abb[26] * (T(11) / T(2));
z[26] = z[9] + (T(11) / T(2)) * z[22] + -z[25];
z[26] = abb[6] * z[26];
z[25] = -5 * abb[25] + abb[20] * (T(-11) / T(2)) + z[23] + z[25];
z[25] = abb[8] * z[25];
z[25] = z[25] + z[26];
z[18] = z[18] + z[20] + -11 * z[22];
z[18] = abb[26] * (T(7) / T(2)) + (T(1) / T(3)) * z[18];
z[18] = abb[0] * z[18];
z[22] = abb[20] * (T(1) / T(4)) + -z[20];
z[22] = abb[26] * (T(-1) / T(12)) + abb[25] * (T(1) / T(4)) + (T(1) / T(3)) * z[22];
z[22] = abb[4] * z[22];
z[18] = (T(-7) / T(4)) * z[15] + (T(1) / T(2)) * z[18] + (T(1) / T(6)) * z[19] + z[22] + (T(1) / T(3)) * z[25];
z[18] = prod_pow(m1_set::bc<T>[0], 2) * z[18];
z[19] = abb[15] * z[36];
z[6] = z[6] + z[29];
z[22] = -abb[4] * z[27];
z[25] = -abb[0] * z[37];
z[14] = z[6] + z[14] + z[22] + z[25];
z[14] = prod_pow(abb[10], 2) * z[14];
z[22] = 2 * z[29];
z[25] = abb[4] * z[28];
z[2] = -z[2] + -z[17] + z[22] + z[25];
z[2] = abb[14] * z[2];
z[1] = abb[29] + z[1] + z[2] + z[10] + z[11] + z[13] + z[14] + z[18] + z[19] + z[21] + z[24];
z[2] = abb[25] * (T(-7) / T(2)) + 4 * z[3] + z[23];
z[2] = abb[8] * z[2];
z[3] = z[4] * z[38];
z[2] = z[2] + z[3] + -z[8] + -z[12] + -4 * z[15] + z[22] + z[34];
z[2] = abb[12] * z[2];
z[3] = abb[4] * z[9];
z[3] = z[3] + -2 * z[6] + z[8] + z[39];
z[3] = abb[10] * z[3];
z[4] = -z[20] + z[33];
z[4] = abb[4] * z[4];
z[6] = -z[31] * z[38];
z[4] = z[4] + z[6] + z[7] + z[16];
z[4] = abb[11] * z[4];
z[2] = z[2] + z[3] + z[4];
z[2] = m1_set::bc<T>[0] * z[2];
z[0] = abb[17] * z[0];
z[3] = -abb[18] * z[5];
z[0] = abb[19] + z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_72_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-6.0742657438101381959682209659761817418268722392557805511274275392"),stof<T>("-11.966078532656791533961480412362773585634487366888877373170901703")}, std::complex<T>{stof<T>("12.167344128214953571732473671194683908099341997763856428278242457"),stof<T>("20.550552081361626739730676952202164030099696606619048054700665324")}, std::complex<T>{stof<T>("-0.8968035811165683255097666066265694557989011445528072852234950345"),stof<T>("-6.2201122807431781040338473758367132925688561857526177952023091884")}, std::complex<T>{stof<T>("-6.7182789718561576069294429130509825412330651296114964771935896886"),stof<T>("-2.9059201366585011814269374309669465557484230049023527858895057752")}, std::complex<T>{stof<T>("-13.436557943712315213858885826101965082466130259222992954387179377"),stof<T>("-5.81184027331700236285387486193389311149684600980470557177901155")}, std::complex<T>{stof<T>("13.708160937377541308203462224896054163304436032672379639567899641"),stof<T>("17.710505966106514491229981346643050292782488430385141262621578584")}, std::complex<T>{stof<T>("-6.0930783844048153757642527052185021662724697585080758771508149182"),stof<T>("-8.5844735487048352057691965398393904444652092397301706815297636205")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_72_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_72_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (-v[3] + v[5]) * (24 + -16 * m1_set::bc<T>[1] + 24 * v[0] + -8 * v[1] + v[3] + -4 * m1_set::bc<T>[1] * v[3] + 11 * v[5] + -4 * m1_set::bc<T>[1] * v[5] + -m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(-1) / T(2)) * (-v[3] + v[5])) / tend;


		return (abb[21] + abb[22] + -abb[26]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = prod_pow(abb[13], 2);
z[1] = -abb[13] + abb[11] * (T(3) / T(2));
z[1] = abb[11] * z[1];
z[0] = abb[15] + (T(-1) / T(2)) * z[0] + z[1];
z[1] = -abb[21] + -abb[22] + abb[26];
z[0] = z[0] * z[1];
z[2] = -abb[14] + abb[16];
z[1] = 2 * z[1];
z[1] = z[1] * z[2];
z[0] = z[0] + z[1];
return abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_72_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_72_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-17.198260816996975924904914877180478028432885152222468213688563383"),stof<T>("30.652511832428231690907754560212643085007013751625916924395421676")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dl[1], dl[5], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_2_3(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), f_2_4_re(k), f_2_6_re(k), T{0}};
abb[19] = SpDLog_f_4_72_W_16_Im(t, path, abb);
abb[29] = SpDLog_f_4_72_W_16_Re(t, path, abb);

                    
            return f_4_72_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_72_DLogXconstant_part(base_point<T>, kend);
	value += f_4_72_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_72_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_72_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_72_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_72_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_72_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_72_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
