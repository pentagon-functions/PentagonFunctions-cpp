/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_361.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_361_abbreviated (const std::array<T,56>& abb) {
T z[86];
z[0] = 3 * abb[5];
z[1] = abb[6] + abb[15];
z[2] = -z[0] + z[1];
z[3] = abb[29] * (T(1) / T(2));
z[2] = z[2] * z[3];
z[4] = abb[5] + -abb[15];
z[5] = -abb[7] + z[4];
z[6] = abb[33] * z[5];
z[7] = abb[4] * abb[30];
z[8] = abb[5] * (T(1) / T(2));
z[9] = -abb[7] + abb[15] * (T(-1) / T(2)) + z[8];
z[10] = abb[6] * (T(-3) / T(2)) + z[9];
z[10] = abb[31] * z[10];
z[11] = abb[30] * z[0];
z[12] = -abb[5] * abb[32];
z[13] = -abb[30] + abb[32];
z[14] = -abb[33] + z[13];
z[14] = abb[14] * z[14];
z[2] = z[2] + -z[6] + -z[7] + z[10] + z[11] + z[12] + z[14];
z[10] = -abb[29] + abb[31];
z[11] = (T(1) / T(2)) * z[10];
z[12] = abb[30] + z[11];
z[14] = abb[32] * (T(1) / T(2));
z[15] = z[12] + -z[14];
z[16] = abb[11] * z[15];
z[17] = abb[6] + -abb[15];
z[18] = abb[4] + abb[14] + z[17];
z[18] = -abb[11] + (T(1) / T(2)) * z[18];
z[19] = abb[28] * (T(1) / T(2));
z[20] = z[18] * z[19];
z[2] = (T(1) / T(4)) * z[2] + z[16] + z[20];
z[2] = m1_set::bc<T>[0] * z[2];
z[20] = abb[6] + abb[7];
z[21] = abb[4] * (T(1) / T(2));
z[22] = abb[14] * (T(1) / T(2));
z[23] = 3 * abb[11];
z[24] = abb[5] + (T(-3) / T(2)) * z[20] + -z[21] + -z[22] + z[23];
z[25] = abb[40] * (T(1) / T(2));
z[26] = z[24] * z[25];
z[27] = z[3] + z[14];
z[28] = -abb[30] + z[27];
z[28] = m1_set::bc<T>[0] * z[28];
z[28] = abb[39] * (T(-1) / T(2)) + z[28];
z[29] = -z[25] + z[28];
z[29] = abb[8] * z[29];
z[28] = abb[12] * z[28];
z[30] = abb[39] * (T(1) / T(4));
z[31] = abb[5] + z[20];
z[31] = z[30] * z[31];
z[32] = abb[13] * (T(1) / T(4));
z[33] = abb[29] + abb[31];
z[33] = -abb[30] + (T(1) / T(2)) * z[33];
z[33] = m1_set::bc<T>[0] * z[33];
z[33] = -abb[39] + z[33];
z[33] = z[32] * z[33];
z[34] = abb[38] * (T(1) / T(2));
z[35] = z[18] * z[34];
z[2] = z[2] + z[26] + z[28] + (T(-1) / T(2)) * z[29] + z[31] + z[33] + z[35];
z[2] = abb[48] * z[2];
z[26] = abb[4] + -abb[9];
z[31] = abb[0] * (T(1) / T(2));
z[33] = (T(1) / T(2)) * z[4] + z[26] + z[31];
z[35] = -abb[1] + abb[11];
z[36] = z[33] + -z[35];
z[36] = abb[46] * z[36];
z[37] = abb[50] * (T(1) / T(2));
z[38] = -abb[0] + z[4];
z[39] = z[37] * z[38];
z[40] = abb[42] + abb[43] + -abb[44];
z[41] = (T(1) / T(2)) * z[40];
z[42] = abb[20] * z[41];
z[43] = abb[25] * z[41];
z[44] = z[42] + z[43];
z[45] = abb[0] + z[26];
z[46] = -z[35] + z[45];
z[46] = abb[45] * z[46];
z[36] = z[36] + -z[39] + -z[44] + z[46];
z[36] = z[19] * z[36];
z[15] = abb[1] * z[15];
z[15] = z[15] + -z[16];
z[16] = abb[9] * abb[30];
z[16] = -z[7] + z[16];
z[46] = abb[0] * abb[33];
z[47] = abb[0] * z[10];
z[48] = z[16] + -z[46] + z[47];
z[48] = -z[15] + (T(1) / T(2)) * z[48];
z[48] = abb[45] * z[48];
z[49] = abb[0] + z[4];
z[50] = abb[33] * (T(1) / T(2));
z[49] = z[49] * z[50];
z[51] = -abb[6] + z[4];
z[51] = -z[10] * z[51];
z[52] = z[10] * z[31];
z[49] = z[49] + (T(1) / T(4)) * z[51] + -z[52];
z[53] = -z[16] + z[49];
z[15] = -z[15] + (T(-1) / T(2)) * z[53];
z[15] = abb[46] * z[15];
z[54] = abb[23] * z[40];
z[27] = -z[27] * z[54];
z[55] = abb[32] * z[40];
z[56] = abb[33] * z[40];
z[57] = z[55] + -z[56];
z[58] = abb[30] * z[40];
z[59] = z[57] + -z[58];
z[60] = abb[20] * (T(1) / T(2));
z[61] = -z[59] * z[60];
z[62] = abb[23] * z[58];
z[27] = z[27] + z[61] + z[62];
z[51] = (T(1) / T(2)) * z[51];
z[38] = abb[33] * z[38];
z[38] = z[38] + z[47] + z[51];
z[61] = abb[50] * (T(1) / T(4));
z[63] = z[38] * z[61];
z[64] = abb[31] * z[40];
z[65] = abb[29] * z[40];
z[66] = z[64] + -z[65];
z[67] = (T(1) / T(2)) * z[66];
z[68] = -z[56] + z[67];
z[69] = abb[25] * (T(1) / T(4));
z[70] = -z[68] * z[69];
z[15] = z[15] + (T(1) / T(2)) * z[27] + z[36] + z[48] + z[63] + z[70];
z[15] = m1_set::bc<T>[0] * z[15];
z[27] = m1_set::bc<T>[0] * z[53];
z[36] = abb[28] * m1_set::bc<T>[0];
z[48] = z[33] * z[36];
z[53] = abb[4] + abb[7];
z[63] = abb[6] + -abb[14] + z[53];
z[70] = abb[40] * z[63];
z[27] = z[27] + -z[48] + z[70];
z[17] = abb[4] + (T(1) / T(2)) * z[17] + z[31];
z[48] = abb[2] + -abb[14] + z[17];
z[70] = z[34] * z[48];
z[71] = abb[41] * (T(1) / T(8));
z[72] = abb[16] + abb[19];
z[73] = abb[17] + z[72];
z[74] = z[71] * z[73];
z[27] = (T(1) / T(2)) * z[27] + -z[28] + z[29] + -z[70] + z[74];
z[28] = abb[47] + abb[49];
z[27] = -z[27] * z[28];
z[29] = -abb[5] + (T(1) / T(2)) * z[1] + z[31];
z[29] = abb[50] * z[29];
z[17] = -abb[11] + z[17];
z[17] = abb[46] * z[17];
z[70] = abb[5] + -abb[6];
z[74] = abb[0] + abb[4] + -abb[11] + -z[70];
z[74] = abb[45] * z[74];
z[75] = abb[45] + abb[50];
z[76] = abb[2] * z[75];
z[17] = z[17] + z[29] + -z[44] + z[74] + z[76];
z[29] = z[17] * z[34];
z[34] = z[64] + z[65];
z[34] = (T(1) / T(2)) * z[34] + z[59];
z[34] = m1_set::bc<T>[0] * z[34];
z[44] = abb[39] * z[40];
z[34] = z[34] + -z[44];
z[44] = abb[22] + abb[24];
z[44] = (T(1) / T(4)) * z[44];
z[34] = -z[34] * z[44];
z[59] = abb[20] * z[40];
z[59] = z[54] + z[59];
z[70] = z[53] + -z[70];
z[23] = abb[1] + -z[23] + z[70];
z[74] = abb[45] + abb[46];
z[23] = z[23] * z[74];
z[23] = -z[23] + (T(1) / T(2)) * z[59];
z[25] = z[23] * z[25];
z[59] = abb[19] * abb[31];
z[76] = abb[17] * z[10];
z[77] = abb[19] * abb[29];
z[59] = z[59] + -z[76] + -z[77];
z[11] = abb[16] * z[11];
z[11] = z[11] + (T(1) / T(2)) * z[59];
z[59] = abb[17] + -abb[19];
z[76] = -abb[33] * z[59];
z[76] = -z[11] + z[76];
z[76] = m1_set::bc<T>[0] * z[76];
z[78] = z[36] * z[59];
z[79] = -abb[38] * z[72];
z[76] = z[76] + z[78] + z[79];
z[1] = abb[5] + z[1];
z[1] = -abb[27] + (T(1) / T(4)) * z[1];
z[78] = -abb[41] * z[1];
z[76] = (T(1) / T(2)) * z[76] + z[78];
z[78] = abb[51] * (T(1) / T(2));
z[76] = z[76] * z[78];
z[30] = z[30] * z[54];
z[79] = -abb[33] + z[10];
z[80] = -m1_set::bc<T>[0] * z[79];
z[36] = -z[36] + z[80];
z[36] = -abb[38] + (T(1) / T(2)) * z[36];
z[36] = abb[10] * z[36] * z[75];
z[80] = z[58] + z[67];
z[81] = -z[55] + z[80];
z[81] = m1_set::bc<T>[0] * z[81];
z[82] = abb[40] * z[40];
z[81] = z[81] + z[82];
z[82] = abb[21] * (T(1) / T(4));
z[81] = z[81] * z[82];
z[83] = -abb[46] + abb[50];
z[83] = z[73] * z[83];
z[84] = abb[26] * z[40];
z[83] = z[83] + -z[84];
z[84] = -abb[18] + -z[73];
z[84] = abb[48] * z[84];
z[84] = z[83] + z[84];
z[71] = z[71] * z[84];
z[2] = z[2] + z[15] + z[25] + z[27] + z[29] + z[30] + z[34] + z[36] + z[71] + z[76] + z[81];
z[2] = (T(1) / T(4)) * z[2];
z[0] = z[0] + -z[20];
z[0] = abb[30] * z[0];
z[15] = abb[5] * abb[29];
z[25] = -abb[31] * z[20];
z[0] = (T(1) / T(2)) * z[0] + -z[15] + z[25];
z[0] = abb[30] * z[0];
z[25] = abb[37] + abb[53];
z[25] = -z[20] * z[25];
z[27] = abb[36] + abb[37];
z[29] = prod_pow(abb[30], 2);
z[29] = z[27] + (T(1) / T(2)) * z[29];
z[30] = abb[4] * z[29];
z[34] = abb[6] * (T(1) / T(2));
z[9] = -z[9] + -z[34];
z[36] = abb[29] * abb[31];
z[9] = z[9] * z[36];
z[71] = abb[37] + -abb[53];
z[71] = abb[5] * z[71];
z[76] = abb[6] + z[4];
z[81] = -abb[3] + abb[7] + -z[76];
z[81] = abb[35] * z[81];
z[84] = -abb[3] + abb[5];
z[85] = -abb[7] + z[84];
z[85] = abb[36] * z[85];
z[0] = z[0] + z[9] + z[25] + -z[30] + z[71] + z[81] + z[85];
z[8] = -abb[3] + abb[7] * (T(-1) / T(2)) + z[8] + -z[21] + -z[34];
z[8] = abb[33] * z[8];
z[9] = z[53] + -z[84];
z[9] = abb[32] * z[9];
z[5] = abb[29] * z[5];
z[21] = abb[3] + abb[6];
z[21] = abb[31] * z[21];
z[5] = z[5] + z[8] + z[9] + z[21];
z[5] = z[5] * z[50];
z[8] = -abb[3] + abb[6];
z[8] = abb[31] * z[8];
z[7] = z[7] + z[8] + z[15];
z[8] = abb[32] * (T(1) / T(4));
z[9] = -z[8] * z[70];
z[15] = -abb[5] + (T(1) / T(2)) * z[20];
z[15] = abb[30] * z[15];
z[7] = (T(1) / T(2)) * z[7] + z[9] + z[15];
z[7] = abb[32] * z[7];
z[9] = -abb[29] + abb[30];
z[9] = -z[9] * z[13];
z[15] = prod_pow(m1_set::bc<T>[0], 2);
z[9] = -abb[53] + z[9] + (T(-5) / T(6)) * z[15];
z[21] = abb[12] * z[9];
z[25] = abb[30] + z[10];
z[25] = z[13] * z[25];
z[53] = -abb[32] + -z[79];
z[53] = abb[33] * z[53];
z[25] = abb[36] + z[25] + z[53];
z[22] = z[22] * z[25];
z[0] = (T(-1) / T(2)) * z[0] + -z[5] + -z[7] + z[21] + -z[22];
z[5] = -abb[32] * z[70];
z[7] = abb[30] * z[20];
z[20] = abb[30] + z[79];
z[20] = abb[14] * z[20];
z[22] = abb[13] * z[12];
z[5] = z[5] + -z[6] + z[7] + z[20] + -z[22] + -z[51];
z[6] = abb[11] * z[13];
z[7] = abb[4] + abb[13];
z[20] = -abb[7] + -abb[15] + z[7];
z[20] = abb[28] * z[20];
z[5] = (T(1) / T(2)) * z[5] + z[6] + (T(1) / T(4)) * z[20];
z[5] = z[5] * z[19];
z[20] = abb[54] * (T(1) / T(2));
z[22] = -z[20] * z[24];
z[24] = (T(1) / T(3)) * z[15];
z[25] = abb[31] * z[3];
z[51] = abb[30] * abb[31];
z[51] = abb[53] + z[24] + -z[25] + z[51];
z[32] = z[32] * z[51];
z[9] = -abb[54] + z[9];
z[9] = abb[8] * z[9];
z[7] = -abb[5] + -abb[14] + z[7];
z[7] = abb[34] * z[7];
z[7] = z[7] + z[9];
z[8] = z[8] + -z[12];
z[8] = abb[32] * z[8];
z[12] = abb[30] * (T(3) / T(2)) + z[10];
z[12] = abb[30] * z[12];
z[12] = abb[37] + z[12];
z[8] = z[8] + (T(1) / T(2)) * z[12];
z[12] = abb[11] * z[8];
z[51] = (T(1) / T(12)) * z[15];
z[53] = -abb[11] + abb[15] + abb[5] * (T(-7) / T(2)) + abb[6] * (T(-5) / T(2)) + abb[3] * (T(1) / T(2));
z[53] = z[51] * z[53];
z[70] = abb[52] * (T(1) / T(2));
z[18] = -z[18] * z[70];
z[71] = abb[55] * z[73];
z[73] = abb[55] * (T(1) / T(8));
z[81] = abb[18] * z[73];
z[0] = (T(-1) / T(2)) * z[0] + z[5] + (T(1) / T(4)) * z[7] + z[12] + z[18] + z[22] + z[32] + z[53] + (T(1) / T(8)) * z[71] + z[81];
z[0] = abb[48] * z[0];
z[5] = -z[31] + z[34];
z[7] = z[5] + z[26];
z[7] = z[7] * z[50];
z[18] = abb[6] * abb[31];
z[22] = abb[29] * z[4];
z[18] = z[18] + z[22];
z[22] = abb[32] * z[26];
z[7] = z[7] + (T(-1) / T(2)) * z[18] + -z[22] + z[52];
z[7] = abb[33] * z[7];
z[32] = z[14] * z[26];
z[16] = z[16] + z[32];
z[16] = abb[32] * z[16];
z[29] = -abb[9] * z[29];
z[16] = z[16] + z[29] + z[30];
z[29] = z[36] * z[76];
z[30] = abb[35] * z[76];
z[7] = z[7] + z[16] + (T(1) / T(4)) * z[29] + (T(1) / T(2)) * z[30];
z[29] = z[22] + z[49];
z[32] = z[19] * z[33];
z[32] = -z[29] + z[32];
z[32] = abb[28] * z[32];
z[31] = z[4] + z[31] + z[34];
z[34] = (T(1) / T(6)) * z[15];
z[36] = z[31] * z[34];
z[48] = abb[52] * z[48];
z[49] = abb[34] * z[26];
z[52] = abb[54] * z[63];
z[9] = -z[7] + z[9] + -z[21] + z[32] + -z[36] + -z[48] + z[49] + z[52] + (T(1) / T(4)) * z[71];
z[21] = (T(1) / T(2)) * z[28];
z[9] = z[9] * z[21];
z[21] = z[40] * z[50];
z[21] = z[21] + -z[55] + z[65];
z[21] = abb[33] * z[21];
z[28] = z[57] + z[67];
z[28] = abb[28] * z[28];
z[27] = -abb[34] + -abb[35] + z[27];
z[27] = z[27] * z[40];
z[32] = (T(1) / T(2)) * z[58] + -z[65];
z[32] = abb[30] * z[32];
z[14] = z[14] * z[40];
z[14] = z[14] + -z[65];
z[14] = abb[32] * z[14];
z[15] = z[15] * z[41];
z[3] = z[3] * z[64];
z[36] = abb[53] * z[40];
z[14] = -z[3] + z[14] + z[15] + z[21] + z[27] + z[28] + -z[32] + z[36];
z[14] = -z[14] * z[44];
z[21] = abb[1] * z[13];
z[6] = -z[6] + z[21];
z[21] = -z[6] + -z[29];
z[21] = abb[46] * z[21];
z[27] = abb[46] * z[33];
z[28] = abb[45] * z[45];
z[27] = z[27] + z[28] + -z[39] + -z[43];
z[27] = z[19] * z[27];
z[22] = -z[22] + z[47];
z[6] = -z[6] + z[22] + -z[46];
z[6] = abb[45] * z[6];
z[28] = z[37] * z[38];
z[29] = z[58] + z[66];
z[32] = -z[29] + z[56];
z[32] = z[32] * z[60];
z[33] = abb[25] * z[68];
z[6] = z[6] + z[21] + z[27] + z[28] + z[32] + (T(-1) / T(2)) * z[33];
z[6] = z[6] * z[19];
z[21] = z[19] * z[40];
z[21] = z[21] + -z[80];
z[21] = abb[28] * z[21];
z[27] = abb[34] + -abb[54];
z[27] = z[27] * z[40];
z[28] = abb[30] * z[29];
z[32] = -z[58] + z[65];
z[32] = abb[32] * z[32];
z[15] = -z[3] + -z[15] + z[21] + z[27] + z[28] + z[32];
z[15] = z[15] * z[82];
z[8] = abb[1] * z[8];
z[8] = z[8] + -z[12];
z[7] = (T(-1) / T(2)) * z[7] + -z[8];
z[7] = abb[46] * z[7];
z[12] = -z[31] + -z[35];
z[12] = abb[46] * z[12];
z[4] = z[4] + z[5];
z[4] = abb[50] * z[4];
z[5] = -abb[0] + -z[35];
z[5] = abb[45] * z[5];
z[21] = abb[25] * z[40];
z[4] = z[4] + z[5] + z[12] + z[21] + (T(-5) / T(2)) * z[54];
z[4] = z[4] * z[51];
z[5] = -z[17] * z[70];
z[12] = abb[35] + z[25];
z[17] = -z[12] * z[59];
z[21] = abb[17] * abb[29];
z[21] = z[21] + -z[77];
z[21] = abb[33] * z[21];
z[27] = abb[31] + -z[50];
z[27] = abb[33] * z[27];
z[12] = -z[12] + z[27];
z[12] = abb[16] * z[12];
z[27] = abb[16] * (T(-1) / T(2)) + -z[59];
z[24] = z[24] * z[27];
z[12] = z[12] + z[17] + z[21] + z[24];
z[17] = -abb[33] + z[19];
z[17] = z[17] * z[59];
z[11] = -z[11] + z[17];
z[11] = z[11] * z[19];
z[17] = z[70] * z[72];
z[1] = abb[55] * z[1];
z[1] = z[1] + z[11] + (T(1) / T(2)) * z[12] + z[17];
z[1] = z[1] * z[78];
z[11] = abb[0] + -z[26];
z[11] = z[11] * z[50];
z[11] = z[11] + -z[22];
z[11] = abb[33] * z[11];
z[11] = z[11] + -z[16];
z[8] = -z[8] + (T(1) / T(2)) * z[11];
z[8] = abb[45] * z[8];
z[11] = z[57] + z[66];
z[11] = abb[33] * z[11];
z[12] = -z[13] * z[29];
z[16] = -abb[36] * z[40];
z[11] = z[11] + z[12] + z[16];
z[11] = abb[20] * z[11];
z[12] = abb[23] * z[65];
z[12] = z[12] + -z[62];
z[12] = z[12] * z[13];
z[13] = -abb[23] * z[36];
z[11] = z[11] + z[12] + z[13];
z[12] = abb[35] * z[40];
z[13] = -abb[33] * z[65];
z[3] = z[3] + z[12] + z[13];
z[3] = z[3] * z[69];
z[12] = -z[20] * z[23];
z[13] = abb[0] + abb[6];
z[13] = z[13] * z[50];
z[13] = z[13] + -z[18] + -z[47];
z[13] = abb[33] * z[13];
z[16] = z[25] * z[76];
z[13] = z[13] + z[16] + z[30];
z[13] = z[13] * z[61];
z[10] = -z[10] + z[50];
z[10] = -abb[33] * z[10];
z[16] = -z[19] + -z[79];
z[16] = abb[28] * z[16];
z[10] = z[10] + z[16] + z[34];
z[10] = abb[52] + (T(1) / T(2)) * z[10];
z[10] = abb[10] * z[10] * z[75];
z[16] = -z[73] * z[83];
z[17] = z[26] * z[74];
z[17] = z[17] + z[42];
z[17] = abb[34] * z[17];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + (T(1) / T(4)) * z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + (T(1) / T(2)) * z[17];
z[0] = (T(1) / T(4)) * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_361_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("-0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("-0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.11516678706594108742361726332622699188162080055784267447687125469"),stof<T>("-0.15967331047572889567111450251944426399616135919349254833595335085")}, std::complex<T>{stof<T>("0.64327845013205730075312761736834506896170356336707235997977068155"),stof<T>("-0.23397317051375081348420301808564723648502047838645737013773050632")}, std::complex<T>{stof<T>("0.48177557361562590026845974547593072779242268052883464356757916658"),stof<T>("-0.25240696326664240727669601760048436671535535645260066644085229889")}, std::complex<T>{stof<T>("1.22696565504315095292748854430439156434744611660086565024376050754"),stof<T>("-0.4213706799633750719200672353528652184799295133154047429026928427")}, std::complex<T>{stof<T>("0.48177557361562590026845974547593072779242268052883464356757916658"),stof<T>("-0.25240696326664240727669601760048436671535535645260066644085229889")}, std::complex<T>{stof<T>("-0.52811166306611621332951035404211807708008276280922968550289942685"),stof<T>("0.07429986003802191781308851556620297248885911919296482180177715548")}, std::complex<T>{stof<T>("-0.45125754221970090106940426838234235012675726337916039721744055787"),stof<T>("0.22622761458343275164959743267577683199320261638088151564227982501")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_361_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_361_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.24220292015549555436953264399266282736242802068542167463767936419"),stof<T>("-0.39973085924531559803430107158245660085084504353464389218255370179")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k)};

                    
            return f_4_361_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_361_DLogXconstant_part(base_point<T>, kend);
	value += f_4_361_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_361_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_361_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_361_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_361_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_361_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_361_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
