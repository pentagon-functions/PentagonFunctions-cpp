/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_121.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_121_abbreviated (const std::array<T,53>& abb) {
T z[61];
z[0] = abb[45] + abb[47];
z[1] = abb[48] + z[0];
z[2] = abb[15] * z[1];
z[3] = abb[19] * abb[49];
z[2] = z[2] + -z[3];
z[3] = -abb[42] + abb[43];
z[4] = abb[20] * z[3];
z[5] = z[2] + -z[4];
z[6] = abb[20] * abb[41];
z[7] = z[5] + z[6];
z[3] = -abb[41] + z[3];
z[8] = abb[23] * z[3];
z[9] = abb[5] * z[1];
z[10] = abb[17] * abb[49];
z[11] = -z[7] + z[8] + z[9] + z[10];
z[12] = abb[16] * abb[49];
z[13] = abb[21] * z[3];
z[14] = z[12] + z[13];
z[15] = abb[6] * z[1];
z[16] = z[14] + z[15];
z[17] = z[11] + z[16];
z[18] = abb[8] * abb[44];
z[19] = abb[22] * z[3];
z[18] = z[18] + z[19];
z[20] = (T(1) / T(2)) * z[3];
z[21] = abb[24] * z[20];
z[22] = abb[44] * (T(1) / T(2));
z[23] = abb[13] * z[22];
z[24] = (T(1) / T(2)) * z[17] + z[18] + -z[21] + -z[23];
z[25] = abb[31] * (T(1) / T(2));
z[26] = abb[28] * (T(1) / T(2));
z[27] = z[25] + z[26];
z[27] = z[24] * z[27];
z[28] = abb[4] * abb[44];
z[29] = abb[24] * z[3];
z[17] = z[17] + z[18] + -z[28] + -z[29];
z[30] = abb[32] * (T(1) / T(2));
z[31] = -z[17] * z[30];
z[32] = abb[46] + z[0];
z[33] = z[22] + z[32];
z[34] = abb[7] * z[33];
z[35] = abb[23] * (T(1) / T(2));
z[35] = z[3] * z[35];
z[36] = z[34] + -z[35];
z[37] = abb[5] * z[22];
z[38] = z[36] + -z[37];
z[39] = abb[6] * z[22];
z[40] = abb[21] * z[20];
z[39] = z[38] + -z[39] + -z[40];
z[41] = abb[4] * z[22];
z[42] = -z[39] + -z[41];
z[42] = abb[30] * z[42];
z[43] = abb[8] * z[22];
z[20] = abb[22] * z[20];
z[43] = z[20] + z[43];
z[44] = -z[23] + z[43];
z[39] = z[39] + -z[44];
z[45] = abb[29] * z[39];
z[46] = abb[29] + -abb[30];
z[47] = -abb[12] * z[32] * z[46];
z[27] = z[27] + z[31] + z[42] + z[45] + z[47];
z[27] = m1_set::bc<T>[0] * z[27];
z[31] = -abb[16] + -abb[17];
z[31] = z[1] * z[31];
z[42] = abb[5] + abb[6] + abb[15];
z[42] = abb[49] * z[42];
z[45] = abb[45] + abb[48];
z[48] = abb[47] + z[45];
z[48] = abb[19] * z[48];
z[49] = abb[18] * abb[44];
z[3] = abb[25] * z[3];
z[3] = z[3] + z[31] + -z[42] + z[48] + z[49];
z[31] = abb[26] * abb[49];
z[3] = (T(1) / T(4)) * z[3] + z[31];
z[31] = abb[40] * z[3];
z[42] = -abb[39] * z[17];
z[48] = abb[12] * z[32];
z[39] = z[39] + -z[48];
z[49] = abb[38] * z[39];
z[27] = z[27] + z[31] + (T(1) / T(2)) * z[42] + z[49];
z[27] = (T(1) / T(8)) * z[27];
z[12] = (T(1) / T(2)) * z[12] + z[21] + z[40];
z[31] = abb[44] + abb[47];
z[42] = z[31] + -z[45];
z[42] = abb[0] * z[42];
z[2] = -z[2] + z[42];
z[49] = abb[47] + -z[45];
z[49] = abb[6] * z[49];
z[49] = z[2] + z[49];
z[50] = abb[44] * (T(3) / T(2)) + z[32];
z[50] = abb[14] * z[50];
z[51] = abb[47] + z[22];
z[51] = abb[3] * z[51];
z[52] = abb[45] + abb[46];
z[53] = abb[44] + z[52];
z[54] = abb[11] * z[53];
z[55] = abb[5] * z[45];
z[49] = -z[12] + (T(1) / T(2)) * z[49] + -z[50] + z[51] + z[54] + z[55];
z[49] = abb[33] * z[49];
z[55] = abb[6] * abb[47];
z[56] = abb[9] * z[31];
z[57] = z[55] + z[56];
z[32] = abb[44] + z[32];
z[32] = abb[14] * z[32];
z[58] = abb[8] * abb[47];
z[59] = abb[1] * z[52];
z[60] = -abb[5] * z[52];
z[60] = z[32] + -z[54] + -z[57] + -z[58] + -z[59] + z[60];
z[60] = abb[30] * z[60];
z[52] = -z[22] + z[52];
z[52] = abb[5] * z[52];
z[33] = -abb[14] * z[33];
z[33] = z[33] + z[36] + z[52] + z[57] + z[58];
z[33] = (T(1) / T(2)) * z[33] + z[59];
z[33] = abb[29] * z[33];
z[33] = z[33] + z[60];
z[33] = abb[29] * z[33];
z[6] = (T(1) / T(2)) * z[6];
z[36] = z[6] + z[51];
z[52] = abb[5] * z[53];
z[53] = (T(1) / T(2)) * z[4];
z[20] = -z[20] + -z[34] + z[36] + z[52] + -z[53];
z[34] = -abb[6] + -abb[8];
z[52] = -abb[47] + z[22];
z[34] = z[34] * z[52];
z[34] = z[20] + z[34] + -z[40] + -z[50] + 3 * z[54] + -z[59];
z[34] = abb[35] * z[34];
z[7] = -z[7] + z[8] + z[14] + z[15] + (T(1) / T(2)) * z[42] + z[59];
z[8] = abb[44] + abb[48];
z[0] = z[0] + (T(1) / T(3)) * z[8];
z[0] = abb[46] * (T(1) / T(3)) + (T(1) / T(2)) * z[0];
z[0] = abb[5] * z[0];
z[8] = (T(1) / T(2)) * z[10];
z[0] = z[0] + (T(1) / T(3)) * z[7] + z[8];
z[7] = abb[46] + -abb[48] + z[31];
z[7] = abb[2] * z[7];
z[10] = abb[13] * abb[44];
z[10] = z[7] + z[10] + z[29] + z[48];
z[14] = abb[44] + abb[47] * (T(1) / T(2));
z[14] = abb[8] * z[14];
z[14] = z[14] + z[19];
z[0] = (T(1) / T(2)) * z[0] + (T(-1) / T(6)) * z[10] + (T(1) / T(3)) * z[14];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[10] = -abb[8] * z[52];
z[10] = z[10] + z[20] + -z[32] + -z[41] + z[55];
z[10] = (T(1) / T(2)) * z[10] + z[54];
z[10] = prod_pow(abb[30], 2) * z[10];
z[14] = -z[35] + z[36] + z[37] + -z[56];
z[19] = abb[6] * abb[44];
z[4] = z[4] + z[19];
z[20] = -z[54] + z[59];
z[4] = (T(-1) / T(2)) * z[4] + z[14] + -z[20] + -z[40] + -z[43];
z[31] = -abb[37] * z[4];
z[3] = -abb[52] * z[3];
z[22] = abb[14] * z[22];
z[14] = z[14] + z[22] + -z[53];
z[32] = z[14] + -z[41] + -z[43];
z[32] = abb[36] * z[32];
z[14] = z[14] + -z[23];
z[14] = abb[34] * z[14];
z[22] = z[20] + z[22] + z[38];
z[22] = -abb[28] * z[22] * z[46];
z[36] = abb[50] * z[39];
z[37] = -abb[28] + abb[29];
z[37] = z[37] * z[47];
z[0] = z[0] + z[3] + z[10] + z[14] + z[22] + z[31] + z[32] + z[33] + z[34] + -z[36] + z[37] + z[49];
z[3] = z[5] + -z[9] + z[15];
z[5] = abb[14] * abb[44];
z[9] = -z[5] + z[42];
z[3] = (T(1) / T(2)) * z[3] + z[6] + -z[8] + -z[9] + z[12] + -z[23] + -z[35];
z[6] = z[3] * z[26];
z[10] = abb[10] * z[45];
z[3] = (T(-1) / T(2)) * z[3] + z[10];
z[3] = abb[31] * z[3];
z[1] = abb[44] + z[1];
z[1] = abb[5] * z[1];
z[1] = z[1] + z[2];
z[1] = (T(1) / T(2)) * z[1] + z[8] + z[10] + -z[21] + z[44] + z[51] + -z[56];
z[2] = abb[27] * (T(1) / T(2));
z[1] = z[1] * z[2];
z[4] = -abb[30] * z[4];
z[5] = z[5] + z[13] + z[19];
z[5] = (T(1) / T(2)) * z[5] + z[20];
z[8] = z[5] + z[44];
z[8] = abb[29] * z[8];
z[12] = abb[28] * z[10];
z[1] = z[1] + z[3] + z[4] + z[6] + -z[8] + -z[12];
z[1] = z[1] * z[2];
z[2] = -z[24] * z[26];
z[3] = -z[5] + z[41];
z[3] = abb[30] * z[3];
z[2] = z[2] + z[3] + z[8];
z[2] = z[2] * z[25];
z[3] = z[9] + z[11] + z[18] + -z[29];
z[4] = z[3] * z[26];
z[3] = (T(-1) / T(2)) * z[3] + -z[10];
z[3] = abb[27] * z[3];
z[5] = z[9] + -z[16] + z[28];
z[5] = (T(1) / T(2)) * z[5] + z[10];
z[6] = -abb[31] + z[30];
z[5] = z[5] * z[6];
z[3] = z[3] + z[4] + z[5] + z[12];
z[3] = z[3] * z[30];
z[4] = -abb[51] * z[17];
z[5] = (T(1) / T(2)) * z[7] + z[10];
z[5] = abb[33] * z[5];
z[0] = (T(1) / T(2)) * z[0] + z[1] + z[2] + z[3] + (T(-1) / T(4)) * z[4] + z[5];
z[0] = (T(1) / T(4)) * z[0];
return {z[27], z[0]};
}


template <typename T> std::complex<T> f_4_121_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.25181468721714400349575805464931716529289181369576215045419913977"),stof<T>("0.65823193110701463786320814541213321183725638227755712994038203931")}, std::complex<T>{stof<T>("0.11760863289754866385998927069140140662759159776895632633035234723"),stof<T>("0.46664416599773942032219974408597736484925567298577620731988081447")}, std::complex<T>{stof<T>("0.15857841537533631273724318157876290766383516510697648013934649957"),stof<T>("0.78962621475086254715007802603495023153910242888041274580055308638")}, std::complex<T>{stof<T>("0.09977862973960918087541117413659542970091102574537469680351348968"),stof<T>("0.46664416599773942032219974408597736484925567298577620731988081447")}, std::complex<T>{stof<T>("-0.04096978247778764887725391088736150103624356733802015380899415234"),stof<T>("-0.32298204875312312682787828194897286668984675589463653848067227191")}, std::complex<T>{stof<T>("0.10616851963873196397097676345843907373564737355384419620189994362"),stof<T>("-0.15389981359476749750044889295661157415145599188157372634275060798")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_121_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_121_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.13305680338994742963069498214053208883208019363135258179383488028"),stof<T>("0.19866858098008036264840377005922946980291070087594261951747322104")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,53> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_121_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_121_DLogXconstant_part(base_point<T>, kend);
	value += f_4_121_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_121_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_121_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_121_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_121_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_121_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_121_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
