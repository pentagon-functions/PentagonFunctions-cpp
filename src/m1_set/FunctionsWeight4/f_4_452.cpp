/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_452.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_452_abbreviated (const std::array<T,58>& abb) {
T z[69];
z[0] = -abb[26] + abb[31];
z[1] = abb[27] * (T(1) / T(2));
z[2] = abb[29] * (T(1) / T(2));
z[3] = z[0] + z[1] + -z[2];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = -abb[36] + z[3];
z[5] = abb[45] + abb[46];
z[6] = abb[24] * z[5];
z[7] = -z[4] * z[6];
z[8] = -abb[30] + z[2];
z[9] = -abb[28] + z[1];
z[10] = z[8] + -z[9];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = abb[38] + z[10];
z[11] = abb[20] * z[10];
z[12] = abb[25] * (T(1) / T(2));
z[13] = abb[39] * z[12];
z[11] = z[11] + -z[13];
z[13] = -z[5] * z[11];
z[14] = abb[36] + -abb[38];
z[15] = -abb[26] + abb[28];
z[16] = abb[31] + z[15];
z[17] = -abb[30] + z[16];
z[17] = m1_set::bc<T>[0] * z[17];
z[18] = z[14] + -z[17];
z[19] = abb[19] * z[5] * z[18];
z[20] = z[1] + z[2];
z[21] = abb[28] + abb[31];
z[22] = abb[30] + z[20] + -z[21];
z[22] = m1_set::bc<T>[0] * z[22];
z[22] = -abb[37] + z[22];
z[23] = z[5] * z[22];
z[24] = abb[21] * z[23];
z[23] = abb[23] * z[23];
z[7] = z[7] + z[13] + z[19] + z[23] + z[24];
z[13] = abb[31] * (T(1) / T(2));
z[8] = z[8] + z[13] + z[15];
z[8] = m1_set::bc<T>[0] * z[8];
z[19] = abb[37] + -abb[38];
z[8] = z[8] + (T(-1) / T(2)) * z[19];
z[8] = abb[6] * z[8];
z[23] = m1_set::bc<T>[0] * z[15];
z[24] = -z[14] + z[23];
z[25] = abb[3] * (T(1) / T(2));
z[24] = z[24] * z[25];
z[26] = z[2] + z[9];
z[26] = m1_set::bc<T>[0] * z[26];
z[26] = -abb[37] + z[26];
z[27] = abb[12] * (T(1) / T(2));
z[28] = z[26] * z[27];
z[29] = abb[30] * (T(1) / T(2));
z[9] = z[9] + z[29];
z[9] = m1_set::bc<T>[0] * z[9];
z[30] = abb[37] + abb[38];
z[9] = z[9] + (T(-1) / T(2)) * z[30];
z[31] = abb[7] * z[9];
z[32] = abb[5] + -abb[14];
z[33] = abb[36] * z[32];
z[8] = z[8] + -z[24] + z[28] + -z[31] + (T(1) / T(2)) * z[33];
z[24] = abb[27] + abb[29];
z[28] = abb[28] * (T(1) / T(2));
z[31] = -abb[26] + z[28];
z[24] = z[13] + (T(1) / T(4)) * z[24] + -z[29] + z[31];
z[24] = m1_set::bc<T>[0] * z[24];
z[24] = abb[37] * (T(-1) / T(2)) + z[24];
z[24] = abb[4] * z[24];
z[33] = abb[15] + abb[17];
z[34] = -abb[16] + abb[18] + z[33];
z[35] = abb[39] * (T(1) / T(4));
z[34] = z[34] * z[35];
z[36] = abb[10] * z[14];
z[23] = abb[8] * z[23];
z[24] = -z[23] + z[24] + z[34] + z[36];
z[3] = abb[14] * z[3];
z[20] = -abb[26] + z[20];
z[20] = m1_set::bc<T>[0] * z[20];
z[30] = z[20] + -z[30];
z[30] = abb[5] * z[30];
z[3] = z[3] + z[30];
z[30] = -abb[41] + z[3];
z[30] = z[8] + -z[24] + (T(1) / T(2)) * z[30];
z[30] = abb[47] * z[30];
z[3] = (T(1) / T(2)) * z[3] + z[8];
z[8] = z[3] + -z[24];
z[8] = abb[48] * z[8];
z[7] = (T(1) / T(2)) * z[7] + z[8] + z[30];
z[4] = abb[24] * z[4];
z[8] = abb[21] + abb[23];
z[24] = z[8] * z[22];
z[30] = -abb[19] * z[18];
z[4] = -z[4] + -z[11] + z[24] + -z[30];
z[11] = abb[44] * (T(1) / T(4));
z[24] = abb[42] * (T(-1) / T(2)) + -z[11];
z[24] = z[4] * z[24];
z[30] = abb[16] + z[33];
z[30] = z[30] * z[35];
z[22] = (T(1) / T(2)) * z[22];
z[34] = abb[4] * z[22];
z[3] = -z[3] + z[30] + -z[34];
z[30] = abb[26] + -abb[27];
z[34] = abb[28] + z[30];
z[35] = -abb[29] + z[34];
z[36] = m1_set::bc<T>[0] * (T(1) / T(2));
z[35] = z[35] * z[36];
z[35] = abb[37] + z[35];
z[35] = abb[11] * z[35];
z[36] = abb[18] * (T(1) / T(8));
z[37] = abb[39] * z[36];
z[37] = -z[35] + z[37];
z[38] = abb[10] * (T(1) / T(2));
z[39] = -z[14] * z[38];
z[39] = (T(-1) / T(2)) * z[3] + -z[37] + z[39];
z[39] = abb[50] * z[39];
z[3] = -z[3] + z[23];
z[3] = (T(1) / T(2)) * z[3] + -z[37];
z[3] = abb[49] * z[3];
z[37] = abb[47] + abb[48];
z[10] = z[10] * z[37];
z[34] = -abb[30] + z[34];
z[34] = m1_set::bc<T>[0] * z[34];
z[34] = abb[36] + abb[37] + z[34];
z[34] = abb[49] * z[34];
z[10] = z[10] + z[34];
z[34] = -abb[50] * z[9];
z[10] = (T(1) / T(2)) * z[10] + z[34];
z[10] = abb[0] * z[10];
z[34] = abb[49] + abb[50];
z[40] = z[9] * z[34];
z[41] = abb[29] + z[15];
z[42] = -abb[30] + z[41];
z[42] = m1_set::bc<T>[0] * z[42];
z[19] = -z[19] + z[42];
z[19] = -z[19] * z[37];
z[19] = (T(1) / T(2)) * z[19] + z[40];
z[19] = abb[1] * z[19];
z[40] = -abb[44] + z[5];
z[40] = -abb[42] + (T(1) / T(2)) * z[40];
z[42] = abb[22] * z[9] * z[40];
z[3] = z[3] + (T(1) / T(2)) * z[7] + z[10] + z[19] + z[24] + z[39] + z[42];
z[7] = -abb[37] + z[20];
z[7] = abb[5] * z[7];
z[10] = abb[12] * z[26];
z[19] = abb[39] * z[33];
z[19] = -abb[41] + z[19];
z[20] = abb[6] * z[17];
z[7] = z[7] + z[10] + (T(-1) / T(2)) * z[19] + z[20] + z[23];
z[10] = -abb[0] * z[22];
z[7] = (T(1) / T(2)) * z[7] + z[10] + z[35];
z[10] = abb[51] * (T(1) / T(8));
z[7] = z[7] * z[10];
z[9] = -abb[22] * z[9];
z[4] = (T(-1) / T(2)) * z[4] + z[9];
z[4] = abb[43] * z[4];
z[9] = abb[50] + z[37];
z[18] = z[9] * z[18];
z[14] = -z[14] + -z[17];
z[14] = abb[49] * z[14];
z[14] = z[14] + z[18];
z[17] = -abb[51] * z[17];
z[14] = (T(1) / T(2)) * z[14] + z[17];
z[17] = abb[13] * (T(1) / T(16));
z[14] = z[14] * z[17];
z[3] = abb[56] + (T(1) / T(8)) * z[3] + (T(1) / T(16)) * z[4] + z[7] + z[14];
z[4] = abb[26] * (T(1) / T(2));
z[7] = abb[30] * (T(1) / T(4)) + z[4];
z[14] = -abb[27] + z[28];
z[18] = z[7] + -z[13] + z[14];
z[18] = abb[30] * z[18];
z[19] = z[13] + -z[30];
z[20] = z[13] * z[19];
z[22] = -abb[27] + abb[28];
z[23] = z[2] * z[22];
z[24] = abb[26] * (T(1) / T(4));
z[26] = -abb[27] + z[24];
z[26] = abb[26] * z[26];
z[28] = -abb[27] + abb[28] * (T(3) / T(4)) + -z[4];
z[28] = abb[28] * z[28];
z[35] = abb[34] + -abb[53] + abb[54];
z[35] = (T(-1) / T(2)) * z[35];
z[39] = abb[33] * (T(1) / T(2));
z[42] = prod_pow(m1_set::bc<T>[0], 2);
z[43] = (T(1) / T(6)) * z[42];
z[18] = z[18] + z[20] + -z[23] + z[26] + -z[28] + -z[35] + -z[39] + z[43];
z[18] = abb[6] * z[18];
z[20] = abb[26] + abb[27];
z[26] = (T(1) / T(2)) * z[20];
z[21] = -z[21] + z[26];
z[21] = abb[29] * z[21];
z[21] = abb[33] + z[21];
z[28] = -z[29] + z[41];
z[28] = abb[30] * z[28];
z[41] = (T(1) / T(2)) * z[42];
z[44] = abb[53] + z[41];
z[31] = abb[28] * z[31];
z[45] = prod_pow(abb[31], 2);
z[46] = (T(1) / T(2)) * z[45];
z[47] = abb[26] * z[1];
z[28] = abb[54] + -z[21] + -z[28] + z[31] + z[44] + -z[46] + z[47];
z[28] = abb[5] * z[28];
z[31] = z[4] * z[30];
z[2] = z[2] * z[30];
z[48] = -abb[33] + z[2];
z[49] = abb[31] * z[30];
z[50] = (T(1) / T(3)) * z[42];
z[49] = z[31] + z[48] + -z[49] + -z[50];
z[51] = -abb[14] * z[49];
z[52] = abb[28] * z[22];
z[42] = abb[53] + (T(5) / T(6)) * z[42];
z[53] = abb[30] * z[22];
z[53] = abb[54] + z[53];
z[52] = z[42] + -z[52] + z[53];
z[54] = abb[7] * z[52];
z[33] = abb[55] * z[33];
z[33] = (T(1) / T(2)) * z[33];
z[28] = z[28] + z[33] + z[51] + -z[54];
z[51] = abb[29] * abb[31];
z[54] = abb[29] + -abb[31];
z[55] = abb[30] * z[54];
z[56] = -abb[33] + z[43];
z[51] = -abb[34] + -z[45] + z[51] + -z[55] + z[56];
z[55] = abb[2] * (T(1) / T(2));
z[51] = z[51] * z[55];
z[55] = prod_pow(abb[28], 2);
z[57] = prod_pow(abb[26], 2);
z[55] = -z[45] + -z[55] + z[57];
z[58] = z[16] + -z[29];
z[58] = abb[30] * z[58];
z[55] = (T(1) / T(2)) * z[55] + z[58];
z[58] = abb[32] + -abb[34];
z[59] = -abb[35] + z[58];
z[60] = abb[54] + z[55] + z[59];
z[25] = z[25] * z[60];
z[20] = z[4] * z[20];
z[26] = -abb[28] + z[26];
z[26] = abb[29] * z[26];
z[26] = -z[20] + z[26];
z[60] = -abb[32] + z[26];
z[61] = abb[26] * abb[28];
z[62] = abb[53] + z[50];
z[63] = z[60] + z[61] + -z[62];
z[27] = z[27] * z[63];
z[25] = z[25] + -z[27] + (T(1) / T(2)) * z[28] + z[51];
z[27] = abb[5] + abb[6];
z[28] = abb[35] * (T(1) / T(2));
z[51] = z[27] * z[28];
z[18] = z[18] + -z[25] + z[51];
z[51] = abb[16] * abb[55];
z[51] = (T(1) / T(4)) * z[51];
z[64] = -z[18] + -z[51];
z[65] = -abb[31] + z[30];
z[29] = z[29] + z[65];
z[29] = abb[30] * z[29];
z[14] = abb[28] * z[14];
z[14] = z[14] + -z[29];
z[19] = abb[31] * z[19];
z[19] = z[14] + -z[19];
z[29] = -abb[26] + abb[27] * (T(3) / T(2));
z[29] = abb[26] * z[29];
z[29] = abb[33] + abb[53] + z[2] + z[19] + z[29] + z[43];
z[66] = abb[32] + abb[34] + abb[35] + -z[29];
z[67] = abb[4] * (T(1) / T(4));
z[66] = z[66] * z[67];
z[15] = abb[29] * z[15];
z[15] = z[15] + z[57] + -z[61];
z[15] = abb[32] + (T(1) / T(2)) * z[15];
z[15] = abb[8] * z[15];
z[61] = abb[35] + abb[52] + -abb[54];
z[38] = z[38] * z[61];
z[32] = abb[3] + z[32];
z[32] = abb[52] * z[32];
z[36] = abb[55] * z[36];
z[61] = (T(1) / T(4)) * z[32] + -z[36];
z[68] = z[38] + -z[61];
z[64] = -z[15] + (T(1) / T(2)) * z[64] + z[66] + z[68];
z[64] = abb[48] * z[64];
z[18] = z[18] + -z[51];
z[44] = z[19] + -z[44] + z[47] + -z[48];
z[48] = z[44] + z[59];
z[66] = abb[4] * (T(1) / T(2));
z[66] = -z[48] * z[66];
z[32] = -z[18] + (T(-1) / T(2)) * z[32] + z[66];
z[66] = abb[28] * z[4];
z[23] = -z[23] + -z[47] + -z[62] + z[66];
z[23] = abb[11] * z[23];
z[32] = z[23] + (T(1) / T(2)) * z[32] + z[36] + z[38];
z[32] = abb[50] * z[32];
z[36] = -z[48] * z[67];
z[38] = -abb[34] + z[55];
z[38] = abb[32] + -z[28] + (T(1) / T(2)) * z[38];
z[38] = abb[8] * z[38];
z[18] = (T(-1) / T(2)) * z[18] + z[23] + z[36] + -z[38] + -z[61];
z[18] = abb[49] * z[18];
z[6] = z[6] * z[49];
z[36] = z[22] + z[54];
z[36] = abb[30] * z[36];
z[47] = abb[26] * abb[27];
z[54] = -abb[26] + z[22];
z[54] = abb[28] * z[54];
z[47] = z[47] + z[54];
z[16] = abb[29] * z[16];
z[55] = abb[31] * z[65];
z[16] = z[16] + -z[36] + z[47] + z[55];
z[55] = -abb[54] + z[16];
z[61] = -abb[34] + z[55];
z[61] = -z[5] * z[61];
z[65] = abb[32] * z[5];
z[61] = z[61] + -z[65];
z[61] = abb[19] * z[61];
z[44] = -abb[34] + -abb[35] + z[44];
z[44] = z[5] * z[44];
z[44] = z[44] + z[65];
z[66] = -abb[21] * z[44];
z[44] = abb[23] * z[44];
z[41] = -z[41] + -z[53] + z[54];
z[26] = -z[26] + z[41];
z[26] = -z[5] * z[26];
z[26] = z[26] + -z[65];
z[26] = abb[20] * z[26];
z[12] = abb[55] * z[12];
z[65] = abb[19] + abb[24];
z[65] = abb[52] * z[65];
z[12] = z[12] + z[65];
z[5] = -z[5] * z[12];
z[5] = z[5] + z[6] + z[26] + -z[44] + z[61] + z[66];
z[6] = abb[28] * (T(3) / T(2));
z[0] = z[0] + -z[1] + z[6];
z[0] = abb[29] * z[0];
z[1] = abb[27] + -abb[29] + -z[6] + z[7] + z[13];
z[1] = abb[30] * z[1];
z[6] = abb[31] * (T(-3) / T(2)) + z[30];
z[6] = z[6] * z[13];
z[7] = abb[27] + z[24];
z[7] = abb[26] * z[7];
z[13] = -abb[27] + abb[26] * (T(-3) / T(2)) + abb[28] * (T(5) / T(4));
z[13] = abb[28] * z[13];
z[0] = z[0] + z[1] + z[6] + z[7] + z[13] + z[35] + -z[39];
z[0] = abb[6] * z[0];
z[1] = -abb[5] + abb[6];
z[1] = z[1] * z[28];
z[0] = z[0] + z[1] + z[25] + -z[51];
z[1] = 3 * abb[35] + -z[29] + -z[58];
z[1] = z[1] * z[67];
z[0] = (T(1) / T(2)) * z[0] + z[1] + -z[38] + z[68];
z[0] = abb[47] * z[0];
z[1] = -abb[54] + z[20] + -z[21] + -z[36] + -z[45] + -z[50] + z[54] + z[58];
z[1] = abb[47] * z[1];
z[6] = -abb[52] + z[14] + -z[42] + -z[46] + (T(1) / T(2)) * z[57] + z[59];
z[6] = abb[49] * z[6];
z[7] = z[41] + -z[60];
z[13] = abb[48] * z[7];
z[14] = -abb[50] * z[52];
z[1] = z[1] + z[6] + z[13] + z[14];
z[6] = abb[0] * (T(1) / T(2));
z[1] = z[1] * z[6];
z[13] = abb[24] * z[49];
z[8] = z[8] * z[48];
z[14] = z[55] + z[58];
z[20] = abb[19] * z[14];
z[7] = abb[20] * z[7];
z[7] = z[7] + z[8] + z[12] + -z[13] + z[20];
z[8] = z[7] * z[11];
z[11] = abb[29] * z[22];
z[11] = abb[53] + z[11] + -z[43] + z[47] + -z[53];
z[11] = -z[11] * z[37];
z[12] = z[34] * z[52];
z[11] = z[11] + z[12];
z[11] = abb[1] * z[11];
z[0] = z[0] + z[1] + (T(1) / T(4)) * z[5] + z[8] + (T(1) / T(2)) * z[11] + z[18] + z[32] + z[64];
z[1] = -abb[22] * z[52];
z[1] = z[1] + z[7];
z[1] = abb[43] * z[1];
z[5] = abb[47] + -abb[51];
z[5] = abb[57] * z[5];
z[1] = z[1] + z[5];
z[5] = abb[42] * z[7];
z[7] = abb[22] * z[40] * z[52];
z[5] = z[5] + z[7];
z[2] = z[2] + -z[31] + z[62];
z[2] = abb[5] * z[2];
z[4] = abb[27] + -z[4];
z[4] = abb[26] * z[4];
z[4] = -abb[34] + z[4] + z[19] + -z[56];
z[4] = abb[6] * z[4];
z[7] = -abb[12] * z[63];
z[8] = -abb[35] * z[27];
z[2] = z[2] + z[4] + z[7] + z[8] + z[33];
z[4] = z[6] * z[48];
z[2] = (T(1) / T(2)) * z[2] + z[4] + -z[15] + z[23];
z[2] = z[2] * z[10];
z[4] = abb[52] + z[14];
z[4] = -z[4] * z[9];
z[6] = abb[52] + -abb[54] + -z[16] + -z[58];
z[6] = abb[35] + (T(1) / T(2)) * z[6];
z[6] = abb[49] * z[6];
z[7] = -z[16] + -z[59];
z[7] = abb[51] * z[7];
z[4] = (T(1) / T(2)) * z[4] + z[6] + z[7];
z[4] = z[4] * z[17];
z[0] = abb[40] + (T(1) / T(8)) * z[0] + (T(1) / T(32)) * z[1] + z[2] + z[4] + (T(1) / T(16)) * z[5];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_452_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.25882026428478713445743298464361446146557975334912340494472285905"),stof<T>("0.2609877071471083312900970628554752505621442058450728515324213611")}, std::complex<T>{stof<T>("0.48664448314444038312565461785832844850005218639148328695070142887"),stof<T>("0.2609877071471083312900970628554752505621442058450728515324213611")}, std::complex<T>{stof<T>("0.48195449955120231746018184934737231775710566043504348432322095709"),stof<T>("0.25824662055587686301639274133557152984171442304486007510819287997")}, std::complex<T>{stof<T>("0.47545341071950454568343583010671366567649273593187729114674517307"),stof<T>("0.40687630736128679929112184295536286802749357323732339648371288076")}, std::complex<T>{stof<T>("0.42417916664465846209173721767453931134663348831501127174786619582"),stof<T>("0.27200154240207612778771479084562495971707996417798871991158691521")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_452_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_452_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_452_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(512)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (-30 * v[0] + 14 * v[1] + -6 * v[2] + -6 * v[3] + 6 * v[5] + m1_set::bc<T>[2] * (21 * v[0] + -v[1] + -3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[2] * (T(-3) / T(64)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[47] + abb[49] + abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[47] + -abb[49] + -abb[51];
z[1] = -abb[32] + abb[35];
return abb[9] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_452_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.48248148640221261076385217222013159313429013656826037603043918887"),stof<T>("0.00681280026213762734428781509628845927330893600981912544301744637")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W17(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k), T{0}, T{0}};
abb[40] = SpDLog_f_4_452_W_17_Im(t, path, abb);
abb[41] = SpDLogQ_W_74(k,dl,dlr).imag();
abb[56] = SpDLog_f_4_452_W_17_Re(t, path, abb);
abb[57] = SpDLogQ_W_74(k,dl,dlr).real();

                    
            return f_4_452_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_452_DLogXconstant_part(base_point<T>, kend);
	value += f_4_452_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_452_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_452_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_452_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_452_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_452_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_452_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
