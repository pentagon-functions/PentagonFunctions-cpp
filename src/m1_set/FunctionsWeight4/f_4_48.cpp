/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_48.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_48_abbreviated (const std::array<T,27>& abb) {
T z[55];
z[0] = abb[18] * (T(3) / T(2));
z[1] = abb[21] * (T(1) / T(2));
z[2] = 2 * abb[19] + abb[20];
z[3] = 2 * z[2];
z[4] = z[0] + z[1] + -z[3];
z[5] = abb[13] + -abb[14];
z[4] = z[4] * z[5];
z[6] = abb[19] + abb[20];
z[6] = 2 * z[6];
z[7] = abb[18] * (T(1) / T(2));
z[8] = z[6] + z[7];
z[8] = abb[11] * z[8];
z[9] = abb[11] * abb[21];
z[10] = (T(1) / T(2)) * z[9];
z[8] = z[8] + -z[10];
z[4] = z[4] + z[8];
z[4] = abb[13] * z[4];
z[11] = 3 * abb[19];
z[12] = 2 * abb[20];
z[13] = z[11] + z[12];
z[14] = -abb[21] + z[13];
z[14] = -z[5] * z[14];
z[15] = abb[11] * z[13];
z[14] = -z[9] + z[14] + z[15];
z[15] = 2 * z[13];
z[16] = 3 * abb[21];
z[17] = -abb[18] + -z[15] + z[16];
z[17] = abb[12] * z[17];
z[14] = 2 * z[14] + z[17];
z[14] = abb[12] * z[14];
z[17] = -abb[18] + z[3];
z[18] = -abb[21] + z[17];
z[19] = prod_pow(abb[11], 2);
z[18] = z[18] * z[19];
z[8] = -abb[14] * z[8];
z[20] = -abb[19] + abb[21];
z[21] = 2 * abb[16];
z[22] = z[20] * z[21];
z[23] = 3 * z[13];
z[24] = 4 * abb[21];
z[25] = -abb[18] + -z[23] + z[24];
z[25] = abb[15] * z[25];
z[4] = z[4] + z[8] + z[14] + -z[18] + z[22] + z[25];
z[4] = abb[0] * z[4];
z[8] = z[3] + -z[7];
z[14] = abb[11] * z[8];
z[6] = z[6] + -z[7];
z[1] = z[1] + z[6];
z[1] = abb[14] * z[1];
z[7] = abb[21] * (T(3) / T(2));
z[8] = -z[7] + z[8];
z[22] = -abb[13] * z[8];
z[1] = z[1] + (T(-3) / T(2)) * z[9] + z[14] + z[22];
z[1] = abb[13] * z[1];
z[14] = abb[18] + abb[21];
z[3] = z[3] + -z[14];
z[22] = abb[14] * z[3];
z[6] = -abb[11] * z[6];
z[6] = z[6] + -z[10] + -z[22];
z[6] = abb[14] * z[6];
z[10] = -abb[18] + z[13];
z[25] = abb[11] + -z[5];
z[10] = z[10] * z[25];
z[25] = 3 * abb[18];
z[15] = -abb[21] + -z[15] + z[25];
z[15] = abb[12] * z[15];
z[10] = 2 * z[10] + z[15];
z[10] = abb[12] * z[10];
z[15] = abb[11] * abb[14];
z[26] = abb[11] + abb[14];
z[27] = -abb[13] + z[26];
z[28] = abb[13] * z[27];
z[29] = prod_pow(abb[12], 2);
z[15] = -z[15] + z[28] + 2 * z[29];
z[30] = z[15] + z[21];
z[30] = abb[23] * z[30];
z[31] = 4 * abb[18];
z[23] = -abb[21] + -z[23] + z[31];
z[23] = abb[16] * z[23];
z[32] = abb[18] + -abb[19];
z[33] = 2 * abb[15];
z[34] = z[32] * z[33];
z[1] = z[1] + z[6] + z[10] + z[23] + z[30] + z[34];
z[1] = abb[3] * z[1];
z[6] = z[19] + z[33];
z[10] = 2 * z[27];
z[23] = -abb[12] + z[10];
z[23] = abb[12] * z[23];
z[30] = abb[14] * z[26];
z[34] = abb[13] * z[26];
z[30] = -z[6] + -z[21] + z[23] + -z[30] + z[34];
z[35] = abb[6] * z[30];
z[36] = z[6] + -z[29];
z[36] = abb[4] * z[36];
z[37] = prod_pow(abb[14], 2);
z[37] = z[21] + z[37];
z[29] = -z[29] + z[37];
z[38] = -abb[5] * z[29];
z[38] = z[35] + z[36] + -z[38];
z[39] = abb[15] + abb[16];
z[40] = -3 * abb[11] + -2 * abb[14];
z[40] = abb[14] * z[40];
z[41] = -5 * abb[12] + 6 * z[27];
z[41] = abb[12] * z[41];
z[34] = -2 * z[19] + 3 * z[34] + -4 * z[39] + z[40] + z[41];
z[34] = abb[2] * z[34];
z[23] = z[23] + (T(3) / T(2)) * z[28];
z[28] = abb[11] * (T(3) / T(2));
z[40] = abb[14] * z[28];
z[40] = z[6] + -z[23] + z[40];
z[40] = abb[0] * z[40];
z[28] = abb[14] + z[28];
z[28] = abb[14] * z[28];
z[23] = z[21] + -z[23] + z[28];
z[28] = abb[3] * z[23];
z[41] = 2 * abb[11] + abb[14];
z[41] = abb[14] * z[41];
z[42] = -abb[13] + 2 * z[26];
z[42] = abb[13] * z[42];
z[41] = z[41] + -z[42];
z[42] = -z[19] + -z[41];
z[43] = -7 * abb[12] + 10 * z[27];
z[43] = abb[12] * z[43];
z[39] = -6 * z[39] + 3 * z[42] + z[43];
z[39] = abb[1] * z[39];
z[42] = 2 * abb[9] + abb[7] * (T(3) / T(2));
z[42] = abb[17] * z[42];
z[28] = z[28] + z[34] + -2 * z[38] + z[39] + z[40] + z[42];
z[28] = abb[24] * z[28];
z[0] = abb[20] * (T(-7) / T(6)) + z[0] + z[7] + -z[11];
z[0] = abb[2] * z[0];
z[7] = abb[20] * (T(3) / T(2)) + abb[19] * (T(11) / T(3));
z[34] = abb[21] * (T(13) / T(6));
z[38] = z[7] + -z[34];
z[38] = abb[0] * z[38];
z[39] = abb[18] + abb[23];
z[40] = (T(13) / T(6)) * z[39];
z[7] = z[7] + -z[40];
z[7] = abb[3] * z[7];
z[34] = -3 * abb[20] + abb[19] * (T(-22) / T(3)) + z[34] + z[40];
z[34] = abb[1] * z[34];
z[40] = abb[0] + abb[3];
z[42] = 2 * abb[1];
z[43] = z[40] + -z[42];
z[43] = abb[24] * z[43];
z[44] = abb[25] + abb[26];
z[45] = (T(-1) / T(3)) * z[44];
z[45] = abb[7] * z[45];
z[46] = -abb[0] + abb[1];
z[47] = abb[22] * z[46];
z[0] = z[0] + z[7] + z[34] + z[38] + (T(1) / T(3)) * z[43] + z[45] + (T(13) / T(6)) * z[47];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[7] = abb[23] * z[30];
z[7] = z[7] + -z[18];
z[17] = abb[11] * z[17];
z[17] = -z[9] + z[17];
z[34] = 2 * z[17] + z[22];
z[34] = abb[14] * z[34];
z[38] = abb[13] * z[3];
z[43] = -z[22] + z[38];
z[45] = -z[17] + z[43];
z[47] = abb[12] * z[3];
z[48] = z[45] + z[47];
z[49] = 4 * abb[12];
z[48] = z[48] * z[49];
z[34] = z[34] + z[48];
z[22] = z[17] + z[22];
z[14] = z[2] + -z[14];
z[48] = abb[13] * z[14];
z[48] = -z[22] + z[48];
z[49] = 2 * abb[13];
z[48] = z[48] * z[49];
z[50] = 2 * abb[21];
z[12] = 5 * abb[19] + z[12];
z[51] = -abb[18] + z[12] + -z[50];
z[51] = z[21] * z[51];
z[52] = 2 * abb[18];
z[53] = -abb[21] + z[12] + -z[52];
z[53] = z[33] * z[53];
z[48] = -z[7] + z[34] + z[48] + z[51] + z[53];
z[48] = abb[2] * z[48];
z[51] = -z[21] + -z[33];
z[51] = z[3] * z[51];
z[53] = z[5] * z[22];
z[54] = -2 * z[45] + -z[47];
z[54] = abb[12] * z[54];
z[7] = z[7] + z[51] + z[53] + z[54];
z[7] = abb[6] * z[7];
z[51] = -2 * z[22] + z[38];
z[51] = abb[13] * z[51];
z[53] = 12 * abb[19] + 5 * abb[20];
z[24] = -z[24] + -z[25] + z[53];
z[24] = abb[16] * z[24];
z[31] = -z[16] + -z[31] + z[53];
z[31] = abb[15] * z[31];
z[18] = z[18] + z[24] + z[31] + z[34] + z[51];
z[10] = abb[12] + z[10];
z[10] = abb[12] * z[10];
z[10] = z[10] + -z[41];
z[24] = z[6] + -z[10];
z[24] = abb[23] * z[24];
z[18] = 2 * z[18] + z[24];
z[18] = abb[1] * z[18];
z[24] = -abb[2] * z[30];
z[15] = z[15] + z[33];
z[15] = abb[0] * z[15];
z[10] = -z[10] + z[19] + z[21];
z[10] = abb[1] * z[10];
z[21] = abb[8] + -abb[9];
z[21] = abb[17] * z[21];
z[10] = z[10] + z[15] + z[21] + z[24] + z[35] + 2 * z[36];
z[10] = abb[22] * z[10];
z[15] = abb[9] * z[30];
z[6] = z[6] + -z[37];
z[6] = abb[8] * z[6];
z[21] = abb[7] * z[23];
z[23] = abb[2] + -2 * abb[10] + (T(1) / T(2)) * z[40];
z[23] = abb[6] + 3 * z[23];
z[23] = abb[17] * z[23];
z[6] = -z[6] + -z[15] + z[21] + z[23];
z[6] = -z[6] * z[44];
z[15] = abb[11] * abb[19];
z[9] = -z[9] + z[15];
z[15] = abb[20] + z[11];
z[21] = -z[15] + z[50];
z[21] = abb[12] * z[21];
z[23] = abb[14] * z[20];
z[20] = abb[13] * z[20];
z[20] = z[9] + z[20] + z[21] + -z[23];
z[20] = abb[12] * z[20];
z[21] = -abb[21] + z[2];
z[21] = abb[14] * z[21];
z[9] = -z[9] + z[21];
z[9] = abb[14] * z[9];
z[21] = -abb[13] * z[23];
z[9] = z[9] + z[20] + z[21];
z[20] = 2 * abb[23];
z[21] = z[20] * z[29];
z[11] = 4 * abb[20] + z[11];
z[24] = abb[21] + z[11];
z[24] = abb[16] * z[24];
z[9] = 2 * z[9] + z[21] + z[24];
z[9] = abb[5] * z[9];
z[21] = z[5] * z[32];
z[15] = -z[15] + z[52];
z[15] = abb[12] * z[15];
z[24] = abb[11] * z[32];
z[15] = z[15] + z[21] + -z[24];
z[15] = abb[12] * z[15];
z[21] = -z[5] * z[24];
z[2] = -abb[18] + z[2];
z[2] = z[2] * z[19];
z[2] = z[2] + z[15] + z[21];
z[11] = abb[18] + z[11];
z[11] = abb[15] * z[11];
z[2] = 2 * z[2] + z[11];
z[2] = abb[4] * z[2];
z[3] = -abb[23] + -z[3];
z[3] = abb[9] * z[3];
z[8] = -abb[23] + -z[8];
z[8] = abb[7] * z[8];
z[11] = abb[21] + -z[39];
z[11] = abb[8] * z[11];
z[3] = z[3] + z[8] + z[11];
z[3] = abb[17] * z[3];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[6] + z[7] + z[9] + z[10] + z[18] + z[28] + z[48];
z[1] = abb[21] + z[13] + -z[52];
z[1] = abb[12] * z[1];
z[2] = 2 * abb[12];
z[3] = -abb[23] * z[2];
z[1] = z[1] + z[3] + -z[17] + -z[23] + z[38];
z[1] = abb[3] * z[1];
z[3] = abb[12] + -abb[14];
z[3] = abb[5] * z[3];
z[4] = -abb[11] + abb[12];
z[4] = abb[4] * z[4];
z[6] = -z[3] + -z[4];
z[2] = z[2] + -z[26];
z[2] = abb[2] * z[2];
z[7] = abb[12] + -z[27];
z[7] = z[7] * z[42];
z[5] = -abb[0] * z[5];
z[8] = -abb[11] + abb[13];
z[9] = -abb[3] * z[8];
z[2] = z[2] + z[5] + 2 * z[6] + z[7] + z[9];
z[2] = abb[24] * z[2];
z[5] = 2 * z[47];
z[6] = -z[14] * z[49];
z[6] = -z[5] + z[6] + z[22];
z[6] = abb[2] * z[6];
z[7] = z[12] + -z[16] + z[20];
z[3] = z[3] * z[7];
z[7] = abb[18] + z[13] + -z[50];
z[7] = abb[12] * z[7];
z[7] = z[7] + -z[24] + z[43];
z[7] = abb[0] * z[7];
z[9] = abb[12] * abb[23];
z[5] = -z[5] + z[9] + -z[45];
z[5] = z[5] * z[42];
z[9] = -abb[11] + abb[14];
z[9] = abb[8] * z[9];
z[8] = abb[7] * z[8];
z[8] = z[8] + z[9];
z[8] = z[8] * z[44];
z[9] = -z[12] + z[25];
z[9] = -z[4] * z[9];
z[10] = abb[12] * z[46];
z[4] = z[4] + z[10];
z[4] = abb[22] * z[4];
z[1] = z[1] + z[2] + z[3] + 2 * z[4] + z[5] + z[6] + z[7] + z[8] + z[9];
z[1] = 2 * m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_48_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.8742691948245303969612672122180536053732680916646295835217907"),stof<T>("-39.833880071614355688647587564328552413312898317304811739516838307")}, std::complex<T>{stof<T>("-53.732978609221249629238752626557891109031379935235954035498314276"),stof<T>("22.755612255801653928817376150152361921475396123299845682965988934")}, std::complex<T>{stof<T>("-27.990457030907411866549297555463193072482579716816864838033733418"),stof<T>("-1.320568075096230650358167647681362941151242414809926594584266385")}, std::complex<T>{stof<T>("46.995927610188462567691014469128726606395927035362035971262342521"),stof<T>("23.9441878386074336941506077992027214572360275931736645044076572")}, std::complex<T>{stof<T>("26.020008535337350086719711905342405389990540756374283712513714011"),stof<T>("25.638171302187677012676173826844938907631284341544786708623200238")}, std::complex<T>{stof<T>("-14.235614369537093677332968124518190850967208527247491741360852382"),stof<T>("-24.811106568733956045617349238159990093643421220443962861420478247")}, std::complex<T>{stof<T>("-1.586536985971469475438869205588498215507305618794122161697549587"),stof<T>("-3.8464923781242264506896104868557398896931942601347611475297649083")}, std::complex<T>{stof<T>("-3.223914705502374423711964070030749960523055343174597416322871061"),stof<T>("-15.748931692217380713457583612152853300165062016478596541063558734")}, std::complex<T>{stof<T>("-3.223914705502374423711964070030749960523055343174597416322871061"),stof<T>("-15.748931692217380713457583612152853300165062016478596541063558734")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_48_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_48_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-13.931774549010400184365465077473426398504375572811063138582766005"),stof<T>("48.888963568732113554103077905733627948307592886185031624601006595")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_48_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_48_DLogXconstant_part(base_point<T>, kend);
	value += f_4_48_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_48_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_48_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_48_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_48_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_48_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_48_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
