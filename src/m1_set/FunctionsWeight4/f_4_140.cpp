/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_140.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_140_abbreviated (const std::array<T,29>& abb) {
T z[59];
z[0] = abb[12] * (T(1) / T(2));
z[1] = abb[11] + -abb[13];
z[2] = -abb[14] + z[0] + (T(1) / T(2)) * z[1];
z[2] = abb[12] * z[2];
z[3] = abb[14] * (T(1) / T(2));
z[4] = -z[1] + z[3];
z[4] = abb[14] * z[4];
z[5] = abb[11] * (T(1) / T(2));
z[6] = z[1] * z[5];
z[2] = z[2] + z[4] + z[6];
z[4] = -abb[6] * z[2];
z[6] = prod_pow(abb[13], 2);
z[7] = -abb[11] * abb[13];
z[8] = abb[11] + abb[13];
z[9] = abb[12] * z[8];
z[7] = -z[6] + z[7] + z[9];
z[7] = abb[0] * z[7];
z[9] = 3 * abb[13];
z[10] = -abb[11] + z[9];
z[10] = z[5] * z[10];
z[10] = -z[6] + z[10];
z[11] = 2 * abb[14];
z[12] = -abb[12] + z[11];
z[13] = (T(-5) / T(4)) * z[1] + z[12];
z[13] = abb[12] * z[13];
z[14] = 2 * z[1];
z[15] = -abb[14] + z[14];
z[15] = abb[14] * z[15];
z[10] = (T(1) / T(2)) * z[10] + z[13] + z[15];
z[10] = abb[2] * z[10];
z[13] = -abb[11] + abb[14];
z[16] = 2 * abb[12];
z[13] = z[13] * z[16];
z[17] = -z[1] + z[11];
z[18] = -abb[14] * z[17];
z[19] = abb[11] * z[8];
z[18] = z[13] + z[18] + z[19];
z[18] = abb[4] * z[18];
z[19] = 2 * abb[13];
z[20] = -abb[11] + z[19];
z[21] = abb[11] * z[20];
z[21] = -z[6] + z[21];
z[22] = 3 * z[17];
z[23] = z[16] + -z[22];
z[23] = abb[12] * z[23];
z[24] = 5 * z[1];
z[25] = 4 * abb[14] + -z[24];
z[25] = abb[14] * z[25];
z[23] = -z[21] + z[23] + z[25];
z[23] = abb[1] * z[23];
z[25] = abb[5] * z[0];
z[26] = abb[5] * abb[14];
z[25] = z[25] + z[26];
z[25] = abb[12] * z[25];
z[27] = prod_pow(abb[14], 2);
z[28] = abb[5] * z[27];
z[25] = z[25] + (T(-3) / T(2)) * z[28];
z[29] = abb[15] * (T(1) / T(4));
z[30] = -abb[7] + -5 * abb[8];
z[30] = z[29] * z[30];
z[4] = z[4] + (T(1) / T(2)) * z[7] + z[10] + z[18] + z[23] + z[25] + z[30];
z[4] = abb[22] * z[4];
z[7] = 2 * abb[23];
z[10] = 2 * abb[20] + z[7];
z[18] = -abb[4] + abb[6];
z[23] = -abb[1] + z[18];
z[10] = z[10] * z[23];
z[23] = -abb[18] + -abb[21] + abb[22];
z[30] = 2 * abb[3];
z[23] = z[23] * z[30];
z[31] = 7 * abb[18];
z[32] = 2 * abb[19];
z[33] = abb[21] + z[31] + -z[32];
z[33] = abb[1] * z[33];
z[18] = z[18] * z[32];
z[34] = 2 * abb[2];
z[35] = 5 * abb[4] + z[34];
z[35] = abb[21] * z[35];
z[36] = abb[6] + z[34];
z[37] = abb[4] + z[36];
z[38] = 5 * abb[1] + -z[37];
z[38] = abb[22] * z[38];
z[39] = -abb[4] + z[34];
z[39] = abb[18] * z[39];
z[40] = abb[18] + -abb[21];
z[41] = 3 * z[40];
z[42] = abb[6] * z[41];
z[10] = z[10] + -z[18] + z[23] + -z[33] + z[35] + z[38] + z[39] + z[42];
z[18] = -abb[27] * z[10];
z[23] = abb[19] + abb[21];
z[23] = 3 * z[23] + -z[31];
z[23] = abb[1] * z[23];
z[31] = -abb[22] + -z[32] + z[41];
z[31] = abb[3] * z[31];
z[33] = 2 * abb[6];
z[35] = -abb[5] + z[33];
z[38] = -3 * abb[1] + -z[30] + z[35];
z[38] = abb[20] * z[38];
z[39] = -abb[2] + abb[3];
z[41] = 2 * abb[1];
z[43] = -abb[6] + z[39] + z[41];
z[43] = z[7] * z[43];
z[36] = -4 * abb[1] + -abb[5] + z[36];
z[36] = abb[22] * z[36];
z[35] = abb[19] * z[35];
z[44] = abb[2] + -abb[5];
z[45] = abb[18] * z[44];
z[23] = z[23] + -z[31] + -z[35] + -z[36] + z[38] + z[42] + -z[43] + 2 * z[45];
z[31] = -abb[28] * z[23];
z[35] = z[0] * z[1];
z[36] = 3 * abb[11];
z[38] = -abb[13] + z[36];
z[42] = z[5] * z[38];
z[35] = z[6] + z[35] + -z[42];
z[42] = abb[2] * z[35];
z[43] = abb[12] + z[1];
z[45] = -z[11] + z[43];
z[45] = abb[12] * z[45];
z[46] = abb[11] * z[1];
z[47] = z[15] + -z[45] + -z[46];
z[47] = abb[6] * z[47];
z[48] = abb[14] * z[1];
z[48] = -z[46] + z[48];
z[49] = abb[4] * z[48];
z[47] = z[47] + -2 * z[49];
z[49] = prod_pow(abb[11], 2);
z[49] = -z[6] + 3 * z[49];
z[50] = abb[11] * abb[12];
z[49] = (T(1) / T(2)) * z[49] + -z[50];
z[49] = abb[0] * z[49];
z[50] = -abb[7] + abb[8];
z[29] = z[29] * z[50];
z[25] = -z[25] + z[29] + (T(1) / T(2)) * z[42] + -z[47] + z[49];
z[29] = -abb[19] * z[25];
z[42] = 5 * abb[13];
z[49] = -abb[11] + z[42];
z[49] = (T(1) / T(4)) * z[49];
z[51] = z[12] + z[49];
z[51] = abb[12] * z[51];
z[49] = abb[11] * z[49];
z[51] = -z[27] + -z[49] + z[51];
z[52] = abb[3] * z[51];
z[53] = abb[12] * (T(3) / T(2));
z[54] = 5 * abb[14] + -z[14];
z[55] = z[53] + -z[54];
z[55] = abb[12] * z[55];
z[56] = abb[14] * (T(7) / T(2)) + -z[14];
z[56] = abb[14] * z[56];
z[55] = z[55] + z[56];
z[56] = -abb[1] * z[55];
z[25] = z[25] + z[52] + z[56];
z[25] = abb[20] * z[25];
z[52] = abb[4] + -abb[5] + abb[6];
z[30] = z[30] + z[41] + -z[52];
z[30] = abb[20] * z[30];
z[32] = -z[32] + (T(7) / T(2)) * z[40];
z[32] = abb[1] * z[32];
z[56] = abb[2] + abb[4] * (T(5) / T(2));
z[56] = abb[21] * z[56];
z[57] = abb[4] * (T(1) / T(2));
z[58] = -abb[2] + 2 * abb[5] + -z[57];
z[58] = abb[18] * z[58];
z[52] = abb[19] * z[52];
z[30] = z[30] + z[32] + z[52] + z[56] + z[58];
z[32] = abb[6] * (T(1) / T(2));
z[44] = z[32] + z[44] + -z[57];
z[44] = abb[1] * (T(-1) / T(2)) + (T(1) / T(3)) * z[44];
z[44] = abb[22] * z[44];
z[32] = -z[32] * z[40];
z[52] = abb[3] * (T(2) / T(3));
z[56] = abb[18] + -abb[19] + -2 * abb[21];
z[56] = z[52] * z[56];
z[37] = abb[1] + (T(-1) / T(3)) * z[37] + z[52];
z[37] = abb[23] * z[37];
z[30] = (T(1) / T(3)) * z[30] + z[32] + z[37] + z[44] + z[56];
z[30] = prod_pow(m1_set::bc<T>[0], 2) * z[30];
z[32] = -z[5] * z[8];
z[37] = (T(3) / T(2)) * z[1] + -z[12];
z[37] = abb[12] * z[37];
z[6] = z[6] + -z[15] + z[32] + z[37];
z[6] = abb[2] * z[6];
z[15] = z[1] + z[11];
z[15] = abb[14] * z[15];
z[32] = -abb[11] * z[38];
z[15] = -z[13] + z[15] + z[32];
z[15] = abb[4] * z[15];
z[32] = abb[5] * abb[12];
z[37] = -abb[5] * z[11];
z[37] = -z[32] + z[37];
z[37] = abb[12] * z[37];
z[44] = -abb[11] + abb[12];
z[52] = abb[13] + z[36];
z[52] = -z[44] * z[52];
z[52] = (T(1) / T(2)) * z[52];
z[56] = abb[0] * z[52];
z[6] = z[6] + z[15] + 3 * z[28] + z[37] + z[56];
z[6] = abb[18] * z[6];
z[15] = 7 * abb[13] + -z[36];
z[5] = z[5] * z[15];
z[3] = -z[3] + z[14];
z[3] = abb[14] * z[3];
z[15] = -abb[11] + -z[9];
z[15] = -abb[14] + (T(1) / T(2)) * z[15] + z[53];
z[15] = abb[12] * z[15];
z[3] = z[3] + z[5] + z[15];
z[3] = abb[21] * z[3];
z[5] = -abb[19] * z[51];
z[15] = 3 * abb[14];
z[9] = 7 * abb[11] + -z[9];
z[0] = z[0] + (T(1) / T(4)) * z[9] + -z[15];
z[0] = abb[12] * z[0];
z[9] = abb[14] * (T(5) / T(2)) + -z[14];
z[9] = abb[14] * z[9];
z[0] = z[0] + z[9] + -z[49];
z[0] = abb[22] * z[0];
z[9] = -abb[18] * z[55];
z[0] = z[0] + z[3] + z[5] + z[9];
z[0] = abb[3] * z[0];
z[3] = -z[36] + z[42];
z[5] = -abb[11] * z[3];
z[9] = z[11] + -z[24];
z[9] = abb[14] * z[9];
z[5] = z[5] + z[9] + -z[13];
z[5] = abb[4] * z[5];
z[9] = abb[0] * z[35];
z[13] = -z[34] * z[48];
z[5] = z[5] + z[9] + z[13];
z[5] = abb[21] * z[5];
z[9] = 3 * abb[6];
z[9] = z[9] * z[40];
z[2] = z[2] * z[9];
z[13] = -z[1] + z[15];
z[28] = -z[13] + z[53];
z[28] = abb[12] * z[28];
z[34] = abb[14] * (T(3) / T(2)) + z[1];
z[34] = abb[14] * z[34];
z[28] = z[21] + z[28] + z[34];
z[28] = abb[21] * z[28];
z[34] = abb[14] * (T(-15) / T(2)) + 7 * z[1];
z[34] = abb[14] * z[34];
z[24] = 11 * abb[14] + -z[24];
z[36] = abb[12] * (T(-7) / T(2)) + z[24];
z[36] = abb[12] * z[36];
z[21] = z[21] + z[34] + z[36];
z[21] = abb[18] * z[21];
z[34] = abb[19] * z[55];
z[21] = z[21] + z[28] + z[34];
z[21] = abb[1] * z[21];
z[27] = z[27] + z[45] + -z[46];
z[27] = -z[27] * z[39];
z[28] = -abb[14] + z[1];
z[28] = abb[14] * z[28];
z[28] = z[28] + -z[45];
z[28] = z[28] * z[41];
z[34] = -abb[9] + z[50];
z[34] = abb[15] * z[34];
z[27] = z[27] + z[28] + z[34] + -z[47];
z[27] = abb[23] * z[27];
z[28] = abb[8] * z[35];
z[34] = abb[7] * z[52];
z[28] = -z[28] + z[34];
z[34] = -abb[0] + abb[10] + abb[3] * (T(-3) / T(4)) + abb[2] * (T(-1) / T(4));
z[34] = abb[15] * z[34];
z[28] = (T(1) / T(2)) * z[28] + z[34];
z[34] = abb[24] + -abb[25] + abb[26];
z[28] = z[28] * z[34];
z[35] = abb[7] * abb[21];
z[36] = abb[8] * abb[18];
z[35] = z[35] + z[36];
z[36] = abb[19] + -abb[20] + abb[22] * (T(1) / T(2)) + (T(-3) / T(2)) * z[40];
z[36] = abb[9] * z[36];
z[35] = (T(3) / T(2)) * z[35] + z[36];
z[35] = abb[15] * z[35];
z[0] = z[0] + z[2] + z[4] + z[5] + z[6] + z[18] + z[21] + z[25] + z[27] + z[28] + z[29] + z[30] + z[31] + z[35];
z[2] = z[26] + z[32];
z[4] = -z[16] + z[17];
z[5] = -abb[2] * z[4];
z[6] = -z[11] + z[16];
z[8] = z[6] + -z[8];
z[8] = abb[4] * z[8];
z[11] = -4 * abb[12] + z[22];
z[11] = abb[1] * z[11];
z[17] = -abb[14] + z[43];
z[18] = abb[6] * z[17];
z[21] = abb[12] + -abb[13];
z[21] = -abb[0] * z[21];
z[5] = -z[2] + z[5] + z[8] + z[11] + z[18] + z[21];
z[5] = abb[22] * z[5];
z[8] = z[17] * z[33];
z[11] = -2 * abb[11] + abb[12] + abb[13];
z[11] = abb[0] * z[11];
z[14] = abb[4] * z[14];
z[21] = abb[2] * z[1];
z[8] = z[2] + -z[8] + z[11] + z[14] + z[21];
z[11] = -abb[19] * z[8];
z[14] = 3 * abb[12];
z[22] = z[14] + -z[54];
z[25] = abb[1] * z[22];
z[12] = abb[11] + -z[12];
z[26] = abb[3] * z[12];
z[8] = z[8] + z[25] + z[26];
z[8] = abb[20] * z[8];
z[15] = z[15] + -z[16] + z[20];
z[15] = abb[22] * z[15];
z[16] = abb[12] + -abb[14];
z[19] = -z[16] + -z[19];
z[19] = abb[21] * z[19];
z[20] = abb[18] * z[22];
z[12] = -abb[19] * z[12];
z[12] = z[12] + z[15] + z[19] + z[20];
z[12] = abb[3] * z[12];
z[15] = z[16] * z[39];
z[4] = -abb[1] * z[4];
z[19] = abb[4] * z[1];
z[4] = z[4] + z[15] + -z[18] + z[19];
z[4] = z[4] * z[7];
z[3] = z[3] + -z[6];
z[3] = abb[4] * z[3];
z[7] = abb[0] * z[1];
z[7] = z[7] + -z[21];
z[3] = z[3] + 2 * z[7];
z[3] = abb[21] * z[3];
z[7] = abb[0] * z[44];
z[15] = -abb[2] * z[16];
z[2] = z[2] + z[7] + z[15];
z[6] = -z[6] + z[38];
z[6] = abb[4] * z[6];
z[2] = 2 * z[2] + z[6];
z[2] = abb[18] * z[2];
z[6] = z[13] + -z[14];
z[6] = abb[21] * z[6];
z[7] = -abb[19] * z[22];
z[13] = 7 * abb[12] + -z[24];
z[13] = abb[18] * z[13];
z[6] = z[6] + z[7] + z[13];
z[6] = abb[1] * z[6];
z[1] = abb[8] * z[1];
z[7] = abb[7] * z[44];
z[1] = z[1] + -z[7];
z[1] = -z[1] * z[34];
z[7] = -z[9] * z[17];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[11] + z[12];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[17] * z[23];
z[3] = -abb[16] * z[10];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_140_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.7444879519697299500114871294339077570939272891127743398732245032"),stof<T>("5.4572973370690093012412751342821759582066965594559431694162263972")}, std::complex<T>{stof<T>("-9.4836733716480489250452406149158055438897136750401057810714064609"),stof<T>("-2.7233450740786076994713869535259990765953567879075159640477636932")}, std::complex<T>{stof<T>("9.4836733716480489250452406149158055438897136750401057810714064609"),stof<T>("2.7233450740786076994713869535259990765953567879075159640477636932")}, std::complex<T>{stof<T>("-23.331751364071429312292886896918160591771964384806307357085929561"),stof<T>("-8.344368391479002492933747170086404892626198217796592228353621256")}, std::complex<T>{stof<T>("-1.2608145803216810249662465145181022132042136140726685588018180423"),stof<T>("-2.733952262990401601769888180756176881611339771548427205368462704")}, std::complex<T>{stof<T>("15.108892572745061412213892796520457261086464323838870134816341142"),stof<T>("8.354975580390796395232248397316582697642181201437503469674320267")}, std::complex<T>{stof<T>("-0.3565823545324497271198310363827073499462153868107105955646427133"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("0.3565823545324497271198310363827073499462153868107105955646427133"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-0.3565823545324497271198310363827073499462153868107105955646427133"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[41].real()/kbase.W[41].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_140_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_140_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("15.554603762166656349501460478940725252939900139930838963360160996"),stof<T>("-20.31605565824020161725374012570488206232123268951654362385585106")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[41].real()/k.W[41].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_140_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_140_DLogXconstant_part(base_point<T>, kend);
	value += f_4_140_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_140_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_140_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_140_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_140_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_140_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_140_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
