/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_522.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_522_abbreviated (const std::array<T,30>& abb) {
T z[40];
z[0] = abb[19] + abb[20];
z[1] = 2 * abb[26];
z[2] = 2 * abb[21] + z[0] + -z[1];
z[3] = -abb[6] * z[2];
z[4] = 4 * abb[21] + abb[22];
z[5] = 7 * abb[26] + -z[4];
z[6] = 2 * z[0] + -z[5];
z[7] = 2 * abb[3];
z[6] = z[6] * z[7];
z[7] = -abb[26] + abb[19] * (T(1) / T(4));
z[4] = abb[20] * (T(9) / T(4)) + z[4] + 7 * z[7];
z[4] = abb[0] * z[4];
z[8] = abb[20] * (T(7) / T(4));
z[5] = abb[19] * (T(9) / T(4)) + -z[5] + z[8];
z[9] = abb[2] * z[5];
z[10] = abb[24] * (T(7) / T(2));
z[11] = abb[23] + z[10];
z[12] = 2 * abb[25];
z[11] = (T(1) / T(2)) * z[11] + -z[12];
z[13] = abb[8] * z[11];
z[0] = 14 * abb[21] + 2 * abb[22] + -20 * abb[26] + 7 * z[0];
z[0] = abb[10] * z[0];
z[14] = 2 * abb[24];
z[15] = -abb[25] + z[14];
z[16] = abb[9] * z[15];
z[17] = abb[24] * (T(1) / T(2));
z[18] = abb[23] + z[17];
z[19] = abb[7] * z[18];
z[0] = -z[0] + -z[3] + z[4] + z[6] + z[9] + -z[13] + z[16] + -z[19];
z[3] = -abb[28] * z[0];
z[4] = abb[9] * z[2];
z[6] = abb[6] * z[15];
z[9] = z[4] + z[6];
z[13] = 3 * abb[25];
z[15] = abb[23] + z[13];
z[16] = 3 * abb[24];
z[19] = z[15] + -z[16];
z[19] = abb[3] * z[19];
z[20] = abb[23] + abb[25];
z[21] = abb[24] + z[20];
z[21] = abb[2] * z[21];
z[21] = -z[19] + z[21];
z[22] = abb[22] * (T(1) / T(2));
z[1] = abb[21] * (T(1) / T(2)) + -z[1] + z[22];
z[23] = abb[20] * (T(1) / T(2));
z[24] = z[1] + z[23];
z[24] = abb[8] * z[24];
z[25] = 4 * abb[26];
z[26] = -abb[19] + z[25];
z[27] = abb[20] * (T(3) / T(2)) + abb[21] * (T(5) / T(2)) + z[22] + -z[26];
z[27] = abb[7] * z[27];
z[28] = -abb[24] + abb[25];
z[29] = abb[1] * z[28];
z[30] = 3 * z[29];
z[31] = -abb[23] + z[28];
z[31] = abb[0] * z[31];
z[32] = abb[4] * z[20];
z[24] = z[9] + (T(-1) / T(2)) * z[21] + z[24] + z[27] + z[30] + -z[31] + z[32];
z[24] = abb[11] * z[24];
z[25] = -abb[21] + -abb[22] + z[25];
z[27] = abb[19] * (T(1) / T(2));
z[23] = -z[23] + z[25] + -z[27];
z[23] = abb[7] * z[23];
z[33] = z[9] + 6 * z[29];
z[18] = (T(1) / T(2)) * z[18];
z[34] = -abb[25] + z[18];
z[34] = abb[0] * z[34];
z[35] = 2 * z[32];
z[34] = z[33] + z[34] + -z[35];
z[5] = abb[8] * z[5];
z[11] = abb[2] * z[11];
z[5] = -z[5] + z[11] + z[19] + z[23] + z[34];
z[5] = abb[14] * z[5];
z[11] = -abb[19] + abb[20];
z[19] = abb[7] * (T(1) / T(2));
z[11] = z[11] * z[19];
z[19] = abb[21] + -abb[26];
z[8] = abb[19] * (T(5) / T(4)) + z[8] + 3 * z[19];
z[8] = abb[8] * z[8];
z[18] = abb[25] + z[18];
z[18] = abb[2] * z[18];
z[8] = z[8] + z[18];
z[18] = 4 * abb[25] + -z[16];
z[18] = abb[3] * z[18];
z[11] = -z[8] + -z[11] + z[18] + z[34];
z[11] = abb[13] * z[11];
z[5] = -z[5] + z[11];
z[11] = z[5] + z[24];
z[11] = abb[11] * z[11];
z[18] = abb[8] * z[2];
z[18] = z[18] + z[31];
z[19] = abb[2] * z[20];
z[19] = -z[9] + z[19];
z[24] = 3 * z[28];
z[24] = abb[3] * z[24];
z[28] = 5 * z[29];
z[24] = -z[18] + -z[19] + z[24] + z[28] + -z[32];
z[34] = -abb[11] + abb[14];
z[34] = z[24] * z[34];
z[2] = abb[7] * z[2];
z[36] = abb[2] * abb[23];
z[20] = -abb[3] * z[20];
z[20] = -z[2] + z[20] + -z[29] + z[32] + -z[36];
z[20] = abb[13] * z[20];
z[20] = z[20] + z[34];
z[15] = -z[14] + z[15];
z[15] = abb[3] * z[15];
z[34] = abb[23] + abb[24];
z[37] = 2 * z[34];
z[38] = abb[0] * z[37];
z[15] = z[2] + z[15] + z[30] + -3 * z[32] + -z[36] + z[38];
z[15] = abb[12] * z[15];
z[15] = z[15] + 2 * z[20];
z[15] = abb[12] * z[15];
z[20] = abb[23] * (T(1) / T(2));
z[38] = abb[24] + abb[25] * (T(-5) / T(2)) + -z[20];
z[38] = abb[3] * z[38];
z[28] = -2 * z[6] + -z[28] + z[38];
z[38] = -abb[19] + 8 * abb[26];
z[38] = abb[21] * (T(-7) / T(6)) + abb[20] * (T(-5) / T(6)) + -z[22] + (T(1) / T(3)) * z[38];
z[38] = abb[8] * z[38];
z[39] = -abb[19] + 5 * abb[26];
z[39] = abb[21] * (T(-11) / T(6)) + abb[20] * (T(-7) / T(6)) + -z[22] + (T(2) / T(3)) * z[39];
z[39] = abb[7] * z[39];
z[17] = abb[23] * (T(1) / T(3)) + abb[25] * (T(1) / T(6)) + z[17];
z[17] = abb[2] * z[17];
z[34] = abb[25] * (T(2) / T(3)) + (T(-1) / T(2)) * z[34];
z[34] = abb[0] * z[34];
z[17] = (T(-2) / T(3)) * z[4] + z[17] + (T(1) / T(3)) * z[28] + -z[32] + z[34] + z[38] + z[39];
z[17] = prod_pow(m1_set::bc<T>[0], 2) * z[17];
z[16] = abb[23] + -z[12] + z[16];
z[16] = abb[3] * z[16];
z[28] = 4 * z[29];
z[32] = 2 * abb[23] + abb[25];
z[32] = abb[2] * z[32];
z[9] = z[2] + -z[9] + z[16] + z[18] + -z[28] + z[32];
z[9] = abb[15] * z[9];
z[14] = abb[23] + z[12] + -z[14];
z[14] = abb[3] * z[14];
z[16] = -abb[25] + z[37];
z[16] = abb[0] * z[16];
z[2] = z[2] + z[14] + z[16] + -z[19] + z[28];
z[2] = 2 * z[2];
z[14] = -abb[27] * z[2];
z[7] = abb[21] * (T(-3) / T(2)) + abb[20] * (T(-3) / T(4)) + -3 * z[7] + -z[22];
z[7] = abb[8] * z[7];
z[16] = abb[25] * (T(3) / T(2));
z[18] = abb[23] + abb[24] * (T(9) / T(4)) + -z[16];
z[18] = abb[2] * z[18];
z[16] = -abb[23] + abb[24] * (T(5) / T(4)) + -z[16];
z[16] = abb[0] * z[16];
z[10] = -abb[3] * z[10];
z[7] = 3 * z[7] + z[10] + z[16] + z[18] + z[30];
z[7] = prod_pow(abb[14], 2) * z[7];
z[10] = abb[23] + abb[24] * (T(-5) / T(2));
z[10] = (T(1) / T(2)) * z[10] + z[12];
z[10] = abb[0] * z[10];
z[12] = abb[23] + 4 * abb[24] + -z[13];
z[12] = abb[3] * z[12];
z[8] = z[8] + z[10] + z[12] + z[23] + -z[33];
z[8] = abb[14] * z[8];
z[10] = abb[0] + -abb[3];
z[12] = -abb[24] + abb[25] * (T(1) / T(2)) + -z[20];
z[10] = z[10] * z[12];
z[1] = -z[1] + -z[27];
z[1] = abb[7] * z[1];
z[1] = z[1] + z[10] + -z[29] + z[36];
z[1] = abb[13] * z[1];
z[1] = z[1] + z[8];
z[1] = abb[13] * z[1];
z[1] = abb[29] + z[1] + z[3] + z[7] + 2 * z[9] + z[11] + z[14] + z[15] + z[17];
z[0] = -abb[17] * z[0];
z[3] = -z[4] + -z[6] + -z[30] + z[31];
z[4] = -abb[20] + z[25];
z[4] = abb[8] * z[4];
z[6] = -3 * abb[20] + -5 * abb[21] + -abb[22] + 2 * z[26];
z[6] = abb[7] * z[6];
z[3] = 2 * z[3] + z[4] + z[6] + z[21] + -z[35];
z[3] = abb[11] * z[3];
z[4] = abb[12] * z[24];
z[3] = z[3] + 2 * z[4] + -z[5];
z[3] = m1_set::bc<T>[0] * z[3];
z[2] = -abb[16] * z[2];
z[0] = abb[18] + z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_522_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("6.010479646775139612510559319446561556854164353811704578748747264"),stof<T>("5.6984702373224524557827481551823536381457454900028397604110836716")}, std::complex<T>{stof<T>("0.4760849195553405649970868157906801028490783896627466421742433117"),stof<T>("13.1302645057531669016031843075091655348174583740653546961645784174")}, std::complex<T>{stof<T>("6.486564566330480177507646135237241659703242743474451220922990576"),stof<T>("18.828734743075619357385932462691519172963203864068194456575662089")}, std::complex<T>{stof<T>("-3.7750428937635931319690940113069686134663500614979661092813990745"),stof<T>("6.147904513184628857106431949762537542394915181439860528724860466")}, std::complex<T>{stof<T>("-15.261238408254840353349329045448463772647232007235391470231262426"),stof<T>("11.674492254097347520326813678288346820037131762306513476022259354")}, std::complex<T>{stof<T>("-16.817212403773655425461292445421290644357567786312002481339098589"),stof<T>("5.717383165172064088655906324503324244022838995635456454029552053")}, std::complex<T>{stof<T>("-10.9958162173831464797979925367896019898430508081727037042133300232"),stof<T>("-6.4663568061930289117177549575834310708879088800277819997275569531")}, std::complex<T>{stof<T>("4.838564114960299218399635898683664180695807441019447106921206648"),stof<T>("-37.272448282629505928705228311979131800147949408387776042750243487")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real()), rlog(k.W[195].real()/kbase.W[195].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_522_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_522_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (v[2] + v[3]) * (8 + 4 * v[0] + 2 * v[1] + -v[2] + -3 * v[3] + -2 * v[4] + 3 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = (-3 * m1_set::bc<T>[1] * (v[2] + v[3])) / tend;


		return (abb[23] + abb[24] + -abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[4];
z[0] = -abb[11] + abb[14];
z[1] = abb[23] + abb[24] + -abb[25];
z[0] = z[0] * z[1];
z[2] = abb[13] * z[1];
z[3] = -2 * z[0] + -z[2];
z[3] = abb[13] * z[3];
z[0] = z[0] + -z[2];
z[1] = abb[12] * z[1];
z[0] = 2 * z[0] + 3 * z[1];
z[0] = abb[12] * z[0];
z[0] = z[0] + z[3];
return abb[5] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_522_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[23] + abb[24] + -abb[25]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = abb[12] + -abb[13];
z[1] = abb[23] + abb[24] + -abb[25];
return 2 * abb[5] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_522_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-34.70268143613298085154672409662954861069520625988234795660829406"),stof<T>("-9.261224572104762345520056696232221396711615866973965193257661264")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_13(k), f_2_4_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), rlog(kend.W[195].real()/k.W[195].real()), f_2_4_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_522_W_19_Im(t, path, abb);
abb[29] = SpDLog_f_4_522_W_19_Re(t, path, abb);

                    
            return f_4_522_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_522_DLogXconstant_part(base_point<T>, kend);
	value += f_4_522_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_522_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_522_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_522_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_522_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_522_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_522_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
