/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_167.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_167_abbreviated (const std::array<T,54>& abb) {
T z[105];
z[0] = abb[8] * (T(1) / T(2));
z[1] = abb[4] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[1] + abb[14];
z[4] = abb[7] * (T(1) / T(2));
z[3] = -abb[2] + z[2] + (T(1) / T(2)) * z[3] + -z[4];
z[3] = abb[27] * z[3];
z[5] = abb[30] + -abb[31];
z[6] = abb[29] + -abb[32];
z[7] = 2 * z[6];
z[8] = abb[28] + z[5] + z[7];
z[8] = abb[14] * z[8];
z[9] = abb[4] + abb[8];
z[10] = 3 * abb[1];
z[11] = z[9] + z[10];
z[11] = abb[28] * z[11];
z[12] = 3 * abb[10];
z[13] = -abb[8] + z[12];
z[13] = z[6] * z[13];
z[14] = abb[1] + abb[2];
z[15] = z[5] * z[14];
z[16] = -abb[28] + z[6];
z[17] = abb[7] * z[16];
z[18] = abb[4] * z[6];
z[3] = -z[3] + -z[8] + z[11] + z[13] + z[15] + z[17] + -z[18];
z[3] = abb[27] * z[3];
z[2] = abb[1] + z[2];
z[8] = 3 * abb[28];
z[2] = z[2] * z[8];
z[11] = abb[4] + z[10];
z[11] = abb[30] * z[11];
z[13] = abb[4] * abb[31];
z[15] = abb[8] * z[5];
z[2] = -z[2] + -z[11] + z[13] + -z[15];
z[2] = abb[28] * z[2];
z[11] = abb[28] * (T(3) / T(2)) + z[5];
z[11] = abb[28] * z[11];
z[13] = abb[31] * z[6];
z[15] = 2 * abb[37];
z[17] = abb[30] * z[6];
z[11] = z[11] + z[13] + -z[15] + -z[17];
z[11] = abb[14] * z[11];
z[13] = 3 * abb[12];
z[19] = z[13] + -z[14];
z[19] = -abb[4] + (T(1) / T(2)) * z[19];
z[19] = abb[30] * z[19];
z[13] = -abb[1] + 2 * abb[2] + abb[4] + -z[13];
z[13] = abb[31] * z[13];
z[19] = -z[13] + -2 * z[18] + z[19];
z[19] = abb[30] * z[19];
z[20] = 2 * abb[31];
z[21] = abb[30] * (T(1) / T(2));
z[22] = z[6] + z[20] + z[21];
z[22] = abb[30] * z[22];
z[23] = abb[31] + z[6];
z[24] = z[20] * z[23];
z[25] = abb[30] + z[20];
z[26] = abb[28] * z[25];
z[22] = -abb[37] + z[22] + z[24] + -z[26];
z[22] = abb[7] * z[22];
z[24] = abb[30] + abb[31];
z[7] = z[7] + z[24];
z[7] = abb[30] * z[7];
z[23] = abb[31] * z[23];
z[7] = abb[37] + z[7] + z[23];
z[7] = abb[8] * z[7];
z[23] = abb[12] * abb[32];
z[26] = abb[2] * abb[32];
z[23] = z[23] + -z[26];
z[18] = z[18] + 3 * z[23];
z[23] = -abb[4] + z[14];
z[23] = abb[31] * z[23];
z[23] = -z[18] + z[23];
z[23] = abb[31] * z[23];
z[27] = prod_pow(abb[32], 2);
z[28] = abb[12] * z[27];
z[29] = abb[2] * z[27];
z[28] = z[28] + -z[29];
z[30] = -abb[10] + abb[14];
z[31] = -abb[5] + z[9];
z[32] = z[30] + z[31];
z[33] = 3 * abb[36];
z[32] = z[32] * z[33];
z[34] = prod_pow(abb[29], 2);
z[35] = -z[27] + z[34];
z[36] = (T(1) / T(2)) * z[35];
z[17] = -z[17] + z[36];
z[37] = 3 * abb[5];
z[17] = z[17] * z[37];
z[38] = abb[37] + z[36];
z[39] = z[12] * z[38];
z[40] = abb[4] * abb[37];
z[2] = z[2] + z[3] + -z[7] + z[11] + -z[17] + z[19] + -z[22] + z[23] + (T(-3) / T(2)) * z[28] + z[32] + z[39] + -z[40];
z[3] = abb[43] + abb[46];
z[2] = -z[2] * z[3];
z[7] = 2 * abb[12] + abb[2] * (T(-10) / T(3)) + abb[7] * (T(1) / T(6)) + abb[1] * (T(7) / T(6)) + (T(4) / T(3)) * z[9];
z[7] = -z[3] * z[7];
z[11] = abb[13] * abb[41];
z[17] = abb[45] + abb[49];
z[19] = -abb[44] + z[17];
z[22] = abb[13] * z[19];
z[23] = -z[11] + (T(1) / T(2)) * z[22];
z[28] = -abb[42] + abb[48];
z[32] = abb[44] + -abb[49];
z[39] = abb[45] + z[32];
z[40] = (T(1) / T(2)) * z[39];
z[41] = -z[28] + z[40];
z[42] = abb[15] * z[41];
z[43] = abb[45] * (T(1) / T(3));
z[44] = -13 * abb[47] + abb[49] * (T(19) / T(3)) + -7 * z[28] + z[43];
z[44] = abb[2] * z[44];
z[45] = abb[48] + z[32];
z[45] = abb[41] * (T(-7) / T(6)) + abb[42] * (T(5) / T(6)) + z[43] + (T(1) / T(2)) * z[45];
z[45] = abb[4] * z[45];
z[46] = abb[44] + abb[49];
z[43] = abb[42] * (T(1) / T(3)) + z[43] + z[46];
z[43] = -abb[48] + abb[41] * (T(-5) / T(6)) + (T(1) / T(2)) * z[43];
z[43] = abb[7] * z[43];
z[47] = -abb[20] + -abb[22] + -abb[24] + -abb[25];
z[48] = abb[21] + abb[23];
z[47] = (T(1) / T(2)) * z[47] + -z[48];
z[47] = abb[50] * z[47];
z[49] = abb[41] + -abb[45];
z[50] = abb[11] * z[49];
z[51] = -abb[44] + abb[45] + -z[28];
z[51] = abb[12] * z[51];
z[52] = -abb[41] + (T(1) / T(3)) * z[17];
z[52] = abb[1] * z[52];
z[53] = 3 * abb[44];
z[54] = abb[49] * (T(-13) / T(6)) + abb[45] * (T(5) / T(6)) + abb[47] * (T(13) / T(2)) + -z[53];
z[54] = abb[8] * z[54];
z[55] = abb[41] + -abb[49];
z[56] = abb[42] + z[55];
z[57] = abb[0] * z[56];
z[7] = z[7] + z[23] + z[42] + z[43] + (T(1) / T(2)) * z[44] + z[45] + z[47] + 3 * z[50] + -2 * z[51] + (T(7) / T(2)) * z[52] + z[54] + (T(13) / T(6)) * z[57];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[43] = 3 * abb[48];
z[44] = 3 * abb[42] + -z[43];
z[45] = 3 * abb[47];
z[47] = -z[17] + -z[44] + z[45];
z[47] = abb[2] * z[47];
z[52] = 3 * abb[41];
z[54] = -z[17] + z[52];
z[57] = abb[1] * z[54];
z[47] = 3 * z[23] + z[47] + z[57];
z[58] = abb[49] * (T(1) / T(2));
z[59] = abb[48] * (T(3) / T(2));
z[60] = abb[42] * (T(-1) / T(2)) + z[49] + -z[58] + z[59];
z[60] = abb[7] * z[60];
z[61] = abb[45] * (T(1) / T(2));
z[62] = abb[47] + (T(-1) / T(2)) * z[46] + z[61];
z[63] = abb[3] * z[62];
z[64] = (T(3) / T(2)) * z[42];
z[65] = abb[41] * (T(1) / T(2));
z[66] = -abb[42] + z[65];
z[67] = abb[45] + -abb[49];
z[68] = z[66] + (T(-1) / T(2)) * z[67];
z[68] = abb[4] * z[68];
z[69] = -z[17] + z[53];
z[70] = abb[8] * z[69];
z[71] = abb[49] + abb[47] * (T(-3) / T(2)) + -z[61];
z[71] = abb[14] * z[71];
z[47] = (T(1) / T(2)) * z[47] + z[60] + (T(-3) / T(2)) * z[63] + z[64] + z[68] + (T(-1) / T(4)) * z[70] + z[71];
z[47] = abb[27] * z[47];
z[60] = 2 * abb[45];
z[68] = -abb[49] + z[60];
z[44] = z[44] + z[68];
z[70] = abb[2] * z[44];
z[71] = z[64] + -z[70];
z[72] = 3 * abb[45];
z[73] = z[53] + z[72];
z[74] = abb[49] + z[73];
z[75] = -abb[42] + z[43];
z[74] = (T(1) / T(2)) * z[74] + -z[75];
z[74] = -abb[41] + (T(1) / T(2)) * z[74];
z[74] = abb[4] * z[74];
z[76] = (T(3) / T(2)) * z[23];
z[74] = -z[57] + z[71] + z[74] + -z[76];
z[74] = z[5] * z[74];
z[77] = 2 * abb[42];
z[78] = -z[59] + z[65] + z[77];
z[79] = 5 * abb[49];
z[73] = z[73] + -z[79];
z[73] = (T(1) / T(4)) * z[73] + z[78];
z[73] = -z[5] * z[73];
z[44] = abb[32] * z[44];
z[80] = (T(1) / T(2)) * z[69];
z[81] = abb[28] * z[80];
z[82] = abb[29] * z[80];
z[73] = z[44] + z[73] + -z[81] + z[82];
z[73] = abb[7] * z[73];
z[82] = abb[31] * z[80];
z[83] = z[21] * z[69];
z[84] = z[82] + -z[83];
z[85] = z[53] + -z[79];
z[85] = z[45] + z[61] + (T(1) / T(2)) * z[85];
z[85] = abb[32] * z[85];
z[86] = -abb[45] + 2 * abb[49];
z[45] = -z[45] + z[86];
z[87] = abb[29] * z[45];
z[85] = -z[81] + z[84] + z[85] + 2 * z[87];
z[85] = abb[14] * z[85];
z[67] = -abb[47] + z[28] + -z[67];
z[87] = z[26] * z[67];
z[88] = abb[29] * z[63];
z[87] = z[87] + -z[88];
z[88] = abb[32] * z[42];
z[88] = 3 * z[88];
z[89] = z[6] * z[69];
z[90] = z[1] * z[89];
z[91] = z[88] + z[90];
z[92] = z[23] + z[57];
z[54] = abb[4] * z[54];
z[54] = z[54] + 3 * z[92];
z[92] = abb[8] * z[80];
z[93] = -z[54] + z[92];
z[93] = abb[28] * z[93];
z[94] = abb[47] + z[32];
z[95] = z[6] * z[94];
z[96] = z[12] * z[95];
z[0] = -z[0] * z[89];
z[0] = z[0] + z[47] + z[73] + z[74] + z[85] + -3 * z[87] + -z[91] + z[93] + z[96];
z[0] = abb[27] * z[0];
z[47] = abb[2] * z[67];
z[67] = -abb[7] * z[41];
z[73] = abb[14] * z[62];
z[47] = z[42] + z[47] + -z[63] + z[67] + z[73];
z[47] = abb[33] * z[47];
z[67] = abb[20] + z[48];
z[73] = abb[50] * (T(1) / T(2));
z[67] = z[67] * z[73];
z[74] = -abb[1] + abb[7];
z[74] = z[3] * z[74];
z[72] = -z[46] + z[72];
z[4] = z[4] * z[72];
z[85] = abb[6] * z[39];
z[87] = z[22] + z[85];
z[87] = -z[11] + (T(1) / T(2)) * z[87];
z[19] = -abb[41] + (T(1) / T(2)) * z[19];
z[93] = abb[4] * z[19];
z[93] = -z[57] + z[93];
z[96] = abb[8] * z[40];
z[4] = z[4] + z[67] + z[74] + -z[87] + z[93] + z[96];
z[67] = abb[51] * z[4];
z[74] = -abb[44] + 3 * abb[49];
z[96] = 2 * abb[47];
z[97] = z[61] + (T(-1) / T(2)) * z[74] + z[96];
z[97] = abb[14] * z[97];
z[98] = abb[10] * z[94];
z[63] = -z[63] + z[97] + -z[98];
z[30] = -abb[1] + z[30];
z[30] = -z[3] * z[30];
z[23] = z[23] + z[30] + z[63] + -z[93];
z[23] = abb[34] * z[23];
z[30] = z[27] * z[51];
z[93] = z[34] * z[40];
z[39] = abb[35] * z[39];
z[93] = z[39] + z[93];
z[93] = abb[6] * z[93];
z[30] = z[30] + z[93];
z[93] = (T(1) / T(2)) * z[34];
z[97] = abb[37] + -z[93];
z[97] = z[62] * z[97];
z[98] = abb[35] * z[62];
z[97] = z[97] + -z[98];
z[97] = abb[3] * z[97];
z[99] = -abb[45] + abb[47];
z[99] = z[28] + (T(1) / T(2)) * z[99];
z[29] = z[29] * z[99];
z[99] = prod_pow(abb[28], 2);
z[100] = prod_pow(abb[27], 2);
z[100] = -z[99] + z[100];
z[55] = abb[47] + z[55];
z[55] = abb[9] * z[55];
z[100] = z[55] * z[100];
z[23] = z[23] + z[29] + (T(1) / T(2)) * z[30] + z[47] + -z[67] + z[97] + z[100];
z[29] = abb[27] * (T(1) / T(2));
z[30] = z[16] + z[29];
z[30] = abb[27] * z[30];
z[47] = abb[30] * abb[31];
z[67] = abb[31] * abb[32];
z[97] = -abb[36] + z[67];
z[93] = abb[35] + z[93];
z[100] = abb[28] * abb[30];
z[30] = abb[37] + z[30] + -z[47] + -z[93] + z[97] + z[100];
z[30] = -z[30] * z[48];
z[47] = abb[31] * (T(1) / T(2));
z[101] = -z[21] + z[47];
z[102] = -abb[32] + z[29] + -z[101];
z[102] = abb[27] * z[102];
z[102] = abb[33] + z[102];
z[103] = -z[6] + z[47];
z[103] = abb[30] * z[103];
z[36] = z[36] + -z[97] + -z[102] + z[103];
z[36] = abb[24] * z[36];
z[29] = -abb[28] + (T(-1) / T(2)) * z[5] + z[29];
z[29] = abb[27] * z[29];
z[97] = abb[31] * z[21];
z[29] = z[29] + -z[97];
z[103] = -abb[29] * abb[31];
z[104] = abb[28] * (T(-1) / T(2)) + -z[5];
z[104] = abb[28] * z[104];
z[93] = -z[29] + z[93] + z[103] + z[104];
z[93] = abb[22] * z[93];
z[67] = -z[67] + z[97] + -z[102];
z[67] = abb[25] * z[67];
z[29] = -z[29] + -z[100];
z[29] = abb[20] * z[29];
z[97] = abb[26] * abb[53];
z[100] = -abb[20] + -abb[22];
z[100] = abb[34] * z[100];
z[29] = z[29] + z[30] + z[36] + z[67] + z[93] + (T(-1) / T(2)) * z[97] + z[100];
z[30] = abb[50] * (T(3) / T(2));
z[29] = z[29] * z[30];
z[36] = abb[5] * z[80];
z[67] = abb[4] * z[80];
z[36] = z[36] + -z[67] + -z[92];
z[72] = 2 * abb[48] + (T(-1) / T(2)) * z[72] + -z[77];
z[72] = abb[7] * z[72];
z[77] = abb[25] + z[48];
z[77] = z[73] * z[77];
z[31] = abb[2] + abb[7] + -abb[12] + z[31];
z[31] = z[3] * z[31];
z[92] = z[51] + -z[70];
z[31] = -z[31] + -z[36] + z[42] + z[72] + -z[77] + z[92];
z[31] = 3 * z[31];
z[42] = abb[52] * z[31];
z[11] = (T(-1) / T(2)) * z[11] + (T(1) / T(4)) * z[22] + z[51];
z[22] = 5 * abb[45];
z[32] = 3 * z[32];
z[72] = z[22] + -z[32];
z[72] = (T(1) / T(2)) * z[72] + -z[75];
z[72] = -abb[41] + (T(1) / T(2)) * z[72];
z[72] = abb[4] * z[72];
z[11] = -3 * z[11] + -z[57] + -z[64] + 2 * z[70] + z[72];
z[11] = abb[31] * z[11];
z[64] = -3 * z[51] + z[57];
z[59] = -abb[45] + z[59];
z[72] = abb[42] * (T(3) / T(2)) + -z[58] + -z[59];
z[72] = abb[2] * z[72];
z[77] = abb[41] + abb[45] + -z[75];
z[1] = z[1] * z[77];
z[1] = z[1] + (T(1) / T(2)) * z[64] + z[72];
z[1] = abb[30] * z[1];
z[64] = -abb[4] * z[89];
z[1] = z[1] + z[11] + z[64];
z[1] = abb[30] * z[1];
z[64] = -z[27] * z[41];
z[39] = -z[39] + z[64];
z[64] = abb[37] * z[69];
z[39] = 3 * z[39] + z[64];
z[64] = abb[29] * z[68];
z[60] = -abb[41] + -abb[42] + z[60];
z[60] = abb[31] * z[60];
z[44] = -z[44] + z[60] + z[64];
z[44] = z[20] * z[44];
z[46] = 3 * z[46];
z[60] = 13 * abb[45] + -z[46];
z[60] = 4 * abb[42] + abb[48] * (T(-9) / T(2)) + (T(1) / T(4)) * z[60] + -z[65];
z[60] = abb[31] * z[60];
z[60] = z[60] + (T(-1) / T(2)) * z[89];
z[59] = -z[59] + -z[66];
z[59] = abb[30] * z[59];
z[59] = z[59] + z[60];
z[59] = abb[30] * z[59];
z[20] = -z[20] * z[68];
z[20] = z[20] + z[83];
z[20] = abb[28] * z[20];
z[20] = z[20] + (T(1) / T(2)) * z[39] + z[44] + z[59];
z[20] = abb[7] * z[20];
z[39] = abb[42] + abb[45];
z[39] = -abb[41] + 2 * z[39] + -z[43];
z[39] = abb[4] * z[39];
z[39] = z[39] + -z[57] + -z[70];
z[39] = abb[31] * z[39];
z[43] = abb[32] * z[92];
z[40] = abb[6] * abb[29] * z[40];
z[43] = -z[40] + z[43];
z[39] = z[39] + 3 * z[43] + z[88] + -z[90];
z[39] = abb[31] * z[39];
z[43] = -z[82] + -z[83] + -z[89];
z[43] = abb[30] * z[43];
z[44] = abb[47] + abb[45] * (T(1) / T(4));
z[59] = abb[49] + z[53];
z[59] = -z[44] + (T(1) / T(4)) * z[59];
z[34] = z[34] * z[59];
z[34] = z[34] + -z[98];
z[59] = abb[32] * z[80];
z[64] = abb[29] + abb[31];
z[64] = z[64] * z[68];
z[64] = z[59] + z[64];
z[64] = abb[31] * z[64];
z[65] = -abb[37] * z[80];
z[34] = 3 * z[34] + z[43] + z[64] + z[65];
z[34] = abb[8] * z[34];
z[43] = abb[44] + z[17];
z[64] = abb[41] + (T(-1) / T(4)) * z[43];
z[64] = abb[4] * z[64];
z[58] = -abb[47] + z[58];
z[58] = abb[8] * z[58];
z[58] = z[57] + z[58] + z[64] + (T(-1) / T(4)) * z[85];
z[58] = z[8] * z[58];
z[54] = abb[30] * z[54];
z[64] = abb[4] * z[69];
z[64] = z[64] + 3 * z[85];
z[64] = z[47] * z[64];
z[65] = -abb[31] * z[68];
z[65] = z[65] + -z[83];
z[65] = abb[8] * z[65];
z[54] = z[54] + z[58] + z[64] + z[65];
z[54] = abb[28] * z[54];
z[58] = z[89] * z[101];
z[27] = z[27] * z[62];
z[27] = (T(1) / T(2)) * z[27] + z[98];
z[62] = z[5] * z[69];
z[44] = z[44] + (T(-1) / T(4)) * z[74];
z[8] = z[8] * z[44];
z[8] = z[8] + (T(1) / T(2)) * z[62];
z[8] = abb[28] * z[8];
z[15] = z[15] * z[45];
z[8] = z[8] + z[15] + 3 * z[27] + z[58];
z[8] = abb[14] * z[8];
z[15] = -z[36] + z[63];
z[15] = z[15] * z[33];
z[27] = abb[31] * z[56];
z[33] = abb[30] * z[56];
z[36] = z[27] + -z[33];
z[36] = (T(1) / T(2)) * z[36];
z[44] = 2 * abb[27];
z[58] = -z[44] * z[56];
z[58] = z[36] + z[58];
z[58] = abb[27] * z[58];
z[62] = prod_pow(abb[31], 2);
z[63] = z[56] * z[62];
z[27] = -z[27] + -z[33];
z[27] = z[21] * z[27];
z[27] = z[27] + z[58] + z[63];
z[27] = abb[0] * z[27];
z[12] = z[12] * z[38] * z[94];
z[33] = -z[35] * z[69];
z[35] = abb[30] * z[89];
z[33] = (T(1) / T(2)) * z[33] + z[35];
z[35] = abb[5] * (T(3) / T(2));
z[33] = z[33] * z[35];
z[38] = z[62] + -z[99];
z[58] = 3 * abb[11];
z[38] = z[38] * z[49] * z[58];
z[49] = abb[17] + abb[19];
z[41] = z[41] * z[49];
z[43] = abb[41] + abb[48] + (T(-1) / T(2)) * z[43];
z[43] = abb[16] * z[43];
z[19] = abb[18] * z[19];
z[19] = z[19] + z[41] + -z[43];
z[41] = abb[53] * z[19];
z[43] = -abb[37] * z[67];
z[0] = z[0] + z[1] + z[2] + z[7] + z[8] + z[12] + z[15] + z[20] + 3 * z[23] + z[27] + z[29] + z[33] + z[34] + z[38] + z[39] + (T(3) / T(2)) * z[41] + z[42] + z[43] + z[54];
z[1] = 6 * abb[10];
z[2] = -z[1] + z[37];
z[2] = z[2] * z[6];
z[7] = 2 * z[9] + z[10];
z[7] = abb[28] * z[7];
z[5] = z[5] + z[6];
z[5] = abb[8] * z[5];
z[6] = z[16] + z[25];
z[6] = abb[7] * z[6];
z[8] = -abb[14] + z[14];
z[8] = z[8] * z[44];
z[9] = 2 * abb[14];
z[10] = z[9] * z[16];
z[12] = 2 * abb[1] + -abb[2] + abb[4];
z[12] = abb[30] * z[12];
z[2] = z[2] + z[5] + -z[6] + z[7] + -z[8] + z[10] + z[12] + -z[13] + z[18];
z[2] = -z[2] * z[3];
z[3] = abb[45] + -z[32];
z[3] = (T(1) / T(2)) * z[3] + z[75];
z[3] = -2 * abb[41] + (T(1) / T(2)) * z[3];
z[3] = abb[4] * z[3];
z[3] = z[3] + -2 * z[57] + -z[71] + -z[76];
z[3] = abb[30] * z[3];
z[5] = -z[57] + z[87];
z[6] = -z[53] + -z[79];
z[7] = 6 * abb[47];
z[6] = (T(1) / T(2)) * z[6] + z[7] + z[61];
z[6] = abb[8] * z[6];
z[8] = z[17] + z[53];
z[8] = (T(1) / T(2)) * z[8] + -z[52];
z[8] = abb[4] * z[8];
z[5] = 3 * z[5] + z[6] + z[8] + 6 * z[50];
z[5] = abb[28] * z[5];
z[6] = z[7] + -z[53] + -z[86];
z[6] = abb[29] * z[6];
z[6] = z[6] + -z[59] + -z[84];
z[6] = abb[8] * z[6];
z[7] = z[22] + -z[46];
z[7] = (T(1) / T(4)) * z[7] + z[78];
z[7] = abb[30] * z[7];
z[7] = z[7] + z[60] + z[81];
z[7] = abb[7] * z[7];
z[8] = abb[4] + -abb[7];
z[8] = z[8] * z[56];
z[10] = abb[2] + -abb[14];
z[10] = z[10] * z[45];
z[8] = z[8] + z[10] + z[57];
z[8] = z[8] * z[44];
z[10] = abb[28] + abb[32];
z[12] = z[10] + -z[24];
z[12] = -z[12] * z[48];
z[13] = z[21] + z[47];
z[14] = -abb[29] + z[13];
z[15] = abb[22] + abb[24];
z[14] = z[14] * z[15];
z[13] = -abb[32] + z[13];
z[13] = abb[25] * z[13];
z[15] = -abb[28] + (T(1) / T(2)) * z[24];
z[15] = abb[20] * z[15];
z[12] = z[12] + z[13] + z[14] + z[15];
z[12] = z[12] * z[30];
z[13] = abb[49] + -z[28] + -z[96];
z[13] = z[13] * z[26];
z[14] = -abb[32] * z[51];
z[13] = z[13] + z[14] + -z[40];
z[10] = -abb[29] + z[10];
z[9] = z[9] * z[10] * z[45];
z[1] = -z[1] * z[95];
z[10] = z[35] * z[89];
z[14] = abb[27] * z[56];
z[14] = 4 * z[14] + -z[36];
z[14] = abb[0] * z[14];
z[15] = -abb[27] + abb[28];
z[15] = z[15] * z[55];
z[1] = z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + 3 * z[13] + z[14] + 6 * z[15] + z[91];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[39] * z[31];
z[3] = -abb[38] * z[4];
z[4] = -abb[26] * z[73];
z[4] = z[4] + z[19];
z[4] = abb[40] * z[4];
z[3] = z[3] + (T(1) / T(2)) * z[4];
z[1] = z[1] + z[2] + 3 * z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_167_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-29.812321165349265736397436937642014984677930848764465516646227806"),stof<T>("21.120501999199564205866697115437052890912296680443702455469877917")}, std::complex<T>{stof<T>("15.450078917274434293101657209467872430583744502505932285414849113"),stof<T>("-53.134356214699376742634265603799826706847615954368603431025468477")}, std::complex<T>{stof<T>("-17.615507745935394102840818345096835064136201461388195528260961886"),stof<T>("8.584324298882360639391283163367212198296924741406416600296859453")}, std::complex<T>{stof<T>("-7.687120263558075704086300984559788078432254748391678327741697953"),stof<T>("-14.488192071346316483026338377651072600386143246861793296830825326")}, std::complex<T>{stof<T>("6.649017022093725609138036969297300005090047660717341708608668748"),stof<T>("-13.09577886334928700723304252220086849418291639961518254186002122")}, std::complex<T>{stof<T>("-17.615507745935394102840818345096835064136201461388195528260961886"),stof<T>("8.584324298882360639391283163367212198296924741406416600296859453")}, std::complex<T>{stof<T>("45.329731165513963189616948820715415230753058547167730857428348457"),stof<T>("33.446911998062466965682072841213907965343579306196733408363483264")}, std::complex<T>{stof<T>("-41.024978470615073700210068323774544434220028566552717566617134902"),stof<T>("25.031710826068782179829619406738751342025710693049350032508948328")}, std::complex<T>{stof<T>("5.5477963973201460244185816232478799154516817266589282797765971709"),stof<T>("0.5596011630320834407576285701310278015675444605778966866870027563")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_167_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_167_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("20.169288915866978759965121300858341356942001402230393024982752214"),stof<T>("32.215972030982383726121371183691864231929787980224831705591448018")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,54> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W18(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_167_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_167_DLogXconstant_part(base_point<T>, kend);
	value += f_4_167_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_167_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_167_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_167_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_167_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_167_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_167_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
