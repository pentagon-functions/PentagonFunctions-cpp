/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_666.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_666_abbreviated (const std::array<T,58>& abb) {
T z[80];
z[0] = -abb[29] + abb[30];
z[1] = abb[33] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[34] * (T(1) / T(2));
z[4] = z[2] + -z[3];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = abb[41] * (T(3) / T(2));
z[4] = z[4] + -z[5];
z[4] = abb[2] * z[4];
z[6] = abb[30] * (T(1) / T(2));
z[7] = z[1] + z[6];
z[8] = -abb[34] + z[7];
z[9] = m1_set::bc<T>[0] * z[8];
z[9] = -abb[41] + z[9];
z[10] = abb[17] * z[9];
z[11] = abb[32] + -abb[34];
z[11] = m1_set::bc<T>[0] * z[11];
z[12] = abb[11] * z[11];
z[12] = -z[4] + (T(3) / T(4)) * z[10] + z[12];
z[13] = -abb[29] + abb[31];
z[14] = abb[9] * m1_set::bc<T>[0] * z[13];
z[14] = -z[12] + z[14];
z[15] = abb[30] * (T(1) / T(4));
z[16] = abb[33] * (T(1) / T(4));
z[17] = -abb[29] + z[16];
z[18] = -z[3] + z[15] + z[17];
z[19] = m1_set::bc<T>[0] * z[18];
z[20] = abb[41] * (T(1) / T(2));
z[19] = z[19] + -z[20];
z[21] = abb[5] * (T(1) / T(2));
z[22] = -z[19] * z[21];
z[23] = abb[31] + abb[32];
z[23] = m1_set::bc<T>[0] * z[23];
z[23] = -abb[40] + z[23];
z[24] = abb[14] * (T(1) / T(2));
z[23] = z[23] * z[24];
z[24] = abb[19] + abb[21];
z[25] = abb[42] * (T(3) / T(8));
z[26] = z[24] * z[25];
z[27] = abb[30] + -abb[33];
z[28] = abb[0] * (T(1) / T(8));
z[27] = m1_set::bc<T>[0] * z[27] * z[28];
z[29] = abb[13] * (T(1) / T(2));
z[30] = -abb[33] + 3 * abb[34];
z[30] = m1_set::bc<T>[0] * z[30];
z[30] = abb[41] + z[30];
z[29] = z[29] * z[30];
z[31] = abb[31] + abb[34];
z[32] = -abb[29] + z[31];
z[33] = -abb[32] + z[32];
z[33] = abb[16] * m1_set::bc<T>[0] * z[33];
z[29] = z[29] + z[33];
z[2] = z[2] + z[3];
z[2] = m1_set::bc<T>[0] * z[2];
z[33] = abb[41] * (T(3) / T(4));
z[34] = -abb[40] + (T(1) / T(2)) * z[2] + -z[33];
z[34] = abb[6] * z[34];
z[35] = abb[32] * m1_set::bc<T>[0];
z[35] = -abb[40] + z[35];
z[35] = abb[8] * z[35];
z[36] = -abb[31] + abb[32];
z[37] = m1_set::bc<T>[0] * z[36];
z[38] = -abb[40] + z[37];
z[39] = abb[7] * z[38];
z[22] = z[14] + z[22] + -z[23] + z[26] + -z[27] + -z[29] + z[34] + z[35] + (T(-1) / T(2)) * z[39];
z[22] = abb[51] * z[22];
z[26] = abb[31] * (T(1) / T(2));
z[34] = -abb[29] + abb[33];
z[35] = z[26] + -z[34];
z[39] = -z[6] + z[35];
z[39] = m1_set::bc<T>[0] * z[39];
z[40] = abb[40] * (T(3) / T(2));
z[39] = z[39] + z[40];
z[39] = abb[1] * z[39];
z[41] = -abb[31] + z[7];
z[41] = m1_set::bc<T>[0] * z[41];
z[41] = -abb[40] + z[41];
z[42] = abb[15] * (T(3) / T(4));
z[41] = z[41] * z[42];
z[42] = abb[7] * (T(1) / T(4));
z[38] = z[38] * z[42];
z[37] = abb[12] * z[37];
z[29] = z[29] + z[37];
z[23] = z[23] + z[29] + -z[38] + z[39] + z[41];
z[37] = abb[32] * (T(1) / T(2));
z[38] = z[26] + -z[37];
z[39] = z[15] + -z[17];
z[41] = -z[38] + z[39];
z[41] = m1_set::bc<T>[0] * z[41];
z[41] = abb[40] * (T(-1) / T(2)) + z[41];
z[43] = abb[6] * (T(1) / T(2));
z[41] = z[41] * z[43];
z[44] = -abb[33] + abb[34];
z[45] = abb[30] + -abb[31];
z[46] = z[44] + z[45];
z[46] = abb[32] + (T(1) / T(4)) * z[46];
z[46] = m1_set::bc<T>[0] * z[46];
z[46] = abb[40] * (T(-1) / T(4)) + z[46];
z[46] = abb[8] * z[46];
z[47] = abb[4] + -abb[8];
z[33] = z[33] * z[47];
z[48] = abb[18] + abb[20];
z[49] = z[25] * z[48];
z[11] = abb[4] * z[11];
z[33] = (T(-3) / T(4)) * z[11] + -z[23] + z[27] + z[33] + z[41] + z[46] + z[49];
z[33] = abb[49] * z[33];
z[36] = abb[34] + abb[30] * (T(5) / T(2)) + z[1] + z[36];
z[36] = m1_set::bc<T>[0] * z[36];
z[36] = -abb[40] + -3 * abb[41] + z[36];
z[36] = abb[6] * z[36];
z[41] = -abb[30] + -abb[33] + z[31];
z[46] = -m1_set::bc<T>[0] * z[41];
z[46] = -abb[40] + z[46];
z[46] = abb[8] * z[46];
z[47] = abb[41] * z[47];
z[11] = -z[11] + z[36] + z[46] + z[47];
z[24] = z[24] + z[48];
z[24] = z[24] * z[25];
z[11] = (T(1) / T(4)) * z[11] + -z[12] + -z[23] + z[24];
z[11] = abb[50] * z[11];
z[12] = abb[32] + z[7] + -z[31];
z[23] = abb[23] + abb[25];
z[12] = z[12] * z[23];
z[8] = abb[22] * z[8];
z[24] = abb[24] * z[41];
z[8] = z[8] + z[12] + -z[24];
z[8] = m1_set::bc<T>[0] * z[8];
z[12] = abb[26] * z[9];
z[24] = abb[22] + z[23];
z[36] = abb[24] + z[24];
z[36] = abb[41] * z[36];
z[23] = abb[24] + z[23];
z[23] = abb[40] * z[23];
z[8] = z[8] + -z[12] + -z[23] + -z[36];
z[12] = (T(3) / T(2)) * z[8];
z[12] = abb[47] * z[12];
z[23] = abb[27] * (T(1) / T(2));
z[36] = abb[42] * z[23];
z[8] = z[8] + z[36];
z[36] = abb[43] * z[8];
z[18] = -abb[19] * z[18];
z[41] = -abb[29] + z[1];
z[46] = z[3] + -z[41];
z[46] = abb[18] * z[46];
z[18] = z[18] + z[46];
z[18] = m1_set::bc<T>[0] * z[18];
z[46] = abb[18] + abb[19];
z[20] = z[20] * z[46];
z[46] = abb[6] + abb[0] * (T(3) / T(4));
z[46] = abb[42] * z[46];
z[18] = z[18] + z[20] + z[46];
z[18] = abb[53] * z[18];
z[20] = abb[27] * abb[47];
z[46] = abb[17] * abb[53];
z[20] = 3 * z[20] + z[46];
z[20] = abb[42] * z[20];
z[12] = z[12] + z[18] + (T(1) / T(4)) * z[20] + z[36];
z[18] = abb[44] + abb[45];
z[20] = abb[54] + abb[46] * (T(1) / T(4));
z[36] = (T(3) / T(4)) * z[18] + -z[20];
z[8] = z[8] * z[36];
z[2] = z[2] + -z[5];
z[2] = z[2] * z[43];
z[2] = z[2] + -z[27];
z[5] = abb[19] * abb[42];
z[5] = (T(-1) / T(2)) * z[5] + z[10];
z[10] = abb[13] * z[30];
z[5] = (T(3) / T(2)) * z[5] + z[10];
z[4] = -z[2] + -z[4] + (T(1) / T(2)) * z[5];
z[4] = abb[52] * z[4];
z[5] = abb[19] * z[25];
z[2] = z[2] + z[5] + z[14] + -z[29];
z[2] = abb[48] * z[2];
z[5] = abb[48] + -abb[52];
z[10] = -z[5] * z[19];
z[14] = -abb[29] + abb[34];
z[19] = z[7] + z[14] + z[38];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = -abb[41] + z[19] + -z[40];
z[19] = abb[49] * z[19];
z[7] = abb[32] + z[7] + z[31];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = -3 * abb[40] + abb[41] + z[7];
z[7] = abb[50] * z[7];
z[25] = abb[42] * abb[53];
z[7] = (T(1) / T(2)) * z[7] + z[10] + z[19] + (T(3) / T(4)) * z[25];
z[7] = z[7] * z[21];
z[9] = -abb[53] * z[9];
z[10] = (T(-3) / T(2)) * z[5];
z[19] = -abb[42] * z[10];
z[9] = z[9] + z[19];
z[9] = abb[21] * z[9];
z[19] = abb[49] + abb[52];
z[19] = abb[10] * z[19];
z[14] = m1_set::bc<T>[0] * z[14] * z[19];
z[21] = -abb[28] * z[25];
z[2] = z[2] + z[4] + z[7] + z[8] + (T(1) / T(4)) * z[9] + z[11] + (T(1) / T(2)) * z[12] + z[14] + z[21] + z[22] + z[33];
z[2] = (T(1) / T(8)) * z[2];
z[4] = abb[31] * z[35];
z[7] = abb[35] + abb[37];
z[8] = abb[29] + abb[33];
z[9] = abb[34] * (T(5) / T(2)) + -z[8];
z[9] = abb[34] * z[9];
z[11] = abb[30] * z[32];
z[12] = 3 * abb[38];
z[14] = 3 * abb[36];
z[21] = abb[29] * abb[33];
z[4] = z[4] + 3 * z[7] + z[9] + z[11] + z[12] + z[14] + z[21];
z[7] = abb[32] * (T(1) / T(4));
z[9] = z[7] + -z[26];
z[11] = z[9] + z[39];
z[11] = abb[32] * z[11];
z[4] = -abb[39] + (T(1) / T(4)) * z[4] + -z[11];
z[4] = abb[16] * z[4];
z[11] = 3 * abb[55];
z[14] = z[11] + z[14];
z[22] = abb[33] * z[41];
z[25] = prod_pow(abb[29], 2);
z[27] = (T(1) / T(2)) * z[25];
z[22] = z[14] + z[22] + z[27];
z[29] = -z[34] + z[45];
z[29] = z[29] * z[37];
z[30] = z[34] + z[45];
z[30] = z[6] * z[30];
z[32] = abb[39] * (T(1) / T(2));
z[33] = abb[37] * (T(1) / T(2));
z[35] = z[32] + -z[33];
z[36] = abb[31] * z[34];
z[38] = prod_pow(m1_set::bc<T>[0], 2);
z[22] = (T(-1) / T(2)) * z[22] + z[29] + z[30] + z[35] + -z[36] + (T(-5) / T(12)) * z[38];
z[22] = abb[1] * z[22];
z[29] = -abb[29] + z[37];
z[29] = abb[32] * z[29];
z[39] = abb[37] + z[29];
z[40] = -abb[38] + abb[39];
z[41] = -abb[36] + z[40];
z[46] = abb[35] + z[27] + z[39] + -z[41];
z[46] = abb[3] * z[46];
z[4] = z[4] + z[22] + (T(-3) / T(4)) * z[46];
z[22] = abb[30] * z[44];
z[47] = abb[55] + z[36];
z[49] = -abb[37] + z[47];
z[50] = abb[29] * abb[34];
z[51] = -abb[56] + z[50];
z[22] = z[22] + z[27] + -z[29] + z[40] + z[49] + -z[51];
z[29] = (T(1) / T(3)) * z[38];
z[40] = (T(1) / T(2)) * z[22] + z[29];
z[40] = abb[8] * z[40];
z[52] = -abb[33] + z[37];
z[52] = abb[32] * z[52];
z[53] = -abb[33] + z[3];
z[53] = abb[34] * z[53];
z[53] = -abb[56] + z[53];
z[52] = abb[38] + z[52] + -z[53];
z[52] = abb[4] * z[52];
z[48] = abb[57] * z[48];
z[54] = abb[19] * abb[57];
z[55] = -z[48] + -z[54];
z[40] = z[40] + (T(-1) / T(2)) * z[52] + (T(3) / T(4)) * z[55];
z[55] = z[21] + z[25];
z[56] = (T(1) / T(2)) * z[8];
z[57] = -abb[34] + z[56];
z[58] = abb[30] * z[57];
z[59] = z[51] + z[58];
z[55] = -abb[35] + (T(-1) / T(2)) * z[55] + z[59];
z[60] = -z[38] + 3 * z[55];
z[61] = abb[17] * z[60];
z[62] = (T(1) / T(4)) * z[61];
z[40] = -z[4] + (T(1) / T(2)) * z[40] + z[62];
z[63] = -abb[33] + abb[29] * (T(3) / T(2));
z[63] = z[1] * z[63];
z[64] = abb[35] * (T(3) / T(2));
z[63] = abb[55] * (T(1) / T(2)) + z[25] + z[63] + z[64];
z[65] = -z[7] * z[34];
z[66] = -abb[37] + abb[39] + z[36];
z[67] = (T(1) / T(4)) * z[38];
z[16] = abb[29] + z[16];
z[68] = abb[34] * (T(5) / T(8)) + -z[16];
z[68] = abb[34] * z[68];
z[69] = abb[56] * (T(3) / T(4));
z[70] = 3 * abb[29];
z[71] = -5 * abb[33] + -z[70];
z[71] = abb[34] + (T(1) / T(8)) * z[71];
z[71] = abb[30] * z[71];
z[63] = (T(1) / T(2)) * z[63] + z[65] + (T(1) / T(4)) * z[66] + -z[67] + z[68] + z[69] + z[71];
z[63] = z[43] * z[63];
z[65] = z[21] + -z[25];
z[65] = (T(1) / T(2)) * z[65];
z[66] = z[6] * z[34];
z[47] = abb[36] + z[47] + -z[65] + -z[66];
z[47] = z[38] + 3 * z[47];
z[71] = abb[15] * (T(1) / T(8));
z[47] = z[47] * z[71];
z[71] = -abb[30] + z[37];
z[71] = abb[32] * z[71];
z[72] = abb[30] * abb[31];
z[73] = prod_pow(abb[31], 2);
z[74] = (T(1) / T(2)) * z[73];
z[75] = -abb[55] + z[74];
z[71] = abb[37] + z[71] + z[72] + -z[75];
z[72] = abb[7] * z[71];
z[76] = -abb[31] + abb[32] * (T(3) / T(2));
z[76] = abb[32] * z[76];
z[76] = abb[37] + -z[74] + z[76];
z[77] = abb[12] * (T(1) / T(4));
z[76] = z[76] * z[77];
z[77] = abb[30] + abb[32];
z[45] = z[45] * z[77];
z[45] = -abb[55] + (T(7) / T(6)) * z[38] + z[45];
z[77] = abb[14] * (T(1) / T(4));
z[45] = z[45] * z[77];
z[47] = -z[45] + z[47] + (T(-1) / T(8)) * z[72] + z[76];
z[17] = abb[33] * z[17];
z[17] = z[17] + -z[27] + -z[64];
z[17] = (T(1) / T(2)) * z[17] + -z[69];
z[15] = z[15] + -z[57];
z[15] = abb[30] * z[15];
z[16] = abb[34] * (T(3) / T(8)) + -z[16];
z[16] = abb[34] * z[16];
z[15] = z[15] + z[16] + -z[17] + (T(7) / T(24)) * z[38];
z[15] = abb[2] * z[15];
z[16] = -abb[33] + abb[34] * (T(3) / T(2));
z[16] = abb[34] * z[16];
z[64] = prod_pow(abb[33], 2);
z[69] = (T(1) / T(2)) * z[64];
z[16] = abb[56] + -z[16] + z[69];
z[16] = (T(1) / T(4)) * z[16] + z[29];
z[16] = abb[13] * z[16];
z[15] = z[15] + z[16];
z[72] = prod_pow(abb[32], 2);
z[77] = prod_pow(abb[34], 2);
z[72] = z[72] + -z[77];
z[78] = abb[11] * (T(1) / T(4));
z[72] = z[72] * z[78];
z[78] = abb[21] * abb[57];
z[78] = z[72] + (T(3) / T(16)) * z[78];
z[40] = z[15] + (T(1) / T(2)) * z[40] + -z[47] + z[63] + -z[78];
z[40] = abb[50] * z[40];
z[63] = z[3] + -z[8];
z[63] = abb[34] * z[63];
z[79] = abb[33] * z[34];
z[79] = -z[25] + z[79];
z[57] = -abb[30] + z[57];
z[57] = abb[30] * z[57];
z[63] = abb[35] + (T(1) / T(6)) * z[38] + -z[57] + z[63] + (T(-1) / T(2)) * z[79];
z[28] = z[28] * z[63];
z[15] = z[15] + -z[28];
z[63] = abb[29] + z[9];
z[63] = abb[32] * z[63];
z[79] = abb[35] + z[77];
z[73] = (T(1) / T(4)) * z[73];
z[63] = z[33] + -z[41] + -z[63] + z[73] + (T(1) / T(2)) * z[79];
z[63] = abb[16] * z[63];
z[13] = abb[32] * z[13];
z[13] = -z[13] + z[27] + z[41] + -z[74];
z[13] = abb[9] * z[13];
z[13] = z[13] + -z[46] + (T(3) / T(4)) * z[54];
z[13] = (T(1) / T(2)) * z[13] + -z[62] + z[63];
z[33] = -abb[55] + -z[33] + z[73];
z[7] = -abb[30] + z[7] + z[26];
z[7] = abb[32] * z[7];
z[26] = -abb[31] + z[6];
z[46] = -abb[30] * z[26];
z[7] = z[7] + -z[33] + (T(-7) / T(12)) * z[38] + z[46];
z[7] = abb[8] * z[7];
z[7] = z[7] + -z[13];
z[17] = -z[17] + z[68];
z[31] = -z[31] + z[56];
z[31] = abb[30] * z[31];
z[9] = -abb[30] + -z[9];
z[9] = abb[32] * z[9];
z[9] = z[9] + z[17] + -z[31] + -z[33];
z[9] = z[9] * z[43];
z[33] = -z[42] * z[71];
z[42] = -z[51] + -z[57] + z[65];
z[46] = (T(3) / T(2)) * z[38] + z[42];
z[56] = abb[5] * z[46];
z[7] = (T(1) / T(2)) * z[7] + z[9] + z[15] + z[33] + z[45] + (T(-1) / T(8)) * z[56] + -z[78];
z[7] = abb[51] * z[7];
z[9] = abb[24] * z[22];
z[22] = abb[29] * z[1];
z[33] = z[22] + z[41];
z[31] = z[31] + -z[33] + z[51] + z[75];
z[31] = abb[25] * z[31];
z[41] = -z[49] + z[66];
z[45] = abb[35] + z[53];
z[22] = -abb[39] + z[22] + z[41] + z[45];
z[22] = abb[23] * z[22];
z[33] = -z[33] + z[39] + z[59];
z[33] = abb[22] * z[33];
z[39] = abb[23] * z[34];
z[0] = abb[25] * z[0];
z[0] = z[0] + z[39];
z[0] = abb[32] * z[0];
z[23] = abb[57] * z[23];
z[0] = z[0] + -z[9] + z[22] + z[23] + z[31] + z[33];
z[9] = abb[24] + (T(1) / T(2)) * z[24];
z[22] = z[9] * z[38];
z[22] = (T(3) / T(2)) * z[0] + -z[22];
z[23] = abb[26] * z[60];
z[23] = (T(-1) / T(2)) * z[22] + (T(1) / T(4)) * z[23];
z[23] = abb[47] * z[23];
z[24] = -z[29] + z[55];
z[31] = abb[26] * (T(1) / T(2));
z[33] = z[24] * z[31];
z[9] = z[9] * z[29];
z[0] = (T(-1) / T(2)) * z[0] + z[9] + z[33];
z[9] = abb[43] * z[0];
z[29] = -abb[19] * z[42];
z[33] = z[25] + z[45] + -z[69];
z[33] = abb[18] * z[33];
z[29] = z[29] + z[33];
z[33] = abb[19] * (T(-3) / T(8)) + abb[18] * (T(-1) / T(3));
z[33] = z[33] * z[38];
z[39] = abb[28] + abb[0] * (T(-3) / T(8)) + abb[17] * (T(-1) / T(8)) + -z[43];
z[39] = abb[57] * z[39];
z[29] = (T(1) / T(4)) * z[29] + z[33] + z[39];
z[29] = abb[53] * z[29];
z[9] = z[9] + z[23] + z[29];
z[12] = z[12] + z[27];
z[23] = abb[55] + z[12] + z[36] + -z[64];
z[3] = abb[33] + z[3];
z[27] = abb[29] * (T(1) / T(2));
z[29] = z[3] + -z[27];
z[29] = abb[34] * z[29];
z[6] = z[6] * z[44];
z[6] = abb[56] * (T(-3) / T(2)) + -z[6] + (T(-1) / T(2)) * z[23] + -z[29] + -z[35] + z[38];
z[23] = -abb[33] + abb[29] * (T(1) / T(4)) + abb[32] * (T(3) / T(8));
z[23] = abb[32] * z[23];
z[6] = (T(-1) / T(2)) * z[6] + z[23];
z[6] = abb[8] * z[6];
z[4] = -z[4] + z[6] + (T(-3) / T(8)) * z[48] + (T(-3) / T(4)) * z[52];
z[1] = -z[1] * z[8];
z[6] = -abb[32] * z[34];
z[1] = abb[39] + z[1] + z[6] + z[25] + -z[38] + -z[41];
z[1] = abb[6] * z[1];
z[1] = (T(1) / T(8)) * z[1] + (T(1) / T(2)) * z[4] + z[16] + z[28] + -z[47];
z[1] = abb[49] * z[1];
z[4] = z[11] + z[12] + -z[21];
z[6] = abb[31] * (T(1) / T(4)) + z[34];
z[6] = abb[31] * z[6];
z[6] = z[6] + -z[32];
z[8] = z[26] + z[27];
z[11] = -abb[33] + -z[8] + -z[37];
z[11] = abb[32] * z[11];
z[3] = abb[34] * z[3];
z[3] = abb[56] + abb[36] * (T(3) / T(2)) + z[3] + (T(1) / T(2)) * z[4] + z[6] + z[11] + -z[30] + -z[67];
z[3] = abb[49] * z[3];
z[4] = abb[38] + -abb[56] + z[14] + (T(-3) / T(2)) * z[21] + z[25] + z[50];
z[8] = -abb[32] * z[8];
z[11] = -abb[33] + z[70];
z[11] = abb[31] + -abb[34] + (T(1) / T(2)) * z[11];
z[11] = -abb[30] + (T(1) / T(2)) * z[11];
z[11] = abb[30] * z[11];
z[4] = (T(1) / T(2)) * z[4] + z[6] + z[8] + z[11] + -z[38];
z[4] = abb[50] * z[4];
z[5] = (T(-1) / T(2)) * z[5];
z[5] = z[5] * z[46];
z[6] = abb[53] * abb[57];
z[3] = z[3] + z[4] + z[5] + (T(-3) / T(4)) * z[6];
z[3] = abb[5] * z[3];
z[4] = -abb[35] + -z[25] + z[77];
z[4] = z[4] * z[19];
z[3] = z[3] + z[4];
z[4] = z[31] * z[60];
z[4] = z[4] + -z[22];
z[5] = (T(1) / T(4)) * z[18];
z[4] = z[4] * z[5];
z[0] = -z[0] * z[20];
z[5] = z[17] + -z[58];
z[5] = z[5] * z[43];
z[5] = z[5] + z[15];
z[6] = z[5] + (T(-1) / T(2)) * z[13] + -z[72] + -z[76];
z[6] = abb[48] * z[6];
z[8] = (T(3) / T(2)) * z[54] + -z[61];
z[5] = -z[5] + (T(1) / T(8)) * z[8];
z[5] = abb[52] * z[5];
z[8] = abb[53] * z[24];
z[10] = abb[57] * z[10];
z[8] = z[8] + z[10];
z[8] = abb[21] * z[8];
z[0] = z[0] + z[1] + (T(1) / T(4)) * z[3] + z[4] + z[5] + z[6] + z[7] + (T(1) / T(8)) * z[8] + (T(1) / T(2)) * z[9] + z[40];
z[0] = (T(1) / T(4)) * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_666_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.11789240879621754367425929345808656678169346153818033150987311839"),stof<T>("-0.11835221263710411862374078700782989482205105856262933055305025081")}, std::complex<T>{stof<T>("0.11789240879621754367425929345808656678169346153818033150987311839"),stof<T>("-0.11835221263710411862374078700782989482205105856262933055305025081")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.11789240879621754367425929345808656678169346153818033150987311839"),stof<T>("-0.11835221263710411862374078700782989482205105856262933055305025081")}, std::complex<T>{stof<T>("0.38605199787543754005867743647602163369229196728858589056746834998"),stof<T>("-0.05462777403727628132968390432214680527589957274552976337736672577")}, std::complex<T>{stof<T>("0.21945571895616166790204361312878344543365879790256298618645812607"),stof<T>("-0.33232176065512709257173834116745282065033655913926370871199467729")}, std::complex<T>{stof<T>("0.92953867732467126680803459708106683801492331367908990311706460344"),stof<T>("-0.30653772032192214364009841618265780858707552115937444600237712513")}, std::complex<T>{stof<T>("0.23278599629982400352580411098768759817764256425557593451543848991"),stof<T>("-0.33232176065512709257173834116745282065033655913926370871199467729")}, std::complex<T>{stof<T>("-0.52312809939159841497487518109363381152894127604243967373093441427"),stof<T>("0.31244576478475254301903719950951518573127637349040735905052986715")}, std::complex<T>{stof<T>("-0.3440041854750969875943185853218086014763060625783884745339469414"),stof<T>("0.50131549372286322696668907466859795704656012298773609643632390436")}, std::complex<T>{stof<T>("-0.15718987839495672489901239127744875570892461538424044201316415785"),stof<T>("0.15780295018280549149832104934377319309606807808350577407073366775")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}, rlog(k.W[197].imag()/kbase.W[197].imag())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_666_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_666_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.42002044008581330307653894483807892152708057766629896299684017463"),stof<T>("-0.21065982564672047995608167662649569857313529072453934704168884147")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), rlog(kend.W[197].imag()/k.W[197].imag()), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_666_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_666_DLogXconstant_part(base_point<T>, kend);
	value += f_4_666_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_666_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_666_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_666_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_666_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_666_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_666_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
