/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_199.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_199_abbreviated (const std::array<T,25>& abb) {
T z[26];
z[0] = prod_pow(abb[12], 2);
z[0] = abb[16] + z[0];
z[1] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = (T(13) / T(6)) * z[1];
z[2] = prod_pow(abb[11], 2);
z[3] = z[0] + -z[1] + z[2];
z[3] = abb[4] * z[3];
z[4] = abb[13] * (T(1) / T(2));
z[5] = abb[10] + z[4];
z[5] = abb[13] * z[5];
z[5] = -abb[14] + z[5];
z[6] = prod_pow(abb[10], 2);
z[7] = (T(1) / T(2)) * z[6];
z[8] = -z[1] + z[5] + z[7];
z[8] = abb[2] * z[8];
z[9] = z[3] + -z[8];
z[10] = -z[2] + z[6];
z[10] = abb[5] * z[10];
z[11] = abb[3] + -abb[9];
z[12] = abb[14] * z[11];
z[11] = abb[16] * z[11];
z[10] = z[10] + -z[11] + -z[12];
z[5] = -z[5] + (T(3) / T(2)) * z[6];
z[5] = abb[6] * z[5];
z[0] = z[0] + -z[2];
z[0] = abb[8] * z[0];
z[11] = z[0] + z[5];
z[12] = -z[1] + 2 * z[2];
z[12] = abb[1] * z[12];
z[1] = -z[1] + 2 * z[6];
z[1] = abb[0] * z[1];
z[13] = -z[1] + z[12];
z[14] = -abb[15] + -abb[17] + abb[18];
z[15] = 2 * z[14];
z[16] = 2 * abb[9];
z[17] = abb[3] + abb[7] + -z[16];
z[15] = z[15] * z[17];
z[18] = abb[3] + abb[9];
z[6] = -z[6] * z[18];
z[19] = abb[7] + -abb[9];
z[20] = abb[10] * z[19];
z[21] = abb[13] * z[19];
z[22] = -2 * z[20] + -z[21];
z[22] = abb[13] * z[22];
z[23] = -abb[3] + abb[7];
z[23] = abb[12] * z[23];
z[24] = abb[10] * z[17];
z[25] = z[23] + 2 * z[24];
z[25] = abb[12] * z[25];
z[16] = z[2] * z[16];
z[6] = z[6] + -z[9] + 2 * z[10] + z[11] + z[13] + z[15] + z[16] + z[22] + z[25];
z[6] = abb[24] * z[6];
z[14] = z[14] * z[17];
z[15] = (T(1) / T(2)) * z[23] + z[24];
z[15] = abb[12] * z[15];
z[4] = z[4] * z[19];
z[4] = z[4] + z[20];
z[4] = abb[13] * z[4];
z[7] = z[7] * z[18];
z[2] = abb[9] * z[2];
z[2] = -z[2] + z[4] + z[7] + -z[10] + -z[14] + -z[15];
z[4] = z[2] + z[9];
z[4] = abb[23] * z[4];
z[2] = z[2] + -z[11];
z[2] = abb[20] * z[2];
z[0] = -z[0] + -z[3] + -z[12];
z[0] = abb[21] * z[0];
z[1] = z[1] + -z[5] + z[8];
z[1] = abb[19] * z[1];
z[3] = -abb[22] * z[13];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[6];
z[1] = abb[9] * abb[10];
z[2] = abb[12] * z[19];
z[1] = z[1] + -z[2] + z[21];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[11] * m1_set::bc<T>[0];
z[3] = abb[9] * z[2];
z[1] = z[1] + -z[3];
z[3] = abb[1] * z[2];
z[4] = abb[10] * m1_set::bc<T>[0];
z[5] = abb[0] * z[4];
z[6] = z[3] + -z[5];
z[4] = -z[2] + z[4];
z[4] = abb[5] * z[4];
z[7] = -z[1] + z[4] + z[6];
z[8] = abb[12] * m1_set::bc<T>[0];
z[9] = -z[2] + z[8];
z[9] = abb[8] * z[9];
z[10] = -abb[10] + abb[13];
z[10] = abb[6] * m1_set::bc<T>[0] * z[10];
z[11] = -z[9] + z[10];
z[2] = z[2] + z[8];
z[2] = abb[4] * z[2];
z[8] = abb[10] + abb[13];
z[8] = abb[2] * m1_set::bc<T>[0] * z[8];
z[12] = -z[2] + z[8];
z[7] = -2 * z[7] + z[11] + -z[12];
z[7] = abb[24] * z[7];
z[1] = z[1] + -z[4];
z[4] = -z[1] + z[12];
z[4] = abb[23] * z[4];
z[1] = -z[1] + -z[11];
z[1] = abb[20] * z[1];
z[2] = z[2] + 2 * z[3] + z[9];
z[2] = abb[21] * z[2];
z[3] = -2 * z[5] + -z[8] + -z[10];
z[3] = abb[19] * z[3];
z[5] = abb[22] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + 2 * z[5] + z[7];
z[1] = 2 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_199_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.63044610169493535091196880111479307520521581628708430584049572"),stof<T>("38.858042149405861786122438198413189216559838772369452802595963388")}, std::complex<T>{stof<T>("-1.8805516337250412070087073952166337406582818115254627635354279225"),stof<T>("4.1250878519596201870296527229978940872860288119180415307706102621")}, std::complex<T>{stof<T>("43.468707688259471674512059498143635106315898926517387294284966824"),stof<T>("-16.000499654332133701576761067472889438096574774327067176999971788")}, std::complex<T>{stof<T>("22.60890283178492313406981515200700402818461207269985546617044954"),stof<T>("7.583483977093285575622000127537769702729375417392099625370886916")}, std::complex<T>{stof<T>("-15.109910388504654396538982940238471743584352849055910285809449486"),stof<T>("-11.148970666020822321894024280404635988447859768732244469454494421")}, std::complex<T>{stof<T>("-5.6184408095552275305221248165518985439419774121184824168255721319"),stof<T>("-0.5596011630320834407576285701310278015675444605778966866870027563")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_199_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_199_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("22.529972719577018518875127564660231918196781712245159519316676644"),stof<T>("-24.880895239741483988445878242750971885601469620701978606818411432")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,25> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), f_1_1(k), f_1_3(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real())};

                    
            return f_4_199_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_199_DLogXconstant_part(base_point<T>, kend);
	value += f_4_199_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_199_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_199_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_199_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_199_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_199_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_199_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
