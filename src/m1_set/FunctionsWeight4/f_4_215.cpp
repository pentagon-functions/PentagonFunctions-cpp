/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_215.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_215_abbreviated (const std::array<T,17>& abb) {
T z[19];
z[0] = -abb[7] + abb[8];
z[0] = abb[8] * z[0];
z[1] = abb[7] + abb[8];
z[2] = abb[6] + z[1];
z[3] = abb[6] * z[2];
z[4] = prod_pow(abb[7], 2);
z[0] = z[0] + z[3] + z[4];
z[5] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = (T(1) / T(2)) * z[0] + (T(-2) / T(3)) * z[5];
z[0] = abb[0] * z[0];
z[3] = (T(-1) / T(4)) * z[3] + (T(1) / T(12)) * z[5];
z[6] = abb[7] * (T(1) / T(2));
z[7] = abb[8] + z[6];
z[7] = abb[8] * z[7];
z[8] = z[3] + 2 * z[4] + (T(1) / T(2)) * z[7];
z[8] = abb[2] * z[8];
z[9] = abb[6] * (T(1) / T(2));
z[2] = z[2] * z[9];
z[10] = (T(1) / T(2)) * z[5];
z[2] = z[2] + -z[10];
z[11] = abb[8] * z[6];
z[11] = -z[2] + z[4] + z[11];
z[11] = abb[1] * z[11];
z[8] = -z[0] + z[8] + (T(1) / T(2)) * z[11];
z[8] = abb[14] * z[8];
z[11] = abb[3] + abb[4];
z[6] = z[6] * z[11];
z[12] = abb[3] * abb[8];
z[6] = z[6] + z[12];
z[6] = abb[8] * z[6];
z[13] = abb[8] * z[11];
z[14] = abb[7] * z[11];
z[13] = z[13] + z[14];
z[15] = abb[6] * z[11];
z[16] = z[13] + z[15];
z[9] = z[9] * z[16];
z[10] = z[10] * z[11];
z[17] = abb[4] * z[4];
z[6] = -z[6] + z[9] + -z[10] + -z[17];
z[9] = abb[11] + abb[12];
z[10] = (T(3) / T(2)) * z[9];
z[18] = -abb[10] + -z[10];
z[6] = z[6] * z[18];
z[12] = -2 * z[12] + -z[14];
z[12] = abb[8] * z[12];
z[14] = abb[6] * z[16];
z[5] = -z[5] * z[11];
z[5] = z[5] + z[12] + z[14] + -2 * z[17];
z[5] = abb[15] * z[5];
z[12] = 2 * abb[8] + abb[7] * (T(1) / T(4));
z[12] = abb[8] * z[12];
z[3] = z[3] + (T(1) / T(2)) * z[4] + z[12];
z[3] = abb[1] * z[3];
z[2] = -z[2] + z[7];
z[2] = abb[2] * z[2];
z[0] = -z[0] + (T(1) / T(2)) * z[2] + z[3];
z[0] = abb[13] * z[0];
z[2] = 3 * z[9];
z[3] = abb[1] + abb[2];
z[4] = abb[0] + (T(3) / T(2)) * z[3];
z[7] = -abb[5] + (T(1) / T(2)) * z[4];
z[2] = z[2] * z[7];
z[7] = abb[13] + abb[14];
z[9] = (T(3) / T(4)) * z[11];
z[9] = z[7] * z[9];
z[11] = 2 * abb[0] + -4 * abb[5] + 3 * z[3];
z[11] = abb[15] * z[11];
z[2] = -z[2] + -z[9] + z[11];
z[4] = 2 * abb[5] + -z[4];
z[9] = -abb[10] * z[4];
z[9] = -z[2] + z[9];
z[9] = abb[16] * z[9];
z[0] = z[0] + z[5] + z[6] + z[8] + z[9];
z[5] = m1_set::bc<T>[0] * (T(1) / T(2));
z[3] = z[3] * z[5];
z[5] = abb[0] * m1_set::bc<T>[0];
z[3] = z[3] + z[5];
z[1] = abb[6] + (T(1) / T(2)) * z[1];
z[1] = z[1] * z[3] * z[7];
z[3] = (T(1) / T(2)) * z[13] + z[15];
z[5] = z[3] * z[10];
z[6] = -z[13] + -2 * z[15];
z[6] = abb[15] * z[6];
z[5] = z[5] + z[6];
z[5] = m1_set::bc<T>[0] * z[5];
z[2] = -abb[9] * z[2];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = -abb[9] * z[4];
z[3] = z[3] + z[4];
z[3] = abb[10] * z[3];
z[1] = z[1] + z[2] + z[3] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_215_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("3.7750428937635931319690940113069686134663500614979661092813990745"),stof<T>("-6.147904513184628857106431949762537542394915181439860528724860466")}, std::complex<T>{stof<T>("5.6625643406453896979536410169604529201995250922469491639220986118"),stof<T>("-9.221856769776943285659647924643806313592372772159790793087290699")}, std::complex<T>{stof<T>("5.6625643406453896979536410169604529201995250922469491639220986118"),stof<T>("-9.221856769776943285659647924643806313592372772159790793087290699")}, std::complex<T>{stof<T>("12.566746881450349585252947280583076751192871383858558146164888611"),stof<T>("-7.316650242258947503059593500842803694562876468963928804631194841")}, std::complex<T>{stof<T>("12.566746881450349585252947280583076751192871383858558146164888611"),stof<T>("-7.316650242258947503059593500842803694562876468963928804631194841")}, std::complex<T>{stof<T>("-7.550085787527186263938188022613937226932700122995932218562798149"),stof<T>("12.295809026369257714212863899525075084789830362879721057449720932")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[196].real()/kbase.W[196].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_215_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_215_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("4.5869504081885687151714309395736211957041328424382255089715747919"),stof<T>("0.0792548838408560019443555704378996677844803007494005944890137061")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,17> abb = {dl[0], dlog_W8(k,dl), dl[3], dlog_W120(k,dv), dlog_W121(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[196].real()/k.W[196].real()), f_2_24_re(k)};

                    
            return f_4_215_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_215_DLogXconstant_part(base_point<T>, kend);
	value += f_4_215_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_215_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_215_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_215_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_215_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_215_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_215_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
