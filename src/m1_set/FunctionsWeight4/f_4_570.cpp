/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_570.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_570_abbreviated (const std::array<T,33>& abb) {
T z[54];
z[0] = abb[20] + -abb[23];
z[1] = 2 * abb[26];
z[2] = 2 * abb[28];
z[3] = z[0] + -z[1] + z[2];
z[4] = 3 * abb[29];
z[5] = 2 * abb[24];
z[6] = z[4] + -z[5];
z[7] = 2 * abb[21];
z[8] = -z[6] + z[7];
z[9] = z[3] + -z[8];
z[10] = abb[8] * z[9];
z[11] = 2 * abb[27];
z[12] = z[1] + z[11];
z[13] = abb[20] + abb[23];
z[14] = z[4] + -z[13];
z[15] = 2 * abb[22];
z[16] = z[12] + -z[14] + -z[15];
z[16] = abb[2] * z[16];
z[10] = z[10] + -z[16];
z[17] = -z[6] + z[15];
z[3] = z[3] + z[17];
z[18] = abb[6] * z[3];
z[19] = abb[27] + abb[28];
z[20] = abb[25] + abb[26];
z[21] = z[13] + z[19] + z[20];
z[22] = 6 * abb[29];
z[23] = -z[5] + z[22];
z[24] = z[21] + -z[23];
z[24] = abb[4] * z[24];
z[25] = 2 * z[24];
z[26] = z[18] + z[25];
z[27] = 2 * abb[25];
z[28] = z[0] + z[11] + -z[27];
z[8] = z[8] + z[28];
z[29] = abb[5] * z[8];
z[30] = abb[21] + abb[22];
z[31] = 4 * abb[24];
z[21] = -9 * abb[29] + z[21] + z[30] + z[31];
z[32] = abb[9] * z[21];
z[33] = z[2] + z[27];
z[14] = z[7] + z[14] + -z[33];
z[14] = abb[1] * z[14];
z[0] = z[0] + z[19] + -z[20];
z[34] = -z[6] + z[30];
z[35] = -z[0] + z[34];
z[35] = abb[0] * z[35];
z[0] = z[0] + z[34];
z[0] = abb[3] * z[0];
z[0] = z[0] + -z[10] + -z[14] + z[26] + z[29] + -3 * z[32] + z[35];
z[0] = abb[16] * z[0];
z[34] = abb[23] * (T(1) / T(2));
z[35] = abb[29] * (T(3) / T(2));
z[36] = z[34] + z[35];
z[37] = abb[24] + abb[20] * (T(1) / T(2));
z[38] = z[36] + -z[37];
z[39] = -abb[21] + abb[25] + -abb[27];
z[40] = z[38] + z[39];
z[40] = abb[5] * z[40];
z[40] = -z[24] + z[40];
z[20] = -abb[20] + z[20];
z[41] = abb[24] + z[30];
z[42] = -z[4] + z[20] + z[41];
z[42] = abb[0] * z[42];
z[43] = 2 * abb[9];
z[21] = z[21] * z[43];
z[43] = abb[21] + -abb[28];
z[44] = (T(-1) / T(2)) * z[43];
z[45] = abb[26] * (T(1) / T(2));
z[46] = z[44] + -z[45];
z[34] = abb[24] + z[34];
z[47] = z[34] + -z[35] + -z[46];
z[47] = abb[8] * z[47];
z[35] = (T(1) / T(2)) * z[13] + -z[35];
z[48] = abb[22] + -abb[26];
z[49] = abb[27] + z[35] + -z[48];
z[49] = abb[2] * z[49];
z[50] = abb[28] + z[48];
z[38] = -z[38] + z[50];
z[38] = abb[6] * z[38];
z[35] = abb[25] + z[35] + -z[43];
z[35] = abb[1] * z[35];
z[44] = z[44] + z[45];
z[45] = -abb[22] + abb[25] + abb[27];
z[45] = (T(1) / T(2)) * z[45];
z[51] = abb[24] + -z[44] + -z[45];
z[51] = abb[3] * z[51];
z[47] = -z[21] + z[35] + z[38] + -z[40] + z[42] + 3 * z[47] + z[49] + z[51];
z[47] = abb[11] * z[47];
z[32] = -z[24] + z[32];
z[51] = -abb[23] + abb[24];
z[52] = abb[20] + -z[22] + 2 * z[51];
z[53] = abb[22] + z[7];
z[2] = -abb[25] + 3 * abb[27] + z[2] + z[52] + z[53];
z[2] = abb[3] * z[2];
z[17] = -z[17] + z[28];
z[17] = abb[0] * z[17];
z[2] = -z[2] + -z[16] + z[17] + -z[29] + 2 * z[32];
z[16] = abb[13] * z[2];
z[17] = abb[21] + abb[26];
z[28] = 2 * abb[20];
z[29] = abb[23] + abb[28];
z[6] = z[6] + z[17] + -z[28] + -z[29];
z[6] = abb[8] * z[6];
z[6] = z[6] + z[18];
z[18] = z[21] + -z[25];
z[21] = z[14] + z[18];
z[11] = abb[21] + -abb[26] + 3 * abb[28] + z[11] + z[15] + z[52];
z[11] = abb[3] * z[11];
z[9] = abb[0] * z[9];
z[9] = z[6] + -z[9] + z[11] + -z[21];
z[11] = -abb[12] * z[9];
z[19] = -z[4] + z[19] + z[30] + z[51];
z[19] = abb[3] * z[19];
z[19] = z[19] + -z[32] + z[42];
z[25] = 2 * abb[10];
z[19] = z[19] * z[25];
z[11] = z[11] + z[16] + z[19] + z[47];
z[11] = abb[11] * z[11];
z[2] = -abb[15] * z[2];
z[16] = -z[22] + z[28] + z[31];
z[19] = -abb[23] + -z[16] + z[39];
z[19] = abb[5] * z[19];
z[24] = z[19] + z[24];
z[30] = abb[29] * (T(15) / T(2));
z[31] = -z[28] + z[30] + -5 * z[34] + z[46];
z[31] = abb[8] * z[31];
z[32] = z[36] + z[37];
z[36] = -abb[21] + abb[22];
z[42] = z[32] + -z[33] + -z[36];
z[42] = abb[0] * z[42];
z[35] = 5 * z[35];
z[44] = -abb[25] + z[37] + -z[44];
z[44] = abb[3] * z[44];
z[31] = z[24] + z[31] + z[35] + z[38] + z[42] + z[44];
z[31] = prod_pow(abb[12], 2) * z[31];
z[38] = abb[29] * (T(13) / T(2));
z[42] = abb[22] * (T(11) / T(6));
z[44] = abb[26] + -abb[28];
z[44] = -abb[23] + -13 * abb[24] + abb[20] * (T(-13) / T(2)) + (T(11) / T(2)) * z[44];
z[44] = z[38] + -z[42] + (T(1) / T(3)) * z[44];
z[44] = abb[6] * z[44];
z[17] = -abb[28] + z[17];
z[34] = abb[20] * (T(11) / T(2)) + z[17] + 13 * z[34];
z[34] = (T(1) / T(3)) * z[34] + -z[38];
z[34] = abb[8] * z[34];
z[30] = 5 * abb[25] + abb[20] * (T(-5) / T(2)) + abb[23] * (T(2) / T(3)) + abb[28] * (T(11) / T(6)) + abb[26] * (T(19) / T(6)) + -z[30] + z[42];
z[30] = abb[0] * z[30];
z[28] = -abb[23] + z[28];
z[38] = -abb[21] + z[28];
z[42] = -abb[26] + z[38];
z[46] = -abb[29] + abb[27] * (T(2) / T(3));
z[42] = abb[28] + (T(1) / T(3)) * z[42] + z[46];
z[42] = abb[3] * z[42];
z[47] = -z[1] + -z[13];
z[46] = abb[22] * (T(2) / T(3)) + -z[46] + (T(1) / T(3)) * z[47];
z[46] = abb[2] * z[46];
z[30] = z[30] + z[34] + -z[35] + z[42] + z[44] + z[46];
z[30] = prod_pow(m1_set::bc<T>[0], 2) * z[30];
z[16] = z[16] + z[29] + z[48];
z[16] = abb[6] * z[16];
z[12] = -z[12] + z[32] + z[36];
z[12] = abb[0] * z[12];
z[29] = -abb[26] + z[37] + -z[45];
z[29] = abb[3] * z[29];
z[12] = z[12] + -z[16] + z[29] + -z[40] + 5 * z[49];
z[12] = prod_pow(abb[13], 2) * z[12];
z[28] = 3 * abb[26] + -z[28];
z[29] = -abb[28] + -z[23] + z[27] + z[28] + z[53];
z[29] = abb[0] * z[29];
z[3] = abb[3] * z[3];
z[32] = abb[23] + abb[24];
z[32] = abb[20] + -z[4] + 2 * z[32];
z[34] = z[32] + -z[50];
z[34] = abb[6] * z[34];
z[3] = z[3] + -z[10] + -z[18] + z[29] + -z[34];
z[10] = abb[30] * z[3];
z[18] = -abb[31] * z[9];
z[29] = 3 * abb[25];
z[1] = abb[27] + -z[1] + -z[15] + z[23] + -z[29] + z[38];
z[1] = abb[0] * z[1];
z[8] = -abb[3] * z[8];
z[23] = z[32] + z[39];
z[23] = abb[5] * z[23];
z[1] = z[1] + z[8] + z[21] + z[23];
z[1] = abb[14] * z[1];
z[8] = -z[22] + z[28] + z[29] + z[41];
z[8] = abb[0] * z[8];
z[20] = -abb[24] + z[20];
z[20] = abb[3] * z[20];
z[8] = -z[8] + z[16] + z[20] + -z[24];
z[16] = prod_pow(abb[10], 2) * z[8];
z[0] = abb[32] + z[0] + z[1] + z[2] + z[10] + z[11] + z[12] + z[16] + z[18] + z[30] + z[31];
z[1] = -z[4] + -z[13] + z[15];
z[2] = abb[25] + abb[28];
z[2] = z[1] + 4 * z[2] + -z[5] + -z[7];
z[2] = abb[0] * z[2];
z[4] = -abb[20] + abb[26] + -z[5] + z[27] + -z[43];
z[4] = abb[3] * z[4];
z[5] = abb[23] + z[5];
z[5] = 4 * abb[20] + -15 * abb[29] + 5 * z[5] + z[17];
z[5] = abb[8] * z[5];
z[2] = z[2] + z[4] + z[5] + 5 * z[14] + -2 * z[19] + -z[26];
z[2] = abb[12] * z[2];
z[4] = -z[8] * z[25];
z[1] = -z[1] + -z[33];
z[1] = abb[0] * z[1];
z[5] = abb[20] + -z[17];
z[5] = abb[3] * z[5];
z[1] = z[1] + z[5] + z[6] + -z[14];
z[1] = abb[11] * z[1];
z[1] = z[1] + z[2] + z[4];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[18] * z[9];
z[3] = abb[17] * z[3];
z[1] = abb[19] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_570_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.853492675422079986704543174316743567226325072836585885076212185"),stof<T>("58.710287179796775783563633308926186988840991676902758917737879488")}, std::complex<T>{stof<T>("34.831270440658729878178805626939262322269584388855310554362311841"),stof<T>("-29.570803362486875899202347079972676806646652606456068236883157959")}, std::complex<T>{stof<T>("-28.645759650817762825142251843624296813001486121815095084074945105"),stof<T>("-20.550552081361626739730676952202164030099696606619048054700665324")}, std::complex<T>{stof<T>("16.087968906282078441749096440934352602862016640134518309679497875"),stof<T>("12.076644868850007339211785941993528351053244381314466523770225968")}, std::complex<T>{stof<T>("20.419987020700965508081107049932574544903789834338147894890652426"),stof<T>("20.665576604798280483842395218744874503147886845142109149924282173")}, std::complex<T>{stof<T>("-45.535568509772334274867417855312462965182728797699301443681546886"),stof<T>("-37.613391029821519284880177239162145861240791295751272211785160884")}, std::complex<T>{stof<T>("17.94146158170415842845363961525109617008834171297110419475571006"),stof<T>("-46.633642310946768444351847366932658637787747295588292393967653519")}, std::complex<T>{stof<T>("-63.477030091476492703321057470563559135271070510670405638437256946"),stof<T>("9.020251281125249159471670127770512776546955999837020182182492636")}, std::complex<T>{stof<T>("37.673372233607053150179466208069832630418408445041730323186341689"),stof<T>("25.421721637534858201556673030625907037139356675913744592791318066")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_570_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_570_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (-m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + 4 * (12 + 10 * v[0] + v[1] + 7 * v[2] + v[3] + -7 * v[4] + -4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -6 * v[5]))) / prod_pow(tend, 2);
c[1] = ((T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (8 * v[0] + 5 * v[1] + 5 * v[2] + -v[3] + -5 * v[4] + 4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) + -m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]))) / prod_pow(tend, 2);
c[2] = ((2 + 8 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[3] = ((-1 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * (abb[22] + abb[23] + 2 * abb[24] + abb[25] + -abb[27] + -3 * abb[29]) * c[0] + t * (abb[20] + -abb[22] + -abb[25] + abb[27]) * c[1] + abb[23] * c[2] + 2 * abb[24] * c[2] + -3 * abb[29] * c[2] + abb[20] * (c[2] + -c[3]) + abb[22] * c[3] + abb[25] * c[3] + -abb[27] * c[3];
	}
	{
T z[7];
z[0] = -abb[11] + abb[13] * (T(5) / T(2));
z[0] = abb[13] * z[0];
z[1] = abb[14] + -abb[16];
z[2] = prod_pow(abb[11], 2);
z[0] = abb[15] + z[0] + z[1] + (T(-3) / T(2)) * z[2];
z[3] = -abb[23] + 3 * abb[29];
z[0] = z[0] * z[3];
z[3] = abb[22] + abb[25] + -abb[27];
z[4] = -abb[24] + -z[3];
z[4] = z[1] * z[4];
z[5] = (T(1) / T(2)) * z[3];
z[6] = abb[24] + z[5];
z[2] = z[2] * z[6];
z[3] = -2 * abb[24] + z[3];
z[6] = -abb[11] * z[3];
z[5] = -5 * abb[24] + -z[5];
z[5] = abb[13] * z[5];
z[5] = z[5] + z[6];
z[5] = abb[13] * z[5];
z[6] = abb[11] + -abb[13];
z[6] = abb[13] * z[6];
z[6] = -abb[15] + z[6];
z[1] = z[1] + 2 * z[6];
z[1] = abb[20] * z[1];
z[3] = abb[15] * z[3];
z[0] = z[0] + z[1] + 3 * z[2] + z[3] + 2 * z[4] + z[5];
return abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_570_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_570_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("8.069811860745754865447764393693568263766654619812047785888270555"),stof<T>("-28.528650924580970035876783786637542832943202450857837674914007534")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,33> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), f_1_1(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), f_2_2_re(k), f_2_10_re(k), T{0}};
abb[19] = SpDLog_f_4_570_W_20_Im(t, path, abb);
abb[32] = SpDLog_f_4_570_W_20_Re(t, path, abb);

                    
            return f_4_570_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_570_DLogXconstant_part(base_point<T>, kend);
	value += f_4_570_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_570_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_570_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_570_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_570_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_570_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_570_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
