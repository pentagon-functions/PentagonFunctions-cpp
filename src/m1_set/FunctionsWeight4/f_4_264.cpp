/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_264.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_264_abbreviated (const std::array<T,56>& abb) {
T z[120];
z[0] = abb[44] * (T(1) / T(2));
z[1] = -abb[45] + z[0];
z[2] = abb[43] * (T(1) / T(2));
z[3] = abb[42] * (T(5) / T(2)) + z[2];
z[4] = z[1] + z[3];
z[5] = abb[46] * (T(1) / T(4));
z[6] = -abb[48] + z[5];
z[7] = abb[49] * (T(3) / T(4));
z[4] = (T(1) / T(2)) * z[4] + z[6] + -z[7];
z[8] = 3 * abb[3];
z[9] = -z[4] * z[8];
z[10] = abb[42] * (T(3) / T(2));
z[11] = abb[43] * (T(3) / T(2));
z[12] = z[10] + z[11];
z[13] = abb[45] + z[0];
z[14] = z[12] + -z[13];
z[15] = abb[49] * (T(1) / T(4));
z[14] = -abb[48] + -z[5] + (T(1) / T(2)) * z[14] + -z[15];
z[14] = abb[8] * z[14];
z[16] = 3 * abb[42];
z[17] = abb[44] + z[16];
z[18] = -abb[45] + -abb[47] + z[17];
z[19] = abb[49] * (T(1) / T(2));
z[18] = -abb[48] + (T(1) / T(2)) * z[18] + -z[19];
z[18] = abb[9] * z[18];
z[20] = abb[46] * (T(1) / T(2));
z[21] = z[19] + z[20];
z[22] = abb[44] + -abb[45];
z[22] = -abb[48] + (T(1) / T(2)) * z[22];
z[23] = abb[41] * (T(-3) / T(2)) + z[21] + z[22];
z[23] = abb[2] * z[23];
z[24] = -abb[42] + abb[49];
z[25] = abb[41] + -z[24];
z[25] = abb[10] * z[25];
z[26] = -abb[47] + abb[49];
z[27] = -abb[41] + z[26];
z[28] = abb[0] * z[27];
z[29] = 3 * z[25] + 2 * z[28];
z[30] = z[19] + -z[20];
z[31] = abb[43] + -abb[44];
z[32] = -abb[42] + z[31];
z[33] = abb[47] + (T(1) / T(2)) * z[32];
z[34] = -z[30] + z[33];
z[35] = abb[14] * z[34];
z[36] = (T(3) / T(2)) * z[35];
z[37] = z[24] + -z[31];
z[38] = -abb[41] + (T(1) / T(2)) * z[37];
z[39] = z[20] + z[38];
z[40] = abb[16] * z[39];
z[41] = (T(3) / T(2)) * z[40];
z[42] = -abb[41] + abb[46];
z[43] = abb[42] + -abb[47];
z[43] = -z[42] + 3 * z[43];
z[44] = abb[6] * (T(1) / T(2));
z[43] = z[43] * z[44];
z[45] = abb[50] + abb[51] + abb[52];
z[46] = (T(3) / T(4)) * z[45];
z[47] = abb[25] * z[46];
z[26] = -abb[46] + z[26];
z[48] = abb[1] * z[26];
z[49] = abb[23] * z[45];
z[9] = z[9] + z[14] + 3 * z[18] + -z[23] + z[29] + z[36] + z[41] + z[43] + -z[47] + z[48] + (T(3) / T(4)) * z[49];
z[9] = abb[27] * z[9];
z[14] = abb[41] + abb[47];
z[43] = abb[46] * (T(3) / T(4));
z[50] = z[14] + -z[15] + (T(3) / T(4)) * z[32] + -z[43];
z[50] = abb[6] * z[50];
z[51] = abb[21] * z[45];
z[52] = (T(3) / T(2)) * z[51];
z[53] = abb[22] * z[45];
z[54] = (T(3) / T(2)) * z[53];
z[55] = -z[28] + z[52] + -z[54];
z[56] = abb[45] + 2 * abb[48];
z[57] = z[12] + -z[56];
z[58] = z[0] + z[20];
z[59] = z[19] + z[58];
z[60] = z[57] + -z[59];
z[61] = abb[15] * z[60];
z[17] = z[17] + -z[56];
z[62] = -abb[47] + -abb[49] + z[17];
z[63] = abb[9] * z[62];
z[64] = -z[61] + z[63];
z[65] = abb[44] + abb[49];
z[66] = abb[46] + z[65];
z[67] = 3 * abb[41];
z[68] = -z[56] + z[66] + -z[67];
z[69] = -abb[2] * z[68];
z[70] = 2 * z[48];
z[36] = z[36] + -z[41] + -z[50] + (T(-1) / T(2)) * z[55] + z[64] + -z[69] + z[70];
z[41] = -abb[31] * z[36];
z[46] = abb[24] * z[46];
z[46] = z[46] + -z[47];
z[36] = z[36] + -z[46];
z[36] = abb[28] * z[36];
z[47] = 3 * z[40];
z[50] = z[47] + z[52];
z[55] = (T(3) / T(2)) * z[49];
z[71] = z[50] + z[55] + z[61];
z[71] = abb[32] * z[71];
z[72] = abb[32] * z[69];
z[72] = 3 * z[72];
z[73] = 2 * abb[45] + 4 * abb[48];
z[74] = 3 * abb[43];
z[75] = z[73] + -z[74];
z[16] = -z[16] + z[66] + z[75];
z[66] = -abb[30] + abb[32];
z[76] = -z[16] * z[66];
z[77] = abb[11] * z[76];
z[78] = abb[32] * z[45];
z[79] = (T(1) / T(2)) * z[45];
z[80] = abb[31] * z[79];
z[78] = z[78] + -z[80];
z[80] = abb[25] * (T(3) / T(2));
z[81] = z[78] * z[80];
z[71] = z[71] + z[72] + -z[77] + -z[81];
z[82] = 3 * z[35];
z[83] = z[54] + z[82];
z[84] = 4 * z[48];
z[85] = z[55] + z[64] + z[83] + z[84];
z[85] = abb[29] * z[85];
z[86] = 2 * abb[49];
z[17] = -abb[46] + -z[17] + z[86];
z[87] = 2 * abb[15];
z[87] = z[17] * z[87];
z[88] = z[55] + z[87];
z[89] = z[51] + z[53];
z[90] = z[88] + (T(3) / T(2)) * z[89];
z[3] = abb[49] * (T(-3) / T(2)) + z[3] + -z[56];
z[58] = z[3] + z[58];
z[91] = z[8] * z[58];
z[92] = z[90] + z[91];
z[92] = abb[30] * z[92];
z[93] = -abb[32] * z[68];
z[94] = abb[30] * z[60];
z[32] = (T(3) / T(2)) * z[32];
z[95] = abb[47] + z[30] + z[32];
z[95] = abb[29] * z[95];
z[96] = -z[93] + -z[94] + z[95];
z[96] = abb[6] * z[96];
z[97] = abb[32] * z[60];
z[94] = z[94] + -z[97];
z[98] = abb[29] * z[60];
z[99] = z[94] + -z[98];
z[99] = abb[8] * z[99];
z[100] = abb[30] * z[45];
z[78] = -z[78] + z[100];
z[100] = abb[24] * (T(3) / T(2));
z[101] = z[78] * z[100];
z[9] = z[9] + z[36] + z[41] + -z[71] + -z[85] + z[92] + z[96] + z[99] + z[101];
z[9] = abb[27] * z[9];
z[36] = abb[25] * z[79];
z[41] = abb[24] * z[79];
z[36] = z[36] + -z[40] + -z[41];
z[40] = (T(1) / T(2)) * z[53];
z[92] = z[40] + (T(1) / T(2)) * z[51];
z[96] = abb[41] + z[31];
z[99] = abb[46] + -z[96];
z[101] = 3 * abb[13];
z[99] = z[99] * z[101];
z[22] = z[19] + z[22];
z[101] = abb[41] * (T(-1) / T(2)) + abb[46] * (T(1) / T(6)) + (T(1) / T(3)) * z[22];
z[101] = abb[2] * z[101];
z[31] = z[24] + z[31];
z[102] = abb[41] * (T(-7) / T(3)) + -z[31];
z[102] = abb[46] * (T(5) / T(3)) + (T(1) / T(2)) * z[102];
z[102] = abb[6] * z[102];
z[103] = -z[19] + z[33];
z[104] = abb[41] * (T(-5) / T(6)) + abb[46] * (T(1) / T(3)) + -z[103];
z[104] = abb[5] * z[104];
z[105] = 5 * abb[44] + 7 * abb[45];
z[105] = abb[42] + -2 * abb[43] + abb[49] * (T(-13) / T(6)) + abb[48] * (T(7) / T(3)) + abb[46] * (T(10) / T(3)) + (T(1) / T(6)) * z[105];
z[105] = abb[8] * z[105];
z[101] = (T(-13) / T(6)) * z[28] + z[35] + -z[36] + (T(19) / T(6)) * z[48] + z[49] + z[92] + -z[99] + 7 * z[101] + z[102] + z[104] + z[105];
z[101] = prod_pow(m1_set::bc<T>[0], 2) * z[101];
z[102] = z[28] + -z[48];
z[104] = -z[42] * z[44];
z[22] = -abb[46] + z[22];
z[105] = abb[8] * z[22];
z[23] = -z[23] + (T(1) / T(2)) * z[102] + z[104] + z[105];
z[23] = abb[28] * z[23];
z[50] = 3 * z[49] + z[50] + z[83];
z[104] = -z[28] + z[50];
z[105] = (T(3) / T(4)) * z[31];
z[5] = -abb[41] + z[5] + z[105];
z[5] = abb[6] * z[5];
z[5] = z[5] + -z[69] + -z[70] + (T(-1) / T(2)) * z[104];
z[5] = abb[31] * z[5];
z[37] = -abb[46] + z[37];
z[70] = abb[29] + -abb[30];
z[104] = abb[7] * (T(3) / T(2));
z[106] = z[37] * z[70] * z[104];
z[107] = abb[29] * z[45];
z[78] = -z[78] + z[107];
z[78] = z[78] * z[100];
z[5] = z[5] + z[78] + z[106];
z[78] = abb[31] * z[60];
z[78] = z[78] + -z[97];
z[97] = 2 * abb[46];
z[106] = z[56] + z[97];
z[65] = -z[65] + z[106];
z[107] = abb[30] * z[65];
z[65] = abb[29] * z[65];
z[65] = z[65] + -z[78] + -2 * z[107];
z[65] = abb[8] * z[65];
z[108] = -abb[30] * z[61];
z[64] = -z[48] + -z[64];
z[64] = abb[29] * z[64];
z[109] = abb[29] * z[26];
z[93] = z[93] + -z[107] + -z[109];
z[93] = abb[6] * z[93];
z[23] = z[5] + z[23] + z[64] + z[65] + z[71] + z[93] + z[108];
z[23] = abb[28] * z[23];
z[64] = 2 * abb[47];
z[65] = abb[49] * (T(5) / T(2));
z[71] = abb[46] * (T(3) / T(2));
z[57] = z[0] + -z[57] + -z[64] + z[65] + -z[71];
z[57] = abb[34] * z[57];
z[93] = abb[44] + abb[46];
z[10] = abb[43] * (T(9) / T(2)) + -z[10] + -z[56] + z[65] + (T(-7) / T(2)) * z[93];
z[10] = abb[36] * z[10];
z[93] = abb[37] * z[60];
z[10] = z[10] + z[93];
z[95] = z[94] + z[95];
z[103] = 3 * z[103];
z[107] = abb[46] * (T(5) / T(2));
z[108] = -abb[41] + z[103] + z[107];
z[110] = abb[31] * (T(1) / T(2));
z[108] = z[108] * z[110];
z[108] = -z[95] + z[108];
z[111] = abb[28] * (T(1) / T(2));
z[112] = z[42] * z[111];
z[112] = z[108] + z[112];
z[112] = abb[28] * z[112];
z[65] = z[32] + z[65];
z[14] = -z[14] + z[65] + -z[71];
z[110] = -z[110] + z[111];
z[14] = z[14] * z[110];
z[110] = -abb[27] * z[42];
z[14] = z[14] + z[94] + -2 * z[109] + z[110];
z[14] = abb[27] * z[14];
z[74] = -2 * abb[44] + abb[49] + z[74] + -z[106];
z[94] = -z[66] * z[74];
z[106] = abb[31] * z[42];
z[106] = -z[94] + z[106] + z[109];
z[106] = abb[31] * z[106];
z[65] = -abb[47] + z[65] + -z[107];
z[109] = prod_pow(abb[29], 2);
z[110] = (T(1) / T(2)) * z[109];
z[65] = z[65] * z[110];
z[113] = 3 * abb[53];
z[114] = -z[34] * z[113];
z[14] = z[10] + z[14] + z[57] + z[65] + 2 * z[106] + z[112] + z[114];
z[14] = abb[5] * z[14];
z[57] = abb[42] * (T(1) / T(2));
z[65] = z[56] + -z[57] + z[71];
z[71] = abb[43] * (T(-5) / T(2)) + abb[44] * (T(3) / T(2));
z[106] = -z[19] + z[65] + z[71];
z[112] = abb[5] + abb[8];
z[112] = z[106] * z[112];
z[106] = abb[4] * z[106];
z[89] = z[49] + z[89];
z[89] = (T(1) / T(2)) * z[89];
z[114] = abb[6] * z[39];
z[36] = -z[36] + z[69] + z[89] + z[106] + -z[112] + -z[114];
z[112] = abb[54] * z[36];
z[114] = -abb[3] + abb[15];
z[114] = z[58] * z[114];
z[19] = z[0] + z[19];
z[65] = -z[2] + -z[19] + z[65];
z[65] = abb[8] * z[65];
z[115] = (T(1) / T(2)) * z[37];
z[116] = abb[7] * z[115];
z[117] = -abb[6] * z[115];
z[65] = z[65] + -z[89] + z[114] + z[116] + z[117];
z[65] = abb[35] * z[65];
z[89] = prod_pow(abb[32], 2);
z[114] = -abb[33] + -z[89];
z[68] = abb[2] * z[68] * z[114];
z[0] = z[0] + z[3];
z[0] = abb[33] * z[0];
z[3] = abb[33] * z[20];
z[0] = z[0] + z[3];
z[114] = abb[15] * z[0];
z[117] = prod_pow(abb[30], 2);
z[118] = -z[109] + z[117];
z[24] = -abb[46] + z[24];
z[24] = abb[12] * z[24];
z[119] = -z[24] * z[118];
z[40] = -abb[33] * z[40];
z[40] = z[40] + z[65] + z[68] + z[112] + z[114] + z[119];
z[65] = -abb[36] * z[16];
z[1] = -z[1] + -z[2] + -z[57];
z[1] = abb[41] + (T(1) / T(2)) * z[1] + -z[6] + -z[15];
z[2] = 3 * z[89];
z[1] = z[1] * z[2];
z[6] = abb[33] * z[38];
z[3] = z[3] + z[6];
z[3] = 3 * z[3];
z[6] = abb[42] + abb[43] + -abb[45];
z[6] = -abb[48] + (T(1) / T(2)) * z[6] + -z[20];
z[38] = 3 * z[117];
z[6] = z[6] * z[38];
z[26] = -z[26] * z[110];
z[1] = z[1] + -z[3] + z[6] + z[26] + z[65] + -z[93];
z[1] = abb[6] * z[1];
z[6] = 3 * z[106];
z[26] = z[6] + z[54];
z[54] = z[26] + -z[61];
z[54] = z[54] * z[66];
z[61] = -abb[6] * z[95];
z[42] = abb[6] * z[42];
z[42] = z[42] + -z[69] + -z[99] + -z[102];
z[42] = abb[31] * z[42];
z[42] = z[42] + z[54] + z[61] + z[85];
z[42] = abb[31] * z[42];
z[54] = abb[44] * (T(5) / T(2)) + abb[42] * (T(9) / T(2)) + -z[11];
z[56] = abb[49] * (T(-7) / T(2)) + z[54] + -z[56] + z[107];
z[56] = abb[15] * z[56];
z[52] = -z[52] + z[56];
z[56] = abb[6] * z[62];
z[56] = z[52] + z[56] + z[63] + z[82] + z[84] + -z[91];
z[56] = abb[34] * z[56];
z[61] = abb[45] + -z[57] + z[71];
z[15] = abb[48] + -z[15] + z[43] + (T(1) / T(2)) * z[61];
z[15] = abb[4] * z[15];
z[43] = abb[15] * z[4];
z[61] = abb[13] * z[96];
z[25] = z[25] + z[61];
z[61] = abb[13] * abb[46];
z[43] = -z[15] + -z[25] + z[43] + (T(-1) / T(4)) * z[53] + z[61];
z[43] = z[2] * z[43];
z[61] = -abb[31] * z[74];
z[61] = z[61] + -2 * z[94] + z[98];
z[61] = abb[31] * z[61];
z[11] = z[11] + -z[13] + -z[57];
z[13] = abb[46] * (T(5) / T(4));
z[7] = -abb[48] + z[7] + (T(1) / T(2)) * z[11] + -z[13];
z[7] = z[7] * z[38];
z[11] = -z[22] * z[109];
z[7] = z[7] + z[10] + z[11] + z[61];
z[7] = abb[8] * z[7];
z[10] = z[49] + z[53];
z[11] = -abb[46] + z[31];
z[22] = z[11] * z[44];
z[31] = abb[8] * z[115];
z[10] = (T(-1) / T(2)) * z[10] + z[22] + z[31] + -z[35] + -z[48];
z[22] = -z[10] * z[113];
z[2] = -z[2] + z[38];
z[2] = z[2] * z[60];
z[31] = abb[34] + -abb[37];
z[35] = -2 * abb[36] + -z[31];
z[16] = z[16] * z[35];
z[35] = abb[31] * z[76];
z[2] = z[2] + z[16] + z[35];
z[2] = abb[11] * z[2];
z[11] = abb[17] * z[11];
z[16] = abb[26] * z[45];
z[11] = z[11] + z[16];
z[16] = abb[41] + -z[21] + z[33];
z[16] = abb[18] * z[16];
z[21] = abb[19] * z[34];
z[33] = abb[20] * z[39];
z[11] = (T(1) / T(2)) * z[11] + z[16] + -z[21] + -z[33];
z[16] = abb[55] * z[11];
z[21] = z[6] + z[52] + -z[55];
z[21] = abb[36] * z[21];
z[33] = -abb[36] + abb[37];
z[33] = z[33] * z[58];
z[4] = -z[4] * z[117];
z[0] = -z[0] + z[4] + z[33];
z[0] = z[0] * z[8];
z[4] = -z[49] + -z[51];
z[4] = (T(1) / T(4)) * z[4] + z[15];
z[4] = z[4] * z[38];
z[8] = abb[37] * z[90];
z[15] = -abb[45] + z[54];
z[15] = -abb[48] + abb[49] * (T(-7) / T(4)) + z[13] + (T(1) / T(2)) * z[15];
z[15] = abb[15] * z[15];
z[15] = z[15] + -z[18] + (T(-1) / T(2)) * z[48];
z[15] = z[15] * z[109];
z[18] = abb[53] + (T(1) / T(2)) * z[118];
z[18] = z[18] * z[37] * z[104];
z[31] = -abb[36] + abb[53] + -z[31];
z[31] = z[31] * z[45];
z[33] = -z[79] * z[109];
z[31] = z[31] + z[33];
z[31] = z[31] * z[100];
z[3] = abb[16] * z[3];
z[33] = -abb[33] * z[45] * z[80];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[7] + z[8] + z[9] + z[14] + z[15] + (T(-3) / T(2)) * z[16] + z[18] + z[21] + z[22] + z[23] + z[31] + z[33] + 3 * z[40] + z[42] + z[43] + z[56] + z[101];
z[1] = -z[28] + -z[50];
z[2] = -z[12] + z[73];
z[3] = z[2] + -z[19] + z[107];
z[4] = abb[8] * z[3];
z[7] = -2 * abb[41] + z[13] + z[105];
z[7] = abb[6] * z[7];
z[1] = (T(1) / T(2)) * z[1] + z[4] + z[7] + -z[46] + -z[48] + -2 * z[69];
z[1] = abb[28] * z[1];
z[4] = -abb[15] * z[17];
z[7] = -abb[6] * z[27];
z[4] = z[4] + z[7] + -z[29] + z[48] + -z[63] + z[69];
z[7] = 2 * abb[27];
z[4] = z[4] * z[7];
z[8] = -abb[13] * z[97];
z[8] = z[8] + 2 * z[25] + z[92];
z[6] = z[6] + 3 * z[8] + z[47] + z[88];
z[6] = abb[32] * z[6];
z[8] = abb[30] * z[3];
z[2] = -z[2] + z[59] + -z[67];
z[2] = abb[32] * z[2];
z[9] = z[30] + -z[32] + -z[64];
z[9] = abb[29] * z[9];
z[2] = z[2] + z[8] + z[9];
z[2] = abb[6] * z[2];
z[8] = 5 * z[48] + 2 * z[63] + z[83] + z[88];
z[8] = abb[29] * z[8];
z[3] = -abb[29] * z[3];
z[9] = abb[44] + 4 * abb[46] + z[75] + -z[86];
z[9] = abb[30] * z[9];
z[3] = z[3] + 2 * z[9] + -z[78];
z[3] = abb[8] * z[3];
z[9] = abb[41] + z[20] + z[103];
z[9] = z[9] * z[111];
z[7] = z[7] * z[27];
z[7] = z[7] + z[9] + z[108];
z[7] = abb[5] * z[7];
z[9] = -z[26] + -z[87];
z[9] = abb[30] * z[9];
z[12] = -z[24] * z[70];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + 6 * z[12] + -z[72] + 2 * z[77] + -z[81];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[39] * z[36];
z[3] = -abb[5] * z[34];
z[3] = z[3] + -z[10] + z[41] + z[116];
z[3] = abb[38] * z[3];
z[4] = abb[40] * z[11];
z[2] = z[2] + z[3] + (T(-1) / T(2)) * z[4];
z[1] = z[1] + 3 * z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_264_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-29.812321165349265736397436937642014984677930848764465516646227806"),stof<T>("21.120501999199564205866697115437052890912296680443702455469877917")}, std::complex<T>{stof<T>("6.6638457612309815136351091535161537510360849826576993189619766952"),stof<T>("-4.3292663793854642962410414458778339783202554025640303972787963686")}, std::complex<T>{stof<T>("-5.8611628528904734679645278935876502018742341813352511315133635778"),stof<T>("-1.8354233672926516309069811541707324986093303035217831288881022342")}, std::complex<T>{stof<T>("-2.7746497569405719883393697404087014389888469506069783755378657547"),stof<T>("-8.7340709293922015227353946246992615888657892883107180369487199436")}, std::complex<T>{stof<T>("8.6358126098310454563038976339963516408630811319422295070512293325"),stof<T>("10.5694942966848531536423757788699940874751195918325011658368221778")}, std::complex<T>{stof<T>("32.586832771024153115841736464661367024747806973898205762818911765"),stof<T>("-10.176453568299202168243448569686147120741720682222187379634005653")}, std::complex<T>{stof<T>("-21.160959375218185046798604894489504992692371121408418086891751401"),stof<T>("-5.865673783083879973420461566111248426459407782879456858558975723")}, std::complex<T>{stof<T>("17.271625219662090912607795267992703281726162263884459014102458665"),stof<T>("21.138988593369706307284751557739988174950239183665002331673644356")}, std::complex<T>{stof<T>("5.8613010041561580768595981069769996007932050068084892608785453734"),stof<T>("-0.374554134215508883980872766880911682695456406389013909999050086")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_264_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_264_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("26.186762785843156371612863720404858726247262629530927245005616939"),stof<T>("-0.66459106497212509293170202386920653165777274837504852969350685")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_264_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_264_DLogXconstant_part(base_point<T>, kend);
	value += f_4_264_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_264_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_264_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_264_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_264_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_264_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_264_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
