/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_775.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_775_abbreviated (const std::array<T,68>& abb) {
T z[79];
z[0] = abb[7] * abb[58];
z[1] = -abb[55] + abb[56];
z[2] = -abb[53] + z[1];
z[3] = abb[23] * z[2];
z[0] = z[0] + z[3];
z[3] = abb[22] * z[2];
z[4] = abb[24] * z[2];
z[5] = z[3] + z[4];
z[6] = abb[21] * z[2];
z[2] = abb[25] * z[2];
z[7] = z[2] + z[6];
z[8] = abb[5] + abb[15];
z[8] = abb[58] * z[8];
z[9] = abb[58] + abb[61];
z[10] = abb[60] + z[9];
z[11] = abb[59] + z[10];
z[12] = 2 * z[11];
z[13] = abb[1] * z[12];
z[8] = z[0] + z[5] + z[7] + -z[8] + z[13];
z[8] = abb[34] * z[8];
z[14] = abb[13] * abb[61];
z[15] = z[13] + -z[14];
z[16] = 2 * abb[60];
z[17] = z[9] + z[16];
z[18] = 2 * abb[59];
z[19] = z[17] + z[18];
z[19] = abb[2] * z[19];
z[20] = z[15] + z[19];
z[21] = abb[54] + -abb[57];
z[22] = z[1] + z[21];
z[23] = abb[26] * z[22];
z[24] = abb[16] * z[9];
z[23] = z[23] + z[24];
z[24] = abb[3] * abb[58];
z[25] = z[23] + -z[24];
z[26] = abb[53] + z[21];
z[27] = abb[22] * z[26];
z[28] = abb[5] * abb[61];
z[29] = z[27] + z[28];
z[30] = abb[24] * z[26];
z[31] = z[29] + z[30];
z[32] = abb[21] * z[26];
z[33] = z[31] + z[32];
z[34] = -z[2] + z[33];
z[35] = abb[15] * abb[58];
z[36] = z[20] + z[25] + -z[34] + -z[35];
z[36] = abb[31] * z[36];
z[8] = -z[8] + z[36];
z[36] = abb[10] * z[9];
z[37] = 2 * z[36];
z[38] = abb[6] * abb[61];
z[39] = z[37] + -z[38];
z[40] = abb[4] * abb[58];
z[41] = -z[25] + z[31] + z[39] + -z[40];
z[41] = abb[33] * z[41];
z[42] = abb[7] * abb[61];
z[26] = abb[23] * z[26];
z[26] = z[26] + z[42];
z[42] = z[26] + -z[38];
z[43] = -z[14] + z[37];
z[44] = abb[4] * abb[61];
z[45] = z[43] + -z[44];
z[46] = z[42] + z[45];
z[47] = 2 * abb[58];
z[48] = abb[4] * z[47];
z[31] = -z[31] + -z[46] + z[48];
z[49] = abb[30] * z[31];
z[50] = z[0] + z[24];
z[51] = abb[6] + abb[15];
z[52] = abb[4] + z[51];
z[52] = abb[58] * z[52];
z[52] = -z[50] + z[52];
z[53] = -z[7] + 2 * z[38];
z[54] = -z[37] + z[52] + z[53];
z[55] = abb[35] * z[54];
z[56] = abb[3] * abb[61];
z[15] = -z[15] + z[56];
z[57] = z[15] + -z[26];
z[58] = abb[29] * z[57];
z[41] = -z[8] + z[41] + z[49] + z[55] + -z[58];
z[55] = abb[21] * z[22];
z[58] = abb[23] * z[22];
z[59] = abb[7] * z[9];
z[58] = z[58] + z[59];
z[55] = z[55] + z[58];
z[59] = abb[24] * z[22];
z[59] = -z[23] + z[55] + z[59];
z[60] = abb[22] * z[22];
z[61] = abb[58] + -abb[61];
z[62] = abb[3] * z[61];
z[60] = z[13] + z[28] + z[59] + z[60] + z[62];
z[62] = abb[5] * abb[58];
z[19] = z[19] + -z[60] + z[62];
z[62] = 4 * z[19];
z[63] = abb[32] * z[62];
z[58] = z[23] + z[58];
z[64] = 2 * abb[4] + z[51];
z[64] = abb[58] * z[64];
z[53] = -z[45] + z[53] + -z[58] + z[64];
z[53] = 4 * z[53];
z[64] = -abb[28] * z[53];
z[41] = 4 * z[41] + z[63] + z[64];
z[41] = m1_set::bc<T>[0] * z[41];
z[63] = abb[49] * z[62];
z[64] = -abb[47] * z[53];
z[31] = 4 * z[31];
z[65] = abb[48] * z[31];
z[66] = 4 * z[54];
z[67] = abb[50] * z[66];
z[41] = abb[66] + abb[67] + z[41] + z[63] + z[64] + z[65] + z[67];
z[63] = 2 * z[14];
z[28] = 2 * z[28] + -z[44] + -z[63];
z[64] = -z[2] + z[32];
z[65] = 3 * abb[53] + -z[1] + 2 * z[21];
z[67] = -abb[22] + -abb[24];
z[65] = z[65] * z[67];
z[36] = 4 * z[36];
z[12] = abb[2] * z[12];
z[67] = abb[6] + 2 * abb[15];
z[68] = -abb[5] + z[67];
z[68] = abb[58] * z[68];
z[69] = -abb[61] + -z[47];
z[69] = abb[3] * z[69];
z[13] = -z[12] + z[13] + z[23] + -z[28] + -z[36] + -2 * z[64] + z[65] + z[68] + z[69];
z[13] = abb[31] * z[13];
z[23] = abb[5] + -z[51];
z[23] = abb[58] * z[23];
z[51] = abb[60] + abb[61];
z[64] = abb[59] + z[51];
z[64] = abb[58] + 2 * z[64];
z[64] = abb[2] * z[64];
z[5] = -z[2] + -z[5] + z[23] + z[24] + z[37] + -z[63] + z[64];
z[23] = 2 * abb[34];
z[5] = z[5] * z[23];
z[5] = z[5] + z[13];
z[5] = abb[31] * z[5];
z[13] = -z[14] + z[29];
z[63] = abb[14] * z[9];
z[64] = z[13] + -z[63];
z[65] = abb[2] * z[9];
z[25] = -z[25] + z[65];
z[51] = abb[9] * z[51];
z[65] = 2 * z[51];
z[68] = -z[4] + z[65];
z[10] = abb[0] * z[10];
z[69] = abb[58] + abb[60];
z[69] = abb[8] * z[69];
z[70] = 2 * z[69];
z[71] = abb[6] + -abb[15];
z[72] = -abb[4] + -z[71];
z[72] = abb[58] * z[72];
z[72] = -4 * z[10] + -z[25] + -z[38] + -z[44] + -z[64] + z[68] + z[70] + z[72];
z[72] = abb[28] * z[72];
z[73] = abb[14] * abb[58];
z[74] = 2 * z[10];
z[68] = z[68] + z[73] + -z[74];
z[75] = -z[7] + z[35] + -z[50] + z[68];
z[75] = abb[35] * z[75];
z[76] = abb[14] * abb[61];
z[77] = z[70] + -z[74] + z[76];
z[13] = z[13] + z[26] + -z[77];
z[13] = abb[30] * z[13];
z[13] = -z[13] + z[75];
z[25] = -z[25] + -z[34] + z[35] + -z[43];
z[25] = abb[31] * z[25];
z[34] = -z[38] + z[43];
z[43] = -z[6] + -z[34] + z[52];
z[52] = abb[2] * abb[61];
z[75] = z[43] + -z[52];
z[78] = abb[34] * z[75];
z[78] = -z[13] + -z[25] + z[78];
z[51] = z[51] + z[69];
z[1] = 2 * abb[53] + -z[1] + z[21];
z[21] = abb[21] + abb[24];
z[1] = z[1] * z[21];
z[1] = -z[1] + 6 * z[10] + -4 * z[51] + z[58] + -z[63];
z[21] = -z[2] + -z[38];
z[51] = z[9] + -z[16];
z[51] = abb[2] * z[51];
z[21] = -z[1] + 2 * z[21] + z[36] + -z[48] + z[51];
z[21] = abb[33] * z[21];
z[36] = abb[2] * z[17];
z[51] = z[2] + -z[14];
z[58] = 2 * z[35];
z[1] = z[1] + z[36] + 2 * z[51] + -z[58];
z[1] = abb[32] * z[1];
z[1] = z[1] + z[21] + z[72] + 2 * z[78];
z[1] = abb[28] * z[1];
z[21] = abb[61] + z[16];
z[18] = z[18] + z[21] + z[47];
z[18] = abb[2] * z[18];
z[18] = z[18] + z[33] + z[45] + z[56] + -z[58];
z[18] = abb[31] * z[18];
z[45] = abb[4] + abb[15];
z[47] = abb[58] * z[45];
z[51] = abb[2] * abb[58];
z[46] = -z[33] + -z[46] + z[47] + -z[51];
z[47] = abb[28] * z[46];
z[56] = abb[4] + -abb[15];
z[58] = abb[58] * z[56];
z[20] = -z[20] + -z[38] + -z[58];
z[20] = abb[34] * z[20];
z[58] = z[32] + z[51] + z[58];
z[58] = abb[33] * z[58];
z[57] = -abb[32] * z[57];
z[18] = z[18] + z[20] + z[47] + -z[49] + z[57] + z[58];
z[20] = abb[29] * z[46];
z[18] = 2 * z[18] + z[20];
z[18] = abb[29] * z[18];
z[20] = abb[2] * z[16];
z[20] = -z[4] + z[20] + -z[24] + z[35] + -z[55] + -z[64] + -z[74];
z[20] = abb[33] * z[20];
z[35] = -z[14] + z[38];
z[47] = -z[2] + z[35] + z[52];
z[47] = abb[34] * z[47];
z[25] = -z[13] + z[25] + z[47];
z[20] = z[20] + 2 * z[25];
z[20] = abb[33] * z[20];
z[8] = -z[8] + z[13];
z[13] = 2 * z[24];
z[25] = z[13] + 2 * z[29] + -z[36] + z[59] + -z[63] + z[74];
z[25] = abb[33] * z[25];
z[19] = abb[32] * z[19];
z[8] = 2 * z[8] + z[19] + z[25];
z[8] = abb[32] * z[8];
z[19] = z[26] + z[27];
z[19] = -2 * z[19] + -z[28] + -z[30] + -z[39] + z[48] + z[77];
z[19] = prod_pow(abb[30], 2) * z[19];
z[0] = z[0] + z[7] + -z[38];
z[7] = abb[4] + z[67];
z[7] = abb[58] * z[7];
z[0] = -2 * z[0] + z[7] + -z[13] + -z[37] + z[68];
z[0] = abb[35] * z[0];
z[7] = -z[23] * z[54];
z[0] = z[0] + z[7];
z[0] = abb[35] * z[0];
z[7] = prod_pow(abb[34], 2) * z[75];
z[0] = z[0] + z[1] + z[5] + z[7] + z[8] + z[18] + z[19] + z[20];
z[1] = abb[58] + z[16];
z[1] = abb[2] * z[1];
z[1] = z[1] + -z[4] + -z[6] + z[40] + -z[50] + -z[70] + z[73];
z[1] = abb[39] * z[1];
z[5] = abb[2] * z[21];
z[5] = z[5] + -z[29] + -z[32] + -z[42] + -z[65] + z[76];
z[5] = abb[38] * z[5];
z[6] = z[44] + z[52];
z[7] = -z[6] + z[14] + z[30] + z[32] + z[77];
z[7] = abb[37] * z[7];
z[6] = z[6] + -z[15] + -z[33];
z[6] = abb[41] * z[6];
z[8] = -abb[58] * z[71];
z[8] = -z[2] + z[8] + -z[51] + z[68];
z[8] = abb[36] * z[8];
z[13] = abb[42] * z[46];
z[14] = abb[43] * z[43];
z[15] = -abb[58] + -z[21];
z[15] = abb[44] * z[15];
z[16] = abb[40] * abb[58];
z[18] = -abb[43] * abb[61];
z[15] = z[15] + z[16] + z[18];
z[15] = abb[2] * z[15];
z[16] = -abb[44] * z[56];
z[18] = abb[25] * abb[46];
z[16] = z[16] + z[18];
z[16] = abb[58] * z[16];
z[18] = abb[21] * abb[46] * abb[61];
z[1] = z[1] + z[5] + z[6] + z[7] + z[8] + z[13] + z[14] + z[15] + z[16] + z[18];
z[5] = -abb[5] + z[71];
z[5] = abb[58] * z[5];
z[2] = z[2] + z[3] + z[4] + z[5] + -z[24];
z[3] = abb[1] * z[11];
z[2] = 4 * z[2] + 8 * z[3];
z[2] = abb[40] * z[2];
z[3] = -abb[5] + z[45];
z[3] = abb[58] * z[3];
z[3] = z[3] + -z[12] + -z[34] + z[60];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[4] = -abb[64] * z[62];
z[5] = -abb[17] + abb[20];
z[5] = z[5] * z[9];
z[6] = abb[27] * z[22];
z[7] = abb[18] * z[17];
z[8] = abb[19] * z[61];
z[5] = z[5] + z[6] + z[7] + -z[8];
z[5] = 2 * z[5];
z[5] = abb[45] * z[5];
z[6] = abb[62] * z[53];
z[7] = abb[44] * abb[60];
z[8] = abb[44] * abb[61];
z[8] = z[7] + z[8];
z[8] = abb[9] * z[8];
z[9] = abb[44] * abb[58];
z[7] = z[7] + z[9];
z[7] = abb[8] * z[7];
z[7] = z[7] + z[8];
z[8] = -abb[65] * z[66];
z[9] = -abb[63] * z[31];
z[10] = -8 * z[10] + -4 * z[35];
z[10] = abb[44] * z[10];
z[0] = abb[51] + abb[52] + 2 * z[0] + 4 * z[1] + z[2] + (T(2) / T(3)) * z[3] + z[4] + z[5] + z[6] + 8 * z[7] + z[8] + z[9] + z[10];
return {z[41], z[0]};
}


template <typename T> std::complex<T> f_4_775_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-7.3061296264474758664919516205978361371242677354393911034758419912"),stof<T>("6.300373455586862118120648214657704964338918906046829282594375544")}, std::complex<T>{stof<T>("14.424682837915131424796857938137399889179835508085986581507258664"),stof<T>("20.670851120199880116984210044734263468150192377256738462763025403")}, std::complex<T>{stof<T>("7.859117179210422963589665546873658674845781852900161900017576952"),stof<T>("20.670851120199880116984210044734263468150192377256738462763025403")}, std::complex<T>{stof<T>("-7.3061296264474758664919516205978361371242677354393911034758419912"),stof<T>("6.300373455586862118120648214657704964338918906046829282594375544")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_775_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_775_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (-1 * v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return (abb[58] + abb[59] + abb[60]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[58] + -abb[59] + -abb[60];
z[1] = abb[29] + -abb[31];
return 8 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_775_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(2)) * (-8 + -5 * v[3] + v[5]) * (-v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (4 * (-v[3] + v[5])) / tend;


		return (abb[58] + abb[59] + abb[60]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[29] + abb[31];
z[1] = -abb[32] + abb[34];
z[0] = z[0] * z[1];
z[0] = abb[41] + z[0];
z[1] = -abb[58] + -abb[59] + -abb[60];
return 8 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_775_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[59] + abb[60] + abb[61]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[59] + abb[60] + abb[61];
z[1] = abb[31] + -abb[34];
return 8 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_775_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(2)) * (v[2] + v[3]) * (-8 + 5 * v[2] + 5 * v[3] + -4 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * (v[2] + v[3])) / tend;


		return (abb[59] + abb[60] + abb[61]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[29] + abb[32];
z[1] = -abb[31] + abb[34];
z[0] = z[0] * z[1];
z[0] = abb[40] + z[0];
z[1] = -abb[59] + -abb[60] + -abb[61];
return 8 * abb[12] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_775_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-9.164641211541796764127685634255617945918232437963978350659362605"),stof<T>("21.575584374546039739091214887501912652151697835318205572939116127")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,68> abb = {dl[0], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_21(k), f_2_23(k), f_2_28_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_11_im(k), T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_11_re(k), T{0}, T{0}};
abb[51] = SpDLog_f_4_775_W_16_Im(t, path, abb);
abb[52] = SpDLog_f_4_775_W_19_Im(t, path, abb);
abb[66] = SpDLog_f_4_775_W_16_Re(t, path, abb);
abb[67] = SpDLog_f_4_775_W_19_Re(t, path, abb);

                    
            return f_4_775_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_775_DLogXconstant_part(base_point<T>, kend);
	value += f_4_775_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_775_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_775_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_775_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_775_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_775_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_775_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
