/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_129.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_129_abbreviated (const std::array<T,54>& abb) {
T z[51];
z[0] = abb[28] + -abb[33];
z[1] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[39] + abb[41];
z[3] = z[1] + -z[2];
z[4] = abb[7] * (T(1) / T(2));
z[3] = z[3] * z[4];
z[5] = -abb[29] + abb[30];
z[6] = -abb[28] + abb[31];
z[7] = z[5] + -z[6];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = abb[39] + z[7];
z[9] = abb[0] * (T(1) / T(2));
z[10] = z[8] * z[9];
z[3] = z[3] + z[10];
z[10] = -abb[30] + z[0];
z[11] = abb[8] * z[10];
z[12] = -abb[28] + abb[29];
z[13] = abb[33] + z[12];
z[14] = abb[14] * z[13];
z[15] = -abb[6] * abb[30];
z[14] = -z[11] + z[14] + z[15];
z[15] = -abb[33] + abb[28] * (T(1) / T(2));
z[16] = abb[31] * (T(1) / T(2));
z[17] = z[15] + z[16];
z[18] = abb[3] * z[17];
z[19] = abb[31] + -abb[33];
z[20] = abb[11] * z[19];
z[14] = (T(1) / T(2)) * z[14] + z[18] + -z[20];
z[14] = m1_set::bc<T>[0] * z[14];
z[18] = m1_set::bc<T>[0] * z[19];
z[18] = -abb[41] + z[18];
z[18] = abb[5] * z[18];
z[21] = abb[6] * abb[40];
z[21] = z[18] + -z[21];
z[22] = abb[32] * m1_set::bc<T>[0];
z[23] = (T(1) / T(2)) * z[22];
z[24] = abb[8] + abb[14];
z[25] = abb[6] + -z[24];
z[25] = z[23] * z[25];
z[26] = abb[41] * (T(1) / T(2));
z[27] = z[24] * z[26];
z[28] = -z[5] + z[19];
z[29] = -m1_set::bc<T>[0] * z[28];
z[29] = abb[40] + abb[41] + z[29];
z[30] = abb[4] * (T(1) / T(2));
z[29] = z[29] * z[30];
z[1] = abb[9] * z[1];
z[31] = abb[39] * (T(1) / T(2));
z[32] = -abb[14] * z[31];
z[14] = -z[1] + z[3] + z[14] + (T(1) / T(2)) * z[21] + z[25] + z[27] + z[29] + z[32];
z[21] = abb[30] * (T(1) / T(2));
z[25] = -z[17] + z[21];
z[27] = m1_set::bc<T>[0] * z[25];
z[23] = -z[23] + z[27];
z[26] = z[23] + z[26];
z[26] = abb[1] * z[26];
z[14] = (T(1) / T(2)) * z[14] + -z[26];
z[14] = abb[47] * z[14];
z[27] = abb[3] + -abb[14];
z[29] = abb[8] * (T(1) / T(2));
z[32] = z[27] + -z[29];
z[33] = abb[39] * z[32];
z[34] = abb[40] + z[7];
z[35] = abb[3] * z[34];
z[36] = abb[40] + z[8];
z[37] = abb[15] * (T(1) / T(2));
z[38] = z[36] * z[37];
z[39] = abb[40] * z[29];
z[40] = abb[41] * z[27];
z[41] = m1_set::bc<T>[0] * z[20];
z[18] = z[18] + -z[33] + (T(-1) / T(2)) * z[35] + z[38] + -z[39] + z[40] + -z[41];
z[23] = abb[41] * (T(3) / T(2)) + z[23] + -z[31];
z[23] = abb[12] * z[23];
z[23] = z[23] + -z[26];
z[26] = abb[0] * (T(1) / T(4));
z[8] = z[8] * z[26];
z[7] = abb[39] + (T(1) / T(2)) * z[7];
z[7] = abb[10] * z[7];
z[8] = -z[7] + z[8];
z[18] = z[8] + (T(1) / T(2)) * z[18] + z[23];
z[31] = abb[45] + abb[46];
z[18] = z[18] * z[31];
z[33] = m1_set::bc<T>[0] * z[13];
z[33] = z[2] + -z[22] + z[33];
z[33] = abb[24] * z[33];
z[39] = m1_set::bc<T>[0] * z[10];
z[22] = -abb[40] + z[22];
z[2] = -z[2] + z[22] + z[39];
z[39] = abb[22] * z[2];
z[40] = abb[30] * m1_set::bc<T>[0];
z[22] = -z[22] + z[40];
z[40] = -abb[21] + -abb[23];
z[22] = z[22] * z[40];
z[40] = abb[25] * z[36];
z[22] = z[22] + -z[33] + z[39] + z[40];
z[33] = abb[42] + abb[43] + -abb[44];
z[39] = (T(-1) / T(4)) * z[33];
z[22] = z[22] * z[39];
z[40] = abb[3] + -abb[8];
z[41] = -abb[4] + z[40];
z[42] = abb[41] * z[41];
z[43] = -abb[2] + z[40];
z[44] = -abb[39] * z[43];
z[1] = -z[1] + z[42] + z[44];
z[1] = (T(1) / T(2)) * z[1] + z[3] + -z[7] + z[23];
z[1] = abb[48] * z[1];
z[3] = -abb[8] * abb[40];
z[3] = z[3] + -z[35];
z[7] = abb[2] + -abb[3] + z[29];
z[23] = abb[39] * z[7];
z[3] = (T(1) / T(2)) * z[3] + z[23] + z[38];
z[3] = (T(1) / T(2)) * z[3] + z[8];
z[3] = abb[49] * z[3];
z[8] = -abb[19] * z[36];
z[23] = -abb[17] * z[34];
z[34] = abb[39] + -abb[40];
z[34] = abb[16] * z[34];
z[8] = z[8] + z[23] + z[34];
z[8] = abb[50] * z[8];
z[23] = abb[20] * (T(1) / T(4));
z[23] = z[23] * z[33];
z[2] = -z[2] * z[23];
z[1] = z[1] + z[2] + z[3] + (T(1) / T(4)) * z[8] + z[14] + z[18] + z[22];
z[1] = (T(1) / T(4)) * z[1];
z[2] = prod_pow(abb[33], 2);
z[3] = abb[37] + z[2];
z[8] = prod_pow(abb[29], 2);
z[14] = (T(1) / T(2)) * z[8];
z[18] = prod_pow(abb[31], 2);
z[22] = prod_pow(abb[28], 2);
z[33] = (T(1) / T(2)) * z[22];
z[34] = -z[3] + -z[14] + (T(1) / T(2)) * z[18] + z[33];
z[34] = abb[3] * z[34];
z[35] = abb[28] * abb[33];
z[36] = -z[2] + z[35];
z[38] = abb[30] * abb[33];
z[38] = abb[37] + z[38];
z[16] = -abb[33] + z[16];
z[16] = abb[31] * z[16];
z[42] = z[16] + -z[36] + z[38];
z[42] = abb[8] * z[42];
z[44] = abb[29] + z[6];
z[44] = abb[29] * z[44];
z[0] = abb[31] * z[0];
z[45] = abb[30] * z[13];
z[0] = -z[0] + z[44] + -z[45];
z[36] = abb[51] + -z[0] + -z[36];
z[36] = abb[14] * z[36];
z[18] = -z[2] + z[18];
z[19] = abb[32] * z[19];
z[19] = -abb[35] + (T(-1) / T(2)) * z[18] + z[19];
z[19] = abb[5] * z[19];
z[18] = abb[11] * z[18];
z[45] = prod_pow(abb[30], 2);
z[45] = -z[8] + z[45];
z[45] = (T(1) / T(2)) * z[45];
z[46] = abb[52] + -z[45];
z[46] = abb[6] * z[46];
z[47] = prod_pow(m1_set::bc<T>[0], 2);
z[47] = (T(1) / T(6)) * z[47];
z[48] = abb[6] + -abb[8];
z[48] = z[47] * z[48];
z[49] = abb[3] + abb[14];
z[50] = abb[6] + -z[49];
z[50] = abb[36] * z[50];
z[49] = abb[34] * z[49];
z[34] = z[18] + z[19] + -z[34] + -z[36] + -z[42] + -z[46] + -z[48] + -z[49] + -z[50];
z[36] = abb[32] * (T(1) / T(2));
z[13] = -z[13] + z[36];
z[13] = abb[32] * z[13];
z[42] = abb[29] + abb[31] + -abb[33];
z[46] = z[21] + -z[42];
z[46] = abb[30] * z[46];
z[15] = abb[28] * z[15];
z[48] = abb[52] + z[47];
z[49] = -abb[35] + z[48];
z[3] = abb[36] + z[3] + z[13] + z[15] + z[16] + z[44] + z[46] + -z[49];
z[3] = z[3] * z[30];
z[12] = abb[30] * z[12];
z[16] = abb[28] * abb[31];
z[12] = z[12] + z[16] + -z[33] + z[47];
z[30] = abb[29] * (T(1) / T(2));
z[33] = z[6] + z[30];
z[33] = abb[29] * z[33];
z[44] = -abb[51] + z[33];
z[46] = z[12] + -z[44];
z[9] = z[9] * z[46];
z[8] = z[2] + z[8] + -z[22];
z[50] = abb[34] + -abb[36];
z[8] = abb[37] + (T(1) / T(2)) * z[8] + z[13] + -z[50];
z[13] = abb[51] + -abb[53] + z[8];
z[4] = z[4] * z[13];
z[4] = z[4] + z[9];
z[9] = -abb[30] + z[42];
z[9] = abb[14] * z[9];
z[13] = abb[6] * z[5];
z[9] = z[9] + z[11] + z[13];
z[11] = -z[17] + z[30];
z[11] = abb[3] * z[11];
z[13] = abb[32] * (T(1) / T(4));
z[30] = -z[13] * z[40];
z[9] = (T(1) / T(2)) * z[9] + z[11] + z[20] + z[30];
z[9] = abb[32] * z[9];
z[8] = abb[9] * z[8];
z[11] = abb[53] * (T(1) / T(2));
z[24] = -abb[4] + abb[5] + -z[24];
z[24] = z[11] * z[24];
z[29] = -abb[11] + abb[3] * (T(1) / T(2)) + z[29];
z[29] = abb[35] * z[29];
z[3] = z[3] + -z[4] + z[8] + z[9] + z[24] + z[29] + (T(-1) / T(2)) * z[34];
z[9] = abb[31] * abb[33];
z[9] = (T(3) / T(2)) * z[2] + -z[9] + z[38] + -z[47];
z[24] = z[9] + -z[35];
z[13] = z[13] + -z[25];
z[13] = abb[32] * z[13];
z[11] = -z[11] + z[13] + (T(1) / T(2)) * z[24];
z[11] = abb[1] * z[11];
z[15] = (T(1) / T(2)) * z[2] + z[15];
z[24] = abb[31] * z[17];
z[17] = abb[30] * z[17];
z[17] = -abb[34] + -abb[35] + -z[15] + z[17] + -z[24];
z[17] = abb[13] * z[17];
z[29] = abb[38] * (T(1) / T(8));
z[30] = abb[18] * z[29];
z[3] = (T(1) / T(2)) * z[3] + -z[11] + (T(1) / T(4)) * z[17] + z[30];
z[3] = abb[47] * z[3];
z[6] = -abb[29] + (T(-1) / T(2)) * z[6] + z[21];
z[6] = abb[30] * z[6];
z[17] = z[16] + -z[22];
z[6] = z[6] + (T(-1) / T(2)) * z[17] + -z[48];
z[17] = -z[6] + -z[33];
z[21] = abb[3] * z[17];
z[22] = -abb[28] + abb[30] + -abb[31];
z[22] = abb[30] * z[22];
z[16] = z[16] + z[22];
z[16] = -abb[52] + (T(1) / T(2)) * z[16];
z[22] = abb[8] * z[16];
z[21] = -z[21] + z[22];
z[18] = z[18] + z[21];
z[6] = -z[6] + -z[44];
z[22] = z[6] * z[37];
z[30] = abb[51] * z[32];
z[27] = -abb[5] + z[27];
z[27] = abb[53] * z[27];
z[32] = abb[11] * abb[35];
z[20] = abb[32] * z[20];
z[18] = (T(1) / T(2)) * z[18] + z[19] + -z[20] + z[22] + z[27] + -z[30] + z[32];
z[19] = -abb[51] + z[35];
z[9] = z[9] + -z[19];
z[9] = abb[53] * (T(-3) / T(2)) + (T(1) / T(2)) * z[9] + z[13];
z[9] = abb[12] * z[9];
z[12] = z[12] + -z[33];
z[12] = abb[51] + (T(1) / T(2)) * z[12];
z[12] = abb[10] * z[12];
z[9] = z[9] + -z[11] + z[12];
z[11] = z[26] * z[46];
z[13] = abb[16] + abb[17] + -abb[19];
z[13] = z[13] * z[29];
z[11] = z[11] + z[13];
z[13] = -z[9] + z[11] + (T(1) / T(2)) * z[18];
z[13] = -z[13] * z[31];
z[18] = abb[30] * z[25];
z[18] = -z[18] + -z[24] + z[49];
z[5] = abb[32] * z[5];
z[14] = z[5] + z[14] + -z[15] + z[18] + -z[50];
z[14] = abb[23] * z[14];
z[15] = abb[32] * z[28];
z[2] = abb[53] + -z[2] + z[19];
z[0] = -z[0] + -z[2] + z[15] + z[50];
z[0] = abb[24] * z[0];
z[10] = z[10] + z[36];
z[10] = abb[32] * z[10];
z[2] = -abb[37] + z[2] + -z[10] + z[18];
z[10] = abb[22] * z[2];
z[5] = -abb[36] + -z[5] + z[45] + -z[48];
z[5] = abb[21] * z[5];
z[15] = abb[25] * z[6];
z[18] = abb[26] * abb[38];
z[0] = z[0] + z[5] + -z[10] + -z[14] + z[15] + (T(-1) / T(2)) * z[18];
z[0] = -z[0] * z[39];
z[5] = abb[51] * z[43];
z[10] = -abb[53] * z[41];
z[5] = z[5] + z[8] + z[10];
z[4] = -z[4] + (T(1) / T(2)) * z[5] + z[9];
z[4] = abb[48] * z[4];
z[5] = -abb[51] * z[7];
z[5] = z[5] + (T(-1) / T(2)) * z[21] + -z[22];
z[5] = (T(1) / T(2)) * z[5] + -z[11] + z[12];
z[5] = abb[49] * z[5];
z[6] = abb[19] * z[6];
z[7] = abb[17] * z[17];
z[8] = -abb[51] + -z[16];
z[8] = abb[16] * z[8];
z[6] = z[6] + z[7] + z[8];
z[7] = -abb[3] + -abb[8] + -abb[15];
z[7] = abb[27] + (T(1) / T(4)) * z[7];
z[7] = abb[38] * z[7];
z[6] = (T(1) / T(2)) * z[6] + z[7];
z[6] = abb[50] * z[6];
z[2] = -z[2] * z[23];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + (T(1) / T(2)) * z[6] + z[13];
z[0] = (T(1) / T(4)) * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_129_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.32783505779352584141901041773387745058899685637351807678096670347"),stof<T>("-0.02117969308723342926587963376439328458141610239887590962529290064")}, std::complex<T>{stof<T>("-0.32783505779352584141901041773387745058899685637351807678096670347"),stof<T>("-0.02117969308723342926587963376439328458141610239887590962529290064")}, std::complex<T>{stof<T>("-0.039434361861106221413238151536700863639926158869808051179463352543"),stof<T>("-0.133957808662581341355372377224295301428277861762339930663699188624")}, std::complex<T>{stof<T>("0.09768604677786429431275133139085887213217589538973625879348237781"),stof<T>("-0.44884017236342461996103013408604024550381413551460746113004692871")}, std::complex<T>{stof<T>("0.028506725943667559253406397001870086321410769674888617946429444721"),stof<T>("-0.085291897119023604635171637206313103698356982662243759378095386177")}, std::complex<T>{stof<T>("-0.19757415930072218380369023884017404767300818427486720038558645755"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_129_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_129_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.18015282672072209931603483675635724138602115369846005316782506576"),stof<T>("-0.09057116692072558972886564779102762741615190556831182642412473628")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,54> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k)};

                    
            return f_4_129_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_129_DLogXconstant_part(base_point<T>, kend);
	value += f_4_129_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_129_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_129_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_129_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_129_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_129_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_129_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
