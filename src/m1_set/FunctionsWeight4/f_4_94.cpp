/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_94.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_94_abbreviated (const std::array<T,61>& abb) {
T z[100];
z[0] = abb[24] + abb[26];
z[1] = abb[22] + abb[25];
z[2] = z[0] + -z[1];
z[3] = abb[28] * (T(1) / T(2));
z[4] = z[2] * z[3];
z[5] = abb[24] * abb[29];
z[6] = abb[23] + abb[25];
z[7] = abb[22] + z[6];
z[8] = -abb[26] + z[7];
z[9] = abb[31] * z[8];
z[5] = z[5] + z[9];
z[10] = abb[24] + -abb[26] + z[1];
z[10] = abb[23] + (T(1) / T(2)) * z[10];
z[10] = abb[33] * z[10];
z[11] = abb[23] + abb[24];
z[12] = abb[30] * z[11];
z[8] = abb[24] + z[8];
z[13] = abb[32] * z[8];
z[4] = z[4] + z[5] + z[10] + -z[12] + (T(-1) / T(2)) * z[13];
z[4] = abb[32] * z[4];
z[10] = abb[28] + abb[33];
z[12] = z[10] * z[11];
z[14] = abb[25] * (T(1) / T(2)) + z[11];
z[14] = abb[30] * z[14];
z[15] = abb[24] + z[6];
z[16] = abb[29] * z[15];
z[12] = -z[12] + z[14] + -z[16];
z[12] = abb[30] * z[12];
z[14] = abb[28] + abb[29] * (T(1) / T(2));
z[16] = abb[24] + z[7];
z[14] = z[14] * z[16];
z[17] = abb[22] * abb[33];
z[18] = abb[31] * z[7];
z[14] = z[14] + -z[17] + -z[18];
z[14] = abb[29] * z[14];
z[17] = abb[23] + -abb[26];
z[3] = z[3] * z[17];
z[17] = abb[33] * (T(1) / T(2));
z[2] = z[2] * z[17];
z[2] = z[2] + z[3] + -z[9];
z[2] = abb[28] * z[2];
z[3] = abb[26] * (T(1) / T(2)) + -z[7];
z[18] = prod_pow(abb[31], 2);
z[3] = z[3] * z[18];
z[11] = abb[22] + z[11];
z[17] = z[11] * z[17];
z[9] = z[9] + z[17];
z[9] = abb[33] * z[9];
z[1] = abb[34] * z[1];
z[7] = abb[36] * z[7];
z[17] = abb[58] * z[15];
z[16] = abb[37] * z[16];
z[19] = abb[57] * z[0];
z[11] = abb[35] * z[11];
z[20] = abb[27] * abb[38];
z[1] = z[1] + -z[2] + z[3] + z[4] + -z[7] + -z[9] + -z[11] + -z[12] + -z[14] + -z[16] + z[17] + -z[19] + (T(1) / T(2)) * z[20];
z[2] = abb[55] + abb[56];
z[1] = z[1] * z[2];
z[3] = abb[43] * (T(-13) / T(6)) + abb[53] * (T(8) / T(3));
z[4] = abb[47] * (T(1) / T(2));
z[7] = abb[48] * (T(1) / T(6));
z[9] = 3 * abb[44];
z[11] = -abb[49] + abb[46] * (T(1) / T(2));
z[12] = abb[52] * (T(-1) / T(2)) + -z[11];
z[12] = abb[50] + abb[45] * (T(7) / T(3)) + -z[3] + z[4] + z[7] + -z[9] + (T(1) / T(3)) * z[12];
z[12] = abb[3] * z[12];
z[14] = abb[46] + abb[49];
z[16] = 13 * abb[51] + -z[14];
z[16] = abb[45] + abb[52] + (T(1) / T(2)) * z[16];
z[17] = abb[47] * (T(7) / T(6));
z[3] = abb[48] * (T(-5) / T(2)) + abb[44] * (T(-1) / T(2)) + z[3] + (T(1) / T(3)) * z[16] + -z[17];
z[3] = abb[8] * z[3];
z[16] = abb[54] * (T(1) / T(2)) + abb[50] * (T(2) / T(3));
z[11] = abb[47] * (T(-17) / T(6)) + abb[45] * (T(2) / T(3)) + abb[53] * (T(5) / T(3)) + -z[7] + z[11] + -z[16];
z[11] = abb[6] * z[11];
z[19] = -abb[49] + abb[52];
z[16] = abb[44] * (T(-5) / T(3)) + z[16] + -z[17] + (T(1) / T(2)) * z[19];
z[16] = abb[2] * z[16];
z[17] = 2 * abb[53];
z[20] = abb[45] + -abb[48];
z[21] = z[17] + z[20];
z[22] = 2 * abb[47];
z[23] = -abb[54] + z[22];
z[24] = z[21] + z[23];
z[25] = abb[14] * z[24];
z[26] = -abb[44] + abb[54];
z[27] = -abb[53] + z[26];
z[28] = abb[10] * z[27];
z[29] = z[25] + z[28];
z[8] = z[2] * z[8];
z[30] = abb[45] * (T(1) / T(2));
z[31] = abb[48] * (T(1) / T(2));
z[32] = z[30] + -z[31];
z[33] = -abb[52] + (T(1) / T(2)) * z[14];
z[34] = z[32] + z[33];
z[35] = abb[17] * z[34];
z[36] = -abb[44] + abb[45];
z[37] = -abb[46] + abb[52];
z[38] = abb[47] + z[37];
z[36] = (T(-7) / T(2)) * z[36] + (T(4) / T(3)) * z[38];
z[36] = abb[13] * z[36];
z[38] = abb[46] + -abb[49];
z[7] = -abb[50] + abb[47] * (T(-2) / T(3)) + abb[45] * (T(5) / T(6)) + z[7] + (T(1) / T(6)) * z[38];
z[7] = abb[4] * z[7];
z[39] = -abb[51] + -abb[54];
z[39] = -abb[53] + z[30] + (T(1) / T(2)) * z[39];
z[4] = abb[43] * (T(2) / T(3)) + z[4] + (T(1) / T(3)) * z[39];
z[4] = abb[1] * z[4];
z[39] = -abb[43] + z[26];
z[39] = abb[0] * z[39];
z[3] = z[3] + 13 * z[4] + z[7] + (T(1) / T(6)) * z[8] + z[11] + z[12] + z[16] + (T(-8) / T(3)) * z[29] + (T(1) / T(3)) * z[35] + z[36] + (T(13) / T(6)) * z[39];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[4] = 6 * abb[53];
z[7] = z[4] + z[20];
z[11] = abb[46] + 5 * abb[49];
z[12] = 4 * abb[47];
z[16] = -z[7] + z[11] + -z[12];
z[36] = abb[7] * z[16];
z[40] = 2 * abb[50];
z[41] = -z[38] + z[40];
z[42] = -z[21] + z[41];
z[43] = abb[15] * z[42];
z[44] = 2 * abb[54];
z[45] = z[21] + -z[44];
z[46] = z[12] + z[41] + z[45];
z[47] = abb[3] * z[46];
z[48] = -abb[50] + abb[54];
z[49] = 3 * abb[49];
z[50] = z[17] + z[48] + -z[49];
z[51] = 2 * abb[8];
z[50] = z[50] * z[51];
z[52] = 3 * abb[47];
z[53] = -abb[50] + abb[53];
z[54] = z[20] + z[52] + z[53];
z[55] = 4 * abb[6];
z[54] = z[54] * z[55];
z[56] = 2 * z[25];
z[47] = z[36] + z[43] + z[47] + z[50] + z[54] + z[56];
z[47] = abb[33] * z[47];
z[50] = 6 * abb[47];
z[54] = z[32] + (T(-3) / T(2)) * z[38] + z[50] + -z[53];
z[54] = abb[3] * z[54];
z[57] = abb[45] * (T(3) / T(2));
z[58] = (T(1) / T(2)) * z[38];
z[59] = abb[48] * (T(-3) / T(2)) + z[50] + z[53] + z[57] + -z[58];
z[59] = abb[8] * z[59];
z[60] = 5 * abb[50];
z[61] = 5 * abb[53];
z[62] = abb[46] + 11 * abb[49];
z[62] = 16 * abb[47] + -abb[54] + abb[45] * (T(11) / T(2)) + z[31] + -z[60] + -z[61] + (T(1) / T(2)) * z[62];
z[62] = abb[6] * z[62];
z[63] = 3 * abb[50];
z[64] = -z[58] + z[63];
z[65] = 3 * abb[53];
z[66] = z[32] + z[65];
z[67] = 3 * abb[54] + -z[64] + -z[66];
z[67] = abb[5] * z[67];
z[68] = abb[47] + -abb[53];
z[48] = z[48] + z[68];
z[69] = 4 * abb[15];
z[48] = z[48] * z[69];
z[54] = -z[25] + -z[48] + z[54] + z[59] + z[62] + z[67];
z[54] = abb[29] * z[54];
z[59] = z[40] + z[44];
z[62] = 8 * abb[47];
z[69] = -z[7] + -3 * z[38] + z[59] + z[62];
z[69] = abb[6] * z[69];
z[70] = z[20] + z[38];
z[71] = 4 * abb[54] + z[12];
z[72] = -z[4] + -z[40] + -z[70] + z[71];
z[72] = abb[15] * z[72];
z[73] = 6 * abb[50];
z[74] = -z[38] + z[73];
z[7] = -6 * abb[54] + z[7] + z[74];
z[7] = abb[5] * z[7];
z[72] = z[7] + z[72];
z[75] = 5 * abb[45] + abb[48];
z[59] = z[38] + z[59] + -z[75];
z[59] = abb[3] * z[59];
z[75] = z[12] + z[74] + -z[75];
z[76] = abb[4] * z[75];
z[77] = z[23] + z[41];
z[77] = z[51] * z[77];
z[59] = z[59] + z[69] + -z[72] + -z[76] + z[77];
z[69] = -abb[31] * z[59];
z[77] = -abb[3] + abb[8];
z[42] = z[42] * z[77];
z[78] = abb[6] * z[46];
z[42] = z[7] + -z[42] + z[48] + -z[56] + z[78];
z[48] = -abb[28] * z[42];
z[47] = z[47] + z[48] + z[54] + z[69];
z[47] = abb[29] * z[47];
z[48] = (T(-1) / T(2)) * z[11] + z[22] + z[66];
z[48] = abb[7] * z[48];
z[54] = 3 * abb[43];
z[69] = z[49] + -z[54];
z[79] = 2 * abb[51];
z[80] = 2 * abb[48] + -z[22] + z[69] + -z[79];
z[80] = abb[12] * z[80];
z[48] = z[48] + z[80];
z[81] = -z[43] + z[78];
z[82] = 2 * abb[43];
z[83] = -z[44] + z[82];
z[84] = abb[45] + z[17] + z[38] + z[83];
z[85] = 3 * abb[48];
z[86] = -z[79] + -z[84] + z[85];
z[86] = abb[8] * z[86];
z[30] = -abb[51] + z[30] + z[31];
z[87] = z[30] + -z[58];
z[88] = abb[16] * z[87];
z[61] = -abb[43] + 2 * abb[45] + -abb[48] + -abb[51] + -z[44] + z[61];
z[61] = abb[3] * z[61];
z[89] = abb[45] + -abb[51];
z[90] = -abb[54] + z[89];
z[91] = 10 * abb[47];
z[92] = -11 * abb[43] + 9 * abb[53] + -z[90] + -z[91];
z[92] = abb[1] * z[92];
z[93] = abb[43] + -abb[54];
z[94] = abb[50] + z[93];
z[95] = abb[9] * z[94];
z[61] = 5 * z[25] + -z[48] + z[61] + z[67] + -z[81] + z[86] + -z[88] + z[92] + z[95];
z[61] = abb[30] * z[61];
z[86] = z[17] + z[44];
z[92] = 3 * abb[45];
z[62] = -z[62] + z[74] + z[85] + -z[86] + -z[92];
z[62] = abb[6] * z[62];
z[70] = -z[17] + z[70] + z[71] + -z[73];
z[70] = abb[15] * z[70];
z[70] = z[7] + z[70];
z[11] = -z[11] + -z[20] + z[86];
z[11] = abb[8] * z[11];
z[24] = abb[3] * z[24];
z[24] = z[24] + z[56];
z[11] = z[11] + 2 * z[24] + z[36] + -z[62] + -z[70];
z[24] = -abb[29] * z[11];
z[62] = z[38] + z[45] + z[82];
z[62] = abb[8] * z[62];
z[71] = abb[53] + z[89] + z[93];
z[85] = abb[3] * z[71];
z[85] = z[25] + z[85] + -z[95];
z[86] = abb[54] + z[22];
z[93] = abb[43] + -z[65] + z[86] + -z[89];
z[96] = 2 * abb[1];
z[93] = z[93] * z[96];
z[79] = -abb[45] + z[79];
z[97] = -abb[48] + z[79];
z[98] = z[38] + z[97];
z[99] = abb[16] * z[98];
z[93] = z[93] + -z[99];
z[62] = z[62] + z[81] + -2 * z[85] + z[93];
z[10] = z[10] * z[62];
z[10] = z[10] + z[24] + z[61];
z[10] = abb[30] * z[10];
z[24] = abb[53] + -abb[54] + -z[40] + z[92];
z[24] = abb[3] * z[24];
z[24] = z[24] + z[25];
z[38] = -z[38] + z[52] + -z[53];
z[38] = z[38] * z[55];
z[46] = abb[8] * z[46];
z[24] = 2 * z[24] + -z[38] + z[43] + -z[46] + z[76];
z[24] = abb[29] * z[24];
z[38] = 2 * abb[44];
z[46] = -z[20] + z[38];
z[41] = z[41] + -z[44];
z[52] = z[41] + z[46];
z[52] = abb[3] * z[52];
z[53] = abb[44] + z[19] + -z[63] + z[86];
z[55] = 2 * abb[2];
z[53] = z[53] * z[55];
z[61] = 2 * z[28];
z[63] = -z[43] + z[61];
z[26] = -abb[50] + z[19] + z[26];
z[85] = z[26] * z[51];
z[52] = z[52] + z[53] + -z[63] + z[78] + z[85];
z[52] = abb[31] * z[52];
z[24] = z[24] + -z[52];
z[85] = -z[38] + z[82];
z[57] = abb[51] + z[31] + z[33] + -z[57] + -z[85];
z[57] = abb[3] * z[57];
z[55] = z[26] * z[55];
z[86] = z[55] + z[78];
z[96] = z[71] * z[96];
z[96] = -z[88] + z[96];
z[49] = abb[46] + -z[49];
z[49] = abb[52] + z[30] + (T(1) / T(2)) * z[49] + z[85];
z[49] = abb[8] * z[49];
z[49] = -2 * z[39] + z[49] + z[57] + z[86] + -z[96];
z[49] = abb[33] * z[49];
z[57] = -abb[30] * z[62];
z[31] = abb[45] * (T(-5) / T(2)) + z[22] + -z[31] + z[64];
z[31] = abb[4] * z[31];
z[62] = z[9] + -z[92];
z[22] = z[22] + 2 * z[37] + z[62];
z[22] = abb[13] * z[22];
z[31] = -z[22] + z[31];
z[23] = -abb[50] + -abb[53] + -z[23] + -z[32] + z[58];
z[23] = abb[6] * z[23];
z[26] = abb[2] * z[26];
z[71] = abb[1] * z[71];
z[23] = z[23] + -z[26] + z[71];
z[26] = abb[43] + z[32] + z[38] + -z[64];
z[26] = abb[3] * z[26];
z[32] = abb[43] + -abb[44];
z[64] = z[30] + z[32] + -z[33];
z[71] = -abb[8] * z[64];
z[26] = z[23] + z[26] + z[31] + -z[35] + z[39] + z[71];
z[26] = abb[32] * z[26];
z[30] = z[30] + z[33];
z[33] = z[30] + z[38] + z[83];
z[33] = -z[33] * z[77];
z[35] = 2 * z[95];
z[33] = -z[33] + z[35] + -z[55] + z[63] + -z[96];
z[63] = abb[28] * z[33];
z[71] = abb[28] + -abb[33];
z[77] = z[34] * z[71];
z[83] = 2 * abb[52];
z[14] = -z[14] + z[83];
z[85] = z[14] + -z[20];
z[92] = abb[31] * z[85];
z[77] = z[77] + z[92];
z[96] = -abb[17] * z[77];
z[26] = -z[24] + z[26] + z[49] + z[57] + z[63] + z[96];
z[26] = abb[32] * z[26];
z[40] = 3 * abb[46] + -abb[49] + -z[40] + z[44] + -z[46] + -z[83];
z[40] = abb[3] * z[40];
z[44] = -abb[44] + -abb[46] + 2 * abb[49] + -abb[52] + -z[44] + z[60];
z[44] = abb[8] * z[44];
z[46] = -11 * abb[44] + 9 * abb[50] + abb[54] + z[19] + -z[91];
z[46] = abb[2] * z[46];
z[31] = -z[28] + -z[31] + z[40] + -z[43] + z[44] + z[46] + z[67] + -z[78];
z[31] = z[18] * z[31];
z[40] = -abb[46] + abb[48];
z[9] = 10 * abb[45] + abb[49] + -z[9] + -z[40] + z[50] + -z[73] + -z[83];
z[9] = abb[3] * z[9];
z[37] = -z[12] + -4 * z[37] + z[62];
z[37] = abb[13] * z[37];
z[44] = abb[6] * z[75];
z[46] = abb[17] * z[85];
z[49] = z[46] + z[76];
z[57] = abb[44] + abb[47] + -abb[50];
z[60] = 6 * abb[2];
z[57] = z[57] * z[60];
z[60] = abb[8] * z[85];
z[8] = -z[8] + z[9] + z[37] + -z[44] + z[49] + -z[57] + -z[60];
z[9] = -abb[59] * z[8];
z[37] = -abb[58] * z[11];
z[23] = z[23] + -z[88];
z[44] = abb[3] * z[64];
z[57] = abb[44] + -z[58] + -z[66] + z[82];
z[57] = abb[8] * z[57];
z[44] = z[23] + z[44] + z[48] + z[57];
z[44] = abb[33] * z[44];
z[44] = z[44] + -z[52];
z[44] = abb[33] * z[44];
z[48] = -z[28] + z[95];
z[57] = z[32] + z[89];
z[57] = abb[3] * z[57];
z[32] = -z[19] + -z[32];
z[32] = abb[8] * z[32];
z[23] = z[23] + z[32] + -3 * z[48] + z[57] + -z[67];
z[23] = abb[28] * z[23];
z[32] = -abb[33] * z[33];
z[23] = z[23] + z[32] + z[52];
z[23] = abb[28] * z[23];
z[4] = 10 * abb[49] + -z[4] + z[40] + z[50] + -z[54] + -z[79];
z[4] = abb[8] * z[4];
z[16] = abb[6] * z[16];
z[32] = 4 * abb[51];
z[33] = 4 * abb[48] + -z[12] + -z[32] + -z[69];
z[33] = abb[12] * z[33];
z[40] = -abb[3] * z[98];
z[48] = abb[43] + z[68];
z[50] = abb[1] * z[48];
z[4] = z[4] + z[16] + z[33] + -z[36] + z[40] + -6 * z[50] + z[99];
z[4] = abb[35] * z[4];
z[16] = abb[36] * z[59];
z[33] = -abb[37] * z[42];
z[14] = -z[14] + z[38];
z[38] = z[14] + z[45];
z[38] = abb[8] * z[38];
z[27] = abb[3] * z[27];
z[25] = -z[25] + z[27];
z[27] = 2 * z[25] + z[38] + z[46] + -z[53] + z[61] + z[70];
z[40] = -abb[57] * z[27];
z[41] = z[41] + z[82] + -z[97];
z[41] = abb[3] * z[41];
z[42] = -z[51] * z[94];
z[41] = -z[35] + z[41] + z[42] + z[72] + -z[93];
z[41] = abb[34] * z[41];
z[42] = -abb[28] * z[77];
z[18] = -z[18] * z[34];
z[45] = abb[33] * z[92];
z[18] = z[18] + z[42] + z[45];
z[18] = abb[17] * z[18];
z[42] = -abb[18] + -abb[19];
z[30] = z[30] * z[42];
z[34] = -abb[21] * z[34];
z[42] = -abb[20] * z[87];
z[30] = z[30] + z[34] + z[42];
z[30] = abb[38] * z[30];
z[34] = prod_pow(abb[33], 2);
z[42] = prod_pow(abb[28], 2);
z[34] = z[34] + -2 * z[42];
z[34] = z[34] * z[39];
z[1] = abb[60] + z[1] + z[3] + z[4] + z[9] + z[10] + z[16] + z[18] + z[23] + z[26] + z[30] + z[31] + z[33] + z[34] + z[37] + z[40] + z[41] + z[44] + z[47];
z[3] = 4 * abb[1];
z[4] = z[3] * z[48];
z[9] = -z[25] + z[35];
z[9] = -z[4] + -z[7] + 2 * z[9] + -6 * z[28] + -z[38] + 4 * z[39] + z[43] + z[55];
z[9] = abb[28] * z[9];
z[10] = -z[36] + 2 * z[80];
z[16] = -z[56] + -z[95];
z[18] = -4 * abb[53] + abb[54] + -z[20] + z[82];
z[18] = abb[3] * z[18];
z[16] = 2 * z[16] + z[18];
z[18] = -5 * abb[48] + z[32] + z[84];
z[18] = abb[8] * z[18];
z[12] = 5 * abb[43] + z[12] + -z[65] + z[90];
z[3] = z[3] * z[12];
z[3] = z[3] + z[7] + z[10] + 2 * z[16] + z[18] + z[81];
z[3] = abb[30] * z[3];
z[4] = z[4] + z[86];
z[7] = -4 * abb[44] + -z[21] + z[74];
z[7] = abb[3] * z[7];
z[12] = -z[14] + z[21];
z[12] = abb[8] * z[12];
z[7] = z[4] + z[7] + z[12] + 2 * z[22] + -z[49] + -z[56] + -z[61];
z[7] = abb[32] * z[7];
z[6] = -abb[26] + z[6];
z[6] = abb[33] * z[6];
z[12] = abb[30] * z[15];
z[14] = abb[28] * z[0];
z[5] = z[5] + z[6] + -z[12] + -z[13] + z[14];
z[5] = -z[2] * z[5];
z[6] = abb[44] + z[17] + -z[19] + -z[54];
z[6] = abb[8] * z[6];
z[12] = -abb[44] + abb[53] + z[20];
z[12] = abb[3] * z[12];
z[6] = z[6] + z[12] + z[29];
z[4] = -z[4] + 2 * z[6] + -z[10];
z[4] = abb[33] * z[4];
z[6] = -z[71] * z[85];
z[6] = z[6] + z[92];
z[6] = abb[17] * z[6];
z[3] = z[3] + z[4] + z[5] + z[6] + z[7] + z[9] + z[24];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = -abb[41] * z[8];
z[5] = -abb[40] * z[11];
z[6] = -abb[39] * z[27];
z[7] = abb[40] * z[15];
z[0] = abb[39] * z[0];
z[0] = -z[0] + z[7];
z[0] = z[0] * z[2];
z[0] = abb[42] + z[0] + z[3] + z[4] + z[5] + z[6];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_94_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("65.879166082837796935572721603555096999493718859701565104530342898"),stof<T>("-62.043725838942075668588093994512772473442811046282338124687551489")}, std::complex<T>{stof<T>("11.818739223413243445745795987723437790666512983892190367890315121"),stof<T>("44.754399886010530852318458314120175237537482781549564337076155391")}, std::complex<T>{stof<T>("-13.021270994015708623865005644429316643937731182069318685197577669"),stof<T>("-40.770881982060263797409294286963742228363931147405017681445075007")}, std::complex<T>{stof<T>("-11.595246956722915450066907220024011012509770083197367548360795171"),stof<T>("-8.550799722671620200528872298323175527394160533667792478950411291")}, std::complex<T>{stof<T>("-18.688491362182091527113373370845541383594268123669497648083374903"),stof<T>("-42.699832963396815448111803220809056052745503021421371719876539208")}, std::complex<T>{stof<T>("-5.0868616221304144532389323202907086744574045354542976972389888906"),stof<T>("-6.3356073919658355856693634081766835552946287252264899872358874776")}, std::complex<T>{stof<T>("3.5233541977180355023414680173768846980898456852559759885164864596"),stof<T>("-11.5317422799914715205284351009741972532504716991494846242902983441")}, std::complex<T>{stof<T>("14.235614369537093677332968124518190850967208527247491741360852382"),stof<T>("24.811106568733956045617349238159990093643421220443962861420478247")}, std::complex<T>{stof<T>("28.047142876499479648595544777175144846893072748278675484952211386"),stof<T>("13.006971778289777983726806804056996473558405155221492303361916016")}, std::complex<T>{stof<T>("-63.301908878788412641598797524714208892981284069846555517884539182"),stof<T>("63.539658901733851567275641798393917143682286505912596354835844172")}, std::complex<T>{stof<T>("-15.249364149174099020409427900445254981112084466904179619284153667"),stof<T>("-8.439689959568424838107098532890253955337796255698246480042585109")}, std::complex<T>{stof<T>("1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("-1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("-1.5961736222681129079204730221135863214600019276878811243393385392")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_94_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_94_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(112)) * (-v[3] + v[5]) * ((56 + -84 * m1_set::bc<T>[2]) * v[0] + 2 * (112 * m1_set::bc<T>[1] + -84 * m1_set::bc<T>[2] + 56 * m1_set::bc<T>[4] + -28 * v[2] + -21 * v[3] + 8 * v[4] + -7 * v[5] + 28 * m1_set::bc<T>[1] * (v[3] + v[5])) + 7 * (4 * (-2 + m1_set::bc<T>[2]) * v[1] + 4 * m1_set::bc<T>[4] * (v[3] + v[5]) + -3 * m1_set::bc<T>[2] * (v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (-(4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (-1 * v[3] + v[5])) / tend;


		return (abb[46] + -2 * abb[47] + -abb[49] + -2 * abb[50] + abb[54]) * (-2 * t * c[0] + c[1]) * (T(-1) / T(2));
	}
	{
T z[6];
z[0] = prod_pow(abb[31], 2);
z[0] = 2 * abb[34] + -2 * abb[37] + -5 * z[0];
z[1] = abb[47] + abb[50];
z[1] = abb[46] + -abb[49] + abb[54] + -2 * z[1];
z[0] = z[0] * z[1];
z[2] = abb[31] * z[1];
z[3] = abb[29] * z[1];
z[4] = z[2] + -z[3];
z[5] = abb[28] + abb[32] + -abb[33];
z[5] = 2 * z[5];
z[4] = z[4] * z[5];
z[2] = 4 * z[2] + z[3];
z[2] = abb[29] * z[2];
z[1] = 4 * z[1];
z[1] = -abb[36] * z[1];
z[0] = z[0] + z[1] + z[2] + z[4];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_94_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[3] + v[5])) / tend;


		return (abb[46] + -2 * abb[47] + -abb[49] + -2 * abb[50] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[29] + -abb[31];
z[1] = abb[47] + abb[50];
z[1] = abb[46] + -abb[49] + abb[54] + -2 * z[1];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_94_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-91.839574159149954502048497615416143478191699154082212461626411046"),stof<T>("13.437294422386908445724416362317914335742764758470218670248136872")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,61> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[42] = SpDLog_f_4_94_W_16_Im(t, path, abb);
abb[60] = SpDLog_f_4_94_W_16_Re(t, path, abb);

                    
            return f_4_94_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_94_DLogXconstant_part(base_point<T>, kend);
	value += f_4_94_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_94_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_94_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_94_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_94_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_94_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_94_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
