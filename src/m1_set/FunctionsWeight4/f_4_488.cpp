/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_488.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_488_abbreviated (const std::array<T,29>& abb) {
T z[25];
z[0] = abb[21] + -abb[23];
z[1] = -abb[19] + z[0];
z[1] = abb[20] + (T(1) / T(2)) * z[1];
z[2] = abb[2] * (T(1) / T(2));
z[2] = z[1] * z[2];
z[3] = abb[24] + abb[25];
z[4] = abb[8] * z[3];
z[2] = z[2] + (T(-1) / T(4)) * z[4];
z[4] = abb[19] + abb[20] + abb[21] + -abb[22];
z[4] = abb[0] * z[4];
z[5] = abb[7] * z[3];
z[6] = abb[19] + abb[23];
z[6] = -7 * abb[21] + 5 * z[6];
z[6] = -7 * abb[20] + abb[22] + (T(1) / T(2)) * z[6];
z[6] = abb[3] * z[6];
z[7] = abb[23] + abb[21] * (T(-7) / T(3));
z[7] = abb[20] * (T(-11) / T(6)) + abb[22] * (T(2) / T(3)) + (T(1) / T(2)) * z[7];
z[7] = abb[1] * z[7];
z[8] = abb[22] + abb[23];
z[9] = -abb[21] + (T(1) / T(2)) * z[8];
z[10] = abb[20] * (T(-1) / T(2)) + (T(1) / T(3)) * z[9];
z[10] = abb[4] * z[10];
z[8] = -2 * abb[21] + z[8];
z[11] = abb[20] + (T(-1) / T(3)) * z[8];
z[11] = abb[6] * z[11];
z[5] = -z[2] + (T(2) / T(3)) * z[4] + (T(1) / T(4)) * z[5] + (T(1) / T(6)) * z[6] + z[7] + z[10] + z[11];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[6] = (T(1) / T(2)) * z[3];
z[7] = abb[7] * z[6];
z[4] = z[4] + z[7];
z[2] = -z[2] + (T(1) / T(2)) * z[4];
z[10] = abb[19] + -5 * abb[21] + 3 * abb[23];
z[10] = abb[22] + (T(1) / T(2)) * z[10];
z[11] = 2 * abb[20];
z[12] = (T(1) / T(2)) * z[10] + -z[11];
z[12] = abb[3] * z[12];
z[9] = abb[20] * (T(3) / T(2)) + -z[9];
z[13] = abb[6] * z[9];
z[8] = -3 * abb[20] + z[8];
z[14] = abb[1] * z[8];
z[12] = z[2] + z[12] + z[13] + z[14];
z[15] = abb[13] * z[12];
z[16] = 2 * z[8];
z[16] = abb[3] * z[16];
z[17] = abb[6] * z[8];
z[18] = abb[4] * z[8];
z[16] = -3 * z[14] + -z[16] + z[17] + z[18];
z[19] = abb[12] * z[16];
z[15] = z[15] + z[19];
z[19] = -abb[19] + -11 * abb[21] + 5 * abb[23];
z[19] = 3 * abb[22] + (T(1) / T(2)) * z[19];
z[20] = 4 * abb[20];
z[19] = (T(1) / T(2)) * z[19] + -z[20];
z[19] = abb[3] * z[19];
z[21] = 2 * z[14];
z[2] = -z[2] + z[13] + -z[18] + z[19] + z[21];
z[13] = abb[11] * z[2];
z[6] = abb[8] * z[6];
z[19] = abb[2] * z[1];
z[6] = z[6] + -z[19];
z[19] = 3 * abb[21];
z[22] = -abb[23] + z[19];
z[23] = -abb[19] + -z[22];
z[11] = abb[22] + -z[11] + (T(1) / T(2)) * z[23];
z[11] = abb[0] * z[11];
z[23] = 2 * abb[3];
z[24] = -abb[19] + abb[20];
z[23] = z[23] * z[24];
z[11] = z[6] + z[11] + -z[14] + z[23];
z[11] = abb[14] * z[11];
z[11] = z[11] + z[13] + z[15];
z[11] = abb[14] * z[11];
z[13] = -abb[11] + -abb[13];
z[12] = abb[11] * z[12] * z[13];
z[13] = -abb[11] * z[16];
z[8] = abb[3] * z[8];
z[14] = 5 * abb[20] + -2 * abb[22] + z[22];
z[14] = abb[1] * z[14];
z[14] = -z[8] + z[14] + z[18];
z[14] = abb[12] * z[14];
z[13] = z[13] + z[14];
z[13] = abb[12] * z[13];
z[14] = -abb[26] * z[16];
z[18] = abb[2] + abb[3];
z[22] = (T(3) / T(2)) * z[3];
z[18] = z[18] * z[22];
z[22] = 3 * abb[19] + abb[21] + abb[23];
z[22] = -abb[22] + (T(1) / T(2)) * z[22];
z[22] = abb[7] * z[22];
z[23] = abb[0] * z[3];
z[24] = abb[8] * z[1];
z[18] = z[18] + z[22] + z[23] + -3 * z[24];
z[3] = abb[10] * z[3];
z[9] = abb[9] * z[9];
z[3] = -z[3] + z[9] + (T(1) / T(2)) * z[18];
z[9] = abb[27] * z[3];
z[18] = abb[22] + 2 * abb[23] + -z[19] + -z[20];
z[18] = abb[1] * z[18];
z[8] = z[8] + -z[17] + z[18];
z[8] = abb[15] * z[8];
z[18] = abb[0] + -abb[3];
z[1] = z[1] * z[18];
z[0] = -abb[20] + -z[0];
z[0] = abb[1] * z[0];
z[0] = z[0] + z[1] + z[7];
z[0] = prod_pow(abb[13], 2) * z[0];
z[0] = abb[28] + z[0] + z[5] + z[8] + z[9] + z[11] + z[12] + z[13] + z[14];
z[1] = z[10] + -z[20];
z[1] = abb[3] * z[1];
z[1] = z[1] + z[4] + z[6] + -z[17] + z[21];
z[1] = abb[11] * z[1];
z[2] = -abb[14] * z[2];
z[1] = z[1] + z[2] + z[15];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[16];
z[3] = abb[17] * z[3];
z[1] = abb[18] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_488_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("1.5811365330484767496464284989633348145914419678243990423004201795"),stof<T>("-7.316650242258947503059593500842803694562876468963928804631194841")}, std::complex<T>{stof<T>("12.8937714599570745225939812153515860483008472544037144801749766562"),stof<T>("-4.9645254902285770771601595526223206387995799085955292747912181719")}, std::complex<T>{stof<T>("5.4610230348535843197426240493489216581972356868658896329286350201"),stof<T>("-5.7485670742387005524599708686958149907206787620516624514045437282")}, std::complex<T>{stof<T>("-9.0138849581519669524977856649659992046950535353622238895467618156"),stof<T>("6.5326086582488240277597821847693093426417776155077956280178692846")}, std::complex<T>{stof<T>("3.5528619232983826327551616156170775464978178484963342566181267954"),stof<T>("-0.7840415840101234752998113160734943519210988534561331766133255564")}, std::complex<T>{stof<T>("1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("-3.073952256592314428553215974881268771197457590719930264362430233")}, std::complex<T>{stof<T>("1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("-3.073952256592314428553215974881268771197457590719930264362430233")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_488_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_488_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[2] + v[3]) * (-8 + 3 * v[2] + 3 * v[3] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + 2 * m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[2] + v[3])) / tend;


		return (abb[20] + abb[21] + -abb[23]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[13], 2);
z[1] = prod_pow(abb[12], 2);
z[0] = -abb[15] + z[0] + -z[1];
z[1] = abb[20] + abb[21] + -abb[23];
return abb[5] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_488_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_488_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-4.7787343731488556790487328872531747889095522697720262960103428624"),stof<T>("10.2179893599971016339217166762493630290632784074806564368698506647")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_13(k), f_2_4_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_488_W_19_Im(t, path, abb);
abb[28] = SpDLog_f_4_488_W_19_Re(t, path, abb);

                    
            return f_4_488_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_488_DLogXconstant_part(base_point<T>, kend);
	value += f_4_488_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_488_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_488_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_488_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_488_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_488_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_488_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
