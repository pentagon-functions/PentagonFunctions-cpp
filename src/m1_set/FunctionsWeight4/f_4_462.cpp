/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_462.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_462_abbreviated (const std::array<T,60>& abb) {
T z[61];
z[0] = abb[27] + -abb[32];
z[1] = -abb[29] + z[0];
z[2] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[37] + abb[39];
z[4] = abb[31] * m1_set::bc<T>[0];
z[2] = z[2] + -z[3] + z[4];
z[5] = abb[13] * z[2];
z[6] = abb[30] * (T(1) / T(2));
z[7] = -abb[29] + z[6];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = m1_set::bc<T>[0] * (T(1) / T(2));
z[9] = abb[28] * z[8];
z[7] = -abb[38] + z[7] + z[9];
z[7] = abb[12] * z[7];
z[10] = -abb[27] + abb[29];
z[10] = m1_set::bc<T>[0] * z[10];
z[3] = z[3] + z[10];
z[3] = abb[4] * z[3];
z[10] = abb[30] + -abb[32];
z[11] = m1_set::bc<T>[0] * z[10];
z[12] = abb[38] + -abb[39];
z[13] = z[11] + z[12];
z[13] = abb[7] * z[13];
z[14] = -abb[16] + abb[17];
z[15] = abb[40] * (T(1) / T(2));
z[16] = z[14] * z[15];
z[17] = z[0] + z[6];
z[17] = m1_set::bc<T>[0] * z[17];
z[18] = -abb[37] + z[9];
z[17] = z[17] + -z[18];
z[19] = abb[0] * z[17];
z[3] = z[3] + z[5] + z[7] + -z[13] + -z[16] + -z[19];
z[5] = abb[29] * m1_set::bc<T>[0];
z[7] = abb[31] * z[8];
z[5] = -z[5] + z[7] + z[9];
z[8] = abb[38] * (T(1) / T(2));
z[13] = abb[39] * (T(1) / T(2)) + -z[5] + z[8];
z[16] = abb[8] * z[13];
z[20] = (T(1) / T(2)) * z[3] + z[16];
z[20] = abb[48] * z[20];
z[3] = abb[42] + z[3];
z[21] = abb[6] * (T(1) / T(2));
z[22] = -z[12] * z[21];
z[3] = (T(1) / T(2)) * z[3] + -z[16] + z[22];
z[3] = abb[51] * z[3];
z[4] = z[4] + z[9];
z[9] = -abb[32] + z[6];
z[16] = -abb[29] + z[9];
z[16] = m1_set::bc<T>[0] * z[16];
z[16] = -abb[38] + z[4] + z[16];
z[22] = abb[21] + abb[23];
z[16] = z[16] * z[22];
z[23] = abb[24] * z[17];
z[16] = z[16] + z[23];
z[23] = abb[22] * z[13];
z[16] = (T(1) / T(2)) * z[16] + -z[23];
z[2] = abb[19] * z[2];
z[23] = (T(1) / T(2)) * z[2] + z[16];
z[24] = abb[25] * (T(1) / T(4));
z[25] = -abb[40] * z[24];
z[25] = -z[23] + z[25];
z[25] = abb[45] * z[25];
z[26] = abb[29] * (T(1) / T(2));
z[27] = abb[32] + abb[30] * (T(-3) / T(4)) + z[26];
z[28] = abb[28] * (T(-1) / T(4)) + z[27];
z[28] = m1_set::bc<T>[0] * z[28];
z[7] = -z[7] + z[8] + z[28];
z[7] = abb[51] * z[7];
z[8] = abb[29] + z[6];
z[8] = m1_set::bc<T>[0] * z[8];
z[4] = -z[4] + z[8];
z[8] = abb[38] + z[4];
z[8] = abb[48] * z[8];
z[7] = z[7] + (T(1) / T(2)) * z[8];
z[7] = abb[5] * z[7];
z[3] = z[3] + z[7] + z[20] + z[25];
z[7] = abb[14] * z[17];
z[8] = -abb[27] + z[6];
z[20] = m1_set::bc<T>[0] * z[8];
z[18] = z[18] + z[20];
z[20] = abb[6] * z[18];
z[25] = abb[5] * z[11];
z[7] = z[7] + z[20] + z[25];
z[13] = abb[1] * z[13];
z[20] = abb[40] * (T(1) / T(4));
z[25] = -abb[15] + abb[18];
z[20] = -z[20] * z[25];
z[28] = abb[0] * (T(1) / T(2));
z[29] = z[17] * z[28];
z[30] = abb[7] * z[12];
z[20] = z[20] + z[29] + -z[30];
z[20] = (T(-1) / T(4)) * z[7] + -z[13] + (T(1) / T(2)) * z[20];
z[20] = abb[49] * z[20];
z[5] = -abb[38] + z[5];
z[29] = abb[49] + abb[50] + abb[51];
z[29] = abb[11] * z[29];
z[5] = -z[5] * z[29];
z[5] = z[5] + z[20];
z[20] = -z[15] * z[25];
z[7] = -z[7] + z[19] + z[20];
z[7] = abb[52] * z[7];
z[15] = abb[53] * z[15];
z[12] = abb[48] * z[12];
z[12] = z[12] + z[15];
z[12] = abb[6] * z[12];
z[4] = abb[39] + z[4];
z[15] = abb[43] + abb[46];
z[19] = -abb[44] + z[15];
z[20] = -abb[45] + z[19];
z[20] = abb[20] * z[20];
z[4] = -z[4] * z[20];
z[31] = abb[42] * abb[47];
z[4] = -z[4] + -z[7] + -z[12] + z[31];
z[2] = (T(-1) / T(32)) * z[2] + (T(-1) / T(16)) * z[16];
z[2] = abb[44] * z[2];
z[7] = (T(1) / T(16)) * z[15];
z[7] = z[7] * z[23];
z[12] = (T(-1) / T(8)) * z[13] + (T(-1) / T(16)) * z[30];
z[12] = abb[50] * z[12];
z[13] = -abb[15] * z[18];
z[15] = abb[18] * z[17];
z[11] = -abb[16] * z[11];
z[11] = z[11] + z[13] + z[15];
z[13] = abb[0] + abb[14];
z[15] = abb[26] * (T(-1) / T(16)) + (T(1) / T(64)) * z[13];
z[15] = abb[40] * z[15];
z[11] = (T(1) / T(32)) * z[11] + z[15];
z[11] = abb[53] * z[11];
z[15] = abb[25] * abb[40] * z[19];
z[2] = abb[58] + z[2] + (T(1) / T(16)) * z[3] + (T(-1) / T(32)) * z[4] + (T(1) / T(8)) * z[5] + z[7] + z[11] + z[12] + (T(1) / T(64)) * z[15];
z[3] = abb[31] * (T(1) / T(2));
z[4] = z[1] + z[3];
z[4] = abb[31] * z[4];
z[5] = abb[35] + z[4];
z[7] = -abb[27] + abb[30];
z[11] = z[7] + z[26];
z[12] = abb[29] * z[11];
z[15] = -abb[55] + abb[56];
z[16] = abb[28] * z[10];
z[17] = prod_pow(abb[32], 2);
z[18] = (T(1) / T(2)) * z[17];
z[23] = abb[36] + z[18];
z[30] = abb[27] * (T(1) / T(2));
z[31] = -abb[32] + z[30];
z[32] = abb[27] * z[31];
z[33] = prod_pow(m1_set::bc<T>[0], 2);
z[34] = (T(1) / T(3)) * z[33];
z[35] = abb[34] + -z[5] + -z[12] + -z[15] + z[16] + -z[23] + -z[32] + -z[34];
z[36] = abb[7] * (T(1) / T(2));
z[35] = z[35] * z[36];
z[36] = abb[28] + -abb[29];
z[37] = z[10] + -z[36];
z[37] = abb[31] * z[37];
z[38] = -abb[33] + abb[35];
z[39] = abb[27] * abb[32];
z[40] = z[38] + -z[39];
z[1] = abb[28] * z[1];
z[41] = abb[30] * z[0];
z[42] = -abb[54] + abb[56];
z[43] = abb[29] + z[7];
z[44] = abb[29] * z[43];
z[1] = -z[1] + z[17] + z[37] + z[40] + z[41] + z[42] + -z[44];
z[37] = abb[13] * z[1];
z[45] = prod_pow(abb[27], 2);
z[46] = -z[17] + z[45];
z[46] = (T(1) / T(2)) * z[46];
z[47] = prod_pow(abb[29], 2);
z[48] = (T(1) / T(2)) * z[47];
z[4] = abb[36] + z[4] + z[38] + -z[42] + -z[46] + z[48];
z[38] = abb[4] * (T(1) / T(2));
z[4] = z[4] * z[38];
z[38] = abb[28] * (T(1) / T(2));
z[42] = z[7] * z[38];
z[6] = abb[27] * z[6];
z[49] = -z[6] + z[34];
z[50] = abb[29] * z[7];
z[51] = (T(1) / T(2)) * z[45];
z[52] = abb[33] + z[51];
z[50] = abb[55] + -z[42] + z[49] + z[50] + z[52];
z[53] = abb[12] * (T(1) / T(2));
z[50] = z[50] * z[53];
z[53] = z[9] + z[30];
z[54] = abb[30] * z[53];
z[55] = abb[28] * z[53];
z[32] = abb[54] + -z[32] + z[55];
z[55] = (T(1) / T(6)) * z[33];
z[54] = -z[18] + z[32] + -z[54] + z[55];
z[54] = z[28] * z[54];
z[56] = z[17] + z[45];
z[56] = (T(1) / T(2)) * z[56];
z[12] = abb[36] + z[12];
z[57] = abb[27] * abb[30];
z[5] = z[5] + z[12] + z[56] + -z[57];
z[5] = abb[9] * z[5];
z[58] = abb[57] * (T(1) / T(4));
z[14] = z[14] * z[58];
z[4] = z[4] + -z[5] + z[14] + z[35] + (T(1) / T(2)) * z[37] + z[50] + z[54];
z[5] = -abb[34] + z[55];
z[14] = abb[30] * abb[32];
z[10] = abb[31] * z[10];
z[10] = abb[35] + -z[5] + z[10] + -z[14] + z[17];
z[14] = abb[3] * (T(1) / T(2));
z[10] = z[10] * z[14];
z[14] = abb[28] * abb[29];
z[35] = abb[31] * z[36];
z[35] = -abb[56] + z[35];
z[14] = abb[55] + z[14] + (T(5) / T(6)) * z[33] + -z[35] + -z[47];
z[37] = abb[8] * z[14];
z[10] = z[10] + (T(1) / T(2)) * z[37];
z[37] = -z[3] + z[43];
z[43] = abb[31] * z[37];
z[12] = -z[12] + z[43];
z[8] = abb[30] * z[8];
z[8] = z[8] + -z[12] + z[51];
z[43] = z[8] + z[34];
z[50] = abb[1] * z[43];
z[50] = z[4] + -z[10] + z[50];
z[50] = abb[48] * z[50];
z[51] = -abb[56] + z[17] + -z[51];
z[54] = abb[55] * (T(1) / T(2));
z[59] = abb[36] * (T(1) / T(2));
z[60] = z[54] + -z[59];
z[11] = -z[11] * z[26];
z[26] = z[3] * z[37];
z[31] = abb[30] * (T(1) / T(4)) + z[31];
z[31] = abb[30] * z[31];
z[11] = abb[34] + z[11] + z[26] + z[31] + (T(-1) / T(12)) * z[33] + (T(1) / T(2)) * z[51] + z[60];
z[11] = abb[6] * z[11];
z[4] = z[4] + z[10] + z[11];
z[4] = abb[51] * z[4];
z[10] = -z[17] + z[47];
z[11] = -abb[28] + z[0] + z[3];
z[17] = -z[3] * z[11];
z[26] = abb[35] * (T(1) / T(2));
z[27] = abb[27] * (T(1) / T(4)) + -z[27];
z[27] = abb[28] * z[27];
z[10] = abb[34] + abb[33] * (T(1) / T(2)) + (T(1) / T(4)) * z[10] + z[17] + -z[26] + z[27] + -z[34] + z[39] + -z[54] + (T(-3) / T(4)) * z[57] + -z[59];
z[10] = abb[51] * z[10];
z[17] = abb[33] + z[18] + z[45] + (T(-3) / T(2)) * z[57];
z[27] = abb[29] + (T(1) / T(2)) * z[7];
z[31] = -z[27] * z[38];
z[0] = -abb[29] + abb[31] * (T(1) / T(4)) + (T(1) / T(2)) * z[0] + z[38];
z[0] = abb[31] * z[0];
z[7] = abb[29] * (T(3) / T(4)) + z[7];
z[7] = abb[29] * z[7];
z[0] = z[0] + z[7] + (T(1) / T(2)) * z[17] + z[26] + z[31] + -z[55] + -z[60];
z[0] = abb[48] * z[0];
z[7] = z[5] + -z[12] + -z[41] + z[46];
z[7] = abb[47] * z[7];
z[0] = z[0] + z[7] + z[10];
z[0] = abb[5] * z[0];
z[1] = abb[19] * z[1];
z[7] = -abb[29] + z[53];
z[7] = abb[28] * z[7];
z[10] = abb[31] * z[11];
z[11] = (T(1) / T(2)) * z[33];
z[6] = z[6] + z[11];
z[12] = -abb[55] + z[48];
z[7] = z[6] + -z[7] + z[10] + -z[12] + z[23] + z[40];
z[7] = z[7] * z[22];
z[10] = abb[22] * z[14];
z[17] = z[32] + z[49];
z[23] = abb[24] * z[17];
z[1] = z[1] + z[7] + z[10] + -z[23];
z[7] = -abb[44] + abb[46];
z[10] = z[1] * z[7];
z[22] = abb[24] + z[22];
z[23] = abb[34] * z[22];
z[26] = abb[57] * (T(1) / T(2));
z[31] = abb[25] * z[26];
z[23] = -z[1] + z[23] + z[31];
z[23] = abb[45] * z[23];
z[30] = z[9] + -z[30];
z[30] = abb[30] * z[30];
z[31] = -abb[54] + z[30] + -z[34] + z[42] + z[56];
z[33] = abb[15] * z[31];
z[37] = -abb[18] * z[17];
z[16] = z[16] + z[39] + -z[57];
z[39] = z[16] + -z[55];
z[39] = abb[16] * z[39];
z[33] = z[33] + z[37] + z[39];
z[33] = abb[53] * z[33];
z[27] = abb[28] * z[27];
z[6] = z[6] + z[27] + -z[35] + -z[44] + -z[52];
z[6] = z[6] * z[20];
z[6] = z[6] + z[10] + z[23] + z[33];
z[10] = abb[43] * (T(1) / T(2));
z[1] = z[1] * z[10];
z[11] = z[11] + z[18] + z[30] + z[32];
z[11] = z[11] * z[28];
z[20] = abb[34] + z[31];
z[20] = z[20] * z[21];
z[5] = -z[5] + z[16];
z[16] = abb[5] * (T(1) / T(2));
z[5] = z[5] * z[16];
z[16] = abb[34] + z[17];
z[17] = abb[14] * (T(1) / T(2));
z[16] = z[16] * z[17];
z[9] = abb[30] * z[9];
z[9] = z[9] + z[18] + z[55];
z[17] = abb[34] + z[9];
z[18] = abb[2] * z[17];
z[23] = z[25] * z[58];
z[27] = abb[0] * abb[34];
z[5] = z[5] + -z[11] + z[16] + z[18] + z[20] + z[23] + -z[27];
z[11] = abb[1] * z[14];
z[16] = z[15] + z[55];
z[18] = -abb[7] * z[16];
z[11] = z[5] + z[11] + z[18];
z[11] = abb[49] * z[11];
z[14] = abb[50] * z[14];
z[18] = abb[47] * z[43];
z[14] = z[14] + z[18];
z[14] = abb[1] * z[14];
z[9] = abb[47] * z[9];
z[17] = abb[51] * z[17];
z[18] = abb[34] * abb[47];
z[17] = z[9] + z[17] + z[18];
z[17] = abb[2] * z[17];
z[16] = -abb[50] * z[16];
z[16] = -z[9] + z[16];
z[16] = abb[7] * z[16];
z[9] = -abb[0] * z[9];
z[7] = -z[7] * z[22];
z[18] = abb[16] + -z[25];
z[18] = abb[53] * z[18];
z[7] = z[7] + z[18];
z[10] = -z[10] * z[22];
z[18] = -abb[0] + -abb[7];
z[18] = abb[47] * z[18];
z[7] = (T(1) / T(2)) * z[7] + z[10] + z[18];
z[7] = abb[34] * z[7];
z[8] = -z[8] + z[15] + -z[55];
z[8] = abb[48] * z[8];
z[10] = -abb[53] * z[26];
z[8] = z[8] + z[10];
z[8] = z[8] * z[21];
z[10] = -z[19] * z[24];
z[13] = abb[26] + (T(-1) / T(4)) * z[13];
z[13] = abb[53] * z[13];
z[10] = z[10] + z[13];
z[10] = abb[57] * z[10];
z[0] = z[0] + z[1] + z[4] + (T(1) / T(2)) * z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[14] + z[16] + z[17] + z[50];
z[1] = z[3] * z[36];
z[3] = abb[29] * z[38];
z[1] = -z[1] + z[3] + -z[12] + z[34];
z[1] = -z[1] * z[29];
z[0] = (T(1) / T(2)) * z[0] + z[1];
z[1] = abb[52] * z[5];
z[3] = abb[47] + -abb[51];
z[3] = abb[59] * z[3];
z[0] = abb[41] + (T(1) / T(8)) * z[0] + (T(1) / T(16)) * z[1] + (T(1) / T(32)) * z[3];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_462_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, stof<T>("0.19445478182325857965854926342930396208173717667651950271598702203"), std::complex<T>{stof<T>("0.17719635505422959370303329117791937474040753852689526686436291469"),stof<T>("-0.63165923465736881544752894570639029867684773255603970204843585344")}, std::complex<T>{stof<T>("0.26309090279216278001407381946305763106540864060601367103901342582"),stof<T>("-0.38068040225873842530728415778143086555260350644490332516158241055")}, std::complex<T>{stof<T>("0.12797730314989006657016400038373510871120895438833118525733431523"),stof<T>("-0.68322367136259992182320903078175455831082304786216446223982724434")}, std::complex<T>{stof<T>("0.20531556988561892464562918779022462465493646848598145846365866456"),stof<T>("-0.36692548041253916053596210827137743567723796531177468035818837531")}, std::complex<T>{stof<T>("0.1351135996422727134439098190793225223541996862176824857816791106"),stof<T>("0.30254326910386149651592487300032369275821954141726113707824483379")}, std::complex<T>{stof<T>("0.052678935884231730299906118451924879625719695309925263090767030428"),stof<T>("0.119129763596743450607089913422324455774389719436512833921156798303")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_462_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_462_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[49] + abb[51] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[49] + abb[51] + abb[52];
z[1] = abb[30] + -abb[32];
return abb[10] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_462_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (4 + 4 * v[0] + -v[1] + v[2] + 2 * v[3] + -v[4] + -2 * v[5]) + m1_set::bc<T>[1] * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5]))) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(-1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[49] + abb[51] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = abb[27] + abb[32] + abb[30] * (T(-1) / T(2));
z[0] = abb[30] * z[0];
z[1] = abb[27] + abb[32] * (T(1) / T(2));
z[1] = abb[32] * z[1];
z[2] = abb[30] + -abb[32];
z[2] = abb[28] * z[2];
z[0] = z[0] + -z[1] + -z[2];
z[0] = -abb[34] + (T(1) / T(2)) * z[0];
z[1] = abb[49] + abb[51] + abb[52];
return abb[10] * (T(1) / T(8)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_462_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.28255429200904087779361517960953856897985361988895684852379685137"),stof<T>("-0.02533597840639754950538030421562829449461506953229206289013496404")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W20(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k), T{0}, T{0}};
abb[41] = SpDLog_f_4_462_W_20_Im(t, path, abb);
abb[42] = SpDLogQ_W_74(k,dl,dlr).imag();
abb[58] = SpDLog_f_4_462_W_20_Re(t, path, abb);
abb[59] = SpDLogQ_W_74(k,dl,dlr).real();

                    
            return f_4_462_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_462_DLogXconstant_part(base_point<T>, kend);
	value += f_4_462_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_462_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_462_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_462_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_462_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_462_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_462_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
