/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_515.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_515_abbreviated (const std::array<T,62>& abb) {
T z[128];
z[0] = abb[51] * (T(1) / T(4));
z[1] = abb[46] * (T(3) / T(4));
z[2] = z[0] + z[1];
z[3] = abb[47] * (T(1) / T(3));
z[4] = abb[45] + abb[50];
z[5] = z[3] + -z[4];
z[6] = abb[42] + -abb[49];
z[7] = abb[52] * (T(1) / T(3));
z[8] = abb[43] * (T(-7) / T(6)) + abb[44] * (T(-5) / T(12)) + z[2] + (T(-1) / T(4)) * z[5] + (T(5) / T(6)) * z[6] + -z[7];
z[8] = abb[8] * z[8];
z[9] = abb[44] * (T(13) / T(6));
z[10] = abb[43] * (T(13) / T(6));
z[11] = abb[47] * (T(5) / T(3));
z[12] = abb[50] + z[11];
z[12] = abb[52] * (T(-8) / T(3)) + abb[51] * (T(5) / T(6)) + abb[48] * (T(7) / T(2)) + (T(-4) / T(3)) * z[6] + -z[9] + -z[10] + (T(1) / T(2)) * z[12];
z[12] = abb[0] * z[12];
z[13] = 3 * abb[45];
z[14] = -abb[50] + z[13];
z[11] = -z[11] + -z[14];
z[7] = abb[46] * (T(-19) / T(12)) + abb[51] * (T(-5) / T(12)) + abb[44] * (T(7) / T(12)) + (T(-13) / T(6)) * z[6] + z[7] + z[10] + (T(1) / T(4)) * z[11];
z[7] = abb[3] * z[7];
z[10] = -abb[44] + abb[51];
z[11] = abb[47] + -abb[50];
z[15] = abb[45] + z[11];
z[16] = -z[10] + z[15];
z[17] = (T(1) / T(2)) * z[16];
z[18] = abb[46] * (T(1) / T(2));
z[19] = z[6] + z[17] + z[18];
z[20] = abb[14] * z[19];
z[21] = abb[44] + abb[49];
z[22] = abb[52] * (T(2) / T(3));
z[3] = -abb[45] + -z[3] + (T(1) / T(3)) * z[21] + z[22];
z[3] = abb[11] * z[3];
z[23] = abb[11] * abb[46];
z[24] = abb[11] * abb[42];
z[23] = z[23] + -z[24];
z[3] = z[3] + (T(1) / T(3)) * z[23];
z[25] = abb[46] + abb[51];
z[26] = abb[44] + z[25];
z[5] = (T(1) / T(2)) * z[5] + z[22] + (T(1) / T(6)) * z[26];
z[5] = abb[15] * z[5];
z[22] = abb[21] + abb[23];
z[26] = abb[53] + abb[55] + abb[56] + abb[57];
z[26] = abb[54] + (T(1) / T(2)) * z[26];
z[27] = (T(1) / T(2)) * z[26];
z[28] = z[22] * z[27];
z[29] = -abb[46] + z[10];
z[30] = -z[6] + z[29];
z[31] = abb[2] * z[30];
z[32] = 2 * abb[52];
z[33] = 11 * abb[45] + 3 * abb[47] + abb[50];
z[33] = abb[44] * (T(-19) / T(12)) + abb[46] * (T(-7) / T(4)) + abb[43] * (T(1) / T(6)) + abb[51] * (T(7) / T(12)) + -z[32] + (T(1) / T(4)) * z[33];
z[33] = abb[7] * z[33];
z[34] = -abb[52] + abb[47] * (T(1) / T(2));
z[35] = abb[48] * (T(1) / T(2));
z[36] = (T(-1) / T(3)) * z[34] + -z[35];
z[9] = -abb[51] + abb[46] * (T(-7) / T(6)) + abb[43] * (T(10) / T(3)) + z[9] + 7 * z[36];
z[9] = abb[1] * z[9];
z[36] = abb[25] * z[27];
z[3] = 4 * z[3] + z[5] + z[7] + z[8] + z[9] + z[12] + (T(1) / T(2)) * z[20] + z[28] + (T(-1) / T(3)) * z[31] + z[33] + z[36];
z[5] = prod_pow(m1_set::bc<T>[0], 2);
z[3] = z[3] * z[5];
z[7] = abb[49] * (T(1) / T(2));
z[8] = abb[42] * (T(1) / T(2)) + -z[7];
z[9] = 2 * abb[43];
z[12] = z[8] + -z[9];
z[28] = abb[44] * (T(5) / T(4));
z[33] = abb[48] * (T(3) / T(2));
z[37] = 3 * z[4];
z[38] = abb[47] + z[37];
z[2] = z[2] + z[12] + -z[28] + -z[32] + z[33] + (T(1) / T(4)) * z[38];
z[2] = abb[3] * z[2];
z[38] = -abb[47] + z[4];
z[38] = -abb[48] + (T(1) / T(2)) * z[38];
z[39] = abb[44] * (T(1) / T(2));
z[40] = z[38] + z[39];
z[41] = abb[51] * (T(1) / T(2));
z[42] = z[18] + z[41];
z[43] = abb[43] + z[40] + -z[42];
z[44] = abb[13] * z[43];
z[45] = (T(3) / T(2)) * z[44];
z[46] = 2 * abb[44];
z[47] = -abb[47] + z[32];
z[48] = z[46] + z[47];
z[49] = 3 * abb[48];
z[50] = z[48] + -z[49];
z[51] = 3 * abb[43] + -z[25];
z[52] = z[50] + z[51];
z[53] = abb[1] * z[52];
z[54] = z[45] + -z[53];
z[55] = (T(3) / T(2)) * z[20];
z[56] = -abb[47] + z[37];
z[57] = (T(1) / T(2)) * z[56];
z[58] = -z[32] + z[57];
z[59] = z[39] + z[42] + -z[58];
z[60] = abb[12] * z[59];
z[61] = 3 * z[15];
z[62] = 5 * z[10] + -z[61];
z[63] = -3 * z[6];
z[64] = abb[46] * (T(3) / T(2));
z[62] = -abb[43] + (T(1) / T(2)) * z[62] + z[63] + -z[64];
z[65] = abb[8] * (T(1) / T(2));
z[62] = z[62] * z[65];
z[66] = 2 * z[31];
z[67] = abb[22] * z[26];
z[68] = abb[20] * z[26];
z[69] = z[67] + z[68];
z[70] = z[36] + -z[69];
z[11] = abb[48] + z[11];
z[71] = 3 * z[11];
z[72] = -abb[43] + z[10];
z[73] = z[71] + z[72];
z[74] = abb[0] * (T(1) / T(2));
z[75] = -z[73] * z[74];
z[76] = (T(3) / T(2)) * z[26];
z[77] = abb[24] * z[76];
z[2] = z[2] + z[54] + z[55] + z[60] + z[62] + z[66] + 3 * z[70] + z[75] + -z[77];
z[2] = abb[30] * z[2];
z[62] = abb[51] * (T(5) / T(4));
z[70] = -z[8] + z[62];
z[75] = 5 * abb[47];
z[78] = 3 * abb[50];
z[79] = abb[45] + z[78];
z[79] = -z[75] + 3 * z[79];
z[80] = abb[44] * (T(1) / T(4));
z[81] = abb[43] * (T(1) / T(2));
z[79] = z[1] + -z[32] + -z[70] + (T(1) / T(4)) * z[79] + z[80] + -z[81];
z[79] = abb[8] * z[79];
z[82] = z[32] + z[39] + z[41];
z[83] = z[6] + z[81];
z[84] = -abb[47] + z[78];
z[84] = -z[33] + z[82] + z[83] + (T(-1) / T(2)) * z[84];
z[84] = abb[0] * z[84];
z[21] = z[13] + -z[21] + -z[47];
z[21] = abb[11] * z[21];
z[21] = -z[21] + z[23];
z[23] = abb[25] * z[76];
z[23] = z[23] + z[55];
z[77] = z[23] + -z[77];
z[84] = -z[21] + -z[77] + z[84];
z[85] = -z[37] + -z[75];
z[86] = 4 * abb[52];
z[87] = abb[48] * (T(9) / T(2)) + -z[86];
z[88] = 4 * abb[43];
z[70] = abb[44] * (T(13) / T(4)) + -z[1] + -z[70] + (T(1) / T(4)) * z[85] + -z[87] + z[88];
z[70] = abb[3] * z[70];
z[85] = abb[44] + abb[47];
z[25] = z[25] + -z[37] + z[85] + z[86];
z[25] = abb[15] * z[25];
z[86] = z[25] + 2 * z[53];
z[89] = -z[66] + z[86];
z[45] = z[45] + z[60];
z[90] = 3 * z[26];
z[91] = abb[20] * z[90];
z[92] = abb[22] * z[90];
z[91] = z[91] + z[92];
z[70] = -z[45] + z[70] + z[79] + z[84] + z[89] + z[91];
z[70] = abb[29] * z[70];
z[79] = abb[46] * (T(1) / T(4));
z[56] = -abb[52] + (T(1) / T(4)) * z[56] + -z[79];
z[80] = z[0] + -z[56] + z[80];
z[80] = abb[7] * z[80];
z[93] = abb[15] * z[59];
z[80] = -z[31] + z[80] + -z[93];
z[94] = (T(-3) / T(2)) * z[6];
z[95] = abb[50] * (T(3) / T(2));
z[96] = -abb[47] + z[95];
z[97] = abb[52] + z[39];
z[98] = abb[46] + -z[81] + -z[94] + -z[96] + z[97];
z[98] = abb[8] * z[98];
z[33] = -abb[44] + z[33] + z[34];
z[12] = abb[46] + z[12] + z[33] + z[41];
z[12] = abb[3] * z[12];
z[34] = z[18] + z[39];
z[39] = z[34] + -z[41];
z[99] = -z[38] + z[39];
z[100] = abb[4] * z[99];
z[101] = (T(3) / T(2)) * z[100];
z[71] = z[71] + -z[72];
z[71] = z[71] * z[74];
z[12] = z[12] + z[54] + z[71] + -z[77] + z[80] + z[98] + z[101];
z[12] = abb[27] * z[12];
z[71] = 2 * abb[51];
z[74] = abb[46] + z[49];
z[77] = z[32] + z[71] + -z[74] + -z[85];
z[85] = 2 * abb[12];
z[77] = z[77] * z[85];
z[85] = abb[3] * z[59];
z[98] = abb[7] * z[59];
z[102] = z[85] + z[98];
z[103] = abb[8] * z[59];
z[103] = -z[25] + z[103];
z[104] = 3 * z[100];
z[105] = -z[77] + z[102] + z[103] + -z[104];
z[105] = abb[31] * z[105];
z[106] = 3 * z[20];
z[107] = abb[25] * z[90];
z[107] = z[21] + z[106] + z[107];
z[90] = abb[24] * z[90];
z[108] = -z[60] + z[90];
z[91] = z[91] + z[108];
z[109] = z[91] + -z[107];
z[110] = 2 * abb[8];
z[30] = z[30] * z[110];
z[30] = z[30] + -z[102];
z[102] = 2 * abb[47];
z[111] = -z[78] + z[102];
z[112] = abb[51] + z[32];
z[113] = z[111] + z[112];
z[114] = z[6] + z[113];
z[114] = abb[0] * z[114];
z[114] = -4 * z[31] + z[114];
z[115] = -z[30] + z[109] + z[114];
z[115] = abb[32] * z[115];
z[2] = z[2] + z[12] + z[70] + z[105] + -z[115];
z[2] = abb[27] * z[2];
z[12] = abb[52] + z[41];
z[70] = z[12] + -z[18] + z[81] + -z[96];
z[70] = abb[7] * z[70];
z[81] = 2 * abb[46];
z[96] = abb[51] * (T(5) / T(2));
z[78] = abb[47] + z[78];
z[78] = abb[43] * (T(-13) / T(2)) + abb[44] * (T(-9) / T(2)) + (T(1) / T(2)) * z[78] + z[81] + z[87] + z[96];
z[78] = abb[1] * z[78];
z[87] = z[93] + z[101];
z[93] = abb[43] + -abb[46];
z[105] = abb[47] + z[13];
z[105] = (T(1) / T(2)) * z[105];
z[116] = z[12] + -z[93] + -z[105];
z[116] = abb[8] * z[116];
z[33] = abb[43] * (T(-3) / T(2)) + z[33] + z[42];
z[117] = 3 * abb[3];
z[33] = z[33] * z[117];
z[16] = -abb[46] + z[16];
z[16] = abb[43] + (T(1) / T(2)) * z[16];
z[118] = abb[6] * z[16];
z[119] = -z[67] + z[118];
z[119] = -z[68] + (T(1) / T(2)) * z[119];
z[33] = z[33] + z[45] + z[70] + z[78] + -z[87] + z[116] + 3 * z[119];
z[33] = abb[28] * z[33];
z[45] = 3 * z[44];
z[61] = 3 * abb[46] + -z[61];
z[70] = -z[10] + z[61];
z[70] = -abb[43] + (T(1) / T(2)) * z[70];
z[70] = abb[7] * z[70];
z[70] = -z[45] + z[70];
z[47] = -z[47] + z[74];
z[71] = 3 * abb[44] + -z[47] + -z[71] + z[88];
z[71] = abb[1] * z[71];
z[74] = 3 * z[69];
z[61] = -z[10] + -z[61];
z[61] = z[9] + (T(1) / T(2)) * z[61];
z[61] = abb[8] * z[61];
z[78] = abb[3] * z[52];
z[61] = -z[60] + z[61] + z[70] + z[71] + z[74] + z[78];
z[61] = abb[30] * z[61];
z[71] = z[74] + -z[103];
z[74] = 2 * abb[3];
z[52] = z[52] * z[74];
z[52] = z[52] + -z[60];
z[45] = z[45] + -z[52] + -3 * z[53] + -z[71] + z[98];
z[78] = -abb[27] + abb[29];
z[45] = z[45] * z[78];
z[78] = -z[18] + z[58];
z[116] = abb[44] * (T(3) / T(2));
z[119] = z[9] + z[78] + -z[96] + z[116];
z[120] = abb[7] + abb[8];
z[119] = z[119] * z[120];
z[68] = -z[68] + z[118];
z[120] = 3 * z[68];
z[121] = -z[104] + z[120];
z[122] = abb[44] * (T(5) / T(2));
z[123] = abb[46] * (T(5) / T(2));
z[124] = z[122] + z[123];
z[37] = z[37] + -z[75];
z[37] = -6 * abb[48] + abb[51] * (T(7) / T(2)) + z[32] + (T(1) / T(2)) * z[37] + -z[124];
z[37] = abb[12] * z[37];
z[113] = z[93] + z[113];
z[113] = abb[1] * z[113];
z[37] = z[25] + z[37] + z[113] + z[119] + -z[121];
z[37] = abb[31] * z[37];
z[33] = z[33] + z[37] + z[45] + z[61];
z[33] = abb[28] * z[33];
z[29] = z[15] + z[29];
z[37] = z[29] * z[65];
z[37] = z[37] + z[67] + z[118];
z[45] = -abb[3] * z[43];
z[61] = abb[7] * z[16];
z[67] = z[11] + z[72];
z[67] = abb[1] * z[67];
z[45] = -z[37] + z[44] + z[45] + z[61] + z[67];
z[45] = abb[34] * z[45];
z[19] = abb[8] * z[19];
z[61] = abb[24] * z[26];
z[61] = z[61] + z[69];
z[67] = abb[0] * z[11];
z[119] = abb[25] * z[26];
z[19] = z[19] + -z[20] + -z[31] + z[61] + z[67] + -z[119];
z[20] = abb[12] * z[99];
z[67] = -z[19] + z[20];
z[67] = abb[58] * z[67];
z[99] = z[32] + z[116];
z[116] = abb[47] + z[4];
z[116] = (T(1) / T(2)) * z[116];
z[125] = 2 * abb[48];
z[42] = -z[9] + z[42] + -z[99] + z[116] + z[125];
z[42] = abb[3] * z[42];
z[126] = -z[32] + z[125];
z[127] = abb[51] * (T(3) / T(2));
z[34] = z[34] + z[116] + z[126] + -z[127];
z[34] = abb[12] * z[34];
z[34] = z[34] + -z[100];
z[42] = -z[34] + z[42] + z[44] + -z[53] + -z[69];
z[42] = abb[33] * z[42];
z[44] = abb[36] + abb[58];
z[69] = -z[44] * z[100];
z[42] = z[42] + z[45] + z[67] + z[69];
z[8] = -z[8] + -z[12] + z[49] + -z[95] + z[102];
z[8] = abb[0] * z[8];
z[7] = z[7] + z[18] + z[97] + -z[105];
z[7] = abb[11] * z[7];
z[7] = z[7] + (T(-1) / T(2)) * z[24];
z[8] = z[7] + z[8] + -z[23] + -z[30] + -z[31] + z[91] + -z[101];
z[23] = prod_pow(abb[32], 2);
z[8] = z[8] * z[23];
z[24] = -abb[48] + abb[52] + abb[50] * (T(-1) / T(2));
z[24] = z[9] + 3 * z[24] + -z[41] + z[46] + -z[94];
z[24] = abb[0] * z[24];
z[7] = -z[7] + -z[36];
z[30] = -2 * z[6];
z[36] = z[9] + z[30];
z[14] = 3 * z[14] + z[75];
z[45] = -abb[52] + abb[44] * (T(-7) / T(4)) + (T(1) / T(4)) * z[14] + -z[36] + z[62] + z[79];
z[45] = abb[3] * z[45];
z[28] = abb[43] + abb[51] * (T(-3) / T(4)) + z[28] + -z[56];
z[28] = abb[8] * z[28];
z[7] = 3 * z[7] + z[24] + z[28] + z[45] + -z[53] + -z[55] + z[80];
z[7] = abb[29] * z[7];
z[7] = z[7] + -z[115];
z[7] = abb[29] * z[7];
z[24] = -abb[27] + abb[28];
z[28] = -z[24] * z[26];
z[45] = abb[31] * z[26];
z[28] = z[28] + z[45];
z[49] = abb[30] * z[26];
z[53] = abb[29] * z[26];
z[49] = z[49] + -z[53];
z[55] = z[28] + z[49];
z[55] = abb[28] * z[55];
z[67] = -abb[29] + abb[30];
z[67] = z[27] * z[67];
z[45] = z[45] + z[67];
z[45] = abb[27] * z[45];
z[44] = abb[34] + -abb[59] + -abb[60] + z[44];
z[44] = z[26] * z[44];
z[67] = abb[31] * z[27];
z[67] = -z[53] + z[67];
z[67] = abb[31] * z[67];
z[49] = abb[30] * z[49];
z[69] = prod_pow(abb[29], 2);
z[27] = z[27] * z[69];
z[27] = z[27] + z[44] + z[45] + (T(1) / T(2)) * z[49] + -z[55] + z[67];
z[22] = 3 * z[22];
z[27] = -z[22] * z[27];
z[30] = z[30] + z[58] + -z[124] + z[127];
z[30] = abb[8] * z[30];
z[14] = abb[44] * (T(7) / T(2)) + (T(-1) / T(2)) * z[14] + z[32] + -z[96];
z[44] = abb[46] * (T(7) / T(2)) + z[14];
z[45] = abb[7] * z[44];
z[25] = z[25] + z[30] + z[45] + z[107] + -z[114];
z[30] = abb[60] * z[25];
z[45] = abb[44] * (T(3) / T(4));
z[49] = 5 * abb[45] + abb[47] + abb[50];
z[0] = -abb[52] + z[0] + -z[1] + -z[45] + (T(1) / T(4)) * z[49];
z[1] = -z[0] * z[117];
z[45] = -abb[43] + -z[45] + -z[56] + z[62];
z[45] = abb[8] * z[45];
z[55] = abb[51] * (T(-7) / T(4)) + abb[44] * (T(9) / T(4)) + z[9] + -z[56];
z[55] = abb[7] * z[55];
z[47] = abb[43] + abb[51] + -z[47];
z[47] = abb[1] * z[47];
z[1] = z[1] + z[45] + z[47] + z[55] + -z[77] + -z[87];
z[1] = abb[31] * z[1];
z[45] = -z[41] + (T(-1) / T(2)) * z[49] + z[64] + z[99];
z[47] = abb[3] + abb[7];
z[47] = z[45] * z[47];
z[34] = z[34] + z[47] + z[61];
z[34] = 3 * z[34];
z[47] = -abb[32] * z[34];
z[13] = -abb[51] + -z[13] + z[48] + z[81];
z[48] = 2 * abb[7];
z[49] = z[48] + z[74];
z[13] = z[13] * z[49];
z[13] = z[13] + z[71] + z[108];
z[49] = abb[29] * z[13];
z[1] = z[1] + z[47] + z[49];
z[1] = abb[31] * z[1];
z[47] = z[66] + -z[98];
z[10] = z[10] + z[15];
z[15] = z[6] + z[123];
z[49] = abb[43] + (T(3) / T(2)) * z[10] + -z[15];
z[49] = z[49] * z[65];
z[40] = z[40] + -z[41];
z[15] = -z[15] + 3 * z[40];
z[15] = z[9] + (T(1) / T(2)) * z[15];
z[15] = abb[3] * z[15];
z[15] = z[15] + z[47] + z[49] + -z[54] + -z[84];
z[15] = abb[29] * z[15];
z[48] = z[48] + z[110];
z[48] = z[48] * z[72];
z[48] = z[48] + z[60] + -z[85] + -z[113] + z[120];
z[48] = abb[31] * z[48];
z[49] = abb[1] * z[73];
z[37] = -3 * z[37] + z[49] + -z[70];
z[40] = (T(-3) / T(2)) * z[40] + -z[79] + -z[83];
z[40] = abb[3] * z[40];
z[49] = abb[0] * z[72];
z[31] = -z[31] + (T(1) / T(2)) * z[37] + z[40] + z[49];
z[31] = abb[30] * z[31];
z[15] = z[15] + z[31] + z[48] + z[115];
z[15] = abb[30] * z[15];
z[31] = abb[18] * z[43];
z[17] = -abb[49] + z[17];
z[17] = abb[19] * z[17];
z[10] = abb[43] + abb[49] + (T(-1) / T(2)) * z[10];
z[10] = abb[16] * z[10];
z[37] = abb[16] + -abb[19];
z[40] = -z[18] * z[37];
z[38] = abb[49] + -z[38] + -z[39];
z[38] = abb[17] * z[38];
z[37] = -abb[17] + -z[37];
z[37] = abb[42] * z[37];
z[10] = z[10] + z[17] + z[31] + z[37] + z[38] + z[40];
z[17] = abb[26] * z[76];
z[10] = (T(3) / T(2)) * z[10] + z[17];
z[10] = abb[37] * z[10];
z[17] = abb[43] + abb[44];
z[31] = abb[48] + z[4];
z[37] = z[17] + -z[31] + z[32];
z[38] = abb[29] * z[37];
z[39] = z[31] + -z[112];
z[40] = abb[31] * z[39];
z[38] = z[38] + z[40];
z[37] = -abb[27] * z[37];
z[4] = (T(1) / T(2)) * z[4] + z[35];
z[17] = abb[52] + -z[4] + z[17] + -z[41];
z[17] = abb[28] * z[17];
z[17] = z[17] + z[37] + z[38];
z[17] = abb[28] * z[17];
z[35] = abb[32] * z[39];
z[4] = z[4] + -z[12];
z[12] = -abb[31] * z[4];
z[12] = z[12] + z[35];
z[12] = abb[31] * z[12];
z[35] = -abb[27] * z[4];
z[35] = z[35] + -z[38];
z[35] = abb[27] * z[35];
z[37] = -abb[35] + -abb[36];
z[37] = z[37] * z[39];
z[4] = -z[4] * z[23];
z[38] = -abb[33] * z[72];
z[4] = z[4] + z[12] + z[17] + z[35] + z[37] + z[38];
z[12] = 3 * abb[9];
z[4] = z[4] * z[12];
z[17] = z[98] + z[103];
z[35] = z[17] + -z[77];
z[35] = abb[36] * z[35];
z[34] = abb[35] * z[34];
z[20] = z[20] + -z[100];
z[16] = abb[8] * z[16];
z[29] = abb[7] * z[29];
z[11] = abb[1] * z[11];
z[11] = z[11] + z[16] + -z[20] + (T(-1) / T(2)) * z[29] + -z[68];
z[11] = 3 * z[11];
z[16] = abb[59] * z[11];
z[29] = -abb[29] + abb[32];
z[29] = abb[31] * z[29];
z[29] = -abb[35] + -abb[60] + z[29];
z[29] = z[29] * z[45];
z[23] = z[23] + -z[69];
z[23] = z[0] * z[23];
z[23] = z[23] + z[29];
z[0] = z[0] * z[5];
z[0] = z[0] + 3 * z[23];
z[0] = abb[5] * z[0];
z[5] = abb[60] * z[44];
z[23] = abb[36] * z[59];
z[5] = z[5] + z[23];
z[5] = abb[3] * z[5];
z[0] = abb[61] + z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[7] + z[8] + z[10] + z[15] + z[16] + z[27] + z[30] + z[33] + z[34] + z[35] + 3 * z[42];
z[1] = -z[9] + z[78] + -z[122] + z[127];
z[1] = abb[8] * z[1];
z[2] = -4 * z[6] + z[14] + -z[18] + z[88];
z[2] = abb[3] * z[2];
z[3] = z[21] + z[119];
z[4] = abb[50] + z[126];
z[4] = -4 * abb[44] + abb[51] + 3 * z[4] + z[63] + -z[88];
z[4] = abb[0] * z[4];
z[1] = z[1] + z[2] + 3 * z[3] + z[4] + z[47] + z[86] + z[106];
z[1] = abb[29] * z[1];
z[2] = z[32] + z[46];
z[3] = -abb[51] + z[9];
z[4] = z[2] + z[3];
z[5] = z[4] + z[6] + z[111];
z[5] = abb[0] * z[5];
z[7] = z[74] + z[110];
z[8] = z[6] + -z[93];
z[7] = z[7] * z[8];
z[2] = z[2] + z[51];
z[8] = -z[2] + -z[111];
z[8] = abb[1] * z[8];
z[7] = z[5] + z[7] + z[8] + -z[47] + z[90] + z[92] + -z[107] + 3 * z[118];
z[7] = abb[30] * z[7];
z[8] = z[36] + -z[57] + -z[64] + z[82];
z[8] = abb[8] * z[8];
z[3] = -z[3] + -z[6] + -z[50];
z[3] = z[3] * z[74];
z[3] = z[3] + -z[5] + z[8] + -z[89] + -z[104] + -z[109];
z[3] = abb[27] * z[3];
z[5] = -abb[31] * z[13];
z[2] = -abb[50] + z[2] + -z[125];
z[2] = abb[1] * z[2];
z[2] = 3 * z[2] + -z[17] + z[52] + -z[121];
z[2] = abb[28] * z[2];
z[4] = z[4] + -z[31];
z[4] = -z[4] * z[12] * z[24];
z[6] = z[28] + -z[53];
z[6] = -z[6] * z[22];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[115];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[3] * z[44];
z[2] = z[2] + z[25];
z[2] = abb[40] * z[2];
z[3] = -z[19] + z[20];
z[3] = abb[38] * z[3];
z[4] = -abb[29] + abb[31];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = -abb[40] + z[4];
z[4] = abb[5] * z[4] * z[45];
z[3] = z[3] + z[4];
z[4] = abb[39] * z[11];
z[5] = -abb[38] + abb[39] + abb[40];
z[5] = z[5] * z[22] * z[26];
z[1] = abb[41] + z[1] + z[2] + 3 * z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_515_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.107030853033549347562179671205675854551103932387966687007657496"),stof<T>("29.580462627378923262853323453281354234575591471931257053965937591")}, std::complex<T>{stof<T>("22.001273285782619494841150209536247518088940587201980409118634933"),stof<T>("-38.245806323991095430851588696023475730505503093986119107464871917")}, std::complex<T>{stof<T>("27.128560073043796063181836465996204305954575085772682378994271365"),stof<T>("-13.876122873218955150654817691976001505485478827099428366863344566")}, std::complex<T>{stof<T>("-25.315646701782271368819612832717050228501757107881230267615439754"),stof<T>("13.034420213808143724446199702862594282502964410128585982339051897")}, std::complex<T>{stof<T>("5.127286787261176568340686256459956787865634498570701969875636432"),stof<T>("24.369683450772140280196771004047474225020024266886690740601527352")}, std::complex<T>{stof<T>("-14.029794064066092549509505954561015021230216874110080542990016669"),stof<T>("6.816965289077107930140606399237019070511832626530414426834265521")}, std::complex<T>{stof<T>("-26.876805485258912086523386931946799964898497120073973496838868568"),stof<T>("9.429827149671685689273701203631853333903662626569867782444496939")}, std::complex<T>{stof<T>("1.107030853033549347562179671205675854551103932387966687007657496"),stof<T>("-29.580462627378923262853323453281354234575591471931257053965937591")}, std::complex<T>{stof<T>("-5.0515349974214529034072409504904025648548018028124810677421291576"),stof<T>("1.0066757481242528116490408543916952024355645785536052421403761367")}, std::complex<T>{stof<T>("-8.0472310115562506102650895609447867198695564088501207682621255385"),stof<T>("6.0524818360175944088651704383472872325380816220154086978887029082")}, std::complex<T>{stof<T>("38.162658122975090905833493810102835172170037353845123221464291653"),stof<T>("-15.647282074402721483579294507257428545894794410168039337949283315")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-3.9008635571742125011019775686967458012138538309289797447447215239"),stof<T>("4.7885208668043387237614190663407589643800057830636433730180156175")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[70].real()/kbase.W[70].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_515_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_515_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(-3) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (-3 * m1_set::bc<T>[1] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[44] + -abb[45] + abb[46] + abb[48] + -abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -prod_pow(abb[32], 2);
z[1] = prod_pow(abb[31], 2);
z[0] = z[0] + z[1];
z[1] = -abb[44] + abb[45] + -abb[46] + -abb[48] + abb[51];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_515_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_515_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-21.861368008368651252551170577587974127852694738009518663455939702"),stof<T>("-33.025506868950396526757141904909741619253521236220840322034431087")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W17(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W71(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[70].real()/k.W[70].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k), T{0}};
abb[41] = SpDLog_f_4_515_W_17_Im(t, path, abb);
abb[61] = SpDLog_f_4_515_W_17_Re(t, path, abb);

                    
            return f_4_515_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_515_DLogXconstant_part(base_point<T>, kend);
	value += f_4_515_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_515_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_515_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_515_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_515_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_515_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_515_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
