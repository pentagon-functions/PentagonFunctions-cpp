/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_183.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_183_abbreviated (const std::array<T,19>& abb) {
T z[10];
z[0] = -abb[5] + abb[7];
z[1] = abb[14] + -abb[15];
z[2] = abb[12] + z[1];
z[0] = z[0] * z[2];
z[3] = -abb[6] + abb[8];
z[4] = abb[13] + -abb[15];
z[5] = abb[11] + z[4];
z[3] = z[3] * z[5];
z[1] = abb[11] + z[1];
z[1] = abb[2] * z[1];
z[4] = abb[12] + z[4];
z[4] = abb[1] * z[4];
z[6] = abb[13] + -abb[14];
z[6] = abb[0] * z[6];
z[7] = -abb[11] + abb[12];
z[7] = abb[4] * z[7];
z[6] = -z[0] + z[1] + z[3] + -z[4] + z[6] + z[7];
z[6] = abb[18] * z[6];
z[7] = -abb[9] + abb[10] * (T(1) / T(2));
z[7] = abb[10] * z[7];
z[8] = prod_pow(abb[9], 2);
z[7] = abb[12] + abb[14] + z[5] + z[7] + (T(1) / T(2)) * z[8];
z[8] = abb[0] * z[7];
z[9] = -abb[3] * z[7];
z[5] = abb[8] * z[5];
z[2] = abb[7] * z[2];
z[1] = -z[1] + z[2] + -z[4] + z[5] + z[8] + z[9];
z[1] = abb[16] * z[1];
z[2] = -abb[4] * z[7];
z[0] = z[0] + z[2] + z[3] + z[8];
z[0] = abb[17] * z[0];
z[0] = z[0] + z[1] + z[6];
return {z[0], 0};
}


template <typename T> std::complex<T> f_4_183_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {stof<T>("5.7186342206049604869927556596083579112693707085908457961419970138"), stof<T>("-0.65221275531063822086727906425665276638687660258322095294250494738")};
	
	std::vector<C> intdlogs = {rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_183_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_183_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = stof<T>("7.0077237987191574853969848973895797782394733342797986267693429461");
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,19> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), f_1_1(k), f_1_6(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_183_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_183_DLogXconstant_part(base_point<T>, kend);
	value += f_4_183_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_183_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_183_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_183_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_183_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_183_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_183_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
