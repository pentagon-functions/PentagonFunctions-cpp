/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_571.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_571_abbreviated (const std::array<T,19>& abb) {
T z[21];
z[0] = abb[12] + abb[15];
z[1] = 2 * abb[16];
z[2] = abb[13] + abb[14] + -z[1];
z[3] = 2 * z[2];
z[4] = z[0] + z[3];
z[5] = abb[2] * z[4];
z[6] = abb[12] + z[2];
z[7] = 2 * abb[1];
z[8] = z[6] * z[7];
z[9] = -abb[13] + abb[14];
z[10] = -abb[12] + abb[15];
z[11] = z[9] + -z[10];
z[11] = abb[5] * z[11];
z[9] = abb[0] * z[9];
z[8] = z[5] + -z[8] + z[9] + z[11];
z[12] = abb[6] * z[8];
z[13] = abb[12] * (T(1) / T(2));
z[14] = abb[15] * (T(1) / T(2));
z[15] = z[13] + z[14];
z[16] = 3 * abb[13] + abb[14];
z[1] = -z[1] + z[15] + (T(1) / T(2)) * z[16];
z[1] = abb[0] * z[1];
z[14] = abb[12] * (T(3) / T(2)) + z[2] + -z[14];
z[14] = abb[3] * z[14];
z[1] = z[1] + -z[14];
z[14] = z[2] + z[15];
z[14] = abb[2] * z[14];
z[6] = abb[1] * z[6];
z[6] = z[1] + z[6] + (T(1) / T(2)) * z[11] + -z[14];
z[6] = abb[8] * z[6];
z[15] = abb[0] * z[4];
z[17] = 2 * abb[2];
z[18] = z[7] + z[17];
z[18] = z[10] * z[18];
z[19] = 3 * abb[12] + -abb[15] + z[3];
z[20] = abb[3] * z[19];
z[15] = z[15] + -z[18] + -z[20];
z[15] = abb[7] * z[15];
z[6] = z[6] + z[12] + -z[15];
z[6] = abb[8] * z[6];
z[12] = abb[15] * (T(1) / T(6));
z[13] = (T(-1) / T(3)) * z[2] + z[12] + -z[13];
z[13] = abb[3] * z[13];
z[4] = abb[1] * z[4];
z[12] = abb[13] + abb[16] * (T(-2) / T(3)) + abb[14] * (T(-1) / T(3)) + abb[12] * (T(1) / T(6)) + z[12];
z[12] = abb[0] * z[12];
z[3] = -abb[12] + z[3];
z[3] = abb[15] + (T(1) / T(3)) * z[3];
z[3] = abb[2] * z[3];
z[3] = z[3] + (T(-1) / T(3)) * z[4] + (T(4) / T(3)) * z[11] + z[12] + z[13];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[4] = abb[2] + -abb[3];
z[4] = z[4] * z[19];
z[2] = abb[15] + z[2];
z[7] = z[2] * z[7];
z[0] = -4 * abb[16] + z[0];
z[12] = abb[13] + 3 * abb[14] + z[0];
z[12] = abb[0] * z[12];
z[4] = z[4] + -z[7] + z[11] + z[12];
z[12] = abb[17] * z[4];
z[13] = abb[1] * z[2];
z[1] = -z[1] + (T(-3) / T(2)) * z[11] + z[13] + -z[14];
z[1] = abb[6] * z[1];
z[1] = z[1] + z[15];
z[1] = abb[6] * z[1];
z[0] = z[0] + z[16];
z[0] = abb[0] * z[0];
z[13] = abb[1] + -abb[3];
z[13] = z[13] * z[19];
z[2] = -z[2] * z[17];
z[2] = z[0] + z[2] + z[13];
z[2] = abb[9] * z[2];
z[10] = -abb[1] * z[10];
z[9] = -z[9] + z[10];
z[9] = prod_pow(abb[7], 2) * z[9];
z[1] = abb[18] + z[1] + z[2] + z[3] + z[6] + z[9] + z[12];
z[0] = z[0] + z[5] + -z[7] + 3 * z[11] + -z[20];
z[0] = abb[6] * z[0];
z[2] = -abb[8] * z[8];
z[0] = z[0] + z[2] + -z[15];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[10] * z[4];
z[0] = abb[11] + z[0] + z[2];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_571_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("9.541284877569299152858961531246038185495730365056981786485152841"),stof<T>("1.18961275727625567966761314631362711587150060242345822559251915")}, std::complex<T>{stof<T>("-8.8165054409560189543087758454521118990165243169350390871125179021"),stof<T>("1.3942436327487293349554131092781558176472734447336340822528590253")}, std::complex<T>{stof<T>("8.8165054409560189543087758454521118990165243169350390871125179021"),stof<T>("-1.3942436327487293349554131092781558176472734447336340822528590253")}, std::complex<T>{stof<T>("-9.541284877569299152858961531246038185495730365056981786485152841"),stof<T>("-1.18961275727625567966761314631362711587150060242345822559251915")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_571_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_571_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (8 + 4 * v[0] + -3 * v[1] + v[2] + 3 * v[3] + -v[4] + -4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + 2 * m1_set::bc<T>[1]) * (T(-1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[12] + abb[13] + -abb[14] + -abb[15]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[7], 2);
z[1] = prod_pow(abb[8], 2);
z[0] = -abb[9] + z[0] + -z[1];
z[1] = -abb[12] + -abb[13] + abb[14] + abb[15];
return abb[4] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_571_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_571_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,19> abb = {dlog_W4(k,dl), dl[2], dlog_W8(k,dl), dlog_W10(k,dl), dlog_W20(k,dl), dlog_W25(k,dl), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_12_im(k), T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), IntDLogL_W_17(t,k,kend,dl), f_2_12_re(k), T{0}};
abb[11] = SpDLog_f_4_571_W_20_Im(t, path, abb);
abb[18] = SpDLog_f_4_571_W_20_Re(t, path, abb);

                    
            return f_4_571_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_571_DLogXconstant_part(base_point<T>, kend);
	value += f_4_571_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_571_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_571_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_571_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_571_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_571_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_571_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
