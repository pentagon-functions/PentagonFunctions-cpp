/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_823.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_823_abbreviated (const std::array<T,81>& abb) {
T z[131];
z[0] = abb[2] + -abb[7];
z[1] = -abb[4] + abb[6];
z[2] = z[0] + z[1];
z[3] = abb[38] * z[2];
z[4] = abb[39] + abb[40];
z[5] = -abb[38] + z[4];
z[6] = -abb[37] + z[5];
z[7] = 2 * abb[10];
z[8] = -z[6] * z[7];
z[9] = 2 * abb[37];
z[10] = -abb[41] + z[9];
z[11] = -abb[36] + z[10];
z[12] = abb[40] + -z[11];
z[12] = abb[4] * z[12];
z[13] = abb[5] * abb[39];
z[14] = abb[37] + abb[42];
z[15] = -abb[35] + z[14];
z[16] = -abb[41] + z[15];
z[17] = abb[14] * z[16];
z[18] = abb[5] * abb[37];
z[19] = abb[35] + -abb[37];
z[20] = abb[41] + z[19];
z[21] = abb[7] * z[20];
z[22] = -abb[2] * abb[42];
z[23] = abb[39] + -z[14];
z[23] = abb[6] * z[23];
z[24] = abb[3] * z[19];
z[8] = z[3] + z[8] + z[12] + -z[13] + z[17] + z[18] + z[21] + z[22] + z[23] + z[24];
z[12] = 2 * m1_set::bc<T>[0];
z[8] = z[8] * z[12];
z[18] = abb[3] + abb[7] + -abb[14];
z[21] = abb[49] + -abb[51];
z[18] = z[18] * z[21];
z[22] = abb[4] * abb[50];
z[23] = abb[6] * abb[51];
z[24] = abb[5] * abb[51];
z[25] = -abb[20] * abb[53];
z[26] = abb[50] * z[7];
z[18] = z[18] + -2 * z[22] + -z[23] + z[24] + z[25] + z[26];
z[24] = abb[35] + -abb[42];
z[25] = -abb[39] + z[24];
z[26] = abb[38] + z[25];
z[26] = m1_set::bc<T>[0] * z[26];
z[26] = abb[49] + z[26];
z[27] = 2 * abb[17];
z[26] = z[26] * z[27];
z[26] = abb[54] + z[26];
z[27] = abb[36] + -abb[40] + -2 * z[24];
z[28] = m1_set::bc<T>[0] * z[27];
z[29] = 2 * abb[49];
z[28] = z[28] + -z[29];
z[30] = 2 * abb[0];
z[31] = z[28] * z[30];
z[31] = -abb[56] + -abb[57] + z[31];
z[32] = -abb[17] + z[7];
z[33] = z[2] + z[32];
z[34] = 2 * abb[52];
z[33] = z[33] * z[34];
z[8] = abb[55] + z[8] + 2 * z[18] + -z[26] + -z[31] + z[33];
z[8] = abb[66] * z[8];
z[18] = abb[4] * z[14];
z[33] = z[13] + z[18];
z[1] = -z[0] + z[1];
z[35] = abb[38] * z[1];
z[36] = abb[5] + abb[7];
z[37] = abb[35] + abb[37];
z[36] = z[36] * z[37];
z[36] = z[33] + z[35] + -z[36];
z[38] = -abb[4] + abb[5];
z[39] = 2 * abb[7];
z[40] = z[38] + z[39];
z[41] = abb[36] * z[40];
z[42] = abb[40] * z[38];
z[41] = z[41] + z[42];
z[42] = 2 * abb[6];
z[43] = -abb[37] + z[4];
z[43] = z[42] * z[43];
z[44] = 4 * abb[10];
z[6] = -z[6] * z[44];
z[45] = -abb[36] + -abb[40] + z[9];
z[45] = abb[13] * z[45];
z[46] = 2 * abb[42];
z[47] = -abb[2] * z[46];
z[6] = z[6] + -2 * z[36] + -z[41] + z[43] + -z[45] + z[47];
z[6] = m1_set::bc<T>[0] * z[6];
z[36] = 2 * abb[13];
z[43] = -z[36] + -z[42] + z[44];
z[43] = abb[50] * z[43];
z[44] = -abb[19] + abb[20] + abb[24];
z[47] = abb[53] * z[44];
z[31] = abb[55] + z[31] + z[47];
z[47] = abb[49] + abb[50];
z[48] = abb[5] * z[47];
z[48] = -z[22] + z[48];
z[47] = z[39] * z[47];
z[32] = -z[1] + z[32];
z[32] = z[32] * z[34];
z[49] = abb[23] * abb[53];
z[6] = z[6] + -z[26] + -z[31] + z[32] + z[43] + z[47] + 2 * z[48] + -z[49];
z[6] = abb[70] * z[6];
z[26] = -abb[41] + z[14];
z[32] = -abb[4] * z[26];
z[43] = abb[5] * abb[35];
z[47] = abb[7] * z[10];
z[17] = z[17] + z[32] + z[43] + z[47];
z[32] = abb[42] + z[9];
z[43] = 2 * abb[39];
z[47] = -abb[40] + z[32] + -z[43];
z[47] = z[42] * z[47];
z[48] = 2 * z[19];
z[50] = abb[3] * z[48];
z[17] = 2 * z[17] + -z[41] + z[45] + -z[47] + z[50];
z[17] = m1_set::bc<T>[0] * z[17];
z[41] = 2 * abb[14];
z[45] = 2 * abb[3] + -z[41];
z[45] = z[21] * z[45];
z[39] = z[39] + -z[42];
z[50] = abb[50] + abb[51];
z[39] = z[39] * z[50];
z[21] = abb[50] + z[21];
z[21] = abb[5] * z[21];
z[21] = z[21] + -z[22];
z[22] = -abb[37] + abb[41];
z[51] = abb[36] + -abb[39] + z[22];
z[51] = m1_set::bc<T>[0] * z[51];
z[51] = -abb[50] + z[51];
z[51] = abb[12] * z[51];
z[36] = abb[50] * z[36];
z[17] = abb[54] + z[17] + 2 * z[21] + -z[31] + z[36] + z[39] + z[45] + z[49] + 4 * z[51];
z[17] = abb[69] * z[17];
z[21] = abb[28] + abb[30];
z[31] = abb[39] * z[21];
z[25] = abb[32] * z[25];
z[36] = abb[35] * z[21];
z[25] = z[25] + z[31] + -z[36];
z[31] = abb[30] * abb[37];
z[39] = abb[27] * abb[35];
z[37] = abb[29] * z[37];
z[45] = abb[27] + abb[29];
z[49] = abb[32] + z[45];
z[52] = abb[38] * z[49];
z[53] = abb[28] * abb[37];
z[31] = -z[25] + z[31] + z[37] + z[39] + -z[52] + z[53];
z[37] = 2 * abb[29];
z[39] = abb[27] + z[21];
z[54] = z[37] + z[39];
z[55] = abb[36] * z[54];
z[27] = abb[31] * z[27];
z[56] = -abb[27] + z[21];
z[56] = abb[40] * z[56];
z[27] = z[27] + -z[55] + -z[56];
z[31] = z[27] + 2 * z[31];
z[31] = m1_set::bc<T>[0] * z[31];
z[55] = z[21] + z[45];
z[57] = abb[49] * z[55];
z[58] = abb[32] * abb[49];
z[59] = abb[29] + z[21];
z[60] = abb[50] * z[59];
z[57] = z[57] + -z[58] + z[60];
z[61] = abb[34] * abb[53];
z[29] = abb[31] * z[29];
z[29] = z[29] + z[61];
z[31] = -z[29] + z[31] + 2 * z[57];
z[57] = -abb[62] * z[31];
z[34] = z[34] * z[49];
z[61] = abb[80] + z[34];
z[31] = -z[31] + z[61];
z[31] = abb[65] * z[31];
z[62] = z[20] * z[45];
z[63] = abb[26] * z[16];
z[64] = abb[41] * z[21];
z[63] = z[63] + -z[64];
z[65] = abb[30] * abb[42];
z[66] = abb[28] * abb[42];
z[67] = z[65] + z[66];
z[25] = z[25] + z[52] + -z[62] + z[63] + z[67];
z[62] = -m1_set::bc<T>[0] * z[25];
z[68] = abb[26] + z[55];
z[68] = abb[49] * z[68];
z[69] = abb[26] + z[45];
z[70] = abb[51] * z[69];
z[58] = -z[58] + z[68] + -z[70];
z[62] = z[58] + z[62];
z[62] = abb[61] * z[62];
z[12] = -z[12] * z[25];
z[12] = z[12] + 2 * z[58] + -z[61];
z[12] = abb[63] * z[12];
z[11] = m1_set::bc<T>[0] * z[11];
z[11] = z[11] + z[50];
z[11] = abb[1] * z[11];
z[25] = -abb[37] + abb[39];
z[50] = abb[6] * z[25];
z[58] = m1_set::bc<T>[0] * z[50];
z[11] = z[11] + -z[23] + z[51] + z[58];
z[23] = abb[67] + abb[68];
z[51] = 4 * z[23];
z[11] = z[11] * z[51];
z[51] = z[53] + z[66];
z[58] = abb[30] * z[14];
z[58] = z[51] + z[58];
z[61] = abb[27] * z[22];
z[66] = abb[29] * z[10];
z[61] = z[58] + -z[61] + z[63] + z[66];
z[27] = z[27] + 2 * z[61];
z[27] = m1_set::bc<T>[0] * z[27];
z[61] = abb[26] * abb[49];
z[60] = z[60] + -z[61] + z[70];
z[27] = z[27] + -z[29] + 2 * z[60];
z[29] = -abb[80] + z[27];
z[29] = abb[64] * z[29];
z[27] = abb[60] * z[27];
z[60] = abb[61] + -abb[62];
z[34] = -z[34] * z[60];
z[61] = abb[69] + abb[70];
z[63] = abb[16] * z[61];
z[28] = z[28] * z[63];
z[66] = 2 * abb[59];
z[68] = abb[80] * z[66];
z[6] = z[6] + z[8] + z[11] + z[12] + z[17] + z[27] + z[28] + z[29] + z[31] + z[34] + z[57] + 2 * z[62] + z[68];
z[6] = 2 * z[6];
z[8] = -abb[39] + abb[40];
z[11] = z[8] * z[15];
z[12] = 2 * abb[35];
z[17] = -abb[37] + abb[42];
z[27] = z[12] * z[17];
z[28] = prod_pow(abb[42], 2);
z[29] = prod_pow(abb[37], 2);
z[31] = z[28] + -z[29];
z[34] = -abb[40] + z[15];
z[34] = abb[38] * z[34];
z[15] = -abb[39] + z[15];
z[15] = abb[36] * z[15];
z[57] = prod_pow(m1_set::bc<T>[0], 2);
z[62] = (T(2) / T(3)) * z[57];
z[68] = 2 * abb[43];
z[70] = 2 * abb[71];
z[11] = z[11] + -z[15] + z[27] + -z[31] + z[34] + z[62] + z[68] + z[70];
z[11] = z[11] * z[30];
z[11] = abb[78] + z[11];
z[5] = z[5] + z[17];
z[5] = abb[38] * z[5];
z[15] = z[4] * z[17];
z[27] = z[4] + -z[14];
z[30] = abb[36] * z[27];
z[34] = 2 * abb[72];
z[71] = -z[29] + z[34];
z[72] = 2 * abb[74];
z[5] = z[5] + -z[15] + -z[30] + z[71] + z[72];
z[5] = z[5] * z[7];
z[7] = -abb[35] + z[46];
z[15] = abb[35] * z[7];
z[15] = z[15] + z[70];
z[73] = abb[39] * z[7];
z[73] = -z[15] + z[28] + z[73];
z[74] = abb[39] + z[7];
z[75] = -abb[38] + z[74];
z[75] = abb[38] * z[75];
z[76] = (T(1) / T(3)) * z[57];
z[75] = -z[72] + z[73] + -z[75] + -z[76];
z[75] = abb[17] * z[75];
z[77] = abb[18] + -abb[21] + -abb[25];
z[77] = abb[48] * z[77];
z[5] = -abb[76] + z[5] + z[11] + z[75] + z[77];
z[26] = -abb[35] + 2 * z[26];
z[26] = abb[35] * z[26];
z[75] = 2 * z[14];
z[77] = -abb[41] + z[75];
z[77] = abb[41] * z[77];
z[78] = 2 * abb[73];
z[79] = 2 * abb[46];
z[80] = 2 * abb[44];
z[75] = abb[39] + z[75];
z[75] = abb[39] * z[75];
z[75] = -z[26] + 2 * z[31] + z[75] + -z[77] + z[78] + z[79] + z[80];
z[75] = abb[6] * z[75];
z[24] = abb[37] + z[24];
z[81] = -z[24] + z[43];
z[82] = abb[40] + z[81];
z[82] = abb[2] * z[82];
z[81] = abb[7] * z[81];
z[81] = -z[81] + z[82];
z[82] = 3 * abb[37];
z[83] = -z[12] + z[82];
z[84] = abb[42] + -z[83];
z[84] = abb[4] * z[84];
z[85] = abb[39] + z[17];
z[86] = -z[42] * z[85];
z[87] = abb[4] + abb[5];
z[88] = abb[39] * z[87];
z[89] = abb[5] * z[14];
z[87] = abb[7] + z[87];
z[87] = abb[40] * z[87];
z[3] = z[3] + -z[81] + z[84] + z[86] + z[87] + z[88] + -z[89];
z[3] = abb[38] * z[3];
z[84] = abb[11] * z[25];
z[50] = -z[50] + z[84];
z[84] = z[12] + -z[14];
z[86] = 2 * abb[41];
z[87] = z[84] + z[86];
z[90] = -abb[39] + z[87];
z[90] = abb[7] * z[90];
z[91] = abb[7] + z[38];
z[91] = abb[40] * z[91];
z[89] = -z[89] + z[91];
z[91] = abb[2] * z[27];
z[92] = z[16] * z[41];
z[93] = z[14] + -z[86];
z[93] = abb[4] * z[93];
z[88] = 4 * z[50] + z[88] + z[89] + -z[90] + -z[91] + -z[92] + z[93];
z[88] = abb[36] * z[88];
z[93] = abb[38] + abb[40];
z[48] = z[48] * z[93];
z[93] = z[28] + z[79];
z[77] = -z[77] + z[93];
z[94] = 3 * abb[35] + -2 * z[10];
z[94] = abb[35] * z[94];
z[48] = 4 * abb[43] + z[29] + -z[48] + z[77] + z[94];
z[95] = 2 * abb[8];
z[48] = z[48] * z[95];
z[96] = -z[29] + z[78];
z[97] = -abb[35] + z[86];
z[97] = abb[35] * z[97];
z[98] = z[70] + z[77] + -z[96] + z[97];
z[98] = abb[3] * z[98];
z[99] = abb[35] + abb[41];
z[99] = z[17] * z[99];
z[99] = -z[31] + z[99];
z[100] = abb[46] + -abb[71] + abb[73] + -z[99];
z[41] = z[41] * z[100];
z[41] = -z[41] + z[48] + -z[98];
z[48] = abb[35] + z[86];
z[48] = abb[35] * z[48];
z[98] = prod_pow(abb[41], 2);
z[100] = z[48] + z[98];
z[101] = z[76] + z[100];
z[102] = z[4] * z[87];
z[27] = -abb[38] * z[27];
z[27] = z[27] + -z[30] + -z[101] + z[102];
z[30] = 2 * abb[1];
z[27] = z[27] * z[30];
z[10] = abb[41] * z[10];
z[102] = -z[10] + z[48] + -z[80] + -z[96];
z[102] = abb[5] * z[102];
z[103] = abb[4] * z[84];
z[104] = abb[5] * z[87];
z[105] = abb[7] * z[14];
z[106] = z[92] + z[103] + -z[104] + -z[105];
z[106] = abb[40] * z[106];
z[19] = abb[42] + z[19];
z[107] = abb[39] + z[19];
z[107] = abb[39] * z[107];
z[108] = z[80] + z[107];
z[109] = abb[35] + z[46];
z[109] = abb[35] * z[109];
z[110] = z[108] + -z[109];
z[111] = -abb[41] * abb[42];
z[111] = z[28] + z[111];
z[112] = abb[40] * z[87];
z[111] = z[79] + z[110] + 2 * z[111] + z[112];
z[111] = abb[2] * z[111];
z[112] = 2 * abb[4];
z[113] = abb[6] * (T(1) / T(3));
z[114] = abb[10] * (T(2) / T(3));
z[115] = abb[5] * (T(1) / T(3)) + (T(-2) / T(3)) * z[0] + z[112] + -z[113] + -z[114];
z[115] = z[57] * z[115];
z[116] = z[29] + z[80];
z[117] = abb[37] + abb[41];
z[117] = -abb[39] + 2 * z[117];
z[117] = abb[39] * z[117];
z[118] = abb[41] * z[9];
z[119] = -z[116] + z[117] + -z[118];
z[119] = abb[11] * z[119];
z[120] = abb[20] * abb[75];
z[120] = z[119] + -z[120];
z[32] = -abb[41] + 2 * z[32];
z[32] = abb[41] * z[32];
z[32] = z[32] + -z[94];
z[94] = 2 * z[29];
z[93] = 4 * abb[72] + z[32] + -z[93] + -z[94];
z[93] = abb[4] * z[93];
z[121] = abb[5] * z[84];
z[33] = -z[33] + -z[121];
z[33] = abb[39] * z[33];
z[122] = abb[37] * abb[41];
z[123] = -z[29] + z[122];
z[124] = 2 * z[123];
z[125] = abb[35] + z[9];
z[125] = abb[35] * z[125];
z[126] = -z[70] + z[125];
z[127] = z[124] + z[126];
z[108] = z[78] + -z[108] + z[127];
z[108] = abb[7] * z[108];
z[2] = -z[2] * z[72];
z[128] = abb[3] + abb[14];
z[112] = -abb[2] + -z[112] + z[128];
z[112] = z[68] * z[112];
z[95] = abb[3] + -abb[6] + z[30] + -z[95];
z[129] = z[38] + -z[95];
z[130] = 2 * abb[47];
z[129] = z[129] * z[130];
z[2] = -abb[77] + z[2] + z[3] + -z[5] + z[27] + z[33] + z[41] + z[75] + z[88] + z[93] + z[102] + z[106] + z[108] + z[111] + z[112] + z[115] + -2 * z[120] + z[129];
z[2] = abb[66] * z[2];
z[3] = z[34] + z[78];
z[27] = abb[37] * abb[39];
z[33] = abb[40] * z[9];
z[26] = z[3] + -z[26] + 4 * z[27] + -3 * z[29] + z[33] + z[77];
z[26] = abb[6] * z[26];
z[33] = abb[39] * z[38];
z[75] = z[33] + z[89];
z[77] = abb[4] * z[87];
z[8] = z[8] + z[84];
z[8] = abb[2] * z[8];
z[8] = z[8] + z[75] + z[77] + -z[90] + z[92];
z[8] = abb[38] * z[8];
z[77] = abb[36] + abb[38];
z[84] = -abb[40] + z[85];
z[84] = z[77] * z[84];
z[88] = abb[39] * z[85];
z[89] = abb[40] * z[14];
z[84] = -z[28] + -z[80] + z[84] + -z[88] + z[89];
z[88] = 2 * abb[9];
z[84] = z[84] * z[88];
z[90] = abb[35] + z[14];
z[90] = -z[38] * z[90];
z[93] = -z[90] + z[105];
z[93] = abb[40] * z[93];
z[84] = abb[77] + z[84] + -z[93];
z[93] = -abb[42] * z[86];
z[93] = z[31] + z[34] + z[79] + z[93] + -z[109];
z[93] = abb[4] * z[93];
z[102] = -abb[35] + abb[40];
z[102] = abb[36] * z[102];
z[105] = -abb[35] + z[9];
z[106] = abb[40] * z[105];
z[108] = abb[35] * z[105];
z[102] = z[34] + -z[102] + z[106] + -z[108];
z[102] = abb[13] * z[102];
z[106] = -z[34] + z[126];
z[111] = z[31] + z[78] + z[106] + z[118];
z[111] = abb[5] * z[111];
z[43] = abb[42] + z[43] + -z[82] + -z[86];
z[43] = abb[39] * z[43];
z[82] = -abb[42] + -z[83];
z[82] = abb[40] * z[82];
z[32] = 4 * abb[44] + z[32] + z[43] + -z[79] + z[82];
z[32] = abb[2] * z[32];
z[43] = -z[12] + z[85];
z[43] = abb[7] * z[43];
z[82] = abb[7] + 2 * z[38];
z[82] = abb[40] * z[82];
z[33] = z[33] + z[43] + z[82] + z[90];
z[43] = z[33] + z[47] + z[91];
z[43] = abb[36] * z[43];
z[47] = z[4] + -z[87];
z[47] = -z[47] * z[77];
z[82] = abb[39] * z[14];
z[47] = z[47] + z[82] + z[89] + -z[101];
z[30] = z[30] * z[47];
z[47] = abb[39] + -abb[41];
z[47] = abb[36] * z[47];
z[83] = abb[72] + -z[27] + z[47] + z[122];
z[85] = (T(5) / T(3)) * z[57];
z[83] = 2 * z[83] + z[85];
z[83] = abb[12] * z[83];
z[89] = abb[21] * abb[48];
z[89] = z[83] + z[89];
z[90] = z[18] + -z[92] + -z[104];
z[90] = abb[39] * z[90];
z[10] = -z[10] + z[94];
z[48] = z[10] + z[48];
z[3] = -z[3] + z[48] + -z[82];
z[3] = abb[7] * z[3];
z[82] = -2 * abb[2] + -abb[4] + -abb[13] + z[128];
z[82] = z[68] * z[82];
z[40] = -abb[13] + -z[40];
z[40] = z[40] * z[62];
z[0] = -z[0] + -z[95];
z[0] = z[0] * z[130];
z[92] = -abb[23] + z[44];
z[92] = abb[75] * z[92];
z[0] = -abb[76] + z[0] + z[3] + z[8] + -z[11] + z[26] + z[30] + z[32] + z[40] + z[41] + z[43] + z[82] + z[84] + 2 * z[89] + z[90] + z[92] + z[93] + -z[102] + z[111];
z[0] = abb[69] * z[0];
z[3] = abb[40] * z[42];
z[3] = z[3] + -z[35] + z[75] + -z[81] + z[103];
z[3] = abb[38] * z[3];
z[8] = z[34] + z[80];
z[11] = prod_pow(abb[39], 2);
z[26] = -abb[40] * z[46];
z[11] = z[8] + z[11] + z[26] + z[31];
z[11] = abb[6] * z[11];
z[25] = -z[25] * z[42];
z[25] = z[25] + z[33] + -z[91];
z[25] = abb[36] * z[25];
z[26] = z[28] + -z[80] + z[106];
z[26] = abb[5] * z[26];
z[13] = -z[13] + z[18] + -z[121];
z[13] = abb[39] * z[13];
z[18] = -z[8] + -z[107] + z[126];
z[18] = abb[7] * z[18];
z[30] = z[12] + z[17];
z[30] = abb[40] * z[30];
z[30] = z[30] + z[110];
z[30] = abb[2] * z[30];
z[32] = -abb[2] + abb[4] + -2 * abb[5];
z[32] = -abb[7] + abb[13] * (T(2) / T(3)) + (T(1) / T(3)) * z[32] + z[113] + -z[114];
z[32] = z[32] * z[57];
z[33] = z[71] + -z[109];
z[33] = abb[4] * z[33];
z[1] = z[1] * z[72];
z[35] = -abb[2] + -abb[4] + abb[13];
z[35] = z[35] * z[68];
z[40] = abb[23] + z[44];
z[40] = abb[75] * z[40];
z[1] = z[1] + z[3] + -z[5] + z[11] + z[13] + z[18] + z[25] + z[26] + z[30] + z[32] + z[33] + z[35] + z[40] + z[84] + z[102];
z[1] = abb[70] * z[1];
z[3] = 2 * abb[26];
z[5] = z[3] * z[16];
z[11] = z[5] + z[58];
z[13] = -z[45] * z[87];
z[16] = z[12] * z[21];
z[18] = z[21] * z[86];
z[25] = abb[39] * z[55];
z[26] = abb[40] * z[55];
z[13] = z[11] + z[13] + -z[16] + -z[18] + z[25] + z[26];
z[16] = abb[38] * z[13];
z[30] = z[12] + -z[17];
z[30] = abb[29] * z[30];
z[32] = z[36] + z[58];
z[33] = 2 * z[21];
z[35] = abb[29] + z[33];
z[35] = abb[40] * z[35];
z[40] = abb[27] * z[24];
z[25] = -z[25] + z[30] + z[32] + -z[35] + z[40];
z[25] = abb[36] * z[25];
z[30] = abb[27] * z[19];
z[35] = abb[29] * z[14];
z[32] = z[30] + z[32] + z[35];
z[32] = abb[40] * z[32];
z[25] = z[25] + z[32];
z[7] = -abb[40] + z[7];
z[7] = abb[36] * z[7];
z[32] = abb[35] * abb[40];
z[7] = z[7] + -z[15] + z[32];
z[15] = abb[31] * z[7];
z[32] = z[34] * z[59];
z[34] = abb[34] * abb[75];
z[15] = -z[15] + -z[32] + z[34];
z[31] = abb[30] * z[31];
z[32] = abb[26] + z[39];
z[34] = z[32] * z[68];
z[41] = abb[26] * z[70];
z[43] = abb[28] * z[28];
z[31] = z[15] + -z[31] + z[34] + z[41] + -z[43];
z[3] = z[3] * z[99];
z[34] = z[69] * z[78];
z[41] = abb[26] + z[21];
z[41] = z[41] * z[79];
z[44] = abb[28] * z[29];
z[3] = -z[3] + z[34] + z[41] + -z[44];
z[34] = z[67] * z[86];
z[34] = -z[3] + z[34];
z[20] = z[12] * z[20];
z[20] = z[10] + z[20];
z[20] = abb[27] * z[20];
z[14] = abb[27] * z[14];
z[11] = z[11] + z[14] + z[35];
z[14] = -abb[39] * z[11];
z[35] = 2 * z[67];
z[41] = z[35] + z[36];
z[41] = abb[35] * z[41];
z[44] = abb[29] * z[48];
z[46] = abb[31] + -z[54];
z[46] = z[46] * z[62];
z[14] = z[14] + z[16] + z[20] + -z[25] + z[31] + z[34] + z[41] + z[44] + z[46];
z[14] = abb[64] * z[14];
z[16] = 2 * abb[27];
z[20] = z[16] + z[37];
z[41] = abb[28] + z[20];
z[41] = abb[39] * z[41];
z[44] = abb[32] * z[74];
z[44] = z[44] + -z[52];
z[17] = abb[30] * z[17];
z[17] = z[17] + z[51];
z[24] = abb[29] * z[24];
z[46] = abb[30] * abb[35];
z[24] = z[17] + z[24] + -z[26] + z[40] + -z[41] + -z[44] + z[46];
z[24] = abb[38] * z[24];
z[26] = abb[28] + z[45];
z[40] = abb[39] * z[26];
z[41] = 2 * abb[28];
z[48] = abb[30] + z[41];
z[48] = abb[35] * z[48];
z[40] = z[40] + z[48];
z[19] = abb[29] * z[19];
z[17] = z[17] + -z[19] + -z[30] + -z[40];
z[17] = abb[39] * z[17];
z[19] = abb[33] * abb[48];
z[17] = -z[17] + -z[19] + z[24];
z[24] = abb[36] * z[13];
z[30] = abb[32] * z[73];
z[52] = z[49] * z[72];
z[26] = z[26] * z[80];
z[26] = -z[26] + -z[30] + z[52];
z[30] = 2 * abb[30];
z[41] = z[30] + z[41];
z[41] = z[28] * z[41];
z[52] = abb[26] + abb[28];
z[52] = z[52] * z[68];
z[32] = z[32] * z[70];
z[32] = z[26] + -z[32] + z[41] + -z[52];
z[35] = z[35] + -z[64];
z[35] = abb[41] * z[35];
z[41] = z[21] * z[130];
z[3] = -z[3] + z[35] + -z[41];
z[35] = abb[29] * z[127];
z[41] = -abb[40] * z[11];
z[52] = -z[64] + z[67];
z[54] = -z[52] + z[53];
z[30] = abb[28] + z[30];
z[30] = abb[35] * z[30];
z[54] = z[30] + 2 * z[54];
z[54] = abb[35] * z[54];
z[59] = z[124] + z[125];
z[59] = abb[27] * z[59];
z[20] = abb[32] + z[20] + z[21];
z[20] = z[20] * z[76];
z[20] = -z[3] + -z[17] + z[20] + z[24] + z[32] + z[35] + z[41] + z[54] + z[59];
z[20] = abb[63] * z[20];
z[24] = z[51] + z[65];
z[35] = z[24] + -z[64];
z[41] = abb[35] + 2 * z[22];
z[51] = abb[27] * z[41];
z[51] = -z[5] + z[51];
z[54] = -abb[30] + z[45];
z[54] = abb[39] * z[54];
z[41] = abb[29] * z[41];
z[35] = -2 * z[35] + z[41] + z[44] + z[48] + z[51] + z[54];
z[35] = abb[38] * z[35];
z[41] = abb[27] * z[105];
z[5] = z[5] + z[41];
z[41] = abb[29] * z[105];
z[24] = z[5] + 2 * z[24] + -z[40] + z[41];
z[24] = abb[39] * z[24];
z[24] = z[24] + z[35];
z[35] = abb[27] + abb[30];
z[35] = z[35] * z[68];
z[40] = z[39] * z[70];
z[28] = z[28] + z[29];
z[28] = abb[30] * z[28];
z[15] = z[15] + z[26] + z[28] + z[35] + -z[40] + z[43];
z[26] = -z[58] + z[64];
z[28] = z[22] * z[37];
z[26] = 2 * z[26] + z[28] + z[36] + z[51] + z[56];
z[26] = abb[36] * z[26];
z[5] = z[5] + -z[36];
z[5] = abb[40] * z[5];
z[5] = z[5] + z[26];
z[22] = z[12] * z[22];
z[26] = z[22] + z[70] + z[98];
z[26] = abb[29] * z[26];
z[28] = z[21] * z[98];
z[35] = -z[53] + z[64];
z[35] = 2 * z[35] + -z[46];
z[35] = abb[35] * z[35];
z[40] = z[97] + z[98];
z[40] = abb[27] * z[40];
z[41] = abb[31] * (T(2) / T(3));
z[43] = abb[32] * (T(1) / T(3)) + z[41];
z[44] = abb[27] * (T(2) / T(3)) + abb[29] * (T(4) / T(3)) + z[21] + -z[43];
z[44] = z[44] * z[57];
z[26] = -z[5] + -z[15] + -z[24] + z[26] + z[28] + z[35] + z[40] + z[44];
z[26] = abb[62] * z[26];
z[12] = z[12] * z[52];
z[35] = -z[108] + -z[124];
z[35] = abb[27] * z[35];
z[37] = -z[37] * z[123];
z[39] = abb[29] * (T(-5) / T(3)) + -z[39] + z[41];
z[39] = z[39] * z[57];
z[40] = 2 * abb[45];
z[21] = abb[31] + z[21];
z[41] = z[21] * z[40];
z[3] = z[3] + z[5] + z[12] + z[31] + z[35] + z[37] + z[39] + z[41];
z[3] = abb[60] * z[3];
z[5] = z[10] + z[22];
z[10] = -abb[27] * z[5];
z[5] = -z[5] + -z[70];
z[5] = abb[29] * z[5];
z[12] = -z[53] + z[67];
z[12] = -2 * z[12] + z[46];
z[12] = abb[35] * z[12];
z[22] = z[49] * z[76];
z[5] = z[5] + z[10] + z[12] + z[22] + z[24] + z[32] + -z[34];
z[5] = abb[61] * z[5];
z[10] = -z[13] * z[77];
z[12] = -z[76] + -z[130];
z[12] = z[12] * z[55];
z[4] = z[4] * z[11];
z[11] = -z[45] * z[100];
z[13] = -z[18] + -z[36];
z[13] = abb[35] * z[13];
z[4] = z[4] + z[10] + z[11] + z[12] + z[13] + -z[28];
z[4] = z[4] * z[66];
z[9] = -abb[28] * z[9];
z[9] = z[9] + -z[30];
z[9] = abb[35] * z[9];
z[10] = -abb[29] * z[126];
z[11] = abb[27] + z[33];
z[11] = abb[29] + (T(1) / T(3)) * z[11] + -z[43];
z[11] = z[11] * z[57];
z[12] = -prod_pow(abb[35], 2) * z[16];
z[9] = z[9] + z[10] + z[11] + z[12] + -z[15] + z[17] + z[25];
z[9] = abb[65] * z[9];
z[8] = z[8] + 2 * z[47] + z[85] + z[96] + -z[117] + 4 * z[122];
z[8] = abb[1] * z[8];
z[10] = abb[73] + z[27] + -z[29];
z[10] = z[10] * z[42];
z[11] = 2 * abb[36];
z[11] = z[11] * z[50];
z[8] = -z[8] + z[10] + z[11] + z[83] + -z[119];
z[10] = 2 * z[23];
z[8] = z[8] * z[10];
z[10] = abb[38] + -abb[39];
z[11] = -abb[39] + z[105];
z[10] = z[10] * z[11];
z[10] = z[10] + z[68] + -z[108] + z[116];
z[10] = abb[15] * z[10];
z[11] = abb[22] * abb[48];
z[10] = z[10] + z[11];
z[11] = -abb[66] + abb[70];
z[10] = z[10] * z[11];
z[11] = -abb[64] + abb[65];
z[12] = -abb[62] + -z[11];
z[12] = z[12] * z[21];
z[13] = -z[38] + z[88];
z[15] = -abb[2] + abb[6];
z[16] = -z[13] + z[15];
z[16] = abb[70] * z[16];
z[13] = -z[13] + -z[15];
z[13] = abb[69] * z[13];
z[12] = z[12] + z[13] + z[16];
z[12] = z[12] * z[40];
z[13] = -abb[61] + abb[64];
z[13] = z[13] * z[45];
z[15] = abb[62] * z[55];
z[13] = z[13] + z[15];
z[13] = z[13] * z[130];
z[7] = -z[7] + z[40] + z[62];
z[7] = z[7] * z[63];
z[15] = z[19] * z[60];
z[11] = -abb[63] + z[11] + z[66];
z[11] = abb[58] * z[11];
z[16] = -abb[66] + -z[61];
z[16] = abb[79] * z[16];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[20] + z[26];
z[0] = 2 * z[0];
return {z[6], z[0]};
}


template <typename T> std::complex<T> f_4_823_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{T(0),stof<T>("-43.326309677614922727734444793627968626357572945891214112613952377")}, std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("10.2312275182042318649043666124740212509707261335343338040808817496"),stof<T>("5.344960794459669883860928844665613777776125166322616271306655103")}, std::complex<T>{stof<T>("5.201151409565616668135970091595661068285138441238639659659628698"),stof<T>("15.278460349735009732185330308359639027338778762194082558949622032")}, std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("22.702888533420243111688185640602715821242669017374515282357675242")}, std::complex<T>{stof<T>("10.23122751820423186490436661247402125097072613353433380408088175"),stof<T>("-16.318194044347791480006293552148370535402661306622990785000321086")}, std::complex<T>{stof<T>("21.218442766049301460020153152194451887324690016793731910269612578"),stof<T>("41.235454068598596870617160596564355452954487097259701094409026511")}, std::complex<T>{stof<T>("25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("25.302557486369301199240572550949005734949124827372778298499792718"),stof<T>("35.155922194044081085036539936540929621236005412970168900628053236")}, std::complex<T>{stof<T>("27.684105602702568343335706257260005235672152911204732120021121204"),stof<T>("19.779461597949540164356049334922093363520271008752783240987601397")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_823_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_823_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-17.54788233364647729229789933765235065335508026836022713055770326"),stof<T>("38.493820526471870964829953237337169380356630871743224037035876198")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({199});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,81> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_12_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_12_re(k), f_2_24_re(k), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W163(k,dv) * f_2_31(k);
abb[76] = c.real();
abb[54] = c.imag();
SpDLog_Sigma5<T,199,162>(k, dv, abb[76], abb[54], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W166(k,dv) * f_2_31(k);
abb[77] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,199,165>(k, dv, abb[77], abb[55], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W177(k,dv) * f_2_31(k);
abb[78] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,199,176>(k, dv, abb[78], abb[56], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W179(k,dv) * f_2_31(k);
abb[79] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,199,178>(k, dv, abb[79], abb[57], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W190(k,dv) * f_2_31(k);
abb[80] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,199,189>(k, dv, abb[80], abb[58], f_2_31_series_coefficients<T>);
}

                    
            return f_4_823_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_823_DLogXconstant_part(base_point<T>, kend);
	value += f_4_823_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_823_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_823_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_823_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_823_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_823_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_823_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
