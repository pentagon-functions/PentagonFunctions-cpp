/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_278.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_278_abbreviated (const std::array<T,57>& abb) {
T z[118];
z[0] = -abb[41] + abb[48];
z[1] = abb[42] + z[0];
z[2] = abb[46] * (T(1) / T(3));
z[3] = abb[45] + -abb[49];
z[4] = abb[50] + z[3];
z[5] = abb[43] * (T(1) / T(2));
z[6] = abb[44] * (T(1) / T(2));
z[7] = (T(5) / T(6)) * z[1] + -z[2] + (T(-1) / T(2)) * z[4] + z[5] + -z[6];
z[7] = abb[7] * z[7];
z[8] = abb[46] * (T(1) / T(2));
z[9] = z[5] + z[8];
z[10] = -abb[51] + abb[45] * (T(1) / T(2));
z[11] = abb[41] * (T(1) / T(2));
z[12] = -z[9] + z[10] + z[11];
z[12] = abb[11] * z[12];
z[13] = abb[11] * abb[48];
z[12] = z[12] + (T(-1) / T(2)) * z[13];
z[14] = abb[11] * z[6];
z[12] = (T(1) / T(3)) * z[12] + z[14];
z[14] = abb[43] + -abb[50];
z[2] = -z[2] + z[3] + -z[14];
z[15] = -abb[47] + z[6];
z[2] = abb[42] * (T(-1) / T(3)) + (T(2) / T(3)) * z[0] + (T(1) / T(2)) * z[2] + -z[15];
z[2] = abb[4] * z[2];
z[16] = abb[50] * (T(5) / T(2));
z[17] = abb[49] + abb[51] * (T(-11) / T(3)) + abb[43] * (T(2) / T(3)) + abb[45] * (T(5) / T(6)) + -z[16];
z[18] = abb[47] * (T(9) / T(2));
z[19] = abb[42] * (T(-2) / T(3)) + (T(-11) / T(6)) * z[0] + -z[17] + -z[18];
z[19] = abb[0] * z[19];
z[20] = abb[21] + abb[23];
z[21] = -abb[52] + abb[53];
z[22] = z[20] * z[21];
z[23] = abb[45] + abb[46];
z[24] = -abb[49] + z[23];
z[25] = -z[14] + z[24];
z[25] = (T(1) / T(2)) * z[25];
z[26] = -abb[42] + -z[15] + z[25];
z[27] = abb[12] * z[26];
z[28] = (T(1) / T(2)) * z[21];
z[29] = abb[20] * z[28];
z[30] = -z[27] + z[29];
z[31] = abb[24] + abb[25];
z[32] = abb[22] + z[31];
z[32] = z[28] * z[32];
z[24] = z[14] + z[24];
z[24] = (T(1) / T(2)) * z[24];
z[33] = -z[0] + z[24];
z[34] = z[6] + z[33];
z[35] = abb[14] * z[34];
z[36] = abb[46] + z[14];
z[37] = -z[0] + z[36];
z[38] = abb[2] * z[37];
z[39] = abb[44] * (T(9) / T(2));
z[17] = abb[42] * (T(-10) / T(3)) + abb[46] * (T(8) / T(3)) + -z[17] + -z[39];
z[17] = abb[8] * z[17];
z[40] = abb[50] * (T(3) / T(2));
z[41] = abb[42] * (T(-13) / T(6)) + abb[43] * (T(-1) / T(3)) + abb[47] * (T(11) / T(2)) + abb[46] * (T(11) / T(6)) + (T(11) / T(3)) * z[10] + -z[40];
z[41] = abb[1] * z[41];
z[2] = z[2] + z[7] + 11 * z[12] + z[17] + z[19] + z[22] + z[30] + z[32] + -z[35] + (T(7) / T(2)) * z[38] + z[41];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[7] = 5 * abb[45];
z[12] = 3 * abb[49];
z[17] = z[7] + -z[12];
z[19] = abb[50] * (T(7) / T(4));
z[32] = abb[46] * (T(5) / T(4));
z[41] = abb[43] * (T(5) / T(4));
z[42] = abb[44] * (T(3) / T(4));
z[43] = 3 * abb[47];
z[44] = abb[51] + (T(-1) / T(4)) * z[17] + z[19] + -z[32] + -z[41] + z[42] + -z[43];
z[44] = abb[13] * z[44];
z[45] = abb[46] * (T(3) / T(4));
z[46] = z[42] + z[45];
z[47] = -abb[45] + z[12];
z[48] = -abb[51] + (T(1) / T(4)) * z[47];
z[49] = abb[50] * (T(5) / T(4));
z[50] = abb[43] * (T(3) / T(4));
z[51] = -z[0] + z[46] + z[48] + -z[49] + z[50];
z[51] = abb[7] * z[51];
z[47] = (T(1) / T(2)) * z[47];
z[52] = 2 * abb[51];
z[53] = z[47] + -z[52];
z[54] = abb[50] * (T(1) / T(2));
z[55] = z[5] + -z[53] + z[54];
z[56] = z[8] + z[55];
z[57] = abb[15] * z[56];
z[58] = abb[50] * (T(1) / T(4));
z[59] = abb[45] + abb[49];
z[60] = -abb[51] + -z[50] + z[58] + (T(1) / T(4)) * z[59];
z[45] = abb[44] * (T(5) / T(4)) + -z[45] + z[60];
z[61] = 3 * abb[5];
z[62] = z[45] * z[61];
z[63] = -abb[44] + z[36];
z[64] = abb[47] + z[63];
z[64] = abb[10] * z[64];
z[65] = (T(3) / T(4)) * z[21];
z[66] = abb[24] * z[65];
z[67] = 3 * z[64] + z[66];
z[68] = abb[43] + abb[46];
z[69] = -abb[45] + z[52];
z[70] = -abb[41] + z[68] + z[69];
z[70] = abb[11] * z[70];
z[13] = z[13] + z[70];
z[70] = 3 * abb[44];
z[71] = -abb[11] + abb[15] * (T(-1) / T(2));
z[71] = z[70] * z[71];
z[72] = z[43] + -z[52];
z[73] = abb[45] + z[72];
z[74] = -abb[50] + z[0] + z[73];
z[74] = abb[0] * z[74];
z[44] = z[13] + -z[38] + z[44] + z[51] + z[57] + z[62] + z[67] + z[71] + z[74];
z[44] = prod_pow(abb[32], 2) * z[44];
z[51] = 2 * abb[42];
z[62] = abb[47] * (T(3) / T(2));
z[71] = z[51] + -z[62];
z[74] = abb[43] + -abb[46];
z[11] = abb[48] * (T(1) / T(2)) + -z[11];
z[75] = z[10] + -z[11] + z[54] + -z[71] + -z[74];
z[75] = abb[4] * z[75];
z[76] = -abb[51] + abb[49] * (T(3) / T(2));
z[77] = abb[42] * (T(1) / T(2));
z[5] = (T(-3) / T(2)) * z[0] + z[5] + z[23] + -z[76] + -z[77];
z[5] = abb[7] * z[5];
z[78] = abb[20] * z[65];
z[79] = abb[22] * z[65];
z[78] = (T(3) / T(2)) * z[27] + -z[78] + -z[79];
z[80] = 2 * abb[43];
z[81] = -z[72] + z[80];
z[82] = 3 * abb[42];
z[83] = -abb[50] + -z[23] + z[81] + z[82];
z[84] = abb[1] * z[83];
z[85] = z[78] + z[84];
z[86] = abb[46] * (T(1) / T(4));
z[87] = abb[43] * (T(1) / T(4));
z[88] = z[86] + z[87];
z[48] = -z[42] + -z[48] + z[58] + z[88];
z[48] = abb[8] * z[48];
z[89] = abb[44] * (T(3) / T(2));
z[90] = abb[15] * z[89];
z[57] = -z[57] + z[90];
z[24] = -z[15] + z[24];
z[90] = abb[3] * z[24];
z[91] = z[57] + (T(3) / T(2)) * z[90];
z[92] = abb[25] * z[65];
z[66] = (T(3) / T(2)) * z[35] + -z[66] + -z[92];
z[65] = z[20] * z[65];
z[92] = 3 * z[3];
z[43] = z[43] + z[92];
z[93] = abb[42] + z[14];
z[94] = z[43] + z[93];
z[95] = abb[0] * (T(1) / T(2));
z[94] = z[94] * z[95];
z[5] = z[5] + z[38] + z[48] + z[65] + -z[66] + z[75] + -z[85] + z[91] + z[94];
z[5] = abb[27] * z[5];
z[48] = -z[11] + z[46];
z[75] = abb[45] + z[12];
z[94] = -z[52] + (T(1) / T(4)) * z[75];
z[41] = -z[41] + z[48] + z[58] + -z[71] + z[94];
z[41] = abb[4] * z[41];
z[58] = 2 * z[38];
z[66] = -z[58] + z[66];
z[56] = z[56] + -z[89];
z[96] = abb[13] * z[56];
z[92] = -3 * abb[46] + -5 * z[14] + -z[92];
z[92] = -abb[42] + 3 * z[0] + -z[89] + (T(1) / T(2)) * z[92];
z[97] = abb[7] * (T(1) / T(2));
z[92] = z[92] * z[97];
z[43] = -z[43] + z[93];
z[43] = z[43] * z[95];
z[41] = z[41] + z[43] + z[66] + -z[85] + z[92] + z[96];
z[41] = abb[31] * z[41];
z[43] = 2 * abb[45] + -z[12];
z[92] = abb[50] + z[52];
z[95] = z[43] + z[92];
z[98] = -z[0] + z[95];
z[98] = abb[0] * z[98];
z[21] = (T(3) / T(2)) * z[21];
z[99] = abb[25] * z[21];
z[99] = 3 * z[35] + -z[99];
z[98] = 4 * z[38] + z[98] + -z[99];
z[100] = abb[4] * z[56];
z[101] = abb[8] * z[56];
z[102] = z[20] * z[21];
z[101] = z[101] + z[102];
z[103] = z[100] + z[101];
z[104] = abb[24] * z[21];
z[105] = -z[96] + z[104];
z[106] = 2 * abb[7];
z[37] = z[37] * z[106];
z[107] = abb[11] * z[70];
z[107] = -z[13] + z[107];
z[37] = z[37] + z[98] + z[103] + z[105] + z[107];
z[37] = abb[32] * z[37];
z[108] = 2 * abb[50];
z[73] = z[73] + -z[108];
z[109] = z[68] + z[73];
z[109] = abb[13] * z[109];
z[110] = 2 * z[109];
z[103] = z[103] + z[110];
z[111] = 4 * abb[51];
z[112] = abb[45] + abb[50];
z[113] = -z[12] + z[68] + z[111] + z[112];
z[113] = abb[15] * z[113];
z[56] = abb[7] * z[56];
z[56] = z[56] + -z[113];
z[114] = abb[15] * z[70];
z[115] = z[56] + z[114];
z[116] = 3 * z[90] + -z[103] + -z[115];
z[117] = -abb[29] * z[116];
z[5] = z[5] + -z[37] + z[41] + z[117];
z[5] = abb[27] * z[5];
z[19] = abb[44] * (T(9) / T(4)) + -z[19] + -z[32] + z[50] + z[51] + z[94];
z[19] = abb[8] * z[19];
z[32] = -abb[45] + z[8] + -z[54] + z[76] + -z[77];
z[32] = abb[7] * z[32];
z[41] = abb[42] + -abb[46];
z[47] = z[41] + -z[47] + -z[62] + z[92];
z[47] = abb[1] * z[47];
z[50] = -abb[50] + z[9] + z[10] + z[62];
z[50] = abb[13] * z[50];
z[14] = -abb[46] + z[3] + z[14];
z[14] = abb[42] + z[6] + (T(1) / T(2)) * z[14];
z[76] = abb[6] * z[14];
z[79] = (T(3) / T(2)) * z[76] + z[79];
z[19] = z[19] + -z[32] + z[47] + z[50] + -z[65] + -z[67] + -z[79] + z[91];
z[47] = prod_pow(abb[29], 2);
z[19] = z[19] * z[47];
z[50] = 9 * abb[49] + -z[7];
z[48] = z[48] + -z[49] + (T(1) / T(4)) * z[50] + -z[52] + -z[77] + z[87];
z[48] = abb[7] * z[48];
z[18] = z[18] + -z[111];
z[7] = -z[7] + -z[12];
z[7] = 4 * abb[42] + abb[43] * (T(13) / T(4)) + (T(1) / T(4)) * z[7] + -z[11] + -z[18] + -z[46] + -z[49];
z[7] = abb[4] * z[7];
z[11] = -abb[11] + abb[15];
z[11] = z[11] * z[70];
z[11] = z[11] + z[13] + -z[113];
z[12] = -z[0] + z[55] + -z[62] + z[77];
z[12] = abb[0] * z[12];
z[12] = z[12] + -z[66];
z[7] = z[7] + -z[11] + z[12] + z[48] + z[78] + 2 * z[84] + -z[96];
z[7] = abb[27] * z[7];
z[46] = 3 * abb[43];
z[48] = 3 * z[4] + -z[46];
z[50] = 5 * abb[46];
z[55] = z[48] + z[50];
z[55] = -z[0] + (T(1) / T(2)) * z[55];
z[55] = -z[42] + (T(1) / T(2)) * z[55] + -z[71];
z[55] = abb[4] * z[55];
z[48] = z[48] + -z[50];
z[48] = z[1] + (T(1) / T(2)) * z[48] + z[89];
z[48] = z[48] * z[97];
z[12] = z[12] + -z[48] + z[55] + -z[85] + z[101] + z[107];
z[48] = -abb[31] * z[12];
z[50] = 2 * abb[46];
z[55] = z[50] + -z[70];
z[65] = -z[52] + -z[55] + -z[80] + z[112];
z[66] = -abb[8] * z[65];
z[67] = z[0] + -z[51] + -z[81] + z[112];
z[71] = -abb[0] * z[67];
z[77] = 2 * abb[4];
z[78] = abb[7] + -z[77];
z[80] = -abb[46] + z[1];
z[78] = z[78] * z[80];
z[66] = -z[38] + z[66] + z[71] + z[78] + -z[84] + z[107];
z[66] = abb[30] * z[66];
z[71] = 2 * abb[8];
z[78] = z[71] + z[77];
z[65] = z[65] * z[78];
z[59] = -z[52] + (T(1) / T(2)) * z[59];
z[78] = abb[43] * (T(-3) / T(2)) + z[54] + z[59];
z[81] = abb[44] * (T(-5) / T(2)) + abb[46] * (T(3) / T(2)) + -z[78];
z[85] = z[61] * z[81];
z[65] = z[65] + z[85] + -z[105] + z[115];
z[87] = -abb[29] + abb[32];
z[65] = z[65] * z[87];
z[7] = z[7] + z[48] + z[65] + z[66];
z[7] = abb[30] * z[7];
z[48] = -2 * z[0] + -z[40] + -z[53] + (T(5) / T(2)) * z[68] + -z[89];
z[48] = abb[7] * z[48];
z[16] = z[16] + (T(1) / T(2)) * z[17] + z[39] + -z[52] + (T(-7) / T(2)) * z[68];
z[17] = abb[4] + abb[8];
z[16] = z[16] * z[17];
z[39] = abb[11] + abb[15];
z[39] = z[39] * z[70];
z[16] = -z[13] + z[16] + z[39] + z[48] + z[98] + z[102] + -z[113];
z[39] = abb[55] * z[16];
z[48] = abb[47] + z[3];
z[53] = z[48] + -z[93];
z[53] = abb[1] * z[53];
z[26] = abb[4] * z[26];
z[65] = z[20] * z[28];
z[66] = abb[8] * z[14];
z[63] = -z[3] + z[63];
z[68] = z[63] * z[97];
z[26] = z[26] + z[30] + z[53] + z[65] + z[66] + z[68] + -z[76];
z[26] = 3 * z[26];
z[30] = abb[54] * z[26];
z[42] = -z[42] + z[49] + -z[62] + -z[88] + -z[94];
z[42] = abb[13] * z[42];
z[10] = -z[10] + -z[41] + z[54] + -z[89];
z[10] = abb[8] * z[10];
z[18] = -5 * abb[42] + abb[50] + z[18] + -z[46] + z[50] + (T(1) / T(2)) * z[75];
z[18] = abb[1] * z[18];
z[46] = -abb[42] + abb[47];
z[49] = abb[44] * (T(1) / T(4)) + z[46] + z[60] + z[86];
z[49] = abb[4] * z[49];
z[10] = z[10] + z[18] + -z[32] + z[42] + 3 * z[49] + z[57] + z[79];
z[10] = abb[28] * z[10];
z[18] = 3 * z[27];
z[27] = abb[20] * z[21];
z[27] = -z[18] + z[27] + z[115];
z[32] = z[77] * z[83];
z[21] = abb[22] * z[21];
z[42] = z[21] + z[96];
z[32] = z[27] + -z[32] + z[42] + -3 * z[84] + z[101];
z[49] = -abb[27] + abb[30];
z[32] = z[32] * z[49];
z[49] = z[55] + z[69];
z[50] = -abb[50] + -z[49] + z[51];
z[50] = abb[8] * z[50];
z[23] = abb[42] + abb[50] + -z[23] + -z[72];
z[23] = abb[1] * z[23];
z[53] = z[41] + z[95];
z[55] = -abb[7] * z[53];
z[23] = z[23] + z[50] + z[55] + -z[109] + z[113] + -z[114];
z[23] = abb[29] * z[23];
z[50] = 3 * z[76];
z[55] = z[50] + -z[100];
z[57] = z[71] + z[106];
z[57] = z[57] * z[93];
z[53] = abb[1] * z[53];
z[42] = -z[42] + z[53] + -z[55] + z[57];
z[53] = abb[31] * z[42];
z[10] = z[10] + z[23] + z[32] + z[53];
z[10] = abb[28] * z[10];
z[23] = abb[13] * z[24];
z[23] = z[23] + -z[90];
z[24] = abb[8] * z[63];
z[22] = -z[22] + z[24];
z[14] = abb[7] * z[14];
z[24] = abb[22] * z[28];
z[32] = abb[1] * z[48];
z[14] = z[14] + (T(1) / T(2)) * z[22] + -z[23] + -z[24] + z[32] + -z[76];
z[14] = abb[35] * z[14];
z[22] = z[28] * z[31];
z[31] = abb[7] * z[34];
z[34] = abb[0] * z[48];
z[22] = z[22] + -z[23] + z[31] + z[34] + -z[35] + z[38];
z[22] = abb[33] * z[22];
z[23] = 2 * abb[47];
z[31] = z[6] + z[23];
z[9] = z[9] + z[31] + -z[40] + z[59];
z[9] = abb[13] * z[9];
z[9] = z[9] + -z[90];
z[20] = -abb[24] + -z[20];
z[20] = z[20] * z[28];
z[17] = abb[5] + -z[17];
z[17] = z[17] * z[81];
z[17] = -z[9] + z[17] + z[20];
z[17] = abb[36] * z[17];
z[14] = z[14] + z[17] + z[22];
z[3] = -z[3] + z[36];
z[3] = -z[0] + (T(1) / T(2)) * z[3] + z[15];
z[3] = abb[17] * z[3];
z[4] = -z[4] + z[74];
z[4] = (T(1) / T(2)) * z[4] + -z[46];
z[4] = abb[18] * z[4];
z[15] = abb[16] + -abb[18] + abb[19];
z[15] = z[6] * z[15];
z[1] = z[1] + -z[25];
z[1] = abb[16] * z[1];
z[17] = abb[19] * z[33];
z[20] = abb[26] * z[28];
z[1] = z[1] + -z[3] + z[4] + -z[15] + -z[17] + z[20];
z[3] = abb[56] * z[1];
z[4] = -abb[29] * z[42];
z[15] = -abb[0] + abb[1] + -abb[8];
z[15] = z[15] * z[93];
z[17] = abb[4] * z[80];
z[15] = z[15] + z[17] + z[38];
z[15] = abb[31] * z[15];
z[4] = z[4] + z[15] + z[37];
z[4] = abb[31] * z[4];
z[8] = z[8] + z[31] + -z[51] + z[78];
z[8] = abb[4] * z[8];
z[8] = z[8] + -z[9] + z[24] + z[29] + -z[84];
z[8] = 3 * z[8] + -z[18];
z[8] = abb[34] * z[8];
z[9] = -abb[37] * z[116];
z[15] = abb[47] + abb[49];
z[17] = abb[44] + z[15];
z[18] = z[17] + -z[92];
z[20] = abb[29] * z[18];
z[17] = z[17] + -z[52];
z[22] = abb[42] + abb[43];
z[24] = z[17] + -z[22];
z[25] = abb[30] * z[24];
z[20] = z[20] + -z[25];
z[6] = -abb[51] + z[6] + (T(1) / T(2)) * z[15];
z[15] = -z[6] + z[22] + -z[54];
z[15] = abb[28] * z[15];
z[24] = abb[27] * z[24];
z[15] = z[15] + z[20] + z[24];
z[15] = abb[28] * z[15];
z[6] = -z[6] + z[54];
z[6] = abb[27] * z[6];
z[6] = z[6] + -z[20];
z[6] = abb[27] * z[6];
z[20] = abb[36] + -abb[37];
z[18] = z[18] * z[20];
z[20] = abb[34] * z[93];
z[6] = z[6] + z[15] + z[18] + z[20];
z[15] = 3 * abb[9];
z[6] = z[6] * z[15];
z[18] = -z[45] * z[47];
z[20] = abb[55] * z[81];
z[18] = z[18] + z[20];
z[18] = z[18] * z[61];
z[2] = z[2] + (T(3) / T(2)) * z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + 3 * z[14] + z[18] + z[19] + z[30] + z[39] + z[44];
z[3] = -abb[43] + z[49] + -z[82] + z[108];
z[3] = z[3] * z[71];
z[4] = 6 * z[64] + -z[85];
z[5] = abb[49] + z[23] + -z[92];
z[6] = z[5] + -z[41];
z[6] = abb[1] * z[6];
z[3] = z[3] + z[4] + 3 * z[6] + z[21] + z[55] + z[104] + -z[110] + -z[115];
z[3] = abb[29] * z[3];
z[5] = -z[0] + -z[5];
z[5] = abb[0] * z[5];
z[5] = -z[5] + z[13];
z[6] = 3 * abb[11] + abb[15];
z[6] = z[6] * z[70];
z[4] = -z[4] + -3 * z[5] + z[6] + 6 * z[38] + z[56] + -z[99] + z[103];
z[4] = abb[32] * z[4];
z[5] = abb[4] * z[67];
z[6] = z[22] + -z[108];
z[7] = z[0] + z[6] + -z[43] + -z[52];
z[7] = abb[7] * z[7];
z[0] = -z[0] + -z[22] + -z[73];
z[0] = abb[0] * z[0];
z[0] = z[0] + z[5] + z[7] + -z[11] + z[58] + -z[84] + -z[110];
z[0] = abb[27] * z[0];
z[5] = -abb[30] + -abb[31];
z[5] = z[5] * z[12];
z[7] = z[27] + 3 * z[32] + -z[50] + z[103];
z[7] = abb[28] * z[7];
z[6] = z[6] + z[17];
z[8] = abb[27] + -abb[28];
z[6] = z[6] * z[8] * z[15];
z[0] = z[0] + z[3] + z[4] + z[5] + z[6] + z[7];
z[0] = m1_set::bc<T>[0] * z[0];
z[3] = z[16] + z[85];
z[3] = abb[39] * z[3];
z[4] = abb[38] * z[26];
z[1] = abb[40] * z[1];
z[0] = z[0] + (T(3) / T(2)) * z[1] + z[3] + z[4];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_278_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("-60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-50.753230340904010132424722396808666651715583559439082167006918678"),stof<T>("34.081682310324995686088672650457391383990622786459470662902339143")}, std::complex<T>{stof<T>("-6.4728848884311135370344688408294995525324252005990025093280016611"),stof<T>("-7.46249334498498083634381434235989191476518227252497846562749018")}, std::complex<T>{stof<T>("-13.490678737724229576388000192776270368528148022631162466132111686"),stof<T>("-7.950728224719451816036195091289869905517934538622874076227542381")}, std::complex<T>{stof<T>("7.7691405800748865872202464486523257300927602883641503557166568482"),stof<T>("0.9279987808995460879225665469713760549679629066638179096498495597")}, std::complex<T>{stof<T>("44.280345452472896595390253555979167099183158358840079657678917017"),stof<T>("-41.544175655309976522432486992817283298755805058984449128529829323")}, std::complex<T>{stof<T>("45.563567763947112572112705731626154617787502660050213118301287389"),stof<T>("30.572639609146429529044097999753893635241788726625451800264414552")}, std::complex<T>{stof<T>("-18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("4.3827336429991116170319830964464535294905368861632997925096025793"),stof<T>("9.9189575208641697246201016337749477824099827340055619975436617462")}, std::complex<T>{stof<T>("-5.6789893346428846672177607042692797070508719739284476388982577664"),stof<T>("-3.3844629567787349761988538383864319226127633681444014415660211259")}, std::complex<T>{stof<T>("-24.303748446147996408504459090197558519166594349054900296452518855"),stof<T>("-21.693912603527431625085336361492647674755891281338759814387022612")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[72].real()/kbase.W[72].real()), C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_278_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_278_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-26.986672380774296321390070915721496290329239903904055530551261089"),stof<T>("42.839273860445306593362705682689067954951045754876709402369339523")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W18(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W73(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[72].real()/k.W[72].real()), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_278_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_278_DLogXconstant_part(base_point<T>, kend);
	value += f_4_278_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_278_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_278_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_278_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_278_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_278_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_278_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
