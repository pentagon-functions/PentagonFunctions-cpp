/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_99.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_99_abbreviated (const std::array<T,60>& abb) {
T z[108];
z[0] = abb[3] * (T(1) / T(2));
z[1] = abb[33] + -abb[34];
z[2] = abb[35] + z[1];
z[3] = -abb[30] + z[2];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = z[0] * z[3];
z[5] = abb[0] * (T(1) / T(2));
z[6] = -abb[41] + z[3];
z[7] = z[5] * z[6];
z[8] = m1_set::bc<T>[0] * z[1];
z[9] = abb[13] * z[8];
z[4] = z[4] + z[7] + z[9];
z[10] = -abb[30] + abb[32];
z[11] = m1_set::bc<T>[0] * z[10];
z[12] = abb[41] + -abb[42];
z[13] = z[11] + -z[12];
z[14] = abb[5] * z[13];
z[15] = -abb[31] + abb[33];
z[10] = z[10] + z[15];
z[16] = m1_set::bc<T>[0] * z[10];
z[17] = abb[15] * z[16];
z[18] = -abb[32] + abb[35];
z[18] = m1_set::bc<T>[0] * z[18];
z[19] = abb[12] * z[18];
z[17] = -z[17] + z[19];
z[19] = abb[41] * (T(1) / T(2));
z[20] = -z[16] + z[19];
z[20] = abb[8] * z[20];
z[21] = abb[3] + abb[8];
z[22] = abb[43] * (T(1) / T(2));
z[23] = -z[21] * z[22];
z[24] = -abb[43] + z[6];
z[25] = abb[17] * z[24];
z[26] = (T(1) / T(2)) * z[25];
z[11] = abb[9] * z[11];
z[27] = abb[6] * z[18];
z[28] = -abb[42] + z[18];
z[29] = abb[10] * z[28];
z[20] = z[4] + -z[11] + z[14] + -z[17] + z[20] + z[23] + -z[26] + z[27] + z[29];
z[23] = abb[31] + abb[34];
z[27] = abb[30] * (T(1) / T(2));
z[29] = -abb[33] + z[27];
z[23] = (T(1) / T(2)) * z[23] + z[29];
z[30] = abb[35] * (T(1) / T(2));
z[31] = z[23] + -z[30];
z[31] = m1_set::bc<T>[0] * z[31];
z[32] = abb[2] * z[31];
z[20] = (T(1) / T(2)) * z[20] + z[32];
z[32] = abb[49] * (T(1) / T(4));
z[20] = z[20] * z[32];
z[33] = -z[12] + z[16];
z[33] = abb[15] * z[33];
z[34] = abb[30] + -abb[34];
z[35] = abb[31] + z[34];
z[36] = -abb[32] + z[35];
z[37] = m1_set::bc<T>[0] * z[36];
z[37] = z[12] + z[37];
z[38] = abb[8] * z[37];
z[39] = -abb[3] * z[8];
z[40] = abb[3] + -abb[8];
z[41] = abb[43] * z[40];
z[2] = -abb[32] + z[2];
z[2] = m1_set::bc<T>[0] * z[2];
z[2] = -abb[42] + z[2];
z[42] = -abb[43] + z[2];
z[43] = abb[6] * z[42];
z[14] = z[14] + -z[25] + z[33] + z[38] + z[39] + z[41] + z[43];
z[33] = abb[7] * (T(1) / T(2));
z[38] = abb[10] + -z[33];
z[38] = z[28] * z[38];
z[39] = abb[31] + -abb[34];
z[41] = m1_set::bc<T>[0] * z[39];
z[41] = -abb[43] + z[41];
z[43] = abb[2] * z[41];
z[14] = (T(1) / T(2)) * z[14] + z[38] + z[43];
z[38] = abb[53] * (T(1) / T(8));
z[14] = z[14] * z[38];
z[12] = z[12] + z[16];
z[16] = abb[15] * (T(1) / T(2));
z[12] = z[12] * z[16];
z[43] = abb[5] * (T(1) / T(2));
z[13] = z[13] * z[43];
z[44] = abb[32] * (T(1) / T(2));
z[23] = z[23] + -z[44];
z[23] = m1_set::bc<T>[0] * z[23];
z[45] = abb[42] * (T(1) / T(2));
z[23] = z[23] + -z[45];
z[23] = abb[8] * z[23];
z[46] = -abb[30] + abb[35];
z[46] = m1_set::bc<T>[0] * z[46];
z[47] = z[0] * z[46];
z[48] = abb[14] * abb[42];
z[12] = z[7] + z[12] + z[13] + z[23] + z[47] + z[48];
z[13] = abb[6] * (T(1) / T(16));
z[23] = z[13] * z[42];
z[42] = abb[7] * (T(1) / T(16));
z[28] = -z[28] * z[42];
z[11] = (T(-1) / T(8)) * z[11] + (T(1) / T(8)) * z[12] + z[23] + z[28];
z[11] = abb[52] * z[11];
z[12] = abb[33] * (T(1) / T(2));
z[23] = -abb[31] + z[12];
z[28] = z[23] + -z[27];
z[47] = abb[34] * (T(1) / T(2));
z[49] = z[30] + -z[47];
z[50] = -abb[32] + -z[28] + z[49];
z[50] = abb[3] * m1_set::bc<T>[0] * z[50];
z[17] = z[17] + -z[48];
z[48] = abb[8] * (T(1) / T(2));
z[51] = abb[41] * z[48];
z[7] = -z[7] + -z[17] + z[50] + z[51];
z[21] = abb[43] * z[21];
z[21] = z[21] + z[25];
z[25] = (T(1) / T(2)) * z[35];
z[35] = -abb[32] + z[30];
z[50] = z[25] + z[35];
z[50] = m1_set::bc<T>[0] * z[50];
z[45] = -z[45] + z[50];
z[45] = abb[1] * z[45];
z[50] = abb[13] * (T(1) / T(2));
z[51] = abb[6] * (T(1) / T(2));
z[52] = z[50] + -z[51];
z[53] = z[8] * z[52];
z[46] = -abb[41] + (T(1) / T(2)) * z[46];
z[46] = abb[10] * z[46];
z[7] = (T(1) / T(2)) * z[7] + (T(-1) / T(4)) * z[21] + z[45] + z[46] + z[53];
z[21] = abb[51] * (T(1) / T(4));
z[7] = z[7] * z[21];
z[19] = z[8] + z[19];
z[19] = abb[8] * z[19];
z[40] = z[22] * z[40];
z[4] = -z[4] + z[19] + -z[26] + z[40];
z[19] = -z[27] + z[30];
z[26] = z[1] + z[19];
z[26] = m1_set::bc<T>[0] * z[26];
z[22] = -z[22] + z[26];
z[22] = abb[2] * z[22];
z[4] = (T(1) / T(2)) * z[4] + z[22] + z[46];
z[22] = abb[54] * (T(1) / T(4));
z[4] = z[4] * z[22];
z[26] = abb[45] + abb[47];
z[40] = abb[46] + abb[48];
z[46] = z[26] + -z[40];
z[2] = z[2] * z[46];
z[53] = -abb[43] * z[46];
z[2] = z[2] + z[53];
z[54] = abb[24] * (T(1) / T(16));
z[55] = abb[26] * (T(1) / T(16));
z[56] = z[54] + z[55];
z[2] = z[2] * z[56];
z[56] = -abb[8] * z[8];
z[18] = abb[3] * z[18];
z[17] = -z[17] + z[18] + z[56];
z[17] = (T(1) / T(8)) * z[17] + (T(1) / T(4)) * z[45];
z[17] = abb[50] * z[17];
z[18] = z[37] * z[46];
z[18] = z[18] + z[53];
z[18] = abb[25] * z[18];
z[37] = abb[50] + abb[52];
z[45] = abb[2] * (T(1) / T(4));
z[45] = z[37] * z[45];
z[31] = z[31] * z[45];
z[8] = z[8] * z[46];
z[8] = z[8] + z[53];
z[56] = abb[23] * (T(1) / T(16));
z[8] = z[8] * z[56];
z[57] = abb[41] + -abb[43];
z[57] = abb[19] * z[57];
z[24] = abb[22] * z[24];
z[3] = -abb[43] + z[3];
z[3] = abb[20] * z[3];
z[3] = z[3] + z[24] + z[57];
z[24] = abb[55] * (T(1) / T(16));
z[3] = z[3] * z[24];
z[6] = -z[6] * z[46];
z[6] = z[6] + -z[53];
z[53] = abb[27] * (T(1) / T(16));
z[6] = z[6] * z[53];
z[9] = z[9] * z[37];
z[57] = abb[52] + abb[53];
z[58] = abb[4] * (T(1) / T(16));
z[57] = z[57] * z[58];
z[41] = -z[41] * z[57];
z[2] = abb[59] + z[2] + z[3] + z[4] + z[6] + z[7] + z[8] + (T(1) / T(8)) * z[9] + z[11] + z[14] + z[17] + (T(1) / T(16)) * z[18] + z[20] + z[31] + z[41];
z[3] = prod_pow(abb[31], 2);
z[4] = prod_pow(abb[34], 2);
z[6] = -z[3] + z[4];
z[7] = -abb[30] + z[15] + z[47];
z[7] = abb[30] * z[7];
z[8] = z[15] + z[30];
z[9] = abb[30] * (T(3) / T(2)) + -z[47];
z[11] = -z[8] + z[9];
z[11] = abb[35] * z[11];
z[14] = abb[37] + abb[39];
z[17] = prod_pow(m1_set::bc<T>[0], 2);
z[18] = (T(1) / T(2)) * z[17];
z[20] = -abb[36] + abb[38];
z[31] = abb[33] * z[39];
z[6] = -abb[58] + (T(1) / T(2)) * z[6] + z[7] + z[11] + -z[14] + -z[18] + z[20] + z[31];
z[6] = z[0] * z[6];
z[7] = abb[35] * z[10];
z[10] = abb[30] * z[1];
z[11] = abb[31] * abb[34];
z[31] = -abb[31] + z[1];
z[31] = abb[33] * z[31];
z[41] = abb[32] * z[36];
z[7] = z[7] + -z[10] + z[11] + z[31] + z[41];
z[58] = -abb[56] + abb[57] + z[20];
z[59] = -z[7] + -z[58];
z[59] = z[16] * z[59];
z[23] = abb[33] * z[23];
z[60] = (T(1) / T(2)) * z[3] + z[23];
z[61] = abb[39] + z[60];
z[62] = -abb[31] + z[27];
z[62] = abb[30] * z[62];
z[62] = -z[61] + z[62];
z[63] = -abb[31] + z[44];
z[63] = abb[32] * z[63];
z[64] = -abb[57] + z[63];
z[65] = z[20] + z[64];
z[66] = -abb[56] + z[62] + -z[65];
z[43] = z[43] * z[66];
z[66] = z[15] + -z[27];
z[66] = abb[30] * z[66];
z[67] = (T(1) / T(6)) * z[17];
z[61] = z[61] + -z[66] + z[67];
z[68] = -abb[30] + z[8];
z[68] = abb[35] * z[68];
z[68] = abb[37] + z[61] + z[68];
z[68] = abb[18] * z[68];
z[69] = z[43] + -z[68];
z[70] = -z[34] + z[44];
z[70] = abb[32] * z[70];
z[70] = abb[36] + abb[37] + z[70];
z[71] = z[27] * z[34];
z[72] = (T(1) / T(2)) * z[34] + z[35];
z[72] = abb[35] * z[72];
z[73] = z[70] + z[71] + z[72];
z[74] = abb[16] * z[73];
z[74] = (T(1) / T(2)) * z[74];
z[73] = -abb[0] * z[73];
z[75] = z[1] + z[35];
z[75] = abb[35] * z[75];
z[66] = -z[66] + z[75];
z[76] = z[3] + z[4];
z[77] = abb[58] + z[67];
z[31] = z[31] + (T(1) / T(2)) * z[76] + -z[77];
z[76] = abb[39] + z[31];
z[78] = z[66] + z[76];
z[79] = -abb[37] + z[41];
z[80] = abb[57] + z[79];
z[81] = abb[38] + z[78] + -z[80];
z[82] = -z[51] * z[81];
z[83] = abb[30] + -abb[33];
z[84] = abb[35] * z[83];
z[85] = -z[61] + -z[64] + z[84];
z[85] = abb[10] * z[85];
z[86] = z[27] + z[47];
z[87] = -abb[33] + z[86];
z[88] = abb[30] * z[87];
z[87] = abb[35] * z[87];
z[89] = -abb[34] + z[12];
z[90] = abb[33] * z[89];
z[88] = z[87] + -z[88] + -z[90];
z[91] = (T(1) / T(2)) * z[4];
z[77] = z[77] + -z[91];
z[92] = z[77] + z[88];
z[93] = abb[56] + z[92];
z[94] = abb[17] * z[93];
z[95] = (T(1) / T(2)) * z[94];
z[96] = -abb[31] + z[47];
z[96] = abb[30] * z[96];
z[72] = -z[72] + z[77] + z[96];
z[96] = abb[31] * (T(1) / T(2));
z[97] = -abb[34] + z[96];
z[97] = abb[31] * z[97];
z[98] = abb[39] + z[97];
z[99] = z[72] + -z[98];
z[80] = -abb[56] + z[80] + z[99];
z[100] = z[48] * z[80];
z[23] = abb[38] + z[11] + z[23] + z[77];
z[101] = abb[2] * z[23];
z[102] = -abb[31] + z[30];
z[102] = abb[35] * z[102];
z[64] = abb[37] + -z[64] + z[102];
z[33] = -z[33] * z[64];
z[6] = z[6] + z[33] + z[59] + -z[69] + z[73] + z[74] + z[82] + z[85] + -z[95] + z[100] + z[101];
z[6] = z[6] * z[38];
z[33] = -z[79] + z[98];
z[38] = abb[56] * (T(1) / T(2));
z[59] = abb[58] * (T(1) / T(2));
z[73] = z[38] + -z[59];
z[82] = (T(-1) / T(4)) * z[34] + -z[35];
z[82] = abb[35] * z[82];
z[98] = abb[30] * (T(1) / T(4));
z[100] = -abb[31] + abb[34] * (T(3) / T(4)) + -z[98];
z[100] = abb[30] * z[100];
z[82] = -z[33] + -z[67] + -z[73] + z[82] + z[90] + z[100];
z[82] = z[48] * z[82];
z[100] = (T(1) / T(12)) * z[17];
z[101] = z[59] + -z[100];
z[3] = -z[3] + -z[91];
z[102] = abb[31] + z[47];
z[103] = abb[33] * (T(3) / T(4));
z[104] = z[102] + -z[103];
z[104] = abb[33] * z[104];
z[105] = -abb[31] + abb[33] * (T(3) / T(2));
z[106] = -z[86] + z[105];
z[106] = abb[30] * z[106];
z[49] = abb[30] + -z[49] + -z[105];
z[49] = abb[35] * z[49];
z[3] = (T(1) / T(2)) * z[3] + -z[14] + z[49] + z[101] + z[104] + z[106];
z[3] = z[0] * z[3];
z[49] = -z[7] * z[16];
z[104] = abb[33] + abb[34] * (T(-3) / T(2)) + z[27];
z[104] = z[35] + (T(1) / T(2)) * z[104];
z[104] = abb[35] * z[104];
z[105] = z[90] + z[91];
z[9] = -abb[33] + z[9];
z[9] = abb[30] * z[9];
z[9] = z[9] + z[105];
z[9] = (T(1) / T(2)) * z[9] + -z[38] + z[70] + -z[100] + z[104];
z[38] = -z[5] * z[9];
z[70] = -abb[34] + -z[96] + z[103];
z[70] = abb[33] * z[70];
z[11] = z[11] + z[91];
z[19] = -z[1] * z[19];
z[96] = abb[38] * (T(1) / T(2));
z[11] = (T(1) / T(2)) * z[11] + (T(-1) / T(4)) * z[17] + -z[19] + z[70] + z[96];
z[70] = abb[2] * z[11];
z[103] = abb[35] * z[36];
z[103] = -z[41] + z[103];
z[103] = abb[12] * z[103];
z[85] = z[85] + z[103];
z[104] = abb[40] * (T(1) / T(8));
z[106] = -abb[19] + abb[22];
z[107] = z[104] * z[106];
z[94] = (T(1) / T(4)) * z[94] + -z[107];
z[61] = z[61] + z[75] + -z[79];
z[51] = -z[51] * z[61];
z[18] = z[18] + -z[105];
z[50] = z[18] * z[50];
z[62] = z[62] + -z[63];
z[62] = abb[36] + (T(1) / T(2)) * z[62] + -z[96];
z[62] = abb[9] * z[62];
z[3] = z[3] + z[38] + z[49] + z[50] + z[51] + z[62] + -z[69] + z[70] + z[82] + (T(1) / T(2)) * z[85] + -z[94];
z[3] = z[3] * z[32];
z[32] = z[91] + -z[97];
z[38] = z[27] + z[39];
z[27] = -z[27] * z[38];
z[36] = z[36] * z[44];
z[39] = -z[30] * z[35];
z[14] = (T(1) / T(2)) * z[14];
z[44] = abb[57] * (T(1) / T(2)) + -z[14];
z[27] = (T(-5) / T(12)) * z[17] + z[27] + (T(1) / T(2)) * z[32] + z[36] + z[39] + z[44] + z[90];
z[27] = abb[8] * z[27];
z[32] = abb[36] + z[60];
z[36] = (T(1) / T(2)) * z[102];
z[29] = -z[29] + -z[36];
z[29] = abb[30] * z[29];
z[39] = abb[35] * (T(1) / T(4));
z[36] = -abb[33] + abb[30] * (T(3) / T(4)) + z[36] + -z[39];
z[36] = abb[35] * z[36];
z[14] = -z[14] + z[29] + (T(-1) / T(2)) * z[32] + z[36] + -z[67] + z[96];
z[14] = abb[3] * z[14];
z[29] = -z[7] + z[58];
z[16] = z[16] * z[29];
z[9] = -abb[0] * z[9];
z[29] = abb[57] + -z[67];
z[29] = abb[14] * z[29];
z[29] = z[29] + -z[68];
z[9] = z[9] + z[14] + z[16] + z[27] + -z[29] + -z[43] + z[74];
z[9] = abb[52] * z[9];
z[7] = abb[15] * z[7];
z[7] = z[7] + z[29] + -z[103];
z[14] = -abb[3] * z[61];
z[16] = -abb[8] * z[18];
z[14] = -z[7] + z[14] + z[16];
z[14] = abb[50] * z[14];
z[9] = z[9] + z[14];
z[14] = z[26] * z[80];
z[16] = -z[41] + -z[72] + z[97];
z[16] = abb[46] * z[16];
z[27] = -z[41] + -z[99];
z[27] = abb[48] * z[27];
z[29] = abb[37] * z[40];
z[32] = abb[57] * z[40];
z[36] = z[29] + -z[32];
z[43] = abb[56] * z[40];
z[49] = abb[39] * abb[46];
z[14] = z[14] + z[16] + z[27] + z[36] + z[43] + z[49];
z[14] = abb[25] * z[14];
z[16] = abb[30] * z[38];
z[16] = z[16] + z[91] + z[97];
z[25] = -abb[32] + z[25] + z[39];
z[25] = abb[35] * z[25];
z[16] = (T(1) / T(2)) * z[16] + z[25] + -z[41] + -z[44] + -z[100];
z[16] = abb[1] * z[16];
z[25] = -abb[50] * z[16];
z[27] = z[37] * z[50];
z[9] = (T(1) / T(2)) * z[9] + (T(1) / T(4)) * z[14] + z[25] + z[27];
z[12] = z[12] * z[89];
z[14] = z[28] + z[47];
z[14] = abb[30] * z[14];
z[25] = (T(-1) / T(2)) * z[1] + -z[35];
z[25] = abb[35] * z[25];
z[4] = (T(-1) / T(4)) * z[4] + z[12] + z[14] + z[25] + -z[33] + z[101];
z[4] = abb[3] * z[4];
z[12] = abb[56] + z[67] + z[88] + -z[91];
z[5] = z[5] * z[12];
z[12] = z[30] * z[34];
z[12] = -abb[56] + abb[58] + -z[12] + z[71];
z[14] = z[12] * z[48];
z[4] = z[4] + -z[5] + -z[7] + z[14];
z[7] = abb[30] * z[83];
z[14] = (T(1) / T(3)) * z[17];
z[7] = z[7] + z[14] + -z[84];
z[7] = -abb[56] + (T(1) / T(2)) * z[7];
z[7] = abb[10] * z[7];
z[17] = z[18] * z[52];
z[4] = (T(1) / T(2)) * z[4] + -z[7] + -z[16] + z[17] + -z[94];
z[4] = z[4] * z[21];
z[8] = z[8] + -z[86];
z[8] = abb[35] * z[8];
z[15] = -z[15] + z[47];
z[15] = abb[30] * z[15];
z[16] = z[15] + z[76];
z[17] = z[8] + z[16];
z[20] = abb[37] + z[17] + z[20];
z[20] = -z[20] * z[26];
z[15] = z[15] + z[31];
z[8] = z[8] + z[15];
z[8] = abb[46] * z[8];
z[17] = abb[48] * z[17];
z[21] = abb[38] * z[40];
z[21] = z[21] + z[49];
z[25] = abb[36] * z[40];
z[25] = -z[21] + z[25];
z[8] = z[8] + z[17] + z[20] + -z[25] + z[29];
z[8] = z[8] * z[56];
z[17] = -z[39] + z[98];
z[17] = z[17] * z[34];
z[17] = z[17] + z[18] + -z[73];
z[17] = abb[8] * z[17];
z[1] = abb[35] * z[1];
z[1] = -z[1] + z[10] + z[77] + -z[90];
z[0] = -z[0] * z[1];
z[10] = -abb[13] * z[18];
z[0] = z[0] + -z[5] + z[10] + z[17] + -z[95];
z[5] = z[14] + z[19] + z[59] + -z[105];
z[5] = abb[2] * z[5];
z[0] = (T(1) / T(2)) * z[0] + z[5] + -z[7];
z[0] = z[0] * z[22];
z[5] = -z[26] * z[81];
z[7] = z[31] + -z[41] + z[66];
z[7] = abb[46] * z[7];
z[10] = -z[41] + z[78];
z[10] = abb[48] * z[10];
z[5] = z[5] + z[7] + z[10] + z[21] + z[36];
z[5] = z[5] * z[54];
z[7] = z[11] * z[45];
z[1] = abb[20] * z[1];
z[10] = abb[19] * z[12];
z[11] = abb[22] * z[93];
z[1] = z[1] + z[10] + z[11];
z[1] = z[1] * z[24];
z[10] = z[16] + -z[87];
z[11] = z[10] + z[65];
z[11] = -z[11] * z[26];
z[12] = z[15] + z[63] + -z[87];
z[12] = abb[46] * z[12];
z[10] = z[10] + z[63];
z[10] = abb[48] * z[10];
z[10] = z[10] + z[11] + z[12] + -z[25] + -z[32];
z[10] = z[10] * z[55];
z[11] = -z[13] * z[81];
z[12] = -z[42] * z[64];
z[11] = z[11] + z[12] + (T(1) / T(4)) * z[62];
z[11] = abb[52] * z[11];
z[12] = z[40] * z[92];
z[13] = -z[26] * z[93];
z[12] = z[12] + z[13] + z[43];
z[12] = z[12] * z[53];
z[13] = -z[23] * z[57];
z[14] = abb[28] * z[46];
z[15] = abb[20] + abb[21];
z[16] = z[15] + z[106];
z[16] = abb[53] * z[16];
z[15] = abb[52] * z[15];
z[14] = z[14] + z[15] + z[16];
z[15] = z[22] * z[106];
z[16] = abb[0] + abb[8] + abb[17];
z[16] = abb[29] + (T(-1) / T(4)) * z[16];
z[16] = abb[55] * z[16];
z[14] = (T(1) / T(4)) * z[14] + z[15] + z[16];
z[14] = z[14] * z[104];
z[0] = abb[44] + z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + (T(1) / T(4)) * z[9] + z[10] + z[11] + z[12] + z[13] + z[14];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_99_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.68100189604850741458276919433524342903178900403529910445223187792"),stof<T>("0.78536821792749070075978791437572304567722324573841271382427093501")}, std::complex<T>{stof<T>("0.40650510103928915767761138168204405470069365125319944442596661075"),stof<T>("0.35432335853178595855704624993251035367121324561773452233210691608")}, std::complex<T>{stof<T>("1.15043948057849416651173275012352639563720853081341291451433959248"),stof<T>("0.2690314614127623539218746127261972499728562629554907629540115299")}, std::complex<T>{stof<T>("0.4660789388603628522448265365742721799206307678991526679000972957"),stof<T>("0.4882811671943672999124186271568056550994911073800744529958061047")}, std::complex<T>{stof<T>("0.48810413056084514581620813306394554149779815996143888170939218584"),stof<T>("0.32611234922308966845409602241346304780841904853983561223823301383")}, std::complex<T>{stof<T>("0.20367351587692326052492846867765289984606122414154710521258928289"),stof<T>("-0.08529189711902360463517163720631310369835698266224375937809538618")}, std::complex<T>{stof<T>("-0.25186751860815181882323830401799984856501583090687365866823217018"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_99_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_99_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_99_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (-v[3] + v[5]) * (8 + v[3] + 3 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (-v[3] + v[5])) / tend;


		return (abb[49] + abb[50] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[1];
z[0] = -abb[49] + -abb[50] + -abb[52];
return abb[11] * abb[38] * (T(1) / T(8)) * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_99_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.47993850471359930014718455092534543292138343170674402108251270148"),stof<T>("0.10972068855864484282781305002504959913323014567398279971250554171")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W93(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[44] = SpDLog_f_4_99_W_16_Im(t, path, abb);
abb[59] = SpDLog_f_4_99_W_16_Re(t, path, abb);

                    
            return f_4_99_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_99_DLogXconstant_part(base_point<T>, kend);
	value += f_4_99_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_99_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_99_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_99_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_99_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_99_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_99_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
