/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_684.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_684_abbreviated (const std::array<T,38>& abb) {
T z[50];
z[0] = 2 * abb[31];
z[1] = abb[8] * z[0];
z[2] = abb[29] + abb[30];
z[3] = abb[8] * z[2];
z[1] = -z[1] + 2 * z[3];
z[4] = 2 * abb[30];
z[5] = -abb[31] + z[4];
z[5] = abb[9] * z[5];
z[6] = abb[28] + -abb[32];
z[7] = abb[26] + abb[27] + 2 * z[6];
z[8] = abb[13] * z[7];
z[5] = z[5] + z[8];
z[9] = abb[12] * z[7];
z[10] = z[5] + -z[9];
z[11] = 2 * abb[29];
z[12] = abb[31] + z[11];
z[12] = abb[3] * z[12];
z[13] = abb[29] + abb[31];
z[14] = abb[5] * z[13];
z[15] = 2 * z[14];
z[16] = -abb[30] + abb[31];
z[17] = abb[1] * z[16];
z[18] = 2 * z[2];
z[19] = -abb[31] + z[18];
z[20] = -abb[0] * z[19];
z[21] = abb[4] * z[16];
z[20] = -z[1] + -z[10] + z[12] + z[15] + -6 * z[17] + z[20] + -4 * z[21];
z[20] = prod_pow(abb[19], 2) * z[20];
z[21] = abb[31] + z[4];
z[21] = abb[9] * z[21];
z[21] = -z[8] + z[21];
z[9] = -z[9] + z[21];
z[22] = -abb[29] + abb[31];
z[22] = abb[6] * z[22];
z[23] = 2 * z[22];
z[24] = abb[30] + abb[31];
z[25] = abb[29] + z[24];
z[26] = abb[7] * z[25];
z[27] = 2 * z[26];
z[28] = abb[2] * z[24];
z[29] = abb[31] + -z[11];
z[29] = abb[3] * z[29];
z[18] = abb[31] + z[18];
z[18] = abb[0] * z[18];
z[18] = z[9] + z[18] + z[23] + z[27] + -6 * z[28] + z[29];
z[29] = prod_pow(abb[16], 2);
z[18] = z[18] * z[29];
z[30] = abb[8] * abb[31];
z[3] = -z[3] + z[30];
z[30] = -z[3] + z[22];
z[31] = abb[9] * z[0];
z[31] = 2 * z[8] + -z[31];
z[32] = 2 * abb[3];
z[33] = 4 * abb[29];
z[34] = -abb[31] + -z[33];
z[34] = z[32] * z[34];
z[34] = -13 * z[14] + -z[28] + 5 * z[30] + z[31] + z[34];
z[35] = z[11] + z[24];
z[35] = abb[10] * z[35];
z[36] = abb[0] * abb[31];
z[19] = abb[4] * z[19];
z[19] = 3 * z[17] + (T(-4) / T(3)) * z[19] + -z[26] + (T(1) / T(3)) * z[34] + (T(8) / T(3)) * z[35] + (T(-2) / T(3)) * z[36];
z[19] = prod_pow(m1_set::bc<T>[0], 2) * z[19];
z[34] = 2 * abb[4];
z[37] = z[24] * z[34];
z[38] = 4 * z[28];
z[9] = z[9] + -z[38];
z[39] = abb[3] * abb[31];
z[40] = z[36] + z[39];
z[37] = z[9] + -z[37] + z[40];
z[37] = abb[16] * z[37];
z[16] = z[16] * z[34];
z[34] = 4 * z[17];
z[10] = z[10] + z[34];
z[16] = z[10] + z[16] + -z[40];
z[16] = abb[19] * z[16];
z[16] = -z[16] + z[37];
z[37] = 2 * z[17];
z[40] = 2 * z[28] + z[37];
z[41] = -abb[26] + abb[27];
z[42] = abb[12] * z[41];
z[43] = abb[30] + z[0];
z[44] = abb[3] * z[43];
z[45] = -abb[30] + z[0];
z[46] = abb[0] * z[45];
z[44] = -z[40] + z[42] + z[44] + z[46];
z[44] = abb[18] * z[44];
z[44] = 2 * z[16] + z[44];
z[44] = abb[18] * z[44];
z[18] = z[18] + z[19] + z[20] + z[44];
z[19] = 3 * abb[30];
z[20] = -abb[29] + z[0];
z[44] = z[19] + -z[20];
z[44] = abb[4] * z[44];
z[46] = -abb[31] + z[2];
z[47] = abb[0] * z[46];
z[44] = z[44] + -z[47];
z[13] = z[13] * z[32];
z[7] = abb[11] * z[7];
z[32] = z[7] + 2 * z[35];
z[5] = z[5] + -z[13] + -3 * z[14] + 5 * z[17] + -z[26] + z[32] + -z[44];
z[48] = abb[33] * z[5];
z[49] = abb[30] + z[20];
z[49] = abb[4] * z[49];
z[25] = abb[0] * z[25];
z[49] = -z[25] + z[49];
z[9] = z[9] + z[15] + z[27] + -z[32] + z[39] + -z[49];
z[32] = abb[35] * z[9];
z[3] = z[3] + z[26];
z[26] = abb[3] * abb[29];
z[39] = abb[4] * z[46];
z[26] = z[3] + z[15] + -z[17] + z[26] + -z[35] + z[39];
z[35] = abb[37] * z[26];
z[21] = z[7] + z[21] + -3 * z[28] + -z[30] + -z[49];
z[30] = abb[34] * z[21];
z[7] = -z[7] + z[10] + -z[12] + -z[44];
z[10] = abb[36] * z[7];
z[10] = z[10] + -z[30] + -z[32] + -z[35] + z[48];
z[14] = z[14] + z[22];
z[22] = 3 * abb[26];
z[30] = -4 * z[6];
z[32] = abb[27] + z[22] + -z[30];
z[35] = abb[11] + abb[12];
z[32] = z[32] * z[35];
z[35] = 4 * abb[31];
z[39] = abb[9] * z[35];
z[39] = 4 * z[8] + -z[39];
z[0] = z[0] + 3 * z[2];
z[0] = abb[0] * z[0];
z[2] = -abb[30] + z[11] + -z[35];
z[2] = abb[4] * z[2];
z[44] = abb[30] + z[33];
z[44] = -abb[3] * z[44];
z[0] = 2 * z[0] + z[2] + -4 * z[14] + z[32] + -z[34] + -z[38] + -z[39] + z[44];
z[0] = abb[15] * z[0];
z[2] = z[17] + z[28];
z[2] = 8 * z[2] + z[39];
z[14] = z[33] + -z[35];
z[17] = -abb[30] + z[14];
z[17] = abb[3] * z[17];
z[32] = abb[26] + 3 * abb[27] + -z[30];
z[32] = abb[12] * z[32];
z[34] = abb[11] * z[41];
z[38] = 8 * abb[31];
z[39] = abb[30] + z[38];
z[39] = abb[4] * z[39];
z[17] = z[2] + z[17] + -4 * z[25] + -z[32] + -z[34] + z[39];
z[17] = abb[18] * z[17];
z[39] = 4 * z[16];
z[2] = z[2] + z[32];
z[32] = 5 * abb[27];
z[22] = -8 * z[6] + -z[22] + -z[32];
z[22] = abb[11] * z[22];
z[19] = z[19] + z[33] + z[38];
z[44] = -abb[4] * z[19];
z[14] = -abb[30] + -z[14];
z[14] = abb[3] * z[14];
z[14] = -z[2] + z[14] + z[22] + 4 * z[36] + z[44];
z[14] = abb[17] * z[14];
z[0] = z[0] + z[14] + z[17] + -z[39];
z[0] = abb[15] * z[0];
z[14] = abb[27] + z[6];
z[14] = abb[11] * z[14];
z[17] = abb[9] * abb[31];
z[3] = -z[3] + -z[8] + z[12] + z[14] + z[17] + -z[28];
z[8] = z[4] + -z[20];
z[8] = abb[4] * z[8];
z[4] = -3 * abb[29] + -z[4];
z[4] = abb[0] * z[4];
z[3] = 2 * z[3] + z[4] + z[8] + -z[37];
z[3] = abb[17] * z[3];
z[4] = abb[30] + -z[33] + -z[35];
z[4] = abb[3] * z[4];
z[8] = -abb[30] + z[38];
z[8] = abb[4] * z[8];
z[2] = z[2] + z[4] + z[8] + z[34] + 4 * z[47];
z[2] = abb[18] * z[2];
z[2] = z[2] + 2 * z[3] + -z[39];
z[2] = abb[17] * z[2];
z[3] = abb[12] * z[19];
z[4] = -abb[26] + -z[6];
z[4] = abb[0] * z[4];
z[6] = abb[14] * z[41];
z[4] = z[4] + -z[6];
z[6] = -abb[26] + -z[30] + z[32];
z[6] = abb[3] * z[6];
z[8] = 5 * abb[11] + 8 * abb[13];
z[8] = abb[30] * z[8];
z[3] = z[3] + 4 * z[4] + z[6] + z[8];
z[3] = abb[20] * z[3];
z[4] = z[24] * z[29];
z[6] = abb[20] * z[41];
z[4] = -8 * z[4] + 3 * z[6];
z[4] = abb[4] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + 4 * z[10] + 2 * z[18];
z[0] = 4 * z[0];
z[2] = z[31] + z[40];
z[3] = abb[30] + z[11];
z[4] = abb[0] * z[3];
z[6] = abb[4] * z[45];
z[1] = -z[1] + z[2] + z[4] + z[6] + -z[13] + z[27] + -z[34];
z[1] = abb[17] * z[1];
z[2] = z[2] + z[42];
z[3] = abb[3] * z[3];
z[4] = abb[4] * z[43];
z[3] = z[2] + z[3] + z[4] + z[15] + z[23] + -2 * z[25] + z[34];
z[3] = abb[15] * z[3];
z[4] = abb[0] + -abb[3];
z[4] = abb[30] * z[4];
z[6] = -abb[4] * z[35];
z[2] = -z[2] + z[4] + z[6];
z[2] = abb[18] * z[2];
z[1] = z[1] + z[2] + z[3] + z[16];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[21] * z[5];
z[3] = -abb[25] * z[26];
z[4] = -abb[23] * z[9];
z[5] = -abb[22] * z[21];
z[6] = abb[24] * z[7];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
z[1] = 16 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_684_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-5.705317672519195633917296582123317599139446188971369529034283413"),stof<T>("-125.991453537739045707660668897222826401320496131828772328508469875")}, std::complex<T>{stof<T>("-22.092882292884777496091312788923492457568468886615937111680321457"),stof<T>("125.991453537739045707660668897222826401320496131828772328508469875")}, stof<T>("-27.798199965403973130008609371046810056707915075587306640714604869"), std::complex<T>{stof<T>("423.83192844570989376038795905092657361938670148555978987012769625"),stof<T>("-188.56745743608156690960237938370586139384591512463871505505349955")}, std::complex<T>{stof<T>("221.47021266764823351389311525483808379997310665654329269975418311"),stof<T>("-268.85443304831246246612245655468847877469264136452050468818546893")}, std::complex<T>{stof<T>("9.530254697506458330366177977756489869201017321208368866048547077"),stof<T>("63.980943890468741000571269798176635800199154956095095053712380312")}, stof<T>("27.798199965403973130008609371046810056707915075587306640714604869")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real()), rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_684_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_684_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-2.08928139868399970115768256193022330714661590170312565769830851"),stof<T>("227.55140511399561263144370373508185988214480508860657294840459586")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,38> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W66(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), rlog(kend.W[194].real()/k.W[194].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_684_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_684_DLogXconstant_part(base_point<T>, kend);
	value += f_4_684_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_684_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_684_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_684_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_684_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_684_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_684_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
