/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_843.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_843_abbreviated (const std::array<T,96>& abb) {
T z[153];
z[0] = abb[73] + abb[75];
z[1] = 3 * z[0];
z[2] = abb[74] + abb[76];
z[3] = 3 * z[2];
z[4] = 2 * abb[78];
z[5] = abb[77] + abb[79];
z[6] = z[1] + z[3] + z[4] + -z[5];
z[6] = abb[6] * z[6];
z[7] = abb[78] + z[0];
z[8] = 2 * abb[74] + 2 * abb[76];
z[9] = -abb[79] + z[8];
z[10] = z[7] + z[9];
z[11] = abb[17] * z[10];
z[12] = abb[69] + abb[72];
z[13] = -abb[70] + z[12];
z[14] = -abb[68] + z[13];
z[15] = abb[32] * z[14];
z[15] = z[11] + z[15];
z[16] = abb[67] + abb[71];
z[17] = abb[68] + abb[70] + -z[16];
z[18] = abb[28] * z[17];
z[17] = abb[30] * z[17];
z[19] = z[15] + z[17] + z[18];
z[20] = -z[12] + z[16];
z[21] = abb[33] * z[20];
z[22] = -abb[79] + z[2];
z[23] = -abb[77] + z[0];
z[24] = z[22] + -z[23];
z[25] = abb[16] * z[24];
z[21] = z[21] + -z[25];
z[26] = z[0] + z[2];
z[27] = -z[5] + z[26];
z[28] = abb[9] * z[27];
z[29] = 2 * z[28];
z[30] = abb[78] + abb[79];
z[31] = z[0] + z[30];
z[32] = abb[10] * z[31];
z[33] = 2 * z[32];
z[34] = z[29] + -z[33];
z[35] = 2 * abb[77];
z[1] = -z[1] + -z[30] + z[35];
z[1] = abb[5] * z[1];
z[36] = -abb[77] + z[26];
z[37] = abb[11] * z[36];
z[38] = 2 * z[37];
z[39] = abb[12] * z[7];
z[40] = 2 * z[39];
z[41] = z[38] + -z[40];
z[42] = abb[29] * z[13];
z[43] = abb[29] + abb[31];
z[44] = abb[68] * z[43];
z[45] = z[42] + -z[44];
z[46] = abb[77] + abb[78];
z[47] = z[2] + z[46];
z[48] = abb[8] * z[47];
z[49] = 4 * z[48];
z[50] = 2 * abb[79];
z[51] = z[46] + z[50];
z[52] = -abb[76] + z[51];
z[52] = abb[4] * z[52];
z[53] = -z[2] + z[46];
z[54] = z[50] + z[53];
z[54] = abb[3] * z[54];
z[55] = abb[4] * abb[74];
z[13] = abb[31] * z[13];
z[1] = z[1] + z[6] + -z[13] + z[19] + -z[21] + -z[34] + z[41] + -z[45] + -z[49] + z[52] + z[54] + -z[55];
z[1] = abb[44] * z[1];
z[6] = 2 * abb[66];
z[52] = -abb[69] + z[6];
z[54] = -abb[70] + 2 * z[52];
z[54] = -z[43] * z[54];
z[56] = 2 * z[0];
z[57] = abb[78] + z[56];
z[58] = -abb[77] + z[57];
z[59] = z[2] + z[58];
z[60] = abb[6] * z[59];
z[61] = abb[76] + z[58];
z[62] = abb[5] * z[61];
z[60] = z[60] + -z[62];
z[62] = 4 * abb[1];
z[63] = z[2] * z[62];
z[64] = abb[3] * z[47];
z[65] = abb[67] * z[43];
z[66] = abb[71] * z[43];
z[67] = abb[76] + z[46];
z[68] = abb[4] * z[67];
z[69] = abb[4] + -abb[5];
z[70] = abb[74] * z[69];
z[41] = z[41] + -z[44] + z[54] + z[60] + -z[63] + z[64] + -z[65] + z[66] + z[68] + z[70];
z[41] = abb[40] * z[41];
z[1] = z[1] + z[41];
z[41] = abb[14] * z[27];
z[54] = abb[27] * z[20];
z[41] = z[41] + z[54];
z[64] = abb[5] * abb[74];
z[68] = -z[41] + z[64];
z[70] = abb[3] * z[27];
z[71] = abb[28] * z[20];
z[72] = z[70] + z[71];
z[73] = abb[4] * z[31];
z[13] = -z[13] + z[73];
z[45] = -z[13] + z[45];
z[74] = abb[13] * z[31];
z[75] = z[17] + z[74];
z[76] = abb[76] + -z[50] + -z[58];
z[76] = abb[5] * z[76];
z[77] = z[3] + -z[50];
z[58] = z[58] + z[77];
z[58] = abb[6] * z[58];
z[77] = -z[46] + -z[77];
z[77] = abb[7] * z[77];
z[58] = z[34] + -z[45] + z[58] + z[68] + -z[72] + z[75] + z[76] + z[77];
z[58] = abb[39] * z[58];
z[76] = 3 * abb[77];
z[77] = 3 * abb[79];
z[78] = z[4] + -z[26] + z[76] + z[77];
z[78] = abb[3] * z[78];
z[79] = 2 * abb[30];
z[80] = z[20] * z[79];
z[80] = 2 * z[71] + z[80];
z[81] = z[65] + z[80];
z[82] = 4 * abb[66];
z[83] = -abb[69] + z[82];
z[84] = 2 * abb[70];
z[85] = abb[72] + z[83] + -z[84];
z[86] = abb[31] * z[85];
z[87] = abb[78] + z[5];
z[88] = 2 * z[87];
z[89] = abb[4] * z[88];
z[86] = -z[66] + z[86] + -z[89];
z[85] = abb[29] * z[85];
z[89] = abb[6] * z[27];
z[90] = -abb[77] + z[30] + z[56];
z[91] = abb[5] * z[90];
z[78] = z[78] + -z[81] + -z[85] + -z[86] + 3 * z[89] + -2 * z[91];
z[92] = 4 * z[37];
z[93] = 4 * z[39];
z[94] = z[92] + -z[93];
z[63] = -z[63] + z[94];
z[95] = abb[0] * z[88];
z[96] = z[41] + z[95];
z[32] = 4 * z[32];
z[97] = z[21] + -z[32] + -z[63] + -z[78] + z[96];
z[97] = abb[42] * z[97];
z[98] = 3 * abb[78];
z[77] = z[0] + z[35] + z[77] + z[98];
z[77] = abb[4] * z[77];
z[99] = z[44] + z[74];
z[100] = z[14] * z[79];
z[101] = z[99] + -z[100];
z[57] = -z[5] + z[8] + z[57];
z[102] = abb[6] * z[57];
z[103] = abb[70] + -abb[72];
z[82] = abb[69] + z[82] + -3 * z[103];
z[82] = z[43] * z[82];
z[104] = abb[28] * z[14];
z[105] = z[15] + -z[104];
z[106] = abb[3] * z[88];
z[106] = z[105] + z[106];
z[107] = abb[5] * z[31];
z[108] = 2 * z[66];
z[77] = z[77] + -z[82] + z[101] + 2 * z[102] + z[106] + -3 * z[107] + z[108];
z[82] = z[47] * z[62];
z[32] = z[32] + -z[77] + z[82] + -z[95];
z[32] = abb[38] * z[32];
z[109] = z[21] + -z[95];
z[28] = 4 * z[28];
z[78] = -z[28] + z[41] + -z[78] + z[82] + z[109];
z[78] = abb[43] * z[78];
z[28] = z[28] + -z[63] + -z[77] + z[95];
z[28] = abb[41] * z[28];
z[63] = z[6] + -z[103];
z[77] = abb[31] * z[63];
z[82] = abb[4] * z[87];
z[77] = z[77] + -z[82];
z[110] = -z[66] + z[77];
z[111] = 2 * z[48];
z[112] = abb[29] * z[63];
z[113] = abb[3] * z[87];
z[114] = z[112] + -z[113];
z[115] = 2 * abb[1];
z[116] = z[47] * z[115];
z[117] = abb[5] * z[87];
z[118] = abb[6] * z[87];
z[119] = -z[110] + z[111] + -z[114] + -z[116] + -z[117] + -z[118];
z[119] = abb[37] * z[119];
z[120] = 2 * abb[28];
z[121] = abb[33] + z[120];
z[79] = abb[32] + -z[79] + -z[121];
z[122] = -abb[58] + abb[59];
z[79] = z[79] * z[122];
z[122] = abb[16] + abb[17] + -abb[18];
z[123] = z[115] + -z[122];
z[123] = abb[90] * z[123];
z[124] = 2 * z[43];
z[125] = -abb[59] * z[124];
z[126] = abb[58] + abb[59];
z[126] = abb[34] * z[126];
z[127] = abb[32] + abb[34];
z[128] = abb[33] + z[127];
z[129] = -abb[56] * z[128];
z[79] = z[79] + z[123] + z[125] + z[126] + z[129];
z[79] = m1_set::bc<T>[0] * z[79];
z[79] = z[79] + z[119];
z[1] = 2 * z[1] + z[28] + z[32] + 2 * z[58] + z[78] + 4 * z[79] + z[97];
z[1] = m1_set::bc<T>[0] * z[1];
z[28] = z[4] + z[5] + z[26];
z[32] = abb[19] * z[28];
z[58] = -z[0] + z[5];
z[78] = -abb[76] + z[58];
z[79] = -abb[74] + z[78];
z[79] = abb[22] * z[79];
z[97] = abb[23] * z[27];
z[119] = abb[35] * z[20];
z[123] = abb[26] * z[24];
z[32] = z[32] + -z[79] + z[97] + z[119] + -z[123];
z[79] = -abb[55] * z[32];
z[51] = -z[3] + -z[51];
z[51] = abb[44] * z[51];
z[58] = z[2] + z[58];
z[58] = abb[42] * z[58];
z[97] = abb[38] * z[10];
z[119] = abb[43] * z[24];
z[123] = z[8] + z[31];
z[123] = abb[41] * z[123];
z[51] = z[51] + z[58] + z[97] + z[119] + z[123];
z[51] = m1_set::bc<T>[0] * z[51];
z[58] = -abb[52] * z[31];
z[97] = -abb[51] * z[24];
z[119] = -abb[50] * z[10];
z[51] = z[51] + z[58] + z[97] + z[119];
z[58] = 2 * abb[7];
z[51] = z[51] * z[58];
z[97] = z[21] + -z[33];
z[119] = abb[6] * z[28];
z[78] = abb[4] * z[78];
z[78] = -z[55] + z[78] + -z[119];
z[20] = abb[30] * z[20];
z[20] = z[20] + z[72];
z[72] = abb[7] * z[27];
z[123] = z[46] * z[115];
z[72] = z[20] + -z[40] + -z[72] + -z[78] + z[97] + z[123];
z[72] = 2 * z[72];
z[125] = -abb[53] * z[72];
z[126] = z[7] + z[8];
z[126] = abb[25] * z[126];
z[129] = -z[0] + z[35];
z[130] = abb[78] + z[129];
z[130] = abb[20] * z[130];
z[131] = abb[21] + abb[24];
z[131] = z[31] * z[131];
z[132] = abb[36] * z[14];
z[133] = abb[20] + abb[25];
z[133] = abb[79] * z[133];
z[126] = z[126] + -z[130] + z[131] + z[132] + -z[133];
z[130] = abb[54] * z[126];
z[131] = -abb[76] + abb[79] + z[23];
z[131] = abb[5] * z[131];
z[20] = z[20] + -z[29] + -z[68] + -z[89] + z[131];
z[20] = 2 * z[20];
z[68] = -abb[51] * z[20];
z[132] = -z[74] + z[107];
z[14] = abb[30] * z[14];
z[14] = z[14] + z[45];
z[133] = abb[6] * z[10];
z[134] = -z[14] + -z[33] + -z[132] + z[133];
z[134] = 2 * z[134];
z[135] = abb[50] * z[134];
z[136] = abb[3] * z[31];
z[129] = z[30] + z[129];
z[137] = abb[5] * z[129];
z[136] = -z[123] + z[136] + -z[137];
z[138] = z[29] + -z[105];
z[139] = z[38] + -z[136] + z[138];
z[45] = -z[45] + -z[139];
z[45] = 2 * z[45] + -z[100];
z[45] = abb[52] * z[45];
z[140] = abb[64] * z[57];
z[141] = abb[61] + abb[62];
z[141] = z[87] * z[141];
z[142] = -abb[63] * z[90];
z[143] = prod_pow(m1_set::bc<T>[0], 2);
z[128] = abb[57] * z[128] * z[143];
z[144] = abb[72] + z[6];
z[145] = abb[70] + abb[71] + -z[144];
z[145] = abb[95] * z[145];
z[1] = z[1] + z[45] + z[51] + z[68] + z[79] + z[125] + -4 * z[128] + z[130] + z[135] + z[140] + z[141] + z[142] + z[145];
z[1] = 2 * z[1];
z[45] = abb[7] * z[47];
z[51] = z[41] + z[45] + z[71];
z[30] = z[30] + -z[56] + z[76];
z[30] = abb[5] * z[30];
z[8] = z[8] + z[56];
z[5] = z[5] + z[8] + z[98];
z[5] = abb[6] * z[5];
z[68] = abb[2] * abb[74];
z[67] = abb[2] * z[67];
z[68] = z[67] + z[68];
z[15] = -z[15] + z[68];
z[71] = abb[69] + 2 * abb[72] + z[6];
z[76] = -abb[70] + z[71];
z[76] = -z[43] * z[76];
z[79] = -z[82] + z[116];
z[75] = -z[5] + z[15] + z[21] + -z[30] + 6 * z[48] + z[51] + z[65] + -z[75] + z[76] + -z[79] + z[108] + z[113];
z[75] = abb[37] * z[75];
z[76] = 2 * abb[15];
z[98] = z[23] * z[76];
z[108] = abb[29] + -abb[31];
z[125] = abb[68] * z[108];
z[125] = z[74] + -z[95] + -z[98] + z[125];
z[128] = z[23] * z[62];
z[130] = z[42] + z[137];
z[83] = z[83] + -z[103];
z[135] = abb[31] * z[83];
z[129] = -abb[4] * z[129];
z[137] = abb[31] * abb[71];
z[140] = abb[6] * z[88];
z[129] = -z[49] + z[105] + -z[125] + -z[128] + z[129] + z[130] + z[135] + -2 * z[137] + z[140];
z[129] = abb[38] * z[129];
z[135] = abb[15] * z[47];
z[140] = abb[31] * abb[68];
z[141] = -z[135] + z[140];
z[142] = abb[31] * abb[67];
z[145] = z[137] + z[142];
z[146] = abb[31] * abb[70];
z[146] = z[141] + -z[145] + z[146];
z[147] = z[49] + z[109];
z[148] = abb[78] + z[26];
z[149] = abb[6] * z[148];
z[150] = abb[5] * z[23];
z[151] = -z[149] + z[150];
z[19] = z[19] + -z[70] + z[73] + z[146] + -z[147] + -2 * z[151];
z[73] = -z[47] * z[58];
z[19] = 2 * z[19] + z[73];
z[19] = abb[44] * z[19];
z[73] = z[76] * z[148];
z[76] = abb[67] * z[108];
z[76] = z[41] + -z[73] + z[76];
z[152] = abb[29] * z[12];
z[70] = z[70] + z[152];
z[86] = z[70] + -z[76] + z[86] + -z[89] + z[109] + -z[128];
z[86] = abb[42] * z[86];
z[109] = abb[31] * z[12];
z[119] = -z[109] + -z[119] + z[147];
z[28] = -abb[3] * z[28];
z[62] = z[62] * z[148];
z[88] = abb[5] * z[88];
z[28] = z[28] + z[62] + -z[66] + z[76] + z[85] + z[88] + -z[119];
z[28] = abb[43] * z[28];
z[76] = abb[29] * z[83];
z[83] = abb[29] * abb[71];
z[62] = -z[13] + z[62] + z[76] + -2 * z[83] + -z[106] + z[107] + z[125];
z[62] = abb[41] * z[62];
z[19] = z[19] + z[28] + z[62] + z[75] + z[86] + z[129];
z[19] = abb[37] * z[19];
z[28] = abb[3] * z[57];
z[62] = z[59] * z[115];
z[73] = -z[62] + z[73];
z[28] = z[28] + z[73];
z[75] = z[29] + z[33];
z[76] = z[75] + z[95];
z[6] = 2 * abb[69] + -z[6];
z[85] = abb[70] + abb[72] + z[6];
z[86] = abb[31] * z[85];
z[86] = -z[86] + 2 * z[142];
z[21] = 2 * z[21] + z[28] + -z[30] + z[49] + z[66] + -z[76] + z[80] + -z[82] + z[86] + -z[102] + -z[112];
z[21] = abb[44] * z[21];
z[26] = -abb[78] + z[26] + -z[35] + -z[50];
z[30] = -abb[3] + abb[6];
z[26] = z[26] * z[30];
z[35] = -abb[70] + z[52];
z[82] = abb[31] * z[35];
z[35] = abb[29] * z[35];
z[26] = z[26] + z[29] + -z[35] + -z[41] + -z[79] + -z[81] + -z[82] + -z[91] + -z[97];
z[26] = abb[42] * z[26];
z[29] = z[77] + -z[102];
z[77] = z[91] + z[114];
z[79] = z[29] + z[77];
z[34] = z[34] + z[95];
z[81] = -z[66] + z[116];
z[88] = z[34] + -z[79] + -z[81];
z[88] = abb[38] * z[88];
z[21] = z[21] + z[26] + z[88];
z[21] = abb[43] * z[21];
z[18] = z[17] + z[18];
z[26] = abb[3] * z[148];
z[88] = abb[4] * z[23];
z[97] = abb[31] * z[52];
z[52] = abb[29] * z[52];
z[44] = z[18] + -z[26] + z[44] + z[52] + z[68] + z[81] + z[88] + z[97] + -z[111] + -z[151];
z[88] = abb[49] * z[44];
z[18] = z[18] + z[67];
z[59] = -abb[3] * z[59];
z[61] = abb[4] * z[61];
z[67] = abb[2] + abb[4];
z[106] = abb[74] * z[67];
z[59] = z[18] + z[59] + z[61] + z[106] + z[123] + z[146];
z[59] = abb[48] * z[59];
z[61] = abb[29] * abb[68];
z[106] = z[61] + z[135];
z[107] = abb[29] * abb[67];
z[112] = z[83] + z[107];
z[114] = abb[29] * abb[70];
z[114] = z[106] + -z[112] + z[114];
z[116] = abb[2] + -abb[5];
z[116] = abb[74] * z[116];
z[60] = z[18] + z[60] + -z[111] + z[114] + z[116];
z[60] = abb[47] * z[60];
z[13] = z[13] + -z[70] + z[112] + z[141];
z[68] = z[13] + z[68] + -z[74] + z[96] + -z[104];
z[68] = abb[46] * z[68];
z[70] = z[106] + z[145];
z[15] = z[15] + z[70] + z[119] + -z[130];
z[15] = abb[45] * z[15];
z[14] = z[14] + z[139];
z[14] = abb[82] * z[14];
z[14] = z[14] + z[15] + z[59] + z[60] + z[68] + -z[88];
z[15] = abb[38] * z[148];
z[59] = abb[42] + -abb[44];
z[60] = abb[79] + z[2];
z[68] = -z[59] * z[60];
z[68] = -z[15] + z[68];
z[68] = abb[41] * z[68];
z[60] = abb[42] * z[60];
z[15] = z[15] + z[60];
z[15] = abb[44] * z[15];
z[60] = -abb[47] + abb[49];
z[47] = z[47] * z[60];
z[60] = abb[82] * z[31];
z[24] = abb[81] * z[24];
z[59] = z[23] * z[59];
z[74] = -abb[38] * z[22];
z[59] = z[59] + z[74];
z[59] = abb[43] * z[59];
z[3] = z[3] + (T(2) / T(3)) * z[46];
z[3] = z[3] * z[143];
z[74] = -abb[48] * z[53];
z[10] = abb[80] * z[10];
z[88] = prod_pow(abb[44], 2);
z[96] = -abb[79] * z[88];
z[3] = z[3] + z[10] + z[15] + z[24] + z[47] + z[59] + z[60] + z[68] + z[74] + z[96];
z[3] = z[3] * z[58];
z[10] = -z[44] + z[45];
z[10] = abb[37] * z[10];
z[15] = abb[76] + z[7];
z[24] = abb[4] * z[15];
z[24] = z[24] + -z[82];
z[44] = abb[15] * z[148];
z[7] = -z[7] * z[115];
z[7] = z[7] + -z[24] + z[40] + z[44] + -z[55] + z[142] + -z[149];
z[7] = abb[42] * z[7];
z[40] = abb[3] + -abb[15];
z[40] = z[23] * z[40];
z[36] = z[36] * z[115];
z[36] = z[36] + -z[38] + z[40] + z[52] + z[61] + -z[83] + z[150];
z[36] = abb[41] * z[36];
z[15] = abb[5] * z[15];
z[15] = z[15] + z[35];
z[35] = -abb[15] + z[115];
z[38] = z[35] * z[148];
z[26] = z[15] + -z[26] + z[38] + z[64] + z[107];
z[26] = abb[43] * z[26];
z[37] = -z[37] + z[39];
z[38] = abb[1] * z[46];
z[38] = -z[37] + z[38] + -z[48] + -z[151];
z[39] = -z[2] * z[58];
z[38] = 2 * z[38] + z[39];
z[38] = abb[44] * z[38];
z[35] = abb[4] + -abb[6] + -z[35];
z[35] = z[23] * z[35];
z[35] = z[35] + z[97] + -z[137] + z[140];
z[35] = abb[38] * z[35];
z[7] = z[7] + z[10] + z[26] + z[35] + z[36] + z[38];
z[10] = z[23] * z[30];
z[26] = -z[2] * z[115];
z[30] = -abb[7] * z[53];
z[35] = -abb[5] + z[67];
z[35] = abb[74] * z[35];
z[10] = z[10] + -z[15] + z[18] + z[24] + z[26] + z[30] + z[35] + -z[65];
z[10] = abb[40] * z[10];
z[7] = 2 * z[7] + z[10];
z[7] = abb[40] * z[7];
z[10] = -z[73] + z[118];
z[15] = z[75] + -z[95];
z[18] = abb[29] * z[85];
z[18] = -z[18] + 2 * z[107];
z[24] = 2 * z[27];
z[24] = abb[14] * z[24];
z[24] = z[10] + -z[15] + z[18] + z[24] + 2 * z[54] + z[80] + z[91] + -z[110] + -z[113];
z[24] = abb[42] * z[24];
z[9] = z[9] + z[46];
z[26] = -abb[5] * z[9];
z[18] = -z[18] + z[26] + z[28] + z[29] + -z[34] + -z[66];
z[18] = abb[43] * z[18];
z[17] = z[17] + -z[51] + z[89] + -z[95] + z[114] + -z[132];
z[17] = abb[37] * z[17];
z[26] = -z[22] * z[58];
z[13] = z[13] + z[26] + z[34] + z[64] + -z[131] + z[133];
z[13] = abb[39] * z[13];
z[26] = abb[71] * z[108];
z[26] = z[26] + -z[62] + z[98];
z[27] = abb[4] * z[90];
z[6] = z[6] + -z[103];
z[28] = abb[31] * z[6];
z[27] = -z[26] + -z[27] + z[28] + z[77] + -2 * z[140];
z[9] = -abb[6] * z[9];
z[9] = z[9] + z[27] + -z[34];
z[9] = abb[38] * z[9];
z[6] = abb[29] * z[6];
z[6] = z[6] + z[26] + z[29] + -2 * z[61] + z[117];
z[26] = 2 * z[31];
z[26] = -abb[13] * z[26];
z[26] = z[6] + z[26] + z[76] + z[100] + z[113];
z[26] = abb[41] * z[26];
z[28] = abb[38] + abb[43];
z[22] = z[22] * z[28];
z[28] = abb[41] * z[148];
z[23] = -abb[42] * z[23];
z[22] = z[22] + z[23] + z[28];
z[22] = z[22] * z[58];
z[9] = z[9] + z[13] + 2 * z[17] + z[18] + z[22] + z[24] + z[26];
z[9] = abb[39] * z[9];
z[13] = 20 * z[0];
z[17] = 13 * abb[79];
z[18] = 13 * abb[77] + z[17];
z[22] = -7 * abb[78] + -20 * z[2] + -z[13] + z[18];
z[22] = abb[6] * z[22];
z[23] = 13 * abb[70] + -11 * z[144];
z[23] = -z[23] * z[43];
z[17] = 13 * abb[78] + z[17];
z[13] = -7 * abb[77] + z[13] + z[17];
z[13] = abb[5] * z[13];
z[17] = -11 * abb[77] + -z[17] + -z[56];
z[17] = abb[4] * z[17];
z[8] = -11 * abb[78] + z[8] + -z[18];
z[8] = abb[3] * z[8];
z[8] = z[8] + z[13] + z[17] + z[22] + z[23];
z[11] = z[11] + z[25] + -z[41] + -z[65] + z[99];
z[13] = abb[86] + abb[87];
z[17] = (T(-1) / T(3)) * z[12] + 2 * z[13];
z[18] = 2 * abb[88];
z[22] = abb[68] * (T(1) / T(3));
z[23] = abb[70] * (T(1) / T(3)) + z[18] + z[22];
z[24] = z[17] + z[23];
z[24] = abb[32] * z[24];
z[25] = (T(1) / T(3)) * z[16];
z[17] = z[17] + -z[18] + z[25];
z[17] = abb[33] * z[17];
z[17] = z[17] + z[24];
z[18] = z[121] + z[124] + -z[127];
z[18] = abb[89] * z[18];
z[23] = 2 * abb[89] + -z[23] + z[25];
z[23] = abb[30] * z[23];
z[13] = -abb[88] + z[13];
z[13] = abb[34] * z[13];
z[13] = z[13] + z[18] + z[23];
z[12] = -abb[70] + -z[12];
z[12] = -4 * abb[88] + (T(1) / T(3)) * z[12] + (T(2) / T(3)) * z[16] + -z[22];
z[12] = z[12] * z[120];
z[16] = 4 * abb[60];
z[18] = -z[16] * z[122];
z[2] = 11 * z[2] + 4 * z[46];
z[2] = (T(1) / T(3)) * z[2] + z[16];
z[2] = z[2] * z[115];
z[2] = z[2] + (T(1) / T(3)) * z[8] + (T(-2) / T(3)) * z[11] + z[12] + 4 * z[13] + 2 * z[17] + z[18] + (T(14) / T(3)) * z[37] + -3 * z[66];
z[2] = z[2] * z[143];
z[8] = z[34] + -z[66];
z[11] = abb[4] * z[57];
z[10] = z[8] + z[10] + z[11] + z[77] + -z[86] + -z[92];
z[10] = abb[42] * z[10];
z[5] = -z[5] + z[15] + z[27] + z[49] + z[100] + -2 * z[105];
z[5] = abb[38] * z[5];
z[5] = z[5] + z[10];
z[5] = abb[44] * z[5];
z[0] = abb[77] + z[0] + z[4] + z[50];
z[0] = z[0] * z[69];
z[4] = z[71] + -z[84];
z[4] = -z[4] * z[43];
z[0] = z[0] + z[4] + -z[33] + -z[81] + z[101] + z[102] + z[113] + -z[138];
z[0] = abb[38] * z[0];
z[4] = -abb[3] * z[90];
z[4] = z[4] + z[6] + z[34] + z[93];
z[4] = abb[44] * z[4];
z[6] = z[53] * z[115];
z[6] = z[6] + -z[8] + -z[79] + z[94];
z[6] = abb[42] * z[6];
z[0] = z[0] + z[4] + z[6];
z[0] = abb[41] * z[0];
z[4] = -z[34] + -z[42] + z[70] + z[78] + -z[109] + z[111] + z[136];
z[4] = z[4] * z[88];
z[6] = abb[85] * z[32];
z[8] = abb[83] * z[72];
z[10] = -abb[84] * z[126];
z[11] = abb[81] * z[20];
z[12] = -abb[80] * z[134];
z[13] = -abb[94] * z[57];
z[15] = -abb[91] + -abb[92];
z[15] = z[15] * z[87];
z[16] = abb[93] * z[90];
z[17] = abb[71] + -z[63];
z[17] = abb[65] * z[17];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + 2 * z[14] + z[15] + z[16] + z[17] + z[19] + z[21];
z[0] = 2 * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_843_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("65.16124175127668153126058446024402888911289348178807025267998752"),stof<T>("187.82926484472532126748614561461626579056996112332777451354691725")}, std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("10.060152217277230393536793041756720365371175384591388288842506102"),stof<T>("-10.099388811699551455892547158001484358148356997344369540526954736")}, std::complex<T>{stof<T>("-37.610696984276955962398688751000374627242034433189729270761246812"),stof<T>("-88.864938016512884905796799228307390716210802062991702486509981258")}, std::complex<T>{stof<T>("-37.610696984276955962398688751000374627242034433189729270761246812"),stof<T>("-88.864938016512884905796799228307390716210802062991702486509981258")}, std::complex<T>{stof<T>("42.640773092915571159167085271878734809927622125485423415182499863"),stof<T>("83.815243610663109177850525649306648537136623564319517716246503889")}, std::complex<T>{stof<T>("88.438761380254411799178704222807150816421330713990782794771155063"),stof<T>("-29.3588069102377698164638159932981645214751773211859412438357027")}, std::complex<T>{stof<T>("88.438761380254411799178704222807150816421330713990782794771155063"),stof<T>("-29.3588069102377698164638159932981645214751773211859412438357027")}, std::complex<T>{stof<T>("99.710178044973750252386082667054357175919656034901473263628816938"),stof<T>("-15.052445305866626618174960633655949945311434730448914446779643992")}, std::complex<T>{stof<T>("99.710178044973750252386082667054357175919656034901473263628816938"),stof<T>("-15.052445305866626618174960633655949945311434730448914446779643992")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_843_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_843_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("49.499357542415443179582979236732505355273196721728966272143772872"),stof<T>("-50.088749805493674438732238674734619143005036603016267680525524149")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({200});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,96> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), f_2_25_im(k), (log2_im(k.W[164]) - log2_im(base_point<T>.W[164])), (log2_im(k.W[166]) - log2_im(base_point<T>.W[166])), (log2_im(k.W[170]) - log2_im(base_point<T>.W[170])), (log2_im(k.W[172]) - log2_im(base_point<T>.W[172])), (log3_im(k.W[190]) - log3_im(base_point<T>.W[190])), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), f_2_25_re(k), (log2_re(k.W[164]) - log2_re(base_point<T>.W[164])), (log2_re(k.W[166]) - log2_re(base_point<T>.W[166])), (log2_re(k.W[170]) - log2_re(base_point<T>.W[170])), (log2_re(k.W[172]) - log2_re(base_point<T>.W[172])), (log3_re(k.W[190]) - log3_re(base_point<T>.W[190])), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W165(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[91] = c.real();
abb[61] = c.imag();
SpDLog_Sigma5<T,200,164>(k, dv, abb[91], abb[61], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W167(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[92] = c.real();
abb[62] = c.imag();
SpDLog_Sigma5<T,200,166>(k, dv, abb[92], abb[62], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W171(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[93] = c.real();
abb[63] = c.imag();
SpDLog_Sigma5<T,200,170>(k, dv, abb[93], abb[63], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W173(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[94] = c.real();
abb[64] = c.imag();
SpDLog_Sigma5<T,200,172>(k, dv, abb[94], abb[64], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W191(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[95] = c.real();
abb[65] = c.imag();
SpDLog_Sigma5<T,200,190>(k, dv, abb[95], abb[65], f_2_32_series_coefficients<T>);
}

                    
            return f_4_843_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_843_DLogXconstant_part(base_point<T>, kend);
	value += f_4_843_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_843_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_843_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_843_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_843_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_843_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_843_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
