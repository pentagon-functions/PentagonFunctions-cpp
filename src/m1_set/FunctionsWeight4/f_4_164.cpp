/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_164.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_164_abbreviated (const std::array<T,15>& abb) {
T z[14];
z[0] = abb[10] + abb[12];
z[1] = abb[11] + (T(1) / T(2)) * z[0];
z[2] = abb[0] * z[1];
z[3] = abb[13] * (T(3) / T(2));
z[4] = abb[3] * z[3];
z[2] = z[2] + z[4];
z[2] = (T(1) / T(2)) * z[2];
z[5] = abb[2] * (T(1) / T(2));
z[6] = z[1] * z[5];
z[7] = abb[1] * z[1];
z[6] = z[2] + z[6] + z[7];
z[6] = abb[8] * z[6];
z[5] = abb[1] + z[5];
z[5] = z[1] * z[5];
z[2] = z[2] + z[5];
z[2] = abb[7] * z[2];
z[5] = -z[2] + z[6];
z[7] = -abb[1] + abb[2];
z[8] = -z[1] * z[7];
z[9] = abb[11] + -abb[12];
z[9] = abb[0] * z[9];
z[3] = abb[4] * z[3];
z[4] = -z[3] + -z[4] + z[8] + 2 * z[9];
z[4] = abb[6] * z[4];
z[4] = z[4] + -z[5];
z[4] = abb[6] * z[4];
z[2] = z[2] + z[6];
z[2] = abb[7] * z[2];
z[6] = abb[3] * abb[13];
z[6] = 3 * z[6];
z[8] = z[6] + (T(-13) / T(3)) * z[9];
z[0] = 2 * abb[11] + z[0];
z[10] = abb[1] * z[0];
z[11] = abb[12] * (T(1) / T(2)) + abb[10] * (T(2) / T(3)) + abb[11] * (T(5) / T(6));
z[11] = abb[2] * z[11];
z[8] = z[3] + (T(1) / T(2)) * z[8] + (T(-2) / T(3)) * z[10] + z[11];
z[8] = prod_pow(m1_set::bc<T>[0], 2) * z[8];
z[10] = abb[3] * (T(1) / T(2));
z[10] = z[1] * z[10];
z[11] = -abb[5] + abb[1] * (T(1) / T(2)) + abb[0] * (T(3) / T(4));
z[12] = abb[13] * z[11];
z[12] = z[10] + z[12];
z[12] = abb[14] * z[12];
z[13] = -abb[0] + abb[1];
z[1] = z[1] * z[13];
z[1] = z[1] + z[3];
z[3] = prod_pow(abb[8], 2);
z[1] = z[1] * z[3];
z[13] = -abb[10] + abb[11];
z[3] = z[3] * z[13];
z[13] = abb[13] * abb[14];
z[3] = 2 * z[3] + (T(9) / T(4)) * z[13];
z[3] = abb[2] * z[3];
z[1] = z[1] + z[2] + z[3] + z[4] + z[8] + 3 * z[12];
z[0] = z[0] * z[7];
z[2] = abb[4] * abb[13];
z[0] = z[0] + 3 * z[2] + z[6] + -4 * z[9];
z[0] = abb[6] * z[0];
z[0] = z[0] + z[5];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[2] * (T(3) / T(4)) + z[11];
z[2] = abb[13] * z[2];
z[2] = z[2] + z[10];
z[2] = abb[9] * z[2];
z[0] = z[0] + 3 * z[2];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_164_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("1.3427569402766636491166813735746629073271065237564429612567340896"),stof<T>("1.1320407441903722082871918205716230148170823948741651600313691976")}, std::complex<T>{stof<T>("25.574899553340639407108411114306672003636284064046785281202285788"),stof<T>("28.102645388630594562804646197061075364821905261319253398516520148")}, std::complex<T>{stof<T>("0.424591964293097211454670569656800591547894339290861810296853344"),stof<T>("-24.706523156059477937943070735346206320370658076696757918422412556")}, std::complex<T>{stof<T>("13.96415643147508826922384977244427510120715403847038606878385454"),stof<T>("-20.369548172423014954390302153134024158599942098253563196717532818")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[118].real()/kbase.W[118].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_164_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_164_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("3.905217360613558972731814832792167265107693827868926941042545612"),stof<T>("-10.3904067215425307129331903671323429726419596568508986498093896242")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,15> abb = {dl[0], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[118].real()/k.W[118].real()), f_2_25_re(k)};

                    
            return f_4_164_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_164_DLogXconstant_part(base_point<T>, kend);
	value += f_4_164_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_164_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_164_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_164_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_164_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_164_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_164_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
