/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_135.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_135_abbreviated (const std::array<T,28>& abb) {
T z[76];
z[0] = 6 * abb[3] + abb[1] * (T(33) / T(2));
z[1] = abb[5] * (T(13) / T(2));
z[2] = 4 * abb[4];
z[3] = 2 * abb[2];
z[4] = abb[0] + z[3];
z[5] = -z[0] + z[1] + z[2] + z[4];
z[5] = abb[22] * z[5];
z[6] = 4 * abb[1];
z[7] = 2 * abb[3];
z[8] = z[6] + z[7];
z[9] = 2 * abb[4];
z[10] = z[8] + -z[9];
z[11] = 2 * abb[5];
z[12] = abb[0] + abb[2];
z[13] = z[10] + -z[11] + -z[12];
z[14] = abb[23] * z[13];
z[15] = -abb[0] + abb[3];
z[16] = 2 * abb[1];
z[17] = z[15] + z[16];
z[18] = abb[25] * z[17];
z[14] = z[14] + -z[18];
z[19] = abb[17] + -abb[25];
z[20] = abb[19] + -abb[24];
z[21] = abb[22] + z[20];
z[22] = abb[21] + z[19] + z[21];
z[23] = abb[6] * z[22];
z[24] = -abb[2] + abb[3];
z[25] = z[16] + z[24];
z[26] = abb[24] * z[25];
z[27] = z[23] + z[26];
z[28] = abb[4] * (T(3) / T(2));
z[29] = abb[1] * (T(1) / T(2));
z[30] = z[28] + -z[29];
z[31] = -abb[0] + abb[5];
z[32] = -z[7] + z[31];
z[33] = -z[30] + z[32];
z[33] = abb[17] * z[33];
z[34] = 4 * abb[5];
z[35] = 2 * abb[0];
z[36] = abb[2] + z[35];
z[0] = abb[4] * (T(13) / T(2)) + -z[0] + z[34] + z[36];
z[0] = abb[21] * z[0];
z[29] = abb[5] * (T(3) / T(2)) + -z[29];
z[37] = abb[2] + z[7];
z[38] = abb[4] + -z[29] + -z[37];
z[38] = abb[19] * z[38];
z[0] = z[0] + z[5] + -z[14] + z[27] + z[33] + z[38];
z[0] = abb[13] * z[0];
z[5] = z[12] + -z[16];
z[33] = abb[23] * z[5];
z[38] = abb[1] + abb[4];
z[38] = abb[24] * z[38];
z[39] = abb[1] + abb[5];
z[39] = abb[25] * z[39];
z[40] = z[33] + z[38] + z[39];
z[41] = 6 * abb[1];
z[42] = -abb[4] + z[41];
z[43] = -z[7] + z[36] + -z[42];
z[43] = abb[21] * z[43];
z[44] = -abb[5] + z[7];
z[45] = z[4] + -z[41] + -z[44];
z[45] = abb[22] * z[45];
z[46] = -z[16] + z[32];
z[46] = abb[17] * z[46];
z[47] = -abb[4] + z[16];
z[48] = z[37] + z[47];
z[49] = -abb[19] * z[48];
z[43] = z[40] + z[43] + z[45] + z[46] + z[49];
z[45] = 2 * abb[12];
z[43] = z[43] * z[45];
z[32] = -z[3] + -z[9] + -z[32] + z[41];
z[41] = 2 * abb[22];
z[32] = z[32] * z[41];
z[49] = 3 * abb[4];
z[50] = 3 * abb[1];
z[51] = z[11] + z[49] + -z[50];
z[51] = abb[17] * z[51];
z[52] = 2 * abb[19];
z[48] = z[48] * z[52];
z[53] = 2 * z[18];
z[38] = 2 * z[38] + z[53];
z[48] = -z[38] + z[48];
z[54] = abb[0] + -abb[2];
z[55] = 4 * abb[3];
z[56] = -z[11] + z[54] + z[55];
z[57] = 19 * abb[1];
z[58] = 9 * abb[4] + -z[57];
z[56] = 2 * z[56] + -z[58];
z[56] = abb[21] * z[56];
z[59] = abb[4] + abb[5];
z[60] = z[12] + z[59];
z[50] = abb[3] + z[50];
z[61] = -z[50] + z[60];
z[61] = abb[23] * z[61];
z[62] = 2 * z[61];
z[32] = z[32] + z[48] + z[51] + z[56] + -z[62];
z[32] = abb[10] * z[32];
z[56] = -abb[10] + abb[12];
z[56] = z[22] * z[56];
z[63] = 2 * abb[6];
z[64] = z[56] * z[63];
z[0] = z[0] + z[32] + z[43] + z[64];
z[0] = abb[13] * z[0];
z[32] = 3 * abb[3];
z[43] = 8 * abb[1];
z[65] = z[32] + z[43];
z[66] = -abb[0] + z[3];
z[67] = z[9] + z[34] + -z[65] + -z[66];
z[67] = abb[21] * z[67];
z[68] = -abb[2] + z[35];
z[69] = z[2] + z[11] + -z[65] + -z[68];
z[69] = abb[22] * z[69];
z[70] = -abb[3] + -z[11] + z[66];
z[70] = abb[17] * z[70];
z[71] = -abb[3] + -z[9] + z[68];
z[71] = abb[19] * z[71];
z[14] = -z[14] + z[26] + z[67] + z[69] + z[70] + z[71];
z[14] = abb[10] * z[14];
z[67] = z[11] + -z[15];
z[67] = abb[25] * z[67];
z[67] = z[33] + z[67];
z[16] = -abb[3] + abb[5] + -z[12] + -z[16];
z[16] = abb[17] * z[16];
z[69] = -abb[21] * z[17];
z[29] = -z[29] + -z[54];
z[29] = abb[19] * z[29];
z[70] = abb[1] * (T(9) / T(2)) + z[7];
z[71] = abb[5] * (T(5) / T(2)) + -z[66] + -z[70];
z[71] = abb[22] * z[71];
z[16] = z[16] + z[27] + z[29] + z[67] + z[69] + z[71];
z[16] = abb[11] * z[16];
z[27] = z[11] + z[36];
z[29] = -z[27] + z[65];
z[29] = abb[22] * z[29];
z[32] = z[6] + z[32];
z[69] = abb[0] + -z[11] + z[32];
z[69] = abb[17] * z[69];
z[6] = abb[3] + z[6];
z[71] = 3 * abb[0];
z[72] = z[6] + -z[71];
z[72] = abb[21] * z[72];
z[73] = abb[2] + abb[3];
z[73] = abb[19] * z[73];
z[29] = -z[26] + z[29] + -z[67] + z[69] + z[72] + z[73];
z[29] = abb[12] * z[29];
z[39] = z[26] + z[39];
z[46] = z[39] + z[46];
z[67] = -z[11] + z[37];
z[42] = -z[35] + z[42] + z[67];
z[42] = abb[21] * z[42];
z[42] = z[42] + -z[46] + -z[61];
z[69] = abb[1] + -abb[5];
z[69] = -z[9] + 3 * z[69];
z[69] = abb[19] * z[69];
z[22] = z[22] * z[63];
z[63] = z[22] + z[69];
z[69] = 8 * abb[3];
z[57] = -9 * abb[5] + z[57] + z[69];
z[72] = -z[2] + -2 * z[54] + z[57];
z[72] = abb[22] * z[72];
z[42] = 2 * z[42] + -z[63] + z[72];
z[42] = abb[13] * z[42];
z[56] = abb[6] * z[56];
z[14] = z[14] + z[16] + z[29] + z[42] + -z[56];
z[14] = abb[11] * z[14];
z[16] = abb[1] * (T(8) / T(3)) + z[7] + -z[9] + -z[11] + (T(-1) / T(3)) * z[12];
z[16] = abb[23] * z[16];
z[29] = abb[3] * (T(5) / T(6));
z[42] = abb[2] * (T(-13) / T(3)) + -z[71];
z[28] = abb[1] * (T(-7) / T(6)) + abb[5] * (T(10) / T(3)) + z[28] + -z[29] + (T(1) / T(2)) * z[42];
z[28] = abb[17] * z[28];
z[42] = 3 * abb[2];
z[71] = abb[3] * (T(5) / T(3));
z[72] = 3 * abb[5];
z[73] = abb[0] * (T(-13) / T(3)) + abb[1] * (T(-7) / T(3)) + -z[42] + -z[71] + z[72];
z[73] = abb[4] * (T(10) / T(3)) + (T(1) / T(2)) * z[73];
z[73] = abb[19] * z[73];
z[74] = 5 * abb[0] + 4 * abb[2];
z[1] = -z[1] + 2 * z[74];
z[74] = abb[3] + abb[1] * (T(13) / T(6));
z[1] = (T(1) / T(3)) * z[1] + -z[2] + z[74];
z[1] = abb[22] * z[1];
z[44] = z[35] + -z[44];
z[44] = -abb[1] + (T(1) / T(3)) * z[44];
z[44] = abb[25] * z[44];
z[75] = 4 * abb[0] + 5 * abb[2];
z[75] = -z[11] + (T(1) / T(3)) * z[75];
z[74] = abb[4] * (T(-13) / T(6)) + z[74] + 2 * z[75];
z[74] = abb[21] * z[74];
z[75] = -abb[1] + abb[4] * (T(1) / T(3)) + (T(-2) / T(3)) * z[24];
z[75] = abb[24] * z[75];
z[1] = z[1] + z[16] + (T(-1) / T(3)) * z[23] + z[28] + z[44] + z[73] + z[74] + z[75];
z[16] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = z[1] * z[16];
z[23] = z[9] + -z[24];
z[23] = abb[24] * z[23];
z[18] = z[18] + z[23] + z[33];
z[23] = -z[30] + z[54];
z[23] = abb[17] * z[23];
z[28] = abb[4] * (T(5) / T(2)) + -z[68] + -z[70];
z[28] = abb[21] * z[28];
z[30] = abb[3] + z[47];
z[44] = -z[12] + -z[30];
z[44] = abb[19] * z[44];
z[47] = -abb[22] * z[25];
z[23] = z[18] + z[23] + z[28] + z[44] + z[47];
z[23] = abb[10] * z[23];
z[28] = z[4] + z[9];
z[44] = -z[28] + z[65];
z[44] = abb[21] * z[44];
z[32] = abb[2] + -z[9] + z[32];
z[32] = abb[19] * z[32];
z[6] = z[6] + -z[42];
z[6] = abb[22] * z[6];
z[42] = abb[0] + abb[3];
z[42] = abb[17] * z[42];
z[6] = z[6] + -z[18] + z[32] + z[42] + z[44];
z[6] = abb[12] * z[6];
z[6] = z[6] + z[23] + -z[56];
z[6] = abb[10] * z[6];
z[18] = 9 * abb[1];
z[23] = z[18] + z[55];
z[28] = -z[23] + z[28];
z[28] = abb[21] * z[28];
z[32] = -z[3] + z[50];
z[32] = z[32] * z[41];
z[7] = abb[0] + abb[1] + z[7];
z[7] = abb[17] * z[7];
z[7] = z[7] + -z[22] + -z[28] + z[32] + -2 * z[33] + z[48];
z[28] = abb[26] * z[7];
z[32] = z[43] + z[55];
z[42] = 5 * abb[5];
z[43] = -z[32] + z[42];
z[44] = z[9] + z[43] + z[68];
z[44] = abb[13] * z[44];
z[17] = -abb[5] + z[17];
z[47] = abb[12] * z[17];
z[30] = -abb[5] + z[30];
z[48] = abb[10] * z[30];
z[47] = z[47] + -z[48];
z[44] = z[44] + -2 * z[47];
z[47] = -z[8] + z[68] + z[72];
z[47] = abb[11] * z[47];
z[44] = 2 * z[44] + -z[47];
z[44] = abb[11] * z[44];
z[47] = 5 * abb[4];
z[32] = z[11] + -z[32] + z[47] + z[66];
z[32] = abb[10] * z[32];
z[48] = -z[8] + z[60];
z[48] = z[45] * z[48];
z[32] = z[32] + -z[48];
z[48] = 16 * abb[1] + -z[12] + -7 * z[59] + z[69];
z[48] = abb[13] * z[48];
z[32] = 2 * z[32] + z[48];
z[32] = abb[13] * z[32];
z[48] = -z[8] + z[49] + z[66];
z[48] = abb[10] * z[48];
z[25] = -abb[4] + z[25];
z[54] = 4 * z[25];
z[56] = abb[12] * z[54];
z[48] = z[48] + z[56];
z[48] = abb[10] * z[48];
z[5] = -abb[3] + z[5];
z[56] = prod_pow(abb[12], 2);
z[56] = 2 * z[56];
z[5] = z[5] * z[56];
z[60] = abb[27] * z[17];
z[5] = -z[5] + z[32] + z[44] + -z[48] + -4 * z[60];
z[32] = abb[1] * (T(-10) / T(3)) + -3 * z[12] + (T(14) / T(3)) * z[59] + -z[71];
z[32] = z[16] * z[32];
z[25] = 8 * z[25];
z[44] = -abb[26] * z[25];
z[32] = 2 * z[5] + z[32] + z[44];
z[32] = abb[18] * z[32];
z[29] = abb[1] * (T(-5) / T(3)) + (T(-3) / T(2)) * z[12] + -z[29] + (T(7) / T(3)) * z[59];
z[16] = z[16] * z[29];
z[29] = -abb[26] * z[54];
z[5] = z[5] + z[16] + z[29];
z[5] = abb[20] * z[5];
z[16] = abb[22] + z[19];
z[29] = abb[7] + -abb[9];
z[16] = z[16] * z[29];
z[20] = -abb[21] + -z[20];
z[29] = abb[7] + abb[9];
z[20] = z[20] * z[29];
z[19] = abb[23] + 2 * z[19] + z[21];
z[19] = abb[8] * z[19];
z[16] = z[16] + z[19] + z[20];
z[16] = abb[14] * z[16];
z[19] = -abb[1] + -z[24];
z[19] = abb[17] * z[19];
z[20] = abb[0] + -abb[1];
z[20] = abb[21] * z[20];
z[15] = -abb[1] + -z[15];
z[15] = abb[19] * z[15];
z[21] = -abb[1] + abb[2];
z[21] = abb[22] * z[21];
z[15] = z[15] + z[19] + z[20] + z[21];
z[15] = z[15] * z[56];
z[19] = -z[35] + z[50];
z[19] = abb[21] * z[19];
z[19] = z[19] + -z[33] + -z[46];
z[20] = -z[23] + z[27];
z[20] = abb[22] * z[20];
z[21] = abb[1] + z[37];
z[21] = abb[19] * z[21];
z[19] = -2 * z[19] + z[20] + -z[21] + z[22];
z[20] = -abb[27] * z[19];
z[0] = z[0] + z[1] + z[5] + z[6] + z[14] + z[15] + z[16] + z[20] + z[28] + z[32];
z[1] = 2 * abb[23];
z[1] = -z[1] * z[13];
z[5] = -z[12] + z[34] + -z[55];
z[5] = 2 * z[5] + z[58];
z[5] = abb[21] * z[5];
z[6] = 8 * abb[4] + -2 * z[12] + -z[57];
z[6] = abb[22] * z[6];
z[1] = z[1] + z[5] + z[6] + 2 * z[26] + -z[51] + z[53] + z[63];
z[1] = abb[13] * z[1];
z[5] = z[35] + z[67];
z[5] = 2 * z[5] + z[18] + -z[47];
z[5] = abb[21] * z[5];
z[6] = -abb[1] + 2 * z[31] + z[49];
z[6] = abb[17] * z[6];
z[13] = abb[2] + abb[4];
z[14] = z[13] * z[52];
z[15] = z[10] + -z[31];
z[15] = z[15] * z[41];
z[5] = z[5] + z[6] + z[14] + z[15] + -z[38] + -z[62];
z[5] = abb[10] * z[5];
z[6] = -z[3] + z[31];
z[6] = abb[17] * z[6];
z[13] = -z[8] + z[13];
z[13] = abb[21] * z[13];
z[14] = abb[0] + abb[5];
z[15] = -z[8] + z[14];
z[15] = abb[22] * z[15];
z[16] = abb[4] + -z[36];
z[16] = abb[19] * z[16];
z[6] = z[6] + z[13] + z[15] + z[16] + z[40];
z[6] = z[6] * z[45];
z[8] = z[8] + -z[11];
z[11] = abb[2] + -abb[4] + z[8];
z[11] = abb[21] * z[11];
z[13] = abb[17] * z[14];
z[11] = z[11] + z[13] + -z[39] + -z[61];
z[2] = -z[2] + 2 * z[4] + z[23] + -z[42];
z[2] = abb[22] * z[2];
z[3] = -abb[1] + -z[3] + z[9] + z[72];
z[3] = abb[19] * z[3];
z[2] = z[2] + z[3] + 2 * z[11] + -z[22];
z[2] = abb[11] * z[2];
z[1] = z[1] + z[2] + z[5] + z[6] + z[64];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[18] * z[25];
z[3] = -abb[20] * z[54];
z[2] = z[2] + z[3] + z[7];
z[2] = abb[15] * z[2];
z[3] = -z[12] + z[43] + z[47];
z[3] = abb[13] * z[3];
z[4] = abb[2] + z[10] + -z[72];
z[4] = abb[11] * z[4];
z[5] = abb[0] + z[8] + -z[49];
z[5] = abb[10] * z[5];
z[6] = z[30] * z[45];
z[3] = z[3] + z[4] + z[5] + -z[6];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = abb[16] * z[17];
z[3] = z[3] + 2 * z[4];
z[4] = -4 * abb[18] + -2 * abb[20];
z[3] = z[3] * z[4];
z[4] = -abb[16] * z[19];
z[1] = z[1] + z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_135_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.663804198741382916323003363798535663406020240960870280043881857"),stof<T>("44.578413247099866696588107410191515115522659040161760163289735341")}, std::complex<T>{stof<T>("96.576423119483800638541712763655208617336184268575365829491113045"),stof<T>("39.710084677870850222856078519316728127069507497910447031125689175")}, std::complex<T>{stof<T>("10.039597783872935736739861250442821507956218446973836745051678499"),stof<T>("56.026001671076411092083835250596457869155237518108679491344352049")}, std::complex<T>{stof<T>("48.288211559741900319270856381827604308668092134287682914745556523"),stof<T>("19.855042338935425111428039259658364063534753748955223515562844588")}, std::complex<T>{stof<T>("-40.649630980957068862662165606047940944874453881504406570551133054"),stof<T>("-45.973448705097816815533311702898649198220146078127589594780064425")}, std::complex<T>{stof<T>("-32.025424566088621683079023492692226789424652087517373035558929697"),stof<T>("-57.421037129074361211029039543303591951852724556074508922834681132")}, std::complex<T>{stof<T>("4.8745100762453897497441617156603955114077372244256888918179120247"),stof<T>("-0.6713527524053635221576758238084778882013466340959214186396851835")}, std::complex<T>{stof<T>("-15.108892572745061412213892796520457261086464323838870134816341142"),stof<T>("-8.354975580390796395232248397316582697642181201437503469674320267")}, std::complex<T>{stof<T>("-16.068002281026542710461963058718137277520931394330965381239876208"),stof<T>("-9.433678548141315075092910745826169394993738875455969195758510053")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[41].real()/kbase.W[41].real()), rlog(k.W[46].real()/kbase.W[46].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_135_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_135_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("8.831594633261983065343147544910618491992566437110539899401547803"),stof<T>("-69.312057020728738185628147451701041861663325245838383298739100947")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[41].real()/k.W[41].real()), rlog(kend.W[46].real()/k.W[46].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_135_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_135_DLogXconstant_part(base_point<T>, kend);
	value += f_4_135_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_135_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_135_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_135_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_135_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_135_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_135_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
