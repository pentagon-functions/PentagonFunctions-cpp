/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_608.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_608_abbreviated (const std::array<T,65>& abb) {
T z[109];
z[0] = abb[46] + abb[51];
z[1] = abb[43] + -abb[47];
z[2] = abb[50] * (T(1) / T(4));
z[3] = 3 * abb[48];
z[0] = abb[44] * (T(5) / T(3)) + (T(5) / T(6)) * z[0] + (T(13) / T(12)) * z[1] + z[2] + -z[3];
z[0] = abb[0] * z[0];
z[4] = abb[45] + abb[51];
z[5] = abb[47] * (T(11) / T(6));
z[6] = abb[49] * (T(5) / T(2));
z[7] = 3 * abb[50];
z[4] = abb[44] * (T(-22) / T(3)) + abb[46] * (T(-11) / T(3)) + abb[48] * (T(5) / T(2)) + (T(-11) / T(6)) * z[4] + -z[5] + z[6] + z[7];
z[4] = abb[1] * z[4];
z[8] = 2 * abb[48];
z[9] = abb[47] + -abb[50];
z[10] = abb[45] + -abb[51];
z[11] = -z[8] + -z[9] + -z[10];
z[11] = abb[8] * z[11];
z[12] = abb[22] * abb[54];
z[13] = abb[55] + abb[56] + -abb[57];
z[14] = abb[22] * z[13];
z[12] = z[12] + z[14];
z[15] = abb[54] + z[13];
z[16] = abb[20] * z[15];
z[11] = z[11] + z[12] + z[16];
z[16] = abb[50] * (T(3) / T(2));
z[17] = 2 * abb[44] + abb[46];
z[18] = z[16] + -z[17];
z[19] = abb[51] * (T(1) / T(2));
z[20] = abb[47] * (T(1) / T(2));
z[21] = z[19] + z[20];
z[22] = z[18] + -z[21];
z[23] = abb[45] * (T(1) / T(2));
z[24] = z[22] + -z[23];
z[25] = -abb[12] * z[24];
z[26] = (T(1) / T(2)) * z[15];
z[27] = abb[19] * z[26];
z[25] = z[25] + z[27];
z[27] = abb[51] * (T(3) / T(4));
z[28] = abb[44] + abb[46] * (T(1) / T(2));
z[29] = abb[47] + z[28];
z[6] = abb[43] * (T(-1) / T(12)) + abb[45] * (T(19) / T(12)) + -z[6] + -z[27] + (T(5) / T(3)) * z[29];
z[6] = abb[7] * z[6];
z[29] = -abb[47] + -abb[50] + abb[51];
z[29] = abb[49] + -z[23] + (T(1) / T(2)) * z[29];
z[30] = abb[5] * z[29];
z[31] = -z[20] + z[28];
z[32] = abb[48] * (T(3) / T(2));
z[33] = abb[51] + z[31] + -z[32];
z[33] = abb[14] * z[33];
z[34] = (T(3) / T(4)) * z[15];
z[35] = abb[21] * z[34];
z[36] = abb[23] * z[34];
z[35] = z[35] + z[36];
z[37] = -abb[48] + abb[51];
z[37] = abb[11] * z[37];
z[38] = -z[35] + 3 * z[37];
z[39] = abb[43] + z[10];
z[40] = abb[13] * z[39];
z[41] = abb[52] + -abb[53];
z[42] = abb[18] * z[41];
z[43] = z[40] + -z[42];
z[44] = abb[24] * z[26];
z[44] = (T(-1) / T(2)) * z[43] + -z[44];
z[45] = z[19] + z[28];
z[46] = abb[50] * (T(1) / T(2));
z[47] = abb[48] * (T(1) / T(2)) + -z[45] + z[46];
z[47] = abb[9] * z[47];
z[10] = z[9] + -z[10];
z[48] = abb[6] * z[10];
z[49] = abb[14] * (T(1) / T(2));
z[50] = 3 * abb[11];
z[51] = z[49] + z[50];
z[51] = abb[45] * z[51];
z[52] = abb[15] * z[41];
z[53] = abb[16] * z[41];
z[54] = abb[2] * z[39];
z[55] = abb[45] * (T(-31) / T(12)) + abb[44] * (T(-11) / T(3)) + abb[46] * (T(-11) / T(6)) + abb[43] * (T(-1) / T(6)) + abb[50] * (T(5) / T(4)) + abb[47] * (T(7) / T(12)) + z[27];
z[55] = abb[4] * z[55];
z[5] = -abb[50] + abb[45] * (T(-11) / T(3)) + abb[44] * (T(-8) / T(3)) + abb[46] * (T(-4) / T(3)) + abb[49] * (T(1) / T(2)) + abb[51] * (T(7) / T(3)) + z[5];
z[5] = abb[3] * z[5];
z[0] = z[0] + z[4] + z[5] + z[6] + 2 * z[11] + z[25] + (T(-1) / T(2)) * z[30] + -z[33] + -z[38] + -z[44] + -z[47] + (T(1) / T(4)) * z[48] + z[51] + (T(-5) / T(4)) * z[52] + -z[53] + (T(-1) / T(6)) * z[54] + z[55];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[4] = abb[34] + abb[60];
z[5] = abb[35] + z[4];
z[5] = z[5] * z[15];
z[6] = abb[21] * z[5];
z[11] = abb[60] * z[15];
z[51] = abb[58] * z[15];
z[11] = z[11] + z[51];
z[11] = abb[20] * z[11];
z[55] = abb[51] + z[9];
z[56] = -abb[45] + z[55];
z[56] = abb[6] * z[4] * z[56];
z[57] = abb[35] * z[15];
z[57] = z[51] + z[57];
z[57] = abb[19] * z[57];
z[5] = z[5] + z[51];
z[5] = abb[23] * z[5];
z[5] = z[5] + z[6] + z[11] + z[56] + z[57];
z[6] = -abb[58] * z[24];
z[11] = abb[35] * z[22];
z[51] = abb[34] * (T(1) / T(2));
z[56] = abb[43] + -abb[51];
z[57] = z[51] * z[56];
z[4] = -abb[35] + z[4];
z[4] = z[4] * z[23];
z[58] = abb[60] * z[55];
z[4] = z[4] + -z[6] + z[11] + z[57] + (T(-1) / T(2)) * z[58];
z[4] = abb[4] * z[4];
z[11] = -z[17] + z[46];
z[46] = z[11] + z[21];
z[58] = -abb[60] * z[46];
z[59] = 2 * abb[49] + abb[47] * (T(-3) / T(2)) + z[11] + z[19];
z[59] = abb[35] * z[59];
z[55] = -z[51] * z[55];
z[60] = -abb[35] + abb[60];
z[60] = abb[34] + 3 * z[60];
z[60] = z[23] * z[60];
z[55] = z[55] + z[58] + z[59] + z[60];
z[55] = abb[3] * z[55];
z[8] = abb[51] * (T(3) / T(2)) + -z[8] + -z[11] + -z[20];
z[8] = abb[14] * z[8];
z[11] = abb[22] + -abb[25];
z[58] = abb[54] * (T(1) / T(2));
z[11] = z[11] * z[58];
z[13] = abb[25] * z[13];
z[60] = (T(1) / T(2)) * z[13];
z[61] = abb[51] + z[17];
z[62] = -abb[48] + -abb[50] + z[61];
z[62] = abb[9] * z[62];
z[8] = z[8] + -z[11] + z[60] + -z[62];
z[11] = abb[14] * z[23];
z[63] = z[11] + (T(1) / T(2)) * z[14];
z[64] = -z[8] + z[63];
z[65] = abb[60] * z[64];
z[66] = abb[21] * z[26];
z[64] = z[64] + z[66];
z[64] = abb[58] * z[64];
z[67] = -abb[34] + -3 * abb[35];
z[67] = z[23] * z[67];
z[57] = -z[57] + z[59] + z[67];
z[57] = abb[7] * z[57];
z[59] = -abb[35] * z[24];
z[6] = z[6] + z[59];
z[6] = abb[12] * z[6];
z[42] = -z[42] + z[52] + z[53];
z[42] = z[42] * z[51];
z[51] = -abb[2] + abb[13] * (T(1) / T(2));
z[56] = abb[45] + z[56];
z[51] = z[51] * z[56];
z[56] = abb[24] * z[15];
z[51] = z[51] + (T(1) / T(2)) * z[56];
z[51] = abb[34] * z[51];
z[56] = abb[47] + abb[51];
z[59] = -4 * abb[44] + -2 * abb[46] + z[7] + -z[56];
z[67] = -abb[45] + z[59];
z[68] = abb[35] + abb[58];
z[68] = abb[1] * z[67] * z[68];
z[69] = -abb[51] + z[9];
z[69] = abb[48] + (T(1) / T(2)) * z[69];
z[70] = z[23] + z[69];
z[70] = abb[8] * z[70];
z[71] = -abb[58] + -abb[60];
z[71] = z[70] * z[71];
z[72] = -abb[35] * z[30];
z[4] = z[4] + (T(1) / T(2)) * z[5] + z[6] + z[42] + z[51] + z[55] + z[57] + z[64] + z[65] + z[68] + z[71] + z[72];
z[5] = abb[47] * (T(5) / T(4));
z[6] = abb[50] * (T(3) / T(4));
z[42] = z[3] + z[5] + -z[6];
z[45] = abb[43] * (T(-5) / T(4)) + z[42] + -z[45];
z[45] = abb[0] * z[45];
z[51] = -abb[1] * z[24];
z[55] = abb[47] * (T(1) / T(4));
z[57] = -z[6] + z[55];
z[64] = z[28] + z[57];
z[65] = abb[51] * (T(1) / T(4));
z[68] = abb[45] * (T(1) / T(4));
z[71] = z[64] + z[65] + z[68];
z[72] = 3 * abb[12];
z[73] = z[71] * z[72];
z[74] = abb[19] * z[34];
z[73] = z[73] + z[74];
z[74] = (T(3) / T(4)) * z[41];
z[75] = abb[16] * z[74];
z[76] = z[73] + -z[75];
z[77] = abb[24] * z[34];
z[78] = abb[18] * z[74];
z[40] = (T(3) / T(4)) * z[40] + z[77] + -z[78];
z[77] = (T(1) / T(4)) * z[15];
z[78] = -abb[21] * z[77];
z[78] = (T(-1) / T(4)) * z[12] + -z[47] + z[78];
z[79] = abb[43] * (T(1) / T(4));
z[80] = z[23] + z[64] + z[79];
z[81] = abb[4] * z[80];
z[82] = abb[3] * z[71];
z[34] = abb[20] * z[34];
z[34] = -z[34] + (T(3) / T(2)) * z[70];
z[18] = z[18] + -z[20];
z[20] = abb[43] * (T(1) / T(2));
z[83] = -abb[45] + z[18] + -z[20];
z[84] = -abb[7] * z[83];
z[85] = (T(3) / T(2)) * z[52];
z[36] = z[34] + -z[36] + -z[40] + z[45] + z[51] + z[54] + -z[76] + 3 * z[78] + z[81] + z[82] + z[84] + z[85];
z[36] = prod_pow(abb[28], 2) * z[36];
z[32] = -z[17] + z[32];
z[78] = abb[49] * (T(3) / T(2));
z[81] = z[21] + z[23] + -z[32] + -z[78];
z[81] = abb[1] * z[81];
z[82] = abb[51] * (T(7) / T(4));
z[57] = abb[45] * (T(11) / T(4)) + z[17] + -z[57] + -z[78] + -z[82];
z[57] = abb[3] * z[57];
z[84] = abb[45] + z[28];
z[78] = abb[47] + -z[19] + -z[78] + z[84];
z[86] = -abb[7] * z[78];
z[87] = (T(-3) / T(2)) * z[30];
z[88] = (T(3) / T(4)) * z[48];
z[49] = z[49] + -z[50];
z[49] = abb[45] * z[49];
z[21] = -z[21] + z[84];
z[84] = abb[4] * z[21];
z[12] = (T(-3) / T(4)) * z[12] + -z[33] + z[34] + z[38] + z[49] + z[57] + z[81] + z[84] + z[86] + -z[87] + -z[88];
z[12] = abb[30] * z[12];
z[33] = -abb[7] * z[24];
z[34] = -abb[3] * z[24];
z[38] = z[33] + z[34];
z[49] = -abb[4] * z[24];
z[57] = -abb[1] * z[67];
z[49] = z[49] + z[57];
z[3] = 2 * abb[51] + -z[3];
z[84] = -abb[47] + z[3] + z[17];
z[84] = abb[14] * z[84];
z[86] = (T(3) / T(2)) * z[15];
z[89] = abb[20] * z[86];
z[90] = abb[54] * (T(3) / T(2));
z[90] = abb[22] * z[90];
z[91] = 2 * abb[45];
z[92] = abb[14] * z[91];
z[93] = 3 * z[62];
z[14] = (T(3) / T(2)) * z[14] + z[38] + -z[49] + -3 * z[70] + -2 * z[84] + z[89] + z[90] + z[92] + z[93];
z[89] = abb[28] * z[14];
z[90] = (T(3) / T(2)) * z[48];
z[94] = z[17] + z[91];
z[56] = -z[56] + z[94];
z[95] = abb[3] * z[56];
z[96] = z[90] + 2 * z[95];
z[22] = abb[14] * z[22];
z[97] = abb[25] * abb[54];
z[98] = z[13] + z[97];
z[22] = -z[11] + z[22] + (T(-3) / T(2)) * z[98];
z[99] = abb[21] + abb[23];
z[100] = z[86] * z[99];
z[101] = z[22] + z[100];
z[56] = abb[4] * z[56];
z[33] = -z[33] + z[56] + z[57] + z[96] + z[101];
z[33] = abb[31] * z[33];
z[56] = 2 * abb[47] + -3 * abb[49];
z[3] = 8 * abb[44] + 4 * abb[46] + z[3] + -z[7] + z[56] + z[91];
z[3] = abb[1] * z[3];
z[7] = z[84] + -z[93];
z[91] = -abb[4] * z[67];
z[93] = -abb[51] + z[94];
z[94] = z[56] + z[93];
z[102] = abb[3] * z[94];
z[103] = abb[7] * z[94];
z[104] = abb[14] * abb[45];
z[3] = z[3] + z[7] + z[91] + z[102] + z[103] + -z[104];
z[3] = abb[33] * z[3];
z[103] = -3 * z[30] + -2 * z[102];
z[105] = 2 * abb[7];
z[94] = z[94] * z[105];
z[94] = z[22] + z[49] + z[94] + -z[103];
z[106] = -abb[32] * z[94];
z[3] = z[3] + z[12] + z[33] + z[89] + z[106];
z[3] = abb[30] * z[3];
z[12] = z[28] + -z[42] + z[82];
z[12] = abb[14] * z[12];
z[21] = abb[3] * z[21];
z[33] = abb[15] * z[74];
z[21] = -z[21] + z[33];
z[33] = (T(-1) / T(4)) * z[13] + -z[37];
z[42] = (T(1) / T(2)) * z[54];
z[89] = (T(3) / T(4)) * z[97];
z[106] = (T(1) / T(2)) * z[39];
z[107] = abb[4] * z[106];
z[50] = abb[14] * (T(-5) / T(4)) + z[50];
z[50] = abb[45] * z[50];
z[108] = abb[7] * z[39];
z[12] = z[12] + z[21] + 3 * z[33] + z[35] + -z[42] + z[45] + z[50] + z[88] + -z[89] + -z[107] + (T(5) / T(4)) * z[108];
z[12] = abb[29] * z[12];
z[33] = z[39] * z[105];
z[45] = -abb[51] + z[18] + z[20];
z[45] = abb[0] * z[45];
z[50] = (T(3) / T(2)) * z[53];
z[53] = z[45] + z[50];
z[88] = abb[24] * z[86];
z[43] = (T(-3) / T(2)) * z[43] + -z[88];
z[33] = z[22] + z[33] + z[34] + z[43] + -z[53] + 4 * z[54] + -z[107];
z[88] = -abb[28] * z[33];
z[106] = abb[7] * z[106];
z[106] = z[54] + z[106];
z[108] = -abb[4] * z[39];
z[45] = z[45] + -z[85] + -z[90] + -z[95] + -z[101] + -z[106] + z[108];
z[45] = abb[31] * z[45];
z[12] = z[12] + z[45] + z[88];
z[12] = abb[29] * z[12];
z[45] = abb[62] * z[14];
z[85] = -z[19] + -z[64] + z[79];
z[85] = abb[0] * z[85];
z[88] = 2 * z[54];
z[40] = z[40] + z[85] + -z[88];
z[90] = z[18] + -z[27] + -z[68] + z[79];
z[90] = abb[4] * z[90];
z[27] = -abb[43] + abb[45] * (T(-5) / T(4)) + z[27] + -z[64];
z[27] = abb[7] * z[27];
z[27] = z[27] + -z[34] + z[35] + z[40] + -z[57] + z[73] + z[75] + z[90];
z[27] = abb[31] * z[27];
z[33] = abb[29] * z[33];
z[34] = abb[45] * (T(3) / T(4));
z[65] = -z[18] + z[34] + z[65] + z[79];
z[65] = abb[4] * z[65];
z[40] = z[40] + -z[76];
z[35] = -z[35] + z[40];
z[73] = abb[51] * (T(5) / T(4));
z[34] = -abb[43] + -z[34] + z[64] + z[73];
z[34] = abb[7] * z[34];
z[22] = -z[22] + z[34] + z[35] + z[57] + z[65];
z[22] = abb[28] * z[22];
z[17] = abb[43] + -abb[45] + -z[17] + -z[56];
z[17] = z[17] * z[105];
z[34] = abb[4] * z[83];
z[17] = z[17] + z[34] + -z[53] + z[54] + -z[57] + -z[102];
z[17] = abb[32] * z[17];
z[34] = abb[33] * z[94];
z[17] = z[17] + z[22] + z[27] + z[33] + z[34];
z[17] = abb[32] * z[17];
z[6] = -z[6] + -z[32] + -z[55] + -z[68] + z[73];
z[6] = abb[14] * z[6];
z[27] = -abb[3] + -abb[7];
z[27] = z[27] * z[78];
z[32] = abb[4] * z[71];
z[6] = z[6] + (T(3) / T(4)) * z[13] + z[27] + z[32] + 3 * z[47] + z[81] + z[87] + z[89];
z[6] = abb[33] * z[6];
z[13] = -z[24] * z[72];
z[24] = abb[19] * z[86];
z[13] = z[13] + z[24];
z[24] = z[13] + -z[38] + -2 * z[57] + -z[91] + z[101];
z[27] = abb[28] + -abb[31];
z[24] = z[24] * z[27];
z[6] = z[6] + z[24];
z[6] = abb[33] * z[6];
z[24] = -abb[7] * z[80];
z[23] = -z[20] + z[23] + z[31];
z[23] = abb[4] * z[23];
z[21] = -z[21] + z[23] + z[24] + -z[42] + z[51] + z[85];
z[21] = abb[31] * z[21];
z[21] = z[21] + -z[22];
z[21] = abb[31] * z[21];
z[22] = abb[15] + -abb[17];
z[22] = z[22] * z[71];
z[23] = abb[7] + -2 * abb[27] + abb[13] * (T(1) / T(4));
z[23] = z[23] * z[41];
z[24] = abb[18] * z[39];
z[27] = abb[0] * z[41];
z[24] = -z[24] + 3 * z[27];
z[27] = abb[4] * z[74];
z[31] = (T(1) / T(4)) * z[39];
z[32] = abb[16] * z[31];
z[34] = abb[26] * z[77];
z[22] = z[22] + z[23] + (T(1) / T(4)) * z[24] + z[27] + z[32] + -z[34];
z[22] = 3 * z[22];
z[23] = -abb[63] * z[22];
z[24] = abb[20] * z[26];
z[24] = z[24] + z[63] + -z[70];
z[27] = abb[22] + abb[25];
z[27] = z[27] * z[58];
z[32] = abb[14] * z[69];
z[32] = z[32] + z[60];
z[27] = z[24] + z[27] + z[32];
z[34] = -abb[48] + (T(-1) / T(2)) * z[9] + z[20];
z[34] = abb[0] * z[34];
z[34] = -z[27] + -z[34] + z[44] + (T(1) / T(2)) * z[52] + z[106];
z[34] = 3 * z[34];
z[38] = -abb[59] * z[34];
z[39] = -abb[7] * z[29];
z[41] = abb[48] + abb[49] + -abb[50];
z[41] = abb[1] * z[41];
z[42] = z[39] + z[41];
z[29] = -abb[3] * z[29];
z[27] = -z[27] + z[29] + z[30] + z[42];
z[27] = 3 * z[27];
z[44] = -abb[61] * z[27];
z[0] = abb[64] + z[0] + z[3] + 3 * z[4] + z[6] + z[12] + z[17] + z[21] + z[23] + z[36] + z[38] + z[44] + z[45];
z[1] = -6 * abb[48] + (T(5) / T(2)) * z[1] + z[16] + z[61];
z[1] = abb[0] * z[1];
z[3] = abb[45] * (T(5) / T(2));
z[4] = -abb[43] + -z[3] + 3 * z[18] + -z[19];
z[4] = abb[7] * z[4];
z[6] = abb[3] * z[67];
z[1] = z[1] + z[4] + z[6] + 2 * z[7] + z[13] + -z[43] + -z[50] + -3 * z[52] + -z[88] + -z[92] + z[100] + -z[107];
z[1] = abb[28] * z[1];
z[4] = abb[23] * z[26];
z[4] = z[4] + z[66];
z[6] = z[4] + -z[62];
z[7] = (T(1) / T(2)) * z[48];
z[12] = abb[4] * (T(1) / T(2));
z[10] = z[10] * z[12];
z[13] = abb[49] + -z[93];
z[13] = abb[3] * z[13];
z[16] = 2 * abb[11] + -abb[14];
z[16] = abb[45] * z[16];
z[13] = z[6] + z[7] + z[10] + z[13] + z[16] + -z[30] + -2 * z[37] + z[42] + z[84];
z[13] = abb[30] * z[13];
z[6] = -z[6] + z[11] + -z[25] + -z[29] + z[32] + -z[39] + z[41] + (T(1) / T(2)) * z[97];
z[6] = abb[33] * z[6];
z[6] = z[6] + z[13];
z[11] = abb[14] * z[59];
z[11] = z[11] + -3 * z[98] + -z[104];
z[13] = -abb[43] + 3 * z[64] + -z[68] + z[82];
z[13] = abb[7] * z[13];
z[3] = abb[51] * (T(5) / T(2)) + -z[3] + 3 * z[9] + z[20];
z[3] = z[3] * z[12];
z[9] = (T(-9) / T(4)) * z[99];
z[9] = z[9] * z[15];
z[3] = z[3] + z[9] + -z[11] + z[13] + z[40] + -z[96];
z[3] = abb[31] * z[3];
z[2] = z[2] + z[5] + z[28];
z[2] = abb[43] + -6 * abb[49] + abb[51] * (T(-13) / T(4)) + abb[45] * (T(19) / T(4)) + 3 * z[2];
z[2] = abb[7] * z[2];
z[5] = -abb[4] * z[31];
z[2] = z[2] + z[5] + z[11] + -z[35] + -z[103];
z[2] = abb[32] * z[2];
z[1] = z[1] + z[2] + z[3] + 3 * z[6] + z[33];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[40] * z[14];
z[3] = -z[4] + z[8] + -z[24];
z[4] = -z[3] + z[25] + -z[49];
z[4] = abb[36] * z[4];
z[5] = abb[45] * (T(3) / T(2)) + -z[46];
z[5] = abb[3] * z[5];
z[3] = -z[3] + z[5] + z[7] + -z[10];
z[3] = abb[38] * z[3];
z[3] = z[3] + z[4];
z[4] = -abb[41] * z[22];
z[5] = -abb[39] * z[27];
z[6] = -abb[37] * z[34];
z[1] = abb[42] + z[1] + z[2] + 3 * z[3] + z[4] + z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_608_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.5456475103687967511626615423861988683086098975062501467963607855"),stof<T>("-11.5033015991443592773318823824912092128823182683640941530054341614")}, std::complex<T>{stof<T>("3.402800050253254215055999418538193991107424795644827539591794253"),stof<T>("53.005038414406225442918449250355425377080589699495969496998979947")}, std::complex<T>{stof<T>("4.506327512280110241669155214187873596628702543494398012230264319"),stof<T>("37.078855698803254118411860117702453011443658874134524866902840008")}, std::complex<T>{stof<T>("1.701400025126627107527999709269096995553712397822413769795897126"),stof<T>("26.502519207203112721459224625177712688540294849747984748499489973")}, std::complex<T>{stof<T>("-9.802134862913920151869041703399913166617528186632045364256450569"),stof<T>("-21.181965232726981999077966400784476980680922130742873291916936564")}, std::complex<T>{stof<T>("-5.4515598653916402665652246560949376972339281434538109750257225907"),stof<T>("0.8976728580175186752065514742314725551047601620077609794918476323")}, std::complex<T>{stof<T>("8.1007348377872930443410419941308161710638157888096315944605534422"),stof<T>("-5.3205539744761307223812582243932357078593727190051114565825534096")}, std::complex<T>{stof<T>("-2.8049274871534831341411555049187766010749901456719842424343671925"),stof<T>("-10.5763364916001413969526354925247403229033640243865401184033500348")}, std::complex<T>{stof<T>("-3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("-14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[124].real()/kbase.W[124].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_608_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_608_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(8)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (-24 + -3 * v[0] + -5 * v[1] + v[2] + 5 * v[3] + 8 * v[4] + 4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 3 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * (-1 + 2 * m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (2 * abb[44] + abb[45] + abb[46] + abb[47] + -abb[49] + -abb[50]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[3];
z[0] = abb[32] + -abb[33];
z[1] = 2 * abb[44] + abb[45] + abb[46] + abb[47] + -abb[49] + -abb[50];
z[0] = z[0] * z[1];
z[2] = abb[30] * z[1];
z[0] = z[0] + z[2];
z[0] = abb[32] * z[0];
z[1] = abb[35] * z[1];
z[2] = -abb[33] * z[2];
z[0] = z[0] + z[1] + z[2];
return 3 * abb[10] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_608_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-3) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (-3 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (2 * abb[44] + abb[45] + abb[46] + abb[47] + -abb[49] + -abb[50]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -2 * abb[44] + -abb[45] + -abb[46] + -abb[47] + abb[49] + abb[50];
z[1] = abb[32] + -abb[33];
return 3 * abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_608_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("60.151579686058178482824320303096999919365554354263710139575613413"),stof<T>("31.89417336517168893789946242170821194401905998580203096991505474")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,65> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W17(k,dl), dlog_W22(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[124].real()/k.W[124].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}};
abb[42] = SpDLog_f_4_608_W_17_Im(t, path, abb);
abb[64] = SpDLog_f_4_608_W_17_Re(t, path, abb);

                    
            return f_4_608_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_608_DLogXconstant_part(base_point<T>, kend);
	value += f_4_608_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_608_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_608_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_608_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_608_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_608_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_608_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
