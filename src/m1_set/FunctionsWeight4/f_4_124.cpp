/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_124.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_124_abbreviated (const std::array<T,57>& abb) {
T z[77];
z[0] = abb[43] + -abb[44] + -abb[45] + -abb[46] + abb[47];
z[1] = (T(1) / T(4)) * z[0];
z[2] = abb[25] * z[1];
z[3] = abb[22] * z[0];
z[4] = (T(1) / T(4)) * z[3];
z[2] = z[2] + z[4];
z[1] = abb[23] * z[1];
z[5] = abb[53] * (T(1) / T(2));
z[6] = abb[19] * z[5];
z[7] = (T(1) / T(2)) * z[0];
z[8] = abb[24] * z[7];
z[9] = z[1] + z[2] + z[6] + z[8];
z[10] = abb[21] * abb[53];
z[11] = -abb[48] + abb[52];
z[12] = -abb[50] + z[11];
z[13] = abb[17] * z[12];
z[10] = z[10] + -z[13];
z[14] = abb[49] + abb[50];
z[15] = abb[51] + z[14];
z[16] = z[11] + -z[15];
z[16] = abb[13] * z[16];
z[17] = abb[18] * z[5];
z[18] = z[16] + z[17];
z[19] = abb[49] + abb[51];
z[20] = -abb[50] + z[19];
z[21] = abb[8] * (T(1) / T(2));
z[22] = z[20] * z[21];
z[23] = 5 * abb[50] + 3 * z[19];
z[23] = -z[11] + (T(1) / T(2)) * z[23];
z[24] = abb[5] * (T(1) / T(2));
z[25] = z[23] * z[24];
z[26] = abb[15] * z[15];
z[27] = (T(1) / T(4)) * z[26];
z[28] = abb[14] * z[15];
z[29] = (T(1) / T(4)) * z[15];
z[30] = abb[6] * z[29];
z[22] = z[9] + (T(1) / T(4)) * z[10] + z[18] + z[22] + z[25] + z[27] + -z[28] + z[30];
z[22] = abb[33] * z[22];
z[25] = (T(1) / T(2)) * z[10];
z[30] = abb[25] * (T(1) / T(2));
z[30] = z[0] * z[30];
z[31] = abb[22] * z[7];
z[31] = z[30] + z[31];
z[32] = z[25] + z[31];
z[33] = 3 * abb[50] + z[19];
z[33] = -z[11] + (T(1) / T(2)) * z[33];
z[33] = abb[6] * z[33];
z[34] = abb[23] * (T(1) / T(2));
z[34] = z[0] * z[34];
z[35] = (T(1) / T(2)) * z[15];
z[36] = abb[15] * z[35];
z[37] = z[34] + z[36];
z[38] = abb[5] * z[35];
z[39] = abb[24] * z[0];
z[40] = abb[8] * z[15];
z[41] = z[32] + -z[33] + z[37] + z[38] + z[39] + -z[40];
z[41] = abb[30] * z[41];
z[42] = abb[23] + abb[25];
z[42] = z[0] * z[42];
z[43] = abb[6] * z[15];
z[40] = z[40] + z[43];
z[43] = -z[26] + z[40];
z[44] = abb[5] * z[15];
z[45] = abb[7] * z[15];
z[44] = z[39] + z[42] + -z[43] + z[44] + -z[45];
z[46] = -abb[31] * z[44];
z[41] = z[41] + z[46];
z[46] = abb[7] * z[35];
z[30] = -z[30] + z[46];
z[47] = abb[4] * z[35];
z[48] = z[34] + z[47];
z[20] = z[20] * z[24];
z[24] = -abb[6] * z[35];
z[49] = -abb[8] * z[19];
z[20] = z[20] + z[24] + z[28] + -z[30] + z[48] + z[49];
z[20] = abb[32] * z[20];
z[24] = z[8] + z[31];
z[31] = abb[8] * z[35];
z[49] = -z[6] + -z[24] + z[31];
z[50] = z[18] + z[48];
z[51] = (T(-1) / T(2)) * z[11];
z[52] = abb[50] * (T(1) / T(2)) + z[51];
z[53] = z[19] + z[52];
z[53] = abb[5] * z[53];
z[54] = abb[6] * z[12];
z[13] = -z[13] + z[54];
z[5] = abb[21] * z[5];
z[5] = z[5] + (T(1) / T(2)) * z[13];
z[13] = z[5] + -z[49] + z[50] + z[53];
z[53] = -abb[34] * z[13];
z[20] = z[20] + z[22] + (T(1) / T(2)) * z[41] + z[53];
z[20] = m1_set::bc<T>[0] * z[20];
z[22] = -abb[41] * z[13];
z[41] = (T(1) / T(2)) * z[44];
z[44] = -abb[40] * z[41];
z[53] = abb[17] * abb[53];
z[12] = abb[21] * z[12];
z[12] = -z[12] + z[53];
z[53] = abb[6] * abb[53];
z[12] = (T(1) / T(2)) * z[12] + z[53];
z[53] = abb[18] + abb[19] + abb[20];
z[53] = z[29] * z[53];
z[55] = abb[28] * abb[53];
z[12] = (T(1) / T(2)) * z[12] + z[53] + -z[55];
z[53] = abb[27] * z[0];
z[55] = -z[12] + (T(1) / T(4)) * z[53];
z[55] = abb[42] * z[55];
z[56] = -abb[30] + -abb[33];
z[56] = z[0] * z[56];
z[57] = abb[34] * z[0];
z[56] = (T(1) / T(2)) * z[56] + z[57];
z[56] = m1_set::bc<T>[0] * z[56];
z[58] = abb[41] * z[0];
z[56] = z[56] + z[58];
z[56] = abb[26] * z[56];
z[20] = z[20] + z[22] + z[44] + z[55] + (T(1) / T(2)) * z[56];
z[20] = (T(1) / T(16)) * z[20];
z[22] = -abb[49] + abb[50] + 3 * abb[51];
z[22] = -z[11] + (T(1) / T(2)) * z[22];
z[44] = abb[5] + -abb[6];
z[22] = z[22] * z[44];
z[44] = abb[16] * z[15];
z[55] = abb[19] * abb[53];
z[56] = abb[49] + -abb[51] + z[11];
z[58] = abb[0] * z[56];
z[22] = z[22] + z[32] + -z[37] + z[44] + z[55] + -z[58];
z[22] = (T(1) / T(2)) * z[22];
z[32] = -abb[30] * z[22];
z[37] = abb[50] + abb[51];
z[44] = z[11] + -z[37];
z[59] = abb[2] * z[44];
z[22] = z[22] + z[59];
z[22] = abb[33] * z[22];
z[60] = abb[48] + z[14];
z[60] = abb[12] * z[60];
z[61] = -z[34] + z[60];
z[62] = abb[48] + z[35];
z[62] = abb[16] * z[62];
z[63] = abb[48] + abb[51];
z[63] = abb[9] * z[63];
z[43] = -z[8] + (T(1) / T(2)) * z[43] + z[61] + -z[62] + z[63];
z[43] = abb[31] * z[43];
z[44] = abb[6] * z[44];
z[44] = z[25] + z[44];
z[15] = abb[48] + z[15];
z[15] = abb[16] * z[15];
z[64] = (T(1) / T(2)) * z[58];
z[31] = z[15] + -z[31] + z[36] + z[44] + -z[64];
z[65] = abb[1] * z[14];
z[66] = (T(1) / T(2)) * z[65];
z[67] = -abb[51] + z[14];
z[68] = abb[3] * z[67];
z[69] = z[63] + z[66] + (T(1) / T(4)) * z[68];
z[56] = abb[5] * z[56];
z[56] = z[55] + -z[56];
z[70] = (T(1) / T(2)) * z[59];
z[71] = (T(1) / T(4)) * z[39];
z[31] = (T(1) / T(2)) * z[31] + (T(1) / T(4)) * z[56] + -z[69] + z[70] + z[71];
z[31] = abb[29] * z[31];
z[24] = z[24] + z[38] + z[63];
z[38] = (T(1) / T(2)) * z[67];
z[56] = abb[3] * z[38];
z[61] = -z[56] + z[61];
z[72] = z[24] + (T(-1) / T(2)) * z[40] + -z[61] + z[65];
z[73] = abb[32] * z[72];
z[52] = abb[51] + z[52];
z[74] = abb[5] * z[52];
z[44] = z[44] + -z[49] + z[74];
z[35] = abb[16] * z[35];
z[49] = -z[35] + z[64];
z[74] = z[49] + -z[59];
z[75] = -z[44] + z[74];
z[75] = abb[34] * z[75];
z[76] = abb[30] * z[59];
z[22] = z[22] + z[31] + z[32] + z[43] + z[73] + z[75] + -z[76];
z[22] = abb[29] * z[22];
z[31] = z[28] + -z[65];
z[32] = abb[8] * abb[51];
z[32] = -z[31] + z[32];
z[43] = abb[6] * abb[51];
z[73] = z[32] + z[43];
z[14] = abb[5] * z[14];
z[63] = -z[14] + z[15] + -z[60] + -z[63] + -z[73];
z[63] = abb[31] * z[63];
z[75] = -abb[49] + z[37];
z[21] = z[21] * z[75];
z[21] = z[21] + z[47] + -z[56];
z[47] = abb[5] * abb[49];
z[15] = -z[15] + z[21] + z[43] + -z[46] + z[47];
z[4] = z[4] + -z[60] + z[71];
z[15] = -z[4] + (T(1) / T(2)) * z[15];
z[15] = abb[32] * z[15];
z[43] = abb[5] * z[38];
z[30] = z[30] + -z[35] + z[43] + z[73];
z[43] = abb[30] * z[30];
z[15] = z[15] + z[43] + z[63];
z[15] = abb[32] * z[15];
z[30] = abb[31] * z[30];
z[30] = (T(1) / T(2)) * z[30] + -z[43];
z[30] = abb[31] * z[30];
z[41] = abb[54] * z[41];
z[13] = abb[55] * z[13];
z[10] = z[10] + z[54] + -z[58];
z[43] = abb[49] + abb[52];
z[43] = abb[10] * z[43];
z[10] = (T(1) / T(2)) * z[10] + -z[17] + z[43] + z[61] + -z[62];
z[10] = abb[35] * z[10];
z[17] = -abb[48] + -z[37];
z[17] = abb[11] * z[17];
z[17] = z[17] + z[21] + -z[24] + z[62];
z[17] = abb[38] * z[17];
z[21] = abb[50] + (T(1) / T(2)) * z[19] + z[51];
z[24] = abb[5] * z[21];
z[6] = z[6] + z[24] + z[32] + -z[64];
z[6] = (T(1) / T(2)) * z[6] + z[70];
z[6] = prod_pow(abb[30], 2) * z[6];
z[24] = abb[39] * z[72];
z[12] = abb[56] * z[12];
z[6] = z[6] + z[10] + z[12] + z[13] + z[15] + z[17] + z[22] + z[24] + z[30] + z[41];
z[10] = -abb[5] * z[23];
z[12] = abb[8] * z[67];
z[10] = z[10] + z[12] + -z[25] + z[33] + -z[36] + z[58];
z[9] = -z[9] + (T(1) / T(2)) * z[10] + z[31];
z[9] = abb[30] * z[9];
z[10] = -abb[6] + -abb[8];
z[10] = z[10] * z[67];
z[10] = z[10] + z[26];
z[8] = z[8] + (T(1) / T(2)) * z[10] + z[14] + -z[31] + z[34] + -z[35];
z[8] = abb[31] * z[8];
z[10] = abb[8] * abb[49];
z[10] = z[10] + -z[65];
z[12] = abb[6] * z[38];
z[12] = z[10] + z[12] + z[35] + -z[47] + -z[48];
z[12] = abb[32] * z[12];
z[13] = -abb[6] * z[21];
z[10] = -z[10] + z[13] + -z[18] + z[59] + -z[64];
z[13] = abb[33] * (T(1) / T(2));
z[10] = z[10] * z[13];
z[8] = z[8] + z[9] + z[10] + z[12] + -z[76];
z[8] = z[8] * z[13];
z[9] = -abb[50] + abb[51] * (T(1) / T(2));
z[9] = abb[8] * z[9];
z[5] = z[5] + z[9] + z[16] + z[36] + z[39] + (T(-1) / T(4)) * z[58];
z[9] = abb[18] * abb[53];
z[3] = z[3] + z[9] + z[42] + z[59] + z[65];
z[9] = abb[50] + (T(5) / T(8)) * z[19];
z[9] = (T(1) / T(3)) * z[9] + (T(-1) / T(8)) * z[11];
z[9] = abb[5] * z[9];
z[3] = (T(1) / T(12)) * z[3] + (T(1) / T(6)) * z[5] + z[9] + (T(-1) / T(4)) * z[28] + (T(1) / T(8)) * z[55];
z[5] = prod_pow(m1_set::bc<T>[0], 2);
z[3] = z[3] * z[5];
z[9] = -abb[5] * z[29];
z[2] = -z[2] + z[9] + z[27] + (T(1) / T(2)) * z[62] + -z[69];
z[2] = abb[36] * z[2];
z[9] = abb[6] * z[52];
z[9] = z[9] + z[47] + z[50] + z[74];
z[10] = abb[34] * (T(1) / T(2));
z[11] = abb[33] + -z[10];
z[9] = z[9] * z[11];
z[11] = z[44] + -z[49];
z[11] = abb[30] * z[11];
z[9] = z[9] + z[11] + z[76];
z[9] = z[9] * z[10];
z[10] = z[40] + -z[45] + -z[68];
z[10] = (T(1) / T(2)) * z[10] + -z[62];
z[1] = -z[1] + -z[4] + (T(1) / T(2)) * z[10] + -z[66];
z[1] = abb[37] * z[1];
z[4] = abb[30] + -abb[33];
z[4] = z[0] * z[4];
z[10] = -abb[29] * z[7];
z[4] = (T(1) / T(2)) * z[4] + z[10] + z[57];
z[4] = abb[29] * z[4];
z[10] = -abb[30] * abb[34];
z[5] = -abb[35] + -abb[55] + (T(-1) / T(3)) * z[5] + z[10];
z[0] = z[0] * z[5];
z[5] = abb[30] * abb[33] * z[7];
z[0] = z[0] + z[4] + z[5];
z[0] = abb[26] * z[0];
z[4] = -abb[56] * z[53];
z[0] = (T(1) / T(4)) * z[0] + z[1] + z[2] + z[3] + (T(1) / T(8)) * z[4] + (T(1) / T(2)) * z[6] + z[8] + z[9];
z[0] = (T(1) / T(8)) * z[0];
return {z[20], z[0]};
}


template <typename T> std::complex<T> f_4_124_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.006717931454922734126621924607293166975331558412816135896262661779"),stof<T>("0.126232960322030635352057627960412692271023448377990484355604991324")}, std::complex<T>{stof<T>("-0.004685267124466033269924154686551727964680509959508663575432080289"),stof<T>("0.102089612120166519864396681937988028470749117128770402672924081893")}, std::complex<T>{stof<T>("-0.10008051447798893754928177361011678232881992032254754429695053896"),stof<T>("0.15667203808942366193361105525774746104804654211009050740095937023")}, std::complex<T>{stof<T>("-0.013600268703435774762213202963954716428020795971299478338851509062"),stof<T>("0.102089612120166519864396681937988028470749117128770402672924081893")}, std::complex<T>{stof<T>("0.026259555863268533604775457023865736020838253946328496338764480911"),stof<T>("-0.126232960322030635352057627960412692271023448377990484355604991324")}, std::complex<T>{stof<T>("0.05308425981936598198548838172921953686782368677692209810094997181"),stof<T>("-0.076949906797383748750224446478305787075727995940786863171375303992")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_124_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_124_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.0338312628989066314987710296439447261305518220647766021407303074"),stof<T>("0.038631280347612859422852000582678734739397174701595147393936191928")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_124_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_124_DLogXconstant_part(base_point<T>, kend);
	value += f_4_124_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_124_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_124_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_124_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_124_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_124_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_124_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
