/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_154.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_154_abbreviated (const std::array<T,60>& abb) {
T z[119];
z[0] = abb[42] * (T(1) / T(2));
z[1] = 3 * abb[49];
z[2] = abb[46] + abb[50];
z[3] = abb[47] + z[2];
z[4] = abb[45] * (T(1) / T(2));
z[5] = -5 * abb[51] + z[0] + -z[1] + (T(5) / T(2)) * z[3] + z[4];
z[5] = abb[9] * z[5];
z[6] = abb[44] + abb[49];
z[7] = abb[46] + -abb[50];
z[8] = abb[43] + abb[45];
z[9] = -z[6] + z[7] + z[8];
z[9] = abb[42] + (T(1) / T(2)) * z[9];
z[10] = abb[3] * z[9];
z[11] = -abb[52] + abb[53] + -abb[54] + abb[55];
z[12] = abb[20] * z[11];
z[13] = abb[21] * z[11];
z[14] = z[10] + z[12] + (T(5) / T(2)) * z[13];
z[15] = -abb[43] + z[2];
z[15] = -abb[47] + (T(1) / T(2)) * z[15];
z[16] = abb[44] * (T(1) / T(2));
z[17] = -z[15] + z[16];
z[18] = abb[49] * (T(1) / T(2));
z[19] = -z[4] + z[18];
z[20] = -abb[42] + z[17] + z[19];
z[21] = abb[14] * z[20];
z[22] = (T(1) / T(2)) * z[11];
z[23] = abb[25] * z[22];
z[21] = -z[21] + z[23];
z[23] = abb[24] * z[22];
z[24] = abb[22] * z[22];
z[25] = z[23] + z[24];
z[26] = z[21] + z[25];
z[27] = abb[45] * (T(3) / T(2));
z[28] = abb[49] * (T(3) / T(2));
z[29] = z[27] + -z[28];
z[30] = 5 * abb[46];
z[31] = abb[43] + -abb[50];
z[32] = z[30] + 3 * z[31];
z[32] = -abb[47] + abb[44] * (T(-23) / T(6)) + abb[42] * (T(7) / T(3)) + -z[29] + (T(1) / T(2)) * z[32];
z[33] = abb[41] + -abb[48];
z[32] = (T(1) / T(2)) * z[32] + (T(-2) / T(3)) * z[33];
z[32] = abb[7] * z[32];
z[34] = abb[46] + z[31];
z[35] = 3 * abb[45];
z[36] = 3 * z[34] + -z[35];
z[37] = abb[44] * (T(1) / T(3));
z[38] = -z[1] + -z[36] + -z[37];
z[39] = -abb[42] + z[33];
z[38] = (T(1) / T(2)) * z[38] + (T(-5) / T(3)) * z[39];
z[40] = abb[4] * (T(1) / T(2));
z[38] = z[38] * z[40];
z[41] = abb[45] + abb[48];
z[42] = abb[43] * (T(1) / T(3));
z[43] = abb[46] + z[42];
z[37] = -z[37] + (T(-1) / T(3)) * z[41] + z[43];
z[37] = abb[51] * (T(-1) / T(3)) + abb[41] * (T(1) / T(6)) + (T(1) / T(2)) * z[37];
z[37] = abb[10] * z[37];
z[41] = abb[42] + abb[45];
z[44] = abb[49] * (T(5) / T(2));
z[45] = -abb[50] + abb[47] * (T(-9) / T(2)) + abb[43] * (T(-5) / T(6)) + abb[51] * (T(11) / T(3)) + (T(11) / T(6)) * z[33] + (T(-2) / T(3)) * z[41] + z[44];
z[45] = abb[0] * z[45];
z[46] = 3 * abb[47];
z[42] = -abb[50] + -z[42] + -z[46];
z[47] = abb[51] * (T(4) / T(3)) + abb[45] * (T(7) / T(6)) + -z[18];
z[48] = abb[44] * (T(2) / T(3));
z[42] = abb[42] * (T(11) / T(6)) + (T(1) / T(2)) * z[42] + z[47] + -z[48];
z[42] = abb[2] * z[42];
z[49] = 3 * abb[44];
z[50] = z[35] + z[49];
z[51] = abb[43] + abb[49];
z[30] = abb[50] + z[30] + -z[50] + z[51];
z[52] = -abb[51] + (T(1) / T(4)) * z[30];
z[53] = abb[8] * z[52];
z[54] = abb[44] + abb[45];
z[55] = -abb[49] + z[54];
z[34] = z[34] + z[55];
z[34] = (T(1) / T(2)) * z[34];
z[56] = -abb[48] + z[34];
z[57] = abb[41] + z[56];
z[57] = abb[13] * z[57];
z[58] = z[33] + z[55];
z[59] = abb[1] * z[58];
z[43] = z[43] + -z[48];
z[43] = abb[42] * (T(1) / T(6)) + 2 * z[43] + -z[47];
z[43] = abb[6] * z[43];
z[47] = -abb[46] + abb[47] + z[55];
z[47] = abb[11] * z[47];
z[48] = 3 * z[47];
z[55] = abb[23] * z[11];
z[5] = z[5] + (T(-1) / T(2)) * z[14] + -z[26] + z[32] + 11 * z[37] + z[38] + z[42] + z[43] + z[45] + -z[48] + -5 * z[53] + (T(-5) / T(4)) * z[55] + -z[57] + (T(7) / T(2)) * z[59];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[14] = 3 * abb[50];
z[32] = 3 * abb[46];
z[37] = z[14] + z[32];
z[38] = 5 * abb[49];
z[42] = -z[37] + z[38];
z[43] = abb[43] + abb[44];
z[45] = -z[35] + z[42] + z[43];
z[53] = -abb[42] + abb[51] + (T(1) / T(4)) * z[45];
z[60] = abb[4] * z[53];
z[17] = z[17] + -z[19];
z[61] = abb[5] * z[17];
z[62] = (T(3) / T(2)) * z[61];
z[60] = z[60] + -z[62];
z[63] = -z[37] + z[51] + z[54];
z[64] = 2 * abb[51];
z[65] = (T(1) / T(2)) * z[63] + z[64];
z[66] = abb[15] * z[65];
z[48] = z[48] + z[66];
z[67] = -abb[7] * z[52];
z[68] = -abb[49] + z[3];
z[68] = -abb[51] + (T(1) / T(2)) * z[68];
z[68] = abb[9] * z[68];
z[69] = (T(1) / T(4)) * z[11];
z[70] = abb[22] * z[69];
z[71] = z[68] + -z[70];
z[72] = abb[24] * z[69];
z[67] = z[67] + -z[71] + z[72];
z[73] = z[46] + -z[64];
z[74] = 2 * abb[49];
z[75] = -z[73] + z[74];
z[76] = -abb[43] + -z[54] + z[75];
z[76] = 2 * z[76];
z[77] = abb[12] * z[76];
z[78] = 9 * abb[45] + -7 * abb[49] + -z[37] + z[43];
z[79] = 2 * abb[42];
z[78] = abb[51] + (T(1) / T(4)) * z[78] + z[79];
z[78] = abb[6] * z[78];
z[73] = -abb[49] + z[73];
z[80] = abb[42] + -z[43] + -z[73];
z[80] = abb[2] * z[80];
z[67] = -z[48] + z[60] + 3 * z[67] + -z[77] + z[78] + z[80];
z[67] = abb[28] * z[67];
z[78] = 3 * z[61];
z[80] = 4 * abb[51];
z[81] = z[63] + z[80];
z[81] = abb[15] * z[81];
z[82] = z[78] + z[81];
z[83] = abb[49] + z[64];
z[84] = z[3] + -z[83];
z[84] = abb[9] * z[84];
z[85] = -z[25] + z[84];
z[86] = (T(1) / T(2)) * z[13];
z[87] = z[10] + z[86];
z[88] = z[85] + -z[87];
z[89] = 5 * abb[43];
z[90] = z[37] + -z[89];
z[91] = abb[44] * (T(5) / T(2));
z[92] = abb[45] * (T(5) / T(2));
z[90] = -6 * abb[47] + abb[49] * (T(7) / T(2)) + z[64] + (T(1) / T(2)) * z[90] + -z[91] + -z[92];
z[90] = abb[12] * z[90];
z[45] = (T(1) / T(2)) * z[45] + z[64] + -z[79];
z[93] = -abb[4] + -abb[6];
z[45] = z[45] * z[93];
z[93] = 2 * abb[43] + -z[14];
z[94] = z[83] + z[93];
z[95] = abb[42] + -abb[44] + z[94];
z[95] = abb[2] * z[95];
z[45] = z[45] + z[82] + 3 * z[88] + z[90] + z[95];
z[45] = abb[30] * z[45];
z[88] = abb[7] * z[65];
z[90] = abb[12] * z[65];
z[96] = 2 * abb[4];
z[97] = 2 * abb[6];
z[98] = z[96] + z[97];
z[99] = -abb[49] + z[41];
z[98] = z[98] * z[99];
z[95] = 3 * z[87] + -z[88] + z[90] + -z[95] + -z[98];
z[98] = abb[31] * z[95];
z[100] = abb[6] * z[65];
z[88] = z[88] + z[100];
z[65] = abb[4] * z[65];
z[82] = z[65] + -z[82] + -3 * z[85] + z[88];
z[77] = -z[77] + z[82];
z[77] = abb[27] * z[77];
z[101] = (T(1) / T(2)) * z[30] + -z[64];
z[102] = abb[6] + abb[7];
z[103] = z[101] * z[102];
z[104] = abb[24] * z[11];
z[104] = z[55] + z[104];
z[24] = -z[24] + z[84] + z[103] + (T(-1) / T(2)) * z[104];
z[103] = abb[43] + z[2];
z[104] = 2 * abb[47];
z[105] = z[16] + -z[64] + (T(1) / T(2)) * z[103] + z[104];
z[28] = z[4] + -z[28] + z[105];
z[106] = abb[12] * z[28];
z[106] = -z[61] + z[106];
z[101] = abb[8] * z[101];
z[107] = z[24] + -z[101] + -z[106];
z[108] = abb[29] * z[107];
z[109] = z[81] + -z[90];
z[65] = -z[65] + z[109];
z[110] = 2 * abb[45] + -z[51] + z[64];
z[111] = 2 * abb[44];
z[112] = -z[32] + z[110] + z[111];
z[113] = 2 * abb[7];
z[114] = z[112] * z[113];
z[97] = z[97] * z[112];
z[114] = z[65] + z[97] + z[114];
z[101] = 3 * z[101];
z[115] = (T(3) / T(2)) * z[55] + z[101] + z[114];
z[116] = abb[32] * z[115];
z[45] = z[45] + z[67] + z[77] + z[98] + 3 * z[108] + z[116];
z[45] = abb[28] * z[45];
z[67] = abb[43] + z[32] + z[33] + -z[54] + -z[64];
z[67] = abb[10] * z[67];
z[52] = -z[52] * z[102];
z[77] = z[23] + z[55];
z[52] = z[52] + -z[71] + (T(1) / T(2)) * z[77];
z[62] = z[59] + z[62];
z[42] = -abb[43] + -z[42] + z[50];
z[42] = -abb[51] + z[33] + (T(1) / T(4)) * z[42];
z[42] = abb[4] * z[42];
z[50] = abb[43] + -z[33] + z[73];
z[50] = abb[0] * z[50];
z[42] = z[42] + z[48] + z[50] + 3 * z[52] + -z[62] + -z[67] + -z[90] + z[101];
z[42] = abb[29] * z[42];
z[48] = z[33] + z[94];
z[48] = abb[0] * z[48];
z[48] = z[48] + 4 * z[59] + z[67];
z[12] = z[12] + z[13];
z[12] = (T(1) / T(2)) * z[12] + z[57];
z[50] = z[58] * z[96];
z[50] = -3 * z[12] + z[48] + z[50] + z[88] + -z[90];
z[52] = abb[31] * z[50];
z[42] = z[42] + z[52];
z[42] = abb[29] * z[42];
z[52] = abb[41] * (T(1) / T(2));
z[58] = abb[48] * (T(1) / T(2)) + -z[52];
z[71] = z[58] + z[79];
z[73] = -abb[44] + abb[45];
z[88] = abb[43] + z[46];
z[88] = -abb[51] + z[18] + -z[71] + -z[73] + (T(1) / T(2)) * z[88];
z[88] = abb[7] * z[88];
z[94] = -abb[50] + z[33];
z[0] = abb[51] + -z[0] + z[4] + z[43] + (T(3) / T(2)) * z[94];
z[0] = abb[4] * z[0];
z[4] = abb[20] * z[22];
z[4] = z[4] + z[57];
z[57] = -z[4] + z[21];
z[57] = (T(1) / T(2)) * z[57] + -z[68];
z[63] = abb[51] + (T(1) / T(4)) * z[63];
z[63] = abb[6] * z[63];
z[68] = -z[46] + z[110];
z[96] = 3 * abb[42] + -abb[44] + z[68];
z[98] = abb[2] * z[96];
z[108] = abb[47] + z[31];
z[110] = 3 * z[108];
z[116] = z[99] + z[110];
z[117] = abb[0] * (T(1) / T(2));
z[116] = z[116] * z[117];
z[0] = z[0] + 3 * z[57] + z[62] + z[63] + -z[66] + z[88] + -z[98] + z[116];
z[0] = abb[27] * z[0];
z[57] = abb[43] + z[37];
z[62] = abb[44] * (T(3) / T(2));
z[57] = z[18] + z[46] + (T(1) / T(2)) * z[57] + z[62] + -z[92];
z[63] = (T(1) / T(2)) * z[57] + -z[64] + -z[71];
z[63] = abb[7] * z[63];
z[13] = z[4] + z[13];
z[88] = z[13] + z[26];
z[43] = -z[7] + -z[43];
z[92] = 5 * abb[45];
z[43] = z[38] + 3 * z[43] + -z[92];
z[43] = -abb[42] + -3 * z[33] + (T(1) / T(2)) * z[43];
z[43] = z[40] * z[43];
z[116] = 2 * z[59];
z[118] = z[99] + -z[110];
z[117] = z[117] * z[118];
z[43] = z[43] + z[63] + (T(3) / T(2)) * z[88] + z[90] + -z[98] + -z[116] + z[117];
z[43] = abb[31] * z[43];
z[50] = -abb[29] * z[50];
z[63] = abb[42] + -abb[47];
z[88] = abb[45] + z[63];
z[2] = -z[2] + z[64] + z[88];
z[2] = abb[9] * z[2];
z[117] = z[2] + z[26] + z[86] + -z[98];
z[96] = z[96] * z[113];
z[65] = -z[65] + -z[96] + z[100] + 3 * z[117];
z[65] = abb[30] * z[65];
z[0] = z[0] + z[43] + z[50] + -z[65];
z[0] = abb[27] * z[0];
z[43] = z[67] + z[116];
z[50] = 3 * z[15] + z[29] + -z[91];
z[50] = (T(1) / T(2)) * z[50] + z[71];
z[50] = abb[7] * z[50];
z[4] = z[4] + -z[26];
z[71] = z[14] + z[46];
z[91] = z[41] + z[51] + -z[71];
z[96] = z[33] + z[64];
z[91] = (T(1) / T(2)) * z[91] + z[96];
z[91] = abb[0] * z[91];
z[1] = 5 * abb[44] + -z[1];
z[36] = -z[1] + z[36];
z[36] = (T(1) / T(2)) * z[36] + -z[39];
z[36] = z[36] * z[40];
z[36] = z[36] + -z[91];
z[4] = (T(3) / T(2)) * z[4] + z[36] + -z[43] + z[50] + z[98] + -z[100];
z[4] = abb[31] * z[4];
z[4] = z[4] + z[65];
z[43] = z[43] + 2 * z[98];
z[50] = z[43] + z[109];
z[2] = z[2] + z[70];
z[13] = z[13] + z[21];
z[23] = -z[13] + -z[23];
z[23] = -z[2] + (T(1) / T(2)) * z[23];
z[65] = -z[37] + -z[89];
z[44] = -9 * abb[47] + abb[45] * (T(13) / T(2)) + -z[44] + -z[62] + (T(1) / T(2)) * z[65];
z[62] = 4 * abb[42];
z[58] = -z[58] + z[62];
z[44] = (T(1) / T(2)) * z[44] + z[58] + z[80];
z[44] = abb[7] * z[44];
z[38] = z[38] + z[89];
z[32] = abb[45] + 9 * abb[50] + z[32] + -z[38] + z[49];
z[32] = -abb[42] + -abb[48] + (T(1) / T(2)) * z[32];
z[32] = (T(1) / T(2)) * z[32] + z[52] + -z[64];
z[32] = abb[4] * z[32];
z[23] = 3 * z[23] + z[32] + z[44] + z[50] + z[91];
z[23] = abb[27] * z[23];
z[32] = abb[6] * z[112];
z[44] = z[33] + z[68] + z[79];
z[44] = abb[0] * z[44];
z[49] = -abb[4] + z[113];
z[52] = abb[44] + z[39];
z[49] = z[49] * z[52];
z[32] = z[32] + z[44] + z[49] + -z[59] + z[67] + -z[98];
z[32] = abb[32] * z[32];
z[44] = -abb[29] * z[115];
z[23] = z[4] + z[23] + z[32] + z[44];
z[23] = abb[32] * z[23];
z[32] = -abb[7] * z[20];
z[44] = -abb[6] * z[9];
z[7] = z[7] + z[51];
z[49] = z[7] + -z[54];
z[40] = z[40] * z[49];
z[51] = abb[50] + -z[51] + z[88];
z[51] = abb[2] * z[51];
z[10] = z[10] + -z[26] + z[32] + z[40] + z[44] + z[51];
z[10] = abb[34] * z[10];
z[25] = z[25] + -z[61];
z[9] = -abb[4] * z[9];
z[26] = abb[12] * z[17];
z[32] = abb[6] * z[49];
z[40] = -abb[2] * z[108];
z[9] = z[9] + z[25] + z[26] + (T(1) / T(2)) * z[32] + z[40] + z[87];
z[9] = abb[35] * z[9];
z[18] = -z[18] + z[27] + z[79] + -z[105];
z[18] = abb[7] * z[18];
z[21] = z[21] + z[86];
z[26] = abb[9] * z[99];
z[18] = z[18] + -z[21] + -z[26] + z[98] + z[106];
z[26] = abb[56] * z[18];
z[24] = z[24] + z[61];
z[24] = abb[57] * z[24];
z[27] = z[33] + z[34];
z[27] = abb[4] * z[27];
z[25] = -z[12] + -z[25] + z[27];
z[25] = abb[33] * z[25];
z[9] = z[9] + z[10] + z[24] + z[25] + z[26];
z[1] = -abb[43] + -z[1] + z[37] + -z[92];
z[1] = (T(1) / T(2)) * z[1] + -2 * z[33] + -z[64];
z[1] = abb[4] * z[1];
z[10] = abb[23] * z[22];
z[10] = z[10] + z[12];
z[14] = -9 * abb[46] + z[14] + -z[38] + 7 * z[54];
z[14] = (T(1) / T(2)) * z[14] + z[64];
z[14] = z[14] * z[102];
z[1] = z[1] + 3 * z[10] + z[14] + -z[48] + z[81] + z[101];
z[10] = -abb[58] * z[1];
z[3] = -abb[49] + -z[3];
z[3] = abb[51] + (T(1) / T(2)) * z[3] + z[41];
z[3] = abb[9] * z[3];
z[6] = z[6] + z[103];
z[6] = -abb[51] + abb[45] * (T(-3) / T(4)) + (T(1) / T(4)) * z[6] + -z[63];
z[6] = abb[7] * z[6];
z[3] = z[3] + z[6] + z[70] + z[72] + z[87];
z[6] = -5 * abb[42] + -abb[43] + abb[49] + -z[35] + z[71] + -z[80] + z[111];
z[6] = abb[2] * z[6];
z[14] = abb[6] * z[53];
z[3] = 3 * z[3] + z[6] + z[14] + z[60] + -z[66] + z[90];
z[3] = prod_pow(abb[30], 2) * z[3];
z[6] = abb[36] * z[82];
z[14] = -abb[30] * z[95];
z[22] = -abb[0] + abb[2] + -abb[6];
z[22] = z[22] * z[99];
z[24] = -abb[7] * z[52];
z[22] = z[22] + z[24] + z[59];
z[22] = abb[31] * z[22];
z[14] = z[14] + z[22];
z[14] = abb[31] * z[14];
z[15] = -abb[48] + z[15] + z[16] + -z[19];
z[15] = abb[16] * z[15];
z[16] = abb[19] * z[20];
z[7] = -z[7] + z[73];
z[7] = (T(1) / T(2)) * z[7] + -z[39];
z[7] = abb[17] * z[7];
z[19] = abb[18] * z[56];
z[20] = abb[16] + abb[18];
z[20] = abb[41] * z[20];
z[7] = -z[7] + z[15] + -z[16] + z[19] + z[20];
z[11] = abb[26] * z[11];
z[11] = (T(-3) / T(2)) * z[7] + (T(-3) / T(4)) * z[11];
z[11] = abb[59] * z[11];
z[15] = -abb[57] * z[28];
z[16] = -abb[33] * z[17];
z[15] = z[15] + z[16];
z[16] = -abb[36] * z[76];
z[15] = 3 * z[15] + z[16];
z[15] = abb[12] * z[15];
z[16] = -abb[57] * z[101];
z[17] = abb[0] * z[110];
z[17] = z[17] + 3 * z[59];
z[17] = abb[33] * z[17];
z[0] = z[0] + z[3] + z[5] + z[6] + 3 * z[9] + z[10] + z[11] + z[14] + z[15] + z[16] + z[17] + z[23] + z[42] + z[45];
z[3] = -z[33] + -z[57] + z[62] + z[80];
z[3] = abb[7] * z[3];
z[5] = z[41] + -z[74] + -z[93] + -z[96];
z[5] = abb[4] * z[5];
z[6] = -z[21] + z[84];
z[8] = -z[8] + z[39] + z[75];
z[8] = abb[0] * z[8];
z[3] = z[3] + z[5] + 3 * z[6] + z[8] + z[50] + -z[78];
z[3] = abb[27] * z[3];
z[5] = z[13] + z[77];
z[2] = z[2] + (T(1) / T(2)) * z[5];
z[5] = -7 * abb[46] + -z[31];
z[5] = (T(1) / T(2)) * z[5] + z[46];
z[5] = abb[44] * (T(19) / T(2)) + 3 * z[5] + z[29];
z[5] = (T(1) / T(2)) * z[5] + -z[58];
z[5] = abb[7] * z[5];
z[2] = 3 * z[2] + z[5] + z[36] + -z[43] + z[97] + z[101];
z[2] = abb[32] * z[2];
z[5] = z[83] + z[94] + -z[104];
z[5] = abb[0] * z[5];
z[6] = -z[30] + z[80];
z[6] = abb[8] * z[6];
z[5] = z[5] + z[6] + -z[12] + -z[55] + z[67] + z[85];
z[6] = z[47] + -z[59];
z[5] = 3 * z[5] + -6 * z[6] + z[78] + -z[114];
z[5] = abb[29] * z[5];
z[6] = abb[28] * z[115];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6];
z[2] = m1_set::bc<T>[0] * z[2];
z[1] = -abb[39] * z[1];
z[3] = -abb[26] * z[69];
z[3] = z[3] + (T(-1) / T(2)) * z[7];
z[3] = abb[40] * z[3];
z[4] = abb[38] * z[107];
z[5] = abb[37] * z[18];
z[3] = z[3] + z[4] + z[5];
z[1] = z[1] + z[2] + 3 * z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_154_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("-60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-15.254538031344221882234898815395995301210979956066736334501994495"),stof<T>("4.964525490228577077160159552622320638799579908595529274791218172")}, std::complex<T>{stof<T>("1.9230263948588290453996676827664011057810529065392366644334176496"),stof<T>("9.307535070333749016582503752162411395015383528204727111045930336")}, std::complex<T>{stof<T>("23.667023644882242041093684061750462195567611322144569253113963371"),stof<T>("-11.16998045677481054764776071189320617079041358990336901583320923")}, std::complex<T>{stof<T>("8.4124856135380201588587852463544668943566313660778329186119688761"),stof<T>("-6.2054549665462334704876011592708855319908336813078397410419910577")}, std::complex<T>{stof<T>("-5.531856137578063120391541743576013059963026160707794451212596247"),stof<T>("-33.915119523662488760840652524024513749197737097890297224342025657")}, std::complex<T>{stof<T>("6.449632563695912377695446426483077494809823066757646632807656336"),stof<T>("73.736174996707927462469030334058439387540189727061098078939252239")}, std::complex<T>{stof<T>("-18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-7.9098606291675302648109037464247985588895033658331406771927900959"),stof<T>("-9.0513052348096113720353358696648715896727008691751971307336570652")}, std::complex<T>{stof<T>("-2.4256513792293189394475491826960694412481809067839289058525964298"),stof<T>("5.949225131022095825940433276773345726648151022278309760729717787")}, std::complex<T>{stof<T>("1.00524996874097978809576299985933667093425600048938448283835756"),stof<T>("-30.513520402711689685045874057871514243327069100966073743551296246")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[90].real()/kbase.W[90].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_154_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_154_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-47.823770164394209598422715464646930589120511918748693474957142454"),stof<T>("32.905444792205210045668180505886577453864605846940315980172814194")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W91(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[90].real()/k.W[90].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k)};

                    
            return f_4_154_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_154_DLogXconstant_part(base_point<T>, kend);
	value += f_4_154_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_154_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_154_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_154_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_154_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_154_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_154_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
