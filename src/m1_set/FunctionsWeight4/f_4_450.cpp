/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_450.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_450_abbreviated (const std::array<T,62>& abb) {
T z[87];
z[0] = abb[50] + abb[52];
z[1] = -abb[53] + z[0];
z[2] = (T(1) / T(2)) * z[1];
z[3] = abb[16] * z[2];
z[4] = abb[50] + abb[53];
z[5] = abb[0] * z[4];
z[6] = z[3] + z[5];
z[7] = abb[44] + -abb[45] + -abb[46] + -abb[47] + abb[48];
z[8] = (T(1) / T(2)) * z[7];
z[9] = abb[26] * z[8];
z[10] = z[6] + z[9];
z[11] = abb[50] + abb[54];
z[11] = abb[10] * z[11];
z[12] = abb[52] + z[4];
z[13] = (T(1) / T(2)) * z[12];
z[14] = abb[14] * z[13];
z[15] = z[11] + z[14];
z[16] = z[10] + -z[15];
z[17] = abb[49] + abb[51];
z[18] = z[4] + z[17];
z[18] = abb[9] * z[18];
z[19] = abb[17] * abb[55];
z[20] = abb[49] + abb[50];
z[21] = abb[1] * z[20];
z[19] = (T(1) / T(2)) * z[19] + z[21];
z[22] = -abb[52] + z[4];
z[23] = abb[5] * z[22];
z[24] = (T(1) / T(2)) * z[23];
z[25] = abb[52] + abb[53];
z[26] = abb[51] + z[25];
z[27] = abb[13] * z[26];
z[28] = z[24] + z[27];
z[29] = z[0] + z[17];
z[30] = abb[7] * (T(1) / T(2));
z[31] = z[29] * z[30];
z[32] = abb[4] * (T(1) / T(2));
z[33] = -abb[53] + -z[17];
z[33] = z[32] * z[33];
z[34] = abb[0] + z[30] + -z[32];
z[35] = -abb[54] * z[34];
z[36] = abb[18] * abb[55];
z[37] = (T(1) / T(2)) * z[36];
z[31] = -z[16] + z[18] + -z[19] + -z[28] + z[31] + z[33] + z[35] + -z[37];
z[31] = abb[28] * z[31];
z[33] = abb[23] + abb[25];
z[35] = z[8] * z[33];
z[38] = abb[6] * z[13];
z[39] = z[35] + z[38];
z[40] = abb[51] + z[13];
z[41] = abb[4] * z[40];
z[41] = -z[39] + z[41];
z[42] = -abb[0] + abb[4];
z[43] = abb[54] * z[42];
z[16] = z[16] + -z[41] + -z[43];
z[44] = abb[31] * z[16];
z[45] = abb[51] + abb[52];
z[45] = abb[1] * z[45];
z[46] = abb[8] * z[2];
z[47] = z[45] + -z[46];
z[48] = -z[27] + z[47];
z[49] = abb[4] * z[13];
z[50] = z[14] + z[48] + -z[49];
z[51] = abb[29] * z[50];
z[31] = z[31] + z[44] + z[51];
z[36] = (T(1) / T(4)) * z[36];
z[43] = z[19] + z[36] + (T(3) / T(4)) * z[43];
z[51] = (T(1) / T(4)) * z[7];
z[52] = z[33] * z[51];
z[53] = abb[49] * (T(1) / T(4));
z[54] = abb[50] + abb[51] * (T(1) / T(4)) + abb[53] * (T(3) / T(4)) + z[53];
z[54] = abb[0] * z[54];
z[55] = abb[26] * z[51];
z[54] = z[52] + z[54] + z[55];
z[56] = z[11] + z[27];
z[51] = abb[21] * z[51];
z[57] = abb[15] * z[12];
z[51] = z[51] + (T(1) / T(4)) * z[57];
z[58] = 3 * abb[52];
z[59] = 3 * abb[50];
z[60] = -abb[53] + -z[58] + -z[59];
z[60] = -z[17] + (T(1) / T(2)) * z[60];
z[60] = abb[7] * z[60];
z[3] = -z[3] + z[60];
z[60] = abb[53] + abb[52] * (T(1) / T(2)) + abb[51] * (T(3) / T(2));
z[61] = abb[49] * (T(1) / T(2));
z[62] = abb[50] * (T(1) / T(2)) + z[60] + -z[61];
z[63] = z[32] * z[62];
z[3] = (T(1) / T(2)) * z[3] + -z[38] + z[43] + z[51] + -z[54] + z[56] + z[63];
z[63] = abb[32] * (T(1) / T(2));
z[3] = z[3] * z[63];
z[23] = (T(1) / T(4)) * z[23];
z[52] = z[23] + -z[52];
z[64] = (T(3) / T(2)) * z[12] + z[17];
z[64] = z[32] * z[64];
z[65] = abb[14] * z[12];
z[64] = (T(-1) / T(2)) * z[18] + -z[48] + -z[51] + z[52] + z[64] + (T(-1) / T(4)) * z[65];
z[64] = abb[30] * z[64];
z[3] = z[3] + (T(1) / T(2)) * z[31] + z[64];
z[3] = m1_set::bc<T>[0] * z[3];
z[31] = abb[1] * z[29];
z[64] = abb[15] * z[13];
z[66] = abb[21] * z[8];
z[64] = z[64] + z[66];
z[67] = abb[6] * z[2];
z[68] = abb[49] + z[0];
z[68] = abb[12] * z[68];
z[69] = abb[7] * z[40];
z[31] = z[31] + -z[46] + -z[49] + z[64] + z[67] + -z[68] + -z[69];
z[46] = abb[40] * z[31];
z[49] = z[4] + z[58];
z[49] = abb[51] + (T(1) / T(2)) * z[49];
z[49] = abb[7] * z[49];
z[58] = abb[4] * z[26];
z[67] = 3 * abb[53] + z[0];
z[67] = abb[51] + (T(1) / T(2)) * z[67];
z[67] = abb[14] * z[67];
z[49] = -z[24] + -3 * z[27] + z[49] + -z[58] + z[67];
z[58] = -abb[50] + z[25];
z[70] = (T(1) / T(2)) * z[58];
z[71] = -abb[6] * z[70];
z[71] = -z[35] + -z[47] + -z[49] + z[71];
z[71] = abb[39] * z[71];
z[54] = z[51] + z[54];
z[72] = -abb[7] + abb[0] * (T(-3) / T(2)) + z[32];
z[73] = abb[54] * (T(1) / T(2));
z[72] = z[72] * z[73];
z[74] = abb[7] * z[12];
z[75] = abb[16] * z[1];
z[76] = z[74] + z[75];
z[72] = z[36] + z[54] + -z[72] + (T(1) / T(4)) * z[76];
z[76] = abb[51] * (T(1) / T(2));
z[61] = z[61] + z[76];
z[0] = -abb[53] + (T(-3) / T(2)) * z[0] + -z[61];
z[0] = z[0] * z[32];
z[0] = z[0] + -z[11] + z[48] + z[72];
z[48] = abb[33] * m1_set::bc<T>[0];
z[0] = z[0] * z[48];
z[0] = -z[0] + z[46] + -z[71];
z[46] = abb[2] + abb[7];
z[71] = -z[42] + z[46];
z[71] = abb[54] * z[71];
z[22] = z[22] * z[30];
z[77] = abb[4] * abb[50];
z[78] = abb[51] + z[4];
z[78] = abb[2] * z[78];
z[79] = z[77] + -z[78];
z[10] = -z[10] + -z[22] + -z[28] + z[67] + -z[71] + z[79];
z[10] = (T(1) / T(2)) * z[10] + z[11];
z[22] = abb[38] * z[10];
z[67] = abb[20] * z[1];
z[71] = -abb[19] * z[12];
z[80] = -abb[0] * abb[55];
z[71] = -z[67] + z[71] + z[80];
z[80] = -abb[54] + -z[13];
z[80] = abb[17] * z[80];
z[81] = -abb[27] * z[8];
z[82] = abb[55] * z[32];
z[71] = (T(1) / T(2)) * z[71] + z[80] + z[81] + z[82];
z[71] = abb[41] * z[71];
z[80] = -abb[38] + -abb[40] + z[48];
z[80] = z[7] * z[80];
z[81] = abb[28] + -abb[31];
z[81] = z[7] * z[81];
z[82] = abb[30] * z[7];
z[83] = -z[81] + -z[82];
z[83] = m1_set::bc<T>[0] * z[83];
z[80] = z[80] + z[83];
z[80] = abb[22] * z[80];
z[83] = -abb[50] + abb[51];
z[84] = -abb[43] * z[83];
z[71] = z[71] + z[80] + z[84];
z[80] = abb[54] + z[29];
z[80] = abb[18] * z[80];
z[84] = -abb[41] * z[80];
z[0] = (T(-1) / T(2)) * z[0] + z[3] + z[22] + (T(1) / T(4)) * z[71] + (T(1) / T(8)) * z[84];
z[3] = -abb[39] + -abb[40];
z[3] = z[3] * z[7];
z[22] = abb[29] * z[8];
z[22] = z[22] + -z[82];
z[22] = m1_set::bc<T>[0] * z[22];
z[8] = z[8] * z[48];
z[3] = (T(1) / T(2)) * z[3] + z[8] + z[22];
z[3] = abb[24] * z[3];
z[0] = abb[60] + (T(1) / T(4)) * z[0] + (T(1) / T(8)) * z[3];
z[3] = abb[50] + abb[53] * (T(1) / T(2));
z[8] = abb[49] * (T(3) / T(2));
z[22] = -z[3] + -z[8] + -z[76];
z[22] = z[22] * z[32];
z[21] = z[5] + z[21];
z[48] = abb[55] * (T(1) / T(4));
z[48] = abb[17] * z[48];
z[36] = z[36] + z[48];
z[48] = (T(1) / T(2)) * z[4] + z[17];
z[48] = abb[9] * z[48];
z[34] = -z[34] * z[73];
z[11] = (T(1) / T(2)) * z[11];
z[71] = (T(1) / T(4)) * z[75];
z[76] = abb[6] * z[12];
z[84] = (T(1) / T(4)) * z[76];
z[85] = -abb[53] + z[17];
z[86] = abb[7] * z[85];
z[21] = z[11] + (T(-1) / T(2)) * z[21] + z[22] + -z[23] + z[34] + -z[36] + z[48] + z[51] + -z[55] + -z[71] + -z[84] + (T(1) / T(4)) * z[86];
z[21] = abb[28] * z[21];
z[22] = abb[4] * z[20];
z[23] = abb[9] * z[17];
z[34] = abb[7] * z[13];
z[22] = z[14] + z[22] + -z[23] + -z[27] + z[34] + z[38] + -z[64];
z[22] = abb[30] * z[22];
z[4] = abb[9] * z[4];
z[34] = z[4] + z[34];
z[38] = abb[51] + z[70];
z[38] = abb[4] * z[38];
z[28] = z[28] + -z[34] + z[38] + -z[39];
z[39] = abb[29] * z[28];
z[21] = z[21] + z[22] + z[39];
z[21] = abb[28] * z[21];
z[48] = abb[50] * (T(5) / T(2));
z[8] = z[8] + z[48] + z[60];
z[8] = z[8] * z[32];
z[2] = -z[2] + -z[17];
z[2] = z[2] * z[30];
z[2] = z[2] + z[8] + z[15] + -z[23] + z[43] + -z[54] + -z[71];
z[2] = abb[28] * z[2];
z[8] = abb[6] * abb[50];
z[23] = abb[3] * z[40];
z[40] = abb[54] * z[46];
z[40] = -z[8] + -z[15] + z[23] + z[40] + z[69] + -z[79];
z[40] = abb[31] * z[40];
z[43] = abb[49] + -abb[51] + -abb[53];
z[43] = z[30] * z[43];
z[3] = z[3] + z[61];
z[3] = abb[0] * z[3];
z[51] = abb[0] * (T(1) / T(2));
z[54] = -abb[2] + -z[30] + z[51];
z[54] = abb[54] * z[54];
z[3] = z[3] + z[8] + -z[19] + z[43] + z[54] + -z[78];
z[3] = z[3] * z[63];
z[8] = z[65] + z[74];
z[8] = (T(1) / T(2)) * z[8] + -z[23] + -z[27] + z[77];
z[8] = abb[29] * z[8];
z[2] = z[2] + z[3] + z[8] + -z[22] + z[40];
z[2] = abb[32] * z[2];
z[3] = -abb[0] * z[85];
z[3] = z[3] + -z[57] + -z[74] + z[75];
z[8] = -abb[4] * z[62];
z[19] = -abb[7] + abb[4] * (T(-3) / T(2)) + z[51];
z[19] = abb[54] * z[19];
z[3] = (T(1) / T(2)) * z[3] + z[8] + z[9] + z[19] + z[35] + -z[37] + -z[66] + z[76];
z[3] = z[3] * z[63];
z[8] = abb[4] * (T(1) / T(4));
z[9] = -z[8] * z[29];
z[9] = z[9] + -z[15] + z[72];
z[9] = abb[28] * z[9];
z[15] = -abb[29] + abb[30];
z[15] = z[15] * z[50];
z[3] = z[3] + z[9] + z[15] + -z[44];
z[3] = abb[33] * z[3];
z[9] = abb[58] * z[31];
z[15] = z[69] + z[78];
z[19] = -z[4] + -z[15] + z[65];
z[22] = z[25] + z[59];
z[29] = abb[51] + (T(1) / T(2)) * z[22];
z[29] = z[29] * z[32];
z[31] = -z[46] * z[73];
z[37] = abb[6] * z[58];
z[11] = z[11] + (T(1) / T(2)) * z[19] + -z[23] + z[29] + z[31] + (T(-1) / T(4)) * z[37] + z[52];
z[11] = abb[31] * z[11];
z[19] = z[14] + z[24];
z[4] = z[4] + -z[19] + z[23] + -z[41];
z[24] = abb[29] * z[4];
z[16] = abb[28] * z[16];
z[11] = z[11] + z[16] + z[24];
z[11] = abb[31] * z[11];
z[16] = abb[26] * z[7];
z[24] = abb[6] * abb[52];
z[24] = z[16] + z[23] + -z[24] + -z[56] + z[57] + z[68];
z[29] = abb[52] * (T(5) / T(2));
z[20] = abb[51] * (T(5) / T(2)) + z[20] + z[29];
z[20] = abb[1] * z[20];
z[30] = -abb[2] + z[30];
z[8] = abb[0] + -z[8] + (T(1) / T(2)) * z[30];
z[8] = abb[54] * z[8];
z[5] = z[5] + z[8] + z[20];
z[8] = -abb[53] + -z[29] + -z[48];
z[8] = (T(1) / T(3)) * z[8] + -z[61];
z[8] = abb[7] * z[8];
z[20] = z[75] + -z[78];
z[8] = z[8] + (T(1) / T(3)) * z[20];
z[20] = abb[21] + z[33];
z[20] = (T(1) / T(6)) * z[20];
z[20] = z[7] * z[20];
z[29] = -abb[53] + abb[52] * (T(-7) / T(4));
z[29] = abb[50] * (T(-3) / T(4)) + abb[51] * (T(-1) / T(12)) + (T(1) / T(3)) * z[29] + -z[53];
z[29] = abb[4] * z[29];
z[1] = abb[8] * z[1];
z[5] = (T(-5) / T(12)) * z[1] + (T(1) / T(3)) * z[5] + (T(1) / T(2)) * z[8] + z[20] + (T(1) / T(6)) * z[24] + z[29] + z[36];
z[8] = prod_pow(m1_set::bc<T>[0], 2);
z[5] = z[5] * z[8];
z[20] = -z[52] + z[84];
z[24] = abb[50] + 3 * z[25];
z[24] = abb[51] + (T(1) / T(2)) * z[24];
z[24] = z[24] * z[32];
z[24] = -z[14] + -z[20] + z[24] + (T(3) / T(2)) * z[27] + (T(-1) / T(2)) * z[34] + -z[47];
z[25] = prod_pow(abb[30], 2);
z[24] = z[24] * z[25];
z[13] = -z[13] + -z[17];
z[13] = abb[4] * z[13];
z[13] = z[13] + z[18] + -z[19] + z[35] + z[64];
z[13] = abb[34] * z[13];
z[4] = abb[36] * z[4];
z[17] = abb[37] * z[28];
z[2] = z[2] + z[3] + -z[4] + z[5] + z[9] + z[11] + z[13] + z[17] + z[21] + z[24];
z[3] = abb[56] * z[10];
z[4] = z[14] + z[34] + z[45];
z[5] = -z[26] * z[32];
z[4] = (T(-1) / T(4)) * z[1] + (T(1) / T(2)) * z[4] + z[5] + z[20] + -z[27];
z[4] = abb[30] * z[4];
z[4] = z[4] + (T(1) / T(4)) * z[39];
z[4] = abb[29] * z[4];
z[3] = -z[3] + z[4];
z[4] = abb[58] + (T(5) / T(6)) * z[8];
z[5] = abb[34] + -abb[36] + abb[56] + z[4];
z[5] = z[5] * z[7];
z[8] = abb[29] * z[7];
z[9] = z[8] + z[81];
z[10] = abb[31] * z[9];
z[9] = -z[9] + z[82];
z[9] = abb[32] * z[9];
z[11] = abb[33] * z[81];
z[13] = -abb[28] * z[82];
z[5] = z[5] + z[9] + z[10] + z[11] + z[13];
z[5] = abb[22] * z[5];
z[4] = abb[57] + z[4] + -z[25];
z[4] = z[4] * z[7];
z[8] = z[8] + -z[82];
z[8] = -abb[33] * z[8];
z[9] = abb[29] * z[82];
z[4] = z[4] + z[8] + z[9];
z[4] = abb[24] * z[4];
z[8] = abb[35] * z[22];
z[9] = abb[57] * z[58];
z[8] = z[8] + z[9];
z[8] = abb[6] * z[8];
z[9] = abb[61] * z[83];
z[4] = z[4] + z[5] + z[8] + z[9];
z[5] = z[45] + z[49];
z[1] = (T(-1) / T(16)) * z[1] + (T(1) / T(8)) * z[5];
z[1] = abb[57] * z[1];
z[5] = -z[42] + -z[46];
z[5] = abb[54] * z[5];
z[5] = z[5] + z[6] + -z[15] + -z[23] + -z[38];
z[5] = (T(1) / T(8)) * z[5] + (T(1) / T(16)) * z[16];
z[5] = abb[35] * z[5];
z[6] = abb[17] + abb[19];
z[6] = z[6] * z[12];
z[8] = -abb[55] * z[42];
z[9] = abb[27] * z[7];
z[6] = z[6] + z[8] + z[9] + z[67];
z[8] = abb[17] * abb[54];
z[6] = (T(1) / T(2)) * z[6] + z[8];
z[6] = (T(1) / T(16)) * z[6] + (T(1) / T(32)) * z[80];
z[6] = abb[59] * z[6];
z[8] = (T(1) / T(16)) * z[33];
z[9] = abb[35] + abb[57];
z[7] = z[7] * z[8] * z[9];
z[1] = abb[42] + z[1] + (T(1) / T(8)) * z[2] + (T(1) / T(4)) * z[3] + (T(1) / T(16)) * z[4] + z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_450_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("-0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("-0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("-0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("-0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.48524910110241459790862234534767796332734538495666512704993262706"),stof<T>("-0.29052638286008647118722438741794650126285966554119689314774511816")}, std::complex<T>{stof<T>("0.9788439106304728393271007548870268657247688714666463408495838392"),stof<T>("-1.6281447122128460198953707943969300711054142498462411190463939805")}, std::complex<T>{stof<T>("0.5905135479560673365080984240154818046675684078767304602635326423"),stof<T>("-0.80156009859685418721786506277313509703174432196358364022511828185")}, std::complex<T>{stof<T>("1.482920261342931086067816545071861781769864025377528020758429138"),stof<T>("-1.7878180226885749155664852969163743351015756090397336673823473314")}, std::complex<T>{stof<T>("0.47496288214297962577120916066981579184662406363666603511282311976"),stof<T>("-0.77405025490445565767522096375302823728101323969732635061833021137")}, std::complex<T>{stof<T>("0.25788446378157078644169071588347303237168339037386471393954120566"),stof<T>("-0.67938639824574491084493826156685035800529820202748709595826682305")}, std::complex<T>{stof<T>("0.34589967045123744046959203147849259087531787275930987103590649702"),stof<T>("-0.46448714177691965286377725952042574354198205525390718348459342161")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_450_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_450_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(64)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(-1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[51] + abb[53] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[51] + abb[53] + abb[54];
z[1] = abb[31] + -abb[32];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_450_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(64)) * (v[2] + v[3]) * (-4 * v[0] + -2 * v[1] + -m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 2 * (-4 + v[2] + 2 * v[3] + v[4] + -2 * v[5]))) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[51] + abb[53] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = -abb[28] + abb[32] + abb[31] * (T(-1) / T(2));
z[0] = abb[31] * z[0];
z[1] = -abb[28] + abb[32] * (T(1) / T(2));
z[1] = abb[32] * z[1];
z[2] = abb[31] + -abb[32];
z[2] = abb[33] * z[2];
z[0] = z[0] + -z[1] + z[2];
z[0] = -abb[35] + (T(1) / T(2)) * z[0];
z[1] = -abb[51] + -abb[53] + -abb[54];
return abb[11] * (T(1) / T(4)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_450_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.32290566386258620121769771522641431059727921909249202240991433855"),stof<T>("0.45040281605811069704506168001371318984007518259922801796282362986")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}, T{0}};
abb[42] = SpDLog_f_4_450_W_19_Im(t, path, abb);
abb[43] = SpDLogQ_W_72(k,dl,dlr).imag();
abb[60] = SpDLog_f_4_450_W_19_Re(t, path, abb);
abb[61] = SpDLogQ_W_72(k,dl,dlr).real();

                    
            return f_4_450_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_450_DLogXconstant_part(base_point<T>, kend);
	value += f_4_450_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_450_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_450_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_450_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_450_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_450_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_450_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
