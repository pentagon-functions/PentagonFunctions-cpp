/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_339.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_339_abbreviated (const std::array<T,27>& abb) {
T z[58];
z[0] = 3 * abb[0];
z[1] = 3 * abb[3];
z[2] = -8 * abb[2] + z[0] + z[1];
z[3] = 8 * abb[1];
z[4] = -abb[6] + z[3];
z[5] = 3 * abb[4];
z[6] = 3 * abb[5];
z[4] = -z[2] + 2 * z[4] + -z[5] + -z[6];
z[7] = 2 * abb[19];
z[4] = z[4] * z[7];
z[8] = 4 * abb[2];
z[3] = z[3] + z[8];
z[9] = -abb[6] + z[3];
z[10] = 2 * abb[0];
z[11] = 2 * abb[3];
z[12] = -abb[4] + z[9] + -z[10] + -z[11];
z[13] = 2 * abb[20];
z[12] = z[12] * z[13];
z[14] = abb[2] + -abb[6];
z[15] = -abb[1] + z[14];
z[16] = 2 * abb[5];
z[17] = z[11] + z[15] + -z[16];
z[17] = abb[23] * z[17];
z[18] = abb[20] + -abb[24];
z[19] = z[16] * z[18];
z[15] = z[10] + z[15];
z[15] = abb[22] * z[15];
z[20] = 2 * abb[6];
z[21] = abb[0] + abb[3];
z[22] = z[20] + z[21];
z[23] = 2 * abb[4];
z[24] = -7 * abb[1] + -5 * abb[2] + z[22] + z[23];
z[24] = abb[24] * z[24];
z[25] = -abb[3] + 4 * abb[5] + z[0] + -z[9];
z[25] = abb[21] * z[25];
z[9] = -abb[0] + 4 * abb[4] + z[1] + -z[9];
z[9] = abb[18] * z[9];
z[26] = abb[22] * z[23];
z[27] = abb[25] + -abb[26];
z[28] = abb[9] * z[27];
z[29] = abb[7] * z[27];
z[4] = z[4] + z[9] + z[12] + z[15] + z[17] + -z[19] + z[24] + z[25] + -z[26] + z[28] + -z[29];
z[4] = abb[14] * z[4];
z[9] = 5 * abb[1];
z[12] = 3 * abb[2];
z[15] = -z[9] + -z[12] + z[22];
z[15] = abb[24] * z[15];
z[17] = abb[1] + z[14];
z[22] = abb[22] * z[17];
z[22] = z[22] + z[28];
z[24] = abb[23] * z[17];
z[24] = z[22] + z[24];
z[25] = 4 * abb[1];
z[30] = 2 * abb[2];
z[31] = z[25] + z[30];
z[32] = -abb[4] + z[31];
z[33] = -abb[3] + -abb[6] + z[32];
z[33] = abb[18] * z[33];
z[34] = -abb[6] + z[31];
z[35] = -z[21] + z[34];
z[35] = z[13] * z[35];
z[15] = z[15] + z[24] + -z[29] + -z[33] + z[35];
z[33] = abb[12] * z[15];
z[36] = abb[0] + abb[6];
z[37] = z[31] + -z[36];
z[38] = -abb[5] + z[37];
z[38] = abb[21] * z[38];
z[39] = -abb[6] + z[25];
z[40] = -abb[4] + -z[2] + 4 * z[39];
z[41] = -abb[5] + z[40];
z[41] = abb[19] * z[41];
z[15] = z[15] + -z[38] + z[41];
z[38] = -abb[13] * z[15];
z[37] = -abb[12] * z[37];
z[41] = abb[5] * abb[12];
z[37] = z[37] + z[41];
z[37] = abb[21] * z[37];
z[40] = abb[12] * z[40];
z[40] = z[40] + -z[41];
z[40] = abb[19] * z[40];
z[33] = z[33] + z[37] + z[38] + z[40];
z[4] = z[4] + 2 * z[33];
z[4] = abb[14] * z[4];
z[33] = 4 * abb[19];
z[37] = z[13] + z[33];
z[38] = 2 * abb[1];
z[40] = z[14] + z[38];
z[42] = -abb[0] + z[40];
z[43] = abb[4] + z[42];
z[37] = z[37] * z[43];
z[20] = z[20] + -z[30];
z[43] = 3 * abb[1];
z[44] = z[20] + -z[43];
z[45] = abb[0] + -z[23] + z[44];
z[45] = abb[24] * z[45];
z[46] = abb[8] * z[27];
z[47] = z[26] + z[46];
z[48] = -abb[21] * z[42];
z[49] = -z[23] + -z[42];
z[49] = abb[18] * z[49];
z[24] = z[24] + z[37] + z[45] + z[47] + z[48] + z[49];
z[24] = abb[11] * z[24];
z[37] = abb[0] * (T(3) / T(2));
z[48] = abb[3] * (T(3) / T(2));
z[49] = z[37] + z[48];
z[50] = -abb[6] + z[43];
z[51] = z[12] + -z[49] + 2 * z[50];
z[51] = abb[24] * z[51];
z[40] = -abb[3] + z[40];
z[52] = abb[23] * z[40];
z[42] = abb[22] * z[42];
z[53] = (T(3) / T(2)) * z[29];
z[35] = -z[28] + -z[35] + -z[42] + z[51] + -z[52] + z[53];
z[42] = abb[0] * (T(1) / T(2));
z[51] = abb[3] * (T(1) / T(2));
z[52] = z[42] + -z[51];
z[54] = -z[23] + z[34] + z[52];
z[54] = abb[18] * z[54];
z[54] = z[35] + z[54];
z[48] = z[42] + z[48];
z[55] = z[34] + -z[48];
z[55] = abb[21] * z[55];
z[39] = -abb[4] + z[8] + 2 * z[39];
z[56] = -z[11] + z[39];
z[57] = abb[0] + -z[56];
z[57] = z[7] * z[57];
z[55] = z[54] + z[55] + z[57];
z[55] = abb[12] * z[55];
z[52] = z[16] + -z[34] + z[52];
z[52] = abb[21] * z[52];
z[39] = -abb[5] + -z[21] + z[39];
z[39] = z[7] * z[39];
z[39] = z[39] + z[52] + -z[54];
z[39] = abb[13] * z[39];
z[15] = abb[14] * z[15];
z[15] = -2 * z[15] + z[24] + z[39] + z[55];
z[15] = abb[11] * z[15];
z[24] = abb[3] + z[44];
z[24] = abb[24] * z[24];
z[22] = z[22] + z[24];
z[24] = z[19] + -z[46];
z[39] = -abb[18] + z[13];
z[44] = z[39] * z[40];
z[54] = z[16] + z[17];
z[54] = abb[23] * z[54];
z[55] = -z[16] + -z[40];
z[55] = abb[21] * z[55];
z[40] = abb[5] + z[40];
z[40] = z[33] * z[40];
z[40] = z[22] + z[24] + -z[29] + z[40] + z[44] + z[54] + z[55];
z[40] = abb[13] * z[40];
z[37] = z[37] + z[51];
z[34] = z[34] + -z[37];
z[34] = abb[18] * z[34];
z[34] = z[34] + z[35] + -z[52];
z[34] = abb[12] * z[34];
z[25] = -z[25] + z[36];
z[25] = 2 * z[25];
z[35] = abb[3] + -z[8] + z[25];
z[35] = abb[12] * z[35];
z[35] = z[35] + z[41];
z[7] = z[7] * z[35];
z[7] = z[7] + z[34] + z[40];
z[7] = abb[13] * z[7];
z[9] = z[9] + -z[20];
z[1] = -z[1] + z[9];
z[1] = abb[20] * z[1];
z[20] = abb[3] + z[14] + z[16];
z[20] = abb[23] * z[20];
z[34] = z[14] + z[43];
z[35] = z[11] + -z[34];
z[35] = abb[18] * z[35];
z[1] = z[1] + z[19] + z[20] + z[22] + -z[29] + z[35] + -z[46];
z[19] = 10 * abb[2];
z[20] = -abb[0] + 4 * z[50];
z[20] = -9 * abb[3] + z[6] + z[19] + 2 * z[20];
z[20] = abb[19] * z[20];
z[8] = abb[3] + z[8];
z[22] = abb[5] + -z[8] + z[25];
z[22] = abb[21] * z[22];
z[1] = 2 * z[1] + z[20] + z[22];
z[1] = abb[16] * z[1];
z[20] = -abb[6] + 6 * abb[10] + -z[12] + -z[49];
z[20] = z[20] * z[27];
z[22] = abb[21] + abb[24];
z[25] = abb[23] + z[13];
z[22] = (T(3) / T(2)) * z[22] + -z[25];
z[22] = abb[7] * z[22];
z[27] = -abb[7] + abb[9];
z[27] = z[27] * z[33];
z[25] = -abb[21] + abb[22] + -2 * abb[24] + z[25];
z[25] = abb[9] * z[25];
z[35] = abb[21] + abb[22] + -abb[23];
z[35] = abb[8] * z[35];
z[36] = -abb[8] + -abb[9] + abb[7] * (T(1) / T(2));
z[36] = abb[18] * z[36];
z[20] = z[20] + z[22] + z[25] + z[27] + z[35] + z[36];
z[20] = abb[17] * z[20];
z[0] = -z[0] + z[9] + z[23];
z[0] = abb[20] * z[0];
z[9] = abb[0] + z[14];
z[9] = abb[22] * z[9];
z[0] = z[0] + z[9] + z[26] + z[28] + z[45];
z[0] = abb[15] * z[0];
z[9] = abb[0] + -abb[1];
z[14] = (T(-13) / T(3)) * z[9] + z[12];
z[14] = abb[21] * z[14];
z[22] = -abb[1] + abb[3];
z[25] = z[12] + (T(-13) / T(3)) * z[22];
z[25] = abb[18] * z[25];
z[14] = z[14] + z[25];
z[25] = z[21] + -z[38];
z[26] = abb[24] * z[25];
z[9] = abb[22] * z[9];
z[27] = (T(13) / T(2)) * z[9] + -z[26] + z[29];
z[28] = -abb[1] + z[42] + z[51];
z[28] = 3 * z[28];
z[35] = abb[2] * (T(-7) / T(6)) + z[28];
z[35] = abb[20] * z[35];
z[12] = -z[12] + (T(11) / T(3)) * z[25];
z[12] = abb[19] * z[12];
z[36] = abb[23] * z[22];
z[12] = z[12] + (T(1) / T(2)) * z[14] + (T(-1) / T(3)) * z[27] + z[35] + (T(-13) / T(6)) * z[36];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[14] = -abb[2] + z[25];
z[27] = z[13] * z[14];
z[28] = abb[24] * z[28];
z[28] = -z[9] + -z[27] + z[28] + -z[53];
z[35] = prod_pow(abb[12], 2);
z[28] = z[28] * z[35];
z[19] = -9 * abb[0] + z[5] + -z[11] + z[19] + 8 * z[50];
z[19] = abb[15] * z[19];
z[36] = z[14] * z[35];
z[19] = z[19] + -4 * z[36];
z[19] = abb[19] * z[19];
z[36] = 2 * abb[15];
z[17] = z[17] * z[36];
z[40] = -z[22] * z[35];
z[17] = z[17] + z[40];
z[17] = abb[23] * z[17];
z[40] = z[30] + z[38];
z[41] = -z[40] + z[48];
z[41] = z[35] * z[41];
z[10] = z[10] + -z[34];
z[10] = z[10] * z[36];
z[10] = z[10] + z[41];
z[10] = abb[21] * z[10];
z[34] = z[37] + -z[40];
z[34] = z[34] * z[35];
z[35] = -abb[0] + -z[56];
z[35] = abb[15] * z[35];
z[34] = z[34] + z[35];
z[34] = abb[18] * z[34];
z[35] = z[36] * z[46];
z[0] = 2 * z[0] + z[1] + z[4] + z[7] + z[10] + z[12] + z[15] + z[17] + z[19] + z[20] + z[28] + z[34] + z[35];
z[1] = abb[2] + -abb[3] + z[38];
z[4] = -z[1] + z[23];
z[4] = abb[24] * z[4];
z[7] = -abb[4] + z[1];
z[7] = z[7] * z[13];
z[10] = 5 * abb[4];
z[3] = abb[0] + -4 * abb[3] + z[3] + -z[10];
z[3] = abb[19] * z[3];
z[5] = -abb[0] + z[5];
z[12] = -z[1] + z[5];
z[12] = abb[18] * z[12];
z[1] = -abb[21] * z[1];
z[1] = z[1] + z[3] + z[4] + z[7] + z[12] + -z[29] + -z[47];
z[1] = abb[11] * z[1];
z[3] = z[21] + -z[32];
z[3] = abb[20] * z[3];
z[4] = abb[4] * abb[22];
z[7] = abb[1] + abb[2] + -abb[4];
z[7] = abb[24] * z[7];
z[12] = abb[5] * z[18];
z[13] = abb[5] + -z[22];
z[13] = abb[23] * z[13];
z[3] = z[3] + z[4] + z[7] + -z[9] + z[12] + z[13];
z[4] = -abb[3] + z[6];
z[6] = -abb[0] + z[38];
z[7] = -z[4] + 2 * z[6] + z[30];
z[7] = abb[21] * z[7];
z[9] = 5 * abb[5];
z[2] = -16 * abb[1] + z[2] + z[9] + z[10];
z[2] = abb[19] * z[2];
z[5] = -z[5] + -z[11] + z[31];
z[5] = abb[18] * z[5];
z[2] = z[2] + 2 * z[3] + z[5] + z[7];
z[2] = abb[14] * z[2];
z[3] = z[25] + -z[30];
z[5] = -abb[18] + -abb[21];
z[3] = z[3] * z[5];
z[5] = z[14] * z[33];
z[3] = z[3] + z[5] + -z[26] + z[27] + z[29];
z[3] = abb[12] * z[3];
z[5] = 4 * z[6] + z[8] + -z[9];
z[5] = abb[19] * z[5];
z[7] = -abb[24] + z[39];
z[6] = abb[2] + z[6];
z[7] = z[6] * z[7];
z[4] = z[4] + -z[6];
z[4] = abb[21] * z[4];
z[6] = -abb[23] * z[16];
z[4] = z[4] + z[5] + z[6] + z[7] + -z[24];
z[4] = abb[13] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = 2 * m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_339_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.8742691948245303969612672122180536053732680916646295835217907"),stof<T>("-39.833880071614355688647587564328552413312898317304811739516838307")}, std::complex<T>{stof<T>("-53.732978609221249629238752626557891109031379935235954035498314276"),stof<T>("22.755612255801653928817376150152361921475396123299845682965988934")}, std::complex<T>{stof<T>("-27.990457030907411866549297555463193072482579716816864838033733418"),stof<T>("-1.320568075096230650358167647681362941151242414809926594584266385")}, std::complex<T>{stof<T>("46.995927610188462567691014469128726606395927035362035971262342521"),stof<T>("23.9441878386074336941506077992027214572360275931736645044076572")}, std::complex<T>{stof<T>("26.020008535337350086719711905342405389990540756374283712513714011"),stof<T>("25.638171302187677012676173826844938907631284341544786708623200238")}, std::complex<T>{stof<T>("-14.235614369537093677332968124518190850967208527247491741360852382"),stof<T>("-24.811106568733956045617349238159990093643421220443962861420478247")}, std::complex<T>{stof<T>("-1.586536985971469475438869205588498215507305618794122161697549587"),stof<T>("-3.8464923781242264506896104868557398896931942601347611475297649083")}, std::complex<T>{stof<T>("-3.223914705502374423711964070030749960523055343174597416322871061"),stof<T>("-15.748931692217380713457583612152853300165062016478596541063558734")}, std::complex<T>{stof<T>("3.223914705502374423711964070030749960523055343174597416322871061"),stof<T>("15.748931692217380713457583612152853300165062016478596541063558734")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_339_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_339_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-13.931774549010400184365465077473426398504375572811063138582766005"),stof<T>("48.888963568732113554103077905733627948307592886185031624601006595")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dlog_W3(k,dl), dl[2], dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_339_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_339_DLogXconstant_part(base_point<T>, kend);
	value += f_4_339_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_339_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_339_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_339_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_339_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_339_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_339_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
