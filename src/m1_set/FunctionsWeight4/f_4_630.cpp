/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_630.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_630_abbreviated (const std::array<T,62>& abb) {
T z[103];
z[0] = -abb[45] + abb[49];
z[1] = abb[48] * (T(1) / T(2));
z[2] = abb[52] * (T(1) / T(2));
z[0] = -abb[50] + (T(1) / T(2)) * z[0] + z[1] + z[2];
z[3] = abb[46] * (T(1) / T(2));
z[4] = z[0] + -z[3];
z[5] = abb[16] * z[4];
z[6] = 3 * abb[52];
z[7] = -abb[45] + abb[48];
z[8] = 3 * abb[49];
z[9] = abb[46] * (T(11) / T(3)) + -z[6] + z[7] + z[8];
z[9] = abb[44] * (T(-1) / T(3)) + (T(1) / T(2)) * z[9];
z[9] = abb[5] * z[9];
z[10] = abb[44] + abb[50];
z[11] = -abb[52] + z[10];
z[12] = abb[0] * z[11];
z[9] = z[9] + (T(13) / T(3)) * z[12];
z[13] = abb[51] + abb[47] * (T(1) / T(2));
z[14] = abb[45] * (T(3) / T(4));
z[15] = abb[49] * (T(3) / T(4));
z[16] = -abb[50] + abb[44] * (T(-5) / T(6)) + abb[46] * (T(-5) / T(12)) + abb[48] * (T(-1) / T(4)) + abb[52] * (T(3) / T(4)) + -z[13] + z[14] + z[15];
z[16] = abb[6] * z[16];
z[17] = abb[45] + abb[49];
z[18] = abb[48] + abb[52];
z[19] = abb[46] + z[18];
z[20] = z[17] + -z[19];
z[20] = abb[44] + (T(1) / T(2)) * z[20];
z[21] = abb[14] * z[20];
z[22] = abb[46] + z[17] + -z[18];
z[23] = abb[8] * z[22];
z[24] = abb[47] + -abb[52];
z[25] = abb[51] * (T(1) / T(3));
z[24] = abb[45] + abb[48] * (T(-7) / T(6)) + abb[46] * (T(1) / T(3)) + (T(1) / T(6)) * z[24] + z[25];
z[24] = abb[4] * z[24];
z[26] = -abb[47] + z[19];
z[26] = -abb[44] + (T(1) / T(3)) * z[26];
z[25] = -z[25] + (T(1) / T(2)) * z[26];
z[25] = abb[2] * z[25];
z[26] = abb[22] + abb[24];
z[27] = abb[21] + z[26];
z[28] = 5 * abb[23] + 3 * z[27];
z[28] = -abb[25] + (T(1) / T(2)) * z[28];
z[29] = abb[53] * (T(1) / T(2));
z[30] = z[28] * z[29];
z[31] = abb[49] + -abb[52];
z[32] = abb[46] + z[31];
z[32] = abb[13] * z[32];
z[33] = 3 * z[32];
z[34] = abb[44] + z[31];
z[34] = abb[10] * z[34];
z[35] = 3 * z[34];
z[36] = abb[46] + -abb[52];
z[37] = abb[50] + z[36];
z[38] = abb[1] * z[37];
z[39] = 3 * abb[46];
z[40] = 5 * abb[45] + -3 * abb[48] + abb[49] + abb[52] + -z[39];
z[40] = (T(1) / T(2)) * z[40];
z[41] = abb[47] + -z[40];
z[41] = abb[51] + (T(1) / T(2)) * z[41];
z[41] = abb[3] * z[41];
z[9] = -z[5] + (T(1) / T(2)) * z[9] + z[16] + z[21] + (T(-5) / T(4)) * z[23] + z[24] + z[25] + z[30] + z[33] + -z[35] + (T(-19) / T(6)) * z[38] + z[41];
z[16] = prod_pow(m1_set::bc<T>[0], 2);
z[9] = z[9] * z[16];
z[24] = -abb[21] + abb[25];
z[25] = abb[28] * (T(1) / T(2));
z[24] = z[24] * z[25];
z[25] = (T(1) / T(2)) * z[26];
z[30] = abb[21] * (T(1) / T(2)) + z[25];
z[41] = abb[25] * (T(1) / T(2));
z[42] = abb[26] + z[30] + z[41];
z[43] = -abb[29] + abb[31];
z[42] = -z[42] * z[43];
z[44] = abb[21] + abb[26];
z[45] = z[26] + z[44];
z[45] = abb[30] * z[45];
z[46] = abb[32] * z[26];
z[24] = z[24] + -z[42] + z[45] + -z[46];
z[24] = abb[28] * z[24];
z[42] = abb[23] + z[44];
z[47] = z[26] + z[42];
z[48] = abb[31] * z[47];
z[49] = abb[26] + z[26];
z[50] = abb[30] * z[49];
z[51] = abb[26] * abb[29];
z[46] = (T(1) / T(2)) * z[46] + -z[48] + -z[50] + z[51];
z[46] = abb[32] * z[46];
z[50] = abb[26] + z[25];
z[50] = abb[30] * z[50];
z[50] = z[50] + -z[51];
z[50] = abb[30] * z[50];
z[51] = -abb[25] + z[27];
z[52] = abb[29] * z[51];
z[45] = -z[45] + (T(1) / T(2)) * z[52];
z[52] = abb[31] * z[45];
z[53] = abb[25] + z[49];
z[53] = abb[34] * z[53];
z[26] = abb[37] * z[26];
z[54] = abb[58] * z[42];
z[27] = abb[36] * z[27];
z[49] = abb[35] * z[49];
z[55] = abb[27] * (T(1) / T(2));
z[56] = abb[59] * z[55];
z[51] = abb[23] + z[51];
z[57] = abb[57] * z[51];
z[24] = -z[24] + z[26] + -z[27] + z[46] + z[49] + z[50] + -z[52] + -z[53] + z[54] + -z[56] + z[57];
z[26] = abb[56] * z[44];
z[26] = -z[24] + z[26];
z[27] = abb[28] + -abb[29];
z[46] = abb[25] + abb[26];
z[46] = z[27] * z[46];
z[49] = abb[32] * z[42];
z[46] = z[46] + z[48] + z[49];
z[48] = z[25] + z[42];
z[49] = abb[33] * z[48];
z[49] = -z[46] + z[49];
z[50] = 3 * abb[33];
z[49] = z[49] * z[50];
z[16] = z[16] * z[28];
z[16] = -z[16] + 3 * z[26] + z[49];
z[26] = abb[54] + abb[55];
z[28] = (T(-1) / T(2)) * z[26];
z[16] = z[16] * z[28];
z[28] = abb[53] * (T(3) / T(2));
z[24] = z[24] * z[28];
z[49] = (T(3) / T(2)) * z[5];
z[50] = z[38] + z[49];
z[52] = 3 * abb[44];
z[53] = -z[19] + z[52];
z[54] = abb[47] + z[53];
z[54] = abb[51] + (T(1) / T(2)) * z[54];
z[54] = abb[2] * z[54];
z[56] = abb[46] + abb[48];
z[6] = abb[45] + 5 * abb[49] + -z[6] + z[56];
z[6] = (T(1) / T(2)) * z[6];
z[57] = -abb[47] + z[6];
z[57] = -abb[51] + (T(1) / T(2)) * z[57];
z[58] = 3 * abb[7];
z[57] = z[57] * z[58];
z[58] = (T(3) / T(2)) * z[21];
z[59] = abb[44] + -abb[46];
z[60] = abb[49] + -abb[50];
z[60] = z[59] + 3 * z[60];
z[61] = abb[5] * (T(1) / T(2));
z[60] = z[60] * z[61];
z[62] = 3 * abb[45];
z[63] = z[8] + z[62];
z[64] = -z[19] + z[63];
z[65] = (T(1) / T(2)) * z[64];
z[66] = -abb[47] + z[65];
z[66] = -abb[51] + (T(1) / T(2)) * z[66];
z[66] = abb[4] * z[66];
z[67] = abb[6] * z[59];
z[60] = -2 * z[12] + z[35] + -z[50] + z[54] + -z[57] + -z[58] + z[60] + z[66] + z[67];
z[60] = abb[28] * z[60];
z[14] = abb[52] * (T(1) / T(4)) + -z[10] + -z[14] + z[15] + (T(3) / T(4)) * z[56];
z[14] = abb[5] * z[14];
z[15] = abb[52] * (T(5) / T(2));
z[66] = abb[46] * (T(3) / T(2));
z[67] = z[15] + -z[66];
z[68] = abb[45] * (T(3) / T(2));
z[69] = abb[48] * (T(3) / T(2)) + -z[68];
z[70] = abb[49] * (T(3) / T(2));
z[71] = z[69] + z[70];
z[10] = -z[10] + z[67] + -z[71];
z[72] = abb[6] * (T(1) / T(2));
z[10] = z[10] * z[72];
z[73] = 2 * z[38];
z[49] = z[49] + z[73];
z[74] = abb[47] + 2 * abb[51];
z[53] = z[53] + z[74];
z[75] = abb[2] * z[53];
z[58] = (T(1) / T(2)) * z[12] + -z[58] + z[75];
z[65] = z[65] + -z[74];
z[76] = abb[15] * z[65];
z[10] = z[10] + z[14] + -z[49] + -z[58] + -z[76];
z[10] = z[10] * z[43];
z[14] = 3 * z[75] + z[76];
z[77] = abb[6] * z[65];
z[78] = abb[4] * z[65];
z[79] = z[77] + z[78];
z[53] = abb[5] * z[53];
z[80] = 3 * z[21];
z[53] = z[14] + z[53] + z[79] + -z[80];
z[53] = abb[30] * z[53];
z[81] = -z[8] + z[74];
z[82] = 2 * abb[52] + -z[56] + z[81];
z[83] = 2 * abb[15];
z[82] = z[82] * z[83];
z[65] = abb[5] * z[65];
z[6] = z[6] + -z[74];
z[83] = abb[7] * z[6];
z[84] = 3 * z[83];
z[79] = -z[65] + z[79] + z[82] + z[84];
z[85] = abb[32] * z[79];
z[10] = z[10] + -z[53] + z[60] + z[85];
z[10] = abb[28] * z[10];
z[60] = (T(3) / T(2)) * z[23];
z[85] = z[60] + -z[76];
z[69] = z[69] + -z[70];
z[86] = -abb[50] + -z[2] + z[3];
z[87] = z[69] + -z[86];
z[87] = z[61] * z[87];
z[88] = 5 * abb[46];
z[89] = z[18] + z[63] + -z[88];
z[89] = -z[74] + (T(1) / T(4)) * z[89];
z[89] = abb[4] * z[89];
z[90] = abb[46] * (T(5) / T(2));
z[15] = -abb[50] + z[15] + -z[71] + -z[90];
z[15] = z[15] * z[72];
z[48] = -z[28] * z[48];
z[91] = abb[48] + -abb[50] + -abb[52];
z[92] = -abb[47] + z[8] + z[91];
z[92] = -abb[51] + (T(1) / T(2)) * z[92];
z[93] = -abb[9] * z[92];
z[94] = (T(1) / T(2)) * z[38];
z[15] = z[15] + -z[33] + z[48] + z[57] + z[85] + z[87] + z[89] + z[93] + z[94];
z[15] = abb[33] * z[15];
z[48] = z[71] + z[86];
z[71] = abb[5] * z[48];
z[86] = 3 * z[5];
z[87] = 4 * z[38] + z[86];
z[89] = 2 * abb[6];
z[93] = z[37] * z[89];
z[71] = -z[71] + z[76] + -z[78] + z[87] + z[93];
z[27] = z[27] * z[71];
z[17] = -z[17] + -z[18] + z[39];
z[17] = (T(1) / T(2)) * z[17] + z[74];
z[17] = abb[4] * z[17];
z[6] = abb[15] * z[6];
z[6] = z[6] + -z[83];
z[39] = z[22] * z[61];
z[17] = z[6] + z[17] + (T(-1) / T(2)) * z[23] + z[39];
z[39] = 3 * z[17];
z[61] = abb[32] * z[39];
z[48] = abb[6] * z[48];
z[71] = 2 * abb[46] + z[74];
z[83] = -z[18] + z[71];
z[93] = abb[4] * z[83];
z[48] = z[48] + z[93];
z[37] = abb[5] * z[37];
z[37] = z[37] + z[38] + z[48] + -z[85];
z[37] = abb[31] * z[37];
z[46] = z[28] * z[46];
z[81] = -z[81] + z[91];
z[91] = abb[9] * z[81];
z[95] = -abb[28] + -z[43];
z[95] = z[91] * z[95];
z[15] = z[15] + z[27] + z[37] + z[46] + z[61] + z[95];
z[15] = abb[33] * z[15];
z[27] = 2 * abb[48] + -abb[52] + -z[62] + z[71];
z[37] = z[27] * z[89];
z[27] = abb[4] * z[27];
z[40] = z[40] + -z[74];
z[46] = abb[3] * z[40];
z[61] = 3 * z[46];
z[37] = 2 * z[27] + z[37] + z[61] + -z[65] + z[76];
z[37] = abb[29] * z[37];
z[65] = 7 * abb[46];
z[71] = 5 * abb[52] + -z[65];
z[95] = 9 * abb[45];
z[96] = 7 * abb[48];
z[97] = -z[8] + z[71] + z[95] + -z[96];
z[97] = (T(1) / T(2)) * z[97];
z[98] = -abb[47] + z[97];
z[98] = -abb[51] + (T(1) / T(2)) * z[98];
z[99] = -abb[6] * z[98];
z[100] = 5 * abb[48];
z[71] = -z[63] + z[71] + z[100];
z[71] = -abb[47] + (T(1) / T(2)) * z[71];
z[71] = -abb[51] + (T(1) / T(2)) * z[71];
z[71] = abb[5] * z[71];
z[101] = 7 * abb[52];
z[63] = -17 * abb[46] + z[63] + z[96] + z[101];
z[96] = 5 * abb[47];
z[63] = (T(1) / T(2)) * z[63] + -z[96];
z[102] = 5 * abb[51];
z[63] = (T(1) / T(2)) * z[63] + -z[102];
z[63] = abb[4] * z[63];
z[33] = z[33] + z[57] + z[63] + z[71] + z[82] + z[99];
z[33] = abb[32] * z[33];
z[63] = 9 * abb[49];
z[62] = -z[62] + z[63] + z[88] + z[100] + -z[101];
z[62] = (T(1) / T(2)) * z[62] + -z[74];
z[62] = abb[15] * z[62];
z[62] = z[62] + -z[84];
z[71] = -z[74] + z[97];
z[82] = abb[4] + abb[6];
z[71] = z[71] * z[82];
z[64] = 2 * abb[47] + 4 * abb[51] + -z[64];
z[64] = abb[5] * z[64];
z[64] = z[61] + -z[62] + z[64] + -z[71];
z[71] = -abb[30] * z[64];
z[83] = abb[5] * z[83];
z[77] = z[77] + z[83] + -z[85] + 2 * z[93];
z[83] = -abb[31] * z[77];
z[33] = z[33] + z[37] + z[71] + z[83];
z[33] = abb[32] * z[33];
z[71] = -z[82] * z[98];
z[19] = -z[19] + -z[63] + -z[95];
z[19] = (T(1) / T(2)) * z[19] + z[96];
z[19] = (T(1) / T(2)) * z[19] + z[52] + z[102];
z[19] = abb[5] * z[19];
z[14] = z[14] + z[19] + z[57] + z[61] + z[71];
z[14] = abb[30] * z[14];
z[14] = z[14] + -z[37];
z[14] = abb[30] * z[14];
z[19] = 2 * abb[50];
z[1] = z[1] + -z[19] + z[67] + -z[68] + -z[70] + z[74];
z[1] = abb[6] * z[1];
z[37] = abb[5] * z[81];
z[1] = z[1] + z[37] + z[62] + -z[87];
z[1] = abb[34] * z[1];
z[37] = 3 * z[0];
z[52] = -abb[44] + -z[37] + z[90];
z[52] = z[52] * z[72];
z[7] = z[7] + z[31];
z[7] = 3 * z[7];
z[57] = -abb[46] + z[7];
z[57] = abb[44] + (T(1) / T(4)) * z[57];
z[57] = abb[5] * z[57];
z[49] = z[49] + z[52] + -z[57] + -z[58] + -z[78];
z[49] = abb[29] * z[49];
z[49] = z[49] + z[53];
z[52] = abb[5] * z[59];
z[52] = -z[12] + z[52];
z[13] = -abb[46] + -z[13] + (T(1) / T(2)) * z[18];
z[13] = abb[4] * z[13];
z[18] = -z[59] * z[72];
z[13] = z[13] + z[18] + (T(1) / T(2)) * z[52] + z[54] + z[94];
z[13] = abb[31] * z[13];
z[13] = z[13] + z[49];
z[13] = abb[31] * z[13];
z[18] = abb[4] * z[22];
z[22] = -abb[45] + z[31] + z[56];
z[31] = abb[5] * z[22];
z[18] = z[18] + -z[23] + z[31];
z[31] = abb[6] * z[4];
z[5] = -z[5] + (T(1) / T(2)) * z[18] + z[31] + -z[38];
z[18] = abb[57] * z[5];
z[31] = abb[5] * z[20];
z[21] = -z[21] + z[31] + z[75];
z[31] = -z[40] * z[82];
z[31] = -z[21] + z[31] + z[46];
z[31] = abb[36] * z[31];
z[18] = z[18] + z[31];
z[31] = abb[35] * z[64];
z[39] = abb[58] * z[39];
z[40] = -abb[5] + -z[89];
z[40] = z[40] * z[59];
z[27] = z[12] + z[27] + -z[38] + z[40] + -z[75];
z[27] = prod_pow(abb[29], 2) * z[27];
z[40] = abb[37] * z[79];
z[0] = -abb[44] + z[0] + z[3];
z[0] = abb[17] * z[0];
z[4] = abb[20] * z[4];
z[22] = abb[18] * z[22];
z[0] = z[0] + -z[4] + (T(1) / T(2)) * z[22];
z[4] = abb[19] * z[20];
z[4] = (T(3) / T(2)) * z[4];
z[20] = (T(3) / T(2)) * z[0] + z[4];
z[20] = abb[59] * z[20];
z[22] = z[29] * z[44];
z[6] = z[6] + z[21] + z[22];
z[6] = 3 * z[6];
z[21] = -abb[56] * z[6];
z[22] = z[43] * z[81];
z[29] = abb[28] * z[92];
z[22] = z[22] + 3 * z[29];
z[22] = abb[28] * z[22];
z[29] = abb[34] * z[81];
z[22] = z[22] + z[29];
z[22] = abb[9] * z[22];
z[29] = -prod_pow(abb[30], 2) * z[35];
z[1] = abb[60] + abb[61] + z[1] + z[9] + z[10] + z[13] + z[14] + z[15] + z[16] + 3 * z[18] + z[20] + z[21] + z[22] + z[24] + z[27] + z[29] + z[31] + z[33] + z[39] + z[40];
z[9] = z[76] + -z[84] + 2 * z[91];
z[2] = -abb[44] + -z[2] + z[19] + z[66] + z[69];
z[2] = abb[5] * z[2];
z[10] = -z[11] * z[89];
z[2] = z[2] + -z[9] + z[10] + 4 * z[12] + -6 * z[34] + -z[73] + -z[75] + z[80];
z[2] = abb[28] * z[2];
z[3] = abb[44] + z[3] + -z[37];
z[3] = z[3] * z[72];
z[7] = -z[7] + -z[65];
z[7] = abb[44] + (T(1) / T(4)) * z[7];
z[7] = abb[5] * z[7];
z[3] = z[3] + z[7] + z[50] + z[58] + z[60] + -z[93];
z[3] = abb[31] * z[3];
z[7] = -abb[32] * z[77];
z[10] = -abb[23] + z[41];
z[11] = z[10] + -z[30];
z[11] = abb[31] * z[11];
z[12] = abb[32] * z[47];
z[13] = abb[28] * z[44];
z[11] = z[11] + -z[12] + -z[13] + -z[45];
z[12] = z[11] * z[28];
z[2] = z[2] + z[3] + z[7] + z[12] + z[49];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[21] + abb[26] * (T(1) / T(2)) + -z[10] + z[25];
z[7] = abb[53] * z[3];
z[7] = -z[7] + z[23];
z[8] = z[8] + -z[19] + z[36];
z[8] = abb[5] * z[8];
z[7] = -3 * z[7] + z[8] + z[9] + 6 * z[32] + -5 * z[38] + z[48] + -z[86];
z[8] = abb[33] * m1_set::bc<T>[0];
z[7] = z[7] * z[8];
z[5] = abb[39] * z[5];
z[9] = abb[40] * z[17];
z[0] = abb[41] * z[0];
z[0] = (T(1) / T(2)) * z[0] + z[5] + z[9];
z[5] = m1_set::bc<T>[0] * z[11];
z[9] = abb[40] * z[42];
z[10] = abb[41] * z[55];
z[11] = abb[39] * z[51];
z[9] = z[9] + -z[10] + z[11];
z[10] = abb[38] * z[44];
z[5] = -z[5] + -z[9] + z[10];
z[3] = z[3] * z[8];
z[3] = -z[3] + (T(1) / T(2)) * z[5];
z[5] = -3 * z[26];
z[3] = z[3] * z[5];
z[5] = -abb[38] * z[6];
z[6] = z[9] * z[28];
z[4] = abb[41] * z[4];
z[0] = abb[42] + abb[43] + 3 * z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_630_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.842303589037347753716178442501725703371338453839769594880634106"),stof<T>("-15.122073742449383876612057114347449851412820922669855048999498447")}, std::complex<T>{stof<T>("-5.8353936399983307679581584692371563069721225375490980027885774203"),stof<T>("-0.2944280064184412209788379563404994829815088917188172714968998916")}, std::complex<T>{stof<T>("-7.627599979258414394697256932890284247236991584379268680010266196"),stof<T>("14.761637334374721982133367462817102553647503900240402103445876479")}, std::complex<T>{stof<T>("5.8353936399983307679581584692371563069721225375490980027885774203"),stof<T>("0.2944280064184412209788379563404994829815088917188172714968998916")}, std::complex<T>{stof<T>("9.6910558069224228983851695190974950420840410831893798120008510992"),stof<T>("5.5052373750092180789417719145809011286940907604500039130053537553")}, std::complex<T>{stof<T>("-21.160959375218185046798604894489504992692371121408418086891751401"),stof<T>("-5.865673783083879973420461566111248426459407782879456858558975723")}, std::complex<T>{stof<T>("11.4699035682957621484134353753920099506083300382190382748909003019"),stof<T>("0.3604364080746618944786896515303472977653170224294529455536219678")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_630_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_630_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (-v[3] + v[5])) / tend;


		return (abb[44] + abb[45] + -abb[46] + -abb[48]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[30], 2);
z[1] = -prod_pow(abb[29], 2);
z[0] = z[0] + z[1];
z[1] = -abb[44] + -abb[45] + abb[46] + abb[48];
return 3 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_630_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_630_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(32)) * (v[2] + v[3]) * (2 * (-4 * v[0] + 8 * v[1] + 7 * v[2] + 3 * v[3] + -4 * v[4] + -6 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5])) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[1] = (-(4 + 6 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (v[2] + v[3])) / tend;


		return (3 * abb[45] + -abb[46] + -2 * abb[47] + -abb[48] + 3 * abb[49] + -4 * abb[51] + -abb[52]) * (4 * t * c[0] + -3 * c[1]) * (T(1) / T(12));
	}
	{
T z[7];
z[0] = abb[28] + abb[29];
z[1] = -2 * abb[30] + z[0];
z[2] = abb[32] * (T(-1) / T(2)) + z[1];
z[2] = abb[32] * z[2];
z[3] = -abb[34] + 2 * abb[35] + abb[37];
z[4] = abb[30] * (T(-5) / T(2)) + z[0];
z[4] = abb[30] * z[4];
z[5] = -abb[30] + abb[32];
z[6] = abb[31] * z[5];
z[2] = -z[2] + -z[3] + z[4] + z[6];
z[4] = abb[45] + abb[49];
z[4] = -abb[46] + -abb[48] + -abb[52] + 3 * z[4];
z[2] = z[2] * z[4];
z[4] = 2 * abb[31];
z[4] = z[4] * z[5];
z[0] = 5 * abb[30] + -2 * z[0];
z[0] = abb[30] * z[0];
z[1] = abb[32] + -2 * z[1];
z[1] = abb[32] * z[1];
z[0] = z[0] + -z[1] + 2 * z[3] + -z[4];
z[1] = abb[47] + 2 * abb[51];
z[0] = z[0] * z[1];
z[0] = z[0] + z[2];
return abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_630_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(8)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-2 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (3 * abb[45] + -abb[46] + -2 * abb[47] + -abb[48] + 3 * abb[49] + -4 * abb[51] + -abb[52]) * (4 * t * c[0] + c[1]) * (T(-1) / T(4));
	}
	{
T z[2];
z[0] = -abb[45] + -abb[49];
z[0] = abb[46] + 2 * abb[47] + abb[48] + 4 * abb[51] + abb[52] + 3 * z[0];
z[1] = abb[30] + -abb[32];
return abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_630_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W26(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), T{0}, T{0}};
abb[42] = SpDLog_f_4_630_W_16_Im(t, path, abb);
abb[43] = SpDLog_f_4_630_W_19_Im(t, path, abb);
abb[60] = SpDLog_f_4_630_W_16_Re(t, path, abb);
abb[61] = SpDLog_f_4_630_W_19_Re(t, path, abb);

                    
            return f_4_630_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_630_DLogXconstant_part(base_point<T>, kend);
	value += f_4_630_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_630_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_630_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_630_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_630_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_630_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_630_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
