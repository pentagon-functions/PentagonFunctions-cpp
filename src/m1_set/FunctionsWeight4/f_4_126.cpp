/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_126.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_126_abbreviated (const std::array<T,59>& abb) {
T z[67];
z[0] = abb[19] + abb[20];
z[1] = -abb[18] + -z[0];
z[2] = abb[43] * (T(1) / T(2));
z[1] = z[1] * z[2];
z[3] = abb[31] * m1_set::bc<T>[0];
z[4] = abb[41] + z[3];
z[5] = abb[32] * m1_set::bc<T>[0];
z[6] = z[4] + -z[5];
z[6] = abb[6] * z[6];
z[7] = abb[35] * m1_set::bc<T>[0];
z[8] = abb[42] + z[7];
z[9] = z[3] + z[8];
z[10] = abb[33] + abb[34];
z[11] = m1_set::bc<T>[0] * z[10];
z[11] = -z[9] + z[11];
z[11] = abb[8] * z[11];
z[12] = z[5] + -z[8];
z[12] = abb[5] * z[12];
z[13] = abb[7] + abb[8];
z[14] = abb[41] * z[13];
z[1] = z[1] + z[6] + -z[11] + z[12] + z[14];
z[6] = -abb[33] + 3 * abb[34];
z[11] = m1_set::bc<T>[0] * (T(1) / T(2));
z[6] = z[6] * z[11];
z[6] = z[5] + z[6] + -z[9];
z[6] = abb[7] * z[6];
z[9] = z[10] * z[11];
z[10] = -z[8] + z[9];
z[12] = abb[17] * z[10];
z[14] = abb[21] * z[2];
z[6] = -z[1] + z[6] + -z[12] + z[14];
z[15] = -abb[52] * z[6];
z[16] = -abb[44] + abb[45] + abb[46] + abb[47] + -abb[48];
z[17] = -z[8] * z[16];
z[18] = z[3] * z[16];
z[19] = abb[34] * z[16];
z[20] = -abb[33] * z[16];
z[21] = z[19] + -z[20];
z[22] = m1_set::bc<T>[0] * z[21];
z[23] = z[17] + -z[18] + z[22];
z[24] = abb[23] + abb[25];
z[23] = -z[23] * z[24];
z[25] = abb[33] * m1_set::bc<T>[0];
z[26] = z[8] + -z[25];
z[27] = abb[18] + abb[19];
z[27] = z[26] * z[27];
z[10] = -abb[21] * z[10];
z[2] = abb[17] * z[2];
z[28] = abb[7] * abb[43];
z[2] = z[2] + z[10] + z[27] + z[28];
z[2] = abb[55] * z[2];
z[10] = abb[34] * m1_set::bc<T>[0];
z[10] = -z[8] + z[10];
z[10] = abb[7] * z[10];
z[10] = z[10] + -z[12];
z[12] = -z[10] + -z[14];
z[12] = abb[54] * z[12];
z[14] = z[5] * z[16];
z[21] = z[11] * z[21];
z[14] = z[14] + -z[21];
z[27] = abb[24] * z[14];
z[17] = -z[17] + -z[21];
z[17] = abb[27] * z[17];
z[14] = abb[26] * z[14];
z[21] = -abb[22] * z[22];
z[22] = abb[28] * z[16];
z[28] = abb[43] * z[22];
z[21] = z[21] + z[28];
z[18] = abb[22] * z[18];
z[28] = abb[22] + z[24];
z[28] = abb[41] * z[16] * z[28];
z[9] = z[4] + -z[9];
z[29] = abb[50] + abb[52];
z[30] = abb[49] + z[29];
z[31] = abb[15] * z[30];
z[9] = -z[9] * z[31];
z[2] = z[2] + z[9] + z[12] + z[14] + z[15] + z[17] + z[18] + (T(1) / T(2)) * z[21] + z[23] + z[27] + z[28];
z[9] = abb[50] * (T(1) / T(2));
z[6] = -z[6] * z[9];
z[12] = -abb[33] + abb[34];
z[14] = -z[11] * z[12];
z[3] = z[3] + -z[5] + z[14];
z[3] = abb[7] * z[3];
z[4] = z[4] + z[5];
z[14] = 3 * abb[33];
z[15] = abb[34] + z[14];
z[15] = z[11] * z[15];
z[15] = -z[4] + z[15];
z[15] = abb[4] * z[15];
z[1] = z[1] + z[3] + z[15];
z[3] = abb[49] * (T(1) / T(2));
z[1] = z[1] * z[3];
z[15] = 5 * abb[33] + abb[34];
z[11] = z[11] * z[15];
z[4] = -z[4] + -z[8] + z[11];
z[8] = abb[52] * z[4];
z[11] = -abb[54] * z[26];
z[8] = z[8] + z[11];
z[4] = z[4] * z[9];
z[7] = abb[42] + z[7] + z[25];
z[7] = -z[5] + (T(1) / T(2)) * z[7];
z[9] = abb[53] * z[7];
z[4] = z[4] + (T(1) / T(2)) * z[8] + z[9];
z[4] = abb[4] * z[4];
z[5] = -z[5] + z[25];
z[8] = abb[8] * z[5];
z[9] = abb[21] * abb[43];
z[8] = z[8] + (T(1) / T(4)) * z[9] + (T(1) / T(2)) * z[10];
z[9] = -abb[53] * z[8];
z[7] = abb[4] * z[7];
z[7] = z[7] + -z[8];
z[7] = abb[51] * z[7];
z[8] = abb[13] * z[30];
z[5] = -z[5] * z[8];
z[10] = abb[54] + z[29];
z[10] = abb[14] * z[10];
z[11] = z[10] * z[26];
z[15] = abb[29] * abb[55];
z[17] = -abb[43] * z[15];
z[1] = z[1] + (T(1) / T(2)) * z[2] + z[4] + z[5] + z[6] + z[7] + z[9] + z[11] + z[17];
z[1] = (T(1) / T(16)) * z[1];
z[2] = prod_pow(abb[30], 2);
z[2] = (T(1) / T(2)) * z[2];
z[4] = z[2] * z[16];
z[5] = -abb[39] * z[16];
z[6] = abb[56] * z[16];
z[7] = abb[34] * z[20];
z[4] = z[4] + z[5] + z[6] + z[7];
z[9] = abb[32] * (T(1) / T(2));
z[11] = z[9] * z[16];
z[17] = abb[30] * z[16];
z[18] = z[11] + -z[17];
z[18] = z[9] * z[18];
z[21] = abb[40] * (T(1) / T(2));
z[23] = abb[57] * (T(1) / T(2));
z[25] = z[21] + z[23];
z[25] = z[16] * z[25];
z[26] = z[17] + -z[19];
z[27] = abb[35] * (T(1) / T(2));
z[28] = z[26] * z[27];
z[17] = z[17] + z[20];
z[30] = abb[31] * (T(1) / T(2));
z[32] = z[17] * z[30];
z[33] = prod_pow(m1_set::bc<T>[0], 2);
z[34] = (T(1) / T(3)) * z[33];
z[35] = z[16] * z[34];
z[36] = abb[38] * (T(1) / T(2));
z[37] = z[16] * z[36];
z[4] = (T(-1) / T(2)) * z[4] + z[18] + -z[25] + z[28] + z[32] + -z[35] + z[37];
z[4] = z[4] * z[24];
z[18] = abb[34] * (T(1) / T(2));
z[24] = -abb[33] + z[18];
z[25] = abb[34] * z[24];
z[28] = z[25] + (T(1) / T(2)) * z[33];
z[32] = abb[30] * (T(1) / T(2));
z[37] = -z[12] + z[32];
z[37] = abb[30] * z[37];
z[38] = z[28] + z[37];
z[39] = abb[30] + -abb[34];
z[40] = abb[35] * z[39];
z[41] = -abb[57] + z[40];
z[42] = z[38] + -z[41];
z[43] = -abb[19] * z[42];
z[44] = -abb[33] + z[27];
z[45] = abb[35] * z[44];
z[46] = abb[57] + z[34];
z[47] = prod_pow(abb[33], 2);
z[48] = (T(1) / T(2)) * z[47];
z[49] = z[45] + -z[46] + z[48];
z[50] = abb[36] + z[49];
z[50] = abb[18] * z[50];
z[51] = -abb[30] + z[12];
z[52] = abb[30] * z[51];
z[53] = abb[33] * abb[34];
z[54] = z[52] + z[53];
z[40] = abb[36] + -z[40] + z[46] + (T(-1) / T(2)) * z[54];
z[46] = -abb[21] * z[40];
z[55] = abb[17] * (T(1) / T(2));
z[56] = -abb[7] + -z[55];
z[56] = abb[58] * z[56];
z[43] = z[43] + z[46] + z[50] + z[56];
z[43] = abb[55] * z[43];
z[46] = z[18] * z[20];
z[46] = z[35] + z[46];
z[50] = z[17] + z[19];
z[56] = z[32] * z[50];
z[56] = z[46] + z[56];
z[57] = -z[16] * z[30];
z[50] = z[50] + z[57];
z[50] = abb[31] * z[50];
z[57] = -abb[37] + abb[38];
z[57] = z[16] * z[57];
z[19] = z[11] + -z[19];
z[19] = abb[32] * z[19];
z[19] = z[19] + z[50] + -z[56] + z[57];
z[19] = abb[24] * z[19];
z[50] = -abb[36] + -abb[57];
z[50] = z[16] * z[50];
z[57] = -z[20] + z[26];
z[58] = -abb[30] * z[57];
z[7] = -z[7] + z[58];
z[26] = abb[35] * z[26];
z[7] = (T(1) / T(2)) * z[7] + z[26] + -z[35] + z[50];
z[7] = abb[27] * z[7];
z[17] = abb[31] * z[17];
z[26] = -abb[37] * z[16];
z[6] = -z[6] + z[17] + z[26] + -z[56];
z[6] = abb[22] * z[6];
z[17] = abb[58] * (T(1) / T(2));
z[22] = -z[17] * z[22];
z[6] = z[6] + z[7] + z[19] + z[22] + z[43];
z[7] = -abb[39] + abb[56];
z[19] = z[2] + z[7] + -z[53];
z[22] = z[27] * z[39];
z[22] = z[22] + -z[23];
z[26] = -abb[30] + z[9];
z[35] = z[9] * z[26];
z[43] = abb[30] + -abb[33];
z[50] = z[30] * z[43];
z[19] = (T(1) / T(2)) * z[19] + z[21] + -z[22] + z[34] + -z[35] + -z[50];
z[19] = abb[8] * z[19];
z[25] = z[25] + (T(1) / T(6)) * z[33] + z[48];
z[35] = abb[32] + z[12];
z[56] = -z[30] + z[35];
z[56] = abb[31] * z[56];
z[58] = abb[30] + z[12];
z[59] = abb[32] * z[58];
z[2] = abb[37] + z[2];
z[60] = abb[38] + -abb[40] + z[2];
z[59] = z[25] + -z[56] + z[59] + -z[60];
z[59] = abb[1] * z[59];
z[61] = -abb[34] + z[30];
z[61] = abb[31] * z[61];
z[62] = -abb[34] + z[9];
z[62] = abb[32] * z[62];
z[61] = -abb[38] + -abb[56] + z[61] + -z[62];
z[62] = abb[6] * (T(1) / T(2));
z[61] = z[61] * z[62];
z[62] = -abb[33] + z[9];
z[62] = abb[32] * z[62];
z[63] = abb[39] + z[62];
z[45] = abb[57] + -z[45] + z[63];
z[64] = abb[5] * (T(1) / T(2));
z[45] = z[45] * z[64];
z[26] = abb[32] * z[26];
z[26] = abb[36] + abb[39] + z[26] + z[60];
z[26] = abb[3] * z[26];
z[13] = z[13] * z[36];
z[60] = abb[58] * (T(1) / T(4));
z[64] = abb[18] * z[60];
z[13] = z[13] + -z[19] + (T(-1) / T(2)) * z[26] + z[45] + z[59] + z[61] + z[64];
z[19] = -abb[30] + (T(1) / T(2)) * z[12];
z[19] = abb[30] * z[19];
z[45] = abb[32] * z[43];
z[45] = abb[40] + z[45];
z[61] = abb[56] + z[34];
z[19] = -z[19] + z[45] + z[47] + (T(-3) / T(2)) * z[53] + z[61];
z[39] = abb[35] * (T(-1) / T(4)) + abb[33] * (T(1) / T(2)) + z[39];
z[39] = abb[35] * z[39];
z[64] = abb[36] * (T(1) / T(2)) + z[23];
z[39] = z[39] + -z[64];
z[19] = (T(1) / T(2)) * z[19] + -z[39] + -z[50];
z[19] = abb[7] * z[19];
z[50] = z[27] + z[51];
z[50] = abb[35] * z[50];
z[35] = abb[32] * z[35];
z[65] = -abb[36] + abb[37] + abb[39];
z[35] = z[35] + -z[50] + z[52] + -z[65];
z[52] = -abb[32] + abb[31] * (T(1) / T(4));
z[66] = (T(1) / T(2)) * z[51] + -z[52];
z[66] = abb[31] * z[66];
z[35] = (T(-1) / T(2)) * z[35] + -z[36] + z[66];
z[35] = abb[16] * z[35];
z[25] = z[25] + z[37] + z[50];
z[36] = abb[2] * z[25];
z[55] = z[40] * z[55];
z[66] = abb[21] * z[60];
z[36] = z[36] + -z[55] + -z[66];
z[55] = abb[11] * abb[39];
z[55] = z[36] + z[55];
z[60] = z[0] * z[60];
z[19] = z[13] + -z[19] + z[35] + -z[55] + z[60];
z[19] = z[19] * z[29];
z[25] = abb[36] + z[25];
z[25] = abb[0] * z[25];
z[35] = z[34] + z[48];
z[54] = z[35] + -z[54];
z[39] = -z[39] + (T(1) / T(2)) * z[54];
z[39] = abb[7] * z[39];
z[54] = abb[10] * abb[36];
z[39] = (T(-1) / T(2)) * z[25] + z[39] + z[54];
z[36] = -z[36] + -z[39];
z[36] = abb[54] * z[36];
z[48] = -z[34] + z[48] + z[63];
z[48] = abb[8] * z[48];
z[39] = -z[39] + z[48] + -z[55];
z[39] = abb[53] * z[39];
z[48] = abb[32] * abb[34];
z[48] = z[48] + -z[56];
z[54] = z[28] + z[48];
z[8] = -z[8] * z[54];
z[10] = z[10] * z[49];
z[4] = z[4] + (T(1) / T(2)) * z[6] + z[8] + z[10] + z[19] + z[36] + z[39];
z[6] = -abb[30] * z[12];
z[6] = z[6] + -z[35] + z[53];
z[8] = -z[9] + -z[12];
z[8] = abb[32] * z[8];
z[10] = z[27] * z[44];
z[6] = (T(1) / T(2)) * z[6] + z[8] + z[10] + z[56] + -z[64];
z[6] = abb[7] * z[6];
z[8] = abb[17] * z[40];
z[8] = z[8] + -z[25];
z[10] = z[28] + -z[37];
z[10] = (T(1) / T(2)) * z[10] + z[22] + z[48];
z[10] = abb[4] * z[10];
z[19] = -abb[8] * z[54];
z[6] = z[6] + (T(1) / T(2)) * z[8] + z[10] + z[19] + -z[26] + z[59] + z[66];
z[8] = -abb[31] + abb[32];
z[10] = -abb[30] + abb[32];
z[8] = z[8] * z[10];
z[8] = abb[36] + -abb[40] + z[8];
z[8] = abb[38] + (T(1) / T(2)) * z[8];
z[8] = abb[12] * z[8];
z[6] = (T(1) / T(2)) * z[6] + z[8];
z[6] = abb[51] * z[6];
z[19] = z[12] * z[32];
z[22] = -z[47] + z[53];
z[26] = abb[31] * z[43];
z[22] = -abb[56] + -z[19] + (T(1) / T(2)) * z[22] + z[26] + -z[45];
z[22] = abb[7] * z[22];
z[28] = -abb[30] + -abb[32];
z[12] = z[12] * z[28];
z[28] = -z[30] + z[58];
z[28] = abb[31] * z[28];
z[12] = abb[38] + z[12] + z[28] + z[50] + -z[65];
z[12] = abb[16] * z[12];
z[0] = z[0] * z[17];
z[0] = z[0] + z[12] + z[22] + -z[25];
z[12] = abb[34] + -z[14];
z[12] = z[12] * z[18];
z[14] = z[32] * z[51];
z[22] = abb[30] + abb[34];
z[22] = abb[32] * z[22];
z[22] = abb[40] + z[22];
z[7] = -abb[37] + z[7];
z[12] = z[7] + z[12] + z[14] + z[22] + (T(5) / T(6)) * z[33];
z[14] = -z[24] + z[52];
z[25] = abb[31] * z[14];
z[12] = (T(1) / T(2)) * z[12] + z[25];
z[12] = abb[4] * z[12];
z[0] = (T(1) / T(2)) * z[0] + z[12] + z[13];
z[0] = z[0] * z[3];
z[3] = -z[16] * z[27];
z[3] = z[3] + z[57];
z[3] = abb[35] * z[3];
z[12] = -z[32] * z[57];
z[11] = z[11] + z[20];
z[11] = abb[32] * z[11];
z[13] = -abb[36] * z[16];
z[3] = z[3] + -z[5] + z[11] + z[12] + z[13] + -z[46];
z[3] = abb[26] * z[3];
z[5] = z[32] * z[58];
z[11] = abb[33] * z[18];
z[5] = -abb[37] + -z[5] + z[11] + z[26] + -z[61];
z[5] = -z[5] * z[31];
z[3] = z[3] + z[5];
z[5] = -abb[34] + abb[33] * (T(5) / T(2));
z[5] = abb[34] * z[5];
z[5] = z[5] + -z[7] + z[19] + -z[22] + z[41];
z[7] = z[14] * z[30];
z[5] = (T(1) / T(4)) * z[5] + -z[7] + -z[34];
z[5] = -z[5] * z[29];
z[7] = z[24] + z[27] + -z[32];
z[7] = abb[35] * z[7];
z[7] = z[7] + -z[23] + (T(1) / T(2)) * z[38] + -z[62];
z[7] = abb[53] * z[7];
z[11] = abb[54] * z[42];
z[5] = z[5] + (T(1) / T(2)) * z[7] + (T(1) / T(4)) * z[11];
z[5] = abb[4] * z[5];
z[7] = z[10] * z[30];
z[9] = abb[30] * z[9];
z[2] = abb[39] * (T(1) / T(2)) + z[2] + z[7] + -z[9] + -z[21];
z[7] = abb[49] + abb[51];
z[2] = abb[9] * z[2] * z[7];
z[7] = z[8] * z[29];
z[8] = z[15] * z[17];
z[0] = z[0] + z[2] + (T(1) / T(4)) * z[3] + (T(1) / T(2)) * z[4] + z[5] + z[6] + z[7] + z[8];
z[0] = (T(1) / T(8)) * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_126_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.093362583023066203422659849002823615353488361909731408400687877181"),stof<T>("0.030439077767393026581553427297334768777023093732100023045354378901")}, std::complex<T>{stof<T>("-0.10008051447798893754928177361011678232881992032254754429695053896"),stof<T>("0.15667203808942366193361105525774746104804654211009050740095937023")}, std::complex<T>{stof<T>("-0.086480245774553162787068570646162065900799124351248065958099029899"),stof<T>("0.054582425969257142069214373319759432577297424981320104728035288333")}, std::complex<T>{stof<T>("-0.10008051447798893754928177361011678232881992032254754429695053896"),stof<T>("0.15667203808942366193361105525774746104804654211009050740095937023")}, std::complex<T>{stof<T>("-0.095395247353522904279357618923565054364139410363038880721518458671"),stof<T>("0.054582425969257142069214373319759432577297424981320104728035288333")}, std::complex<T>{stof<T>("-0.026259555863268533604775457023865736020838253946328496338764480911"),stof<T>("0.126232960322030635352057627960412692271023448377990484355604991324")}, std::complex<T>{stof<T>("-0.05308425981936598198548838172921953686782368677692209810094997181"),stof<T>("0.076949906797383748750224446478305787075727995940786863171375303992")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_126_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_126_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.0338312628989066314987710296439447261305518220647766021407303074"),stof<T>("-0.038631280347612859422852000582678734739397174701595147393936191928")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_126_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_126_DLogXconstant_part(base_point<T>, kend);
	value += f_4_126_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_126_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_126_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_126_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_126_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_126_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_126_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
