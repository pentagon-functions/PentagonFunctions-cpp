/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_692.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_692_abbreviated (const std::array<T,36>& abb) {
T z[135];
z[0] = 6 * abb[2];
z[1] = 2 * abb[1];
z[2] = z[0] + -z[1];
z[3] = 6 * abb[5];
z[4] = 2 * abb[4];
z[5] = 7 * abb[0];
z[6] = 8 * abb[6];
z[7] = 3 * abb[3];
z[8] = 2 * abb[8];
z[9] = -z[2] + z[3] + z[4] + -z[5] + z[6] + -z[7] + -z[8];
z[9] = abb[25] * z[9];
z[10] = 10 * abb[1];
z[11] = 8 * abb[5];
z[12] = -z[6] + z[10] + -z[11];
z[13] = 5 * abb[0];
z[14] = 2 * abb[7];
z[15] = z[13] + -z[14];
z[16] = 15 * abb[3];
z[17] = 8 * abb[9];
z[18] = z[0] + -z[8] + z[12] + z[15] + z[16] + -z[17];
z[18] = abb[32] * z[18];
z[19] = 3 * abb[9];
z[20] = z[8] + -z[19];
z[21] = 4 * abb[6];
z[22] = 3 * abb[4];
z[23] = z[21] + z[22];
z[24] = abb[0] + abb[1];
z[25] = 5 * abb[3];
z[26] = -z[20] + z[23] + -z[24] + -z[25];
z[27] = 2 * abb[31];
z[26] = z[26] * z[27];
z[28] = abb[0] + z[19];
z[29] = 4 * abb[1];
z[30] = z[14] + -z[25] + z[28] + z[29];
z[30] = abb[29] * z[30];
z[31] = 15 * abb[1];
z[32] = 5 * abb[5];
z[33] = z[31] + -z[32];
z[34] = 2 * abb[3];
z[35] = abb[4] + z[34];
z[36] = 3 * z[35];
z[28] = 6 * abb[8] + z[28] + -z[33] + -z[36];
z[37] = 2 * abb[26];
z[28] = z[28] * z[37];
z[38] = -abb[3] + abb[9];
z[39] = -abb[0] + z[1];
z[40] = z[38] + -z[39];
z[40] = abb[33] * z[40];
z[41] = abb[34] + abb[35];
z[42] = abb[13] * z[41];
z[43] = 2 * z[42];
z[44] = -z[40] + z[43];
z[45] = -abb[0] + abb[9];
z[46] = -abb[8] + z[45];
z[46] = abb[30] * z[46];
z[47] = abb[12] * z[41];
z[48] = 2 * z[47];
z[49] = abb[11] * z[41];
z[50] = 3 * abb[2];
z[51] = -abb[1] + z[50];
z[52] = abb[3] + z[51];
z[53] = -abb[4] + abb[7] + -z[52];
z[53] = abb[9] + 2 * z[53];
z[53] = abb[28] * z[53];
z[9] = z[9] + z[18] + z[26] + z[28] + z[30] + z[44] + 2 * z[46] + -z[48] + -z[49] + z[53];
z[9] = abb[17] * z[9];
z[18] = z[0] + -z[19];
z[26] = 16 * abb[0];
z[6] = abb[4] + z[6];
z[28] = 7 * abb[3] + -z[1] + -z[3] + -z[6] + z[18] + z[26];
z[28] = abb[25] * z[28];
z[30] = 6 * abb[0];
z[53] = -abb[4] + z[7];
z[12] = -z[12] + -z[18] + -z[30] + -3 * z[53];
z[12] = abb[32] * z[12];
z[54] = 2 * abb[9];
z[23] = abb[1] + -z[23] + z[34] + z[54];
z[23] = z[23] * z[27];
z[55] = 5 * abb[9];
z[56] = z[29] + -z[55];
z[57] = 9 * abb[0];
z[58] = -z[25] + -z[56] + -z[57];
z[58] = abb[29] * z[58];
z[59] = abb[0] + z[7];
z[60] = 6 * abb[9];
z[59] = 2 * z[59] + -z[60];
z[33] = z[33] + z[59];
z[33] = z[33] * z[37];
z[61] = -abb[0] + z[2] + z[35] + -z[54];
z[61] = abb[28] * z[61];
z[44] = z[44] + z[49];
z[12] = z[12] + z[23] + z[28] + z[33] + -z[44] + z[58] + z[61];
z[12] = abb[16] * z[12];
z[23] = z[42] + z[49];
z[28] = z[23] + -z[40];
z[33] = -z[34] + z[54];
z[58] = -abb[0] + z[33];
z[61] = 2 * abb[5];
z[62] = 5 * abb[1];
z[63] = z[58] + -z[61] + -z[62];
z[63] = abb[29] * z[63];
z[20] = abb[3] + z[20] + -z[39];
z[20] = abb[31] * z[20];
z[20] = z[20] + -z[28] + -z[46] + z[63];
z[63] = 3 * abb[0];
z[64] = -abb[4] + z[63];
z[65] = 10 * abb[3];
z[66] = 7 * abb[9];
z[67] = -18 * abb[1] + 12 * abb[5] + z[8] + -z[64] + -z[65] + z[66];
z[67] = abb[32] * z[67];
z[68] = z[8] + -z[34];
z[69] = 3 * abb[5];
z[70] = -abb[4] + abb[9];
z[71] = -7 * abb[1] + z[5] + z[68] + -z[69] + -z[70];
z[71] = abb[25] * z[71];
z[72] = 4 * abb[0];
z[16] = -z[4] + z[16] + -z[72];
z[73] = 12 * abb[8];
z[16] = 69 * abb[1] + -17 * abb[5] + -14 * abb[9] + 2 * z[16] + -z[73];
z[16] = abb[26] * z[16];
z[74] = abb[0] + abb[4];
z[75] = -z[34] + z[74];
z[76] = -z[29] + z[75];
z[77] = abb[9] + z[76];
z[77] = abb[28] * z[77];
z[16] = z[16] + 2 * z[20] + z[67] + z[71] + z[77];
z[16] = abb[15] * z[16];
z[20] = 12 * abb[2];
z[67] = 6 * abb[3];
z[71] = z[20] + z[67];
z[78] = abb[0] + -abb[4];
z[79] = -z[14] + -z[55] + z[71] + -z[78];
z[79] = abb[28] * z[79];
z[6] = z[6] + -z[71];
z[71] = -z[5] + z[6] + z[55];
z[71] = abb[25] * z[71];
z[6] = -z[6] + z[15] + -z[19];
z[6] = abb[32] * z[6];
z[15] = -abb[6] + z[70];
z[80] = 4 * abb[31];
z[81] = z[15] * z[80];
z[82] = -z[42] + z[49];
z[83] = -abb[7] + z[45];
z[83] = abb[29] * z[83];
z[81] = z[81] + z[82] + z[83];
z[84] = 6 * abb[26];
z[85] = -abb[9] + z[74];
z[86] = z[84] * z[85];
z[6] = -z[6] + -z[71] + z[79] + 2 * z[81] + z[86];
z[71] = abb[18] * z[6];
z[9] = z[9] + z[12] + z[16] + z[71];
z[12] = 8 * abb[1];
z[16] = -abb[0] + z[55];
z[71] = abb[3] + abb[6];
z[71] = z[12] + -z[16] + z[50] + -z[61] + 2 * z[71];
z[71] = abb[32] * z[71];
z[79] = -abb[0] + abb[1];
z[81] = z[7] + -z[19] + z[32] + -z[79];
z[81] = abb[29] * z[81];
z[87] = z[22] + z[34];
z[88] = z[1] + -z[87];
z[89] = -abb[6] + -abb[9] + -z[88];
z[89] = abb[31] * z[89];
z[90] = abb[30] * z[85];
z[44] = z[44] + z[47] + z[71] + z[81] + z[89] + -z[90];
z[71] = -abb[3] + abb[4];
z[81] = 2 * abb[6];
z[89] = -z[13] + z[71] + -z[81];
z[91] = 3 * abb[1];
z[89] = -z[0] + z[60] + z[69] + 2 * z[89] + z[91];
z[89] = abb[25] * z[89];
z[92] = 20 * abb[9];
z[64] = -9 * abb[3] + z[64];
z[64] = -39 * abb[1] + -z[32] + 2 * z[64] + z[92];
z[64] = abb[26] * z[64];
z[52] = z[52] + -z[54];
z[93] = 2 * abb[0];
z[94] = -abb[6] + -z[4] + -z[52] + z[93];
z[95] = 2 * abb[28];
z[94] = z[94] * z[95];
z[44] = 2 * z[44] + z[64] + z[89] + z[94];
z[44] = abb[14] * z[44];
z[9] = 2 * z[9] + z[44];
z[9] = abb[14] * z[9];
z[44] = -abb[4] + z[67];
z[64] = 4 * abb[8];
z[89] = -abb[9] + -z[3] + z[31] + z[44] + -z[64] + -z[93];
z[89] = abb[15] * z[89];
z[94] = -abb[4] + z[8];
z[96] = -abb[3] + z[94];
z[97] = z[61] + -z[91] + z[96];
z[98] = 2 * abb[17];
z[97] = z[97] * z[98];
z[99] = z[35] + z[63];
z[100] = 6 * abb[1];
z[101] = -z[19] + z[100];
z[102] = 4 * abb[5];
z[103] = z[101] + -z[102];
z[104] = z[99] + z[103];
z[105] = abb[16] * z[104];
z[85] = abb[18] * z[85];
z[106] = 3 * z[85];
z[89] = z[89] + z[97] + z[105] + z[106];
z[97] = 4 * abb[3];
z[105] = abb[0] + z[97];
z[107] = 9 * abb[1];
z[108] = 4 * abb[9];
z[61] = z[61] + -z[105] + -z[107] + z[108];
z[61] = abb[14] * z[61];
z[61] = z[61] + 2 * z[89];
z[61] = abb[14] * z[61];
z[89] = -z[19] + z[64];
z[109] = z[75] + z[89];
z[109] = abb[17] * z[109];
z[103] = z[64] + z[78] + z[103];
z[103] = abb[15] * z[103];
z[109] = -z[103] + z[109];
z[110] = abb[3] + z[78];
z[111] = abb[16] * z[110];
z[111] = z[109] + z[111];
z[112] = 2 * abb[16];
z[111] = z[111] * z[112];
z[113] = -z[63] + z[81];
z[11] = z[11] + -z[67];
z[114] = z[11] + -z[55];
z[115] = abb[4] + -10 * abb[8] + z[100] + -z[113] + -z[114];
z[115] = abb[23] * z[115];
z[116] = -abb[5] + z[70];
z[117] = 8 * abb[22];
z[116] = z[116] * z[117];
z[115] = z[115] + -z[116];
z[116] = 11 * abb[8];
z[117] = 2 * z[35];
z[118] = z[116] + -z[117];
z[24] = -abb[6] + -z[24] + z[60] + -z[118];
z[119] = 2 * abb[21];
z[24] = z[24] * z[119];
z[120] = 10 * abb[5];
z[121] = -abb[0] + 21 * abb[1] + 2 * z[53];
z[122] = z[108] + z[120] + -z[121];
z[123] = prod_pow(abb[15], 2);
z[122] = z[122] * z[123];
z[89] = abb[3] + -z[89] + -z[93];
z[89] = abb[17] * z[89];
z[89] = z[89] + z[103];
z[89] = z[89] * z[98];
z[60] = -abb[4] + z[60];
z[103] = 5 * abb[8];
z[124] = z[60] + -z[103];
z[125] = -z[30] + z[124];
z[126] = 2 * abb[19];
z[127] = z[125] * z[126];
z[128] = z[69] + z[108];
z[129] = -z[97] + z[128];
z[130] = 11 * abb[1];
z[131] = z[129] + -z[130];
z[132] = 2 * abb[20];
z[131] = z[131] * z[132];
z[133] = abb[16] + -abb[17];
z[134] = -abb[18] + -z[133];
z[85] = z[85] * z[134];
z[24] = z[24] + z[61] + 6 * z[85] + z[89] + z[111] + -z[115] + z[122] + z[127] + z[131];
z[61] = 2 * abb[27];
z[24] = z[24] * z[61];
z[85] = z[80] + z[84];
z[89] = abb[11] + abb[13];
z[85] = z[85] * z[89];
z[41] = -z[41] * z[78];
z[89] = 3 * abb[29];
z[80] = -abb[33] + z[80] + z[89];
z[80] = abb[12] * z[80];
z[89] = -abb[33] + -z[89];
z[89] = abb[13] * z[89];
z[111] = 2 * abb[11];
z[122] = -abb[33] * z[111];
z[127] = -4 * abb[11] + -3 * abb[12];
z[127] = abb[32] * z[127];
z[131] = abb[12] + -2 * abb[13];
z[131] = abb[25] * z[131];
z[111] = -3 * abb[13] + z[111];
z[111] = abb[28] * z[111];
z[41] = z[41] + z[80] + z[85] + z[89] + z[111] + z[122] + z[127] + z[131];
z[41] = abb[24] * z[41];
z[80] = 5 * abb[6];
z[53] = -z[53] + z[80];
z[85] = -z[0] + z[53] + z[54] + -z[72];
z[85] = abb[25] * z[85];
z[89] = 5 * abb[7] + -z[20];
z[111] = -abb[4] + -z[7];
z[111] = abb[6] + z[19] + z[89] + z[93] + 2 * z[111];
z[111] = abb[28] * z[111];
z[122] = abb[9] + z[63];
z[53] = -2 * z[53] + -z[89] + z[122];
z[53] = abb[32] * z[53];
z[15] = abb[31] * z[15];
z[53] = -9 * z[15] + z[53] + -z[82] + -5 * z[83] + 2 * z[85] + -z[86] + z[90] + z[111];
z[53] = abb[18] * z[53];
z[6] = -z[6] * z[133];
z[6] = z[6] + z[53];
z[6] = abb[18] * z[6];
z[53] = z[1] + z[81];
z[85] = 8 * abb[8];
z[89] = 9 * abb[9];
z[111] = z[53] + -z[78] + -z[85] + z[89] + -z[97];
z[111] = abb[23] * z[111];
z[127] = abb[3] + -z[70];
z[127] = abb[22] * z[127];
z[111] = z[111] + 12 * z[127];
z[111] = abb[31] * z[111];
z[53] = abb[4] + z[53];
z[127] = z[45] + z[53] + -z[64];
z[127] = abb[23] * abb[30] * z[127];
z[131] = z[19] + -z[79] + -z[97];
z[131] = abb[23] * z[131];
z[133] = abb[7] + -z[19] + z[105];
z[133] = abb[22] * z[133];
z[131] = z[131] + z[133];
z[133] = 2 * abb[29];
z[131] = z[131] * z[133];
z[82] = -z[47] + -z[82];
z[134] = 2 * abb[22];
z[82] = z[82] * z[134];
z[6] = z[6] + z[41] + z[82] + z[111] + z[127] + z[131];
z[41] = abb[4] + z[80];
z[80] = 9 * abb[2];
z[82] = z[41] + -z[80];
z[17] = abb[8] + z[17] + -z[26] + -z[65] + z[82];
z[17] = abb[25] * z[17];
z[26] = -abb[8] + z[60] + -z[80] + -z[97] + -z[113];
z[26] = abb[28] * z[26];
z[60] = 7 * abb[6];
z[65] = abb[0] + z[60];
z[80] = -18 * abb[9] + z[65] + z[85] + 2 * z[87];
z[80] = abb[31] * z[80];
z[85] = -abb[8] + z[54];
z[82] = z[30] + -z[82] + z[85];
z[82] = abb[32] * z[82];
z[111] = -z[45] + z[94];
z[111] = abb[30] * z[111];
z[111] = z[42] + z[111];
z[125] = z[37] * z[125];
z[127] = abb[3] + -z[45];
z[127] = abb[29] * z[127];
z[17] = z[17] + z[26] + z[48] + z[80] + z[82] + 2 * z[111] + z[125] + 10 * z[127];
z[17] = z[17] * z[126];
z[26] = z[7] + -z[63];
z[80] = 3 * abb[7];
z[82] = -z[4] + z[80];
z[111] = -z[19] + -z[26] + z[50] + z[82] + z[91] + z[103];
z[111] = abb[32] * z[111];
z[125] = -abb[8] + z[7] + -z[29] + z[93] + -z[108];
z[125] = abb[31] * z[125];
z[126] = z[8] + z[19];
z[50] = abb[1] + z[50];
z[87] = -z[50] + -z[63] + -z[87] + z[126];
z[87] = abb[25] * z[87];
z[127] = 12 * abb[9];
z[131] = 5 * abb[4];
z[30] = -17 * abb[8] + -z[30] + -z[100] + z[127] + z[131];
z[30] = abb[26] * z[30];
z[52] = z[8] + -z[52] + -z[82] + -z[93];
z[52] = abb[28] * z[52];
z[80] = 8 * abb[3] + -z[16] + z[29] + -z[80];
z[80] = abb[29] * z[80];
z[45] = abb[3] + -z[8] + z[45];
z[45] = abb[33] * z[45];
z[43] = -z[43] + z[45];
z[30] = z[30] + -z[43] + z[47] + z[52] + z[80] + z[87] + z[111] + z[125];
z[30] = abb[17] * z[30];
z[45] = abb[0] + -z[62] + z[64] + -z[97];
z[45] = abb[31] * z[45];
z[47] = z[58] + -z[91];
z[47] = z[47] * z[133];
z[52] = -abb[1] + abb[9];
z[58] = abb[8] + -z[52];
z[58] = abb[33] * z[58];
z[23] = z[23] + z[58];
z[45] = -z[23] + -z[45] + z[47];
z[47] = -z[32] + -z[89] + z[103] + z[121];
z[47] = z[37] * z[47];
z[3] = z[3] + -z[57] + z[68] + z[70];
z[3] = abb[25] * z[3];
z[11] = 16 * abb[1] + -z[11] + z[78] + -z[126];
z[11] = abb[32] * z[11];
z[3] = -z[3] + -z[11] + 2 * z[45] + z[47] + z[77];
z[3] = abb[15] * z[3];
z[11] = z[3] + z[30];
z[11] = z[11] * z[98];
z[30] = -z[0] + -z[7] + z[14] + z[94] + -z[101];
z[30] = abb[32] * z[30];
z[26] = -z[12] + -z[14] + -z[26] + z[55];
z[26] = abb[29] * z[26];
z[14] = z[2] + -z[14] + -z[33] + z[74];
z[14] = abb[28] * z[14];
z[33] = z[54] + -z[64] + -z[76];
z[33] = z[27] * z[33];
z[45] = z[1] + z[18];
z[47] = z[45] + -z[96];
z[47] = abb[25] * z[47];
z[57] = z[100] + -z[124];
z[57] = z[37] * z[57];
z[14] = z[14] + z[26] + z[30] + z[33] + z[43] + z[47] + z[49] + z[57];
z[14] = abb[17] * z[14];
z[26] = z[50] + -z[99];
z[26] = abb[25] * z[26];
z[30] = z[51] + -z[75];
z[30] = abb[28] * z[30];
z[33] = abb[1] + -abb[4];
z[33] = abb[31] * z[33];
z[43] = abb[29] * z[79];
z[33] = z[33] + -z[43];
z[43] = z[79] * z[84];
z[47] = -abb[1] + -abb[2] + z[74];
z[47] = abb[32] * z[47];
z[26] = z[26] + z[30] + 4 * z[33] + z[43] + 3 * z[47];
z[30] = -abb[16] * z[26];
z[3] = -z[3] + z[14] + z[30];
z[3] = z[3] * z[112];
z[14] = 12 * abb[1];
z[30] = 15 * abb[8] + -10 * abb[9] + -z[14] + -z[22] + z[72] + -z[81] + -z[97];
z[30] = abb[31] * z[30];
z[43] = z[60] + -z[63] + -z[67] + z[85] + z[130];
z[43] = abb[32] * z[43];
z[49] = -z[22] + z[64] + -z[65] + -z[91] + z[108];
z[49] = abb[25] * z[49];
z[50] = -z[53] + -z[54] + z[93] + z[103];
z[50] = abb[30] * z[50];
z[38] = -abb[1] + z[38];
z[51] = abb[29] * z[38];
z[23] = 2 * z[23] + z[30] + z[43] + z[48] + z[49] + z[50] + -16 * z[51];
z[23] = z[23] * z[119];
z[30] = 15 * abb[0];
z[43] = -z[30] + z[62] + z[102] + z[108] + -z[117];
z[43] = abb[25] * z[43];
z[49] = -abb[9] + z[62] + -z[69] + z[93];
z[49] = abb[32] * z[49];
z[27] = -z[27] * z[38];
z[27] = z[27] + -z[40] + z[42] + z[49];
z[14] = 16 * abb[3] + z[5] + z[14] + z[69] + -z[127];
z[14] = abb[29] * z[14];
z[38] = -11 * abb[3] + abb[4] + z[63];
z[32] = -53 * abb[1] + z[32] + 2 * z[38] + z[92];
z[32] = abb[26] * z[32];
z[38] = abb[25] + -abb[28] + abb[32] + -z[133];
z[40] = 2 * abb[10];
z[38] = z[38] * z[40];
z[39] = abb[3] + z[39];
z[40] = z[39] * z[95];
z[14] = z[14] + 2 * z[27] + z[32] + z[38] + z[40] + z[43] + -z[48];
z[14] = z[14] * z[132];
z[27] = 16 * abb[5] + 14 * abb[6];
z[8] = -z[8] + z[27] + -z[30] + -z[44] + -z[55];
z[8] = abb[23] * z[8];
z[30] = z[0] + z[102];
z[32] = -z[30] + z[71] + z[108];
z[40] = 4 * abb[22];
z[32] = z[32] * z[40];
z[8] = z[8] + z[32];
z[8] = abb[25] * z[8];
z[25] = 9 * abb[8] + z[1] + -z[4] + -z[25] + -z[54];
z[25] = abb[31] * z[25];
z[32] = -abb[5] + z[31] + z[59];
z[32] = abb[29] * z[32];
z[42] = 13 * abb[1] + -7 * abb[8] + -abb[9] + 2 * z[105] + -z[120];
z[42] = abb[32] * z[42];
z[25] = z[25] + z[28] + z[32] + z[42];
z[4] = -z[4] + -z[5];
z[4] = 2 * z[4] + z[107] + z[128];
z[4] = abb[25] * z[4];
z[28] = 4 * abb[4];
z[32] = -21 * abb[3] + z[13] + z[28];
z[32] = -111 * abb[1] + 39 * abb[5] + 14 * abb[8] + 2 * z[32] + z[92];
z[32] = abb[26] * z[32];
z[39] = -abb[8] + z[39];
z[39] = abb[28] * z[39];
z[4] = z[4] + 2 * z[25] + z[32] + 4 * z[39];
z[4] = z[4] * z[123];
z[25] = 11 * abb[0] + 22 * abb[3] + -z[27] + z[29] + -z[66] + z[94];
z[25] = abb[23] * z[25];
z[27] = -abb[7] + -abb[9] + -z[7] + z[30] + -z[74];
z[27] = z[27] * z[40];
z[25] = z[25] + z[27];
z[25] = abb[32] * z[25];
z[27] = -28 * abb[1] + 7 * abb[4] + -27 * abb[8] + 24 * abb[9] + -z[34] + -z[81] + z[93];
z[27] = abb[21] * z[27];
z[27] = z[27] + -z[115];
z[27] = z[27] * z[37];
z[30] = -z[19] + z[68] + -z[74] + z[81];
z[30] = abb[23] * z[30];
z[7] = abb[7] + -z[7] + -z[18] + z[78];
z[7] = z[7] * z[40];
z[32] = -abb[0] + -abb[6] + abb[8] + z[34] + z[91];
z[32] = z[32] * z[119];
z[7] = z[7] + z[30] + z[32];
z[7] = abb[28] * z[7];
z[30] = -abb[23] + z[134];
z[30] = z[30] * z[38];
z[3] = z[3] + z[4] + 2 * z[6] + z[7] + z[8] + z[9] + z[11] + z[14] + z[17] + z[23] + z[24] + z[25] + z[27] + z[30];
z[4] = z[37] * z[79];
z[6] = -abb[1] + -z[75];
z[6] = abb[2] + (T(1) / T(3)) * z[6];
z[6] = abb[28] * z[6];
z[7] = -abb[1] + z[35];
z[7] = -abb[0] + abb[2] + (T(-1) / T(3)) * z[7];
z[7] = abb[25] * z[7];
z[8] = abb[27] * z[110];
z[4] = z[4] + z[6] + z[7] + (T(-2) / T(3)) * z[8] + (T(4) / T(3)) * z[33] + z[47];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[3] = 2 * z[3] + 13 * z[4];
z[3] = 4 * z[3];
z[4] = 8 * abb[7];
z[6] = z[4] + -z[55];
z[7] = z[0] + -z[6] + -z[13] + z[36] + -z[73] + z[100];
z[7] = abb[32] * z[7];
z[8] = -abb[4] + z[34];
z[9] = -2 * z[8] + -z[16] + -z[29] + z[116];
z[9] = abb[31] * z[9];
z[11] = abb[7] + -z[52];
z[11] = abb[29] * z[11];
z[9] = z[9] + 4 * z[11] + -z[46];
z[11] = z[22] + -z[45] + -z[63] + z[68];
z[11] = abb[25] * z[11];
z[2] = z[2] + z[34];
z[4] = z[4] + z[122];
z[14] = -z[2] + z[4] + -z[64] + -z[131];
z[14] = abb[28] * z[14];
z[17] = abb[8] + -z[28] + z[63] + -z[101];
z[17] = z[17] * z[37];
z[7] = z[7] + 2 * z[9] + z[11] + z[14] + z[17];
z[7] = abb[17] * z[7];
z[0] = -12 * abb[6] + z[0] + z[1] + z[5] + -z[114] + -z[131];
z[0] = abb[32] * z[0];
z[1] = 11 * abb[5] + z[19];
z[9] = z[1] + -3 * z[8] + -z[13] + -z[31];
z[9] = z[9] * z[37];
z[11] = z[13] + z[100] + -z[129];
z[11] = abb[29] * z[11];
z[13] = z[41] + z[56];
z[13] = abb[31] * z[13];
z[11] = z[11] + z[13] + z[90];
z[13] = z[21] + -z[35];
z[13] = -13 * abb[0] + 3 * z[13] + -z[18] + z[29];
z[13] = abb[25] * z[13];
z[2] = abb[9] + -z[2] + z[22] + z[113];
z[2] = abb[28] * z[2];
z[0] = z[0] + z[2] + z[9] + 2 * z[11] + z[13];
z[0] = abb[14] * z[0];
z[2] = -z[12] + z[73] + -z[74] + z[114];
z[2] = abb[32] * z[2];
z[9] = -abb[9] + z[63];
z[11] = z[9] + -z[29] + -z[35] + z[64];
z[11] = abb[28] * z[11];
z[10] = -z[10] + -z[105] + z[128];
z[10] = abb[29] * z[10];
z[12] = z[16] + -z[118];
z[12] = abb[31] * z[12];
z[10] = z[10] + z[12] + z[46];
z[5] = z[5] + -z[88] + -z[126];
z[5] = abb[25] * z[5];
z[1] = -abb[8] + -z[1] + z[121];
z[1] = z[1] * z[37];
z[1] = z[1] + z[2] + z[5] + 2 * z[10] + z[11];
z[1] = abb[15] * z[1];
z[2] = -z[4] + z[20] + z[36] + -z[81];
z[2] = abb[28] * z[2];
z[4] = 5 * z[15] + 4 * z[83] + -z[90];
z[5] = z[8] + -z[21];
z[6] = -abb[0] + -3 * z[5] + z[6] + -z[20];
z[6] = abb[32] * z[6];
z[5] = 4 * abb[2] + z[5] + z[9];
z[5] = abb[25] * z[5];
z[2] = z[2] + 2 * z[4] + 3 * z[5] + z[6] + z[86];
z[2] = abb[18] * z[2];
z[4] = z[26] * z[112];
z[5] = -abb[14] * z[104];
z[6] = -z[110] * z[112];
z[5] = z[5] + z[6] + z[106] + -z[109];
z[5] = z[5] * z[61];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[7];
z[0] = 16 * m1_set::bc<T>[0] * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_692_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-244.37300566006329070864959755753957159630835175820259016842433718"),stof<T>("-97.39611495360723787802164301319716354697912002521745314397428377")}, std::complex<T>{stof<T>("563.8163711942118973579775722469827253703238941863211020323346863"),stof<T>("-1156.8187235267090374865077802431694655087468522700780874149346868")}, std::complex<T>{stof<T>("49.85251191333757139777124538652395794920871933704495294772108423"),stof<T>("-314.08272837986695310203234846002186406233785555068720658579423392")}, std::complex<T>{stof<T>("308.82542020037316866964613978570994685331768338800271714456136185"),stof<T>("-555.00268603052221216585177851682940330406058820722806357927231979")}, std::complex<T>{stof<T>("-176.91614773818144299751070592661821796022441492851748956945174693"),stof<T>("762.95587802213775214559433748184204838241367800183009023868186482")}, std::complex<T>{stof<T>("-28.240549396660009698402753265115480102125719254216771579574702074"),stof<T>("95.71967846624625301542642783129893430402463067444315793003585604")}, std::complex<T>{stof<T>("-237.76267683442113811496860853081744683581827530827482916597022606"),stof<T>("596.18950386583134213555496183104961645139937434069386850059782583")}, std::complex<T>{stof<T>("14.8520538069864127587193631971664287763124668233938116497513321"),stof<T>("177.17623207831157011690273731450271661097443874277824373443287184")}, stof<T>("-107.52675968961600501276538657346740664089487498594988099648885006"), stof<T>("-78.039413497218364182385893645573224160329866409059849676046121531"), stof<T>("-78.039413497218364182385893645573224160329866409059849676046121531")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[56].real()/kbase.W[56].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_692_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_692_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-108.00060044758115657960883457496514956939702239478926649817015676"),stof<T>("672.0453727514179875707391177962045413725734887562403262605272941")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,36> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W58(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[56].real()/k.W[56].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_692_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_692_DLogXconstant_part(base_point<T>, kend);
	value += f_4_692_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_692_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_692_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_692_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_692_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_692_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_692_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
