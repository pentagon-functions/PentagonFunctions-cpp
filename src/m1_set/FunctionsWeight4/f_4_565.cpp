/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_565.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_565_abbreviated (const std::array<T,32>& abb) {
T z[80];
z[0] = abb[13] + abb[10] * (T(1) / T(2));
z[1] = 3 * abb[10];
z[0] = z[0] * z[1];
z[2] = prod_pow(abb[13], 2);
z[3] = (T(11) / T(2)) * z[2];
z[4] = 2 * abb[13];
z[5] = -abb[11] + z[4];
z[6] = abb[11] * z[5];
z[7] = 2 * abb[15];
z[8] = 2 * abb[16];
z[9] = z[0] + -z[3] + z[6] + -z[7] + z[8];
z[9] = abb[2] * z[9];
z[10] = 6 * abb[14];
z[11] = 3 * abb[16];
z[12] = z[10] + -z[11];
z[13] = 3 * abb[13];
z[14] = 2 * abb[10];
z[15] = z[13] + -z[14];
z[15] = z[14] * z[15];
z[16] = -abb[12] + z[1];
z[17] = z[4] + -z[16];
z[18] = 3 * abb[11];
z[17] = 2 * z[17] + -z[18];
z[17] = abb[11] * z[17];
z[19] = prod_pow(abb[12], 2);
z[20] = 2 * z[19];
z[15] = -4 * abb[15] + -z[2] + z[12] + z[15] + z[17] + z[20];
z[15] = abb[0] * z[15];
z[17] = -abb[10] + z[4];
z[17] = abb[10] * z[17];
z[21] = z[17] + z[19];
z[22] = 3 * abb[15];
z[23] = -z[2] + z[21] + z[22];
z[24] = 5 * abb[12];
z[25] = abb[10] + -abb[13];
z[26] = 4 * abb[11] + -z[24] + 3 * z[25];
z[27] = 2 * abb[11];
z[26] = z[26] * z[27];
z[28] = 9 * abb[16];
z[10] = -z[10] + 2 * z[23] + z[26] + z[28];
z[10] = abb[9] * z[10];
z[23] = 4 * abb[6];
z[26] = abb[2] + 2 * abb[3];
z[29] = 2 * abb[8];
z[30] = abb[0] + z[29];
z[31] = 6 * abb[4];
z[32] = 10 * abb[9] + -z[31];
z[33] = z[23] + z[26] + z[30] + -z[32];
z[34] = -abb[29] * z[33];
z[35] = 2 * abb[14];
z[36] = -z[19] + z[35];
z[37] = prod_pow(abb[10], 2);
z[38] = z[36] + z[37];
z[39] = z[2] + z[7];
z[40] = z[8] + z[39];
z[41] = -abb[10] + abb[12];
z[42] = abb[13] + z[41];
z[43] = -abb[11] + 2 * z[42];
z[43] = abb[11] * z[43];
z[43] = z[38] + -z[40] + z[43];
z[44] = 3 * abb[4];
z[45] = z[43] * z[44];
z[46] = abb[11] * (T(1) / T(2));
z[47] = -abb[12] + z[46];
z[48] = 4 * z[25] + z[47];
z[48] = abb[11] * z[48];
z[49] = (T(1) / T(2)) * z[19];
z[50] = z[37] + z[49];
z[48] = abb[16] + -z[2] + z[48] + z[50];
z[51] = -abb[3] * z[48];
z[52] = -z[8] + (T(3) / T(2)) * z[19];
z[53] = abb[12] + abb[11] * (T(-3) / T(2));
z[53] = z[18] * z[53];
z[53] = z[52] + z[53];
z[53] = abb[8] * z[53];
z[54] = z[8] + z[19];
z[55] = 2 * abb[12];
z[56] = -abb[11] + z[55];
z[57] = abb[11] * z[56];
z[58] = -z[54] + z[57];
z[59] = 7 * abb[13];
z[60] = abb[10] * (T(3) / T(2)) + -z[59];
z[60] = abb[10] * z[60];
z[3] = z[3] + z[58] + z[60];
z[3] = abb[6] * z[3];
z[3] = z[3] + z[9] + z[10] + z[15] + z[34] + z[45] + z[51] + z[53];
z[3] = abb[25] * z[3];
z[9] = abb[4] * z[43];
z[10] = (T(1) / T(2)) * z[2];
z[15] = -abb[13] + abb[11] * (T(-5) / T(2));
z[15] = abb[11] * z[15];
z[15] = -abb[16] + -z[7] + -z[10] + z[15];
z[15] = abb[2] * z[15];
z[34] = -abb[6] * z[48];
z[46] = -abb[13] + z[46];
z[46] = abb[11] * z[46];
z[46] = abb[15] + z[46];
z[48] = z[10] + z[46];
z[38] = abb[16] + -z[38] + z[48];
z[51] = abb[5] * z[38];
z[53] = abb[11] + -abb[13];
z[60] = abb[12] + z[14];
z[61] = z[53] + z[60];
z[61] = abb[11] * z[61];
z[62] = -abb[15] + z[10];
z[50] = -abb[14] + -z[50] + -z[61] + z[62];
z[61] = -abb[0] * z[50];
z[63] = -abb[12] + abb[11] * (T(9) / T(2));
z[63] = abb[11] * z[63];
z[64] = (T(7) / T(2)) * z[19];
z[63] = abb[16] + z[63] + -z[64];
z[63] = abb[8] * z[63];
z[65] = z[27] + -z[42];
z[65] = z[27] * z[65];
z[65] = abb[15] + abb[16] + z[65];
z[65] = -abb[14] + 2 * z[65];
z[65] = abb[3] * z[65];
z[66] = -z[41] + z[53];
z[67] = z[27] * z[66];
z[67] = abb[16] + z[67];
z[68] = 2 * abb[9];
z[69] = z[67] * z[68];
z[15] = z[9] + z[15] + z[34] + z[51] + z[61] + -z[63] + z[65] + z[69];
z[15] = abb[22] * z[15];
z[34] = 4 * abb[13];
z[51] = abb[11] + z[34];
z[51] = abb[11] * z[51];
z[51] = -abb[15] + -5 * z[2] + z[8] + z[51];
z[51] = abb[2] * z[51];
z[61] = z[35] + z[62];
z[62] = 6 * abb[10];
z[65] = 6 * abb[12] + abb[11] * (T(-15) / T(2)) + z[59] + -z[62];
z[65] = abb[11] * z[65];
z[65] = -z[11] + z[61] + z[65];
z[65] = abb[3] * z[65];
z[69] = abb[11] * z[66];
z[70] = abb[14] + -abb[15];
z[28] = z[28] + 6 * z[69] + -10 * z[70];
z[28] = abb[9] * z[28];
z[71] = abb[10] * abb[13];
z[72] = abb[11] * z[25];
z[71] = -z[2] + z[71] + -z[72];
z[23] = -z[23] * z[71];
z[37] = -z[19] + z[37];
z[72] = -abb[14] + z[37];
z[40] = z[6] + -z[40];
z[72] = z[40] + -3 * z[72];
z[72] = abb[5] * z[72];
z[73] = -z[14] + -z[53];
z[73] = z[27] * z[73];
z[74] = abb[14] + -abb[16];
z[75] = abb[10] * z[4];
z[75] = -abb[15] + z[75];
z[73] = z[73] + z[74] + 2 * z[75];
z[73] = abb[0] * z[73];
z[23] = z[23] + z[28] + z[45] + z[51] + z[65] + z[72] + z[73];
z[23] = abb[26] * z[23];
z[28] = abb[10] + 6 * abb[13];
z[28] = abb[10] * z[28];
z[28] = (T(5) / T(2)) * z[19] + z[28];
z[45] = 5 * abb[16];
z[47] = abb[11] * z[47];
z[51] = -7 * z[2] + z[28] + z[45] + 5 * z[47];
z[51] = abb[6] * z[51];
z[65] = 5 * abb[13];
z[62] = -abb[12] + z[18] + z[62] + -z[65];
z[62] = abb[11] * z[62];
z[12] = 5 * abb[15] + (T(5) / T(2)) * z[2] + -z[12] + -z[28] + z[62];
z[12] = abb[0] * z[12];
z[28] = 4 * z[37] + z[48] + -z[74];
z[28] = abb[5] * z[28];
z[37] = (T(9) / T(2)) * z[2] + -z[45] + z[46];
z[37] = abb[2] * z[37];
z[42] = abb[11] + -3 * z[42];
z[42] = z[27] * z[42];
z[11] = -abb[14] + z[11] + z[42];
z[11] = abb[3] * z[11];
z[42] = -z[69] + z[70];
z[45] = -17 * abb[16] + 14 * z[42];
z[45] = abb[9] * z[45];
z[11] = -4 * z[9] + z[11] + z[12] + z[28] + z[37] + z[45] + z[51] + z[63];
z[11] = abb[28] * z[11];
z[12] = 5 * abb[10];
z[4] = -z[4] + z[12];
z[4] = abb[10] * z[4];
z[37] = 4 * abb[10];
z[45] = -z[13] + z[37];
z[48] = -abb[12] + z[27] + z[45];
z[48] = abb[11] * z[48];
z[51] = 4 * abb[14];
z[4] = z[4] + z[10] + z[22] + z[48] + -z[51] + -z[52];
z[4] = abb[0] * z[4];
z[22] = abb[11] + 2 * z[25];
z[22] = abb[11] * z[22];
z[21] = -abb[16] + z[21] + -z[22] + z[35] + -z[39];
z[22] = -z[21] * z[68];
z[35] = abb[10] * (T(-5) / T(2)) + z[13];
z[35] = abb[10] * z[35];
z[10] = abb[16] + -z[10] + z[35] + z[47] + z[49];
z[10] = abb[6] * z[10];
z[35] = -abb[3] * z[50];
z[0] = -abb[16] + -z[0] + -z[46];
z[0] = abb[2] * z[0];
z[46] = abb[12] * z[27];
z[20] = -abb[16] + -z[20] + z[46];
z[46] = -abb[8] * z[20];
z[0] = z[0] + z[4] + z[9] + z[10] + z[22] + -z[28] + z[35] + z[46];
z[0] = abb[20] * z[0];
z[4] = 4 * abb[9];
z[9] = 2 * abb[4];
z[10] = z[4] + -z[9];
z[22] = abb[2] + abb[8];
z[28] = 2 * abb[6];
z[35] = -abb[0] + -3 * abb[3] + -z[10] + z[22] + z[28];
z[35] = abb[22] * z[35];
z[46] = 2 * abb[0];
z[48] = abb[3] + z[28] + z[46];
z[49] = 2 * abb[2];
z[50] = -abb[8] + z[9] + -z[48] + z[49];
z[50] = abb[20] * z[50];
z[52] = 8 * abb[4];
z[62] = 5 * abb[6];
z[63] = -14 * abb[9] + z[52] + z[62];
z[69] = 5 * abb[3];
z[70] = z[22] + z[63] + z[69];
z[70] = abb[28] * z[70];
z[72] = abb[8] + z[49];
z[68] = abb[3] + -z[68] + z[72];
z[68] = abb[6] + 2 * z[68];
z[68] = abb[21] * z[68];
z[73] = -abb[3] + abb[9];
z[75] = z[22] + z[73];
z[62] = -z[62] + 2 * z[75];
z[62] = abb[27] * z[62];
z[26] = -3 * abb[9] + z[26] + z[44];
z[75] = 2 * abb[26];
z[26] = z[26] * z[75];
z[76] = abb[6] + -abb[8];
z[77] = z[73] + z[76];
z[78] = 2 * abb[23];
z[77] = z[77] * z[78];
z[79] = abb[21] + abb[27];
z[78] = z[78] + -z[79];
z[78] = abb[0] * z[78];
z[26] = -z[26] + -z[35] + -z[50] + z[62] + z[68] + z[70] + z[77] + -z[78];
z[35] = abb[29] * z[26];
z[50] = z[28] + -z[73];
z[50] = abb[27] * z[50];
z[62] = -abb[6] + 2 * z[73];
z[62] = abb[21] * z[62];
z[68] = -abb[23] + z[79];
z[68] = abb[0] * z[68];
z[70] = abb[23] * z[73];
z[50] = -z[50] + -z[62] + z[68] + z[70];
z[62] = 3 * abb[8];
z[32] = -z[32] + z[48] + z[62];
z[32] = abb[25] * z[32];
z[48] = abb[0] + abb[8];
z[10] = -4 * abb[3] + abb[6] + -z[10] + z[48];
z[10] = abb[22] * z[10];
z[30] = -abb[3] + abb[6] + -z[9] + z[30];
z[30] = abb[20] * z[30];
z[48] = 6 * abb[3] + z[48] + z[63];
z[48] = abb[28] * z[48];
z[63] = -abb[4] + z[73];
z[68] = 6 * abb[26];
z[63] = z[63] * z[68];
z[68] = abb[20] + abb[22] + -abb[28];
z[68] = abb[1] * z[68];
z[10] = -z[10] + z[30] + -z[32] + z[48] + 2 * z[50] + z[63] + z[68];
z[30] = -abb[30] * z[10];
z[32] = -4 * abb[12] + abb[11] * (T(11) / T(2)) + z[37] + -z[65];
z[32] = abb[11] * z[32];
z[32] = z[8] + z[32] + -z[61];
z[32] = abb[3] * z[32];
z[34] = -z[1] + z[34];
z[34] = abb[10] * z[34];
z[37] = -z[2] + z[34] + -z[58];
z[37] = abb[6] * z[37];
z[48] = abb[5] * z[40];
z[50] = abb[8] * z[8];
z[58] = -z[48] + z[50];
z[6] = -z[6] + z[39];
z[6] = z[6] * z[49];
z[39] = -z[8] + z[42];
z[39] = z[4] * z[39];
z[6] = z[6] + z[32] + z[37] + z[39] + z[58];
z[6] = abb[21] * z[6];
z[32] = abb[11] + -abb[12];
z[14] = -abb[13] + z[14] + z[32];
z[14] = z[14] * z[27];
z[14] = z[7] + z[8] + z[14] + -z[19] + -z[34] + -z[51];
z[14] = abb[21] * z[14];
z[19] = abb[23] * z[43];
z[20] = -abb[27] * z[20];
z[14] = z[14] + z[19] + z[20];
z[14] = abb[0] * z[14];
z[19] = abb[6] * (T(13) / T(2));
z[20] = z[19] + -z[22];
z[20] = abb[27] * z[20];
z[19] = abb[3] + z[19] + -z[72];
z[19] = abb[21] * z[19];
z[22] = abb[0] * (T(11) / T(2));
z[34] = -z[22] * z[79];
z[22] = -4 * abb[2] + -5 * abb[8] + abb[6] * (T(-1) / T(2)) + z[22] + z[69];
z[22] = abb[25] * z[22];
z[37] = -abb[23] * z[76];
z[39] = -abb[2] + abb[3];
z[42] = abb[26] * z[39];
z[19] = z[19] + z[20] + z[22] + z[34] + z[37] + -z[42];
z[20] = 7 * abb[8];
z[22] = -7 * abb[6] + z[20] + z[39];
z[34] = 4 * abb[0];
z[22] = (T(1) / T(2)) * z[22] + z[34];
z[22] = abb[28] * z[22];
z[37] = -13 * z[39] + 11 * z[76];
z[34] = -z[34] + (T(1) / T(6)) * z[37];
z[34] = abb[20] * z[34];
z[20] = abb[2] + abb[3] * (T(-1) / T(3)) + -z[20];
z[20] = abb[0] * (T(-13) / T(6)) + abb[6] * (T(5) / T(3)) + (T(1) / T(2)) * z[20];
z[20] = abb[22] * z[20];
z[19] = (T(1) / T(3)) * z[19] + z[20] + z[22] + z[34] + 4 * z[68];
z[19] = prod_pow(m1_set::bc<T>[0], 2) * z[19];
z[2] = 4 * abb[16] + -z[2] + z[7] + z[17] + -z[36] + -z[57];
z[2] = abb[9] * z[2];
z[7] = abb[3] * z[43];
z[17] = -z[28] * z[71];
z[2] = z[2] + z[7] + z[17] + -z[58];
z[2] = abb[23] * z[2];
z[7] = abb[0] + -abb[9];
z[17] = z[7] * z[21];
z[20] = abb[3] * z[38];
z[21] = -abb[2] * z[40];
z[17] = z[17] + z[20] + z[21] + -z[48];
z[17] = abb[24] * z[17];
z[20] = -z[25] + z[56];
z[20] = abb[11] * z[20];
z[20] = z[20] + -z[54];
z[20] = z[20] * z[28];
z[21] = z[67] * z[73];
z[8] = abb[2] * z[8];
z[8] = z[8] + z[20] + z[21] + z[50];
z[8] = abb[27] * z[8];
z[20] = -z[47] + z[64] + z[74];
z[20] = -z[20] * z[68];
z[0] = abb[31] + z[0] + z[2] + z[3] + z[6] + z[8] + z[11] + z[14] + z[15] + z[17] + z[19] + z[20] + z[23] + z[30] + z[35];
z[2] = -abb[25] * z[33];
z[2] = z[2] + z[26];
z[2] = abb[17] * z[2];
z[3] = abb[10] + abb[13];
z[6] = 3 * abb[2];
z[3] = z[3] * z[6];
z[4] = z[4] * z[66];
z[6] = z[18] + z[60];
z[8] = abb[3] * z[6];
z[3] = z[3] + -z[4] + -z[8];
z[4] = z[12] + -z[13] + z[32];
z[4] = abb[6] * z[4];
z[8] = z[9] * z[41];
z[9] = -z[29] * z[56];
z[11] = abb[13] + -z[12];
z[11] = 3 * abb[12] + 2 * z[11] + -z[18];
z[11] = abb[0] * z[11];
z[12] = abb[5] * z[41];
z[14] = 8 * z[12];
z[4] = z[3] + z[4] + z[8] + z[9] + z[11] + -z[14];
z[4] = abb[20] * z[4];
z[9] = -z[27] + z[55];
z[1] = -z[1] + z[9] + z[59];
z[1] = abb[6] * z[1];
z[9] = -z[9] + z[45];
z[9] = z[9] * z[46];
z[11] = -abb[11] + -abb[12];
z[11] = z[11] * z[62];
z[15] = z[31] * z[41];
z[1] = z[1] + -z[3] + z[9] + z[11] + z[15];
z[1] = abb[25] * z[1];
z[3] = abb[9] * z[66];
z[9] = abb[3] * z[41];
z[11] = -abb[6] * z[53];
z[3] = z[3] + z[9] + z[11];
z[3] = abb[23] * z[3];
z[5] = -z[5] + z[16];
z[5] = abb[21] * z[5];
z[11] = abb[27] * z[56];
z[5] = z[5] + z[11];
z[11] = abb[6] * z[5];
z[15] = abb[23] * z[41];
z[5] = -z[5] + z[15];
z[5] = abb[0] * z[5];
z[7] = z[7] * z[66];
z[7] = z[7] + -z[9];
z[7] = abb[24] * z[7];
z[3] = z[3] + z[5] + z[7] + z[11];
z[5] = abb[0] + -abb[6];
z[6] = -z[5] * z[6];
z[7] = abb[11] + 7 * abb[12];
z[9] = abb[8] * z[7];
z[6] = z[6] + z[8] + -z[9] + -2 * z[12];
z[6] = abb[22] * z[6];
z[8] = abb[10] + z[13];
z[8] = 5 * abb[11] + -2 * z[8] + -z[24];
z[5] = -z[5] * z[8];
z[8] = -z[41] * z[52];
z[5] = z[5] + z[8] + z[9] + z[14];
z[5] = abb[28] * z[5];
z[8] = -z[28] + z[46];
z[8] = z[8] * z[53];
z[9] = z[41] * z[44];
z[8] = z[8] + z[9] + -3 * z[12];
z[8] = z[8] * z[75];
z[7] = z[7] * z[68];
z[1] = z[1] + 2 * z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[18] * z[10];
z[1] = abb[19] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_565_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("38.891649656066697489526721755298504962796890101176253737205386283"),stof<T>("-3.732206932324982023385463862730598598113348519405623838421206751")}, std::complex<T>{stof<T>("8.9355918257565805235446931745427985699928989710605492634144162201"),stof<T>("7.9227530088752506129764550893352085587582499580779098372565950967")}, std::complex<T>{stof<T>("16.022932941421895497686840532348174352889417935591644281981623098"),stof<T>("-5.245539052962331886470565458773336352967775235543834861078880999")}, std::complex<T>{stof<T>("2.6989329335688356128280386944181271650101525687743445377940731922"),stof<T>("-7.1688234678097726973562900761918332883114666374365674571081310854")}, std::complex<T>{stof<T>("-2.5227027010291868203548963308375087066797685616498828632978490141"),stof<T>("9.027343662547195718527086849653584602450954462076343541626156661")}, std::complex<T>{stof<T>("-22.868716714644801991839881222950330609907472165584609455223763185"),stof<T>("-1.513332120637349863085101596042737754854426716138211022657674248")}, std::complex<T>{stof<T>("0.121058435063374396626252553158405508338306300325456954603282636"),stof<T>("24.076671325498680271351592723443014794561188193929953036645662074")}, std::complex<T>{stof<T>("-16.320221609024918686786235449087198319558108243041562911081129913"),stof<T>("-20.68965246727377140605182403813142975573290078302589426008480665")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_565_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_565_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,8> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (8 + -16 * m1_set::bc<T>[1] + 24 * m1_set::bc<T>[2] + -12 * v[0] + -23 * v[1] + -19 * v[2] + 15 * v[3] + 19 * v[4] + m1_set::bc<T>[2] * (20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + -4 * v[5] + 4 * m1_set::bc<T>[1] * (-2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5]))) / prod_pow(tend, 2);
c[1] = ((T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (-16 * v[0] + -22 * v[1] + -22 * v[2] + 14 * v[3] + 22 * v[4] + m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + -8 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + 8 * v[0] + 3 * v[1] + 3 * v[2] + -3 * v[3] + -3 * v[4] + -2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]))) / prod_pow(tend, 2);
c[3] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (-m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + 8 * (8 + 6 * v[0] + v[1] + 5 * v[2] + -5 * v[4] + -4 * v[5] + m1_set::bc<T>[1] * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])))) / prod_pow(tend, 2);
c[4] = ((T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (3 * v[0] + v[2] + -v[4] + -v[5]) + m1_set::bc<T>[1] * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5]))) / prod_pow(tend, 2);
c[5] = (-(1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -1 * v[3] + -1 * v[4])) / tend;
c[6] = ((-5 + 6 * m1_set::bc<T>[2]) * (T(-1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[7] = (2 * m1_set::bc<T>[1] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (T(1) / T(16)) * (16 * abb[23] * c[5] + 16 * abb[24] * c[6] + -2 * abb[21] * (6 * c[5] + 4 * c[6] + -7 * c[7]) + 16 * abb[25] * c[7] + abb[22] * (6 * c[5] + -4 * c[6] + 5 * c[7]) + abb[20] * (-6 * c[5] + 4 * c[6] + 11 * c[7]) + 2 * abb[26] * (-6 * c[5] + 4 * c[6] + 11 * c[7]) + -abb[28] * (10 * c[5] + 20 * c[6] + 27 * c[7]) + 16 * t * (abb[21] * c[0] + abb[22] * c[1] + abb[23] * c[2] + abb[26] * c[3] + abb[20] * (c[1] + c[3] + -c[4]) + abb[25] * c[4] + abb[28] * (5 * c[0] + -7 * c[1] + 5 * c[2] + -c[3] + c[4] * (T(-5) / T(2))) + abb[24] * (-5 * c[0] + 6 * c[1] + -6 * c[2] + c[4] * (T(5) / T(2)))));
	}
	{
T z[7];
z[0] = abb[21] + -abb[26];
z[1] = 5 * abb[24];
z[2] = 2 * abb[20];
z[3] = -abb[23] + -abb[25];
z[0] = abb[22] + abb[28] + 3 * z[0] + z[1] + z[2] + 4 * z[3];
z[0] = abb[13] * z[0];
z[2] = -abb[23] + z[2];
z[3] = abb[24] + -2 * abb[25] + z[2];
z[4] = 2 * abb[10];
z[3] = z[3] * z[4];
z[4] = abb[24] * (T(5) / T(2));
z[2] = abb[21] * (T(-5) / T(2)) + abb[22] * (T(-3) / T(2)) + abb[26] * (T(-1) / T(2)) + abb[28] * (T(7) / T(2)) + -z[2] + -z[4];
z[2] = abb[11] * z[2];
z[0] = z[0] + z[2] + -z[3];
z[0] = abb[11] * z[0];
z[2] = -abb[21] + abb[22];
z[2] = 3 * abb[23] + 4 * abb[25] + abb[26] * (T(7) / T(2)) + (T(1) / T(2)) * z[2] + -z[4];
z[2] = abb[13] * z[2];
z[2] = z[2] + z[3];
z[2] = abb[13] * z[2];
z[3] = -abb[14] + abb[16];
z[4] = abb[20] + 2 * abb[26];
z[5] = 2 * abb[21] + -4 * abb[24] + -z[4];
z[5] = z[3] * z[5];
z[4] = abb[23] + -z[4];
z[1] = abb[21] + -z[1] + 2 * z[4];
z[1] = abb[15] * z[1];
z[4] = abb[15] + z[3];
z[6] = prod_pow(abb[13], 2);
z[4] = 5 * z[4] + (T(-9) / T(2)) * z[6];
z[4] = abb[28] * z[4];
z[3] = 2 * abb[15] + z[3];
z[3] = abb[22] * z[3];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5];
return abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_565_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (2 * abb[20] + -abb[23] + abb[24] + -2 * abb[25]) * (t * c[0] + -2 * c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = abb[20] + -abb[25];
z[0] = -abb[23] + abb[24] + 2 * z[0];
z[1] = abb[11] + -abb[13];
return 2 * abb[7] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_565_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-25.654135952774907923017165637915549982274889953322985226491178667"),stof<T>("-9.565965440728155807368413847966537287851262751854237295169690448")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,32> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), f_1_1(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), f_2_2_re(k), f_2_10_re(k), T{0}};
abb[19] = SpDLog_f_4_565_W_20_Im(t, path, abb);
abb[31] = SpDLog_f_4_565_W_20_Re(t, path, abb);

                    
            return f_4_565_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_565_DLogXconstant_part(base_point<T>, kend);
	value += f_4_565_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_565_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_565_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_565_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_565_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_565_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_565_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
