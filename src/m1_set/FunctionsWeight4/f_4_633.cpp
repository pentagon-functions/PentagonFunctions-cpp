/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_633.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_633_abbreviated (const std::array<T,65>& abb) {
T z[142];
z[0] = abb[42] + -abb[49];
z[1] = abb[43] * (T(5) / T(6));
z[2] = abb[46] * (T(11) / T(12));
z[3] = abb[47] * (T(5) / T(4));
z[4] = abb[50] * (T(3) / T(4));
z[5] = abb[52] + abb[45] * (T(11) / T(4));
z[5] = abb[51] * (T(-3) / T(4)) + abb[44] * (T(1) / T(12)) + -z[0] + z[1] + -z[2] + -z[3] + z[4] + (T(1) / T(3)) * z[5];
z[5] = abb[6] * z[5];
z[6] = abb[45] * (T(7) / T(2));
z[7] = 2 * abb[52];
z[8] = z[6] + -z[7];
z[9] = abb[44] * (T(1) / T(3));
z[10] = abb[48] * (T(1) / T(2));
z[11] = abb[51] * (T(3) / T(2));
z[12] = abb[50] * (T(1) / T(2));
z[1] = abb[46] * (T(-1) / T(6)) + z[1] + (T(1) / T(3)) * z[8] + z[9] + z[10] + -z[11] + z[12];
z[1] = abb[2] * z[1];
z[8] = abb[44] * (T(1) / T(2));
z[13] = abb[46] * (T(1) / T(2));
z[14] = z[8] + z[13];
z[15] = abb[47] * (T(1) / T(2));
z[16] = z[0] + -z[12] + z[14] + z[15];
z[17] = -abb[45] + abb[51];
z[18] = (T(1) / T(2)) * z[17];
z[19] = z[16] + -z[18];
z[20] = abb[13] * z[19];
z[21] = -abb[53] + -abb[54] + abb[56];
z[22] = abb[55] + z[21];
z[23] = (T(1) / T(2)) * z[22];
z[24] = abb[23] * z[23];
z[24] = -z[20] + z[24];
z[25] = abb[45] * (T(7) / T(4));
z[26] = 2 * abb[49];
z[27] = abb[51] * (T(11) / T(4)) + z[7] + -z[25] + z[26];
z[28] = abb[47] * (T(3) / T(4));
z[2] = abb[44] * (T(-7) / T(4)) + abb[50] * (T(-5) / T(4)) + abb[42] * (T(-2) / T(3)) + abb[43] * (T(7) / T(6)) + z[2] + -z[10] + (T(1) / T(3)) * z[27] + z[28];
z[2] = abb[5] * z[2];
z[27] = -abb[47] + abb[50];
z[29] = abb[44] + z[17] + z[27];
z[30] = abb[43] + z[13];
z[29] = (T(1) / T(2)) * z[29] + -z[30];
z[31] = abb[4] * z[29];
z[32] = -abb[20] * z[21];
z[33] = abb[19] * z[21];
z[34] = z[31] + (T(5) / T(2)) * z[32] + -3 * z[33];
z[35] = abb[47] + abb[50];
z[36] = 2 * abb[48];
z[37] = z[17] + z[35] + -z[36];
z[38] = 2 * abb[44];
z[37] = 2 * z[37] + -z[38];
z[37] = abb[8] * z[37];
z[39] = abb[45] * (T(3) / T(4));
z[40] = abb[46] * (T(1) / T(4));
z[41] = abb[51] * (T(1) / T(4)) + z[40];
z[42] = -abb[52] + abb[50] * (T(1) / T(4)) + -z[39] + z[41];
z[43] = abb[44] * (T(3) / T(4));
z[3] = z[3] + z[42] + -z[43];
z[3] = abb[7] * z[3];
z[44] = abb[45] * (T(3) / T(2));
z[9] = abb[43] * (T(-11) / T(6)) + abb[46] * (T(1) / T(3)) + abb[52] * (T(4) / T(3)) + abb[51] * (T(13) / T(6)) + z[9] + -z[35] + -z[44];
z[9] = abb[3] * z[9];
z[45] = abb[43] * (T(1) / T(2));
z[46] = abb[51] * (T(1) / T(2));
z[47] = z[45] + z[46];
z[48] = abb[48] + z[35];
z[49] = -z[7] + z[48];
z[50] = abb[45] * (T(1) / T(2));
z[51] = z[47] + -z[49] + z[50];
z[51] = abb[9] * z[51];
z[52] = z[14] + z[50];
z[53] = abb[48] * (T(3) / T(2));
z[54] = -abb[51] + z[53];
z[55] = -abb[52] + z[52] + z[54];
z[55] = abb[14] * z[55];
z[15] = z[12] + z[15];
z[56] = -abb[48] + z[15];
z[57] = -z[18] + z[56];
z[58] = abb[43] + z[57];
z[59] = -z[8] + z[58];
z[59] = abb[12] * z[59];
z[60] = -abb[44] + z[17];
z[61] = -z[0] + z[60];
z[62] = abb[1] * z[61];
z[63] = abb[55] * (T(1) / T(2));
z[64] = 3 * abb[19] + abb[20] * (T(5) / T(2));
z[64] = z[63] * z[64];
z[65] = -abb[43] + z[17];
z[66] = abb[46] + abb[48] + -abb[50];
z[67] = (T(2) / T(3)) * z[65] + -z[66];
z[67] = abb[0] * z[67];
z[68] = abb[47] + -abb[48] + z[60];
z[68] = abb[11] * z[68];
z[69] = abb[21] * z[22];
z[70] = abb[22] * z[22];
z[71] = -2 * abb[8] + abb[12] * (T(1) / T(2));
z[71] = abb[46] * z[71];
z[1] = z[1] + z[2] + z[3] + z[5] + z[9] + -z[24] + (T(-1) / T(2)) * z[34] + z[37] + z[51] + z[55] + -z[59] + (T(1) / T(3)) * z[62] + z[64] + z[67] + -3 * z[68] + 2 * z[69] + (T(5) / T(4)) * z[70] + z[71];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[2] = -abb[8] + abb[12];
z[2] = z[2] * z[13];
z[5] = z[18] + z[56];
z[9] = -z[5] + z[8];
z[9] = abb[8] * z[9];
z[34] = abb[21] * z[23];
z[2] = z[2] + -z[9] + z[34] + -z[59];
z[37] = (T(3) / T(2)) * z[20];
z[51] = abb[50] * (T(3) / T(2));
z[56] = z[50] + -z[51];
z[64] = abb[44] + abb[46];
z[67] = abb[52] + (T(3) / T(2)) * z[0] + -z[45] + z[56] + z[64];
z[67] = abb[6] * z[67];
z[71] = abb[45] * (T(1) / T(4));
z[72] = abb[44] * (T(1) / T(4));
z[73] = z[71] + z[72];
z[74] = z[4] + z[28];
z[75] = abb[52] + z[41] + z[73] + -z[74];
z[75] = abb[3] * z[75];
z[76] = (T(-1) / T(2)) * z[0];
z[77] = 2 * abb[43];
z[78] = -z[53] + z[76] + z[77];
z[79] = abb[45] + abb[52];
z[80] = -z[46] + z[79];
z[81] = abb[44] + z[13] + -z[78] + -z[80];
z[81] = abb[5] * z[81];
z[82] = 3 * abb[48];
z[83] = z[64] + z[82];
z[79] = 2 * z[79];
z[84] = -3 * abb[43] + abb[51] + -z[79] + z[83];
z[85] = abb[2] * z[84];
z[86] = 3 * abb[50];
z[87] = 3 * abb[46] + z[82] + -z[86];
z[88] = -z[65] + z[87];
z[89] = abb[0] * (T(1) / T(2));
z[90] = z[88] * z[89];
z[10] = z[10] + z[15];
z[91] = abb[52] + -z[10] + z[46];
z[92] = 3 * abb[9];
z[91] = z[91] * z[92];
z[67] = (T(-3) / T(2)) * z[2] + -z[37] + -z[62] + z[67] + z[75] + z[81] + z[85] + z[90] + z[91];
z[67] = abb[26] * z[67];
z[75] = z[45] + z[76];
z[81] = abb[46] * (T(5) / T(4));
z[90] = abb[51] * (T(5) / T(4));
z[91] = z[81] + z[90];
z[93] = abb[50] * (T(9) / T(4));
z[28] = -z[7] + z[28] + z[43] + z[71] + -z[75] + -z[91] + z[93];
z[28] = abb[6] * z[28];
z[94] = z[7] + z[46] + z[56];
z[45] = z[0] + z[13] + z[45] + -z[53] + z[94];
z[45] = abb[0] * z[45];
z[95] = 2 * z[85];
z[96] = z[45] + -z[95];
z[97] = 2 * z[62];
z[37] = z[37] + z[97];
z[98] = z[32] + -z[33];
z[99] = abb[19] + abb[20];
z[99] = z[63] * z[99];
z[100] = abb[22] * z[23];
z[98] = (T(-1) / T(2)) * z[98] + z[99] + z[100];
z[99] = abb[12] * z[13];
z[59] = -z[59] + z[99];
z[99] = z[59] + -z[98];
z[100] = -z[37] + (T(3) / T(2)) * z[99];
z[101] = abb[47] * (T(3) / T(2));
z[94] = z[94] + -z[101];
z[102] = -z[14] + -z[94];
z[103] = abb[14] * z[102];
z[104] = z[100] + z[103];
z[105] = abb[48] * (T(9) / T(2));
z[106] = 4 * abb[43];
z[76] = z[76] + z[105] + -z[106];
z[43] = z[43] + z[74];
z[107] = 4 * abb[52];
z[91] = abb[45] * (T(13) / T(4)) + -z[43] + -z[76] + -z[91] + z[107];
z[91] = abb[5] * z[91];
z[28] = z[28] + z[91] + z[96] + z[104];
z[28] = abb[30] * z[28];
z[91] = abb[45] * (T(5) / T(4));
z[108] = -z[7] + z[41] + z[43] + -z[78] + -z[91];
z[108] = abb[5] * z[108];
z[109] = abb[46] * (T(3) / T(2));
z[110] = -z[51] + z[101] + z[109];
z[111] = -3 * z[0];
z[112] = abb[44] * (T(3) / T(2));
z[113] = z[110] + -z[111] + z[112];
z[114] = -abb[43] + (T(5) / T(2)) * z[17] + -z[113];
z[115] = abb[6] * (T(1) / T(2));
z[114] = z[114] * z[115];
z[87] = z[65] + z[87];
z[87] = z[87] * z[89];
z[89] = z[85] + -z[87] + -z[104] + z[108] + z[114];
z[89] = abb[27] * z[89];
z[104] = abb[6] * z[102];
z[108] = z[103] + z[104];
z[114] = 3 * z[99];
z[116] = abb[3] * z[102];
z[117] = 2 * abb[5];
z[84] = z[84] * z[117];
z[118] = -z[84] + -3 * z[85] + z[108] + z[114] + z[116];
z[119] = abb[43] + abb[45];
z[49] = -z[49] + z[119];
z[120] = -z[49] * z[92];
z[120] = z[118] + z[120];
z[120] = abb[28] * z[120];
z[121] = abb[5] * z[102];
z[122] = z[116] + z[121];
z[123] = abb[8] * z[13];
z[123] = z[9] + z[123];
z[124] = -z[98] + z[123];
z[125] = -abb[45] + 2 * abb[51] + z[7];
z[126] = -z[83] + z[125];
z[126] = abb[14] * z[126];
z[127] = 2 * z[126];
z[128] = (T(3) / T(2)) * z[69];
z[104] = z[104] + z[122] + 3 * z[124] + z[127] + -z[128];
z[124] = -abb[29] * z[104];
z[129] = abb[51] + z[7];
z[48] = z[48] + -z[129];
z[130] = abb[29] * z[48];
z[49] = abb[30] * z[49];
z[49] = z[49] + z[130];
z[49] = z[49] * z[92];
z[28] = z[28] + -z[49] + z[67] + z[89] + z[120] + z[124];
z[28] = abb[26] * z[28];
z[67] = abb[44] * (T(5) / T(2));
z[89] = z[0] + z[67];
z[120] = (T(3) / T(2)) * z[17];
z[110] = abb[43] + -z[89] + z[110] + z[120];
z[110] = abb[6] * z[110];
z[110] = z[110] + z[114];
z[89] = -z[51] + z[89] + -z[101] + z[120];
z[120] = -z[82] + -z[89];
z[120] = abb[46] * (T(-3) / T(4)) + z[77] + (T(1) / T(2)) * z[120];
z[120] = abb[5] * z[120];
z[45] = z[37] + -z[45] + -z[85] + (T(1) / T(2)) * z[110] + z[116] + z[120];
z[45] = abb[27] * z[45];
z[110] = z[7] + z[44] + -z[46];
z[12] = abb[47] * (T(-5) / T(2)) + -z[12] + -z[13] + z[110] + z[112];
z[112] = abb[7] * z[12];
z[116] = abb[20] * abb[55];
z[116] = -z[32] + z[70] + z[116];
z[120] = z[112] + (T(1) / T(2)) * z[116];
z[124] = 3 * z[120];
z[130] = abb[46] + abb[51];
z[79] = z[79] + -z[130];
z[131] = 3 * abb[47];
z[132] = -z[38] + z[131];
z[133] = z[79] + -z[132];
z[134] = z[117] * z[133];
z[135] = 2 * abb[3];
z[136] = z[133] * z[135];
z[108] = z[108] + -z[124] + z[134] + z[136];
z[134] = abb[29] * z[108];
z[137] = z[77] + -z[82];
z[79] = z[0] + z[79] + z[137];
z[79] = abb[0] * z[79];
z[133] = abb[3] * z[133];
z[138] = abb[43] + -abb[44];
z[139] = -z[0] + z[138];
z[117] = abb[6] + -z[117];
z[117] = z[117] * z[139];
z[79] = z[62] + z[79] + z[85] + z[117] + z[133];
z[79] = abb[30] * z[79];
z[45] = z[45] + z[79] + z[134];
z[45] = abb[30] * z[45];
z[79] = -z[7] + z[40];
z[53] = -z[53] + -z[73] + -z[74] + -z[79] + z[90];
z[53] = abb[14] * z[53];
z[73] = 5 * abb[43];
z[117] = -3 * abb[45] + abb[51] + z[38] + -z[73] + -z[107];
z[105] = z[13] + z[51] + z[105] + z[117];
z[105] = abb[2] * z[105];
z[47] = -abb[46] + -abb[52] + z[8] + -z[47] + z[51];
z[47] = abb[6] * z[47];
z[10] = abb[43] + -z[10] + z[80];
z[10] = z[10] * z[92];
z[80] = (T(3) / T(2)) * z[31];
z[46] = -z[46] + z[101];
z[30] = -abb[44] + z[30];
z[133] = abb[52] + -z[30] + -z[46];
z[133] = abb[3] * z[133];
z[42] = -abb[43] + abb[48] + abb[47] * (T(1) / T(4)) + z[42] + z[72];
z[42] = abb[5] * z[42];
z[10] = z[10] + 3 * z[42] + -z[47] + z[53] + -z[80] + z[105] + z[133];
z[10] = abb[28] * z[10];
z[42] = 2 * abb[46];
z[53] = z[42] + -z[86] + z[129];
z[105] = z[53] + z[138];
z[133] = abb[2] * z[105];
z[134] = z[65] * z[135];
z[135] = 3 * z[31];
z[134] = z[134] + -z[135];
z[138] = 2 * abb[6];
z[140] = z[65] * z[138];
z[133] = -z[103] + z[121] + -z[133] + z[134] + z[140];
z[140] = -abb[27] * z[133];
z[118] = -abb[30] * z[118];
z[105] = -abb[6] * z[105];
z[132] = abb[46] + z[132];
z[141] = z[77] + -z[129] + z[132];
z[141] = abb[3] * z[141];
z[83] = abb[43] + -z[83] + z[129];
z[83] = abb[2] * z[83];
z[83] = z[83] + z[105] + z[126] + z[141];
z[83] = abb[29] * z[83];
z[10] = z[10] + z[49] + z[83] + z[118] + z[140];
z[10] = abb[28] * z[10];
z[58] = -z[14] + z[58];
z[58] = abb[5] * z[58];
z[27] = -abb[46] + z[27] + -z[60];
z[60] = -z[27] * z[115];
z[83] = -z[65] + -z[66];
z[83] = abb[2] * z[83];
z[105] = abb[3] * z[29];
z[58] = -z[31] + z[58] + z[60] + z[83] + z[99] + z[105];
z[58] = abb[33] * z[58];
z[60] = abb[6] * z[19];
z[5] = z[5] + -z[14];
z[5] = abb[14] * z[5];
z[34] = -z[34] + z[123];
z[83] = z[5] + z[34] + -z[98];
z[105] = abb[0] * z[66];
z[60] = z[60] + -z[62] + z[83] + z[105];
z[105] = z[20] + -z[60];
z[105] = abb[57] * z[105];
z[29] = abb[6] * z[29];
z[27] = abb[3] * z[27];
z[66] = abb[2] * z[66];
z[27] = (T(1) / T(2)) * z[27] + -z[29] + z[31] + z[66] + z[83];
z[29] = abb[59] * z[27];
z[29] = -z[29] + z[58] + z[105];
z[31] = abb[51] * (T(7) / T(4));
z[58] = abb[47] * (T(9) / T(4));
z[66] = z[31] + -z[58];
z[83] = abb[44] * (T(5) / T(4));
z[79] = z[4] + z[39] + -z[66] + z[77] + z[79] + -z[83];
z[79] = abb[3] * z[79];
z[3] = z[3] + -z[68];
z[105] = abb[19] * z[63];
z[105] = (T(1) / T(2)) * z[33] + z[105];
z[118] = -z[105] + z[123];
z[118] = -z[3] + (T(1) / T(2)) * z[118];
z[123] = -z[7] + z[51];
z[30] = z[30] + -z[54] + -z[123];
z[30] = abb[2] * z[30];
z[30] = z[30] + -z[47] + z[55] + (T(-3) / T(4)) * z[69] + z[79] + z[80] + 3 * z[118];
z[30] = prod_pow(abb[29], 2) * z[30];
z[47] = abb[47] * (T(9) / T(2));
z[54] = abb[46] * (T(5) / T(2));
z[6] = abb[44] * (T(-7) / T(2)) + abb[51] * (T(5) / T(2)) + -z[6] + -z[7] + z[47] + -z[51] + z[54];
z[51] = abb[3] + abb[5];
z[6] = -z[6] * z[51];
z[55] = z[11] + -z[13];
z[26] = -2 * abb[42] + abb[45] * (T(-5) / T(2)) + z[26] + z[55] + -z[67] + z[101] + z[123];
z[26] = abb[6] * z[26];
z[20] = 3 * z[20];
z[53] = z[0] + z[53];
z[53] = abb[0] * z[53];
z[53] = -z[20] + z[53] + -4 * z[62];
z[6] = z[6] + z[26] + -z[53] + -z[124];
z[6] = abb[32] * z[6];
z[26] = -abb[30] * z[108];
z[31] = abb[52] + z[31] + z[74] + -z[81] + -z[82] + -z[83] + -z[91];
z[31] = abb[14] * z[31];
z[39] = -abb[52] + z[0] + z[39] + -z[40] + z[43] + -z[90];
z[39] = abb[6] * z[39];
z[3] = z[3] + (T(-1) / T(4)) * z[116];
z[40] = abb[46] + -z[0] + z[82] + -z[129];
z[40] = abb[0] * z[40];
z[43] = abb[24] * z[22];
z[3] = 3 * z[3] + z[31] + z[39] + z[40] + (T(3) / T(4)) * z[43] + z[62];
z[3] = abb[31] * z[3];
z[31] = z[61] * z[138];
z[31] = -z[31] + z[53] + z[103] + -z[122];
z[39] = -abb[26] + abb[27];
z[39] = z[31] * z[39];
z[40] = abb[30] * z[22];
z[43] = abb[27] * z[22];
z[40] = z[40] + -z[43];
z[53] = abb[26] * z[22];
z[61] = z[40] + z[53];
z[69] = abb[24] * (T(3) / T(2));
z[74] = -z[61] * z[69];
z[43] = z[43] + -z[53];
z[79] = abb[23] * (T(3) / T(2));
z[43] = z[43] * z[79];
z[3] = z[3] + z[26] + z[39] + z[43] + z[74];
z[3] = abb[31] * z[3];
z[16] = -abb[43] + z[16] + z[18];
z[16] = abb[15] * z[16];
z[18] = abb[18] * z[19];
z[19] = -z[13] + z[57];
z[0] = z[0] + z[8] + z[19];
z[0] = abb[16] * z[0];
z[21] = (T(1) / T(2)) * z[21] + z[63];
z[21] = abb[25] * z[21];
z[26] = -abb[43] + z[8];
z[19] = -z[19] + z[26];
z[19] = abb[17] * z[19];
z[0] = z[0] + z[16] + -z[18] + -z[19] + z[21];
z[16] = (T(-3) / T(2)) * z[0];
z[16] = abb[62] * z[16];
z[18] = abb[29] * z[133];
z[19] = abb[0] + -abb[2] + abb[3];
z[19] = z[19] * z[65];
z[21] = abb[5] * z[139];
z[19] = z[19] + z[21] + -z[62];
z[19] = abb[27] * z[19];
z[18] = z[18] + z[19];
z[18] = abb[27] * z[18];
z[15] = z[15] + z[36];
z[14] = -z[14] + -z[15] + z[77] + z[110];
z[14] = abb[5] * z[14];
z[15] = -z[7] + -z[11] + z[15] + z[52];
z[15] = abb[14] * z[15];
z[19] = abb[24] * z[23];
z[21] = z[15] + z[19];
z[39] = abb[9] * z[65];
z[2] = z[2] + z[14] + z[21] + z[39] + -z[85];
z[2] = 3 * z[2];
z[14] = abb[58] * z[2];
z[34] = -z[34] + z[105] + -z[112];
z[15] = z[15] + z[34];
z[39] = 3 * abb[60];
z[15] = z[15] * z[39];
z[43] = abb[29] * z[40];
z[52] = -abb[57] + -abb[59] + abb[60];
z[52] = z[22] * z[52];
z[57] = abb[28] * z[23];
z[57] = z[40] + z[57];
z[62] = -abb[28] * z[57];
z[65] = abb[28] * z[22];
z[65] = z[40] + z[65];
z[65] = abb[26] * z[65];
z[43] = z[43] + z[52] + z[62] + z[65];
z[43] = z[43] * z[69];
z[39] = z[12] * z[39];
z[8] = z[8] + z[94];
z[52] = z[8] + z[13];
z[52] = abb[61] * z[52];
z[39] = z[39] + z[52];
z[39] = z[39] * z[51];
z[9] = -z[9] + z[98];
z[62] = abb[44] + abb[46] + z[82] + -z[125];
z[65] = 2 * abb[14];
z[62] = z[62] * z[65];
z[9] = 3 * z[9] + z[62] + z[128];
z[9] = abb[61] * z[9];
z[8] = abb[64] * z[8];
z[52] = abb[6] * z[52];
z[62] = -abb[32] + -abb[57];
z[62] = z[22] * z[62];
z[61] = abb[26] * z[61];
z[23] = -abb[27] * abb[30] * z[23];
z[23] = z[23] + (T(1) / T(2)) * z[61] + z[62];
z[23] = z[23] * z[79];
z[61] = abb[8] * abb[61];
z[61] = abb[64] + -3 * z[61];
z[13] = z[13] * z[61];
z[61] = z[48] * z[92];
z[62] = -abb[60] + -abb[61];
z[62] = z[61] * z[62];
z[1] = abb[63] + z[1] + z[3] + z[6] + z[8] + z[9] + z[10] + z[13] + z[14] + z[15] + z[16] + z[18] + z[23] + z[28] + 3 * z[29] + z[30] + z[39] + z[43] + z[45] + z[52] + z[62];
z[3] = abb[50] * (T(9) / T(2)) + -z[54] + -z[107];
z[6] = abb[43] + z[3] + -z[44] + z[46] + -z[67] + z[111];
z[6] = abb[6] * z[6];
z[8] = abb[51] + z[107];
z[9] = abb[45] + z[8] + z[64] + -z[86] + -z[131];
z[10] = -abb[3] * z[9];
z[13] = -z[82] + z[106];
z[14] = z[13] + -z[89] + -z[109];
z[14] = abb[5] * z[14];
z[15] = abb[9] * z[48];
z[16] = -abb[0] * z[88];
z[6] = z[6] + z[10] + z[14] + 6 * z[15] + z[16] + z[20] + -z[95] + z[97] + z[114] + z[127];
z[6] = abb[26] * z[6];
z[10] = -3 * z[32] + z[33];
z[14] = abb[19] + 3 * abb[20];
z[14] = z[14] * z[63];
z[10] = (T(1) / T(2)) * z[10] + z[14] + -z[59] + (T(3) / T(2)) * z[70];
z[10] = (T(1) / T(2)) * z[10] + z[112];
z[9] = abb[14] * z[9];
z[14] = z[72] + -z[107];
z[16] = abb[50] * (T(-15) / T(4)) + abb[46] * (T(7) / T(4)) + -z[14] + z[66] + z[71] + z[75];
z[16] = abb[6] * z[16];
z[18] = -abb[44] + z[130];
z[4] = -8 * abb[52] + abb[45] * (T(-29) / T(4)) + abb[47] * (T(27) / T(4)) + z[4] + (T(13) / T(4)) * z[18] + z[76];
z[4] = abb[5] * z[4];
z[4] = z[4] + z[9] + 3 * z[10] + z[16] + z[37] + -z[96] + -z[136];
z[4] = abb[30] * z[4];
z[10] = -z[14] + z[25] + z[41] + -z[58] + z[78] + -z[93];
z[10] = abb[5] * z[10];
z[14] = (T(-13) / T(2)) * z[17] + z[73] + z[113];
z[14] = z[14] * z[115];
z[16] = -abb[46] + z[38] + z[86];
z[7] = abb[45] + z[7];
z[7] = 2 * z[7] + z[13] + -z[16];
z[7] = abb[2] * z[7];
z[7] = z[7] + -z[9] + z[10] + z[14] + z[87] + z[100] + -z[134];
z[7] = abb[27] * z[7];
z[3] = -z[3] + z[11] + -z[26] + z[50] + -z[101];
z[3] = abb[6] * z[3];
z[9] = -2 * z[68] + -z[120] + z[126];
z[10] = z[16] + -2 * z[129] + -z[137];
z[10] = abb[2] * z[10];
z[11] = -abb[45] + abb[52];
z[11] = 3 * abb[51] + 2 * z[11] + -z[106] + -z[132];
z[11] = abb[3] * z[11];
z[9] = -z[3] + 3 * z[9] + z[10] + z[11] + z[121] + -z[135];
z[9] = abb[29] * z[9];
z[10] = -z[47] + z[55] + z[56] + z[67] + -z[77] + z[107];
z[10] = abb[3] * z[10];
z[5] = -z[5] + -z[99];
z[8] = z[8] + -2 * z[35] + -z[36] + z[119];
z[8] = z[8] * z[92];
z[11] = 6 * abb[48] + z[42] + z[117];
z[11] = abb[2] * z[11];
z[3] = z[3] + 3 * z[5] + z[8] + z[10] + 2 * z[11] + z[84];
z[3] = abb[28] * z[3];
z[5] = abb[23] + abb[24];
z[8] = (T(3) / T(2)) * z[22];
z[5] = z[5] * z[8];
z[5] = z[5] + z[31];
z[5] = abb[31] * z[5];
z[8] = abb[24] * z[57];
z[10] = (T(-1) / T(2)) * z[40] + -z[53];
z[10] = z[10] * z[79];
z[3] = z[3] + z[4] + z[5] + z[6] + z[7] + -3 * z[8] + z[9] + z[10] + z[49];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = z[12] * z[51];
z[4] = z[4] + -z[15] + z[21] + z[34];
z[4] = abb[37] * z[4];
z[5] = -z[19] + -z[27];
z[5] = abb[36] * z[5];
z[6] = -z[19] + -z[24] + -z[60];
z[6] = abb[34] * z[6];
z[4] = z[4] + z[5] + z[6];
z[0] = -abb[39] * z[0];
z[2] = abb[35] * z[2];
z[5] = -z[61] + -z[104];
z[5] = abb[38] * z[5];
z[6] = -abb[41] * z[102];
z[0] = abb[40] + (T(3) / T(2)) * z[0] + z[2] + z[3] + 3 * z[4] + z[5] + z[6];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_633_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-16.315117475882714975232606427433962540778181982867325106331406861"),stof<T>("25.200457682612449653334884331424039000956326613344919618649303889")}, std::complex<T>{stof<T>("13.520532721999173173545069256485226228627114083772312677716535739"),stof<T>("-17.386759367193904843959491212689517294226368958474589454510441549")}, std::complex<T>{stof<T>("-2.7945847538835418016875371709487363121510678990950124286148711218"),stof<T>("7.8136983154185448093753931187345217067299576548703301641388623408")}, std::complex<T>{stof<T>("4.5363404964470617690693169990853290478736060834504222195264249611"),stof<T>("2.6010685132538211552717620307685730920260478585891584247501089139")}, std::complex<T>{stof<T>("-9.987310931381147525231323173252721588276601788362186881344933583"),stof<T>("13.472297104405035124574425314534006023606117373844394579709013962")}, std::complex<T>{stof<T>("21.590095009064260590928132338803059916851232462632860693614562856"),stof<T>("-18.700153116728953408072795080076455472820572684515625904561760222")}, std::complex<T>{stof<T>("-1.0031187058290361207555709158528244075230937880402964231548228049"),stof<T>("-6.5155307760426908746568279289240843626462994432193532995515365006")}, std::complex<T>{stof<T>("-0.7386370367344838466262089122837683281994443963151133677567310344"),stof<T>("-3.899236052629675089990327220579010436109706070240135289337434754")}, std::complex<T>{stof<T>("-7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[87])) - rlog(abs(kbase.W[87])), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_633_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_633_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + 4 * v[0] + 3 * v[1] + -v[2] + v[3] + v[4] + 2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = (-(1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -1 * v[3] + -1 * v[4])) / tend;


		return (abb[42] + -abb[44] + -abb[45] + abb[46] + 3 * abb[47] + -abb[49] + -2 * abb[52]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[26] + abb[27];
z[1] = -abb[31] + z[0];
z[1] = abb[31] * z[1];
z[0] = -abb[30] + z[0];
z[0] = abb[30] * z[0];
z[0] = -abb[32] + -z[0] + z[1];
z[1] = abb[42] + -abb[44] + -abb[45] + abb[46] + 3 * abb[47] + -abb[49] + -2 * abb[52];
return abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_633_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[42] + -abb[44] + -abb[45] + abb[46] + 3 * abb[47] + -abb[49] + -2 * abb[52]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[42] + abb[44] + abb[45] + -abb[46] + -3 * abb[47] + abb[49] + 2 * abb[52];
z[1] = abb[30] + -abb[31];
return abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_633_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.663911052344174559143628823059281987488577398391067804090459948"),stof<T>("22.959153459357400100908915587616044701579534801928719703358948151")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19, 87});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,65> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W20(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[87])) - rlog(abs(k.W[87])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}};
abb[40] = SpDLog_f_4_633_W_20_Im(t, path, abb);
abb[41] = SpDLogQ_W_88(k,dl,dlr).imag();
abb[63] = SpDLog_f_4_633_W_20_Re(t, path, abb);
abb[64] = SpDLogQ_W_88(k,dl,dlr).real();

                    
            return f_4_633_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_633_DLogXconstant_part(base_point<T>, kend);
	value += f_4_633_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_633_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_633_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_633_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_633_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_633_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_633_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
