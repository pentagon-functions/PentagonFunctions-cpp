/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_307.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_307_abbreviated (const std::array<T,32>& abb) {
T z[67];
z[0] = 3 * abb[21];
z[1] = 25 * abb[20] + -19 * abb[26];
z[1] = abb[22] * (T(19) / T(6)) + z[0] + (T(1) / T(6)) * z[1];
z[1] = abb[5] * z[1];
z[2] = 13 * abb[26];
z[3] = abb[19] + 2 * abb[25];
z[4] = -z[2] + 2 * z[3];
z[4] = -7 * abb[20] + -abb[21] + 2 * z[4];
z[5] = 9 * abb[22];
z[4] = (T(1) / T(3)) * z[4] + z[5];
z[4] = abb[9] * z[4];
z[6] = 2 * abb[21];
z[7] = 10 * abb[26] + -z[3] + -z[6];
z[8] = 3 * abb[22];
z[9] = (T(1) / T(3)) * z[7] + -z[8];
z[9] = abb[4] * z[9];
z[10] = 3 * abb[23];
z[11] = abb[25] * (T(5) / T(2));
z[12] = abb[21] * (T(-47) / T(12)) + abb[20] * (T(-11) / T(2)) + abb[24] * (T(-10) / T(3)) + abb[19] * (T(-7) / T(4)) + abb[22] * (T(-4) / T(3)) + abb[26] * (T(41) / T(12)) + z[10] + z[11];
z[12] = abb[0] * z[12];
z[13] = 7 * abb[26];
z[14] = abb[19] * (T(1) / T(2));
z[15] = abb[25] + z[14];
z[16] = -z[13] + -5 * z[15];
z[17] = abb[21] * (T(5) / T(2));
z[16] = abb[22] * (T(-1) / T(6)) + abb[20] * (T(11) / T(3)) + abb[24] * (T(59) / T(12)) + (T(1) / T(2)) * z[16] + z[17];
z[16] = abb[6] * z[16];
z[18] = abb[20] + -abb[25];
z[10] = abb[21] * (T(-65) / T(12)) + abb[19] * (T(-47) / T(12)) + abb[22] * (T(-8) / T(3)) + abb[24] * (T(5) / T(2)) + abb[26] * (T(13) / T(12)) + z[10] + (T(5) / T(12)) * z[18];
z[10] = abb[3] * z[10];
z[19] = abb[19] + abb[21];
z[20] = 2 * abb[23];
z[19] = abb[25] * (T(-53) / T(12)) + abb[26] * (T(-5) / T(4)) + abb[24] * (T(11) / T(3)) + abb[20] * (T(41) / T(12)) + (T(-5) / T(12)) * z[19] + -z[20];
z[19] = abb[1] * z[19];
z[21] = abb[21] + -abb[26];
z[22] = abb[24] + z[21];
z[22] = abb[2] * z[22];
z[23] = abb[9] * abb[24];
z[24] = abb[22] + -abb[26];
z[25] = abb[21] + z[24];
z[26] = abb[7] * z[25];
z[27] = abb[21] + abb[22];
z[27] = 5 * abb[19] + -9 * abb[24] + abb[20] * (T(-25) / T(3)) + abb[26] * (T(1) / T(3)) + abb[25] * (T(29) / T(3)) + (T(11) / T(3)) * z[27];
z[27] = abb[8] * z[27];
z[1] = z[1] + z[4] + 4 * z[9] + z[10] + z[12] + z[16] + z[19] + (T(1) / T(4)) * z[22] + (T(-5) / T(3)) * z[23] + (T(7) / T(6)) * z[26] + (T(1) / T(2)) * z[27];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[4] = 3 * abb[20];
z[9] = abb[26] * (T(-23) / T(2)) + -z[4] + z[5] + 3 * z[15] + z[17];
z[9] = abb[9] * z[9];
z[10] = -abb[20] + z[15];
z[12] = -abb[24] + z[10];
z[16] = abb[26] * (T(3) / T(2));
z[17] = abb[21] * (T(1) / T(2));
z[19] = abb[22] + -z[12] + -z[16] + z[17];
z[19] = abb[0] * z[19];
z[27] = abb[21] * (T(3) / T(2));
z[16] = z[16] + z[27];
z[28] = 2 * abb[22];
z[12] = -z[12] + z[16] + -z[28];
z[12] = abb[3] * z[12];
z[29] = abb[21] + abb[26];
z[30] = -z[28] + z[29];
z[31] = 2 * abb[20];
z[32] = z[3] + -z[31];
z[33] = 2 * abb[24];
z[34] = z[32] + -z[33];
z[35] = z[30] + -z[34];
z[36] = abb[6] * z[35];
z[37] = z[29] + z[34];
z[37] = abb[1] * z[37];
z[38] = 2 * abb[26];
z[39] = -abb[22] + 4 * abb[24] + -z[32] + -z[38];
z[39] = abb[8] * z[39];
z[5] = z[5] + -z[7];
z[5] = abb[4] * z[5];
z[40] = abb[20] + z[24];
z[41] = abb[5] * z[40];
z[42] = -z[5] + 2 * z[41];
z[9] = z[9] + z[12] + -z[19] + z[23] + -z[26] + z[36] + -z[37] + -z[39] + z[42];
z[12] = abb[31] * z[9];
z[19] = 4 * abb[21];
z[37] = -abb[22] + z[38];
z[43] = abb[19] + -z[18] + z[19] + -z[37];
z[43] = abb[3] * z[43];
z[44] = 3 * abb[26];
z[45] = -abb[19] + abb[25] * (T(-7) / T(4)) + abb[20] * (T(5) / T(4)) + -z[6] + -z[28] + z[44];
z[45] = abb[8] * z[45];
z[46] = 5 * abb[26];
z[47] = -abb[21] + -z[15] + z[46];
z[48] = abb[22] * (T(9) / T(2)) + -z[47];
z[48] = abb[4] * z[48];
z[49] = -abb[20] + abb[21] + z[38];
z[50] = -z[28] + z[49];
z[51] = abb[9] * z[50];
z[52] = -abb[23] + z[18];
z[14] = z[14] + z[52];
z[53] = abb[21] + abb[22] * (T(1) / T(2));
z[54] = z[14] + z[53];
z[54] = abb[0] * z[54];
z[55] = abb[25] * (T(5) / T(4));
z[56] = abb[23] + abb[20] * (T(-3) / T(4)) + -z[29] + z[55];
z[56] = abb[1] * z[56];
z[57] = 3 * z[26];
z[43] = z[43] + z[45] + z[48] + z[51] + z[54] + z[56] + -z[57];
z[43] = abb[12] * z[43];
z[45] = z[29] + z[52];
z[45] = abb[3] * z[45];
z[52] = abb[5] * z[50];
z[51] = -z[45] + -z[51] + z[52];
z[52] = z[6] + z[38];
z[54] = abb[20] * (T(3) / T(2));
z[11] = z[11] + -z[52] + -z[54];
z[56] = z[11] + z[20];
z[56] = abb[1] * z[56];
z[6] = abb[25] * (T(1) / T(2)) + z[6] + -z[54];
z[54] = 4 * abb[22];
z[58] = 4 * abb[26];
z[59] = z[54] + -z[58];
z[60] = z[6] + -z[59];
z[60] = abb[8] * z[60];
z[51] = 2 * z[51] + z[56] + z[60];
z[51] = abb[13] * z[51];
z[43] = z[43] + z[51];
z[43] = abb[12] * z[43];
z[51] = abb[24] * (T(3) / T(2));
z[47] = -z[8] + z[47] + -z[51];
z[47] = abb[6] * z[47];
z[27] = abb[26] * (T(9) / T(2)) + -z[8] + -z[10] + -z[27];
z[27] = abb[9] * z[27];
z[60] = abb[26] * (T(1) / T(2)) + z[17];
z[61] = abb[24] * (T(1) / T(2));
z[62] = -abb[20] + z[60] + -z[61];
z[62] = abb[0] * z[62];
z[10] = abb[24] + z[10] + -z[16];
z[10] = abb[3] * z[10];
z[10] = z[10] + -3 * z[22] + -z[23] + z[27] + -z[47] + z[57] + z[62];
z[10] = prod_pow(abb[11], 2) * z[10];
z[16] = 4 * abb[20] + -z[3];
z[21] = z[16] + -z[21];
z[21] = abb[1] * z[21];
z[22] = -abb[21] + z[24];
z[27] = 2 * abb[3];
z[22] = z[22] * z[27];
z[27] = -9 * abb[26] + z[0] + z[32];
z[57] = 8 * abb[22] + z[27];
z[57] = abb[9] * z[57];
z[40] = abb[0] * z[40];
z[21] = -z[5] + z[21] + -z[22] + -z[26] + -z[40] + 3 * z[41] + z[57];
z[22] = abb[27] * z[21];
z[40] = 2 * abb[19];
z[41] = abb[25] * (T(7) / T(2)) + z[40];
z[62] = 5 * abb[24];
z[63] = abb[21] + z[44];
z[64] = abb[20] * (T(-9) / T(2)) + z[41] + -z[62] + z[63];
z[64] = abb[8] * z[64];
z[65] = abb[20] * (T(5) / T(2));
z[29] = -abb[19] + abb[25] * (T(-3) / T(2)) + -z[29] + z[33] + z[65];
z[29] = abb[1] * z[29];
z[2] = abb[21] + 10 * abb[22] + -z[2] + z[3];
z[2] = abb[9] * z[2];
z[3] = abb[9] * z[33];
z[5] = z[3] + -z[5];
z[2] = z[2] + z[5];
z[18] = abb[24] + z[18] + z[30];
z[18] = abb[3] * z[18];
z[30] = z[34] + z[37];
z[30] = abb[0] * z[30];
z[18] = z[2] + z[18] + z[29] + z[30] + z[36] + z[64];
z[29] = abb[30] * z[18];
z[30] = abb[24] * (T(7) / T(2)) + z[4] + z[53] + -z[58];
z[30] = abb[0] * z[30];
z[34] = abb[9] * abb[22];
z[34] = -z[23] + z[34];
z[4] = -abb[21] + -z[4] + -z[59];
z[4] = abb[5] * z[4];
z[36] = -abb[22] + abb[24];
z[37] = abb[19] + -abb[23];
z[53] = -z[36] + z[37];
z[53] = abb[3] * z[53];
z[59] = abb[1] * z[37];
z[4] = z[4] + z[30] + -z[34] + z[47] + z[48] + z[53] + -z[59];
z[4] = abb[10] * z[4];
z[30] = 2 * z[37];
z[47] = -abb[22] + z[30] + -z[33] + z[63];
z[47] = abb[0] * z[47];
z[44] = z[0] + z[44];
z[31] = z[31] + z[44];
z[48] = -abb[23] + -abb[25];
z[48] = abb[19] + -z[28] + z[31] + 2 * z[48];
z[48] = abb[3] * z[48];
z[53] = 2 * abb[5];
z[50] = z[50] * z[53];
z[53] = 6 * abb[22];
z[64] = z[27] + z[53];
z[64] = abb[9] * z[64];
z[36] = abb[8] * z[36];
z[66] = 2 * z[36];
z[5] = z[5] + z[47] + z[48] + -z[50] + z[64] + -z[66];
z[5] = abb[12] * z[5];
z[37] = abb[20] + -abb[24] + abb[26] + z[37];
z[37] = abb[0] * z[37];
z[24] = abb[20] + -z[24];
z[24] = abb[5] * z[24];
z[24] = z[24] + z[34] + z[36] + -z[37] + -z[59];
z[24] = abb[13] * z[24];
z[4] = z[4] + z[5] + 2 * z[24];
z[4] = abb[10] * z[4];
z[5] = abb[26] * (T(13) / T(2)) + -z[15] + -z[17] + -z[53];
z[5] = abb[9] * z[5];
z[13] = abb[19] + abb[20] * (T(1) / T(4)) + -z[13] + z[53] + z[55];
z[13] = abb[8] * z[13];
z[14] = z[14] + z[60];
z[17] = -abb[0] + -abb[3];
z[14] = z[14] * z[17];
z[17] = 6 * abb[26];
z[24] = abb[20] + abb[21] + -z[17] + z[53];
z[24] = abb[5] * z[24];
z[34] = abb[25] * (T(-9) / T(4)) + abb[20] * (T(7) / T(4)) + -z[20] + z[38];
z[34] = abb[1] * z[34];
z[5] = z[5] + z[13] + z[14] + z[24] + 3 * z[34];
z[5] = prod_pow(abb[13], 2) * z[5];
z[13] = z[8] + z[15] + -z[51] + -z[52];
z[13] = abb[6] * z[13];
z[14] = -z[15] + z[25] + z[61];
z[14] = abb[0] * z[14];
z[15] = abb[3] * z[35];
z[2] = -z[2] + z[13] + z[14] + -z[15] + z[39];
z[13] = -abb[28] * z[2];
z[14] = -z[28] + -z[32] + z[44];
z[14] = abb[3] * z[14];
z[15] = abb[0] * z[25];
z[14] = z[14] + -z[15] + z[42] + z[57];
z[15] = abb[29] * z[14];
z[1] = z[1] + z[4] + z[5] + z[10] + z[12] + z[13] + z[15] + z[22] + z[29] + z[43];
z[4] = abb[18] * z[9];
z[5] = -z[17] + z[19] + z[28] + z[33] + z[41] + -z[65];
z[5] = abb[8] * z[5];
z[9] = abb[25] + z[20];
z[10] = 3 * abb[19];
z[9] = -2 * z[9] + z[10] + z[31] + -z[33];
z[12] = -abb[0] * z[9];
z[13] = -5 * abb[21] + z[16] + -z[28] + z[46];
z[13] = abb[9] * z[13];
z[10] = -11 * abb[21] + abb[26] + -z[10] + z[20];
z[10] = abb[3] * z[10];
z[3] = -z[3] + z[5] + z[10] + z[12] + z[13] + 6 * z[26] + z[50] + -z[56];
z[3] = abb[12] * z[3];
z[0] = -6 * abb[20] + -z[0] + -z[30] + z[46] + -z[62];
z[0] = abb[0] * z[0];
z[5] = -abb[3] * z[9];
z[9] = abb[20] + z[25];
z[9] = abb[5] * z[9];
z[9] = -z[9] + z[23];
z[10] = -z[27] + -z[54];
z[10] = abb[9] * z[10];
z[7] = 3 * abb[24] + -z[7] + z[53];
z[7] = abb[6] * z[7];
z[0] = z[0] + z[5] + z[7] + -4 * z[9] + z[10] + 2 * z[59] + z[66];
z[0] = abb[10] * z[0];
z[5] = z[8] + -z[63];
z[5] = abb[5] * z[5];
z[7] = -z[8] + z[49];
z[7] = abb[9] * z[7];
z[5] = z[5] + z[7] + z[23] + z[37] + z[45];
z[6] = -z[6] + -z[33] + z[53] + -z[58];
z[6] = abb[8] * z[6];
z[7] = -4 * abb[23] + -z[11] + z[40];
z[7] = abb[1] * z[7];
z[5] = 2 * z[5] + z[6] + z[7];
z[5] = abb[13] * z[5];
z[0] = z[0] + z[3] + z[5];
z[0] = m1_set::bc<T>[0] * z[0];
z[3] = abb[14] * z[21];
z[5] = abb[17] * z[18];
z[2] = -abb[15] * z[2];
z[6] = abb[16] * z[14];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_307_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-17.72012968785438722205258770714544914990350064154576556895656665"),stof<T>("56.162184922443557732420994294470260567582417834751550755965929777")}, std::complex<T>{stof<T>("23.864800565166368629079667224314257244000309582999029876440256492"),stof<T>("42.409270322822967271749478718969778285042969974355498050941661651")}, std::complex<T>{stof<T>("17.538530230968506036186901346430738750032027250793583332271716526"),stof<T>("84.41512831620855055361324673045556087432872499887662125985023705")}, std::complex<T>{stof<T>("-80.380782225619653594553987357584374090953452388394476169828187498"),stof<T>("-46.344315114115948111128228746402587493321442691119812336095236381")}, std::complex<T>{stof<T>("6.8410884638571165448474791539540960712997790481879135153241318485"),stof<T>("-66.540716337361129184101397416489282463718906711266876028414552998")}, std::complex<T>{stof<T>("19.913625209669944324281725720118159916833288403779148578131044244"),stof<T>("-5.140053236477388012631756790204970004502668340355552642527245435")}, std::complex<T>{stof<T>("-15.374412750417629467794532492158634379982435759501058991920224599"),stof<T>("-38.786485546924154534341568707438520225849977875581003634253743717")}, std::complex<T>{stof<T>("71.346198239946979947477370190657567252723885577973179645329578056"),stof<T>("61.862899765510907575440388658626579393960599907990690251071105037")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_307_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_307_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("93.587788238915058444769458907188494698070877704400447412140845426"),stof<T>("-34.183038333330959881527046702995885346612622942040808097163658788")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,32> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_307_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_307_DLogXconstant_part(base_point<T>, kend);
	value += f_4_307_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_307_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_307_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_307_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_307_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_307_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_307_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
