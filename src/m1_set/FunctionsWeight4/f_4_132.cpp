/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_132.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_132_abbreviated (const std::array<T,56>& abb) {
T z[88];
z[0] = abb[48] * (T(1) / T(2));
z[1] = abb[46] * (T(1) / T(2));
z[2] = -z[0] + -z[1];
z[3] = -abb[3] + abb[15];
z[4] = -abb[0] + -abb[7] + z[3];
z[2] = z[2] * z[4];
z[4] = abb[46] + abb[48] + abb[50] + -abb[51];
z[5] = abb[2] * (T(1) / T(2));
z[4] = z[4] * z[5];
z[5] = abb[7] + -abb[15];
z[6] = abb[8] + abb[14];
z[7] = -abb[11] + (T(1) / T(4)) * z[6];
z[5] = abb[3] * (T(3) / T(4)) + (T(1) / T(2)) * z[5] + z[7];
z[5] = abb[49] * z[5];
z[8] = abb[3] * (T(1) / T(4));
z[9] = z[7] + z[8];
z[9] = abb[51] * z[9];
z[10] = -abb[42] + abb[43] + abb[44] + -abb[45];
z[11] = abb[21] + abb[23];
z[12] = z[10] * z[11];
z[13] = abb[25] * z[10];
z[14] = z[12] + z[13];
z[14] = (T(1) / T(4)) * z[14];
z[15] = abb[47] + abb[50] * (T(1) / T(2));
z[16] = abb[3] * z[15];
z[17] = abb[47] + -abb[50];
z[18] = abb[5] * z[17];
z[19] = (T(1) / T(4)) * z[18];
z[20] = z[16] + z[19];
z[21] = abb[47] + abb[50];
z[22] = -abb[51] + z[21];
z[23] = abb[0] * z[22];
z[24] = abb[8] * (T(1) / T(2));
z[25] = -abb[11] + z[24];
z[25] = abb[47] * z[25];
z[15] = abb[15] * z[15];
z[26] = abb[7] * z[21];
z[2] = -z[2] + z[4] + -z[5] + -z[9] + z[14] + z[15] + -z[20] + (T(-1) / T(2)) * z[23] + -z[25] + (T(-3) / T(4)) * z[26];
z[4] = abb[41] * z[2];
z[5] = abb[48] + abb[49];
z[9] = abb[47] * (T(1) / T(2)) + z[5];
z[9] = abb[1] * z[9];
z[23] = (T(1) / T(4)) * z[21];
z[27] = abb[4] * z[23];
z[9] = z[9] + -z[27];
z[28] = abb[8] * abb[50];
z[29] = z[12] + -z[28];
z[30] = abb[8] * abb[47];
z[31] = z[29] + -z[30];
z[32] = (T(1) / T(2)) * z[31];
z[33] = abb[3] * abb[47];
z[34] = z[32] + -z[33];
z[35] = abb[6] * z[17];
z[36] = (T(1) / T(2)) * z[35];
z[37] = z[34] + -z[36];
z[38] = (T(1) / T(2)) * z[3];
z[39] = abb[49] * z[38];
z[40] = (T(1) / T(2)) * z[21];
z[41] = abb[9] * z[40];
z[42] = z[39] + -z[41];
z[43] = abb[12] * z[23];
z[44] = abb[20] * z[10];
z[43] = -z[43] + (T(1) / T(4)) * z[44];
z[45] = -abb[1] + z[3];
z[1] = z[1] * z[45];
z[1] = z[1] + z[15];
z[38] = abb[48] * z[38];
z[38] = z[1] + z[38];
z[46] = abb[47] + z[5];
z[47] = (T(1) / T(2)) * z[46];
z[48] = abb[10] * z[47];
z[49] = abb[7] * z[23];
z[37] = -z[9] + (T(1) / T(2)) * z[37] + z[38] + z[42] + z[43] + z[48] + -z[49];
z[37] = abb[29] * z[37];
z[48] = -abb[11] + abb[14] * (T(1) / T(2));
z[50] = abb[3] * (T(1) / T(2));
z[51] = z[48] + z[50];
z[52] = abb[49] * z[51];
z[51] = abb[51] * z[51];
z[22] = -abb[49] + z[22];
z[22] = abb[0] * z[22];
z[53] = (T(1) / T(2)) * z[10];
z[54] = abb[25] * z[53];
z[52] = (T(1) / T(2)) * z[22] + z[51] + z[52] + -z[54];
z[55] = abb[7] * z[40];
z[56] = abb[11] * abb[47];
z[55] = z[55] + -z[56];
z[57] = abb[22] * z[53];
z[58] = z[52] + z[55] + z[57];
z[59] = abb[10] * z[46];
z[60] = abb[1] * z[5];
z[61] = z[59] + -z[60];
z[62] = z[17] * z[50];
z[63] = abb[52] * (T(1) / T(2));
z[64] = abb[17] + -abb[19];
z[65] = z[63] * z[64];
z[66] = abb[24] * z[53];
z[62] = z[36] + z[58] + -z[61] + z[62] + z[65] + z[66];
z[65] = abb[31] * (T(1) / T(2));
z[62] = z[62] * z[65];
z[67] = z[13] + -z[29];
z[68] = z[21] * z[50];
z[24] = abb[11] + z[24];
z[24] = abb[47] * z[24];
z[67] = z[24] + -z[51] + -z[66] + (T(1) / T(2)) * z[67] + z[68];
z[68] = abb[22] * z[10];
z[69] = (T(1) / T(4)) * z[68];
z[43] = z[43] + z[69];
z[47] = abb[1] * z[47];
z[38] = z[38] + -z[47];
z[47] = z[38] + z[43];
z[70] = -z[41] + z[47];
z[71] = -abb[15] + z[50];
z[72] = -z[48] + z[71];
z[73] = abb[49] * (T(1) / T(2));
z[72] = z[72] * z[73];
z[74] = (T(1) / T(4)) * z[22];
z[67] = -z[27] + (T(1) / T(2)) * z[67] + -z[70] + z[72] + -z[74];
z[67] = abb[28] * z[67];
z[72] = z[13] + z[29];
z[25] = -z[25] + -z[51] + (T(1) / T(2)) * z[72];
z[25] = (T(1) / T(2)) * z[25];
z[48] = -abb[15] + z[48];
z[51] = abb[3] * (T(3) / T(2)) + z[48];
z[51] = z[51] * z[73];
z[38] = z[38] + -z[49];
z[72] = -z[38] + z[74];
z[20] = z[20] + -z[25] + z[51] + z[72];
z[51] = -abb[30] * z[20];
z[75] = z[33] + z[66];
z[76] = abb[12] * z[40];
z[77] = abb[20] * z[53];
z[76] = z[76] + -z[77];
z[58] = z[58] + z[75] + -z[76];
z[58] = abb[33] * z[58];
z[77] = (T(1) / T(2)) * z[58];
z[78] = abb[3] * z[21];
z[32] = z[32] + z[66] + -z[78];
z[19] = -z[19] + (T(1) / T(2)) * z[32] + z[39] + z[47];
z[32] = -abb[32] * z[19];
z[32] = z[32] + z[37] + z[51] + z[62] + z[67] + z[77];
z[32] = m1_set::bc<T>[0] * z[32];
z[39] = -abb[8] + abb[14];
z[47] = abb[49] * z[39];
z[51] = abb[51] * z[39];
z[12] = z[12] + -z[13] + z[47] + z[51];
z[47] = z[12] + z[22];
z[62] = abb[12] * z[21];
z[44] = z[44] + -z[62];
z[62] = z[44] + z[68];
z[66] = abb[24] * z[10];
z[67] = z[62] + z[66];
z[68] = abb[4] * z[21];
z[79] = abb[16] + abb[19];
z[80] = abb[52] * z[79];
z[80] = -z[47] + -z[67] + -z[68] + z[80];
z[80] = abb[39] * z[80];
z[81] = -abb[28] + -abb[30];
z[81] = z[64] * z[81];
z[82] = abb[33] * z[64];
z[81] = z[81] + z[82];
z[81] = m1_set::bc<T>[0] * z[81];
z[83] = abb[16] + z[64];
z[84] = -abb[41] * z[83];
z[81] = z[81] + z[84];
z[81] = abb[52] * z[81];
z[80] = z[80] + z[81];
z[81] = z[26] + z[29] + z[30];
z[84] = -z[35] + z[81];
z[5] = abb[7] * z[5];
z[85] = z[5] + -z[76];
z[86] = abb[4] * z[40];
z[84] = -z[60] + (T(1) / T(2)) * z[84] + z[85] + z[86];
z[84] = (T(1) / T(2)) * z[84];
z[87] = abb[40] * z[84];
z[4] = z[4] + z[32] + (T(1) / T(4)) * z[80] + z[87];
z[4] = (T(1) / T(4)) * z[4];
z[2] = -abb[55] * z[2];
z[29] = z[29] + -z[33] + -z[36];
z[0] = z[0] + z[73];
z[32] = abb[7] + z[3];
z[0] = z[0] * z[32];
z[32] = abb[13] * z[23];
z[0] = z[0] + z[1] + -z[9] + (T(1) / T(2)) * z[29] + z[32] + -z[41] + z[69] + -z[76];
z[0] = abb[29] * z[0];
z[1] = z[38] + z[42];
z[9] = z[1] + z[32] + (T(1) / T(2)) * z[34] + z[43];
z[29] = -abb[28] + abb[30];
z[9] = z[9] * z[29];
z[0] = (T(1) / T(2)) * z[0] + z[9];
z[0] = abb[29] * z[0];
z[9] = z[7] + z[50];
z[29] = abb[49] + abb[51];
z[33] = z[9] * z[29];
z[34] = 3 * abb[47];
z[36] = -abb[50] + z[34];
z[8] = z[8] * z[36];
z[38] = (T(1) / T(4)) * z[66];
z[8] = z[8] + -z[14] + -z[32] + z[33] + z[38] + z[55] + z[74];
z[8] = abb[30] * z[8];
z[14] = -abb[13] * z[40];
z[14] = z[14] + -z[57] + z[59] + (T(-1) / T(2)) * z[81] + -z[85];
z[14] = abb[29] * z[14];
z[33] = z[47] + z[66] + z[78];
z[33] = (T(1) / T(2)) * z[33] + z[62];
z[42] = abb[28] * (T(1) / T(2));
z[33] = z[33] * z[42];
z[35] = -z[35] + z[78];
z[43] = z[35] + -z[81];
z[5] = -z[5] + (T(1) / T(2)) * z[43] + -z[57] + -z[60];
z[5] = (T(1) / T(2)) * z[5] + z[59];
z[43] = -z[5] + z[32];
z[43] = abb[31] * z[43];
z[35] = z[35] + z[44];
z[35] = (T(1) / T(2)) * z[35] + z[61];
z[35] = abb[32] * z[35];
z[44] = z[42] * z[79];
z[44] = z[44] + z[82];
z[47] = abb[16] + -abb[19];
z[55] = -abb[17] + (T(-1) / T(2)) * z[47];
z[57] = -abb[30] * z[55];
z[57] = -z[44] + z[57];
z[57] = z[57] * z[63];
z[32] = abb[28] * z[32];
z[8] = z[8] + z[14] + z[32] + z[33] + z[35] + z[43] + z[57] + -z[58];
z[8] = z[8] * z[65];
z[12] = z[12] + z[22] + z[66];
z[12] = abb[53] * z[12];
z[14] = abb[20] + abb[22];
z[32] = abb[34] + abb[53];
z[14] = z[10] * z[14] * z[32];
z[29] = -z[29] * z[47];
z[32] = abb[17] + -abb[18];
z[32] = -z[21] * z[32];
z[10] = -abb[26] * z[10];
z[10] = z[10] + z[29] + z[32];
z[29] = abb[38] * (T(1) / T(2));
z[10] = z[10] * z[29];
z[32] = abb[34] * z[21];
z[33] = abb[35] * z[21];
z[35] = prod_pow(abb[28], 2) * z[40];
z[33] = z[32] + z[33] + z[35];
z[33] = abb[13] * z[33];
z[35] = abb[53] * z[21];
z[40] = -z[32] + -z[35];
z[40] = abb[12] * z[40];
z[35] = -z[32] + z[35];
z[35] = abb[4] * z[35];
z[32] = abb[3] * z[32];
z[10] = z[10] + z[12] + z[14] + z[32] + z[33] + z[35] + z[40];
z[12] = z[26] + z[67];
z[14] = -z[36] * z[50];
z[26] = abb[9] * z[21];
z[14] = -z[12] + z[14] + (T(1) / T(2)) * z[18] + -z[26] + -z[52] + z[56] + z[86];
z[32] = prod_pow(abb[33], 2);
z[32] = (T(1) / T(4)) * z[32];
z[14] = z[14] * z[32];
z[17] = abb[3] * z[17];
z[12] = -z[12] + -z[17] + z[18] + z[68];
z[12] = (T(1) / T(2)) * z[12] + -z[26];
z[17] = abb[33] * (T(1) / T(2));
z[17] = -z[12] * z[17];
z[19] = -abb[30] * z[19];
z[26] = abb[50] + z[34];
z[26] = abb[3] * z[26];
z[26] = z[26] + -z[31];
z[1] = -z[1] + (T(1) / T(4)) * z[26] + -z[27];
z[26] = -abb[28] + abb[32] * (T(-1) / T(2));
z[26] = z[1] * z[26];
z[17] = z[17] + z[19] + z[26] + -z[37];
z[17] = abb[32] * z[17];
z[11] = -z[11] * z[53];
z[11] = z[11] + z[28] + z[54];
z[9] = -abb[51] * z[9];
z[19] = abb[3] * z[23];
z[9] = z[9] + (T(1) / T(2)) * z[11] + z[19] + z[24] + -z[38];
z[11] = abb[13] * z[21];
z[11] = z[11] + z[22];
z[7] = -z[7] + z[71];
z[7] = z[7] * z[73];
z[7] = z[7] + (T(1) / T(2)) * z[9] + (T(-1) / T(8)) * z[11] + -z[70];
z[7] = abb[28] * z[7];
z[9] = abb[30] * (T(1) / T(2));
z[11] = -z[9] * z[20];
z[7] = z[7] + z[11] + z[77];
z[7] = abb[30] * z[7];
z[11] = -z[15] + z[16] + -z[25] + z[49];
z[15] = abb[1] * z[46];
z[16] = abb[48] * z[3];
z[19] = abb[46] * z[45];
z[15] = z[15] + -z[16] + -z[19];
z[16] = z[18] + z[22];
z[18] = (T(1) / T(3)) * z[48] + z[50];
z[18] = z[18] * z[73];
z[11] = (T(1) / T(3)) * z[11] + (T(1) / T(6)) * z[15] + (T(1) / T(12)) * z[16] + z[18];
z[15] = prod_pow(m1_set::bc<T>[0], 2);
z[11] = z[11] * z[15];
z[12] = abb[36] * z[12];
z[11] = z[11] + z[12];
z[12] = -z[13] + z[28] + z[30] + z[51];
z[12] = (T(-1) / T(2)) * z[12] + -z[75];
z[3] = z[3] + (T(-1) / T(2)) * z[39];
z[3] = z[3] * z[73];
z[3] = z[3] + (T(1) / T(2)) * z[12] + -z[27] + -z[41] + -z[72];
z[3] = abb[28] * z[3];
z[3] = z[3] + z[58];
z[3] = z[3] * z[42];
z[6] = -abb[0] + -z[6];
z[6] = z[6] * z[29];
z[12] = -abb[53] * z[79];
z[13] = abb[55] * z[83];
z[6] = z[6] + z[12] + z[13];
z[12] = -z[9] * z[64];
z[13] = abb[28] * z[55];
z[12] = z[12] + z[13] + z[82];
z[9] = z[9] * z[12];
z[12] = (T(1) / T(12)) * z[15] + -z[32];
z[12] = z[12] * z[64];
z[13] = z[42] * z[44];
z[15] = abb[27] * abb[38];
z[6] = (T(1) / T(2)) * z[6] + z[9] + z[12] + z[13] + z[15];
z[6] = z[6] * z[63];
z[5] = -abb[35] * z[5];
z[9] = -abb[54] * z[84];
z[1] = -abb[37] * z[1];
z[0] = z[0] + z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + z[8] + z[9] + (T(1) / T(4)) * z[10] + (T(1) / T(2)) * z[11] + z[14] + z[17];
z[0] = (T(1) / T(4)) * z[0];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_132_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.03821248390872231344252662188781923345220967043504184902307007321"),stof<T>("-0.51967346510026199823181284194628829558252737077167126374593754432")}, std::complex<T>{stof<T>("-0.36592670338888305867820188017678718783605126286578785934035055596"),stof<T>("-0.23484810379384114742990757804271275258777548953261695507789753691")}, std::complex<T>{stof<T>("-0.1225756954927281487473960892732614546650077294939669130711783302"),stof<T>("-0.33754006219430824546733494744034390445325251659371445657628470889")}, std::complex<T>{stof<T>("-0.15544607849632252103623350723968708096710118774860114818442850773"),stof<T>("-0.27679292663235168124861761364236671935592401363901717350041320928")}, std::complex<T>{stof<T>("-0.12980184566440311198787746570222585772309336536008568984148359593"),stof<T>("-0.20358225353172690411196257021604860302497465483137452591258552027")}, std::complex<T>{stof<T>("-0.032870383003594372288837417966425626302093458254634235113250177528"),stof<T>("0.060747135561956564218717333797977185097328502954697283075871499618")}, std::complex<T>{stof<T>("-0.25186751860815181882323830401799984856501583090687365866823217018"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_132_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_132_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.29125453957040087051919869849396333399652537439058339158447977451"),stof<T>("-0.03068996749074059118317976479273059248876755809690744889799906302")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W71(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k)};

                    
            return f_4_132_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_132_DLogXconstant_part(base_point<T>, kend);
	value += f_4_132_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_132_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_132_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_132_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_132_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_132_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_132_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
