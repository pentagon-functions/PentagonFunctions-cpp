/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_130.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_130_abbreviated (const std::array<T,55>& abb) {
T z[63];
z[0] = abb[46] * (T(1) / T(2));
z[1] = abb[32] * z[0];
z[2] = abb[32] * abb[49];
z[1] = z[1] + z[2];
z[1] = abb[6] * z[1];
z[3] = abb[45] + abb[48];
z[4] = z[0] + z[3];
z[5] = abb[32] * z[4];
z[6] = abb[32] * abb[47];
z[5] = z[5] + z[6];
z[5] = abb[4] * z[5];
z[7] = abb[32] * z[3];
z[8] = z[6] + z[7];
z[8] = abb[11] * z[8];
z[9] = abb[32] * abb[46];
z[10] = z[2] + z[9];
z[10] = abb[9] * z[10];
z[11] = -abb[42] + -abb[43] + abb[44];
z[12] = abb[20] + abb[22];
z[12] = z[11] * z[12];
z[13] = abb[23] * z[11];
z[14] = z[12] + z[13];
z[15] = abb[5] + abb[8];
z[16] = abb[46] * z[15];
z[16] = z[14] + z[16];
z[17] = abb[24] * z[11];
z[18] = -z[16] + z[17];
z[18] = abb[32] * z[18];
z[19] = abb[21] * z[11];
z[20] = abb[32] * z[19];
z[20] = -z[18] + z[20];
z[21] = abb[14] * z[0];
z[22] = abb[32] * z[21];
z[23] = abb[3] * z[9];
z[1] = -z[1] + z[5] + -z[8] + z[10] + (T(1) / T(2)) * z[20] + z[22] + -z[23];
z[5] = -abb[25] * z[11];
z[8] = -abb[17] + abb[19];
z[8] = abb[51] * z[8];
z[10] = z[5] + z[8];
z[20] = -abb[47] + abb[50];
z[22] = abb[46] + z[3];
z[23] = -z[20] + z[22];
z[24] = abb[15] * z[23];
z[25] = z[10] + z[24];
z[26] = abb[5] * abb[46];
z[26] = -z[25] + z[26];
z[27] = z[17] + z[26];
z[28] = -z[3] + z[20];
z[28] = (T(1) / T(2)) * z[28];
z[29] = abb[49] + z[28];
z[29] = abb[0] * z[29];
z[30] = -abb[49] + z[23];
z[31] = abb[10] * z[30];
z[32] = z[29] + z[31];
z[33] = -z[21] + z[32];
z[34] = abb[3] * z[23];
z[35] = (T(1) / T(2)) * z[34];
z[27] = (T(1) / T(2)) * z[27] + z[33] + z[35];
z[27] = abb[33] * z[27];
z[36] = abb[14] * abb[46];
z[36] = -z[17] + z[36];
z[37] = -abb[7] + abb[8];
z[37] = abb[46] * z[37];
z[37] = z[12] + z[36] + z[37];
z[38] = abb[47] + z[3];
z[39] = -abb[49] + z[38];
z[40] = abb[1] * z[39];
z[41] = -abb[47] + abb[49] + -z[22];
z[42] = abb[12] * z[41];
z[43] = z[40] + z[42];
z[37] = (T(1) / T(2)) * z[37] + z[43];
z[37] = abb[31] * z[37];
z[44] = -z[1] + z[27] + z[37];
z[45] = abb[3] * z[28];
z[45] = -z[32] + z[45];
z[46] = z[0] + z[38];
z[46] = abb[4] * z[46];
z[47] = abb[11] * z[38];
z[47] = -z[46] + z[47];
z[48] = (T(1) / T(2)) * z[26] + z[43] + -z[45] + -z[47];
z[49] = abb[30] * (T(1) / T(2));
z[48] = z[48] * z[49];
z[50] = -z[17] + z[19];
z[51] = abb[8] * abb[46];
z[52] = z[14] + z[25] + z[50] + z[51];
z[28] = abb[46] + -z[28];
z[28] = abb[3] * z[28];
z[53] = abb[46] + abb[49];
z[54] = abb[9] * z[53];
z[55] = abb[49] + z[0];
z[55] = abb[6] * z[55];
z[54] = z[54] + -z[55];
z[28] = z[28] + -z[54];
z[52] = -z[28] + -z[33] + z[43] + (T(1) / T(2)) * z[52];
z[56] = abb[28] * (T(1) / T(2));
z[52] = z[52] * z[56];
z[57] = -abb[7] + z[15];
z[57] = abb[46] * z[57];
z[25] = z[12] + -z[25] + z[57];
z[58] = (T(1) / T(2)) * z[25] + z[32] + z[35] + z[43];
z[59] = abb[29] * (T(1) / T(2));
z[60] = -z[58] * z[59];
z[2] = -z[2] + z[6];
z[6] = abb[32] * z[22];
z[6] = z[2] + z[6];
z[6] = abb[12] * z[6];
z[2] = z[2] + z[7];
z[2] = abb[1] * z[2];
z[2] = -z[2] + z[6];
z[6] = z[2] + (T(1) / T(2)) * z[44] + z[48] + z[52] + z[60];
z[6] = m1_set::bc<T>[0] * z[6];
z[7] = abb[8] * z[3];
z[14] = z[7] + z[14];
z[44] = z[5] + z[14] + z[51];
z[48] = abb[8] * abb[47];
z[51] = abb[16] * abb[51];
z[48] = z[48] + z[51];
z[52] = abb[19] * abb[51];
z[50] = z[44] + z[48] + z[50] + z[52];
z[52] = abb[46] * (T(3) / T(2)) + z[38];
z[52] = abb[14] * z[52];
z[52] = z[52] + -z[55];
z[24] = (T(1) / T(2)) * z[24];
z[60] = abb[8] * (T(1) / T(2));
z[61] = abb[2] + z[60];
z[61] = abb[50] * z[61];
z[62] = abb[2] + abb[8];
z[62] = abb[49] * z[62];
z[29] = -z[24] + z[29] + -z[42] + (T(-1) / T(2)) * z[50] + -z[52] + z[61] + z[62];
z[50] = abb[3] * (T(1) / T(2));
z[30] = z[30] * z[50];
z[29] = (T(1) / T(2)) * z[29] + z[30] + z[31];
z[30] = -abb[39] * z[29];
z[31] = abb[8] * abb[50];
z[7] = -z[7] + z[31] + -z[48];
z[31] = -abb[3] + abb[15];
z[38] = abb[46] + -abb[50] + z[38];
z[31] = z[31] * z[38];
z[5] = z[5] + z[7] + z[8] + -z[12] + z[31] + -z[57];
z[5] = abb[41] * z[5];
z[12] = z[16] + z[19];
z[31] = z[12] + -z[17];
z[15] = abb[49] * z[15];
z[38] = abb[3] * z[41];
z[15] = -z[15] + (T(1) / T(2)) * z[31] + z[38] + z[46] + z[52];
z[31] = -z[15] + -z[40] + -3 * z[42];
z[31] = abb[40] * z[31];
z[5] = (T(1) / T(4)) * z[5] + z[6] + z[30] + (T(1) / T(2)) * z[31];
z[5] = (T(1) / T(4)) * z[5];
z[6] = z[10] + -z[13];
z[30] = (T(1) / T(2)) * z[19];
z[31] = -abb[5] + abb[7] * (T(-1) / T(2));
z[31] = abb[46] * z[31];
z[6] = (T(1) / T(2)) * z[6] + z[24] + -z[30] + z[31] + z[36] + z[45] + -z[54];
z[6] = prod_pow(abb[33], 2) * z[6];
z[7] = z[7] + -z[25];
z[25] = -abb[54] * z[7];
z[31] = abb[13] * z[0];
z[36] = z[17] + z[31];
z[30] = -z[16] + -z[30] + z[36];
z[38] = prod_pow(abb[32], 2);
z[30] = z[30] * z[38];
z[4] = -z[4] * z[38];
z[40] = abb[47] * z[38];
z[4] = z[4] + -z[40];
z[4] = abb[4] * z[4];
z[42] = z[0] * z[38];
z[45] = abb[49] * z[38];
z[42] = z[42] + z[45];
z[42] = abb[6] * z[42];
z[46] = abb[36] * abb[46];
z[48] = abb[46] * z[38];
z[52] = z[46] + -z[48];
z[52] = abb[14] * z[52];
z[3] = z[3] * z[38];
z[3] = z[3] + z[40];
z[57] = abb[11] * z[3];
z[4] = z[4] + z[6] + z[25] + z[30] + z[42] + z[52] + z[57];
z[6] = abb[17] + abb[19];
z[25] = -z[6] * z[22];
z[30] = -abb[8] * abb[51];
z[42] = abb[16] * z[22];
z[11] = abb[26] * z[11];
z[52] = abb[18] * abb[46];
z[11] = z[11] + z[25] + z[30] + -z[42] + -z[52];
z[25] = (T(1) / T(4)) * z[20];
z[6] = abb[16] + z[6];
z[6] = z[6] * z[25];
z[25] = abb[3] + abb[15];
z[25] = abb[27] + (T(-1) / T(4)) * z[25];
z[25] = abb[51] * z[25];
z[6] = z[6] + (T(1) / T(4)) * z[11] + z[25];
z[6] = abb[38] * z[6];
z[11] = abb[53] * z[15];
z[15] = prod_pow(m1_set::bc<T>[0], 2) * z[58];
z[25] = abb[13] + -abb[14];
z[9] = z[9] * z[25];
z[9] = z[9] + z[18];
z[9] = z[2] + (T(1) / T(2)) * z[9];
z[18] = z[9] + z[27];
z[25] = abb[13] * abb[46];
z[16] = -z[16] + z[25];
z[0] = abb[3] * z[0];
z[30] = z[0] + z[47];
z[16] = (T(1) / T(2)) * z[16] + -z[30];
z[42] = z[16] * z[49];
z[42] = -z[18] + z[42];
z[42] = abb[30] * z[42];
z[0] = -z[0] + z[54];
z[12] = z[0] + (T(1) / T(2)) * z[12] + z[43];
z[47] = abb[37] * z[12];
z[52] = z[17] + z[19];
z[54] = abb[5] + abb[7];
z[54] = abb[46] * z[54];
z[13] = z[13] + z[52] + z[54];
z[13] = (T(1) / T(2)) * z[13];
z[54] = -z[13] + z[55];
z[54] = abb[36] * z[54];
z[23] = abb[54] * z[23];
z[23] = z[23] + z[46] + z[48];
z[23] = z[23] * z[50];
z[0] = z[0] + -z[21];
z[46] = z[25] + z[52];
z[46] = z[0] + (T(1) / T(2)) * z[46];
z[46] = abb[34] * z[46];
z[22] = z[22] * z[38];
z[22] = z[22] + z[40] + -z[45];
z[38] = abb[53] * z[41];
z[22] = (T(1) / T(2)) * z[22] + z[38];
z[22] = abb[12] * z[22];
z[16] = abb[35] * z[16];
z[38] = -z[45] + -z[48];
z[40] = -abb[36] * z[53];
z[38] = (T(1) / T(2)) * z[38] + z[40];
z[38] = abb[9] * z[38];
z[3] = -z[3] + z[45];
z[39] = abb[53] * z[39];
z[3] = (T(3) / T(2)) * z[3] + z[39];
z[3] = abb[1] * z[3];
z[3] = z[3] + (T(1) / T(2)) * z[4] + z[6] + z[11] + (T(1) / T(6)) * z[15] + z[16] + 3 * z[22] + z[23] + z[38] + z[42] + z[46] + -z[47] + z[54];
z[4] = abb[52] * z[29];
z[6] = z[8] + z[44];
z[8] = -z[20] * z[60];
z[8] = z[8] + (T(1) / T(2)) * z[51];
z[6] = (T(-1) / T(2)) * z[6] + -z[8] + -z[24] + z[36];
z[6] = (T(1) / T(2)) * z[6] + z[33] + (T(1) / T(4)) * z[34];
z[11] = abb[30] * z[6];
z[15] = z[19] + z[25] + -z[26];
z[15] = (T(1) / T(2)) * z[15] + -z[28] + -z[32];
z[15] = z[15] * z[56];
z[9] = -z[9] + z[11] + z[15] + z[27];
z[9] = z[9] * z[56];
z[0] = z[0] + z[13];
z[0] = abb[33] * z[0];
z[0] = z[0] + z[1];
z[1] = abb[31] * (T(-1) / T(4)) + -z[56];
z[1] = z[1] * z[12];
z[11] = (T(1) / T(2)) * z[17] + -z[21] + z[30] + -z[43];
z[11] = z[11] * z[49];
z[0] = (T(1) / T(2)) * z[0] + z[1] + -z[2] + z[11];
z[0] = abb[31] * z[0];
z[1] = -z[10] + z[14];
z[2] = abb[5] + z[60];
z[2] = abb[46] * z[2];
z[1] = (T(1) / T(2)) * z[1] + z[2] + z[8] + -z[24] + -z[31] + z[35];
z[1] = z[1] * z[49];
z[2] = -abb[28] * z[6];
z[6] = z[7] + -z[34];
z[6] = abb[29] * z[6];
z[1] = z[1] + z[2] + (T(1) / T(4)) * z[6] + z[18] + z[37];
z[1] = z[1] * z[59];
z[0] = z[0] + z[1] + (T(1) / T(2)) * z[3] + z[4] + z[9];
z[0] = (T(1) / T(4)) * z[0];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_4_130_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.32783505779352584141901041773387745058899685637351807678096670347"),stof<T>("-0.02117969308723342926587963376439328458141610239887590962529290064")}, std::complex<T>{stof<T>("-0.28840069593241962000577226619717658694907069750371002560150335093"),stof<T>("0.11277811557534791208949274345990201684686175936346402103840628798")}, std::complex<T>{stof<T>("-0.32783505779352584141901041773387745058899685637351807678096670347"),stof<T>("-0.02117969308723342926587963376439328458141610239887590962529290064")}, std::complex<T>{stof<T>("-0.32783505779352584141901041773387745058899685637351807678096670347"),stof<T>("-0.02117969308723342926587963376439328458141610239887590962529290064")}, std::complex<T>{stof<T>("-0.09768604677786429431275133139085887213217589538973625879348237781"),stof<T>("0.44884017236342461996103013408604024550381413551460746113004692871")}, std::complex<T>{stof<T>("-0.028506725943667559253406397001870086321410769674888617946429444721"),stof<T>("0.085291897119023604635171637206313103698356982662243759378095386177")}, std::complex<T>{stof<T>("-0.19757415930072218380369023884017404767300818427486720038558645755"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_130_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_130_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.18015282672072209931603483675635724138602115369846005316782506576"),stof<T>("0.09057116692072558972886564779102762741615190556831182642412473628")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W18(k,dl), dlog_W23(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k)};

                    
            return f_4_130_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_130_DLogXconstant_part(base_point<T>, kend);
	value += f_4_130_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_130_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_130_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_130_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_130_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_130_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_130_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
