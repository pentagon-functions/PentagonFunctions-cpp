/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_152.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_152_abbreviated (const std::array<T,9>& abb) {
T z[9];
z[0] = abb[0] + abb[2];
z[1] = abb[6] + abb[7];
z[0] = z[0] * z[1];
z[2] = abb[8] * z[0];
z[3] = -abb[0] + 3 * abb[2];
z[3] = z[1] * z[3];
z[4] = abb[6] + -abb[7];
z[4] = abb[1] * z[4];
z[5] = (T(-1) / T(2)) * z[3] + 2 * z[4];
z[5] = prod_pow(abb[3], 2) * z[5];
z[6] = abb[4] * z[0];
z[7] = abb[3] + abb[4] * (T(1) / T(2));
z[7] = z[6] * z[7];
z[8] = 4 * abb[2] + abb[0] * (T(-5) / T(2));
z[1] = z[1] * z[8];
z[1] = z[1] + (T(-13) / T(2)) * z[4];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[1] = (T(1) / T(3)) * z[1] + z[2] + z[5] + z[7];
z[2] = z[3] + -4 * z[4];
z[2] = abb[3] * z[2];
z[2] = z[2] + -z[6];
z[2] = m1_set::bc<T>[0] * z[2];
z[0] = abb[5] * z[0];
z[0] = z[0] + z[2];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_152_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("6.119938861643658700220336319163661693018395636593371160328722637"),stof<T>("21.425544949660128255842337925115345478028247307172848569450604868")}, std::complex<T>{stof<T>("22.498682368670252044003734153360304041616082413310866117760781599"),stof<T>("-24.214032215157586925753164143671657113322794196640116733956322918")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[24].real()/kbase.W[24].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_152_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_152_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.3452300741116899856206382188750856574076922572065438786402139286"),stof<T>("-13.0696169544682534187287997278453095318753819961094677696729435139")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,9> abb = {dlog_W4(k,dl), dl[2], dlog_W25(k,dl), f_1_5(k), f_1_11(k), f_2_12_im(k), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[24].real()/k.W[24].real()), f_2_12_re(k)};

                    
            return f_4_152_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_152_DLogXconstant_part(base_point<T>, kend);
	value += f_4_152_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_152_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_152_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_152_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_152_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_152_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_152_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
