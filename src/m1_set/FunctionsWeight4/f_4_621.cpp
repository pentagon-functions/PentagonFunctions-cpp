/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_621.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_621_abbreviated (const std::array<T,68>& abb) {
T z[157];
z[0] = 2 * abb[46];
z[1] = 2 * abb[45];
z[2] = z[0] + -z[1];
z[3] = 5 * abb[55] + -abb[59];
z[4] = abb[50] + -abb[51];
z[5] = -abb[57] + 3 * z[4];
z[6] = 2 * abb[53];
z[7] = abb[48] + abb[49];
z[8] = z[2] + -z[3] + (T(1) / T(2)) * z[5] + z[6] + z[7];
z[8] = abb[7] * z[8];
z[9] = 6 * abb[49];
z[10] = 6 * abb[55];
z[11] = z[9] + -z[10];
z[12] = 6 * abb[59];
z[13] = 4 * abb[53];
z[14] = abb[57] + z[4];
z[15] = 6 * abb[48] + z[11] + -z[12] + z[13] + z[14];
z[15] = abb[8] * z[15];
z[16] = abb[60] + abb[61] + -abb[62];
z[17] = abb[25] * z[16];
z[18] = abb[23] * z[16];
z[15] = z[15] + z[17] + z[18];
z[19] = (T(1) / T(2)) * z[14];
z[20] = 3 * abb[55];
z[21] = 2 * abb[56];
z[22] = z[13] + z[19] + -z[20] + -z[21];
z[22] = abb[6] * z[22];
z[23] = abb[24] + abb[26];
z[23] = z[16] * z[23];
z[24] = (T(1) / T(2)) * z[23];
z[25] = z[22] + z[24];
z[26] = 2 * abb[49];
z[27] = 2 * abb[48];
z[28] = z[26] + z[27];
z[29] = 2 * abb[59];
z[30] = z[28] + -z[29];
z[31] = 2 * abb[55];
z[32] = z[13] + -z[31];
z[33] = 4 * abb[56];
z[34] = z[14] + -z[33];
z[35] = z[30] + z[32] + z[34];
z[36] = abb[16] * z[35];
z[37] = abb[27] * z[16];
z[36] = z[36] + -z[37];
z[38] = -abb[51] + abb[52];
z[39] = z[7] + z[38];
z[40] = 4 * abb[45];
z[41] = 5 * abb[46] + -z[20] + z[33] + -z[39] + -z[40];
z[42] = 2 * abb[1];
z[41] = z[41] * z[42];
z[43] = 3 * abb[46];
z[44] = 2 * abb[52];
z[45] = 5 * abb[45] + -z[43] + -z[44];
z[46] = -abb[50] + abb[56];
z[47] = -z[45] + 2 * z[46];
z[47] = abb[9] * z[47];
z[48] = z[6] + -z[21];
z[49] = z[7] + z[48];
z[50] = abb[57] + -z[29] + z[49];
z[51] = abb[17] * z[50];
z[52] = z[47] + -z[51];
z[53] = 3 * abb[59];
z[54] = -z[21] + z[53];
z[55] = -abb[57] + z[4];
z[56] = (T(1) / T(2)) * z[55];
z[57] = z[54] + z[56];
z[58] = abb[55] + -z[28] + z[57];
z[59] = abb[4] * z[58];
z[60] = -abb[57] + -z[33] + z[53];
z[61] = 2 * z[4] + z[60];
z[62] = abb[10] * z[61];
z[59] = z[59] + z[62];
z[49] = z[4] + -z[31] + z[49];
z[62] = abb[12] * z[49];
z[63] = 3 * abb[45];
z[64] = 3 * abb[56] + -z[6];
z[65] = -abb[51] + abb[59] + -z[64];
z[65] = abb[46] + z[44] + -z[63] + 2 * z[65];
z[65] = abb[0] * z[65];
z[58] = -abb[5] * z[58];
z[66] = -abb[45] + abb[46];
z[67] = -abb[59] + z[7];
z[68] = z[66] + -z[67];
z[68] = abb[13] * z[68];
z[69] = 2 * z[68];
z[8] = z[8] + -z[15] + -z[25] + -z[36] + z[41] + -z[52] + z[58] + -z[59] + 5 * z[62] + z[65] + -z[69];
z[8] = abb[34] * z[8];
z[5] = 8 * abb[56] + -z[5] + -z[12] + z[28] + -z[32];
z[5] = abb[16] * z[5];
z[5] = z[5] + -z[37];
z[41] = 2 * z[49];
z[41] = abb[7] * z[41];
z[12] = z[12] + z[55];
z[49] = 4 * abb[49];
z[58] = 4 * abb[48] + z[33];
z[65] = z[49] + z[58];
z[70] = z[12] + z[31] + -z[65];
z[71] = abb[4] * z[70];
z[72] = 8 * abb[53];
z[34] = -z[10] + z[34] + z[72];
z[73] = abb[6] * z[34];
z[74] = z[23] + z[73];
z[75] = 2 * abb[10];
z[61] = z[61] * z[75];
z[61] = z[61] + z[74];
z[41] = -z[5] + -z[15] + z[41] + -z[61] + 4 * z[62] + -z[71];
z[71] = abb[5] * z[70];
z[71] = -z[41] + z[71];
z[76] = abb[31] * z[71];
z[77] = z[1] + -z[21];
z[78] = -abb[46] + z[20] + -z[39] + z[77];
z[78] = z[42] * z[78];
z[79] = -abb[50] + abb[57];
z[80] = -abb[51] + z[79];
z[81] = z[44] + z[80];
z[82] = -z[1] + z[81];
z[83] = abb[15] * z[82];
z[84] = abb[28] * z[16];
z[83] = -z[83] + z[84];
z[84] = abb[57] + z[48];
z[85] = -abb[59] + z[84];
z[86] = z[66] + z[85];
z[87] = 2 * abb[0];
z[86] = z[86] * z[87];
z[78] = -z[69] + z[78] + -z[83] + -z[86];
z[86] = 2 * z[62];
z[88] = z[29] + z[55];
z[89] = -z[31] + z[88];
z[90] = abb[4] * z[89];
z[90] = -z[86] + z[90];
z[91] = abb[5] * z[89];
z[92] = z[90] + z[91];
z[93] = -abb[46] + -abb[55] + z[39];
z[94] = 2 * abb[7];
z[95] = z[93] * z[94];
z[95] = z[36] + z[78] + z[92] + -z[95];
z[96] = abb[30] + -abb[33];
z[95] = z[95] * z[96];
z[96] = z[15] + -z[86];
z[97] = 2 * abb[17];
z[50] = z[50] * z[97];
z[97] = z[36] + -z[50];
z[98] = abb[55] + -abb[56];
z[99] = -z[67] + z[98];
z[100] = 4 * z[99];
z[101] = abb[4] * z[100];
z[100] = abb[5] * z[100];
z[101] = z[97] + z[100] + z[101];
z[89] = abb[7] * z[89];
z[89] = z[89] + -z[96] + -z[101];
z[102] = -abb[32] * z[89];
z[8] = z[8] + z[76] + z[95] + z[102];
z[8] = abb[34] * z[8];
z[76] = 2 * abb[58];
z[95] = z[55] + z[76];
z[102] = 3 * abb[47];
z[103] = 3 * abb[53];
z[20] = 3 * abb[48] + z[20] + -z[54] + -z[95] + z[102] + -z[103];
z[104] = abb[10] * z[20];
z[105] = -abb[53] + abb[58];
z[106] = -abb[46] + abb[52] + z[67] + z[79] + -z[105];
z[106] = abb[7] * z[106];
z[107] = abb[47] + abb[48];
z[84] = -z[2] + z[84] + -z[107];
z[84] = abb[0] * z[84];
z[108] = 2 * abb[47];
z[109] = -abb[58] + z[108];
z[110] = z[27] + z[109];
z[111] = abb[59] + z[48] + z[66] + -z[110];
z[111] = abb[4] * z[111];
z[112] = abb[1] * z[93];
z[113] = (T(1) / T(2)) * z[80];
z[114] = -abb[45] + abb[52];
z[115] = z[113] + z[114];
z[116] = abb[15] * z[115];
z[117] = (T(1) / T(2)) * z[16];
z[118] = abb[28] * z[117];
z[116] = -z[116] + z[118];
z[118] = -abb[53] + z[107];
z[119] = abb[56] + -abb[59] + z[118];
z[120] = 2 * abb[2];
z[121] = -z[119] * z[120];
z[122] = abb[55] + -abb[59];
z[123] = -z[56] + z[122];
z[123] = abb[5] * z[123];
z[124] = abb[58] + z[56];
z[125] = -abb[53] + z[124];
z[126] = abb[14] * z[125];
z[22] = z[22] + z[52] + z[84] + z[104] + z[106] + z[111] + z[112] + z[116] + z[121] + z[123] + z[126];
z[22] = abb[30] * z[22];
z[52] = -abb[53] + abb[55];
z[84] = z[52] + -z[107];
z[21] = -z[4] + z[21];
z[104] = z[21] + z[84];
z[106] = z[75] * z[104];
z[111] = abb[47] + -abb[49];
z[121] = -abb[58] + abb[59] + z[111];
z[121] = z[120] * z[121];
z[123] = z[106] + z[121];
z[42] = z[42] * z[93];
z[42] = z[42] + z[116];
z[24] = -z[24] + z[126];
z[93] = -z[24] + z[42];
z[126] = -abb[45] + -abb[52] + z[0];
z[127] = z[6] + z[126];
z[128] = 3 * abb[51];
z[129] = -abb[50] + z[128];
z[130] = -abb[57] + z[129];
z[130] = -z[110] + z[127] + (T(1) / T(2)) * z[130];
z[130] = abb[7] * z[130];
z[131] = z[27] + z[108];
z[132] = abb[51] + abb[57];
z[133] = abb[53] + z[132];
z[134] = z[126] + z[133];
z[135] = z[131] + -z[134];
z[135] = abb[0] * z[135];
z[56] = -abb[53] + z[56];
z[136] = -abb[58] + -z[2] + z[29] + z[56];
z[136] = abb[4] * z[136];
z[130] = z[36] + -z[93] + z[123] + z[130] + z[135] + z[136];
z[130] = abb[33] * z[130];
z[54] = z[54] + z[55];
z[110] = z[52] + z[110];
z[135] = z[54] + -z[110];
z[135] = z[75] * z[135];
z[136] = -z[97] + z[135];
z[137] = -z[6] + z[55];
z[138] = z[26] + -z[29];
z[139] = z[108] + z[137] + -z[138];
z[140] = abb[7] * z[139];
z[141] = abb[47] + abb[49] + z[27];
z[48] = abb[58] + -z[48] + -z[53] + z[141];
z[48] = z[48] * z[120];
z[53] = z[76] + z[137];
z[137] = abb[14] * z[53];
z[23] = -z[23] + z[137];
z[142] = 4 * z[119];
z[143] = abb[4] * z[142];
z[140] = -z[23] + z[48] + z[91] + z[136] + z[140] + z[143];
z[140] = abb[35] * z[140];
z[35] = abb[7] * z[35];
z[64] = -z[4] + z[64] + z[122];
z[143] = 4 * abb[16];
z[64] = z[64] * z[143];
z[143] = z[50] + z[61];
z[35] = -z[35] + z[64] + z[92] + z[143];
z[92] = -abb[31] * z[35];
z[22] = z[22] + z[92] + z[130] + z[140];
z[22] = abb[30] * z[22];
z[92] = 8 * z[62];
z[130] = abb[22] * z[16];
z[113] = abb[47] + -abb[54] + -z[113];
z[113] = abb[3] * z[113];
z[17] = (T(5) / T(2)) * z[17] + -z[23] + z[83] + -z[92] + z[113] + z[130];
z[52] = (T(-8) / T(3)) * z[52];
z[113] = abb[46] * (T(17) / T(6));
z[144] = abb[49] * (T(1) / T(6));
z[145] = abb[50] + z[132];
z[146] = (T(1) / T(2)) * z[145];
z[147] = -abb[58] + z[146];
z[148] = abb[47] * (T(-1) / T(2)) + abb[54] * (T(1) / T(3)) + abb[52] * (T(2) / T(3)) + abb[45] * (T(13) / T(6)) + -z[52] + -z[113] + z[144] + -z[147];
z[148] = abb[7] * z[148];
z[149] = -abb[51] + abb[54];
z[150] = 4 * abb[59];
z[151] = (T(1) / T(2)) * z[149] + -z[150];
z[152] = -abb[53] + abb[57] + -abb[58] + z[151];
z[153] = abb[56] * (T(1) / T(6)) + abb[48] * (T(3) / T(2));
z[154] = abb[47] * (T(3) / T(2));
z[152] = -z[144] + (T(1) / T(3)) * z[152] + z[153] + z[154];
z[152] = abb[2] * z[152];
z[155] = -5 * abb[57] + 8 * z[4];
z[52] = 5 * abb[59] + abb[56] * (T(-16) / T(3)) + abb[58] * (T(5) / T(3)) + z[52] + (T(-7) / T(3)) * z[107] + (T(1) / T(3)) * z[155];
z[52] = abb[10] * z[52];
z[155] = 2 * abb[57];
z[156] = -11 * abb[59] + abb[48] * (T(5) / T(2)) + abb[51] * (T(13) / T(2)) + z[155];
z[113] = abb[52] * (T(-13) / T(6)) + abb[45] * (T(-2) / T(3)) + abb[47] * (T(5) / T(6)) + abb[56] * (T(31) / T(6)) + -z[103] + z[113] + (T(1) / T(3)) * z[156];
z[113] = abb[0] * z[113];
z[151] = -abb[55] + z[151];
z[144] = abb[47] * (T(-7) / T(6)) + -z[144] + (T(-1) / T(3)) * z[151] + -z[153];
z[144] = abb[5] * z[144];
z[151] = 3 * abb[49];
z[153] = -z[55] + -z[151];
z[153] = -abb[58] + abb[53] * (T(-4) / T(3)) + abb[55] * (T(7) / T(3)) + (T(-5) / T(6)) * z[66] + (T(1) / T(2)) * z[153] + z[154];
z[153] = abb[4] * z[153];
z[154] = abb[45] + -abb[56];
z[7] = -abb[51] + z[7];
z[7] = 17 * z[7] + 31 * z[154];
z[7] = 7 * abb[55] + (T(1) / T(2)) * z[7];
z[7] = -8 * abb[46] + abb[52] * (T(17) / T(6)) + (T(1) / T(3)) * z[7];
z[7] = abb[1] * z[7];
z[14] = -abb[55] + abb[53] * (T(2) / T(3)) + (T(1) / T(6)) * z[14] + z[67];
z[14] = abb[8] * z[14];
z[7] = z[7] + 5 * z[14] + (T(1) / T(3)) * z[17] + (T(5) / T(6)) * z[18] + z[52] + (T(11) / T(3)) * z[68] + z[113] + z[144] + z[148] + z[152] + z[153];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[14] = z[79] + -z[128];
z[17] = 6 * abb[56];
z[18] = 2 * abb[54];
z[3] = 6 * abb[47] + z[3] + -z[9] + (T(1) / T(2)) * z[14] + -z[17] + z[18] + z[27];
z[3] = abb[5] * z[3];
z[9] = abb[48] + z[108];
z[14] = -abb[49] + z[6] + z[9] + z[19] + -z[33] + z[122];
z[14] = abb[7] * z[14];
z[19] = z[25] + z[51];
z[25] = -abb[59] + -z[149];
z[25] = -abb[49] + -z[13] + z[17] + 2 * z[25] + z[102];
z[25] = abb[2] * z[25];
z[3] = z[3] + z[14] + -z[19] + z[25] + -z[59] + -z[62] + -z[64];
z[3] = abb[31] * z[3];
z[14] = z[18] + z[80];
z[25] = z[14] + -z[108];
z[27] = abb[3] * z[25];
z[36] = z[27] + z[36];
z[51] = z[85] + -z[111];
z[51] = z[51] * z[120];
z[52] = abb[48] + -abb[55] + z[149];
z[59] = -z[52] * z[94];
z[62] = z[98] + z[111];
z[62] = abb[5] * z[62];
z[59] = z[36] + -z[51] + z[59] + 4 * z[62] + z[90];
z[59] = abb[33] * z[59];
z[3] = z[3] + z[59];
z[3] = abb[31] * z[3];
z[59] = abb[31] * z[89];
z[59] = z[59] + -z[140];
z[24] = z[24] + z[69] + -z[91];
z[62] = -z[26] + z[109] + z[146];
z[64] = z[62] + z[126];
z[64] = abb[7] * z[64];
z[67] = -z[29] + z[134];
z[67] = abb[0] * z[67];
z[80] = abb[4] * z[125];
z[42] = -z[24] + -z[42] + z[64] + -z[67] + z[80] + -z[121];
z[42] = abb[33] * z[42];
z[64] = abb[2] * z[142];
z[64] = z[64] + z[69];
z[67] = z[64] + z[67] + z[93];
z[69] = -3 * abb[57] + -z[129];
z[69] = abb[58] + z[29] + (T(1) / T(2)) * z[69] + -z[127];
z[69] = abb[7] * z[69];
z[80] = z[103] + z[124];
z[85] = 4 * abb[47];
z[58] = z[58] + z[85];
z[89] = z[58] + -z[80] + -z[150];
z[89] = abb[4] * z[89];
z[69] = z[67] + z[69] + z[89] + z[136];
z[69] = abb[30] * z[69];
z[89] = abb[0] + -abb[2];
z[89] = z[89] * z[119];
z[90] = -z[66] + z[98];
z[90] = abb[1] * z[90];
z[91] = -abb[5] * z[99];
z[66] = z[66] + z[111];
z[93] = -abb[4] * z[66];
z[89] = z[68] + z[89] + z[90] + z[91] + z[93];
z[89] = abb[32] * z[89];
z[42] = z[42] + z[59] + z[69] + 2 * z[89];
z[42] = abb[32] * z[42];
z[41] = -abb[64] * z[41];
z[26] = -abb[59] + z[26];
z[69] = abb[50] + abb[57] + z[128];
z[69] = -z[18] + z[26] + (T(1) / T(2)) * z[69] + -z[118];
z[69] = abb[7] * z[69];
z[89] = -abb[53] + -z[9] + z[149] + z[150] + -z[155];
z[89] = abb[2] * z[89];
z[54] = -z[54] + -z[84];
z[54] = abb[10] * z[54];
z[79] = z[79] + z[128];
z[79] = -abb[48] + -abb[54] + -abb[59] + z[31] + (T(1) / T(2)) * z[79];
z[79] = abb[5] * z[79];
z[57] = abb[53] + z[57] + -z[141];
z[57] = abb[4] * z[57];
z[19] = -z[19] + z[36] + z[54] + z[57] + z[69] + z[79] + z[89];
z[19] = abb[35] * z[19];
z[4] = -12 * abb[53] + 16 * abb[56] + -abb[57] + -5 * z[4] + z[10] + -z[28] + -z[29];
z[4] = abb[16] * z[4];
z[18] = -z[18] + z[145];
z[28] = -z[13] + -z[18] + z[33] + -z[138];
z[28] = abb[7] * z[28];
z[49] = -z[49] + z[85];
z[54] = -z[10] + z[33] + -z[49] + z[88];
z[54] = abb[5] * z[54];
z[4] = z[4] + -z[27] + z[28] + z[37] + z[51] + z[54] + z[143];
z[4] = abb[31] * z[4];
z[28] = -abb[54] + z[133];
z[9] = -z[9] + z[26] + z[28];
z[9] = z[9] * z[120];
z[26] = -abb[4] * z[139];
z[51] = -abb[49] + z[118] + z[149];
z[54] = z[51] * z[94];
z[52] = abb[5] * z[52];
z[9] = z[9] + z[26] + -z[36] + 2 * z[52] + z[54] + -z[106];
z[9] = abb[33] * z[9];
z[4] = z[4] + z[9] + z[19];
z[4] = abb[35] * z[4];
z[9] = z[73] + -z[86] + z[137];
z[19] = -z[32] + z[58];
z[12] = -z[12] + z[19];
z[12] = abb[4] * z[12];
z[21] = z[21] + -z[110];
z[21] = z[21] * z[75];
z[26] = z[94] * z[104];
z[5] = -z[5] + -z[9] + z[12] + z[21] + -z[26] + z[48] + z[130];
z[12] = abb[63] * z[5];
z[21] = abb[16] * z[34];
z[21] = -z[21] + z[37] + z[74];
z[26] = -abb[51] + abb[57];
z[32] = -5 * abb[56] + z[13] + z[26];
z[32] = 2 * z[32] + -z[45];
z[32] = abb[0] * z[32];
z[34] = abb[7] * z[82];
z[36] = abb[52] + z[46];
z[36] = -abb[45] + 4 * z[36] + -z[43];
z[36] = abb[9] * z[36];
z[32] = z[21] + z[32] + z[34] + z[36] + z[83] + -6 * z[90];
z[32] = abb[36] * z[32];
z[18] = z[18] + -z[76] + z[108];
z[18] = abb[7] * z[18];
z[28] = -abb[47] + -z[28] + z[76];
z[28] = z[28] * z[120];
z[34] = -abb[5] * z[25];
z[36] = abb[4] * z[53];
z[18] = z[18] + -z[23] + z[27] + z[28] + z[34] + z[36] + z[130];
z[18] = abb[38] * z[18];
z[10] = -z[10] + -z[29] + z[55] + z[65];
z[23] = abb[4] + abb[5];
z[10] = z[10] * z[23];
z[0] = -z[0] + z[30] + z[81];
z[0] = abb[7] * z[0];
z[0] = -z[0] + z[10] + -z[15] + z[50] + z[78];
z[10] = -abb[65] * z[0];
z[23] = -abb[39] * z[35];
z[28] = -abb[54] + -z[26];
z[28] = 5 * abb[47] + 10 * abb[56] + 2 * z[28] + -z[72] + -z[151];
z[28] = abb[2] * z[28];
z[25] = -abb[7] * z[25];
z[21] = -z[21] + z[25] + z[27] + z[28];
z[21] = abb[37] * z[21];
z[25] = abb[4] + -abb[7];
z[25] = z[25] * z[66];
z[27] = abb[46] + -z[38] + -z[118];
z[27] = abb[0] * z[27];
z[28] = abb[2] * z[51];
z[25] = z[25] + z[27] + z[28] + -z[52] + z[112];
z[25] = prod_pow(abb[33], 2) * z[25];
z[27] = -z[114] + z[147];
z[27] = abb[18] * z[27];
z[28] = abb[21] * z[115];
z[30] = abb[58] + z[56];
z[30] = abb[19] * z[30];
z[34] = abb[29] * z[117];
z[35] = abb[20] * z[125];
z[27] = z[27] + z[28] + z[30] + -z[34] + z[35];
z[28] = -abb[66] * z[27];
z[11] = -z[11] + z[14] + -z[17] + z[85];
z[11] = abb[37] * z[11];
z[14] = abb[64] * z[70];
z[11] = z[11] + z[14];
z[11] = abb[5] * z[11];
z[14] = abb[35] * z[16];
z[17] = abb[33] * z[117];
z[14] = z[14] + -z[17];
z[17] = abb[30] * z[117];
z[17] = -z[14] + z[17];
z[30] = -abb[30] + abb[32];
z[17] = abb[22] * z[17] * z[30];
z[3] = abb[67] + z[3] + z[4] + z[7] + z[8] + z[10] + z[11] + z[12] + z[17] + z[18] + z[21] + z[22] + z[23] + z[25] + z[28] + z[32] + z[41] + z[42];
z[4] = -z[20] * z[75];
z[7] = -z[2] + z[19] + -z[95] + -z[150];
z[7] = abb[4] * z[7];
z[8] = z[55] + z[105] + -z[122];
z[8] = z[8] * z[94];
z[10] = -abb[59] + z[43] + -z[63] + z[107];
z[10] = z[10] * z[87];
z[11] = 2 * z[47];
z[12] = 4 * z[90];
z[4] = z[4] + z[7] + z[8] + -z[9] + z[10] + -z[11] + -z[12] + z[64] + -z[97];
z[4] = abb[30] * z[4];
z[7] = -6 * abb[46] + -3 * abb[50] + 8 * abb[55] + -z[13] + -z[29] + z[40] + z[44] + z[132];
z[7] = abb[7] * z[7];
z[1] = 2 * abb[51] + z[1] + -z[6] + -z[44] + -z[60];
z[1] = z[1] * z[87];
z[8] = abb[55] + z[39] + -z[43] + z[77];
z[8] = abb[1] * z[8];
z[8] = z[8] + z[68];
z[1] = z[1] + z[7] + 6 * z[8] + z[11] + 2 * z[15] + z[61] + z[83] + -z[92] + z[101];
z[1] = abb[34] * z[1];
z[7] = -z[26] + z[29] + z[33] + -z[103] + -z[114] + -z[131];
z[7] = abb[0] * z[7];
z[6] = -z[6] + z[31] + z[62] + -z[114];
z[6] = abb[7] * z[6];
z[2] = abb[53] + z[2] + -z[31] + z[124];
z[2] = abb[4] * z[2];
z[2] = z[2] + z[6] + z[7] + z[12] + -z[24] + -z[86] + -z[116] + -z[123];
z[2] = abb[33] * z[2];
z[6] = -z[31] + z[127] + z[147];
z[6] = abb[7] * z[6];
z[7] = -4 * abb[55] + -z[49] + z[80];
z[7] = abb[4] * z[7];
z[6] = z[6] + z[7] + -z[67] + -z[96] + -z[100] + -z[135];
z[6] = abb[32] * z[6];
z[7] = abb[30] * z[16];
z[8] = -abb[32] * z[117];
z[7] = z[7] + z[8] + -z[14];
z[7] = abb[22] * z[7];
z[1] = z[1] + z[2] + z[4] + z[6] + z[7] + z[59];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[41] * z[71];
z[4] = abb[40] * z[5];
z[0] = -abb[42] * z[0];
z[5] = -abb[43] * z[27];
z[0] = abb[44] + z[0] + z[1] + z[2] + z[4] + z[5];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_621_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("26.768985058639030131879448466204587843194209849485869459626654332"),stof<T>("-71.09976665845057414018308566493259719417126011945365390520978474")}, std::complex<T>{stof<T>("-42.916313497125379960109039975440756951706763485975498496114291434"),stof<T>("50.097203268269536149451995402961805638110798703170824860421667433")}, std::complex<T>{stof<T>("-7.8551454723131857530691649068406368897196791591473527220537535163"),stof<T>("0.4050122534483317334704660663551477992360172553497047484794015977")}, std::complex<T>{stof<T>("5.2063200302803987567884716729659191468317334912713219555866656948"),stof<T>("-8.7129759138819560476297777895262982885992384574472649532892979805")}, std::complex<T>{stof<T>("13.061465502593584509857636579806556036551412650418674677640419211"),stof<T>("-9.117988167330287781100243855881446087835255712796969701768699578")}, std::complex<T>{stof<T>("-9.952057916789265596463957496863509091752362812914099722892682529"),stof<T>("-10.253800251466595077470719601136567724195256639803559308130627588")}, std::complex<T>{stof<T>("-6.1952705216970842317656340123726600167601908235755293135949545717"),stof<T>("-10.748763138714442913260370660834223831865204776479269736657489719")}, std::complex<T>{stof<T>("16.147328438486349828229591509236169108512553636489629036487637101"),stof<T>("21.002563390181037990731090261970791556060461416282829044788117307")}, std::complex<T>{stof<T>("11.246167422133003109107223535577817204808642390929792727410914612"),stof<T>("71.163691767514521208089033916028741722293713804574897252202436529")}, std::complex<T>{stof<T>("16.839244542299170354640254101143225377603280674039794036914430504"),stof<T>("-46.625841273145552347392710181317769269200435398040620749525899143")}, std::complex<T>{stof<T>("-24.328624569339992099049154152230193507419751075631016355027617159"),stof<T>("-25.032813381616816696485974794408628560763226543209986931203399517")}, std::complex<T>{stof<T>("1.6171009194862126262983661217973796274987872711892859372350642865"),stof<T>("-3.966150087348063883960288851182230484432100681518873173819811858")}, std::complex<T>{stof<T>("0.3770995406714526367745592174686979133639260616043059036115821508"),stof<T>("-7.0856312627672268401954367563287616384305251855206658797846966884")}, std::complex<T>{stof<T>("-1.00524996874097978809576299985933667093425600048938448283835756"),stof<T>("30.513520402711689685045874057871514243327069100966073743551296246")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("-3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("0.2599334236531954369552408109471828770159706361072270565126747634")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[39])) - rlog(abs(kbase.W[39])), rlog(k.W[82].real()/kbase.W[82].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_621_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_621_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (-24 + -12 * v[0] + 5 * v[1] + -7 * v[2] + -5 * v[3] + 7 * v[4] + 4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 12 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-v[1] + -v[2] + v[3] + v[4] + -2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]))) / prod_pow(tend, 2);
c[2] = ((-1 + 2 * m1_set::bc<T>[1]) * (T(3) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[3] = (-2 * (1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return abb[49] * (t * c[0] + c[2]) + (abb[50] + -abb[54] + -abb[56]) * (t * c[1] + c[3]) + abb[47] * (t * (-c[0] + c[1]) + -c[2] + c[3]);
	}
	{
T z[3];
z[0] = prod_pow(abb[31], 2);
z[1] = prod_pow(abb[35], 2);
z[0] = z[0] + -z[1];
z[1] = -2 * abb[37] + z[0];
z[2] = -abb[50] + abb[54] + abb[56];
z[1] = z[1] * z[2];
z[2] = abb[37] + -5 * z[0];
z[2] = abb[47] * z[2];
z[0] = abb[37] + z[0];
z[0] = abb[49] * z[0];
z[0] = 3 * z[0] + 2 * z[1] + z[2];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_621_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_621_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-34.114692567472980322213382868169567916512369276807794018783148094"),stof<T>("41.870888968994921922672598254108424008015159890570568594610974157")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19, 39});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,68> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W83(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[39])) - rlog(abs(k.W[39])), rlog(kend.W[82].real()/k.W[82].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}};
abb[44] = SpDLog_f_4_621_W_20_Im(t, path, abb);
abb[67] = SpDLog_f_4_621_W_20_Re(t, path, abb);

                    
            return f_4_621_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_621_DLogXconstant_part(base_point<T>, kend);
	value += f_4_621_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_621_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_621_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_621_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_621_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_621_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_621_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
