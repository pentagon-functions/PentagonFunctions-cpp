/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_279.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_279_abbreviated (const std::array<T,57>& abb) {
T z[87];
z[0] = abb[51] + abb[52] + abb[53];
z[1] = abb[26] * z[0];
z[2] = abb[25] * z[0];
z[3] = z[1] + z[2];
z[4] = abb[47] * (T(1) / T(2));
z[5] = -abb[42] + z[4];
z[6] = abb[44] + abb[50];
z[7] = -abb[45] + z[6];
z[8] = -abb[43] + z[7];
z[9] = z[5] + (T(1) / T(2)) * z[8];
z[10] = abb[16] * z[9];
z[11] = (T(1) / T(2)) * z[3] + z[10];
z[12] = abb[22] + abb[24];
z[12] = z[0] * z[12];
z[13] = z[11] + z[12];
z[14] = abb[21] * z[0];
z[7] = abb[43] + z[7];
z[15] = -abb[47] + z[7];
z[15] = -abb[48] + (T(1) / T(2)) * z[15];
z[16] = abb[14] * z[15];
z[17] = (T(1) / T(2)) * z[14] + -z[16];
z[18] = -abb[44] + abb[45];
z[19] = -abb[42] + abb[47];
z[20] = -z[18] + z[19];
z[20] = abb[13] * z[20];
z[21] = 3 * z[20];
z[22] = -abb[43] + abb[50];
z[23] = z[18] + z[22];
z[24] = abb[42] * (T(-7) / T(3)) + -z[23];
z[24] = abb[47] * (T(5) / T(3)) + (T(1) / T(2)) * z[24];
z[24] = abb[7] * z[24];
z[25] = abb[47] * (T(1) / T(3));
z[26] = abb[42] * (T(-5) / T(3)) + z[7];
z[26] = -abb[48] + z[25] + (T(1) / T(2)) * z[26];
z[26] = abb[4] * z[26];
z[27] = -abb[46] + z[6];
z[25] = -abb[42] + z[25] + (T(1) / T(3)) * z[27];
z[25] = abb[49] * (T(-1) / T(3)) + (T(1) / T(2)) * z[25];
z[25] = abb[2] * z[25];
z[28] = abb[23] * z[0];
z[29] = (T(1) / T(2)) * z[28];
z[30] = abb[42] + abb[48];
z[31] = -abb[50] + z[30];
z[32] = abb[0] * z[31];
z[33] = abb[47] + -abb[50];
z[34] = abb[48] + z[33];
z[35] = abb[1] * z[34];
z[36] = abb[43] + -2 * abb[45] + abb[50] * (T(-13) / T(6)) + abb[44] * (T(5) / T(6)) + abb[49] * (T(7) / T(3)) + abb[46] * (T(7) / T(6)) + abb[47] * (T(10) / T(3));
z[36] = abb[8] * z[36];
z[24] = z[13] + z[17] + -z[21] + z[24] + 7 * z[25] + z[26] + z[29] + (T(13) / T(6)) * z[32] + (T(-19) / T(6)) * z[35] + z[36];
z[24] = prod_pow(m1_set::bc<T>[0], 2) * z[24];
z[8] = -abb[47] + z[8];
z[25] = -abb[6] * z[8];
z[26] = z[2] + z[25];
z[36] = z[12] + z[26] + z[28];
z[37] = 3 * abb[43];
z[38] = -z[6] + z[37];
z[39] = -z[4] + (T(1) / T(2)) * z[38];
z[40] = abb[45] * (T(3) / T(2));
z[41] = abb[46] + 2 * abb[49];
z[42] = z[40] + -z[41];
z[43] = z[39] + z[42];
z[44] = abb[11] * z[43];
z[45] = -abb[43] + 3 * abb[44] + -abb[50];
z[46] = abb[47] * (T(3) / T(2));
z[45] = abb[45] * (T(-5) / T(2)) + (T(1) / T(2)) * z[45] + z[46];
z[47] = abb[46] + z[45];
z[47] = abb[49] + (T(1) / T(2)) * z[47];
z[47] = abb[5] * z[47];
z[44] = z[44] + z[47];
z[47] = -abb[44] + 3 * abb[50];
z[48] = 5 * abb[43] + -z[47];
z[49] = abb[45] * (T(1) / T(2));
z[48] = z[4] + (T(1) / T(2)) * z[48] + z[49];
z[50] = -abb[46] + z[48];
z[50] = -abb[49] + (T(1) / T(2)) * z[50];
z[51] = -abb[3] * z[50];
z[52] = abb[47] * (T(5) / T(2));
z[53] = -z[40] + z[52];
z[47] = -abb[43] + z[47];
z[47] = -abb[46] + (T(1) / T(2)) * z[47] + -z[53];
z[47] = -abb[49] + (T(1) / T(2)) * z[47];
z[47] = abb[8] * z[47];
z[54] = abb[43] + z[33];
z[55] = abb[12] * z[54];
z[56] = abb[43] + abb[45] + -abb[46] + -abb[47];
z[56] = -abb[49] + (T(1) / T(2)) * z[56];
z[56] = abb[7] * z[56];
z[36] = (T(-1) / T(4)) * z[36] + z[44] + z[47] + z[51] + z[55] + z[56];
z[36] = prod_pow(abb[30], 2) * z[36];
z[9] = abb[7] * z[9];
z[47] = 3 * abb[42];
z[51] = -abb[47] + -z[6] + z[47];
z[56] = z[41] + z[51];
z[57] = abb[2] * z[56];
z[9] = z[9] + -z[57];
z[48] = -z[41] + z[48];
z[58] = abb[15] * z[48];
z[58] = -z[9] + z[11] + z[58];
z[58] = abb[34] * z[58];
z[1] = z[1] + z[12];
z[1] = (T(1) / T(2)) * z[1] + z[10];
z[45] = z[41] + z[45];
z[59] = abb[4] + abb[8];
z[60] = z[45] * z[59];
z[45] = abb[5] * z[45];
z[9] = z[1] + -z[9] + z[45] + -z[60];
z[60] = abb[55] * z[9];
z[61] = -z[12] + z[25];
z[62] = abb[8] * z[8];
z[62] = z[61] + z[62];
z[54] = -z[18] + z[54];
z[63] = abb[7] * (T(1) / T(2));
z[64] = z[54] * z[63];
z[65] = abb[4] * z[15];
z[17] = z[17] + -z[35] + (T(-1) / T(2)) * z[62] + z[64] + z[65];
z[62] = abb[54] * z[17];
z[64] = -abb[3] + abb[15];
z[64] = z[48] * z[64];
z[65] = abb[43] + z[6];
z[49] = z[49] + (T(1) / T(2)) * z[65];
z[65] = z[41] + z[46] + -z[49];
z[65] = abb[8] * z[65];
z[66] = z[12] + z[25];
z[8] = -z[8] * z[63];
z[8] = z[8] + -z[29] + z[64] + z[65] + (T(-1) / T(2)) * z[66];
z[8] = abb[36] * z[8];
z[18] = -z[18] + z[22];
z[18] = abb[20] * z[18];
z[29] = abb[17] * z[54];
z[0] = abb[27] * z[0];
z[54] = abb[18] * z[7];
z[0] = z[0] + z[18] + z[29] + z[54];
z[18] = abb[18] + abb[20];
z[5] = z[5] * z[18];
z[18] = abb[18] * abb[48];
z[15] = abb[19] * z[15];
z[0] = (T(1) / T(2)) * z[0] + z[5] + -z[15] + -z[18];
z[0] = (T(1) / T(2)) * z[0];
z[5] = abb[56] * z[0];
z[5] = z[5] + z[8] + z[36] + z[58] + z[60] + z[62];
z[8] = (T(3) / T(4)) * z[28];
z[15] = -z[8] + (T(-3) / T(4)) * z[14] + (T(3) / T(2)) * z[16];
z[18] = z[15] + z[35];
z[3] = z[3] + z[12];
z[3] = (T(1) / T(2)) * z[3] + z[10];
z[10] = 3 * abb[48];
z[29] = -z[10] + -z[19] + z[37];
z[29] = z[29] * z[63];
z[36] = 3 * abb[3];
z[54] = -z[36] * z[50];
z[58] = abb[48] + abb[50];
z[60] = abb[44] + z[37];
z[62] = abb[46] + z[58] + -z[60];
z[62] = abb[49] + (T(1) / T(2)) * z[62];
z[62] = abb[9] * z[62];
z[51] = abb[46] + z[51];
z[51] = abb[49] + (T(1) / T(2)) * z[51];
z[51] = abb[2] * z[51];
z[22] = abb[42] + -z[22];
z[22] = abb[10] * z[22];
z[64] = -3 * z[22] + 2 * z[32];
z[39] = -abb[46] + z[39] + z[40];
z[39] = -abb[49] + (T(1) / T(2)) * z[39];
z[39] = abb[8] * z[39];
z[65] = -abb[4] * z[19];
z[29] = (T(3) / T(2)) * z[3] + -z[18] + z[29] + z[39] + z[51] + z[54] + -3 * z[62] + -z[64] + z[65];
z[29] = abb[28] * z[29];
z[39] = abb[43] + abb[44];
z[54] = 3 * abb[45];
z[39] = 3 * z[39] + -z[54];
z[65] = 5 * abb[50];
z[66] = -z[39] + z[65];
z[67] = z[30] + z[46] + (T(-1) / T(2)) * z[66];
z[68] = abb[4] * (T(1) / T(2));
z[67] = z[67] * z[68];
z[15] = z[15] + 2 * z[35];
z[69] = abb[15] * z[43];
z[60] = -z[41] + z[60];
z[58] = z[58] + -z[60];
z[70] = abb[9] * z[58];
z[71] = z[69] + z[70];
z[72] = abb[50] + z[39];
z[30] = abb[47] * (T(3) / T(4)) + -z[30] + (T(1) / T(4)) * z[72];
z[30] = abb[7] * z[30];
z[72] = (T(1) / T(2)) * z[32];
z[73] = z[57] + z[72];
z[11] = (T(3) / T(2)) * z[11] + z[15] + -z[30] + z[67] + z[71] + z[73];
z[30] = abb[31] + -abb[32];
z[11] = z[11] * z[30];
z[28] = (T(3) / T(2)) * z[28];
z[30] = -z[28] + z[71];
z[67] = abb[8] * z[43];
z[71] = 2 * abb[4];
z[74] = z[34] * z[71];
z[75] = z[33] + z[39];
z[75] = -abb[48] + (T(1) / T(2)) * z[75];
z[76] = abb[7] * z[75];
z[14] = (T(-3) / T(2)) * z[14] + 3 * z[16];
z[16] = z[14] + 4 * z[35];
z[77] = (T(3) / T(2)) * z[12];
z[74] = -z[16] + -z[30] + z[67] + -z[74] + z[76] + z[77];
z[76] = -abb[29] * z[74];
z[78] = abb[4] * z[43];
z[79] = 2 * abb[46] + 4 * abb[49];
z[80] = -z[54] + z[79];
z[38] = abb[47] + -z[38] + z[80];
z[81] = abb[11] * z[38];
z[82] = z[78] + z[81];
z[83] = z[67] + z[82];
z[43] = abb[7] * z[43];
z[84] = 2 * abb[50];
z[60] = -abb[47] + -z[60] + z[84];
z[85] = 2 * abb[15];
z[85] = z[60] * z[85];
z[77] = z[43] + -z[77] + -z[83] + -z[85];
z[36] = z[36] * z[48];
z[48] = z[36] + -z[77];
z[48] = abb[30] * z[48];
z[11] = z[11] + z[29] + z[48] + z[76];
z[11] = abb[28] * z[11];
z[25] = (T(3) / T(2)) * z[25];
z[29] = z[25] + z[35];
z[48] = 9 * abb[43] + 5 * abb[44] + -7 * abb[50];
z[48] = (T(1) / T(2)) * z[48] + z[53];
z[76] = -abb[46] + z[48];
z[76] = -abb[49] + (T(1) / T(2)) * z[76];
z[76] = abb[15] * z[76];
z[86] = z[34] * z[63];
z[66] = -5 * abb[47] + z[66];
z[66] = -abb[48] + (T(1) / T(2)) * z[66];
z[66] = z[66] * z[68];
z[27] = abb[47] + abb[49] + (T(-1) / T(2)) * z[27];
z[27] = abb[8] * z[27];
z[8] = z[8] + z[27] + (T(1) / T(2)) * z[29] + -3 * z[55] + z[62] + z[66] + z[76] + z[86];
z[8] = abb[29] * z[8];
z[29] = abb[31] * z[74];
z[8] = z[8] + z[29];
z[8] = abb[29] * z[8];
z[13] = (T(3) / T(2)) * z[13];
z[7] = (T(3) / T(2)) * z[7] + -z[10];
z[10] = abb[42] + z[7] + -z[52];
z[10] = z[10] * z[68];
z[23] = (T(3) / T(4)) * z[23];
z[29] = -abb[42] + abb[47] * (T(1) / T(4));
z[52] = z[23] + z[29];
z[52] = abb[7] * z[52];
z[10] = -z[10] + -z[13] + z[15] + z[52] + -z[67] + -z[73];
z[10] = abb[31] * z[10];
z[15] = abb[4] * z[75];
z[52] = 2 * abb[47] + z[41];
z[62] = -z[6] + z[52];
z[66] = abb[8] * z[62];
z[34] = abb[7] * z[34];
z[30] = z[15] + -z[25] + z[30] + z[34] + z[35] + z[66];
z[30] = abb[29] * z[30];
z[34] = 2 * abb[8];
z[66] = -abb[7] + -z[34];
z[62] = z[62] * z[66];
z[25] = z[25] + z[28] + z[62] + -z[69] + -z[82];
z[25] = abb[30] * z[25];
z[62] = -z[63] + z[68];
z[62] = z[19] * z[62];
z[27] = -z[27] + (T(1) / T(2)) * z[35] + z[51] + z[62] + -z[72];
z[27] = abb[32] * z[27];
z[25] = z[10] + z[25] + z[27] + z[30];
z[25] = abb[32] * z[25];
z[27] = abb[7] * z[56];
z[30] = 3 * z[57];
z[3] = 3 * z[3] + z[27] + z[30] + z[69] + z[83];
z[27] = -abb[28] + abb[32];
z[3] = z[3] * z[27];
z[27] = z[34] + z[71];
z[51] = -2 * abb[44] + abb[50] + -z[52] + z[54];
z[27] = z[27] * z[51];
z[45] = 3 * z[45];
z[27] = (T(3) / T(2)) * z[2] + -z[27] + -z[43] + -z[45] + z[69] + z[81];
z[43] = -abb[31] * z[27];
z[50] = abb[15] * z[50];
z[20] = z[20] + -z[22];
z[22] = abb[46] + -z[49];
z[22] = abb[49] + (T(1) / T(2)) * z[22] + -z[29];
z[22] = abb[7] * z[22];
z[22] = (T(1) / T(4)) * z[2] + z[20] + z[22] + -z[44] + z[50] + z[57];
z[22] = abb[33] * z[22];
z[3] = z[3] + 3 * z[22] + z[43];
z[3] = abb[33] * z[3];
z[22] = -z[37] + z[65];
z[29] = abb[44] + z[22];
z[43] = 2 * abb[48];
z[29] = (T(1) / T(2)) * z[29] + -z[42] + -z[43] + -z[46];
z[29] = abb[4] * z[29];
z[42] = -z[41] + z[48];
z[42] = abb[15] * z[42];
z[44] = -abb[7] * z[58];
z[16] = -z[16] + z[28] + z[29] + z[42] + z[44] + -z[70] + -z[81];
z[16] = abb[35] * z[16];
z[29] = -z[45] + 2 * z[81];
z[38] = -abb[7] * z[38];
z[22] = 7 * abb[44] + -z[22];
z[22] = abb[47] * (T(-7) / T(2)) + abb[45] * (T(9) / T(2)) + (T(-1) / T(2)) * z[22] + -z[41];
z[22] = z[22] * z[59];
z[2] = z[2] + z[12];
z[2] = (T(-3) / T(2)) * z[2] + z[22] + -z[29] + -z[36] + z[38] + z[42];
z[2] = abb[37] * z[2];
z[12] = abb[30] * z[27];
z[22] = abb[7] + z[71];
z[19] = z[19] * z[22];
z[22] = -abb[8] * z[51];
z[19] = z[19] + -z[21] + z[22] + z[32] + -z[35] + -z[57];
z[19] = abb[31] * z[19];
z[12] = z[12] + z[19];
z[12] = abb[31] * z[12];
z[19] = -abb[38] * z[77];
z[21] = -abb[34] + -abb[35] + abb[38];
z[21] = z[21] * z[36];
z[2] = z[2] + z[3] + 3 * z[5] + z[8] + z[11] + z[12] + z[16] + z[19] + z[21] + z[24] + z[25];
z[3] = z[6] + z[37];
z[3] = (T(1) / T(2)) * z[3] + -z[79];
z[5] = z[3] + -z[53];
z[6] = abb[8] * z[5];
z[8] = -z[33] + z[39];
z[8] = (T(1) / T(2)) * z[8] + -z[43];
z[8] = abb[7] * z[8];
z[11] = 6 * z[55];
z[8] = z[6] + z[8] + z[11] + -z[14] + z[15] + -5 * z[35] + (T(-3) / T(2)) * z[61] + -2 * z[70] + z[85];
z[8] = abb[29] * z[8];
z[12] = -2 * abb[42] + abb[47] * (T(5) / T(4)) + z[23];
z[12] = abb[7] * z[12];
z[7] = abb[42] + z[4] + -z[7];
z[7] = z[7] * z[68];
z[6] = -z[6] + z[7] + z[12] + -z[13] + z[18] + -2 * z[57] + z[72];
z[6] = abb[32] * z[6];
z[7] = z[29] + -z[78] + -z[85];
z[5] = -abb[7] * z[5];
z[12] = abb[44] + 4 * abb[47] + z[80] + -z[84];
z[12] = z[12] * z[34];
z[5] = z[5] + z[7] + -z[11] + z[12] + (T(3) / T(2)) * z[26] + z[28];
z[5] = abb[30] * z[5];
z[11] = -abb[4] + abb[7];
z[11] = z[11] * z[31];
z[12] = -abb[15] * z[60];
z[11] = z[11] + z[12] + -z[35] + z[57] + z[64] + z[70];
z[11] = abb[28] * z[11];
z[3] = z[3] + z[4] + z[40] + -z[47];
z[3] = abb[7] * z[3];
z[1] = z[1] + -2 * z[20];
z[1] = 3 * z[1] + z[3] + -z[7] + -z[30] + z[67];
z[1] = abb[33] * z[1];
z[1] = z[1] + z[5] + z[6] + z[8] + z[10] + 2 * z[11];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[39] * z[17];
z[4] = abb[40] * z[9];
z[0] = abb[41] * z[0];
z[0] = z[0] + z[3] + z[4];
z[0] = 3 * z[0] + z[1];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_279_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-29.812321165349265736397436937642014984677930848764465516646227806"),stof<T>("21.120501999199564205866697115437052890912296680443702455469877917")}, std::complex<T>{stof<T>("6.6638457612309815136351091535161537510360849826576993189619766952"),stof<T>("-4.3292663793854642962410414458778339783202554025640303972787963686")}, std::complex<T>{stof<T>("-2.7746497569405719883393697404087014389888469506069783755378657547"),stof<T>("-8.7340709293922015227353946246992615888657892883107180369487199436")}, std::complex<T>{stof<T>("-5.8611628528904734679645278935876502018742341813352511315133635778"),stof<T>("-1.8354233672926516309069811541707324986093303035217831288881022342")}, std::complex<T>{stof<T>("8.6358126098310454563038976339963516408630811319422295070512293325"),stof<T>("10.5694942966848531536423757788699940874751195918325011658368221778")}, std::complex<T>{stof<T>("32.586832771024153115841736464661367024747806973898205762818911765"),stof<T>("-10.176453568299202168243448569686147120741720682222187379634005653")}, std::complex<T>{stof<T>("-21.160959375218185046798604894489504992692371121408418086891751401"),stof<T>("-5.865673783083879973420461566111248426459407782879456858558975723")}, std::complex<T>{stof<T>("17.271625219662090912607795267992703281726162263884459014102458665"),stof<T>("21.138988593369706307284751557739988174950239183665002331673644356")}, std::complex<T>{stof<T>("5.8613010041561580768595981069769996007932050068084892608785453734"),stof<T>("-0.374554134215508883980872766880911682695456406389013909999050086")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_279_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_279_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("26.186762785843156371612863720404858726247262629530927245005616939"),stof<T>("-0.66459106497212509293170202386920653165777274837504852969350685")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_279_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_279_DLogXconstant_part(base_point<T>, kend);
	value += f_4_279_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_279_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_279_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_279_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_279_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_279_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_279_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
