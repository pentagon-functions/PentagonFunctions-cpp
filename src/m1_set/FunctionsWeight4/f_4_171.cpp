/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_171.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_171_abbreviated (const std::array<T,27>& abb) {
T z[66];
z[0] = abb[21] + -abb[26];
z[1] = abb[20] + z[0];
z[2] = 2 * abb[6];
z[3] = z[1] * z[2];
z[4] = 2 * abb[22];
z[5] = 10 * abb[26] + -z[4];
z[6] = 2 * abb[25];
z[7] = abb[19] + z[6];
z[8] = z[5] + -z[7];
z[9] = 9 * abb[21];
z[10] = -z[8] + z[9];
z[10] = abb[3] * z[10];
z[3] = z[3] + -z[10];
z[11] = abb[22] + abb[26];
z[12] = 2 * abb[21];
z[13] = z[11] + -z[12];
z[14] = 2 * abb[20];
z[15] = z[7] + -z[14];
z[16] = 2 * abb[23];
z[17] = z[15] + -z[16];
z[18] = z[13] + -z[17];
z[19] = abb[5] * z[18];
z[20] = abb[19] * (T(1) / T(2));
z[21] = abb[25] + z[20];
z[22] = -abb[20] + z[21];
z[23] = abb[26] * (T(3) / T(2));
z[24] = abb[22] * (T(1) / T(2));
z[25] = -abb[21] + -abb[23] + z[22] + z[23] + -z[24];
z[25] = abb[0] * z[25];
z[26] = abb[22] * (T(3) / T(2));
z[22] = z[22] + -z[23] + -z[26];
z[23] = abb[23] + -z[12] + -z[22];
z[23] = abb[4] * z[23];
z[27] = 3 * abb[20];
z[9] = abb[23] + abb[26] * (T(-23) / T(2)) + abb[22] * (T(5) / T(2)) + z[9] + 3 * z[21] + -z[27];
z[9] = abb[9] * z[9];
z[28] = 2 * abb[26];
z[29] = abb[21] + -4 * abb[23] + z[15] + z[28];
z[29] = abb[7] * z[29];
z[30] = abb[22] + -abb[26];
z[31] = abb[21] + z[30];
z[32] = abb[8] * z[31];
z[9] = z[3] + z[9] + z[19] + z[23] + z[25] + z[29] + -z[32];
z[9] = abb[18] * z[9];
z[23] = -abb[21] + z[28];
z[25] = 4 * abb[22];
z[33] = abb[20] + -abb[25];
z[34] = -abb[19] + z[33];
z[35] = -z[23] + z[25] + -z[34];
z[35] = abb[4] * z[35];
z[36] = -abb[22] + 5 * abb[26];
z[37] = abb[21] * (T(-9) / T(2)) + -z[21] + z[36];
z[37] = abb[3] * z[37];
z[38] = -abb[20] + abb[22] + -z[12] + z[28];
z[39] = abb[9] * z[38];
z[40] = -abb[24] + z[33];
z[20] = z[20] + z[40];
z[41] = abb[21] * (T(1) / T(2));
z[42] = abb[22] + z[20] + z[41];
z[42] = abb[0] * z[42];
z[43] = 3 * abb[26];
z[44] = -z[4] + z[43];
z[45] = -abb[19] + abb[25] * (T(-7) / T(4)) + abb[20] * (T(5) / T(4)) + -z[12] + z[44];
z[45] = abb[7] * z[45];
z[46] = abb[25] * (T(5) / T(4));
z[47] = abb[24] + abb[20] * (T(-3) / T(4)) + -z[11] + z[46];
z[47] = abb[2] * z[47];
z[48] = 3 * z[32];
z[35] = z[35] + -z[37] + z[39] + z[42] + z[45] + z[47] + -z[48];
z[35] = prod_pow(abb[12], 2) * z[35];
z[42] = 10 * abb[21] + abb[22] + -13 * abb[26] + z[7] + z[16];
z[42] = abb[9] * z[42];
z[42] = -z[10] + z[42];
z[23] = -z[17] + -z[23];
z[23] = abb[0] * z[23];
z[13] = -abb[23] + -z[13] + -z[33];
z[13] = abb[4] * z[13];
z[33] = abb[22] + z[43];
z[45] = 2 * abb[19];
z[47] = 5 * abb[23] + abb[25] * (T(-7) / T(2)) + abb[20] * (T(9) / T(2)) + -z[33] + -z[45];
z[47] = abb[7] * z[47];
z[49] = abb[19] + abb[20] * (T(-5) / T(2)) + abb[25] * (T(3) / T(2)) + z[11] + -z[16];
z[49] = abb[2] * z[49];
z[13] = z[13] + -z[19] + z[23] + -z[42] + z[47] + z[49];
z[13] = abb[17] * z[13];
z[19] = 3 * abb[21];
z[23] = z[19] + z[21];
z[47] = abb[23] * (T(3) / T(2));
z[36] = z[23] + -z[36] + z[47];
z[36] = abb[5] * z[36];
z[49] = 4 * abb[26];
z[50] = -abb[22] + -z[27] + z[49];
z[41] = abb[23] * (T(7) / T(2)) + z[41] + -z[50];
z[41] = abb[0] * z[41];
z[51] = -abb[21] + abb[23];
z[52] = abb[19] + -abb[24];
z[53] = -z[51] + z[52];
z[53] = abb[4] * z[53];
z[54] = 4 * abb[21];
z[50] = z[50] + -z[54];
z[50] = abb[6] * z[50];
z[55] = abb[2] * z[52];
z[56] = abb[9] * z[51];
z[37] = -z[36] + -z[37] + z[41] + z[50] + z[53] + -z[55] + z[56];
z[37] = abb[10] * z[37];
z[41] = 3 * abb[22];
z[50] = 9 * abb[26] + -z[15] + -z[41];
z[53] = 6 * abb[21];
z[56] = z[16] + z[53];
z[57] = z[50] + -z[56];
z[57] = abb[9] * z[57];
z[58] = z[41] + z[43];
z[59] = -z[6] + z[14];
z[60] = z[58] + z[59];
z[61] = 2 * abb[24];
z[62] = abb[19] + -z[12] + z[60] + -z[61];
z[62] = abb[4] * z[62];
z[33] = -z[16] + z[33];
z[63] = -abb[21] + z[33] + 2 * z[52];
z[63] = abb[0] * z[63];
z[64] = -z[2] * z[38];
z[65] = abb[7] * z[51];
z[62] = -z[10] + -z[57] + z[62] + z[63] + z[64] + -2 * z[65];
z[62] = abb[12] * z[62];
z[63] = abb[7] + -abb[9];
z[51] = z[51] * z[63];
z[63] = -abb[20] + abb[23];
z[52] = -abb[26] + -z[52] + z[63];
z[52] = abb[0] * z[52];
z[64] = abb[20] + -z[0];
z[64] = abb[6] * z[64];
z[51] = z[51] + z[52] + -z[55] + z[64];
z[51] = abb[13] * z[51];
z[37] = z[37] + 2 * z[51] + z[62];
z[37] = abb[10] * z[37];
z[47] = -z[4] + z[23] + -z[28] + -z[47];
z[47] = abb[5] * z[47];
z[51] = abb[23] * (T(1) / T(2));
z[52] = -z[21] + z[31] + z[51];
z[52] = abb[0] * z[52];
z[18] = -abb[4] * z[18];
z[18] = z[18] + -z[29] + -z[42] + z[47] + z[52];
z[18] = abb[15] * z[18];
z[29] = -z[11] + -z[40];
z[29] = abb[4] * z[29];
z[38] = abb[6] * z[38];
z[29] = z[29] + z[38] + -z[39];
z[38] = abb[20] * (T(3) / T(2));
z[39] = abb[25] * (T(1) / T(2)) + z[4] + -z[38] + z[49] + -z[54];
z[39] = abb[7] * z[39];
z[28] = -z[28] + z[61];
z[4] = abb[25] * (T(5) / T(2)) + -z[4] + z[28] + -z[38];
z[4] = abb[2] * z[4];
z[4] = z[4] + 2 * z[29] + z[39];
z[4] = abb[12] * z[4];
z[21] = abb[26] * (T(13) / T(2)) + -z[21] + -z[24] + -z[53];
z[21] = abb[9] * z[21];
z[29] = 7 * abb[26];
z[38] = abb[19] + abb[20] * (T(1) / T(4)) + -z[29] + z[46] + z[53];
z[38] = abb[7] * z[38];
z[20] = abb[26] * (T(1) / T(2)) + z[20] + z[24];
z[24] = -abb[0] + -abb[4];
z[20] = z[20] * z[24];
z[24] = abb[25] * (T(-9) / T(4)) + abb[20] * (T(7) / T(4)) + -z[28];
z[24] = abb[2] * z[24];
z[28] = abb[20] + abb[22] + -6 * abb[26] + z[53];
z[28] = abb[6] * z[28];
z[20] = z[20] + z[21] + 3 * z[24] + z[28] + z[38];
z[20] = abb[13] * z[20];
z[4] = z[4] + z[20];
z[4] = abb[13] * z[4];
z[20] = abb[23] + z[22];
z[20] = abb[4] * z[20];
z[21] = abb[26] * (T(9) / T(2)) + -z[23] + -z[26] + -z[63];
z[21] = abb[9] * z[21];
z[22] = -abb[20] + (T(1) / T(2)) * z[11] + -z[51];
z[22] = abb[0] * z[22];
z[23] = abb[23] + z[30];
z[24] = abb[1] * z[23];
z[20] = z[20] + z[21] + z[22] + -3 * z[24] + z[36] + z[48];
z[20] = prod_pow(abb[11], 2) * z[20];
z[21] = 8 * abb[21] + -z[50];
z[21] = abb[9] * z[21];
z[22] = abb[0] + -3 * abb[6];
z[1] = z[1] * z[22];
z[0] = -abb[22] + z[0];
z[0] = abb[4] * z[0];
z[0] = 2 * z[0] + z[1] + z[10] + -z[21] + z[32];
z[0] = abb[14] * z[0];
z[1] = -z[15] + z[58];
z[10] = -z[1] + z[12];
z[10] = abb[4] * z[10];
z[12] = abb[0] * z[31];
z[3] = -z[3] + z[10] + z[12] + -z[21];
z[3] = abb[16] * z[3];
z[10] = 4 * abb[20];
z[7] = z[7] + -z[10] + z[30];
z[7] = abb[14] * z[7];
z[12] = -z[11] + -z[17];
z[12] = abb[18] * z[12];
z[7] = z[7] + z[12];
z[7] = abb[2] * z[7];
z[12] = -z[30] + z[34];
z[15] = -abb[2] + abb[4];
z[12] = z[12] * z[15];
z[15] = -abb[0] * z[23];
z[12] = z[12] + z[15] + z[24];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[0] = z[0] + z[3] + z[4] + z[7] + z[9] + (T(13) / T(4)) * z[12] + z[13] + z[18] + z[20] + z[35] + z[37];
z[3] = 4 * abb[24];
z[4] = -5 * abb[25] + -z[3] + z[25] + z[27] + z[49];
z[4] = abb[2] * z[4];
z[7] = abb[20] + -3 * abb[25] + z[5] + -z[45] + -z[56];
z[7] = abb[7] * z[7];
z[9] = 6 * z[32] + z[57];
z[12] = 3 * abb[19];
z[13] = -z[3] + z[12];
z[15] = z[13] + -z[16] + z[60];
z[17] = -abb[0] * z[15];
z[12] = -9 * abb[22] + -z[12] + z[43] + z[59];
z[12] = abb[4] * z[12];
z[4] = z[4] + -z[7] + z[9] + z[12] + z[17];
z[4] = abb[12] * z[4];
z[6] = -z[6] + z[10] + z[13] + z[33];
z[6] = abb[0] * z[6];
z[12] = -abb[20] + -z[19] + z[44];
z[2] = z[2] * z[12];
z[2] = z[2] + -z[57];
z[12] = abb[19] + -4 * abb[25] + -z[3] + z[10] + z[58];
z[12] = abb[4] * z[12];
z[3] = abb[19] + z[3];
z[3] = -9 * abb[20] + 11 * abb[25] + 2 * z[3] + -z[5];
z[3] = abb[2] * z[3];
z[3] = z[2] + z[3] + z[6] + z[7] + z[12];
z[3] = abb[13] * z[3];
z[1] = z[1] + -z[16];
z[1] = abb[4] * z[1];
z[5] = 3 * abb[23] + -z[8] + z[53];
z[5] = abb[5] * z[5];
z[6] = abb[23] + -z[11] + z[14];
z[6] = abb[0] * z[6];
z[1] = z[1] + -z[5] + z[6] + -z[9] + 6 * z[24];
z[1] = abb[11] * z[1];
z[6] = -7 * abb[23] + -z[10] + z[29] + -z[41];
z[6] = abb[0] * z[6];
z[7] = -abb[4] * z[15];
z[2] = -z[2] + z[5] + z[6] + z[7] + 4 * z[55];
z[2] = abb[10] * z[2];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_171_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-27.064003765477361291903343315134194674452482918983161285623938572"),stof<T>("-7.475815598440939566373764000869555250811519303063729817827706412")}, std::complex<T>{stof<T>("25.406887547674456462083990034759512758820608544963668591270561935"),stof<T>("8.070131904408418115506668893688317704311812989564063162806289793")}, std::complex<T>{stof<T>("-3.5823262174412056407399918804820018070905886983814217692874447427"),stof<T>("12.3752635558788605610889581689936822618580864357541245923118307862")}, std::complex<T>{stof<T>("3.3410809551987732464022600225015741210190396777484500563110565468"),stof<T>("10.8849721549012200984460607286720660287055230465905956225658644869")}, std::complex<T>{stof<T>("35.932373961929914325486895145197051595913713498136059270479028952"),stof<T>("3.816690375959525599106521656282461044419170850126331496244336385")}, std::complex<T>{stof<T>("3.8566445899904327610627187593587602109724270979622266648648373222"),stof<T>("-11.6974292283979976256833080040678045298066757864980200816653188429")}, std::complex<T>{stof<T>("-22.041992382078340932419957795233420125262166818909406690629251021"),stof<T>("-19.567980559334830342593821975949394127295349196743778856181023413")}, std::complex<T>{stof<T>("-9.1426885690017801539062787089396153253430689787337028804324829593"),stof<T>("2.9812908950005510318615921796612164743409378036812938109368580839")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_171_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_171_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2.2282605114505826974806659358036071616028321133764056491809925788"),stof<T>("7.6551579261975695026213270820063958884217282201898962183312468494")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), f_1_1(k), f_1_3(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real())};

                    
            return f_4_171_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_171_DLogXconstant_part(base_point<T>, kend);
	value += f_4_171_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_171_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_171_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_171_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_171_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_171_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_171_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
