/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_127.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_127_abbreviated (const std::array<T,56>& abb) {
T z[79];
z[0] = -abb[42] + abb[43] + abb[44];
z[1] = -abb[45] + z[0];
z[2] = abb[25] * z[1];
z[3] = abb[24] * z[1];
z[2] = z[2] + z[3];
z[4] = (T(1) / T(4)) * z[2];
z[5] = abb[46] + abb[48];
z[6] = 3 * abb[47] + z[5];
z[7] = abb[50] * (T(1) / T(2));
z[8] = abb[51] + z[7];
z[6] = (T(1) / T(2)) * z[6] + z[8];
z[6] = abb[49] + (T(1) / T(2)) * z[6];
z[6] = abb[4] * z[6];
z[9] = abb[21] + abb[23];
z[10] = abb[45] * z[9];
z[11] = (T(1) / T(2)) * z[10];
z[12] = abb[52] * (T(1) / T(2));
z[13] = abb[17] * z[12];
z[14] = z[11] + -z[13];
z[15] = abb[48] + abb[50];
z[16] = abb[46] + abb[47];
z[17] = z[15] + z[16];
z[18] = abb[12] * z[17];
z[19] = (T(1) / T(4)) * z[18];
z[0] = z[0] * z[9];
z[9] = abb[48] + z[16];
z[20] = 3 * abb[50] + z[9];
z[21] = abb[8] * z[20];
z[21] = z[0] + z[21];
z[22] = abb[47] + abb[51];
z[23] = (T(1) / T(2)) * z[22];
z[24] = abb[14] * z[23];
z[25] = -z[21] + z[24];
z[26] = abb[51] * (T(3) / T(2));
z[27] = abb[48] * (T(1) / T(2));
z[28] = abb[46] * (T(1) / T(2)) + z[27];
z[29] = -abb[47] + abb[50] * (T(-3) / T(2)) + -z[26] + z[28];
z[30] = abb[0] * (T(1) / T(2));
z[29] = z[29] * z[30];
z[31] = abb[50] + abb[47] * (T(5) / T(2)) + z[5];
z[26] = z[26] + z[31];
z[26] = abb[49] + (T(1) / T(2)) * z[26];
z[26] = abb[7] * z[26];
z[32] = abb[52] * (T(1) / T(4));
z[33] = abb[19] * z[32];
z[34] = abb[47] + abb[50] + abb[51];
z[35] = abb[49] + z[34];
z[35] = abb[2] * z[35];
z[36] = abb[49] + z[16];
z[37] = abb[48] + z[36];
z[38] = abb[1] * z[37];
z[39] = abb[22] * z[1];
z[40] = abb[16] * abb[52];
z[25] = -z[4] + z[6] + z[14] + z[19] + (T(1) / T(2)) * z[25] + z[26] + z[29] + -z[33] + z[35] + -z[38] + (T(-1) / T(4)) * z[39] + (T(-3) / T(4)) * z[40];
z[26] = abb[49] + z[22];
z[26] = abb[11] * z[26];
z[29] = abb[20] * z[1];
z[25] = (T(1) / T(2)) * z[25] + -z[26] + (T(-1) / T(8)) * z[29];
z[25] = abb[31] * z[25];
z[40] = (T(1) / T(2)) * z[17];
z[41] = abb[49] + z[40];
z[42] = abb[5] * z[41];
z[43] = abb[49] + -abb[50];
z[44] = abb[15] * z[43];
z[45] = z[42] + -z[44];
z[46] = abb[16] * z[12];
z[47] = z[35] + -z[46];
z[48] = abb[49] + z[23];
z[49] = abb[7] * z[48];
z[49] = z[45] + z[47] + z[49];
z[43] = z[23] + z[43];
z[43] = abb[4] * z[43];
z[50] = abb[14] * z[22];
z[21] = z[21] + -z[50];
z[34] = abb[0] * z[34];
z[51] = (T(1) / T(2)) * z[1];
z[52] = abb[25] * z[51];
z[12] = abb[19] * z[12];
z[14] = -z[12] + z[14] + (T(-1) / T(2)) * z[21] + -z[34] + z[43] + z[49] + -z[52];
z[14] = (T(1) / T(2)) * z[14] + -z[26];
z[21] = -abb[33] * z[14];
z[34] = abb[51] * (T(1) / T(2));
z[7] = abb[47] + z[7] + z[28] + z[34];
z[28] = abb[0] * z[7];
z[12] = z[12] + z[28];
z[43] = abb[8] * z[17];
z[10] = z[0] + -z[10];
z[43] = z[10] + z[43];
z[23] = abb[7] * z[23];
z[53] = abb[22] * z[51];
z[54] = z[23] + -z[53];
z[55] = abb[24] * z[51];
z[52] = z[12] + -z[24] + z[43] + z[52] + -z[54] + z[55];
z[13] = z[13] + z[26];
z[56] = abb[16] * z[32];
z[56] = z[13] + z[56];
z[19] = -z[19] + (T(1) / T(4)) * z[29];
z[6] = -z[6] + z[19] + (T(1) / T(2)) * z[52] + z[56];
z[52] = abb[32] * (T(1) / T(2));
z[57] = -z[6] * z[52];
z[58] = abb[7] * z[17];
z[59] = z[43] + z[58];
z[60] = abb[12] * z[40];
z[51] = abb[20] * z[51];
z[51] = -z[51] + z[60];
z[60] = abb[6] * z[41];
z[36] = abb[10] * z[36];
z[61] = z[36] + -z[60];
z[62] = abb[48] + abb[49];
z[62] = abb[1] * z[62];
z[63] = z[61] + z[62];
z[64] = abb[9] * z[17];
z[44] = z[44] + z[64];
z[65] = -abb[50] + z[9];
z[65] = abb[49] + (T(1) / T(2)) * z[65];
z[65] = abb[4] * z[65];
z[66] = z[44] + -z[51] + (T(1) / T(2)) * z[59] + -z[63] + -z[65];
z[66] = abb[29] * z[66];
z[58] = -z[3] + z[58];
z[67] = z[39] + -z[58];
z[68] = abb[4] * (T(1) / T(2));
z[20] = -z[20] * z[68];
z[20] = z[20] + z[45] + z[63] + (T(1) / T(2)) * z[67];
z[63] = abb[30] * (T(1) / T(2));
z[20] = z[20] * z[63];
z[67] = abb[7] * z[7];
z[67] = -z[46] + z[67];
z[69] = -z[38] + z[44];
z[70] = abb[50] + abb[51];
z[71] = z[5] + -z[70];
z[72] = z[68] * z[71];
z[72] = z[13] + -z[67] + -z[69] + z[72];
z[73] = abb[28] * (T(1) / T(2));
z[72] = z[72] * z[73];
z[20] = z[20] + z[21] + z[25] + z[57] + (T(1) / T(2)) * z[66] + z[72];
z[20] = m1_set::bc<T>[0] * z[20];
z[21] = -abb[40] * z[14];
z[25] = z[15] + -z[16];
z[25] = abb[8] * z[25];
z[10] = -z[10] + z[25];
z[25] = abb[7] * z[41];
z[57] = abb[4] * z[40];
z[66] = abb[1] * z[16];
z[72] = z[57] + z[66];
z[74] = (T(1) / T(2)) * z[10] + -z[25] + z[51] + -z[60] + -z[72];
z[75] = -z[36] + (T(-1) / T(2)) * z[74];
z[75] = abb[39] * z[75];
z[1] = abb[26] * z[1];
z[76] = abb[19] * z[22];
z[1] = z[1] + -z[76];
z[76] = (T(1) / T(4)) * z[17];
z[76] = abb[17] * z[76];
z[22] = abb[16] * z[22];
z[1] = (T(1) / T(4)) * z[1] + (T(-1) / T(4)) * z[22] + z[76];
z[22] = abb[41] * z[1];
z[76] = abb[0] + abb[7] + abb[14];
z[76] = -abb[27] + (T(1) / T(4)) * z[76];
z[77] = abb[41] * abb[52] * z[76];
z[22] = z[22] + z[77];
z[77] = abb[18] * z[17];
z[77] = (T(1) / T(8)) * z[77];
z[78] = -abb[41] * z[77];
z[20] = z[20] + z[21] + (T(1) / T(2)) * z[22] + z[75] + z[78];
z[20] = (T(1) / T(4)) * z[20];
z[21] = abb[53] * z[74];
z[22] = abb[3] * z[17];
z[74] = z[3] + -z[22] + z[43];
z[41] = -abb[4] * z[41];
z[41] = z[41] + z[42] + z[64] + (T(1) / T(2)) * z[74];
z[41] = abb[37] * z[41];
z[10] = z[10] + z[22] + -z[39];
z[40] = abb[13] * z[40];
z[25] = (T(-1) / T(2)) * z[10] + z[25] + z[40] + -z[60] + -z[66];
z[25] = abb[36] * z[25];
z[15] = abb[8] * z[15];
z[42] = abb[7] * abb[49];
z[15] = z[15] + -z[42];
z[44] = -z[15] + -z[44] + -z[53] + z[60];
z[64] = abb[13] * z[17];
z[27] = abb[49] * (T(1) / T(2)) + z[16] + z[27];
z[27] = abb[1] * z[27];
z[9] = abb[50] + 3 * z[9];
z[9] = abb[49] + (T(1) / T(2)) * z[9];
z[9] = z[9] * z[68];
z[9] = z[9] + z[27] + -z[36] + (T(1) / T(2)) * z[44] + (T(-1) / T(4)) * z[64];
z[9] = prod_pow(abb[29], 2) * z[9];
z[18] = z[18] + -z[29] + -z[39];
z[17] = abb[4] * z[17];
z[17] = z[17] + z[18] + -z[22];
z[17] = abb[35] * z[17];
z[27] = -abb[35] + -abb[37];
z[27] = z[27] * z[64];
z[17] = z[17] + z[27];
z[27] = z[12] + -z[40];
z[2] = -z[2] + z[50];
z[29] = z[2] + -z[22];
z[23] = -z[23] + -z[27] + (T(1) / T(2)) * z[29] + z[46];
z[23] = abb[34] * z[23];
z[29] = -z[22] + z[59];
z[29] = (T(1) / T(2)) * z[29] + -z[65] + z[69];
z[44] = -abb[38] * z[29];
z[9] = z[9] + (T(1) / T(2)) * z[17] + z[21] + z[23] + z[25] + z[41] + z[44];
z[0] = -z[0] + z[24];
z[16] = -abb[48] + abb[50] * (T(-5) / T(4)) + (T(-1) / T(2)) * z[16];
z[16] = abb[8] * z[16];
z[0] = (T(1) / T(2)) * z[0] + -z[4] + z[11] + z[16] + (T(1) / T(4)) * z[35];
z[4] = z[18] + -z[38];
z[11] = -abb[47] + (T(-1) / T(4)) * z[5];
z[11] = (T(1) / T(3)) * z[11] + (T(-1) / T(4)) * z[70];
z[11] = z[11] * z[30];
z[16] = abb[49] + (T(1) / T(3)) * z[31] + z[34];
z[16] = z[16] * z[68];
z[17] = abb[16] * (T(-5) / T(24)) + abb[19] * (T(-1) / T(12));
z[17] = abb[52] * z[17];
z[18] = abb[50] + z[5];
z[21] = abb[47] * (T(1) / T(3)) + abb[49] * (T(1) / T(4)) + abb[51] * (T(5) / T(24)) + (T(1) / T(8)) * z[18];
z[21] = abb[7] * z[21];
z[23] = -abb[17] * z[32];
z[0] = (T(1) / T(3)) * z[0] + (T(1) / T(12)) * z[4] + z[11] + z[16] + z[17] + z[21] + z[23] + (T(-7) / T(12)) * z[26] + (T(-1) / T(6)) * z[36];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[4] = (T(1) / T(2)) * z[2];
z[11] = abb[47] * (T(3) / T(2)) + z[18] + z[34];
z[11] = abb[7] * z[11];
z[11] = z[4] + z[11] + -z[28] + z[53];
z[5] = -abb[47] + -3 * z[5];
z[5] = (T(1) / T(2)) * z[5] + z[8];
z[5] = z[5] * z[68];
z[5] = z[5] + (T(1) / T(2)) * z[11] + z[19] + -z[33] + z[40] + -z[56] + z[69];
z[5] = abb[31] * z[5];
z[4] = -z[4] + z[12] + -z[46] + z[51] + z[54] + z[57] + -z[64];
z[4] = z[4] * z[52];
z[8] = abb[4] * z[37];
z[8] = z[8] + -z[69];
z[11] = z[8] + z[51];
z[2] = z[2] + -z[43];
z[16] = z[2] + -z[22];
z[12] = z[11] + -z[12] + (T(1) / T(2)) * z[16] + -z[53] + -z[67];
z[12] = z[12] * z[73];
z[16] = z[39] + z[59];
z[11] = -z[11] + (T(1) / T(2)) * z[16] + z[40];
z[11] = abb[29] * z[11];
z[16] = abb[4] * z[48];
z[2] = (T(1) / T(2)) * z[2] + -z[13] + z[16] + -z[27];
z[13] = -abb[33] * z[2];
z[16] = -abb[30] * z[29];
z[4] = z[4] + z[5] + z[11] + z[12] + z[13] + z[16];
z[4] = z[4] * z[73];
z[5] = abb[54] * z[14];
z[6] = abb[31] * z[6];
z[12] = z[40] + z[53] + z[61] + -z[72];
z[13] = abb[29] * z[12];
z[6] = z[6] + z[13];
z[6] = z[6] * z[52];
z[3] = z[3] + -z[10];
z[10] = abb[4] * abb[50];
z[3] = (T(1) / T(2)) * z[3] + -z[10] + z[42] + z[45] + -z[60] + z[62];
z[3] = z[3] * z[63];
z[10] = z[10] + z[40];
z[13] = z[10] + -z[38] + -z[45] + (T(1) / T(2)) * z[58];
z[13] = abb[31] * z[13];
z[12] = -abb[32] * z[12];
z[8] = -z[8] + z[15] + z[36];
z[8] = abb[29] * z[8];
z[3] = z[3] + z[8] + z[12] + z[13];
z[3] = z[3] * z[63];
z[8] = z[30] * z[71];
z[12] = abb[8] * abb[50];
z[8] = -z[8] + z[12] + z[26];
z[7] = -abb[49] + -z[7];
z[7] = abb[7] * z[7];
z[7] = z[7] + z[8] + z[38] + -z[47];
z[12] = abb[31] * (T(1) / T(2));
z[7] = z[7] * z[12];
z[7] = z[7] + -z[11];
z[7] = z[7] * z[12];
z[8] = -z[8] + -z[10] + z[49] + z[55];
z[10] = abb[33] * (T(1) / T(2));
z[11] = abb[31] + -z[10];
z[8] = z[8] * z[11];
z[2] = abb[32] * z[2];
z[2] = z[2] + z[8];
z[2] = z[2] * z[10];
z[8] = -abb[52] * z[76];
z[1] = -z[1] + z[8];
z[1] = (T(1) / T(2)) * z[1] + z[77];
z[1] = abb[55] * z[1];
z[8] = abb[53] * z[36];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + (T(1) / T(2)) * z[9];
z[0] = (T(1) / T(4)) * z[0];
return {z[20], z[0]};
}


template <typename T> std::complex<T> f_4_127_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.57848537294422228866713721841823222095640203354545079736478526726"),stof<T>("0.42192066650393438047409426804076352096470571214405250900791616523")}, std::complex<T>{stof<T>("-1.1386357306230456286697480533168712219725428273139849087001593678"),stof<T>("1.2566901704364390583674834715793106417003110092257314607694543054")}, std::complex<T>{stof<T>("-0.78384405423812051775152220081265957196972640515220995034634156948"),stof<T>("0.48941725065523810735097743560424907116525855941236699874812864164")}, std::complex<T>{stof<T>("-1.3212496324146752793794140156688067857013151975707890667213928086"),stof<T>("0.8434002737337653198416459035053544840998950619754997108718017326")}, std::complex<T>{stof<T>("-0.79338851020529031669639358825940753011008742260763087092242069429"),stof<T>("0.65048126298211652630343498218673054426221331309112239872519042328")}, std::complex<T>{stof<T>("-0.56015035767882334000261083489863900101614079376853411133537410049"),stof<T>("0.83476950393250467789338920353854712073560529708167895176153814012")}, std::complex<T>{stof<T>("-0.45206819008996940444056879493693166461096524631315406723780644064"),stof<T>("0.6183869553716871503642261524770373176934380471354809098273440296")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_127_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_127_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.02435556195130010557768050841578811653053069984159512999953262175"),stof<T>("-0.40509873509489876701374346996009466991183351566969552760882937672")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W73(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_127_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_127_DLogXconstant_part(base_point<T>, kend);
	value += f_4_127_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_127_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_127_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_127_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_127_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_127_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_127_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
