/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_521.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_521_abbreviated (const std::array<T,61>& abb) {
T z[99];
z[0] = 3 * abb[47];
z[1] = abb[49] * (T(1) / T(2));
z[2] = z[0] + -z[1];
z[3] = abb[50] * (T(3) / T(4));
z[4] = abb[43] + abb[44] * (T(1) / T(2));
z[5] = z[3] + z[4];
z[6] = abb[42] + -abb[46];
z[7] = z[2] + -z[5] + (T(-5) / T(4)) * z[6];
z[7] = abb[0] * z[7];
z[8] = abb[47] + abb[50];
z[9] = -abb[49] + z[8];
z[9] = -z[4] + (T(1) / T(2)) * z[9];
z[10] = 3 * abb[9];
z[9] = z[9] * z[10];
z[11] = abb[45] + abb[46];
z[12] = (T(1) / T(2)) * z[11];
z[13] = -z[1] + z[12];
z[14] = abb[50] * (T(1) / T(2));
z[15] = -abb[47] + z[14];
z[16] = -z[13] + z[15];
z[17] = abb[4] * z[16];
z[18] = (T(3) / T(2)) * z[17];
z[19] = z[9] + z[18];
z[20] = abb[49] + z[11];
z[21] = 3 * abb[50];
z[22] = z[20] + -z[21];
z[23] = 2 * abb[43] + abb[44];
z[24] = (T(1) / T(2)) * z[22] + z[23];
z[25] = abb[1] * z[24];
z[26] = z[4] + (T(1) / T(4)) * z[22];
z[27] = abb[7] * z[26];
z[28] = -abb[45] + abb[49];
z[29] = -abb[42] + z[28];
z[30] = abb[2] * z[29];
z[25] = z[25] + z[27] + -z[30];
z[27] = abb[53] + abb[54] + abb[55] + abb[56];
z[31] = (T(3) / T(4)) * z[27];
z[32] = abb[24] * z[31];
z[33] = z[25] + -z[32];
z[34] = abb[51] + abb[52];
z[35] = abb[18] * z[34];
z[36] = abb[16] * z[34];
z[37] = z[35] + z[36];
z[38] = abb[42] * (T(1) / T(2));
z[39] = abb[50] * (T(3) / T(2));
z[40] = abb[45] + abb[46] * (T(1) / T(2)) + z[38] + -z[39];
z[41] = z[4] + (T(1) / T(2)) * z[40];
z[41] = abb[3] * z[41];
z[40] = z[23] + z[40];
z[42] = abb[8] * z[40];
z[43] = abb[15] * z[34];
z[44] = (T(3) / T(2)) * z[43];
z[45] = abb[23] * z[31];
z[46] = abb[14] * z[29];
z[47] = (T(3) / T(4)) * z[46];
z[41] = z[7] + -z[19] + z[33] + (T(3) / T(4)) * z[37] + z[41] + z[42] + z[44] + z[45] + z[47];
z[41] = abb[27] * z[41];
z[42] = (T(1) / T(2)) * z[6];
z[48] = z[39] + z[42];
z[49] = -abb[49] + z[48];
z[49] = -z[4] + (T(1) / T(2)) * z[49];
z[49] = abb[0] * z[49];
z[50] = z[35] + -z[36];
z[47] = z[47] + (T(3) / T(4)) * z[50];
z[51] = 2 * z[30];
z[45] = -z[45] + -z[47] + z[49] + z[51];
z[52] = -abb[46] + abb[45] * (T(-3) / T(2)) + -z[1] + z[21] + -z[38];
z[52] = -z[23] + (T(1) / T(2)) * z[52];
z[52] = abb[3] * z[52];
z[3] = z[3] + -z[4];
z[53] = 3 * abb[45];
z[54] = -abb[46] + z[53];
z[55] = abb[49] * (T(5) / T(4));
z[54] = abb[42] + z[3] + (T(1) / T(4)) * z[54] + -z[55];
z[54] = abb[8] * z[54];
z[56] = abb[12] * z[24];
z[57] = (T(3) / T(2)) * z[27];
z[58] = abb[19] * z[57];
z[56] = z[56] + -z[58];
z[59] = abb[21] * z[57];
z[60] = z[56] + -z[59];
z[61] = 4 * abb[43] + 2 * abb[44];
z[22] = z[22] + z[61];
z[62] = abb[1] * z[22];
z[52] = -z[45] + z[52] + z[54] + -z[60] + -z[62];
z[54] = -abb[30] * z[52];
z[63] = abb[30] * z[32];
z[41] = z[41] + z[54] + z[63];
z[41] = abb[27] * z[41];
z[9] = z[9] + -z[18];
z[18] = abb[42] * (T(5) / T(2));
z[28] = abb[46] * (T(-3) / T(2)) + -z[18] + z[28] + z[39];
z[54] = abb[3] * (T(1) / T(2));
z[28] = z[28] * z[54];
z[64] = abb[23] * z[57];
z[65] = -z[60] + z[64];
z[66] = abb[8] * z[29];
z[67] = -z[65] + 2 * z[66];
z[68] = abb[7] * z[24];
z[69] = abb[46] + abb[49];
z[70] = -abb[45] + -abb[50] + z[69];
z[71] = abb[5] * z[70];
z[72] = (T(3) / T(4)) * z[71];
z[7] = z[7] + -z[9] + z[28] + -z[30] + -z[32] + z[47] + -z[67] + z[68] + z[72];
z[7] = abb[32] * z[7];
z[28] = (T(3) / T(2)) * z[46];
z[73] = z[28] + z[68];
z[74] = (T(3) / T(2)) * z[50] + z[73];
z[75] = z[29] * z[54];
z[76] = abb[49] + z[23];
z[48] = z[48] + -z[76];
z[77] = abb[0] * z[48];
z[78] = abb[24] * z[57];
z[67] = 4 * z[30] + z[67] + -z[74] + -z[75] + z[77] + z[78];
z[75] = abb[27] + abb[29] + -abb[30];
z[75] = z[67] * z[75];
z[79] = abb[49] * (T(3) / T(2));
z[80] = z[23] + z[79];
z[81] = 2 * abb[47] + z[12] + z[14] + -z[80];
z[81] = abb[12] * z[81];
z[8] = z[8] + -z[76];
z[82] = abb[9] * z[8];
z[81] = z[17] + z[81] + -z[82];
z[82] = -abb[50] + z[53] + -z[69];
z[82] = z[23] + (T(1) / T(2)) * z[82];
z[82] = abb[7] * z[82];
z[54] = z[54] * z[70];
z[83] = (T(1) / T(2)) * z[27];
z[84] = abb[21] * z[83];
z[85] = abb[19] * z[83];
z[86] = z[84] + z[85];
z[87] = abb[23] * z[83];
z[87] = z[86] + z[87];
z[54] = -z[54] + (T(1) / T(2)) * z[71] + z[81] + z[82] + z[87];
z[54] = 3 * z[54];
z[82] = -abb[31] * z[54];
z[7] = z[7] + z[75] + z[82];
z[7] = abb[32] * z[7];
z[75] = abb[46] + abb[45] * (T(1) / T(2));
z[18] = z[18] + z[75];
z[82] = -z[18] + z[79];
z[82] = z[4] + (T(1) / T(2)) * z[82];
z[82] = abb[3] * z[82];
z[88] = abb[45] + -abb[46];
z[88] = abb[42] + z[39] + (T(1) / T(2)) * z[88];
z[89] = -z[79] + z[88];
z[89] = -z[4] + (T(1) / T(2)) * z[89];
z[90] = abb[8] * z[89];
z[33] = z[33] + -z[44] + z[47] + z[49] + -z[72] + z[82] + z[90];
z[33] = abb[29] * z[33];
z[38] = z[21] + z[38] + -z[75] + -z[79];
z[38] = -z[23] + (T(1) / T(2)) * z[38];
z[38] = abb[3] * z[38];
z[44] = abb[49] * (T(3) / T(4));
z[47] = -5 * abb[45] + -abb[46];
z[3] = -abb[42] + z[3] + z[44] + (T(1) / T(4)) * z[47];
z[3] = abb[8] * z[3];
z[3] = z[3] + z[38] + z[45] + -z[62] + -z[68];
z[3] = abb[30] * z[3];
z[32] = -z[32] + z[52];
z[32] = abb[27] * z[32];
z[38] = 2 * abb[45] + z[23] + -z[69];
z[45] = 2 * abb[7];
z[47] = abb[3] + z[45];
z[38] = z[38] * z[47];
z[47] = abb[8] * z[24];
z[49] = (T(3) / T(2)) * z[71];
z[38] = -z[38] + z[47] + -z[49] + -z[62] + -z[65];
z[38] = abb[31] * z[38];
z[3] = z[3] + z[32] + z[33] + -z[38] + z[63];
z[3] = abb[29] * z[3];
z[32] = 3 * z[17];
z[8] = z[8] * z[10];
z[10] = -z[8] + z[32];
z[33] = z[23] + z[39];
z[52] = 6 * abb[47];
z[63] = abb[49] * (T(7) / T(2)) + (T(-5) / T(2)) * z[11] + z[33] + -z[52];
z[63] = abb[12] * z[63];
z[33] = -6 * abb[48] + abb[49] * (T(-5) / T(2)) + (T(7) / T(2)) * z[11] + z[33];
z[69] = abb[7] + abb[8];
z[33] = z[33] * z[69];
z[13] = -abb[48] + z[13] + z[14];
z[72] = abb[6] * z[13];
z[75] = 3 * z[72];
z[79] = 2 * z[62];
z[82] = -abb[48] + -abb[50] + z[11];
z[90] = z[23] + z[82];
z[91] = 3 * abb[11];
z[92] = z[90] * z[91];
z[22] = abb[3] * z[22];
z[33] = -z[10] + z[22] + z[33] + z[58] + z[63] + -z[75] + z[79] + -z[92];
z[33] = abb[31] * z[33];
z[58] = 2 * z[11];
z[63] = 2 * abb[49] + -z[0];
z[93] = 3 * abb[48];
z[94] = 8 * abb[43] + 4 * abb[44] + -z[21] + z[58] + z[63] + -z[93];
z[94] = abb[1] * z[94];
z[47] = z[47] + z[68];
z[22] = z[22] + z[47];
z[57] = abb[20] * z[57];
z[57] = z[22] + z[56] + -z[57];
z[68] = z[4] + (T(1) / T(2)) * z[82];
z[68] = z[68] * z[91];
z[31] = abb[21] * z[31];
z[19] = z[19] + -z[31] + z[57] + -z[68] + (T(3) / T(2)) * z[72] + z[94];
z[19] = abb[28] * z[19];
z[22] = z[22] + z[60] + z[79];
z[60] = abb[27] + abb[30];
z[79] = -abb[29] + z[60];
z[22] = -z[22] * z[79];
z[82] = abb[29] * z[27];
z[91] = abb[31] * z[27];
z[82] = z[82] + -z[91];
z[94] = abb[30] * z[27];
z[95] = abb[27] * z[27];
z[94] = z[94] + z[95];
z[96] = z[82] + -z[94];
z[97] = abb[20] * (T(3) / T(2));
z[98] = -z[96] * z[97];
z[19] = z[19] + z[22] + z[33] + z[98];
z[19] = abb[28] * z[19];
z[16] = abb[12] * z[16];
z[16] = z[16] + -z[17];
z[13] = z[13] * z[69];
z[17] = -abb[47] + -abb[48] + abb[50];
z[17] = abb[1] * z[17];
z[13] = z[13] + z[16] + -z[17] + z[85];
z[17] = abb[58] * z[13];
z[22] = -abb[15] + -abb[17];
z[22] = z[22] * z[26];
z[33] = -abb[8] + 2 * abb[26] + abb[3] * (T(-3) / T(4)) + abb[14] * (T(-1) / T(4));
z[33] = z[33] * z[34];
z[85] = abb[16] + -abb[18];
z[85] = z[29] * z[85];
z[34] = abb[0] * z[34];
z[34] = -3 * z[34] + z[85];
z[85] = (T(1) / T(4)) * z[27];
z[85] = abb[25] * z[85];
z[22] = z[22] + z[33] + (T(1) / T(4)) * z[34] + z[85];
z[22] = abb[37] * z[22];
z[33] = abb[3] * z[24];
z[33] = z[33] + z[62];
z[34] = z[33] + -z[81] + -z[86];
z[34] = abb[33] * z[34];
z[81] = -abb[27] + abb[29];
z[60] = z[60] * z[81];
z[81] = prod_pow(abb[30], 2);
z[60] = z[60] + -z[81];
z[60] = z[26] * z[60];
z[79] = z[24] * z[79];
z[26] = -abb[28] * z[26];
z[26] = z[26] + z[79];
z[26] = abb[28] * z[26];
z[79] = -abb[33] + -abb[34];
z[24] = z[24] * z[79];
z[24] = z[24] + z[26] + z[60];
z[24] = abb[13] * z[24];
z[26] = abb[30] * abb[31];
z[26] = -abb[58] + z[26] + (T(-1) / T(2)) * z[81];
z[26] = z[26] * z[72];
z[17] = z[17] + z[22] + z[24] + z[26] + z[34];
z[5] = z[5] + -z[93];
z[22] = 7 * abb[46];
z[24] = abb[45] + -z[22];
z[24] = 2 * abb[42] + -z[5] + (T(1) / T(4)) * z[24] + -z[44];
z[24] = abb[8] * z[24];
z[26] = -abb[3] * z[89];
z[24] = z[24] + z[25] + z[26] + -z[31] + (T(-3) / T(2)) * z[36] + z[68] + -z[77];
z[24] = z[24] * z[81];
z[25] = -z[23] + z[93];
z[2] = -z[2] + z[12] + -z[25] + z[39];
z[2] = abb[1] * z[2];
z[12] = -z[11] + z[23] + z[63];
z[26] = 2 * abb[12];
z[12] = z[12] * z[26];
z[26] = 5 * abb[46];
z[31] = 7 * abb[45];
z[34] = -5 * abb[49] + z[21] + -z[26] + z[31];
z[4] = z[4] + (T(1) / T(4)) * z[34];
z[4] = abb[3] * z[4];
z[5] = -z[5] + (T(-7) / T(4)) * z[11] + z[55];
z[5] = abb[8] * z[5];
z[22] = 17 * abb[45] + -7 * abb[49] + -z[22];
z[21] = -z[21] + z[22];
z[21] = 5 * abb[43] + abb[44] * (T(5) / T(2)) + (T(1) / T(4)) * z[21];
z[21] = abb[7] * z[21];
z[2] = z[2] + z[4] + z[5] + -z[9] + -z[12] + z[21];
z[2] = abb[31] * z[2];
z[4] = -z[10] + z[12] + z[33] + -z[47];
z[5] = -abb[27] * z[4];
z[9] = 2 * abb[8];
z[10] = -z[9] + -z[45];
z[12] = -abb[49] + -z[25] + z[58];
z[10] = z[10] * z[12];
z[10] = z[10] + -z[33] + z[56] + z[92];
z[10] = abb[30] * z[10];
z[2] = z[2] + z[5] + z[10];
z[2] = abb[31] * z[2];
z[5] = abb[43] * (T(5) / T(3)) + abb[44] * (T(5) / T(6));
z[10] = abb[50] * (T(1) / T(4)) + z[5];
z[12] = abb[49] * (T(5) / T(6));
z[21] = -z[0] + (T(13) / T(12)) * z[6] + z[10] + z[12];
z[21] = abb[0] * z[21];
z[0] = z[0] + z[93];
z[12] = abb[43] * (T(-10) / T(3)) + abb[44] * (T(-5) / T(3)) + z[0] + (T(-5) / T(6)) * z[11] + -z[12] + -z[14];
z[12] = abb[1] * z[12];
z[34] = -z[46] + -z[50] + z[71];
z[36] = abb[24] * z[27];
z[36] = z[34] + z[36];
z[31] = 23 * abb[46] + z[31];
z[31] = -4 * abb[42] + (T(1) / T(4)) * z[31];
z[10] = abb[49] * (T(1) / T(4)) + z[10] + (T(1) / T(3)) * z[31] + -z[93];
z[10] = abb[8] * z[10];
z[22] = abb[50] + (T(-1) / T(3)) * z[22];
z[22] = -z[5] + (T(1) / T(4)) * z[22];
z[22] = abb[7] * z[22];
z[26] = abb[45] * (T(-7) / T(2)) + abb[42] * (T(13) / T(2)) + z[26];
z[26] = -z[1] + (T(1) / T(3)) * z[26];
z[5] = -z[5] + (T(1) / T(2)) * z[26];
z[5] = abb[3] * z[5];
z[5] = z[5] + z[10] + z[12] + z[21] + z[22] + (T(-1) / T(3)) * z[30] + (T(1) / T(4)) * z[36];
z[10] = prod_pow(m1_set::bc<T>[0], 2);
z[5] = z[5] * z[10];
z[12] = abb[24] * z[83];
z[12] = z[12] + z[30] + (T(1) / T(2)) * z[66];
z[21] = z[35] + z[46];
z[15] = z[15] + z[42];
z[15] = abb[0] * z[15];
z[22] = abb[20] + abb[22];
z[22] = z[22] * z[83];
z[26] = (T(1) / T(2)) * z[43];
z[15] = z[12] + z[15] + -z[16] + (T(-1) / T(2)) * z[21] + -z[22] + -z[26] + -z[87];
z[15] = 3 * z[15];
z[16] = abb[57] * z[15];
z[21] = abb[29] * z[83];
z[21] = -z[21] + z[91] + (T(1) / T(2)) * z[94];
z[21] = abb[29] * z[21];
z[22] = abb[27] * abb[30];
z[22] = z[22] + z[81];
z[22] = z[22] * z[83];
z[30] = abb[36] + -abb[58] + -abb[59];
z[30] = z[27] * z[30];
z[31] = abb[31] * z[83];
z[31] = z[31] + z[95];
z[31] = abb[31] * z[31];
z[21] = -z[21] + z[22] + z[30] + z[31];
z[10] = z[10] * z[83];
z[22] = 3 * abb[34];
z[30] = z[22] * z[27];
z[10] = -z[10] + 3 * z[21] + z[30];
z[21] = abb[20] * z[10];
z[30] = -abb[28] * z[27];
z[30] = z[30] + -z[96];
z[30] = abb[28] * z[30];
z[10] = -z[10] + 3 * z[30];
z[10] = abb[22] * z[10];
z[10] = -z[10] + z[21];
z[4] = -abb[36] * z[4];
z[1] = z[1] + -z[23];
z[11] = 2 * abb[48] + z[1] + (T(-3) / T(2)) * z[11] + z[14];
z[11] = -z[11] * z[69];
z[14] = -abb[11] * z[90];
z[11] = z[11] + z[14] + z[33] + -z[72] + -z[84];
z[11] = z[11] * z[22];
z[14] = abb[35] * z[54];
z[21] = abb[3] * z[29];
z[22] = abb[7] * z[70];
z[21] = -z[21] + -z[22] + z[34];
z[12] = z[12] + (T(1) / T(2)) * z[21] + z[26];
z[12] = 3 * z[12];
z[21] = abb[59] * z[12];
z[2] = abb[60] + z[2] + z[3] + z[4] + z[5] + z[7] + (T(-1) / T(2)) * z[10] + z[11] + z[14] + z[16] + 3 * z[17] + z[19] + z[21] + z[24] + z[41];
z[3] = (T(5) / T(2)) * z[6] + z[39] + -z[52] + z[76];
z[3] = abb[0] * z[3];
z[4] = z[8] + z[32];
z[5] = -abb[46] + -z[53];
z[1] = -abb[42] + z[1] + (T(1) / T(2)) * z[5] + z[39];
z[1] = abb[8] * z[1];
z[5] = (T(3) / T(2)) * z[37];
z[6] = -abb[3] * z[48];
z[7] = 3 * z[43];
z[1] = z[1] + z[3] + z[4] + -z[5] + z[6] + -z[7] + -z[28] + z[51] + z[62] + -z[65] + z[78];
z[1] = abb[27] * z[1];
z[3] = z[51] + -z[62] + -z[77] + z[78];
z[6] = z[75] + -z[92];
z[8] = abb[3] * z[40];
z[10] = -abb[42] + abb[45] + 2 * abb[46] + -z[25];
z[9] = z[9] * z[10];
z[5] = -z[3] + z[5] + z[6] + z[8] + z[9] + z[59] + z[64] + z[73];
z[5] = abb[30] * z[5];
z[8] = z[18] + -z[80];
z[8] = abb[3] * z[8];
z[9] = z[80] + -z[88];
z[9] = abb[8] * z[9];
z[3] = z[3] + z[7] + z[8] + z[9] + z[49] + -z[74];
z[3] = abb[29] * z[3];
z[7] = -abb[32] * z[67];
z[0] = z[0] + -z[20] + -z[61];
z[0] = abb[1] * z[0];
z[0] = 2 * z[0] + -z[4] + -z[6] + -z[57];
z[0] = abb[28] * z[0];
z[0] = z[0] + z[1] + z[3] + z[5] + z[7] + z[38];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[38] * z[15];
z[3] = abb[40] * z[12];
z[4] = z[13] + -z[72];
z[4] = abb[39] * z[4];
z[5] = z[82] + -z[95];
z[5] = m1_set::bc<T>[0] * z[5];
z[6] = abb[39] + abb[40];
z[7] = z[6] * z[27];
z[7] = z[5] + z[7];
z[7] = z[7] * z[97];
z[8] = abb[28] * m1_set::bc<T>[0];
z[6] = z[6] + z[8];
z[6] = z[6] * z[27];
z[5] = z[5] + z[6];
z[5] = abb[22] * z[5];
z[0] = abb[41] + z[0] + z[1] + z[3] + 3 * z[4] + (T(3) / T(2)) * z[5] + z[7];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_521_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-12.647469062186067486841910742974404868797376292554273300051175834"),stof<T>("-32.658337759093888618082200129294428401278554834609311333167076193")}, std::complex<T>{stof<T>("-10.293053808775832717721009899336879750883574606940403753481863196"),stof<T>("27.731619358422268971912884190114824923332565285318170185721753227")}, std::complex<T>{stof<T>("-5.146526904387916358860504949668439875441787303470201876740931598"),stof<T>("13.865809679211134485956442095057412461666282642659085092860876613")}, std::complex<T>{stof<T>("2.5950042358985557438643716361737929768100917418995840993366333551"),stof<T>("11.9707155639956360629457743922804315937884199868170465049396740598")}, std::complex<T>{stof<T>("22.27159359148321089997389334597558122403465204867827240790978262"),stof<T>("14.927533426237727127980115252271100110822023273063294500677031204")}, std::complex<T>{stof<T>("27.020867969250850125997294606705381003227719642862572249811799095"),stof<T>("19.452185123951643192620569008360557671995532244014171037750863862")}, std::complex<T>{stof<T>("-15.639725824704900363543386068086837941856187743739759434226724311"),stof<T>("-26.238591946164474130431078670427989972227183207816396208172040982")}, std::complex<T>{stof<T>("-7.7415311402864721027248765858422328522518790453697859760775649528"),stof<T>("1.8950941152154984230106677027769808678778626558420385879212025536")}, std::complex<T>{stof<T>("-1.4853408623903941775700023282203034067366770014683110969421267106"),stof<T>("-2.5547511592843874835054786769005226002611227079059833853658668355")}, std::complex<T>{stof<T>("-4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("-16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("-4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("-16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_521_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_521_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(-3) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (-3 * m1_set::bc<T>[1] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[45] + abb[47] + -abb[49]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -prod_pow(abb[32], 2);
z[1] = prod_pow(abb[31], 2);
z[0] = z[0] + z[1];
z[1] = -abb[45] + -abb[47] + abb[49];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_521_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_521_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("13.4617138864491369806609356118891406511044252740502085718060189675"),stof<T>("-4.3112481116084540729719503268057823180106757399237961327991304953")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,61> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W17(k,dl), dlog_W21(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k), T{0}};
abb[41] = SpDLog_f_4_521_W_17_Im(t, path, abb);
abb[60] = SpDLog_f_4_521_W_17_Re(t, path, abb);

                    
            return f_4_521_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_521_DLogXconstant_part(base_point<T>, kend);
	value += f_4_521_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_521_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_521_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_521_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_521_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_521_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_521_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
