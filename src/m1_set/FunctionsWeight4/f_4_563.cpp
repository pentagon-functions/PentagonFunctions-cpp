/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_563.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_563_abbreviated (const std::array<T,29>& abb) {
T z[24];
z[0] = abb[22] + abb[24];
z[1] = -abb[25] + (T(1) / T(2)) * z[0];
z[2] = abb[9] * z[1];
z[1] = abb[4] * z[1];
z[3] = z[1] + z[2];
z[4] = abb[24] + -abb[25];
z[5] = -abb[20] + z[4];
z[5] = abb[2] * z[5];
z[6] = (T(1) / T(2)) * z[5];
z[7] = abb[23] + -abb[25];
z[8] = abb[20] + z[7];
z[8] = abb[0] * z[8];
z[9] = abb[22] + -abb[25];
z[10] = abb[20] + z[9];
z[10] = abb[6] * z[10];
z[11] = z[3] + -z[6] + 2 * z[8] + (T(-3) / T(2)) * z[10];
z[12] = prod_pow(abb[10], 2);
z[11] = z[11] * z[12];
z[6] = -z[1] + z[2] + -z[6] + (T(1) / T(2)) * z[10];
z[6] = abb[11] * z[6];
z[13] = z[5] + -z[10];
z[0] = -2 * abb[25] + z[0];
z[14] = abb[9] * z[0];
z[15] = z[13] + z[14];
z[16] = -abb[10] * z[15];
z[6] = z[6] + z[16];
z[6] = abb[11] * z[6];
z[9] = abb[21] + z[9];
z[9] = abb[8] * z[9];
z[16] = z[9] + -z[10];
z[4] = -abb[21] + z[4];
z[4] = abb[3] * z[4];
z[17] = z[4] + -z[5];
z[7] = abb[21] + z[7];
z[7] = abb[1] * z[7];
z[18] = z[7] + -z[8];
z[16] = -4 * z[16] + (T(-5) / T(2)) * z[17] + (T(13) / T(2)) * z[18];
z[16] = prod_pow(m1_set::bc<T>[0], 2) * z[16];
z[17] = (T(1) / T(2)) * z[4];
z[3] = -z[3] + -2 * z[7] + (T(3) / T(2)) * z[9] + z[17];
z[18] = prod_pow(abb[13], 2);
z[3] = z[3] * z[18];
z[1] = -z[1] + 3 * z[2] + (T(-1) / T(2)) * z[9] + z[17];
z[1] = abb[12] * z[1];
z[2] = z[4] + -z[9];
z[17] = abb[4] * z[0];
z[19] = z[14] + -z[17];
z[20] = z[2] + -z[19];
z[21] = abb[13] * z[20];
z[22] = 2 * abb[9];
z[22] = z[0] * z[22];
z[22] = -z[17] + z[22];
z[23] = abb[10] + -abb[11];
z[23] = z[22] * z[23];
z[1] = z[1] + z[21] + z[23];
z[1] = abb[12] * z[1];
z[21] = abb[27] * z[20];
z[13] = z[13] + -z[19];
z[19] = -abb[26] * z[13];
z[23] = -abb[14] + abb[15] + abb[16];
z[22] = z[22] * z[23];
z[12] = -z[12] + z[18];
z[0] = abb[5] * z[0];
z[12] = z[0] * z[12];
z[1] = abb[28] + z[1] + z[3] + z[6] + z[11] + z[12] + (T(1) / T(3)) * z[16] + z[19] + z[21] + z[22];
z[3] = z[14] + z[17];
z[5] = -z[3] + z[5] + -4 * z[8] + 3 * z[10];
z[5] = abb[10] * z[5];
z[3] = z[3] + -z[4] + 4 * z[7] + -3 * z[9];
z[3] = abb[13] * z[3];
z[4] = abb[11] * z[15];
z[2] = -z[2] + -z[14];
z[2] = abb[12] * z[2];
z[6] = abb[10] + -abb[13];
z[0] = z[0] * z[6];
z[0] = 2 * z[0] + z[2] + z[3] + z[4] + z[5];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[17] * z[13];
z[3] = abb[18] * z[20];
z[0] = abb[19] + z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_563_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("17.092293306056192057057633858270108648520582965299943688790563879"),stof<T>("23.27933632906989773930226187286161777982511987753463968767487195")}, std::complex<T>{stof<T>("-0.783380739531793706202640249219795159768385958230961139356235713"),stof<T>("-18.689655014825286707415423873817743885363767154876205942963075529")}, std::complex<T>{stof<T>("-6.2999902652605247832148215429566905394324150656308729167361213737"),stof<T>("-2.993802662848674543735162128493895808268022694733665880659090494")}, std::complex<T>{stof<T>("22.60890283178492313406981515200700402818461207269985546617044954"),stof<T>("7.583483977093285575622000127537769702729375417392099625370886916")}, std::complex<T>{stof<T>("-16.308912566524398350854993609050313488752197007068982549434328166"),stof<T>("-4.589681314244611031886837999043873894461352722658433744711796422")}};
	
	std::vector<C> intdlogs = {rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[30].real()/kbase.W[30].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_563_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_563_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(32)) * (v[2] + v[3]) * (4 * (12 + 2 * v[1] + v[2] + -v[3] + -2 * v[4] + m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 4 * v[5]) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[1] = ((-2 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(-1) / T(4)) * (v[2] + v[3])) / tend;


		return (abb[22] + abb[24] + -2 * abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[11] + abb[12];
z[1] = -abb[10] + -abb[12];
z[0] = z[0] * z[1];
z[0] = abb[14] + -abb[15] + -abb[16] + z[0];
z[1] = abb[22] + abb[24] + -2 * abb[25];
return abb[7] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_563_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(8)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(2)) * (v[2] + v[3])) / tend;


		return (abb[22] + abb[24] + -2 * abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[22] + -abb[24] + 2 * abb[25];
z[1] = abb[11] + -abb[12];
return abb[7] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_563_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), f_1_1(k), f_1_4(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), T{0}, rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[30].real()/k.W[30].real()), f_2_4_re(k), f_2_11_re(k), T{0}};
abb[19] = SpDLog_f_4_563_W_19_Im(t, path, abb);
abb[28] = SpDLog_f_4_563_W_19_Re(t, path, abb);

                    
            return f_4_563_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_563_DLogXconstant_part(base_point<T>, kend);
	value += f_4_563_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_563_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_563_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_563_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_563_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_563_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_563_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
