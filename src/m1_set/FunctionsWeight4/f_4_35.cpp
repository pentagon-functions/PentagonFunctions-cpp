/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_35.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_35_abbreviated (const std::array<T,27>& abb) {
T z[44];
z[0] = -abb[11] + abb[12];
z[1] = abb[13] + -abb[14];
z[2] = z[0] + z[1];
z[3] = 4 * abb[12];
z[3] = z[2] * z[3];
z[4] = 4 * abb[15];
z[5] = z[3] + z[4];
z[6] = prod_pow(abb[14], 2);
z[7] = (T(1) / T(2)) * z[6];
z[8] = 2 * abb[16];
z[9] = abb[13] * abb[14];
z[10] = abb[11] + (T(-5) / T(2)) * z[1];
z[10] = abb[11] * z[10];
z[10] = z[5] + z[7] + z[8] + (T(-3) / T(2)) * z[9] + z[10];
z[10] = abb[2] * z[10];
z[11] = 2 * abb[14];
z[12] = -abb[13] + z[11];
z[12] = abb[13] * z[12];
z[13] = 2 * z[1];
z[14] = -abb[11] + z[13];
z[14] = abb[11] * z[14];
z[15] = 3 * abb[16];
z[4] = z[4] + z[6] + -z[12] + -z[14] + z[15];
z[12] = 3 * abb[12];
z[14] = -abb[11] + z[1];
z[16] = 4 * z[14];
z[17] = z[12] + z[16];
z[17] = abb[12] * z[17];
z[17] = z[4] + z[17];
z[18] = 2 * abb[1];
z[17] = z[17] * z[18];
z[19] = 2 * z[14];
z[12] = -z[12] + -z[19];
z[12] = abb[12] * z[12];
z[20] = 2 * abb[15];
z[21] = abb[11] + z[13];
z[21] = abb[11] * z[21];
z[12] = z[12] + -z[20] + z[21];
z[12] = abb[4] * z[12];
z[21] = abb[12] + z[19];
z[21] = abb[12] * z[21];
z[8] = z[8] + z[20] + z[21];
z[21] = abb[11] + -abb[13];
z[22] = z[1] * z[21];
z[22] = -z[6] + -z[8] + z[22];
z[22] = abb[3] * z[22];
z[23] = abb[11] * z[14];
z[23] = -z[6] + z[23];
z[8] = -z[8] + z[9] + z[23];
z[24] = abb[6] * z[8];
z[25] = abb[16] + z[6];
z[26] = prod_pow(abb[12], 2);
z[27] = z[25] + -z[26];
z[28] = abb[5] * z[27];
z[29] = abb[14] * (T(1) / T(2));
z[30] = -abb[13] + z[29];
z[30] = abb[13] * z[30];
z[30] = z[7] + z[30];
z[31] = (T(1) / T(2)) * z[1];
z[32] = -abb[11] * z[31];
z[32] = z[30] + z[32];
z[32] = abb[0] * z[32];
z[33] = abb[8] * (T(1) / T(2));
z[34] = -2 * abb[7] + -abb[9] + -z[33];
z[34] = abb[17] * z[34];
z[10] = z[10] + z[12] + z[17] + z[22] + z[24] + 2 * z[28] + z[32] + z[34];
z[12] = 3 * abb[24];
z[10] = z[10] * z[12];
z[3] = z[3] + z[4];
z[17] = z[3] * z[18];
z[5] = 6 * abb[16] + z[5];
z[22] = 4 * abb[13];
z[24] = abb[14] * (T(-7) / T(2)) + z[22];
z[24] = abb[13] * z[24];
z[32] = abb[11] + -z[31];
z[32] = abb[11] * z[32];
z[24] = z[5] + (T(5) / T(2)) * z[6] + z[24] + z[32];
z[24] = abb[2] * z[24];
z[32] = 4 * abb[11];
z[34] = z[31] + z[32];
z[34] = abb[11] * z[34];
z[30] = -z[30] + z[34];
z[30] = abb[0] * z[30];
z[34] = 3 * abb[11];
z[35] = -z[13] + z[34];
z[35] = abb[11] * z[35];
z[36] = -abb[12] + z[19];
z[36] = abb[12] * z[36];
z[35] = z[20] + z[35] + z[36];
z[36] = abb[4] * z[35];
z[37] = 3 * abb[6];
z[8] = z[8] * z[37];
z[37] = 3 * abb[5];
z[27] = z[27] * z[37];
z[27] = z[8] + z[27];
z[38] = 2 * abb[12];
z[39] = z[2] * z[38];
z[40] = abb[15] + z[6];
z[15] = z[15] + z[39] + 2 * z[40];
z[39] = 2 * abb[11];
z[40] = -z[1] + z[39];
z[40] = abb[11] * z[40];
z[41] = abb[13] + abb[14];
z[41] = abb[13] * z[41];
z[40] = z[15] + z[40] + -z[41];
z[40] = abb[3] * z[40];
z[42] = 3 * abb[17];
z[33] = -abb[9] + z[33];
z[33] = z[33] * z[42];
z[17] = z[17] + z[24] + z[27] + -z[30] + z[33] + z[36] + z[40];
z[17] = abb[21] * z[17];
z[24] = 4 * abb[1] + 2 * abb[2];
z[3] = z[3] * z[24];
z[24] = 5 * abb[12];
z[19] = z[19] + z[24];
z[19] = abb[12] * z[19];
z[13] = z[13] + z[34];
z[13] = abb[11] * z[13];
z[13] = -z[13] + z[19] + z[20];
z[13] = abb[4] * z[13];
z[19] = z[1] + z[39];
z[19] = abb[11] * z[19];
z[20] = 2 * abb[13];
z[33] = -abb[14] + z[20];
z[33] = abb[13] * z[33];
z[34] = -z[6] + z[33];
z[19] = z[19] + z[34];
z[19] = abb[0] * z[19];
z[40] = abb[11] + z[1];
z[43] = abb[11] * z[40];
z[15] = z[15] + z[33] + -z[43];
z[15] = abb[3] * z[15];
z[33] = abb[7] + abb[9];
z[33] = z[33] * z[42];
z[3] = z[3] + -z[13] + -z[15] + -z[19] + z[27] + -z[33];
z[13] = 4 * abb[19] + 2 * abb[20];
z[3] = -z[3] * z[13];
z[15] = 7 * abb[12] + z[16];
z[15] = abb[12] * z[15];
z[4] = z[4] + z[15];
z[4] = z[4] * z[18];
z[15] = -z[6] + z[9];
z[16] = -abb[11] * z[1];
z[5] = z[5] + -3 * z[15] + z[16];
z[5] = abb[2] * z[5];
z[15] = abb[3] * z[35];
z[16] = -8 * abb[11] + -z[1];
z[16] = abb[11] * z[16];
z[16] = z[16] + -z[34];
z[16] = abb[0] * z[16];
z[4] = z[4] + z[5] + z[8] + z[15] + z[16] + z[36];
z[4] = abb[22] * z[4];
z[5] = z[18] * z[26];
z[8] = -z[25] + -z[26];
z[8] = abb[3] * z[8];
z[5] = z[5] + z[8] + -z[28];
z[8] = 3 * abb[23];
z[5] = z[5] * z[8];
z[15] = z[22] + -z[29];
z[15] = abb[13] * z[15];
z[16] = abb[11] + z[31];
z[16] = abb[11] * z[16];
z[7] = -z[7] + z[15] + z[16];
z[7] = abb[2] * z[7];
z[15] = z[23] + -z[41];
z[15] = abb[3] * z[15];
z[19] = abb[8] * abb[17];
z[7] = z[7] + z[15] + (T(-3) / T(2)) * z[19] + z[30];
z[7] = abb[18] * z[7];
z[15] = -abb[2] + abb[3];
z[19] = -abb[0] + z[15];
z[19] = abb[18] * z[19];
z[23] = abb[1] + abb[3];
z[23] = abb[0] + -abb[2] + (T(-1) / T(2)) * z[23];
z[23] = abb[21] * z[23];
z[19] = z[19] + z[23];
z[15] = abb[0] + z[15] + -z[18];
z[23] = abb[19] * (T(-2) / T(3)) + abb[20] * (T(-1) / T(3));
z[15] = z[15] * z[23];
z[23] = abb[0] + -abb[1];
z[23] = abb[22] * z[23];
z[25] = -abb[1] + abb[3];
z[25] = abb[23] * z[25];
z[15] = z[15] + (T(1) / T(3)) * z[19] + (T(2) / T(3)) * z[23] + (T(1) / T(2)) * z[25];
z[15] = prod_pow(m1_set::bc<T>[0], 2) * z[15];
z[6] = z[6] + z[9];
z[6] = (T(1) / T(2)) * z[6] + -z[16];
z[6] = abb[8] * z[6];
z[9] = prod_pow(abb[13], 2);
z[16] = prod_pow(abb[11], 2);
z[9] = z[9] + -z[16];
z[9] = abb[7] * z[9];
z[6] = z[6] + z[9];
z[9] = abb[10] * abb[17];
z[6] = 3 * z[6] + -6 * z[9];
z[9] = abb[25] + abb[26];
z[6] = z[6] * z[9];
z[16] = abb[0] + abb[2];
z[16] = abb[3] + (T(3) / T(2)) * z[16];
z[16] = z[9] * z[16];
z[19] = abb[8] + -abb[9];
z[19] = abb[22] * z[19];
z[16] = z[16] + z[19];
z[16] = z[16] * z[42];
z[3] = z[3] + z[4] + z[5] + z[6] + z[7] + z[10] + 13 * z[15] + z[16] + z[17];
z[2] = -z[2] * z[18];
z[4] = abb[12] + -abb[14];
z[5] = abb[5] * z[4];
z[6] = abb[4] * z[0];
z[7] = 2 * z[6];
z[10] = abb[0] * z[1];
z[15] = -abb[11] + -abb[14] + z[38];
z[16] = -abb[2] * z[15];
z[17] = -abb[3] * z[21];
z[2] = z[2] + 2 * z[5] + z[7] + z[10] + z[16] + z[17];
z[2] = z[2] * z[12];
z[10] = z[14] + z[38];
z[12] = z[10] * z[18];
z[7] = -z[7] + z[12];
z[1] = z[1] + z[32];
z[1] = abb[0] * z[1];
z[4] = z[4] * z[37];
z[12] = -z[15] + -z[22];
z[12] = abb[2] * z[12];
z[15] = -abb[12] + z[14];
z[15] = abb[3] * z[15];
z[12] = z[1] + z[4] + -z[7] + z[12] + z[15];
z[12] = abb[21] * z[12];
z[15] = abb[12] + abb[14];
z[16] = z[15] + z[20] + -z[39];
z[16] = abb[3] * z[16];
z[10] = abb[2] * z[10];
z[17] = abb[0] * z[40];
z[7] = z[7] + z[10] + -z[17];
z[4] = -z[4] + 2 * z[7] + -z[16];
z[4] = z[4] * z[13];
z[7] = -z[14] + -z[24];
z[7] = abb[1] * z[7];
z[10] = -abb[2] * z[0];
z[6] = z[1] + z[6] + z[7] + z[10];
z[0] = 2 * z[0];
z[0] = abb[3] * z[0];
z[0] = z[0] + 2 * z[6];
z[0] = abb[22] * z[0];
z[6] = -abb[12] * z[18];
z[7] = abb[3] * z[15];
z[5] = -z[5] + z[6] + z[7];
z[5] = z[5] * z[8];
z[6] = abb[11] + -abb[14];
z[7] = -z[6] + -z[22];
z[7] = abb[2] * z[7];
z[8] = abb[11] + abb[13] + z[11];
z[8] = abb[3] * z[8];
z[1] = -z[1] + z[7] + z[8];
z[1] = abb[18] * z[1];
z[7] = abb[7] * z[21];
z[6] = abb[8] * z[6];
z[6] = z[6] + z[7];
z[6] = z[6] * z[9];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + 3 * z[6] + z[12];
z[0] = 2 * m1_set::bc<T>[0] * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_35_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("48.672549073656967609661670099664614208854899337432369592231118698"),stof<T>("77.906048704265954312490577205764097460876408306414968372436367324")}, std::complex<T>{stof<T>("29.94181978950185540037529339183821481234278710804930176509927687"),stof<T>("-173.75579821418008142477894267941776479661075675525794020223169872")}, std::complex<T>{stof<T>("14.970909894750927700187646695919107406171393554024650882549638434"),stof<T>("-86.877899107090040712389471339708882398305378377628970101115849361")}, std::complex<T>{stof<T>("-55.095440767013406380168148966890016246910109378535863706200169069"),stof<T>("13.809441396774375041401348511279973248765329595132947471029100976")}, std::complex<T>{stof<T>("-70.549462865494480637920910315799349601445423017719239995246394766"),stof<T>("-2.567626731548592952169937326622434731650612709190281713909812688")}, std::complex<T>{stof<T>("33.218526975175893351908908750755280854319585698248993303184893002"),stof<T>("61.52898057594298631891929136786168948046046600209173918749745366")}, std::complex<T>{stof<T>("6.9060038970865853280715935202156279864191301262622195204656376332"),stof<T>("11.5394771343726793520688314605672196690795827804042834425892947249")}, std::complex<T>{stof<T>("4.24352664904567504294104949049745697907548322004100448538319176"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}, std::complex<T>{stof<T>("4.24352664904567504294104949049745697907548322004100448538319176"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_35_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_35_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-80.13415909386994824877785910743307380374242824795113246426873199"),stof<T>("31.0588006774705164174018412026844612454990210740564322521494792")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_35_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_35_DLogXconstant_part(base_point<T>, kend);
	value += f_4_35_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_35_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_35_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_35_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_35_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_35_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_35_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
