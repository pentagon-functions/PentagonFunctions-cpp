/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_584.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_584_abbreviated (const std::array<T,29>& abb) {
T z[20];
z[0] = abb[4] + abb[5];
z[1] = abb[22] + abb[23];
z[2] = -abb[25] + (T(1) / T(2)) * z[1];
z[3] = z[0] * z[2];
z[1] = -2 * abb[25] + z[1];
z[4] = 2 * abb[9];
z[5] = z[1] * z[4];
z[6] = abb[24] + -abb[25];
z[7] = abb[21] + z[6];
z[7] = abb[3] * z[7];
z[8] = abb[22] + -abb[25];
z[9] = abb[21] + z[8];
z[9] = abb[8] * z[9];
z[5] = -z[3] + z[5] + 2 * z[7] + -z[9];
z[5] = abb[11] * z[5];
z[0] = -z[0] + z[4];
z[0] = z[0] * z[1];
z[4] = abb[10] + -abb[12] + -abb[13];
z[4] = z[0] * z[4];
z[4] = z[4] + z[5];
z[4] = abb[11] * z[4];
z[5] = abb[20] + z[6];
z[5] = abb[2] * z[5];
z[6] = abb[20] + z[8];
z[6] = abb[6] * z[6];
z[3] = -z[3] + -2 * z[5] + z[6];
z[8] = prod_pow(abb[13], 2);
z[3] = z[3] * z[8];
z[10] = z[6] + -z[9];
z[5] = z[5] + -z[7] + 5 * z[10];
z[7] = prod_pow(m1_set::bc<T>[0], 2);
z[5] = z[5] * z[7];
z[10] = prod_pow(abb[10], 2);
z[8] = -z[7] + z[8] + z[10];
z[11] = abb[23] + -abb[25];
z[12] = -abb[20] + z[11];
z[12] = abb[0] * z[12];
z[8] = z[8] * z[12];
z[13] = prod_pow(abb[12], 2);
z[14] = -prod_pow(abb[11], 2);
z[7] = abb[27] + z[7] + -z[13] + z[14];
z[11] = -abb[21] + z[11];
z[11] = abb[1] * z[11];
z[7] = z[7] * z[11];
z[14] = -abb[4] + abb[9];
z[14] = z[1] * z[14];
z[15] = -z[6] + z[12] + -z[14];
z[16] = -abb[26] * z[15];
z[17] = -abb[14] + abb[15] + abb[16];
z[0] = z[0] * z[17];
z[14] = z[9] + z[14];
z[17] = -abb[27] * z[14];
z[18] = -abb[4] + abb[5];
z[2] = z[2] * z[18];
z[19] = z[2] + z[9];
z[13] = z[13] * z[19];
z[2] = -z[2] + -z[6];
z[2] = z[2] * z[10];
z[0] = abb[28] + z[0] + z[2] + z[3] + z[4] + (T(1) / T(6)) * z[5] + z[7] + z[8] + z[13] + z[16] + z[17];
z[1] = z[1] * z[18];
z[2] = z[1] + 2 * z[6] + -2 * z[12];
z[2] = abb[10] * z[2];
z[1] = -z[1] + -2 * z[9];
z[1] = abb[12] * z[1];
z[1] = z[1] + z[2];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[17] * z[15];
z[3] = -abb[18] * z[14];
z[4] = abb[12] * m1_set::bc<T>[0];
z[4] = abb[18] + 2 * z[4];
z[4] = z[4] * z[11];
z[1] = abb[19] + z[1] + z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_584_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.5262705065963816215023436345020548524166054253027933874835052858"),stof<T>("-13.607267345876588693832657501666255175430300428594528940729547323")}, std::complex<T>{stof<T>("-12.2869686016167497954414642527812294913660399349426835308336473727"),stof<T>("3.9836470990651604947031184630596805143566562142279942800994292899")}, std::complex<T>{stof<T>("-3.8062831184628480416113763517148240093734054792912861683798862935"),stof<T>("-3.1395585388275471349226827024863887704084667996951443147131634125")}, std::complex<T>{stof<T>("9.006955989750283375332431535568460334409239880954190749937266365"),stof<T>("6.4840617079838810642068563361201858906651774146713903459169546207")}, std::complex<T>{stof<T>("-5.2006728712874353337210551838536363250358344016629045815573800715"),stof<T>("-3.3445031691563339292841736336337971202567106149762460312037912082")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[31].real()/kbase.W[31].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_584_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_584_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[1] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[22] + abb[23] + -2 * abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[11], 2);
z[1] = prod_pow(abb[13], 2);
z[0] = z[0] + -z[1];
z[1] = -abb[22] + -abb[23] + 2 * abb[25];
return abb[7] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_584_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_584_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-6.451064030643286567093932182826758138018925391672366203681713663"),stof<T>("10.5686588317840259289285192894766564740172472568331478463019355891")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), f_1_1(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[31].real()/k.W[31].real()), f_2_2_re(k), f_2_10_re(k), T{0}};
abb[19] = SpDLog_f_4_584_W_20_Im(t, path, abb);
abb[28] = SpDLog_f_4_584_W_20_Re(t, path, abb);

                    
            return f_4_584_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_584_DLogXconstant_part(base_point<T>, kend);
	value += f_4_584_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_584_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_584_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_584_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_584_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_584_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_584_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
