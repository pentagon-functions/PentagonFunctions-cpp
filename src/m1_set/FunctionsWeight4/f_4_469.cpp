/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_469.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_469_abbreviated (const std::array<T,28>& abb) {
T z[40];
z[0] = abb[22] * (T(1) / T(2));
z[1] = abb[19] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[20] + abb[18] * (T(1) / T(2));
z[4] = 2 * abb[21];
z[5] = z[2] + z[3] + -z[4];
z[5] = abb[2] * z[5];
z[6] = abb[19] * (T(5) / T(2));
z[7] = abb[22] * (T(5) / T(2)) + -z[3] + z[6];
z[7] = -z[4] + (T(1) / T(2)) * z[7];
z[7] = abb[3] * z[7];
z[8] = abb[18] + abb[20];
z[9] = -abb[22] + 3 * z[8];
z[10] = -abb[19] + z[4];
z[11] = (T(1) / T(2)) * z[9] + -z[10];
z[12] = abb[6] * z[11];
z[13] = -abb[22] + z[8];
z[14] = abb[1] * z[13];
z[15] = z[12] + -2 * z[14];
z[16] = z[2] + -z[3];
z[17] = abb[0] * (T(1) / T(2));
z[17] = z[16] * z[17];
z[18] = abb[23] + abb[24] + -abb[25];
z[19] = abb[7] * z[18];
z[17] = z[17] + (T(-1) / T(4)) * z[19];
z[20] = 4 * abb[21];
z[21] = 2 * abb[19] + -z[20];
z[22] = abb[22] + z[8] + z[21];
z[23] = abb[4] * z[22];
z[5] = -z[5] + z[7] + z[15] + z[17] + -z[23];
z[7] = abb[12] + -abb[13];
z[5] = z[5] * z[7];
z[7] = abb[2] * z[22];
z[9] = z[9] + z[21];
z[21] = abb[6] * z[9];
z[24] = 2 * z[13];
z[25] = abb[3] * z[24];
z[7] = z[7] + 4 * z[14] + -z[21] + z[23] + z[25];
z[25] = abb[14] * z[7];
z[12] = z[12] + -z[14];
z[26] = -abb[18] + abb[22];
z[27] = -abb[19] + z[26];
z[28] = (T(1) / T(2)) * z[27];
z[29] = abb[3] * z[28];
z[30] = (T(1) / T(2)) * z[19];
z[29] = z[29] + z[30];
z[31] = abb[4] * z[11];
z[1] = abb[18] + abb[20] * (T(3) / T(2)) + z[1] + -z[4];
z[1] = abb[2] * z[1];
z[32] = (T(1) / T(2)) * z[18];
z[33] = abb[8] * z[32];
z[1] = z[1] + -z[12] + -z[29] + z[31] + -z[33];
z[1] = abb[11] * z[1];
z[1] = z[1] + z[5] + -z[25];
z[1] = abb[11] * z[1];
z[5] = 3 * abb[20];
z[6] = abb[22] * (T(-3) / T(2)) + abb[18] * (T(7) / T(2)) + z[5] + z[6];
z[6] = -z[4] + (T(1) / T(2)) * z[6];
z[6] = abb[3] * z[6];
z[31] = abb[18] + abb[22];
z[34] = 3 * abb[19] + z[31];
z[35] = -z[4] + (T(1) / T(2)) * z[34];
z[35] = abb[2] * z[35];
z[6] = z[6] + -z[15] + z[17] + z[35];
z[6] = abb[12] * z[6];
z[15] = abb[3] * z[22];
z[22] = abb[5] * z[27];
z[27] = z[14] + z[22];
z[36] = abb[19] + -abb[20];
z[36] = abb[2] * z[36];
z[23] = z[15] + -z[23] + -z[27] + z[36];
z[23] = abb[14] * z[23];
z[16] = abb[0] * z[16];
z[29] = -z[16] + -z[27] + z[29];
z[29] = abb[13] * z[29];
z[23] = -z[6] + z[23] + (T(1) / T(2)) * z[29];
z[23] = abb[13] * z[23];
z[29] = abb[19] + abb[22];
z[37] = abb[21] * (T(4) / T(3));
z[29] = z[3] + (T(1) / T(6)) * z[29] + -z[37];
z[29] = abb[3] * z[29];
z[33] = z[14] + z[33];
z[38] = abb[19] * (T(-2) / T(3)) + abb[22] * (T(1) / T(3)) + -z[8] + z[37];
z[38] = abb[6] * z[38];
z[39] = -abb[18] + 5 * abb[22];
z[39] = abb[19] + (T(1) / T(3)) * z[39];
z[37] = -z[37] + (T(1) / T(2)) * z[39];
z[37] = abb[2] * z[37];
z[29] = (T(-4) / T(3)) * z[22] + z[29] + z[30] + z[33] + z[37] + z[38];
z[29] = prod_pow(m1_set::bc<T>[0], 2) * z[29];
z[30] = abb[10] + abb[0] * (T(-3) / T(4)) + abb[3] * (T(-3) / T(4));
z[30] = z[18] * z[30];
z[2] = z[2] + 3 * z[3];
z[2] = (T(1) / T(2)) * z[2] + -z[4];
z[2] = abb[7] * z[2];
z[3] = abb[9] * z[11];
z[11] = -abb[22] + z[10];
z[11] = abb[8] * z[11];
z[32] = abb[2] * z[32];
z[2] = -z[2] + z[3] + z[11] + z[30] + -z[32];
z[3] = -abb[27] * z[2];
z[11] = abb[2] * z[28];
z[11] = z[11] + z[16] + z[33];
z[11] = abb[12] * z[11];
z[11] = z[11] + z[25];
z[11] = abb[12] * z[11];
z[7] = abb[15] * z[7];
z[16] = -z[20] + z[34];
z[16] = abb[2] * z[16];
z[9] = abb[3] * z[9];
z[9] = -z[9] + -3 * z[14] + -z[16] + z[21] + z[22];
z[16] = -abb[26] * z[9];
z[8] = 3 * abb[22] + -z[8];
z[8] = (T(1) / T(2)) * z[8] + -z[10];
z[10] = -abb[3] + abb[4];
z[8] = z[8] * z[10];
z[8] = z[8] + (T(3) / T(2)) * z[27] + (T(1) / T(2)) * z[36];
z[8] = prod_pow(abb[14], 2) * z[8];
z[1] = z[1] + z[3] + z[7] + z[8] + z[11] + z[16] + z[23] + z[29];
z[3] = 2 * abb[20];
z[7] = abb[19] + z[3] + -z[20] + z[31];
z[7] = abb[2] * z[7];
z[8] = abb[4] * z[24];
z[7] = z[7] + z[8] + z[14] + z[15] + -z[21] + -3 * z[22];
z[7] = abb[14] * z[7];
z[0] = abb[18] * (T(-5) / T(2)) + abb[19] * (T(-3) / T(2)) + z[0] + -z[5];
z[0] = (T(1) / T(2)) * z[0] + z[4];
z[0] = abb[3] * z[0];
z[0] = z[0] + z[12] + z[17] + z[22] + -z[35];
z[0] = abb[13] * z[0];
z[3] = -abb[19] + z[3] + -z[26];
z[4] = -abb[2] + abb[3];
z[3] = z[3] * z[4];
z[4] = -abb[4] * z[13];
z[4] = z[4] + z[14];
z[5] = abb[8] * z[18];
z[3] = z[3] + 2 * z[4] + z[5] + z[19];
z[3] = abb[11] * z[3];
z[0] = z[0] + z[3] + -z[6] + z[7];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[17] * z[2];
z[3] = -abb[16] * z[9];
z[0] = z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_469_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("8.697807850087875830616034184597032992753008955383196010166903847"),stof<T>("-2.4152758465310556736815689365604600381816479286894254369630996991")}, std::complex<T>{stof<T>("10.145751306654613917887244442114532028245249906674633389445601986"),stof<T>("-0.6974656525216500714800303751339387619116802941767826701511885065")}, std::complex<T>{stof<T>("-1.447943456566738087271210257517499035492240951291437379278698139"),stof<T>("-1.7178101940094056022015385614265212762699676345126427668119111926")}, std::complex<T>{stof<T>("-8.697807850087875830616034184597032992753008955383196010166903847"),stof<T>("2.4152758465310556736815689365604600381816479286894254369630996991")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("-4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("6.7898493908076716514634340510446747195333140327511877322391776059")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_469_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_469_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dlog_W3(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_469_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_469_DLogXconstant_part(base_point<T>, kend);
	value += f_4_469_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_469_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_469_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_469_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_469_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_469_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_469_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
