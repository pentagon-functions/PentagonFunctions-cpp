/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_95.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_95_abbreviated (const std::array<T,63>& abb) {
T z[120];
z[0] = abb[22] * abb[34];
z[1] = abb[24] * abb[33];
z[2] = abb[23] + abb[25];
z[3] = abb[22] + z[2];
z[4] = abb[32] * z[3];
z[5] = abb[24] + z[2];
z[6] = abb[31] * z[5];
z[0] = z[0] + z[1] + z[4] + z[6];
z[1] = abb[24] + z[3];
z[4] = abb[30] * z[1];
z[0] = 2 * z[0] + -z[4];
z[0] = abb[30] * z[0];
z[7] = abb[23] + abb[24];
z[8] = abb[31] * z[7];
z[9] = -abb[26] + z[3];
z[10] = abb[32] * z[9];
z[4] = -z[4] + z[8] + z[10];
z[11] = abb[24] + abb[26];
z[12] = abb[22] + abb[25];
z[13] = z[11] + -z[12];
z[14] = abb[33] + -abb[34];
z[13] = z[13] * z[14];
z[15] = -abb[23] + abb[26];
z[15] = abb[29] * z[15];
z[4] = 2 * z[4] + z[13] + z[15];
z[4] = abb[29] * z[4];
z[13] = 2 * abb[23];
z[15] = -abb[24] + abb[26] + -z[12] + -z[13];
z[15] = abb[34] * z[15];
z[8] = z[8] + -z[10];
z[8] = 2 * z[8];
z[9] = abb[24] + z[9];
z[16] = abb[33] * z[9];
z[15] = z[8] + z[15] + z[16];
z[15] = abb[33] * z[15];
z[17] = -abb[26] + 2 * z[3];
z[18] = prod_pow(abb[32], 2);
z[17] = z[17] * z[18];
z[13] = 2 * abb[24] + abb[25] + z[13];
z[19] = prod_pow(abb[31], 2);
z[13] = z[13] * z[19];
z[20] = prod_pow(m1_set::bc<T>[0], 2);
z[21] = 2 * abb[61] + (T(1) / T(3)) * z[20];
z[21] = z[9] * z[21];
z[7] = abb[22] + z[7];
z[22] = abb[34] * z[7];
z[8] = -z[8] + z[22];
z[8] = abb[34] * z[8];
z[22] = 2 * abb[35];
z[12] = z[12] * z[22];
z[23] = 2 * abb[37];
z[3] = z[3] * z[23];
z[24] = 2 * abb[36];
z[7] = z[7] * z[24];
z[25] = 2 * abb[60];
z[26] = z[5] * z[25];
z[27] = 2 * z[11];
z[28] = abb[59] * z[27];
z[1] = 2 * z[1];
z[1] = abb[38] * z[1];
z[0] = -z[0] + z[1] + z[3] + -z[4] + z[7] + z[8] + -z[12] + z[13] + z[15] + z[17] + -z[21] + -z[26] + z[28];
z[1] = abb[58] * z[0];
z[3] = -abb[27] * abb[39];
z[0] = z[0] + z[3];
z[0] = abb[57] * z[0];
z[3] = abb[49] * (T(1) / T(3));
z[4] = abb[50] * (T(2) / T(3)) + -z[3];
z[7] = -abb[52] + abb[46] * (T(4) / T(3));
z[8] = abb[48] * (T(4) / T(3));
z[12] = abb[55] * (T(1) / T(2));
z[13] = abb[47] + z[12];
z[15] = abb[54] * (T(-8) / T(3)) + abb[51] * (T(4) / T(3)) + abb[53] * (T(29) / T(6)) + -z[4] + -z[7] + -z[8] + (T(-1) / T(3)) * z[13];
z[15] = abb[3] * z[15];
z[17] = 2 * abb[44];
z[21] = abb[51] + z[17];
z[26] = 2 * abb[47];
z[28] = abb[55] + z[26];
z[29] = -abb[45] + z[28];
z[30] = 2 * abb[54];
z[31] = z[21] + -z[29] + z[30];
z[31] = abb[10] * z[31];
z[32] = 2 * abb[49];
z[33] = z[30] + -z[32];
z[34] = -abb[47] + abb[48];
z[35] = abb[45] + abb[50];
z[36] = z[34] + z[35];
z[37] = z[33] + z[36];
z[37] = abb[14] * z[37];
z[38] = 2 * abb[46];
z[39] = -z[35] + z[38];
z[40] = 5 * abb[54];
z[41] = 2 * abb[52];
z[42] = -abb[47] + z[40] + -z[41];
z[3] = abb[48] * (T(-7) / T(3)) + -z[3] + z[39] + (T(1) / T(3)) * z[42];
z[3] = abb[6] * z[3];
z[42] = abb[45] * (T(2) / T(3));
z[4] = -abb[48] + abb[47] * (T(1) / T(3)) + -z[4] + z[7] + -z[42];
z[4] = abb[4] * z[4];
z[7] = abb[50] + -abb[53];
z[43] = -abb[46] + abb[49];
z[44] = z[7] + -z[43];
z[45] = z[12] + -z[34] + z[44];
z[46] = abb[51] * (T(1) / T(2));
z[45] = abb[45] * (T(-1) / T(6)) + (T(1) / T(3)) * z[45] + -z[46];
z[45] = abb[17] * z[45];
z[47] = abb[55] * (T(7) / T(2));
z[48] = abb[46] + abb[47];
z[49] = abb[53] + 8 * abb[54] + z[47] + -z[48];
z[8] = -5 * abb[49] + abb[51] * (T(-5) / T(6)) + abb[44] * (T(-1) / T(2)) + abb[50] * (T(7) / T(3)) + -z[8] + -z[42] + (T(1) / T(3)) * z[49];
z[8] = abb[8] * z[8];
z[42] = -abb[46] + abb[48];
z[49] = abb[53] * (T(1) / T(4));
z[42] = abb[50] * (T(7) / T(2)) + 11 * z[42] + -z[47] + z[49];
z[42] = abb[44] * (T(7) / T(2)) + abb[45] * (T(29) / T(12)) + (T(1) / T(3)) * z[42];
z[42] = abb[13] * z[42];
z[47] = abb[44] + -abb[51];
z[50] = abb[0] * z[47];
z[51] = abb[3] * abb[45];
z[52] = z[50] + z[51];
z[53] = 2 * abb[48];
z[54] = z[41] + -z[53];
z[55] = -abb[51] + abb[55] * (T(5) / T(2)) + z[54];
z[49] = abb[45] * (T(-7) / T(12)) + abb[44] * (T(-5) / T(3)) + abb[50] * (T(-1) / T(2)) + z[49] + (T(1) / T(3)) * z[55];
z[49] = abb[2] * z[49];
z[55] = 3 * abb[44];
z[56] = abb[3] * z[55];
z[57] = abb[48] + -abb[54];
z[58] = abb[1] * z[57];
z[3] = z[3] + z[4] + z[8] + z[15] + (T(4) / T(3)) * z[31] + (T(-8) / T(3)) * z[37] + z[42] + z[45] + z[49] + (T(-13) / T(6)) * z[52] + -z[56] + (T(13) / T(3)) * z[58];
z[3] = z[3] * z[20];
z[4] = 2 * abb[45];
z[8] = 3 * abb[48];
z[15] = 2 * abb[50];
z[20] = z[4] + z[8] + z[15];
z[42] = 3 * abb[54];
z[45] = 4 * abb[49] + -z[42];
z[49] = -z[20] + z[45] + z[48];
z[52] = abb[7] * z[49];
z[59] = -abb[47] + abb[52];
z[60] = abb[54] + z[59];
z[61] = -abb[46] + z[35];
z[60] = -abb[48] + -abb[49] + 3 * z[60] + z[61];
z[60] = abb[5] * z[60];
z[62] = -z[51] + z[60];
z[63] = -z[52] + z[62];
z[64] = -abb[47] + abb[50];
z[65] = -z[32] + z[64];
z[66] = z[40] + -z[53] + z[65];
z[66] = abb[3] * z[66];
z[67] = 3 * abb[49];
z[68] = -z[30] + z[67];
z[39] = abb[47] + -z[39] + z[68];
z[39] = abb[8] * z[39];
z[69] = abb[46] + -abb[52] + -abb[54];
z[70] = -abb[49] + z[36] + -z[69];
z[71] = abb[6] * z[70];
z[72] = 2 * z[71];
z[73] = -z[34] + z[43];
z[74] = abb[16] * z[73];
z[75] = abb[48] + -abb[49];
z[76] = abb[12] * z[75];
z[77] = 4 * z[76];
z[78] = -abb[54] + z[43];
z[79] = abb[52] + z[78];
z[80] = abb[15] * z[79];
z[81] = 2 * z[80];
z[82] = abb[9] * z[59];
z[39] = 5 * z[37] + z[39] + -9 * z[58] + -z[63] + z[66] + -z[72] + -z[74] + z[77] + z[81] + z[82];
z[19] = z[19] * z[39];
z[39] = 3 * abb[46];
z[66] = z[39] + -z[41];
z[83] = abb[53] * (T(1) / T(2));
z[84] = z[13] + -z[32] + z[46] + z[66] + -z[83];
z[84] = abb[3] * z[84];
z[85] = -abb[53] + z[15];
z[86] = -abb[55] + z[38];
z[87] = 5 * abb[52];
z[88] = 2 * abb[51];
z[89] = abb[44] + z[88];
z[90] = -abb[47] + -z[53] + z[85] + -z[86] + z[87] + -z[89];
z[90] = abb[8] * z[90];
z[91] = 4 * abb[48];
z[92] = 4 * abb[46];
z[93] = -abb[55] + z[91] + -z[92];
z[83] = abb[50] + z[83];
z[94] = abb[45] * (T(5) / T(2)) + z[55] + z[83] + z[93];
z[94] = abb[13] * z[94];
z[95] = 3 * abb[52];
z[92] = z[92] + -z[95];
z[96] = abb[47] + abb[49];
z[20] = -z[20] + z[92] + z[96];
z[97] = abb[4] * z[20];
z[94] = z[94] + z[97];
z[98] = abb[45] * (T(1) / T(2));
z[99] = -z[12] + z[98];
z[100] = abb[51] * (T(3) / T(2));
z[101] = z[34] + z[100];
z[102] = z[44] + -z[99] + -z[101];
z[103] = abb[17] * z[102];
z[104] = -abb[47] + abb[54];
z[99] = abb[44] + z[46] + z[99] + z[104];
z[99] = abb[10] * z[99];
z[105] = abb[3] * z[17];
z[106] = z[51] + z[105];
z[107] = z[72] + z[106];
z[108] = abb[48] + -abb[52];
z[109] = 5 * abb[45];
z[110] = -11 * abb[44] + -abb[50] + -abb[51] + 4 * abb[53] + 2 * abb[55] + -9 * z[108] + -z[109];
z[110] = abb[2] * z[110];
z[84] = -z[60] + -z[81] + z[84] + z[90] + z[94] + z[99] + -z[103] + -z[107] + z[110];
z[18] = z[18] * z[84];
z[84] = z[28] + -z[32];
z[90] = -8 * abb[46] + 6 * abb[52];
z[83] = -z[53] + -z[83] + z[84] + -z[90];
z[83] = abb[3] * z[83];
z[110] = z[38] + -z[53];
z[111] = 2 * abb[53];
z[112] = z[15] + z[110] + -z[111];
z[113] = 3 * abb[51];
z[29] = -z[29] + z[32] + -z[112] + z[113];
z[114] = abb[8] * z[29];
z[29] = abb[17] * z[29];
z[55] = abb[50] + -abb[55] + abb[53] * (T(-5) / T(2)) + z[55] + -z[98] + z[110];
z[55] = abb[13] * z[55];
z[98] = abb[45] + -abb[55];
z[21] = -z[21] + z[54] + -z[98];
z[54] = 3 * abb[2];
z[21] = z[21] * z[54];
z[54] = 2 * abb[6];
z[20] = z[20] * z[54];
z[115] = 2 * z[97];
z[20] = -z[20] + -z[21] + -z[29] + (T(5) / T(2)) * z[51] + -z[55] + z[56] + -z[83] + z[114] + z[115];
z[21] = abb[61] * z[20];
z[55] = -z[35] + z[67];
z[56] = -z[8] + z[95];
z[48] = -abb[54] + -z[48] + z[55] + z[56];
z[48] = abb[6] * z[48];
z[83] = z[26] + z[53];
z[95] = abb[54] + z[43] + -z[83] + z[95];
z[114] = abb[15] * z[95];
z[116] = 2 * z[37];
z[117] = abb[50] + z[33] + z[34];
z[117] = abb[3] * z[117];
z[118] = -z[32] + z[53];
z[119] = abb[54] + z[61] + z[118];
z[119] = abb[8] * z[119];
z[48] = z[48] + z[63] + -z[114] + -z[116] + -z[117] + -z[119];
z[114] = abb[31] * z[48];
z[8] = abb[52] + z[8] + z[35] + -z[39] + -z[42] + z[96];
z[8] = abb[6] * z[8];
z[38] = -z[38] + z[41];
z[36] = z[36] + z[38];
z[36] = abb[8] * z[36];
z[41] = -abb[52] + -z[42] + z[43] + z[83];
z[41] = abb[15] * z[41];
z[42] = -abb[49] + abb[50];
z[83] = -abb[52] + -z[42] + z[110];
z[83] = abb[3] * z[83];
z[8] = -z[8] + -z[36] + z[41] + z[62] + z[83] + -z[97];
z[36] = abb[32] * z[8];
z[62] = z[37] + z[80];
z[42] = z[34] + z[42] + -z[69];
z[42] = abb[3] * z[42];
z[69] = -abb[52] + abb[54];
z[83] = z[35] + z[69] + z[118];
z[83] = z[54] * z[83];
z[55] = -abb[52] + z[30] + z[53] + -z[55];
z[55] = abb[8] * z[55];
z[42] = z[42] + z[51] + z[52] + z[55] + z[62] + z[83];
z[42] = abb[34] * z[42];
z[35] = -z[35] + z[69] + z[110];
z[35] = z[35] * z[54];
z[53] = abb[50] + -abb[54] + z[53] + -z[66];
z[53] = abb[3] * z[53];
z[54] = abb[8] * z[70];
z[35] = -z[35] + z[51] + z[53] + z[54] + -z[62] + z[97];
z[53] = abb[33] * z[35];
z[36] = z[36] + z[42] + z[53] + z[114];
z[42] = abb[46] + 3 * abb[50];
z[40] = 3 * abb[45] + 7 * abb[48] + -z[40] + z[42] + -z[87] + z[96];
z[40] = abb[6] * z[40];
z[53] = z[15] + z[91];
z[4] = -abb[46] + z[4] + z[53] + -z[67] + z[69];
z[4] = abb[8] * z[4];
z[39] = -abb[49] + -z[39] + z[53] + -z[69];
z[39] = abb[3] * z[39];
z[53] = z[57] + -z[59];
z[54] = abb[15] * z[53];
z[4] = z[4] + -z[37] + z[39] + z[40] + 2 * z[51] + -4 * z[54] + -z[60];
z[4] = abb[30] * z[4];
z[4] = z[4] + 2 * z[36];
z[4] = abb[30] * z[4];
z[36] = -abb[46] + abb[53];
z[39] = abb[47] + z[36];
z[40] = z[39] + z[75];
z[54] = abb[55] * (T(1) / T(4));
z[46] = -z[17] + z[40] + z[46] + z[54];
z[46] = abb[8] * z[46];
z[55] = z[7] + z[47];
z[66] = -z[55] + z[108];
z[67] = 2 * abb[2];
z[69] = z[66] * z[67];
z[70] = z[69] + z[103];
z[75] = -abb[51] + abb[55] + z[40];
z[75] = abb[3] * z[75];
z[83] = -abb[51] + z[12];
z[87] = abb[0] * (T(1) / T(2));
z[83] = z[83] * z[87];
z[87] = 2 * z[58] + z[74];
z[46] = -z[31] + -z[46] + -z[70] + z[75] + -z[81] + 2 * z[82] + z[83] + z[87] + -z[105];
z[46] = z[14] * z[46];
z[51] = (T(1) / T(2)) * z[51] + z[103];
z[75] = z[82] + z[99];
z[83] = z[71] + z[74];
z[91] = abb[2] * z[66];
z[91] = z[58] + z[91];
z[96] = abb[3] * abb[44];
z[97] = z[83] + z[91] + z[96];
z[99] = abb[44] + z[7] + -z[12] + -z[34];
z[99] = abb[8] * z[99];
z[103] = abb[55] + -z[101];
z[103] = abb[3] * z[103];
z[75] = 2 * z[50] + -z[51] + z[60] + -3 * z[75] + -z[97] + z[99] + z[103];
z[75] = abb[29] * z[75];
z[99] = -abb[3] + abb[8];
z[79] = z[79] * z[99];
z[99] = 2 * abb[15];
z[53] = z[53] * z[99];
z[53] = -z[37] + z[53] + z[60] + z[71] + -z[79];
z[79] = 2 * abb[30];
z[103] = -z[53] * z[79];
z[110] = 3 * z[58];
z[114] = z[82] + z[110];
z[78] = abb[47] + z[78];
z[78] = abb[8] * z[78];
z[117] = abb[3] * z[57];
z[62] = z[62] + z[78] + -z[83] + -z[114] + -z[117];
z[78] = 2 * abb[31];
z[62] = z[62] * z[78];
z[7] = z[7] + z[56] + -z[89] + -z[98];
z[7] = z[7] * z[67];
z[56] = z[29] + -z[31];
z[67] = z[56] + -z[106];
z[83] = z[67] + -z[72];
z[38] = abb[51] + z[38] + -z[84];
z[38] = abb[3] * z[38];
z[84] = 2 * abb[8];
z[66] = z[66] * z[84];
z[38] = -z[7] + z[38] + z[66] + z[81] + -z[83];
z[38] = abb[32] * z[38];
z[46] = z[38] + z[46] + -z[62] + z[75] + z[103];
z[46] = abb[29] * z[46];
z[66] = abb[44] + abb[47];
z[44] = z[44] + -z[54] + z[66] + -z[100];
z[44] = abb[8] * z[44];
z[13] = -abb[49] + abb[53] * (T(-9) / T(2)) + z[13] + z[92];
z[13] = abb[3] * z[13];
z[75] = abb[55] * (T(3) / T(2));
z[89] = -abb[51] + z[75];
z[89] = -abb[44] + (T(1) / T(2)) * z[89];
z[89] = abb[0] * z[89];
z[13] = z[13] + z[44] + -z[51] + -z[71] + z[89] + -z[91] + -z[94] + z[105];
z[13] = abb[33] * z[13];
z[44] = -abb[49] + z[54];
z[51] = abb[46] + -z[17] + -z[44] + -z[85] + z[101];
z[51] = abb[8] * z[51];
z[36] = abb[49] + -abb[55] + z[34] + -z[36];
z[36] = abb[3] * z[36];
z[54] = -abb[51] + -z[75];
z[54] = z[17] + (T(1) / T(2)) * z[54];
z[54] = abb[0] * z[54];
z[36] = z[36] + z[51] + z[54] + z[70] + z[87] + z[107];
z[36] = abb[34] * z[36];
z[51] = z[38] + z[62];
z[13] = z[13] + z[36] + z[51];
z[13] = abb[33] * z[13];
z[36] = -z[43] + z[55] + -z[57];
z[36] = z[36] * z[84];
z[54] = z[95] * z[99];
z[28] = -abb[51] + z[28];
z[30] = -z[28] + z[30];
z[30] = abb[3] * z[30];
z[55] = 2 * z[60];
z[7] = z[7] + -z[30] + z[36] + -z[54] + z[55] + z[67] + -z[116];
z[30] = -abb[59] * z[7];
z[25] = z[25] * z[48];
z[36] = -abb[45] + -z[42] + z[45] + z[66];
z[36] = abb[8] * z[36];
z[42] = abb[53] + z[43] + -z[64];
z[42] = abb[3] * z[42];
z[36] = z[36] + z[42] + -z[50] + -z[52] + -z[77] + -z[97];
z[36] = abb[34] * z[36];
z[36] = z[36] + -z[51];
z[36] = abb[34] * z[36];
z[42] = abb[6] * z[49];
z[49] = abb[3] * z[73];
z[34] = -abb[46] + -z[34] + z[45];
z[34] = abb[8] * z[34];
z[34] = z[34] + z[42] + z[49] + -z[52] + -z[74] + -z[76] + -z[110];
z[24] = z[24] * z[34];
z[8] = -z[8] * z[23];
z[23] = z[43] + -z[108];
z[23] = abb[3] * z[23];
z[23] = z[23] + z[41] + z[60] + -z[74] + -z[114];
z[22] = z[22] * z[23];
z[23] = abb[38] * z[53];
z[34] = -abb[21] * z[102];
z[39] = abb[48] + z[39] + z[44] + -z[100];
z[39] = abb[18] * z[39];
z[12] = -z[12] + z[40];
z[12] = abb[19] * z[12];
z[40] = -abb[20] * z[73];
z[41] = -abb[27] * abb[58];
z[12] = z[12] + z[34] + z[39] + z[40] + z[41];
z[12] = abb[39] * z[12];
z[34] = abb[18] * abb[33];
z[39] = abb[18] * (T(1) / T(2));
z[40] = abb[19] + -z[39];
z[40] = abb[34] * z[40];
z[40] = (T(3) / T(2)) * z[34] + z[40];
z[40] = abb[33] * z[40];
z[39] = abb[19] + z[39];
z[14] = -z[14] * z[39];
z[39] = abb[18] + abb[19];
z[39] = abb[29] * z[39];
z[14] = z[14] + -z[39];
z[14] = abb[29] * z[14];
z[14] = z[14] + z[40];
z[40] = abb[3] + -abb[28] + abb[8] * (T(1) / T(4)) + abb[0] * (T(3) / T(4));
z[40] = abb[39] * z[40];
z[14] = (T(1) / T(2)) * z[14] + z[40];
z[14] = abb[56] * z[14];
z[40] = -abb[35] * z[59] * z[84];
z[0] = abb[62] + z[0] + z[1] + z[3] + z[4] + z[8] + z[12] + z[13] + z[14] + z[18] + z[19] + z[21] + z[22] + -2 * z[23] + z[24] + z[25] + z[30] + z[36] + z[40] + z[46];
z[1] = 4 * z[58] + -z[116];
z[3] = abb[55] + -z[17] + -z[33] + -z[112];
z[3] = abb[8] * z[3];
z[4] = z[50] + -z[82];
z[8] = -abb[55] + z[104];
z[8] = 2 * z[8] + z[113];
z[8] = abb[3] * z[8];
z[3] = -z[1] + z[3] + -4 * z[4] + z[8] + -z[29] + 3 * z[31] + -z[55] + z[69] + z[81] + z[106];
z[3] = abb[29] * z[3];
z[4] = -2 * z[76] + -z[82];
z[8] = abb[48] + -4 * abb[54] + -z[65];
z[8] = abb[3] * z[8];
z[12] = abb[54] + -z[32] + -z[61];
z[12] = abb[8] * z[12];
z[4] = 2 * z[4] + z[8] + z[12] + -4 * z[37] + 6 * z[58] + z[63] + z[71] + -z[80];
z[4] = z[4] * z[78];
z[8] = -z[17] + -z[26] + z[33] + -z[86] + z[88] + z[111];
z[8] = abb[8] * z[8];
z[12] = -abb[55] + z[88];
z[12] = abb[0] * z[12];
z[1] = z[1] + z[12] + z[69];
z[12] = 9 * abb[53] + -z[28] + -z[33] + z[90];
z[12] = abb[3] * z[12];
z[13] = 6 * abb[44] + abb[53] + z[15] + 2 * z[93] + z[109];
z[13] = abb[13] * z[13];
z[8] = z[1] + z[8] + z[12] + z[13] + -z[56] + z[72] + -4 * z[96] + z[115];
z[8] = abb[33] * z[8];
z[12] = abb[50] + abb[55] + -z[32] + -z[57];
z[12] = -abb[51] + 2 * z[12];
z[12] = abb[3] * z[12];
z[13] = abb[45] + -abb[48] + 4 * abb[50] + -abb[53] + z[47] + -z[68];
z[13] = z[13] * z[84];
z[1] = -z[1] + z[12] + z[13] + 2 * z[52] + 8 * z[76] + z[83];
z[1] = abb[34] * z[1];
z[12] = -z[35] * z[79];
z[13] = -abb[19] * abb[34];
z[13] = z[13] + -z[34] + z[39];
z[13] = abb[56] * z[13];
z[1] = z[1] + z[3] + z[4] + z[8] + z[12] + z[13] + -z[38];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[42] * z[20];
z[4] = abb[57] + abb[58];
z[8] = z[4] * z[27];
z[7] = -z[7] + z[8];
z[7] = abb[40] * z[7];
z[8] = abb[41] * z[48];
z[2] = -abb[26] + z[2];
z[2] = abb[34] * z[2];
z[12] = abb[24] * abb[30];
z[11] = abb[29] * z[11];
z[2] = -z[2] + z[6] + -z[10] + -z[11] + -z[12] + z[16];
z[2] = m1_set::bc<T>[0] * z[2];
z[6] = abb[42] * z[9];
z[5] = abb[41] * z[5];
z[2] = z[2] + z[5] + z[6];
z[4] = -2 * z[4];
z[2] = z[2] * z[4];
z[1] = abb[43] + z[1] + z[2] + z[3] + z[7] + 2 * z[8];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_95_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("11.818739223413243445745795987723437790666512983892190367890315121"),stof<T>("44.754399886010530852318458314120175237537482781549564337076155391")}, std::complex<T>{stof<T>("-37.027995434542604999043048785710571153547612773450516089004662653"),stof<T>("27.637550264215732165451884728185959882003042953334876164584246247")}, std::complex<T>{stof<T>("-28.944413222383027147156015065311228666600510142399734562986769782"),stof<T>("2.92970187156815851982918021919083860559265279510362952929692326")}, std::complex<T>{stof<T>("21.769924145399934588806295815261225317645605623326053212092370974"),stof<T>("0.154846479227575669052226127147132929676576412344910747929386386")}, std::complex<T>{stof<T>("-9.3158024497901647230267165564942523843653569481861814086571217436"),stof<T>("-0.1875263779002324884380101836113872368315554181279792831875918093")}, std::complex<T>{stof<T>("-15.729879473522533798281407757855197689633633510169444857681094052"),stof<T>("-6.842445878046103090736471064975462617205686868808615377586587492")}, std::complex<T>{stof<T>("14.291599933015901164820824267642516659885048981301149606213429671"),stof<T>("-1.234586117356051657197391093435147525638958312306249364893172377")}, std::complex<T>{stof<T>("-9.989991038362600690470491340646529778979602033327728667205886776"),stof<T>("-26.130531549626869818921677828367347755756149227829778958231870191")}, std::complex<T>{stof<T>("41.271901037933508080058708545353312695417859223681234115620988239"),stof<T>("-42.386053156425279531643936383942068145152932229951611321422394863")}, std::complex<T>{stof<T>("-63.301908878788412641598797524714208892981284069846555517884539182"),stof<T>("63.539658901733851567275641798393917143682286505912596354835844172")}, std::complex<T>{stof<T>("0.5509176442707733610293774216651768976988735591392508117968224384"),stof<T>("-3.4799568974849239280430675881673453769503367422757952496162231153")}, std::complex<T>{stof<T>("1.4922734029422123673502366485390513835483401929256758991575869079"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("-2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("3.1923472445362258158409460442271726429200038553757622486786770783")}};
	
	std::vector<C> intdlogs = {rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_95_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_95_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(112)) * (-v[3] + v[5]) * (7 * (4 * (-2 + 3 * m1_set::bc<T>[2]) * v[0] + -4 * m1_set::bc<T>[4] * (v[3] + v[5]) + m1_set::bc<T>[2] * (-4 * v[1] + 3 * v[3] + 9 * v[5])) + 2 * (-112 * m1_set::bc<T>[1] + 84 * m1_set::bc<T>[2] + -56 * m1_set::bc<T>[4] + 28 * v[1] + 28 * v[2] + 21 * v[3] + -8 * v[4] + 7 * (v[5] + -4 * m1_set::bc<T>[1] * (v[3] + v[5]))))) / prod_pow(tend, 2);
c[1] = (-(4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (-1 * v[3] + v[5])) / tend;


		return (abb[45] + -2 * abb[46] + -abb[47] + abb[48] + abb[50] + 2 * abb[52]) * (2 * t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[6];
z[0] = -abb[35] + abb[38];
z[1] = abb[46] + -abb[52];
z[1] = abb[45] + -abb[47] + abb[48] + abb[50] + -2 * z[1];
z[0] = z[0] * z[1];
z[2] = abb[32] * z[1];
z[3] = abb[30] * z[1];
z[4] = z[2] + -z[3];
z[4] = abb[34] * z[4];
z[0] = z[0] + z[4];
z[4] = abb[29] + abb[33];
z[4] = z[1] * z[4];
z[5] = -z[3] + 2 * z[4];
z[5] = abb[30] * z[5];
z[3] = -2 * z[3] + -z[4];
z[2] = 5 * z[2] + 2 * z[3];
z[2] = abb[32] * z[2];
z[1] = abb[37] * z[1];
z[0] = 2 * z[0] + 4 * z[1] + z[2] + z[5];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_95_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return (abb[45] + -2 * abb[46] + -abb[47] + abb[48] + abb[50] + 2 * abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[46] + -abb[52];
z[0] = -abb[45] + abb[47] + -abb[48] + -abb[50] + 2 * z[0];
z[1] = abb[30] + -abb[32];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_95_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("25.228597064040919499987426981303137824714798146483871772823608227"),stof<T>("58.210988977603606177648004051781877792579761371629004839185840977")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[43] = SpDLog_f_4_95_W_16_Im(t, path, abb);
abb[62] = SpDLog_f_4_95_W_16_Re(t, path, abb);

                    
            return f_4_95_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_95_DLogXconstant_part(base_point<T>, kend);
	value += f_4_95_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_95_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_95_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_95_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_95_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_95_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_95_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
