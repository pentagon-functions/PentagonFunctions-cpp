/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_460.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_460_abbreviated (const std::array<T,62>& abb) {
T z[89];
z[0] = abb[43] + abb[44] + -abb[45];
z[0] = abb[42] + (T(1) / T(2)) * z[0];
z[1] = abb[46] * (T(1) / T(2));
z[2] = z[0] + -z[1];
z[3] = abb[23] * z[2];
z[4] = abb[19] * z[2];
z[5] = z[3] + -z[4];
z[6] = abb[47] + -abb[53];
z[7] = abb[49] + abb[51];
z[8] = z[6] + z[7];
z[9] = abb[12] * z[8];
z[10] = -abb[53] + z[7];
z[11] = -abb[47] + z[10];
z[12] = abb[13] * z[11];
z[13] = z[9] + z[12];
z[5] = (T(-1) / T(4)) * z[5] + (T(1) / T(8)) * z[13];
z[14] = abb[24] * z[2];
z[15] = abb[14] * (T(1) / T(2));
z[15] = z[11] * z[15];
z[15] = z[14] + z[15];
z[16] = abb[2] * z[10];
z[16] = -z[15] + z[16];
z[17] = 5 * abb[53];
z[18] = 3 * abb[47];
z[19] = -3 * z[7] + z[17] + -z[18];
z[19] = abb[52] + (T(1) / T(2)) * z[19];
z[19] = abb[5] * z[19];
z[20] = abb[52] * (T(1) / T(2));
z[21] = abb[47] * (T(1) / T(2));
z[22] = abb[53] * (T(-3) / T(2)) + z[7] + -z[20] + -z[21];
z[23] = abb[0] * (T(1) / T(2));
z[22] = z[22] * z[23];
z[24] = (T(1) / T(2)) * z[7];
z[25] = -abb[53] + z[24];
z[26] = z[20] + -z[21] + -z[25];
z[26] = abb[1] * z[26];
z[27] = abb[6] * z[8];
z[28] = (T(1) / T(4)) * z[27];
z[29] = abb[22] * z[2];
z[0] = abb[20] * z[0];
z[0] = z[0] + z[29];
z[1] = abb[20] * z[1];
z[29] = z[0] + -z[1];
z[30] = abb[4] * z[11];
z[19] = z[5] + z[16] + (T(1) / T(4)) * z[19] + z[22] + z[26] + z[28] + (T(3) / T(4)) * z[29] + (T(1) / T(8)) * z[30];
z[19] = abb[30] * z[19];
z[22] = abb[47] + abb[53];
z[26] = z[7] + z[22];
z[31] = abb[52] + (T(1) / T(2)) * z[26];
z[32] = abb[5] * z[31];
z[33] = abb[47] + abb[52];
z[34] = 3 * abb[53];
z[35] = z[33] + z[34];
z[35] = abb[0] * z[35];
z[35] = -z[29] + z[32] + z[35];
z[36] = abb[2] * abb[53];
z[37] = z[15] + z[36];
z[38] = (T(1) / T(4)) * z[8];
z[39] = -abb[3] * z[38];
z[40] = 5 * abb[47];
z[41] = -3 * z[10] + -z[40];
z[41] = abb[4] * z[41];
z[33] = abb[8] * z[33];
z[5] = -z[5] + (T(-1) / T(2)) * z[33] + (T(1) / T(4)) * z[35] + z[37] + z[39] + (T(1) / T(8)) * z[41];
z[5] = abb[27] * z[5];
z[35] = z[0] + z[4];
z[39] = (T(1) / T(2)) * z[8];
z[41] = abb[12] * z[39];
z[42] = z[35] + z[41];
z[43] = abb[2] * (T(1) / T(2));
z[44] = z[11] * z[43];
z[45] = abb[5] * z[39];
z[45] = z[1] + z[45];
z[44] = z[44] + -z[45];
z[46] = abb[0] * abb[47];
z[46] = z[15] + -z[42] + -z[44] + z[46];
z[46] = abb[28] * z[46];
z[47] = abb[53] * (T(1) / T(2));
z[48] = z[21] + z[47];
z[49] = abb[48] + abb[50];
z[50] = (T(-3) / T(2)) * z[7] + z[48] + -z[49];
z[50] = abb[14] * z[50];
z[50] = -z[14] + z[50];
z[51] = abb[52] + abb[53];
z[52] = abb[0] * z[51];
z[53] = abb[1] * z[51];
z[54] = (T(1) / T(2)) * z[12];
z[52] = z[52] + -z[53] + -z[54];
z[55] = abb[4] * z[8];
z[56] = (T(3) / T(2)) * z[55];
z[57] = abb[47] + z[7];
z[17] = z[17] + -z[57];
z[17] = z[17] * z[43];
z[58] = abb[53] + z[49];
z[59] = abb[11] * z[58];
z[17] = z[17] + -z[50] + z[52] + -z[56] + -z[59];
z[60] = (T(1) / T(2)) * z[3];
z[61] = z[10] + z[49];
z[20] = -abb[47] + -z[20] + (T(-1) / T(2)) * z[61];
z[20] = abb[8] * z[20];
z[17] = (T(1) / T(2)) * z[17] + z[20] + z[60];
z[17] = abb[31] * z[17];
z[20] = -z[41] + z[54];
z[62] = -abb[52] + z[6];
z[62] = abb[0] * z[62];
z[62] = z[1] + z[20] + -z[35] + z[62];
z[63] = z[6] + -z[7];
z[64] = abb[2] * z[63];
z[18] = z[10] + z[18];
z[65] = abb[4] * (T(1) / T(2));
z[66] = z[18] * z[65];
z[34] = -z[34] + z[57];
z[57] = -abb[52] + (T(1) / T(2)) * z[34];
z[67] = abb[5] * z[57];
z[64] = z[62] + z[64] + z[66] + z[67];
z[66] = z[33] + -z[60];
z[64] = (T(1) / T(2)) * z[64] + z[66];
z[64] = abb[26] * z[64];
z[5] = z[5] + z[17] + z[19] + (T(-1) / T(2)) * z[46] + z[64];
z[17] = abb[6] * z[39];
z[19] = abb[1] * z[8];
z[64] = z[17] + -z[19];
z[0] = z[0] + -z[45] + z[64];
z[67] = z[7] + z[49];
z[68] = abb[14] * z[67];
z[68] = -z[59] + z[68];
z[69] = z[8] + z[49];
z[70] = abb[8] * z[69];
z[71] = z[68] + -z[70];
z[72] = abb[3] * z[39];
z[73] = -abb[53] + -z[7];
z[73] = abb[2] * z[73];
z[74] = -abb[0] * z[7];
z[56] = -z[0] + z[56] + -z[71] + z[72] + z[73] + z[74];
z[73] = abb[29] * (T(1) / T(16));
z[56] = z[56] * z[73];
z[5] = (T(1) / T(8)) * z[5] + z[56];
z[5] = m1_set::bc<T>[0] * z[5];
z[56] = abb[16] + -abb[18];
z[74] = abb[15] + abb[17];
z[75] = -z[56] + z[74];
z[21] = z[21] * z[75];
z[56] = -abb[15] + abb[17] + z[56];
z[47] = z[47] * z[56];
z[56] = abb[25] * z[2];
z[75] = abb[49] * (T(1) / T(2)) + abb[51] * (T(1) / T(2));
z[74] = -abb[18] + z[74];
z[74] = z[74] * z[75];
z[76] = abb[15] * abb[52];
z[77] = abb[16] * z[24];
z[21] = z[21] + -z[47] + z[56] + z[74] + z[76] + z[77];
z[47] = abb[39] * z[21];
z[2] = abb[21] * z[2];
z[56] = abb[7] * z[39];
z[56] = z[2] + z[56];
z[35] = z[35] + z[56];
z[74] = z[35] + -z[70];
z[76] = abb[0] * z[67];
z[63] = z[43] * z[63];
z[76] = -z[63] + z[74] + z[76];
z[77] = abb[10] * z[61];
z[1] = -z[1] + z[77];
z[78] = abb[5] * (T(1) / T(2));
z[79] = z[34] * z[78];
z[65] = z[11] * z[65];
z[79] = z[1] + z[65] + -z[68] + z[76] + -z[79];
z[79] = (T(1) / T(16)) * z[79];
z[80] = abb[38] * z[79];
z[44] = z[44] + z[72];
z[81] = abb[0] * z[61];
z[74] = -z[15] + z[44] + z[74] + z[81];
z[74] = abb[36] * z[74];
z[48] = z[48] + z[49] + z[75];
z[48] = abb[14] * z[48];
z[75] = abb[4] * z[39];
z[14] = -z[14] + z[48] + -z[59] + -z[75];
z[48] = z[4] + z[56];
z[81] = abb[2] * z[39];
z[82] = z[14] + -z[48] + z[81];
z[83] = abb[0] * z[58];
z[83] = -z[64] + -z[82] + z[83];
z[83] = abb[37] * z[83];
z[74] = z[74] + z[83];
z[14] = -z[14] + -z[41] + z[56];
z[41] = abb[47] + z[67];
z[67] = abb[0] * (T(1) / T(16));
z[83] = z[41] * z[67];
z[83] = (T(1) / T(16)) * z[14] + z[83];
z[83] = abb[35] * z[83];
z[35] = -z[15] + z[35];
z[1] = z[1] + z[35];
z[84] = -abb[52] + -z[39];
z[84] = abb[5] * z[84];
z[85] = -abb[52] + z[61];
z[86] = abb[0] * z[85];
z[84] = z[1] + z[54] + z[84] + z[86];
z[86] = (T(1) / T(16)) * z[3];
z[87] = abb[8] * (T(1) / T(8));
z[85] = -z[85] * z[87];
z[84] = (T(1) / T(16)) * z[84] + z[85] + -z[86];
z[84] = abb[34] * z[84];
z[85] = (T(1) / T(32)) * z[6];
z[88] = -abb[41] * z[85];
z[5] = abb[60] + z[5] + (T(-1) / T(32)) * z[47] + (T(1) / T(16)) * z[74] + z[80] + z[83] + z[84] + z[88];
z[34] = z[34] * z[43];
z[15] = z[3] + z[15] + -z[33] + -z[34] + z[52] + -z[75];
z[15] = abb[31] * z[15];
z[33] = z[4] + z[29];
z[22] = abb[52] + z[22];
z[22] = z[22] * z[23];
z[13] = (T(1) / T(4)) * z[13] + -z[22] + (T(1) / T(2)) * z[33];
z[18] = abb[4] * z[18];
z[22] = z[31] * z[78];
z[18] = -z[13] + (T(-1) / T(4)) * z[18] + z[22] + z[37] + -z[66];
z[18] = abb[27] * z[18];
z[22] = -abb[52] + z[69];
z[22] = abb[0] * z[22];
z[33] = abb[4] * abb[47];
z[34] = -abb[5] * z[51];
z[20] = z[20] + z[22] + z[33] + z[34] + z[56] + z[63];
z[11] = -z[11] + -z[49];
z[11] = abb[52] + (T(1) / T(2)) * z[11];
z[11] = abb[8] * z[11];
z[11] = z[11] + (T(1) / T(2)) * z[20] + -z[60];
z[11] = abb[26] * z[11];
z[11] = z[11] + z[15] + z[18] + -z[46];
z[11] = abb[26] * z[11];
z[18] = z[29] + z[50] + z[59] + z[70];
z[20] = -abb[5] * z[38];
z[22] = z[10] * z[23];
z[23] = -abb[1] * z[39];
z[25] = abb[2] * z[25];
z[18] = (T(1) / T(2)) * z[18] + z[20] + z[22] + z[23] + z[25] + z[28] + z[75];
z[18] = prod_pow(abb[31], 2) * z[18];
z[20] = z[53] + -z[60];
z[22] = -z[57] * z[78];
z[13] = z[13] + z[16] + z[20] + z[22] + (T(1) / T(4)) * z[30];
z[13] = abb[26] * z[13];
z[22] = abb[0] * z[10];
z[0] = z[0] + z[16] + z[22];
z[16] = -abb[31] * z[0];
z[22] = z[32] + z[65];
z[23] = -abb[2] * z[8];
z[23] = -z[22] + z[23] + -z[62];
z[20] = -z[20] + (T(1) / T(2)) * z[23];
z[20] = abb[27] * z[20];
z[13] = z[13] + z[16] + z[20] + z[46];
z[13] = abb[30] * z[13];
z[16] = abb[59] * z[21];
z[20] = z[72] + z[75];
z[21] = abb[0] * abb[53];
z[21] = -z[20] + z[21] + z[37];
z[23] = -abb[28] * z[21];
z[15] = -z[15] + z[23];
z[15] = abb[27] * z[15];
z[20] = -z[20] + z[42] + -z[45] + -z[63];
z[20] = abb[33] * z[20];
z[1] = -abb[54] * z[1];
z[23] = abb[57] * z[82];
z[25] = -z[35] + -z[44];
z[25] = abb[56] * z[25];
z[28] = -abb[0] * z[41];
z[14] = -z[14] + z[28];
z[14] = abb[55] * z[14];
z[28] = abb[54] * z[39];
z[30] = abb[52] * abb[54];
z[28] = z[28] + z[30];
z[28] = abb[5] * z[28];
z[1] = z[1] + z[11] + z[13] + z[14] + z[15] + (T(1) / T(2)) * z[16] + z[18] + z[20] + z[23] + z[25] + z[28];
z[9] = -z[9] + z[12] + z[19];
z[11] = -23 * abb[53] + 5 * z[7] + z[40];
z[11] = abb[52] + (T(1) / T(4)) * z[11];
z[11] = abb[5] * z[11];
z[13] = abb[47] + -z[24];
z[13] = abb[2] * z[13];
z[3] = z[3] + z[11] + z[13];
z[11] = -abb[47] + z[51];
z[11] = (T(-7) / T(8)) * z[7] + (T(1) / T(4)) * z[11] + -z[49];
z[11] = abb[0] * z[11];
z[2] = z[2] + -z[11];
z[4] = -z[4] + (T(-5) / T(6)) * z[29];
z[11] = abb[7] * (T(-1) / T(6)) + abb[3] * (T(-1) / T(48));
z[8] = z[8] * z[11];
z[11] = abb[47] * (T(1) / T(3)) + z[61];
z[11] = abb[52] * (T(-1) / T(3)) + (T(1) / T(2)) * z[11];
z[11] = abb[8] * z[11];
z[10] = -abb[47] + -7 * z[10];
z[10] = abb[4] * z[10];
z[2] = (T(-1) / T(3)) * z[2] + (T(1) / T(12)) * z[3] + (T(1) / T(4)) * z[4] + z[8] + (T(-1) / T(24)) * z[9] + (T(1) / T(16)) * z[10] + (T(1) / T(2)) * z[11] + (T(1) / T(48)) * z[27] + (T(13) / T(24)) * z[68] + (T(-11) / T(24)) * z[77];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[3] = -z[26] * z[43];
z[4] = abb[0] * z[49];
z[3] = z[3] + z[4] + z[48] + z[55] + -z[64] + -z[68] + z[72];
z[3] = abb[29] * z[3];
z[4] = abb[27] * z[21];
z[8] = z[45] + z[75] + -z[76];
z[8] = abb[26] * z[8];
z[0] = abb[30] * z[0];
z[9] = z[36] + -z[55] + z[71];
z[9] = abb[31] * z[9];
z[0] = z[0] + (T(1) / T(2)) * z[3] + z[4] + z[8] + z[9];
z[0] = z[0] * z[73];
z[3] = abb[52] + z[7];
z[3] = abb[0] * z[3];
z[3] = z[3] + z[17] + -z[22] + z[29] + -z[54] + -z[81];
z[4] = -abb[1] * z[31];
z[3] = (T(1) / T(2)) * z[3] + z[4] + z[60];
z[3] = abb[32] * z[3];
z[4] = -abb[58] * z[79];
z[7] = abb[54] * z[61];
z[7] = z[7] + -z[30];
z[8] = -abb[56] * z[61];
z[9] = -abb[57] * z[58];
z[6] = -abb[33] * z[6];
z[6] = z[6] + -z[7] + z[8] + z[9];
z[6] = z[6] * z[67];
z[8] = prod_pow(abb[28], 2) * z[21];
z[9] = (T(-1) / T(16)) * z[19] + (T(1) / T(32)) * z[27];
z[9] = abb[57] * z[9];
z[10] = abb[56] * z[69];
z[7] = z[7] + (T(1) / T(2)) * z[10];
z[7] = z[7] * z[87];
z[10] = (T(-1) / T(32)) * z[12] + z[86];
z[10] = abb[54] * z[10];
z[11] = abb[61] * z[85];
z[0] = abb[40] + z[0] + (T(1) / T(16)) * z[1] + (T(1) / T(4)) * z[2] + (T(1) / T(8)) * z[3] + z[4] + z[6] + z[7] + (T(1) / T(32)) * z[8] + z[9] + z[10] + z[11];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_4_460_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("-0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.020898306371438252515741060746933841823397787250839508815725475101"),stof<T>("0.13574022450088939322201724851925175755513123840040319374065701043")}, std::complex<T>{stof<T>("-1.8894418669371645730204993893283593184686245560274281144335764771"),stof<T>("1.471792070798879671164463795263226221586895519171419839578721322")}, std::complex<T>{stof<T>("-1.2515314633952394846857061461871166213156859800874300356297644481"),stof<T>("1.2449984739144736067303912244927573371002963207903634319358680512")}, std::complex<T>{stof<T>("-1.8894418669371645730204993893283593184686245560274281144335764771"),stof<T>("1.471792070798879671164463795263226221586895519171419839578721322")}, std::complex<T>{stof<T>("-1.2515314633952394846857061461871166213156859800874300356297644481"),stof<T>("1.2449984739144736067303912244927573371002963207903634319358680512")}, std::complex<T>{stof<T>("-0.04416491205478169634732093462559222042060343709867799365733542925"),stof<T>("-0.22004800893372987519086463530647671709403933474386728638382060846")}, std::complex<T>{stof<T>("-0.0762911296698978992321854584363258363070662741253910680252919535"),stof<T>("-0.28234313269784647430148255327235801478488005727145989535697007576")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_460_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_460_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[49] + abb[51] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[49] + -abb[51] + -abb[52];
z[1] = abb[30] + -abb[31];
return abb[9] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_460_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + -8 * v[0] + v[1] + -3 * v[2] + -3 * v[3] + 3 * v[4] + m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[49] + abb[51] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + abb[31];
z[0] = (T(1) / T(2)) * z[0];
z[1] = -abb[26] + abb[27];
z[0] = z[0] * z[1];
z[0] = -abb[32] + z[0];
z[1] = -abb[49] + -abb[51] + -abb[52];
return abb[9] * (T(1) / T(8)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_460_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.4017171165845835044469807367406551350310648587238345235749258873"),stof<T>("-1.9627230957397030912488869930680979285028291252356113868661741411")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W20(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}};
abb[40] = SpDLog_f_4_460_W_20_Im(t, path, abb);
abb[41] = SpDLogQ_W_84(k,dl,dlr).imag();
abb[60] = SpDLog_f_4_460_W_20_Re(t, path, abb);
abb[61] = SpDLogQ_W_84(k,dl,dlr).real();

                    
            return f_4_460_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_460_DLogXconstant_part(base_point<T>, kend);
	value += f_4_460_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_460_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_460_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_460_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_460_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_460_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_460_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
