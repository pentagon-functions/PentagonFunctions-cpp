/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_14.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_14_abbreviated (const std::array<T,9>& abb) {
T z[5];
z[0] = prod_pow(abb[3], 2);
z[1] = -abb[3] + abb[4] * (T(1) / T(2));
z[1] = abb[4] * z[1];
z[0] = (T(1) / T(2)) * z[0] + z[1];
z[1] = -abb[6] + abb[7];
z[2] = abb[2] * z[1];
z[3] = abb[1] * z[1];
z[4] = z[2] + -z[3];
z[0] = z[0] * z[4];
z[1] = abb[0] * z[1];
z[2] = (T(1) / T(6)) * z[1] + (T(-1) / T(2)) * z[2] + (T(1) / T(3)) * z[3];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[1] = z[1] + -z[3];
z[3] = -abb[8] * z[1];
z[0] = z[0] + z[2] + z[3];
z[1] = -abb[5] * z[1];
z[2] = abb[3] + -abb[4];
z[2] = m1_set::bc<T>[0] * z[2] * z[4];
z[1] = z[1] + z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_14_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {stof<T>("4.3220869092982539864735431981106355287236292433493309190062695908"), stof<T>("-4.3220869092982539864735431981106355287236292433493309190062695908")};
	
	std::vector<C> intdlogs = {rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_14_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_14_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{T(0),stof<T>("19.825958964856164367818247914575849812881459457182429812310504525")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,9> abb = {dl[5], dl[2], dlog_W22(k,dl), f_1_4(k), f_1_5(k), f_2_7_im(k), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), f_2_7_re(k)};

                    
            return f_4_14_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_14_DLogXconstant_part(base_point<T>, kend);
	value += f_4_14_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_14_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_14_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_14_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_14_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_14_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_14_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
