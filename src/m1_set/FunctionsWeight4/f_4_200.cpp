/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_200.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_200_abbreviated (const std::array<T,25>& abb) {
T z[18];
z[0] = -abb[22] + abb[24];
z[1] = abb[20] + z[0];
z[1] = abb[2] * z[1];
z[2] = (T(1) / T(2)) * z[1];
z[3] = -abb[23] + abb[24];
z[4] = abb[20] + -z[3];
z[4] = abb[4] * z[4];
z[5] = 2 * z[4];
z[6] = abb[21] + abb[22];
z[7] = -abb[24] + (T(1) / T(2)) * z[6];
z[8] = abb[3] * z[7];
z[9] = abb[9] * z[7];
z[10] = -abb[21] + abb[24];
z[11] = -abb[20] + z[10];
z[11] = abb[7] * z[11];
z[6] = 2 * abb[24] + -z[6];
z[12] = abb[8] * z[6];
z[12] = z[2] + z[5] + z[8] + z[9] + (T(3) / T(2)) * z[11] + z[12];
z[12] = prod_pow(abb[12], 2) * z[12];
z[7] = abb[6] * z[7];
z[2] = z[2] + z[7] + -z[9] + (T(-1) / T(2)) * z[11];
z[2] = abb[13] * z[2];
z[9] = -z[1] + z[11];
z[13] = -abb[6] + abb[9];
z[13] = z[6] * z[13];
z[14] = z[9] + -z[13];
z[15] = -abb[12] * z[14];
z[2] = z[2] + z[15];
z[2] = abb[13] * z[2];
z[0] = abb[19] + z[0];
z[0] = abb[0] * z[0];
z[3] = abb[19] + -z[3];
z[15] = abb[1] * z[3];
z[4] = z[0] + -z[1] + -z[4] + z[15];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[15] = -abb[8] + abb[9];
z[15] = z[6] * z[15];
z[16] = 2 * abb[1];
z[3] = z[3] * z[16];
z[10] = -abb[19] + z[10];
z[10] = abb[5] * z[10];
z[3] = z[0] + z[3] + z[10] + -z[15];
z[16] = -prod_pow(abb[11], 2) * z[3];
z[17] = -abb[3] + abb[9];
z[17] = z[6] * z[17];
z[0] = -z[0] + z[10];
z[10] = z[0] + z[17];
z[10] = abb[15] * z[10];
z[9] = z[9] + z[17];
z[9] = abb[17] * z[9];
z[7] = z[0] + -z[7] + z[8];
z[7] = abb[10] * z[7];
z[8] = -abb[3] + -abb[6] + 2 * abb[9];
z[6] = z[6] * z[8];
z[8] = -abb[12] * z[6];
z[7] = z[7] + z[8];
z[7] = abb[10] * z[7];
z[8] = abb[14] + abb[16] + -abb[18];
z[6] = z[6] * z[8];
z[2] = z[2] + (T(13) / T(6)) * z[4] + z[6] + z[7] + z[9] + z[10] + z[12] + z[16];
z[1] = -z[1] + -z[5] + -z[11] + z[15];
z[1] = abb[12] * z[1];
z[3] = abb[11] * z[3];
z[4] = abb[13] * z[14];
z[0] = -z[0] + z[13];
z[0] = abb[10] * z[0];
z[0] = z[0] + z[1] + z[3] + z[4];
z[0] = 2 * m1_set::bc<T>[0] * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_200_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("12.309665232445104243494171093389046166704526849384416923684068631"),stof<T>("-53.120987485953610435365611833951473570097059987826090073162833252")}, std::complex<T>{stof<T>("-50.419172892063490390390915606005077334153600823961937838957660498"),stof<T>("26.858784701253073517521367133391742486838213377713421699132211851")}, std::complex<T>{stof<T>("-2.1932176242585739986591639138817948453863889400616605306154570286"),stof<T>("4.1250878519596201870296527229978940872860288119180415307706102621")}, std::complex<T>{stof<T>("21.825784102250015917846654175358414047859546913250564372758398406"),stof<T>("14.179076821432122271211719083143155610547985974413513307890074136")}, std::complex<T>{stof<T>("-14.090505933109796230390926423375822274203138121265296011899736434"),stof<T>("-16.208213815228034833662178340414469559996889447617196596911157526")}, std::complex<T>{stof<T>("-5.5420605448816456887965638381007969282700198519236078302432049434"),stof<T>("-2.0959508581637076245791934657265801378371253387143582417495268727")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_200_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_200_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-34.813336625853520386889768023935851896109811203256614689606003671"),stof<T>("46.298441757408904226827102348109590721274075773218360474061165012")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,25> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), f_1_1(k), f_1_3(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real())};

                    
            return f_4_200_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_200_DLogXconstant_part(base_point<T>, kend);
	value += f_4_200_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_200_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_200_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_200_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_200_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_200_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_200_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
