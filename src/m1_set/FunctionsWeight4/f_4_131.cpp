/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_131.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_131_abbreviated (const std::array<T,56>& abb) {
T z[69];
z[0] = abb[8] * (T(1) / T(2));
z[1] = abb[28] + abb[31];
z[2] = -abb[32] + z[1];
z[3] = -abb[29] + z[2];
z[4] = z[0] * z[3];
z[5] = abb[6] * (T(1) / T(2));
z[6] = -abb[9] + z[5];
z[7] = abb[32] * z[6];
z[8] = abb[28] * z[6];
z[4] = z[4] + z[7] + -z[8];
z[7] = -abb[30] + abb[32];
z[9] = abb[4] * (T(1) / T(2));
z[10] = -abb[1] + z[9];
z[11] = z[7] * z[10];
z[12] = abb[29] + -abb[31];
z[13] = abb[7] * (T(1) / T(2));
z[14] = z[12] * z[13];
z[15] = -abb[29] + abb[33];
z[16] = abb[3] * (T(1) / T(2));
z[17] = -z[15] * z[16];
z[18] = -abb[33] + z[2];
z[19] = abb[13] * (T(1) / T(2));
z[20] = -z[18] * z[19];
z[21] = abb[28] + -abb[30];
z[22] = abb[29] + z[21];
z[23] = -abb[33] + z[22];
z[24] = (T(1) / T(2)) * z[23];
z[25] = abb[14] * z[24];
z[26] = abb[15] * z[3];
z[27] = abb[0] * z[23];
z[11] = -z[4] + z[11] + z[14] + z[17] + z[20] + -z[25] + z[26] + z[27];
z[11] = abb[50] * z[11];
z[14] = -abb[42] + abb[43] + -abb[44] + abb[45];
z[17] = abb[32] * z[14];
z[20] = abb[31] * z[14];
z[27] = z[17] + -z[20];
z[28] = abb[28] * z[14];
z[29] = z[27] + -z[28];
z[30] = abb[29] * z[14];
z[31] = z[29] + z[30];
z[32] = abb[20] + abb[22];
z[31] = z[31] * z[32];
z[32] = z[17] + -z[28];
z[33] = abb[21] + abb[23];
z[32] = z[32] * z[33];
z[34] = abb[19] * abb[52];
z[35] = abb[30] * z[34];
z[36] = abb[30] * z[14];
z[37] = abb[25] * z[36];
z[35] = z[35] + z[37];
z[37] = abb[17] * abb[52];
z[38] = abb[30] * z[37];
z[39] = z[35] + -z[38];
z[40] = abb[25] * z[14];
z[34] = z[34] + z[40];
z[40] = z[34] + -z[37];
z[41] = abb[28] + -z[15];
z[41] = z[40] * z[41];
z[42] = abb[33] * z[14];
z[43] = -z[29] + -z[42];
z[43] = abb[24] * z[43];
z[31] = z[31] + z[32] + -z[39] + z[41] + z[43];
z[15] = z[7] + -z[15];
z[32] = abb[5] * (T(1) / T(2));
z[41] = abb[50] * z[32];
z[43] = z[15] * z[41];
z[44] = abb[1] + abb[3];
z[3] = -z[3] * z[44];
z[3] = z[3] + z[26];
z[3] = abb[49] * z[3];
z[3] = z[3] + z[11] + (T(1) / T(2)) * z[31] + z[43];
z[3] = m1_set::bc<T>[0] * z[3];
z[11] = abb[40] + abb[41];
z[11] = z[11] * z[32];
z[31] = abb[39] + -abb[40];
z[31] = z[19] * z[31];
z[43] = -abb[6] + -abb[8];
z[43] = z[10] + (T(1) / T(2)) * z[43];
z[43] = abb[40] * z[43];
z[45] = abb[8] + -abb[14];
z[46] = abb[6] + z[45];
z[46] = abb[0] + (T(1) / T(2)) * z[46];
z[46] = abb[39] * z[46];
z[43] = z[11] + -z[31] + z[43] + z[46];
z[43] = abb[50] * z[43];
z[46] = -abb[0] + abb[15];
z[47] = z[13] + z[16] + (T(1) / T(2)) * z[45] + -z[46];
z[48] = abb[50] * z[47];
z[49] = abb[20] * z[14];
z[50] = z[40] + z[49];
z[51] = abb[22] * z[14];
z[51] = z[50] + z[51];
z[52] = abb[16] * abb[52];
z[53] = z[51] + -z[52];
z[46] = abb[2] + -abb[3] + -abb[5] + z[46];
z[54] = -abb[49] * z[46];
z[53] = z[48] + (T(1) / T(2)) * z[53] + z[54];
z[53] = abb[41] * z[53];
z[54] = abb[22] + -abb[24] + z[33];
z[54] = z[14] * z[54];
z[49] = z[49] + z[54];
z[52] = z[34] + -z[49] + z[52];
z[52] = abb[39] * z[52];
z[49] = abb[40] * z[49];
z[49] = z[49] + z[52];
z[3] = z[3] + z[43] + (T(1) / T(2)) * z[49] + z[53];
z[43] = abb[0] * z[24];
z[49] = abb[10] * z[7];
z[43] = z[43] + z[49];
z[4] = -z[4] + z[43];
z[49] = abb[30] + z[1];
z[52] = abb[29] * (T(1) / T(2));
z[53] = abb[32] + z[52];
z[49] = (T(1) / T(2)) * z[49] + -z[53];
z[49] = abb[1] * z[49];
z[54] = abb[7] * (T(1) / T(4));
z[12] = -z[12] * z[54];
z[55] = abb[4] * (T(1) / T(4));
z[56] = -z[7] * z[55];
z[57] = abb[28] + abb[30];
z[58] = -abb[32] + (T(1) / T(2)) * z[57];
z[59] = z[16] * z[58];
z[60] = abb[13] * (T(1) / T(4));
z[18] = -z[18] * z[60];
z[61] = abb[5] * (T(1) / T(4));
z[15] = z[15] * z[61];
z[4] = (T(1) / T(2)) * z[4] + z[12] + z[15] + z[18] + z[49] + z[56] + z[59];
z[4] = m1_set::bc<T>[0] * z[4];
z[12] = -abb[6] + abb[8];
z[9] = -abb[1] + -z[9] + (T(1) / T(2)) * z[12];
z[9] = abb[40] * z[9];
z[12] = abb[39] * (T(1) / T(2));
z[15] = abb[0] + abb[6];
z[15] = z[12] * z[15];
z[18] = -abb[41] * z[13];
z[9] = z[9] + z[11] + z[15] + z[18] + -z[31];
z[4] = z[4] + (T(1) / T(2)) * z[9];
z[4] = abb[48] * z[4];
z[9] = -z[25] + -z[26] + z[43];
z[11] = abb[31] + z[58];
z[15] = abb[33] * (T(1) / T(2));
z[18] = abb[29] * (T(-3) / T(2)) + z[11] + z[15];
z[18] = z[16] * z[18];
z[9] = (T(-1) / T(2)) * z[9] + -z[18] + -z[49];
z[9] = m1_set::bc<T>[0] * z[9];
z[18] = abb[0] + z[45];
z[12] = z[12] * z[18];
z[18] = -abb[40] + abb[41];
z[18] = abb[5] * z[18];
z[25] = abb[1] * abb[40];
z[26] = abb[8] + abb[14];
z[26] = (T(1) / T(2)) * z[26];
z[31] = -abb[15] + abb[3] * (T(3) / T(2)) + z[26];
z[43] = abb[41] * z[31];
z[18] = -z[12] + z[18] + z[25] + z[43];
z[24] = m1_set::bc<T>[0] * z[24];
z[24] = abb[41] + z[24];
z[24] = abb[11] * z[24];
z[9] = z[9] + (T(1) / T(2)) * z[18] + -z[24];
z[18] = abb[46] + abb[47];
z[9] = -z[9] * z[18];
z[25] = -abb[0] + abb[2] + z[16] + z[26];
z[26] = -abb[41] * z[25];
z[43] = abb[0] + -abb[14];
z[45] = -abb[3] + z[43];
z[49] = m1_set::bc<T>[0] * z[23] * z[45];
z[12] = z[12] + z[26] + (T(1) / T(2)) * z[49];
z[12] = (T(1) / T(2)) * z[12] + z[24];
z[12] = abb[51] * z[12];
z[3] = (T(1) / T(2)) * z[3] + z[4] + z[9] + z[12];
z[3] = (T(1) / T(4)) * z[3];
z[4] = -z[20] + -z[28] + -z[30] + z[36] + z[42];
z[4] = abb[33] * z[4];
z[9] = abb[53] + -abb[54];
z[12] = abb[34] + z[9];
z[24] = -z[12] * z[14];
z[26] = -z[1] * z[36];
z[30] = z[29] + -z[36];
z[30] = abb[32] * z[30];
z[29] = -abb[29] * z[29];
z[4] = z[4] + z[24] + z[26] + z[29] + -z[30];
z[4] = abb[24] * z[4];
z[24] = prod_pow(abb[30], 2);
z[26] = z[14] * z[24];
z[29] = abb[28] * (T(1) / T(2));
z[42] = z[29] * z[36];
z[49] = abb[31] * (T(1) / T(2));
z[56] = z[14] * z[49];
z[56] = z[28] + z[56];
z[56] = abb[31] * z[56];
z[26] = (T(1) / T(2)) * z[26] + z[30] + z[42] + z[56];
z[30] = abb[37] + z[9];
z[42] = abb[35] + z[30];
z[59] = z[14] * z[42];
z[59] = z[26] + z[59];
z[62] = z[14] * z[15];
z[62] = -z[20] + z[62];
z[62] = abb[33] * z[62];
z[28] = z[28] + z[36];
z[28] = (T(1) / T(2)) * z[28];
z[17] = z[17] + -z[28];
z[17] = abb[29] * z[17];
z[17] = z[17] + z[59] + z[62];
z[17] = abb[23] * z[17];
z[36] = z[14] * z[52];
z[27] = z[27] + -z[28] + z[36];
z[27] = abb[29] * z[27];
z[28] = z[27] + z[59];
z[28] = abb[20] * z[28];
z[36] = -abb[55] + z[42];
z[36] = z[14] * z[36];
z[26] = z[26] + z[27] + z[36];
z[26] = abb[22] * z[26];
z[27] = prod_pow(abb[28], 2);
z[36] = (T(1) / T(2)) * z[27];
z[30] = -abb[34] + z[30] + -z[36];
z[30] = z[14] * z[30];
z[42] = abb[32] * (T(1) / T(2));
z[59] = z[14] * z[42];
z[20] = -z[20] + z[59];
z[20] = abb[32] * z[20];
z[20] = z[20] + z[30] + z[56] + z[62];
z[20] = abb[21] * z[20];
z[30] = (T(1) / T(2)) * z[34] + -z[37];
z[30] = abb[28] * z[30];
z[37] = z[40] * z[52];
z[30] = z[30] + (T(-1) / T(2)) * z[35] + z[37] + z[38];
z[30] = abb[29] * z[30];
z[37] = abb[28] * z[34];
z[35] = -z[35] + z[37];
z[35] = z[29] * z[35];
z[37] = -abb[28] + -abb[29] + z[15];
z[37] = z[37] * z[40];
z[37] = z[37] + z[39];
z[37] = abb[33] * z[37];
z[34] = -abb[53] * z[34];
z[38] = -abb[55] * z[50];
z[39] = z[29] + -z[52];
z[39] = z[21] * z[39];
z[39] = -abb[53] + abb[55] + z[39];
z[39] = abb[16] * abb[52] * z[39];
z[40] = abb[0] + abb[8];
z[50] = -abb[14] + -z[40];
z[50] = abb[52] * z[50];
z[56] = -abb[26] * z[14];
z[50] = z[50] + z[56];
z[50] = abb[38] * z[50];
z[33] = abb[24] + z[33];
z[14] = abb[36] * z[14] * z[33];
z[4] = z[4] + z[14] + z[17] + z[20] + z[26] + z[28] + z[30] + z[34] + z[35] + z[37] + z[38] + z[39] + (T(1) / T(2)) * z[50];
z[14] = z[6] * z[42];
z[17] = abb[9] * abb[28];
z[20] = abb[31] * z[6];
z[14] = z[14] + z[17] + -z[20];
z[14] = abb[32] * z[14];
z[5] = abb[9] + z[5];
z[5] = z[5] * z[27];
z[9] = abb[6] * z[9];
z[5] = z[5] + -z[9];
z[9] = abb[9] * abb[32];
z[9] = z[9] + -z[17];
z[9] = abb[29] * z[9];
z[17] = z[6] * z[15];
z[17] = z[17] + -z[20];
z[17] = abb[33] * z[17];
z[20] = z[6] * z[49];
z[8] = z[8] + z[20];
z[8] = abb[31] * z[8];
z[20] = abb[37] * z[6];
z[5] = (T(-1) / T(2)) * z[5] + z[8] + -z[9] + z[14] + z[17] + z[20];
z[8] = abb[28] * abb[30];
z[9] = abb[29] * z[2];
z[14] = abb[31] + z[23];
z[14] = abb[33] * z[14];
z[17] = -abb[36] + z[14];
z[20] = abb[30] + z[2];
z[20] = abb[32] * z[20];
z[23] = abb[30] * abb[31];
z[8] = z[8] + -z[9] + z[12] + z[17] + -z[20] + z[23];
z[9] = z[8] * z[19];
z[12] = -z[42] + z[57];
z[12] = abb[32] * z[12];
z[19] = z[29] * z[57];
z[26] = abb[29] * z[58];
z[24] = abb[35] + (T(1) / T(2)) * z[24];
z[12] = abb[34] + -z[12] + z[19] + z[24] + -z[26];
z[12] = abb[12] * z[12];
z[19] = abb[3] + -abb[6];
z[19] = abb[34] * z[19];
z[12] = z[12] + z[19];
z[19] = z[22] * z[52];
z[26] = z[21] * z[29];
z[26] = -abb[53] + z[26];
z[22] = z[15] + -z[22];
z[27] = abb[33] * z[22];
z[28] = z[19] + z[26] + z[27];
z[30] = abb[14] * z[28];
z[33] = z[12] + z[30];
z[34] = 3 * abb[28];
z[35] = abb[30] + -z[34];
z[35] = -abb[31] + (T(1) / T(2)) * z[35] + z[53];
z[35] = abb[29] * z[35];
z[37] = -abb[54] + z[24];
z[38] = abb[28] + z[49];
z[39] = abb[31] * z[38];
z[39] = abb[37] + z[39];
z[50] = abb[30] * (T(1) / T(2));
z[53] = abb[28] + -z[50];
z[53] = abb[28] * z[53];
z[56] = -abb[31] + -z[21];
z[56] = abb[32] * z[56];
z[35] = -abb[53] + z[35] + -z[37] + z[39] + z[53] + z[56];
z[0] = z[0] * z[35];
z[35] = z[21] + z[49];
z[35] = abb[31] * z[35];
z[53] = -abb[30] * z[29];
z[14] = abb[37] + -z[14] + z[19] + z[24] + z[35] + z[53];
z[14] = z[14] * z[16];
z[19] = abb[0] * z[28];
z[28] = -abb[55] * z[47];
z[35] = -abb[31] + z[42];
z[47] = abb[32] * z[35];
z[53] = abb[54] + z[24];
z[23] = z[23] + z[47] + -z[53];
z[10] = z[10] * z[23];
z[1] = -z[1] + z[42];
z[1] = abb[32] * z[1];
z[39] = z[36] + z[39];
z[47] = z[1] + z[39];
z[2] = z[2] + -z[52];
z[56] = abb[29] * z[2];
z[57] = z[47] + -z[56];
z[58] = abb[15] * z[57];
z[59] = -abb[31] + z[52];
z[59] = abb[29] * z[59];
z[62] = -abb[31] + z[15];
z[63] = abb[33] * z[62];
z[59] = z[59] + -z[63];
z[63] = z[13] * z[59];
z[64] = abb[17] + abb[18];
z[65] = abb[16] + -abb[19];
z[66] = z[64] + -z[65];
z[66] = abb[38] * z[66];
z[67] = z[13] + -z[16];
z[68] = -z[6] + -z[67];
z[68] = abb[36] * z[68];
z[0] = z[0] + -z[5] + z[9] + z[10] + z[14] + z[19] + z[28] + (T(-1) / T(2)) * z[33] + -z[58] + z[63] + (T(1) / T(4)) * z[66] + z[68];
z[0] = abb[50] * z[0];
z[9] = z[44] * z[57];
z[10] = abb[55] * z[46];
z[9] = z[9] + z[10] + -z[58];
z[9] = abb[49] * z[9];
z[10] = -abb[1] + abb[15];
z[14] = -abb[3] + z[10];
z[14] = abb[49] * z[14];
z[14] = z[14] + -z[41] + -z[48] + (T(-1) / T(2)) * z[51];
z[28] = prod_pow(m1_set::bc<T>[0], 2);
z[14] = z[14] * z[28];
z[20] = z[20] + -z[37] + -z[39];
z[7] = z[7] + z[52];
z[7] = abb[29] * z[7];
z[7] = -abb[55] + z[7] + -z[17] + -z[20];
z[17] = z[7] * z[41];
z[33] = abb[27] * abb[38] * abb[52];
z[0] = z[0] + (T(1) / T(2)) * z[4] + z[9] + (T(1) / T(6)) * z[14] + z[17] + z[33];
z[4] = -z[6] + z[67];
z[4] = abb[36] * z[4];
z[4] = -z[4] + z[5];
z[5] = z[8] * z[60];
z[6] = abb[30] + z[38];
z[6] = abb[31] * z[6];
z[8] = z[6] + z[36] + -z[53];
z[9] = -z[29] + z[35];
z[9] = abb[32] * z[9];
z[2] = z[2] * z[52];
z[14] = abb[37] * (T(1) / T(2));
z[2] = -z[2] + (T(1) / T(2)) * z[8] + z[9] + z[14];
z[2] = abb[1] * z[2];
z[8] = -abb[30] + abb[31];
z[8] = z[8] * z[42];
z[9] = abb[30] * z[49];
z[8] = z[8] + -z[9] + z[24];
z[8] = abb[10] * z[8];
z[2] = z[2] + z[8] + (T(-1) / T(4)) * z[19];
z[8] = -abb[28] + -z[50];
z[8] = abb[28] * z[8];
z[6] = -z[6] + z[8] + z[24];
z[8] = -z[15] * z[62];
z[9] = abb[30] + z[34];
z[9] = -abb[32] + (T(1) / T(4)) * z[9];
z[9] = abb[29] * z[9];
z[1] = -z[1] + (T(1) / T(2)) * z[6] + z[8] + z[9] + -z[14];
z[1] = z[1] * z[16];
z[6] = -z[20] + -z[56];
z[8] = abb[8] * (T(1) / T(4));
z[6] = z[6] * z[8];
z[9] = -z[23] * z[55];
z[7] = z[7] * z[61];
z[13] = abb[1] + z[13] + -z[32] + (T(-1) / T(2)) * z[40];
z[14] = (T(1) / T(12)) * z[28];
z[13] = z[13] * z[14];
z[17] = abb[55] + -z[59];
z[17] = z[17] * z[54];
z[20] = abb[38] * z[64];
z[1] = z[1] + -z[2] + (T(-1) / T(2)) * z[4] + z[5] + z[6] + z[7] + z[9] + (T(-1) / T(4)) * z[12] + z[13] + z[17] + (T(1) / T(8)) * z[20];
z[1] = abb[48] * z[1];
z[4] = abb[55] * z[31];
z[4] = z[4] + z[58];
z[5] = z[15] * z[22];
z[6] = abb[29] * (T(3) / T(4)) + -z[11];
z[6] = abb[29] * z[6];
z[5] = z[5] + z[6] + z[47];
z[5] = z[5] * z[16];
z[6] = z[21] * z[52];
z[6] = z[6] + -z[26];
z[7] = z[6] * z[8];
z[8] = z[21] + z[52];
z[8] = abb[29] * z[8];
z[8] = z[8] + z[27];
z[9] = -abb[55] + (T(1) / T(2)) * z[8] + -z[14];
z[9] = abb[11] * z[9];
z[10] = z[10] + (T(1) / T(2)) * z[43];
z[10] = (T(1) / T(3)) * z[10] + -z[16];
z[11] = (T(1) / T(4)) * z[28];
z[10] = z[10] * z[11];
z[11] = -abb[30] + z[42];
z[11] = abb[32] * z[11];
z[11] = abb[55] + z[11] + z[37];
z[11] = z[11] * z[32];
z[12] = abb[38] * z[65];
z[2] = z[2] + (T(-1) / T(2)) * z[4] + z[5] + z[7] + -z[9] + z[10] + -z[11] + (T(1) / T(8)) * z[12] + (T(1) / T(4)) * z[30];
z[2] = -z[2] * z[18];
z[4] = -abb[8] * z[6];
z[5] = -abb[3] * z[8];
z[4] = z[4] + z[5] + z[19] + -z[30];
z[5] = abb[55] * z[25];
z[6] = -z[14] * z[45];
z[4] = (T(1) / T(2)) * z[4] + z[5] + z[6] + (T(-1) / T(4)) * z[12];
z[4] = (T(1) / T(2)) * z[4] + z[9];
z[4] = abb[51] * z[4];
z[0] = (T(1) / T(2)) * z[0] + z[1] + z[2] + z[4];
z[0] = (T(1) / T(4)) * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_131_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.15544607849632252103623350723968708096710118774860114818442850773"),stof<T>("-0.27679292663235168124861761364236671935592401363901717350041320928")}, std::complex<T>{stof<T>("-0.15544607849632252103623350723968708096710118774860114818442850773"),stof<T>("-0.27679292663235168124861761364236671935592401363901717350041320928")}, std::complex<T>{stof<T>("0.00722615017167496324048137642896440305808563586611877677030526573"),stof<T>("-0.133957808662581341355372377224295301428277861762339930663699188624")}, std::complex<T>{stof<T>("-0.03821248390872231344252662188781923345220967043504184902307007321"),stof<T>("0.51967346510026199823181284194628829558252737077167126374593754432")}, std::complex<T>{stof<T>("0.21048062489256053764196837293710010686895007511718671115592204823"),stof<T>("-0.04194482283851053381871003559965396676814852410640021842251567236")}, std::complex<T>{stof<T>("-0.032870383003594372288837417966425626302093458254634235113250177528"),stof<T>("0.060747135561956564218717333797977185097328502954697283075871499618")}, std::complex<T>{stof<T>("-0.25186751860815181882323830401799984856501583090687365866823217018"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_131_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_131_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.29125453957040087051919869849396333399652537439058339158447977451"),stof<T>("0.03068996749074059118317976479273059248876755809690744889799906302")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W77(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k)};

                    
            return f_4_131_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_131_DLogXconstant_part(base_point<T>, kend);
	value += f_4_131_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_131_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_131_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_131_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_131_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_131_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_131_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
