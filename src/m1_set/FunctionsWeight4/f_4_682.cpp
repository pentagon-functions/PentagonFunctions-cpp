/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_682.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_682_abbreviated (const std::array<T,41>& abb) {
T z[139];
z[0] = 3 * abb[25];
z[1] = 2 * abb[27];
z[2] = z[0] + z[1];
z[3] = 3 * abb[32];
z[4] = z[2] + -z[3];
z[5] = 12 * abb[26];
z[6] = 6 * abb[28];
z[7] = -abb[33] + z[6];
z[8] = z[5] + z[7];
z[9] = 5 * abb[29];
z[10] = 4 * abb[31];
z[11] = z[4] + z[8] + -z[9] + -z[10];
z[11] = abb[9] * z[11];
z[12] = abb[34] + -abb[35];
z[13] = 2 * z[12];
z[14] = abb[13] * z[13];
z[15] = z[11] + -z[14];
z[16] = 8 * abb[29];
z[17] = 2 * abb[25];
z[18] = z[16] + -z[17];
z[19] = 3 * abb[31];
z[20] = -abb[33] + z[19];
z[21] = 2 * abb[28];
z[22] = -abb[27] + -z[3] + z[18] + z[20] + z[21];
z[22] = abb[4] * z[22];
z[23] = 5 * abb[32];
z[24] = z[1] + z[23];
z[25] = 17 * abb[26];
z[26] = 2 * abb[33];
z[27] = -8 * abb[28] + -abb[31] + z[17] + z[24] + -z[25] + z[26];
z[27] = abb[8] * z[27];
z[28] = 4 * abb[28];
z[29] = -abb[33] + z[28];
z[30] = 2 * abb[31];
z[31] = 6 * abb[26];
z[4] = abb[29] + -z[4] + -z[29] + z[30] + -z[31];
z[4] = abb[0] * z[4];
z[32] = 4 * abb[29];
z[33] = -abb[25] + abb[27];
z[34] = z[32] + z[33];
z[35] = -z[3] + z[10];
z[36] = -z[31] + z[34] + -z[35];
z[37] = abb[1] * z[36];
z[38] = 2 * abb[32];
z[39] = -z[1] + z[38];
z[40] = 5 * abb[26];
z[41] = -z[0] + -z[39] + z[40];
z[41] = abb[3] * z[41];
z[42] = abb[27] + -abb[32];
z[43] = abb[29] + z[42];
z[44] = abb[7] * z[43];
z[45] = -abb[25] + abb[32];
z[46] = -abb[27] + z[45];
z[47] = abb[2] * z[46];
z[48] = z[44] + -z[47];
z[48] = 3 * z[48];
z[49] = abb[11] * z[12];
z[4] = z[4] + z[15] + z[22] + z[27] + z[37] + z[41] + -z[48] + z[49];
z[4] = prod_pow(abb[16], 2) * z[4];
z[22] = z[6] + z[31];
z[27] = 9 * abb[31];
z[41] = -z[22] + z[27];
z[50] = 4 * abb[25];
z[51] = abb[30] + -abb[32];
z[52] = 3 * abb[27];
z[53] = z[9] + z[41] + -z[50] + z[51] + -z[52];
z[54] = -abb[9] * z[53];
z[55] = abb[30] + z[1];
z[56] = -8 * abb[25] + z[3] + z[9] + -z[22] + z[55];
z[56] = abb[0] * z[56];
z[57] = -z[17] + z[38];
z[41] = abb[30] + -z[1] + z[41] + -z[57];
z[41] = abb[3] * z[41];
z[58] = abb[12] * z[12];
z[59] = abb[13] * z[12];
z[60] = z[58] + z[59];
z[61] = 10 * abb[32];
z[62] = 10 * abb[25] + abb[27] + z[27] + -z[61];
z[62] = abb[6] * z[62];
z[63] = abb[4] * z[46];
z[41] = z[41] + 5 * z[44] + 12 * z[47] + z[54] + z[56] + -z[60] + z[62] + 6 * z[63];
z[41] = abb[15] * z[41];
z[54] = 2 * abb[29];
z[56] = z[3] + z[54];
z[62] = 5 * abb[27];
z[64] = 8 * abb[31];
z[65] = 5 * abb[25];
z[66] = z[22] + -z[56] + z[62] + -z[64] + z[65];
z[66] = abb[9] * z[66];
z[67] = z[23] + z[54];
z[68] = 7 * abb[25];
z[69] = abb[27] + -z[22] + z[67] + -z[68];
z[69] = abb[0] * z[69];
z[70] = abb[27] + z[45];
z[64] = z[64] + -z[70];
z[71] = -z[22] + z[64];
z[71] = abb[3] * z[71];
z[72] = 6 * z[47];
z[73] = z[44] + z[72];
z[63] = 3 * z[63] + z[73];
z[74] = 8 * abb[6];
z[75] = -abb[31] + z[45];
z[74] = z[74] * z[75];
z[76] = z[14] + z[74];
z[77] = abb[12] * z[13];
z[63] = 2 * z[63] + z[66] + z[69] + z[71] + -z[76] + -z[77];
z[66] = -abb[16] * z[63];
z[41] = z[41] + z[66];
z[41] = abb[15] * z[41];
z[66] = 7 * abb[29];
z[69] = 15 * abb[25];
z[71] = 4 * abb[32];
z[78] = z[1] + z[26] + -z[31] + -z[66] + z[69] + -z[71];
z[78] = abb[0] * z[78];
z[79] = abb[31] + abb[33];
z[80] = 4 * z[79];
z[81] = 4 * abb[27] + z[80];
z[82] = 12 * abb[29];
z[83] = 22 * abb[28];
z[61] = 53 * abb[26] + -z[61] + -z[65] + -z[81] + -z[82] + z[83];
z[61] = abb[1] * z[61];
z[84] = 6 * abb[29];
z[85] = 10 * abb[26];
z[86] = z[84] + -z[85];
z[87] = abb[32] + z[30];
z[88] = -z[17] + -z[29] + z[86] + z[87];
z[88] = abb[9] * z[88];
z[89] = abb[27] + z[30];
z[90] = -11 * abb[26] + z[18] + -z[29] + z[89];
z[90] = abb[4] * z[90];
z[91] = -abb[25] + z[42] + z[54];
z[91] = abb[10] * z[91];
z[90] = -z[88] + z[90] + -z[91];
z[92] = -z[3] + z[17];
z[93] = 3 * abb[29];
z[92] = z[6] + z[40] + 2 * z[92] + z[93];
z[92] = abb[5] * z[92];
z[94] = -abb[25] + abb[26];
z[95] = 2 * abb[3];
z[96] = z[94] * z[95];
z[97] = abb[11] * z[13];
z[61] = z[14] + z[61] + z[78] + -2 * z[90] + -z[92] + -z[96] + z[97];
z[78] = abb[36] * z[61];
z[90] = 2 * abb[9];
z[53] = z[53] * z[90];
z[92] = -z[1] + z[9];
z[96] = z[30] + -z[65] + z[92];
z[98] = 2 * abb[4];
z[96] = z[96] * z[98];
z[99] = 10 * abb[28] + z[85];
z[100] = 4 * abb[30];
z[64] = -z[64] + z[99] + -z[100];
z[64] = abb[8] * z[64];
z[24] = 7 * abb[31] + -z[24];
z[101] = z[24] + z[65];
z[101] = abb[6] * z[101];
z[53] = 9 * z[47] + -z[53] + -z[64] + z[96] + z[101];
z[96] = -abb[37] * z[53];
z[101] = 2 * abb[30];
z[102] = z[52] + z[101];
z[103] = 6 * abb[32];
z[104] = 16 * abb[25] + -z[103];
z[105] = 12 * abb[28];
z[106] = 10 * abb[29];
z[107] = -abb[31] + z[5] + -z[102] + z[104] + z[105] + -z[106];
z[108] = abb[37] * z[107];
z[12] = abb[19] * z[12];
z[108] = -z[12] + z[108];
z[108] = abb[0] * z[108];
z[109] = z[10] + z[31];
z[110] = abb[33] + z[17] + z[52] + z[93] + -z[109];
z[110] = abb[19] * z[110];
z[13] = abb[37] * z[13];
z[110] = z[13] + z[110];
z[110] = abb[13] * z[110];
z[111] = 3 * abb[26];
z[112] = -abb[33] + -z[38] + z[89] + z[111];
z[112] = abb[12] * abb[19] * z[112];
z[113] = 6 * abb[31];
z[114] = -z[70] + z[113];
z[115] = z[21] + z[101];
z[116] = 2 * abb[26];
z[117] = z[114] + -z[115] + -z[116];
z[118] = -abb[37] * z[117];
z[12] = z[12] + z[118];
z[12] = abb[3] * z[12];
z[118] = abb[25] + z[35];
z[119] = -abb[33] + z[93];
z[120] = z[118] + z[119];
z[120] = abb[19] * z[120];
z[13] = -z[13] + z[120];
z[13] = abb[11] * z[13];
z[4] = z[4] + z[12] + z[13] + z[41] + z[78] + z[96] + z[108] + z[110] + 2 * z[112];
z[12] = -z[10] + z[29] + z[40] + z[45];
z[12] = abb[8] * z[12];
z[13] = z[1] + -z[29] + -z[93] + z[118];
z[13] = abb[4] * z[13];
z[41] = abb[27] + z[21];
z[78] = -z[30] + z[41] + z[119];
z[78] = abb[0] * z[78];
z[96] = 2 * abb[1];
z[108] = -z[36] * z[96];
z[110] = 3 * z[47];
z[112] = z[44] + z[110];
z[118] = -z[58] + 2 * z[112];
z[119] = z[21] + -z[30];
z[120] = z[116] + z[119];
z[46] = -z[46] + z[120];
z[46] = abb[3] * z[46];
z[13] = 2 * z[12] + z[13] + -z[15] + z[46] + z[78] + z[108] + -z[118];
z[13] = abb[16] * z[13];
z[46] = 18 * abb[26] + z[6];
z[78] = z[3] + z[16] + z[26] + z[33] + -z[46];
z[78] = abb[9] * z[78];
z[108] = 9 * abb[25];
z[119] = z[32] + -z[42] + -z[108] + z[116] + -z[119];
z[119] = abb[0] * z[119];
z[121] = 8 * abb[32];
z[122] = 5 * abb[31];
z[123] = 21 * abb[26] + -z[1] + z[7] + -z[84] + -z[121] + z[122];
z[124] = z[96] * z[123];
z[36] = abb[4] * z[36];
z[36] = -z[12] + z[36];
z[125] = abb[25] + abb[27];
z[126] = z[21] + -z[125];
z[127] = 4 * abb[26];
z[128] = -abb[32] + z[127];
z[129] = z[126] + z[128];
z[129] = abb[3] * z[129];
z[130] = -z[14] + z[77];
z[129] = z[129] + z[130];
z[131] = -z[0] + -z[28] + z[71];
z[132] = -z[40] + z[131];
z[133] = 2 * abb[5];
z[134] = z[132] * z[133];
z[36] = -2 * z[36] + z[78] + -z[119] + z[124] + -z[129] + z[134];
z[78] = -abb[18] * z[36];
z[63] = abb[15] * z[63];
z[41] = -z[31] + z[32] + -z[41] + 3 * z[45];
z[119] = -abb[0] * z[41];
z[124] = z[35] + -z[126];
z[124] = abb[3] * z[124];
z[126] = abb[28] + -z[33];
z[126] = z[98] * z[126];
z[37] = z[37] + z[110] + z[119] + z[124] + z[126];
z[37] = abb[17] * z[37];
z[13] = z[13] + z[37] + z[63] + z[78];
z[37] = 2 * abb[17];
z[13] = z[13] * z[37];
z[35] = -z[21] + z[35] + z[101] + z[116] + -z[125];
z[35] = abb[0] * z[35];
z[42] = -27 * abb[26] + 5 * abb[30] + 15 * abb[31] + z[26] + z[42] + z[50] + -z[83];
z[42] = abb[8] * z[42];
z[50] = 11 * abb[32];
z[78] = -z[0] + z[50];
z[83] = -abb[33] + z[113];
z[119] = -28 * abb[26] + 16 * abb[29] + z[52] + z[78] + -2 * z[83] + -z[115];
z[119] = abb[1] * z[119];
z[16] = abb[27] + z[16];
z[124] = abb[26] + z[3] + -z[16] + -z[28] + z[30];
z[124] = z[98] * z[124];
z[8] = z[8] + -z[18] + -z[51] + -z[122];
z[8] = z[8] * z[90];
z[18] = 7 * abb[32];
z[90] = z[18] + -z[115];
z[89] = -z[68] + -z[89] + z[90] + -z[116];
z[126] = abb[6] * z[89];
z[135] = abb[25] + abb[31];
z[136] = 7 * abb[26];
z[137] = -abb[30] + z[28];
z[138] = 3 * z[135] + -z[136] + -z[137];
z[138] = abb[3] * z[138];
z[8] = z[8] + z[35] + z[42] + z[97] + z[119] + -z[124] + z[126] + z[130] + -z[138];
z[8] = 2 * z[8];
z[35] = -abb[39] * z[8];
z[42] = -z[28] + 2 * z[79];
z[79] = 8 * abb[26];
z[119] = abb[27] + z[101];
z[124] = -z[42] + z[56] + -z[68] + z[79] + -z[119];
z[124] = abb[0] * z[124];
z[71] = -abb[25] + z[71];
z[25] = z[25] + z[32] + -3 * z[71] + z[105];
z[25] = abb[5] * z[25];
z[71] = 14 * abb[26];
z[20] = -2 * z[20] + z[34] + -z[71] + z[90];
z[20] = abb[9] * z[20];
z[7] = 15 * abb[26] + abb[31] + z[7] + -z[67] + -z[125];
z[34] = abb[4] * z[7];
z[67] = abb[25] + z[87];
z[87] = -z[31] + z[67] + -z[137];
z[87] = abb[8] * z[87];
z[34] = z[34] + z[87];
z[81] = 69 * abb[26] + 30 * abb[28] + -18 * abb[32] + -z[68] + -z[81] + -z[106];
z[81] = abb[1] * z[81];
z[20] = z[20] + -z[25] + 2 * z[34] + z[81] + -z[124] + -z[129];
z[20] = abb[18] * z[20];
z[20] = z[20] + -z[63];
z[25] = z[31] + z[83] + z[93] + z[119] + -z[121];
z[25] = abb[9] * z[25];
z[34] = abb[33] + z[68];
z[63] = abb[29] + z[23] + -z[30] + -z[34] + -z[101] + z[116];
z[63] = abb[0] * z[63];
z[81] = z[7] * z[96];
z[81] = z[81] + z[134];
z[76] = z[76] + z[81];
z[83] = z[2] + z[29];
z[90] = 15 * abb[32];
z[9] = -10 * abb[31] + -z[5] + -z[9] + -z[83] + z[90];
z[9] = abb[4] * z[9];
z[96] = z[21] + z[33];
z[105] = z[19] + -z[96] + -z[111];
z[105] = z[95] * z[105];
z[9] = z[9] + z[25] + z[63] + -z[76] + -2 * z[87] + -z[97] + z[105] + z[118];
z[9] = abb[16] * z[9];
z[25] = -9 * abb[32] + z[5] + z[10] + z[29] + z[68] + -z[92];
z[25] = abb[4] * z[25];
z[63] = 9 * abb[29];
z[68] = -abb[27] + abb[33] + z[6] + -z[63] + z[104] + z[127];
z[68] = abb[0] * z[68];
z[92] = -z[3] + z[113];
z[96] = -z[92] + z[96];
z[96] = abb[3] * z[96];
z[11] = -z[11] + z[25] + -z[58] + z[68] + -z[72] + z[76] + z[96];
z[11] = abb[17] * z[11];
z[9] = z[9] + z[11] + z[20];
z[11] = 9 * abb[26] + z[29];
z[25] = -z[11] + z[30] + z[38] + z[93] + -z[125];
z[25] = abb[4] * z[25];
z[29] = abb[30] + -abb[31] + -z[23] + z[83] + z[85] + -z[93];
z[29] = abb[9] * z[29];
z[25] = z[25] + z[29] + z[110];
z[29] = -abb[31] + abb[33];
z[38] = 39 * abb[26];
z[2] = -18 * abb[28] + 16 * abb[32] + z[2] + 4 * z[29] + -z[38] + -z[54];
z[2] = abb[1] * z[2];
z[58] = -z[40] + z[106] + -z[131];
z[58] = abb[5] * z[58];
z[57] = -abb[27] + -abb[31] + z[57];
z[68] = 2 * abb[6];
z[57] = z[57] * z[68];
z[72] = 4 * z[59];
z[2] = z[2] + z[57] + z[58] + -z[72];
z[57] = abb[29] + z[111];
z[51] = -abb[28] + -abb[33] + z[1] + -z[51] + z[57] + -z[65];
z[58] = 2 * abb[0];
z[51] = z[51] * z[58];
z[55] = z[19] + -z[55] + -z[94];
z[55] = z[55] * z[95];
z[25] = z[2] + 2 * z[25] + z[51] + z[55] + z[77] + z[97];
z[25] = abb[14] * z[25];
z[9] = 2 * z[9] + z[25];
z[9] = abb[14] * z[9];
z[17] = -z[17] + z[19];
z[17] = -2 * z[17] + z[28] + z[52] + -z[93] + z[128];
z[17] = abb[9] * z[17];
z[19] = abb[25] + -z[32] + z[52] + -z[92];
z[19] = abb[4] * z[19];
z[25] = -z[28] + z[114] + -z[127];
z[25] = abb[3] * z[25];
z[28] = abb[26] + abb[28];
z[51] = -z[28] + z[45];
z[51] = abb[5] * z[51];
z[43] = abb[0] * z[43];
z[17] = -z[17] + z[19] + -z[25] + -z[43] + z[49] + -4 * z[51] + z[60] + -z[73] + z[91];
z[17] = 4 * z[17];
z[19] = abb[38] * z[17];
z[25] = 18 * abb[31] + -z[18] + -z[52] + -z[65] + z[82] + -z[99] + z[101];
z[25] = abb[9] * z[25];
z[32] = z[22] + z[30] + -z[32] + -z[50] + z[69] + z[119];
z[32] = abb[0] * z[32];
z[16] = z[10] + z[16] + z[22] + -z[78];
z[16] = abb[4] * z[16];
z[16] = z[16] + -z[64] + -z[91];
z[22] = z[68] * z[89];
z[43] = z[70] + -z[101] + z[120];
z[43] = abb[3] * z[43];
z[49] = -3 * abb[28] + abb[30] + abb[31] + abb[32] + -z[57];
z[55] = 4 * abb[1];
z[49] = z[49] * z[55];
z[16] = 2 * z[16] + z[22] + -z[25] + z[32] + z[43] + -z[49] + 16 * z[51];
z[22] = -abb[40] * z[16];
z[25] = abb[16] * z[36];
z[32] = -abb[4] * z[123];
z[27] = -z[1] + -z[18] + z[27] + z[136];
z[27] = abb[8] * z[27];
z[27] = z[27] + z[32] + -z[88];
z[32] = 20 * abb[32];
z[36] = 20 * abb[28] + z[0] + -z[32] + z[38] + -z[54];
z[36] = abb[5] * z[36];
z[34] = abb[28] + -z[34] + z[39] + z[40] + z[54];
z[34] = z[34] * z[58];
z[38] = -111 * abb[26] + 8 * abb[27] + -42 * abb[28] + 30 * abb[29] + 26 * abb[32] + z[80] + z[108];
z[38] = abb[1] * z[38];
z[40] = abb[28] + z[116] + -z[135];
z[40] = abb[3] * z[40];
z[27] = 2 * z[27] + z[34] + z[36] + z[38] + 4 * z[40] + z[130];
z[27] = abb[18] * z[27];
z[25] = 2 * z[25] + z[27];
z[25] = abb[18] * z[25];
z[4] = 2 * z[4] + z[9] + z[13] + z[19] + z[22] + z[25] + z[35];
z[9] = 40 * abb[28] + z[26] + -z[32] + z[65] + z[122];
z[9] = -9 * abb[30] + abb[26] * (T(53) / T(3)) + (T(1) / T(3)) * z[9];
z[9] = abb[8] * z[9];
z[13] = -abb[29] + abb[33] + z[122];
z[1] = -abb[25] + abb[26] * (T(-10) / T(3)) + abb[30] * (T(-5) / T(3)) + abb[28] * (T(-4) / T(3)) + -z[1] + z[3] + (T(1) / T(3)) * z[13];
z[1] = abb[9] * z[1];
z[13] = abb[30] + z[28];
z[19] = 35 * abb[31] + -94 * z[45];
z[13] = (T(22) / T(3)) * z[13] + (T(1) / T(3)) * z[19] + z[62];
z[13] = z[13] * z[68];
z[19] = -25 * abb[31] + -z[26];
z[19] = -77 * abb[25] + 2 * z[19];
z[19] = 36 * abb[32] + abb[28] * (T(-49) / T(3)) + abb[29] * (T(-34) / T(3)) + abb[27] * (T(-5) / T(3)) + (T(1) / T(3)) * z[19] + -z[71];
z[19] = z[19] * z[98];
z[22] = 83 * abb[25] + -82 * abb[32];
z[22] = 36 * abb[26] + abb[28] * (T(82) / T(3)) + (T(1) / T(3)) * z[22] + -z[63];
z[22] = z[22] * z[133];
z[25] = -abb[33] + z[122];
z[3] = -22 * abb[26] + -28 * abb[28] + abb[25] * (T(-13) / T(3)) + abb[27] * (T(1) / T(3)) + abb[29] * (T(16) / T(3)) + abb[30] * (T(44) / T(3)) + -z[3] + 4 * z[25];
z[3] = abb[1] * z[3];
z[25] = -abb[25] + -z[30];
z[18] = 10 * abb[30] + abb[27] * (T(-31) / T(3)) + abb[26] * (T(-26) / T(3)) + abb[28] * (T(-10) / T(3)) + z[18] + (T(5) / T(3)) * z[25];
z[18] = abb[3] * z[18];
z[25] = 4 * abb[33] + z[122];
z[25] = -295 * abb[25] + 145 * abb[32] + 2 * z[25];
z[25] = -7 * abb[27] + -54 * abb[28] + 58 * abb[29] + abb[26] * (T(-194) / T(3)) + abb[30] * (T(20) / T(3)) + (T(1) / T(3)) * z[25];
z[25] = abb[0] * z[25];
z[1] = 4 * z[1] + z[3] + 2 * z[9] + z[13] + z[18] + z[19] + z[22] + z[25] + (T(40) / T(3)) * z[44] + z[47] + (T(8) / T(3)) * z[59] + (T(16) / T(3)) * z[91];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[1] = z[1] + 2 * z[4];
z[1] = 4 * z[1];
z[3] = -abb[28] + -z[0] + -z[30] + z[56] + -z[111];
z[3] = z[3] * z[98];
z[4] = -11 * abb[25] + -abb[27] + abb[31] + -z[6] + z[66] + -z[79] + z[103];
z[4] = abb[0] * z[4];
z[9] = -abb[5] * z[132];
z[7] = -abb[1] * z[7];
z[13] = -abb[25] + -abb[26] + z[39];
z[13] = abb[3] * z[13];
z[18] = abb[6] * z[75];
z[3] = z[3] + z[4] + z[7] + z[9] + -z[12] + z[13] + z[15] + -4 * z[18] + z[112];
z[3] = z[3] * z[37];
z[4] = 2 * z[29] + -z[46] + z[54] + z[78] + -z[102];
z[4] = abb[9] * z[4];
z[7] = -abb[4] * z[41];
z[9] = abb[30] + -abb[33] + z[24] + z[31];
z[9] = abb[8] * z[9];
z[7] = z[7] + z[9] + z[48];
z[6] = 13 * abb[25] + z[6] + -z[50] + -z[86] + z[102];
z[6] = abb[0] * z[6];
z[0] = z[0] + z[21];
z[9] = z[0] + z[23] + -z[52] + -z[109];
z[9] = abb[3] * z[9];
z[4] = z[4] + z[6] + 2 * z[7] + z[9] + z[72] + z[74] + z[81];
z[4] = abb[16] * z[4];
z[6] = -abb[33] + z[10];
z[0] = -z[0] + -2 * z[6] + -z[52] + -z[54] + -z[71] + z[90] + -z[100];
z[0] = abb[9] * z[0];
z[5] = -z[5] + z[42] + -z[45] + -z[52] + z[84] + z[100];
z[5] = abb[0] * z[5];
z[6] = abb[31] + z[11] + -z[23] + z[33] + z[54];
z[6] = abb[4] * z[6];
z[6] = z[6] + z[87] + -z[112];
z[7] = z[62] + -3 * z[67] + z[79] + z[115];
z[7] = abb[3] * z[7];
z[0] = z[0] + -z[2] + z[5] + 2 * z[6] + z[7];
z[0] = abb[14] * z[0];
z[0] = z[0] + z[3] + z[4] + -z[20];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[20] * z[61];
z[3] = abb[0] * z[107];
z[4] = -abb[3] * z[117];
z[3] = z[3] + z[4] + z[14] + -z[53] + -z[97];
z[3] = abb[21] * z[3];
z[0] = z[0] + z[2] + z[3];
z[2] = -abb[23] * z[8];
z[3] = abb[22] * z[17];
z[4] = -abb[24] * z[16];
z[0] = 2 * z[0] + z[2] + z[3] + z[4];
z[0] = 8 * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_682_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("637.0761889038387725731974651331162271310219419679747166480820512"),stof<T>("4031.1781285682026535995632717109914662445940241837646947072174181")}, std::complex<T>{stof<T>("401.5295976983805568002252058686292962091736753628017218020459795"),stof<T>("3803.7018106044276899516769382611433357348161907807136737440094343")}, std::complex<T>{stof<T>("690.44355177179759468203220784868110066378692825656862971210767189"),stof<T>("726.62417573608429684206349387714672211231562358006426356180903845")}, std::complex<T>{stof<T>("473.3480059880724584620391561949540588132644128986817338106273582"),stof<T>("2710.1078375540594347154792388613364989379260056769728015403007763")}, std::complex<T>{stof<T>("501.4905125071612986623249388644376857874876522696704168372847709"),stof<T>("-4715.0925752653135740968765066960074120119385380175634042669215099")}, std::complex<T>{stof<T>("23.52192830417613104223088940300867749149465382856570481087931659"),stof<T>("-995.91750269459288656917693128734520395072702579482445546870831536")}, std::complex<T>{stof<T>("-120.5651929427380146125937997754146866541271382820509468568289034"),stof<T>("-2031.1349842422935102494813370363471260699689321922704469893969333")}, std::complex<T>{stof<T>("-809.66025662488348620631704473173895387408395439246967537694742398"),stof<T>("-258.74066958299629353739789248157218070964584926264665551186840532")}, std::complex<T>{stof<T>("-257.08803649642468336739140893949019644033490230929544609983801932"),stof<T>("-150.93885677026104120148657193321871031989982200729550713213616085")}, stof<T>("-27.798199965403973130008609371046810056707915075587306640714604869"), stof<T>("27.798199965403973130008609371046810056707915075587306640714604869")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[46].real()/kbase.W[46].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_682_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_682_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2817.491278756908939899510450726241651740607345999699291432071195"),stof<T>("-2572.511054660116824599426000048445764395827697412662768419257969")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,41> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W66(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[46].real()/k.W[46].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_682_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_682_DLogXconstant_part(base_point<T>, kend);
	value += f_4_682_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_682_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_682_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_682_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_682_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_682_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_682_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
