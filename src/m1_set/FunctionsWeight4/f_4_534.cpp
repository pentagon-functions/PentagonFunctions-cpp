/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_534.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_534_abbreviated (const std::array<T,60>& abb) {
T z[107];
z[0] = abb[27] * (T(5) / T(4));
z[1] = abb[30] * (T(3) / T(4));
z[2] = -abb[31] + abb[32];
z[3] = 2 * z[2];
z[4] = abb[28] * (T(1) / T(2));
z[5] = -z[0] + z[1] + z[3] + z[4];
z[5] = abb[30] * z[5];
z[6] = abb[29] * (T(3) / T(4));
z[7] = -z[1] + z[6];
z[8] = abb[31] * (T(1) / T(2));
z[9] = -z[4] + z[8];
z[10] = 2 * abb[32];
z[0] = z[0] + z[7] + z[9] + -z[10];
z[0] = abb[29] * z[0];
z[11] = abb[34] * (T(3) / T(2));
z[12] = abb[36] * (T(1) / T(2));
z[13] = z[11] + -z[12];
z[14] = prod_pow(m1_set::bc<T>[0], 2);
z[15] = (T(1) / T(4)) * z[14];
z[16] = -z[13] + z[15];
z[17] = 3 * abb[31];
z[18] = abb[27] + z[17];
z[19] = z[4] * z[18];
z[20] = abb[57] * (T(3) / T(2));
z[19] = z[19] + z[20];
z[21] = prod_pow(abb[31], 2);
z[22] = (T(1) / T(2)) * z[21];
z[23] = abb[56] + z[22];
z[24] = prod_pow(abb[32], 2);
z[25] = -abb[58] + z[24];
z[26] = -z[23] + z[25];
z[27] = abb[27] * (T(1) / T(2));
z[28] = abb[32] + z[27];
z[29] = -z[8] + -z[28];
z[29] = abb[27] * z[29];
z[0] = z[0] + z[5] + -z[16] + z[19] + (T(3) / T(2)) * z[26] + z[29];
z[0] = abb[8] * z[0];
z[5] = abb[32] * (T(1) / T(2));
z[26] = abb[29] * (T(1) / T(4)) + -z[5];
z[29] = abb[31] + z[4];
z[30] = abb[30] * (T(1) / T(2));
z[31] = z[26] + -z[29] + -z[30];
z[31] = abb[29] * z[31];
z[32] = (T(23) / T(12)) * z[14];
z[33] = z[31] + z[32];
z[34] = abb[31] * abb[32];
z[35] = -z[22] + z[34];
z[36] = abb[35] + abb[58];
z[37] = z[35] + -z[36];
z[37] = 3 * z[37];
z[38] = z[2] + -z[27];
z[39] = abb[27] * z[38];
z[40] = z[37] + z[39];
z[41] = 3 * abb[28];
z[42] = -abb[27] + z[41];
z[43] = z[17] + z[42];
z[44] = abb[28] * z[43];
z[44] = -z[40] + z[44];
z[45] = abb[30] * (T(1) / T(4)) + z[5];
z[46] = -z[29] + -z[45];
z[46] = abb[30] * z[46];
z[11] = z[11] + z[12] + z[20] + -z[33] + (T(1) / T(2)) * z[44] + z[46];
z[11] = abb[7] * z[11];
z[44] = z[27] + z[30];
z[46] = -abb[28] + z[44];
z[47] = abb[29] * z[46];
z[46] = abb[30] * z[46];
z[48] = -abb[27] + z[4];
z[48] = abb[28] * z[48];
z[49] = prod_pow(abb[27], 2);
z[50] = (T(1) / T(2)) * z[49];
z[47] = -abb[33] + -z[46] + z[47] + -z[48] + -z[50];
z[47] = abb[14] * z[47];
z[51] = -z[30] + z[42];
z[51] = abb[30] * z[51];
z[52] = abb[29] * (T(1) / T(2));
z[53] = -abb[30] + z[52];
z[54] = z[42] + z[53];
z[54] = abb[29] * z[54];
z[55] = -abb[27] + abb[28] * (T(3) / T(2));
z[41] = z[41] * z[55];
z[41] = 3 * abb[33] + z[41] + z[50] + -z[51] + z[54];
z[50] = abb[1] * z[41];
z[51] = (T(13) / T(6)) * z[14];
z[54] = abb[1] * z[51];
z[54] = -z[50] + z[54];
z[56] = -abb[31] + z[52];
z[56] = abb[29] * z[56];
z[57] = -abb[31] + z[5];
z[58] = abb[32] * z[57];
z[56] = -z[36] + z[56] + -z[58];
z[59] = (T(1) / T(2)) * z[14];
z[56] = 3 * z[56] + -z[59];
z[60] = abb[5] * z[56];
z[61] = -abb[1] + abb[14] * (T(1) / T(2));
z[62] = 3 * abb[34];
z[61] = z[61] * z[62];
z[61] = (T(-3) / T(2)) * z[47] + z[54] + (T(1) / T(2)) * z[60] + z[61];
z[63] = abb[33] + -abb[35];
z[58] = z[58] + -z[63];
z[64] = -abb[56] + abb[57];
z[65] = -abb[36] + -z[22] + z[64];
z[66] = -abb[31] + z[27];
z[66] = abb[27] * z[66];
z[67] = -abb[31] + z[4];
z[67] = abb[28] * z[67];
z[66] = -z[58] + z[65] + z[66] + -z[67];
z[68] = abb[4] * (T(3) / T(2));
z[69] = z[66] * z[68];
z[70] = -z[30] + z[52];
z[71] = -abb[27] + abb[28];
z[72] = z[2] + z[71];
z[73] = z[70] * z[72];
z[17] = abb[32] + -z[17];
z[17] = z[5] * z[17];
z[74] = abb[56] + -3 * z[63];
z[17] = z[17] + z[21] + (T(1) / T(2)) * z[74];
z[43] = z[4] * z[43];
z[43] = z[20] + z[43];
z[74] = 4 * abb[31];
z[75] = abb[27] + abb[32] * (T(-5) / T(2)) + z[74];
z[75] = abb[27] * z[75];
z[17] = 4 * abb[36] + 3 * z[17] + -z[43] + -z[73] + z[75];
z[17] = abb[13] * z[17];
z[75] = -abb[27] + abb[30];
z[76] = 3 * abb[32];
z[77] = z[75] + z[76];
z[78] = abb[30] * z[77];
z[79] = 2 * abb[30];
z[80] = -abb[27] + z[79];
z[81] = z[76] + z[80];
z[82] = -abb[29] + z[81];
z[82] = abb[29] * z[82];
z[78] = z[78] + -z[82];
z[83] = -z[27] + -z[76];
z[83] = abb[27] * z[83];
z[84] = (T(5) / T(3)) * z[14];
z[85] = (T(1) / T(2)) * z[24];
z[86] = -abb[58] + z[85];
z[87] = -abb[56] + z[86];
z[83] = z[78] + z[83] + z[84] + 3 * z[87];
z[83] = abb[2] * z[83];
z[87] = -abb[32] + z[27];
z[88] = z[70] + z[87];
z[88] = abb[29] * z[88];
z[89] = abb[27] * z[87];
z[89] = -abb[56] + z[89];
z[90] = abb[30] * z[87];
z[90] = z[89] + -z[90];
z[88] = z[86] + z[88] + z[90];
z[88] = -z[59] + 3 * z[88];
z[91] = abb[15] * z[88];
z[92] = abb[29] + abb[30];
z[93] = abb[27] + z[92];
z[93] = z[52] * z[93];
z[94] = -z[51] + z[93];
z[95] = abb[30] + z[27];
z[95] = abb[30] * z[95];
z[95] = -2 * z[49] + -z[94] + z[95];
z[95] = abb[0] * z[95];
z[96] = prod_pow(abb[28], 2);
z[97] = -z[49] + z[96];
z[98] = 3 * abb[9];
z[97] = z[97] * z[98];
z[99] = z[95] + -z[97];
z[100] = abb[16] + abb[18];
z[101] = -abb[17] + abb[19];
z[102] = z[100] + z[101];
z[102] = abb[37] * z[102];
z[0] = z[0] + z[11] + z[17] + z[61] + z[69] + z[83] + (T(-1) / T(2)) * z[91] + -z[99] + (T(3) / T(4)) * z[102];
z[0] = abb[50] * z[0];
z[11] = abb[28] + -abb[31];
z[17] = abb[29] + -z[10] + z[11] + -z[79];
z[17] = abb[29] * z[17];
z[83] = -abb[28] + z[3] + -z[30];
z[83] = abb[30] * z[83];
z[55] = abb[28] * z[55];
z[91] = -abb[36] + z[39] + -z[55];
z[17] = z[17] + z[62] + z[83] + z[84] + 3 * z[86] + -z[91];
z[17] = abb[8] * z[17];
z[3] = z[3] + -z[27];
z[3] = abb[27] * z[3];
z[58] = z[22] + z[58];
z[83] = abb[30] * z[72];
z[84] = abb[29] * z[72];
z[86] = 2 * abb[36];
z[3] = z[3] + z[55] + -3 * z[58] + -z[83] + z[84] + -z[86];
z[3] = abb[13] * z[3];
z[58] = 2 * abb[31];
z[102] = abb[28] + abb[32];
z[53] = z[53] + -z[58] + -z[102];
z[53] = abb[29] * z[53];
z[103] = -abb[28] + abb[30];
z[104] = -z[2] + z[103];
z[104] = abb[30] * z[104];
z[37] = (T(23) / T(6)) * z[14] + z[37] + z[53] + z[91] + -z[104];
z[53] = abb[7] * z[37];
z[91] = (T(5) / T(6)) * z[14];
z[104] = -3 * z[25] + -z[49] + -z[78] + z[91];
z[104] = abb[2] * z[104];
z[105] = abb[30] + abb[32];
z[105] = abb[29] * z[105];
z[106] = abb[30] * abb[32];
z[25] = -z[25] + z[105] + -z[106];
z[25] = (T(-7) / T(2)) * z[14] + 3 * z[25];
z[25] = abb[12] * z[25];
z[105] = abb[1] * z[62];
z[3] = -z[3] + -z[17] + -z[25] + -z[53] + -z[54] + z[60] + z[104] + z[105];
z[17] = abb[44] + abb[46];
z[3] = z[3] * z[17];
z[53] = abb[27] * (T(3) / T(4));
z[54] = abb[30] * (T(7) / T(4)) + -4 * z[2] + z[4] + z[53];
z[54] = abb[30] * z[54];
z[60] = 3 * abb[58];
z[23] = z[23] + z[60];
z[23] = (T(1) / T(2)) * z[23] + -z[24];
z[8] = -abb[27] + -z[8] + z[10];
z[8] = abb[27] * z[8];
z[104] = 4 * abb[32] + abb[29] * (T(-5) / T(4)) + abb[30] * (T(13) / T(4)) + z[9] + -z[53];
z[104] = abb[29] * z[104];
z[8] = abb[34] * (T(-9) / T(2)) + z[8] + -z[12] + (T(-43) / T(12)) * z[14] + 3 * z[23] + -z[43] + z[54] + z[104];
z[8] = abb[8] * z[8];
z[18] = -abb[28] * z[18];
z[18] = z[18] + -z[40];
z[23] = -z[5] + z[58];
z[40] = -z[4] + z[23];
z[43] = abb[30] * (T(5) / T(4));
z[54] = z[40] + z[43];
z[54] = abb[30] * z[54];
z[18] = -z[13] + (T(1) / T(2)) * z[18] + -z[20] + -z[33] + z[54];
z[18] = abb[7] * z[18];
z[20] = -abb[56] + -z[21] + z[34] + z[63];
z[33] = -z[23] + -z[27];
z[33] = abb[27] * z[33];
z[19] = z[19] + (T(3) / T(2)) * z[20] + z[33] + -z[73] + -z[86];
z[19] = abb[13] * z[19];
z[20] = prod_pow(abb[30], 2);
z[20] = z[20] + -z[96];
z[20] = abb[11] * z[20];
z[8] = z[8] + z[18] + z[19] + -3 * z[20] + -z[25] + z[61] + -z[69];
z[8] = abb[47] * z[8];
z[18] = -z[11] + -z[26] + z[43] + z[53];
z[18] = abb[29] * z[18];
z[19] = -abb[58] + z[35] + z[63];
z[19] = 3 * z[19];
z[26] = -abb[27] + -z[2];
z[26] = abb[27] * z[26];
z[26] = -z[19] + z[26];
z[33] = abb[27] * (T(3) / T(2));
z[34] = -z[2] + -z[33];
z[34] = abb[28] + (T(1) / T(2)) * z[34] + z[43];
z[34] = abb[30] * z[34];
z[13] = -z[13] + z[18] + (T(1) / T(2)) * z[26] + -z[32] + z[34] + -z[55];
z[13] = abb[47] * z[13];
z[18] = -z[17] * z[37];
z[26] = abb[31] * z[76];
z[26] = -z[26] + 3 * z[36];
z[32] = z[30] + z[38];
z[34] = abb[30] * z[32];
z[21] = (T(3) / T(2)) * z[21];
z[35] = abb[27] * z[2];
z[34] = abb[33] + -z[21] + -z[26] + z[34] + z[35];
z[35] = abb[27] * (T(1) / T(4));
z[6] = -abb[31] + z[6] + z[35] + -z[45];
z[6] = abb[29] * z[6];
z[36] = abb[34] * (T(1) / T(2));
z[6] = z[6] + -z[12] + -z[15] + (T(1) / T(2)) * z[34] + z[36];
z[15] = 3 * abb[45];
z[6] = z[6] * z[15];
z[5] = z[5] + -z[7] + -z[11] + z[35];
z[5] = abb[29] * z[5];
z[7] = abb[27] + -z[2];
z[7] = abb[27] * z[7];
z[7] = z[7] + -z[19];
z[19] = -z[2] + -z[27];
z[1] = abb[28] + -z[1] + (T(1) / T(2)) * z[19];
z[1] = abb[30] * z[1];
z[1] = z[1] + z[5] + (T(1) / T(2)) * z[7] + z[16] + -z[55];
z[1] = abb[50] * z[1];
z[5] = z[41] + -z[51] + z[62];
z[5] = abb[42] * z[5];
z[7] = -z[27] + z[79];
z[7] = abb[30] * z[7];
z[7] = z[7] + -z[49] + z[94];
z[16] = abb[43] * z[7];
z[19] = -abb[30] + z[27];
z[19] = abb[30] * z[19];
z[19] = z[19] + -z[93];
z[34] = (T(13) / T(2)) * z[14];
z[19] = 3 * z[19] + z[34];
z[19] = abb[49] * z[19];
z[1] = z[1] + z[5] + z[6] + z[13] + z[16] + z[18] + z[19];
z[1] = abb[3] * z[1];
z[5] = abb[32] * z[23];
z[6] = -z[58] + -z[87];
z[6] = abb[27] * z[6];
z[13] = abb[28] * z[29];
z[5] = z[5] + z[6] + z[13] + -z[21] + 2 * z[63] + z[64] + -z[86];
z[5] = abb[13] * z[5];
z[6] = abb[4] * z[66];
z[5] = -z[5] + z[6];
z[13] = -abb[57] + -z[13] + -z[22];
z[16] = (T(5) / T(2)) * z[14];
z[13] = 3 * z[13] + z[16];
z[13] = abb[7] * z[13];
z[18] = abb[27] * z[28];
z[18] = abb[56] + z[18] + z[85];
z[16] = -z[16] + 3 * z[18];
z[16] = abb[2] * z[16];
z[5] = -3 * z[5] + z[13] + z[16] + -z[97];
z[5] = abb[48] * z[5];
z[13] = -z[11] + z[44] + -z[52];
z[13] = abb[29] * z[13];
z[16] = abb[27] * abb[31];
z[11] = -abb[27] + z[11];
z[18] = abb[28] * z[11];
z[13] = -abb[34] + abb[58] + z[13] + -z[16] + -z[18] + -z[46] + z[65];
z[13] = 3 * z[13] + z[59];
z[16] = abb[21] + abb[23];
z[19] = -z[13] * z[16];
z[21] = abb[27] * abb[32];
z[29] = abb[32] * z[2];
z[21] = z[21] + -z[29] + z[63] + z[84];
z[35] = -abb[32] + z[30] + -z[71];
z[35] = abb[30] * z[35];
z[35] = abb[34] + abb[56] + z[21] + z[35] + z[48];
z[35] = abb[22] * z[35];
z[21] = -z[18] + -z[21] + z[64] + z[83];
z[37] = abb[20] * z[21];
z[32] = abb[29] * z[32];
z[29] = -abb[35] + -z[29] + z[32] + -z[90];
z[29] = abb[24] * z[29];
z[29] = z[29] + z[35] + -z[37];
z[32] = abb[25] * z[88];
z[35] = abb[37] * (T(3) / T(2));
z[37] = abb[26] * z[35];
z[19] = z[19] + 3 * z[29] + z[32] + -z[37];
z[29] = abb[51] + abb[52] + abb[53] + abb[54] + abb[55];
z[32] = (T(1) / T(2)) * z[29];
z[19] = z[19] * z[32];
z[32] = -abb[14] * abb[34];
z[6] = z[6] + z[32] + z[47];
z[13] = -abb[8] * z[13];
z[32] = 3 * abb[5];
z[37] = -z[32] * z[56];
z[6] = 3 * z[6] + z[13] + z[37];
z[13] = -z[30] + z[102];
z[13] = abb[30] * z[13];
z[13] = abb[57] + z[13] + -z[18] + -z[22] + -z[26] + z[39];
z[12] = -z[12] + (T(1) / T(2)) * z[13] + z[31] + -z[36];
z[12] = 3 * z[12] + (T(13) / T(4)) * z[14];
z[12] = abb[7] * z[12];
z[13] = abb[13] * z[21];
z[6] = (T(1) / T(2)) * z[6] + z[12] + (T(-3) / T(2)) * z[13] + z[25];
z[6] = abb[45] * z[6];
z[12] = -z[27] + z[76];
z[12] = abb[27] * z[12];
z[13] = z[77] * z[79];
z[18] = 2 * abb[58] + (T(-3) / T(2)) * z[24];
z[21] = abb[56] + z[18];
z[12] = -z[12] + z[13] + -3 * z[21] + -2 * z[82] + z[91];
z[13] = -abb[8] * z[12];
z[13] = z[13] + -z[25] + z[95];
z[13] = abb[43] * z[13];
z[12] = -abb[47] * z[12];
z[21] = (T(-5) / T(2)) * z[24] + z[60] + -z[78] + -z[89];
z[22] = 3 * abb[43];
z[24] = z[21] * z[22];
z[12] = z[12] + z[24];
z[12] = abb[2] * z[12];
z[24] = z[10] + -z[27];
z[24] = abb[30] * z[24];
z[10] = z[10] + -z[52];
z[26] = abb[30] * (T(-3) / T(2)) + -z[10] + z[27];
z[26] = abb[29] * z[26];
z[14] = z[14] + -z[18] + z[24] + z[26] + z[89];
z[14] = abb[8] * z[14];
z[18] = abb[2] * z[21];
z[14] = z[14] + -z[18];
z[18] = -abb[16] + z[101];
z[21] = z[18] * z[35];
z[14] = 3 * z[14] + z[21] + z[25];
z[14] = abb[49] * z[14];
z[21] = 3 * abb[1] + -abb[14];
z[21] = abb[34] * z[21];
z[20] = z[20] + z[21] + z[47] + z[50];
z[7] = -abb[8] * z[7];
z[21] = -z[35] * z[100];
z[24] = -abb[1] * z[34];
z[7] = z[7] + 3 * z[20] + z[21] + z[24] + z[99];
z[7] = abb[42] * z[7];
z[20] = abb[47] * (T(1) / T(2));
z[21] = abb[18] + -z[18];
z[21] = z[20] * z[21];
z[24] = abb[45] * (T(1) / T(2));
z[18] = -abb[18] + -z[18];
z[18] = z[18] * z[24];
z[25] = -abb[43] * z[101];
z[18] = z[18] + z[21] + z[25];
z[18] = z[18] * z[35];
z[20] = -abb[43] + abb[49] + -z[20] + -z[24];
z[20] = abb[15] * z[20];
z[21] = -z[20] * z[88];
z[24] = -abb[45] + -abb[47] + abb[50];
z[25] = abb[6] * (T(3) / T(2));
z[24] = z[24] * z[25];
z[25] = -abb[31] + z[30];
z[25] = abb[30] * z[25];
z[25] = -abb[34] + -abb[57] + -z[25] + z[67];
z[25] = z[24] * z[25];
z[0] = abb[59] + z[0] + z[1] + z[3] + z[5] + z[6] + z[7] + z[8] + z[12] + z[13] + z[14] + z[18] + z[19] + z[21] + z[25];
z[1] = abb[29] * (T(3) / T(2));
z[3] = -z[1] + z[9] + z[28];
z[3] = m1_set::bc<T>[0] * z[3];
z[5] = -abb[38] + abb[39];
z[6] = -abb[40] + z[5];
z[3] = z[3] + (T(3) / T(2)) * z[6];
z[3] = abb[8] * z[3];
z[6] = abb[28] * (T(5) / T(2));
z[7] = -z[6] + -z[23] + -z[70];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = abb[39] + abb[40];
z[7] = z[7] + (T(3) / T(2)) * z[8];
z[7] = abb[7] * z[7];
z[8] = abb[29] + -abb[30];
z[9] = abb[27] + -abb[32];
z[12] = z[8] + z[9];
z[12] = m1_set::bc<T>[0] * z[12];
z[12] = abb[38] + z[12];
z[13] = abb[40] + z[12];
z[14] = abb[15] * z[13];
z[18] = m1_set::bc<T>[0] * z[71];
z[19] = z[5] + z[18];
z[21] = z[19] * z[68];
z[25] = 2 * abb[27];
z[26] = z[8] + z[25];
z[27] = abb[0] * z[26];
z[28] = 2 * m1_set::bc<T>[0];
z[31] = z[27] * z[28];
z[34] = z[8] + z[42];
z[35] = abb[1] * z[34];
z[36] = z[28] * z[35];
z[37] = m1_set::bc<T>[0] * z[72];
z[38] = 3 * z[5];
z[39] = 5 * z[37] + -z[38];
z[41] = abb[13] * (T(1) / T(2));
z[39] = z[39] * z[41];
z[8] = abb[27] + -z[8] + z[76];
z[8] = m1_set::bc<T>[0] * z[8];
z[42] = -abb[38] + -abb[40];
z[8] = z[8] + 3 * z[42];
z[8] = abb[2] * z[8];
z[42] = abb[29] + -abb[31];
z[42] = m1_set::bc<T>[0] * z[42];
z[42] = abb[40] + z[42];
z[43] = abb[5] * z[42];
z[43] = (T(3) / T(2)) * z[43];
z[44] = abb[9] * z[18];
z[3] = z[3] + z[7] + z[8] + (T(3) / T(2)) * z[14] + z[21] + -z[31] + z[36] + z[39] + -z[43] + -6 * z[44];
z[3] = abb[50] * z[3];
z[7] = 4 * abb[30];
z[8] = 2 * abb[28];
z[14] = abb[29] + -abb[32] + z[8];
z[39] = z[7] + -z[14] + z[58];
z[39] = m1_set::bc<T>[0] * z[39];
z[44] = 3 * abb[40];
z[39] = z[39] + -z[44];
z[39] = abb[8] * z[39];
z[14] = z[14] + z[74] + z[79];
z[14] = m1_set::bc<T>[0] * z[14];
z[14] = z[14] + -z[44];
z[45] = abb[7] * z[14];
z[26] = m1_set::bc<T>[0] * z[26];
z[26] = z[26] + z[44];
z[26] = abb[2] * z[26];
z[46] = abb[13] * z[28] * z[72];
z[32] = z[32] * z[42];
z[42] = m1_set::bc<T>[0] * z[92];
z[47] = -abb[40] + z[42];
z[47] = abb[12] * z[47];
z[48] = 3 * z[47];
z[26] = -z[26] + z[32] + z[36] + z[39] + z[45] + -z[46] + -z[48];
z[26] = -z[17] * z[26];
z[6] = -8 * abb[30] + abb[31] * (T(-5) / T(2)) + z[6] + -z[10] + z[33];
z[6] = m1_set::bc<T>[0] * z[6];
z[10] = -z[5] + z[44];
z[6] = z[6] + (T(3) / T(2)) * z[10];
z[6] = abb[8] * z[6];
z[10] = abb[11] * z[103];
z[33] = 3 * z[10] + z[35];
z[33] = z[28] * z[33];
z[36] = abb[30] * (T(-5) / T(2)) + -z[40] + -z[52];
z[36] = m1_set::bc<T>[0] * z[36];
z[39] = -abb[39] + abb[40];
z[36] = z[36] + (T(3) / T(2)) * z[39];
z[36] = abb[7] * z[36];
z[38] = -z[37] + z[38];
z[38] = z[38] * z[41];
z[6] = z[6] + -z[21] + z[33] + z[36] + z[38] + -z[43] + z[48];
z[6] = abb[47] * z[6];
z[7] = -z[7] + z[8] + -z[23] + -z[52];
z[7] = m1_set::bc<T>[0] * z[7];
z[21] = abb[40] * (T(3) / T(2));
z[7] = z[7] + z[21];
z[7] = abb[47] * z[7];
z[14] = -z[14] * z[17];
z[1] = z[1] + -z[23];
z[8] = z[1] + z[8] + -z[25];
z[8] = m1_set::bc<T>[0] * z[8];
z[8] = z[8] + z[21];
z[8] = abb[50] * z[8];
z[17] = -abb[42] * z[34];
z[23] = abb[29] + z[80];
z[25] = -abb[43] * z[23];
z[17] = z[17] + z[25];
z[17] = z[17] * z[28];
z[1] = -m1_set::bc<T>[0] * z[1];
z[1] = z[1] + -z[21];
z[1] = z[1] * z[15];
z[21] = abb[49] * z[42];
z[1] = z[1] + z[7] + z[8] + z[14] + z[17] + 6 * z[21];
z[1] = abb[3] * z[1];
z[7] = abb[4] * z[19];
z[8] = z[5] + -z[37];
z[8] = abb[13] * z[8];
z[14] = abb[9] * z[28] * z[71];
z[17] = abb[28] + abb[31];
z[17] = m1_set::bc<T>[0] * z[17];
z[17] = -abb[39] + z[17];
z[17] = abb[7] * z[17];
z[19] = -abb[27] + -abb[32];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = abb[38] + z[19];
z[19] = abb[2] * z[19];
z[8] = -z[7] + z[8] + z[14] + z[17] + z[19];
z[8] = abb[48] * z[8];
z[9] = -z[9] + z[79];
z[9] = m1_set::bc<T>[0] * z[9];
z[14] = abb[38] + 2 * abb[40];
z[9] = z[9] + -z[14];
z[9] = abb[8] * z[9];
z[12] = z[12] + z[44];
z[17] = -abb[2] * z[12];
z[9] = z[9] + z[17] + -z[47];
z[9] = abb[49] * z[9];
z[17] = z[13] * z[20];
z[8] = z[8] + z[9] + z[17];
z[2] = z[2] + z[75];
z[9] = abb[22] + abb[24];
z[2] = z[2] * z[9];
z[9] = abb[20] * z[72];
z[2] = z[2] + z[9];
z[2] = m1_set::bc<T>[0] * z[2];
z[9] = abb[29] + z[11];
z[9] = m1_set::bc<T>[0] * z[9];
z[9] = abb[40] + z[5] + z[9];
z[11] = z[9] * z[16];
z[13] = abb[25] * z[13];
z[16] = -abb[20] + -abb[22] + -abb[24];
z[16] = abb[38] * z[16];
z[17] = abb[20] * abb[39];
z[2] = z[2] + z[11] + z[13] + z[16] + z[17];
z[11] = (T(-3) / T(2)) * z[29];
z[2] = z[2] * z[11];
z[9] = -abb[8] * z[9];
z[7] = z[7] + z[9] + z[32];
z[4] = z[4] + z[30] + z[52] + -z[57];
z[4] = m1_set::bc<T>[0] * z[4];
z[9] = abb[39] + -z[44];
z[4] = z[4] + (T(1) / T(2)) * z[9];
z[4] = abb[7] * z[4];
z[5] = -z[5] + -z[37];
z[5] = z[5] * z[41];
z[4] = z[4] + z[5] + (T(1) / T(2)) * z[7] + -z[47];
z[4] = z[4] * z[15];
z[5] = 2 * abb[29] + -z[81];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = z[5] + 3 * z[14];
z[7] = abb[8] * z[5];
z[7] = z[7] + z[31] + z[48];
z[7] = abb[43] * z[7];
z[5] = abb[47] * z[5];
z[9] = z[12] * z[22];
z[5] = z[5] + z[9];
z[5] = abb[2] * z[5];
z[9] = -z[10] + -z[35];
z[10] = abb[8] * z[23];
z[9] = 3 * z[9] + z[10] + z[27];
z[9] = m1_set::bc<T>[0] * z[9];
z[10] = z[18] * z[98];
z[9] = z[9] + z[10];
z[9] = abb[42] * z[9];
z[10] = m1_set::bc<T>[0] * z[103];
z[10] = -abb[39] + z[10];
z[10] = z[10] * z[24];
z[1] = abb[41] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + 3 * z[8] + 2 * z[9] + z[10] + z[26];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_534_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-36.682310515106630392833272475757325351289707818356945272902581058"),stof<T>("52.741337696243548012391414832183606836297750117548411587622738085")}, std::complex<T>{stof<T>("-26.198296627818665715852294092733382933427874830835336739337282745"),stof<T>("8.503853713337012963867488862109638201182081099726697992227678228")}, std::complex<T>{stof<T>("-49.524956236407311254082823283016140611008461205871124774759687584"),stof<T>("73.841783123950370409383066140526278607337617840009044329562912174")}, std::complex<T>{stof<T>("-25.29960182007458360275951304817094766106727341782048314949503923"),stof<T>("-0.231393115198248609905564510504544725433921351183129945062375423")}, std::complex<T>{stof<T>("-49.524956236407311254082823283016140611008461205871124774759687584"),stof<T>("73.841783123950370409383066140526278607337617840009044329562912174")}, std::complex<T>{stof<T>("-34.994242349480808660486572442622549822969684600108166592620316202"),stof<T>("26.641826692748775297875689373194511221230188847269298308405341226")}, std::complex<T>{stof<T>("78.871258056120432773457608788556736642252678241932957572828799883"),stof<T>("-76.572862457047181862460952427279532632895456463743946810263047842")}, std::complex<T>{stof<T>("22.151596628180127799237021635363734563250931212593987090763209676"),stof<T>("-5.541381265041952900884038064851839450190321124808665566465167137")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_534_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_534_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-3) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 * (-12 + -5 * v[0] + v[1] + -3 * v[2] + v[3] + 4 * v[4] + m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 3 * v[5]) + m1_set::bc<T>[2] * (21 * v[0] + -v[1] + -3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((-2 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(-3) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[44] + -abb[45] + abb[46] + -abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[31] + -abb[32];
z[1] = abb[27] + abb[31];
z[0] = z[0] * z[1];
z[0] = -abb[33] + abb[35] + abb[36] + z[0];
z[1] = -abb[44] + abb[45] + -abb[46] + abb[48] + -abb[50];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_534_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(3) / T(8)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(3) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[44] + -abb[45] + abb[46] + -abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[44] + -abb[45] + abb[46] + -abb[48] + abb[50];
z[1] = abb[31] + -abb[32];
return 3 * abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_534_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("134.00016935824535130876180219353654991258256745266805246001734843"),stof<T>("57.62304878618258613068434794966887603493917368650055524885138918")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W17(k,dl), dlog_W21(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k), T{0}};
abb[41] = SpDLog_f_4_534_W_17_Im(t, path, abb);
abb[59] = SpDLog_f_4_534_W_17_Re(t, path, abb);

                    
            return f_4_534_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_534_DLogXconstant_part(base_point<T>, kend);
	value += f_4_534_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_534_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_534_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_534_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_534_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_534_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_534_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
