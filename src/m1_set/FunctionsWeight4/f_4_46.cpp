/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_46.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_46_abbreviated (const std::array<T,29>& abb) {
T z[62];
z[0] = abb[18] + abb[20];
z[1] = 4 * abb[21];
z[2] = z[0] + -z[1];
z[3] = abb[22] + abb[23];
z[4] = 16 * abb[19];
z[5] = -5 * abb[24] + -2 * z[2] + -z[3] + z[4];
z[6] = 2 * abb[1];
z[7] = z[5] * z[6];
z[8] = 8 * abb[19];
z[9] = 3 * abb[24];
z[10] = z[2] + -z[8] + z[9];
z[11] = 2 * abb[3];
z[12] = z[10] * z[11];
z[13] = 10 * abb[19] + z[1];
z[14] = abb[24] + 3 * z[0] + -z[13];
z[15] = z[3] + z[14];
z[16] = abb[6] * z[15];
z[17] = abb[9] * abb[26];
z[16] = z[16] + z[17];
z[18] = abb[8] * abb[26];
z[19] = z[16] + z[18];
z[20] = 2 * abb[21];
z[21] = 4 * abb[19] + z[20];
z[9] = -z[9] + z[21];
z[22] = abb[20] + abb[22];
z[23] = z[9] + z[22];
z[24] = abb[4] * z[23];
z[25] = z[19] + -z[24];
z[26] = 2 * abb[19];
z[27] = z[20] + z[26];
z[28] = 2 * abb[24];
z[29] = z[27] + -z[28];
z[30] = abb[18] + abb[23];
z[31] = z[29] + z[30];
z[31] = abb[0] * z[31];
z[29] = z[22] + z[29];
z[29] = abb[2] * z[29];
z[32] = z[9] + z[30];
z[33] = abb[5] * z[32];
z[7] = -z[7] + z[12] + -z[25] + z[29] + z[31] + z[33];
z[12] = 2 * abb[12];
z[12] = z[7] * z[12];
z[29] = abb[1] * z[10];
z[29] = -z[16] + 4 * z[29];
z[31] = abb[24] * (T(9) / T(2));
z[34] = 6 * abb[19];
z[35] = -z[1] + z[31] + -z[34];
z[36] = 2 * abb[23];
z[37] = abb[22] + -z[35] + z[36];
z[37] = abb[0] * z[37];
z[38] = 2 * z[10];
z[39] = abb[22] + -abb[23];
z[40] = z[38] + z[39];
z[40] = abb[3] * z[40];
z[41] = (T(3) / T(2)) * z[18] + -2 * z[33];
z[42] = 3 * abb[26];
z[43] = 2 * abb[25];
z[44] = z[42] + z[43];
z[45] = abb[7] * z[44];
z[46] = abb[24] * (T(3) / T(2)) + z[26];
z[47] = 3 * abb[23];
z[48] = z[46] + -z[47];
z[49] = 2 * abb[18];
z[50] = -z[48] + z[49];
z[50] = abb[2] * z[50];
z[37] = z[29] + z[37] + z[40] + -z[41] + -z[45] + z[50];
z[37] = abb[13] * z[37];
z[29] = 2 * z[24] + z[29];
z[40] = 2 * abb[22];
z[35] = abb[23] + -z[35] + z[40];
z[35] = abb[2] * z[35];
z[38] = z[38] + -z[39];
z[38] = abb[3] * z[38];
z[50] = abb[26] * (T(3) / T(2));
z[43] = z[43] + z[50];
z[51] = abb[8] * z[43];
z[52] = 3 * abb[22];
z[46] = z[46] + -z[52];
z[53] = 2 * abb[20];
z[54] = -z[46] + z[53];
z[54] = abb[0] * z[54];
z[35] = z[29] + z[35] + z[38] + z[45] + z[51] + z[54];
z[35] = abb[11] * z[35];
z[38] = abb[0] + abb[2];
z[45] = z[10] * z[38];
z[10] = z[6] * z[10];
z[26] = abb[21] + z[26];
z[51] = -z[0] + z[26];
z[51] = abb[3] * z[51];
z[54] = abb[25] + abb[26];
z[55] = abb[8] * z[54];
z[45] = -z[10] + z[45] + 4 * z[51] + -3 * z[55];
z[45] = abb[14] * z[45];
z[35] = -z[12] + z[35] + z[37] + z[45];
z[35] = abb[14] * z[35];
z[37] = 7 * abb[24];
z[45] = 5 * abb[23];
z[51] = 5 * abb[22];
z[2] = 32 * abb[19] + -4 * z[2] + -z[37] + -z[45] + -z[51];
z[2] = z[2] * z[6];
z[5] = z[5] * z[11];
z[6] = -abb[24] + z[27];
z[27] = -abb[18] + -z[6];
z[27] = z[3] + 2 * z[27];
z[27] = abb[0] * z[27];
z[6] = -abb[20] + -z[6];
z[6] = z[3] + 2 * z[6];
z[6] = abb[2] * z[6];
z[55] = 8 * abb[21];
z[37] = -18 * abb[19] + z[37] + -z[55];
z[56] = 3 * abb[20];
z[57] = abb[18] + z[37] + z[56];
z[57] = abb[5] * z[57];
z[58] = 3 * abb[18];
z[37] = abb[20] + z[37] + z[58];
z[37] = abb[4] * z[37];
z[2] = z[2] + z[5] + z[6] + z[19] + z[27] + z[37] + z[57];
z[2] = prod_pow(abb[12], 2) * z[2];
z[5] = -z[10] + z[16];
z[1] = z[1] + z[8];
z[6] = -z[1] + z[28];
z[8] = abb[20] + z[6] + -z[49] + z[51];
z[8] = abb[0] * z[8];
z[1] = abb[24] * (T(5) / T(2)) + -z[1];
z[27] = abb[23] + z[58];
z[37] = -abb[20] + -z[1] + -z[27] + z[40];
z[37] = abb[3] * z[37];
z[57] = z[14] + z[40];
z[57] = abb[4] * z[57];
z[59] = abb[20] + abb[23];
z[60] = z[46] + -z[59];
z[60] = abb[2] * z[60];
z[61] = abb[25] + abb[26] * (T(1) / T(2));
z[61] = abb[7] * z[61];
z[50] = abb[25] + z[50];
z[50] = abb[8] * z[50];
z[8] = z[5] + z[8] + z[37] + z[50] + -z[57] + z[60] + z[61];
z[8] = abb[11] * z[8];
z[37] = 9 * abb[24];
z[4] = z[3] + z[4] + -z[37] + z[55];
z[4] = abb[3] * z[4];
z[46] = -z[36] + z[46];
z[46] = abb[0] * z[46];
z[50] = -z[40] + z[48];
z[50] = abb[2] * z[50];
z[4] = z[4] + -z[29] + z[41] + z[46] + z[50];
z[4] = abb[13] * z[4];
z[4] = z[4] + z[8] + z[12];
z[4] = abb[11] * z[4];
z[8] = -z[38] * z[43];
z[13] = abb[23] + -z[13] + z[31];
z[13] = abb[8] * z[13];
z[29] = 4 * abb[25];
z[31] = -z[29] + -z[42];
z[31] = abb[3] * z[31];
z[15] = abb[9] * z[15];
z[38] = -abb[7] * z[39];
z[29] = abb[10] * z[29];
z[39] = abb[6] * abb[26];
z[8] = z[8] + z[13] + z[15] + z[29] + z[31] + z[38] + z[39];
z[8] = abb[15] * z[8];
z[13] = z[14] + z[36];
z[13] = abb[5] * z[13];
z[10] = z[10] + z[13];
z[6] = abb[18] + z[6] + z[45] + -z[53];
z[6] = abb[2] * z[6];
z[13] = abb[22] + z[56];
z[1] = -abb[18] + -z[1] + -z[13] + z[36];
z[1] = abb[3] * z[1];
z[14] = abb[18] + abb[22];
z[15] = -z[14] + z[48];
z[15] = abb[0] * z[15];
z[1] = z[1] + z[6] + -z[10] + z[15] + z[19] + -z[61];
z[1] = abb[13] * z[1];
z[1] = z[1] + z[12];
z[1] = abb[13] * z[1];
z[6] = (T(-1) / T(2)) * z[0] + (T(-7) / T(2)) * z[3] + z[21] + z[28];
z[6] = abb[1] * z[6];
z[12] = 8 * z[0] + -7 * z[3] + z[26] + -z[28];
z[12] = abb[3] * z[12];
z[6] = -z[6] + -z[12] + z[17] + z[18];
z[12] = -z[28] + z[55];
z[15] = 4 * abb[18];
z[17] = -13 * abb[20] + z[12] + z[15];
z[19] = 7 * abb[19];
z[17] = abb[22] * (T(-16) / T(3)) + abb[23] * (T(8) / T(3)) + (T(1) / T(3)) * z[17] + z[19];
z[17] = abb[0] * z[17];
z[26] = 4 * abb[20];
z[12] = -13 * abb[18] + z[12] + z[26];
z[12] = abb[23] * (T(-16) / T(3)) + abb[22] * (T(8) / T(3)) + (T(1) / T(3)) * z[12] + z[19];
z[12] = abb[2] * z[12];
z[3] = abb[24] + z[3];
z[3] = abb[21] * (T(4) / T(3)) + abb[19] * (T(10) / T(3)) + -z[0] + (T(-1) / T(3)) * z[3];
z[3] = abb[6] * z[3];
z[19] = 11 * abb[19] + abb[24] * (T(-5) / T(3)) + abb[21] * (T(13) / T(3));
z[29] = abb[18] * (T(8) / T(3)) + -z[19] + (T(5) / T(2)) * z[59];
z[29] = abb[5] * z[29];
z[14] = abb[20] * (T(8) / T(3)) + (T(5) / T(2)) * z[14] + -z[19];
z[14] = abb[4] * z[14];
z[3] = z[3] + (T(-1) / T(3)) * z[6] + z[12] + z[14] + z[17] + z[29];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[6] = 30 * abb[19] + 14 * abb[21] + -z[37];
z[12] = -4 * abb[23] + z[6] + -z[13] + -z[15];
z[12] = abb[1] * z[12];
z[14] = abb[0] * z[32];
z[15] = abb[7] * abb[26];
z[12] = z[12] + -z[14] + z[15] + z[18];
z[14] = 4 * abb[24];
z[17] = z[14] + -z[34];
z[18] = z[17] + -z[20];
z[19] = -z[18] + -z[47] + z[49] + z[56];
z[19] = abb[5] * z[19];
z[14] = 14 * abb[19] + 6 * abb[21] + -z[14];
z[13] = -z[13] + z[14] + -z[49];
z[13] = z[11] * z[13];
z[29] = abb[20] + z[20];
z[29] = -abb[23] + -z[17] + 3 * z[29];
z[29] = abb[2] * z[29];
z[16] = 2 * z[16];
z[12] = -2 * z[12] + -z[13] + -z[16] + z[19] + z[29];
z[13] = abb[28] * z[12];
z[18] = -z[18] + -z[52] + z[53] + z[58];
z[18] = abb[4] * z[18];
z[6] = -4 * abb[22] + z[6] + -z[26] + -z[27];
z[6] = abb[1] * z[6];
z[19] = abb[2] * z[23];
z[6] = z[6] + -z[15] + -z[19];
z[14] = z[14] + -z[27] + -z[53];
z[14] = z[11] * z[14];
z[15] = abb[18] + z[20];
z[15] = -abb[22] + 3 * z[15] + -z[17];
z[15] = abb[0] * z[15];
z[6] = -2 * z[6] + -z[14] + z[15] + -z[16] + z[18];
z[14] = abb[27] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[8] + z[13] + z[14] + z[35];
z[2] = -abb[12] * z[7];
z[3] = z[20] + z[34];
z[4] = -z[3] + z[40];
z[7] = abb[18] + z[4] + z[47];
z[7] = abb[0] * z[7];
z[8] = -abb[18] + abb[20];
z[13] = -abb[24] + z[21];
z[14] = z[8] + z[13];
z[14] = abb[22] + 2 * z[14] + -z[45];
z[14] = abb[2] * z[14];
z[15] = -z[21] + z[28];
z[16] = abb[20] + -abb[23] + z[15];
z[16] = z[11] * z[16];
z[17] = abb[7] * z[54];
z[17] = 2 * z[17];
z[7] = z[7] + z[10] + z[14] + z[16] + z[17] + -z[25];
z[7] = abb[13] * z[7];
z[10] = abb[8] * z[44];
z[5] = z[5] + z[10] + -z[33];
z[8] = -z[8] + z[13];
z[8] = abb[23] + 2 * z[8] + -z[51];
z[8] = abb[0] * z[8];
z[3] = -z[3] + z[36];
z[10] = abb[20] + z[3] + z[52];
z[10] = abb[2] * z[10];
z[13] = abb[18] + -abb[22] + z[15];
z[13] = z[11] * z[13];
z[8] = -z[5] + z[8] + z[10] + z[13] + -z[17] + z[57];
z[8] = abb[11] * z[8];
z[4] = -z[4] + -z[30] + -z[53];
z[4] = abb[0] * z[4];
z[3] = -z[3] + -z[22] + -z[49];
z[3] = abb[2] * z[3];
z[0] = z[0] + z[9];
z[0] = z[0] * z[11];
z[0] = z[0] + z[3] + z[4] + z[5] + -z[24];
z[0] = abb[14] * z[0];
z[0] = z[0] + z[2] + z[7] + z[8];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[16] * z[6];
z[3] = abb[17] * z[12];
z[0] = 2 * z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_46_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("26.373769617953217667716020207179467700365148014113187721891810058"),stof<T>("41.898597939731451129987253183441337752224748227662862548007745164")}, std::complex<T>{stof<T>("33.62188404722948466792582347305678920743084924471908427300399933"),stof<T>("-211.66196507500450055083955282584383528213104900712130431905297317")}, std::complex<T>{stof<T>("58.913388864566206565800439326398107036139518695173343635576621579"),stof<T>("69.525131143524278696168520150953893778548523699885597754512996055")}, std::complex<T>{stof<T>("36.972875356851305572128968448576604352429443594556765655566542541"),stof<T>("-82.29962708020841922761993782936017835616594867046359880692329042")}, std::complex<T>{stof<T>("-36.85339432915201201447618681762485695036579198520044115191725646"),stof<T>("52.637071061571859861364794159718644407672252435595051140478045808")}, std::complex<T>{stof<T>("-80.329740369799917464993057748295273681733510582586534857226661488"),stof<T>("25.611181058092712537581225861321427821154681516040708534912914545")}, std::complex<T>{stof<T>("-0.8629539153984897109865192203571166559181066931093198106642565075"),stof<T>("10.994991936042099162868879735204265761265421563968542170570635796")}, std::complex<T>{stof<T>("5.188521423284097173545332293007922738779999795547604887538974547"),stof<T>("-31.497863384434761426915167224305706600330124032957193082127117469")}, std::complex<T>{stof<T>("12.379588269826081215576188978738705078588384038455458632400072591"),stof<T>("-40.330736035232100338300946594399986655364254611090980700318994212")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_46_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_46_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-52.382219104125583088931925520357193911671952622499932687873647478"),stof<T>("-61.636011103887433877882181052780224115976576427195742835250824176")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_46_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_46_DLogXconstant_part(base_point<T>, kend);
	value += f_4_46_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_46_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_46_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_46_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_46_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_46_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_46_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
