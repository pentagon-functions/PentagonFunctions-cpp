/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_602.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_602_abbreviated (const std::array<T,31>& abb) {
T z[58];
z[0] = 5 * abb[25];
z[1] = 2 * abb[20];
z[2] = abb[22] + z[1];
z[3] = 8 * z[2];
z[4] = 2 * abb[21];
z[5] = z[0] + -z[3] + z[4];
z[6] = abb[23] + abb[24];
z[7] = 2 * abb[19];
z[8] = z[5] + z[6] + z[7];
z[9] = 2 * abb[1];
z[8] = z[8] * z[9];
z[10] = 5 * abb[20];
z[11] = 2 * abb[22];
z[12] = z[10] + z[11];
z[13] = 2 * z[12];
z[14] = -abb[25] + z[13];
z[15] = -z[6] + z[14];
z[16] = 3 * abb[21];
z[17] = 3 * abb[19];
z[18] = z[16] + z[17];
z[19] = -z[15] + z[18];
z[20] = abb[6] * z[19];
z[21] = -abb[8] + abb[9];
z[21] = abb[27] * z[21];
z[21] = z[20] + z[21];
z[22] = 4 * z[2];
z[23] = abb[19] + abb[21];
z[24] = 3 * abb[25];
z[25] = z[22] + -z[23] + -z[24];
z[26] = 2 * z[25];
z[27] = abb[3] * z[26];
z[28] = abb[19] + abb[24];
z[29] = abb[20] + abb[22];
z[30] = -abb[25] + z[29];
z[30] = 2 * z[30];
z[31] = z[28] + z[30];
z[31] = abb[0] * z[31];
z[32] = abb[21] + abb[23];
z[30] = z[30] + z[32];
z[33] = abb[2] * z[30];
z[34] = 2 * z[2];
z[35] = -z[24] + z[34];
z[36] = z[32] + z[35];
z[37] = abb[4] * z[36];
z[8] = z[8] + -z[21] + -z[27] + z[31] + z[33] + z[37];
z[27] = 2 * abb[12];
z[8] = z[8] * z[27];
z[31] = abb[1] * z[25];
z[31] = 4 * z[31];
z[33] = z[20] + z[31];
z[38] = z[33] + -2 * z[37];
z[39] = 9 * abb[25];
z[3] = -z[3] + -z[6] + z[39];
z[3] = abb[3] * z[3];
z[6] = abb[25] * (T(3) / T(2));
z[40] = z[1] + z[6];
z[41] = 3 * abb[24] + -z[40];
z[42] = 2 * abb[23];
z[43] = z[41] + z[42];
z[43] = abb[2] * z[43];
z[44] = 3 * abb[23];
z[40] = -z[40] + z[44];
z[45] = 2 * abb[24];
z[46] = z[40] + z[45];
z[46] = abb[0] * z[46];
z[47] = abb[8] * (T(3) / T(2));
z[48] = -abb[9] + z[47];
z[48] = abb[27] * z[48];
z[3] = z[3] + -z[38] + z[43] + z[46] + z[48];
z[3] = abb[13] * z[3];
z[3] = z[3] + -z[8];
z[14] = z[14] + -z[18] + -z[42];
z[14] = abb[4] * z[14];
z[18] = -abb[25] + z[34];
z[18] = -abb[21] + -5 * abb[23] + z[7] + 2 * z[18];
z[18] = abb[0] * z[18];
z[14] = -z[14] + z[18];
z[18] = abb[24] + z[17];
z[22] = -abb[21] + abb[25] * (T(-5) / T(2)) + -z[18] + z[22] + z[42];
z[22] = abb[3] * z[22];
z[34] = z[9] * z[25];
z[43] = abb[21] + abb[24];
z[46] = -z[40] + -z[43];
z[46] = abb[2] * z[46];
z[49] = abb[8] * abb[26];
z[22] = -z[14] + z[20] + z[22] + z[34] + z[46] + -z[48] + z[49];
z[22] = abb[11] * z[22];
z[22] = -z[3] + z[22];
z[22] = abb[11] * z[22];
z[34] = abb[19] + abb[22];
z[46] = abb[21] * (T(5) / T(6));
z[50] = abb[24] * (T(1) / T(2));
z[34] = abb[25] * (T(-5) / T(6)) + abb[23] * (T(8) / T(3)) + -z[1] + (T(-1) / T(3)) * z[34] + z[46] + z[50];
z[34] = abb[2] * z[34];
z[51] = 4 * abb[20];
z[43] = abb[22] * (T(-5) / T(3)) + abb[19] * (T(7) / T(3)) + abb[25] * (T(7) / T(6)) + -z[42] + (T(2) / T(3)) * z[43] + -z[51];
z[43] = abb[3] * z[43];
z[12] = abb[19] + -abb[25] + z[12];
z[52] = 4 * abb[21];
z[12] = -16 * abb[23] + 5 * z[12] + -z[52];
z[12] = abb[0] * z[12];
z[53] = 2 * abb[9];
z[54] = abb[8] * (T(7) / T(2)) + -z[53];
z[54] = abb[27] * z[54];
z[12] = z[12] + z[54];
z[15] = (T(1) / T(3)) * z[15] + -z[23];
z[15] = abb[6] * z[15];
z[54] = abb[20] * (T(-13) / T(3)) + -z[11];
z[46] = abb[23] * (T(-5) / T(6)) + abb[24] * (T(1) / T(6)) + abb[19] * (T(7) / T(6)) + abb[25] * (T(11) / T(3)) + z[46] + 2 * z[54];
z[46] = abb[1] * z[46];
z[54] = abb[19] + abb[23];
z[54] = -11 * abb[20] + abb[22] * (T(-13) / T(3)) + abb[25] * (T(5) / T(3)) + abb[21] * (T(8) / T(3)) + (T(5) / T(2)) * z[54];
z[54] = abb[4] * z[54];
z[55] = -abb[26] + abb[27] * (T(5) / T(6));
z[55] = abb[7] * z[55];
z[12] = (T(1) / T(3)) * z[12] + 2 * z[15] + z[34] + z[43] + z[46] + -z[49] + z[54] + z[55];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[15] = abb[23] + -abb[24];
z[34] = z[15] + z[26];
z[34] = abb[3] * z[34];
z[40] = z[4] + z[40];
z[40] = abb[0] * z[40];
z[43] = 3 * abb[20];
z[46] = z[11] + z[43];
z[54] = abb[25] * (T(9) / T(2));
z[46] = 2 * z[46] + -z[54];
z[42] = abb[24] + z[42];
z[55] = z[42] + z[46];
z[55] = abb[2] * z[55];
z[47] = abb[9] + z[47];
z[47] = abb[27] * z[47];
z[56] = 2 * abb[26];
z[57] = abb[8] * z[56];
z[34] = -z[34] + -z[38] + z[40] + -z[47] + z[55] + z[57];
z[38] = abb[11] * z[34];
z[26] = z[15] + -z[26];
z[26] = abb[3] * z[26];
z[40] = abb[23] + z[45];
z[46] = z[40] + z[46];
z[46] = abb[0] * z[46];
z[41] = z[7] + z[41];
z[41] = abb[2] * z[41];
z[26] = z[26] + -z[33] + z[41] + z[46] + z[48];
z[26] = abb[13] * z[26];
z[33] = -abb[0] + -abb[2] + z[9];
z[25] = z[25] * z[33];
z[2] = z[2] + -z[23];
z[2] = abb[3] * z[2];
z[23] = 3 * abb[27];
z[33] = abb[8] * z[23];
z[2] = 4 * z[2] + z[25] + z[33] + -3 * z[49];
z[2] = abb[14] * z[2];
z[23] = z[23] + -z[56];
z[23] = abb[7] * z[23];
z[25] = -abb[11] + abb[13];
z[25] = z[23] * z[25];
z[2] = z[2] + -z[8] + z[25] + z[26] + z[38];
z[2] = abb[14] * z[2];
z[8] = abb[9] * z[19];
z[13] = abb[24] + -z[13] + z[54];
z[13] = abb[8] * z[13];
z[19] = abb[27] * (T(3) / T(2));
z[25] = z[19] + -z[56];
z[25] = abb[2] * z[25];
z[26] = abb[3] + -abb[10];
z[33] = 4 * abb[26];
z[26] = z[26] * z[33];
z[33] = abb[3] + abb[0] * (T(1) / T(2));
z[33] = -abb[6] + 3 * z[33];
z[33] = abb[27] * z[33];
z[38] = abb[0] * z[56];
z[15] = abb[7] * z[15];
z[8] = z[8] + -z[13] + z[15] + -z[25] + z[26] + -z[33] + z[38];
z[13] = -abb[29] * z[8];
z[15] = 15 * abb[20] + 7 * abb[22];
z[15] = 2 * z[15] + -z[39];
z[25] = 4 * abb[23];
z[26] = -z[15] + z[18] + z[25] + z[52];
z[9] = z[9] * z[26];
z[26] = abb[22] + z[43];
z[33] = 2 * abb[25];
z[38] = abb[21] + z[26] + -z[33];
z[38] = z[17] + 2 * z[38] + -z[44];
z[38] = abb[4] * z[38];
z[20] = 2 * z[20];
z[39] = 7 * abb[20] + 3 * abb[22];
z[41] = -z[33] + z[39];
z[43] = -abb[21] + z[41];
z[18] = -z[18] + 2 * z[43];
z[43] = 2 * abb[3];
z[18] = z[18] * z[43];
z[33] = 3 * z[29] + -z[33];
z[33] = 2 * z[33];
z[43] = -abb[23] + z[17] + z[33];
z[43] = abb[0] * z[43];
z[9] = z[9] + -z[18] + -z[20] + z[38] + z[43];
z[18] = 2 * abb[27];
z[38] = -abb[9] * z[18];
z[38] = z[9] + z[38];
z[38] = abb[28] * z[38];
z[43] = z[7] + z[32];
z[44] = 9 * abb[20];
z[46] = 5 * abb[22] + -z[24] + z[44];
z[46] = -z[43] + -z[45] + 2 * z[46];
z[46] = abb[3] * z[46];
z[47] = 4 * abb[19];
z[48] = 17 * abb[20] + 9 * abb[22];
z[0] = -5 * abb[21] + -9 * abb[23] + -6 * abb[24] + -z[0] + -z[47] + 2 * z[48];
z[0] = abb[1] * z[0];
z[48] = abb[21] * (T(1) / T(2));
z[49] = abb[23] + -z[29] + -z[48] + z[50];
z[49] = abb[2] * z[49];
z[44] = -4 * abb[22] + -z[44];
z[17] = abb[21] + 7 * abb[25] + z[17] + 2 * z[44];
z[17] = abb[4] * z[17];
z[11] = -abb[19] + -abb[25] + -z[11] + z[40];
z[11] = abb[0] * z[11];
z[0] = z[0] + z[11] + z[17] + z[46] + z[49];
z[11] = prod_pow(abb[12], 2);
z[0] = z[0] * z[11];
z[17] = abb[23] + z[16];
z[44] = 4 * abb[24];
z[15] = -z[15] + z[17] + z[44] + z[47];
z[15] = abb[1] * z[15];
z[7] = z[7] + z[17] + -2 * z[41];
z[7] = abb[3] * z[7];
z[17] = z[28] + z[35];
z[17] = abb[0] * z[17];
z[7] = z[7] + z[15] + z[17] + -z[21];
z[7] = abb[15] * z[7];
z[15] = z[24] + -2 * z[39] + z[43] + z[44];
z[15] = abb[1] * z[15];
z[6] = -z[6] + 2 * z[26];
z[17] = abb[19] + -z[6] + z[45];
z[17] = abb[3] * z[17];
z[6] = z[6] + -z[40];
z[6] = abb[0] * z[6];
z[10] = abb[19] + -abb[22] + abb[24] * (T(9) / T(2)) + -z[10] + -z[48];
z[10] = abb[2] * z[10];
z[6] = z[6] + z[10] + z[15] + z[17];
z[6] = abb[13] * z[6];
z[10] = 2 * z[29];
z[15] = abb[25] + -z[10] + z[32] + -z[45];
z[15] = abb[1] * z[15];
z[17] = -abb[3] * z[30];
z[1] = abb[25] + -z[1];
z[1] = abb[0] * z[1];
z[1] = z[1] + z[15] + z[17] + z[37];
z[10] = -abb[21] + -z[10] + z[42];
z[10] = abb[2] * z[10];
z[1] = 2 * z[1] + z[10];
z[1] = abb[12] * z[1];
z[1] = z[1] + z[6];
z[1] = abb[13] * z[1];
z[6] = 2 * z[36];
z[10] = abb[28] * z[6];
z[15] = -abb[24] + z[16] + z[33];
z[15] = abb[15] * z[15];
z[10] = z[10] + z[15];
z[10] = abb[2] * z[10];
z[15] = abb[15] + -abb[28];
z[11] = z[11] + 2 * z[15];
z[11] = abb[27] * z[11];
z[15] = -abb[27] * z[27];
z[16] = -abb[26] + z[19];
z[16] = abb[13] * z[16];
z[15] = z[15] + z[16];
z[15] = abb[13] * z[15];
z[16] = abb[26] + abb[27] * (T(-1) / T(2));
z[16] = prod_pow(abb[11], 2) * z[16];
z[11] = z[11] + z[15] + z[16];
z[11] = abb[7] * z[11];
z[0] = abb[30] + z[0] + z[1] + z[2] + 2 * z[7] + z[10] + z[11] + z[12] + z[13] + z[22] + z[38];
z[1] = 6 * abb[23] + z[4] + -z[24] + z[45] + -z[51];
z[1] = abb[2] * z[1];
z[2] = 6 * abb[19] + z[5] + -z[25] + z[45];
z[2] = abb[3] * z[2];
z[4] = 3 * abb[8] + -z[53];
z[4] = abb[27] * z[4];
z[5] = abb[27] + -z[56];
z[5] = abb[7] * z[5];
z[1] = z[1] + z[2] + z[4] + z[5] + 2 * z[14] + -z[20] + -z[31] + -z[57];
z[1] = abb[11] * z[1];
z[2] = z[23] + -z[34];
z[2] = abb[14] * z[2];
z[1] = z[1] + z[2] + z[3];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[17] * z[8];
z[3] = -abb[7] + -abb[9];
z[3] = z[3] * z[18];
z[4] = abb[2] * z[6];
z[3] = z[3] + z[4] + z[9];
z[3] = abb[16] * z[3];
z[1] = abb[18] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_602_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-10.112112346992752960266613395731485815741126306582950092078292575"),stof<T>("-30.716664529873359908527552785599650196433477927352389853051729077")}, std::complex<T>{stof<T>("-6.499992666735887191859315870924777897112448320658681206028828798"),stof<T>("-62.558748016781812588194622264061122232967450333318203479860607844")}, std::complex<T>{stof<T>("-31.113095909067721035000930291619538276015715549176640373288752608"),stof<T>("4.986682365396501359114418353286692219132182170358630187579093286")}, std::complex<T>{stof<T>("-2.896377341037651041252037782183296488097600955264854458864392019"),stof<T>("-22.83981944256428762951070614154787180809097283237875741489151295")}, std::complex<T>{stof<T>("-21.804907541957397219019670339307025867734341384099572171577690849"),stof<T>("45.438129255056121083736377492138134709834551733670996671072215437")}, std::complex<T>{stof<T>("34.765054232376879203073264948791413928301815780258921921486782415"),stof<T>("21.4253004631012750269356896021179727502170971783204832371305141")}, std::complex<T>{stof<T>("-7.550085787527186263938188022613937226932700122995932218562798149"),stof<T>("12.295809026369257714212863899525075084789830362879721057449720932")}, std::complex<T>{stof<T>("16.455932411337816050645652107193010976983846586980106992946392049"),stof<T>("-12.103298424608391321179545592823121811900601203005414622249180586")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_602_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_602_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,5> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(16)) * (v[2] + v[3]) * (8 + -v[2] + -v[3] + 4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(8)) * (v[2] + v[3]) * (-56 + -16 * v[0] + -8 * v[1] + 11 * v[2] + 19 * v[3] + 8 * v[4] + -16 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -28 * v[5])) / prod_pow(tend, 2);
c[2] = ((T(-1) / T(16)) * (v[2] + v[3]) * (16 * v[0] + 8 * v[1] + 17 * v[2] + 9 * v[3] + -8 * (5 + v[4]) + -20 * v[5])) / prod_pow(tend, 2);
c[3] = (-(1 + m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;
c[4] = ((T(3) / T(2)) * (v[2] + v[3])) / tend;


		return abb[22] * (t * c[1] + -8 * c[3] + -6 * c[4]) + abb[24] * (t * c[2] + c[4]) + abb[21] * (t * c[0] + 3 * c[3] + c[4]) + abb[25] * (t * (4 * c[0] + 5 * c[1] + 2 * c[2]) + -4 * (7 * c[3] + 6 * c[4])) * (T(-1) / T(4)) + abb[20] * (9 * t * c[1] + -2 * t * c[2] + -72 * c[3] + -56 * c[4]) * (T(1) / T(4)) + abb[19] * (c[3] + t * (4 * c[0] + c[1] + 2 * c[2]) * (T(1) / T(4)));
	}
	{
T z[7];
z[0] = 3 * abb[21];
z[1] = 2 * abb[19];
z[2] = 2 * abb[22];
z[3] = 6 * abb[20] + -3 * abb[24] + -4 * abb[25] + z[0] + z[1] + z[2];
z[3] = abb[15] * z[3];
z[4] = 3 * abb[25];
z[5] = -abb[19] + -4 * abb[20] + -abb[24] + -z[2] + z[4];
z[6] = abb[11] + -abb[14];
z[5] = z[5] * z[6];
z[5] = 2 * z[5];
z[4] = 13 * abb[20] + 5 * abb[22] + abb[24] * (T(-7) / T(2)) + abb[21] * (T(-3) / T(2)) + -z[1] + -z[4];
z[4] = abb[13] * z[4];
z[4] = z[4] + z[5];
z[4] = abb[13] * z[4];
z[6] = abb[20] + -abb[25];
z[0] = 5 * abb[24] + -z[0] + z[2] + 2 * z[6];
z[0] = abb[13] * z[0];
z[1] = -15 * abb[20] + -7 * abb[22] + 5 * abb[25] + abb[24] * (T(-3) / T(2)) + abb[21] * (T(9) / T(2)) + z[1];
z[1] = abb[12] * z[1];
z[0] = z[0] + z[1] + -z[5];
z[0] = abb[12] * z[0];
z[0] = z[0] + z[3] + z[4];
return abb[5] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_602_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[19] + 4 * abb[20] + 2 * abb[22] + abb[24] + -3 * abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[19] + -4 * abb[20] + -2 * abb[22] + -abb[24] + 3 * abb[25];
z[1] = abb[12] + -abb[13];
return 2 * abb[5] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_602_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("3.346187233104430897595190003797925266095490384787505100739566465"),stof<T>("34.569635356976266494622085032758195920685700215274317799905080039")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_13(k), f_2_4_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_602_W_19_Im(t, path, abb);
abb[30] = SpDLog_f_4_602_W_19_Re(t, path, abb);

                    
            return f_4_602_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_602_DLogXconstant_part(base_point<T>, kend);
	value += f_4_602_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_602_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_602_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_602_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_602_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_602_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_602_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
