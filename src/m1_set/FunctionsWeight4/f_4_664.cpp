/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_664.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_664_abbreviated (const std::array<T,63>& abb) {
T z[101];
z[0] = abb[48] + abb[51];
z[1] = 3 * abb[52];
z[2] = abb[45] + abb[46] * (T(-13) / T(3)) + (T(11) / T(3)) * z[0] + -z[1];
z[3] = -abb[43] + abb[50];
z[4] = abb[53] * (T(1) / T(3));
z[5] = abb[47] * (T(3) / T(4));
z[2] = abb[49] + abb[44] * (T(-1) / T(3)) + (T(1) / T(4)) * z[2] + (T(2) / T(3)) * z[3] + -z[4] + -z[5];
z[2] = abb[4] * z[2];
z[6] = -abb[54] + abb[55];
z[7] = (T(1) / T(2)) * z[6];
z[8] = abb[23] + abb[24];
z[8] = z[7] * z[8];
z[9] = abb[46] + -abb[51];
z[10] = abb[45] + z[9];
z[11] = abb[48] + -abb[52];
z[12] = z[10] + z[11];
z[13] = abb[47] + z[12];
z[13] = -z[3] + (T(1) / T(2)) * z[13];
z[14] = abb[14] * z[13];
z[8] = z[8] + z[14];
z[15] = abb[44] * (T(2) / T(3));
z[16] = abb[49] + z[11];
z[17] = (T(-2) / T(3)) * z[9] + -z[15] + -z[16];
z[17] = abb[0] * z[17];
z[18] = abb[47] + abb[53];
z[19] = -abb[48] + abb[51];
z[15] = abb[45] + z[15] + (T(-2) / T(3)) * z[18] + (T(-1) / T(3)) * z[19];
z[15] = abb[6] * z[15];
z[20] = abb[44] + abb[47];
z[21] = 2 * abb[49];
z[20] = abb[52] * (T(-5) / T(2)) + abb[46] * (T(-1) / T(3)) + abb[51] * (T(1) / T(2)) + abb[48] * (T(7) / T(3)) + z[4] + (T(-1) / T(6)) * z[20] + z[21];
z[20] = abb[1] * z[20];
z[22] = 3 * abb[51];
z[23] = 3 * abb[45];
z[24] = abb[52] + abb[48] * (T(-5) / T(3)) + abb[46] * (T(11) / T(3)) + -z[22] + -z[23];
z[4] = abb[47] * (T(-5) / T(12)) + abb[44] * (T(4) / T(3)) + z[3] + z[4] + (T(1) / T(4)) * z[24];
z[4] = abb[7] * z[4];
z[24] = abb[22] * z[6];
z[25] = abb[20] * z[6];
z[25] = z[24] + z[25];
z[10] = z[10] + -z[11];
z[10] = (T(1) / T(2)) * z[10];
z[26] = abb[47] * (T(1) / T(2));
z[27] = abb[49] + z[26];
z[28] = -abb[44] + -z[10] + z[27];
z[29] = abb[13] * z[28];
z[30] = abb[19] * z[7];
z[30] = -z[29] + z[30];
z[12] = -abb[47] + z[12];
z[31] = abb[44] + (T(1) / T(2)) * z[12];
z[32] = abb[8] * z[31];
z[33] = abb[47] + z[9];
z[34] = -z[3] + z[33];
z[35] = abb[2] * z[34];
z[36] = 3 * abb[46];
z[37] = -abb[52] + -z[0] + z[36];
z[38] = -5 * abb[45] + z[37];
z[39] = abb[53] + z[5] + (T(1) / T(4)) * z[38];
z[40] = abb[3] * z[39];
z[41] = abb[21] * z[7];
z[2] = z[2] + z[4] + z[8] + z[15] + z[17] + z[20] + (T(5) / T(4)) * z[25] + z[30] + (T(-5) / T(2)) * z[32] + (T(-1) / T(3)) * z[35] + z[40] + z[41];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[4] = 4 * abb[53];
z[15] = 2 * abb[47];
z[17] = 3 * abb[49];
z[20] = -5 * abb[44] + z[1] + -z[4] + z[15] + z[17] + z[19] + -z[36];
z[20] = abb[1] * z[20];
z[25] = z[1] + z[23];
z[40] = -abb[48] + z[25];
z[42] = 5 * abb[51];
z[36] = z[36] + z[40] + -z[42];
z[43] = (T(1) / T(4)) * z[36];
z[44] = abb[47] * (T(1) / T(4));
z[45] = abb[53] + z[44];
z[46] = abb[44] + z[43] + -z[45];
z[47] = abb[7] * z[46];
z[48] = (T(3) / T(4)) * z[6];
z[49] = abb[21] * z[48];
z[47] = z[47] + z[49];
z[50] = -abb[46] + -z[0] + z[1];
z[51] = z[23] + z[50];
z[52] = 2 * abb[53];
z[53] = z[26] + z[52];
z[54] = (T(1) / T(2)) * z[51] + -z[53];
z[55] = abb[12] * z[54];
z[46] = -abb[6] * z[46];
z[56] = abb[44] + abb[46];
z[57] = -abb[53] + abb[49] * (T(1) / T(2));
z[58] = abb[45] + abb[52];
z[59] = -abb[51] + -z[58];
z[59] = z[56] + -z[57] + (T(1) / T(2)) * z[59];
z[60] = 3 * abb[9];
z[59] = z[59] * z[60];
z[61] = -abb[45] + z[9] + z[11];
z[27] = z[27] + (T(1) / T(2)) * z[61];
z[61] = abb[5] * z[27];
z[62] = abb[23] * z[7];
z[62] = z[61] + z[62];
z[63] = z[24] + z[62];
z[64] = 3 * z[32];
z[37] = -abb[45] + z[37];
z[44] = -abb[44] + abb[49] + -abb[53] + (T(-1) / T(4)) * z[37] + z[44];
z[44] = abb[4] * z[44];
z[20] = z[20] + 3 * z[44] + z[46] + -z[47] + -z[55] + z[59] + (T(-3) / T(2)) * z[63] + z[64];
z[20] = abb[28] * z[20];
z[44] = abb[7] * z[54];
z[6] = (T(3) / T(2)) * z[6];
z[46] = abb[20] * z[6];
z[59] = z[46] + -z[55];
z[65] = z[44] + -z[59];
z[66] = -abb[47] + z[52];
z[67] = -z[17] + z[66];
z[68] = 2 * abb[46];
z[69] = -z[0] + z[68];
z[70] = 3 * abb[44] + z[67] + z[69];
z[71] = 2 * abb[4];
z[72] = z[70] * z[71];
z[73] = -abb[49] + z[52];
z[56] = z[56] + -z[58] + z[73];
z[56] = z[56] * z[60];
z[74] = abb[6] * z[54];
z[70] = abb[1] * z[70];
z[75] = abb[19] * z[6];
z[75] = 3 * z[29] + -z[75];
z[72] = -z[56] + z[65] + 3 * z[70] + z[72] + z[74] + z[75];
z[76] = -abb[30] * z[72];
z[77] = 2 * abb[44];
z[36] = (T(1) / T(2)) * z[36] + -z[53] + z[77];
z[53] = abb[6] + abb[7];
z[36] = z[36] * z[53];
z[53] = abb[44] + z[66];
z[66] = 2 * abb[48] + abb[51] + -z[1];
z[78] = z[53] + z[66];
z[78] = abb[1] * z[78];
z[78] = -z[64] + z[78];
z[79] = 5 * abb[46];
z[80] = -z[25] + z[79];
z[81] = 7 * abb[51];
z[82] = 5 * abb[48];
z[83] = -z[80] + z[81] + -z[82];
z[84] = abb[47] * (T(5) / T(2));
z[83] = -6 * abb[49] + z[52] + (T(1) / T(2)) * z[83] + -z[84];
z[83] = abb[12] * z[83];
z[85] = -abb[51] + z[58];
z[73] = z[73] + -z[85];
z[86] = z[60] * z[73];
z[87] = abb[22] * z[7];
z[88] = z[62] + z[87];
z[89] = abb[21] * z[6];
z[36] = z[36] + z[78] + z[83] + -z[86] + 3 * z[88] + z[89];
z[36] = abb[27] * z[36];
z[20] = z[20] + z[36] + z[76];
z[20] = abb[28] * z[20];
z[36] = z[3] + -z[52];
z[76] = abb[49] * (T(3) / T(2));
z[83] = abb[44] * (T(1) / T(2));
z[50] = z[36] + (T(1) / T(2)) * z[50] + z[76] + -z[83];
z[50] = abb[0] * z[50];
z[88] = 2 * z[35];
z[90] = -z[49] + z[88];
z[50] = (T(3) / T(2)) * z[8] + z[50] + -z[90];
z[91] = z[42] + z[82];
z[25] = 13 * abb[46] + -z[25] + -z[91];
z[92] = abb[43] * (T(1) / T(2));
z[93] = abb[50] * (T(1) / T(2));
z[94] = z[92] + -z[93];
z[4] = 4 * abb[44] + abb[49] * (T(-9) / T(2)) + z[4] + -z[5] + (T(1) / T(4)) * z[25] + z[94];
z[4] = abb[4] * z[4];
z[25] = z[5] + -z[52];
z[95] = abb[46] + 9 * abb[52] + z[23] + -z[91];
z[94] = z[25] + -z[83] + z[94] + (T(1) / T(4)) * z[95];
z[94] = abb[7] * z[94];
z[29] = (T(3) / T(2)) * z[29];
z[4] = z[4] + z[29] + -z[50] + -z[56] + -z[59] + 2 * z[70] + z[94];
z[4] = abb[30] * z[4];
z[56] = abb[24] * z[7];
z[14] = z[14] + z[56] + -z[61];
z[56] = 3 * z[11];
z[61] = z[17] + z[56];
z[94] = abb[44] + z[9];
z[95] = z[61] + z[94];
z[95] = abb[0] * z[95];
z[95] = -3 * z[14] + z[95];
z[96] = z[29] + z[70];
z[97] = abb[19] * z[48];
z[98] = z[96] + -z[97];
z[93] = z[77] + z[93];
z[76] = z[76] + z[92] + -z[93];
z[92] = -abb[46] + abb[47] + -abb[53] + (T(1) / T(2)) * z[0] + z[76];
z[92] = abb[4] * z[92];
z[99] = abb[52] + z[3];
z[18] = abb[48] + abb[46] * (T(1) / T(2)) + z[18] + -z[83] + (T(-3) / T(2)) * z[99];
z[18] = abb[7] * z[18];
z[57] = z[57] + (T(1) / T(2)) * z[85];
z[57] = z[57] * z[60];
z[51] = z[45] + (T(-1) / T(4)) * z[51];
z[51] = abb[6] * z[51];
z[18] = z[18] + z[35] + z[51] + -z[57] + z[92] + (T(1) / T(2)) * z[95] + -z[98];
z[18] = abb[26] * z[18];
z[51] = abb[28] * z[72];
z[72] = abb[4] * z[54];
z[83] = z[72] + z[74] + z[89];
z[85] = 2 * abb[51];
z[92] = abb[46] + abb[48];
z[99] = z[67] + z[85] + -z[92];
z[100] = 2 * abb[12];
z[99] = z[99] * z[100];
z[44] = -z[44] + -3 * z[62] + -z[83] + z[86] + -z[99];
z[86] = abb[27] * z[44];
z[100] = 3 * z[8];
z[66] = -z[36] + z[66];
z[66] = abb[0] * z[66];
z[66] = 4 * z[35] + z[66] + -z[100];
z[34] = abb[7] * z[34];
z[34] = 2 * z[34] + -z[59] + z[66] + -z[83];
z[34] = abb[29] * z[34];
z[83] = abb[30] * z[97];
z[4] = z[4] + z[18] + -z[34] + z[51] + -z[83] + z[86];
z[4] = abb[26] * z[4];
z[18] = 3 * z[39];
z[39] = abb[4] * z[18];
z[39] = -z[39] + z[57] + (T(3) / T(2)) * z[62];
z[5] = -abb[53] + -z[3] + z[5] + z[43];
z[5] = abb[7] * z[5];
z[38] = abb[47] * (T(3) / T(2)) + (T(1) / T(2)) * z[38] + z[52];
z[43] = abb[3] * z[38];
z[51] = 3 * z[43];
z[36] = z[17] + z[36];
z[57] = -z[19] + z[36];
z[57] = abb[0] * z[57];
z[18] = abb[6] * z[18];
z[5] = z[5] + z[18] + -z[35] + -z[39] + -z[49] + -z[51] + z[57] + -z[59];
z[5] = abb[29] * z[5];
z[15] = z[15] + -z[23] + z[52] + z[69];
z[18] = z[15] * z[71];
z[15] = abb[6] * z[15];
z[18] = 2 * z[15] + z[18] + -z[51] + z[65];
z[18] = abb[30] * z[18];
z[5] = z[5] + -z[18];
z[5] = abb[29] * z[5];
z[49] = 9 * abb[46] + -z[40] + -z[81];
z[45] = z[45] + (T(1) / T(4)) * z[49] + z[77];
z[45] = abb[6] * z[45];
z[19] = abb[44] + z[19] + z[67];
z[19] = abb[1] * z[19];
z[19] = z[19] + -z[39] + z[45] + -z[47] + -z[99];
z[19] = abb[27] * z[19];
z[39] = z[22] + -z[58] + -z[92];
z[21] = z[21] + z[26] + -z[52];
z[39] = -z[21] + (T(1) / T(2)) * z[39];
z[39] = abb[12] * z[39];
z[41] = z[41] + z[62];
z[39] = z[39] + z[41];
z[45] = abb[4] + abb[6];
z[38] = z[38] * z[45];
z[47] = abb[9] * z[73];
z[7] = abb[20] * z[7];
z[38] = z[7] + -z[38] + z[39] + z[43] + -z[47];
z[38] = 3 * z[38];
z[43] = abb[29] * z[38];
z[18] = z[18] + z[19] + z[43];
z[18] = abb[27] * z[18];
z[19] = z[16] + -z[94];
z[19] = abb[1] * z[19];
z[43] = abb[4] * z[28];
z[47] = abb[6] * z[31];
z[11] = abb[45] + z[11];
z[33] = -z[11] + z[33];
z[49] = abb[7] * (T(1) / T(2));
z[57] = z[33] * z[49];
z[32] = -z[32] + z[87];
z[30] = z[7] + z[30];
z[19] = z[19] + z[30] + z[32] + z[43] + z[47] + z[57];
z[19] = 3 * z[19];
z[43] = abb[58] * z[19];
z[0] = z[0] + -z[80];
z[0] = (T(1) / T(4)) * z[0] + z[25] + z[76];
z[0] = abb[4] * z[0];
z[25] = z[61] + -z[94];
z[25] = abb[0] * z[25];
z[47] = -z[25] + z[100];
z[23] = 3 * abb[47] + z[23] + z[56];
z[42] = -z[23] + z[42] + -z[79];
z[56] = -3 * z[3];
z[42] = -abb[44] + (T(1) / T(2)) * z[42] + -z[56];
z[42] = z[42] * z[49];
z[0] = z[0] + z[42] + (T(1) / T(2)) * z[47] + z[59] + -z[90] + -z[98];
z[0] = abb[26] * z[0];
z[10] = -abb[49] + z[10];
z[42] = -abb[43] + 3 * z[10] + -z[84];
z[42] = (T(1) / T(2)) * z[42] + z[93];
z[42] = abb[4] * z[42];
z[11] = -z[9] + z[11];
z[47] = 5 * abb[47] + -3 * z[11];
z[57] = abb[44] + z[3];
z[47] = (T(1) / T(2)) * z[47] + -z[57];
z[47] = z[47] * z[49];
z[47] = -z[42] + z[47] + -z[50] + -z[74] + -z[96];
z[47] = abb[30] * z[47];
z[49] = 2 * z[94];
z[50] = abb[7] * z[49];
z[49] = abb[6] * z[49];
z[50] = z[49] + z[50] + z[55] + -z[72];
z[6] = abb[22] * z[6];
z[6] = z[6] + z[50] + z[78];
z[55] = abb[27] * z[6];
z[34] = -z[34] + z[47] + z[55] + z[83];
z[6] = abb[28] * z[6];
z[47] = -abb[0] + abb[1] + -abb[6];
z[47] = z[47] * z[94];
z[55] = -abb[47] + z[57];
z[58] = abb[4] * z[55];
z[47] = z[35] + z[47] + z[58];
z[47] = abb[31] * z[47];
z[0] = z[0] + z[6] + -z[34] + z[47];
z[0] = abb[31] * z[0];
z[6] = abb[7] * z[31];
z[31] = abb[6] * z[33];
z[27] = abb[12] * z[27];
z[33] = abb[1] * z[16];
z[6] = z[6] + -z[27] + (T(1) / T(2)) * z[31] + z[33] + z[41];
z[31] = z[6] + z[32];
z[31] = abb[57] * z[31];
z[21] = z[21] + (T(-1) / T(2)) * z[37] + -z[77];
z[21] = abb[4] * z[21];
z[32] = abb[9] * z[94];
z[21] = z[21] + z[30] + z[32] + z[39] + -z[70];
z[21] = abb[32] * z[21];
z[21] = z[21] + z[31];
z[30] = abb[7] * z[13];
z[16] = abb[0] * z[16];
z[7] = z[7] + z[14] + -z[16] + z[27] + -z[30] + -z[35];
z[7] = 3 * z[7];
z[14] = abb[56] * z[7];
z[16] = z[36] + -z[69] + -z[77];
z[27] = -abb[0] * z[16];
z[30] = abb[7] + -z[71];
z[30] = z[30] * z[55];
z[15] = z[15] + z[27] + z[30] + -z[35] + -z[70];
z[15] = prod_pow(abb[30], 2) * z[15];
z[27] = abb[35] * z[44];
z[22] = z[22] + z[40] + -z[79];
z[22] = 2 * z[3] + (T(1) / T(2)) * z[22] + -z[52] + -z[84];
z[22] = abb[7] * z[22];
z[1] = 9 * abb[45] + -7 * abb[46] + -z[1] + z[91];
z[1] = abb[47] * (T(-7) / T(2)) + (T(1) / T(2)) * z[1] + -z[52];
z[1] = -z[1] * z[45];
z[1] = z[1] + z[22] + -z[51] + -z[66] + z[89];
z[1] = abb[33] * z[1];
z[22] = -abb[34] * z[38];
z[28] = abb[17] * z[28];
z[13] = abb[18] * z[13];
z[11] = abb[47] + z[11];
z[11] = (T(1) / T(2)) * z[11] + -z[57];
z[30] = abb[15] * z[11];
z[13] = -z[13] + z[28] + z[30];
z[3] = -z[3] + z[10] + z[26];
z[10] = abb[16] * (T(3) / T(2));
z[3] = z[3] * z[10];
z[10] = abb[25] * z[48];
z[3] = z[3] + -z[10] + (T(3) / T(2)) * z[13];
z[10] = -abb[59] * z[3];
z[13] = -abb[62] * z[54];
z[0] = abb[60] + abb[61] + z[0] + z[1] + z[2] + z[4] + z[5] + z[10] + z[13] + z[14] + z[15] + z[18] + z[20] + 3 * z[21] + z[22] + z[27] + z[43];
z[1] = -z[61] + z[68] + z[77] + -z[85];
z[1] = abb[1] * z[1];
z[2] = -z[8] + -z[24];
z[2] = 3 * z[2] + z[25];
z[4] = abb[7] * z[11];
z[1] = z[1] + (T(1) / T(2)) * z[2] + (T(3) / T(2)) * z[4] + z[29] + z[42] + -z[46] + -z[49] + z[64] + z[90] + -z[97];
z[1] = abb[31] * z[1];
z[2] = abb[4] * z[16];
z[4] = z[60] * z[94];
z[5] = -z[9] + -z[23];
z[5] = abb[44] + (T(1) / T(2)) * z[5] + -z[56];
z[5] = abb[7] * z[5];
z[2] = z[2] + z[4] + z[5] + z[59] + -z[70] + -z[88] + -z[95];
z[2] = abb[26] * z[2];
z[5] = -z[12] + -z[77];
z[5] = abb[8] * z[5];
z[5] = z[5] + z[63];
z[8] = abb[51] + -6 * abb[52] + z[17] + z[53] + z[82];
z[8] = abb[1] * z[8];
z[4] = -z[4] + 3 * z[5] + z[8] + z[46] + z[50] + -z[75] + z[89];
z[4] = abb[28] * z[4];
z[1] = z[1] + z[2] + z[4] + -z[34];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[38] * z[19];
z[4] = abb[36] * z[7];
z[5] = z[6] + z[87];
z[5] = 3 * z[5] + -z[64];
z[5] = abb[37] * z[5];
z[3] = -abb[39] * z[3];
z[6] = -abb[42] * z[54];
z[1] = abb[40] + abb[41] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_664_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.9199712708610032646570607360206978943387699704876487992275079596"),stof<T>("-2.3683656031641206184443785616499261670486199736651258493215837874")}, std::complex<T>{stof<T>("-1.926546076142315960506474136442282937262294152631277069540486659"),stof<T>("-15.797566346568351308716995489891197084094153174661371118569168943")}, std::complex<T>{stof<T>("0.8090152622118106713088205189275937870807318983997179292064373884"),stof<T>("10.879288800354537126384607810000054925551118483372207518027185407")}, std::complex<T>{stof<T>("2.728986533072813935965881254948291681419501868887366728433945348"),stof<T>("13.2476544035186577448289863716499810925997384570373333673487691944")}, std::complex<T>{stof<T>("3.5756564364446030331447494388019930518598719780176358625104872301"),stof<T>("-3.2101481916672582729114674947237806255119606920142127017833134406")}, std::complex<T>{stof<T>("7.1070834264479149445700378122562934774365815631610922498378912673"),stof<T>("7.4875942688017059080295097586849844755933630473990829143450560048")}, std::complex<T>{stof<T>("-1.6114557191423086467682276374336025312379396146558075880998960776"),stof<T>("-8.329376857304843562496598691758838934056703765748169766806785658")}, std::complex<T>{stof<T>("-2.7732159795141050576853423202959843077026642617615462036170285409"),stof<T>("0.6602362486175647090234583764825646340175459743901749505629136915")}, std::complex<T>{stof<T>("-1.6048809138609959509188142370120174883144154325121793177869173784"),stof<T>("5.0998238860993871277760182364824319829888294352480755024407994981")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(abs(k.W[71])) - rlog(abs(kbase.W[71])), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_664_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_664_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (-v[3] + v[5])) / tend;


		return (abb[45] + -abb[46] + -abb[47] + -abb[49] + abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[29], 2);
z[1] = -prod_pow(abb[27], 2);
z[0] = z[0] + z[1];
z[1] = -abb[45] + abb[46] + abb[47] + abb[49] + -abb[51];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_664_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_664_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[2] + v[3]) * (-8 + 8 * v[0] + 4 * v[1] + 3 * v[2] + -v[3] + -4 * v[4] + -2 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = (-(1 + m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (abb[43] + 3 * abb[45] + -abb[46] + -abb[47] + abb[48] + -abb[50] + -2 * abb[53]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[5];
z[0] = -abb[43] + -3 * abb[45] + abb[46] + abb[47] + -abb[48] + abb[50] + 2 * abb[53];
z[1] = abb[31] * z[0];
z[2] = abb[29] * z[0];
z[3] = -z[1] + z[2];
z[3] = abb[29] * z[3];
z[4] = abb[30] * z[0];
z[1] = z[1] + -z[4];
z[1] = abb[30] * z[1];
z[2] = z[2] + -z[4];
z[2] = abb[26] * z[2];
z[0] = abb[33] * z[0];
z[0] = z[0] + z[1] + z[2] + z[3];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_664_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(8)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[43] + 3 * abb[45] + -abb[46] + -abb[47] + abb[48] + -abb[50] + -2 * abb[53]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = abb[43] + 3 * abb[45] + -abb[46] + -abb[47] + abb[48] + -abb[50] + -2 * abb[53];
z[1] = abb[29] + -abb[30];
return abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_664_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2.7324371176930167648795330290797422262601471755726559175486423585"),stof<T>("-3.2220739863461210789907795984768614188744419325826183336426377993")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18, 71});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(abs(kend.W[71])) - rlog(abs(k.W[71])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[40] = SpDLog_f_4_664_W_16_Im(t, path, abb);
abb[41] = SpDLog_f_4_664_W_19_Im(t, path, abb);
abb[42] = SpDLogQ_W_72(k,dl,dlr).imag();
abb[60] = SpDLog_f_4_664_W_16_Re(t, path, abb);
abb[61] = SpDLog_f_4_664_W_19_Re(t, path, abb);
abb[62] = SpDLogQ_W_72(k,dl,dlr).real();

                    
            return f_4_664_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_664_DLogXconstant_part(base_point<T>, kend);
	value += f_4_664_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_664_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_664_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_664_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_664_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_664_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_664_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
