/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_648.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_648_abbreviated (const std::array<T,73>& abb) {
T z[164];
z[0] = abb[51] + abb[56];
z[1] = abb[48] + z[0];
z[2] = 2 * abb[50];
z[3] = z[1] + -z[2];
z[4] = 4 * abb[49];
z[5] = 2 * abb[52];
z[6] = -z[3] + z[4] + z[5];
z[6] = abb[16] * z[6];
z[7] = abb[62] + abb[63];
z[8] = abb[27] * z[7];
z[6] = z[6] + -z[8];
z[8] = abb[22] * z[7];
z[9] = abb[48] + abb[51];
z[10] = -abb[56] + z[9];
z[11] = abb[15] * z[10];
z[8] = z[8] + -z[11];
z[12] = abb[57] * (T(1) / T(2));
z[13] = abb[51] * (T(1) / T(2));
z[14] = z[12] + -z[13];
z[15] = abb[52] + -abb[56];
z[15] = abb[49] + -z[14] + (T(1) / T(2)) * z[15];
z[15] = abb[6] * z[15];
z[16] = -z[6] + z[8] + -z[15];
z[17] = 5 * abb[54];
z[18] = -7 * abb[53] + 29 * abb[58];
z[18] = -5 * abb[48] + z[17] + (T(1) / T(3)) * z[18];
z[19] = abb[52] * (T(4) / T(3));
z[20] = abb[49] * (T(11) / T(3));
z[21] = abb[51] * (T(4) / T(3));
z[18] = abb[55] * (T(-59) / T(12)) + abb[50] * (T(-1) / T(12)) + (T(1) / T(2)) * z[18] + -z[19] + -z[20] + z[21];
z[18] = abb[14] * z[18];
z[22] = abb[53] + abb[55];
z[23] = abb[49] + -abb[54];
z[24] = abb[59] + abb[50] * (T(1) / T(3));
z[25] = 2 * abb[58];
z[26] = abb[52] + abb[48] * (T(4) / T(3)) + (T(5) / T(3)) * z[22] + (T(8) / T(3)) * z[23] + -z[24] + -z[25];
z[26] = abb[17] * z[26];
z[27] = abb[50] + abb[52];
z[28] = abb[57] + z[27];
z[29] = -z[0] + z[28];
z[25] = -abb[53] + z[25];
z[30] = 2 * abb[54];
z[31] = z[25] + z[29] + -z[30];
z[31] = abb[8] * z[31];
z[32] = abb[23] * z[7];
z[33] = abb[25] * z[7];
z[31] = z[31] + z[32] + z[33];
z[32] = abb[57] + abb[59];
z[33] = -abb[53] + z[32];
z[21] = abb[56] * (T(2) / T(3)) + z[21];
z[23] = abb[58] * (T(-15) / T(4)) + abb[52] * (T(7) / T(3)) + abb[55] * (T(10) / T(3)) + abb[50] * (T(29) / T(12)) + abb[48] * (T(35) / T(12)) + -z[21] + (T(5) / T(2)) * z[23] + (T(4) / T(3)) * z[33];
z[23] = abb[3] * z[23];
z[33] = abb[59] * (T(2) / T(3));
z[34] = 3 * abb[48];
z[35] = 3 * abb[50];
z[36] = abb[51] * (T(-17) / T(6)) + abb[57] * (T(-7) / T(6)) + abb[56] * (T(-5) / T(6)) + abb[49] * (T(4) / T(3)) + abb[52] * (T(25) / T(6)) + z[33] + z[34] + z[35];
z[36] = abb[7] * z[36];
z[37] = -abb[52] + abb[57];
z[38] = abb[56] * (T(1) / T(2));
z[39] = abb[48] + z[38];
z[20] = abb[51] * (T(-5) / T(6)) + abb[59] * (T(1) / T(3)) + -z[20] + (T(11) / T(6)) * z[37] + z[39];
z[20] = abb[4] * z[20];
z[40] = 7 * abb[54] + abb[48] * (T(-7) / T(2)) + abb[58] * (T(13) / T(2));
z[19] = abb[55] * (T(-11) / T(3)) + abb[57] * (T(-7) / T(3)) + abb[56] * (T(1) / T(3)) + abb[49] * (T(1) / T(6)) + abb[50] * (T(3) / T(4)) + z[19] + -z[33] + (T(1) / T(2)) * z[40];
z[19] = abb[2] * z[19];
z[33] = abb[52] * (T(1) / T(2));
z[24] = abb[57] * (T(1) / T(6)) + abb[51] * (T(7) / T(6)) + -z[24] + -z[33] + (T(1) / T(3)) * z[39];
z[24] = abb[5] * z[24];
z[40] = 2 * abb[56];
z[41] = abb[58] * (T(-17) / T(2)) + abb[48] * (T(35) / T(2)) + -z[17];
z[41] = abb[51] + z[40] + (T(1) / T(2)) * z[41];
z[42] = 4 * abb[52];
z[41] = abb[55] * (T(-5) / T(2)) + abb[57] * (T(-2) / T(3)) + abb[49] * (T(11) / T(6)) + abb[50] * (T(35) / T(12)) + (T(1) / T(3)) * z[41] + z[42];
z[41] = abb[0] * z[41];
z[43] = abb[26] * abb[62];
z[44] = (T(1) / T(2)) * z[43];
z[45] = abb[26] * abb[63];
z[46] = (T(1) / T(2)) * z[45];
z[47] = abb[49] + -abb[58];
z[48] = abb[53] + z[47];
z[49] = abb[13] * z[48];
z[50] = (T(1) / T(2)) * z[7];
z[51] = abb[24] * z[50];
z[52] = abb[48] + -abb[58];
z[53] = abb[53] + z[52];
z[54] = abb[9] * z[53];
z[55] = abb[55] + z[52];
z[56] = abb[1] * z[55];
z[16] = (T(-1) / T(3)) * z[16] + z[18] + z[19] + z[20] + z[23] + z[24] + z[26] + (T(-4) / T(3)) * z[31] + z[36] + z[41] + -z[44] + -z[46] + (T(-11) / T(6)) * z[49] + -z[51] + (T(-5) / T(2)) * z[54] + (T(-1) / T(4)) * z[56];
z[18] = prod_pow(m1_set::bc<T>[0], 2);
z[16] = z[16] * z[18];
z[19] = 3 * abb[18];
z[20] = 3 * abb[19];
z[23] = abb[20] + abb[21];
z[24] = z[19] + z[20] + -z[23];
z[24] = abb[34] * z[24];
z[19] = -abb[20] + z[19];
z[26] = abb[19] + abb[21] + z[19];
z[26] = abb[31] * z[26];
z[24] = z[24] + -z[26];
z[26] = abb[19] * abb[36];
z[36] = abb[19] * abb[35];
z[24] = (T(1) / T(2)) * z[24] + -z[26] + z[36];
z[24] = abb[35] * z[24];
z[41] = -abb[31] + abb[35];
z[56] = abb[19] + -abb[21];
z[41] = z[41] * z[56];
z[57] = abb[18] * abb[32];
z[58] = abb[18] * abb[34];
z[41] = -z[41] + (T(1) / T(2)) * z[57] + -z[58];
z[41] = abb[32] * z[41];
z[57] = abb[18] + abb[19];
z[59] = abb[31] * z[57];
z[36] = -z[36] + -z[58] + z[59];
z[36] = abb[33] * z[36];
z[20] = -abb[21] + z[20];
z[58] = abb[18] + abb[20] + z[20];
z[58] = abb[34] * z[58];
z[59] = abb[31] * z[23];
z[58] = z[58] + -z[59];
z[59] = abb[31] * (T(1) / T(2));
z[58] = z[58] * z[59];
z[59] = (T(1) / T(3)) * z[23] + -z[57];
z[59] = z[18] * z[59];
z[60] = abb[31] + -abb[34];
z[61] = abb[18] + -abb[20];
z[62] = z[60] * z[61];
z[26] = (T(1) / T(2)) * z[26] + z[62];
z[26] = abb[36] * z[26];
z[62] = -abb[21] + z[57];
z[62] = abb[37] * z[62];
z[63] = prod_pow(abb[34], 2);
z[64] = abb[18] * z[63];
z[65] = -abb[4] + -abb[7] + abb[15] + abb[16];
z[65] = abb[3] + 2 * abb[30] + (T(-1) / T(2)) * z[65];
z[66] = abb[69] * z[65];
z[67] = abb[19] + abb[20];
z[68] = abb[64] * z[67];
z[69] = abb[18] + abb[21];
z[70] = abb[65] * z[69];
z[24] = -z[24] + -z[26] + z[36] + -z[41] + z[58] + z[59] + -z[62] + -z[64] + -z[66] + z[68] + z[70];
z[26] = -abb[60] * z[24];
z[36] = -abb[20] + z[57];
z[41] = abb[38] * z[36];
z[24] = -z[24] + z[41];
z[24] = abb[61] * z[24];
z[41] = -2 * abb[53] + 5 * abb[58];
z[58] = 2 * abb[55];
z[59] = z[35] + z[41] + -z[58];
z[62] = 5 * abb[52];
z[64] = 6 * abb[59];
z[66] = -abb[57] + z[1] + z[4] + -z[30] + z[59] + z[62] + -z[64];
z[66] = abb[17] * z[66];
z[68] = abb[28] * z[7];
z[66] = z[66] + -z[68];
z[70] = 7 * abb[51];
z[71] = -z[64] + z[70];
z[72] = -abb[56] + abb[57];
z[73] = abb[52] + z[72];
z[74] = -abb[54] + abb[50] * (T(3) / T(2));
z[75] = abb[55] * (T(1) / T(2)) + -z[4] + -z[53] + -z[71] + z[73] + z[74];
z[75] = abb[3] * z[75];
z[76] = 4 * abb[51];
z[77] = z[40] + -z[64] + z[76];
z[78] = abb[50] + z[5];
z[79] = 2 * abb[49];
z[80] = z[78] + z[79];
z[81] = 2 * abb[57];
z[82] = z[80] + -z[81];
z[83] = 4 * abb[55];
z[84] = -abb[53] + 4 * abb[58] + z[30] + z[77] + z[82] + -z[83];
z[85] = abb[10] * z[84];
z[86] = 2 * abb[51];
z[87] = -z[5] + z[86];
z[88] = -abb[53] + z[74];
z[89] = z[87] + -z[88];
z[90] = abb[55] * (T(3) / T(2));
z[91] = 2 * abb[48];
z[92] = z[89] + -z[90] + -z[91];
z[92] = abb[14] * z[92];
z[93] = -abb[57] + z[2];
z[94] = 3 * abb[52];
z[95] = z[93] + z[94];
z[96] = -abb[56] + z[95];
z[71] = -z[71] + -z[91] + z[96];
z[97] = abb[7] * z[71];
z[52] = -z[30] + z[52];
z[98] = z[52] + z[58];
z[99] = z[80] + -z[86] + z[98];
z[100] = abb[0] * z[99];
z[71] = abb[5] * z[71];
z[75] = z[31] + -z[66] + z[71] + -z[75] + z[85] + -z[92] + -z[97] + z[100];
z[85] = -abb[67] * z[75];
z[92] = abb[51] * (T(3) / T(2));
z[97] = z[38] + z[92];
z[100] = abb[53] * (T(1) / T(2));
z[101] = 3 * abb[58];
z[102] = 2 * abb[59];
z[103] = -abb[49] + abb[55] * (T(-7) / T(2)) + -z[12] + z[33] + z[97] + z[100] + z[101] + -z[102];
z[103] = abb[3] * z[103];
z[104] = abb[52] * (T(3) / T(2));
z[105] = 3 * abb[59];
z[39] = -abb[50] + abb[51] * (T(7) / T(2)) + z[12] + z[39] + -z[104] + -z[105];
z[39] = abb[5] * z[39];
z[106] = -abb[48] + z[101];
z[89] = -z[89] + -z[90] + z[106];
z[89] = abb[14] * z[89];
z[39] = z[39] + z[89];
z[14] = z[14] + -z[38];
z[38] = abb[50] * (T(1) / T(2));
z[33] = z[33] + z[38];
z[89] = abb[54] + z[100];
z[90] = abb[58] + z[14] + z[33] + -z[89];
z[90] = abb[8] * z[90];
z[100] = abb[23] + abb[25];
z[100] = z[50] * z[100];
z[107] = -abb[56] + abb[59];
z[108] = abb[4] * z[107];
z[46] = z[46] + z[90] + z[100] + z[108];
z[90] = -abb[55] + z[25];
z[100] = -abb[59] + z[27] + z[90];
z[100] = abb[17] * z[100];
z[15] = z[15] + -z[51];
z[109] = 2 * z[49];
z[110] = -abb[56] + -abb[58] + z[102];
z[111] = abb[2] * z[110];
z[112] = -abb[51] + abb[55];
z[112] = abb[0] * z[112];
z[44] = -z[15] + -z[39] + z[44] + z[46] + z[100] + z[103] + z[109] + z[111] + z[112];
z[44] = abb[33] * z[44];
z[103] = -abb[48] + z[30];
z[111] = -z[79] + z[103];
z[112] = 4 * abb[59];
z[113] = z[42] + -z[112];
z[59] = -z[59] + z[111] + -z[113];
z[59] = abb[17] * z[59];
z[114] = 3 * abb[51];
z[115] = -z[102] + z[114];
z[116] = z[37] + -z[79];
z[117] = abb[48] + abb[56];
z[118] = -abb[53] + abb[58] + z[58] + -z[115] + z[116] + -z[117];
z[118] = abb[3] * z[118];
z[119] = -z[0] + z[102];
z[120] = -abb[48] + z[119];
z[121] = abb[4] * z[120];
z[122] = abb[7] * z[120];
z[118] = -z[31] + -z[59] + z[118] + z[121] + z[122];
z[118] = abb[31] * z[118];
z[95] = abb[48] + z[95];
z[123] = z[79] + -z[86] + z[95];
z[124] = abb[0] * z[123];
z[119] = z[116] + z[119];
z[125] = abb[17] * z[119];
z[125] = z[68] + z[125];
z[126] = z[121] + z[125];
z[127] = -abb[48] + z[102];
z[128] = abb[49] + z[86];
z[129] = z[127] + -z[128];
z[130] = 2 * abb[3];
z[129] = z[129] * z[130];
z[130] = -abb[59] + z[9];
z[131] = abb[7] * z[130];
z[124] = z[71] + z[124] + z[126] + -z[129] + 4 * z[131];
z[129] = -abb[35] * z[124];
z[45] = z[43] + z[45];
z[131] = abb[24] * z[7];
z[132] = z[45] + z[131];
z[133] = abb[51] + -abb[56];
z[134] = -z[116] + z[133];
z[135] = abb[6] * z[134];
z[136] = z[132] + -z[135];
z[137] = z[122] + z[136];
z[138] = 2 * abb[2];
z[107] = z[107] * z[138];
z[107] = z[107] + -z[125] + z[137];
z[107] = abb[34] * z[107];
z[80] = z[52] + z[80];
z[139] = -abb[31] * z[80];
z[140] = abb[48] + z[116];
z[141] = abb[34] * z[140];
z[139] = z[139] + -z[141];
z[139] = abb[0] * z[139];
z[44] = z[44] + -z[107] + z[118] + z[129] + z[139];
z[44] = abb[33] * z[44];
z[118] = abb[48] * (T(1) / T(2));
z[104] = z[104] + z[118];
z[129] = -z[79] + z[102];
z[139] = abb[58] * (T(1) / T(2));
z[142] = abb[54] + z[14] + -z[38] + -z[104] + z[129] + -z[139];
z[142] = abb[17] * z[142];
z[139] = -abb[49] + z[139];
z[74] = abb[59] + z[74] + -z[87] + z[118] + -z[139];
z[74] = abb[3] * z[74];
z[87] = z[42] + -z[81];
z[143] = z[2] + z[87];
z[144] = (T(1) / T(2)) * z[117];
z[145] = z[13] + z[144];
z[146] = 6 * abb[49];
z[147] = -z[105] + z[143] + z[145] + z[146];
z[147] = abb[7] * z[147];
z[148] = 8 * abb[49];
z[149] = z[35] + -z[81] + z[148];
z[150] = 6 * abb[52];
z[151] = -z[112] + z[150];
z[117] = -z[30] + z[117] + z[149] + z[151];
z[117] = abb[2] * z[117];
z[152] = -z[12] + z[33];
z[153] = abb[54] + -z[139] + z[152];
z[153] = abb[0] * z[153];
z[154] = abb[28] * z[50];
z[15] = z[15] + (T(-1) / T(2)) * z[45] + z[74] + z[108] + z[117] + z[142] + z[147] + z[153] + z[154];
z[15] = abb[32] * z[15];
z[74] = abb[55] + abb[59];
z[117] = -z[74] + z[128];
z[117] = abb[3] * z[117];
z[128] = -abb[58] + abb[59];
z[128] = abb[2] * z[128];
z[100] = -z[49] + -z[100] + -z[108] + z[117] + z[128];
z[100] = abb[33] * z[100];
z[108] = -z[79] + -z[96] + z[115];
z[117] = abb[3] * z[108];
z[128] = 3 * abb[49];
z[142] = z[78] + z[128];
z[147] = z[32] + -z[142];
z[153] = abb[2] * z[147];
z[117] = z[117] + -z[126] + 6 * z[153];
z[126] = 4 * z[147];
z[153] = abb[7] * z[126];
z[153] = z[6] + z[153];
z[155] = z[117] + z[153];
z[155] = abb[31] * z[155];
z[156] = -abb[48] + z[37];
z[157] = abb[0] * z[156];
z[153] = -z[153] + z[157];
z[117] = -z[117] + z[153];
z[117] = abb[35] * z[117];
z[157] = -abb[31] * z[156];
z[141] = z[141] + z[157];
z[141] = abb[0] * z[141];
z[15] = z[15] + 2 * z[100] + z[107] + z[117] + z[141] + z[155];
z[15] = abb[32] * z[15];
z[100] = z[11] + -z[45];
z[107] = abb[22] * z[50];
z[51] = z[51] + z[107];
z[100] = -z[51] + (T(1) / T(2)) * z[100];
z[117] = -z[27] + -z[79] + z[145];
z[141] = abb[16] * z[117];
z[50] = abb[27] * z[50];
z[50] = z[50] + z[141];
z[141] = z[50] + z[100];
z[126] = abb[2] * z[126];
z[155] = -z[126] + z[141];
z[123] = -abb[3] * z[123];
z[62] = z[62] + z[149];
z[149] = -z[62] + z[112] + -z[145];
z[149] = abb[7] * z[149];
z[157] = z[13] + -z[116];
z[158] = 3 * abb[56];
z[159] = -abb[48] + z[158];
z[159] = -z[102] + z[157] + (T(1) / T(2)) * z[159];
z[159] = abb[4] * z[159];
z[123] = z[123] + -z[125] + z[149] + -z[155] + z[159];
z[123] = abb[31] * z[123];
z[149] = -abb[56] + z[34];
z[159] = (T(1) / T(2)) * z[149];
z[160] = z[13] + z[116] + z[159];
z[160] = abb[4] * z[160];
z[147] = z[138] * z[147];
z[161] = -z[50] + z[147];
z[162] = abb[50] + z[4] + -z[81];
z[94] = z[94] + z[162];
z[163] = -z[94] + z[145];
z[163] = abb[7] * z[163];
z[120] = -abb[3] * z[120];
z[100] = z[100] + z[120] + z[160] + z[161] + z[163];
z[100] = abb[34] * z[100];
z[120] = abb[36] * z[124];
z[95] = -z[4] + -z[95] + z[102];
z[95] = abb[0] * z[95];
z[124] = -abb[3] * z[130];
z[130] = -abb[48] + abb[49];
z[130] = abb[7] * z[130];
z[124] = z[124] + 2 * z[130];
z[130] = abb[4] * z[140];
z[95] = z[95] + 2 * z[124] + z[130] + -z[147];
z[95] = abb[35] * z[95];
z[124] = abb[48] + -z[72] + z[78] + -z[129];
z[124] = abb[34] * z[124];
z[129] = -abb[56] + -z[27] + z[129];
z[129] = abb[31] * z[129];
z[124] = z[124] + z[129];
z[124] = abb[0] * z[124];
z[95] = z[95] + z[100] + z[120] + z[123] + z[124];
z[95] = abb[35] * z[95];
z[99] = abb[14] * z[99];
z[99] = z[31] + z[99];
z[100] = -abb[48] + 10 * abb[49] + abb[56] + -z[64] + z[114] + z[143];
z[100] = abb[7] * z[100];
z[114] = abb[53] + z[30];
z[120] = -z[58] + z[82] + z[114];
z[120] = abb[3] * z[120];
z[64] = 5 * abb[50] + -z[64];
z[123] = 16 * abb[49] + 10 * abb[52] + -4 * abb[57] + z[64] + z[98];
z[123] = abb[2] * z[123];
z[124] = abb[55] + z[47] + z[156];
z[124] = abb[0] * z[124];
z[66] = -z[6] + -z[66] + z[99] + z[100] + z[120] + z[123] + z[124];
z[100] = 5 * abb[49];
z[120] = 5 * abb[55];
z[41] = 4 * abb[54] + z[2] + z[41] + -z[100] + -z[120];
z[123] = -abb[10] * z[41];
z[123] = -z[66] + z[123];
z[123] = abb[65] * z[123];
z[89] = abb[59] + z[89] + -z[97] + z[152];
z[89] = abb[3] * z[89];
z[32] = -abb[48] + z[4] + -z[32] + z[78];
z[32] = abb[7] * z[32];
z[43] = -z[11] + z[43];
z[32] = z[32] + (T(1) / T(2)) * z[43] + z[46] + z[51] + z[54] + z[89] + -z[161];
z[32] = abb[31] * z[32];
z[43] = abb[3] * z[140];
z[43] = z[43] + z[160];
z[46] = z[94] + -z[102] + z[145];
z[46] = abb[7] * z[46];
z[46] = -z[43] + z[46] + z[125] + z[141] + -z[147];
z[46] = abb[34] * z[46];
z[32] = z[32] + z[46];
z[32] = abb[31] * z[32];
z[14] = abb[58] * (T(-5) / T(2)) + -z[14] + z[74] + -z[88] + -z[104];
z[14] = abb[17] * z[14];
z[12] = -abb[54] + -abb[55] + abb[58] * (T(3) / T(2)) + z[9] + z[12] + z[33] + -z[102];
z[12] = abb[0] * z[12];
z[33] = 3 * abb[1] + abb[3] * (T(1) / T(2));
z[33] = z[33] * z[55];
z[1] = -abb[59] + (T(1) / T(2)) * z[1];
z[1] = abb[4] * z[1];
z[1] = z[1] + z[12] + z[14] + z[33] + z[39] + -z[54] + -z[154];
z[1] = abb[36] * z[1];
z[8] = z[8] + z[132];
z[12] = -z[40] + z[102] + z[140];
z[12] = abb[0] * z[12];
z[14] = abb[3] * z[119];
z[12] = z[8] + z[12] + z[14] + z[122] + -z[125];
z[14] = -z[12] * z[60];
z[1] = z[1] + z[14];
z[1] = abb[36] * z[1];
z[14] = abb[50] * (T(5) / T(2));
z[22] = -3 * abb[54] + abb[48] * (T(3) / T(2)) + abb[58] * (T(7) / T(2)) + z[14] + -z[22] + z[113] + z[128];
z[22] = abb[17] * z[22];
z[33] = z[118] + z[139];
z[39] = abb[55] + z[33] + z[73] + z[88] + -z[115];
z[39] = abb[3] * z[39];
z[46] = abb[4] * z[119];
z[51] = abb[7] * z[108];
z[22] = z[22] + z[39] + z[46] + -z[51] + -z[54] + -z[99];
z[39] = z[82] + -z[98];
z[46] = abb[2] * z[39];
z[33] = -abb[54] + abb[55] + -z[33] + -z[38];
z[38] = -abb[0] * z[33];
z[38] = z[22] + z[38] + z[46];
z[38] = abb[68] * z[38];
z[46] = -abb[53] + -z[101];
z[14] = abb[49] * (T(-7) / T(2)) + abb[55] * (T(9) / T(2)) + -z[14] + -z[17] + -z[21] + (T(11) / T(3)) * z[37] + (T(1) / T(2)) * z[46] + z[102];
z[14] = z[14] * z[18];
z[17] = abb[56] + z[86];
z[18] = z[17] + -z[105];
z[21] = -abb[58] + z[30];
z[46] = -z[18] + -z[21] + -z[78] + z[81] + -z[100];
z[46] = abb[32] * z[46];
z[51] = -z[37] + z[128];
z[55] = z[18] + z[51];
z[60] = abb[35] * z[55];
z[73] = z[18] + z[90];
z[73] = abb[33] * z[73];
z[60] = -z[60] + z[73];
z[55] = abb[31] * z[55];
z[55] = z[55] + z[60];
z[46] = z[46] + 2 * z[55];
z[46] = abb[32] * z[46];
z[55] = -abb[68] * z[84];
z[73] = -2 * z[60];
z[73] = abb[31] * z[73];
z[74] = abb[49] + -abb[50];
z[18] = -z[18] + z[21] + -z[74];
z[18] = prod_pow(abb[31], 2) * z[18];
z[14] = z[14] + z[18] + z[46] + z[55] + z[73];
z[14] = abb[10] * z[14];
z[18] = -abb[56] + z[91];
z[21] = -z[18] + -z[37] + -z[115];
z[21] = abb[3] * z[21];
z[46] = -z[115] + -z[149];
z[46] = abb[7] * z[46];
z[55] = -abb[48] + -abb[59] + z[0] + -z[27];
z[55] = abb[0] * z[55];
z[36] = abb[60] * z[36];
z[21] = -z[8] + z[21] + z[36] + z[46] + 2 * z[55] + -z[71] + -z[121];
z[21] = abb[38] * z[21];
z[3] = z[3] + -z[87] + -z[146];
z[3] = abb[7] * z[3];
z[36] = -abb[3] * z[134];
z[46] = abb[56] + -z[94];
z[46] = z[46] * z[138];
z[55] = abb[4] * z[10];
z[3] = z[3] + z[6] + z[36] + z[46] + z[55] + -z[136];
z[3] = abb[37] * z[3];
z[36] = -z[37] + z[48] + -z[133];
z[36] = abb[3] * z[36];
z[29] = abb[58] + z[29] + -z[103];
z[29] = abb[17] * z[29];
z[29] = z[29] + -z[31] + z[68];
z[31] = abb[4] * z[134];
z[31] = z[29] + -z[31] + -z[36] + -z[49] + -z[54] + -z[136];
z[36] = -abb[58] + z[40];
z[46] = -abb[50] + z[30];
z[48] = -abb[48] + z[36] + z[46] + -z[81];
z[73] = abb[2] * z[48];
z[73] = z[31] + z[73];
z[73] = abb[66] * z[73];
z[13] = -z[13] + z[27] + z[159];
z[13] = abb[18] * z[13];
z[78] = abb[21] * z[117];
z[81] = -z[144] + z[157];
z[81] = abb[19] * z[81];
z[82] = abb[29] * (T(1) / T(2));
z[7] = z[7] * z[82];
z[10] = abb[20] * z[10];
z[7] = z[7] + (T(-1) / T(2)) * z[10] + -z[13] + z[78] + z[81];
z[10] = -abb[69] * z[7];
z[13] = abb[3] * z[53];
z[53] = 2 * z[54];
z[13] = z[8] + z[13] + -z[29] + z[53] + z[55];
z[28] = -z[28] + z[30] + z[36] + -z[91];
z[29] = abb[0] * z[28];
z[29] = -z[13] + z[29];
z[29] = abb[64] * z[29];
z[30] = abb[7] * z[140];
z[36] = -abb[49] + z[37];
z[54] = z[36] * z[138];
z[30] = z[30] + z[54];
z[30] = z[30] * z[63];
z[18] = -z[18] + -z[27];
z[18] = abb[34] * z[18];
z[37] = -abb[56] + z[37] + -z[47];
z[37] = abb[31] * z[37];
z[18] = z[18] + z[37];
z[18] = abb[31] * z[18];
z[37] = -z[63] * z[140];
z[36] = abb[37] * z[36];
z[18] = z[18] + -2 * z[36] + z[37];
z[18] = abb[0] * z[18];
z[17] = z[17] + -z[27] + -z[102];
z[27] = -abb[72] * z[17];
z[1] = abb[70] + abb[71] + z[1] + z[3] + z[10] + z[14] + z[15] + z[16] + z[18] + z[21] + z[24] + z[26] + z[27] + z[29] + z[30] + z[32] + z[38] + z[44] + z[73] + z[85] + z[95] + z[123];
z[3] = 2 * z[125];
z[10] = z[92] + -z[112];
z[14] = -abb[52] + -z[144];
z[14] = -z[10] + 3 * z[14] + -z[162];
z[14] = abb[7] * z[14];
z[15] = z[72] + z[102] + -z[142];
z[15] = z[15] * z[138];
z[11] = -z[11] + 3 * z[45];
z[11] = -z[3] + (T(1) / T(2)) * z[11] + z[14] + z[15] + z[43] + -z[50] + z[107] + (T(3) / T(2)) * z[131] + -z[135];
z[11] = abb[34] * z[11];
z[14] = z[4] + z[35] + z[42] + -z[72] + -z[86] + -z[127];
z[14] = abb[0] * z[14];
z[15] = abb[48] + abb[52];
z[15] = z[4] + 3 * z[15] + z[86] + z[93] + -z[112];
z[15] = abb[3] * z[15];
z[16] = 9 * abb[48] + abb[56];
z[16] = -8 * abb[59] + abb[51] * (T(9) / T(2)) + (T(1) / T(2)) * z[16] + z[62];
z[16] = abb[7] * z[16];
z[18] = -abb[48] + -5 * abb[56];
z[10] = -z[10] + (T(1) / T(2)) * z[18] + z[116];
z[10] = abb[4] * z[10];
z[3] = z[3] + z[10] + z[14] + z[15] + z[16] + z[71] + z[155];
z[3] = abb[35] * z[3];
z[10] = 4 * abb[53] + -9 * abb[58] + -z[64] + z[83] + z[111] + -z[150];
z[10] = abb[17] * z[10];
z[9] = z[9] + -z[112] + z[158];
z[9] = abb[4] * z[9];
z[5] = -abb[53] + -abb[54] + z[5] + z[106];
z[5] = -3 * abb[55] + 2 * z[5] + z[35] + -z[76];
z[5] = abb[14] * z[5];
z[14] = abb[48] + -7 * abb[58] + z[4] + z[102] + z[120];
z[14] = abb[3] * z[14];
z[15] = -z[58] + z[80] + z[86];
z[15] = abb[0] * z[15];
z[16] = -z[110] * z[138];
z[5] = z[5] + z[9] + z[10] + z[14] + z[15] + z[16] + -4 * z[49] + -z[71] + -z[137];
z[5] = abb[33] * z[5];
z[10] = z[58] + z[112];
z[14] = -abb[50] + z[79];
z[15] = 6 * abb[51] + -z[10] + z[14] + z[40] + z[52];
z[15] = abb[3] * z[15];
z[16] = z[0] + z[34] + -z[143] + -z[148];
z[16] = abb[7] * z[16];
z[6] = z[6] + -z[8] + z[9] + z[15] + z[16] + -z[53] + z[59] + z[126];
z[6] = abb[31] * z[6];
z[0] = abb[52] + abb[57] + -z[0] + z[2] + 2 * z[25] + -z[58] + -z[79];
z[0] = abb[17] * z[0];
z[2] = z[10] + -z[70] + z[96];
z[2] = abb[3] * z[2];
z[8] = 9 * abb[49] + -3 * abb[57] + abb[58] + z[35] + z[151];
z[8] = z[8] * z[138];
z[0] = z[0] + z[2] + z[8] + -z[9] + z[68] + z[109] + z[153];
z[0] = abb[32] * z[0];
z[2] = abb[36] * z[12];
z[4] = z[4] + z[40] + -z[46] + z[87] + -z[106];
z[4] = abb[31] * z[4];
z[8] = -z[14] + z[34] + z[72];
z[8] = abb[34] * z[8];
z[4] = z[4] + z[8];
z[4] = abb[0] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[11];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[42] * z[75];
z[3] = -abb[40] * z[66];
z[4] = abb[43] * z[22];
z[5] = z[20] + -z[61];
z[6] = abb[34] * (T(1) / T(2));
z[5] = z[5] * z[6];
z[6] = z[19] + -z[56];
z[8] = abb[35] * (T(1) / T(2));
z[6] = z[6] * z[8];
z[8] = z[23] + -z[57];
z[8] = abb[31] * z[8];
z[9] = abb[33] * z[57];
z[10] = abb[32] * z[56];
z[11] = abb[36] * z[61];
z[5] = z[5] + z[6] + -z[8] + z[9] + -z[10] + -z[11];
z[5] = m1_set::bc<T>[0] * z[5];
z[6] = abb[39] * z[67];
z[8] = abb[40] * z[69];
z[5] = z[5] + -z[6] + -z[8];
z[6] = abb[60] + abb[61];
z[5] = z[5] * z[6];
z[8] = abb[41] * z[31];
z[9] = -abb[55] + z[74] + z[77] + z[101] + -z[114];
z[9] = abb[31] * z[9];
z[10] = -z[51] + -z[77] + -z[90];
z[10] = abb[32] * z[10];
z[9] = z[9] + z[10] + z[60];
z[9] = m1_set::bc<T>[0] * z[9];
z[10] = -abb[43] * z[84];
z[11] = -abb[40] * z[41];
z[9] = 2 * z[9] + z[10] + z[11];
z[9] = abb[10] * z[9];
z[10] = -abb[39] * z[13];
z[6] = z[6] * z[65];
z[6] = z[6] + -z[7];
z[6] = abb[44] * z[6];
z[7] = abb[43] * z[39];
z[11] = abb[41] * z[48];
z[7] = z[7] + z[11];
z[7] = abb[2] * z[7];
z[11] = abb[39] * z[28];
z[12] = -abb[43] * z[33];
z[11] = z[11] + z[12];
z[11] = abb[0] * z[11];
z[12] = -abb[47] * z[17];
z[0] = abb[45] + abb[46] + z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_648_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-6.29528515329787257381662736342755648630045211758223188305213529"),stof<T>("-64.523801920969199735358964251936598490033742049441984430025219812")}, std::complex<T>{stof<T>("-33.191100308207320131550318266562191383193779680128316816138219234"),stof<T>("-46.221600699162129032767430418644372766945054376522902069174229204")}, std::complex<T>{stof<T>("12.099491000985158508890548363100308502866350863069668498960914948"),stof<T>("-72.650642308036528454707782944359898074474816556365823300717959959")}, std::complex<T>{stof<T>("10.808913559091387235236103361320073994482499127648662398962360616"),stof<T>("6.4077927811544311565818295087385280728529920676325660171598369699")}, std::complex<T>{stof<T>("-9.556190782033000772068973201238137127998521344493552120569657174"),stof<T>("-89.775795015664713550209997856273018164177651713319197598178604883")}, std::complex<T>{stof<T>("-45.936503567357004834645510728662396876855909934067356986017349428"),stof<T>("33.735508803839325404217870049469907446528133334582252098590365979")}, std::complex<T>{stof<T>("5.988687094436881345811310662738584446203671990155887283914284001"),stof<T>("45.889135226615389161817956217069956419934936560811249088292254641")}, std::complex<T>{stof<T>("-6.9020956721917328427791134245095963619886505098679704905266234863"),stof<T>("11.5377331185761566759916865718435094622925464415053162839469128778")}, std::complex<T>{stof<T>("-0.6687458038860240805037139439018829383487291920268642821032152032"),stof<T>("-4.3436871840284605831045519526160562417641996288129021997010243338")}, std::complex<T>{stof<T>("18.661338235799718608053866232969153407763036212485276977573430121"),stof<T>("-5.819414905679509485406763196621858120264633123452250246685482398")}, std::complex<T>{stof<T>("37.665384064594267310429178539118502960545464236064112453467116866"),stof<T>("17.484370262917336362250816970612986793324841172198201351119013293")}, std::complex<T>{stof<T>("-7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("-10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("-10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[89])) - rlog(abs(kbase.W[89])), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_648_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_648_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (8 + -7 * v[0] + 3 * v[1] + 5 * v[2] + v[3] + -4 * v[4] + -2 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -5 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[48] + abb[51] + -abb[59]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[31] + -abb[34];
z[1] = abb[35] + z[0];
z[1] = abb[35] * z[1];
z[0] = abb[36] + z[0];
z[0] = abb[36] * z[0];
z[0] = -abb[38] + -z[0] + z[1];
z[1] = abb[48] + abb[51] + -abb[59];
return 2 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_648_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[48] + abb[51] + -abb[59]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = -abb[35] + abb[36];
z[1] = abb[48] + abb[51] + -abb[59];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_648_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (v[2] + v[3]) * (-4 * v[0] + -2 * v[1] + -3 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 2 * (-4 + v[2] + 2 * v[3] + v[4] + -2 * v[5]))) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[2] + v[3])) / tend;


		return (abb[49] + abb[52] + -abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = abb[31] + -abb[35];
z[1] = abb[49] + abb[52] + -abb[57];
z[0] = z[0] * z[1];
z[2] = abb[32] * z[1];
z[2] = z[0] + -z[2];
z[2] = abb[32] * z[2];
z[1] = abb[34] * z[1];
z[0] = -z[0] + z[1];
z[0] = abb[34] * z[0];
z[0] = z[0] + z[2];
return 2 * abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_648_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[49] + abb[52] + -abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[32] + abb[34];
z[1] = abb[49] + abb[52] + -abb[57];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_648_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-126.507991365947897700912162513529227247739084552934068024746558466"),stof<T>("-5.948048920541609705065113771683172881204344917769751691760698988")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18, 89});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,73> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[89])) - rlog(abs(k.W[89])), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[45] = SpDLog_f_4_648_W_17_Im(t, path, abb);
abb[46] = SpDLog_f_4_648_W_19_Im(t, path, abb);
abb[47] = SpDLogQ_W_90(k,dl,dlr).imag();
abb[70] = SpDLog_f_4_648_W_17_Re(t, path, abb);
abb[71] = SpDLog_f_4_648_W_19_Re(t, path, abb);
abb[72] = SpDLogQ_W_90(k,dl,dlr).real();

                    
            return f_4_648_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_648_DLogXconstant_part(base_point<T>, kend);
	value += f_4_648_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_648_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_648_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_648_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_648_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_648_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_648_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
