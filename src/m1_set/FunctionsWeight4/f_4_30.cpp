/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_30.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_30_abbreviated (const std::array<T,27>& abb) {
T z[37];
z[0] = -abb[18] + abb[21];
z[1] = 3 * abb[19];
z[2] = abb[20] + z[1];
z[3] = abb[22] * (T(1) / T(2));
z[2] = z[0] + (T(1) / T(2)) * z[2] + -z[3];
z[4] = abb[6] * z[2];
z[5] = abb[22] * (T(5) / T(2));
z[6] = 7 * abb[19] + 5 * abb[20];
z[7] = z[0] + -z[5] + (T(1) / T(2)) * z[6];
z[7] = abb[3] * z[7];
z[8] = -abb[20] + abb[22];
z[9] = -abb[18] + z[8];
z[10] = 2 * abb[21];
z[11] = -z[9] + z[10];
z[11] = abb[4] * z[11];
z[12] = 5 * abb[19];
z[13] = 7 * abb[20] + z[12];
z[13] = abb[22] * (T(-7) / T(2)) + -z[0] + (T(1) / T(2)) * z[13];
z[13] = abb[1] * z[13];
z[14] = 2 * abb[19];
z[15] = -abb[18] + -z[8] + z[14];
z[16] = z[10] + -z[15];
z[16] = abb[0] * z[16];
z[17] = -abb[19] + z[8];
z[18] = 2 * z[17];
z[19] = abb[5] * z[18];
z[7] = -z[4] + z[7] + -z[11] + z[13] + z[16] + z[19];
z[7] = prod_pow(abb[12], 2) * z[7];
z[13] = abb[18] * (T(1) / T(2));
z[5] = abb[20] * (T(5) / T(2)) + z[1] + -z[5] + -z[13];
z[5] = abb[21] + (T(1) / T(2)) * z[5];
z[5] = abb[0] * z[5];
z[16] = 2 * abb[18];
z[20] = -z[10] + z[16];
z[12] = 3 * abb[20] + z[12];
z[21] = 3 * abb[22] + -z[12] + z[20];
z[22] = abb[1] * z[21];
z[5] = z[4] + z[5] + z[22];
z[23] = abb[20] * (T(1) / T(2));
z[24] = abb[19] + -z[3] + -z[13] + z[23];
z[25] = abb[2] * z[24];
z[26] = abb[23] + -abb[24];
z[27] = abb[8] * z[26];
z[28] = (T(-3) / T(2)) * z[25] + (T(3) / T(4)) * z[27];
z[29] = -abb[21] + z[8];
z[16] = -z[1] + z[16] + z[29];
z[16] = abb[3] * z[16];
z[30] = abb[7] * z[26];
z[31] = (T(1) / T(2)) * z[30];
z[16] = z[5] + z[16] + -z[28] + -z[31];
z[16] = abb[14] * z[16];
z[32] = z[1] + -z[8] + -z[20];
z[33] = abb[6] * z[32];
z[34] = z[19] + -z[33];
z[6] = 5 * abb[22] + -z[6] + z[20];
z[6] = abb[1] * z[6];
z[20] = abb[3] * z[21];
z[6] = z[6] + z[20] + -z[34];
z[20] = abb[0] * z[18];
z[20] = z[6] + -z[20];
z[21] = abb[12] * z[20];
z[12] = abb[22] * (T(3) / T(2)) + -z[0] + (T(-1) / T(2)) * z[12];
z[12] = abb[1] * z[12];
z[4] = z[4] + z[12];
z[35] = abb[3] * z[2];
z[36] = abb[0] * z[9];
z[11] = z[11] + z[36];
z[35] = -z[4] + z[11] + z[35];
z[35] = abb[11] * z[35];
z[16] = z[16] + z[21] + z[35];
z[16] = abb[11] * z[16];
z[1] = z[1] + -2 * z[8];
z[35] = z[0] + z[1];
z[35] = abb[3] * z[35];
z[5] = z[5] + -z[19] + (T(-1) / T(2)) * z[25] + (T(1) / T(4)) * z[27] + z[31] + -z[35];
z[19] = -abb[11] + abb[14];
z[5] = z[5] * z[19];
z[19] = abb[19] * (T(3) / T(2)) + -z[13] + -z[29];
z[19] = abb[3] * z[19];
z[4] = -z[4] + z[19] + z[31] + (T(1) / T(2)) * z[36];
z[4] = abb[13] * z[4];
z[4] = z[4] + z[5] + z[21];
z[4] = abb[13] * z[4];
z[2] = -abb[9] * z[2];
z[5] = (T(3) / T(2)) * z[24];
z[19] = -abb[8] * z[5];
z[24] = abb[3] + -abb[10];
z[24] = z[24] * z[26];
z[25] = (T(3) / T(4)) * z[26];
z[25] = abb[2] * z[25];
z[9] = abb[21] + (T(-1) / T(2)) * z[9];
z[9] = abb[7] * z[9];
z[2] = z[2] + z[9] + z[19] + z[24] + z[25];
z[2] = abb[15] * z[2];
z[5] = -abb[21] + -z[5];
z[5] = abb[0] * z[5];
z[9] = -abb[18] + abb[19];
z[9] = abb[3] * z[9];
z[5] = z[5] + (T(3) / T(2)) * z[9] + -z[12] + z[28];
z[5] = abb[14] * z[5];
z[5] = z[5] + -z[21];
z[5] = abb[14] * z[5];
z[9] = -z[14] + -z[23];
z[12] = abb[22] * (T(1) / T(6));
z[14] = abb[21] * (T(1) / T(2));
z[9] = (T(1) / T(3)) * z[9] + z[12] + z[13] + -z[14];
z[9] = abb[1] * z[9];
z[13] = -abb[19] + abb[20];
z[3] = -z[0] + -z[3] + (T(1) / T(2)) * z[13];
z[3] = abb[3] * z[3];
z[13] = abb[19] * (T(-5) / T(2)) + abb[18] * (T(7) / T(2)) + -z[8] + z[14];
z[13] = abb[0] * z[13];
z[14] = abb[5] * z[17];
z[3] = z[3] + z[13] + z[14];
z[13] = abb[19] + abb[20] * (T(1) / T(3));
z[0] = (T(1) / T(3)) * z[0] + -z[12] + (T(1) / T(2)) * z[13];
z[0] = abb[6] * z[0];
z[8] = abb[18] * (T(-7) / T(6)) + abb[21] * (T(-4) / T(3)) + abb[19] * (T(1) / T(2)) + (T(2) / T(3)) * z[8];
z[8] = abb[4] * z[8];
z[0] = z[0] + (T(1) / T(3)) * z[3] + z[8] + z[9];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[3] = abb[26] * z[6];
z[6] = abb[3] * z[32];
z[1] = -abb[18] + z[1];
z[8] = 4 * abb[21] + z[1];
z[8] = abb[4] * z[8];
z[9] = 3 * z[17];
z[12] = abb[1] * z[9];
z[6] = z[6] + z[8] + -z[12] + -z[33];
z[8] = -abb[25] * z[6];
z[12] = z[1] + -z[10];
z[13] = abb[25] * z[12];
z[14] = -abb[26] * z[18];
z[17] = abb[15] * z[26];
z[13] = z[13] + z[14] + (T(1) / T(4)) * z[17];
z[13] = abb[0] * z[13];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[7] + z[8] + z[13] + z[16];
z[1] = z[1] + z[10];
z[1] = abb[3] * z[1];
z[2] = -z[1] + z[22] + -z[30] + z[33] + -z[36];
z[2] = abb[13] * z[2];
z[3] = abb[2] * z[15];
z[3] = z[3] + -z[22] + -z[27] + z[34];
z[4] = -z[9] + z[10];
z[4] = abb[3] * z[4];
z[4] = z[3] + z[4] + z[36];
z[4] = abb[14] * z[4];
z[1] = -z[1] + -z[3] + -2 * z[11] + z[30];
z[1] = abb[11] * z[1];
z[1] = z[1] + z[2] + z[4] + -z[21];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[17] * z[20];
z[3] = abb[0] * z[12];
z[3] = z[3] + -z[6];
z[3] = abb[16] * z[3];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_30_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-10.5310788947465738925997198593769860843356285156687147469774000497"),stof<T>("1.4588598612491342812118147996542339667044971325437066488445343151")}, std::complex<T>{stof<T>("10.53621819428656309846082257230952671766659978586591822473931155"),stof<T>("-3.4270936220792020731818444221714519030298827434077062112535346285")}, std::complex<T>{stof<T>("0.0051392995399892058611027129325406333309712701972034777619115003"),stof<T>("-1.9682337608300677919700296225172179363253856108639995624090003133")}, std::complex<T>{stof<T>("-26.880210816872341450909897200596016269818166498766283936203828411"),stof<T>("-4.65572360152186764833772650702743528282549532619367129579613118")}, std::complex<T>{stof<T>("-0.0051392995399892058611027129325406333309712701972034777619115003"),stof<T>("1.9682337608300677919700296225172179363253856108639995624090003133")}, std::complex<T>{stof<T>("-1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_30_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_30_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-36.869322902457852185574564728002899894329581299997821189718565005"),stof<T>("27.766363206743743975979840957242261308202491038358400957529679802")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_30_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_30_DLogXconstant_part(base_point<T>, kend);
	value += f_4_30_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_30_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_30_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_30_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_30_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_30_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_30_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
