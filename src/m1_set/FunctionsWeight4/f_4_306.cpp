/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_306.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_306_abbreviated (const std::array<T,33>& abb) {
T z[81];
z[0] = abb[22] * (T(-7) / T(3)) + abb[20] * (T(13) / T(2)) + abb[27] * (T(22) / T(3));
z[1] = abb[23] * (T(9) / T(2));
z[2] = 4 * abb[26];
z[3] = 4 * abb[19];
z[4] = abb[25] * (T(-9) / T(2)) + abb[21] * (T(7) / T(2)) + z[0] + -z[1] + -z[2] + z[3];
z[4] = abb[5] * z[4];
z[5] = 4 * abb[21];
z[6] = abb[26] * (T(9) / T(2));
z[7] = 4 * abb[23];
z[0] = abb[24] * (T(-9) / T(2)) + abb[19] * (T(7) / T(2)) + z[0] + z[5] + -z[6] + -z[7];
z[0] = abb[8] * z[0];
z[8] = 3 * abb[23];
z[9] = 3 * abb[26];
z[10] = z[8] + z[9];
z[11] = abb[19] * (T(1) / T(3));
z[12] = 8 * abb[27];
z[13] = -abb[20] + -z[12];
z[13] = abb[21] + 4 * z[13];
z[13] = abb[25] * (T(11) / T(3)) + z[10] + z[11] + (T(1) / T(3)) * z[13];
z[13] = abb[1] * z[13];
z[14] = abb[23] + abb[26];
z[15] = 4 * abb[27];
z[16] = -abb[21] + z[15];
z[11] = -z[11] + -z[14] + (T(1) / T(3)) * z[16];
z[11] = abb[4] * z[11];
z[17] = abb[22] + z[14];
z[18] = 5 * abb[21];
z[12] = -7 * abb[20] + -z[12];
z[12] = 2 * z[12] + 13 * z[17] + -z[18];
z[19] = 5 * abb[25];
z[20] = 5 * abb[24];
z[12] = -10 * abb[19] + 2 * z[12] + -z[19] + -z[20];
z[12] = abb[9] * z[12];
z[21] = -abb[20] + abb[22];
z[22] = 7 * abb[27];
z[21] = -11 * z[21] + -z[22];
z[23] = abb[21] * (T(5) / T(2));
z[24] = abb[26] * (T(-1) / T(6)) + abb[19] * (T(7) / T(3)) + (T(1) / T(3)) * z[21] + z[23];
z[24] = abb[6] * z[24];
z[25] = abb[27] * (T(2) / T(3)) + abb[22] * (T(8) / T(3)) + abb[20] * (T(9) / T(2));
z[26] = 7 * abb[21];
z[27] = abb[19] * (T(-13) / T(3)) + abb[24] * (T(5) / T(2)) + abb[25] * (T(5) / T(3)) + abb[23] * (T(7) / T(2)) + abb[26] * (T(37) / T(6)) + -z[25] + -z[26];
z[27] = abb[3] * z[27];
z[28] = 7 * abb[19];
z[25] = abb[21] * (T(-13) / T(3)) + abb[25] * (T(5) / T(2)) + abb[24] * (T(5) / T(3)) + abb[26] * (T(7) / T(2)) + abb[23] * (T(37) / T(6)) + -z[25] + -z[28];
z[25] = abb[0] * z[25];
z[21] = 11 * abb[25] + abb[23] * (T(-1) / T(2)) + z[21] + z[26];
z[29] = abb[19] * (T(5) / T(2));
z[21] = (T(1) / T(3)) * z[21] + z[29];
z[21] = abb[7] * z[21];
z[30] = abb[21] + -abb[27];
z[31] = -abb[19] + -z[30];
z[31] = abb[2] * z[31];
z[32] = abb[1] + abb[6];
z[33] = abb[24] * z[32];
z[0] = z[0] + z[4] + 8 * z[11] + (T(1) / T(3)) * z[12] + z[13] + z[21] + z[24] + z[25] + z[27] + -2 * z[31] + (T(11) / T(3)) * z[33];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[4] = 5 * abb[27];
z[11] = 2 * abb[20];
z[12] = z[4] + z[11];
z[13] = abb[19] + -abb[21];
z[21] = 2 * abb[23];
z[24] = z[13] + -z[21];
z[25] = 4 * abb[25] + -z[12] + -z[24];
z[25] = abb[5] * z[25];
z[27] = abb[19] + abb[21];
z[33] = z[10] + -z[15] + z[27];
z[34] = 2 * abb[4];
z[34] = z[33] * z[34];
z[35] = z[31] + z[34];
z[25] = z[25] + z[35];
z[36] = 2 * abb[21];
z[37] = abb[25] + z[36];
z[38] = 2 * abb[19];
z[39] = abb[24] + z[38];
z[40] = 8 * abb[20] + -4 * abb[22] + 17 * abb[27] + -9 * z[14] + -z[37] + -z[39];
z[40] = abb[9] * z[40];
z[41] = -z[9] + z[36];
z[42] = 2 * abb[22];
z[43] = -z[11] + z[42];
z[44] = 3 * abb[27];
z[45] = z[43] + -z[44];
z[46] = -abb[23] + abb[24] + -abb[25] + z[41] + -z[45];
z[46] = abb[3] * z[46];
z[47] = abb[25] + z[38];
z[48] = abb[24] + z[8];
z[49] = abb[26] + z[45] + -z[47] + z[48];
z[49] = abb[0] * z[49];
z[50] = 2 * abb[25];
z[51] = z[21] + z[50];
z[52] = -z[4] + z[51];
z[53] = 2 * abb[26];
z[54] = -z[27] + z[52] + z[53];
z[54] = abb[1] * z[54];
z[13] = z[13] + z[53];
z[55] = -abb[27] + z[43];
z[56] = z[13] + z[55];
z[57] = abb[6] * z[56];
z[58] = 2 * abb[24];
z[32] = z[32] * z[58];
z[32] = z[32] + -z[57];
z[24] = z[24] + z[50] + -z[55];
z[55] = abb[7] * z[24];
z[40] = z[25] + -z[32] + z[40] + -z[46] + z[49] + -z[54] + -z[55];
z[46] = -abb[32] * z[40];
z[49] = 4 * abb[20];
z[54] = abb[23] + z[30] + z[38] + -z[49] + -z[50] + z[53];
z[54] = abb[1] * z[54];
z[57] = 3 * abb[21];
z[52] = -abb[19] + z[2] + z[43] + z[52] + -z[57];
z[52] = abb[3] * z[52];
z[59] = -z[36] + z[38];
z[60] = -z[4] + z[7];
z[61] = abb[20] + z[42];
z[19] = -z[19] + z[59] + -z[60] + z[61];
z[19] = abb[5] * z[19];
z[62] = z[9] + -z[11];
z[63] = 5 * abb[23];
z[64] = -abb[22] + z[22];
z[65] = z[37] + z[62] + z[63] + -z[64];
z[66] = 2 * abb[9];
z[65] = z[65] * z[66];
z[42] = -abb[20] + z[42];
z[47] = -abb[21] + abb[23] + z[42] + -z[47];
z[47] = abb[0] * z[47];
z[19] = -z[19] + z[34] + z[47] + z[52] + z[54] + -z[55] + -z[65];
z[47] = -abb[28] * z[19];
z[52] = 3 * abb[20];
z[54] = z[30] + z[52];
z[55] = -abb[22] + abb[24];
z[67] = 5 * abb[19];
z[68] = -z[7] + z[54] + -z[55] + z[67];
z[68] = abb[0] * z[68];
z[16] = -z[3] + -z[9] + z[16];
z[16] = abb[6] * z[16];
z[33] = abb[4] * z[33];
z[69] = abb[1] * (T(3) / T(2));
z[70] = abb[19] + -abb[23];
z[71] = -z[69] * z[70];
z[55] = -z[55] + z[70];
z[55] = abb[3] * z[55];
z[72] = -abb[22] + -z[21] + z[39];
z[73] = abb[9] * z[72];
z[29] = abb[23] * (T(3) / T(2)) + -z[29] + -z[54];
z[29] = abb[5] * z[29];
z[29] = z[16] + z[29] + z[33] + z[55] + z[68] + z[71] + z[73];
z[29] = prod_pow(abb[10], 2) * z[29];
z[4] = z[2] + -z[4] + z[20] + z[59] + -z[61];
z[4] = abb[8] * z[4];
z[20] = z[53] + z[58];
z[54] = 3 * abb[19];
z[55] = abb[21] + -z[20] + -z[43] + z[54] + -z[60];
z[55] = abb[0] * z[55];
z[59] = abb[19] + -abb[27];
z[49] = abb[26] + z[21] + z[36] + -z[49] + z[59];
z[49] = abb[1] * z[49];
z[60] = -z[11] + z[39];
z[61] = z[8] + z[60];
z[68] = 5 * abb[26];
z[64] = z[61] + -z[64] + z[68];
z[64] = z[64] * z[66];
z[71] = abb[19] + abb[24] + -abb[26] + z[36] + -z[42];
z[71] = abb[3] * z[71];
z[4] = z[4] + -z[32] + z[34] + z[49] + -z[55] + -z[64] + -z[71];
z[32] = -abb[31] * z[4];
z[34] = z[52] + z[59];
z[49] = -abb[22] + abb[25];
z[55] = -z[2] + z[18] + z[34] + -z[49];
z[55] = abb[3] * z[55];
z[30] = abb[19] + z[8] + 4 * z[30];
z[30] = abb[7] * z[30];
z[71] = -abb[21] + abb[26];
z[69] = z[69] * z[71];
z[73] = abb[22] + z[53];
z[74] = -z[37] + z[73];
z[75] = -abb[9] * z[74];
z[49] = -z[49] + -z[71];
z[49] = abb[0] * z[49];
z[23] = abb[26] * (T(3) / T(2)) + -z[23] + -z[34];
z[23] = abb[8] * z[23];
z[23] = z[23] + -z[30] + z[33] + z[49] + z[55] + z[69] + z[75];
z[23] = abb[12] * z[23];
z[34] = -z[22] + z[37];
z[37] = z[10] + z[34] + z[60];
z[37] = abb[9] * z[37];
z[11] = z[11] + z[44];
z[44] = z[11] + z[38];
z[49] = abb[21] + z[44] + -z[48] + -z[73];
z[49] = abb[0] * z[49];
z[55] = abb[5] * z[74];
z[60] = -z[9] + z[11];
z[69] = z[36] + z[60];
z[75] = -abb[22] + z[69];
z[21] = abb[25] + z[21];
z[76] = abb[19] + -z[21] + z[75];
z[76] = abb[3] * z[76];
z[33] = -z[33] + z[37] + z[49] + z[55] + z[76];
z[33] = abb[10] * z[33];
z[49] = abb[8] * abb[10] * z[72];
z[33] = z[33] + -z[49];
z[23] = z[23] + 2 * z[33];
z[23] = abb[12] * z[23];
z[33] = 7 * abb[26];
z[76] = z[33] + -z[57];
z[77] = -abb[22] + z[11];
z[77] = 2 * z[77];
z[78] = -z[7] + -z[76] + z[77];
z[78] = abb[8] * z[78];
z[21] = abb[21] + -z[21] + z[60];
z[60] = 2 * abb[3];
z[21] = z[21] * z[60];
z[55] = 2 * z[55];
z[79] = 3 * abb[1];
z[71] = z[71] * z[79];
z[74] = z[66] * z[74];
z[74] = -z[21] + -z[55] + z[71] + z[74] + z[78];
z[74] = abb[12] * z[74];
z[78] = abb[21] * (T(1) / T(2));
z[80] = abb[19] * (T(1) / T(2));
z[1] = -z[1] + -z[6] + z[15] + z[78] + z[80];
z[1] = abb[1] * z[1];
z[6] = z[22] + -z[43] + -z[63] + -z[68];
z[6] = abb[9] * z[6];
z[15] = -z[22] + z[42];
z[42] = abb[21] + abb[23] * (T(11) / T(2)) + z[2] + z[15] + -z[80];
z[42] = abb[5] * z[42];
z[15] = abb[19] + abb[26] * (T(11) / T(2)) + z[7] + z[15] + -z[78];
z[15] = abb[8] * z[15];
z[14] = z[14] + -z[27];
z[27] = -abb[20] + z[14];
z[43] = abb[0] + abb[3];
z[27] = z[27] * z[43];
z[1] = z[1] + z[6] + z[15] + z[27] + z[42];
z[1] = abb[13] * z[1];
z[6] = -abb[19] + -z[11] + z[48] + z[53];
z[15] = 2 * abb[0];
z[6] = z[6] * z[15];
z[27] = 7 * abb[23];
z[42] = z[27] + -z[54];
z[11] = z[11] + -z[73];
z[11] = 2 * z[11] + -z[42];
z[11] = abb[5] * z[11];
z[43] = z[70] * z[79];
z[48] = -z[66] * z[72];
z[11] = z[6] + z[11] + -z[43] + z[48];
z[11] = abb[10] * z[11];
z[48] = 2 * z[49];
z[1] = z[1] + z[11] + z[48] + z[74];
z[1] = abb[13] * z[1];
z[11] = abb[21] + 6 * abb[23] + abb[26] + -z[3] + z[58] + -z[77];
z[11] = abb[0] * z[11];
z[49] = z[56] + -z[58];
z[49] = abb[3] * z[49];
z[41] = z[41] + -z[59];
z[41] = abb[6] * z[41];
z[11] = -z[11] + -z[35] + z[41] + -z[49] + z[64];
z[35] = abb[29] * z[11];
z[41] = abb[24] + abb[25];
z[49] = abb[19] + z[41] + -z[69];
z[49] = abb[3] * z[49];
z[41] = -z[41] + z[44];
z[44] = abb[21] + z[8];
z[53] = -z[41] + z[44];
z[53] = abb[0] * z[53];
z[31] = -z[16] + z[30] + 4 * z[31] + -z[37] + z[49] + z[53];
z[31] = prod_pow(abb[11], 2) * z[31];
z[24] = abb[0] * z[24];
z[37] = abb[19] + abb[23] + z[50] + -2 * z[75];
z[37] = abb[3] * z[37];
z[44] = -abb[27] + -z[38] + z[44];
z[44] = abb[7] * z[44];
z[24] = -z[24] + z[25] + z[37] + z[44] + -z[65];
z[25] = -abb[30] * z[24];
z[12] = 4 * abb[24] + -z[12] + z[13];
z[12] = abb[8] * z[12];
z[13] = -abb[29] + -abb[32];
z[13] = z[12] * z[13];
z[0] = z[0] + z[1] + z[13] + z[23] + z[25] + z[29] + z[31] + z[32] + z[35] + z[46] + z[47];
z[1] = 5 * abb[20] + 2 * abb[27];
z[13] = -z[1] + -z[26] + z[33] + -z[38] + z[51];
z[13] = z[13] * z[60];
z[23] = -abb[27] + z[52];
z[25] = -abb[22] + z[23];
z[7] = 6 * abb[19] + -z[7] + -z[9] + z[18] + 2 * z[25] + z[58];
z[7] = abb[8] * z[7];
z[5] = abb[22] + -abb[26] + -z[5] + z[22] + -z[50] + -z[61];
z[5] = z[5] * z[66];
z[9] = -z[10] + z[36] + z[41];
z[10] = -z[9] * z[15];
z[5] = z[5] + z[7] + z[10] + z[13] + 2 * z[30] + -z[55] + -z[71];
z[5] = abb[12] * z[5];
z[1] = -z[1] + z[20] + z[27] + -z[28] + -z[36];
z[1] = z[1] * z[15];
z[3] = z[3] + z[58];
z[7] = abb[22] + -abb[23] + -z[3] + -z[34] + -z[62];
z[7] = z[7] * z[66];
z[10] = z[23] + z[57] + -z[73];
z[8] = -z[8] + 2 * z[10] + z[50] + z[67];
z[8] = abb[5] * z[8];
z[9] = -z[9] * z[60];
z[1] = z[1] + z[7] + z[8] + z[9] + -2 * z[16] + z[43];
z[1] = abb[10] * z[1];
z[3] = 8 * abb[23] + -z[3] + 2 * z[45] + z[76];
z[3] = abb[8] * z[3];
z[2] = z[2] + -z[36] + z[45];
z[2] = 2 * z[2] + z[42] + -z[50];
z[2] = abb[5] * z[2];
z[7] = -z[14] * z[79];
z[8] = abb[21] + -z[17];
z[8] = abb[25] + 2 * z[8] + z[39];
z[8] = z[8] * z[66];
z[2] = z[2] + z[3] + -z[6] + z[7] + z[8] + z[21];
z[2] = abb[13] * z[2];
z[1] = z[1] + z[2] + z[5] + z[48];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[18] * z[40];
z[3] = -abb[14] * z[19];
z[4] = -abb[17] * z[4];
z[5] = abb[15] * z[11];
z[6] = -abb[16] * z[24];
z[7] = -abb[15] + -abb[18];
z[7] = z[7] * z[12];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_306_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("78.135410095317006254736202490325081849004317829218278764627143899"),stof<T>("76.795105705183438587062138532197338067845863996167576407415733066")}, std::complex<T>{stof<T>("123.537363246780312765448432585952655057876045602262698331997061197"),stof<T>("62.449565911963225079288696877138454353026628729490688069048271101")}, std::complex<T>{stof<T>("53.244619184109923112274903638284260001464986704536325329577431102"),stof<T>("93.482772403330668237050015628131808994985779184491328995920294377")}, std::complex<T>{stof<T>("-109.356027963322682846051058722470609110618245794656743721499354372"),stof<T>("88.451760461163170739580591359235959179444522156009531067682977588")}, std::complex<T>{stof<T>("-121.35106966431089897094711604182470212616357453500015049721206558"),stof<T>("-147.32204086687222196925641272895146918380777265552697310392410874")}, std::complex<T>{stof<T>("1.470120519011008190139430587688042343218415864285356550679957287"),stof<T>("-62.24484391841205541057355820545907524692043911217652846679426971")}, std::complex<T>{stof<T>("20.126853833625761274242277192687764210513436985270529121195400339"),stof<T>("-66.930758053291214191362429759842574081003883826678419908731223757")}, std::complex<T>{stof<T>("-96.46027875310381582848581718978388027862424341031819706216235279"),stof<T>("-164.00970756501945161924428982488594011094768784385072569242867005")}, std::complex<T>{stof<T>("130.97471317967980609788026449359442283404564965088272978220891843"),stof<T>("111.25077667222888224454967080281982126444170944220481400435089155")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_306_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_306_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-9.08239599607100444499835871257484632869974500843565895930497672"),stof<T>("-154.15919366899997391712137303054484271994292702678922212182053963")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,33> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_306_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_306_DLogXconstant_part(base_point<T>, kend);
	value += f_4_306_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_306_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_306_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_306_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_306_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_306_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_306_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
