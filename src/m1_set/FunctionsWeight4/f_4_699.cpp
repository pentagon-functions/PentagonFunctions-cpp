/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_699.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_699_abbreviated (const std::array<T,70>& abb) {
T z[119];
z[0] = abb[56] + abb[57];
z[1] = abb[58] + abb[60];
z[2] = z[0] + z[1];
z[3] = abb[16] * z[2];
z[4] = -abb[52] + abb[55];
z[5] = -abb[51] + abb[54];
z[6] = -2 * abb[53] + z[4] + z[5];
z[7] = abb[26] * z[6];
z[8] = z[3] + z[7];
z[9] = -abb[61] + z[2];
z[10] = abb[0] * z[9];
z[11] = abb[59] + z[2];
z[12] = -abb[61] + z[11];
z[12] = abb[9] * z[12];
z[13] = 2 * z[12];
z[14] = abb[59] + abb[61];
z[14] = abb[10] * z[14];
z[15] = 2 * z[14];
z[16] = -z[8] + 4 * z[10] + -z[13] + z[15];
z[17] = abb[8] * z[2];
z[18] = 4 * z[17];
z[19] = abb[7] * z[2];
z[20] = z[18] + z[19];
z[21] = -abb[59] + z[2];
z[22] = abb[4] * z[21];
z[23] = abb[58] + abb[60] + z[0];
z[23] = abb[59] + 2 * z[23];
z[24] = abb[3] * z[23];
z[25] = z[22] + z[24];
z[26] = -abb[53] + z[5];
z[27] = abb[25] * z[26];
z[28] = abb[23] * z[26];
z[29] = z[27] + z[28];
z[30] = abb[22] * z[26];
z[31] = abb[21] * z[26];
z[32] = z[30] + z[31];
z[33] = -z[4] + z[5];
z[34] = -abb[24] * z[33];
z[35] = abb[13] * z[11];
z[36] = abb[14] * abb[59];
z[37] = z[35] + -z[36];
z[38] = -z[16] + z[20] + -z[25] + -z[29] + -z[32] + z[34] + -z[37];
z[38] = abb[28] * z[38];
z[39] = abb[4] * z[11];
z[40] = abb[3] * abb[59];
z[18] = z[18] + -z[39] + z[40];
z[41] = -abb[53] + z[4];
z[42] = abb[23] * z[41];
z[43] = abb[25] * z[41];
z[44] = z[42] + z[43];
z[45] = z[34] + z[44];
z[46] = abb[5] + abb[6];
z[47] = z[2] * z[46];
z[48] = abb[59] + z[1];
z[49] = abb[12] * z[48];
z[50] = 2 * z[49];
z[51] = abb[59] + z[0];
z[51] = abb[11] * z[51];
z[52] = 2 * z[51];
z[53] = z[50] + -z[52];
z[54] = -3 * z[0] + z[1];
z[54] = abb[7] * z[54];
z[54] = -z[18] + z[32] + 3 * z[37] + -z[45] + -z[47] + -z[53] + z[54];
z[54] = abb[30] * z[54];
z[55] = z[16] + z[19];
z[56] = abb[5] * z[11];
z[57] = abb[6] * abb[59];
z[58] = z[56] + -z[57];
z[59] = z[45] + z[58];
z[60] = -abb[22] * z[33];
z[61] = abb[2] * z[2];
z[62] = z[60] + z[61];
z[63] = abb[3] * z[11];
z[64] = abb[4] * abb[59];
z[65] = -z[63] + z[64];
z[66] = 2 * z[35];
z[67] = 2 * z[36];
z[68] = -z[66] + z[67];
z[69] = -z[55] + -z[59] + -z[62] + z[65] + -z[68];
z[69] = abb[32] * z[69];
z[70] = abb[25] * z[6];
z[71] = abb[23] * z[6];
z[72] = abb[1] * z[0];
z[16] = z[16] + z[47] + -z[53] + z[70] + z[71] + -4 * z[72];
z[16] = abb[33] * z[16];
z[47] = -z[0] + z[48];
z[47] = abb[7] * z[47];
z[47] = z[35] + z[47];
z[48] = z[36] + z[72];
z[71] = 2 * z[64];
z[73] = abb[24] * z[41];
z[48] = -z[47] + 2 * z[48] + z[53] + z[63] + -z[71] + z[73];
z[48] = abb[29] * z[48];
z[74] = abb[59] + 2 * z[0];
z[74] = abb[7] * z[74];
z[74] = z[36] + -z[66] + z[74];
z[75] = abb[24] * z[26];
z[75] = z[30] + z[75];
z[76] = z[31] + z[75];
z[77] = z[29] + z[76];
z[78] = 2 * z[63];
z[79] = 2 * z[72];
z[53] = z[53] + z[79];
z[80] = z[53] + -z[64] + z[74] + -z[77] + z[78];
z[80] = abb[34] * z[80];
z[81] = 2 * abb[0];
z[9] = z[9] * z[81];
z[81] = z[9] + z[15];
z[82] = abb[16] * z[11];
z[83] = abb[26] * z[41];
z[83] = z[82] + z[83];
z[84] = -z[81] + z[83];
z[85] = z[35] + z[84];
z[86] = abb[2] * z[11];
z[87] = abb[22] * z[41];
z[86] = z[86] + z[87];
z[88] = z[56] + z[86];
z[89] = z[85] + -z[88];
z[89] = abb[31] * z[89];
z[90] = -z[9] + z[13];
z[91] = abb[16] * abb[59];
z[92] = z[90] + -z[91];
z[93] = z[31] + z[36];
z[94] = z[92] + -z[93];
z[95] = abb[2] * abb[59];
z[96] = z[57] + z[95];
z[97] = z[94] + z[96];
z[97] = abb[35] * z[97];
z[98] = abb[31] * z[41];
z[99] = abb[35] * z[26];
z[98] = z[98] + z[99];
z[100] = abb[23] + abb[25];
z[98] = -z[98] * z[100];
z[101] = abb[26] * z[99];
z[89] = z[89] + z[97] + z[98] + z[101];
z[16] = z[16] + z[38] + z[48] + z[54] + z[69] + z[80] + -z[89];
z[16] = m1_set::bc<T>[0] * z[16];
z[38] = -z[64] + z[96];
z[48] = z[38] + z[75];
z[54] = abb[26] * z[26];
z[69] = abb[7] * abb[61];
z[80] = 2 * z[69];
z[96] = abb[7] * abb[59];
z[91] = z[15] + z[48] + z[54] + -z[80] + -z[91] + -z[96];
z[97] = abb[48] * z[91];
z[98] = abb[6] * z[11];
z[101] = z[44] + z[73];
z[102] = -z[39] + z[56] + z[98] + z[101];
z[103] = abb[7] * z[11];
z[104] = -z[35] + z[103];
z[105] = z[50] + z[79];
z[106] = -z[102] + z[104] + z[105];
z[106] = abb[45] * z[106];
z[107] = abb[5] * abb[59];
z[108] = -z[40] + z[57] + z[107];
z[109] = -z[52] + z[108];
z[110] = -z[36] + z[96];
z[111] = abb[21] + abb[22];
z[112] = -abb[24] + -z[111];
z[112] = -z[26] * z[112];
z[112] = z[79] + z[109] + -z[110] + z[112];
z[112] = abb[47] * z[112];
z[13] = -z[13] + -z[80] + z[103];
z[88] = -z[63] + z[88];
z[101] = z[88] + z[101];
z[83] = z[13] + z[83] + -z[101];
z[83] = abb[46] * z[83];
z[39] = -z[39] + z[73] + z[85] + -z[103];
z[39] = abb[44] * z[39];
z[85] = z[37] + z[65];
z[113] = -z[19] + z[80];
z[15] = -z[15] + z[85] + z[90] + z[113];
z[90] = -abb[49] * z[15];
z[111] = z[100] + z[111];
z[111] = abb[59] * z[111];
z[114] = 2 * abb[59] + z[2];
z[115] = abb[24] * z[114];
z[111] = z[111] + z[115];
z[115] = -abb[69] * z[111];
z[75] = -z[75] + z[96];
z[40] = z[40] + z[75] + z[94];
z[29] = -z[29] + z[40] + z[54];
z[29] = abb[43] * z[29];
z[16] = z[16] + z[29] + z[39] + z[83] + z[90] + z[97] + z[106] + z[112] + z[115];
z[16] = 4 * z[16];
z[17] = 2 * z[17];
z[29] = -z[17] + z[37];
z[39] = abb[15] * z[2];
z[25] = -z[25] + z[28] + -z[29] + z[31] + z[39] + z[43] + z[61] + z[87];
z[25] = abb[36] * z[25];
z[54] = abb[67] * z[91];
z[83] = abb[3] * z[2];
z[48] = z[29] + z[48] + z[83] + -z[103];
z[87] = abb[41] * z[48];
z[22] = -z[17] + z[22];
z[44] = z[22] + z[35] + -z[44] + -z[88];
z[90] = abb[39] * z[44];
z[81] = z[81] + -z[82] + z[104];
z[81] = abb[63] * z[81];
z[24] = -z[17] + z[24];
z[38] = -z[24] + -z[38] + z[93];
z[91] = -abb[38] * z[38];
z[13] = -z[13] + z[73] + -z[82] + z[88];
z[13] = abb[65] * z[13];
z[82] = abb[4] * z[2];
z[88] = -z[29] + -z[82] + -z[96] + z[101];
z[94] = abb[40] * z[88];
z[93] = z[79] + z[93];
z[75] = z[75] + -z[93] + -z[109];
z[75] = abb[66] * z[75];
z[96] = -z[34] + z[39];
z[97] = -abb[23] * z[33];
z[97] = -z[96] + z[97];
z[101] = -z[0] + z[1];
z[101] = abb[7] * z[101];
z[103] = z[97] + z[101];
z[106] = abb[5] + -abb[6];
z[106] = z[106] * z[114];
z[106] = z[62] + z[79] + z[103] + z[106];
z[109] = abb[37] * z[106];
z[15] = abb[68] * z[15];
z[112] = abb[59] + abb[60];
z[115] = z[0] + z[112];
z[115] = abb[64] * z[115];
z[116] = abb[58] * abb[64];
z[115] = z[115] + z[116];
z[11] = abb[63] * z[11];
z[11] = z[11] + -z[115];
z[11] = abb[4] * z[11];
z[40] = abb[62] * z[40];
z[111] = abb[50] * z[111];
z[46] = -abb[7] + abb[13] + z[46];
z[46] = z[46] * z[115];
z[115] = abb[63] * z[41];
z[117] = abb[64] * z[41];
z[118] = -z[115] + z[117];
z[118] = abb[24] * z[118];
z[79] = -abb[64] * z[79];
z[11] = z[11] + z[13] + z[15] + z[25] + -z[40] + z[46] + -z[54] + z[75] + z[79] + z[81] + z[87] + z[90] + z[91] + -z[94] + -z[109] + -z[111] + z[118];
z[1] = -z[0] + -8 * z[1];
z[13] = 2 * abb[7];
z[1] = z[1] * z[13];
z[1] = -z[1] + z[3] + z[7] + -z[37] + -z[45] + -z[58] + -z[60] + -z[61] + -20 * z[69] + -14 * z[72];
z[3] = z[49] + -z[51];
z[7] = z[12] + -z[14];
z[1] = (T(-1) / T(3)) * z[1] + (T(14) / T(3)) * z[3] + 6 * z[7] + (T(-16) / T(3)) * z[10] + (T(2) / T(3)) * z[65];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[3] = -z[32] + z[58];
z[12] = -z[3] + -z[19] + z[29] + -z[97];
z[13] = -abb[28] + abb[32];
z[12] = z[12] * z[13];
z[13] = z[17] + z[50] + z[74] + -z[76] + -z[108];
z[13] = abb[34] * z[13];
z[3] = z[3] + z[37] + -z[53] + z[103];
z[3] = abb[33] * z[3];
z[14] = z[17] + -z[47] + -z[52] + z[67] + z[102];
z[14] = abb[29] * z[14];
z[3] = z[3] + z[12] + z[13] + z[14];
z[0] = -abb[7] * z[0];
z[0] = -z[0] + -z[30] + z[34] + -z[37] + -z[72];
z[12] = -abb[6] * z[21];
z[4] = abb[53] + -2 * z[4] + z[5];
z[4] = abb[23] * z[4];
z[5] = 2 * z[31];
z[13] = -abb[5] * z[23];
z[0] = -2 * z[0] + z[4] + z[5] + z[12] + z[13] + -z[18] + z[39] + -z[43];
z[0] = abb[30] * z[0];
z[0] = z[0] + 2 * z[3];
z[0] = abb[30] * z[0];
z[3] = -z[27] + z[37] + z[39] + -z[42] + -z[55] + -z[58] + -z[82] + -z[83];
z[3] = abb[28] * z[3];
z[4] = abb[34] * z[48];
z[12] = z[4] + -z[89];
z[3] = z[3] + 2 * z[12];
z[3] = abb[28] * z[3];
z[6] = abb[22] * z[6];
z[6] = z[5] + z[6] + -4 * z[7] + -z[8] + 6 * z[10] + 2 * z[28] + z[61] + z[70];
z[7] = -z[6] + z[20] + z[71] + -z[78] + z[80] + -z[96];
z[7] = abb[28] * z[7];
z[10] = z[63] + -z[77] + z[104];
z[10] = abb[34] * z[10];
z[10] = z[10] + -z[89];
z[8] = -z[8] + z[9] + z[62];
z[9] = -z[8] + z[37] + -z[59] + -z[80];
z[9] = abb[32] * z[9];
z[7] = z[7] + z[9] + 2 * z[10];
z[7] = abb[32] * z[7];
z[9] = -z[96] + z[113];
z[6] = z[6] + -z[9] + z[68];
z[6] = abb[28] * z[6];
z[10] = 2 * z[56];
z[12] = 2 * z[57];
z[8] = z[8] + z[9] + z[10] + -z[12] + 2 * z[42] + z[70];
z[8] = abb[32] * z[8];
z[9] = -z[52] + z[93] + -z[95] + z[107];
z[9] = abb[34] * z[9];
z[9] = z[9] + z[89];
z[13] = -abb[33] * z[106];
z[6] = z[6] + z[8] + 2 * z[9] + z[13];
z[6] = abb[33] * z[6];
z[8] = -z[10] + z[22] + z[63] + z[66] + z[84] + -2 * z[86];
z[9] = prod_pow(abb[31], 2);
z[8] = z[8] * z[9];
z[10] = -abb[31] * z[44];
z[13] = -z[35] + z[86] + -z[98] + z[105];
z[13] = abb[33] * z[13];
z[14] = -z[53] + z[85] + z[101];
z[14] = abb[34] * z[14];
z[15] = -abb[28] * z[88];
z[17] = -z[64] + z[73] + -z[110];
z[17] = abb[32] * z[17];
z[10] = z[10] + z[13] + z[14] + z[15] + z[17];
z[13] = -abb[29] * z[88];
z[10] = 2 * z[10] + z[13];
z[10] = abb[29] * z[10];
z[9] = z[9] * z[41];
z[13] = prod_pow(abb[35], 2);
z[14] = z[13] * z[26];
z[15] = abb[38] * z[26];
z[17] = abb[62] * z[26];
z[18] = abb[65] * z[41];
z[9] = -z[9] + -z[14] + -z[15] + z[17] + z[18] + z[117];
z[15] = 2 * z[100];
z[9] = z[9] * z[15];
z[15] = -z[36] + z[95];
z[5] = -z[5] + z[12] + 2 * z[15] + z[24] + -z[64] + z[92];
z[5] = z[5] * z[13];
z[12] = -abb[18] + abb[19];
z[12] = z[2] * z[12];
z[13] = -abb[27] * z[33];
z[15] = abb[20] * z[114];
z[2] = -2 * abb[61] + z[2];
z[2] = abb[17] * z[2];
z[2] = z[2] + z[12] + z[13] + z[15];
z[2] = abb[42] * z[2];
z[12] = abb[35] * z[38];
z[13] = z[99] * z[100];
z[12] = z[12] + z[13];
z[4] = z[4] + 2 * z[12];
z[4] = abb[34] * z[4];
z[12] = z[17] + z[18] + z[115];
z[12] = -2 * z[12] + z[14];
z[12] = abb[26] * z[12];
z[13] = -abb[64] * z[112];
z[13] = z[13] + -z[116];
z[13] = abb[12] * z[13];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + 2 * z[11] + z[12] + 4 * z[13];
z[0] = 2 * z[0];
return {z[16], z[0]};
}


template <typename T> std::complex<T> f_4_699_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("-12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("53.578578824072759289728574844223385287980097839800100636397660361"),stof<T>("33.047500621757702881943217170995313409495833620192392272446330992")}, std::complex<T>{stof<T>("53.578578824072759289728574844223385287980097839800100636397660361"),stof<T>("33.047500621757702881943217170995313409495833620192392272446330992")}, std::complex<T>{stof<T>("64.849995488792097742935953288470591647478423160710791105255322236"),stof<T>("47.3538622261288460802320725306375279856595762109294190695023897")}, std::complex<T>{stof<T>("64.849995488792097742935953288470591647478423160710791105255322236"),stof<T>("47.3538622261288460802320725306375279856595762109294190695023897")}, std::complex<T>{stof<T>("-25.722252780192909327092031474302414320260816278425355599495051827"),stof<T>("-23.243718192739844943996252882180639840840353626088414697033473416")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_699_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_699_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("39.791654278296587363448479654626997047413156050698524340387821261"),stof<T>("-34.600555759611050309919618566339220328265813945433386627803780576")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,70> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_27_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_27_re(k)};

                    
            return f_4_699_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_699_DLogXconstant_part(base_point<T>, kend);
	value += f_4_699_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_699_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_699_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_699_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_699_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_699_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_699_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
