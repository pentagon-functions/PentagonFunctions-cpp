/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_674.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_674_abbreviated (const std::array<T,73>& abb) {
T z[108];
z[0] = abb[32] * (T(1) / T(4));
z[1] = abb[33] * (T(1) / T(4));
z[2] = abb[34] * (T(3) / T(2));
z[3] = -abb[31] + abb[36];
z[4] = z[0] + -z[1] + z[2] + z[3];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = 3 * abb[39];
z[6] = 3 * abb[41] + -abb[43];
z[7] = z[5] + z[6];
z[7] = (T(1) / T(4)) * z[7];
z[4] = z[4] + -z[7];
z[4] = abb[4] * z[4];
z[8] = abb[36] * (T(1) / T(2));
z[9] = -abb[31] + z[8];
z[10] = abb[33] * (T(1) / T(2));
z[11] = z[9] + -z[10];
z[12] = abb[34] * (T(1) / T(2));
z[13] = abb[35] * (T(1) / T(2));
z[14] = z[12] + -z[13];
z[15] = z[11] + z[14];
z[15] = m1_set::bc<T>[0] * z[15];
z[16] = abb[40] + abb[42];
z[17] = abb[43] + z[16];
z[15] = z[15] + (T(1) / T(2)) * z[17];
z[17] = abb[7] * (T(1) / T(2));
z[15] = z[15] * z[17];
z[18] = -abb[35] + z[8];
z[19] = abb[32] * (T(1) / T(2));
z[20] = abb[34] + z[18] + -z[19];
z[20] = m1_set::bc<T>[0] * z[20];
z[21] = abb[39] + abb[41];
z[22] = z[16] + -z[21];
z[20] = z[20] + (T(1) / T(2)) * z[22];
z[22] = abb[17] * (T(1) / T(2));
z[23] = z[20] * z[22];
z[24] = abb[43] + z[21];
z[25] = z[16] + z[24];
z[26] = abb[8] * z[25];
z[23] = -z[23] + (T(1) / T(4)) * z[26];
z[27] = abb[36] * (T(3) / T(4));
z[19] = -abb[31] + z[19];
z[28] = (T(1) / T(2)) * z[19] + z[27];
z[29] = abb[33] * (T(3) / T(2));
z[30] = abb[34] + z[28] + -z[29];
z[30] = m1_set::bc<T>[0] * z[30];
z[31] = abb[42] + z[6];
z[30] = z[30] + (T(-1) / T(4)) * z[31];
z[30] = abb[3] * z[30];
z[31] = abb[5] * abb[42];
z[32] = -abb[47] + z[31];
z[33] = abb[9] * z[24];
z[32] = (T(1) / T(2)) * z[32] + -z[33];
z[11] = abb[9] * z[11];
z[34] = abb[13] * abb[33];
z[34] = -z[11] + z[34];
z[35] = abb[33] + -abb[35];
z[36] = abb[5] * z[35];
z[37] = z[34] + (T(1) / T(4)) * z[36];
z[37] = m1_set::bc<T>[0] * z[37];
z[38] = -abb[18] + abb[21] * (T(-1) / T(2)) + abb[20] * (T(3) / T(2));
z[39] = abb[44] * (T(1) / T(4));
z[40] = z[38] * z[39];
z[41] = -abb[31] + abb[34];
z[42] = abb[36] * (T(3) / T(2)) + z[41];
z[43] = -z[13] + z[42];
z[43] = m1_set::bc<T>[0] * z[43];
z[43] = abb[39] * (T(-3) / T(2)) + z[43];
z[43] = abb[1] * z[43];
z[44] = -abb[31] + abb[32];
z[45] = z[14] + -z[44];
z[46] = m1_set::bc<T>[0] * z[45];
z[46] = abb[40] + z[46];
z[47] = abb[16] * (T(1) / T(4));
z[48] = z[46] * z[47];
z[49] = z[3] + z[14];
z[49] = m1_set::bc<T>[0] * z[49];
z[49] = -abb[39] + z[49];
z[50] = abb[15] * z[49];
z[4] = z[4] + z[15] + -z[23] + z[30] + (T(-1) / T(2)) * z[32] + -z[37] + z[40] + z[43] + -z[48] + (T(-3) / T(4)) * z[50];
z[15] = abb[56] + abb[58];
z[30] = (T(1) / T(4)) * z[15];
z[4] = z[4] * z[30];
z[32] = -abb[47] + -z[31];
z[37] = abb[0] * abb[40];
z[40] = abb[41] + abb[43];
z[51] = -abb[9] * z[40];
z[32] = (T(1) / T(2)) * z[32] + z[37] + z[51];
z[3] = -z[3] + z[10] + -z[12];
z[3] = abb[9] * z[3];
z[37] = abb[0] * z[45];
z[45] = (T(-1) / T(2)) * z[36] + z[37];
z[3] = z[3] + (T(1) / T(2)) * z[45];
z[3] = m1_set::bc<T>[0] * z[3];
z[45] = 3 * abb[31];
z[51] = -z[13] + z[45];
z[52] = abb[34] * (T(5) / T(2));
z[53] = 3 * abb[33];
z[54] = -3 * abb[36] + z[51] + -z[52] + z[53];
z[54] = m1_set::bc<T>[0] * z[54];
z[16] = -abb[43] + z[16] + z[54];
z[54] = abb[7] * (T(1) / T(4));
z[16] = z[16] * z[54];
z[44] = -abb[33] + z[44];
z[55] = abb[35] * (T(3) / T(2));
z[56] = z[12] + -z[44] + -z[55];
z[56] = m1_set::bc<T>[0] * z[56];
z[56] = -z[24] + z[56];
z[57] = abb[4] * (T(1) / T(4));
z[56] = z[56] * z[57];
z[58] = -z[8] + -z[19];
z[58] = m1_set::bc<T>[0] * z[58];
z[40] = -abb[42] + -z[40];
z[40] = (T(1) / T(2)) * z[40] + z[58];
z[58] = abb[3] * (T(1) / T(2));
z[40] = z[40] * z[58];
z[59] = abb[1] * (T(1) / T(2));
z[35] = m1_set::bc<T>[0] * z[35];
z[35] = abb[42] + z[35];
z[35] = z[35] * z[59];
z[60] = -abb[18] + abb[19] + abb[20];
z[61] = abb[21] + z[60];
z[62] = abb[44] * (T(1) / T(8));
z[63] = -z[61] * z[62];
z[3] = z[3] + z[16] + z[23] + (T(1) / T(2)) * z[32] + z[35] + z[40] + -z[48] + (T(1) / T(4)) * z[50] + z[56] + z[63];
z[16] = abb[59] * (T(1) / T(4));
z[3] = z[3] * z[16];
z[23] = -abb[0] + 3 * abb[2] + abb[10];
z[23] = abb[40] * z[23];
z[32] = -abb[36] + z[53];
z[32] = m1_set::bc<T>[0] * z[32];
z[32] = abb[42] + z[32];
z[32] = abb[14] * z[32];
z[31] = -abb[47] + 3 * z[31];
z[31] = z[23] + (T(-1) / T(2)) * z[31] + z[32] + z[33];
z[35] = abb[31] * (T(1) / T(2));
z[40] = -abb[32] + z[35];
z[14] = z[14] + z[40];
z[53] = abb[2] * z[14];
z[56] = abb[10] * z[35];
z[37] = (T(1) / T(4)) * z[37] + -z[53] + -z[56];
z[53] = abb[13] * z[10];
z[11] = (T(-1) / T(2)) * z[11] + (T(3) / T(8)) * z[36] + z[37] + z[53];
z[11] = m1_set::bc<T>[0] * z[11];
z[36] = abb[16] * z[46];
z[53] = abb[33] + -abb[34];
z[53] = m1_set::bc<T>[0] * z[53];
z[53] = abb[41] + z[53];
z[56] = abb[6] * z[53];
z[36] = z[36] + z[50] + -z[56];
z[56] = -abb[34] + abb[35];
z[28] = z[10] + -z[28] + z[56];
z[28] = m1_set::bc<T>[0] * z[28];
z[63] = 3 * abb[42];
z[6] = z[6] + -z[63];
z[6] = (T(1) / T(4)) * z[6] + z[28];
z[6] = z[6] * z[58];
z[28] = abb[17] * (T(1) / T(4));
z[64] = -z[20] * z[28];
z[51] = -abb[32] + abb[33] + abb[34] * (T(-13) / T(2)) + z[51];
z[51] = -abb[36] + (T(1) / T(4)) * z[51];
z[51] = m1_set::bc<T>[0] * z[51];
z[7] = z[7] + z[51];
z[51] = abb[4] * (T(1) / T(2));
z[7] = z[7] * z[51];
z[42] = z[10] + -z[42];
z[42] = m1_set::bc<T>[0] * z[42];
z[5] = abb[42] + z[5];
z[5] = (T(1) / T(2)) * z[5] + z[42];
z[5] = z[5] * z[59];
z[42] = abb[31] * (T(1) / T(4));
z[27] = abb[32] + abb[34] * (T(-7) / T(8)) + abb[33] * (T(3) / T(4)) + abb[35] * (T(13) / T(8)) + -z[27] + z[42];
z[27] = m1_set::bc<T>[0] * z[27];
z[63] = -3 * abb[40] + -abb[43] + -z[63];
z[27] = z[27] + (T(1) / T(4)) * z[63];
z[27] = z[17] * z[27];
z[60] = -abb[21] + z[60];
z[60] = (T(3) / T(16)) * z[60];
z[63] = -abb[44] * z[60];
z[5] = z[5] + z[6] + z[7] + z[11] + (T(1) / T(8)) * z[26] + z[27] + (T(-1) / T(4)) * z[31] + (T(3) / T(8)) * z[36] + z[63] + z[64];
z[6] = abb[61] * (T(1) / T(2));
z[5] = z[5] * z[6];
z[7] = -z[45] + z[52] + -z[55];
z[7] = abb[36] + (T(1) / T(4)) * z[7];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = -abb[39] + z[7];
z[7] = abb[4] * z[7];
z[11] = -abb[34] + -abb[36];
z[11] = m1_set::bc<T>[0] * z[11];
z[11] = abb[39] + z[11];
z[11] = abb[9] * z[11];
z[11] = z[11] + -z[50];
z[26] = abb[31] + z[13];
z[27] = z[12] + z[26];
z[27] = m1_set::bc<T>[0] * z[27];
z[31] = -z[27] * z[54];
z[36] = 3 * abb[18] + abb[19];
z[36] = -abb[20] + (T(1) / T(2)) * z[36];
z[45] = -z[36] * z[39];
z[7] = z[7] + (T(1) / T(2)) * z[11] + z[31] + z[43] + z[45];
z[11] = abb[54] * (T(1) / T(4));
z[7] = z[7] * z[11];
z[31] = m1_set::bc<T>[0] * z[37];
z[37] = -abb[32] + abb[34] * (T(5) / T(8));
z[43] = abb[35] * (T(3) / T(8)) + -z[37] + -z[42];
z[43] = m1_set::bc<T>[0] * z[43];
z[45] = abb[40] * (T(1) / T(2));
z[43] = z[43] + -z[45];
z[43] = z[17] * z[43];
z[50] = -abb[21] + abb[18] * (T(-1) / T(2)) + abb[19] * (T(3) / T(2));
z[52] = -z[50] * z[62];
z[55] = abb[4] * (T(1) / T(8));
z[63] = -z[27] * z[55];
z[23] = (T(-1) / T(4)) * z[23] + z[31] + z[43] + z[48] + z[52] + z[63];
z[31] = abb[60] * (T(1) / T(2));
z[23] = z[23] * z[31];
z[43] = abb[23] * z[25];
z[48] = abb[25] * z[25];
z[46] = abb[27] * z[46];
z[49] = abb[22] * z[49];
z[43] = z[43] + z[46] + z[48] + -z[49];
z[20] = abb[28] * z[20];
z[26] = -abb[33] + abb[36] + z[2] + -z[26];
z[26] = m1_set::bc<T>[0] * z[26];
z[21] = -z[21] + z[26];
z[21] = (T(1) / T(2)) * z[21];
z[26] = abb[24] + abb[26];
z[21] = z[21] * z[26];
z[20] = z[20] + -z[21] + (T(1) / T(2)) * z[43];
z[21] = abb[29] * z[39];
z[21] = z[20] + z[21];
z[39] = abb[50] + abb[52];
z[43] = abb[63] * (T(1) / T(2)) + (T(1) / T(8)) * z[39];
z[21] = z[21] * z[43];
z[43] = abb[48] + abb[49] + abb[51] + abb[53];
z[46] = (T(-3) / T(8)) * z[43];
z[20] = z[20] * z[46];
z[46] = abb[10] * abb[40];
z[32] = z[32] + -z[33] + z[46];
z[33] = abb[10] * abb[31];
z[33] = z[33] + z[34];
z[33] = m1_set::bc<T>[0] * z[33];
z[32] = (T(1) / T(2)) * z[32] + z[33];
z[33] = abb[55] + abb[57];
z[34] = (T(-1) / T(4)) * z[33];
z[32] = z[32] * z[34];
z[9] = -z[9] + z[29];
z[9] = m1_set::bc<T>[0] * z[9];
z[9] = abb[42] * (T(1) / T(2)) + -z[9] + z[24] + z[45];
z[24] = z[28] * z[33];
z[9] = -z[9] * z[24];
z[29] = abb[6] * (T(1) / T(16));
z[34] = abb[59] + 3 * z[15];
z[34] = z[29] * z[34] * z[53];
z[45] = -abb[18] + -abb[19];
z[27] = z[27] * z[45];
z[45] = abb[4] * (T(3) / T(32));
z[46] = -abb[44] * z[45];
z[27] = (T(1) / T(16)) * z[27] + z[46];
z[27] = abb[62] * z[27];
z[46] = abb[8] * z[33];
z[25] = z[25] * z[46];
z[48] = abb[0] * (T(1) / T(2));
z[49] = -abb[30] + abb[7] * (T(3) / T(4)) + z[48];
z[49] = abb[62] * z[49];
z[52] = -z[49] * z[62];
z[53] = abb[29] * (T(3) / T(32));
z[53] = z[43] * z[53];
z[62] = -abb[44] * z[53];
z[3] = abb[70] + abb[71] + z[3] + z[4] + z[5] + z[7] + z[9] + z[20] + z[21] + z[23] + (T(1) / T(8)) * z[25] + z[27] + z[32] + z[34] + z[52] + z[62];
z[4] = -abb[35] + (T(1) / T(4)) * z[41];
z[5] = abb[36] * (T(1) / T(4)) + z[4] + -z[10];
z[5] = abb[36] * z[5];
z[7] = abb[34] * (T(1) / T(4));
z[9] = abb[31] * (T(5) / T(2));
z[20] = abb[32] + -z[9];
z[20] = z[7] * z[20];
z[4] = z[1] + -z[4];
z[4] = abb[33] * z[4];
z[21] = abb[68] * (T(1) / T(4));
z[23] = prod_pow(m1_set::bc<T>[0], 2);
z[25] = abb[65] + abb[67];
z[27] = abb[37] + abb[38] + z[25];
z[32] = abb[31] + abb[32] * (T(-5) / T(8));
z[32] = abb[32] * z[32];
z[34] = -abb[32] + abb[31] * (T(5) / T(8));
z[52] = abb[34] * (T(3) / T(8)) + z[34];
z[52] = abb[35] * z[52];
z[4] = z[4] + z[5] + z[20] + z[21] + (T(-3) / T(8)) * z[23] + (T(3) / T(4)) * z[27] + z[32] + z[52];
z[4] = z[4] * z[17];
z[20] = abb[66] * (T(3) / T(4));
z[21] = -z[20] + z[21];
z[27] = -abb[32] + z[12];
z[27] = abb[34] * z[27];
z[27] = -z[21] + z[27];
z[32] = abb[31] * abb[32];
z[52] = 5 * abb[37] + z[32];
z[62] = prod_pow(abb[31], 2);
z[63] = (T(1) / T(2)) * z[62];
z[64] = z[52] + -z[63];
z[65] = 3 * abb[38];
z[66] = 3 * abb[67] + -z[64] + z[65];
z[67] = -abb[32] + abb[34];
z[68] = abb[35] + (T(-1) / T(2)) * z[67];
z[68] = z[13] * z[68];
z[69] = abb[33] * (T(1) / T(8)) + z[42] + z[56];
z[69] = abb[33] * z[69];
z[5] = z[5] + -z[27] + (T(1) / T(4)) * z[66] + z[68] + z[69];
z[5] = z[5] * z[58];
z[44] = z[44] * z[56];
z[56] = z[18] + z[41];
z[56] = abb[36] * z[56];
z[19] = abb[32] * z[19];
z[58] = abb[64] + abb[66];
z[44] = z[19] + -z[25] + z[44] + -z[56] + z[58];
z[56] = abb[17] * z[44];
z[66] = z[13] * z[41];
z[68] = abb[38] + z[66];
z[69] = -abb[64] + z[63];
z[70] = abb[36] * z[41];
z[71] = -z[68] + z[69] + z[70];
z[72] = abb[31] * z[12];
z[73] = z[71] + -z[72];
z[74] = -z[23] + 3 * z[73];
z[75] = abb[15] * z[74];
z[76] = z[12] + z[40];
z[77] = abb[35] * z[76];
z[72] = -z[72] + z[77];
z[32] = abb[37] + z[32];
z[77] = abb[65] + z[32] + -z[63];
z[78] = z[72] + z[77];
z[79] = z[23] + 3 * z[78];
z[80] = abb[16] * z[79];
z[81] = -abb[34] + z[10];
z[81] = abb[33] * z[81];
z[82] = prod_pow(abb[32], 2);
z[83] = abb[37] + (T(1) / T(2)) * z[82];
z[81] = z[81] + -z[83];
z[84] = abb[32] * abb[34];
z[85] = -abb[66] + z[81] + z[84];
z[86] = (T(1) / T(2)) * z[23];
z[85] = 3 * z[85] + -z[86];
z[87] = abb[6] * z[85];
z[56] = z[56] + z[75] + z[80] + z[87];
z[80] = abb[35] + z[41];
z[80] = z[13] * z[80];
z[87] = -abb[31] + z[12];
z[88] = abb[34] * z[87];
z[80] = abb[64] * (T(3) / T(2)) + z[80] + -z[88];
z[65] = -z[62] + z[65];
z[88] = -abb[67] + -z[65];
z[89] = -abb[35] + z[10];
z[90] = z[10] * z[89];
z[91] = 3 * z[41];
z[92] = abb[35] + z[8] + z[91];
z[92] = z[8] * z[92];
z[93] = (T(1) / T(3)) * z[23];
z[88] = -z[80] + (T(1) / T(2)) * z[88] + z[90] + z[92] + -z[93];
z[59] = z[59] * z[88];
z[88] = abb[36] * (T(3) / T(8));
z[90] = abb[35] * (T(1) / T(4));
z[92] = z[41] + z[88] + z[90];
z[92] = abb[36] * z[92];
z[42] = -abb[34] + z[42];
z[94] = z[42] + -z[90];
z[94] = abb[33] * z[94];
z[21] = abb[64] * (T(-3) / T(4)) + z[21] + z[92] + z[94];
z[65] = -z[52] + -z[65];
z[2] = abb[32] + -z[2] + z[35];
z[2] = z[2] * z[90];
z[34] = -abb[34] * z[34];
z[2] = z[2] + z[21] + (T(11) / T(24)) * z[23] + z[34] + (T(1) / T(4)) * z[65];
z[2] = z[2] * z[51];
z[34] = abb[31] * abb[33];
z[34] = abb[68] + z[34] + z[63];
z[51] = abb[33] + z[8];
z[65] = abb[36] * z[51];
z[92] = abb[64] + z[34] + -z[65];
z[92] = abb[9] * z[92];
z[94] = prod_pow(abb[33], 2);
z[95] = -z[82] + z[94];
z[95] = abb[13] * z[95];
z[92] = z[92] + z[95];
z[95] = abb[35] + -z[76];
z[95] = abb[35] * z[95];
z[35] = -abb[34] + z[35];
z[35] = abb[34] * z[35];
z[95] = z[35] + -z[77] + z[95];
z[95] = abb[0] * z[95];
z[96] = abb[72] * (T(1) / T(2));
z[95] = z[92] + z[95] + z[96];
z[76] = -z[76] + z[90];
z[76] = abb[35] * z[76];
z[87] = z[12] * z[87];
z[97] = 3 * abb[65] + -z[62];
z[0] = -abb[31] + z[0];
z[0] = abb[32] * z[0];
z[0] = -abb[37] + z[0] + z[76] + -z[87] + (T(-1) / T(4)) * z[97];
z[0] = abb[2] * z[0];
z[76] = abb[65] + -z[62] + z[82];
z[76] = abb[10] * z[76];
z[0] = z[0] + (T(-1) / T(4)) * z[76];
z[10] = -abb[31] + z[10];
z[10] = abb[33] * z[10];
z[87] = abb[68] + z[25];
z[69] = -z[10] + -z[69] + z[87];
z[97] = abb[66] * (T(1) / T(4));
z[98] = (T(1) / T(4)) * z[69] + z[93] + z[97];
z[99] = abb[8] * z[98];
z[100] = abb[10] * (T(5) / T(6));
z[101] = abb[2] * (T(-1) / T(2)) + abb[0] * (T(1) / T(3)) + -z[100];
z[102] = abb[5] * (T(1) / T(4));
z[103] = abb[9] * (T(1) / T(6));
z[104] = -abb[13] + z[103];
z[105] = -z[101] + -z[102] + z[104];
z[106] = (T(1) / T(4)) * z[23];
z[105] = z[105] * z[106];
z[65] = -abb[67] + -z[65] + (T(3) / T(2)) * z[94];
z[65] = (T(1) / T(4)) * z[65] + -z[93];
z[65] = abb[14] * z[65];
z[94] = -abb[5] * z[18];
z[88] = z[88] * z[94];
z[89] = abb[33] * z[89];
z[89] = -abb[38] + -abb[67] + z[89];
z[107] = abb[5] * z[89];
z[97] = abb[9] * z[97];
z[60] = abb[69] * z[60];
z[2] = -z[0] + z[2] + z[4] + z[5] + (T(-1) / T(8)) * z[56] + z[59] + z[60] + -z[65] + z[88] + (T(1) / T(4)) * z[95] + z[97] + (T(-1) / T(2)) * z[99] + z[105] + (T(3) / T(8)) * z[107];
z[2] = z[2] * z[6];
z[4] = -abb[38] + abb[67] + z[64];
z[5] = z[67] * z[90];
z[6] = -abb[33] + z[8];
z[56] = (T(1) / T(2)) * z[41];
z[59] = z[6] + z[56];
z[60] = z[8] * z[59];
z[42] = abb[33] * (T(5) / T(8)) + z[42];
z[42] = abb[33] * z[42];
z[64] = (T(1) / T(6)) * z[23];
z[4] = (T(1) / T(4)) * z[4] + z[5] + z[27] + -z[42] + -z[60] + z[64];
z[4] = abb[3] * z[4];
z[5] = abb[33] + abb[35];
z[5] = z[5] * z[41];
z[27] = -abb[38] + z[63];
z[42] = z[27] + z[70];
z[60] = abb[34] * z[67];
z[5] = z[5] + -z[42] + z[60] + z[83] + -z[87] + -z[93];
z[5] = z[5] * z[54];
z[60] = abb[31] + abb[32];
z[60] = -abb[34] + -z[13] + (T(1) / T(2)) * z[60];
z[60] = z[13] * z[60];
z[52] = 3 * z[27] + -z[52];
z[40] = abb[34] * z[40];
z[21] = z[21] + -z[40] + (T(1) / T(4)) * z[52] + z[60] + z[93];
z[21] = abb[4] * z[21];
z[52] = abb[9] * abb[66];
z[60] = z[52] + z[92];
z[70] = abb[5] * (T(1) / T(12)) + -z[104];
z[70] = z[23] * z[70];
z[87] = prod_pow(abb[34], 2);
z[88] = prod_pow(abb[35], 2);
z[87] = z[87] + -z[88];
z[88] = abb[0] * z[87];
z[88] = -abb[72] + z[88] + -z[107];
z[90] = z[8] * z[94];
z[70] = -z[60] + z[70] + (T(1) / T(2)) * z[88] + -z[90];
z[28] = z[28] * z[44];
z[88] = abb[36] + z[91];
z[88] = z[8] * z[88];
z[27] = z[27] + -z[80] + z[88] + -z[106];
z[27] = abb[1] * z[27];
z[80] = z[78] + z[93];
z[47] = z[47] * z[80];
z[88] = abb[69] * (T(1) / T(4));
z[38] = z[38] * z[88];
z[4] = -z[4] + -z[5] + z[21] + z[27] + -z[28] + z[38] + -z[47] + (T(-1) / T(2)) * z[70] + (T(-1) / T(4)) * z[75] + -z[99];
z[4] = -z[4] * z[30];
z[5] = abb[32] + abb[31] * (T(-3) / T(2));
z[5] = abb[34] * z[5];
z[21] = abb[33] + -z[41];
z[21] = abb[33] * z[21];
z[5] = abb[68] + z[5] + z[21] + -z[25] + z[62] + z[68] + -z[83];
z[21] = abb[36] * z[59];
z[5] = (T(1) / T(2)) * z[5] + z[21] + -z[106];
z[5] = abb[7] * z[5];
z[21] = -z[22] * z[44];
z[22] = z[73] + -z[93];
z[28] = abb[15] * z[22];
z[30] = abb[16] * z[80];
z[30] = z[28] + -z[30] + z[107];
z[38] = -abb[35] * z[67];
z[10] = abb[66] + abb[67] + abb[68] + -z[10] + -z[32] + z[38] + z[42];
z[10] = (T(1) / T(2)) * z[10] + z[93];
z[10] = abb[3] * z[10];
z[38] = -abb[0] * z[78];
z[6] = z[6] + z[41];
z[6] = abb[36] * z[6];
z[42] = abb[31] * abb[34];
z[6] = z[6] + z[34] + -z[42];
z[6] = abb[9] * z[6];
z[18] = abb[36] * z[18];
z[34] = -z[18] + -z[64] + z[89];
z[34] = abb[1] * z[34];
z[59] = -abb[0] + abb[9] + z[102];
z[59] = z[59] * z[93];
z[5] = z[5] + z[6] + z[10] + z[21] + (T(-1) / T(2)) * z[30] + z[34] + z[38] + z[52] + z[59] + -z[90] + z[96];
z[6] = abb[68] + z[58];
z[10] = abb[31] + -abb[35];
z[10] = abb[33] * z[10];
z[10] = -abb[38] + z[6] + z[10] + -z[18] + (T(5) / T(6)) * z[23] + -z[32] + -z[72];
z[10] = z[10] * z[57];
z[18] = abb[69] * (T(1) / T(8));
z[21] = z[18] * z[61];
z[5] = (T(1) / T(2)) * z[5] + z[10] + z[21] + -z[99];
z[5] = z[5] * z[16];
z[9] = abb[34] + -z[9];
z[7] = z[7] * z[9];
z[9] = abb[65] + (T(-1) / T(4)) * z[62];
z[10] = abb[31] * (T(3) / T(8)) + z[37];
z[10] = abb[35] * z[10];
z[7] = abb[37] + z[7] + (T(1) / T(2)) * z[9] + z[10] + -z[19] + (T(1) / T(24)) * z[23];
z[7] = z[7] * z[17];
z[9] = -z[12] * z[41];
z[10] = -abb[35] * z[14];
z[9] = z[9] + z[10] + -z[77];
z[9] = abb[0] * z[9];
z[10] = z[42] + z[62];
z[12] = abb[35] + z[56];
z[12] = abb[35] * z[12];
z[10] = (T(1) / T(2)) * z[10] + -z[12] + -z[86];
z[12] = -z[10] * z[55];
z[14] = -z[101] * z[106];
z[16] = z[18] * z[50];
z[0] = -z[0] + z[7] + (T(1) / T(4)) * z[9] + z[12] + z[14] + z[16] + -z[47];
z[0] = z[0] * z[31];
z[7] = abb[28] * z[44];
z[9] = abb[66] + -z[81];
z[12] = z[9] + z[40] + -z[71] + z[86];
z[14] = (T(1) / T(4)) * z[12];
z[14] = z[14] * z[26];
z[16] = abb[23] + abb[25];
z[17] = z[16] * z[98];
z[19] = abb[27] * (T(1) / T(4));
z[21] = z[19] * z[80];
z[30] = abb[22] * (T(1) / T(4));
z[22] = z[22] * z[30];
z[31] = abb[29] * z[18];
z[14] = (T(-1) / T(4)) * z[7] + z[14] + z[17] + z[21] + -z[22] + z[31];
z[17] = -abb[63] + (T(-1) / T(4)) * z[39];
z[14] = z[14] * z[17];
z[12] = (T(3) / T(4)) * z[12];
z[12] = z[12] * z[26];
z[17] = z[20] + z[23] + (T(3) / T(4)) * z[69];
z[16] = z[16] * z[17];
z[17] = z[30] * z[74];
z[19] = z[19] * z[79];
z[7] = (T(-3) / T(4)) * z[7] + z[12] + z[16] + -z[17] + z[19];
z[12] = (T(1) / T(4)) * z[43];
z[7] = z[7] * z[12];
z[12] = z[48] * z[87];
z[16] = abb[36] + z[41];
z[16] = abb[36] * z[16];
z[16] = -abb[64] + z[16] + -z[42];
z[16] = abb[9] * z[16];
z[17] = z[23] * z[103];
z[12] = z[12] + z[16] + z[17] + z[28];
z[16] = z[35] + z[63] + -z[66] + -z[86];
z[17] = -z[16] * z[54];
z[19] = abb[35] + (T(3) / T(2)) * z[41];
z[13] = z[13] * z[19];
z[13] = abb[38] + z[13] + (T(5) / T(4)) * z[42] + (T(-3) / T(4)) * z[62];
z[19] = -z[8] + -z[41];
z[19] = abb[36] * z[19];
z[13] = abb[64] + (T(1) / T(2)) * z[13] + z[19] + (T(5) / T(24)) * z[23];
z[13] = abb[4] * z[13];
z[19] = z[36] * z[88];
z[12] = (T(1) / T(2)) * z[12] + z[13] + z[17] + z[19] + -z[27];
z[11] = z[11] * z[12];
z[12] = -z[25] + z[82];
z[8] = z[8] * z[51];
z[1] = abb[31] + z[1];
z[1] = abb[33] * z[1];
z[1] = -z[1] + -z[6] + z[8] + (T(1) / T(2)) * z[12] + -z[106];
z[1] = -z[1] * z[24];
z[6] = z[100] + -z[104];
z[6] = z[6] * z[23];
z[6] = z[6] + -z[60] + z[76];
z[6] = (T(1) / T(4)) * z[6] + -z[65];
z[8] = (T(1) / T(2)) * z[33];
z[6] = z[6] * z[8];
z[8] = -abb[18] * z[16];
z[10] = -abb[19] * z[10];
z[8] = z[8] + z[10];
z[10] = abb[69] * z[45];
z[8] = (T(1) / T(16)) * z[8] + z[10];
z[8] = abb[62] * z[8];
z[10] = -z[46] * z[98];
z[9] = -z[9] + -z[64] + z[84];
z[9] = abb[59] * z[9];
z[12] = z[15] * z[85];
z[9] = z[9] + z[12];
z[9] = z[9] * z[29];
z[12] = z[18] * z[49];
z[13] = abb[69] * z[53];
z[0] = abb[45] + abb[46] + z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + (T(1) / T(2)) * z[10] + z[11] + z[12] + z[13] + z[14];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_674_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("1.5448520906573185141448491015257995970493512535861746399983406047"),stof<T>("-0.5042344713615779054349628597829694003056516098058798406511803345")}, std::complex<T>{stof<T>("1.5448520906573185141448491015257995970493512535861746399983406047"),stof<T>("-0.5042344713615779054349628597829694003056516098058798406511803345")}, std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("1.5448520906573185141448491015257995970493512535861746399983406047"),stof<T>("-0.5042344713615779054349628597829694003056516098058798406511803345")}, std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("1.5448520906573185141448491015257995970493512535861746399983406047"),stof<T>("-0.5042344713615779054349628597829694003056516098058798406511803345")}, std::complex<T>{stof<T>("0.44410774041229782909439520195213469331222723106204178207726584943"),stof<T>("-0.09800519800126543441247641450918679399013735668201664707666569455")}, std::complex<T>{stof<T>("2.1823323326205370932780678467339358021820881991477251431439466897"),stof<T>("-1.8377858365774348087982358482750733510053317274156970037273071422")}, std::complex<T>{stof<T>("1.5725159148782278174320363457364222347262678526407383566519969061"),stof<T>("-0.0738938508699666726752606071517459998878812264342695868238997354")}, std::complex<T>{stof<T>("2.1823323326205370932780678467339358021820881991477251431439466897"),stof<T>("-1.8377858365774348087982358482750733510053317274156970037273071422")}, std::complex<T>{stof<T>("1.5725159148782278174320363457364222347262678526407383566519969061"),stof<T>("-0.0738938508699666726752606071517459998878812264342695868238997354")}, std::complex<T>{stof<T>("0.0417966127428765050314821214938676836467955745016790176314509502"),stof<T>("0.27148044900177878644403449703850351511026247680080638748131402086")}, std::complex<T>{stof<T>("0.53243756452186122178903707120331913415343410525939776939193670792"),stof<T>("0.34209081986619431596925285610376664019794131280571792569097552237")}, std::complex<T>{stof<T>("0.15258225933979579846437091687265167261413254825078213605058390699"),stof<T>("0.56468626539569294860296510654471602956976011454291979071394015152")}, std::complex<T>{stof<T>("0.23594018086022457074806837570668553834164687884362288183008744216"),stof<T>("-0.38424403207403930356915199686015859639968219883999128304530377912")}, std::complex<T>{stof<T>("-2.0598027875430913521931321353677327960658016714482328533311208062"),stof<T>("0.672312628482103873913283813043959200407535479741173120868240446")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}, rlog(k.W[197].imag()/kbase.W[197].imag())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_674_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_674_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_674_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 + -v[0] + -3 * v[1] + -v[2] + 3 * v[3] + 4 * v[4] + 4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + v[5])) / prod_pow(tend, 2);
c[1] = ((1 + 2 * m1_set::bc<T>[1]) * (T(-1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[54] + abb[56] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[35], 2);
z[1] = prod_pow(abb[36], 2);
z[0] = -abb[38] + z[0] + -z[1];
z[1] = -abb[54] + -abb[56] + -abb[58];
return abb[11] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_674_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(64)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[56] + abb[58] + abb[60]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[56] + -abb[58] + -abb[60];
z[1] = abb[32] + -abb[34];
return abb[12] * m1_set::bc<T>[0] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_674_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[2] + v[3]) * (24 + 8 * v[0] + 4 * v[1] + -5 * v[2] + -9 * v[3] + -4 * v[4] + 6 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 12 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 3 * m1_set::bc<T>[1]) * (T(-1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[56] + abb[58] + abb[60]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[31] + -abb[35];
z[1] = -abb[32] + z[0];
z[1] = abb[32] * z[1];
z[0] = -abb[34] + z[0];
z[0] = abb[34] * z[0];
z[0] = abb[37] + -z[0] + z[1];
z[1] = -abb[56] + -abb[58] + -abb[60];
return abb[12] * (T(1) / T(8)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_674_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-5.0095144980148264055064091488905714466985118607996094265564958349"),stof<T>("0.0711481659034544450022470733098129273234118670481749176251359464")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,73> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), rlog(kend.W[197].imag()/k.W[197].imag()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[45] = SpDLog_f_4_674_W_17_Im(t, path, abb);
abb[46] = SpDLog_f_4_674_W_19_Im(t, path, abb);
abb[47] = SpDLogQ_W_82(k,dl,dlr).imag();
abb[70] = SpDLog_f_4_674_W_17_Re(t, path, abb);
abb[71] = SpDLog_f_4_674_W_19_Re(t, path, abb);
abb[72] = SpDLogQ_W_82(k,dl,dlr).real();

                    
            return f_4_674_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_674_DLogXconstant_part(base_point<T>, kend);
	value += f_4_674_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_674_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_674_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_674_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_674_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_674_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_674_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
