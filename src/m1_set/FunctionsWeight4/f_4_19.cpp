/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_19.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_19_abbreviated (const std::array<T,15>& abb) {
T z[17];
z[0] = abb[11] + abb[12];
z[1] = abb[10] + (T(1) / T(2)) * z[0];
z[2] = abb[2] * z[1];
z[3] = abb[0] * z[1];
z[4] = -abb[10] + abb[12];
z[4] = abb[1] * z[4];
z[5] = abb[13] + abb[14];
z[6] = abb[3] * z[5];
z[7] = -z[2] + z[3] + -2 * z[4] + (T(-3) / T(2)) * z[6];
z[7] = abb[7] * z[7];
z[8] = abb[4] * z[5];
z[9] = z[6] + z[8];
z[10] = abb[1] * z[1];
z[11] = (T(3) / T(2)) * z[9] + z[10];
z[12] = abb[6] * z[11];
z[13] = abb[2] * abb[6];
z[14] = z[1] * z[13];
z[12] = z[12] + z[14];
z[14] = abb[0] * abb[6];
z[15] = z[1] * z[14];
z[12] = (T(1) / T(2)) * z[12] + z[15];
z[7] = z[7] + z[12];
z[7] = abb[7] * z[7];
z[15] = -abb[10] + abb[11];
z[15] = abb[2] * z[15];
z[10] = z[3] + (T(-3) / T(2)) * z[8] + -z[10] + -2 * z[15];
z[10] = abb[8] * z[10];
z[2] = -z[2] + -z[11];
z[2] = (T(1) / T(2)) * z[2] + -z[3];
z[2] = abb[7] * z[2];
z[2] = z[2] + z[10] + z[12];
z[2] = abb[8] * z[2];
z[10] = prod_pow(abb[6], 2);
z[11] = z[10] * z[11];
z[10] = z[1] * z[10];
z[12] = abb[9] * z[5];
z[12] = z[10] + (T(-9) / T(2)) * z[12];
z[12] = abb[2] * z[12];
z[11] = z[11] + z[12];
z[12] = -abb[3] + -abb[4];
z[1] = z[1] * z[12];
z[12] = (T(3) / T(2)) * z[5];
z[16] = -abb[1] * z[12];
z[1] = z[1] + z[16];
z[5] = abb[5] * z[5];
z[1] = (T(1) / T(2)) * z[1] + z[5];
z[1] = abb[9] * z[1];
z[5] = -abb[9] * z[12];
z[5] = z[5] + z[10];
z[5] = abb[0] * z[5];
z[10] = z[4] + z[15];
z[3] = -z[3] + (T(1) / T(2)) * z[10];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[1] = 3 * z[1] + z[2] + (T(13) / T(3)) * z[3] + z[5] + z[7] + (T(1) / T(2)) * z[11];
z[2] = -abb[0] + abb[2];
z[0] = 2 * abb[10] + z[0];
z[2] = z[0] * z[2];
z[2] = z[2] + 4 * z[4] + 3 * z[6];
z[2] = abb[7] * z[2];
z[3] = -abb[0] + abb[1];
z[3] = z[0] * z[3];
z[3] = z[3] + 3 * z[8] + 4 * z[15];
z[3] = abb[8] * z[3];
z[4] = -z[13] + -2 * z[14];
z[4] = z[0] * z[4];
z[0] = -abb[1] * z[0];
z[0] = z[0] + -3 * z[9];
z[0] = abb[6] * z[0];
z[0] = z[0] + z[2] + z[3] + z[4];
z[0] = m1_set::bc<T>[0] * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_19_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-41.932887781076756095465304860068490356652329399054794914049634135"),stof<T>("85.018556237964006341815256903219605645311579239202662511530159808")}, std::complex<T>{stof<T>("28.560899908283595051706404099497554936284991946875712445100304191"),stof<T>("28.02908466588477977008962680246466706612581583380564964565574199")}, std::complex<T>{stof<T>("6.4618330470866752146026316359407830922760120337519691486382626601"),stof<T>("7.2561621914044519162379598560367558429117706884607938501240154086")}, std::complex<T>{stof<T>("1.536334183159273299578814019387516392977720877904178210873066067"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("1.536334183159273299578814019387516392977720877904178210873066067"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_19_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_19_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("113.244016378557355626788711421243431165679360822879358914982621684"),stof<T>("21.708891635607554008257639593158050844293964128386178865518661208")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,15> abb = {dl[0], dl[2], dl[4], dlog_W118(k,dv), dlog_W123(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real())};

                    
            return f_4_19_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_19_DLogXconstant_part(base_point<T>, kend);
	value += f_4_19_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_19_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_19_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_19_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_19_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_19_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_19_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
