/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_838.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_838_abbreviated (const std::array<T,102>& abb) {
T z[160];
z[0] = 2 * abb[40];
z[1] = m1_set::bc<T>[0] * z[0];
z[2] = 2 * abb[54];
z[3] = z[1] + z[2];
z[4] = abb[42] * m1_set::bc<T>[0];
z[5] = abb[43] * m1_set::bc<T>[0];
z[6] = z[4] + z[5];
z[7] = z[3] + -z[6];
z[8] = abb[39] + -abb[41];
z[9] = m1_set::bc<T>[0] * z[8];
z[10] = -z[7] + z[9];
z[11] = 4 * abb[1];
z[10] = z[10] * z[11];
z[11] = 2 * abb[38];
z[12] = 2 * abb[45];
z[13] = z[11] + -z[12];
z[14] = -z[8] + z[13];
z[14] = m1_set::bc<T>[0] * z[14];
z[15] = 2 * abb[52];
z[16] = z[14] + z[15];
z[17] = z[4] + -z[5];
z[18] = z[16] + -z[17];
z[19] = 4 * abb[0];
z[18] = z[18] * z[19];
z[19] = -abb[41] + abb[44];
z[20] = m1_set::bc<T>[0] * z[19];
z[21] = abb[52] + -abb[55];
z[22] = z[20] + -z[21];
z[22] = abb[17] * z[22];
z[23] = abb[66] + abb[67];
z[10] = z[10] + z[18] + 8 * z[22] + z[23];
z[18] = abb[41] * m1_set::bc<T>[0];
z[18] = -abb[55] + z[18];
z[24] = -abb[56] + z[4];
z[25] = 2 * z[5];
z[26] = z[3] + z[18] + -z[24] + -z[25];
z[27] = 2 * abb[7];
z[26] = z[26] * z[27];
z[28] = abb[39] + abb[44];
z[29] = m1_set::bc<T>[0] * z[28];
z[30] = abb[40] * m1_set::bc<T>[0];
z[29] = abb[53] + z[24] + -z[29] + z[30];
z[31] = 4 * abb[10];
z[29] = z[29] * z[31];
z[31] = abb[44] * m1_set::bc<T>[0];
z[31] = abb[56] + z[31];
z[31] = 2 * z[31];
z[32] = -z[6] + z[31];
z[32] = abb[14] * z[32];
z[29] = z[29] + z[32];
z[32] = 2 * abb[44];
z[33] = -abb[41] + z[32];
z[34] = -abb[39] + z[33];
z[35] = abb[38] + z[34];
z[36] = m1_set::bc<T>[0] * z[35];
z[36] = abb[56] + -z[21] + z[36];
z[6] = -z[6] + 2 * z[36];
z[6] = abb[3] * z[6];
z[36] = abb[39] + -abb[44];
z[37] = -z[13] + z[36];
z[38] = m1_set::bc<T>[0] * z[37];
z[39] = abb[54] + z[30];
z[40] = -z[4] + z[39];
z[38] = -abb[55] + z[38] + -z[40];
z[41] = 4 * abb[9];
z[42] = -z[38] * z[41];
z[43] = 3 * z[5];
z[44] = 4 * abb[54] + -z[43];
z[30] = 4 * z[30];
z[45] = z[30] + z[44];
z[46] = abb[38] + -abb[45];
z[47] = m1_set::bc<T>[0] * z[46];
z[47] = abb[52] + z[47];
z[48] = 2 * z[47];
z[49] = z[4] + -z[45] + z[48];
z[49] = abb[13] * z[49];
z[50] = 3 * abb[41];
z[51] = abb[38] + z[28] + -z[50];
z[51] = m1_set::bc<T>[0] * z[51];
z[51] = -z[7] + -z[21] + z[51];
z[52] = 2 * abb[5];
z[51] = z[51] * z[52];
z[53] = -abb[45] + z[11];
z[54] = z[36] + -z[53];
z[54] = m1_set::bc<T>[0] * z[54];
z[54] = -abb[56] + z[54];
z[55] = 3 * z[4];
z[45] = -z[45] + 2 * z[54] + z[55];
z[45] = abb[6] * z[45];
z[54] = -abb[38] + z[19];
z[56] = abb[39] + z[54];
z[56] = m1_set::bc<T>[0] * z[56];
z[56] = -z[21] + z[56];
z[56] = abb[11] * z[56];
z[57] = -z[19] + z[46];
z[58] = m1_set::bc<T>[0] * z[57];
z[58] = z[21] + z[58];
z[59] = 2 * abb[15];
z[60] = z[58] * z[59];
z[61] = abb[22] * abb[58];
z[62] = abb[65] + z[61];
z[63] = abb[19] + abb[26];
z[64] = abb[23] + z[63];
z[65] = -abb[58] * z[64];
z[6] = z[6] + -z[10] + z[26] + -z[29] + z[42] + z[45] + z[49] + z[51] + 4 * z[56] + -z[60] + -z[62] + z[65];
z[6] = abb[79] * z[6];
z[26] = z[43] + z[55];
z[42] = z[11] + z[34];
z[42] = m1_set::bc<T>[0] * z[42];
z[43] = abb[55] + abb[56];
z[42] = z[42] + z[43];
z[3] = z[3] + -z[26] + 2 * z[42];
z[3] = abb[3] * z[3];
z[42] = z[8] + z[32];
z[45] = -m1_set::bc<T>[0] * z[42];
z[45] = abb[53] + abb[54] + z[1] + z[17] + -z[43] + z[45];
z[45] = z[27] * z[45];
z[49] = abb[13] * z[7];
z[51] = 2 * abb[53];
z[55] = z[1] + z[51];
z[65] = abb[39] + abb[41];
z[66] = -m1_set::bc<T>[0] * z[65];
z[66] = z[55] + z[66];
z[66] = abb[12] * z[66];
z[39] = -z[5] + z[39];
z[20] = -abb[55] + -z[20] + z[39];
z[67] = 4 * z[20];
z[68] = -abb[9] * z[67];
z[69] = -z[9] + z[17];
z[70] = 2 * abb[0];
z[69] = z[69] * z[70];
z[71] = 2 * abb[55];
z[72] = -m1_set::bc<T>[0] * z[34];
z[72] = -z[71] + z[72];
z[72] = abb[15] * z[72];
z[63] = -abb[23] + z[63];
z[73] = -abb[58] * z[63];
z[3] = z[3] + -z[23] + -z[29] + z[45] + z[49] + z[62] + z[66] + z[68] + z[69] + z[72] + z[73];
z[3] = abb[82] * z[3];
z[23] = abb[69] * m1_set::bc<T>[0];
z[29] = abb[73] * m1_set::bc<T>[0];
z[45] = abb[70] + -abb[74];
z[49] = m1_set::bc<T>[0] * z[45];
z[62] = z[23] + -2 * z[29] + -z[49];
z[62] = z[0] * z[62];
z[66] = -z[2] + z[25];
z[68] = z[45] * z[66];
z[69] = abb[55] * z[45];
z[72] = abb[56] * z[45];
z[73] = z[69] + -z[72];
z[74] = abb[39] * z[45];
z[75] = -abb[41] * z[45];
z[76] = -z[74] + z[75];
z[77] = m1_set::bc<T>[0] * z[76];
z[78] = m1_set::bc<T>[0] * z[33];
z[43] = z[43] + z[78];
z[78] = 2 * abb[71];
z[79] = z[43] * z[78];
z[9] = -z[9] + -z[66];
z[9] = abb[69] * z[9];
z[18] = abb[56] + z[18] + -z[66];
z[80] = 2 * abb[73];
z[81] = -z[18] * z[80];
z[82] = z[29] + z[49];
z[83] = abb[71] * m1_set::bc<T>[0];
z[84] = z[82] + -z[83];
z[85] = 2 * abb[42];
z[86] = z[84] * z[85];
z[87] = 8 * abb[72];
z[88] = -z[20] * z[87];
z[89] = abb[75] * z[67];
z[9] = z[9] + z[62] + z[68] + 2 * z[73] + z[77] + z[79] + z[81] + z[86] + z[88] + z[89];
z[9] = abb[28] * z[9];
z[68] = abb[42] * z[84];
z[73] = z[5] * z[45];
z[68] = z[68] + z[73];
z[77] = abb[61] + abb[62];
z[79] = prod_pow(m1_set::bc<T>[0], 2);
z[81] = 2 * z[79];
z[77] = z[77] * z[81];
z[84] = 4 * abb[72] + -2 * abb[75];
z[20] = -z[20] * z[84];
z[86] = abb[53] + abb[55];
z[88] = -z[45] * z[86];
z[89] = -abb[59] + abb[60];
z[89] = m1_set::bc<T>[0] * z[89];
z[89] = z[76] + 2 * z[89];
z[89] = m1_set::bc<T>[0] * z[89];
z[43] = abb[71] * z[43];
z[90] = -abb[54] + z[5];
z[91] = -abb[39] * m1_set::bc<T>[0];
z[91] = abb[53] + -z[90] + z[91];
z[91] = abb[69] * z[91];
z[18] = -abb[73] * z[18];
z[29] = z[23] + -z[29];
z[29] = z[0] * z[29];
z[92] = -abb[54] * z[45];
z[18] = z[18] + z[20] + z[29] + z[43] + z[68] + -z[72] + z[77] + -z[88] + z[89] + z[91] + z[92];
z[18] = abb[30] * z[18];
z[20] = -z[32] + z[50];
z[29] = z[20] * z[45];
z[43] = abb[38] * z[45];
z[29] = -z[29] + 4 * z[43] + -3 * z[74];
z[29] = m1_set::bc<T>[0] * z[29];
z[77] = -z[12] + z[65];
z[77] = m1_set::bc<T>[0] * z[77];
z[51] = z[25] + -z[51] + z[77];
z[51] = abb[69] * z[51];
z[77] = z[13] + z[34];
z[77] = m1_set::bc<T>[0] * z[77];
z[77] = -z[17] + z[71] + z[77];
z[89] = z[77] * z[84];
z[91] = z[11] + z[19];
z[92] = -abb[39] + z[91];
z[93] = -abb[45] + z[92];
z[93] = m1_set::bc<T>[0] * z[93];
z[93] = abb[55] + z[93];
z[93] = z[5] + 2 * z[93];
z[93] = abb[73] * z[93];
z[94] = -abb[44] + abb[45];
z[95] = m1_set::bc<T>[0] * z[94];
z[95] = -abb[55] + z[95];
z[95] = -z[5] + 2 * z[95];
z[95] = abb[71] * z[95];
z[23] = z[23] + z[49];
z[23] = z[0] * z[23];
z[49] = z[82] + z[83];
z[49] = abb[42] * z[49];
z[23] = -z[23] + -z[29] + z[49] + z[51] + z[73] + 2 * z[88] + -z[89] + -z[93] + z[95];
z[29] = abb[29] + abb[31];
z[23] = -z[23] * z[29];
z[39] = -z[39] + z[47];
z[49] = -4 * abb[75] + z[87];
z[51] = z[39] * z[49];
z[73] = -z[2] * z[45];
z[82] = z[5] + z[48];
z[82] = abb[71] * z[82];
z[83] = -z[47] + -z[90];
z[87] = 2 * abb[69];
z[83] = z[83] * z[87];
z[44] = z[44] + -z[48];
z[44] = -abb[73] * z[44];
z[44] = z[44] + z[51] + z[62] + z[68] + z[73] + z[82] + z[83];
z[44] = abb[27] * z[44];
z[39] = abb[13] * z[39];
z[51] = abb[15] * z[58];
z[39] = z[39] + -z[51];
z[51] = -abb[39] + z[20];
z[51] = m1_set::bc<T>[0] * z[51];
z[51] = z[7] + 2 * z[21] + z[51];
z[62] = 2 * abb[3];
z[73] = -z[51] * z[62];
z[14] = z[7] + z[14];
z[41] = z[14] * z[41];
z[67] = abb[7] * z[67];
z[10] = -3 * abb[65] + -z[10] + 4 * z[39] + z[41] + z[67] + z[73];
z[10] = abb[83] * z[10];
z[39] = 2 * abb[9];
z[38] = z[38] * z[39];
z[7] = abb[3] * z[7];
z[7] = z[7] + z[61];
z[22] = 2 * z[22] + -z[56];
z[38] = z[7] + 2 * z[22] + z[38];
z[4] = 2 * z[4];
z[41] = -z[4] + 3 * z[16] + z[25];
z[41] = abb[0] * z[41];
z[56] = 2 * abb[41];
z[61] = -abb[44] + z[56];
z[67] = -abb[38] + z[61];
z[67] = m1_set::bc<T>[0] * z[67];
z[67] = z[21] + z[67];
z[52] = z[52] * z[67];
z[38] = 2 * z[38] + z[41] + z[52] + z[60];
z[41] = abb[21] * abb[57];
z[41] = z[38] + z[41];
z[52] = abb[77] + abb[80];
z[41] = -z[41] * z[52];
z[73] = 3 * abb[39];
z[82] = 4 * abb[38];
z[83] = z[73] + -z[82];
z[88] = z[20] + z[83];
z[88] = m1_set::bc<T>[0] * z[88];
z[66] = z[4] + -z[30] + z[66] + -2 * z[86] + z[88];
z[66] = abb[82] * z[66];
z[51] = -abb[83] * z[51];
z[88] = abb[81] * z[67];
z[51] = z[51] + z[88];
z[51] = 2 * z[51] + z[66];
z[51] = abb[5] * z[51];
z[38] = -abb[78] * z[38];
z[66] = 4 * abb[44] + -z[50] + -z[83];
z[66] = m1_set::bc<T>[0] * z[66];
z[86] = abb[56] + z[86];
z[1] = z[1] + -z[4] + -z[25] + z[66] + 2 * z[86];
z[1] = abb[82] * z[1];
z[4] = abb[83] * z[77];
z[25] = -abb[41] + z[53] + -z[73];
z[25] = m1_set::bc<T>[0] * z[25];
z[24] = abb[55] + z[24] + z[25] + z[55];
z[24] = abb[79] * z[24];
z[25] = -abb[96] * z[81];
z[4] = z[4] + z[24] + z[25];
z[24] = z[12] + -z[33] + z[83];
z[24] = m1_set::bc<T>[0] * z[24];
z[24] = z[24] + -z[71];
z[25] = abb[78] + z[52];
z[52] = -abb[81] + z[25];
z[53] = -z[24] * z[52];
z[55] = -abb[57] * abb[84];
z[1] = z[1] + 2 * z[4] + z[53] + z[55];
z[1] = abb[4] * z[1];
z[4] = abb[16] + abb[18];
z[53] = abb[96] * z[4];
z[55] = -abb[59] + -abb[60];
z[55] = abb[34] * z[55];
z[53] = z[53] + z[55];
z[53] = z[53] * z[79];
z[55] = abb[35] * z[79];
z[66] = abb[59] + 3 * abb[60];
z[66] = z[55] * z[66];
z[71] = abb[34] * z[79];
z[73] = z[55] + z[71];
z[77] = abb[61] * z[73];
z[53] = z[53] + z[66] + z[77];
z[5] = -z[5] + z[31];
z[31] = -abb[71] + abb[73];
z[5] = -z[5] * z[31];
z[66] = -abb[44] * z[45];
z[77] = m1_set::bc<T>[0] * z[66];
z[72] = -z[72] + z[77];
z[5] = z[5] + z[68] + 2 * z[72];
z[5] = abb[33] * z[5];
z[68] = z[36] + -z[46];
z[68] = m1_set::bc<T>[0] * z[68];
z[21] = z[21] + z[68];
z[21] = abb[9] * z[21];
z[21] = z[21] + z[22];
z[22] = -3 * z[8] + z[13];
z[22] = m1_set::bc<T>[0] * z[22];
z[15] = z[15] + z[22];
z[15] = abb[0] * z[15];
z[15] = z[15] + 4 * z[21] + z[60];
z[15] = abb[81] * z[15];
z[21] = z[49] + z[78] + z[80];
z[21] = z[21] * z[58];
z[22] = -z[33] * z[45];
z[49] = z[22] + z[74];
z[49] = m1_set::bc<T>[0] * z[49];
z[16] = -abb[69] * z[16];
z[16] = z[16] + z[21] + z[49] + -2 * z[69];
z[16] = abb[32] * z[16];
z[21] = -abb[21] * z[24];
z[24] = abb[20] * z[67];
z[49] = 2 * abb[25];
z[58] = -z[49] * z[58];
z[60] = abb[0] * abb[57];
z[21] = z[21] + -2 * z[24] + z[58] + z[60];
z[21] = abb[84] * z[21];
z[24] = -m1_set::bc<T>[0] * z[92];
z[24] = -abb[53] + -abb[56] + z[24];
z[2] = -z[2] + 2 * z[24] + z[26] + -z[30];
z[2] = abb[82] * z[2];
z[14] = abb[83] * z[14];
z[2] = z[2] + -2 * z[14];
z[2] = abb[6] * z[2];
z[14] = z[40] + z[47];
z[14] = z[14] * z[39];
z[17] = z[17] + -z[48];
z[17] = abb[0] * z[17];
z[7] = -z[7] + z[14] + z[17];
z[14] = 2 * abb[76];
z[7] = z[7] * z[14];
z[17] = abb[69] + z[45];
z[24] = abb[37] * z[17];
z[26] = abb[78] + -abb[81];
z[26] = -abb[21] * z[26];
z[30] = -abb[20] + abb[21] + abb[24] + -abb[25];
z[40] = abb[82] * z[30];
z[26] = -z[24] + z[26] + z[40];
z[26] = abb[57] * z[26];
z[40] = abb[9] * z[79];
z[40] = 8 * z[40];
z[47] = 4 * z[79];
z[48] = -abb[12] * z[47];
z[48] = -z[40] + z[48];
z[48] = abb[96] * z[48];
z[58] = 4 * z[73];
z[60] = abb[62] * z[58];
z[67] = 2 * abb[72];
z[68] = -abb[75] + z[67];
z[69] = abb[73] + z[45];
z[72] = z[68] + z[69];
z[72] = abb[101] * z[72];
z[73] = -abb[71] + z[45];
z[77] = -abb[73] + -z[73];
z[77] = abb[36] * abb[58] * z[77];
z[83] = abb[79] + abb[82] + -abb[83];
z[83] = abb[64] * z[83];
z[1] = z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + z[9] + z[10] + z[15] + z[16] + 2 * z[18] + z[21] + z[23] + z[26] + z[38] + z[41] + z[44] + z[48] + z[51] + 4 * z[53] + z[60] + z[72] + z[77] + z[83];
z[2] = z[8] + z[12];
z[2] = abb[38] * z[2];
z[3] = -abb[39] + z[46];
z[5] = abb[43] + 2 * z[3];
z[5] = abb[43] * z[5];
z[6] = abb[49] + abb[50];
z[7] = -abb[47] + -abb[48] + z[6];
z[9] = abb[39] * abb[41];
z[10] = 2 * abb[39];
z[15] = abb[45] + z[10];
z[15] = abb[45] * z[15];
z[2] = z[2] + -z[5] + -2 * z[7] + z[9] + -z[15] + -z[79];
z[2] = abb[69] * z[2];
z[5] = -abb[43] + z[8];
z[7] = abb[73] * z[5];
z[16] = abb[69] * z[10];
z[18] = abb[40] * z[17];
z[21] = abb[43] * z[45];
z[7] = -z[7] + z[16] + -z[18] + z[21] + -z[76];
z[7] = abb[40] * z[7];
z[16] = abb[44] * z[61];
z[18] = 2 * abb[88];
z[23] = prod_pow(abb[41], 2);
z[26] = z[16] + z[18] + -z[23];
z[38] = -abb[41] + abb[45];
z[41] = z[11] * z[38];
z[44] = abb[38] * abb[43];
z[48] = (T(2) / T(3)) * z[79];
z[51] = 2 * abb[49] + -z[44] + z[48];
z[53] = -abb[45] + z[56];
z[53] = abb[45] * z[53];
z[60] = 2 * abb[48];
z[41] = z[26] + z[41] + z[51] + z[53] + -z[60];
z[41] = abb[71] * z[41];
z[72] = abb[40] + -abb[43];
z[77] = -z[19] + z[72];
z[83] = -z[13] + z[77];
z[83] = abb[42] * z[83];
z[37] = -abb[43] + z[37];
z[37] = abb[43] * z[37];
z[5] = abb[40] * z[5];
z[37] = -z[5] + z[37] + -z[83];
z[20] = abb[44] * z[20];
z[83] = 4 * abb[88];
z[86] = abb[39] * z[19];
z[20] = z[20] + -z[23] + z[37] + z[83] + z[86];
z[88] = abb[45] + z[8];
z[89] = z[12] * z[88];
z[90] = -abb[38] + z[12];
z[92] = z[8] + z[90];
z[93] = z[11] * z[92];
z[95] = 2 * abb[50];
z[89] = z[89] + -z[93] + z[95];
z[93] = (T(10) / T(3)) * z[79];
z[96] = z[89] + -z[93];
z[97] = z[20] + -z[96];
z[97] = z[67] * z[97];
z[98] = 2 * abb[51];
z[96] = z[96] + z[98];
z[20] = z[20] + -z[96];
z[99] = abb[75] * z[20];
z[100] = -abb[43] + z[90];
z[101] = abb[71] * z[100];
z[101] = -z[43] + z[101];
z[102] = -z[19] * z[45];
z[103] = abb[38] + z[19];
z[104] = abb[73] * z[103];
z[105] = abb[40] * z[69];
z[104] = z[101] + z[102] + -z[104] + z[105];
z[104] = abb[42] * z[104];
z[3] = z[3] * z[11];
z[106] = 2 * z[6];
z[15] = z[15] + z[106];
z[107] = 2 * abb[46];
z[108] = z[60] + -z[107];
z[3] = z[3] + z[15] + -z[108];
z[109] = z[36] + z[100];
z[109] = abb[43] * z[109];
z[110] = abb[44] * z[19];
z[111] = -z[18] + z[110];
z[112] = -z[86] + z[111];
z[113] = (T(8) / T(3)) * z[79];
z[114] = -z[3] + z[109] + -z[112] + z[113];
z[114] = abb[73] * z[114];
z[115] = 2 * abb[47];
z[116] = (T(11) / T(3)) * z[79] + -z[110] + -z[115];
z[116] = z[45] * z[116];
z[117] = z[11] * z[45];
z[76] = z[76] + z[117];
z[76] = abb[38] * z[76];
z[117] = -z[43] + z[74];
z[118] = -z[66] + z[117];
z[118] = abb[43] * z[118];
z[67] = abb[71] + z[67];
z[67] = z[67] * z[98];
z[119] = z[61] * z[74];
z[120] = z[18] * z[45];
z[121] = 2 * abb[86];
z[17] = z[17] * z[121];
z[122] = z[73] * z[107];
z[2] = -z[2] + z[7] + z[17] + z[41] + -z[67] + -z[76] + z[97] + -z[99] + -z[104] + z[114] + z[116] + -z[118] + -z[119] + z[120] + -z[122];
z[2] = -z[2] * z[29];
z[7] = prod_pow(abb[45], 2);
z[29] = z[7] + z[106];
z[41] = z[11] * z[19];
z[54] = abb[40] + z[54];
z[54] = abb[42] * z[54];
z[67] = 2 * abb[89];
z[76] = 2 * abb[85];
z[5] = -z[5] + -z[29] + z[41] + -z[54] + -z[67] + z[76] + -3 * z[79] + z[109] + z[112];
z[5] = abb[3] * z[5];
z[41] = abb[39] + abb[43];
z[97] = z[41] + -z[61];
z[97] = abb[43] * z[97];
z[99] = 3 * abb[43] + -z[0];
z[104] = z[8] + z[99];
z[104] = abb[40] * z[104];
z[106] = 4 * abb[87];
z[104] = z[104] + z[106];
z[109] = z[19] + z[72];
z[109] = abb[42] * z[109];
z[97] = z[86] + -z[97] + z[104] + z[109];
z[56] = -abb[38] + z[56];
z[56] = abb[38] * z[56];
z[53] = -z[53] + z[56] + z[108];
z[56] = -z[48] + z[76];
z[108] = -abb[50] + z[23];
z[114] = abb[44] + -z[50];
z[114] = abb[44] * z[114];
z[108] = -z[18] + -z[53] + z[56] + z[97] + 2 * z[108] + z[114];
z[108] = abb[5] * z[108];
z[114] = 2 * abb[43];
z[116] = -abb[40] + z[114];
z[103] = z[103] + -z[116];
z[103] = abb[42] * z[103];
z[118] = (T(13) / T(3)) * z[79];
z[103] = z[103] + z[118];
z[119] = z[67] + -z[110];
z[123] = -z[86] + z[119];
z[124] = -z[36] + z[100];
z[124] = abb[43] * z[124];
z[3] = -z[3] + z[103] + z[104] + z[123] + z[124];
z[3] = abb[6] * z[3];
z[124] = (T(4) / T(3)) * z[79];
z[125] = abb[41] * abb[44];
z[126] = z[124] + -z[125];
z[127] = z[23] + z[86];
z[128] = 4 * abb[85];
z[37] = z[37] + -z[89] + z[126] + z[127] + z[128];
z[37] = z[37] * z[70];
z[89] = -z[95] + z[97];
z[97] = z[23] + z[89] + z[126];
z[126] = 2 * abb[1];
z[97] = z[97] * z[126];
z[126] = z[26] + -z[76];
z[129] = z[79] + z[126];
z[129] = abb[17] * z[129];
z[37] = -z[37] + z[97] + -4 * z[129];
z[97] = abb[40] * z[116];
z[116] = 2 * abb[87];
z[97] = z[97] + z[116];
z[130] = 2 * z[46];
z[131] = abb[42] * z[130];
z[130] = abb[43] + z[130];
z[130] = abb[43] * z[130];
z[131] = z[97] + -z[130] + z[131];
z[26] = z[26] + -z[96] + z[131];
z[26] = z[26] * z[39];
z[132] = -abb[38] + 2 * z[88];
z[133] = abb[43] * z[132];
z[134] = abb[45] * z[88];
z[135] = abb[49] + z[134];
z[132] = abb[38] * z[132];
z[133] = -z[106] + z[132] + z[133] + -2 * z[135];
z[136] = abb[38] + abb[43];
z[137] = -z[0] + z[136];
z[137] = abb[42] * z[137];
z[138] = -z[8] + z[72];
z[139] = z[0] * z[138];
z[56] = -z[56] + -z[133] + -z[137] + -z[139];
z[56] = abb[13] * z[56];
z[140] = z[65] + -z[72];
z[141] = abb[40] * z[140];
z[141] = z[121] + z[141];
z[142] = abb[43] * z[28];
z[123] = z[109] + z[123] + -z[141] + z[142];
z[142] = 2 * abb[10];
z[123] = z[123] * z[142];
z[142] = -abb[38] + abb[43];
z[143] = -abb[38] + z[32];
z[144] = -z[142] * z[143];
z[144] = -z[67] + z[144];
z[145] = -z[107] + z[144];
z[146] = abb[42] * z[142];
z[146] = -z[48] + z[146];
z[147] = z[145] + z[146];
z[147] = abb[14] * z[147];
z[148] = abb[99] + abb[100];
z[123] = abb[97] + z[123] + z[147] + -z[148];
z[147] = z[36] * z[114];
z[149] = abb[88] + z[86];
z[150] = -abb[89] + z[149];
z[151] = (T(5) / T(3)) * z[79];
z[106] = -z[106] + z[147] + 2 * z[150] + -z[151];
z[147] = -z[72] * z[85];
z[139] = z[106] + z[139] + z[147];
z[139] = abb[7] * z[139];
z[147] = abb[38] * z[19];
z[147] = -abb[85] + abb[88] + z[16] + -z[127] + z[147];
z[147] = abb[11] * z[147];
z[150] = -abb[39] + z[57];
z[150] = abb[38] * z[150];
z[152] = abb[39] * abb[45];
z[149] = abb[46] + -abb[48] + z[149] + z[150] + z[152];
z[150] = -abb[85] + z[149];
z[152] = z[59] * z[150];
z[153] = -abb[11] + abb[17];
z[154] = 4 * abb[51];
z[153] = z[153] * z[154];
z[152] = z[152] + z[153];
z[153] = z[38] * z[114];
z[38] = abb[45] * z[38];
z[155] = -abb[48] + -abb[49] + -z[38];
z[153] = (T(1) / T(3)) * z[79] + z[153] + 2 * z[155];
z[153] = abb[2] * z[153];
z[155] = abb[22] * abb[91];
z[156] = abb[98] + z[155];
z[64] = abb[91] * z[64];
z[3] = z[3] + z[5] + -z[26] + -z[37] + z[56] + z[64] + z[108] + -z[123] + z[139] + -4 * z[147] + -z[152] + z[153] + z[156];
z[3] = abb[79] * z[3];
z[5] = abb[38] * z[42];
z[42] = -z[11] + z[138];
z[42] = abb[40] * z[42];
z[56] = -abb[39] * abb[44];
z[64] = abb[38] + z[36];
z[64] = abb[43] * z[64];
z[5] = z[5] + z[42] + z[54] + z[56] + z[64] + -z[107] + -z[110] + z[115];
z[5] = z[5] * z[70];
z[42] = prod_pow(abb[38], 2);
z[54] = z[42] + z[115];
z[56] = abb[40] * z[11];
z[44] = -z[44] + -z[48] + -z[54] + z[56] + -z[116] + z[137];
z[44] = abb[13] * z[44];
z[56] = -abb[38] + z[36];
z[56] = abb[43] * z[56];
z[56] = z[56] + -z[103];
z[64] = -z[67] + z[115];
z[70] = abb[40] * z[138];
z[70] = z[70] + -z[116];
z[103] = abb[38] + 2 * z[19];
z[103] = abb[38] * z[103];
z[103] = z[56] + z[64] + z[70] + z[103] + z[112];
z[103] = abb[3] * z[103];
z[108] = abb[38] + z[8];
z[108] = abb[38] * z[108];
z[108] = -z[9] + z[108] + z[115];
z[115] = abb[38] + -abb[41];
z[115] = z[0] * z[115];
z[115] = -z[48] + -z[108] + z[115] + -z[121];
z[115] = abb[12] * z[115];
z[137] = abb[42] * z[77];
z[138] = abb[43] * z[36];
z[137] = -z[70] + z[112] + z[137] + -z[138];
z[139] = z[39] * z[137];
z[153] = -abb[88] + -abb[89] + z[110];
z[157] = abb[39] * abb[43];
z[158] = -abb[39] + z[72];
z[158] = abb[40] * z[158];
z[159] = abb[42] * z[19];
z[157] = -abb[86] + -abb[87] + -z[153] + z[157] + z[158] + z[159];
z[157] = z[27] * z[157];
z[33] = abb[39] * z[33];
z[158] = z[33] + z[107];
z[143] = -z[8] + -z[143];
z[143] = abb[38] * z[143];
z[143] = z[18] + z[48] + z[143] + z[158];
z[143] = abb[15] * z[143];
z[63] = abb[91] * z[63];
z[30] = -abb[90] * z[30];
z[5] = z[5] + z[30] + z[44] + z[63] + z[103] + z[115] + -z[123] + z[139] + z[143] + -z[156] + z[157];
z[5] = abb[82] * z[5];
z[30] = abb[38] * z[92];
z[44] = z[30] + z[76] + -z[134];
z[63] = abb[42] * z[57];
z[92] = abb[43] * z[57];
z[63] = z[44] + z[63] + -z[92] + z[112];
z[103] = -z[63] * z[84];
z[112] = abb[48] + abb[88];
z[94] = -abb[38] * z[94];
z[38] = z[38] + z[92] + z[94] + -z[110] + z[112];
z[38] = z[38] * z[78];
z[78] = abb[39] * z[12];
z[9] = z[9] + z[30] + z[48] + z[60] + -z[78];
z[9] = abb[69] * z[9];
z[30] = -abb[69] + abb[73];
z[92] = abb[71] + z[30];
z[92] = z[76] * z[92];
z[94] = z[45] * z[48];
z[92] = z[92] + -z[94];
z[110] = z[22] + -z[117];
z[110] = abb[38] * z[110];
z[80] = z[80] * z[149];
z[22] = abb[39] * z[22];
z[57] = -abb[71] * z[57] * z[85];
z[9] = z[9] + -z[22] + z[38] + z[57] + z[80] + -z[92] + z[103] + z[110] + z[120] + z[122];
z[9] = abb[32] * z[9];
z[38] = -z[46] + z[72];
z[38] = abb[42] * z[38];
z[57] = -z[8] + z[46];
z[57] = abb[43] * z[57];
z[38] = z[38] + -z[44] + z[57] + -z[70];
z[44] = z[38] * z[84];
z[57] = z[48] + -z[133];
z[57] = abb[73] * z[57];
z[69] = z[0] * z[69];
z[70] = -abb[73] * z[136];
z[69] = -z[21] + z[69] + z[70] + z[101];
z[69] = abb[42] * z[69];
z[70] = z[8] + z[136];
z[70] = abb[69] * z[70];
z[72] = abb[43] + z[8];
z[72] = abb[73] * z[72];
z[30] = abb[40] * z[30];
z[30] = -z[30] + z[72];
z[70] = -z[30] + z[43] + z[70];
z[70] = z[0] * z[70];
z[54] = z[45] * z[54];
z[72] = -abb[69] + z[45];
z[72] = z[72] * z[116];
z[80] = abb[38] * z[90];
z[51] = -z[51] + -z[80];
z[51] = abb[71] * z[51];
z[88] = z[88] * z[136];
z[88] = abb[47] + z[88] + -z[135];
z[87] = z[87] * z[88];
z[88] = abb[43] * z[43];
z[44] = z[44] + z[51] + z[54] + z[57] + z[69] + -z[70] + z[72] + z[87] + z[88] + -z[92];
z[44] = abb[27] * z[44];
z[51] = abb[43] + z[13];
z[54] = z[28] + z[51];
z[54] = abb[43] * z[54];
z[50] = -abb[39] + -z[50] + -z[99];
z[50] = abb[40] * z[50];
z[57] = 3 * abb[40] + -abb[43] + -z[11] + z[19];
z[57] = abb[42] * z[57];
z[69] = -abb[45] + -z[36];
z[69] = abb[38] + 2 * z[69];
z[69] = abb[38] * z[69];
z[15] = -4 * abb[86] + z[15] + -z[18] + z[50] + z[54] + z[57] + -z[60] + z[69] + -3 * z[86] + -z[93] + z[119];
z[15] = abb[79] * z[15];
z[18] = abb[39] * z[61];
z[18] = -z[18] + -z[111] + z[118];
z[50] = abb[40] + abb[43] + -z[91];
z[50] = abb[42] * z[50];
z[35] = abb[38] * z[35];
z[35] = -z[18] + z[35] + z[50] + z[64] + z[138] + -z[141];
z[35] = abb[82] * z[35];
z[34] = z[34] + -z[90];
z[34] = abb[38] * z[34];
z[33] = -z[33] + z[34] + z[78] + -2 * z[112] + -z[113];
z[34] = z[33] * z[52];
z[20] = -abb[83] * z[20];
z[50] = -abb[63] * z[47];
z[52] = abb[84] * abb[90];
z[15] = z[15] + z[20] + z[34] + z[35] + z[50] + z[52];
z[15] = abb[4] * z[15];
z[20] = 5 * abb[41] + -z[32];
z[20] = abb[44] * z[20];
z[20] = -z[20] + 3 * z[23] + -z[48] + -z[83] + z[89] + z[98] + z[128];
z[34] = abb[3] * z[20];
z[13] = z[13] + z[77];
z[13] = abb[42] * z[13];
z[35] = z[36] + z[51];
z[35] = abb[43] * z[35];
z[13] = -z[13] + z[35] + z[96] + -z[104] + -z[125] + z[127];
z[35] = z[13] * z[39];
z[36] = abb[13] * z[38];
z[38] = z[59] * z[63];
z[27] = -z[27] * z[137];
z[50] = -abb[17] * z[154];
z[27] = abb[97] + 3 * abb[98] + z[27] + z[34] + z[35] + 2 * z[36] + -z[37] + z[38] + z[50] + z[148];
z[27] = abb[83] * z[27];
z[16] = -z[16] + z[23] + z[158];
z[23] = -z[8] + z[32];
z[32] = 6 * abb[45] + -z[23] + -z[82];
z[32] = abb[38] * z[32];
z[34] = 3 * abb[38] + abb[43] + -z[12];
z[35] = z[34] * z[114];
z[36] = -abb[40] + 2 * z[136];
z[37] = z[0] * z[36];
z[38] = -z[0] + z[34];
z[50] = z[38] * z[85];
z[8] = 2 * z[8];
z[51] = 3 * abb[45] + z[8];
z[51] = abb[45] * z[51];
z[52] = abb[47] + abb[50];
z[32] = 6 * abb[85] + z[16] + z[32] + -z[35] + z[37] + z[50] + -z[51] + -4 * z[52] + z[81];
z[32] = abb[0] * z[32];
z[35] = abb[40] * z[36];
z[35] = -z[7] + z[35];
z[6] = abb[47] + z[6];
z[6] = 2 * z[6] + -z[35] + z[42] + z[130];
z[6] = abb[8] * z[6];
z[6] = z[6] + z[155];
z[36] = z[81] + z[126];
z[37] = z[36] + z[53];
z[42] = abb[5] * z[37];
z[50] = -z[129] + z[147];
z[51] = abb[21] * abb[90];
z[50] = 4 * z[50] + -z[51] + z[152];
z[51] = abb[43] * z[100];
z[29] = -z[29] + z[51] + z[97] + -z[146];
z[51] = z[29] * z[62];
z[53] = -abb[38] + abb[40];
z[53] = abb[8] * z[53];
z[54] = abb[42] * z[53];
z[26] = 2 * z[6] + -z[26] + z[32] + -z[42] + -z[50] + z[51] + 4 * z[54];
z[25] = z[25] * z[26];
z[26] = abb[43] * abb[73];
z[19] = abb[71] * z[19];
z[19] = -z[19] + -z[21] + -z[26] + z[105];
z[19] = z[19] * z[85];
z[26] = abb[73] * z[106];
z[32] = z[84] * z[137];
z[42] = -z[151] + 2 * z[153];
z[42] = abb[71] * z[42];
z[19] = z[19] + -z[26] + z[32] + z[42] + z[72];
z[26] = z[45] * z[67];
z[26] = z[26] + z[94];
z[32] = z[43] + z[74] + z[75];
z[32] = abb[38] * z[32];
z[42] = abb[39] * z[114];
z[51] = z[42] + -z[79] + z[108];
z[51] = abb[69] * z[51];
z[54] = abb[47] + -abb[88];
z[54] = z[45] * z[54];
z[57] = -z[66] * z[114];
z[22] = z[19] + z[22] + z[26] + z[32] + z[51] + 2 * z[54] + z[57] + -z[70];
z[22] = abb[28] * z[22];
z[31] = -z[31] * z[142];
z[21] = -z[21] + z[31] + z[43];
z[21] = abb[42] * z[21];
z[31] = z[43] + 2 * z[66];
z[31] = -z[31] * z[142];
z[32] = -z[48] + z[144];
z[32] = abb[71] * z[32];
z[43] = z[48] + -z[145];
z[43] = abb[73] * z[43];
z[21] = z[21] + z[26] + z[31] + z[32] + z[43] + z[122];
z[21] = abb[33] * z[21];
z[20] = abb[83] * z[20];
z[26] = -z[11] + z[140];
z[26] = abb[40] * z[26];
z[26] = z[26] + -z[107] + z[116] + z[121];
z[28] = z[11] + -z[28];
z[28] = abb[43] * z[28];
z[31] = -abb[38] + z[65];
z[31] = abb[38] * z[31];
z[18] = z[18] + z[26] + z[28] + z[31] + z[109];
z[18] = abb[82] * z[18];
z[28] = abb[81] * z[37];
z[18] = z[18] + z[20] + z[28];
z[18] = abb[5] * z[18];
z[20] = -abb[88] + abb[89];
z[20] = z[20] * z[45];
z[28] = abb[39] * z[102];
z[31] = abb[92] * z[81];
z[32] = -abb[43] * z[66];
z[20] = z[20] + z[28] + z[31] + z[32];
z[28] = -abb[69] * z[41];
z[28] = z[28] + z[30] + z[75];
z[0] = z[0] * z[28];
z[28] = abb[94] + abb[95];
z[30] = -abb[93] + -z[28];
z[30] = z[30] * z[47];
z[31] = z[42] + -z[151];
z[31] = abb[69] * z[31];
z[0] = z[0] + -z[17] + z[19] + 2 * z[20] + z[30] + z[31];
z[0] = abb[30] * z[0];
z[7] = z[7] + -z[76] + -z[80] + z[95] + -z[124] + -z[131];
z[7] = abb[9] * z[7];
z[17] = z[48] + z[76];
z[11] = -z[11] * z[46];
z[19] = abb[42] * z[38];
z[20] = -abb[43] * z[34];
z[11] = z[11] + z[17] + z[19] + z[20] + z[35] + -2 * z[52];
z[11] = abb[0] * z[11];
z[19] = abb[3] * z[29];
z[20] = z[53] * z[85];
z[6] = z[6] + z[7] + z[11] + z[19] + z[20];
z[6] = z[6] * z[14];
z[7] = abb[45] + z[8];
z[7] = abb[45] * z[7];
z[8] = -z[7] + z[36] + -z[98] + z[132];
z[8] = z[8] * z[39];
z[11] = -z[12] + z[23];
z[11] = abb[38] * z[11];
z[7] = z[7] + z[11] + -z[16] + -z[17];
z[7] = abb[0] * z[7];
z[7] = z[7] + z[8] + z[50];
z[7] = abb[81] * z[7];
z[8] = -abb[83] * z[13];
z[11] = -abb[41] + -abb[44];
z[11] = abb[39] * z[11];
z[10] = -abb[38] + z[10];
z[10] = abb[38] * z[10];
z[10] = z[10] + z[11] + z[26] + -z[56] + z[119];
z[10] = abb[82] * z[10];
z[8] = z[8] + z[10];
z[8] = abb[6] * z[8];
z[10] = abb[21] * z[33];
z[11] = -abb[20] * z[37];
z[12] = -z[49] * z[150];
z[13] = -abb[0] * abb[90];
z[10] = z[10] + z[11] + z[12] + z[13];
z[10] = abb[84] * z[10];
z[11] = -z[28] * z[58];
z[12] = z[45] + z[68];
z[12] = abb[68] * z[12];
z[13] = abb[36] * abb[91];
z[14] = z[13] * z[73];
z[16] = abb[34] + -abb[35];
z[16] = abb[92] * z[16];
z[4] = -abb[12] + z[4];
z[4] = abb[63] * z[4];
z[4] = z[4] + z[16];
z[4] = z[4] * z[47];
z[16] = -3 * z[55] + z[71];
z[16] = abb[93] * z[16];
z[13] = abb[68] + z[13];
z[13] = abb[73] * z[13];
z[17] = -abb[63] * z[40];
z[19] = abb[90] * z[24];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + 4 * z[16] + z[17] + z[18] + z[19] + z[21] + z[22] + z[25] + z[27] + z[44];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_838_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.716189463884924266520168352034841159627932287386486731870255224"),stof<T>("0.5198668473063908739104816218943657540319412722144541130253495268")}, std::complex<T>{stof<T>("-21.320386546457785579583542635939367404963811062742711707591249931"),stof<T>("-41.907621805331554588925262824653324268568311782159758858123251945")}, std::complex<T>{stof<T>("7.716189463884924266520168352034841159627932287386486731870255224"),stof<T>("-0.5198668473063908739104816218943657540319412722144541130253495268")}, std::complex<T>{stof<T>("-11.776015237375874093086411863739370171415892975939476487701478967"),stof<T>("-85.894710999888672673492452136884111553264388653177334168347901997")}, std::complex<T>{stof<T>("-13.604197082572861313063374283904526245335878775356224975720994707"),stof<T>("-42.427488652637945462835744446547690022600253054374212971148601472")}, std::complex<T>{stof<T>("21.320386546457785579583542635939367404963811062742711707591249931"),stof<T>("41.907621805331554588925262824653324268568311782159758858123251945")}, std::complex<T>{stof<T>("5.888007618687937046543205931869685085707946487969738243850739483"),stof<T>("42.947355499944336336746226068442055776632194326588667084173950998")}, std::complex<T>{stof<T>("-13.633032417376269935413032560656827828225362359303341922595635174"),stof<T>("-6.797664124413273680744588049712544846318567323356181378238250271")}, std::complex<T>{stof<T>("11.744594664539802681323952526440144182537686854997323502521521271"),stof<T>("-34.920447561433775146995720967763072623054772025276781345406222811")}, std::complex<T>{stof<T>("11.744594664539802681323952526440144182537686854997323502521521271"),stof<T>("-34.920447561433775146995720967763072623054772025276781345406222811")}, std::complex<T>{stof<T>("-0.920971866576962337941148155750115482473514385517602325172084214"),stof<T>("-46.892406539014338283026912180790505745106726566388320338748512867")}, std::complex<T>{stof<T>("11.744594664539802681323952526440144182537686854997323502521521271"),stof<T>("-34.920447561433775146995720967763072623054772025276781345406222811")}, std::complex<T>{stof<T>("-25.377627081916072616736985087096972010763049214300665425117156444"),stof<T>("28.12278343702050146625113291805052777673620470192059996716797254")}, std::complex<T>{stof<T>("-31.115275361457826513670449515504352313048456280500870869500513354"),stof<T>("-94.334644807388216797982693524221762234388740403884208578572287819")}, std::complex<T>{stof<T>("-28.906423433909449776319138026216768471607120310154062862567903952"),stof<T>("27.027666238563836864442354724890052789972501232523597615027963347")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[108].real()/kbase.W[108].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[141]) - log_iphi_im(kbase.W[141]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_838_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_838_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("45.026063215807729003053257926354577418913717468185901161133767849"),stof<T>("11.303732369445695951231871699928045315626257971877383059408242016")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({200});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,102> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W23(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W65(k,dl), dlog_W109(k,dl), dlog_W119(k,dv), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W142(k,dv), dlog_W187(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), f_2_25_im(k), (log2_im(k.W[164]) - log2_im(base_point<T>.W[164])), (log2_im(k.W[166]) - log2_im(base_point<T>.W[166])), (log2_im(k.W[170]) - log2_im(base_point<T>.W[170])), (log2_im(k.W[172]) - log2_im(base_point<T>.W[172])), (log3_im(k.W[190]) - log3_im(base_point<T>.W[190])), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[108].real()/k.W[108].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[141]) - log_iphi_im(k.W[141])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), f_2_25_re(k), (log2_re(k.W[164]) - log2_re(base_point<T>.W[164])), (log2_re(k.W[166]) - log2_re(base_point<T>.W[166])), (log2_re(k.W[170]) - log2_re(base_point<T>.W[170])), (log2_re(k.W[172]) - log2_re(base_point<T>.W[172])), (log3_re(k.W[190]) - log3_re(base_point<T>.W[190])), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W165(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[97] = c.real();
abb[64] = c.imag();
SpDLog_Sigma5<T,200,164>(k, dv, abb[97], abb[64], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W167(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[98] = c.real();
abb[65] = c.imag();
SpDLog_Sigma5<T,200,166>(k, dv, abb[98], abb[65], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W171(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[99] = c.real();
abb[66] = c.imag();
SpDLog_Sigma5<T,200,170>(k, dv, abb[99], abb[66], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W173(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[100] = c.real();
abb[67] = c.imag();
SpDLog_Sigma5<T,200,172>(k, dv, abb[100], abb[67], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W191(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[101] = c.real();
abb[68] = c.imag();
SpDLog_Sigma5<T,200,190>(k, dv, abb[101], abb[68], f_2_32_series_coefficients<T>);
}

                    
            return f_4_838_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_838_DLogXconstant_part(base_point<T>, kend);
	value += f_4_838_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_838_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_838_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_838_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_838_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_838_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_838_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
