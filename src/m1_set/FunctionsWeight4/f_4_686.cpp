/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_686.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_686_abbreviated (const std::array<T,36>& abb) {
T z[87];
z[0] = 4 * abb[26];
z[1] = 2 * abb[25];
z[2] = 6 * abb[29] + -z[1];
z[3] = -abb[23] + -abb[24] + z[2];
z[4] = 5 * abb[28];
z[5] = 4 * abb[27];
z[6] = z[0] + -3 * z[3] + z[4] + z[5];
z[7] = abb[8] * z[6];
z[8] = abb[23] + abb[27];
z[9] = abb[24] * (T(5) / T(3));
z[10] = 2 * abb[28];
z[8] = abb[26] * (T(5) / T(3)) + -z[2] + (T(1) / T(3)) * z[8] + z[9] + z[10];
z[11] = 4 * abb[4];
z[8] = z[8] * z[11];
z[12] = -abb[23] + abb[24];
z[13] = 2 * abb[26];
z[14] = -abb[28] + z[12] + z[13];
z[15] = abb[2] * z[14];
z[16] = 2 * abb[27];
z[17] = -abb[28] + z[16];
z[18] = -z[12] + z[17];
z[19] = abb[1] * z[18];
z[20] = 2 * abb[5];
z[21] = -abb[25] + 3 * abb[29];
z[22] = abb[24] * (T(1) / T(3)) + abb[23] * (T(2) / T(3)) + -z[21];
z[22] = 9 * abb[26] + abb[28] * (T(29) / T(3)) + abb[27] * (T(41) / T(3)) + 14 * z[22];
z[22] = z[20] * z[22];
z[9] = -8 * abb[27] + abb[23] * (T(-11) / T(3)) + abb[26] * (T(-8) / T(3)) + abb[28] * (T(7) / T(3)) + z[2] + z[9];
z[23] = 2 * abb[6];
z[9] = z[9] * z[23];
z[24] = -abb[23] + 4 * abb[29];
z[25] = 4 * abb[25];
z[24] = 3 * z[24] + -z[25];
z[26] = abb[24] + -z[24];
z[26] = abb[26] * (T(28) / T(3)) + abb[28] * (T(47) / T(3)) + abb[27] * (T(58) / T(3)) + 5 * z[26];
z[26] = abb[3] * z[26];
z[27] = 2 * abb[30] + abb[31] + abb[32];
z[28] = abb[10] * z[27];
z[29] = abb[9] * z[27];
z[30] = abb[26] + -abb[27] + z[12];
z[31] = abb[7] * z[30];
z[32] = 3 * abb[28];
z[33] = -12 * abb[25] + 36 * abb[29] + abb[26] * (T(-104) / T(3)) + abb[24] * (T(-59) / T(3)) + abb[27] * (T(-22) / T(3)) + abb[23] * (T(23) / T(3)) + z[32];
z[33] = abb[0] * z[33];
z[8] = -4 * z[7] + z[8] + z[9] + (T(-1) / T(3)) * z[15] + 11 * z[19] + z[22] + z[26] + (T(-32) / T(3)) * z[28] + -8 * z[29] + (T(16) / T(3)) * z[31] + z[33];
z[8] = prod_pow(m1_set::bc<T>[0], 2) * z[8];
z[9] = -6 * abb[25] + 18 * abb[29];
z[22] = -z[4] + z[9];
z[26] = 5 * abb[24];
z[31] = abb[23] + z[26];
z[33] = 6 * abb[26];
z[34] = z[16] + -z[22] + z[31] + z[33];
z[35] = abb[6] * z[34];
z[36] = abb[11] * z[27];
z[37] = z[7] + z[36];
z[38] = -abb[24] + z[21];
z[39] = 2 * z[38];
z[40] = 5 * abb[26];
z[41] = 3 * abb[27];
z[42] = -abb[28] + -z[39] + z[40] + z[41];
z[43] = 2 * abb[4];
z[42] = z[42] * z[43];
z[44] = 7 * abb[28];
z[45] = -abb[26] + z[44];
z[46] = -abb[23] + z[21];
z[47] = 5 * abb[27];
z[48] = -z[45] + 6 * z[46] + -z[47];
z[48] = abb[5] * z[48];
z[49] = abb[26] + abb[27];
z[50] = -z[3] + z[10] + z[49];
z[51] = abb[0] * z[50];
z[52] = -z[16] + z[32];
z[53] = -z[0] + -z[12] + z[52];
z[54] = 2 * abb[7];
z[55] = z[53] * z[54];
z[56] = -abb[23] + 2 * abb[29];
z[1] = -z[1] + 3 * z[56];
z[56] = abb[24] + z[1];
z[57] = -abb[28] + -z[5] + z[56];
z[58] = abb[3] * z[57];
z[59] = 2 * z[29];
z[35] = 5 * z[15] + z[35] + -2 * z[37] + z[42] + -z[48] + z[51] + z[55] + -z[58] + -z[59];
z[42] = -abb[33] * z[35];
z[48] = z[20] * z[50];
z[49] = -abb[28] + z[49];
z[51] = z[43] * z[49];
z[55] = z[28] + z[51];
z[58] = z[15] + -z[36];
z[60] = 2 * z[58];
z[61] = z[3] + -z[32];
z[62] = abb[7] * z[61];
z[63] = 2 * z[19];
z[64] = abb[24] + abb[28];
z[65] = -abb[26] + z[21] + -z[64];
z[66] = 2 * abb[0];
z[65] = z[65] * z[66];
z[67] = abb[26] + z[16] + -z[46];
z[68] = 2 * abb[3];
z[67] = z[67] * z[68];
z[65] = z[48] + -z[55] + -z[60] + z[62] + -z[63] + z[65] + z[67];
z[65] = abb[15] * z[65];
z[67] = z[5] + -z[12] + z[13] + -z[32];
z[69] = abb[4] * z[67];
z[69] = z[29] + z[69];
z[70] = z[15] + z[19];
z[71] = abb[28] + z[16];
z[72] = -z[3] + z[13] + z[71];
z[73] = abb[3] * z[72];
z[74] = -z[20] * z[72];
z[75] = abb[23] + -2 * abb[24] + z[21];
z[76] = 3 * abb[26];
z[77] = -z[75] + z[76];
z[77] = z[66] * z[77];
z[74] = z[28] + -2 * z[36] + -z[62] + z[69] + z[70] + -z[73] + z[74] + z[77];
z[74] = abb[12] * z[74];
z[77] = 2 * z[15];
z[78] = z[28] + -z[36] + z[77];
z[79] = abb[4] * z[14];
z[79] = z[78] + z[79];
z[16] = z[16] + z[32];
z[80] = abb[24] + z[13];
z[81] = -z[1] + z[16] + -z[80];
z[81] = abb[7] * z[81];
z[82] = abb[0] * z[61];
z[83] = z[48] + z[82];
z[73] = -z[73] + 2 * z[79] + z[81] + -z[83];
z[73] = abb[13] * z[73];
z[65] = z[65] + -z[73] + z[74];
z[65] = abb[12] * z[65];
z[74] = abb[3] * z[18];
z[14] = abb[0] * z[14];
z[70] = -z[14] + z[70] + -z[74];
z[70] = abb[15] * z[70];
z[70] = z[70] + z[73];
z[70] = abb[15] * z[70];
z[79] = abb[7] * z[67];
z[69] = z[14] + -z[36] + z[69] + -z[79];
z[84] = 4 * z[19];
z[69] = 2 * z[69] + z[84];
z[69] = abb[17] * z[69];
z[85] = abb[4] * z[18];
z[86] = z[20] * z[49];
z[14] = -z[14] + -z[29] + -z[85] + z[86];
z[14] = prod_pow(abb[13], 2) * z[14];
z[14] = z[14] + z[42] + z[65] + z[69] + z[70];
z[42] = z[7] + -z[15];
z[65] = 6 * abb[27];
z[69] = z[3] + z[32] + -z[33] + -z[65];
z[70] = abb[4] * z[69];
z[70] = z[42] + z[70];
z[1] = z[1] + -z[13] + z[64] + -z[65];
z[1] = abb[3] * z[1];
z[64] = abb[7] * z[69];
z[69] = 5 * abb[23];
z[22] = -z[22] + z[65] + z[69] + z[80];
z[20] = -z[20] * z[22];
z[34] = -z[23] * z[34];
z[65] = 3 * abb[24];
z[86] = -abb[23] + -z[2] + z[65];
z[17] = -z[17] + -z[33] + -z[86];
z[17] = abb[0] * z[17];
z[17] = z[1] + z[17] + z[20] + z[34] + -z[63] + -3 * z[64] + 2 * z[70];
z[17] = abb[19] * z[17];
z[20] = -abb[3] * z[67];
z[23] = z[23] * z[50];
z[33] = -z[23] + z[63];
z[34] = z[33] + z[60];
z[3] = -z[3] + z[41] + z[76];
z[60] = z[3] * z[54];
z[63] = abb[0] * z[53];
z[20] = z[20] + z[34] + -z[48] + -z[55] + -z[59] + z[60] + z[63];
z[20] = abb[12] * z[20];
z[55] = z[28] + z[62];
z[21] = abb[27] + -z[21] + z[80];
z[21] = z[21] * z[66];
z[60] = abb[27] + abb[28];
z[62] = z[46] + -z[60];
z[62] = z[62] * z[68];
z[21] = z[21] + -z[34] + -z[51] + z[55] + z[62];
z[21] = abb[15] * z[21];
z[11] = z[11] * z[49];
z[1] = -z[1] + z[11] + z[59] + z[64] + z[83];
z[1] = abb[13] * z[1];
z[1] = z[1] + z[20] + z[21];
z[11] = abb[28] + 2 * z[46] + -z[47] + -z[76];
z[11] = abb[4] * z[11];
z[20] = z[11] + z[42] + -z[64];
z[21] = abb[5] * z[22];
z[21] = z[21] + -z[59];
z[22] = 7 * abb[27];
z[2] = -abb[26] + -z[2] + z[22] + -z[65] + z[69];
z[2] = abb[3] * z[2];
z[5] = z[5] + z[32];
z[9] = -8 * abb[26] + -z[5] + z[9] + -z[31];
z[9] = abb[0] * z[9];
z[31] = 11 * abb[28];
z[26] = -2 * abb[23] + -7 * abb[25] + 21 * abb[29] + -z[26];
z[26] = -13 * abb[26] + -z[22] + 2 * z[26] + -z[31];
z[26] = abb[6] * z[26];
z[34] = 3 * z[19];
z[2] = z[2] + z[9] + 2 * z[20] + -z[21] + z[26] + -z[34];
z[2] = abb[14] * z[2];
z[1] = 2 * z[1] + z[2];
z[1] = abb[14] * z[1];
z[2] = -z[28] + -z[36] + z[85];
z[9] = z[13] + z[52] + z[86];
z[9] = abb[7] * z[9];
z[20] = abb[0] * z[72];
z[26] = abb[3] * z[61];
z[2] = -2 * z[2] + -z[9] + z[20] + z[23] + z[26] + -z[84];
z[20] = abb[12] + -abb[15];
z[2] = z[2] * z[20];
z[20] = z[19] + z[21];
z[3] = abb[4] * z[3];
z[23] = z[3] + -z[7];
z[26] = -z[39] + z[60] + z[76];
z[36] = z[26] * z[66];
z[39] = abb[23] + 4 * abb[24] + 5 * abb[25] + -15 * abb[29];
z[39] = 7 * abb[26] + abb[27] + z[31] + 2 * z[39];
z[39] = abb[6] * z[39];
z[42] = abb[3] * z[49];
z[23] = z[20] + 2 * z[23] + z[36] + z[39] + z[42] + z[64];
z[23] = abb[14] * z[23];
z[2] = z[2] + z[23];
z[23] = -abb[4] * z[26];
z[7] = z[7] + z[23];
z[23] = 6 * z[38];
z[26] = -abb[26] + z[23] + -z[31] + z[47];
z[26] = abb[6] * z[26];
z[31] = abb[26] + z[47];
z[36] = -z[31] + z[56];
z[36] = abb[3] * z[36];
z[7] = 2 * z[7] + -z[20] + z[26] + z[36] + z[82];
z[7] = abb[16] * z[7];
z[2] = 2 * z[2] + z[7];
z[2] = abb[16] * z[2];
z[1] = z[1] + z[2] + 2 * z[14] + z[17];
z[2] = z[28] + z[37];
z[7] = z[2] + z[11] + z[79];
z[11] = 5 * z[19] + z[21];
z[14] = abb[3] * z[50];
z[17] = abb[27] + z[23] + -z[40] + -z[44];
z[17] = abb[6] * z[17];
z[19] = abb[28] + z[0] + z[86];
z[19] = abb[0] * z[19];
z[7] = 2 * z[7] + -z[11] + -z[14] + z[17] + -z[19];
z[14] = abb[34] * z[7];
z[19] = abb[9] + abb[11];
z[19] = z[19] * z[30];
z[20] = -abb[0] + abb[3];
z[20] = z[20] * z[27];
z[18] = abb[10] * z[18];
z[18] = -z[18] + 2 * z[19] + z[20];
z[19] = abb[35] * z[18];
z[14] = -z[14] + z[19];
z[19] = abb[4] + -abb[7];
z[19] = z[19] * z[53];
z[19] = z[19] + -z[29] + -z[74] + -z[78];
z[19] = abb[18] * z[19];
z[1] = 2 * z[1] + z[8] + 4 * z[14] + 8 * z[19];
z[1] = 2 * z[1];
z[8] = -z[15] + z[37];
z[14] = -z[43] * z[72];
z[15] = abb[23] + -12 * abb[29] + z[25] + z[65];
z[4] = -z[4] + -z[13] + -z[15];
z[4] = abb[0] * z[4];
z[10] = abb[26] + -z[10] + -z[12] + z[41];
z[10] = abb[3] * z[10];
z[12] = abb[5] * z[57];
z[4] = z[4] + 2 * z[8] + z[10] + z[12] + z[14] + z[17] + 4 * z[29] + -z[34] + z[55];
z[4] = abb[14] * z[4];
z[0] = z[0] + z[15] + z[16];
z[0] = abb[0] * z[0];
z[5] = -z[5] + z[24] + -z[80];
z[5] = abb[3] * z[5];
z[8] = z[43] + -z[54];
z[8] = z[8] * z[30];
z[0] = z[0] + z[5] + z[8] + 3 * z[28] + -z[33] + -z[48] + z[77];
z[0] = abb[15] * z[0];
z[5] = abb[3] * z[6];
z[6] = -2 * z[28] + -z[51] + -z[58];
z[8] = abb[5] * z[72];
z[10] = -14 * abb[26] + -z[71] + -3 * z[86];
z[10] = abb[0] * z[10];
z[5] = z[5] + 2 * z[6] + 4 * z[8] + z[10] + z[33] + -z[59] + -z[81];
z[5] = abb[12] * z[5];
z[2] = -z[2] + z[3];
z[3] = -z[22] + z[45] + -2 * z[75];
z[3] = abb[6] * z[3];
z[6] = z[31] + z[32] + -4 * z[46];
z[6] = abb[3] * z[6];
z[8] = -z[49] * z[66];
z[2] = 2 * z[2] + z[3] + z[6] + z[8] + z[9] + z[11];
z[2] = abb[16] * z[2];
z[0] = z[0] + z[2] + z[4] + z[5] + z[73];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[20] * z[35];
z[3] = -abb[21] * z[7];
z[4] = abb[22] * z[18];
z[0] = z[0] + z[2] + z[3] + z[4];
z[0] = 8 * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_686_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-263.33262288473358337202484754657244520510744637236115983987312298"),stof<T>("-298.95789615119850372338946976591115219117466839723852143290881727")}, std::complex<T>{stof<T>("162.72600486701423491334291704805335936155746093844173639710116183"),stof<T>("847.94312420602694487666021411385246506455122811695803083269805584")}, std::complex<T>{stof<T>("-100.60661801771934845868193049851908584354998543391942344277196114"),stof<T>("548.98522805482844115327074434794131287337655971971950939978923857")}, std::complex<T>{stof<T>("307.96754876328492364355691049788781402777177830184667996957578932"),stof<T>("1208.03935454322419776383790846184916374120975019977354194701230096")}, std::complex<T>{stof<T>("-118.091078988462894641810854096737990538893129008956216267398495489"),stof<T>("61.138334185998749163788224582085546485483853685576989681405427849")}, std::complex<T>{stof<T>("-245.84816191399003718889592394835354050976430279732436701524658863"),stof<T>("188.88899771763118826609304999994461419671803763690399828547499344")}, std::complex<T>{stof<T>("301.8198540531580453760457914955572575306499563017582703283158834"),stof<T>("-1646.9556841644853234598122330438239386201296791591585281993677157")}, std::complex<T>{stof<T>("-462.50277494255119642110620841946829554571392496246500580108646324"),stof<T>("432.44265981702138983107767559824084463956001972037756184044741356")}, std::complex<T>{stof<T>("-231.25138747127559821055310420973414777285696248123250290054323162"),stof<T>("216.22132990851069491553883779912042231978000986018878092022370678")}, std::complex<T>{stof<T>("-231.25138747127559821055310420973414777285696248123250290054323162"),stof<T>("216.22132990851069491553883779912042231978000986018878092022370678")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[67].real()/kbase.W[67].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_686_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_686_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-1019.19420729805664792165950726822624222034828534551616808288915037"),stof<T>("413.59806743649773252620629018170576701896586356026704184743281917")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,36> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W15(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W68(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[67].real()/k.W[67].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_686_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_686_DLogXconstant_part(base_point<T>, kend);
	value += f_4_686_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_686_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_686_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_686_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_686_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_686_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_686_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
