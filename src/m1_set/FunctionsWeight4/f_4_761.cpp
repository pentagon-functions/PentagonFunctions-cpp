/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_761.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_761_abbreviated (const std::array<T,41>& abb) {
T z[155];
z[0] = 4 * abb[34];
z[1] = 4 * abb[32];
z[2] = z[0] + z[1];
z[3] = 10 * abb[33];
z[4] = 12 * abb[30];
z[5] = 5 * abb[26];
z[6] = 2 * abb[29];
z[7] = -11 * abb[28] + z[6];
z[7] = -53 * abb[27] + z[2] + z[3] + z[4] + z[5] + 2 * z[7];
z[7] = abb[1] * z[7];
z[8] = 8 * abb[30];
z[9] = abb[29] + z[8];
z[10] = 11 * abb[27];
z[11] = 2 * abb[26];
z[12] = 4 * abb[28];
z[13] = abb[34] + z[9] + -z[10] + -z[11] + -z[12];
z[13] = abb[3] * z[13];
z[14] = 2 * abb[30];
z[15] = -abb[29] + abb[33];
z[16] = abb[26] + -z[14] + z[15];
z[16] = abb[10] * z[16];
z[17] = abb[35] + abb[36];
z[18] = abb[12] * z[17];
z[19] = -z[16] + z[18];
z[20] = abb[13] * z[17];
z[13] = z[13] + -z[19] + z[20];
z[21] = 5 * abb[27];
z[22] = 6 * abb[28];
z[23] = z[21] + z[22];
z[24] = 6 * abb[33];
z[25] = 4 * abb[26];
z[26] = 3 * abb[30];
z[27] = z[23] + -z[24] + z[25] + z[26];
z[27] = abb[5] * z[27];
z[28] = abb[3] * z[1];
z[7] = z[7] + 2 * z[13] + z[27] + z[28];
z[7] = abb[19] * z[7];
z[13] = 2 * abb[27];
z[27] = abb[28] + z[13];
z[27] = abb[26] + 3 * z[27];
z[29] = abb[31] + -abb[33];
z[30] = abb[34] + z[29];
z[31] = 5 * abb[32];
z[27] = -z[8] + 2 * z[27] + -z[30] + -z[31];
z[27] = abb[9] * z[27];
z[32] = 3 * abb[33];
z[9] = abb[27] + -z[9] + -z[12] + z[32];
z[9] = abb[3] * z[9];
z[33] = z[18] + z[20];
z[34] = abb[11] * z[17];
z[35] = z[33] + z[34];
z[36] = 2 * abb[32];
z[37] = abb[3] * z[36];
z[9] = -z[9] + z[27] + z[35] + -z[37];
z[27] = 3 * abb[29];
z[38] = 3 * abb[26];
z[39] = z[27] + -z[38];
z[40] = 2 * abb[34];
z[41] = 2 * abb[31];
z[42] = z[40] + -z[41];
z[43] = 2 * abb[28];
z[44] = z[42] + -z[43];
z[45] = 12 * abb[32];
z[46] = 11 * abb[33];
z[47] = 28 * abb[27] + -16 * abb[30] + -z[39] + -z[44] + z[45] + -z[46];
z[47] = abb[1] * z[47];
z[48] = -z[1] + z[32];
z[49] = z[13] + -z[43];
z[50] = abb[26] + abb[29];
z[51] = -z[41] + z[48] + -z[49] + z[50];
z[51] = abb[0] * z[51];
z[9] = 2 * z[9] + -z[47] + -z[51];
z[47] = abb[38] * z[9];
z[51] = -abb[29] + z[13];
z[52] = abb[34] + z[26];
z[53] = -z[11] + 3 * z[51] + -z[52];
z[54] = abb[13] * z[53];
z[55] = abb[11] + abb[12] + abb[13];
z[55] = z[1] * z[55];
z[56] = 2 * abb[33];
z[57] = -abb[29] + z[56];
z[58] = 3 * abb[27];
z[59] = abb[34] + z[57] + -z[58];
z[60] = 2 * abb[11];
z[59] = z[59] * z[60];
z[60] = -abb[26] + z[32];
z[61] = -abb[34] + z[26];
z[62] = z[60] + -z[61];
z[62] = abb[12] * z[62];
z[54] = -z[54] + -z[55] + z[59] + z[62];
z[55] = -abb[39] * z[54];
z[59] = abb[29] + z[43];
z[62] = z[13] + z[59];
z[63] = 9 * abb[32];
z[64] = 5 * abb[30];
z[29] = -z[25] + z[29] + -3 * z[62] + z[63] + z[64];
z[65] = abb[37] * z[29];
z[66] = abb[26] + z[43];
z[67] = z[21] + z[66];
z[68] = 6 * abb[30];
z[69] = abb[34] + z[68];
z[67] = -abb[33] + -z[36] + 2 * z[67] + -z[69];
z[67] = abb[19] * z[67];
z[65] = z[65] + z[67];
z[67] = 2 * abb[9];
z[65] = z[65] * z[67];
z[70] = 4 * abb[27];
z[71] = z[12] + z[70];
z[72] = -abb[29] + z[71];
z[73] = 16 * abb[26] + -z[24];
z[74] = 10 * abb[30];
z[72] = abb[32] + z[41] + -3 * z[72] + -z[73] + z[74];
z[75] = -abb[37] * z[72];
z[76] = 15 * abb[26];
z[77] = 4 * abb[33];
z[78] = -abb[29] + z[58];
z[78] = 7 * abb[30] + -z[40] + -z[76] + z[77] + 2 * z[78];
z[78] = abb[19] * z[78];
z[79] = abb[39] * z[17];
z[75] = z[75] + z[78] + -z[79];
z[75] = abb[0] * z[75];
z[78] = abb[27] + abb[28];
z[80] = -abb[26] + abb[33];
z[81] = z[78] + -z[80];
z[81] = abb[5] * z[81];
z[19] = z[19] + 4 * z[81];
z[82] = 6 * abb[32];
z[83] = abb[3] * z[82];
z[84] = z[19] + -z[83];
z[85] = z[5] + z[27];
z[86] = 9 * abb[33];
z[87] = 4 * abb[30];
z[88] = -z[85] + z[86] + -z[87];
z[88] = abb[3] * z[88];
z[89] = 6 * abb[27];
z[90] = z[22] + z[89];
z[91] = -abb[29] + z[90];
z[92] = -8 * abb[26] + abb[31] + z[77] + z[87] + -z[91];
z[92] = abb[0] * z[92];
z[93] = -abb[30] + abb[33] + z[78];
z[94] = 3 * abb[32];
z[95] = abb[31] + z[94];
z[93] = 2 * z[93] + -z[95];
z[96] = abb[9] * z[93];
z[97] = -abb[29] + z[80];
z[98] = abb[2] * z[97];
z[99] = 6 * z[98];
z[88] = z[84] + z[88] + z[92] + z[96] + z[99];
z[92] = prod_pow(abb[17], 2);
z[88] = z[88] * z[92];
z[96] = 7 * abb[27];
z[100] = z[12] + -z[38] + -z[95] + z[96];
z[101] = abb[38] * z[100];
z[102] = z[62] + z[80];
z[103] = -z[41] + z[82];
z[104] = z[102] + -z[103];
z[105] = abb[37] * z[104];
z[106] = z[95] + -z[102];
z[106] = z[92] * z[106];
z[107] = abb[26] + -abb[27];
z[108] = abb[19] * z[107];
z[79] = z[79] + z[101] + z[105] + z[106] + -2 * z[108];
z[79] = abb[4] * z[79];
z[101] = -z[32] + z[38];
z[105] = z[87] + -z[89];
z[106] = z[59] + z[101] + -z[105];
z[106] = abb[0] * z[106];
z[108] = abb[26] + -abb[29];
z[109] = z[32] + -z[108];
z[105] = z[105] + z[109];
z[110] = -z[1] + z[105];
z[111] = abb[1] * z[110];
z[112] = -z[43] + z[50];
z[48] = -z[48] + z[112];
z[48] = abb[4] * z[48];
z[113] = 2 * abb[3];
z[114] = abb[28] + z[108];
z[114] = z[113] * z[114];
z[115] = 3 * z[98];
z[48] = z[48] + z[106] + z[111] + z[114] + z[115];
z[48] = abb[18] * z[48];
z[106] = z[5] + -z[32];
z[111] = 8 * abb[32];
z[114] = -z[90] + z[111];
z[116] = 5 * abb[29];
z[117] = -z[14] + z[106] + -z[114] + z[116];
z[117] = abb[9] * z[117];
z[118] = 5 * abb[33];
z[119] = z[14] + z[118];
z[120] = 7 * abb[26];
z[91] = z[91] + -z[119] + z[120];
z[91] = abb[0] * z[91];
z[121] = -z[34] + z[99];
z[122] = z[20] + z[121];
z[97] = abb[3] * z[97];
z[97] = 3 * z[97] + z[122];
z[91] = z[91] + -2 * z[97] + -z[117];
z[91] = abb[17] * z[91];
z[97] = abb[29] + z[80];
z[114] = z[97] + -z[114];
z[123] = abb[4] * abb[17];
z[114] = z[114] * z[123];
z[124] = -abb[32] + z[80];
z[125] = 8 * abb[6];
z[124] = z[124] * z[125];
z[125] = abb[17] * z[124];
z[91] = z[91] + z[114] + z[125];
z[48] = z[48] + -z[91];
z[48] = abb[18] * z[48];
z[114] = 10 * abb[28];
z[126] = 10 * abb[27] + z[114];
z[111] = 4 * abb[31] + -z[97] + z[111] + -z[126];
z[127] = -abb[37] * z[111];
z[128] = 5 * abb[31];
z[129] = 22 * abb[28] + -z[25] + -z[40] + -z[128];
z[130] = -27 * abb[27] + 15 * abb[32] + -z[15] + -z[129];
z[131] = abb[38] * z[130];
z[127] = z[127] + z[131];
z[127] = abb[8] * z[127];
z[131] = -z[5] + z[6];
z[132] = 7 * abb[32];
z[133] = z[118] + z[131] + -z[132];
z[134] = abb[37] * z[133];
z[135] = z[36] + z[41];
z[136] = z[120] + z[135];
z[137] = 7 * abb[33];
z[138] = z[62] + z[136] + -z[137];
z[139] = -abb[38] * z[138];
z[3] = 10 * abb[26] + abb[29] + -z[3] + z[63];
z[3] = z[3] * z[92];
z[3] = z[3] + z[134] + z[139];
z[3] = abb[6] * z[3];
z[92] = z[5] + z[6] + -z[64];
z[92] = abb[3] * z[92];
z[92] = -z[33] + z[92];
z[92] = -z[28] + 2 * z[92] + -9 * z[98];
z[134] = abb[37] * z[92];
z[3] = z[3] + z[7] + z[47] + z[48] + z[55] + z[65] + z[75] + z[79] + z[88] + z[127] + z[134];
z[7] = 8 * abb[33];
z[47] = abb[29] + z[89];
z[48] = -z[7] + z[41] + z[47] + z[61] + z[82];
z[48] = abb[9] * z[48];
z[55] = 15 * abb[27] + z[22];
z[65] = -abb[34] + -z[50] + z[55] + -z[119];
z[75] = abb[32] + z[65];
z[79] = 2 * abb[1];
z[75] = z[75] * z[79];
z[88] = z[12] + z[21];
z[119] = z[38] + -z[77] + z[88];
z[119] = abb[5] * z[119];
z[75] = z[75] + -2 * z[119] + z[124];
z[127] = z[59] + z[89];
z[127] = 2 * z[127];
z[134] = -abb[34] + z[64];
z[139] = 15 * abb[33];
z[140] = -z[38] + -z[127] + -z[134] + z[139];
z[140] = abb[3] * z[140];
z[141] = z[43] + z[58];
z[142] = abb[31] + abb[33];
z[143] = -abb[26] + -z[36] + 2 * z[141] + -z[142];
z[144] = 2 * abb[8];
z[143] = z[143] * z[144];
z[145] = abb[30] + -abb[34] + z[13] + z[118] + -z[136];
z[145] = abb[0] * z[145];
z[146] = 2 * z[20];
z[147] = z[121] + z[146];
z[148] = -z[94] + z[141];
z[149] = z[108] + -z[148];
z[150] = 2 * abb[4];
z[149] = z[149] * z[150];
z[151] = abb[3] * abb[32];
z[48] = -2 * z[18] + z[48] + -z[75] + z[140] + z[143] + z[145] + z[147] + z[149] + -10 * z[151];
z[48] = abb[15] * z[48];
z[140] = 3 * abb[28];
z[47] = z[47] + z[140];
z[145] = abb[34] + z[1];
z[47] = 2 * z[47] + -z[64] + z[101] + -z[145];
z[47] = abb[9] * z[47];
z[64] = -abb[34] + -z[64] + -z[86] + z[120] + z[127];
z[64] = abb[3] * z[64];
z[73] = -abb[29] + -9 * abb[30] + abb[34] + z[22] + z[70] + z[73];
z[73] = abb[0] * z[73];
z[101] = z[43] + -z[82] + z[109];
z[101] = abb[4] * z[101];
z[127] = z[34] + z[146];
z[64] = z[28] + -z[47] + z[64] + z[73] + z[75] + -z[99] + z[101] + -z[127];
z[64] = abb[18] * z[64];
z[44] = z[44] + -z[108];
z[73] = 14 * abb[27];
z[75] = z[44] + -z[73] + -z[82] + z[87] + z[137];
z[75] = abb[9] * z[75];
z[101] = -abb[28] + abb[33];
z[146] = 17 * abb[27];
z[149] = -z[38] + -z[87] + 12 * z[101] + -z[146];
z[149] = abb[5] * z[149];
z[152] = -8 * abb[27] + -z[12] + z[40];
z[153] = -abb[29] + z[14];
z[136] = -z[32] + z[136] + z[152] + -z[153];
z[136] = abb[0] * z[136];
z[154] = 15 * abb[28] + -z[6];
z[2] = 69 * abb[27] + -18 * abb[33] + -z[2] + -z[74] + -z[120] + 2 * z[154];
z[2] = abb[1] * z[2];
z[65] = abb[3] * z[65];
z[20] = z[20] + z[34];
z[65] = -z[20] + z[65];
z[70] = abb[33] + -z[70] + z[112];
z[70] = abb[4] * z[70];
z[2] = z[2] + z[37] + 2 * z[65] + z[70] + z[75] + z[136] + -z[143] + z[149];
z[2] = abb[16] * z[2];
z[2] = z[2] + z[48] + z[64] + z[91];
z[48] = z[14] + -z[38];
z[64] = 39 * abb[27];
z[65] = -9 * abb[28] + abb[29];
z[0] = 16 * abb[33] + z[0] + -z[1] + -z[48] + -z[64] + 2 * z[65];
z[0] = abb[1] * z[0];
z[65] = -9 * abb[27] + -z[12] + -z[50] + z[52] + z[56];
z[65] = abb[3] * z[65];
z[65] = z[18] + z[65] + z[127];
z[12] = -z[12] + z[77];
z[74] = -z[12] + -z[21] + z[38] + z[74];
z[74] = abb[5] * z[74];
z[21] = z[21] + z[59];
z[21] = abb[31] + -abb[32] + 2 * z[21] + z[38] + -z[52] + -z[118];
z[21] = z[21] * z[67];
z[30] = -abb[28] + abb[30] + -z[30] + z[58] + z[131];
z[75] = 2 * abb[0];
z[30] = z[30] * z[75];
z[107] = -abb[31] + z[107];
z[6] = -z[6] + z[94] + z[107];
z[6] = z[6] * z[150];
z[57] = -abb[32] + z[57];
z[112] = -z[11] + z[57];
z[127] = 2 * abb[6];
z[112] = z[112] * z[127];
z[0] = z[0] + z[6] + z[21] + z[28] + z[30] + 2 * z[65] + z[74] + z[99] + z[112];
z[0] = abb[14] * z[0];
z[0] = z[0] + 2 * z[2];
z[0] = abb[14] * z[0];
z[2] = -z[41] + z[137];
z[6] = z[27] + -z[87];
z[21] = -13 * abb[26] + -z[1] + z[2] + -z[6] + -z[152];
z[21] = abb[0] * z[21];
z[30] = 8 * abb[28];
z[65] = abb[29] + -z[30];
z[65] = 12 * abb[26] + -83 * abb[27] + 14 * abb[30] + 16 * abb[32] + z[40] + z[41] + 5 * z[65] + z[139];
z[65] = abb[1] * z[65];
z[74] = abb[6] * z[138];
z[99] = -20 * abb[27] + abb[34] + z[46] + -z[114] + -z[153];
z[99] = abb[3] * z[99];
z[99] = -z[18] + z[99];
z[48] = -z[48] + z[64] + -20 * z[101];
z[48] = abb[5] * z[48];
z[64] = 41 * abb[27] + -13 * abb[33] + z[94] + -z[116] + z[129];
z[64] = abb[8] * z[64];
z[93] = -z[67] * z[93];
z[101] = -abb[32] + -z[107];
z[101] = abb[4] * z[101];
z[21] = z[21] + z[48] + z[64] + z[65] + z[74] + -z[83] + z[93] + 2 * z[99] + z[101];
z[21] = abb[16] * z[21];
z[48] = 14 * abb[28];
z[64] = -z[11] + z[48];
z[65] = abb[29] + abb[33];
z[93] = -z[64] + z[65] + z[128] + z[132] + -z[146];
z[93] = abb[8] * z[93];
z[24] = abb[26] + -z[24] + z[87] + z[88];
z[24] = abb[3] * z[24];
z[24] = z[24] + z[37];
z[18] = z[18] + z[24] + -z[119];
z[37] = abb[29] + z[38];
z[99] = z[37] + z[90];
z[41] = -10 * abb[32] + -z[8] + -z[41] + z[99] + z[118];
z[41] = abb[9] * z[41];
z[101] = -z[87] + z[135];
z[107] = z[73] + z[114];
z[114] = -z[37] + -z[101] + z[107] + -z[118];
z[114] = abb[1] * z[114];
z[129] = -abb[33] + z[11];
z[95] = z[58] + z[59] + -z[95] + -z[129];
z[95] = abb[4] * z[95];
z[129] = -abb[30] + z[129];
z[129] = abb[31] + abb[32] + 2 * z[129];
z[129] = z[75] * z[129];
z[18] = 2 * z[18] + z[41] + -z[74] + z[93] + z[95] + z[114] + z[129];
z[18] = abb[15] * z[18];
z[41] = -18 * abb[27] + z[8] + -z[22] + z[40] + z[109];
z[41] = abb[9] * z[41];
z[93] = abb[3] * z[105];
z[20] = z[20] + -z[28] + z[93] + z[119];
z[49] = -9 * abb[26] + z[15] + z[36] + z[49] + z[87];
z[49] = abb[0] * z[49];
z[93] = abb[29] + -z[140];
z[7] = -21 * abb[27] + z[7] + -z[31] + z[69] + 2 * z[93];
z[7] = z[7] * z[79];
z[31] = -z[80] + -z[88] + z[145];
z[31] = z[31] * z[144];
z[7] = z[7] + 2 * z[20] + z[31] + -z[41] + z[49] + -z[70];
z[7] = abb[18] * z[7];
z[7] = z[7] + z[18];
z[7] = 2 * z[7] + z[21];
z[7] = abb[16] * z[7];
z[18] = -z[5] + -z[22] + z[42] + z[46] + -z[73] + -z[116];
z[18] = abb[0] * z[18];
z[20] = abb[31] + -z[32] + -z[37] + z[68] + -z[71] + z[132];
z[20] = abb[9] * z[20];
z[21] = -abb[27] + z[43];
z[37] = -abb[34] + -z[21] + -z[87] + z[109];
z[37] = abb[3] * z[37];
z[19] = z[19] + z[20] + z[37] + -z[115] + -z[151];
z[20] = z[27] + z[43];
z[5] = abb[31] + -z[5] + z[10] + 2 * z[20] + -z[56] + -z[63];
z[5] = abb[4] * z[5];
z[20] = -16 * abb[27] + -z[1] + z[8] + z[44] + z[118];
z[20] = abb[1] * z[20];
z[37] = z[40] + z[46];
z[40] = -17 * abb[32] + z[37] + 3 * z[59] + -z[96] + -z[128];
z[40] = abb[8] * z[40];
z[5] = z[5] + z[18] + 2 * z[19] + -z[20] + z[40] + z[74];
z[5] = abb[15] * z[5];
z[18] = z[79] * z[110];
z[18] = z[18] + -z[28] + z[31];
z[19] = z[47] + z[147];
z[31] = -abb[29] + z[43];
z[31] = -2 * z[31] + -z[60] + -z[61];
z[31] = abb[3] * z[31];
z[40] = -z[36] + z[59] + z[61];
z[40] = abb[0] * z[40];
z[44] = -z[36] + z[62] + -z[80];
z[44] = abb[4] * z[44];
z[31] = -z[18] + -z[19] + z[31] + z[40] + z[44];
z[31] = abb[18] * z[31];
z[40] = abb[33] + z[38];
z[44] = -z[1] + z[40] + z[62] + -z[87];
z[44] = abb[9] * z[44];
z[14] = z[14] + -z[60];
z[14] = z[14] * z[113];
z[14] = z[14] + -z[84];
z[47] = abb[29] + z[90];
z[49] = z[47] + z[120] + -z[137];
z[49] = abb[0] * z[49];
z[14] = 2 * z[14] + z[44] + z[49];
z[14] = abb[17] * z[14];
z[44] = z[1] + -z[102];
z[44] = z[44] * z[123];
z[14] = z[14] + z[31] + z[44] + z[125];
z[5] = z[5] + 2 * z[14];
z[5] = abb[15] * z[5];
z[8] = -z[8] + z[46] + -z[99];
z[8] = abb[3] * z[8];
z[8] = z[8] + -z[16] + -z[28] + z[74] + 8 * z[81];
z[2] = 18 * abb[32] + -z[2] + z[4] + -z[85] + -z[126];
z[2] = abb[9] * z[2];
z[4] = z[46] + -z[47] + -z[76] + -z[101];
z[4] = abb[0] * z[4];
z[14] = -z[102] + z[135];
z[14] = abb[4] * z[14];
z[16] = -z[111] * z[144];
z[28] = -abb[30] + abb[32] + -3 * z[78] + z[142];
z[28] = abb[1] * z[28];
z[2] = z[2] + z[4] + 2 * z[8] + z[14] + z[16] + 4 * z[28];
z[2] = abb[21] * z[2];
z[0] = z[0] + z[2] + z[5] + z[7];
z[2] = -22 * abb[27] + -11 * abb[29] + z[48];
z[2] = abb[33] + (T(1) / T(3)) * z[2] + -z[36] + z[87] + z[120];
z[2] = abb[0] * z[2];
z[4] = -34 * abb[27] + -z[30] + z[108] + z[118];
z[5] = abb[31] + -abb[34];
z[7] = abb[32] * (T(4) / T(3));
z[4] = (T(1) / T(3)) * z[4] + (T(-8) / T(3)) * z[5] + z[7] + z[87];
z[4] = abb[1] * z[4];
z[5] = abb[28] + abb[27] * (T(-4) / T(3)) + (T(7) / T(3)) * z[50] + -z[77] + z[87];
z[5] = abb[3] * z[5];
z[5] = z[5] + (T(4) / T(3)) * z[34];
z[8] = abb[26] + abb[33];
z[14] = abb[31] * (T(4) / T(3));
z[16] = 12 * abb[27] + abb[32] * (T(-16) / T(3)) + abb[28] * (T(14) / T(3)) + (T(-11) / T(3)) * z[8] + z[14] + z[27];
z[16] = abb[4] * z[16];
z[28] = -z[80] + z[94];
z[30] = -z[43] + -z[51];
z[14] = -z[14] + z[28] + (T(2) / T(3)) * z[30];
z[14] = z[14] * z[127];
z[7] = -abb[30] + abb[26] * (T(-2) / T(3)) + abb[34] * (T(-1) / T(3)) + z[7] + z[51];
z[7] = abb[9] * z[7];
z[30] = abb[32] * (T(-40) / T(3)) + abb[27] * (T(-34) / T(3)) + abb[31] * (T(2) / T(3)) + abb[34] * (T(4) / T(3)) + abb[29] * (T(13) / T(3)) + -z[66] + z[86];
z[30] = z[30] * z[144];
z[2] = z[2] + z[4] + 2 * z[5] + 8 * z[7] + z[14] + z[16] + z[30] + -7 * z[98] + (T(4) / T(3)) * z[151];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[4] = z[71] + -z[82];
z[5] = abb[33] + -z[4] + -z[25] + z[26] + -z[27];
z[5] = abb[9] * z[5];
z[6] = abb[26] + z[6] + z[32];
z[6] = abb[3] * z[6];
z[4] = z[4] + z[97];
z[4] = abb[4] * z[4];
z[7] = -abb[30] + z[15];
z[7] = abb[0] * z[7];
z[4] = z[4] + z[5] + z[6] + z[7] + z[84] + -z[122];
z[4] = abb[20] * z[4];
z[0] = abb[40] + 8 * z[0] + 4 * z[2] + 16 * z[3] + 32 * z[4];
z[2] = -abb[29] + z[141];
z[3] = -2 * z[2] + -z[36] + z[118] + -z[120] + z[134];
z[3] = abb[0] * z[3];
z[4] = -abb[28] + -abb[29];
z[4] = 4 * z[4] + -z[61] + -z[106];
z[4] = abb[3] * z[4];
z[5] = -z[1] + -z[53];
z[5] = abb[9] * z[5];
z[6] = z[28] + -2 * z[78];
z[6] = z[6] * z[150];
z[3] = z[3] + z[4] + z[5] + z[6] + -z[18] + z[121] + -z[124];
z[3] = abb[18] * z[3];
z[4] = 3 * abb[31];
z[5] = -z[4] + z[15] + z[23] + -z[94];
z[5] = abb[8] * z[5];
z[6] = abb[29] + -z[21];
z[6] = 2 * z[6] + -z[52] + -z[60];
z[6] = abb[3] * z[6];
z[7] = -z[8] + -z[13] + z[52];
z[7] = abb[0] * z[7];
z[8] = abb[6] * z[104];
z[14] = abb[31] + -z[15] + z[148];
z[14] = abb[4] * z[14];
z[5] = z[5] + z[6] + z[7] + z[8] + z[14] + -z[19] + -z[20] + z[83];
z[5] = abb[15] * z[5];
z[6] = -abb[29] + z[42] + -z[45] + -z[87] + -z[106] + z[107];
z[6] = abb[1] * z[6];
z[4] = -29 * abb[27] + z[4] + z[37] + -z[64] + z[116] + -z[132];
z[4] = abb[8] * z[4];
z[7] = z[10] + -z[12] + -z[26];
z[7] = abb[5] * z[7];
z[8] = -z[7] + z[24] + z[35];
z[10] = -abb[30] + z[38] + -z[57];
z[10] = z[10] * z[75];
z[2] = abb[32] + z[2] + -z[142];
z[2] = abb[4] * z[2];
z[2] = z[2] + z[4] + z[6] + 2 * z[8] + z[10] + -z[41] + -z[74];
z[2] = abb[16] * z[2];
z[4] = z[13] + z[22] + -z[36] + z[40] + -z[116];
z[4] = abb[0] * z[4];
z[1] = z[1] + z[11] + -z[55] + z[65] + z[68];
z[1] = z[1] * z[79];
z[6] = -abb[28] + z[50] + -z[58];
z[6] = -abb[30] + 2 * z[6] + z[32] + -z[94];
z[6] = abb[3] * z[6];
z[6] = z[6] + z[7] + -z[33] + -z[115];
z[7] = -abb[33] + z[39] + z[43] + z[89] + -z[103];
z[7] = abb[4] * z[7];
z[1] = z[1] + z[4] + 2 * z[6] + z[7] + -z[112] + -z[117] + z[143];
z[1] = abb[14] * z[1];
z[1] = z[1] + z[2] + z[3] + z[5] + -z[91];
z[1] = 16 * z[1];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = z[29] * z[67];
z[3] = -abb[0] * z[72];
z[4] = abb[4] * z[104];
z[5] = -abb[8] * z[111];
z[6] = abb[6] * z[133];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + z[92];
z[3] = 16 * abb[22];
z[2] = z[2] * z[3];
z[3] = abb[4] * z[100];
z[4] = abb[8] * z[130];
z[3] = z[3] + z[4] + z[9] + -z[74];
z[3] = abb[23] * z[3];
z[4] = -abb[0] + abb[4];
z[4] = z[4] * z[17];
z[4] = z[4] + -z[54];
z[4] = abb[24] * z[4];
z[3] = z[3] + z[4];
z[1] = abb[25] + z[1] + z[2] + 16 * z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_761_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-191.21682248706553021816444171852892104127969930551520122303378415"),stof<T>("-660.21845997384139668407152712061173108787866529627443570782516138")}, std::complex<T>{stof<T>("645.20573904178724213717977316167084663979657657691845366980802708"),stof<T>("-632.78101099455932904971140243659718810274901162970042621786898134")}, std::complex<T>{stof<T>("-9.38546892057048824871477036924944354573898708342708169869792646"),stof<T>("-710.07808383698572007188090399456070734260709890752520642982804619")}, std::complex<T>{stof<T>("-33.55228376347108296022515570685266652524788832069009853942191125"),stof<T>("181.16355802149611551525174690970639329282882549739483960085179445")}, std::complex<T>{stof<T>("-342.89429552486225676837777138155633814247562511763496259513630696"),stof<T>("533.21345230474893709846996862657580645116788889375940899166051055")}, std::complex<T>{stof<T>("-270.40812934864965788610966730609289434038979249509029772450550151"),stof<T>("80.46279930413876803659316948263135880634823535284254554625251778")}, std::complex<T>{stof<T>("433.82597198312117249440192381303003541315690713640284951296120166"),stof<T>("398.74379168468476795660393066615054984657734893259757477851814798")}, std::complex<T>{stof<T>("204.47436925789466550232418233215347041261728727977161292316871418"),stof<T>("178.27614025103627907725033582743688054044236526133231350659219546")}, std::complex<T>{stof<T>("-324.95445920832460890167651276298850141439752651696698447350457661"),stof<T>("-128.56820542437021051381827889136447514358967613664206700238660948")}, std::complex<T>{stof<T>("108.301512400484057380243968185491882489388423256931697860318210292"),stof<T>("43.192173362544334701430238150675116747429604859623394047776250175")}, std::complex<T>{stof<T>("108.301512400484057380243968185491882489388423256931697860318210292"),stof<T>("43.192173362544334701430238150675116747429604859623394047776250175")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[52].real()/kbase.W[52].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[125].real()/kbase.W[125].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_761_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_761_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-2 * (-v[3] + v[5]) * (-40 + -12 * v[0] + 4 * v[1] + -13 * v[3] + -7 * v[5] + 10 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (-16 * (-1 + 5 * m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return (abb[29] + abb[30] + -abb[33]) * (t * c[0] + c[1]);
	}
	{
T z[4];
z[0] = abb[14] + -abb[18];
z[1] = abb[29] + abb[30] + -abb[33];
z[0] = z[0] * z[1];
z[2] = abb[15] * z[1];
z[2] = z[0] + -2 * z[2];
z[2] = abb[15] * z[2];
z[3] = 2 * z[1];
z[3] = abb[17] * z[3];
z[0] = -z[0] + z[3];
z[0] = abb[17] * z[0];
z[1] = -abb[20] * z[1];
z[0] = z[0] + z[1] + z[2];
return 32 * abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_761_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-4 * m1_set::bc<T>[0] * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (16 * m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return (abb[29] + abb[30] + -abb[33]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[15] + abb[17];
z[1] = abb[29] + abb[30] + -abb[33];
return 32 * abb[7] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_761_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-157.09953702676930594889929506353006657765057897592479842975947781"),stof<T>("869.63480651457251238645157746989928565694847862560056336121348054")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,41> abb = {dl[0], dl[1], dl[5], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W60(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_2_3(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_24_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[52].real()/k.W[52].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[125].real()/k.W[125].real()), f_2_4_re(k), f_2_6_re(k), f_2_24_re(k), T{0}};
abb[25] = SpDLog_f_4_761_W_16_Im(t, path, abb);
abb[40] = SpDLog_f_4_761_W_16_Re(t, path, abb);

                    
            return f_4_761_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_761_DLogXconstant_part(base_point<T>, kend);
	value += f_4_761_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_761_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_761_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_761_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_761_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_761_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_761_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
