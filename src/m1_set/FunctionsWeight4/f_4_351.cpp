/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_351.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_351_abbreviated (const std::array<T,57>& abb) {
T z[82];
z[0] = -abb[51] + abb[52];
z[1] = abb[48] + abb[50];
z[2] = abb[47] + z[1];
z[3] = z[0] + z[2];
z[3] = abb[4] * z[3];
z[4] = (T(1) / T(2)) * z[3];
z[5] = abb[42] + abb[43] + abb[44] + -abb[45] + -abb[46];
z[6] = (T(1) / T(2)) * z[5];
z[7] = abb[21] * z[6];
z[8] = abb[23] * z[6];
z[9] = z[7] + z[8];
z[10] = z[4] + -z[9];
z[11] = abb[47] + abb[49];
z[12] = -z[0] + z[11];
z[13] = abb[2] * z[12];
z[14] = abb[12] * z[2];
z[15] = z[10] + -z[13] + -z[14];
z[16] = z[1] + z[12];
z[17] = abb[7] * z[16];
z[18] = abb[47] * (T(1) / T(2));
z[19] = abb[49] + z[18];
z[20] = z[0] + -z[19];
z[21] = -abb[8] * z[20];
z[22] = -abb[15] * z[20];
z[23] = abb[53] * (T(1) / T(2));
z[24] = abb[19] * z[23];
z[22] = z[22] + z[24];
z[24] = abb[25] * z[6];
z[24] = z[22] + z[24];
z[25] = abb[24] * z[6];
z[26] = z[24] + -z[25];
z[17] = z[15] + z[17] + -z[21] + z[26];
z[27] = abb[22] * z[6];
z[6] = abb[20] * z[6];
z[28] = z[6] + z[27];
z[29] = (T(-1) / T(2)) * z[0];
z[30] = z[19] + z[29];
z[31] = (T(1) / T(2)) * z[1];
z[32] = z[30] + z[31];
z[33] = abb[13] * z[32];
z[34] = z[28] + z[33];
z[35] = -z[17] + z[34];
z[35] = abb[28] * z[35];
z[36] = abb[5] * z[32];
z[37] = -z[9] + z[36];
z[38] = abb[8] * z[32];
z[16] = abb[10] * z[16];
z[38] = -z[16] + z[38];
z[39] = z[1] + z[11];
z[40] = abb[1] * z[39];
z[41] = z[14] + -z[40];
z[42] = z[25] + z[34] + -z[37] + z[38] + z[41];
z[42] = abb[32] * z[42];
z[43] = abb[7] * z[32];
z[44] = z[24] + z[43];
z[45] = z[25] + z[27];
z[46] = abb[6] * z[32];
z[47] = -abb[49] + z[0];
z[48] = abb[8] * z[47];
z[48] = -z[13] + z[44] + -z[45] + -z[46] + z[48];
z[48] = abb[31] * z[48];
z[49] = abb[17] * z[23];
z[50] = z[13] + z[49];
z[51] = abb[3] * z[18];
z[44] = z[44] + -z[50] + z[51];
z[12] = abb[8] * z[12];
z[52] = z[12] + z[25] + -z[44];
z[53] = z[29] + z[31];
z[54] = z[11] + z[53];
z[55] = abb[0] * z[54];
z[56] = z[34] + -z[55];
z[57] = z[52] + z[56];
z[57] = abb[33] * z[57];
z[58] = abb[28] * z[55];
z[59] = abb[16] * z[23];
z[60] = abb[28] * z[59];
z[35] = z[35] + z[42] + z[48] + -z[57] + -z[58] + -z[60];
z[42] = -z[21] + z[44];
z[44] = -z[9] + z[59];
z[48] = -z[36] + z[42] + -z[44];
z[61] = abb[30] * (T(1) / T(2));
z[62] = z[48] * z[61];
z[2] = -z[0] + z[2];
z[63] = abb[7] * (T(1) / T(2));
z[64] = z[2] * z[63];
z[38] = z[38] + -z[64];
z[10] = z[6] + -z[10] + z[33] + z[38];
z[65] = (T(1) / T(2)) * z[40];
z[66] = (T(1) / T(2)) * z[46];
z[10] = (T(1) / T(2)) * z[10] + z[14] + -z[65] + -z[66];
z[67] = -abb[29] * z[10];
z[35] = (T(1) / T(2)) * z[35] + -z[62] + z[67];
z[35] = m1_set::bc<T>[0] * z[35];
z[17] = z[17] + -z[56] + z[59];
z[17] = (T(1) / T(2)) * z[17];
z[67] = -abb[39] * z[17];
z[68] = -abb[40] * z[10];
z[48] = abb[41] * z[48];
z[35] = z[35] + (T(-1) / T(2)) * z[48] + z[67] + z[68];
z[35] = (T(1) / T(8)) * z[35];
z[39] = abb[3] * z[39];
z[19] = z[1] + z[19];
z[48] = abb[14] * z[19];
z[67] = abb[9] * z[11];
z[39] = z[39] + -z[48] + -z[67];
z[68] = -z[16] + z[39];
z[69] = abb[13] * z[30];
z[70] = (T(1) / T(2)) * z[55];
z[69] = z[69] + -z[70];
z[71] = (T(1) / T(4)) * z[5];
z[72] = abb[22] * z[71];
z[73] = abb[53] * (T(1) / T(4));
z[74] = abb[16] * z[73];
z[72] = z[72] + -z[74];
z[75] = abb[49] + z[1];
z[29] = abb[47] + z[29] + (T(1) / T(2)) * z[75];
z[29] = abb[7] * z[29];
z[53] = abb[49] + z[53];
z[76] = abb[8] * (T(1) / T(2));
z[77] = -z[53] * z[76];
z[6] = (T(1) / T(4)) * z[3] + -z[6] + -z[9] + (T(-3) / T(2)) * z[14] + z[29] + -z[65] + z[66] + (T(-1) / T(2)) * z[68] + -z[69] + -z[72] + z[77];
z[6] = abb[29] * z[6];
z[29] = z[30] + -z[31];
z[29] = abb[13] * z[29];
z[28] = z[28] + z[29];
z[29] = abb[47] * (T(3) / T(2));
z[30] = -z[29] + -z[53];
z[30] = abb[7] * z[30];
z[31] = abb[8] * z[18];
z[9] = z[9] + z[14] + z[28] + z[30] + -z[31] + z[39];
z[30] = -z[40] + z[70];
z[9] = (T(1) / T(2)) * z[9] + -z[30];
z[9] = abb[28] * z[9];
z[65] = abb[32] * z[10];
z[68] = abb[11] * z[75];
z[77] = -z[40] + z[43] + z[48] + -z[68];
z[44] = -z[31] + z[44] + -z[56] + z[77];
z[56] = abb[31] * (T(1) / T(2));
z[78] = -z[44] * z[56];
z[79] = abb[28] * z[74];
z[6] = (T(1) / T(2)) * z[6] + z[9] + z[65] + z[78] + -z[79];
z[6] = abb[29] * z[6];
z[9] = -z[13] + z[22] + z[43] + z[51];
z[43] = (T(1) / T(2)) * z[36];
z[65] = abb[21] * z[71];
z[78] = abb[23] * z[71];
z[65] = z[65] + z[78];
z[75] = -z[18] + z[75];
z[75] = z[75] * z[76];
z[80] = abb[17] * z[73];
z[81] = abb[25] * z[71];
z[9] = (T(1) / T(2)) * z[9] + -z[16] + z[41] + -z[43] + z[55] + z[65] + z[74] + z[75] + -z[80] + z[81];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[16] = z[4] + -z[13];
z[25] = -z[25] + z[36];
z[32] = abb[3] * z[32];
z[32] = z[32] + -z[67];
z[36] = z[16] + -z[25] + z[28] + z[32] + z[64];
z[41] = -abb[32] * z[36];
z[4] = -z[4] + z[67];
z[64] = abb[3] * z[53];
z[67] = abb[20] * z[5];
z[75] = -abb[7] * abb[49];
z[24] = -z[4] + z[12] + -z[24] + z[64] + z[67] + z[75];
z[24] = (T(1) / T(2)) * z[24] + -z[43] + z[45] + z[69] + z[80];
z[24] = abb[33] * z[24];
z[43] = -z[34] + -z[52];
z[43] = abb[28] * z[43];
z[24] = z[24] + z[41] + z[43] + z[58];
z[24] = abb[33] * z[24];
z[41] = -abb[47] + z[1];
z[43] = z[0] + -z[41];
z[43] = z[43] * z[63];
z[1] = abb[13] * z[1];
z[12] = z[1] + z[12] + -z[16] + -z[26] + -z[39] + z[43];
z[12] = (T(1) / T(2)) * z[12] + -z[40];
z[16] = prod_pow(abb[28], 2);
z[12] = z[12] * z[16];
z[26] = abb[36] * z[36];
z[36] = z[46] + -z[49];
z[39] = abb[21] + abb[22] + abb[23];
z[39] = (T(-1) / T(2)) * z[39];
z[39] = z[5] * z[39];
z[39] = z[31] + -z[36] + z[39] + -z[51] + z[77];
z[39] = abb[35] * z[39];
z[1] = -z[1] + z[15] + z[32] + -z[38] + z[40];
z[15] = abb[37] * z[1];
z[32] = -z[37] + z[42];
z[32] = abb[56] * z[32];
z[12] = z[12] + z[15] + z[24] + z[26] + z[32] + z[39];
z[0] = z[0] + z[41];
z[15] = abb[3] * (T(1) / T(2));
z[15] = z[0] * z[15];
z[22] = z[15] + -z[22];
z[24] = z[22] + -z[48];
z[26] = abb[24] * z[71];
z[26] = z[26] + z[50] + -z[81];
z[29] = -z[29] + z[47];
z[29] = z[29] * z[76];
z[32] = abb[7] * z[54];
z[24] = (T(-1) / T(2)) * z[24] + -z[26] + z[29] + z[32] + -z[34] + -z[40] + -z[65];
z[24] = abb[28] * z[24];
z[24] = z[24] + z[57] + (T(3) / T(2)) * z[58] + z[79];
z[29] = -z[18] + z[53];
z[29] = abb[7] * z[29];
z[7] = -z[7] + z[13] + z[15] + z[29] + z[48];
z[13] = z[68] + z[70];
z[15] = abb[8] * abb[47];
z[7] = (T(1) / T(2)) * z[7] + -z[13] + (T(1) / T(4)) * z[15] + z[49] + -z[66] + -z[72] + -z[78];
z[7] = abb[31] * z[7];
z[29] = -z[33] + z[55];
z[32] = abb[3] * abb[47];
z[33] = z[32] + -z[67];
z[33] = -z[15] + z[29] + (T(1) / T(2)) * z[33] + z[36];
z[33] = abb[32] * z[33];
z[7] = z[7] + z[24] + z[33];
z[7] = z[7] * z[56];
z[33] = abb[21] * z[5];
z[34] = z[33] + z[67];
z[8] = z[8] + -z[25] + z[27] + -z[29] + z[31] + (T(1) / T(2)) * z[34] + -z[59];
z[8] = abb[32] * z[8];
z[21] = z[21] + z[22] + z[48];
z[18] = abb[7] * z[18];
z[13] = z[13] + z[18] + (T(-1) / T(2)) * z[21] + -z[26] + z[65] + -z[74];
z[13] = abb[31] * z[13];
z[18] = abb[29] * z[44];
z[8] = z[8] + z[13] + z[18] + -z[24] + -z[62];
z[8] = z[8] * z[61];
z[2] = abb[7] * z[2];
z[13] = -abb[23] * z[5];
z[18] = abb[17] * abb[53];
z[2] = z[2] + z[3] + z[13] + z[15] + z[18] + -z[32] + -z[33];
z[2] = (T(1) / T(2)) * z[2] + -z[14];
z[2] = abb[28] * z[2];
z[3] = abb[32] * (T(1) / T(2));
z[1] = z[1] * z[3];
z[1] = z[1] + z[2] + z[60];
z[1] = z[1] * z[3];
z[2] = abb[38] * z[19];
z[3] = z[16] * z[23];
z[13] = abb[28] + abb[31];
z[13] = abb[53] * z[13];
z[14] = z[13] * z[56];
z[15] = abb[29] * z[23];
z[15] = -z[13] + z[15];
z[15] = abb[29] * z[15];
z[16] = abb[29] * abb[53];
z[13] = (T(-1) / T(2)) * z[13] + z[16];
z[13] = abb[30] * z[13];
z[16] = abb[35] * abb[53];
z[3] = z[2] + z[3] + z[13] + z[14] + z[15] + z[16];
z[3] = abb[18] * z[3];
z[13] = abb[35] + -abb[56];
z[13] = abb[53] * z[13];
z[2] = z[2] + z[13];
z[2] = abb[16] * z[2];
z[2] = z[2] + z[3];
z[3] = -abb[3] * z[19];
z[11] = abb[7] * z[11];
z[3] = z[3] + z[4] + z[11] + -z[28] + z[48] + -z[49];
z[4] = abb[18] * z[73];
z[3] = (T(1) / T(2)) * z[3] + z[4] + z[30];
z[3] = abb[34] * z[3];
z[4] = abb[54] * z[17];
z[10] = abb[55] * z[10];
z[11] = -abb[19] * z[20];
z[13] = abb[14] + abb[15];
z[13] = z[13] * z[23];
z[14] = abb[3] + -abb[7];
z[14] = abb[53] * z[14];
z[11] = z[11] + z[13] + z[14];
z[0] = abb[17] * z[0];
z[5] = abb[26] * z[5];
z[0] = z[0] + z[5];
z[5] = -abb[27] + abb[0] * (T(1) / T(8)) + abb[8] * (T(3) / T(8));
z[5] = abb[53] * z[5];
z[0] = (T(1) / T(8)) * z[0] + z[5] + (T(1) / T(4)) * z[11];
z[0] = abb[38] * z[0];
z[0] = z[0] + z[1] + (T(1) / T(4)) * z[2] + z[3] + z[4] + z[6] + z[7] + z[8] + (T(1) / T(6)) * z[9] + z[10] + (T(1) / T(2)) * z[12];
z[0] = (T(1) / T(8)) * z[0];
return {z[35], z[0]};
}


template <typename T> std::complex<T> f_4_351_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.004605007544597006482579738436334038473114117462899111926165312795"),stof<T>("-0.029539741934420897032474281210353464953804536315967589131039813275")}, std::complex<T>{stof<T>("0.098430801155425983306243512074946629640624871321852072834017440721"),stof<T>("0.098781766478966211316103650373724145600679262734623516910986311868")}, std::complex<T>{stof<T>("-0.001246092036320680514270926992174916425776621650505652610148283151"),stof<T>("-0.015366031495232227791076722926186704984307155009487936332876107723")}, std::complex<T>{stof<T>("0.098430801155425983306243512074946629640624871321852072834017440721"),stof<T>("0.098781766478966211316103650373724145600679262734623516910986311868")}, std::complex<T>{stof<T>("-0.0035556760155247333333500450720833143010502504038358939258453192"),stof<T>("-0.022243492418331860176737747681213419921989925576052258734573125343")}, std::complex<T>{stof<T>("0.0035556760155247333333500450720833143010502504038358939258453192"),stof<T>("0.022243492418331860176737747681213419921989925576052258734573125343")}, std::complex<T>{stof<T>("0.080610197645943882040336503662042835406726712312518261807545420308"),stof<T>("0.108063422522188153157371941282165206955170803724137639419870031113")}};
	
	std::vector<C> intdlogs = {rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_351_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_351_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.1048986139193034620440683042478487250690525310754507657177961592"),stof<T>("0.10270139769601509863597051635081703607726541005692490421616384826")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k)};

                    
            return f_4_351_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_351_DLogXconstant_part(base_point<T>, kend);
	value += f_4_351_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_351_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_351_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_351_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_351_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_351_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_351_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
