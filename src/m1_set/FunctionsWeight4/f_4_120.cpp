/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_120.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_120_abbreviated (const std::array<T,58>& abb) {
T z[92];
z[0] = abb[32] + -abb[33];
z[1] = m1_set::bc<T>[0] * z[0];
z[1] = abb[41] + z[1];
z[2] = abb[7] * (T(1) / T(2));
z[1] = z[1] * z[2];
z[3] = abb[31] * (T(1) / T(2));
z[4] = abb[34] * (T(1) / T(2));
z[5] = z[3] + z[4];
z[6] = -abb[32] + z[5];
z[7] = m1_set::bc<T>[0] * z[6];
z[7] = -abb[41] + z[7];
z[8] = abb[15] * (T(1) / T(2));
z[9] = z[7] * z[8];
z[1] = z[1] + z[9];
z[9] = abb[33] * (T(1) / T(2));
z[10] = abb[32] * (T(1) / T(2));
z[11] = z[9] + -z[10];
z[12] = -abb[35] + z[4];
z[13] = abb[30] * (T(1) / T(2));
z[14] = z[12] + z[13];
z[15] = z[11] + z[14];
z[15] = m1_set::bc<T>[0] * z[15];
z[16] = abb[41] * (T(1) / T(2));
z[15] = z[15] + -z[16];
z[15] = abb[6] * z[15];
z[17] = abb[34] * (T(1) / T(4));
z[9] = -abb[35] + z[9] + z[17];
z[18] = z[9] + -z[10];
z[19] = abb[31] * (T(1) / T(4));
z[20] = z[13] + z[18] + z[19];
z[20] = m1_set::bc<T>[0] * z[20];
z[16] = -z[16] + z[20];
z[16] = abb[5] * z[16];
z[20] = -abb[30] + abb[32];
z[21] = -abb[33] + abb[35];
z[22] = z[20] + z[21];
z[23] = m1_set::bc<T>[0] * z[22];
z[23] = abb[41] + z[23];
z[23] = abb[18] * z[23];
z[24] = abb[34] + -abb[35];
z[25] = m1_set::bc<T>[0] * z[24];
z[26] = abb[13] * z[25];
z[27] = z[23] + -z[26];
z[28] = m1_set::bc<T>[0] * z[21];
z[28] = abb[42] + z[28];
z[29] = abb[4] * (T(1) / T(2));
z[30] = z[28] * z[29];
z[31] = abb[43] * (T(1) / T(4));
z[32] = abb[20] + abb[21];
z[33] = z[31] * z[32];
z[34] = abb[31] + -abb[32];
z[35] = z[24] + z[34];
z[35] = m1_set::bc<T>[0] * z[35];
z[35] = -abb[41] + z[35];
z[36] = -abb[42] + z[35];
z[37] = abb[8] * (T(1) / T(2));
z[38] = z[36] * z[37];
z[39] = -abb[32] + abb[31] * (T(3) / T(4)) + z[17];
z[39] = m1_set::bc<T>[0] * z[39];
z[39] = -abb[41] + z[39];
z[39] = abb[0] * z[39];
z[40] = -abb[30] + z[21];
z[41] = abb[31] + z[40];
z[41] = abb[2] * m1_set::bc<T>[0] * z[41];
z[15] = -z[1] + z[15] + z[16] + z[27] + z[30] + z[33] + z[38] + z[39] + -z[41];
z[15] = abb[48] * z[15];
z[16] = abb[31] + -abb[34];
z[30] = abb[0] * (T(1) / T(4));
z[16] = m1_set::bc<T>[0] * z[16] * z[30];
z[33] = -abb[19] + abb[22];
z[31] = z[31] * z[33];
z[38] = z[3] + z[12];
z[38] = m1_set::bc<T>[0] * z[38];
z[38] = -abb[42] + z[38];
z[42] = abb[17] * (T(1) / T(2));
z[43] = z[38] * z[42];
z[44] = abb[30] + -abb[35];
z[45] = abb[10] * m1_set::bc<T>[0] * z[44];
z[43] = z[43] + -z[45];
z[16] = z[16] + -z[31] + z[43];
z[31] = m1_set::bc<T>[0] * z[34];
z[31] = -abb[41] + z[31];
z[31] = abb[1] * z[31];
z[27] = z[27] + z[31];
z[31] = -abb[30] + z[3];
z[34] = -abb[35] + abb[34] * (T(3) / T(2));
z[46] = z[31] + z[34];
z[46] = m1_set::bc<T>[0] * z[46];
z[46] = -abb[42] + z[46];
z[47] = abb[6] * (T(1) / T(2));
z[48] = z[46] * z[47];
z[49] = abb[30] + -abb[34];
z[50] = abb[33] + z[49];
z[51] = -abb[31] + z[50];
z[51] = abb[14] * m1_set::bc<T>[0] * z[51];
z[48] = -z[16] + z[27] + z[48] + z[51];
z[48] = abb[49] * z[48];
z[41] = -z[41] + z[51];
z[51] = abb[6] * z[25];
z[27] = z[27] + z[41] + z[51];
z[27] = abb[50] * z[27];
z[46] = abb[19] * z[46];
z[51] = abb[22] * z[38];
z[52] = -abb[31] + abb[35] + z[49];
z[52] = m1_set::bc<T>[0] * z[52];
z[52] = abb[42] + z[52];
z[53] = -abb[20] * z[52];
z[46] = z[46] + z[51] + z[53];
z[51] = abb[6] + abb[17];
z[51] = -abb[29] + z[30] + (T(1) / T(4)) * z[51];
z[53] = -abb[43] * z[51];
z[46] = (T(1) / T(2)) * z[46] + z[53];
z[46] = abb[54] * z[46];
z[15] = z[15] + z[27] + z[46] + z[48];
z[27] = -abb[17] * z[38];
z[6] = z[6] + -z[21];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = -abb[41] + -abb[42] + z[6];
z[6] = abb[6] * z[6];
z[6] = z[6] + z[27];
z[27] = -abb[2] + z[29];
z[28] = z[27] * z[28];
z[7] = abb[0] * z[7];
z[1] = -z[1] + (T(1) / T(2)) * z[6] + z[7] + z[23] + z[28] + z[45];
z[6] = abb[53] * (T(1) / T(2));
z[1] = z[1] * z[6];
z[7] = abb[45] + abb[47];
z[28] = abb[44] + abb[46];
z[29] = z[7] + -z[28];
z[38] = abb[34] * z[29];
z[45] = abb[35] * z[7];
z[46] = abb[35] * z[28];
z[48] = z[38] + -z[45] + z[46];
z[53] = abb[32] * z[29];
z[54] = -abb[31] * z[29];
z[54] = -z[48] + z[53] + z[54];
z[54] = m1_set::bc<T>[0] * z[54];
z[55] = abb[41] * z[29];
z[56] = abb[42] * z[29];
z[55] = z[55] + z[56];
z[54] = z[54] + z[55];
z[54] = abb[25] * z[54];
z[57] = z[7] * z[21];
z[58] = abb[33] * z[28];
z[57] = z[57] + z[58];
z[59] = z[4] * z[29];
z[60] = z[46] + z[59];
z[61] = z[57] + -z[60];
z[53] = z[53] + z[61];
z[62] = z[3] * z[29];
z[63] = z[53] + -z[62];
z[63] = m1_set::bc<T>[0] * z[63];
z[55] = z[55] + z[63];
z[63] = abb[24] * z[55];
z[45] = z[45] + -z[60];
z[60] = -z[45] + z[62];
z[60] = m1_set::bc<T>[0] * z[60];
z[56] = -z[56] + z[60];
z[60] = -abb[23] + abb[27];
z[56] = z[56] * z[60];
z[54] = z[54] + z[56] + z[63];
z[56] = abb[35] * (T(3) / T(2));
z[60] = -z[13] + z[56];
z[62] = z[0] + z[60];
z[63] = abb[34] * (T(3) / T(4));
z[64] = z[19] + -z[62] + z[63];
z[64] = m1_set::bc<T>[0] * z[64];
z[65] = abb[41] + abb[42] * (T(1) / T(2));
z[64] = z[64] + -z[65];
z[64] = abb[6] * z[64];
z[39] = -z[26] + z[39] + z[41] + -z[43] + z[64];
z[23] = z[23] + (T(1) / T(2)) * z[39];
z[23] = abb[51] * z[23];
z[5] = -z[5] + z[62];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = z[5] + z[65];
z[39] = abb[49] + abb[51];
z[5] = -z[5] * z[39];
z[41] = -z[4] + -z[21];
z[41] = abb[30] + -z[10] + -z[19] + (T(1) / T(2)) * z[41];
z[41] = m1_set::bc<T>[0] * z[41];
z[43] = -abb[41] + abb[42];
z[41] = z[41] + (T(1) / T(2)) * z[43];
z[41] = abb[53] * z[41];
z[43] = abb[50] * z[35];
z[62] = abb[52] * (T(1) / T(2));
z[52] = z[52] * z[62];
z[5] = z[5] + z[41] + z[43] + z[52];
z[41] = abb[5] * (T(1) / T(2));
z[5] = z[5] * z[41];
z[43] = abb[26] * (T(1) / T(4));
z[52] = z[43] * z[55];
z[31] = -z[12] + z[31];
z[31] = m1_set::bc<T>[0] * z[31];
z[31] = -abb[42] + z[31];
z[31] = z[31] * z[47];
z[55] = -abb[30] + abb[31];
z[64] = m1_set::bc<T>[0] * z[55];
z[64] = -abb[42] + z[64];
z[64] = abb[2] * z[64];
z[16] = -z[16] + z[26] + z[31] + z[64];
z[16] = z[16] * z[62];
z[26] = -abb[28] * z[29];
z[31] = z[32] + z[33];
z[31] = abb[53] * z[31];
z[26] = z[26] + z[31];
z[31] = abb[51] * z[33];
z[31] = z[26] + z[31];
z[31] = abb[43] * z[31];
z[35] = abb[51] * z[35];
z[36] = z[6] * z[36];
z[25] = abb[49] * z[25];
z[25] = z[25] + z[35] + z[36];
z[25] = z[25] * z[37];
z[1] = z[1] + z[5] + (T(1) / T(2)) * z[15] + z[16] + z[23] + z[25] + (T(1) / T(8)) * z[31] + z[52] + (T(1) / T(4)) * z[54];
z[1] = (T(1) / T(4)) * z[1];
z[5] = abb[30] * z[29];
z[5] = z[5] + -z[38];
z[15] = abb[32] * z[5];
z[16] = -abb[33] * z[7];
z[16] = z[16] + z[58] + z[59];
z[16] = abb[30] * z[16];
z[5] = -z[3] * z[5];
z[23] = prod_pow(abb[35], 2);
z[25] = (T(1) / T(2)) * z[23];
z[31] = -abb[40] + z[25];
z[31] = z[7] * z[31];
z[35] = prod_pow(m1_set::bc<T>[0], 2);
z[36] = (T(1) / T(3)) * z[35];
z[38] = z[29] * z[36];
z[52] = abb[40] * abb[46];
z[52] = -z[38] + z[52];
z[54] = abb[36] + abb[38];
z[58] = -abb[55] + z[54];
z[58] = z[29] * z[58];
z[59] = abb[56] * z[29];
z[64] = abb[40] * abb[44];
z[64] = -z[59] + z[64];
z[65] = -z[25] * z[28];
z[46] = -z[46] + z[57];
z[57] = -abb[34] * z[46];
z[5] = z[5] + z[15] + z[16] + z[31] + z[52] + z[57] + z[58] + z[64] + z[65];
z[5] = abb[24] * z[5];
z[16] = z[13] * z[29];
z[31] = -z[16] + z[45];
z[45] = abb[31] * z[31];
z[45] = z[38] + z[45];
z[31] = -abb[30] * z[31];
z[57] = abb[36] * z[29];
z[31] = z[31] + z[45] + z[57] + z[59];
z[31] = abb[27] * z[31];
z[57] = prod_pow(abb[33], 2);
z[58] = (T(1) / T(2)) * z[57];
z[59] = abb[39] + z[58];
z[65] = -abb[40] + z[59];
z[65] = z[7] * z[65];
z[28] = abb[39] * z[28];
z[28] = z[28] + -z[64];
z[64] = abb[44] * z[58];
z[66] = -abb[40] + z[58];
z[66] = abb[46] * z[66];
z[64] = -z[28] + -z[64] + z[65] + -z[66];
z[61] = abb[30] * z[61];
z[65] = abb[37] + abb[38];
z[65] = z[29] * z[65];
z[45] = -z[45] + z[61] + z[64] + z[65];
z[45] = abb[23] * z[45];
z[5] = z[5] + z[31] + z[45];
z[31] = -z[16] + z[46];
z[31] = abb[30] * z[31];
z[45] = abb[31] * z[48];
z[46] = abb[38] * z[29];
z[15] = z[15] + z[31] + z[45] + z[46] + z[64];
z[31] = abb[55] * (T(1) / T(2));
z[45] = -z[29] * z[31];
z[15] = (T(1) / T(2)) * z[15] + -z[38] + z[45];
z[15] = abb[25] * z[15];
z[38] = -abb[52] + -z[39];
z[33] = z[33] * z[38];
z[32] = abb[48] * z[32];
z[26] = -z[26] + -z[32] + z[33];
z[32] = abb[54] * z[51];
z[26] = (T(1) / T(4)) * z[26] + z[32];
z[26] = abb[57] * z[26];
z[5] = (T(1) / T(2)) * z[5] + z[15] + z[26];
z[15] = z[13] * z[24];
z[26] = -z[4] * z[21];
z[32] = -abb[33] + z[4];
z[33] = abb[32] * (T(-3) / T(4)) + z[13] + -z[32];
z[33] = abb[32] * z[33];
z[38] = z[3] * z[22];
z[45] = abb[38] * (T(1) / T(2));
z[46] = abb[37] + abb[39];
z[48] = (T(1) / T(2)) * z[46];
z[51] = abb[36] * (T(1) / T(2));
z[64] = -abb[33] + abb[35] * (T(3) / T(4));
z[64] = abb[35] * z[64];
z[26] = z[15] + z[26] + z[33] + z[38] + -z[45] + z[48] + -z[51] + z[64];
z[33] = abb[16] * (T(1) / T(2));
z[26] = z[26] * z[33];
z[38] = (T(1) / T(4)) * z[35];
z[64] = z[31] + z[38];
z[65] = abb[34] * z[9];
z[66] = -abb[33] + abb[34] + -z[13];
z[67] = -z[13] * z[66];
z[68] = -z[10] * z[49];
z[69] = abb[40] + -z[23];
z[45] = -z[45] + z[64] + -z[65] + z[67] + z[68] + (T(1) / T(2)) * z[69];
z[45] = abb[6] * z[45];
z[12] = abb[34] * z[12];
z[12] = z[12] + z[25];
z[67] = (T(1) / T(2)) * z[35];
z[68] = -z[12] + z[67];
z[69] = abb[13] * z[68];
z[45] = z[45] + -z[69];
z[70] = z[13] * z[49];
z[71] = z[3] * z[49];
z[72] = abb[32] * z[49];
z[73] = -abb[55] + z[72];
z[74] = -abb[37] + z[73];
z[75] = z[36] + z[70] + z[71] + -z[74];
z[76] = abb[15] * z[75];
z[0] = abb[31] * z[0];
z[77] = prod_pow(abb[32], 2);
z[78] = (T(1) / T(2)) * z[77];
z[79] = abb[55] + z[58];
z[0] = abb[38] + z[0] + -z[78] + z[79];
z[80] = abb[7] * z[0];
z[81] = abb[38] + -abb[40];
z[82] = z[59] + z[81];
z[83] = -abb[33] + z[13];
z[83] = abb[30] * z[83];
z[84] = abb[36] + abb[37] + z[82] + z[83];
z[84] = abb[3] * z[84];
z[85] = z[23] + -z[57];
z[86] = abb[34] * z[21];
z[85] = -abb[39] + -abb[56] + (T(1) / T(2)) * z[85] + -z[86];
z[87] = abb[4] * z[85];
z[76] = -z[76] + z[80] + z[84] + z[87];
z[80] = abb[30] * (T(3) / T(2)) + -z[4];
z[87] = -abb[35] + z[80];
z[87] = abb[30] * z[87];
z[87] = z[12] + z[87];
z[34] = -z[13] + z[34];
z[34] = z[3] * z[34];
z[88] = (T(5) / T(12)) * z[35];
z[34] = -z[34] + z[51] + -z[74] + (T(1) / T(2)) * z[87] + z[88];
z[74] = abb[0] * (T(1) / T(2));
z[34] = z[34] * z[74];
z[74] = -abb[33] + z[10];
z[74] = abb[32] * z[74];
z[87] = abb[39] + -abb[40];
z[83] = z[74] + -z[83] + -z[87];
z[83] = -abb[37] + (T(1) / T(2)) * z[83];
z[83] = abb[9] * z[83];
z[34] = z[34] + z[83];
z[83] = -z[23] + z[59];
z[89] = z[3] * z[24];
z[15] = -z[15] + -z[38] + -z[65] + (T(1) / T(2)) * z[83] + z[89];
z[15] = abb[2] * z[15];
z[38] = -abb[33] + abb[35] * (T(1) / T(2));
z[65] = abb[35] * z[38];
z[59] = z[59] + z[65];
z[83] = abb[11] * (T(1) / T(2));
z[59] = z[59] * z[83];
z[15] = z[15] + -z[59];
z[59] = abb[40] + z[79];
z[79] = z[59] + z[65];
z[83] = -z[13] + z[21];
z[83] = abb[30] * z[83];
z[90] = z[79] + -z[83];
z[22] = abb[31] * z[22];
z[91] = z[22] + -z[78] + z[90];
z[91] = abb[18] * z[91];
z[58] = abb[38] + z[58] + z[74];
z[74] = abb[12] * (T(1) / T(2));
z[58] = z[58] * z[74];
z[58] = z[58] + (T(-1) / T(2)) * z[91];
z[74] = z[15] + z[58];
z[57] = abb[40] + z[57];
z[48] = -z[48] + (T(-1) / T(2)) * z[57] + -z[65] + (T(1) / T(4)) * z[77];
z[57] = z[31] + z[36];
z[9] = z[9] + z[13];
z[9] = abb[30] * z[9];
z[18] = abb[30] * (T(-3) / T(4)) + -z[18];
z[18] = abb[31] * z[18];
z[9] = z[9] + z[18] + -z[48] + z[57];
z[9] = z[9] * z[41];
z[18] = abb[31] * z[24];
z[77] = -abb[56] + z[18];
z[72] = z[72] + z[77] + z[82] + z[83];
z[57] = -z[57] + (T(1) / T(2)) * z[72];
z[72] = -z[37] * z[57];
z[9] = z[9] + z[26] + z[34] + (T(1) / T(2)) * z[45] + z[72] + z[74] + (T(1) / T(4)) * z[76];
z[9] = abb[48] * z[9];
z[26] = z[10] + -z[50];
z[26] = abb[32] * z[26];
z[45] = abb[56] * (T(1) / T(2));
z[59] = z[26] + z[45] + z[59];
z[56] = -abb[33] + z[56];
z[17] = -z[17] + z[56];
z[17] = abb[34] * z[17];
z[19] = z[19] * z[49];
z[63] = abb[33] + abb[30] * (T(1) / T(4)) + -z[63];
z[63] = abb[30] * z[63];
z[17] = z[17] + z[19] + (T(-3) / T(4)) * z[23] + -z[51] + z[59] + z[63] + z[88];
z[17] = abb[6] * z[17];
z[19] = abb[31] * z[44];
z[19] = z[19] + -z[65] + z[81] + z[83];
z[19] = abb[10] * z[19];
z[23] = -abb[32] + z[50];
z[50] = abb[31] * z[23];
z[51] = abb[32] * z[23];
z[50] = z[35] + z[50] + -z[51];
z[50] = abb[14] * z[50];
z[50] = z[50] + z[69];
z[14] = -z[14] * z[55];
z[14] = abb[36] + z[14];
z[36] = abb[56] + z[36];
z[55] = z[14] + z[36];
z[42] = z[42] * z[55];
z[63] = z[42] + z[50];
z[17] = -z[17] + z[19] + z[63];
z[65] = (T(1) / T(2)) * z[84] + -z[91];
z[15] = z[15] + (T(-1) / T(2)) * z[17] + z[34] + z[65];
z[15] = abb[51] * z[15];
z[16] = z[16] + -z[53];
z[16] = abb[31] * z[16];
z[7] = z[7] * z[87];
z[17] = abb[37] + -abb[55] + z[78];
z[17] = z[17] * z[29];
z[7] = z[7] + z[16] + z[17] + -z[28] + z[52] + z[61];
z[7] = z[7] * z[43];
z[16] = -z[25] + z[86];
z[17] = -abb[36] + z[71];
z[28] = -abb[30] * z[32];
z[28] = z[16] + z[17] + z[28] + z[36] + -z[73] + -z[81];
z[28] = abb[6] * z[28];
z[29] = -abb[17] * z[55];
z[28] = z[28] + z[29];
z[8] = abb[0] + -z[8];
z[8] = z[8] * z[75];
z[0] = z[0] * z[2];
z[2] = z[27] * z[85];
z[0] = z[0] + z[2] + z[8] + -z[19] + (T(1) / T(2)) * z[28] + z[65];
z[0] = z[0] * z[6];
z[2] = -z[4] + z[20] + z[56];
z[2] = abb[31] * z[2];
z[8] = abb[33] + z[4] + -z[60];
z[8] = abb[30] * z[8];
z[2] = z[2] + z[8] + z[45] + z[67] + -z[78] + z[79];
z[2] = abb[51] * z[2];
z[8] = -z[4] + -z[40];
z[8] = z[8] * z[13];
z[19] = abb[32] + z[21] + -z[80];
z[19] = z[3] * z[19];
z[20] = (T(1) / T(6)) * z[35];
z[8] = z[8] + z[19] + -z[20] + z[31] + -z[45] + -z[48];
z[8] = abb[53] * z[8];
z[4] = -z[4] + z[13] + -z[38];
z[4] = abb[30] * z[4];
z[4] = z[4] + z[16] + z[59] + z[67] + -z[89];
z[4] = abb[49] * z[4];
z[16] = z[18] + -z[26] + -z[35] + -z[90];
z[18] = -abb[50] * z[16];
z[19] = abb[30] * z[24];
z[24] = -z[19] + z[77];
z[26] = z[24] + -z[35];
z[27] = z[26] * z[62];
z[2] = z[2] + z[4] + z[8] + z[18] + z[27];
z[2] = z[2] * z[41];
z[4] = abb[34] * z[32];
z[8] = abb[30] * z[66];
z[4] = z[4] + -z[8] + -z[81];
z[8] = z[11] + z[49];
z[8] = abb[32] * z[8];
z[11] = z[3] * z[23];
z[4] = (T(-1) / T(2)) * z[4] + z[8] + -z[11] + -z[64];
z[4] = abb[1] * z[4];
z[8] = abb[6] * z[68];
z[8] = z[8] + -z[50];
z[8] = -z[4] + (T(1) / T(2)) * z[8] + z[74];
z[8] = abb[50] * z[8];
z[11] = abb[56] + z[17] + -z[70];
z[17] = z[11] + -z[12] + (T(5) / T(6)) * z[35];
z[18] = z[17] * z[47];
z[18] = z[18] + -z[63];
z[14] = z[12] + z[14] + z[20];
z[14] = z[14] * z[30];
z[3] = z[3] + -z[13];
z[3] = z[3] * z[44];
z[3] = -abb[36] + z[3];
z[3] = abb[10] * z[3];
z[3] = z[3] + z[14];
z[4] = -z[3] + -z[4] + (T(1) / T(2)) * z[18] + z[58];
z[4] = abb[49] * z[4];
z[11] = z[11] + z[12] + -z[20];
z[11] = z[11] * z[47];
z[12] = z[12] + -z[24] + z[67];
z[12] = abb[2] * z[12];
z[11] = z[11] + z[12] + -z[42] + z[69];
z[3] = -z[3] + (T(1) / T(2)) * z[11];
z[3] = abb[52] * z[3];
z[11] = z[19] + z[22] + -z[86];
z[10] = z[10] + -z[49];
z[10] = -abb[32] * z[10];
z[10] = z[10] + z[11] + z[25] + -z[46] + z[54];
z[6] = z[6] * z[10];
z[10] = abb[35] * z[21];
z[10] = z[10] + z[11] + z[51];
z[11] = abb[50] + z[39];
z[10] = z[10] * z[11];
z[6] = z[6] + z[10];
z[6] = z[6] * z[33];
z[10] = -abb[51] * z[16];
z[11] = -abb[53] * z[57];
z[12] = abb[49] * z[68];
z[10] = z[10] + z[11] + z[12];
z[10] = z[10] * z[37];
z[11] = abb[19] * z[17];
z[12] = -abb[20] * z[26];
z[13] = abb[22] * z[55];
z[11] = z[11] + z[12] + z[13];
z[11] = abb[54] * z[11];
z[0] = z[0] + z[2] + z[3] + z[4] + (T(1) / T(2)) * z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + (T(1) / T(4)) * z[11] + z[15];
z[0] = (T(1) / T(4)) * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_120_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.72038211462058131582783922174734499909779426310612627774869502968"),stof<T>("0.12970240680147400336550207526481572820449061504694611196130252805")}, std::complex<T>{stof<T>("-0.5107450234892300903803708191383389082436212597125532197170905114"),stof<T>("0.35398302307852721249066846790110541293463650256313271212367309096")}, std::complex<T>{stof<T>("-1.3212496324146752793794140156688067857013151975707890667213928086"),stof<T>("0.8434002737337653198416459035053544840998950619754997108718017326")}, std::complex<T>{stof<T>("-0.55452167689670963393054142312720756107919530766918409245693284194"),stof<T>("0.19291901075164879353821092131862393983768174888437731214661130931")}, std::complex<T>{stof<T>("0.53776821282895166511817325939540943536902189284932211972746158888"),stof<T>("-0.54299230350414774189133964333877188580490656229717786185895510081")}, std::complex<T>{stof<T>("-0.18261390179162965070966596235193556372877237025680415802123344081"),stof<T>("-0.41328989670267373852583756807395615760041594725023174989765257276")}, std::complex<T>{stof<T>("0.45206819008996940444056879493693166461096524631315406723780644064"),stof<T>("-0.6183869553716871503642261524770373176934380471354809098273440296")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_120_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_120_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.02435556195130010557768050841578811653053069984159512999953262175"),stof<T>("0.40509873509489876701374346996009466991183351566969552760882937672")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W79(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_120_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_120_DLogXconstant_part(base_point<T>, kend);
	value += f_4_120_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_120_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_120_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_120_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_120_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_120_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_120_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
