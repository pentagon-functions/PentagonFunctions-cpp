/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_618.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_618_abbreviated (const std::array<T,31>& abb) {
T z[40];
z[0] = 2 * abb[22];
z[1] = abb[23] * (T(3) / T(4));
z[2] = abb[19] * (T(1) / T(2));
z[3] = abb[20] + -abb[21];
z[4] = -abb[24] + (T(-1) / T(4)) * z[3];
z[5] = z[0] + -z[1] + z[2] + z[4];
z[5] = abb[3] * z[5];
z[6] = abb[19] * (T(3) / T(2));
z[7] = abb[23] * (T(1) / T(2));
z[8] = z[6] + -z[7];
z[9] = abb[22] * (T(3) / T(2));
z[10] = abb[24] + z[3];
z[11] = z[8] + -z[9] + z[10];
z[12] = abb[6] * z[11];
z[13] = -abb[24] + (T(-5) / T(4)) * z[3];
z[14] = abb[23] * (T(5) / T(4));
z[15] = abb[22] * (T(7) / T(2)) + z[13] + -z[14];
z[15] = abb[2] * z[15];
z[16] = -abb[25] + abb[26] + abb[27];
z[17] = abb[8] * z[16];
z[18] = abb[7] * z[16];
z[19] = z[17] + -z[18];
z[20] = -abb[19] + abb[23];
z[21] = -abb[22] + z[20];
z[22] = abb[0] * z[21];
z[19] = (T(1) / T(4)) * z[19] + (T(-1) / T(2)) * z[22];
z[22] = 2 * z[21];
z[23] = abb[1] * z[22];
z[24] = 2 * z[10];
z[25] = abb[19] + -5 * abb[22] + abb[23] + z[24];
z[25] = abb[4] * z[25];
z[5] = -z[5] + -z[12] + z[15] + z[19] + -z[23] + z[25];
z[5] = abb[12] * z[5];
z[6] = z[4] + -z[6] + z[14];
z[14] = abb[3] * z[6];
z[14] = -z[12] + z[14];
z[15] = abb[22] * (T(1) / T(2));
z[8] = -z[3] + -z[8] + z[15];
z[8] = abb[0] * z[8];
z[26] = 2 * abb[19];
z[15] = abb[23] * (T(7) / T(4)) + z[4] + -z[15] + -z[26];
z[15] = abb[2] * z[15];
z[27] = 3 * z[17] + z[18];
z[8] = z[8] + -z[14] + z[15] + (T(-1) / T(4)) * z[27];
z[8] = abb[13] * z[8];
z[15] = 3 * abb[22];
z[27] = 3 * abb[19] + -abb[23] + -z[15] + z[24];
z[28] = abb[6] * z[27];
z[29] = abb[3] * z[22];
z[25] = z[25] + -z[28] + z[29];
z[29] = abb[2] * z[22];
z[29] = -z[25] + z[29];
z[30] = abb[14] * z[29];
z[31] = 7 * abb[19];
z[32] = -abb[22] + 5 * abb[23] + -z[24] + -z[31];
z[33] = abb[14] * z[32];
z[24] = 5 * abb[19] + -abb[22] + -3 * abb[23] + z[24];
z[24] = abb[13] * z[24];
z[24] = z[24] + z[33];
z[24] = abb[1] * z[24];
z[24] = z[24] + z[30];
z[30] = abb[4] * abb[13];
z[33] = z[22] * z[30];
z[5] = z[5] + z[8] + -z[24] + -z[33];
z[8] = -abb[24] + (T(-3) / T(4)) * z[3];
z[33] = abb[23] * (T(1) / T(4));
z[0] = z[0] + -z[2] + z[8] + -z[33];
z[0] = abb[3] * z[0];
z[34] = abb[19] + -abb[22];
z[35] = z[3] + z[34];
z[35] = abb[0] * z[35];
z[17] = z[17] + z[18];
z[36] = (T(3) / T(4)) * z[17] + (T(3) / T(2)) * z[35];
z[33] = -z[4] + -z[9] + z[33];
z[37] = abb[2] * z[33];
z[38] = abb[4] * z[27];
z[39] = abb[1] * z[21];
z[0] = z[0] + z[12] + z[36] + z[37] + -z[38] + z[39];
z[0] = abb[11] * z[0];
z[0] = z[0] + z[5];
z[0] = abb[11] * z[0];
z[2] = abb[22] * (T(5) / T(2)) + -z[2] + -z[7] + -z[10];
z[2] = abb[1] * z[2];
z[12] = abb[22] * (T(-13) / T(2)) + abb[23] * (T(-7) / T(2)) + abb[19] * (T(17) / T(2)) + 5 * z[10];
z[12] = abb[4] * z[12];
z[2] = z[2] + z[12];
z[10] = abb[23] * (T(1) / T(3)) + (T(-2) / T(3)) * z[10] + -z[34];
z[10] = abb[6] * z[10];
z[12] = abb[23] * (T(1) / T(12));
z[8] = abb[22] * (T(-5) / T(3)) + abb[19] * (T(5) / T(6)) + -z[8] + -z[12];
z[8] = abb[3] * z[8];
z[4] = abb[19] * (T(-1) / T(3)) + abb[22] * (T(7) / T(6)) + z[4] + z[12];
z[4] = abb[2] * z[4];
z[2] = (T(1) / T(3)) * z[2] + z[4] + z[8] + z[10] + -z[36];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[1] = -z[1] + -z[9] + -z[13] + z[26];
z[1] = abb[2] * z[1];
z[1] = z[1] + z[14] + -z[19];
z[1] = abb[13] * z[1];
z[4] = (T(1) / T(2)) * z[3];
z[8] = abb[19] + z[4] + -z[7];
z[8] = abb[3] * z[8];
z[9] = abb[22] + -z[4] + -z[7];
z[9] = abb[0] * z[9];
z[8] = z[8] + z[9] + (T(-1) / T(2)) * z[18] + z[39];
z[8] = abb[12] * z[8];
z[1] = z[1] + z[8] + z[24];
z[1] = abb[12] * z[1];
z[8] = abb[8] * z[33];
z[9] = abb[9] * z[11];
z[6] = abb[7] * z[6];
z[10] = abb[0] + -abb[10] + abb[3] * (T(1) / T(4)) + abb[2] * (T(3) / T(4));
z[10] = z[10] * z[16];
z[6] = z[6] + z[8] + z[9] + z[10];
z[8] = -abb[29] * z[6];
z[9] = abb[23] + -abb[24];
z[9] = 3 * z[3] + -4 * z[9] + -z[15] + z[31];
z[9] = abb[15] * z[9];
z[10] = -abb[23] + z[3] + z[26];
z[11] = abb[13] + -abb[14];
z[12] = 2 * abb[14];
z[10] = z[10] * z[11] * z[12];
z[11] = -abb[28] * z[32];
z[9] = z[9] + z[10] + z[11];
z[9] = abb[1] * z[9];
z[10] = abb[28] * z[25];
z[11] = z[21] * z[30];
z[13] = abb[2] * z[21];
z[14] = -abb[4] * z[21];
z[14] = z[13] + z[14];
z[14] = abb[14] * z[14];
z[13] = -abb[13] * z[13];
z[11] = z[11] + z[13] + z[14];
z[11] = z[11] * z[12];
z[12] = -abb[24] + z[20];
z[12] = abb[3] * z[12];
z[12] = 2 * z[12] + -z[28];
z[12] = abb[15] * z[12];
z[13] = abb[15] * z[27];
z[14] = -abb[28] * z[22];
z[13] = z[13] + z[14];
z[13] = abb[2] * z[13];
z[0] = abb[30] + z[0] + z[1] + z[2] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13];
z[1] = 2 * abb[24] + z[7];
z[2] = -z[1] + -z[4] + z[15];
z[2] = abb[2] * z[2];
z[1] = abb[19] + -4 * abb[22] + z[1] + (T(3) / T(2)) * z[3];
z[1] = abb[3] * z[1];
z[1] = z[1] + z[2] + (T(-3) / T(2)) * z[17] + -z[23] + -z[28] + -3 * z[35] + 2 * z[38];
z[1] = abb[11] * z[1];
z[1] = z[1] + -z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[17] * z[6];
z[3] = -abb[1] * z[32];
z[3] = z[3] + -z[29];
z[3] = abb[16] * z[3];
z[1] = abb[18] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_618_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("5.8441052091934394355957795944545083944060231559654381734011444864"),stof<T>("4.5595132367802453436896934862671728395635626532458245263118165742")}, std::complex<T>{stof<T>("-3.2481853203630900633407033833170887234980389256704273032843753355"),stof<T>("9.1722009068429313864008486576030179725601740273379980739751518286")}, std::complex<T>{stof<T>("3.2481853203630900633407033833170887234980389256704273032843753355"),stof<T>("-9.1722009068429313864008486576030179725601740273379980739751518286")}, std::complex<T>{stof<T>("11.9633763092481669255026271436199879280381749457019868763583130066"),stof<T>("-6.6992573141383905889165670726101014671262602159095057418537903947")}, std::complex<T>{stof<T>("-9.0922905295565294989364829777715971179040620816358654766855198219"),stof<T>("4.6126876700626860427111551713358451329966113740921735476633352544")}, std::complex<T>{stof<T>("0.3770995406714526367745592174686979133639260616043059036115821508"),stof<T>("-7.0856312627672268401954367563287616384305251855206658797846966884")}, std::complex<T>{stof<T>("7.4219161741015956134980195093093657607382609948979409912152034895"),stof<T>("-10.5057465250230288743736521272080806678691704747824452001159249788")}, std::complex<T>{stof<T>("-7.4219161741015956134980195093093657607382609948979409912152034895"),stof<T>("10.5057465250230288743736521272080806678691704747824452001159249788")}, std::complex<T>{stof<T>("-7.4219161741015956134980195093093657607382609948979409912152034895"),stof<T>("10.5057465250230288743736521272080806678691704747824452001159249788")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(abs(k.W[39])) - rlog(abs(kbase.W[39])), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_618_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_618_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 + -5 * v[0] + v[1] + -5 * v[2] + -v[3] + 4 * v[4] + 4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 5 * v[5])) / prod_pow(tend, 2);
c[1] = (-(1 + 2 * m1_set::bc<T>[1]) * (v[0] + -1 * v[1] + v[2] + v[3] + -1 * v[5])) / tend;


		return (2 * abb[19] + abb[20] + -abb[21] + -abb[23]) * (2 * t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = abb[13] + -abb[14];
z[0] = abb[14] * z[0];
z[0] = -abb[15] + 2 * z[0];
z[1] = -2 * abb[19] + -abb[20] + abb[21] + abb[23];
return abb[5] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_618_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_618_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-4.8329766544215011532849006427545859612444678092320330616003380173"),stof<T>("2.0530709259527252973344697485800414858536982734356780881184735831")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 39});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W29(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_15(k), f_2_2_im(k), f_2_25_im(k), T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(abs(kend.W[39])) - rlog(abs(k.W[39])), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), f_2_2_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_618_W_17_Im(t, path, abb);
abb[30] = SpDLog_f_4_618_W_17_Re(t, path, abb);

                    
            return f_4_618_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_618_DLogXconstant_part(base_point<T>, kend);
	value += f_4_618_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_618_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_618_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_618_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_618_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_618_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_618_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
