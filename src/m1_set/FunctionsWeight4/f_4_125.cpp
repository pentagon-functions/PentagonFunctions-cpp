/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_125.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_125_abbreviated (const std::array<T,55>& abb) {
T z[70];
z[0] = abb[30] + -abb[32];
z[1] = abb[31] * (T(1) / T(2));
z[2] = z[0] + -z[1];
z[2] = abb[27] + (T(1) / T(2)) * z[2];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[28] * m1_set::bc<T>[0];
z[4] = (T(1) / T(4)) * z[3];
z[5] = abb[38] * (T(1) / T(2));
z[6] = abb[29] * m1_set::bc<T>[0];
z[7] = (T(1) / T(2)) * z[6];
z[2] = abb[39] * (T(-1) / T(2)) + z[2] + -z[4] + z[5] + -z[7];
z[2] = abb[6] * z[2];
z[8] = m1_set::bc<T>[0] * z[1];
z[9] = abb[38] + z[6];
z[10] = abb[28] * (T(1) / T(2));
z[11] = m1_set::bc<T>[0] * z[10];
z[8] = z[8] + -z[9] + z[11];
z[8] = abb[12] * z[8];
z[12] = abb[30] * m1_set::bc<T>[0];
z[13] = -z[6] + z[12];
z[14] = abb[38] + -z[13];
z[14] = abb[7] * z[14];
z[15] = m1_set::bc<T>[0] * z[0];
z[15] = -abb[39] + z[15];
z[15] = abb[4] * z[15];
z[14] = z[14] + z[15];
z[8] = z[8] + -z[14];
z[15] = abb[27] * m1_set::bc<T>[0];
z[15] = -z[6] + z[15];
z[15] = abb[9] * z[15];
z[8] = (T(1) / T(2)) * z[8] + -z[15];
z[15] = abb[27] + z[0];
z[15] = m1_set::bc<T>[0] * z[15];
z[16] = abb[39] + z[6];
z[15] = z[15] + -z[16];
z[15] = abb[15] * z[15];
z[17] = z[8] + z[15];
z[18] = z[0] + z[1];
z[19] = m1_set::bc<T>[0] * z[18];
z[11] = -abb[39] + z[11];
z[19] = z[9] + -z[11] + -z[19];
z[20] = abb[5] * (T(1) / T(2));
z[21] = -z[19] * z[20];
z[22] = -abb[32] + z[1];
z[23] = m1_set::bc<T>[0] * z[22];
z[23] = z[11] + z[23];
z[24] = abb[14] * (T(1) / T(2));
z[25] = z[23] * z[24];
z[26] = abb[0] * z[23];
z[9] = -z[9] + z[12];
z[12] = abb[1] * z[9];
z[2] = z[2] + z[12] + -z[17] + z[21] + -z[25] + z[26];
z[12] = abb[45] * (T(1) / T(2));
z[2] = z[2] * z[12];
z[21] = -abb[32] + abb[31] * (T(3) / T(4));
z[26] = abb[30] * (T(1) / T(2));
z[27] = z[21] + z[26];
z[28] = abb[27] * (T(1) / T(2));
z[29] = z[27] + -z[28];
z[29] = m1_set::bc<T>[0] * z[29];
z[7] = abb[39] + z[7];
z[29] = (T(3) / T(4)) * z[3] + -z[5] + -z[7] + z[29];
z[29] = z[20] * z[29];
z[22] = z[22] + z[28];
z[30] = z[22] + z[26];
z[30] = m1_set::bc<T>[0] * z[30];
z[5] = z[5] + -z[7] + z[30];
z[5] = abb[6] * z[5];
z[7] = abb[31] * m1_set::bc<T>[0];
z[7] = -z[3] + z[7];
z[30] = abb[0] * (T(1) / T(4));
z[7] = z[7] * z[30];
z[30] = -abb[31] + abb[32];
z[31] = m1_set::bc<T>[0] * z[30];
z[32] = abb[39] + z[31];
z[33] = abb[2] * z[32];
z[7] = z[5] + -z[7] + -z[17] + -z[33];
z[7] = (T(1) / T(2)) * z[7] + z[29];
z[17] = z[28] + z[30];
z[17] = m1_set::bc<T>[0] * z[17];
z[17] = -z[11] + z[17];
z[17] = abb[10] * z[17];
z[29] = abb[1] * (T(1) / T(2));
z[9] = z[9] * z[29];
z[9] = z[7] + z[9] + z[17];
z[9] = abb[46] * z[9];
z[34] = abb[38] + -z[32];
z[34] = abb[6] * z[34];
z[14] = z[14] + -z[15] + -z[33] + z[34];
z[13] = -abb[38] + (T(1) / T(2)) * z[13];
z[13] = abb[11] * z[13];
z[13] = z[13] + z[17];
z[34] = abb[27] + -abb[31];
z[35] = abb[30] + z[34];
z[36] = m1_set::bc<T>[0] * z[35];
z[36] = -z[6] + z[36];
z[36] = z[29] * z[36];
z[36] = z[13] + z[36];
z[16] = -z[3] + z[16] + z[31];
z[31] = -z[16] * z[20];
z[14] = (T(1) / T(2)) * z[14] + z[31] + z[36];
z[14] = abb[48] * z[14];
z[7] = z[7] + z[13];
z[7] = abb[49] * z[7];
z[13] = m1_set::bc<T>[0] * z[21];
z[4] = -abb[39] + z[4] + z[13];
z[4] = abb[0] * z[4];
z[5] = z[4] + z[5] + -z[8];
z[8] = abb[27] * (T(1) / T(4));
z[13] = -abb[32] + abb[30] * (T(3) / T(4)) + abb[31] * (T(3) / T(8)) + z[8];
z[13] = m1_set::bc<T>[0] * z[13];
z[13] = -abb[39] + abb[38] * (T(-1) / T(4)) + (T(3) / T(8)) * z[3] + (T(-3) / T(4)) * z[6] + z[13];
z[13] = abb[5] * z[13];
z[5] = (T(1) / T(2)) * z[5] + z[13] + -z[15] + z[36];
z[5] = abb[47] * z[5];
z[13] = abb[46] + abb[47] + abb[49];
z[15] = -abb[17] + abb[18];
z[13] = z[13] * z[15];
z[21] = abb[16] + abb[19];
z[15] = z[15] + z[21];
z[15] = abb[45] * z[15];
z[31] = abb[0] + abb[6];
z[36] = abb[14] + z[31];
z[36] = abb[51] * z[36];
z[21] = abb[50] * z[21];
z[37] = -abb[41] + abb[43] + -abb[44];
z[38] = abb[42] + z[37];
z[39] = abb[25] * z[38];
z[13] = -z[13] + -z[15] + -z[21] + z[36] + -z[39];
z[15] = abb[26] * abb[51];
z[13] = (T(1) / T(4)) * z[13] + -z[15];
z[13] = (T(1) / T(2)) * z[13];
z[15] = -abb[40] * z[13];
z[21] = abb[38] + z[16];
z[36] = -abb[45] + -abb[46];
z[36] = z[21] * z[36];
z[16] = abb[38] + -z[16];
z[16] = abb[49] * z[16];
z[6] = abb[38] + z[3] + -z[6] + -3 * z[32];
z[6] = abb[47] * z[6];
z[6] = z[6] + z[16] + z[36];
z[6] = abb[8] * z[6];
z[16] = abb[21] * z[19];
z[21] = abb[22] * z[21];
z[32] = abb[20] + -abb[24];
z[32] = z[23] * z[32];
z[16] = -z[16] + -z[21] + z[32];
z[21] = abb[42] * (T(1) / T(4));
z[32] = z[21] + (T(1) / T(4)) * z[37];
z[16] = z[16] * z[32];
z[32] = -abb[27] + abb[31] * (T(3) / T(2));
z[36] = -abb[32] + z[32];
z[36] = m1_set::bc<T>[0] * z[36];
z[11] = z[11] + z[36];
z[36] = abb[6] * (T(1) / T(2));
z[39] = -z[11] * z[36];
z[4] = z[4] + -z[25] + z[33] + z[39];
z[25] = abb[32] + z[34];
z[25] = m1_set::bc<T>[0] * z[25];
z[3] = abb[39] + -z[3] + z[25];
z[25] = abb[5] * (T(1) / T(4));
z[33] = z[3] * z[25];
z[4] = (T(1) / T(2)) * z[4] + -z[17] + z[33];
z[4] = abb[50] * z[4];
z[11] = abb[16] * z[11];
z[3] = -abb[17] * z[3];
z[17] = abb[19] * z[23];
z[3] = z[3] + z[11] + z[17];
z[11] = abb[51] * (T(1) / T(4));
z[3] = z[3] * z[11];
z[17] = abb[23] * (T(1) / T(4));
z[17] = z[17] * z[38];
z[19] = -z[17] * z[19];
z[2] = z[2] + z[3] + z[4] + z[5] + (T(1) / T(4)) * z[6] + z[7] + z[9] + z[14] + z[15] + z[16] + z[19];
z[2] = (T(1) / T(4)) * z[2];
z[3] = abb[29] * (T(1) / T(2));
z[4] = z[3] + -z[34];
z[4] = abb[29] * z[4];
z[5] = -abb[29] + z[0];
z[6] = abb[27] + z[5];
z[6] = abb[28] * z[6];
z[7] = abb[27] * z[30];
z[9] = abb[30] * abb[31];
z[9] = abb[35] + z[9];
z[14] = -abb[31] + abb[32] * (T(1) / T(2));
z[14] = abb[32] * z[14];
z[4] = abb[34] + abb[36] + z[4] + z[6] + z[7] + -z[9] + -z[14];
z[6] = abb[13] * (T(1) / T(2));
z[4] = z[4] * z[6];
z[6] = z[10] * z[34];
z[15] = abb[29] * z[34];
z[16] = prod_pow(m1_set::bc<T>[0], 2);
z[19] = (T(1) / T(3)) * z[16];
z[23] = abb[52] + z[19];
z[15] = z[6] + -z[15] + z[23];
z[33] = z[28] * z[34];
z[38] = abb[34] + z[15] + z[33];
z[38] = abb[12] * z[38];
z[39] = prod_pow(abb[30], 2);
z[40] = (T(1) / T(2)) * z[39];
z[41] = abb[35] + z[40];
z[42] = abb[29] + -abb[30];
z[43] = abb[28] * z[42];
z[44] = prod_pow(abb[29], 2);
z[45] = (T(1) / T(2)) * z[44];
z[43] = abb[52] + z[41] + z[43] + -z[45];
z[46] = abb[7] * z[43];
z[47] = -abb[31] + z[26];
z[48] = abb[30] * z[47];
z[49] = -abb[53] + z[14];
z[50] = abb[36] + z[48] + -z[49];
z[50] = abb[4] * z[50];
z[46] = -z[46] + z[50];
z[38] = z[38] + -z[46];
z[50] = -abb[30] + z[28];
z[50] = abb[27] * z[50];
z[51] = -abb[36] + abb[37];
z[50] = abb[34] + z[41] + z[50] + -z[51];
z[52] = abb[3] * (T(1) / T(2));
z[50] = z[50] * z[52];
z[52] = -abb[30] + z[3];
z[53] = -z[34] + z[52];
z[53] = abb[29] * z[53];
z[54] = abb[30] + z[28];
z[55] = -abb[31] + z[54];
z[56] = abb[27] * z[55];
z[56] = z[53] + z[56];
z[51] = z[51] + z[56];
z[51] = abb[9] * z[51];
z[4] = z[4] + (T(-1) / T(2)) * z[38] + z[50] + z[51];
z[38] = z[48] + z[56];
z[48] = abb[37] + abb[53];
z[50] = z[14] + -z[48];
z[51] = z[38] + -z[50];
z[51] = abb[15] * z[51];
z[56] = z[4] + -z[51];
z[57] = abb[30] + -abb[31];
z[58] = abb[30] * z[57];
z[32] = abb[30] + -z[32];
z[32] = abb[27] * z[32];
z[32] = -z[23] + z[32] + -z[50] + z[58];
z[54] = -z[1] + -z[3] + z[54];
z[58] = abb[29] * z[54];
z[59] = abb[35] * (T(1) / T(2));
z[60] = abb[28] * z[34];
z[32] = (T(1) / T(2)) * z[32] + -z[58] + z[59] + (T(-1) / T(4)) * z[60];
z[32] = abb[6] * z[32];
z[5] = z[1] + z[5] + z[28];
z[5] = abb[28] * z[5];
z[18] = abb[27] * z[18];
z[48] = -abb[36] + z[48];
z[18] = -abb[34] + z[18] + z[48];
z[5] = -z[5] + z[18] + z[23] + -z[45];
z[23] = z[5] * z[20];
z[45] = abb[27] * z[22];
z[60] = abb[28] * z[22];
z[61] = -z[19] + z[60];
z[62] = abb[53] + z[45] + -z[61];
z[63] = abb[33] + z[62];
z[24] = z[24] * z[63];
z[64] = abb[0] * z[62];
z[65] = abb[1] * z[43];
z[66] = abb[3] + -abb[13];
z[67] = abb[6] + z[66];
z[67] = abb[0] + (T(1) / T(2)) * z[67];
z[67] = abb[33] * z[67];
z[23] = z[23] + -z[24] + z[32] + z[56] + z[64] + z[65] + z[67];
z[23] = z[12] * z[23];
z[32] = abb[37] * (T(1) / T(2));
z[59] = abb[52] * (T(1) / T(2)) + -z[59];
z[58] = -z[32] + z[58] + z[59];
z[55] = z[28] * z[55];
z[57] = z[26] * z[57];
z[64] = (T(1) / T(4)) * z[16];
z[65] = prod_pow(abb[31], 2);
z[49] = z[49] + -z[55] + -z[57] + z[58] + -z[64] + (T(1) / T(4)) * z[65];
z[49] = abb[6] * z[49];
z[55] = abb[0] + z[66];
z[57] = abb[33] * (T(1) / T(2));
z[55] = z[55] * z[57];
z[49] = z[49] + -z[55];
z[55] = z[10] + -z[28];
z[22] = z[22] * z[55];
z[55] = (T(1) / T(2)) * z[65];
z[66] = z[14] + z[55];
z[67] = -abb[53] + (T(1) / T(2)) * z[66];
z[22] = z[22] + -z[64] + z[67];
z[22] = abb[0] * z[22];
z[4] = z[4] + -z[22] + -z[49];
z[8] = -z[3] + z[8] + z[27];
z[8] = z[8] * z[10];
z[64] = abb[52] * (T(1) / T(4));
z[8] = abb[34] * (T(1) / T(4)) + z[8] + -z[19] + -z[64];
z[68] = abb[36] * (T(1) / T(2));
z[69] = -z[14] + -z[68];
z[26] = z[26] * z[47];
z[47] = -abb[32] + abb[31] * (T(-1) / T(4)) + abb[30] * (T(3) / T(2)) + z[28];
z[47] = z[28] * z[47];
z[35] = abb[29] * (T(1) / T(4)) + -z[35];
z[35] = z[3] * z[35];
z[26] = abb[53] + abb[37] * (T(3) / T(4)) + -z[8] + z[26] + z[35] + z[47] + (T(1) / T(2)) * z[69];
z[26] = abb[5] * z[26];
z[35] = z[28] * z[30];
z[47] = z[10] * z[30];
z[35] = (T(7) / T(12)) * z[16] + -z[35] + z[47] + -z[67];
z[35] = abb[10] * z[35];
z[10] = -z[3] + z[10];
z[10] = z[10] * z[42];
z[42] = (T(1) / T(6)) * z[16];
z[10] = abb[52] + z[10] + -z[42];
z[10] = abb[11] * z[10];
z[10] = z[10] + -z[35];
z[38] = abb[37] + z[38] + -z[42] + z[55];
z[38] = z[29] * z[38];
z[38] = z[10] + z[38];
z[4] = (T(1) / T(2)) * z[4] + z[26] + z[38] + -z[51];
z[4] = abb[47] * z[4];
z[0] = z[0] + z[28];
z[26] = z[0] * z[28];
z[28] = (T(5) / T(6)) * z[16];
z[55] = -abb[36] + -z[65];
z[14] = abb[53] * (T(3) / T(2)) + -z[14] + z[26] + z[28] + (T(1) / T(4)) * z[39] + z[47] + (T(1) / T(2)) * z[55] + -z[58];
z[14] = abb[47] * z[14];
z[0] = abb[27] * z[0];
z[0] = z[0] + z[48];
z[26] = z[0] + -z[40];
z[34] = z[3] * z[34];
z[26] = -z[19] + (T(-1) / T(2)) * z[26] + z[34] + -z[47] + -z[59];
z[12] = abb[46] * (T(-1) / T(2)) + -z[12];
z[12] = z[12] * z[26];
z[30] = abb[28] * z[30];
z[34] = abb[35] + z[0] + z[30] + z[40];
z[3] = -z[3] * z[54];
z[3] = z[3] + z[19] + (T(1) / T(4)) * z[34] + -z[64];
z[3] = abb[49] * z[3];
z[3] = z[3] + z[12] + (T(1) / T(2)) * z[14];
z[3] = abb[8] * z[3];
z[12] = z[42] + z[45] + -z[60] + z[66];
z[14] = abb[0] * (T(1) / T(2));
z[12] = z[12] * z[14];
z[14] = -abb[53] + z[66];
z[19] = z[14] + -z[42];
z[19] = abb[2] * z[19];
z[12] = z[12] + -z[19] + -z[49] + z[56];
z[27] = abb[27] * z[27];
z[27] = abb[53] + z[27] + z[32] + -z[68];
z[8] = -z[8] + (T(1) / T(2)) * z[27] + (T(-1) / T(8)) * z[44];
z[8] = abb[5] * z[8];
z[8] = z[8] + (T(1) / T(2)) * z[12];
z[12] = z[29] * z[43];
z[12] = z[8] + z[12] + -z[35];
z[12] = abb[46] * z[12];
z[1] = -abb[30] + z[1];
z[1] = abb[27] * z[1];
z[1] = abb[33] + z[1] + z[9] + -z[15] + z[50];
z[1] = abb[21] * z[1];
z[9] = z[18] + -z[41] + -z[61];
z[9] = abb[20] * z[9];
z[15] = abb[24] * z[63];
z[1] = z[1] + -z[9] + z[15];
z[9] = abb[22] * z[26];
z[15] = (T(1) / T(2)) * z[1] + z[9];
z[18] = (T(-1) / T(2)) * z[37];
z[15] = z[15] * z[18];
z[6] = -z[6] + z[14] + -z[28] + z[33];
z[18] = z[6] * z[36];
z[26] = z[31] * z[57];
z[18] = z[18] + z[19] + -z[22] + -z[24] + z[26];
z[22] = z[16] + z[30];
z[7] = abb[53] + -z[7] + z[22];
z[24] = -z[7] * z[25];
z[18] = (T(1) / T(2)) * z[18] + z[24] + z[35];
z[18] = abb[50] * z[18];
z[24] = abb[29] * z[52];
z[14] = -abb[52] + -z[14] + (T(1) / T(2)) * z[16] + z[24] + z[41];
z[14] = abb[6] * z[14];
z[14] = z[14] + -z[19] + z[46] + -z[51];
z[0] = z[0] + z[22] + z[53];
z[0] = z[0] * z[20];
z[0] = z[0] + (T(1) / T(2)) * z[14] + z[38];
z[0] = abb[48] * z[0];
z[8] = z[8] + z[10];
z[8] = abb[49] * z[8];
z[1] = -z[1] * z[21];
z[9] = abb[42] * z[9];
z[10] = abb[54] * z[13];
z[6] = -abb[16] * z[6];
z[13] = abb[19] * z[62];
z[7] = abb[17] * z[7];
z[14] = -abb[16] + abb[19];
z[14] = abb[33] * z[14];
z[6] = z[6] + z[7] + z[13] + z[14];
z[6] = z[6] * z[11];
z[5] = z[5] * z[17];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[8] + (T(-1) / T(2)) * z[9] + z[10] + z[12] + z[15] + z[18] + z[23];
z[0] = (T(1) / T(4)) * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_125_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.18261390179162965070966596235193556372877237025680415802123344081"),stof<T>("-0.41328989670267373852583756807395615760041594725023174989765257276")}, std::complex<T>{stof<T>("-0.7427642594704529907122767972505745647449131640253382693566075413"),stof<T>("0.42147960722983093936755163546459096313518934983144720186388556736")}, std::complex<T>{stof<T>("-0.52786112220938496268302042740939925559122777496315819579897211427"),stof<T>("0.19291901075164879353821092131862393983768174888437731214661130931")}, std::complex<T>{stof<T>("-1.3212496324146752793794140156688067857013151975707890667213928086"),stof<T>("0.8434002737337653198416459035053544840998950619754997108718017326")}, std::complex<T>{stof<T>("-0.53740557817655476162789181485614721373158879241857911637505123907"),stof<T>("0.35398302307852721249066846790110541293463650256313271212367309096")}, std::complex<T>{stof<T>("0.56015035767882334000261083489863900101614079376853411133537410049"),stof<T>("-0.83476950393250467789338920353854712073560529708167895176153814012")}, std::complex<T>{stof<T>("0.45206819008996940444056879493693166461096524631315406723780644064"),stof<T>("-0.6183869553716871503642261524770373176934380471354809098273440296")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_125_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_125_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.02435556195130010557768050841578811653053069984159512999953262175"),stof<T>("0.40509873509489876701374346996009466991183351566969552760882937672")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W75(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_125_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_125_DLogXconstant_part(base_point<T>, kend);
	value += f_4_125_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_125_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_125_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_125_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_125_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_125_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_125_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
