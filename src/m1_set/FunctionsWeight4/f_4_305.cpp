/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_305.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_305_abbreviated (const std::array<T,54>& abb) {
T z[99];
z[0] = abb[44] + -abb[49];
z[1] = -abb[43] + z[0];
z[1] = abb[42] + (T(1) / T(2)) * z[1];
z[2] = abb[15] * z[1];
z[3] = abb[50] * (T(1) / T(2));
z[4] = abb[25] * z[3];
z[2] = z[2] + z[4];
z[4] = abb[43] + z[0];
z[5] = abb[5] * z[4];
z[6] = (T(1) / T(2)) * z[5];
z[7] = abb[21] + abb[23];
z[7] = abb[50] * z[7];
z[8] = (T(1) / T(2)) * z[7];
z[9] = -z[2] + z[6] + -z[8];
z[10] = abb[49] * (T(1) / T(4));
z[11] = abb[41] * (T(13) / T(6)) + -z[10];
z[12] = abb[44] * (T(1) / T(4));
z[13] = abb[45] + abb[46];
z[14] = abb[43] * (T(-43) / T(12)) + abb[42] * (T(13) / T(6)) + z[11] + -z[12] + (T(-5) / T(3)) * z[13];
z[14] = abb[3] * z[14];
z[15] = abb[49] * (T(1) / T(2));
z[16] = z[13] + z[15];
z[17] = abb[41] * (T(1) / T(2));
z[18] = abb[48] * (T(1) / T(2));
z[19] = abb[47] * (T(-1) / T(2)) + abb[43] * (T(1) / T(6)) + (T(1) / T(3)) * z[16] + z[17] + -z[18];
z[19] = abb[1] * z[19];
z[20] = abb[43] * (T(-23) / T(4)) + abb[49] * (T(7) / T(4)) + -4 * z[13];
z[12] = z[12] + z[18] + (T(1) / T(3)) * z[20];
z[12] = abb[7] * z[12];
z[20] = -abb[48] + z[13];
z[0] = -z[0] + z[20];
z[21] = abb[11] * z[0];
z[11] = abb[43] * (T(-23) / T(12)) + abb[44] * (T(-13) / T(4)) + abb[42] * (T(1) / T(3)) + abb[47] * (T(13) / T(2)) + -z[11] + (T(-4) / T(3)) * z[13];
z[11] = abb[8] * z[11];
z[22] = abb[43] + abb[49];
z[23] = -z[13] + z[22];
z[24] = abb[42] + (T(-1) / T(3)) * z[23];
z[24] = abb[2] * z[24];
z[25] = abb[42] + -abb[43];
z[26] = abb[12] * z[25];
z[27] = 3 * z[26];
z[28] = abb[42] + -abb[49];
z[29] = abb[48] + z[28];
z[29] = abb[9] * z[29];
z[30] = 3 * z[29];
z[28] = abb[41] + z[28];
z[31] = abb[0] * z[28];
z[11] = (T(-1) / T(2)) * z[9] + z[11] + z[12] + z[14] + 13 * z[19] + -3 * z[21] + z[24] + -z[27] + -z[30] + (T(13) / T(6)) * z[31];
z[11] = prod_pow(m1_set::bc<T>[0], 2) * z[11];
z[12] = abb[43] * (T(3) / T(2));
z[14] = z[12] + -z[15];
z[19] = abb[44] * (T(1) / T(2));
z[24] = z[14] + -z[19];
z[32] = 2 * abb[41];
z[33] = z[13] + z[32];
z[34] = 2 * abb[47];
z[35] = -z[24] + -z[33] + z[34];
z[35] = abb[34] * z[35];
z[36] = abb[43] * (T(1) / T(2));
z[37] = z[19] + z[36];
z[38] = -abb[41] + abb[47];
z[39] = -z[15] + z[37] + -z[38];
z[40] = -abb[33] * z[39];
z[41] = prod_pow(abb[31], 2);
z[42] = z[4] * z[41];
z[4] = (T(1) / T(2)) * z[4];
z[43] = abb[35] * z[4];
z[35] = z[35] + z[40] + (T(1) / T(4)) * z[42] + z[43];
z[35] = abb[3] * z[35];
z[24] = z[13] + z[24];
z[24] = abb[3] * z[24];
z[23] = 3 * abb[42] + -z[23];
z[40] = abb[2] * z[23];
z[42] = abb[8] * z[1];
z[42] = z[40] + z[42];
z[43] = abb[7] * z[4];
z[24] = -z[9] + z[24] + -z[42] + z[43];
z[43] = abb[53] * z[24];
z[44] = abb[22] * abb[50];
z[45] = abb[24] * abb[50];
z[46] = z[44] + z[45];
z[47] = z[7] + z[46];
z[48] = abb[20] * z[3];
z[49] = z[2] + -z[48];
z[47] = (T(1) / T(2)) * z[47] + -z[49];
z[50] = abb[49] * (T(3) / T(2));
z[51] = z[13] + z[50];
z[37] = 2 * abb[48] + z[37] + -z[51];
z[52] = abb[13] * z[37];
z[53] = -abb[43] + abb[44];
z[54] = abb[49] + z[53];
z[54] = -abb[48] + (T(1) / T(2)) * z[54];
z[55] = abb[4] * z[54];
z[56] = z[21] + z[55];
z[52] = z[52] + z[56];
z[42] = z[42] + z[47] + z[52];
z[57] = abb[51] * z[42];
z[58] = -abb[14] * z[39];
z[59] = abb[22] * z[3];
z[59] = z[58] + z[59];
z[53] = -z[13] + z[53];
z[60] = z[38] + z[53];
z[60] = abb[10] * z[60];
z[61] = z[59] + z[60];
z[16] = z[16] + z[36];
z[62] = abb[44] * (T(3) / T(2));
z[63] = -z[16] + z[62];
z[64] = abb[6] * z[63];
z[65] = z[8] + z[64];
z[66] = -z[61] + -z[65];
z[66] = abb[34] * z[66];
z[67] = -z[8] + -z[55];
z[67] = -z[21] + (T(1) / T(2)) * z[67];
z[67] = z[41] * z[67];
z[68] = -z[48] + z[55] + -z[59];
z[68] = abb[33] * z[68];
z[69] = abb[20] * abb[50];
z[70] = z[46] + z[69];
z[5] = -z[5] + z[70];
z[71] = (T(1) / T(2)) * z[5] + -z[55];
z[72] = abb[35] * z[71];
z[73] = abb[8] * z[63];
z[74] = (T(1) / T(2)) * z[41];
z[75] = abb[34] + -z[74];
z[75] = z[73] * z[75];
z[35] = z[35] + z[43] + -z[57] + z[66] + z[67] + z[68] + z[72] + z[75];
z[43] = z[7] + z[45];
z[43] = z[2] + (T(-1) / T(2)) * z[43] + -z[44] + -z[58] + -z[69];
z[44] = abb[42] * (T(1) / T(2));
z[57] = abb[44] * (T(3) / T(4));
z[66] = z[44] + z[57];
z[67] = abb[47] * (T(3) / T(2));
z[68] = abb[49] * (T(-5) / T(4)) + abb[43] * (T(3) / T(4)) + z[32] + z[66] + -z[67];
z[68] = abb[3] * z[68];
z[69] = -abb[49] + z[13];
z[72] = 2 * abb[43];
z[75] = z[69] + z[72];
z[76] = 3 * abb[41];
z[77] = 3 * abb[47];
z[78] = -z[75] + -z[76] + z[77];
z[79] = abb[1] * z[78];
z[15] = z[15] + z[62];
z[12] = abb[41] + z[12] + z[15];
z[80] = abb[42] + z[67];
z[12] = (T(1) / T(2)) * z[12] + -z[80];
z[12] = abb[8] * z[12];
z[81] = (T(1) / T(2)) * z[31];
z[12] = z[12] + -z[40] + (T(3) / T(2)) * z[43] + -z[68] + z[79] + -z[81];
z[12] = abb[27] * z[12];
z[43] = z[8] + z[48];
z[61] = z[43] + z[61] + -z[79];
z[68] = 2 * abb[3];
z[82] = z[68] * z[78];
z[83] = abb[13] * z[63];
z[84] = abb[7] * z[63];
z[83] = z[83] + -z[84];
z[82] = z[73] + z[82] + -z[83];
z[61] = 3 * z[61] + -z[82];
z[61] = abb[28] * z[61];
z[85] = abb[27] * z[63];
z[86] = abb[31] * z[63];
z[87] = z[85] + z[86];
z[87] = abb[13] * z[87];
z[12] = z[12] + z[61] + -z[87];
z[61] = abb[43] * (T(5) / T(4));
z[88] = -z[57] + z[61];
z[89] = z[13] + z[88];
z[90] = abb[49] * (T(3) / T(4));
z[17] = z[17] + -z[80] + z[89] + z[90];
z[17] = abb[8] * z[17];
z[80] = z[40] + 2 * z[79];
z[58] = z[2] + z[58];
z[45] = z[7] + -z[45];
z[45] = (T(1) / T(2)) * z[45] + z[58];
z[45] = (T(1) / T(2)) * z[45] + z[60];
z[91] = 4 * abb[41];
z[92] = 2 * z[13];
z[93] = z[91] + z[92];
z[94] = abb[47] * (T(-9) / T(2)) + abb[43] * (T(13) / T(4)) + -z[66] + -z[90] + z[93];
z[94] = abb[3] * z[94];
z[17] = z[17] + 3 * z[45] + -z[80] + -z[81] + -z[84] + z[94];
z[17] = abb[29] * z[17];
z[45] = -abb[42] + z[50];
z[50] = abb[41] + abb[44] * (T(9) / T(2)) + -z[13] + -z[36] + -z[45] + -z[77];
z[50] = abb[8] * z[50];
z[65] = -z[59] + -z[65];
z[50] = z[50] + 3 * z[65] + z[84];
z[65] = -z[33] + z[90];
z[88] = -z[44] + z[65] + z[67] + -z[88];
z[88] = abb[3] * z[88];
z[90] = (T(1) / T(2)) * z[40];
z[79] = z[79] + z[90];
z[50] = (T(1) / T(2)) * z[50] + z[79] + -z[81] + z[88];
z[50] = abb[30] * z[50];
z[46] = (T(1) / T(2)) * z[46];
z[49] = z[46] + -z[49];
z[23] = abb[8] * z[23];
z[23] = z[23] + 3 * z[40];
z[81] = abb[3] * z[63];
z[49] = z[23] + 3 * z[49] + z[81];
z[83] = z[49] + z[83];
z[88] = abb[32] * z[83];
z[94] = 3 * abb[44];
z[95] = -z[22] + -z[92] + z[94];
z[96] = -abb[7] + -abb[8];
z[95] = abb[31] * z[95] * z[96];
z[96] = abb[3] * z[86];
z[97] = -z[48] + z[64];
z[98] = 3 * abb[31];
z[97] = z[97] * z[98];
z[17] = z[12] + z[17] + z[50] + z[88] + z[95] + -z[96] + z[97];
z[17] = abb[30] * z[17];
z[50] = z[61] + -z[65] + z[66] + -z[77];
z[50] = abb[8] * z[50];
z[57] = -2 * abb[42] + abb[43] * (T(7) / T(4)) + -z[32] + (T(1) / T(2)) * z[51] + z[57];
z[57] = abb[3] * z[57];
z[10] = -z[10] + z[89];
z[10] = abb[7] * z[10];
z[10] = (T(3) / T(2)) * z[9] + z[10] + z[27] + z[31] + z[50] + z[57] + z[79];
z[10] = abb[29] * z[10];
z[5] = z[5] + z[7];
z[27] = abb[31] * (T(3) / T(2));
z[5] = z[5] * z[27];
z[27] = abb[7] + z[68];
z[27] = abb[31] * z[27] * z[75];
z[50] = abb[31] * z[73];
z[5] = z[5] + z[27] + -z[50];
z[10] = z[5] + z[10] + -z[12];
z[10] = abb[29] * z[10];
z[12] = abb[24] * z[3];
z[12] = z[12] + z[55] + -z[58];
z[27] = abb[49] + z[13];
z[44] = -abb[41] + (T(1) / T(2)) * z[27] + -z[36] + z[44];
z[44] = abb[8] * z[44];
z[27] = -abb[41] + -z[27];
z[25] = z[25] + (T(1) / T(2)) * z[27] + z[67];
z[25] = abb[3] * z[25];
z[27] = 2 * z[31];
z[12] = (T(3) / T(2)) * z[12] + z[25] + -z[27] + z[30] + z[44] + z[90];
z[12] = abb[27] * z[12];
z[8] = z[8] + z[56];
z[25] = -z[8] * z[98];
z[12] = z[12] + z[25] + -z[50] + z[96];
z[12] = abb[27] * z[12];
z[7] = -z[7] + -z[55] + -z[59] + z[64];
z[18] = -z[13] + -z[14] + z[18] + (T(5) / T(2)) * z[38];
z[18] = abb[1] * z[18];
z[0] = (T(1) / T(2)) * z[0];
z[25] = abb[13] * z[0];
z[30] = -z[69] + -z[76];
z[30] = -abb[43] + (T(1) / T(2)) * z[30] + z[67];
z[30] = abb[3] * z[30];
z[7] = (T(1) / T(2)) * z[7] + z[18] + z[25] + z[30] + -z[48] + -z[60];
z[7] = abb[28] * z[7];
z[18] = z[43] + -z[64];
z[30] = z[18] + z[56];
z[44] = abb[31] * z[30];
z[7] = z[7] + z[44] + z[50];
z[38] = abb[43] + abb[48] + -abb[49] + -z[38];
z[44] = abb[1] * z[38];
z[43] = z[21] + z[43] + z[44] + z[59];
z[44] = -abb[3] * z[78];
z[43] = 3 * z[43] + z[44] + z[73];
z[43] = abb[27] * z[43];
z[44] = 3 * abb[48];
z[48] = abb[49] * (T(-5) / T(2)) + z[36] + z[44] + z[62] + -z[92];
z[48] = abb[27] * z[48];
z[50] = z[37] * z[98];
z[48] = z[48] + z[50];
z[48] = abb[13] * z[48];
z[50] = z[85] + 3 * z[86];
z[50] = abb[7] * z[50];
z[7] = 3 * z[7] + z[43] + z[48] + z[50];
z[7] = abb[28] * z[7];
z[43] = abb[3] * z[53];
z[6] = -z[2] + -z[6] + z[23] + z[43] + -z[55] + z[70];
z[0] = abb[7] * z[0];
z[0] = z[0] + (T(1) / T(2)) * z[6] + -z[25] + -z[26] + -z[29];
z[0] = abb[32] * z[0];
z[4] = -abb[3] * z[4];
z[4] = z[4] + -z[71];
z[4] = abb[31] * z[4];
z[0] = z[0] + z[4];
z[4] = -abb[27] * z[49];
z[6] = -abb[29] * z[83];
z[23] = z[54] * z[98];
z[23] = z[23] + z[85];
z[25] = abb[7] + -abb[13];
z[23] = z[23] * z[25];
z[0] = 3 * z[0] + z[4] + z[6] + z[23];
z[0] = abb[32] * z[0];
z[4] = abb[48] * (T(3) / T(2));
z[6] = -abb[49] + z[4] + (T(-1) / T(2)) * z[13] + z[36];
z[23] = -z[6] * z[41];
z[25] = -abb[33] + abb[35];
z[25] = z[25] * z[54];
z[23] = z[23] + z[25];
z[6] = -abb[27] * z[6];
z[13] = -abb[43] + 2 * abb[49] + z[13] + -z[44];
z[13] = 2 * z[13];
z[25] = abb[31] * z[13];
z[6] = z[6] + z[25];
z[6] = abb[27] * z[6];
z[25] = 3 * abb[52];
z[36] = z[25] * z[37];
z[6] = z[6] + 3 * z[23] + z[36];
z[6] = abb[13] * z[6];
z[23] = z[73] + z[84];
z[36] = abb[13] * z[13];
z[8] = -3 * z[8] + -z[23] + z[36] + z[81];
z[8] = abb[36] * z[8];
z[36] = abb[34] * z[78];
z[37] = -abb[33] * z[38];
z[36] = z[36] + z[37];
z[4] = abb[41] * (T(-3) / T(2)) + z[4] + -z[16] + z[67];
z[4] = prod_pow(abb[27], 2) * z[4];
z[4] = z[4] + 3 * z[36];
z[4] = abb[1] * z[4];
z[36] = abb[34] * z[63];
z[16] = -abb[48] + z[16] + -z[19];
z[16] = z[16] * z[74];
z[19] = -abb[35] * z[54];
z[16] = z[16] + z[19] + z[36];
z[19] = (T(-1) / T(2)) * z[85] + -z[86];
z[19] = abb[27] * z[19];
z[16] = 3 * z[16] + z[19];
z[16] = abb[7] * z[16];
z[19] = z[23] + z[30];
z[19] = z[19] * z[25];
z[25] = abb[16] + abb[18];
z[25] = z[25] * z[39];
z[22] = abb[44] + z[22];
z[22] = -abb[42] + -abb[47] + (T(1) / T(2)) * z[22];
z[22] = abb[17] * z[22];
z[3] = abb[26] * z[3];
z[1] = abb[19] * z[1];
z[1] = z[1] + z[3] + z[22] + z[25];
z[1] = abb[37] * z[1];
z[0] = z[0] + (T(3) / T(2)) * z[1] + z[4] + z[6] + z[7] + z[8] + z[10] + z[11] + z[12] + z[16] + z[17] + z[19] + 3 * z[35];
z[1] = -z[47] + -z[56];
z[3] = -abb[42] + z[14] + z[32] + -z[62];
z[3] = abb[8] * z[3];
z[4] = -z[28] * z[68];
z[6] = abb[1] * z[13];
z[1] = 3 * z[1] + z[3] + z[4] + z[6] + -6 * z[29] + 4 * z[31] + -z[40];
z[1] = abb[27] * z[1];
z[3] = abb[43] * (T(5) / T(2));
z[4] = 6 * abb[47];
z[6] = -z[3] + z[4] + z[45] + -z[62] + -z[93];
z[6] = abb[8] * z[6];
z[7] = z[27] + z[80];
z[8] = 4 * abb[42] + abb[43] * (T(-7) / T(2)) + -z[51] + -z[62] + z[91];
z[8] = abb[3] * z[8];
z[3] = -z[3] + z[15] + -z[92];
z[3] = abb[7] * z[3];
z[3] = z[3] + z[6] + -z[7] + z[8] + -3 * z[9] + -6 * z[26];
z[3] = abb[29] * z[3];
z[6] = 2 * z[60];
z[2] = -z[2] + -z[6] + z[46] + z[64];
z[8] = -abb[42] + z[33] + z[72];
z[4] = z[4] + -z[8] + -z[94];
z[4] = abb[8] * z[4];
z[8] = -z[8] + z[77];
z[8] = z[8] * z[68];
z[2] = 3 * z[2] + z[4] + z[7] + z[8] + z[84];
z[2] = abb[30] * z[2];
z[4] = z[6] + z[18] + -z[21] + z[55];
z[6] = abb[43] + z[20] + z[32] + -z[34];
z[6] = abb[1] * z[6];
z[4] = 3 * z[4] + 6 * z[6] + -z[82];
z[4] = abb[28] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4] + -z[5] + -z[87] + z[88];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = z[18] + z[23] + z[52];
z[2] = abb[39] * z[2];
z[3] = -abb[38] * z[42];
z[4] = abb[40] * z[24];
z[2] = z[2] + z[3] + z[4];
z[1] = z[1] + 3 * z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_305_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.22705487121682531585184216070836765633796551931412506268836417"),stof<T>("-83.781197666787351751956231260651738209094290445805215079939375928")}, std::complex<T>{stof<T>("-6.82070293910296763206809565657268841048751017346517091363243205"),stof<T>("-48.273888092845965450964722256267332118280718923943817868695212391")}, std::complex<T>{stof<T>("-6.487778227773556684859704839822798395323266192295462149335665204"),stof<T>("50.985310504584355545386129673624559246764242442805024950460033881")}, std::complex<T>{stof<T>("-9.254686010775785804542390477783609725658270984679616371327311634"),stof<T>("-15.55469254458090349004541917808688501560192144786321451017521826")}, std::complex<T>{stof<T>("-7.184124888314354108986160328487616470637305501138732813068445575"),stof<T>("18.6213156475939604861182307099586211964636631965117156096363888")}, std::complex<T>{stof<T>("-7.184124888314354108986160328487616470637305501138732813068445575"),stof<T>("18.6213156475939604861182307099586211964636631965117156096363888")}, std::complex<T>{stof<T>("38.780699012426523743089114106068010223173683305746504857014498087"),stof<T>("15.712188923077182853362767695481843777380915081787966261904006313")}, std::complex<T>{stof<T>("-43.693299263115379353354045628413022018389543203864180985206756727"),stof<T>("49.092492404841541510823828740696764182871493937718239190702631764")}, std::complex<T>{stof<T>("6.124356278562170207941640167907870335173470864621900249899651679"),stof<T>("15.90989323585557039169682329260139406798013967765050852787156731")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_305_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_305_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("41.335149241672406577830469226200241270730304505594626944805218864"),stof<T>("-4.110441131842011108727343692266235889087615146785555234126705769")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,54> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k)};

                    
            return f_4_305_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_305_DLogXconstant_part(base_point<T>, kend);
	value += f_4_305_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_305_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_305_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_305_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_305_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_305_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_305_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
