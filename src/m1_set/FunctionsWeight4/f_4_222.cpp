/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_222.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_222_abbreviated (const std::array<T,59>& abb) {
T z[117];
z[0] = -abb[42] + abb[43];
z[1] = abb[44] + z[0];
z[2] = abb[48] * (T(1) / T(2));
z[1] = (T(1) / T(2)) * z[1] + -z[2];
z[3] = -abb[46] + z[1];
z[4] = abb[40] + -abb[47];
z[5] = abb[45] * (T(1) / T(6));
z[6] = abb[49] * (T(1) / T(2));
z[7] = abb[41] * (T(-1) / T(3)) + -z[3] + (T(-2) / T(3)) * z[4] + -z[5] + -z[6];
z[7] = abb[6] * z[7];
z[8] = abb[45] * (T(1) / T(2));
z[9] = abb[46] + z[8];
z[10] = -z[6] + z[9];
z[1] = -abb[41] + -z[1] + z[10];
z[11] = abb[14] * z[1];
z[12] = abb[51] + abb[52] + abb[53] + abb[55];
z[13] = abb[54] + (T(1) / T(2)) * z[12];
z[14] = abb[20] * z[13];
z[15] = abb[23] * z[13];
z[16] = z[14] + z[15];
z[17] = abb[24] * z[13];
z[18] = -z[16] + z[17];
z[19] = -z[11] + z[18];
z[20] = 2 * abb[43];
z[21] = abb[42] * (T(5) / T(2)) + z[20];
z[22] = abb[50] * (T(11) / T(3));
z[23] = abb[48] * (T(5) / T(2));
z[21] = abb[49] + (T(1) / T(3)) * z[21] + -z[22] + -z[23];
z[24] = abb[46] * (T(9) / T(2));
z[25] = abb[41] * (T(-2) / T(3)) + -z[21] + -z[24];
z[25] = abb[0] * z[25];
z[26] = -abb[41] + z[4];
z[27] = abb[42] + abb[48];
z[28] = -abb[43] + abb[44] + z[27];
z[29] = abb[45] * (T(-1) / T(3)) + z[6] + (T(-5) / T(6)) * z[26] + (T(-1) / T(2)) * z[28];
z[29] = abb[5] * z[29];
z[30] = abb[48] * (T(3) / T(2));
z[31] = -abb[43] + abb[42] * (T(11) / T(2));
z[22] = abb[41] * (T(-13) / T(6)) + abb[46] * (T(11) / T(2)) + abb[45] * (T(11) / T(6)) + -z[22] + -z[30] + (T(1) / T(3)) * z[31];
z[22] = abb[2] * z[22];
z[21] = abb[41] * (T(-10) / T(3)) + abb[44] * (T(-9) / T(2)) + abb[45] * (T(8) / T(3)) + -z[21];
z[21] = abb[8] * z[21];
z[31] = abb[0] + abb[11];
z[31] = abb[40] * z[31];
z[32] = abb[0] * abb[47];
z[31] = z[31] + -z[32];
z[32] = abb[42] + abb[43];
z[33] = abb[44] + z[32];
z[34] = abb[45] + -abb[48];
z[35] = z[33] + z[34];
z[35] = z[4] + (T(1) / T(2)) * z[35];
z[36] = -z[6] + z[35];
z[37] = abb[12] * z[36];
z[38] = abb[21] * z[13];
z[39] = z[37] + z[38];
z[40] = abb[43] + z[34];
z[41] = z[4] + z[40];
z[42] = abb[1] * z[41];
z[43] = abb[44] + (T(-1) / T(3)) * z[0];
z[5] = abb[50] * (T(-1) / T(3)) + abb[47] * (T(-1) / T(6)) + -z[5] + (T(1) / T(2)) * z[43];
z[5] = abb[11] * z[5];
z[12] = -2 * abb[54] + -z[12];
z[12] = abb[22] * z[12];
z[5] = 11 * z[5] + z[7] + z[12] + z[19] + z[21] + z[22] + z[25] + z[29] + (T(11) / T(6)) * z[31] + -z[39] + (T(7) / T(2)) * z[42];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[7] = 3 * abb[46];
z[12] = abb[48] + z[32];
z[21] = z[7] + -z[12];
z[22] = abb[41] * (T(1) / T(2));
z[25] = 2 * abb[50];
z[29] = z[22] + z[25];
z[21] = (T(1) / T(2)) * z[21] + -z[29];
z[21] = abb[0] * z[21];
z[43] = 2 * z[42];
z[44] = abb[49] * (T(3) / T(2));
z[45] = abb[0] * z[44];
z[21] = z[21] + (T(3) / T(2)) * z[39] + -z[43] + z[45];
z[39] = 5 * abb[42];
z[45] = 5 * abb[48];
z[46] = z[39] + z[45];
z[47] = 3 * abb[44];
z[48] = abb[43] + -z[46] + z[47];
z[49] = abb[45] * (T(3) / T(4));
z[50] = (T(-1) / T(2)) * z[4];
z[29] = abb[49] * (T(9) / T(4)) + -z[29] + (T(1) / T(4)) * z[48] + z[49] + -z[50];
z[29] = abb[5] * z[29];
z[48] = abb[49] * (T(3) / T(4));
z[51] = z[48] + z[49];
z[39] = 13 * abb[43] + -z[39] + -z[47];
z[39] = -9 * abb[46] + -z[23] + (T(1) / T(2)) * z[39];
z[52] = 4 * abb[50];
z[39] = 4 * abb[41] + (T(1) / T(2)) * z[39] + -z[50] + -z[51] + z[52];
z[39] = abb[6] * z[39];
z[12] = z[12] + -z[47];
z[53] = 3 * abb[49];
z[54] = abb[45] + z[12] + z[52] + -z[53];
z[54] = abb[15] * z[54];
z[55] = -z[25] + z[44];
z[56] = -z[8] + (T(-1) / T(2)) * z[12] + z[55];
z[57] = abb[13] * z[56];
z[58] = -abb[49] + z[25];
z[59] = abb[44] + abb[46];
z[60] = -z[58] + z[59];
z[61] = abb[41] + abb[43];
z[62] = z[60] + -z[61];
z[63] = 3 * abb[9];
z[62] = z[62] * z[63];
z[62] = z[57] + z[62];
z[64] = (T(3) / T(2)) * z[13];
z[65] = abb[24] * z[64];
z[65] = (T(3) / T(2)) * z[11] + -z[65];
z[15] = (T(3) / T(2)) * z[15];
z[64] = abb[20] * z[64];
z[66] = z[15] + z[64] + z[65];
z[67] = -abb[45] + -abb[47] + -z[0] + -z[25] + z[47];
z[67] = abb[11] * z[67];
z[31] = z[31] + z[67];
z[20] = z[20] + -z[27];
z[67] = z[7] + -z[25];
z[68] = abb[45] + z[67];
z[69] = 3 * abb[41];
z[70] = z[20] + -z[68] + z[69];
z[71] = abb[2] * z[70];
z[29] = -z[21] + z[29] + z[31] + z[39] + z[54] + z[62] + z[66] + 2 * z[71];
z[29] = abb[26] * z[29];
z[39] = z[66] + z[71];
z[66] = abb[45] * (T(5) / T(2));
z[72] = 3 * z[3] + -z[66];
z[73] = 2 * abb[41];
z[50] = z[50] + z[73];
z[72] = z[48] + z[50] + (T(1) / T(2)) * z[72];
z[72] = abb[6] * z[72];
z[74] = 5 * abb[45] + -3 * z[28];
z[74] = z[26] + z[44] + (T(1) / T(2)) * z[74];
z[75] = abb[5] * (T(1) / T(2));
z[74] = z[74] * z[75];
z[76] = 3 * z[13];
z[77] = abb[22] * z[76];
z[78] = z[31] + -z[77];
z[79] = abb[8] * z[56];
z[21] = z[21] + z[39] + z[72] + -z[74] + -z[78] + z[79];
z[72] = abb[30] * z[21];
z[74] = 3 * abb[43];
z[80] = -5 * abb[44] + -z[27] + z[74];
z[81] = z[6] + -z[25];
z[82] = abb[45] * (T(3) / T(2)) + (T(1) / T(2)) * z[80] + -z[81];
z[83] = 3 * abb[7];
z[84] = z[82] * z[83];
z[85] = -abb[28] + abb[29];
z[86] = z[84] * z[85];
z[72] = -z[72] + z[86];
z[86] = abb[5] * z[56];
z[87] = abb[23] * z[76];
z[86] = z[86] + z[87];
z[88] = z[54] + z[86];
z[89] = abb[20] * z[76];
z[90] = z[88] + z[89];
z[91] = 3 * z[11];
z[92] = abb[24] * z[76];
z[92] = -z[90] + -z[91] + z[92];
z[93] = 2 * abb[6];
z[70] = z[70] * z[93];
z[62] = z[62] + z[70] + 3 * z[71] + z[77] + z[79] + -z[92];
z[70] = -abb[31] * z[62];
z[94] = z[31] + -z[71];
z[95] = z[20] + -z[67] + z[73];
z[96] = abb[0] * z[95];
z[97] = 2 * abb[45];
z[98] = z[25] + -z[47] + z[97];
z[20] = z[20] + z[98];
z[99] = abb[8] * z[20];
z[100] = -abb[5] + z[93];
z[26] = abb[45] + z[26];
z[100] = z[26] * z[100];
z[96] = -z[42] + z[94] + z[96] + z[99] + z[100];
z[96] = abb[27] * z[96];
z[99] = 2 * abb[8];
z[93] = z[93] + z[99];
z[20] = z[20] * z[93];
z[20] = z[20] + z[57] + z[88];
z[20] = z[20] * z[85];
z[20] = z[20] + z[29] + z[70] + -z[72] + z[96];
z[20] = abb[27] * z[20];
z[29] = 3 * z[37];
z[70] = -abb[44] + z[32];
z[70] = -z[2] + (T(1) / T(2)) * z[70];
z[9] = z[9] + z[70];
z[9] = abb[3] * z[9];
z[85] = 3 * abb[42];
z[93] = z[7] + z[85];
z[96] = -abb[48] + z[61];
z[100] = z[93] + z[96];
z[100] = abb[0] * z[100];
z[101] = abb[0] + abb[3] * (T(1) / T(2));
z[102] = -z[53] * z[101];
z[77] = 3 * z[9] + -z[29] + -z[77] + z[100] + z[102];
z[100] = abb[15] * z[56];
z[102] = z[42] + z[100];
z[103] = -abb[42] + -abb[50] + z[44];
z[104] = abb[45] + abb[43] * (T(1) / T(2)) + (T(3) / T(2)) * z[4] + -z[22] + -z[103];
z[104] = abb[5] * z[104];
z[105] = abb[45] * (T(1) / T(4));
z[12] = abb[50] + (T(1) / T(4)) * z[12] + -z[48] + z[105];
z[12] = abb[8] * z[12];
z[106] = -abb[50] + z[2];
z[107] = abb[42] * (T(1) / T(2));
z[108] = -abb[43] + abb[45] + abb[46] * (T(3) / T(2)) + -z[50] + z[106] + z[107];
z[108] = abb[6] * z[108];
z[109] = abb[48] + -z[59];
z[109] = abb[50] + -z[6] + (T(1) / T(2)) * z[109];
z[109] = z[63] * z[109];
z[12] = z[12] + -z[65] + -z[71] + (T(1) / T(2)) * z[77] + z[102] + z[104] + z[108] + z[109];
z[12] = abb[26] * z[12];
z[65] = abb[0] * z[53];
z[76] = abb[21] * z[76];
z[29] = z[29] + z[65] + z[76];
z[45] = z[45] + -z[47];
z[65] = 5 * abb[43];
z[76] = -3 * abb[45] + z[45] + -z[65] + -z[85];
z[76] = -abb[41] + -3 * z[4] + z[44] + (T(1) / T(2)) * z[76];
z[76] = abb[5] * z[76];
z[77] = -z[93] + z[96];
z[77] = abb[0] * z[77];
z[76] = z[29] + z[76] + z[77];
z[65] = -z[47] + z[65];
z[77] = abb[42] + -z[65];
z[77] = z[2] + z[7] + (T(1) / T(2)) * z[77];
z[50] = -z[25] + -z[50] + z[51] + (T(1) / T(2)) * z[77];
z[50] = abb[6] * z[50];
z[39] = -z[39] + -z[43] + z[50] + -z[57] + (T(1) / T(2)) * z[76];
z[39] = abb[30] * z[39];
z[50] = 2 * abb[42] + z[25];
z[76] = abb[48] + z[50];
z[76] = abb[0] * z[76];
z[29] = -z[29] + 4 * z[42] + z[76] + z[78];
z[56] = abb[6] * z[56];
z[76] = z[56] + z[79];
z[77] = 2 * abb[5];
z[41] = z[41] * z[77];
z[41] = z[29] + z[41] + z[57] + -z[76];
z[41] = abb[28] * z[41];
z[78] = 2 * abb[48];
z[79] = -z[32] + z[78];
z[68] = -z[68] + z[79];
z[68] = abb[13] * z[68];
z[85] = 2 * z[68];
z[76] = z[76] + z[85];
z[93] = abb[3] * z[6];
z[93] = -z[9] + z[93];
z[104] = abb[22] * z[13];
z[108] = z[38] + z[104];
z[109] = z[93] + -z[108];
z[110] = -abb[48] + z[60];
z[111] = z[63] * z[110];
z[112] = z[76] + -3 * z[109] + z[111];
z[90] = -z[90] + -z[112];
z[90] = abb[29] * z[90];
z[12] = z[12] + z[39] + -z[41] + z[90];
z[12] = abb[26] * z[12];
z[39] = -z[25] + z[73];
z[90] = abb[42] + -abb[48] + z[47];
z[113] = z[39] + z[90] + -z[97];
z[113] = abb[8] * z[113];
z[50] = z[50] + -z[53];
z[34] = -abb[41] + z[34];
z[53] = -z[34] + z[50];
z[114] = -abb[5] * z[53];
z[115] = abb[42] + z[67];
z[116] = -z[34] + -z[115];
z[116] = abb[2] * z[116];
z[68] = z[54] + z[68] + z[111] + z[113] + z[114] + z[116];
z[68] = abb[29] * z[68];
z[62] = abb[26] * z[62];
z[24] = -5 * abb[41] + abb[48] + z[24] + z[44] + -z[52] + -z[74] + z[97] + z[107];
z[24] = abb[2] * z[24];
z[22] = z[2] + -z[8] + z[22] + -z[103];
z[22] = abb[5] * z[22];
z[22] = z[22] + z[100];
z[52] = -z[32] + -z[47];
z[23] = -z[7] + z[23] + (T(1) / T(2)) * z[52];
z[23] = (T(1) / T(2)) * z[23] + z[25] + -z[48] + -z[105];
z[23] = abb[13] * z[23];
z[52] = -z[6] + (T(-1) / T(2)) * z[59] + z[61] + -z[106];
z[52] = z[52] * z[63];
z[59] = -abb[42] + z[74];
z[97] = -abb[44] + z[59];
z[100] = -abb[48] + z[97];
z[103] = -abb[50] + abb[49] * (T(1) / T(4));
z[100] = -abb[41] + abb[46] + (T(-1) / T(4)) * z[100] + z[103] + z[105];
z[100] = abb[6] * z[100];
z[105] = abb[45] + abb[48];
z[106] = z[33] + -z[105];
z[106] = abb[41] + -z[6] + (T(1) / T(2)) * z[106];
z[107] = abb[4] * z[106];
z[111] = z[38] + z[107];
z[90] = -abb[41] + abb[45] + abb[50] + (T(-1) / T(2)) * z[90];
z[90] = abb[8] * z[90];
z[23] = z[22] + z[23] + z[24] + z[52] + z[90] + 3 * z[100] + (T(3) / T(2)) * z[111];
z[23] = abb[31] * z[23];
z[24] = z[56] + 3 * z[111];
z[52] = z[77] + z[99];
z[52] = z[52] * z[96];
z[53] = abb[2] * z[53];
z[52] = -z[24] + z[52] + z[53] + z[57];
z[53] = abb[30] * z[52];
z[23] = z[23] + z[53] + z[62] + z[68];
z[23] = abb[31] * z[23];
z[53] = 2 * abb[46];
z[56] = z[8] + z[53] + z[81];
z[2] = z[2] + z[56] + -z[73] + (T(-1) / T(2)) * z[97];
z[2] = abb[6] * z[2];
z[30] = -z[30] + (T(1) / T(2)) * z[33] + z[56];
z[30] = abb[13] * z[30];
z[33] = abb[9] * z[96];
z[2] = z[2] + -z[11] + z[17] + -z[30] + z[33] + z[38] + -z[71] + -z[93];
z[2] = abb[32] * z[2];
z[10] = z[10] + z[70];
z[10] = abb[13] * z[10];
z[10] = z[10] + -z[16];
z[11] = abb[5] * z[36];
z[16] = abb[42] + abb[46];
z[16] = abb[0] * z[16];
z[17] = -abb[49] * z[101];
z[9] = z[9] + -z[10] + z[11] + z[16] + z[17] + -z[37] + z[42];
z[9] = abb[33] * z[9];
z[11] = -abb[6] + -abb[8];
z[11] = z[11] * z[82];
z[16] = abb[9] * z[110];
z[11] = z[11] + z[14] + z[16] + -z[30] + -z[109];
z[11] = abb[34] * z[11];
z[14] = z[93] + -z[104] + z[107];
z[16] = abb[5] * z[106];
z[17] = -abb[46] + abb[49];
z[30] = -abb[42] + z[17];
z[33] = -abb[2] * z[30];
z[28] = -abb[45] + -abb[49] + z[28];
z[36] = abb[8] * z[28];
z[10] = -z[10] + -z[14] + z[16] + z[33] + (T(-1) / T(2)) * z[36];
z[10] = abb[35] * z[10];
z[2] = z[2] + z[9] + z[10] + z[11];
z[9] = abb[19] * z[1];
z[0] = abb[44] + -z[0] + z[105];
z[0] = -abb[41] + (T(1) / T(2)) * z[0];
z[0] = abb[17] * z[0];
z[10] = abb[16] + abb[17];
z[10] = z[4] * z[10];
z[11] = -abb[16] + abb[17] + abb[18];
z[6] = z[6] * z[11];
z[3] = z[3] + z[8];
z[3] = abb[16] * z[3];
z[11] = abb[18] * z[35];
z[13] = abb[25] * z[13];
z[0] = z[0] + z[3] + -z[6] + z[9] + z[10] + z[11] + -z[13];
z[3] = -abb[58] * z[0];
z[6] = -z[45] + z[59];
z[6] = -abb[50] + z[4] + (T(1) / T(4)) * z[6] + z[51];
z[6] = abb[5] * z[6];
z[9] = -5 * z[32] + z[47];
z[10] = abb[45] * (T(5) / T(4)) + -z[48];
z[9] = abb[50] + abb[48] * (T(7) / T(4)) + -z[7] + (T(1) / T(4)) * z[9] + -z[10];
z[9] = abb[13] * z[9];
z[11] = -abb[48] + z[115];
z[11] = abb[0] * z[11];
z[13] = -abb[44] + abb[46] + z[40];
z[13] = abb[10] * z[13];
z[16] = 3 * z[13];
z[6] = z[6] + z[9] + z[11] + z[15] + z[16] + -z[31] + -z[102];
z[9] = prod_pow(abb[28], 2);
z[6] = z[6] * z[9];
z[11] = abb[42] + -z[7];
z[11] = (T(1) / T(2)) * z[11] + -z[34] + -z[55];
z[11] = abb[2] * z[11];
z[15] = 9 * abb[44];
z[33] = abb[42] + -7 * abb[48] + z[15] + z[74];
z[10] = -z[10] + (T(1) / T(4)) * z[33] + z[39];
z[10] = abb[8] * z[10];
z[7] = z[7] + z[32];
z[7] = -abb[48] + -abb[50] + (T(1) / T(2)) * z[7] + z[8];
z[7] = abb[13] * z[7];
z[7] = z[7] + z[10] + z[11] + (T(-3) / T(2)) * z[14] + -z[16] + z[22] + z[64];
z[8] = prod_pow(abb[29], 2);
z[7] = z[7] * z[8];
z[10] = abb[42] + -3 * abb[48] + z[65];
z[10] = 2 * z[4] + (T(1) / T(2)) * z[10] + -z[55] + z[66];
z[10] = abb[5] * z[10];
z[11] = 7 * abb[43] + -z[15] + -z[46];
z[11] = abb[45] * (T(7) / T(2)) + (T(1) / T(2)) * z[11] + z[25] + z[44];
z[14] = abb[8] * z[11];
z[11] = abb[6] * z[11];
z[10] = z[10] + -z[11] + -z[14] + z[29] + -z[87];
z[11] = abb[56] * z[10];
z[14] = z[17] + -z[27] + z[61];
z[14] = abb[2] * z[14];
z[1] = abb[6] * z[1];
z[15] = abb[8] * z[106];
z[16] = z[28] * z[75];
z[17] = z[104] + z[111];
z[1] = -z[1] + z[14] + -z[15] + z[16] + z[17];
z[14] = -z[1] + z[18];
z[14] = 3 * z[14] + -z[91];
z[14] = abb[57] * z[14];
z[15] = -abb[29] * z[52];
z[16] = -abb[0] + abb[2] + -abb[8];
z[16] = z[16] * z[96];
z[18] = -abb[6] * z[26];
z[16] = z[16] + z[18] + z[42];
z[16] = abb[30] * z[16];
z[15] = z[15] + z[16] + z[41];
z[15] = abb[30] * z[15];
z[16] = -z[86] + -z[89] + -z[112];
z[16] = abb[36] * z[16];
z[18] = -z[49] + (T(-1) / T(4)) * z[80] + z[103];
z[8] = -z[8] + z[9];
z[8] = z[8] * z[18];
z[9] = abb[34] + abb[56];
z[9] = z[9] * z[82];
z[8] = z[8] + z[9];
z[8] = z[8] * z[83];
z[9] = -abb[36] + -abb[56];
z[9] = z[9] * z[54];
z[2] = 3 * z[2] + (T(3) / T(2)) * z[3] + z[5] + z[6] + z[7] + z[8] + z[9] + z[11] + z[12] + z[14] + z[15] + z[16] + z[20] + z[23];
z[3] = -z[61] + z[78];
z[5] = z[3] + -z[60];
z[5] = z[5] * z[63];
z[6] = -abb[41] + -z[67] + z[79];
z[6] = abb[0] * z[6];
z[3] = -z[3] + -z[4] + -z[50];
z[3] = abb[5] * z[3];
z[4] = -z[4] + -z[95];
z[4] = abb[6] * z[4];
z[3] = z[3] + z[4] + -z[5] + z[6] + z[43] + z[54] + z[85] + z[94];
z[3] = abb[26] * z[3];
z[4] = abb[27] * z[21];
z[6] = -z[53] + z[58];
z[7] = abb[48] + z[6];
z[7] = abb[0] * z[7];
z[7] = z[7] + z[31] + -z[37] + -z[108];
z[8] = 6 * z[13] + z[88];
z[7] = 3 * z[7] + -z[8] + 6 * z[42] + -z[76];
z[7] = abb[28] * z[7];
z[9] = -z[69] + z[79] + z[98];
z[9] = z[9] * z[99];
z[6] = -z[6] + z[34];
z[11] = 3 * abb[2];
z[6] = z[6] * z[11];
z[6] = z[6] + z[8] + z[9] + z[24] + z[85];
z[6] = abb[29] * z[6];
z[8] = -z[11] * z[30];
z[5] = z[5] + z[8] + -3 * z[17] + -z[76] + z[92];
z[5] = abb[31] * z[5];
z[3] = z[3] + z[4] + z[5] + z[6] + z[7] + -z[72];
z[3] = m1_set::bc<T>[0] * z[3];
z[0] = abb[39] * z[0];
z[4] = z[10] + -z[54] + z[84];
z[4] = abb[37] * z[4];
z[1] = -z[1] + z[19];
z[1] = abb[38] * z[1];
z[0] = (T(-3) / T(2)) * z[0] + 3 * z[1] + z[3] + z[4];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_222_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("-60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-50.753230340904010132424722396808666651715583559439082167006918678"),stof<T>("34.081682310324995686088672650457391383990622786459470662902339143")}, std::complex<T>{stof<T>("7.7691405800748865872202464486523257300927602883641503557166568482"),stof<T>("0.9279987808995460879225665469713760549679629066638179096498495597")}, std::complex<T>{stof<T>("-6.4728848884311135370344688408294995525324252005990025093280016611"),stof<T>("-7.46249334498498083634381434235989191476518227252497846562749018")}, std::complex<T>{stof<T>("-13.490678737724229576388000192776270368528148022631162466132111686"),stof<T>("-7.950728224719451816036195091289869905517934538622874076227542381")}, std::complex<T>{stof<T>("44.280345452472896595390253555979167099183158358840079657678917017"),stof<T>("-41.544175655309976522432486992817283298755805058984449128529829323")}, std::complex<T>{stof<T>("45.563567763947112572112705731626154617787502660050213118301287389"),stof<T>("30.572639609146429529044097999753893635241788726625451800264414552")}, std::complex<T>{stof<T>("-18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-5.6789893346428846672177607042692797070508719739284476388982577664"),stof<T>("-3.3844629567787349761988538383864319226127633681444014415660211259")}, std::complex<T>{stof<T>("4.3827336429991116170319830964464535294905368861632997925096025793"),stof<T>("9.9189575208641697246201016337749477824099827340055619975436617462")}, std::complex<T>{stof<T>("-24.303748446147996408504459090197558519166594349054900296452518855"),stof<T>("-21.693912603527431625085336361492647674755891281338759814387022612")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("3.7725570814789613975762973906587701370141907692217706083159397883"),stof<T>("-3.787270804387331795959705184250556634305633874004138577697608026")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[78].real()/kbase.W[78].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_222_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_222_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-26.986672380774296321390070915721496290329239903904055530551261089"),stof<T>("42.839273860445306593362705682689067954951045754876709402369339523")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W79(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[78].real()/k.W[78].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_222_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_222_DLogXconstant_part(base_point<T>, kend);
	value += f_4_222_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_222_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_222_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_222_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_222_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_222_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_222_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
