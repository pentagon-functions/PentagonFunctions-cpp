/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_87.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_87_abbreviated (const std::array<T,18>& abb) {
T z[15];
z[0] = abb[13] * (T(1) / T(2));
z[1] = abb[14] + abb[12] * (T(1) / T(2)) + -z[0];
z[1] = abb[2] * z[1];
z[2] = abb[13] + -abb[14];
z[3] = abb[5] * z[2];
z[4] = z[1] + z[3];
z[5] = abb[1] * (T(1) / T(2));
z[6] = abb[12] + abb[13];
z[5] = z[5] * z[6];
z[7] = -abb[14] + abb[15];
z[7] = abb[0] * z[7];
z[8] = -z[4] + z[5] + 2 * z[7];
z[8] = abb[7] * z[8];
z[9] = 2 * abb[3];
z[10] = abb[1] + -abb[2] + z[9];
z[11] = 2 * abb[14];
z[12] = abb[12] + z[11];
z[13] = abb[13] + -z[12];
z[10] = z[10] * z[13];
z[14] = abb[6] * z[10];
z[8] = z[8] + z[14];
z[8] = abb[7] * z[8];
z[4] = z[4] + z[5];
z[4] = abb[8] * z[4];
z[4] = z[4] + -z[14];
z[4] = abb[8] * z[4];
z[5] = abb[12] * (T(-5) / T(2)) + -z[0] + -z[11];
z[5] = abb[1] * z[5];
z[1] = -z[1] + (T(-5) / T(2)) * z[3] + z[5];
z[0] = abb[15] * (T(1) / T(6)) + z[0] + (T(-2) / T(3)) * z[12];
z[0] = abb[3] * z[0];
z[0] = z[0] + (T(1) / T(3)) * z[1] + (T(-1) / T(6)) * z[7];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = -abb[2] + abb[3];
z[1] = z[1] * z[13];
z[5] = abb[1] + abb[5];
z[2] = z[2] * z[5];
z[1] = z[1] + z[2];
z[2] = -abb[16] * z[1];
z[5] = -abb[15] + z[12];
z[5] = z[5] * z[9];
z[7] = -abb[1] * z[13];
z[5] = z[5] + z[7];
z[5] = prod_pow(abb[6], 2) * z[5];
z[7] = -abb[9] * z[10];
z[0] = abb[17] + z[0] + z[2] + z[4] + z[5] + z[7] + z[8];
z[1] = -abb[10] * z[1];
z[2] = -abb[1] * z[6];
z[4] = abb[2] * z[13];
z[2] = z[2] + -2 * z[3] + z[4];
z[2] = abb[8] * z[2];
z[2] = z[2] + z[14];
z[2] = m1_set::bc<T>[0] * z[2];
z[1] = abb[11] + z[1] + z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_87_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-2.3430400977995292230892036556091277928090613380748050028406578102"),stof<T>("9.9005066322839325176826197564345866280816505161210211124765730163")}, std::complex<T>{stof<T>("-5.2191979682710451663854183331354026567721011217076508674788832502"),stof<T>("-1.18961275727625567966761314631362711587150060242345822559251915")}, std::complex<T>{stof<T>("2.87615787047151594329621467752627486396303978363284586463822544"),stof<T>("11.0901193895601881973502329027482137439531511185444793380690921663")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_87_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_87_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(-1) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[1] * (-v[3] + v[5])) / tend;


		return (abb[12] + -abb[13] + 2 * abb[14]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[7], 2);
z[1] = prod_pow(abb[6], 2);
z[0] = z[0] + -z[1];
z[1] = abb[12] + -abb[13] + 2 * abb[14];
return abb[4] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_87_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_87_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("3.58451500955357955352400263421672201614646125395727268411006478"),stof<T>("-13.85337665300554996236260860780921675329243586429433868167386099")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,18> abb = {dl[5], dl[2], dlog_W7(k,dl), dl[3], dlog_W16(k,dl), dlog_W22(k,dl), f_1_2(k), f_1_4(k), f_1_5(k), f_2_16(k), f_2_7_im(k), T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), IntDLogL_W_16(t,k,kend,dl), f_2_7_re(k), T{0}};
abb[11] = SpDLog_f_4_87_W_16_Im(t, path, abb);
abb[17] = SpDLog_f_4_87_W_16_Re(t, path, abb);

                    
            return f_4_87_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_87_DLogXconstant_part(base_point<T>, kend);
	value += f_4_87_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_87_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_87_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_87_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_87_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_87_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_87_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
