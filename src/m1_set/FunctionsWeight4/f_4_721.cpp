/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_721.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_721_abbreviated (const std::array<T,36>& abb) {
T z[66];
z[0] = 2 * abb[18];
z[1] = -abb[16] + z[0];
z[2] = abb[17] + 2 * z[1];
z[3] = 2 * abb[17];
z[2] = z[2] * z[3];
z[4] = -abb[16] + abb[17];
z[5] = z[0] + z[4];
z[5] = -abb[14] + 2 * z[5];
z[6] = 2 * abb[14];
z[5] = z[5] * z[6];
z[7] = prod_pow(abb[16], 2);
z[8] = 3 * z[7];
z[5] = z[5] + z[8];
z[9] = 2 * abb[16];
z[10] = abb[18] + z[9];
z[10] = abb[18] * z[10];
z[11] = prod_pow(m1_set::bc<T>[0], 2);
z[12] = (T(10) / T(3)) * z[11];
z[13] = 2 * abb[21];
z[2] = -z[2] + z[5] + -z[10] + -z[12] + z[13];
z[2] = abb[1] * z[2];
z[14] = 2 * abb[20];
z[15] = z[13] + z[14];
z[16] = prod_pow(abb[14], 2);
z[17] = -abb[15] + z[9];
z[18] = abb[15] * z[17];
z[16] = 2 * z[16] + -z[18];
z[19] = prod_pow(abb[18], 2);
z[20] = z[7] + z[15] + z[16] + -2 * z[19];
z[20] = abb[5] * z[20];
z[21] = -abb[18] + z[9];
z[22] = abb[18] * z[21];
z[23] = -z[7] + z[22];
z[24] = 2 * abb[34];
z[25] = z[23] + z[24];
z[26] = 6 * abb[33];
z[27] = (T(5) / T(3)) * z[11];
z[13] = -z[13] + z[25] + -z[26] + z[27];
z[13] = abb[6] * z[13];
z[28] = 2 * abb[33];
z[29] = abb[2] * z[28];
z[30] = -z[2] + -z[13] + z[20] + -z[29];
z[31] = -z[11] + z[28];
z[32] = 4 * abb[17];
z[33] = -z[1] * z[32];
z[19] = 4 * z[19];
z[34] = 4 * abb[21];
z[5] = z[5] + -z[18] + -z[19] + z[31] + z[33] + z[34];
z[5] = abb[4] * z[5];
z[33] = 6 * abb[16] + -abb[18];
z[33] = abb[18] * z[33];
z[35] = 4 * abb[33];
z[36] = (T(2) / T(3)) * z[11];
z[33] = 5 * z[7] + -z[33] + z[34] + z[35] + -z[36];
z[33] = abb[7] * z[33];
z[37] = 2 * z[33];
z[38] = 3 * abb[16];
z[39] = 2 * abb[15];
z[40] = z[38] + -z[39];
z[41] = 4 * abb[18];
z[42] = z[40] + -z[41];
z[43] = -z[3] + z[42];
z[43] = abb[17] * z[43];
z[8] = z[8] + z[18];
z[44] = z[8] + z[28];
z[45] = z[0] * z[21];
z[46] = 3 * abb[17];
z[47] = z[42] + -z[46];
z[48] = 3 * abb[14];
z[49] = z[47] + z[48];
z[49] = abb[14] * z[49];
z[49] = z[27] + -z[43] + -z[44] + z[45] + z[49];
z[49] = abb[0] * z[49];
z[50] = -z[39] + z[41];
z[51] = -abb[14] + z[4];
z[52] = z[50] + z[51];
z[52] = abb[14] * z[52];
z[50] = -abb[16] + z[50];
z[53] = abb[17] * z[50];
z[54] = 2 * z[11];
z[52] = -z[28] + z[52] + -z[53] + -z[54];
z[53] = -abb[8] * z[52];
z[47] = abb[14] + z[47];
z[47] = abb[14] * z[47];
z[35] = (T(8) / T(3)) * z[11] + -z[35] + -z[43] + z[47];
z[35] = abb[3] * z[35];
z[43] = -abb[3] + abb[4];
z[47] = abb[8] + z[43];
z[47] = z[14] * z[47];
z[55] = 2 * abb[0];
z[56] = 3 * abb[7] + z[55];
z[57] = 2 * abb[4];
z[58] = -5 * abb[1] + z[56] + -z[57];
z[59] = abb[3] + abb[8];
z[60] = z[58] + z[59];
z[61] = 4 * abb[34];
z[62] = z[60] * z[61];
z[59] = abb[0] + -4 * abb[1] + -z[57] + z[59];
z[63] = 4 * abb[19];
z[59] = z[59] * z[63];
z[64] = 5 * abb[11] + 3 * abb[12];
z[65] = abb[35] * z[64];
z[5] = z[5] + -2 * z[30] + z[35] + -z[37] + z[47] + z[49] + z[53] + z[59] + z[62] + z[65];
z[5] = abb[31] * z[5];
z[30] = abb[14] + -z[9];
z[30] = abb[14] * z[30];
z[15] = -z[15] + z[18] + z[23] + -z[28] + z[30];
z[15] = abb[4] * z[15];
z[23] = z[3] + -z[48];
z[23] = abb[14] * z[23];
z[30] = -abb[33] + z[7];
z[35] = -abb[17] * z[9];
z[10] = -z[10] + 5 * z[11] + z[23] + 2 * z[30] + z[35];
z[10] = abb[3] * z[10];
z[23] = abb[16] + abb[17];
z[30] = -z[23] + z[48];
z[30] = abb[14] * z[30];
z[35] = abb[16] * abb[17];
z[47] = abb[18] * z[9];
z[30] = -abb[21] + -abb[33] + -2 * z[7] + -z[12] + z[30] + z[35] + z[47];
z[30] = z[30] * z[55];
z[25] = abb[21] + -z[25] + z[31];
z[31] = 4 * abb[9];
z[25] = z[25] * z[31];
z[47] = z[43] + z[55];
z[47] = abb[19] * z[47];
z[49] = abb[11] * abb[35];
z[47] = z[47] + z[49];
z[43] = z[43] + z[56];
z[49] = z[24] * z[43];
z[10] = z[10] + z[13] + z[15] + z[20] + z[25] + z[30] + -z[33] + 2 * z[47] + z[49];
z[10] = abb[29] * z[10];
z[13] = z[38] + z[39];
z[15] = abb[17] * z[13];
z[20] = 5 * abb[17];
z[13] = 13 * abb[14] + -z[13] + -z[20];
z[13] = abb[14] * z[13];
z[30] = 8 * abb[18];
z[33] = abb[16] * z[30];
z[13] = -7 * z[7] + (T(-41) / T(3)) * z[11] + z[13] + z[15] + -z[18] + -z[26] + z[33] + -z[34];
z[13] = abb[0] * z[13];
z[15] = z[30] + -z[38];
z[26] = 5 * abb[14];
z[33] = z[15] + z[20] + -z[26];
z[33] = abb[14] * z[33];
z[34] = -8 * abb[17] + -z[15];
z[34] = abb[17] * z[34];
z[8] = z[8] + -z[19] + -z[28] + z[33] + z[34] + z[54];
z[8] = abb[4] * z[8];
z[2] = z[2] + -z[29];
z[15] = z[15] + -z[39];
z[19] = -z[15] + -z[46] + z[48];
z[19] = abb[14] * z[19];
z[29] = abb[17] * z[15];
z[12] = z[12] + z[19] + z[28] + z[29];
z[12] = abb[8] * z[12];
z[19] = abb[15] + z[3];
z[19] = abb[17] * z[19];
z[29] = abb[15] + abb[17];
z[33] = -abb[14] + -z[29];
z[33] = abb[14] * z[33];
z[19] = z[19] + z[33];
z[19] = (T(7) / T(3)) * z[11] + 2 * z[19];
z[19] = abb[3] * z[19];
z[33] = abb[4] + -abb[8];
z[34] = -abb[3] + -z[33];
z[34] = z[14] * z[34];
z[47] = 2 * abb[8] + z[58];
z[49] = z[47] * z[61];
z[53] = abb[10] + abb[12];
z[53] = 6 * abb[11] + 5 * z[53];
z[55] = abb[35] * z[53];
z[2] = 2 * z[2] + z[8] + z[12] + z[13] + z[19] + z[34] + -z[37] + z[49] + z[55];
z[2] = abb[30] * z[2];
z[8] = z[7] + z[28];
z[12] = z[9] + -z[26];
z[12] = abb[14] * z[12];
z[13] = (T(16) / T(3)) * z[11];
z[12] = z[8] + z[12] + z[13] + z[18] + z[22];
z[12] = abb[32] * z[12];
z[9] = -z[9] + z[48];
z[9] = abb[14] * z[9];
z[9] = z[9] + (T(-11) / T(3)) * z[11] + -z[22];
z[9] = abb[27] * z[9];
z[9] = z[9] + z[12];
z[9] = abb[10] * z[9];
z[12] = -z[0] + z[40];
z[19] = -z[12] + z[32];
z[19] = abb[17] * z[19];
z[22] = abb[17] + z[48];
z[34] = z[12] + -z[22];
z[34] = abb[14] * z[34];
z[13] = z[13] + z[19] + z[34];
z[13] = abb[32] * z[13];
z[19] = -abb[18] + -z[4];
z[19] = abb[17] * z[19];
z[34] = -abb[16] + abb[18];
z[37] = abb[14] + z[34];
z[37] = abb[14] * z[37];
z[19] = z[19] + z[37] + -z[54];
z[19] = abb[27] * z[19];
z[13] = z[13] + 2 * z[19];
z[13] = abb[11] * z[13];
z[19] = z[0] + z[51];
z[19] = abb[14] * z[19];
z[37] = -abb[17] * z[1];
z[19] = z[19] + -z[36] + z[37];
z[19] = abb[27] * z[19];
z[37] = -abb[14] + abb[17];
z[40] = -abb[15] + abb[18];
z[37] = z[37] * z[40];
z[37] = abb[33] + z[36] + z[37];
z[40] = 2 * abb[32];
z[37] = z[37] * z[40];
z[19] = z[19] + z[37];
z[19] = abb[12] * z[19];
z[37] = abb[27] + -abb[32];
z[49] = abb[11] * z[37];
z[54] = abb[12] * z[37];
z[37] = abb[10] * z[37];
z[49] = z[37] + z[49] + z[54];
z[24] = -z[24] * z[49];
z[55] = abb[10] + -abb[12];
z[56] = -abb[11] + -z[55];
z[56] = abb[32] * z[14] * z[56];
z[58] = -abb[8] + 8 * abb[13];
z[59] = 7 * abb[0] + 5 * abb[3] + 8 * abb[4] + -2 * z[58];
z[59] = abb[32] * z[59];
z[58] = 4 * abb[4] + -z[58];
z[62] = 4 * abb[0] + 2 * abb[3] + z[58];
z[62] = abb[27] * z[62];
z[59] = z[59] + -z[62];
z[62] = abb[35] * z[59];
z[9] = z[9] + z[13] + z[19] + z[24] + z[56] + z[62];
z[13] = 5 * abb[16];
z[19] = 7 * abb[14] + -abb[17] + -z[13];
z[19] = abb[14] * z[19];
z[7] = z[7] + -8 * z[11] + -z[18] + z[19] + -z[28] + z[35] + -z[45];
z[7] = abb[10] * z[7];
z[18] = abb[12] * z[52];
z[19] = abb[10] + abb[11];
z[24] = abb[12] + z[19];
z[52] = z[24] * z[61];
z[55] = z[55] * z[63];
z[19] = -abb[12] + z[19];
z[14] = z[14] * z[19];
z[18] = z[14] + z[18] + -z[52] + z[55];
z[19] = -abb[15] + -z[0] + z[38];
z[52] = -z[3] + z[19];
z[52] = abb[17] * z[52];
z[55] = z[6] + -z[19];
z[55] = abb[14] * z[55];
z[52] = z[52] + z[55];
z[52] = (T(-25) / T(3)) * z[11] + 2 * z[52];
z[52] = abb[11] * z[52];
z[55] = abb[0] + abb[4];
z[56] = 3 * abb[8];
z[55] = 4 * abb[3] + -20 * abb[13] + 9 * z[55] + z[56];
z[61] = -abb[35] * z[55];
z[7] = z[7] + z[18] + z[52] + z[61];
z[7] = abb[26] * z[7];
z[51] = -z[39] + -z[51];
z[51] = abb[14] * z[51];
z[52] = -abb[16] + z[39];
z[61] = abb[17] * z[52];
z[28] = -z[28] + -z[36] + z[51] + z[61];
z[28] = abb[12] * z[28];
z[8] = -z[8] + z[16] + -z[27];
z[8] = abb[10] * z[8];
z[16] = -z[3] + -z[52];
z[16] = abb[17] * z[16];
z[27] = z[4] + z[39];
z[36] = abb[14] + z[27];
z[36] = abb[14] * z[36];
z[16] = (T(-4) / T(3)) * z[11] + z[16] + z[36];
z[16] = abb[11] * z[16];
z[36] = abb[0] + abb[3];
z[36] = 3 * z[36] + z[58];
z[51] = -abb[35] * z[36];
z[8] = z[8] + z[14] + z[16] + z[28] + z[51];
z[8] = abb[28] * z[8];
z[14] = 9 * abb[14] + abb[17] + -z[38];
z[14] = abb[14] * z[14];
z[14] = -10 * z[11] + z[14] + -z[35] + -z[44] + -z[45];
z[14] = abb[10] * z[14];
z[0] = -z[0] + z[17];
z[16] = z[0] + -z[32];
z[16] = abb[17] * z[16];
z[17] = -z[0] + z[22];
z[17] = abb[14] * z[17];
z[16] = z[16] + z[17];
z[11] = (T(-31) / T(3)) * z[11] + 2 * z[16];
z[11] = abb[11] * z[11];
z[16] = 13 * abb[0] + 10 * abb[3] + 15 * abb[4] + -28 * abb[13] + z[56];
z[17] = -abb[35] * z[16];
z[11] = z[11] + z[14] + z[17] + z[18];
z[11] = abb[25] * z[11];
z[14] = abb[0] + -2 * abb[1] + -z[33];
z[14] = abb[30] * z[14];
z[14] = z[14] + z[37] + -z[54];
z[14] = abb[19] * z[14];
z[17] = abb[30] + abb[31];
z[18] = z[17] * z[25];
z[2] = z[2] + z[5] + z[7] + z[8] + 4 * z[9] + 2 * z[10] + z[11] + 8 * z[14] + z[18];
z[2] = 4 * z[2];
z[5] = -abb[18] + z[38];
z[5] = m1_set::bc<T>[0] * z[5];
z[7] = 2 * abb[22];
z[5] = z[5] + z[7];
z[5] = abb[7] * z[5];
z[8] = abb[18] + -z[4] + z[48];
z[8] = m1_set::bc<T>[0] * z[8];
z[8] = -abb[22] + z[8];
z[8] = abb[3] * z[8];
z[9] = 6 * abb[14];
z[4] = z[4] + -z[9];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = -abb[22] + z[4];
z[4] = abb[0] * z[4];
z[10] = abb[14] + -abb[18];
z[11] = abb[5] * z[10];
z[14] = 2 * m1_set::bc<T>[0];
z[18] = -z[11] * z[14];
z[22] = abb[6] + z[43];
z[22] = abb[23] * z[22];
z[25] = m1_set::bc<T>[0] * z[34];
z[28] = -abb[22] + abb[23] + z[25];
z[31] = -z[28] * z[31];
z[25] = 3 * abb[22] + -z[25];
z[25] = abb[6] * z[25];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = -abb[22] + -z[10];
z[10] = abb[4] * z[10];
z[32] = abb[11] * abb[24];
z[4] = z[4] + -z[5] + z[8] + z[10] + z[18] + z[22] + -z[25] + z[31] + z[32];
z[4] = abb[29] * z[4];
z[8] = abb[14] + abb[18] + -z[29];
z[8] = m1_set::bc<T>[0] * z[8];
z[8] = abb[22] + z[8];
z[8] = z[8] * z[40];
z[10] = abb[27] * m1_set::bc<T>[0];
z[1] = abb[17] + -z[1];
z[1] = z[1] * z[10];
z[1] = z[1] + z[8];
z[1] = abb[12] * z[1];
z[8] = -z[21] + z[26];
z[8] = m1_set::bc<T>[0] * z[8];
z[8] = abb[22] + z[8];
z[8] = abb[32] * z[8];
z[18] = z[21] + -z[48];
z[10] = z[10] * z[18];
z[8] = z[8] + z[10];
z[10] = 2 * abb[10];
z[8] = z[8] * z[10];
z[18] = 8 * abb[14] + -abb[17];
z[12] = -z[12] + z[18];
z[12] = abb[32] * m1_set::bc<T>[0] * z[12];
z[21] = abb[17] + -z[34] + -z[48];
z[21] = abb[27] * z[14] * z[21];
z[12] = z[12] + z[21];
z[12] = abb[11] * z[12];
z[21] = abb[23] * z[49];
z[1] = z[1] + z[4] + z[8] + z[12] + -2 * z[21];
z[4] = 7 * abb[16];
z[8] = -abb[17] + -z[4] + -z[6] + z[30] + z[39];
z[8] = m1_set::bc<T>[0] * z[8];
z[8] = -z[7] + z[8];
z[8] = abb[0] * z[8];
z[12] = -z[3] + z[6] + 3 * z[34];
z[12] = abb[1] * m1_set::bc<T>[0] * z[12];
z[21] = abb[2] * abb[22];
z[22] = z[12] + -z[21] + z[25];
z[25] = z[6] + -z[46] + z[50];
z[25] = m1_set::bc<T>[0] * z[25];
z[25] = z[7] + z[25];
z[26] = abb[8] * z[25];
z[5] = 4 * z[5];
z[30] = -abb[17] + z[6] + -z[42];
z[30] = m1_set::bc<T>[0] * z[30];
z[30] = -4 * abb[22] + z[30];
z[30] = abb[3] * z[30];
z[31] = -abb[14] + z[23];
z[31] = z[14] * z[31];
z[31] = abb[22] + z[31];
z[31] = z[31] * z[57];
z[32] = abb[6] + z[60];
z[33] = 4 * abb[23];
z[32] = z[32] * z[33];
z[11] = m1_set::bc<T>[0] * z[11];
z[34] = abb[24] * z[64];
z[8] = -z[5] + z[8] + 8 * z[11] + -4 * z[22] + z[26] + z[30] + z[31] + z[32] + z[34];
z[8] = abb[31] * z[8];
z[11] = -z[12] + -z[21];
z[12] = z[6] + z[15] + -z[20];
z[12] = m1_set::bc<T>[0] * z[12];
z[12] = z[7] + z[12];
z[12] = abb[8] * z[12];
z[13] = -26 * abb[14] + -z[13] + z[20] + z[39];
z[13] = m1_set::bc<T>[0] * z[13];
z[13] = -6 * abb[22] + z[13];
z[13] = abb[0] * z[13];
z[15] = z[6] + z[29];
z[15] = abb[3] * z[14] * z[15];
z[20] = z[6] + 3 * z[23];
z[20] = m1_set::bc<T>[0] * z[20];
z[20] = -z[7] + z[20];
z[20] = abb[4] * z[20];
z[21] = z[33] * z[47];
z[5] = -z[5] + 4 * z[11] + z[12] + z[13] + z[15] + z[20] + z[21];
z[5] = abb[30] * z[5];
z[11] = abb[12] * z[25];
z[12] = z[24] * z[33];
z[11] = z[11] + z[12];
z[3] = z[3] + -z[9] + z[19];
z[9] = abb[11] * z[14];
z[3] = z[3] * z[9];
z[12] = -14 * abb[14] + 9 * abb[16] + abb[17] + -z[41];
z[12] = m1_set::bc<T>[0] * z[12];
z[12] = -z[7] + z[12];
z[12] = abb[10] * z[12];
z[13] = -abb[24] * z[55];
z[3] = z[3] + -z[11] + z[12] + z[13];
z[3] = abb[26] * z[3];
z[12] = -z[6] + z[27];
z[12] = m1_set::bc<T>[0] * z[12];
z[12] = -z[7] + z[12];
z[12] = abb[12] * z[12];
z[13] = -m1_set::bc<T>[0] * z[6];
z[13] = -abb[22] + z[13];
z[10] = z[10] * z[13];
z[6] = -z[6] + -z[27];
z[6] = abb[11] * m1_set::bc<T>[0] * z[6];
z[6] = z[6] + z[10] + z[12];
z[6] = abb[28] * z[6];
z[4] = -18 * abb[14] + -abb[17] + z[4] + -z[41];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = z[4] + -z[7];
z[4] = abb[10] * z[4];
z[0] = z[0] + -z[18];
z[0] = z[0] * z[9];
z[7] = -abb[24] * z[16];
z[0] = z[0] + z[4] + z[7] + -z[11];
z[0] = abb[25] * z[0];
z[4] = -abb[28] * z[36];
z[7] = abb[30] * z[53];
z[4] = z[4] + z[7] + 4 * z[59];
z[4] = abb[24] * z[4];
z[7] = -abb[9] * z[17] * z[28];
z[0] = z[0] + 4 * z[1] + z[3] + z[4] + z[5] + z[6] + 8 * z[7] + z[8];
z[0] = 4 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_721_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-311.59533731262060956323621360047883585122743845114481634383892263"),stof<T>("504.44593277271778840918971369986680571114860019350617429086766279")}, std::complex<T>{stof<T>("-251.19465101240311945173070941956733803576583746717735859533653744"),stof<T>("406.07946056176372669548680250366620503282995729046840583126989533")}, std::complex<T>{stof<T>("-529.36091719753288481701335148652021443990015893303645189314579446"),stof<T>("751.16448216199480668014356933510228870917237822944224069490750993")}, std::complex<T>{stof<T>("-16.714535563745422098976785766762979723546558492642861523014832807"),stof<T>("79.680455586243354212266473434215361017403089627266169713615024093")}, std::complex<T>{stof<T>("141.60300481744739049847491374026241428004437238812509837246000992"),stof<T>("942.76467504631482397349147052924445724894681458296583626841419624")}, std::complex<T>{stof<T>("293.90013259749964495951396379845464918453470517182841978744469681"),stof<T>("1009.30251797897180089359275396416975350929452141382142188193825893")}, std::complex<T>{stof<T>("-17.76386737614438283857160806534641894158309487476453776859629623"),stof<T>("6.081397064348176244279497780963550905772222398119979728280850635")}, std::complex<T>{stof<T>("596.21905945251457321292049455357213333408639290360789798520512569"),stof<T>("-1069.88630450696822352920946307196373277878473673850691954936760631")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real()), rlog(k.W[195].real()/kbase.W[195].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_721_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_721_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("1158.12489346058535259147046540353146834021606644646477966240900867"),stof<T>("-579.18271283000734379668169010579298568987831253447491040414666424")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,36> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W68(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), rlog(kend.W[195].real()/k.W[195].real()), f_2_4_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_721_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_721_DLogXconstant_part(base_point<T>, kend);
	value += f_4_721_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_721_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_721_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_721_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_721_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_721_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_721_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
