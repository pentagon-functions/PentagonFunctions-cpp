/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_634.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_634_abbreviated (const std::array<T,63>& abb) {
T z[138];
z[0] = abb[42] + -abb[49];
z[1] = abb[46] * (T(11) / T(12));
z[2] = abb[43] * (T(5) / T(6));
z[3] = abb[45] * (T(5) / T(4));
z[4] = abb[50] + -abb[51];
z[5] = 3 * z[4];
z[6] = abb[47] * (T(11) / T(3)) + z[5];
z[6] = abb[52] * (T(1) / T(3)) + abb[44] * (T(1) / T(12)) + -z[0] + -z[1] + z[2] + -z[3] + (T(1) / T(4)) * z[6];
z[6] = abb[5] * z[6];
z[7] = abb[48] * (T(1) / T(2));
z[8] = abb[52] * (T(2) / T(3)) + -z[7];
z[9] = abb[45] * (T(3) / T(4));
z[10] = abb[47] * (T(7) / T(3));
z[11] = -5 * abb[50] + abb[51] * (T(11) / T(3)) + -z[10];
z[1] = abb[44] * (T(-7) / T(4)) + abb[43] * (T(7) / T(6)) + (T(-2) / T(3)) * z[0] + z[1] + z[8] + z[9] + (T(1) / T(4)) * z[11];
z[1] = abb[6] * z[1];
z[11] = 2 * abb[52];
z[12] = -abb[48] + z[11];
z[13] = -abb[50] + z[12];
z[14] = abb[51] * (T(1) / T(2));
z[15] = abb[47] * (T(1) / T(2));
z[16] = abb[43] * (T(1) / T(2));
z[17] = z[13] + z[14] + z[15] + z[16];
z[17] = abb[9] * z[17];
z[18] = 3 * abb[51];
z[19] = -abb[50] + z[18];
z[10] = z[10] + -z[19];
z[20] = abb[44] * (T(1) / T(3));
z[2] = abb[46] * (T(-1) / T(6)) + z[2] + -z[8] + (T(1) / T(2)) * z[10] + z[20];
z[2] = abb[1] * z[2];
z[8] = abb[44] * (T(1) / T(2));
z[10] = abb[46] * (T(1) / T(2));
z[21] = z[8] + z[10];
z[22] = abb[48] * (T(3) / T(2));
z[15] = -abb[51] + -abb[52] + z[15] + z[21] + z[22];
z[15] = abb[14] * z[15];
z[23] = 3 * abb[47];
z[24] = abb[50] + abb[51];
z[25] = z[23] + -z[24];
z[26] = abb[46] * (T(1) / T(4));
z[27] = abb[52] + (T(1) / T(4)) * z[25] + -z[26];
z[28] = abb[44] * (T(3) / T(4));
z[3] = -z[3] + z[27] + z[28];
z[3] = abb[4] * z[3];
z[29] = abb[47] + -abb[51];
z[30] = -abb[50] + z[29];
z[31] = abb[45] + z[30];
z[32] = abb[44] + z[31];
z[32] = (T(1) / T(2)) * z[32];
z[33] = z[0] + z[10] + z[32];
z[34] = abb[12] * z[33];
z[35] = abb[44] + z[29];
z[36] = z[0] + z[35];
z[37] = abb[2] * z[36];
z[38] = abb[45] * (T(1) / T(2));
z[39] = -abb[48] + z[38];
z[40] = -z[21] + z[39];
z[41] = abb[50] + z[29];
z[42] = (T(1) / T(2)) * z[41];
z[43] = abb[43] + z[40] + z[42];
z[44] = abb[13] * z[43];
z[45] = abb[53] + -abb[54];
z[46] = (T(1) / T(2)) * z[45];
z[47] = abb[23] * z[46];
z[48] = z[44] + z[47];
z[49] = abb[20] + abb[22];
z[50] = 2 * abb[21] + (T(5) / T(4)) * z[49];
z[50] = z[45] * z[50];
z[51] = -abb[44] + abb[46];
z[31] = z[31] + z[51];
z[31] = abb[43] + (T(1) / T(2)) * z[31];
z[52] = abb[7] * z[31];
z[53] = abb[43] + z[29];
z[54] = abb[46] + -abb[50];
z[55] = abb[48] + z[54];
z[56] = (T(-2) / T(3)) * z[53] + -z[55];
z[56] = abb[0] * z[56];
z[57] = 2 * abb[48];
z[58] = -abb[44] + abb[45] + -abb[46] + -z[30] + -z[57];
z[58] = abb[8] * z[58];
z[35] = -abb[45] + z[35];
z[59] = abb[48] + z[35];
z[59] = abb[11] * z[59];
z[60] = 3 * z[59];
z[61] = (T(3) / T(2)) * z[45];
z[62] = abb[19] * z[61];
z[63] = abb[9] * abb[45];
z[20] = -abb[45] + -abb[50] + abb[43] * (T(-11) / T(6)) + abb[47] * (T(-3) / T(2)) + abb[46] * (T(1) / T(3)) + abb[52] * (T(4) / T(3)) + abb[51] * (T(13) / T(6)) + z[20];
z[20] = abb[3] * z[20];
z[1] = z[1] + z[2] + -z[3] + z[6] + z[15] + z[17] + z[20] + z[34] + (T(-1) / T(3)) * z[37] + -z[48] + z[50] + (T(1) / T(2)) * z[52] + z[56] + 2 * z[58] + z[60] + z[62] + -z[63];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[2] = 7 * abb[51];
z[6] = 3 * abb[50];
z[17] = -z[2] + z[6] + z[23];
z[20] = -z[11] + z[26];
z[50] = abb[44] * (T(5) / T(4));
z[56] = abb[45] * (T(9) / T(4));
z[58] = 2 * abb[43];
z[17] = (T(1) / T(4)) * z[17] + z[20] + -z[50] + z[56] + z[58];
z[17] = abb[3] * z[17];
z[64] = abb[46] + abb[52];
z[65] = -abb[51] + z[6];
z[66] = -z[8] + z[16] + z[64] + (T(-1) / T(2)) * z[65];
z[66] = abb[5] * z[66];
z[67] = z[10] + z[11];
z[68] = z[22] + -z[67];
z[69] = abb[43] + -abb[44];
z[70] = abb[50] * (T(3) / T(2));
z[71] = abb[51] + -z[68] + z[69] + -z[70];
z[71] = abb[1] * z[71];
z[30] = (T(1) / T(2)) * z[30] + -z[40];
z[40] = abb[8] * z[30];
z[72] = (T(3) / T(2)) * z[40];
z[73] = (T(3) / T(2)) * z[52];
z[74] = (T(3) / T(4)) * z[45];
z[75] = abb[19] * z[74];
z[76] = -abb[21] * z[74];
z[15] = 3 * z[3] + z[15] + z[17] + -z[60] + z[66] + z[71] + z[72] + -z[73] + -z[75] + z[76];
z[15] = prod_pow(abb[29], 2) * z[15];
z[17] = -abb[47] + z[6];
z[17] = abb[44] + (T(3) / T(2)) * z[0] + -z[16] + (T(-1) / T(2)) * z[17] + z[64];
z[17] = abb[5] * z[17];
z[64] = abb[52] + z[26];
z[71] = abb[44] * (T(1) / T(4));
z[76] = -abb[47] + z[65];
z[77] = -z[9] + z[64] + z[71] + (T(-1) / T(4)) * z[76];
z[77] = abb[3] * z[77];
z[78] = (T(3) / T(2)) * z[44];
z[79] = 3 * abb[48];
z[80] = -abb[46] + z[11];
z[81] = z[79] + -z[80];
z[82] = 2 * abb[47];
z[83] = -abb[51] + z[82];
z[84] = z[81] + -z[83];
z[85] = 3 * abb[43];
z[86] = -abb[44] + -z[84] + z[85];
z[87] = abb[1] * z[86];
z[88] = z[78] + -z[87];
z[89] = (T(3) / T(2)) * z[34];
z[90] = abb[42] * (T(1) / T(2));
z[91] = abb[49] * (T(1) / T(2)) + -z[90];
z[92] = -z[22] + z[58] + z[91];
z[93] = -abb[52] + z[10];
z[94] = abb[44] + -abb[47] + z[14] + -z[92] + z[93];
z[94] = abb[6] * z[94];
z[7] = -abb[52] + z[7] + z[38];
z[95] = (T(-1) / T(2)) * z[4] + -z[7];
z[95] = abb[9] * z[95];
z[96] = abb[21] * z[45];
z[95] = z[95] + (T(-1) / T(4)) * z[96];
z[96] = 3 * abb[46] + -z[6] + z[79];
z[97] = z[53] + z[96];
z[98] = abb[0] * (T(1) / T(2));
z[99] = z[97] * z[98];
z[100] = abb[23] * z[74];
z[17] = z[17] + z[37] + z[72] + z[77] + z[88] + -z[89] + z[94] + 3 * z[95] + z[99] + z[100];
z[17] = abb[26] * z[17];
z[72] = abb[46] * (T(5) / T(4));
z[77] = z[72] + z[91];
z[28] = z[9] + z[28];
z[94] = 9 * abb[50];
z[95] = 5 * abb[51];
z[99] = abb[47] + z[94] + -z[95];
z[99] = -z[11] + -z[16] + z[28] + -z[77] + (T(1) / T(4)) * z[99];
z[99] = abb[5] * z[99];
z[101] = 13 * abb[47];
z[102] = -z[6] + -z[95] + z[101];
z[103] = abb[48] * (T(9) / T(2));
z[104] = 4 * abb[43];
z[105] = z[103] + -z[104];
z[106] = 4 * abb[52];
z[77] = -z[28] + -z[77] + (T(1) / T(4)) * z[102] + -z[105] + z[106];
z[77] = abb[6] * z[77];
z[76] = (T(1) / T(2)) * z[76];
z[68] = -z[0] + -z[16] + z[68] + z[76];
z[68] = abb[0] * z[68];
z[102] = 2 * z[37];
z[75] = z[75] + -z[89] + z[102];
z[74] = z[49] * z[74];
z[89] = z[74] + z[75];
z[107] = z[89] + -z[100];
z[108] = z[68] + -2 * z[87] + -z[107];
z[109] = abb[45] * (T(3) / T(2));
z[76] = -z[11] + -z[21] + z[76] + z[109];
z[110] = abb[14] * z[76];
z[13] = abb[47] + z[13];
z[13] = abb[9] * z[13];
z[13] = z[13] + -z[63];
z[111] = abb[24] * z[46];
z[112] = z[13] + z[111];
z[85] = abb[9] * z[85];
z[112] = z[85] + 3 * z[112];
z[77] = z[77] + z[99] + -z[108] + z[110] + -z[112];
z[77] = abb[27] * z[77];
z[99] = abb[6] * z[76];
z[113] = z[49] * z[61];
z[99] = -z[99] + z[113];
z[114] = abb[3] * z[76];
z[115] = z[62] + z[99] + -z[114];
z[116] = abb[5] * z[76];
z[117] = 2 * abb[51];
z[118] = abb[44] + abb[47];
z[119] = -z[81] + z[117] + -z[118];
z[119] = abb[14] * z[119];
z[120] = 3 * z[40];
z[4] = z[4] + -z[12];
z[4] = abb[9] * z[4];
z[4] = z[4] + z[63];
z[12] = 3 * z[4];
z[121] = abb[21] * z[61];
z[121] = z[12] + -z[115] + z[116] + 2 * z[119] + z[120] + -z[121];
z[122] = -abb[29] * z[121];
z[123] = abb[27] * z[78];
z[17] = z[17] + z[77] + z[122] + -z[123];
z[17] = abb[26] * z[17];
z[77] = abb[46] * (T(3) / T(2));
z[122] = -abb[45] + z[41];
z[124] = -5 * abb[44] + -3 * z[122];
z[124] = abb[43] + -z[0] + z[77] + (T(1) / T(2)) * z[124];
z[125] = abb[5] * (T(1) / T(2));
z[124] = z[124] * z[125];
z[39] = z[39] + z[42];
z[42] = abb[44] * (T(5) / T(2));
z[126] = -3 * z[39] + z[42] + z[77];
z[127] = abb[49] + -z[126];
z[90] = z[58] + -z[90] + (T(1) / T(2)) * z[127];
z[90] = abb[6] * z[90];
z[68] = z[68] + z[87] + -z[89] + z[90] + -z[100] + z[114] + z[124];
z[68] = abb[27] * z[68];
z[89] = 5 * abb[47];
z[90] = -z[6] + z[89];
z[124] = abb[51] + -z[90];
z[124] = z[20] + z[28] + -z[92] + (T(1) / T(4)) * z[124];
z[124] = abb[6] * z[124];
z[127] = abb[24] * z[61];
z[127] = -z[110] + z[127];
z[128] = 3 * abb[45];
z[129] = 3 * abb[44] + z[128];
z[130] = -z[90] + z[95] + -z[129];
z[131] = -3 * z[0];
z[77] = -z[77] + z[131];
z[130] = -abb[43] + z[77] + (T(1) / T(2)) * z[130];
z[130] = z[125] * z[130];
z[96] = -z[53] + z[96];
z[96] = z[96] * z[98];
z[88] = z[88] + -z[96] + -z[107] + z[124] + z[127] + z[130];
z[88] = abb[26] * z[88];
z[98] = 2 * abb[46];
z[107] = z[11] + -z[65] + z[98];
z[124] = z[69] + z[107];
z[130] = abb[1] * z[124];
z[132] = 2 * abb[3];
z[133] = z[53] * z[132];
z[133] = 3 * z[52] + -z[133];
z[134] = 2 * abb[5];
z[135] = z[53] * z[134];
z[130] = -z[99] + z[127] + -z[130] + z[133] + -z[135];
z[135] = abb[29] * z[130];
z[136] = -abb[0] + abb[1] + -abb[3];
z[53] = z[53] * z[136];
z[136] = -z[0] + z[69];
z[137] = abb[6] * z[136];
z[53] = z[37] + z[53] + z[137];
z[53] = abb[30] * z[53];
z[53] = z[53] + z[68] + z[88] + -z[123] + z[135];
z[53] = abb[30] * z[53];
z[68] = -z[6] + z[95];
z[88] = -abb[47] + z[68];
z[20] = -z[9] + -z[20] + -z[22] + -z[71] + (T(1) / T(4)) * z[88];
z[20] = abb[14] * z[20];
z[22] = 5 * abb[43];
z[88] = 2 * abb[44];
z[95] = -abb[51] + z[22] + z[23] + -z[88] + z[106];
z[70] = z[10] + z[70] + -z[95] + z[103];
z[70] = abb[1] * z[70];
z[14] = z[14] + -z[69] + -z[93] + -z[109];
z[14] = abb[3] * z[14];
z[93] = abb[43] + -abb[48];
z[27] = abb[45] * (T(1) / T(4)) + -z[27] + z[71] + -z[93];
z[27] = abb[6] * z[27];
z[7] = abb[47] + -z[7] + (T(-1) / T(2)) * z[24];
z[7] = abb[9] * z[7];
z[103] = abb[24] * z[45];
z[135] = (T(1) / T(4)) * z[103];
z[7] = z[7] + z[27] + z[135];
z[7] = 3 * z[7] + z[14] + z[20] + z[66] + z[70] + z[73] + -z[74] + z[85];
z[7] = abb[31] * z[7];
z[14] = z[110] + z[116];
z[20] = 2 * abb[6];
z[27] = z[20] * z[86];
z[44] = 3 * z[44];
z[61] = abb[23] * z[61];
z[27] = z[27] + -z[44] + -z[61];
z[66] = z[14] + z[27] + 3 * z[87] + -z[112] + z[114];
z[70] = abb[26] + -abb[27];
z[66] = z[66] * z[70];
z[70] = -abb[30] * z[130];
z[12] = z[12] + z[119];
z[73] = -abb[5] * z[124];
z[74] = z[80] + z[88] + -z[128];
z[80] = -abb[51] + z[58] + -z[74];
z[80] = abb[3] * z[80];
z[81] = -abb[51] + z[81];
z[69] = z[69] + -z[81];
z[69] = abb[1] * z[69];
z[69] = z[12] + z[69] + z[73] + z[80];
z[69] = abb[29] * z[69];
z[7] = z[7] + z[66] + z[69] + z[70];
z[7] = abb[31] * z[7];
z[66] = z[46] * z[49];
z[41] = -abb[46] + z[41] + z[93];
z[41] = abb[1] * z[41];
z[35] = z[35] + -z[54];
z[54] = -z[35] * z[125];
z[69] = -abb[3] * z[31];
z[70] = abb[6] * z[43];
z[41] = z[41] + -z[47] + z[52] + z[54] + -z[66] + z[69] + z[70];
z[41] = 3 * z[41] + -z[44];
z[41] = abb[32] * z[41];
z[21] = z[21] + z[38] + z[57];
z[19] = -abb[47] + z[19];
z[19] = z[11] + (T(1) / T(2)) * z[19] + -z[21];
z[19] = abb[14] * z[19];
z[38] = abb[21] * z[46];
z[40] = z[38] + -z[40];
z[46] = abb[19] * z[46];
z[46] = z[40] + z[46];
z[47] = z[19] + -z[46] + -z[66];
z[25] = z[11] + (T(1) / T(2)) * z[25];
z[21] = z[21] + -z[25] + -z[58];
z[21] = abb[6] * z[21];
z[29] = abb[9] * z[29];
z[54] = abb[9] * abb[43];
z[21] = z[21] + z[29] + z[47] + z[48] + z[54] + -z[87] + z[111];
z[21] = 3 * z[21];
z[29] = -abb[55] * z[21];
z[48] = z[0] + z[107];
z[48] = abb[0] * z[48];
z[66] = 3 * z[34];
z[48] = z[48] + -z[66];
z[36] = z[36] * z[134];
z[36] = z[36] + 4 * z[37] + z[48] + z[115] + -z[127];
z[69] = -abb[26] + abb[30];
z[69] = z[36] * z[69];
z[70] = z[2] + -z[90];
z[9] = abb[52] + z[9] + -z[50] + (T(1) / T(4)) * z[70] + -z[72] + -z[79];
z[9] = abb[14] * z[9];
z[50] = z[23] + -z[68];
z[28] = z[0] + z[28] + (T(1) / T(4)) * z[50] + -z[64];
z[28] = abb[5] * z[28];
z[3] = -z[3] + -z[135];
z[50] = -z[0] + z[81];
z[50] = abb[0] * z[50];
z[3] = 3 * z[3] + z[9] + z[28] + -z[37] + z[50] + z[60];
z[3] = abb[28] * z[3];
z[9] = z[74] + z[83];
z[28] = z[9] * z[20];
z[50] = z[9] * z[132];
z[25] = abb[45] * (T(-5) / T(2)) + abb[44] * (T(3) / T(2)) + -z[10] + z[25];
z[60] = abb[4] * z[25];
z[64] = z[60] + z[111];
z[14] = z[14] + z[28] + z[50] + -3 * z[64];
z[28] = -abb[27] * z[14];
z[3] = z[3] + z[28] + z[69];
z[3] = abb[28] * z[3];
z[28] = z[0] + z[8] + -z[10] + z[39];
z[28] = abb[15] * z[28];
z[39] = -abb[44] + z[122];
z[39] = abb[16] * z[39];
z[69] = abb[25] * abb[54];
z[70] = abb[25] * abb[53];
z[39] = z[39] + z[69] + -z[70];
z[69] = abb[42] + z[10];
z[70] = -abb[16] + abb[17];
z[69] = z[69] * z[70];
z[43] = abb[18] * z[43];
z[32] = -abb[49] + z[32];
z[32] = abb[17] * z[32];
z[70] = abb[43] + abb[49];
z[70] = abb[16] * z[70];
z[28] = -z[28] + z[32] + (T(1) / T(2)) * z[39] + -z[43] + z[69] + z[70];
z[28] = (T(3) / T(2)) * z[28];
z[32] = abb[60] * z[28];
z[24] = 3 * z[24] + -z[89];
z[24] = -2 * z[0] + (T(1) / T(2)) * z[24] + -z[42] + -z[67] + z[109];
z[24] = abb[5] * z[24];
z[39] = 7 * abb[47];
z[43] = z[39] + -z[68];
z[67] = abb[45] * (T(9) / T(2));
z[68] = abb[46] * (T(5) / T(2));
z[11] = abb[44] * (T(-7) / T(2)) + -z[11] + (T(-1) / T(2)) * z[43] + z[67] + z[68];
z[43] = -abb[3] * z[11];
z[24] = z[24] + z[43] + -z[48] + -3 * z[60];
z[24] = abb[33] * z[24];
z[43] = -abb[59] * z[121];
z[9] = abb[3] * z[9];
z[48] = z[0] + z[58] + -z[84];
z[48] = abb[0] * z[48];
z[20] = abb[5] + -z[20];
z[20] = z[20] * z[136];
z[9] = z[9] + z[20] + -z[37] + z[48] + -z[87];
z[9] = abb[27] * z[9];
z[14] = abb[29] * z[14];
z[9] = z[9] + z[14];
z[9] = abb[27] * z[9];
z[14] = abb[5] * z[31];
z[20] = abb[3] * z[35];
z[30] = abb[14] * z[30];
z[30] = z[30] + z[111];
z[31] = abb[1] * z[55];
z[14] = z[14] + (T(1) / T(2)) * z[20] + -z[30] + z[31] + -z[46] + -z[52];
z[14] = 3 * z[14];
z[20] = -abb[58] * z[14];
z[31] = abb[5] * z[33];
z[33] = abb[0] * z[55];
z[31] = z[30] + -z[31] + -z[33] + z[34] + z[40];
z[33] = 3 * abb[56];
z[34] = z[31] * z[33];
z[35] = z[4] + z[64];
z[19] = -z[19] + -z[35] + z[38];
z[38] = 3 * z[25];
z[40] = abb[3] * z[38];
z[19] = 3 * z[19] + z[40] + -z[120];
z[19] = abb[57] * z[19];
z[11] = -abb[33] * z[11];
z[38] = abb[57] * z[38];
z[11] = z[11] + z[38];
z[11] = abb[6] * z[11];
z[38] = -abb[62] * z[76];
z[40] = -abb[33] + abb[57];
z[46] = abb[19] + z[49];
z[46] = (T(3) / T(2)) * z[46];
z[40] = z[40] * z[45] * z[46];
z[33] = -4 * abb[33] + -z[33];
z[33] = z[33] * z[37];
z[1] = abb[61] + z[1] + z[3] + z[7] + z[9] + z[11] + z[15] + z[17] + z[19] + z[20] + z[24] + z[29] + z[32] + z[33] + z[34] + z[38] + z[40] + z[41] + z[43] + z[53];
z[3] = z[68] + z[106] + -z[109];
z[7] = -abb[51] + z[94];
z[9] = z[7] + -z[23];
z[9] = abb[43] + -z[3] + (T(1) / T(2)) * z[9] + -z[42] + z[131];
z[9] = abb[5] * z[9];
z[11] = abb[46] + z[106];
z[15] = z[11] + -z[65] + z[118] + -z[128];
z[17] = -abb[3] * z[15];
z[12] = z[12] + z[87];
z[0] = -z[0] + z[104] + -z[126];
z[0] = abb[6] * z[0];
z[19] = -abb[0] * z[97];
z[0] = z[0] + z[9] + 2 * z[12] + z[17] + z[19] + -z[44] + -z[61] + -z[62] + z[66] + -z[102] + -z[113];
z[0] = abb[26] * z[0];
z[9] = abb[14] * z[15];
z[11] = z[6] + -z[11] + z[79] + z[88];
z[12] = z[82] + z[104];
z[15] = -z[11] + z[12];
z[15] = abb[1] * z[15];
z[17] = z[56] + z[71] + -z[106];
z[7] = -z[7] + z[39];
z[7] = (T(1) / T(4)) * z[7] + -z[17] + z[26] + z[92];
z[7] = abb[6] * z[7];
z[6] = 13 * abb[51] + z[6];
z[19] = -z[6] + z[101] + z[129];
z[19] = (T(1) / T(2)) * z[19] + z[22] + -z[77];
z[19] = z[19] * z[125];
z[20] = (T(9) / T(4)) * z[49];
z[20] = z[20] * z[45];
z[7] = z[7] + -z[9] + z[15] + z[19] + z[20] + z[75] + -z[78] + z[96] + -z[100] + -3 * z[103] + -z[133];
z[7] = abb[30] * z[7];
z[2] = abb[47] + -15 * abb[50] + z[2];
z[2] = abb[46] * (T(7) / T(4)) + (T(1) / T(4)) * z[2] + z[16] + -z[17] + z[91];
z[2] = abb[5] * z[2];
z[6] = -29 * abb[47] + z[6];
z[6] = -8 * abb[52] + abb[45] * (T(27) / T(4)) + (T(1) / T(4)) * z[6] + (T(13) / T(4)) * z[51] + z[91] + z[105];
z[6] = abb[6] * z[6];
z[13] = z[13] + z[60] + z[103];
z[2] = z[2] + z[6] + z[9] + 3 * z[13] + -z[50] + z[85] + z[108];
z[2] = abb[27] * z[2];
z[4] = z[4] + z[52] + -z[60] + z[119];
z[6] = z[11] + -z[58] + -z[117];
z[6] = abb[1] * z[6];
z[9] = -abb[47] + 3 * z[65];
z[3] = -abb[43] + -z[3] + z[8] + (T(1) / T(2)) * z[9];
z[3] = abb[5] * z[3];
z[8] = -z[12] + z[18] + z[74];
z[8] = abb[3] * z[8];
z[4] = z[3] + 3 * z[4] + z[6] + z[8] + 6 * z[59] + -z[99];
z[4] = abb[29] * z[4];
z[5] = abb[47] + -z[5];
z[5] = (T(1) / T(2)) * z[5] + -z[10] + z[42] + -z[58] + -z[67] + z[106];
z[5] = abb[3] * z[5];
z[6] = abb[47] + -2 * abb[50] + abb[51] + -z[57] + z[106];
z[6] = abb[9] * z[6];
z[6] = z[6] + z[30] + z[54] + -2 * z[63];
z[8] = 6 * abb[48] + -z[95] + z[98];
z[8] = abb[1] * z[8];
z[3] = -z[3] + z[5] + 3 * z[6] + 2 * z[8] + -z[27];
z[3] = abb[31] * z[3];
z[5] = abb[28] * z[36];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[7] + z[123];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[34] * z[21];
z[3] = -abb[38] * z[121];
z[4] = abb[39] * z[28];
z[5] = -abb[37] * z[14];
z[6] = z[31] + -z[37];
z[6] = abb[35] * z[6];
z[7] = abb[3] + abb[6];
z[7] = z[7] * z[25];
z[7] = z[7] + -z[35] + -z[47];
z[7] = abb[36] * z[7];
z[6] = z[6] + z[7];
z[7] = -abb[41] * z[76];
z[0] = abb[40] + z[0] + z[2] + z[3] + z[4] + z[5] + 3 * z[6] + z[7];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_634_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-16.315117475882714975232606427433962540778181982867325106331406861"),stof<T>("25.200457682612449653334884331424039000956326613344919618649303889")}, std::complex<T>{stof<T>("13.520532721999173173545069256485226228627114083772312677716535739"),stof<T>("-17.386759367193904843959491212689517294226368958474589454510441549")}, std::complex<T>{stof<T>("-9.987310931381147525231323173252721588276601788362186881344933583"),stof<T>("13.472297104405035124574425314534006023606117373844394579709013962")}, std::complex<T>{stof<T>("4.5363404964470617690693169990853290478736060834504222195264249611"),stof<T>("2.6010685132538211552717620307685730920260478585891584247501089139")}, std::complex<T>{stof<T>("-2.7945847538835418016875371709487363121510678990950124286148711218"),stof<T>("7.8136983154185448093753931187345217067299576548703301641388623408")}, std::complex<T>{stof<T>("21.590095009064260590928132338803059916851232462632860693614562856"),stof<T>("-18.700153116728953408072795080076455472820572684515625904561760222")}, std::complex<T>{stof<T>("-1.0031187058290361207555709158528244075230937880402964231548228049"),stof<T>("-6.5155307760426908746568279289240843626462994432193532995515365006")}, std::complex<T>{stof<T>("-0.7386370367344838466262089122837683281994443963151133677567310344"),stof<T>("-3.899236052629675089990327220579010436109706070240135289337434754")}, std::complex<T>{stof<T>("-7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[83])) - rlog(abs(kbase.W[83])), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_634_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_634_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (-v[3] + v[5]) * (-8 + 12 * v[0] + -4 * v[1] + v[3] + -5 * v[5] + 2 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (-(1 + m1_set::bc<T>[1]) * (-1 * v[3] + v[5])) / tend;


		return (abb[42] + -abb[44] + 3 * abb[45] + abb[46] + -abb[47] + -abb[49] + -2 * abb[52]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = abb[26] + -abb[30];
z[1] = abb[27] + z[0];
z[1] = abb[27] * z[1];
z[0] = abb[28] + z[0];
z[0] = abb[28] * z[0];
z[0] = -abb[33] + -z[0] + z[1];
z[1] = abb[42] + -abb[44] + 3 * abb[45] + abb[46] + -abb[47] + -abb[49] + -2 * abb[52];
return abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_634_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(8)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[3] + v[5])) / tend;


		return (abb[42] + -abb[44] + 3 * abb[45] + abb[46] + -abb[47] + -abb[49] + -2 * abb[52]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[42] + abb[44] + -3 * abb[45] + -abb[46] + abb[47] + abb[49] + 2 * abb[52];
z[1] = abb[27] + -abb[28];
return abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_634_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.663911052344174559143628823059281987488577398391067804090459948"),stof<T>("22.959153459357400100908915587616044701579534801928719703358948151")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 83});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W16(k,dl), dlog_W22(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[83])) - rlog(abs(k.W[83])), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}};
abb[40] = SpDLog_f_4_634_W_16_Im(t, path, abb);
abb[41] = SpDLogQ_W_84(k,dl,dlr).imag();
abb[61] = SpDLog_f_4_634_W_16_Re(t, path, abb);
abb[62] = SpDLogQ_W_84(k,dl,dlr).real();

                    
            return f_4_634_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_634_DLogXconstant_part(base_point<T>, kend);
	value += f_4_634_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_634_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_634_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_634_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_634_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_634_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_634_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
