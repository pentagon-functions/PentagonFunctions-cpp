/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_289.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_289_abbreviated (const std::array<T,24>& abb) {
T z[17];
z[0] = abb[0] + abb[7];
z[1] = abb[16] + abb[17];
z[2] = abb[18] + z[1];
z[0] = z[0] * z[2];
z[3] = abb[3] * abb[16];
z[4] = abb[4] * abb[17];
z[3] = z[3] + z[4];
z[5] = abb[16] + abb[18];
z[5] = abb[5] * z[5];
z[6] = abb[17] + abb[18];
z[6] = abb[2] * z[6];
z[0] = z[0] + -z[3] + -z[5] + -z[6];
z[7] = -abb[21] * z[0];
z[8] = -abb[18] + z[1];
z[9] = abb[8] * z[8];
z[1] = abb[0] * z[1];
z[10] = z[1] + -z[6] + z[9];
z[11] = abb[17] + -abb[18];
z[11] = abb[1] * z[11];
z[12] = abb[6] * abb[18];
z[12] = z[11] + -z[12];
z[13] = abb[3] + abb[6];
z[14] = abb[16] * z[13];
z[14] = -z[4] + -z[12] + -z[14];
z[2] = abb[7] * z[2];
z[2] = z[2] + -z[5];
z[5] = -z[2] + -z[10] + -z[14];
z[15] = abb[23] * z[5];
z[3] = -z[1] + z[3];
z[16] = abb[3] * abb[18];
z[11] = -z[2] + z[3] + z[11] + z[16];
z[16] = abb[19] * z[11];
z[2] = z[2] + -z[6] + z[9] + -z[12];
z[1] = z[1] + -z[4];
z[6] = 4 * abb[3] + 5 * abb[6];
z[6] = abb[16] * z[6];
z[1] = -4 * z[1] + -5 * z[2] + z[6];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[2] = abb[10] * z[3];
z[3] = (T(1) / T(2)) * z[3];
z[6] = -abb[9] * z[3];
z[2] = z[2] + z[6];
z[2] = abb[9] * z[2];
z[6] = abb[16] + -abb[18];
z[6] = z[6] * z[13];
z[4] = -z[4] + -z[6] + z[10];
z[6] = -abb[20] * z[4];
z[9] = abb[0] + abb[8];
z[8] = z[8] * z[9];
z[8] = z[8] + z[14];
z[9] = -abb[22] * z[8];
z[3] = -prod_pow(abb[10], 2) * z[3];
z[1] = (T(1) / T(3)) * z[1] + z[2] + z[3] + z[6] + z[7] + z[9] + z[15] + z[16];
z[2] = abb[11] * z[11];
z[0] = -abb[13] * z[0];
z[3] = abb[15] * z[5];
z[5] = -abb[14] * z[8];
z[4] = -abb[12] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_289_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.7532249646019250799070222744628386360987885555532939900714477228"),stof<T>("4.5154510215622278047994374609422989069048487914860413644876323106")}, std::complex<T>{stof<T>("-6.941931797985130276989496573105894210630668835843527869489205125"),stof<T>("17.794126421768686058133664875070752925778238670102368799572620256")}};
	
	std::vector<C> intdlogs = {rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_289_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_289_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("17.451083205293490898433857678962372719761431434343095409440278581"),stof<T>("-19.87976276723416566877410468261857471849740426528334641903100481")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,24> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), f_1_1(k), f_1_5(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_289_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_289_DLogXconstant_part(base_point<T>, kend);
	value += f_4_289_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_289_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_289_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_289_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_289_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_289_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_289_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
