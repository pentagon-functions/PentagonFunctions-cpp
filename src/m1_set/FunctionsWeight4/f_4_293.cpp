/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_293.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_293_abbreviated (const std::array<T,31>& abb) {
T z[36];
z[0] = abb[19] + abb[25];
z[1] = -abb[20] + (T(1) / T(2)) * z[0];
z[2] = -abb[23] + z[1];
z[2] = -7 * z[2];
z[2] = abb[2] * z[2];
z[3] = -abb[19] + abb[25];
z[4] = -abb[24] + z[3];
z[5] = abb[1] * z[4];
z[6] = abb[2] * abb[21];
z[7] = abb[20] + abb[22];
z[8] = -abb[25] + z[7];
z[9] = abb[7] * z[8];
z[2] = z[2] + (T(13) / T(2)) * z[5] + (T(7) / T(2)) * z[6] + -5 * z[9];
z[10] = abb[21] + -abb[22];
z[11] = 2 * abb[23];
z[12] = z[10] + z[11];
z[13] = 2 * z[3];
z[14] = 2 * abb[24];
z[15] = z[12] + z[13] + -z[14];
z[15] = abb[4] * z[15];
z[16] = -abb[19] + abb[21];
z[17] = -abb[23] + z[7];
z[18] = abb[24] * (T(2) / T(3));
z[16] = -abb[25] + (T(-1) / T(3)) * z[16] + (T(2) / T(3)) * z[17] + z[18];
z[16] = abb[9] * z[16];
z[17] = abb[23] * (T(5) / T(2)) + abb[21] * (T(5) / T(4));
z[19] = 5 * abb[19] + -7 * abb[25];
z[19] = abb[20] + (T(1) / T(2)) * z[19];
z[19] = (T(1) / T(2)) * z[19];
z[20] = 4 * abb[22] + -z[19];
z[18] = -z[17] + -z[18] + (T(1) / T(3)) * z[20];
z[18] = abb[3] * z[18];
z[20] = abb[24] * (T(3) / T(2));
z[21] = abb[23] + abb[21] * (T(1) / T(2));
z[22] = -abb[22] + z[21];
z[23] = z[20] + z[22];
z[24] = -z[3] + z[23];
z[24] = abb[8] * z[24];
z[25] = -abb[22] + z[3];
z[26] = abb[5] * z[25];
z[24] = -z[24] + z[26];
z[27] = abb[20] + -abb[25];
z[28] = -z[10] + -z[27];
z[29] = abb[24] * (T(1) / T(2));
z[28] = -abb[23] + (T(1) / T(2)) * z[28] + -z[29];
z[28] = abb[6] * z[28];
z[17] = -8 * abb[22] + -z[17] + -z[19];
z[19] = abb[24] * (T(5) / T(2));
z[17] = (T(1) / T(3)) * z[17] + z[19];
z[17] = abb[0] * z[17];
z[2] = (T(1) / T(2)) * z[2] + (T(4) / T(3)) * z[15] + 4 * z[16] + z[17] + z[18] + (T(-5) / T(2)) * z[24] + 5 * z[28];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[16] = abb[21] + z[11];
z[17] = 2 * abb[22];
z[18] = z[16] + -z[17];
z[24] = -z[3] + z[14] + z[18];
z[28] = abb[8] * z[24];
z[30] = 2 * abb[20];
z[0] = z[0] + -z[30];
z[11] = -z[0] + z[11];
z[11] = abb[2] * z[11];
z[6] = z[6] + z[11];
z[11] = z[6] + z[15] + z[28];
z[17] = -abb[24] + z[17];
z[28] = -z[1] + z[17] + -z[21];
z[28] = abb[0] * z[28];
z[31] = -abb[24] + z[21];
z[32] = -abb[19] + 3 * abb[25];
z[7] = -z[7] + z[31] + (T(1) / T(2)) * z[32];
z[7] = abb[9] * z[7];
z[12] = z[12] + z[14] + 2 * z[27];
z[33] = abb[6] * z[12];
z[33] = -2 * z[5] + z[33];
z[34] = -abb[24] + z[1] + -z[22];
z[34] = abb[3] * z[34];
z[35] = 2 * z[9];
z[28] = 3 * z[7] + -z[11] + z[26] + z[28] + z[33] + -z[34] + z[35];
z[34] = -abb[30] * z[28];
z[30] = z[14] + -z[18] + z[30] + -z[32];
z[30] = abb[9] * z[30];
z[15] = z[15] + z[30];
z[13] = -z[13] + z[19] + z[22];
z[13] = abb[8] * z[13];
z[19] = -z[0] + z[23];
z[19] = abb[3] * z[19];
z[22] = abb[0] * z[24];
z[13] = z[13] + z[15] + -z[19] + z[22] + -z[33];
z[19] = abb[29] * z[13];
z[12] = abb[3] * z[12];
z[22] = 2 * abb[21] + 4 * abb[23] + -z[17] + z[27];
z[22] = abb[6] * z[22];
z[17] = z[17] + z[27];
z[17] = abb[0] * z[17];
z[11] = -z[11] + z[12] + z[17] + z[22] + -z[30];
z[12] = -abb[27] * z[11];
z[17] = -z[21] + -z[25] + z[29];
z[17] = abb[8] * z[17];
z[22] = -z[1] + -z[31];
z[22] = abb[0] * z[22];
z[1] = -z[1] + z[29];
z[1] = abb[3] * z[1];
z[1] = z[1] + z[5] + z[7] + z[17] + z[22] + -z[26];
z[1] = prod_pow(abb[13], 2) * z[1];
z[22] = abb[22] * (T(1) / T(2));
z[21] = -z[4] + -z[21] + z[22];
z[21] = abb[4] * z[21];
z[23] = -abb[24] + z[16];
z[24] = -z[8] + z[23];
z[24] = abb[6] * z[24];
z[8] = abb[0] * z[8];
z[10] = abb[23] + (T(1) / T(2)) * z[10];
z[10] = abb[3] * z[10];
z[10] = z[8] + z[10] + z[21] + z[24] + z[26];
z[10] = prod_pow(abb[10], 2) * z[10];
z[20] = z[20] + -z[22] + z[27];
z[20] = abb[3] * z[20];
z[22] = abb[22] + -abb[24];
z[22] = abb[0] * z[22];
z[17] = -z[9] + -z[17] + z[20] + z[21] + z[22];
z[17] = abb[12] * z[17];
z[18] = z[0] + z[18];
z[18] = abb[0] * z[18];
z[18] = z[15] + z[18];
z[20] = -abb[22] + z[14];
z[21] = z[0] + -z[20];
z[21] = abb[3] * z[21];
z[21] = z[18] + z[21];
z[21] = abb[10] * z[21];
z[17] = z[17] + z[21];
z[17] = abb[12] * z[17];
z[20] = z[20] + z[27];
z[20] = abb[3] * z[20];
z[6] = z[6] + -z[9];
z[8] = -z[6] + 2 * z[8] + -z[15] + z[20] + z[26];
z[15] = -abb[28] * z[8];
z[20] = abb[21] + -z[3];
z[20] = abb[23] + (T(1) / T(2)) * z[20];
z[20] = abb[0] * z[20];
z[21] = -abb[21] + -z[3];
z[21] = -abb[23] + abb[24] + (T(1) / T(2)) * z[21];
z[21] = abb[3] * z[21];
z[6] = -z[6] + z[7] + z[20] + z[21] + -z[24];
z[6] = prod_pow(abb[11], 2) * z[6];
z[5] = z[5] + -z[9];
z[7] = abb[3] * z[25];
z[9] = 2 * z[26];
z[5] = 2 * z[5] + -z[7] + -z[9] + z[18];
z[7] = abb[26] * z[5];
z[1] = z[1] + z[2] + z[6] + z[7] + z[10] + z[12] + z[15] + z[17] + z[19] + z[34];
z[2] = -abb[18] * z[28];
z[6] = abb[17] * z[13];
z[7] = -abb[15] * z[11];
z[0] = z[0] + -z[14] + z[16];
z[10] = -abb[3] * z[0];
z[3] = z[3] + -z[16];
z[3] = abb[0] * z[3];
z[3] = z[3] + -z[9] + z[10] + -2 * z[24] + -z[30];
z[3] = abb[10] * z[3];
z[0] = -abb[0] * z[0];
z[9] = -z[23] + -2 * z[25];
z[9] = abb[8] * z[9];
z[4] = abb[3] * z[4];
z[0] = z[0] + z[4] + z[9] + -z[30] + z[35];
z[0] = abb[12] * z[0];
z[0] = z[0] + z[3];
z[0] = m1_set::bc<T>[0] * z[0];
z[3] = -abb[16] * z[8];
z[4] = abb[14] * z[5];
z[0] = z[0] + z[2] + z[3] + z[4] + z[6] + z[7];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_293_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.4285480644788474698631322466362858945674834219365712300813161187"),stof<T>("5.073599168998984956694123062407283758768589175952851156371630066")}, std::complex<T>{stof<T>("-32.318245777209719371883693797952253647752971249484800311062996166"),stof<T>("-5.724517080325883961316843887146793206757856374984065277392731427")}, std::complex<T>{stof<T>("2.994343547218440672905655331369292223101835995077943641957142001"),stof<T>("22.944567613307694580908978108534978209967468280405624544146127321")}, std::complex<T>{stof<T>("-14.525636174632261502010469729845101314140075858810342675857097839"),stof<T>("13.361837461724824073044971398953331644252523278903182767928716325")}, std::complex<T>{stof<T>("5.988687094436881345811310662738584446203671990155887283914284001"),stof<T>("45.889135226615389161817956217069956419934936560811249088292254641")}, std::complex<T>{stof<T>("-22.215501214274746012642011646112730451282214807688972507244356447"),stof<T>("-36.957322986359417658576670332227819302209258758340021433095945007")}, std::complex<T>{stof<T>("33.746793841688566841746826044588539542320454671421371541144312285"),stof<T>("0.650917911326899004622720824739509447989267199031214121021101361")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_293_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_293_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("15.330906816992049574692438743967336805613368081464882106883605464"),stof<T>("43.846588895750450287739635465784702885491416390165707425017090232")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_293_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_293_DLogXconstant_part(base_point<T>, kend);
	value += f_4_293_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_293_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_293_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_293_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_293_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_293_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_293_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
