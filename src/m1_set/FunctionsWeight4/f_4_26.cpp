/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_26.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_26_abbreviated (const std::array<T,8>& abb) {
T z[6];
z[0] = -abb[6] + abb[7];
z[0] = abb[1] * z[0];
z[1] = abb[6] + abb[7];
z[2] = abb[2] * z[1];
z[1] = abb[0] * z[1];
z[3] = 2 * z[0] + -z[1] + z[2];
z[4] = -prod_pow(abb[4], 2) * z[3];
z[2] = z[1] + z[2];
z[5] = prod_pow(abb[3], 2);
z[5] = abb[5] + z[5];
z[5] = z[2] * z[5];
z[0] = z[0] + -z[1];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[0] = (T(13) / T(6)) * z[0] + z[4] + z[5];
z[1] = abb[4] * z[3];
z[2] = -abb[3] * z[2];
z[1] = z[1] + z[2];
z[1] = 2 * m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_26_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-12.309665232445104243494171093389046166704526849384416923684068631"),stof<T>("53.120987485953610435365611833951473570097059987826090073162833252")}, std::complex<T>{stof<T>("20.086200130755736313714918821169495321504843511049972602872600526"),stof<T>("16.610827639640481294149086977191354305180329879468443166997043577")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[11].real()/kbase.W[11].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_26_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_26_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("68.922944992660668610071414761543377321711320079481096280722021514"),stof<T>("-24.430704906917331525284071983829196977882491558644995004964254539")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,8> abb = {dl[0], dl[1], dlog_W12(k,dl), f_1_1(k), f_1_3(k), f_2_3(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[11].real()/k.W[11].real())};

                    
            return f_4_26_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_26_DLogXconstant_part(base_point<T>, kend);
	value += f_4_26_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_26_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_26_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_26_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_26_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_26_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_26_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
