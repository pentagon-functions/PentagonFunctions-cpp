/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_445.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_445_abbreviated (const std::array<T,62>& abb) {
T z[85];
z[0] = abb[51] + abb[53];
z[1] = abb[54] + z[0];
z[2] = abb[50] + abb[52];
z[3] = z[1] + z[2];
z[4] = -abb[56] + z[3];
z[5] = abb[17] * z[4];
z[6] = -abb[20] + abb[22];
z[7] = abb[57] * z[6];
z[7] = z[5] + z[7];
z[8] = (T(1) / T(2)) * z[0];
z[9] = abb[54] * (T(1) / T(2)) + z[8];
z[10] = abb[56] * (T(1) / T(2));
z[11] = z[9] + -z[10];
z[12] = -abb[55] + z[11];
z[13] = abb[0] * z[12];
z[14] = (T(1) / T(2)) * z[7] + z[13];
z[15] = -abb[46] + abb[47] + abb[48] + -abb[49];
z[16] = (T(1) / T(2)) * z[15];
z[17] = abb[28] * z[16];
z[18] = z[14] + z[17];
z[19] = (T(1) / T(2)) * z[2];
z[20] = z[1] + z[19];
z[21] = abb[16] * z[20];
z[22] = abb[27] * z[16];
z[21] = z[21] + -z[22];
z[23] = z[18] + -z[21];
z[24] = abb[24] * z[16];
z[25] = abb[55] + z[19];
z[26] = abb[6] * z[25];
z[24] = z[24] + z[26];
z[26] = abb[23] * z[15];
z[27] = (T(1) / T(2)) * z[26];
z[28] = z[24] + z[27];
z[29] = abb[26] * z[16];
z[16] = abb[25] * z[16];
z[30] = z[16] + z[29];
z[31] = abb[8] * z[25];
z[32] = -z[30] + z[31];
z[33] = abb[3] * z[12];
z[34] = -abb[55] + z[1];
z[35] = abb[52] + z[34];
z[36] = -abb[56] + z[35];
z[36] = abb[10] * z[36];
z[37] = abb[52] + abb[55];
z[37] = abb[9] * z[37];
z[38] = abb[1] * z[34];
z[33] = z[23] + -z[28] + z[32] + z[33] + -z[36] + z[37] + z[38];
z[33] = abb[31] * z[33];
z[39] = abb[11] * z[1];
z[40] = abb[5] * z[25];
z[41] = z[39] + -z[40];
z[42] = abb[4] * z[19];
z[43] = -z[38] + z[42];
z[44] = abb[10] * z[4];
z[45] = abb[28] * z[15];
z[46] = abb[56] + z[1];
z[46] = abb[3] * z[46];
z[46] = -z[45] + z[46];
z[14] = -z[14] + -z[41] + -z[43] + z[44] + (T(1) / T(2)) * z[46];
z[14] = abb[33] * z[14];
z[46] = abb[54] + z[2];
z[47] = abb[56] + z[0];
z[48] = -z[46] + z[47];
z[48] = abb[3] * z[48];
z[49] = abb[7] * z[2];
z[49] = z[26] + z[49];
z[48] = z[48] + -z[49];
z[50] = -abb[55] + z[0];
z[51] = -z[19] + z[50];
z[51] = abb[5] * z[51];
z[47] = abb[14] * z[47];
z[51] = -z[47] + z[51];
z[16] = z[16] + -z[38];
z[52] = -abb[56] + z[19];
z[53] = abb[8] * z[52];
z[18] = z[16] + z[18] + (T(-1) / T(2)) * z[48] + z[51] + -z[53];
z[18] = abb[32] * z[18];
z[50] = abb[3] * z[50];
z[16] = z[16] + z[21] + -z[31] + (T(1) / T(2)) * z[49] + -z[50];
z[16] = abb[34] * z[16];
z[14] = z[14] + -z[16] + z[18] + z[33];
z[31] = z[21] + z[39];
z[32] = z[32] + z[40];
z[33] = abb[3] * z[34];
z[33] = -z[31] + z[32] + z[33];
z[34] = abb[50] + abb[55];
z[34] = abb[10] * z[34];
z[39] = -z[28] + z[34];
z[49] = z[37] + z[39];
z[50] = z[33] + z[49];
z[53] = (T(1) / T(4)) * z[2];
z[54] = abb[4] * z[53];
z[50] = z[38] + (T(1) / T(2)) * z[50] + -z[54];
z[54] = -abb[35] * z[50];
z[14] = (T(1) / T(2)) * z[14] + z[54];
z[54] = z[12] + z[19];
z[54] = abb[3] * z[54];
z[55] = abb[55] + abb[56];
z[56] = abb[8] * z[55];
z[51] = z[23] + z[51] + z[54] + z[56];
z[54] = abb[36] * (T(1) / T(8));
z[57] = -z[51] * z[54];
z[14] = (T(1) / T(4)) * z[14] + z[57];
z[14] = m1_set::bc<T>[0] * z[14];
z[57] = abb[25] * z[15];
z[58] = z[45] + z[57];
z[59] = abb[8] * z[4];
z[60] = abb[19] * abb[57];
z[61] = abb[5] * z[2];
z[48] = -z[7] + z[48] + -z[58] + z[59] + z[60] + z[61];
z[48] = (T(1) / T(16)) * z[48];
z[60] = -abb[44] * z[48];
z[61] = -abb[50] + abb[52];
z[62] = abb[16] * (T(1) / T(2));
z[61] = z[61] * z[62];
z[22] = z[22] + z[61];
z[61] = z[22] + -z[30];
z[62] = abb[8] * z[19];
z[63] = abb[5] * z[19];
z[35] = abb[13] * z[35];
z[39] = z[35] + -z[39] + -z[61] + -z[62] + -z[63];
z[62] = -z[38] + z[39];
z[64] = abb[4] * z[2];
z[62] = (T(1) / T(8)) * z[62] + (T(1) / T(16)) * z[64];
z[62] = abb[43] * z[62];
z[28] = z[17] + -z[28] + z[61];
z[61] = abb[57] * (T(1) / T(4));
z[65] = abb[19] * z[61];
z[59] = (T(1) / T(4)) * z[59] + z[65];
z[5] = (T(1) / T(4)) * z[5];
z[66] = abb[22] * z[61];
z[67] = z[5] + z[59] + z[66];
z[68] = (T(1) / T(2)) * z[13];
z[28] = (T(1) / T(2)) * z[28] + -z[36] + z[67] + z[68];
z[28] = (T(1) / T(4)) * z[28];
z[36] = abb[42] * z[28];
z[69] = abb[55] + z[46];
z[70] = abb[34] * z[69];
z[71] = abb[31] * z[55];
z[72] = abb[33] * z[55];
z[70] = z[70] + z[71] + -z[72];
z[71] = (T(-1) / T(2)) * z[70];
z[72] = -abb[55] + z[46];
z[72] = -abb[56] + (T(1) / T(2)) * z[72];
z[72] = abb[32] * z[72];
z[73] = -abb[36] * z[55];
z[73] = -z[71] + -z[72] + z[73];
z[73] = m1_set::bc<T>[0] * z[73];
z[74] = -abb[56] + z[46];
z[75] = abb[44] * z[74];
z[73] = z[73] + (T(-1) / T(2)) * z[75];
z[75] = abb[2] * (T(1) / T(4));
z[73] = z[73] * z[75];
z[14] = abb[61] + z[14] + z[36] + z[60] + z[62] + z[73];
z[36] = abb[15] * z[19];
z[60] = -z[24] + z[36];
z[62] = abb[55] + z[2];
z[73] = z[1] + z[62];
z[73] = abb[18] * z[73];
z[3] = -abb[3] * z[3];
z[3] = -z[3] + -z[17] + z[38] + z[40] + -z[60] + -z[73];
z[76] = -abb[56] + z[0];
z[76] = (T(1) / T(4)) * z[76];
z[77] = abb[54] * (T(1) / T(4));
z[78] = z[76] + z[77];
z[79] = -z[25] + z[78];
z[79] = abb[0] * z[79];
z[79] = z[5] + z[79];
z[80] = -abb[50] + abb[55];
z[80] = -abb[52] + abb[56] + -z[1] + (T(1) / T(2)) * z[80];
z[80] = abb[10] * z[80];
z[81] = abb[8] * (T(1) / T(2));
z[82] = z[12] * z[81];
z[83] = (T(1) / T(2)) * z[37];
z[3] = (T(-1) / T(2)) * z[3] + z[65] + z[66] + z[79] + z[80] + z[82] + z[83];
z[3] = prod_pow(abb[31], 2) * z[3];
z[66] = abb[15] * z[2];
z[66] = z[26] + z[66];
z[57] = z[57] + z[66];
z[11] = z[2] + z[11];
z[82] = abb[0] * (T(1) / T(2));
z[11] = z[11] * z[82];
z[82] = abb[26] * z[15];
z[82] = (T(1) / T(4)) * z[82];
z[45] = (T(1) / T(4)) * z[45];
z[11] = -z[11] + -z[38] + -z[45] + (T(1) / T(4)) * z[57] + z[82];
z[5] = z[5] + -z[21];
z[57] = -abb[20] + abb[22] * (T(1) / T(2));
z[84] = abb[57] * (T(1) / T(2));
z[57] = z[57] * z[84];
z[12] = z[12] + z[53];
z[12] = abb[3] * z[12];
z[53] = abb[55] + z[53];
z[78] = z[53] + -z[78];
z[78] = abb[8] * z[78];
z[12] = z[5] + -z[11] + z[12] + z[57] + -z[65] + z[78];
z[12] = abb[31] * z[12];
z[12] = z[12] + -z[16] + (T(1) / T(2)) * z[18];
z[12] = abb[32] * z[12];
z[16] = (T(1) / T(2)) * z[66];
z[1] = z[1] + -z[52];
z[1] = abb[3] * z[1];
z[1] = z[1] + -z[16] + -z[17] + -z[30];
z[1] = (T(1) / T(2)) * z[1] + z[38] + -z[41] + -z[57] + z[59] + -z[79];
z[1] = abb[32] * z[1];
z[17] = z[44] + -z[73];
z[18] = abb[55] + (T(3) / T(4)) * z[2];
z[18] = abb[3] * z[18];
z[11] = z[11] + z[17] + z[18] + z[31] + -z[67];
z[11] = abb[31] * z[11];
z[18] = abb[3] * z[25];
z[18] = z[18] + z[31] + z[43] + -z[73];
z[18] = abb[34] * z[18];
z[20] = abb[3] * z[20];
z[20] = z[20] + z[32] + z[38] + -z[73];
z[30] = abb[0] * z[62];
z[16] = z[16] + -z[30];
z[31] = -z[16] + z[20] + z[42];
z[32] = abb[33] * z[31];
z[1] = z[1] + z[11] + z[18] + (T(-1) / T(2)) * z[32];
z[1] = abb[33] * z[1];
z[11] = abb[7] * z[19];
z[18] = z[11] + -z[73];
z[27] = -z[18] + -z[27] + z[35];
z[32] = abb[56] * (T(1) / T(3));
z[0] = (T(-1) / T(3)) * z[0] + z[32] + -z[46];
z[0] = abb[3] * z[0];
z[0] = (T(1) / T(2)) * z[0] + (T(1) / T(3)) * z[27] + z[47] + (T(-1) / T(6)) * z[58];
z[13] = z[13] + -z[38];
z[27] = -z[8] + (T(1) / T(3)) * z[53];
z[27] = abb[5] * z[27];
z[35] = -abb[55] + z[19];
z[35] = -abb[56] + (T(1) / T(3)) * z[35];
z[35] = z[35] * z[81];
z[0] = (T(1) / T(2)) * z[0] + (T(-1) / T(12)) * z[7] + (T(-1) / T(6)) * z[13] + z[27] + z[35] + (T(1) / T(3)) * z[80];
z[7] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = z[0] * z[7];
z[13] = -z[29] + z[37];
z[19] = abb[3] * z[19];
z[19] = z[19] + z[22];
z[11] = -z[11] + -z[13] + z[19] + z[24] + -z[63];
z[11] = abb[39] * z[11];
z[22] = -z[39] + -z[43];
z[22] = abb[59] * z[22];
z[27] = abb[20] + -abb[21];
z[2] = z[2] * z[27];
z[27] = -abb[19] + -abb[22];
z[4] = z[4] * z[27];
z[27] = -abb[0] + -abb[8] + -abb[17];
z[27] = abb[57] * z[27];
z[15] = -abb[29] * z[15];
z[2] = z[2] + z[4] + z[15] + z[27];
z[4] = abb[30] * abb[57];
z[2] = (T(1) / T(4)) * z[2] + z[4];
z[2] = abb[41] * z[2];
z[4] = abb[38] * z[31];
z[0] = z[0] + z[1] + z[2] + z[3] + -z[4] + z[11] + z[12] + z[22];
z[1] = z[18] + -z[24];
z[2] = z[1] + z[47] + -z[56];
z[3] = (T(1) / T(2)) * z[34] + z[83];
z[4] = -abb[55] + z[76] + -z[77];
z[4] = abb[3] * z[4];
z[8] = z[8] + -z[25];
z[8] = abb[5] * z[8];
z[6] = z[6] * z[61];
z[2] = (T(-1) / T(2)) * z[2] + -z[3] + z[4] + z[5] + z[6] + z[8] + z[45] + z[68] + z[82];
z[2] = abb[36] * z[2];
z[4] = abb[54] + z[25];
z[4] = abb[3] * z[4];
z[1] = z[1] + z[4] + z[13] + z[21] + z[34] + z[40];
z[1] = abb[34] * z[1];
z[4] = z[9] + z[10] + z[25];
z[4] = abb[3] * z[4];
z[4] = z[4] + z[17] + -z[23] + z[40];
z[5] = abb[31] + -abb[33];
z[4] = z[4] * z[5];
z[5] = -abb[32] * z[51];
z[1] = z[1] + z[2] + z[4] + z[5];
z[1] = z[1] * z[54];
z[2] = abb[60] * z[48];
z[4] = z[24] + z[26] + z[36];
z[5] = (T(1) / T(2)) * z[30];
z[3] = -z[3] + (T(1) / T(2)) * z[4] + -z[5] + -z[33];
z[3] = (T(1) / T(2)) * z[3] + -z[38] + (T(1) / T(8)) * z[64];
z[3] = abb[35] * z[3];
z[4] = z[16] + -z[33];
z[4] = (T(1) / T(2)) * z[4] + -z[38];
z[6] = -abb[31] + abb[32] + -abb[33];
z[4] = z[4] * z[6];
z[6] = abb[34] * z[50];
z[3] = z[3] + z[4] + z[6];
z[3] = abb[35] * z[3];
z[4] = -z[19] + z[60];
z[4] = (T(1) / T(2)) * z[4] + -z[5] + z[37];
z[4] = abb[37] * z[4];
z[3] = z[3] + z[4];
z[4] = z[20] + z[49];
z[5] = -abb[31] + abb[34] * (T(-1) / T(2));
z[5] = abb[34] * z[5];
z[5] = -abb[40] + z[5];
z[5] = (T(1) / T(8)) * z[5];
z[4] = z[4] * z[5];
z[5] = -abb[58] * z[28];
z[6] = z[70] + -z[72];
z[6] = abb[32] * z[6];
z[8] = abb[39] * z[69];
z[9] = abb[60] * z[74];
z[6] = z[6] + z[8] + z[9];
z[8] = -abb[32] * z[55];
z[9] = 3 * abb[55] + z[46];
z[9] = abb[56] + (T(1) / T(2)) * z[9];
z[9] = abb[36] * z[9];
z[8] = z[8] + (T(1) / T(2)) * z[9] + z[71];
z[8] = abb[36] * z[8];
z[9] = -abb[55] + (T(1) / T(3)) * z[46];
z[9] = (T(1) / T(4)) * z[9] + -z[32];
z[7] = z[7] * z[9];
z[6] = (T(1) / T(2)) * z[6] + z[7] + z[8];
z[6] = z[6] * z[75];
z[0] = abb[45] + (T(1) / T(8)) * z[0] + z[1] + z[2] + (T(1) / T(4)) * z[3] + z[4] + z[5] + z[6];
return {z[14], z[0]};
}


template <typename T> std::complex<T> f_4_445_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.48810413056084514581620813306394554149779815996143888170939218584"),stof<T>("0.32611234922308966845409602241346304780841904853983561223823301383")}, std::complex<T>{stof<T>("1.15043948057849416651173275012352639563720853081341291451433959248"),stof<T>("0.2690314614127623539218746127261972499728562629554907629540115299")}, std::complex<T>{stof<T>("0.66975245473728611276975500525192507976669199204069977311268657859"),stof<T>("0.40298927007534369527724698995049255140113412471783069361771071852")}, std::complex<T>{stof<T>("1.15043948057849416651173275012352639563720853081341291451433959248"),stof<T>("0.2690314614127623539218746127261972499728562629554907629540115299")}, std::complex<T>{stof<T>("0.61017861691621241820253985035969695454675487539474654963855589364"),stof<T>("0.2690314614127623539218746127261972499728562629554907629540115299")}, std::complex<T>{stof<T>("-0.46943758452998675192896355578828296660541952677811381006210771456"),stof<T>("0.51633675651472834683791330164952579570436698278292195087025940511")}, std::complex<T>{stof<T>("-0.20367351587692326052492846867765289984606122414154710521258928289"),stof<T>("0.08529189711902360463517163720631310369835698266224375937809538618")}, std::complex<T>{stof<T>("-0.25186751860815181882323830401799984856501583090687365866823217018"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_445_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_445_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_445_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + -4 * v[0] + v[1] + -3 * v[2] + -v[3] + 3 * v[4] + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[52] + abb[54] + abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[1];
z[0] = -abb[52] + -abb[54] + -abb[55];
return abb[12] * abb[39] * (T(1) / T(8)) * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_445_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.47993850471359930014718455092534543292138343170674402108251270148"),stof<T>("0.10972068855864484282781305002504959913323014567398279971250554171")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W18(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W86(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k), T{0}};
abb[45] = SpDLog_f_4_445_W_20_Im(t, path, abb);
abb[61] = SpDLog_f_4_445_W_20_Re(t, path, abb);

                    
            return f_4_445_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_445_DLogXconstant_part(base_point<T>, kend);
	value += f_4_445_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_445_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_445_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_445_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_445_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_445_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_445_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
