/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_613.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_613_abbreviated (const std::array<T,62>& abb) {
T z[111];
z[0] = abb[30] * (T(1) / T(2));
z[1] = abb[28] * (T(1) / T(2));
z[2] = -abb[29] + z[1];
z[3] = z[0] + -z[2];
z[3] = abb[19] * z[3];
z[4] = abb[31] + z[2];
z[5] = -z[0] + z[4];
z[5] = abb[20] * z[5];
z[6] = abb[27] * (T(1) / T(2));
z[7] = abb[19] + -abb[23];
z[7] = z[6] * z[7];
z[8] = abb[22] + abb[23];
z[8] = abb[24] + (T(1) / T(2)) * z[8];
z[8] = abb[30] * z[8];
z[9] = -abb[28] + abb[29];
z[10] = -abb[32] + z[9];
z[10] = abb[24] * z[10];
z[11] = abb[22] * z[4];
z[12] = abb[23] * abb[32];
z[1] = abb[23] * z[1];
z[3] = -z[1] + z[3] + -z[5] + -z[7] + z[8] + z[10] + -z[11] + -z[12];
z[3] = abb[27] * z[3];
z[5] = prod_pow(abb[32], 2);
z[5] = abb[33] + (T(1) / T(2)) * z[5];
z[7] = abb[31] * (T(1) / T(2));
z[8] = -abb[29] + z[7];
z[8] = abb[31] * z[8];
z[10] = prod_pow(abb[29], 2);
z[8] = abb[36] + -z[5] + z[8] + (T(1) / T(2)) * z[10];
z[10] = -abb[31] + abb[32];
z[2] = z[2] + -z[10];
z[11] = abb[30] * z[2];
z[13] = abb[34] + z[8] + -z[11];
z[13] = abb[20] * z[13];
z[14] = abb[22] * z[2];
z[15] = abb[29] + z[10];
z[15] = abb[24] * z[15];
z[1] = z[1] + -z[14] + z[15];
z[14] = abb[30] * z[1];
z[8] = abb[22] * z[8];
z[15] = -abb[29] + abb[31];
z[15] = z[9] * z[15];
z[16] = abb[28] + z[10];
z[16] = abb[32] * z[16];
z[16] = abb[33] + -abb[59] + z[15] + z[16];
z[16] = abb[24] * z[16];
z[17] = abb[32] * z[10];
z[17] = -abb[59] + z[17];
z[11] = z[11] + z[17];
z[11] = abb[19] * z[11];
z[10] = abb[30] * z[10];
z[10] = z[10] + -z[17];
z[10] = abb[21] * z[10];
z[17] = abb[22] + abb[24];
z[17] = abb[34] * z[17];
z[18] = abb[23] * abb[33];
z[19] = abb[25] * abb[60];
z[20] = abb[19] + abb[24];
z[21] = abb[57] * z[20];
z[22] = abb[28] * z[12];
z[23] = abb[19] + abb[20] + abb[22];
z[24] = abb[21] + -abb[23] + z[23];
z[25] = abb[58] * z[24];
z[3] = z[3] + -z[8] + -z[10] + z[11] + -z[13] + -z[14] + z[16] + -z[17] + z[18] + (T(1) / T(2)) * z[19] + z[21] + z[22] + -z[25];
z[8] = -abb[23] + abb[21] * (T(5) / T(2)) + (T(3) / T(2)) * z[23];
z[10] = prod_pow(m1_set::bc<T>[0], 2);
z[8] = z[8] * z[10];
z[3] = 3 * z[3] + -z[8];
z[8] = 3 * abb[35];
z[11] = z[8] * z[23];
z[11] = z[3] + z[11];
z[13] = abb[54] + abb[56];
z[14] = (T(1) / T(2)) * z[13];
z[11] = z[11] * z[14];
z[14] = abb[53] + abb[55];
z[14] = (T(1) / T(2)) * z[14];
z[3] = z[3] * z[14];
z[16] = abb[46] + abb[50];
z[17] = 2 * abb[45];
z[18] = abb[44] + z[16] + z[17];
z[19] = abb[43] * (T(4) / T(3));
z[21] = abb[47] * (T(1) / T(2));
z[22] = abb[49] * (T(1) / T(2));
z[18] = (T(-1) / T(3)) * z[18] + -z[19] + z[21] + z[22];
z[18] = abb[2] * z[18];
z[25] = -abb[44] + abb[50];
z[26] = abb[14] * z[25];
z[27] = -abb[51] + abb[52];
z[28] = abb[18] * z[27];
z[26] = z[26] + z[28];
z[28] = -abb[16] * z[27];
z[29] = -abb[2] + abb[6];
z[30] = abb[4] + z[29];
z[31] = abb[48] * z[30];
z[31] = z[26] + (T(-9) / T(2)) * z[28] + -z[31];
z[32] = abb[46] * (T(1) / T(2));
z[33] = abb[49] * (T(3) / T(2));
z[34] = z[32] + -z[33];
z[35] = 2 * abb[43] + abb[45];
z[36] = z[34] + z[35];
z[37] = abb[44] * (T(1) / T(2));
z[38] = abb[50] * (T(1) / T(2));
z[39] = z[37] + z[38];
z[40] = z[36] + z[39];
z[41] = abb[12] * z[40];
z[42] = abb[50] * (T(3) / T(4));
z[43] = -abb[45] + abb[44] * (T(-13) / T(4));
z[43] = abb[43] * (T(-2) / T(3)) + abb[42] * (T(1) / T(12)) + abb[46] * (T(5) / T(6)) + -z[22] + z[42] + (T(1) / T(3)) * z[43];
z[43] = abb[5] * z[43];
z[44] = abb[44] * (T(1) / T(4));
z[45] = -z[17] + z[44];
z[19] = abb[46] * (T(-1) / T(12)) + abb[42] * (T(1) / T(6)) + abb[49] * (T(5) / T(4)) + -z[19] + -z[42] + (T(1) / T(3)) * z[45];
z[19] = abb[6] * z[19];
z[45] = -abb[49] + abb[50];
z[46] = -abb[47] + z[35] + z[45];
z[47] = 3 * abb[9];
z[48] = z[46] * z[47];
z[49] = abb[46] * (T(5) / T(2));
z[50] = 2 * abb[44] + z[35];
z[51] = -abb[50] + z[50];
z[52] = z[49] + -z[51];
z[52] = abb[4] * z[52];
z[53] = abb[45] + abb[50];
z[54] = abb[42] + -abb[46];
z[55] = abb[47] * (T(-13) / T(2)) + abb[49] * (T(-1) / T(4)) + abb[43] * (T(14) / T(3)) + (T(7) / T(3)) * z[53] + (T(25) / T(12)) * z[54];
z[55] = abb[0] * z[55];
z[56] = abb[46] + -abb[49];
z[57] = z[25] + z[56];
z[58] = abb[8] * z[57];
z[59] = abb[14] * abb[42];
z[60] = (T(1) / T(2)) * z[59];
z[61] = -abb[47] + z[25];
z[61] = abb[11] * z[61];
z[62] = 3 * z[61];
z[63] = abb[15] * z[27];
z[64] = -abb[42] + z[25];
z[65] = abb[1] * z[64];
z[18] = z[18] + z[19] + (T(1) / T(2)) * z[31] + z[41] + z[43] + -z[48] + (T(1) / T(3)) * z[52] + z[55] + (T(-5) / T(4)) * z[58] + -z[60] + z[62] + 2 * z[63] + (T(-10) / T(3)) * z[65];
z[18] = z[10] * z[18];
z[19] = -z[25] + z[56];
z[31] = abb[7] * z[19];
z[43] = z[26] + z[31];
z[52] = z[28] + z[43];
z[55] = abb[7] * abb[47];
z[52] = (T(-1) / T(2)) * z[52] + -z[55] + z[63];
z[53] = -z[33] + z[49] + -z[53];
z[58] = abb[42] * (T(5) / T(4));
z[66] = 3 * abb[47];
z[53] = -abb[43] + (T(1) / T(2)) * z[53] + -z[58] + z[66];
z[53] = abb[0] * z[53];
z[67] = abb[2] * z[40];
z[34] = abb[45] + z[34];
z[39] = z[34] + z[39];
z[39] = abb[43] + (T(1) / T(2)) * z[39];
z[68] = 3 * abb[12];
z[69] = z[39] * z[68];
z[70] = abb[4] * z[39];
z[45] = abb[45] + z[45];
z[21] = abb[43] + -z[21] + (T(1) / T(2)) * z[45];
z[21] = z[21] * z[47];
z[45] = abb[44] + z[34];
z[47] = abb[42] * (T(1) / T(4));
z[45] = abb[43] + (T(1) / T(2)) * z[45] + z[47];
z[71] = abb[5] * z[45];
z[72] = abb[42] * (T(1) / T(2));
z[73] = abb[44] + z[36] + z[72];
z[74] = abb[6] * z[73];
z[75] = (T(3) / T(4)) * z[59];
z[52] = z[21] + (T(-3) / T(2)) * z[52] + z[53] + -z[65] + z[67] + -z[69] + z[70] + z[71] + z[74] + -z[75];
z[52] = abb[27] * z[52];
z[66] = -z[35] + z[66];
z[70] = 2 * abb[50];
z[71] = abb[44] + abb[46];
z[74] = -z[66] + z[70] + -z[71];
z[76] = 2 * abb[13];
z[74] = z[74] * z[76];
z[76] = (T(1) / T(2)) * z[31] + z[55];
z[77] = -z[48] + 3 * z[76];
z[78] = abb[6] * z[40];
z[79] = abb[5] * z[40];
z[80] = 3 * abb[49];
z[17] = 4 * abb[43] + z[17] + -z[80];
z[71] = abb[50] + z[17] + z[71];
z[81] = abb[2] * z[71];
z[79] = z[79] + z[81];
z[82] = abb[4] * z[40];
z[83] = -z[74] + -z[77] + z[78] + -z[79] + z[82];
z[84] = abb[31] * z[83];
z[85] = abb[13] * z[40];
z[86] = -abb[50] + -z[36] + z[72];
z[86] = abb[0] * z[86];
z[87] = z[85] + z[86];
z[88] = abb[5] * (T(1) / T(2));
z[89] = z[64] * z[88];
z[26] = -z[26] + z[28];
z[90] = (T(3) / T(2)) * z[59];
z[89] = (T(3) / T(2)) * z[26] + 4 * z[65] + -z[82] + z[87] + -z[89] + z[90];
z[91] = abb[6] * z[64];
z[92] = z[89] + 2 * z[91];
z[92] = abb[32] * z[92];
z[68] = z[40] * z[68];
z[68] = z[68] + -z[85];
z[71] = abb[5] * z[71];
z[71] = z[71] + 2 * z[81];
z[93] = z[68] + -z[71] + -z[82];
z[93] = abb[29] * z[93];
z[94] = abb[50] * (T(1) / T(4));
z[95] = abb[44] * (T(3) / T(4)) + z[36] + z[47] + z[94];
z[95] = abb[5] * z[95];
z[69] = z[69] + -z[81];
z[75] = (T(3) / T(4)) * z[26] + z[75];
z[96] = z[69] + -z[75];
z[95] = z[85] + z[95] + -z[96];
z[97] = abb[28] * z[95];
z[98] = abb[44] * (T(3) / T(2));
z[99] = abb[50] * (T(5) / T(2));
z[100] = -z[34] + z[98] + -z[99];
z[100] = abb[42] + -abb[43] + (T(1) / T(2)) * z[100];
z[101] = abb[6] * z[100];
z[102] = abb[50] + z[34];
z[102] = abb[43] + -z[47] + (T(1) / T(2)) * z[102];
z[102] = abb[0] * z[102];
z[103] = 2 * z[65];
z[95] = -z[95] + z[101] + z[102] + -z[103];
z[95] = abb[30] * z[95];
z[40] = abb[29] * z[40];
z[100] = -abb[28] * z[100];
z[100] = -z[40] + z[100];
z[100] = abb[6] * z[100];
z[101] = 2 * abb[28];
z[104] = z[65] * z[101];
z[105] = abb[28] * z[102];
z[104] = z[104] + -z[105];
z[52] = z[52] + z[84] + z[92] + z[93] + z[95] + z[97] + z[100] + z[104];
z[52] = abb[27] * z[52];
z[84] = (T(3) / T(2)) * z[76];
z[92] = -z[21] + z[84];
z[94] = abb[49] * (T(-3) / T(4)) + abb[46] * (T(-1) / T(4)) + abb[44] * (T(5) / T(4)) + z[35] + -z[94];
z[94] = abb[4] * z[94];
z[80] = -3 * abb[46] + -z[25] + z[80];
z[80] = -abb[42] + (T(1) / T(2)) * z[80];
z[80] = z[80] * z[88];
z[88] = (T(1) / T(2)) * z[65];
z[53] = z[53] + -z[62] + z[80] + -z[85] + z[88] + (T(-5) / T(4)) * z[91] + -z[92] + z[94];
z[53] = abb[32] * z[53];
z[80] = -abb[28] * z[89];
z[22] = z[22] + -z[35];
z[89] = z[32] + z[37];
z[94] = abb[50] * (T(3) / T(2));
z[95] = 2 * abb[47] + z[22] + z[89] + -z[94];
z[95] = abb[13] * z[95];
z[46] = abb[9] * z[46];
z[46] = z[46] + -z[76] + z[95];
z[22] = -z[22] + z[98];
z[32] = z[32] + z[38];
z[95] = z[22] + -z[32];
z[95] = abb[4] * z[95];
z[97] = (T(1) / T(2)) * z[57];
z[100] = abb[5] * z[97];
z[95] = z[46] + z[95] + -z[100];
z[100] = 3 * z[95];
z[105] = -abb[31] * z[100];
z[106] = -z[91] * z[101];
z[53] = z[53] + z[80] + z[105] + z[106];
z[53] = abb[32] * z[53];
z[19] = abb[47] + (T(1) / T(2)) * z[19];
z[19] = abb[13] * z[19];
z[80] = (T(1) / T(2)) * z[91];
z[91] = z[65] + z[80];
z[56] = abb[47] + (T(1) / T(2)) * z[56] + -z[72];
z[56] = abb[0] * z[56];
z[43] = -z[19] + (T(1) / T(2)) * z[43] + z[55] + z[56] + -z[60] + -z[91];
z[43] = abb[33] * z[43];
z[56] = -abb[4] * z[101];
z[60] = abb[2] + -abb[4];
z[60] = -abb[29] * z[60];
z[56] = z[56] + z[60];
z[56] = abb[29] * z[56];
z[60] = prod_pow(abb[28], 2);
z[105] = 2 * z[60];
z[106] = abb[29] + -z[101];
z[106] = abb[29] * z[106];
z[106] = z[105] + z[106];
z[106] = abb[6] * z[106];
z[107] = abb[4] + abb[6];
z[9] = -z[9] * z[107];
z[29] = abb[31] * z[29];
z[9] = 2 * z[9] + z[29];
z[9] = abb[31] * z[9];
z[29] = abb[4] * z[60];
z[30] = abb[34] * z[30];
z[9] = z[9] + z[29] + z[30] + z[56] + z[106];
z[9] = abb[48] * z[9];
z[29] = -abb[46] + -abb[49] + z[25];
z[29] = (T(1) / T(2)) * z[29];
z[30] = z[29] * z[107];
z[56] = -abb[47] + abb[49];
z[56] = abb[2] * z[56];
z[19] = z[19] + z[30] + z[56] + -z[76];
z[19] = abb[34] * z[19];
z[30] = abb[4] * z[57];
z[56] = abb[5] * z[64];
z[30] = z[26] + -z[30] + -z[56] + z[59];
z[59] = abb[8] * z[97];
z[30] = (T(1) / T(2)) * z[30] + z[59] + z[91];
z[97] = abb[58] * z[30];
z[9] = z[9] + z[19] + z[43] + -z[97];
z[19] = z[33] + z[38] + -z[66] + z[89];
z[19] = abb[2] * z[19];
z[43] = abb[5] * z[39];
z[89] = abb[44] * (T(-7) / T(2)) + -z[33] + z[99];
z[97] = abb[46] * (T(7) / T(2));
z[99] = z[89] + -z[97];
z[106] = -abb[45] + z[99];
z[106] = -abb[43] + (T(1) / T(2)) * z[106];
z[108] = abb[4] * z[106];
z[21] = z[19] + -z[21] + z[43] + -z[84] + z[85] + z[108];
z[21] = abb[29] * z[21];
z[43] = 2 * abb[46];
z[51] = z[43] + z[51];
z[84] = abb[4] * z[51];
z[108] = z[79] + 2 * z[84] + -z[85];
z[108] = abb[28] * z[108];
z[21] = z[21] + z[108];
z[21] = abb[29] * z[21];
z[83] = abb[36] * z[83];
z[109] = abb[50] * (T(7) / T(2)) + z[33];
z[97] = 5 * abb[45] + abb[44] * (T(17) / T(2)) + -z[97] + -z[109];
z[97] = 5 * abb[43] + (T(1) / T(2)) * z[97];
z[97] = abb[4] * z[97];
z[89] = abb[45] + -z[49] + -z[89];
z[89] = abb[43] + (T(1) / T(2)) * z[89];
z[89] = abb[5] * z[89];
z[110] = abb[6] * z[106];
z[19] = z[19] + z[62] + -z[74] + z[89] + -z[92] + z[97] + z[110];
z[19] = abb[31] * z[19];
z[62] = abb[44] * (T(5) / T(2));
z[49] = -6 * abb[47] + z[35] + -z[49] + -z[62] + z[109];
z[49] = abb[13] * z[49];
z[74] = -z[35] + z[99];
z[89] = -abb[4] * z[74];
z[49] = z[49] + z[71] + z[77] + z[89];
z[49] = abb[29] * z[49];
z[51] = z[51] * z[101];
z[71] = -abb[29] * z[74];
z[71] = -z[51] + z[71];
z[71] = abb[6] * z[71];
z[19] = z[19] + z[49] + z[71] + -z[108];
z[19] = abb[31] * z[19];
z[16] = -z[16] + z[50];
z[49] = abb[5] * z[16];
z[16] = abb[4] * z[16];
z[49] = -2 * z[16] + -z[49] + z[78] + -z[81] + z[85];
z[49] = abb[31] * z[49];
z[44] = -z[36] + -z[42] + -z[44] + z[47];
z[44] = abb[5] * z[44];
z[44] = z[44] + z[69] + z[75] + -z[82];
z[44] = abb[28] * z[44];
z[47] = z[34] + z[62] + -z[94];
z[47] = abb[42] + abb[43] + (T(1) / T(2)) * z[47];
z[47] = abb[28] * z[47];
z[40] = -z[40] + z[47];
z[40] = abb[6] * z[40];
z[40] = z[40] + -z[44] + z[49] + z[93] + -z[104];
z[32] = abb[43] + abb[44] + abb[45] * (T(1) / T(2)) + -z[32];
z[32] = abb[4] * z[32];
z[44] = -abb[43] + z[72];
z[47] = abb[44] + abb[45] + -abb[46];
z[47] = -z[44] + (T(1) / T(2)) * z[47];
z[47] = abb[5] * z[47];
z[45] = -abb[6] * z[45];
z[32] = z[32] + z[45] + z[47] + z[67] + z[88] + -z[102];
z[32] = abb[30] * z[32];
z[45] = -z[16] + z[56] + z[87] + z[91];
z[45] = abb[32] * z[45];
z[47] = abb[31] * z[57];
z[49] = abb[32] * z[57];
z[47] = z[47] + -z[49];
z[50] = abb[8] * (T(3) / T(2));
z[56] = z[47] * z[50];
z[32] = z[32] + -z[40] + z[45] + z[56];
z[32] = abb[30] * z[32];
z[45] = -abb[5] * z[73];
z[45] = (T(-3) / T(2)) * z[28] + z[45] + -z[65] + -z[81] + -z[84] + -z[86];
z[45] = z[45] * z[60];
z[56] = 3 * abb[5] + abb[14];
z[56] = z[27] * z[56];
z[60] = abb[16] + abb[18];
z[60] = z[60] * z[64];
z[56] = z[56] + z[60];
z[60] = -abb[6] + 2 * abb[26] + abb[0] * (T(-3) / T(4));
z[60] = z[27] * z[60];
z[56] = (T(1) / T(4)) * z[56] + -z[60];
z[60] = -abb[15] * z[39];
z[62] = abb[17] * z[39];
z[60] = z[56] + z[60] + z[62];
z[60] = 3 * z[60];
z[60] = abb[60] * z[60];
z[41] = z[41] + -z[79];
z[46] = z[41] + z[46];
z[46] = 3 * z[46];
z[62] = abb[57] * z[46];
z[64] = -abb[59] * z[100];
z[67] = abb[46] * (T(3) / T(2));
z[22] = 2 * abb[48] + -z[22] + z[38] + -z[67];
z[22] = z[22] * z[107];
z[14] = z[14] * z[23];
z[14] = z[14] + z[22] + z[41];
z[14] = z[8] * z[14];
z[22] = abb[29] * z[106];
z[22] = z[22] + z[51];
z[22] = abb[29] * z[22];
z[23] = abb[42] + -abb[44] + -z[35] + -z[43];
z[23] = z[23] * z[105];
z[22] = z[22] + z[23];
z[22] = abb[6] * z[22];
z[23] = -abb[32] * z[47];
z[35] = -abb[59] * z[57];
z[23] = z[23] + z[35];
z[23] = z[23] * z[50];
z[35] = abb[32] + z[0];
z[35] = abb[30] * z[35];
z[5] = abb[58] + -z[5] + z[35];
z[35] = abb[15] * (T(3) / T(2));
z[5] = z[5] * z[27] * z[35];
z[15] = -abb[34] + z[15];
z[8] = -z[8] + (T(1) / T(2)) * z[10] + 3 * z[15];
z[10] = abb[48] + z[29];
z[8] = abb[3] * z[8] * z[10];
z[3] = abb[61] + z[3] + z[5] + z[8] + 3 * z[9] + z[11] + z[14] + z[18] + z[19] + z[21] + z[22] + z[23] + z[32] + z[45] + z[52] + z[53] + z[60] + z[62] + z[64] + z[83];
z[5] = z[25] + -z[33] + z[67] + z[72];
z[5] = abb[5] * z[5];
z[8] = abb[50] + z[54] + -z[66];
z[9] = 2 * abb[0];
z[8] = z[8] * z[9];
z[8] = z[8] + -z[48];
z[9] = -z[26] + z[31];
z[9] = (T(1) / T(2)) * z[9] + z[55];
z[5] = z[5] + z[8] + 3 * z[9] + -z[16] + 6 * z[61] + -5 * z[65] + z[80] + z[85] + -z[90];
z[5] = abb[32] * z[5];
z[9] = abb[44] * (T(7) / T(4)) + z[36] + -z[42] + z[58];
z[9] = abb[5] * z[9];
z[10] = z[34] + -z[37] + z[94];
z[10] = (T(1) / T(2)) * z[10] + -z[44];
z[10] = abb[6] * z[10];
z[11] = z[50] * z[57];
z[9] = z[9] + z[10] + z[11] + z[16] + z[65] + -z[96] + z[102];
z[9] = abb[30] * z[9];
z[10] = -z[28] + z[63] + -z[76];
z[11] = -abb[42] + -z[36] + z[38] + -z[98];
z[11] = abb[5] * z[11];
z[14] = -z[17] + z[54] + -z[70];
z[14] = abb[6] * z[14];
z[8] = z[8] + 3 * z[10] + z[11] + z[14] + z[68] + -z[81] + -z[103];
z[8] = abb[27] * z[8];
z[10] = z[7] * z[57];
z[10] = z[10] + -z[49];
z[10] = abb[8] * z[10];
z[11] = -abb[30] + abb[32];
z[11] = z[11] * z[27] * z[35];
z[5] = z[5] + z[8] + z[9] + 3 * z[10] + z[11] + -z[40];
z[5] = m1_set::bc<T>[0] * z[5];
z[8] = abb[22] + -abb[23];
z[8] = z[0] * z[8];
z[1] = -z[1] + z[8] + z[12];
z[7] = -abb[32] + z[0] + z[7];
z[7] = abb[21] * z[7];
z[0] = z[0] + z[2];
z[2] = abb[20] * (T(1) / T(2));
z[0] = z[0] * z[2];
z[2] = z[6] * z[20];
z[4] = -abb[32] + abb[30] * (T(1) / T(4)) + (T(1) / T(2)) * z[4];
z[4] = abb[19] * z[4];
z[0] = z[0] + (T(1) / T(2)) * z[1] + z[2] + z[4] + z[7];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[21] + z[20];
z[1] = abb[39] * z[1];
z[2] = abb[37] * z[20];
z[1] = z[1] + -z[2];
z[0] = z[0] + (T(-1) / T(2)) * z[1];
z[1] = abb[53] * z[0];
z[2] = -z[59] + -z[95];
z[2] = abb[39] * z[2];
z[4] = (T(1) / T(2)) * z[24];
z[6] = -abb[53] * z[4];
z[6] = z[6] + -z[30] + (T(1) / T(2)) * z[63];
z[6] = abb[38] * z[6];
z[7] = -abb[15] + abb[17];
z[7] = z[7] * z[39];
z[8] = abb[25] * (T(1) / T(4));
z[9] = abb[53] * z[8];
z[7] = z[7] + z[9] + z[56];
z[7] = abb[40] * z[7];
z[1] = z[1] + z[2] + z[6] + z[7];
z[2] = abb[38] * z[4];
z[4] = abb[40] * z[8];
z[0] = z[0] + -z[2] + z[4];
z[2] = abb[55] + z[13];
z[2] = 3 * z[2];
z[0] = z[0] * z[2];
z[2] = abb[37] * z[46];
z[0] = abb[41] + z[0] + 3 * z[1] + z[2] + z[5];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_613_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.269512493905512545637669731671240915263504226021180975992455577"),stof<T>("-58.554694814178373260477368471541400794702884064369990307851491817")}, std::complex<T>{stof<T>("3.864737663879488353265753160505984425095431179935268491788284964"),stof<T>("-47.706147699074941751564270035516253070252721807743778987143419911")}, std::complex<T>{stof<T>("9.878152653184990155537763548562580744476206783623852519577901265"),stof<T>("-19.92935011442469201137941997400066842068505199455458671913663283")}, std::complex<T>{stof<T>("1.932368831939744176632876580252992212547715589967634245894142482"),stof<T>("-23.853073849537470875782135017758126535126360903871889493571709955")}, std::complex<T>{stof<T>("-18.268293638099721913065127805053267735940324506964062333523932316"),stof<T>("30.85558242216324742086829337306750850632132959989719188070629115")}, std::complex<T>{stof<T>("-20.592022310759988479797910568414920119275337539329025035832629111"),stof<T>("93.334000971454399545748376888366367415465522573584484962992860093")}, std::complex<T>{stof<T>("16.335924806159977736432251224800275523392608916996428087629789834"),stof<T>("-7.002508572625776545086158355309381971194968696025302387134581194")}, std::complex<T>{stof<T>("-7.9457838212452459789048869683095885319284911936562182736837587825"),stof<T>("-3.9237237351127788644027150437574581144413089093173027744350771258")}, std::complex<T>{stof<T>("-16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("-24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[125].real()/kbase.W[125].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_613_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_613_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(8)) * (-v[3] + v[5]) * (-24 + 4 * v[2] + -v[3] + -7 * v[5] + 4 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (3 * (-1 + 2 * m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return (2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[49]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[28] + abb[29];
z[1] = -abb[28] + -abb[31];
z[0] = z[0] * z[1];
z[0] = abb[35] + z[0];
z[1] = 2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[49];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_613_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_613_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-32.796205031715046932025671777143204995350881675711372250712685776"),stof<T>("48.465065132210457705915045996313609616872406605829586846137138145")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W16(k,dl), dlog_W26(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[125].real()/k.W[125].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), T{0}};
abb[41] = SpDLog_f_4_613_W_16_Im(t, path, abb);
abb[61] = SpDLog_f_4_613_W_16_Re(t, path, abb);

                    
            return f_4_613_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_613_DLogXconstant_part(base_point<T>, kend);
	value += f_4_613_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_613_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_613_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_613_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_613_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_613_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_613_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
