/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_479.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_479_abbreviated (const std::array<T,29>& abb) {
T z[34];
z[0] = 5 * abb[23];
z[1] = -abb[22] + abb[25];
z[2] = -abb[21] + z[1];
z[3] = abb[19] + -z[0] + z[2];
z[4] = abb[20] + abb[24];
z[5] = 4 * z[4];
z[6] = z[3] + z[5];
z[6] = abb[5] * z[6];
z[7] = -abb[19] + z[1];
z[8] = abb[1] * z[7];
z[9] = abb[19] + -abb[23];
z[10] = -abb[3] * z[9];
z[11] = z[8] + z[10];
z[12] = z[6] + -z[11];
z[13] = 2 * z[4];
z[14] = abb[23] + z[1];
z[15] = -z[13] + z[14];
z[15] = abb[4] * z[15];
z[16] = 2 * abb[23];
z[17] = -z[13] + z[16];
z[18] = abb[19] + -abb[21];
z[19] = z[17] + -z[18];
z[19] = abb[0] * z[19];
z[20] = 2 * abb[19];
z[14] = -z[14] + z[20];
z[14] = abb[7] * z[14];
z[15] = z[12] + -z[14] + -z[15] + z[19];
z[19] = abb[10] * z[15];
z[21] = 3 * abb[23];
z[22] = -z[13] + z[21];
z[23] = 2 * z[1];
z[24] = abb[21] + z[22] + -z[23];
z[24] = abb[5] * z[24];
z[25] = abb[0] * z[2];
z[25] = z[10] + z[25];
z[17] = z[7] + z[17];
z[26] = abb[4] * z[17];
z[24] = z[24] + z[25] + z[26];
z[27] = -abb[9] + abb[12];
z[24] = -z[24] * z[27];
z[28] = abb[19] * (T(1) / T(2));
z[29] = abb[23] * (T(1) / T(2)) + -z[13] + z[23] + -z[28];
z[29] = abb[4] * z[29];
z[3] = (T(1) / T(2)) * z[3] + z[13];
z[3] = abb[5] * z[3];
z[30] = -abb[23] + z[1];
z[31] = abb[7] * z[30];
z[31] = z[25] + -3 * z[31];
z[32] = (T(1) / T(2)) * z[8];
z[29] = -z[3] + z[29] + (T(1) / T(2)) * z[31] + z[32];
z[29] = abb[11] * z[29];
z[19] = z[19] + z[24] + z[29];
z[19] = abb[11] * z[19];
z[18] = z[13] + -z[16] + z[18];
z[18] = abb[7] * z[18];
z[24] = -abb[23] + z[4];
z[29] = abb[2] * z[24];
z[18] = z[18] + 2 * z[29];
z[29] = (T(1) / T(2)) * z[1];
z[31] = -abb[21] + z[29];
z[21] = abb[19] * (T(3) / T(2)) + z[4] + -z[21] + -z[31];
z[21] = abb[8] * z[21];
z[24] = z[24] + z[28];
z[31] = z[24] + z[31];
z[31] = abb[0] * z[31];
z[24] = -z[24] + z[29];
z[24] = abb[4] * z[24];
z[12] = -z[12] + z[18] + z[21] + z[24] + z[31];
z[12] = abb[15] * z[12];
z[21] = 2 * abb[21] + abb[23] * (T(11) / T(2)) + -5 * z[4] + -z[23] + -z[28];
z[21] = abb[5] * z[21];
z[24] = abb[23] * (T(-3) / T(2)) + z[2] + z[4] + z[28];
z[24] = abb[0] * z[24];
z[31] = 4 * abb[23] + -z[1];
z[13] = abb[19] + z[13] + -z[31];
z[33] = abb[8] * z[13];
z[18] = z[10] + z[18] + z[21] + z[24] + z[26] + z[33];
z[21] = abb[26] * z[18];
z[24] = abb[21] * (T(1) / T(2));
z[22] = abb[19] + -z[22] + -z[24] + z[29];
z[22] = abb[0] * z[22];
z[9] = abb[4] * z[9];
z[26] = z[9] + -z[10];
z[0] = z[0] + -z[1];
z[29] = (T(-1) / T(2)) * z[0] + z[20];
z[29] = abb[7] * z[29];
z[3] = -z[3] + z[22] + (T(-1) / T(2)) * z[26] + z[29] + -z[32];
z[3] = prod_pow(abb[10], 2) * z[3];
z[15] = z[15] + -z[33];
z[22] = abb[27] * z[15];
z[26] = abb[19] * (T(-11) / T(2)) + -z[4] + z[24] + 2 * z[31];
z[26] = abb[7] * z[26];
z[23] = abb[23] * (T(-43) / T(4)) + abb[19] * (T(11) / T(4)) + (T(13) / T(2)) * z[4] + z[23] + -z[24];
z[23] = abb[5] * z[23];
z[23] = z[23] + z[26];
z[26] = 7 * abb[23] + abb[19] * (T(-11) / T(6)) + (T(-3) / T(2)) * z[1] + (T(-11) / T(3)) * z[4];
z[26] = abb[0] * z[26];
z[29] = (T(-1) / T(4)) * z[1] + -z[16];
z[29] = abb[19] * (T(3) / T(4)) + (T(1) / T(3)) * z[29];
z[29] = abb[4] * z[29];
z[1] = abb[23] + (T(1) / T(3)) * z[1];
z[1] = (T(1) / T(4)) * z[1] + (T(-1) / T(3)) * z[4];
z[1] = abb[2] * z[1];
z[1] = z[1] + (T(3) / T(4)) * z[8] + (T(1) / T(3)) * z[23] + (T(1) / T(2)) * z[26] + z[29];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[4] = -abb[23] + z[24] + z[28];
z[4] = abb[5] * z[4];
z[23] = abb[21] + -abb[23];
z[24] = abb[0] * z[23];
z[26] = -z[10] + z[24];
z[4] = z[4] + (T(-1) / T(2)) * z[26];
z[28] = -prod_pow(abb[9], 2);
z[29] = prod_pow(abb[12], 2);
z[28] = z[28] + z[29];
z[4] = z[4] * z[28];
z[23] = -z[7] + z[23];
z[28] = -abb[13] + abb[14];
z[23] = z[23] * z[28];
z[27] = -abb[10] + abb[11] + -z[27];
z[13] = abb[11] * z[13] * z[27];
z[13] = z[13] + z[23];
z[13] = abb[8] * z[13];
z[7] = abb[4] * z[7];
z[7] = z[7] + z[25];
z[7] = abb[14] * z[7];
z[11] = -z[11] + z[24];
z[11] = abb[13] * z[11];
z[23] = -prod_pow(abb[11], 2);
z[23] = -abb[14] + z[23];
z[23] = abb[2] * z[23] * z[30];
z[1] = abb[28] + z[1] + z[3] + z[4] + z[7] + z[11] + z[12] + z[13] + z[19] + z[21] + z[22] + z[23];
z[3] = abb[16] * z[18];
z[4] = abb[17] * z[15];
z[2] = 6 * abb[23] + -z[2] + -z[5] + -z[20];
z[2] = abb[0] * z[2];
z[5] = z[8] + z[9];
z[0] = -4 * abb[19] + z[0];
z[0] = abb[7] * z[0];
z[0] = z[0] + z[2] + z[5] + z[6] + -z[10];
z[0] = abb[10] * z[0];
z[2] = -abb[0] + abb[5];
z[2] = z[2] * z[17];
z[2] = z[2] + z[5] + z[14];
z[2] = abb[11] * z[2];
z[5] = abb[19] + abb[21] + -z[16];
z[5] = abb[5] * z[5];
z[5] = z[5] + -z[26];
z[5] = abb[9] * z[5];
z[0] = z[0] + z[2] + z[5];
z[0] = m1_set::bc<T>[0] * z[0];
z[0] = abb[18] + z[0] + z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_479_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-18.030354108360705043457066510385725404331099222662823179650728832"),stof<T>("22.220631606669139070965816410528306681055966430303651353712824902")}, std::complex<T>{stof<T>("0.121058435063374396626252553158405508338306300325456954603282636"),stof<T>("24.076671325498680271351592723443014794561188193929953036645662074")}, std::complex<T>{stof<T>("1.5572040147186920840563112853543747394409574486847462220499676863"),stof<T>("-1.763059622068526765810457885620425382230745793655452490972498375")}, std::complex<T>{stof<T>("-3.3503043306934848395162801267626925649850848258030084531446969389"),stof<T>("-3.4155529079438377263706102416100959990427345728853158313456724848")}, std::complex<T>{stof<T>("13.00178732788515372325822254511025259156675064784961154985278157"),stof<T>("-47.949796218043130302877561489960992092429143403463467730731661085")}, std::complex<T>{stof<T>("0.121058435063374396626252553158405508338306300325456954603282636"),stof<T>("24.076671325498680271351592723443014794561188193929953036645662074")}, std::complex<T>{stof<T>("3.3503043306934848395162801267626925649850848258030084531446969389"),stof<T>("3.4155529079438377263706102416100959990427345728853158313456724848")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[28].real()/kbase.W[28].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_479_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_479_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(32)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-4 * (4 + 9 * v[0] + -3 * v[1] + 3 * v[2] + v[3] + -2 * v[4] + -3 * v[5]) + m1_set::bc<T>[2] * (21 * v[0] + -v[1] + -3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((-2 + 3 * m1_set::bc<T>[2]) * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[21] + abb[22] + -abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[21] + abb[22] + -abb[25];
z[1] = abb[13] + -abb[14] + -abb[15];
return abb[6] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_479_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_479_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("21.903506340654724254254961259090857822754430435728516704318767729"),stof<T>("8.762074063584276185453061583697898170822161193376252972046499805")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[1], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), f_1_1(k), f_1_3(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_9_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[28].real()/k.W[28].real()), f_2_2_re(k), f_2_9_re(k), T{0}};
abb[18] = SpDLog_f_4_479_W_17_Im(t, path, abb);
abb[28] = SpDLog_f_4_479_W_17_Re(t, path, abb);

                    
            return f_4_479_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_479_DLogXconstant_part(base_point<T>, kend);
	value += f_4_479_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_479_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_479_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_479_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_479_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_479_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_479_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
