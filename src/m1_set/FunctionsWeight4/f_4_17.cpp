/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_17.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_17_abbreviated (const std::array<T,28>& abb) {
T z[35];
z[0] = abb[19] * (T(1) / T(2));
z[1] = 2 * abb[18];
z[2] = 3 * abb[20];
z[3] = abb[22] + -z[2];
z[3] = -z[0] + -z[1] + (T(1) / T(2)) * z[3];
z[3] = abb[2] * z[3];
z[4] = -abb[20] + abb[22];
z[0] = -abb[18] + z[0] + (T(1) / T(2)) * z[4];
z[5] = abb[0] * z[0];
z[6] = 2 * abb[3];
z[7] = abb[18] + -abb[19];
z[7] = z[6] * z[7];
z[8] = abb[23] + abb[24] + abb[25];
z[9] = abb[8] * z[8];
z[10] = -2 * abb[20] + abb[21] + abb[22];
z[11] = -3 * abb[18] + z[10];
z[12] = abb[1] * z[11];
z[13] = abb[2] * abb[21];
z[3] = z[3] + z[5] + z[7] + (T(-1) / T(2)) * z[9] + -z[12] + z[13];
z[3] = abb[14] * z[3];
z[7] = abb[5] * z[11];
z[14] = abb[6] * z[11];
z[15] = z[7] + z[14];
z[11] = abb[3] * z[11];
z[16] = 2 * z[11] + 3 * z[12] + -z[15];
z[17] = abb[12] * z[16];
z[3] = z[3] + -z[17];
z[3] = abb[14] * z[3];
z[18] = abb[18] + abb[19] + abb[20];
z[19] = -abb[21] + z[18];
z[19] = abb[3] * z[19];
z[20] = abb[7] * z[8];
z[19] = z[9] + z[19] + z[20];
z[21] = -abb[20] + abb[22] * (T(1) / T(2));
z[22] = abb[18] * (T(3) / T(2)) + -z[21];
z[23] = abb[21] * (T(1) / T(2));
z[24] = z[22] + -z[23];
z[25] = abb[6] * z[24];
z[0] = -abb[2] * z[0];
z[26] = -abb[18] + z[4];
z[27] = abb[4] * z[26];
z[28] = abb[18] + abb[20] * (T(1) / T(2)) + -z[23];
z[28] = abb[1] * z[28];
z[0] = z[0] + (T(1) / T(2)) * z[19] + -z[25] + (T(-3) / T(2)) * z[27] + z[28];
z[0] = abb[11] * z[0];
z[28] = 5 * abb[20] + -3 * abb[22];
z[29] = abb[19] * (T(1) / T(4));
z[28] = -z[1] + z[23] + (T(-1) / T(4)) * z[28] + z[29];
z[28] = abb[3] * z[28];
z[18] = abb[2] * z[18];
z[30] = -z[13] + z[18];
z[5] = z[5] + (T(1) / T(2)) * z[20] + z[30];
z[5] = (T(1) / T(2)) * z[5];
z[28] = z[5] + z[12] + z[25] + z[28];
z[31] = -abb[13] + abb[14];
z[31] = z[28] * z[31];
z[32] = 4 * abb[18];
z[2] = z[2] + z[32];
z[33] = 2 * abb[22];
z[34] = -abb[21] + z[2] + -z[33];
z[34] = abb[1] * z[34];
z[11] = -z[11] + z[14] + -z[27] + z[34];
z[34] = abb[12] * z[11];
z[0] = z[0] + z[31] + -z[34];
z[0] = abb[11] * z[0];
z[31] = -11 * abb[20] + 5 * abb[22];
z[29] = abb[21] * (T(3) / T(2)) + -z[29] + (T(1) / T(4)) * z[31] + -z[32];
z[29] = abb[3] * z[29];
z[25] = -z[7] + z[25];
z[5] = -z[5] + 2 * z[12] + z[25] + z[29];
z[5] = abb[14] * z[5];
z[28] = -abb[13] * z[28];
z[5] = z[5] + z[17] + z[28];
z[5] = abb[13] * z[5];
z[17] = -7 * abb[5] + abb[6];
z[21] = abb[21] * (T(-1) / T(6)) + abb[18] * (T(1) / T(2)) + (T(-1) / T(3)) * z[21];
z[17] = z[17] * z[21];
z[21] = abb[1] * z[26];
z[26] = abb[18] * (T(5) / T(6)) + abb[19] * (T(13) / T(6)) + -z[10];
z[26] = abb[3] * z[26];
z[17] = z[17] + (T(-5) / T(6)) * z[21] + z[26] + (T(4) / T(3)) * z[27] + (T(13) / T(6)) * z[30];
z[17] = prod_pow(m1_set::bc<T>[0], 2) * z[17];
z[21] = -abb[26] * z[11];
z[24] = abb[3] * z[24];
z[26] = 7 * abb[18] + abb[21] * (T(-5) / T(2)) + abb[20] * (T(9) / T(2)) + -z[33];
z[26] = abb[1] * z[26];
z[24] = 3 * z[24] + -z[25] + z[26] + (T(1) / T(2)) * z[27];
z[24] = prod_pow(abb[12], 2) * z[24];
z[25] = abb[0] + abb[3];
z[25] = abb[10] + abb[2] * (T(-1) / T(2)) + (T(-3) / T(4)) * z[25];
z[8] = z[8] * z[25];
z[25] = abb[8] + abb[9];
z[22] = -z[22] * z[25];
z[25] = abb[7] + z[25];
z[23] = z[23] * z[25];
z[25] = -3 * abb[19] + -abb[20] + -abb[22];
z[25] = abb[7] * z[25];
z[8] = z[8] + z[22] + z[23] + (T(1) / T(4)) * z[25];
z[8] = abb[15] * z[8];
z[22] = abb[27] * z[16];
z[0] = z[0] + z[3] + z[5] + z[8] + z[17] + z[21] + z[22] + z[24];
z[3] = -abb[19] + z[1] + -z[4];
z[4] = abb[0] * z[3];
z[4] = z[4] + -z[12];
z[5] = -abb[3] * z[3];
z[8] = 2 * z[13];
z[5] = -z[4] + z[5] + z[7] + -z[8] + -z[14] + 2 * z[18] + z[20];
z[5] = abb[13] * z[5];
z[2] = abb[19] + -abb[22] + z[2];
z[2] = abb[2] * z[2];
z[7] = abb[18] + 2 * abb[19] + -z[10];
z[6] = z[6] * z[7];
z[2] = z[2] + z[4] + z[6] + -z[8] + z[9] + z[15];
z[2] = abb[14] * z[2];
z[3] = -abb[2] * z[3];
z[1] = -abb[20] + abb[21] + -z[1];
z[1] = abb[1] * z[1];
z[1] = z[1] + z[3] + -z[14] + -z[19] + 3 * z[27];
z[1] = abb[11] * z[1];
z[1] = z[1] + z[2] + z[5] + z[34];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[11];
z[3] = abb[17] * z[16];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_17_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("9.106003321762473658794660234463274284907291865535393050546718865"),stof<T>("-31.783190042406910703169750734971255981007200977261337679902550179")}, std::complex<T>{stof<T>("44.686185096598243211033028135603090005606276802531386155780851556"),stof<T>("-22.138655475694424244654642548608827584493555249503976297876662586")}, std::complex<T>{stof<T>("27.501769066320572612008485440547452076429342587422460015384637127"),stof<T>("-28.397558032418940993670742474486906021202270738850665041024863484")}, std::complex<T>{stof<T>("-26.290419352040144257819202929518912214084226080644319190942933294"),stof<T>("25.524287485682393954153650809093177544298485487914648936754349281")}, std::complex<T>{stof<T>("-1.2113497142804283541892825110285398623451165067781408244417038325"),stof<T>("2.8732705467365470395170916653937284769037852509360161042705142033")}, std::complex<T>{stof<T>("0.0777645199269873530365534850398990571898457862453410706965229878"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("0.0777645199269873530365534850398990571898457862453410706965229878"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("0.0777645199269873530365534850398990571898457862453410706965229878"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_17_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_17_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-43.217331886924722512974283501811867721515331255912683185274246782"),stof<T>("-38.529595657140733799085947506758195875129491340095888541719576694")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_17_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_17_DLogXconstant_part(base_point<T>, kend);
	value += f_4_17_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_17_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_17_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_17_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_17_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_17_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_17_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
