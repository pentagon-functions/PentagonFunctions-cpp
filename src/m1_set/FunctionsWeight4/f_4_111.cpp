/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_111.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_111_abbreviated (const std::array<T,27>& abb) {
T z[45];
z[0] = abb[24] + -abb[25] + abb[26];
z[1] = abb[0] + -abb[10];
z[1] = z[0] * z[1];
z[2] = -abb[23] + abb[19] * (T(-1) / T(4));
z[3] = -abb[7] + abb[8];
z[2] = z[2] * z[3];
z[4] = abb[21] * (T(3) / T(2));
z[5] = abb[19] + abb[23];
z[6] = -z[4] + z[5];
z[6] = abb[9] * z[6];
z[4] = abb[8] * z[4];
z[3] = -abb[9] + (T(1) / T(4)) * z[3];
z[3] = abb[20] * z[3];
z[7] = -5 * abb[7] + -abb[8];
z[7] = -abb[9] + (T(1) / T(2)) * z[7];
z[7] = abb[22] * z[7];
z[8] = abb[7] + abb[9];
z[8] = abb[18] * z[8];
z[1] = z[1] + z[2] + z[3] + z[4] + z[6] + (T(1) / T(2)) * z[7] + (T(3) / T(2)) * z[8];
z[1] = abb[17] * z[1];
z[2] = abb[11] * (T(1) / T(2));
z[3] = 3 * abb[11];
z[4] = 7 * abb[13] + -z[3];
z[4] = z[2] * z[4];
z[6] = 3 * abb[13];
z[7] = 3 * abb[14];
z[8] = -abb[11] + -z[6] + z[7];
z[9] = abb[14] * (T(1) / T(2));
z[8] = z[8] * z[9];
z[10] = abb[12] * (T(1) / T(2));
z[11] = abb[11] + -abb[13];
z[12] = 2 * z[11];
z[13] = -abb[14] + -z[10] + z[12];
z[13] = abb[12] * z[13];
z[14] = 3 * abb[16];
z[15] = 2 * abb[15];
z[4] = z[4] + z[8] + z[13] + z[14] + -z[15];
z[4] = abb[21] * z[4];
z[8] = 7 * abb[11] + -z[6];
z[8] = abb[14] + (T(1) / T(2)) * z[8];
z[8] = z[8] * z[9];
z[13] = abb[12] * (T(5) / T(2)) + -z[7] + -z[12];
z[13] = abb[12] * z[13];
z[16] = 5 * abb[13];
z[17] = -abb[11] + z[16];
z[17] = (T(1) / T(4)) * z[17];
z[18] = abb[11] * z[17];
z[8] = abb[16] + z[8] + z[13] + z[15] + -z[18];
z[8] = abb[22] * z[8];
z[13] = abb[14] * (T(3) / T(2));
z[19] = z[12] + z[13];
z[19] = abb[14] * z[19];
z[20] = 5 * abb[14] + abb[12] * (T(-7) / T(2)) + z[12];
z[20] = abb[12] * z[20];
z[19] = z[14] + z[15] + z[19] + -z[20];
z[20] = -abb[18] * z[19];
z[21] = 2 * abb[14];
z[22] = -abb[12] + z[21];
z[23] = abb[12] * z[22];
z[24] = 2 * abb[16];
z[23] = z[23] + -z[24];
z[17] = -abb[14] + z[17];
z[17] = abb[14] * z[17];
z[17] = z[17] + -z[18] + z[23];
z[18] = -abb[19] + abb[20];
z[17] = -z[17] * z[18];
z[25] = abb[14] + z[11];
z[26] = abb[14] * z[25];
z[27] = abb[11] * z[11];
z[23] = z[23] + -z[26] + z[27];
z[23] = abb[23] * z[23];
z[28] = abb[17] * z[0];
z[4] = z[4] + z[8] + z[17] + z[20] + z[23] + (T(3) / T(4)) * z[28];
z[4] = abb[2] * z[4];
z[8] = z[10] + -z[25];
z[8] = abb[12] * z[8];
z[10] = z[2] * z[11];
z[17] = z[9] * z[25];
z[20] = abb[15] + abb[16];
z[8] = z[8] + z[10] + z[17] + z[20];
z[10] = abb[6] * z[8];
z[17] = -abb[11] + 2 * abb[13];
z[17] = abb[11] * z[17];
z[29] = prod_pow(abb[13], 2);
z[17] = z[17] + -z[29];
z[30] = 3 * z[11] + z[21];
z[30] = abb[14] * z[30];
z[31] = 5 * abb[15];
z[32] = 5 * z[11];
z[33] = 4 * abb[12] + -6 * abb[14] + -z[32];
z[33] = abb[12] * z[33];
z[30] = 4 * abb[16] + -z[17] + z[30] + z[31] + z[33];
z[30] = abb[1] * z[30];
z[33] = -abb[11] * abb[13];
z[34] = abb[11] + abb[13];
z[35] = abb[14] * z[34];
z[33] = -z[29] + z[33] + z[35];
z[33] = abb[0] * z[33];
z[35] = 2 * abb[12];
z[36] = -z[21] + z[35];
z[37] = -z[11] + z[36];
z[38] = -abb[12] * z[37];
z[39] = abb[11] * z[21];
z[40] = abb[11] * z[34];
z[38] = -abb[15] + z[38] + -z[39] + z[40];
z[38] = abb[4] * z[38];
z[40] = abb[12] * (T(3) / T(2));
z[41] = -abb[14] + z[40];
z[41] = abb[12] * z[41];
z[42] = prod_pow(abb[14], 2);
z[41] = -abb[16] + z[41] + (T(-1) / T(2)) * z[42];
z[41] = abb[5] * z[41];
z[30] = -z[10] + z[30] + (T(1) / T(2)) * z[33] + z[38] + -z[41];
z[30] = abb[22] * z[30];
z[33] = abb[14] * (T(-7) / T(2)) + -z[32];
z[33] = abb[14] * z[33];
z[38] = 11 * abb[14] + abb[12] * (T(-15) / T(2)) + 7 * z[11];
z[38] = abb[12] * z[38];
z[20] = z[17] + -7 * z[20] + z[33] + z[38];
z[20] = abb[1] * z[20];
z[21] = 3 * abb[12] + -z[21];
z[21] = abb[12] * z[21];
z[21] = z[21] + -z[24] + -z[42];
z[21] = abb[5] * z[21];
z[33] = z[11] + z[36];
z[33] = abb[12] * z[33];
z[38] = -abb[13] + z[3];
z[42] = -abb[11] * z[38];
z[33] = -abb[15] + z[33] + z[39] + z[42];
z[33] = abb[4] * z[33];
z[42] = -abb[11] + abb[14];
z[43] = abb[13] + z[3];
z[43] = -z[42] * z[43];
z[43] = (T(1) / T(2)) * z[43];
z[44] = abb[0] * z[43];
z[10] = 3 * z[10] + z[20] + z[21] + z[33] + z[44];
z[10] = abb[18] * z[10];
z[20] = -abb[12] + 2 * z[25];
z[20] = abb[12] * z[20];
z[15] = -z[15] + z[20] + -z[24];
z[6] = -abb[11] + z[6];
z[6] = abb[11] * z[6];
z[20] = -abb[14] + (T(-5) / T(4)) * z[11];
z[20] = abb[14] * z[20];
z[6] = (T(1) / T(4)) * z[6] + z[15] + z[20] + (T(-1) / T(2)) * z[29];
z[6] = abb[22] * z[6];
z[20] = -z[2] * z[34];
z[21] = abb[14] + (T(3) / T(2)) * z[11];
z[21] = abb[14] * z[21];
z[20] = -z[15] + z[20] + z[21] + z[29];
z[20] = abb[18] * z[20];
z[2] = z[2] * z[38];
z[9] = z[9] * z[11];
z[2] = z[2] + -z[9] + -z[29];
z[9] = (T(1) / T(2)) * z[18];
z[9] = z[2] * z[9];
z[21] = abb[12] * z[11];
z[21] = -abb[15] + z[21] + -z[27];
z[33] = 2 * abb[21];
z[34] = -z[21] * z[33];
z[6] = z[6] + z[9] + z[20] + -z[23] + (T(1) / T(4)) * z[28] + z[34];
z[6] = abb[3] * z[6];
z[9] = -abb[19] * z[19];
z[7] = -z[7] + z[11] + z[40];
z[7] = abb[12] * z[7];
z[13] = z[11] + z[13];
z[13] = abb[14] * z[13];
z[7] = -abb[15] + z[7] + z[13] + z[14] + z[17];
z[7] = abb[21] * z[7];
z[13] = z[11] + z[22];
z[13] = abb[12] * z[13];
z[13] = -abb[15] + z[13] + -z[24] + -z[26];
z[13] = abb[23] * z[13];
z[7] = z[7] + z[9] + 2 * z[13];
z[7] = abb[1] * z[7];
z[9] = abb[1] * z[19];
z[13] = prod_pow(abb[11], 2);
z[13] = 3 * z[13] + -z[29];
z[14] = abb[11] * abb[14];
z[13] = (T(1) / T(2)) * z[13] + -z[14];
z[14] = -abb[0] * z[13];
z[9] = z[9] + z[14] + -z[41];
z[9] = abb[20] * z[9];
z[8] = 3 * z[8];
z[8] = -abb[21] * z[8];
z[14] = z[15] + -z[26] + -z[27];
z[15] = -abb[23] + z[18];
z[14] = z[14] * z[15];
z[8] = z[8] + z[14];
z[8] = abb[6] * z[8];
z[3] = z[3] + -z[16];
z[3] = abb[11] * z[3];
z[14] = -z[32] + z[36];
z[14] = abb[12] * z[14];
z[3] = z[3] + z[14] + z[31] + z[39];
z[3] = abb[21] * z[3];
z[5] = -abb[20] + z[5];
z[5] = z[5] * z[21];
z[3] = z[3] + 2 * z[5];
z[3] = abb[4] * z[3];
z[5] = abb[7] * z[2];
z[14] = abb[8] * z[43];
z[5] = z[5] + z[14];
z[5] = -z[0] * z[5];
z[2] = -abb[21] * z[2];
z[13] = abb[19] * z[13];
z[2] = z[2] + z[13];
z[2] = abb[0] * z[2];
z[13] = abb[19] * z[41];
z[1] = z[1] + z[2] + z[3] + z[4] + (T(1) / T(2)) * z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[13] + z[30];
z[2] = 2 * abb[1];
z[3] = -abb[12] + z[25];
z[3] = z[2] * z[3];
z[4] = -abb[11] + abb[12];
z[5] = abb[4] * z[4];
z[6] = abb[12] + -abb[14];
z[7] = 2 * z[6];
z[8] = abb[5] * z[7];
z[9] = -abb[13] + abb[14];
z[9] = -abb[0] * z[9];
z[3] = z[3] + 2 * z[5] + z[8] + z[9];
z[3] = abb[22] * z[3];
z[9] = abb[1] * z[37];
z[10] = abb[0] * z[42];
z[9] = -z[5] + -z[8] + z[9] + z[10];
z[9] = abb[18] * z[9];
z[10] = z[18] * z[42];
z[4] = z[4] * z[33];
z[13] = abb[11] + abb[14] + -z[35];
z[13] = abb[22] * z[13];
z[14] = abb[18] * z[7];
z[4] = z[4] + z[10] + z[13] + z[14];
z[4] = abb[2] * z[4];
z[10] = -2 * abb[11] + abb[13] + abb[14];
z[13] = -abb[0] * z[10];
z[7] = -abb[1] * z[7];
z[7] = z[7] + z[8] + z[13];
z[7] = abb[20] * z[7];
z[13] = abb[8] * z[42];
z[14] = abb[7] * z[11];
z[13] = z[13] + -z[14];
z[0] = -z[0] * z[13];
z[10] = abb[19] * z[10];
z[13] = abb[21] * z[12];
z[10] = z[10] + z[13];
z[10] = abb[0] * z[10];
z[6] = abb[19] * z[6];
z[13] = -abb[21] * z[11];
z[6] = z[6] + z[13];
z[2] = z[2] * z[6];
z[5] = -z[5] * z[33];
z[6] = -abb[22] + -z[18];
z[6] = z[6] * z[11];
z[11] = abb[18] * z[12];
z[6] = z[6] + z[11];
z[6] = abb[3] * z[6];
z[8] = -abb[19] * z[8];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + 2 * z[9] + z[10];
z[0] = m1_set::bc<T>[0] * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_111_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-6.7223567937356590688028336358209623535869424724546325489599349292"),stof<T>("0.3962056075424066238167670391057950336722246181092373927389996997")}, std::complex<T>{stof<T>("-4.8576599141301956460451188937241389358947730105943795269563224958"),stof<T>("2.3194517966045198491615722825336649785188217481766179665038821539")}, std::complex<T>{stof<T>("4.8576599141301956460451188937241389358947730105943795269563224958"),stof<T>("-2.3194517966045198491615722825336649785188217481766179665038821539")}, std::complex<T>{stof<T>("11.1397269709345447933371307061365241324040687912633058437009064269"),stof<T>("-4.242697985666633074506377525961534923365418878243998540268764608")}, std::complex<T>{stof<T>("1.8646968796054634227577147420968234176921694618602530220036124334"),stof<T>("1.9232461890621132253448052434278699448465971300673805737648824541")}, stof<T>("-8.1467639364098125700497265545092086142014652425291793387481963645"), std::complex<T>{stof<T>("1.926620277401649447506621169961657790684401365981727565418752609"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-1.926620277401649447506621169961657790684401365981727565418752609"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("1.926620277401649447506621169961657790684401365981727565418752609"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[33].real()/kbase.W[33].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_111_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_111_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("6.223612273597705695483416393481139516470846787742375208838910236"),stof<T>("25.593813733155332652221084669287940427721962059976028228585510673")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[33].real()/k.W[33].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_111_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_111_DLogXconstant_part(base_point<T>, kend);
	value += f_4_111_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_111_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_111_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_111_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_111_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_111_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_111_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
