/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_456.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_456_abbreviated (const std::array<T,67>& abb) {
T z[94];
z[0] = abb[56] + -abb[59];
z[1] = abb[53] + abb[55];
z[2] = z[0] + 3 * z[1];
z[2] = -abb[57] + (T(1) / T(2)) * z[2];
z[3] = abb[5] * (T(1) / T(2));
z[2] = z[2] * z[3];
z[4] = abb[57] * (T(1) / T(2));
z[5] = abb[56] + z[1];
z[6] = abb[58] + z[4] + (T(1) / T(2)) * z[5];
z[6] = abb[1] * z[6];
z[7] = z[0] + -z[1];
z[8] = abb[16] * z[7];
z[9] = abb[57] + abb[59];
z[10] = abb[58] + z[9];
z[11] = abb[0] * z[10];
z[12] = (T(1) / T(2)) * z[8] + z[11];
z[13] = abb[49] + -abb[50] + abb[51];
z[14] = abb[48] + -z[13];
z[15] = (T(1) / T(2)) * z[14];
z[16] = abb[24] * z[15];
z[17] = z[12] + -z[16];
z[18] = abb[58] + z[1];
z[18] = abb[14] * z[18];
z[19] = (T(1) / T(4)) * z[14];
z[20] = abb[29] * z[19];
z[21] = z[18] + -z[20];
z[22] = abb[56] + abb[59];
z[23] = z[1] + z[22];
z[24] = abb[8] * z[23];
z[25] = abb[26] * z[19];
z[26] = -abb[57] + z[1];
z[27] = -abb[59] + z[26];
z[27] = abb[18] * z[27];
z[28] = (T(1) / T(2)) * z[27];
z[2] = -z[2] + -z[6] + (T(1) / T(2)) * z[17] + z[21] + (T(1) / T(4)) * z[24] + -z[25] + z[28];
z[6] = -z[1] + z[22];
z[17] = abb[57] + (T(1) / T(2)) * z[6];
z[22] = abb[4] * (T(1) / T(2));
z[25] = z[17] * z[22];
z[29] = z[0] + z[1];
z[30] = (T(1) / T(2)) * z[29];
z[31] = abb[58] + z[30];
z[32] = abb[7] * (T(1) / T(2));
z[33] = -z[31] * z[32];
z[25] = z[2] + z[25] + z[33];
z[25] = abb[43] * z[25];
z[33] = -abb[57] + z[30];
z[34] = abb[6] * z[33];
z[35] = (T(1) / T(2)) * z[34];
z[36] = -z[28] + z[35];
z[37] = abb[54] + z[29];
z[38] = -abb[57] + z[37];
z[38] = abb[10] * z[38];
z[39] = (T(1) / T(2)) * z[38];
z[40] = abb[27] * z[19];
z[41] = abb[25] * z[19];
z[40] = z[40] + z[41];
z[42] = z[36] + -z[39] + z[40];
z[43] = abb[54] + abb[57];
z[44] = abb[9] * z[43];
z[45] = abb[24] * z[14];
z[46] = z[44] + z[45];
z[47] = abb[56] + abb[58];
z[47] = abb[9] * z[47];
z[12] = z[12] + -z[47];
z[48] = z[12] + z[18] + -z[46];
z[49] = abb[17] * z[6];
z[49] = (T(1) / T(4)) * z[49];
z[50] = (T(1) / T(2)) * z[23];
z[51] = abb[8] * z[50];
z[52] = abb[26] * z[15];
z[51] = z[51] + -z[52];
z[53] = abb[58] * (T(-1) / T(2)) + z[4] + -z[5];
z[53] = abb[1] * z[53];
z[54] = abb[28] * z[19];
z[48] = -z[20] + -z[42] + (T(1) / T(2)) * z[48] + z[49] + z[51] + z[53] + z[54];
z[48] = m1_set::bc<T>[0] * z[48];
z[53] = abb[7] * z[33];
z[55] = m1_set::bc<T>[0] * z[53];
z[48] = z[48] + (T(-1) / T(2)) * z[55];
z[48] = abb[35] * z[48];
z[56] = abb[15] * z[30];
z[43] = abb[0] * z[43];
z[57] = abb[0] * abb[52];
z[43] = z[43] + z[56] + z[57];
z[56] = abb[28] * (T(1) / T(2));
z[56] = z[14] * z[56];
z[58] = -z[34] + z[43] + z[56];
z[59] = abb[57] + abb[58];
z[59] = abb[1] * z[59];
z[60] = z[18] + z[44] + z[47] + -z[58] + -z[59];
z[61] = abb[57] + z[37];
z[61] = abb[52] + (T(1) / T(2)) * z[61];
z[61] = abb[10] * z[61];
z[10] = z[10] * z[32];
z[19] = abb[23] * z[19];
z[10] = -z[10] + -z[19] + z[61];
z[49] = z[10] + -z[28] + -z[49] + (T(1) / T(2)) * z[60];
z[49] = m1_set::bc<T>[0] * z[49];
z[60] = abb[52] + abb[54];
z[61] = z[30] + z[60];
z[62] = z[22] * z[61];
z[63] = m1_set::bc<T>[0] * z[62];
z[49] = z[49] + -z[63];
z[49] = abb[31] * z[49];
z[25] = z[25] + z[48] + z[49];
z[48] = abb[15] * z[29];
z[49] = z[8] + z[48];
z[64] = z[19] + z[40];
z[65] = (T(1) / T(2)) * z[57];
z[66] = abb[57] + z[60];
z[67] = abb[56] + z[66];
z[67] = abb[2] * z[67];
z[68] = z[64] + z[65] + -z[67];
z[69] = abb[5] * z[33];
z[70] = z[59] + z[69];
z[71] = abb[58] + abb[59];
z[72] = abb[54] + z[71];
z[72] = abb[57] + (T(1) / T(2)) * z[72];
z[72] = abb[0] * z[72];
z[49] = -z[21] + (T(-1) / T(4)) * z[49] + -z[68] + z[70] + -z[72];
z[73] = m1_set::bc<T>[0] * z[49];
z[73] = z[63] + z[73];
z[74] = abb[58] + z[50];
z[75] = abb[7] * m1_set::bc<T>[0] * z[74];
z[73] = (T(1) / T(8)) * z[73] + (T(1) / T(16)) * z[75];
z[73] = abb[34] * z[73];
z[75] = -abb[57] + z[5];
z[76] = abb[1] * z[75];
z[77] = -z[51] + z[76];
z[78] = z[16] + z[77];
z[79] = -z[38] + z[78];
z[80] = abb[54] + z[30];
z[81] = abb[17] * z[80];
z[82] = -z[56] + z[81];
z[83] = abb[27] * z[15];
z[34] = z[34] + z[83];
z[75] = -abb[54] + -z[75];
z[75] = abb[13] * z[75];
z[84] = z[34] + z[75];
z[85] = abb[5] * z[30];
z[86] = abb[25] * z[15];
z[85] = -z[79] + -z[82] + -z[84] + z[85] + -z[86];
z[87] = abb[4] * z[23];
z[85] = (T(1) / T(8)) * z[85] + (T(-1) / T(16)) * z[87];
z[85] = abb[42] * z[85];
z[87] = abb[22] * z[7];
z[13] = abb[30] * z[13];
z[88] = abb[30] * abb[48];
z[13] = z[13] + z[87] + -z[88];
z[87] = abb[21] * z[30];
z[61] = abb[20] * z[61];
z[88] = abb[19] * z[74];
z[13] = (T(1) / T(2)) * z[13] + -z[61] + -z[87] + -z[88];
z[61] = abb[44] * z[13];
z[87] = -abb[56] + abb[57];
z[88] = abb[47] * z[87];
z[61] = z[61] + z[88];
z[21] = -z[21] + z[64];
z[64] = z[29] + z[60];
z[64] = abb[10] * z[64];
z[88] = z[21] + -z[64];
z[89] = -z[8] + z[48];
z[71] = -abb[54] + z[71];
z[71] = abb[0] * z[71];
z[57] = z[57] + -z[71] + (T(1) / T(2)) * z[89];
z[89] = z[45] + z[57];
z[32] = z[32] * z[74];
z[90] = abb[58] + z[5];
z[90] = abb[1] * z[90];
z[51] = z[32] + -z[51] + z[88] + (T(1) / T(2)) * z[89] + z[90];
z[51] = m1_set::bc<T>[0] * z[51];
z[51] = z[51] + z[63];
z[63] = abb[33] * (T(1) / T(8));
z[51] = z[51] * z[63];
z[89] = abb[17] * (T(1) / T(2));
z[80] = z[80] * z[89];
z[90] = abb[52] + abb[57];
z[90] = abb[10] * z[90];
z[58] = z[19] + (T(1) / T(2)) * z[58] + z[62] + -z[80] + -z[90];
z[58] = (T(1) / T(4)) * z[58];
z[90] = -abb[41] * z[58];
z[43] = z[43] + z[83];
z[83] = abb[23] * z[15];
z[64] = z[43] + -z[64] + -z[67] + z[83];
z[6] = z[6] * z[89];
z[67] = z[6] + z[56];
z[83] = z[27] + z[67];
z[89] = -z[69] + z[86];
z[91] = z[64] + z[83] + z[89];
z[91] = m1_set::bc<T>[0] * z[91];
z[91] = -z[55] + z[91];
z[92] = abb[36] * (T(1) / T(8));
z[91] = z[91] * z[92];
z[93] = z[78] + -z[83];
z[93] = m1_set::bc<T>[0] * z[93];
z[55] = z[55] + z[93];
z[93] = abb[32] * (T(1) / T(8));
z[55] = z[55] * z[93];
z[25] = abb[64] + abb[65] + (T(1) / T(4)) * z[25] + z[51] + z[55] + (T(1) / T(16)) * z[61] + z[73] + z[85] + z[90] + z[91];
z[8] = (T(1) / T(4)) * z[8] + -z[20] + -z[47];
z[20] = (T(1) / T(4)) * z[48];
z[47] = (T(1) / T(2)) * z[69];
z[22] = -z[22] * z[66];
z[10] = -z[8] + z[10] + -z[20] + z[22] + z[36] + (T(1) / T(2)) * z[44] + z[47] + -z[65] + -z[72];
z[10] = abb[31] * z[10];
z[22] = abb[52] + abb[54] + -z[30];
z[22] = abb[57] + (T(1) / T(2)) * z[22];
z[22] = abb[4] * z[22];
z[20] = z[8] + -z[20] + z[22] + -z[67] + -z[68] + (T(1) / T(2)) * z[71];
z[20] = abb[34] * z[20];
z[22] = 3 * abb[59] + -z[5];
z[22] = abb[58] + (T(1) / T(2)) * z[22];
z[22] = abb[57] + (T(1) / T(2)) * z[22];
z[22] = abb[7] * z[22];
z[22] = z[22] + (T(1) / T(2)) * z[57] + z[59] + z[62] + z[83] + z[88];
z[22] = abb[33] * z[22];
z[27] = -z[27] + z[53];
z[30] = z[27] + -z[89];
z[36] = -z[30] + z[64] + z[67];
z[48] = abb[36] * z[36];
z[34] = z[34] + z[44];
z[30] = z[30] + -z[34] + z[38];
z[51] = abb[4] * z[33];
z[55] = z[30] + z[51];
z[59] = abb[32] * z[55];
z[32] = abb[34] * z[32];
z[10] = z[10] + z[20] + z[22] + z[32] + z[48] + z[59];
z[10] = abb[31] * z[10];
z[20] = (T(1) / T(2)) * z[33];
z[20] = abb[4] * z[20];
z[20] = -z[20] + (T(1) / T(2)) * z[53];
z[22] = -z[20] + -z[42] + (T(-1) / T(2)) * z[46] + z[47] + z[67] + -z[77];
z[22] = abb[35] * z[22];
z[33] = z[51] + z[67];
z[15] = abb[29] * z[15];
z[12] = -z[12] + z[15] + -z[18] + z[33] + z[70];
z[15] = -abb[31] + abb[34];
z[12] = z[12] * z[15];
z[15] = -z[33] + z[34] + z[79] + z[89];
z[15] = abb[32] * z[15];
z[18] = z[27] + -z[67] + z[78];
z[18] = abb[33] * z[18];
z[12] = z[12] + z[15] + z[18] + z[22];
z[12] = abb[35] * z[12];
z[15] = abb[3] * z[50];
z[9] = abb[2] * z[9];
z[15] = -z[9] + z[15];
z[22] = z[3] * z[7];
z[27] = abb[7] * z[50];
z[22] = -z[15] + z[22] + z[27] + -z[34] + z[82] + -z[86];
z[22] = abb[38] * z[22];
z[27] = abb[61] * z[50];
z[42] = -3 * abb[56] + abb[59] + -z[1];
z[42] = (T(1) / T(2)) * z[42] + -z[60];
z[42] = abb[39] * z[42];
z[0] = z[0] + (T(1) / T(3)) * z[1];
z[0] = (T(1) / T(4)) * z[0] + (T(1) / T(3)) * z[60];
z[1] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = z[0] * z[1];
z[17] = -abb[62] * z[17];
z[0] = z[0] + z[17] + z[27] + z[42];
z[0] = abb[4] * z[0];
z[17] = abb[57] + -z[50];
z[17] = abb[39] * z[17];
z[27] = abb[57] * (T(-1) / T(6)) + abb[58] * (T(1) / T(3)) + (T(1) / T(4)) * z[23];
z[27] = z[1] * z[27];
z[31] = abb[62] * z[31];
z[42] = -abb[37] * z[74];
z[17] = z[17] + z[27] + z[31] + z[42];
z[17] = abb[7] * z[17];
z[27] = abb[40] * z[55];
z[0] = z[0] + z[10] + z[12] + z[17] + z[22] + z[27];
z[2] = abb[62] * z[2];
z[8] = -z[8] + (T(-1) / T(2)) * z[11] + z[35] + z[40] + z[44] + z[54] + -z[80];
z[8] = abb[37] * z[8];
z[10] = abb[3] * z[23];
z[4] = -abb[56] + abb[59] * (T(1) / T(2)) + -z[4] + -z[60];
z[4] = abb[2] * z[4];
z[7] = abb[57] + (T(-1) / T(2)) * z[7];
z[3] = z[3] * z[7];
z[3] = z[3] + z[4] + (T(-1) / T(4)) * z[10] + z[19] + z[41] + (T(1) / T(2)) * z[43];
z[3] = abb[39] * z[3];
z[2] = -z[2] + z[3] + z[8];
z[3] = -abb[28] * z[14];
z[3] = z[3] + -z[34];
z[3] = (T(1) / T(2)) * z[3] + -z[6] + -z[15] + z[20] + -z[28] + z[39] + -z[41] + z[47];
z[3] = abb[36] * z[3];
z[4] = -abb[33] * z[36];
z[6] = z[15] + -z[30] + z[67];
z[6] = abb[32] * z[6];
z[7] = z[15] + z[33];
z[7] = abb[34] * z[7];
z[3] = z[3] + z[4] + z[6] + z[7];
z[3] = z[3] * z[92];
z[4] = z[16] + -z[38] + z[52] + -z[56] + z[76] + z[81] + z[84];
z[6] = abb[5] * z[29];
z[8] = abb[25] * z[14];
z[6] = z[6] + -z[8] + z[24];
z[4] = (T(1) / T(8)) * z[4] + (T(-1) / T(16)) * z[6];
z[4] = abb[61] * z[4];
z[6] = abb[5] * z[26];
z[6] = z[6] + z[9] + z[75];
z[8] = (T(5) / T(4)) * z[45] + z[57];
z[9] = abb[26] * z[14];
z[9] = -z[9] + z[24];
z[11] = abb[57] + -5 * z[37];
z[11] = -abb[52] + (T(1) / T(4)) * z[11];
z[11] = abb[10] * z[11];
z[5] = abb[58] + abb[57] * (T(-1) / T(4)) + (T(5) / T(4)) * z[5];
z[5] = abb[1] * z[5];
z[5] = z[5] + (T(-1) / T(4)) * z[6] + (T(1) / T(2)) * z[8] + (T(-5) / T(8)) * z[9] + (T(1) / T(8)) * z[10] + z[11] + z[21];
z[1] = z[1] * z[5];
z[5] = abb[63] * z[13];
z[6] = abb[66] * z[87];
z[5] = z[5] + z[6];
z[6] = -z[49] + -z[62];
z[6] = abb[34] * z[6];
z[6] = z[6] + -z[32];
z[6] = z[6] * z[63];
z[7] = -z[7] + -z[18] + (T(1) / T(2)) * z[59];
z[7] = z[7] * z[93];
z[8] = abb[60] * z[58];
z[0] = abb[45] + abb[46] + (T(1) / T(8)) * z[0] + (T(1) / T(12)) * z[1] + (T(1) / T(4)) * z[2] + z[3] + z[4] + (T(-1) / T(16)) * z[5] + z[6] + z[7] + z[8];
return {z[25], z[0]};
}


template <typename T> std::complex<T> f_4_456_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("-0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("-0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("-0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("-0.0883298241095633926946418692511844408412068741973559873146708585"),stof<T>("-0.44009601786745975038172927061295343418807866948773457276764121692")}, std::complex<T>{stof<T>("0.931082643430074320744843014431319353053552940108758671152162025"),stof<T>("-1.4233631353722172535071947103457874981687286868204015666601374717")}, std::complex<T>{stof<T>("0.09331850006687757425890500293679509742768695788190490408862353426"),stof<T>("-0.36321909701520572355857830307592393059536359330973949138816351223")}, std::complex<T>{stof<T>("0.931082643430074320744843014431319353053552940108758671152162025"),stof<T>("-1.4233631353722172535071947103457874981687286868204015666601374717")}, std::complex<T>{stof<T>("0.2088691658799652849957942662824611102486313021219693292393330568"),stof<T>("-0.39072894070760425310122240209603079034609467557599678099495158271")}, std::complex<T>{stof<T>("-0.65582715374807264767216749534092624067051533254664778999060144411"),stof<T>("0.61681238682609980126430685452047608707409236983961095385469725478")}, std::complex<T>{stof<T>("-0.02238214484987167488443757550322956564711890091921199160791251161"),stof<T>("0.29177720042835693600204956019977523493069873478450108990258303931")}, std::complex<T>{stof<T>("-0.10106880746788828914364788261233622671867420444933037107719151791"),stof<T>("0.24788438045925399274751805319888940527700629259492957336373824112")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_456_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_456_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(64)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(-1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[52] + abb[54] + abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[52] + -abb[54] + -abb[56];
z[1] = abb[34] + -abb[36];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_456_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(64)) * (8 + 7 * v[0] + v[1] + v[2] + -3 * v[3] + -4 * v[4] + -m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -1 * v[3] + -2 * v[4] + -1 * v[5]) + -v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[52] + abb[54] + abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[34] + abb[36];
z[0] = (T(1) / T(2)) * z[0];
z[1] = -abb[31] + abb[33];
z[0] = z[0] * z[1];
z[0] = -abb[39] + z[0];
z[1] = -abb[52] + -abb[54] + -abb[56];
return abb[11] * (T(1) / T(4)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_456_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_456_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + -4 * v[0] + v[1] + -3 * v[2] + -v[3] + 3 * v[4] + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[54] + abb[56] + abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[1];
z[0] = -abb[54] + -abb[56] + -abb[57];
return abb[12] * abb[38] * (T(1) / T(8)) * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_456_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.17748203116400382437840062080602993547423771837015675471146244909"),stof<T>("0.32821891117033090600109706564717100205819523938030797479575248335")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,67> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W83(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[45] = SpDLog_f_4_456_W_17_Im(t, path, abb);
abb[46] = SpDLog_f_4_456_W_20_Im(t, path, abb);
abb[47] = SpDLogQ_W_80(k,dl,dlr).imag();
abb[64] = SpDLog_f_4_456_W_17_Re(t, path, abb);
abb[65] = SpDLog_f_4_456_W_20_Re(t, path, abb);
abb[66] = SpDLogQ_W_80(k,dl,dlr).real();

                    
            return f_4_456_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_456_DLogXconstant_part(base_point<T>, kend);
	value += f_4_456_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_456_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_456_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_456_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_456_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_456_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_456_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
