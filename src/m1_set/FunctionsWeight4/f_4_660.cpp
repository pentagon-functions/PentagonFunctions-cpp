/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_660.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_660_abbreviated (const std::array<T,70>& abb) {
T z[141];
z[0] = abb[57] * (T(2) / T(3));
z[1] = abb[48] * (T(2) / T(3));
z[2] = abb[58] * (T(1) / T(3));
z[3] = abb[47] * (T(3) / T(2));
z[4] = abb[55] * (T(1) / T(2));
z[5] = abb[49] * (T(1) / T(6)) + abb[50] * (T(2) / T(3)) + abb[51] * (T(4) / T(3)) + -z[0] + z[1] + -z[2] + z[3] + -z[4];
z[5] = abb[6] * z[5];
z[6] = abb[56] * (T(7) / T(4));
z[7] = 2 * abb[51];
z[8] = -5 * abb[55] + z[6] + z[7];
z[9] = abb[50] * (T(3) / T(4));
z[10] = abb[53] * (T(3) / T(2));
z[8] = abb[47] * (T(-3) / T(4)) + abb[48] * (T(13) / T(6)) + -z[0] + z[2] + (T(1) / T(3)) * z[8] + z[9] + z[10];
z[8] = abb[1] * z[8];
z[11] = 2 * abb[50];
z[12] = abb[49] + abb[55];
z[13] = abb[47] + z[12];
z[14] = z[11] + -z[13];
z[15] = 4 * abb[48];
z[16] = z[7] + z[14] + z[15];
z[16] = abb[17] * z[16];
z[17] = abb[61] + abb[62];
z[18] = abb[26] * z[17];
z[19] = abb[59] + -abb[60];
z[20] = abb[21] * z[19];
z[16] = z[16] + -z[18] + z[20];
z[18] = -abb[52] + abb[56];
z[20] = -abb[48] + z[18];
z[20] = abb[14] * z[20];
z[21] = abb[47] + -abb[56];
z[22] = abb[52] + z[21];
z[23] = abb[10] * z[22];
z[24] = z[20] + z[23];
z[25] = abb[20] * z[19];
z[26] = abb[47] + abb[49];
z[27] = -abb[55] + z[26];
z[28] = abb[15] * z[27];
z[29] = z[16] + z[24] + z[25] + -z[28];
z[30] = abb[47] * (T(1) / T(4));
z[31] = 2 * abb[55];
z[6] = -abb[49] + abb[57] + -z[6] + z[7] + -z[30] + z[31];
z[6] = abb[50] * (T(1) / T(4)) + z[1] + (T(1) / T(3)) * z[6] + -z[10];
z[6] = abb[0] * z[6];
z[10] = -abb[49] + -abb[51] + abb[58];
z[32] = abb[54] + -abb[56];
z[0] = abb[48] * (T(4) / T(3)) + -z[0] + (T(-2) / T(3)) * z[10] + z[30] + (T(1) / T(4)) * z[32];
z[0] = abb[4] * z[0];
z[10] = abb[51] + -abb[57] + abb[47] * (T(-5) / T(2)) + z[4];
z[30] = abb[49] * (T(1) / T(2));
z[1] = z[1] + z[2] + (T(1) / T(3)) * z[10] + -z[30];
z[1] = abb[5] * z[1];
z[2] = abb[24] * z[17];
z[10] = abb[19] * z[19];
z[32] = z[2] + z[10];
z[33] = abb[23] + abb[25];
z[34] = (T(1) / T(2)) * z[17];
z[35] = z[33] * z[34];
z[36] = abb[22] * z[34];
z[37] = z[35] + z[36];
z[38] = abb[49] + -abb[55];
z[39] = -abb[51] + abb[57];
z[40] = -z[38] + z[39];
z[40] = -abb[48] + (T(1) / T(2)) * z[40];
z[40] = abb[8] * z[40];
z[41] = -abb[47] + abb[50];
z[42] = abb[51] + abb[55] * (T(-1) / T(3));
z[42] = abb[58] + abb[49] * (T(-7) / T(6)) + abb[57] * (T(-1) / T(6)) + (T(1) / T(3)) * z[41] + (T(1) / T(2)) * z[42];
z[42] = abb[3] * z[42];
z[43] = abb[54] + z[21];
z[43] = abb[2] * z[43];
z[44] = (T(1) / T(2)) * z[19];
z[45] = abb[18] * z[44];
z[0] = z[0] + z[1] + z[5] + z[6] + z[8] + (T(-1) / T(3)) * z[29] + (T(-5) / T(6)) * z[32] + -z[37] + (T(5) / T(3)) * z[40] + z[42] + (T(-1) / T(4)) * z[43] + -z[45];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = 4 * abb[58];
z[5] = abb[50] * (T(5) / T(2)) + -z[1];
z[6] = 8 * abb[48];
z[8] = abb[56] * (T(1) / T(2));
z[29] = abb[47] * (T(1) / T(2));
z[32] = 6 * abb[51] + -abb[53] + -3 * abb[57] + z[5] + z[6] + -z[8] + z[29] + z[31];
z[32] = abb[1] * z[32];
z[40] = 4 * abb[51];
z[42] = 2 * abb[57];
z[46] = z[40] + -z[42];
z[47] = 6 * abb[48] + z[46];
z[48] = z[4] + z[30];
z[49] = z[29] + z[48];
z[50] = 3 * abb[58];
z[51] = z[11] + z[47] + z[49] + -z[50];
z[51] = abb[6] * z[51];
z[52] = 3 * abb[49];
z[53] = -abb[47] + z[52];
z[54] = 3 * abb[50];
z[55] = 3 * abb[51];
z[56] = -abb[55] + abb[57];
z[57] = abb[48] + -abb[52] + -z[53] + z[54] + z[55] + z[56];
z[57] = -abb[53] + abb[58] + (T(1) / T(2)) * z[57];
z[57] = abb[4] * z[57];
z[58] = -abb[51] + abb[52];
z[59] = abb[57] * (T(1) / T(2));
z[60] = (T(1) / T(2)) * z[58] + -z[59];
z[61] = abb[50] * (T(1) / T(2));
z[62] = -abb[53] + z[61];
z[63] = -abb[56] + z[48] + z[60] + -z[62];
z[63] = abb[7] * z[63];
z[64] = 2 * abb[58];
z[65] = -z[12] + z[64];
z[66] = 2 * abb[48];
z[67] = z[39] + -z[66];
z[68] = z[65] + z[67];
z[69] = abb[16] * z[68];
z[70] = abb[27] * z[17];
z[69] = z[69] + z[70];
z[71] = -abb[48] + z[39];
z[72] = 2 * z[71];
z[73] = abb[13] * z[72];
z[74] = z[69] + z[73];
z[75] = z[12] + -z[39];
z[75] = abb[48] + -abb[58] + (T(1) / T(2)) * z[75];
z[75] = abb[5] * z[75];
z[76] = z[20] + -z[23];
z[77] = z[38] + -z[67];
z[78] = abb[8] * z[77];
z[79] = abb[50] + -abb[56];
z[80] = -z[39] + z[79];
z[80] = abb[48] + abb[53] + (T(1) / T(2)) * z[80];
z[80] = abb[0] * z[80];
z[32] = z[32] + z[35] + z[45] + z[51] + z[57] + z[63] + z[74] + -z[75] + (T(1) / T(2)) * z[76] + z[78] + z[80];
z[32] = abb[35] * z[32];
z[51] = 5 * abb[56];
z[57] = 2 * abb[54];
z[80] = z[51] + z[54] + -z[57];
z[81] = -abb[47] + z[64];
z[82] = 2 * abb[52];
z[83] = 2 * abb[53];
z[84] = -abb[57] + z[83];
z[85] = z[12] + -z[55] + -z[80] + z[81] + z[82] + z[84];
z[85] = abb[16] * z[85];
z[86] = -abb[50] + z[83];
z[87] = -z[21] + z[86];
z[88] = z[31] + -z[42];
z[89] = -z[64] + z[87] + z[88];
z[89] = abb[1] * z[89];
z[24] = -z[24] + z[89];
z[89] = abb[5] * z[68];
z[90] = z[52] + -z[64];
z[91] = abb[55] + -z[57] + z[90];
z[92] = 3 * abb[48];
z[93] = -z[39] + z[92];
z[94] = -z[18] + z[91] + z[93];
z[94] = abb[4] * z[94];
z[95] = abb[50] + abb[51];
z[12] = -z[12] + z[95];
z[96] = z[12] + -z[84];
z[97] = 2 * abb[56];
z[98] = -abb[52] + z[97];
z[99] = z[96] + z[98];
z[99] = abb[7] * z[99];
z[100] = -z[70] + z[99];
z[85] = -z[24] + -z[78] + z[85] + -z[89] + z[94] + z[100];
z[85] = abb[34] * z[85];
z[94] = abb[34] * z[17];
z[101] = abb[35] * z[17];
z[102] = z[94] + -z[101];
z[103] = abb[24] * z[102];
z[32] = z[32] + z[85] + -z[103];
z[32] = abb[35] * z[32];
z[85] = 5 * abb[51];
z[6] = z[1] + -z[6] + z[42] + -z[49] + -z[54] + -z[85];
z[6] = abb[6] * z[6];
z[54] = 2 * abb[49];
z[104] = abb[55] + z[54];
z[105] = -z[50] + z[104];
z[93] = z[93] + z[105];
z[106] = 2 * abb[9];
z[107] = z[93] * z[106];
z[108] = z[49] + -z[66] + -z[95];
z[109] = abb[17] * z[108];
z[34] = abb[26] * z[34];
z[110] = abb[21] * z[44];
z[34] = z[34] + z[109] + -z[110];
z[109] = -abb[57] + z[11] + z[55];
z[110] = abb[47] + z[66];
z[111] = -z[54] + z[109] + z[110];
z[112] = -abb[4] * z[111];
z[113] = z[29] + z[67];
z[114] = abb[55] * (T(3) / T(2)) + z[30] + -z[64] + -z[113];
z[114] = abb[5] * z[114];
z[115] = abb[50] + z[7];
z[116] = -abb[57] + z[115];
z[117] = -abb[58] + z[116];
z[118] = z[92] + z[117];
z[119] = abb[1] * z[118];
z[120] = abb[18] * z[19];
z[121] = z[37] + (T(3) / T(2)) * z[120];
z[122] = abb[20] * z[44];
z[122] = (T(1) / T(2)) * z[28] + -z[122];
z[123] = z[64] + -z[66];
z[124] = -abb[55] + -z[95] + z[123];
z[124] = abb[0] * z[124];
z[125] = abb[19] * z[44];
z[6] = z[6] + -z[34] + -z[69] + z[107] + z[112] + z[114] + -4 * z[119] + -z[121] + -z[122] + z[124] + -z[125];
z[6] = abb[30] * z[6];
z[112] = -abb[55] + z[109];
z[114] = -z[66] + z[90] + -z[112];
z[124] = abb[4] * z[114];
z[126] = 4 * abb[6];
z[127] = z[118] * z[126];
z[65] = -abb[47] + z[65];
z[128] = abb[5] * z[65];
z[128] = -z[10] + z[128];
z[129] = -abb[47] + z[39];
z[129] = abb[0] * z[129];
z[119] = -z[16] + z[74] + 6 * z[119] + -z[124] + z[127] + z[128] + z[129];
z[107] = -z[107] + z[119];
z[107] = abb[35] * z[107];
z[111] = abb[0] * z[111];
z[124] = 7 * abb[49];
z[127] = 6 * abb[58];
z[129] = z[124] + -z[127];
z[130] = 2 * abb[47];
z[112] = -z[112] + z[129] + z[130];
z[131] = abb[3] * z[112];
z[132] = z[128] + -z[131];
z[133] = -abb[48] + -z[54] + z[81];
z[134] = 2 * abb[4];
z[133] = z[133] * z[134];
z[26] = -abb[58] + z[26];
z[135] = z[26] * z[126];
z[111] = -z[69] + -z[111] + -z[132] + z[133] + -z[135];
z[133] = abb[34] * z[111];
z[81] = -z[15] + z[81] + -z[109];
z[81] = abb[0] * z[81];
z[109] = abb[47] + z[67];
z[135] = abb[5] * z[109];
z[135] = z[10] + z[135];
z[136] = 2 * abb[1];
z[118] = z[118] * z[136];
z[137] = -abb[47] + abb[48];
z[126] = z[126] * z[137];
z[26] = -z[26] * z[134];
z[26] = z[26] + z[81] + z[118] + z[126] + z[135];
z[26] = abb[31] * z[26];
z[30] = -z[4] + z[30];
z[81] = z[3] + z[30] + z[67];
z[81] = abb[5] * z[81];
z[81] = (T(3) / T(2)) * z[10] + -z[34] + z[81];
z[126] = abb[50] + z[15] + -z[42];
z[134] = z[55] + z[126];
z[138] = -z[49] + z[134];
z[138] = abb[6] * z[138];
z[121] = z[73] + -z[81] + -z[121] + z[138];
z[123] = -abb[47] + z[56] + -z[115] + z[123];
z[123] = abb[0] * z[123];
z[138] = -z[118] + z[122];
z[139] = abb[4] * z[65];
z[123] = z[121] + z[123] + -z[138] + z[139];
z[139] = -abb[33] * z[123];
z[6] = z[6] + z[26] + z[107] + z[133] + z[139];
z[6] = abb[31] * z[6];
z[26] = z[79] + z[83];
z[79] = -abb[48] + z[26] + -z[105];
z[79] = abb[9] * z[79];
z[107] = -abb[47] + z[15] + z[117];
z[107] = abb[6] * z[107];
z[52] = abb[50] + abb[52] + -abb[55] + -z[39] + -z[52];
z[52] = abb[53] + abb[58] + (T(1) / T(2)) * z[52];
z[52] = abb[4] * z[52];
z[133] = -abb[55] + abb[56] + z[71];
z[133] = abb[0] * z[133];
z[139] = -abb[55] + abb[58];
z[140] = abb[5] * z[139];
z[34] = z[23] + z[34] + -z[36] + z[52] + -z[63] + z[79] + z[107] + z[133] + -z[138] + z[140];
z[34] = abb[30] * z[34];
z[36] = z[80] + -z[83];
z[52] = -abb[52] + z[7];
z[1] = -z[1] + z[36] + 2 * z[52] + z[110];
z[1] = abb[16] * z[1];
z[7] = z[7] + z[66];
z[52] = z[7] + -z[87];
z[79] = abb[0] * z[52];
z[65] = abb[6] * z[65];
z[33] = z[17] * z[33];
z[80] = z[33] + z[120];
z[65] = z[65] + -z[80];
z[87] = -z[22] + z[67] + -z[91];
z[87] = abb[4] * z[87];
z[87] = z[1] + z[65] + -z[79] + z[87] + -z[99] + z[128];
z[87] = abb[34] * z[87];
z[91] = -abb[35] * z[119];
z[93] = abb[35] * z[93];
z[98] = -abb[54] + z[98] + z[105];
z[98] = abb[34] * z[98];
z[93] = z[93] + -z[98];
z[93] = z[93] * z[106];
z[34] = z[34] + z[87] + z[91] + z[93];
z[34] = abb[30] * z[34];
z[36] = -abb[57] + z[13] + z[15] + z[36] + -z[82] + z[85] + -z[127];
z[36] = abb[16] * z[36];
z[46] = 10 * abb[48] + abb[55] + z[11] + z[46] + z[53] + -z[127];
z[46] = abb[6] * z[46];
z[53] = 5 * abb[48];
z[11] = 4 * abb[53] + -5 * abb[54] + z[11] + z[51] + -z[53] + -z[82];
z[11] = abb[9] * z[11];
z[51] = z[21] + -z[83];
z[82] = 16 * abb[48] + 5 * abb[50] + 10 * abb[51] + -4 * abb[57] + z[51] + z[57] + -z[127];
z[82] = abb[1] * z[82];
z[85] = abb[47] + abb[56];
z[87] = abb[48] + abb[54] + z[39] + -z[85];
z[87] = abb[0] * z[87];
z[83] = z[83] + z[115];
z[91] = z[66] + z[83];
z[93] = -z[42] + z[91];
z[107] = -z[57] + z[93];
z[115] = abb[52] + z[107];
z[115] = abb[4] * z[115];
z[11] = z[11] + -z[16] + -z[36] + z[46] + z[70] + z[80] + z[82] + z[87] + z[99] + z[115];
z[11] = abb[36] * z[11];
z[46] = z[57] + -z[97];
z[82] = -abb[48] + abb[51] * (T(-1) / T(2)) + z[29] + -z[46] + z[54] + z[59] + -z[64];
z[82] = abb[0] * z[82];
z[55] = -abb[55] + z[55];
z[41] = abb[49] * (T(7) / T(2)) + -z[41] + -z[50] + (T(-1) / T(2)) * z[55] + z[59];
z[41] = abb[6] * z[41];
z[35] = z[35] + z[41] + -z[63];
z[41] = abb[50] * (T(3) / T(2)) + z[56];
z[56] = abb[54] * (T(3) / T(2)) + z[41] + -z[58] + -z[124];
z[59] = abb[53] * (T(1) / T(2));
z[50] = z[50] + (T(1) / T(2)) * z[56] + -z[59] + -z[66];
z[50] = abb[4] * z[50];
z[56] = abb[53] + z[61];
z[46] = abb[52] * (T(-1) / T(2)) + -z[46] + z[56] + -z[71] + z[105];
z[61] = -abb[9] * z[46];
z[13] = -abb[58] + (T(1) / T(2)) * z[13];
z[13] = abb[5] * z[13];
z[13] = z[13] + -z[23] + -z[35] + 3 * z[43] + z[50] + z[61] + -z[69] + z[82] + z[125] + z[131];
z[13] = abb[32] * z[13];
z[43] = abb[49] + -abb[54];
z[50] = 4 * abb[56];
z[43] = abb[52] + -4 * z[43] + -z[50] + -z[88] + -z[91] + z[127];
z[43] = abb[9] * z[43];
z[33] = z[33] + -z[43] + z[99];
z[41] = -abb[53] + z[41];
z[15] = abb[51] + abb[54] * (T(1) / T(2)) + -z[15] + -z[22] + z[41] + -z[129];
z[15] = abb[4] * z[15];
z[43] = z[52] + -z[54] + z[57];
z[43] = abb[0] * z[43];
z[52] = abb[6] * z[112];
z[15] = -z[15] + z[33] + -z[36] + z[43] + z[52] + z[70] + -z[131];
z[36] = abb[34] * z[15];
z[43] = -abb[31] * z[111];
z[52] = z[65] + -z[69];
z[54] = abb[4] * z[68];
z[57] = -z[31] + z[109];
z[61] = z[57] + z[64];
z[61] = abb[0] * z[61];
z[25] = z[25] + -z[28];
z[63] = abb[22] * z[17];
z[65] = z[25] + -z[63];
z[54] = z[52] + z[54] + z[61] + z[65];
z[61] = -abb[30] + abb[33];
z[61] = z[54] * z[61];
z[13] = z[13] + z[36] + z[43] + z[61];
z[13] = abb[32] * z[13];
z[4] = abb[58] + abb[54] * (T(-13) / T(4)) + abb[48] * (T(-7) / T(2)) + abb[49] * (T(-3) / T(2)) + -z[4] + z[9] + -z[29] + z[50] + -z[59] + -z[60];
z[4] = abb[4] * z[4];
z[8] = -abb[48] + z[8];
z[9] = -abb[51] + z[8] + -z[29] + -z[62];
z[9] = abb[0] * z[9];
z[36] = -abb[47] + -3 * abb[56];
z[36] = -abb[57] + (T(1) / T(2)) * z[36] + -z[62] + z[64];
z[36] = abb[1] * z[36];
z[20] = -3 * z[20] + -z[23];
z[1] = z[1] + z[4] + z[9] + (T(1) / T(2)) * z[20] + -z[35] + z[36] + -z[75];
z[4] = prod_pow(abb[34], 2);
z[1] = z[1] * z[4];
z[9] = z[14] + z[47];
z[9] = abb[6] * z[9];
z[14] = z[55] + z[126];
z[14] = z[14] * z[136];
z[20] = abb[4] * z[77];
z[2] = z[2] + z[78];
z[35] = z[2] + z[63];
z[36] = abb[5] * z[27];
z[10] = z[10] + z[36];
z[43] = abb[0] * z[72];
z[9] = z[9] + -z[10] + z[14] + -z[16] + z[20] + -z[35] + z[43] + -z[80];
z[14] = abb[64] * z[9];
z[5] = -abb[52] + -3 * abb[53] + -abb[54] + abb[56] * (T(7) / T(2)) + z[3] + z[5] + z[40] + z[92];
z[5] = abb[16] * z[5];
z[8] = z[8] + z[29];
z[20] = abb[54] + z[8] + z[41] + -z[58] + -z[90];
z[20] = abb[4] * z[20];
z[29] = -abb[6] * z[114];
z[8] = -abb[54] + z[8] + z[56];
z[8] = abb[0] * z[8];
z[40] = -z[21] + z[107];
z[40] = abb[1] * z[40];
z[5] = z[5] + z[8] + z[20] + -z[23] + z[29] + -z[33] + z[40] + z[89];
z[5] = abb[39] * z[5];
z[8] = -abb[37] * z[15];
z[15] = z[74] + z[118];
z[20] = abb[4] * z[109];
z[20] = z[20] + -z[122];
z[29] = z[49] + -z[64] + z[134];
z[29] = abb[6] * z[29];
z[33] = z[95] + z[130];
z[40] = abb[55] + -z[33];
z[40] = abb[0] * z[40];
z[29] = z[15] + -z[20] + z[29] + z[37] + z[40] + -z[45] + -z[81];
z[29] = abb[30] * z[29];
z[37] = abb[0] * z[109];
z[40] = abb[6] * z[109];
z[41] = abb[1] * z[72];
z[40] = -z[37] + z[40] + z[41] + -z[73] + z[120];
z[40] = abb[33] * z[40];
z[41] = z[52] + -z[78];
z[43] = z[136] * z[139];
z[37] = z[37] + z[41] + z[43];
z[43] = -abb[34] + abb[35];
z[37] = z[37] * z[43];
z[43] = abb[22] * z[102];
z[29] = z[29] + z[37] + z[40] + z[43] + z[103];
z[29] = abb[33] * z[29];
z[18] = z[18] + z[38] + z[71];
z[18] = abb[4] * z[18];
z[37] = z[85] + -z[86] + -z[88];
z[37] = abb[1] * z[37];
z[38] = abb[5] * z[77];
z[40] = z[85] + z[96];
z[40] = abb[16] * z[40];
z[18] = -z[18] + z[37] + z[38] + -z[40] + -z[76] + z[100];
z[35] = z[18] + -z[35];
z[35] = abb[65] * z[35];
z[37] = -abb[55] + z[90];
z[38] = -z[37] + -z[39] + -z[130];
z[38] = abb[4] * z[38];
z[37] = -3 * abb[47] + -z[37];
z[37] = abb[6] * z[37];
z[12] = -abb[47] + -abb[58] + -z[12];
z[12] = abb[0] * z[12];
z[12] = 2 * z[12] + z[37] + z[38] + -z[65] + z[80] + -z[132];
z[12] = abb[38] * z[12];
z[3] = z[3] + -z[48] + z[95];
z[3] = abb[18] * z[3];
z[37] = abb[21] * z[108];
z[38] = abb[5] + abb[6] + -abb[17];
z[38] = z[38] * z[44];
z[39] = abb[4] + 2 * abb[29] + abb[15] * (T(-1) / T(2));
z[39] = z[19] * z[39];
z[43] = abb[28] * (T(1) / T(2));
z[43] = z[17] * z[43];
z[3] = z[3] + z[37] + z[38] + z[39] + z[43];
z[37] = abb[66] * z[3];
z[31] = -abb[56] + z[31] + -z[33] + z[84];
z[31] = abb[0] * z[31];
z[22] = abb[4] * z[22];
z[23] = 2 * z[23];
z[22] = -z[22] + -z[23] + z[31] + z[40] + -z[100];
z[28] = z[22] + z[28] + -z[36];
z[28] = abb[63] * z[28];
z[4] = -z[4] * z[46];
z[31] = abb[56] + z[42] + -z[53] + -z[83] + -z[105];
z[31] = abb[35] * z[31];
z[31] = z[31] + 2 * z[98];
z[31] = abb[35] * z[31];
z[4] = z[4] + z[31];
z[4] = abb[9] * z[4];
z[31] = -z[64] + -z[95] + z[104];
z[33] = -abb[69] * z[31];
z[30] = -z[30] + z[113];
z[36] = abb[66] * z[30];
z[19] = abb[63] * z[19];
z[36] = -z[19] + z[36];
z[36] = abb[19] * z[36];
z[27] = (T(1) / T(2)) * z[27];
z[38] = abb[66] * z[27];
z[19] = -z[19] + z[38];
z[19] = abb[20] * z[19];
z[38] = -abb[35] * z[102];
z[17] = abb[63] * z[17];
z[17] = z[17] + z[38];
z[17] = abb[22] * z[17];
z[0] = abb[67] + abb[68] + z[0] + z[1] + z[4] + z[5] + z[6] + z[8] + z[11] + z[12] + z[13] + z[14] + z[17] + z[19] + z[28] + z[29] + z[32] + z[33] + z[34] + z[35] + z[36] + z[37];
z[1] = z[26] + -z[67];
z[1] = z[1] * z[106];
z[1] = z[1] + z[99];
z[4] = abb[52] + z[86];
z[5] = z[4] + -z[42] + z[137];
z[5] = abb[4] * z[5];
z[6] = abb[50] + -4 * abb[55] + -z[51] + z[64] + z[66];
z[6] = abb[1] * z[6];
z[8] = -z[21] + -z[93];
z[8] = abb[0] * z[8];
z[5] = z[1] + z[5] + z[6] + z[8] + -z[16] + z[52] + -z[73] + -z[76] + -2 * z[78] + -z[135];
z[5] = abb[35] * z[5];
z[6] = z[110] + z[117];
z[6] = abb[6] * z[6];
z[8] = -z[57] + -z[97];
z[8] = abb[0] * z[8];
z[4] = -z[4] + z[7];
z[4] = abb[4] * z[4];
z[1] = -z[1] + z[4] + 2 * z[6] + z[8] + -z[10] + z[15] + -z[23] + -z[65];
z[1] = abb[30] * z[1];
z[4] = -abb[55] + z[116];
z[6] = z[4] + z[110];
z[6] = abb[0] * z[6];
z[4] = -z[4] + -z[92];
z[4] = z[4] * z[136];
z[4] = z[2] + z[4] + z[6] + z[20] + -z[121];
z[4] = abb[33] * z[4];
z[6] = -abb[4] * z[137];
z[6] = z[6] + z[24] + -z[41] + z[79] + z[135];
z[6] = abb[34] * z[6];
z[7] = -abb[31] * z[123];
z[8] = abb[32] * z[54];
z[11] = -z[94] + 2 * z[101];
z[12] = -abb[22] + -abb[24];
z[11] = z[11] * z[12];
z[1] = z[1] + z[4] + z[5] + z[6] + z[7] + z[8] + z[11];
z[1] = m1_set::bc<T>[0] * z[1];
z[4] = abb[41] * z[9];
z[2] = -z[2] + z[18];
z[2] = abb[42] * z[2];
z[5] = abb[19] * z[30];
z[6] = abb[20] * z[27];
z[3] = z[3] + z[5] + z[6];
z[3] = abb[43] * z[3];
z[5] = -z[10] + z[22] + -z[25];
z[5] = abb[40] * z[5];
z[6] = -abb[46] * z[31];
z[7] = abb[40] + -abb[42];
z[7] = z[7] * z[63];
z[1] = abb[44] + abb[45] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_660_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("4.0549405544182681858021109701137329747769965591317530237318657023"),stof<T>("5.2105367781938766172750286354578781008753444151395946877934623755")}, std::complex<T>{stof<T>("-15.927884452211109625321039725382567525874898971354366178509680538"),stof<T>("-2.770327363474794988029681283490635497436961693904857615219119947")}, std::complex<T>{stof<T>("1.197783340108982842113156859196858997201643252906002650661903574"),stof<T>("-10.531710897712234205811330326594131389396102116440914079046112629")}, std::complex<T>{stof<T>("-3.0333195632308512894927279627196221507316554311365096099336933851"),stof<T>("2.5248877916572274723048389645350136470954512437313557734568534154")}, std::complex<T>{stof<T>("-11.142246098401870014504996874073717719946404962646231662376745148"),stof<T>("8.983070217804028219509322590022969875115110214350503866977932479")}, std::complex<T>{stof<T>("1.1048384237308970223656392223276463223785147286541254668707201231"),stof<T>("1.4639623029700601432425161240033904706239233795937915111856691633")}, std::complex<T>{stof<T>("14.723031882494874666277338608196022999331801735450616364936254692"),stof<T>("12.219091889099924597567083692411917054300198342654057289205969846")}, std::complex<T>{stof<T>("-1.8488106530094033717902282135306562051351095078410308024113523606"),stof<T>("0.4401574990783764726823055843217097560116973162601166337086091277")}, std::complex<T>{stof<T>("4.1843893662613707477229743317377905246010223899295916125993394271"),stof<T>("1.0028794179071829898056514172515855326762007416075746582541814245")}, std::complex<T>{stof<T>("0.7474105939235813918735996072560840695488486637844138699749244169"),stof<T>("-12.5677283706967630459880254716939147551697581419461767381240639864")}, std::complex<T>{stof<T>("-1.6048809138609959509188142370120174883144154325121793177869173784"),stof<T>("5.0998238860993871277760182364824319829888294352480755024407994981")}, std::complex<T>{stof<T>("-1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(abs(k.W[77])) - rlog(abs(kbase.W[77])), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_660_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_660_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (-v[3] + v[5]) * (-8 + 12 * v[0] + -4 * v[1] + v[3] + -5 * v[5] + 2 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return (abb[47] + abb[49] + -abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[30] + -abb[33];
z[1] = abb[31] + z[0];
z[1] = abb[31] * z[1];
z[0] = abb[32] + z[0];
z[0] = abb[32] * z[0];
z[0] = -abb[38] + -z[0] + z[1];
z[1] = abb[47] + abb[49] + -abb[58];
return 2 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_660_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return (abb[47] + abb[49] + -abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[31] + abb[32];
z[1] = abb[47] + abb[49] + -abb[58];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_660_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[2] + v[3]) * (-24 + 7 * v[2] + 7 * v[3] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -12 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (v[2] + v[3]) * (2 * (8 * v[0] + 4 * v[1] + 7 * v[2] + 3 * v[3] + -4 * v[4]) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(32)) * (v[2] + v[3]) * (2 * (8 * v[0] + 4 * v[1] + 5 * v[2] + v[3] + -4 * v[4] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5])) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[3] = ((-1 + 2 * m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[2] + v[3])) / tend;
c[4] = (m1_set::bc<T>[2] * (T(-3) / T(2)) * (v[2] + v[3])) / tend;
c[5] = ((4 * m1_set::bc<T>[1] + -m1_set::bc<T>[2]) * (T(-3) / T(4)) * (v[2] + v[3])) / tend;


		return abb[52] * c[3] + abb[53] * (c[3] + -c[4]) + abb[49] * (2 * c[3] + -c[4]) + abb[48] * c[4] + abb[51] * (-2 * c[3] + c[4]) + abb[56] * c[5] + -abb[47] * (2 * c[3] + c[5]) + t * ((4 * abb[49] + -3 * abb[50] + -4 * abb[51] + 2 * abb[52] + 2 * abb[53] + abb[54] + -4 * abb[56]) * c[0] + (2 * abb[48] + -2 * abb[49] + abb[50] + 2 * abb[51] + -2 * abb[53] + abb[54]) * c[1] + 2 * (abb[47] + abb[54] + -abb[56]) * c[2]) * (T(1) / T(2)) + abb[50] * (-3 * c[3] + c[4]) * (T(1) / T(2)) + abb[54] * (-3 * c[3] + c[4] + -2 * c[5]) * (T(1) / T(2));
	}
	{
T z[6];
z[0] = abb[52] + abb[53];
z[1] = 2 * abb[47];
z[2] = abb[49] + -abb[51];
z[3] = abb[50] * (T(-3) / T(2)) + z[0] + -z[1] + 2 * z[2];
z[4] = abb[37] * z[3];
z[3] = abb[54] * (T(3) / T(2)) + -z[3];
z[3] = abb[32] * z[3];
z[0] = abb[50] * (T(-3) / T(4)) + (T(1) / T(2)) * z[0] + z[2];
z[5] = -abb[56] + abb[54] * (T(1) / T(4)) + z[0];
z[5] = abb[34] * z[5];
z[3] = z[3] + 3 * z[5];
z[3] = abb[34] * z[3];
z[5] = -abb[36] + abb[39];
z[2] = -abb[48] + abb[53] + z[2];
z[2] = -abb[47] + -abb[50] + abb[56] + 2 * z[2];
z[2] = z[2] * z[5];
z[0] = 3 * abb[56] + -z[0] + -z[1];
z[1] = prod_pow(abb[32], 2);
z[0] = z[0] * z[1];
z[1] = -abb[37] + (T(-3) / T(2)) * z[1];
z[1] = (T(3) / T(2)) * z[1] + -2 * z[5];
z[1] = abb[54] * z[1];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4];
return abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_660_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_660_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("1.641716277584588732796610950561090164051020069258474987822853549"),stof<T>("-15.888915923535740023822939603066782080337307132266650919022404062")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18, 77});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,70> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(abs(kend.W[77])) - rlog(abs(k.W[77])), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_4_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[44] = SpDLog_f_4_660_W_16_Im(t, path, abb);
abb[45] = SpDLog_f_4_660_W_19_Im(t, path, abb);
abb[46] = SpDLogQ_W_78(k,dl,dlr).imag();
abb[67] = SpDLog_f_4_660_W_16_Re(t, path, abb);
abb[68] = SpDLog_f_4_660_W_19_Re(t, path, abb);
abb[69] = SpDLogQ_W_78(k,dl,dlr).real();

                    
            return f_4_660_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_660_DLogXconstant_part(base_point<T>, kend);
	value += f_4_660_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_660_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_660_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_660_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_660_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_660_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_660_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
