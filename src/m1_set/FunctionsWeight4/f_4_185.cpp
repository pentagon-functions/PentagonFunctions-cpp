/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_185.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_185_abbreviated (const std::array<T,26>& abb) {
T z[42];
z[0] = abb[22] + abb[25];
z[1] = 2 * abb[20];
z[2] = z[0] + -z[1];
z[3] = abb[19] + 2 * abb[24];
z[4] = 2 * abb[21];
z[5] = z[3] + -z[4];
z[6] = z[2] + z[5];
z[7] = -abb[16] * z[6];
z[6] = abb[12] * z[6];
z[8] = -abb[21] + abb[23];
z[9] = -abb[10] * z[8];
z[6] = z[6] + z[9];
z[6] = abb[10] * z[6];
z[9] = abb[22] + -abb[25];
z[10] = 2 * abb[23];
z[11] = z[9] + z[10];
z[12] = z[5] + z[11];
z[13] = -abb[15] * z[12];
z[14] = prod_pow(abb[12], 2);
z[15] = -2 * abb[14] + -z[14];
z[16] = abb[20] + -abb[25];
z[17] = -abb[21] + -z[16];
z[15] = z[15] * z[17];
z[4] = -abb[23] + z[4];
z[18] = z[4] + z[16];
z[18] = abb[17] * z[18];
z[19] = abb[22] * (T(1) / T(2));
z[20] = abb[24] + abb[19] * (T(1) / T(2));
z[21] = z[19] + z[20];
z[22] = abb[25] * (T(1) / T(2));
z[23] = -abb[20] + z[22];
z[24] = z[21] + z[23];
z[4] = -z[4] + z[24];
z[4] = abb[18] * z[4];
z[24] = abb[23] + -z[24];
z[25] = prod_pow(abb[11], 2);
z[24] = z[24] * z[25];
z[21] = z[21] + -z[22];
z[26] = prod_pow(abb[13], 2);
z[27] = z[21] * z[26];
z[4] = z[4] + z[6] + z[7] + z[13] + z[15] + z[18] + z[24] + z[27];
z[4] = abb[4] * z[4];
z[6] = z[1] + z[10];
z[7] = -abb[21] + z[3];
z[13] = 2 * abb[25];
z[15] = z[6] + z[7] + -z[13];
z[18] = abb[17] * z[15];
z[24] = abb[23] * (T(3) / T(2));
z[27] = abb[21] * (T(1) / T(2));
z[28] = z[16] + z[24] + -z[27];
z[28] = abb[10] * z[28];
z[0] = z[0] + -z[6];
z[29] = abb[21] + z[0];
z[29] = abb[12] * z[29];
z[28] = z[28] + z[29];
z[28] = abb[10] * z[28];
z[29] = -abb[21] + z[10] + z[16];
z[29] = abb[14] * z[29];
z[30] = z[19] + -z[20];
z[23] = z[23] + z[30];
z[8] = -z[8] + z[23];
z[8] = abb[18] * z[8];
z[31] = abb[23] + -abb[25];
z[19] = abb[20] + -z[19] + (T(1) / T(2)) * z[31];
z[19] = z[19] * z[25];
z[27] = -z[20] + z[27];
z[32] = -z[14] * z[27];
z[33] = abb[21] + z[9];
z[34] = abb[16] * z[33];
z[8] = z[8] + z[18] + z[19] + z[28] + z[29] + z[32] + -z[34];
z[8] = abb[0] * z[8];
z[13] = 2 * abb[22] + -z[13];
z[10] = z[7] + -z[10] + -z[13];
z[10] = abb[3] * z[10];
z[18] = abb[23] + z[30];
z[19] = abb[20] + abb[21] + abb[25] * (T(-3) / T(2)) + z[18];
z[19] = abb[9] * z[19];
z[1] = -abb[22] + z[1];
z[28] = -abb[25] + z[1] + z[3];
z[28] = abb[2] * z[28];
z[29] = abb[6] * z[17];
z[30] = 2 * z[29];
z[32] = abb[8] * z[33];
z[35] = z[10] + 3 * z[19] + z[28] + z[30] + z[32];
z[35] = abb[18] * z[35];
z[6] = -abb[22] + 3 * abb[25] + z[5] + -z[6];
z[6] = abb[9] * z[6];
z[10] = -z[6] + z[10];
z[36] = -abb[21] + z[20];
z[2] = -z[2] + z[24] + z[36];
z[2] = abb[0] * z[2];
z[24] = abb[7] * z[15];
z[36] = abb[23] * (T(-5) / T(2)) + -z[13] + -z[36];
z[36] = abb[5] * z[36];
z[37] = abb[23] + z[9];
z[38] = abb[1] * z[37];
z[39] = 2 * z[38];
z[2] = z[2] + -z[10] + z[24] + z[36] + z[39];
z[2] = abb[15] * z[2];
z[24] = z[10] + z[28];
z[36] = -z[24] + -z[29];
z[36] = abb[14] * z[36];
z[24] = -abb[17] * z[24];
z[40] = -z[10] + -z[30];
z[40] = abb[16] * z[40];
z[10] = abb[12] * z[10];
z[27] = z[27] + z[37];
z[27] = abb[3] * z[27];
z[20] = abb[23] * (T(1) / T(2)) + -z[20] + z[33];
z[41] = -abb[5] * z[20];
z[41] = z[27] + z[29] + z[41];
z[41] = abb[10] * z[41];
z[10] = z[10] + z[41];
z[10] = abb[10] * z[10];
z[18] = z[18] + -z[22];
z[18] = abb[0] * z[18];
z[18] = z[18] + -z[19] + -z[28] + -z[29];
z[18] = z[18] * z[26];
z[22] = z[14] + -z[26];
z[17] = -abb[23] + z[3] + z[17];
z[22] = z[17] * z[22];
z[15] = -abb[18] * z[15];
z[7] = abb[23] + 2 * z[7] + z[16];
z[7] = abb[17] * z[7];
z[7] = z[7] + z[15] + z[22];
z[7] = abb[7] * z[7];
z[15] = abb[0] * z[37];
z[15] = -z[15] + z[38];
z[16] = -abb[2] * z[23];
z[21] = -abb[4] * z[21];
z[15] = (T(1) / T(2)) * z[15] + z[16] + z[21];
z[15] = prod_pow(m1_set::bc<T>[0], 2) * z[15];
z[16] = -abb[17] + abb[18];
z[12] = z[12] * z[16];
z[16] = z[20] * z[25];
z[12] = z[12] + z[16];
z[12] = abb[5] * z[12];
z[16] = -abb[14] * z[33];
z[16] = z[16] + -2 * z[34];
z[16] = abb[8] * z[16];
z[19] = -z[19] + z[32];
z[19] = z[19] * z[25];
z[20] = z[27] + -z[32];
z[14] = z[14] * z[20];
z[20] = abb[16] + -abb[18];
z[20] = 2 * z[20] + -z[25];
z[20] = abb[1] * z[20] * z[37];
z[2] = z[2] + z[4] + z[7] + z[8] + z[10] + z[12] + z[14] + (T(13) / T(6)) * z[15] + z[16] + z[18] + z[19] + z[20] + z[24] + z[35] + z[36] + z[40];
z[4] = z[6] + -z[30];
z[7] = z[3] + -z[11];
z[7] = abb[0] * z[7];
z[7] = -z[4] + z[7] + 2 * z[28];
z[7] = abb[13] * z[7];
z[6] = z[6] + 2 * z[32];
z[5] = abb[23] + -z[5] + z[13];
z[5] = abb[5] * z[5];
z[8] = -z[5] + -z[6] + z[39];
z[8] = abb[11] * z[8];
z[6] = abb[12] * z[6];
z[4] = z[4] + z[5];
z[4] = abb[10] * z[4];
z[1] = -z[1] + -z[31];
z[1] = abb[11] * z[1];
z[0] = z[0] + z[3];
z[5] = -abb[12] * z[0];
z[10] = -abb[10] * z[37];
z[1] = z[1] + z[5] + z[10];
z[1] = abb[0] * z[1];
z[3] = z[3] + z[9];
z[5] = -abb[12] + -abb[13];
z[3] = z[3] * z[5];
z[5] = -abb[10] + abb[11];
z[0] = z[0] * z[5];
z[0] = z[0] + z[3];
z[0] = abb[4] * z[0];
z[3] = -abb[12] + abb[13];
z[3] = abb[7] * z[3] * z[17];
z[0] = z[0] + z[1] + 2 * z[3] + z[4] + z[6] + z[7] + z[8];
z[0] = m1_set::bc<T>[0] * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_185_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.1593569497887474019236227664745536050933946786930038421589071332"),stof<T>("-8.732398216787095267288414339544914309185370144553466779083451114")}, std::complex<T>{stof<T>("30.881072697329204939134332225860000158723687388701896272151845205"),stof<T>("-21.299943681563369475671197538577460840907780099102469624081810496")}, std::complex<T>{stof<T>("-1.7747426801402933890521482081804117531231692603630775385755602766"),stof<T>("4.1250878519596201870296527229978940872860288119180415307706102621")}, std::complex<T>{stof<T>("-27.830626197982790328442925969079509796881321986096423340745505647"),stof<T>("21.953183378149330137424044300428716563149188661277619788268575774")}, std::complex<T>{stof<T>("11.9845461292754554016671772314354557200589293416615543121408069674"),stof<T>("5.2605500614134357420116083783982759441407498948105754124996061298")}, std::complex<T>{stof<T>("-14.318713899577494803847245532949107210186789357386007684317814266"),stof<T>("-17.464796433574190534576828679089828618370740289106933558166902228")}, std::complex<T>{stof<T>("-3.0504464993464146106914062567804903618423654026054729314063395575"),stof<T>("-0.6532396965859606617528467618512557222414085621751501641867652778")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_185_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_185_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-35.03967059373575266366502009290423018028187559447451950074103568"),stof<T>("9.954830542711139226199481966037403883183634726853821460115964006")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), f_1_1(k), f_1_3(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real())};

                    
            return f_4_185_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_185_DLogXconstant_part(base_point<T>, kend);
	value += f_4_185_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_185_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_185_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_185_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_185_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_185_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_185_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
