/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_512.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_512_abbreviated (const std::array<T,61>& abb) {
T z[90];
z[0] = -abb[31] + abb[32];
z[1] = abb[30] * (T(1) / T(2));
z[2] = z[0] + -z[1];
z[2] = abb[27] * z[2];
z[3] = abb[27] * (T(1) / T(2));
z[4] = z[1] + z[3];
z[5] = -abb[29] + abb[31];
z[6] = -abb[32] + z[5];
z[7] = z[4] + z[6];
z[7] = abb[28] * z[7];
z[8] = prod_pow(abb[31], 2);
z[9] = (T(1) / T(2)) * z[8];
z[10] = prod_pow(abb[29], 2);
z[11] = (T(1) / T(2)) * z[10];
z[12] = -abb[36] + -z[9] + z[11];
z[13] = abb[34] + -abb[57];
z[14] = prod_pow(m1_set::bc<T>[0], 2);
z[15] = (T(1) / T(2)) * z[14];
z[16] = -abb[33] + z[15];
z[17] = abb[32] * (T(1) / T(2));
z[18] = -abb[31] + z[17];
z[19] = abb[32] * z[18];
z[2] = z[2] + z[7] + z[12] + z[13] + -z[16] + -z[19];
z[7] = abb[22] * z[2];
z[20] = abb[24] * (T(1) / T(2));
z[21] = abb[20] + z[20];
z[22] = abb[30] * z[21];
z[23] = abb[24] * abb[31];
z[24] = abb[20] + abb[24];
z[25] = abb[32] * z[24];
z[26] = abb[20] * abb[29];
z[22] = z[22] + z[23] + -z[25] + z[26];
z[22] = abb[27] * z[22];
z[27] = abb[32] * z[21];
z[28] = abb[20] * abb[31];
z[23] = -z[23] + z[27] + -z[28];
z[23] = abb[32] * z[23];
z[27] = z[1] + z[5];
z[29] = abb[24] * z[27];
z[26] = z[26] + -z[28];
z[25] = -z[25] + -z[26] + z[29];
z[21] = abb[27] * z[21];
z[21] = z[21] + z[25];
z[21] = abb[28] * z[21];
z[29] = abb[56] + -abb[58];
z[30] = -z[10] + -z[29];
z[30] = abb[20] * z[30];
z[12] = abb[57] + -z[12];
z[12] = abb[24] * z[12];
z[31] = abb[20] * abb[32];
z[26] = z[26] + z[31];
z[26] = abb[30] * z[26];
z[20] = z[14] * z[20];
z[28] = abb[29] * z[28];
z[31] = abb[33] * z[24];
z[7] = -z[7] + z[12] + z[20] + -z[21] + z[22] + z[23] + -z[26] + z[28] + z[30] + -z[31];
z[12] = -z[1] + z[3];
z[20] = z[5] + z[12];
z[20] = abb[28] * z[20];
z[12] = -abb[29] + z[12];
z[12] = abb[27] * z[12];
z[21] = abb[29] * abb[30];
z[22] = z[12] + z[21];
z[23] = abb[29] * abb[31];
z[26] = -abb[58] + z[10] + -z[23];
z[16] = -z[16] + z[20] + z[22] + z[26];
z[20] = 3 * abb[21];
z[16] = z[16] * z[20];
z[20] = -abb[32] + z[1];
z[28] = z[3] + z[20];
z[30] = -abb[27] + abb[28];
z[28] = -z[28] * z[30];
z[28] = -abb[56] + z[28];
z[30] = 3 * abb[34];
z[28] = -z[14] + 3 * z[28] + -z[30];
z[31] = abb[25] * z[28];
z[32] = abb[28] * z[5];
z[26] = -abb[57] + z[26] + z[32];
z[26] = (T(-5) / T(2)) * z[14] + 3 * z[26];
z[32] = abb[23] * z[26];
z[33] = abb[24] * z[30];
z[7] = -3 * z[7] + z[16] + -z[31] + z[32] + z[33];
z[16] = abb[59] * (T(3) / T(2));
z[31] = abb[26] * z[16];
z[31] = z[7] + z[31];
z[32] = abb[51] * z[31];
z[33] = abb[30] * z[0];
z[34] = abb[32] * z[0];
z[33] = z[33] + -z[34];
z[35] = 3 * abb[35];
z[33] = z[15] + -z[30] + 3 * z[33] + -z[35];
z[36] = abb[44] + -abb[50];
z[37] = abb[47] + z[36];
z[33] = abb[3] * z[33] * z[37];
z[32] = z[32] + -z[33];
z[24] = abb[22] + z[24];
z[33] = z[24] * z[35];
z[31] = -z[31] + z[33];
z[33] = abb[53] + abb[54] + abb[55];
z[38] = (T(1) / T(2)) * z[33];
z[31] = z[31] * z[38];
z[38] = abb[52] * (T(1) / T(2));
z[7] = -z[7] * z[38];
z[39] = abb[31] * (T(1) / T(2));
z[40] = -z[17] + z[39];
z[41] = 2 * abb[29];
z[42] = abb[30] * (T(3) / T(4));
z[43] = -abb[27] + -z[40] + z[41] + z[42];
z[43] = abb[27] * z[43];
z[44] = 3 * abb[32];
z[18] = z[18] * z[44];
z[8] = (T(3) / T(2)) * z[8] + z[18];
z[45] = (T(3) / T(2)) * z[10];
z[46] = abb[36] + z[45];
z[47] = z[8] + -z[46];
z[48] = abb[30] * (T(13) / T(2)) + z[6];
z[49] = abb[27] * (T(3) / T(2));
z[50] = z[48] + -z[49];
z[50] = abb[28] + (T(1) / T(2)) * z[50];
z[50] = abb[28] * z[50];
z[51] = abb[33] * (T(3) / T(2));
z[52] = abb[30] + z[6];
z[53] = abb[30] * z[52];
z[13] = (T(9) / T(2)) * z[13] + (T(-7) / T(12)) * z[14] + z[43] + (T(1) / T(2)) * z[47] + z[50] + -z[51] + 4 * z[53];
z[13] = abb[44] * z[13];
z[43] = abb[47] * (T(3) / T(2));
z[2] = -z[2] * z[43];
z[47] = abb[29] + z[3];
z[50] = abb[30] * (T(5) / T(4));
z[40] = -z[40] + -z[47] + -z[50];
z[40] = abb[27] * z[40];
z[54] = abb[57] * (T(3) / T(2));
z[55] = abb[34] * (T(3) / T(2));
z[56] = z[54] + -z[55];
z[57] = (T(3) / T(4)) * z[14] + z[56];
z[8] = -abb[36] + -z[8] + z[45];
z[58] = 2 * abb[30];
z[59] = -z[6] * z[58];
z[60] = abb[28] * (T(1) / T(2));
z[61] = abb[30] * (T(3) / T(2)) + -z[6];
z[62] = abb[27] * (T(5) / T(2)) + -z[61];
z[62] = z[60] * z[62];
z[8] = (T(1) / T(2)) * z[8] + z[40] + z[51] + z[57] + z[59] + z[62];
z[8] = abb[50] * z[8];
z[40] = 3 * abb[29];
z[59] = -z[3] + z[40] + z[58];
z[59] = abb[27] * z[59];
z[62] = abb[30] + z[40];
z[63] = z[58] * z[62];
z[64] = -abb[27] + z[58];
z[64] = abb[28] + 2 * z[64];
z[64] = abb[28] * z[64];
z[65] = 3 * abb[33];
z[66] = (T(1) / T(6)) * z[14];
z[59] = 6 * abb[57] + z[45] + -z[59] + z[63] + -z[64] + z[65] + -z[66];
z[63] = -abb[42] * z[59];
z[64] = abb[27] + abb[30];
z[67] = abb[28] + z[64];
z[67] = z[60] * z[67];
z[68] = abb[27] + z[1];
z[68] = abb[27] * z[68];
z[69] = (T(5) / T(6)) * z[14];
z[70] = prod_pow(abb[30], 2);
z[68] = z[67] + -z[68] + z[69] + 2 * z[70];
z[71] = -abb[43] * z[68];
z[2] = z[2] + z[8] + z[13] + z[63] + z[71];
z[2] = abb[5] * z[2];
z[8] = z[1] * z[6];
z[13] = abb[27] + z[6];
z[63] = z[13] * z[60];
z[8] = z[8] + -z[63];
z[63] = 3 * abb[31];
z[71] = abb[31] + abb[29] * (T(-3) / T(2));
z[63] = z[63] * z[71];
z[71] = abb[32] * (T(3) / T(2));
z[72] = -abb[31] + -abb[32];
z[72] = z[71] * z[72];
z[73] = abb[27] + 4 * abb[31] + abb[29] * (T(-5) / T(2)) + -z[1] + z[17];
z[73] = abb[27] * z[73];
z[63] = 4 * abb[36] + -z[8] + (T(9) / T(2)) * z[29] + z[45] + -z[51] + z[63] + z[72] + z[73];
z[63] = abb[50] * z[63];
z[72] = abb[28] * z[13];
z[73] = abb[30] * z[6];
z[72] = z[72] + -z[73];
z[10] = abb[33] + z[10];
z[74] = abb[29] + abb[30];
z[75] = -abb[32] + z[74];
z[75] = abb[27] * z[75];
z[23] = -z[10] + z[23] + -z[29] + z[34] + -z[72] + z[75];
z[23] = z[23] * z[43];
z[34] = abb[29] * (T(1) / T(2));
z[75] = 2 * abb[31];
z[76] = z[34] + -z[75];
z[4] = -z[4] + z[17] + z[76];
z[4] = abb[27] * z[4];
z[77] = abb[31] * z[5];
z[78] = z[29] + z[77];
z[79] = 2 * abb[36];
z[71] = abb[31] * z[71];
z[4] = z[4] + -z[8] + z[51] + z[71] + (T(-3) / T(2)) * z[78] + -z[79];
z[4] = abb[44] * z[4];
z[4] = z[4] + z[23] + z[63];
z[4] = abb[14] * z[4];
z[8] = prod_pow(abb[32], 2);
z[23] = (T(3) / T(2)) * z[8];
z[51] = -z[23] + 3 * z[29];
z[63] = -abb[29] + z[39];
z[71] = abb[31] * z[63];
z[78] = 3 * z[71];
z[80] = z[46] + z[78];
z[81] = z[51] + z[80];
z[82] = abb[32] + (T(1) / T(2)) * z[5];
z[83] = -z[3] + -z[42] + z[82];
z[83] = abb[27] * z[83];
z[84] = (T(1) / T(2)) * z[6] + z[58];
z[84] = abb[30] * z[84];
z[85] = z[6] + z[60];
z[86] = abb[27] * (T(3) / T(4)) + z[50] + z[85];
z[86] = abb[28] * z[86];
z[83] = (T(1) / T(12)) * z[14] + -z[56] + (T(1) / T(2)) * z[81] + z[83] + z[84] + z[86];
z[83] = abb[44] * z[83];
z[81] = z[73] + z[81];
z[84] = abb[30] * (T(1) / T(4));
z[82] = z[3] + z[82] + -z[84];
z[82] = abb[27] * z[82];
z[42] = z[6] + z[42];
z[86] = abb[27] * (T(1) / T(4));
z[87] = z[42] + z[86];
z[87] = abb[28] * z[87];
z[57] = -z[57] + (T(1) / T(2)) * z[81] + z[82] + z[87];
z[57] = abb[50] * z[57];
z[81] = 3 * abb[58];
z[80] = -z[80] + z[81];
z[27] = -abb[27] * z[27];
z[27] = -abb[56] + (T(-1) / T(2)) * z[8] + z[27] + -z[73] + z[80];
z[73] = z[5] + z[84];
z[82] = -z[73] + z[86];
z[82] = abb[28] * z[82];
z[27] = (T(1) / T(2)) * z[27] + z[54] + z[82];
z[27] = (T(1) / T(4)) * z[14] + 3 * z[27] + -z[55];
z[27] = abb[47] * z[27];
z[82] = -abb[30] + z[44];
z[84] = -abb[27] + z[60];
z[86] = z[82] + z[84];
z[86] = abb[28] * z[86];
z[82] = -z[3] + z[82];
z[82] = abb[27] * z[82];
z[87] = -abb[56] + z[8];
z[82] = -z[70] + -z[82] + z[86] + 3 * z[87];
z[86] = -z[30] + -z[66] + z[82];
z[87] = abb[43] * z[86];
z[68] = abb[42] * z[68];
z[27] = z[27] + z[57] + z[68] + z[83] + z[87];
z[27] = abb[6] * z[27];
z[57] = z[3] + z[6];
z[68] = abb[27] * z[57];
z[83] = abb[32] + 2 * z[5];
z[87] = abb[30] + z[83];
z[88] = abb[28] + z[87];
z[88] = abb[28] * z[88];
z[89] = 3 * abb[57];
z[23] = z[23] + z[53] + z[68] + -z[69] + -z[80] + z[88] + -z[89];
z[53] = abb[6] + abb[7];
z[23] = z[23] * z[53];
z[69] = z[58] + z[85];
z[69] = abb[28] * z[69];
z[52] = z[52] * z[58];
z[88] = (T(2) / T(3)) * z[14];
z[30] = -abb[36] + z[30] + z[52] + -z[68] + z[69] + -z[88] + -z[89];
z[30] = abb[5] * z[30];
z[52] = -abb[30] + z[3] + z[83];
z[52] = abb[27] * z[52];
z[51] = z[45] + z[51] + z[52] + z[72] + z[78] + z[79];
z[51] = abb[14] * z[51];
z[52] = z[58] + z[84];
z[52] = abb[28] * z[52];
z[69] = abb[30] * z[62];
z[52] = z[52] + -z[69] + -z[89];
z[69] = abb[27] + -abb[30];
z[72] = abb[27] * z[69];
z[45] = (T(10) / T(3)) * z[14] + -z[45] + z[52] + -z[72];
z[45] = abb[1] * z[45];
z[72] = abb[2] * z[86];
z[78] = abb[30] + z[60];
z[78] = abb[28] * z[78];
z[78] = -abb[57] + -z[11] + -z[21] + z[78];
z[78] = 2 * z[14] + 3 * z[78];
z[78] = abb[12] * z[78];
z[26] = abb[8] * z[26];
z[23] = z[23] + -z[26] + z[30] + z[45] + z[51] + z[72] + -z[78];
z[30] = abb[46] * z[23];
z[45] = abb[27] * z[5];
z[29] = abb[36] + z[29];
z[45] = z[14] + z[29] + z[45] + z[77];
z[51] = 3 * abb[11];
z[45] = z[45] * z[51];
z[23] = z[23] + -z[45];
z[23] = abb[45] * z[23];
z[39] = -z[39] + z[40];
z[39] = abb[31] * z[39];
z[21] = -z[19] + z[21] + z[39] + -z[46] + -z[68] + z[81];
z[39] = z[1] + z[5] + z[17];
z[46] = z[39] + z[60];
z[46] = abb[28] * z[46];
z[21] = (T(1) / T(2)) * z[21] + -z[46] + z[54];
z[21] = z[15] + 3 * z[21] + z[55];
z[21] = abb[47] * z[21];
z[57] = z[3] * z[57];
z[46] = z[46] + z[57];
z[17] = abb[31] + z[17];
z[57] = z[17] * z[44];
z[57] = z[57] + -z[80];
z[0] = -abb[30] + z[0] + -z[34];
z[0] = abb[30] * z[0];
z[0] = z[0] + z[46] + -z[54] + -z[55] + (T(1) / T(2)) * z[57] + -z[66];
z[0] = abb[50] * z[0];
z[18] = z[18] + -z[80];
z[34] = -2 * abb[32] + z[58] + -z[76];
z[34] = abb[30] * z[34];
z[18] = (T(1) / T(2)) * z[18] + z[34] + z[46] + -z[56] + -z[88];
z[18] = abb[44] * z[18];
z[0] = z[0] + z[18] + z[21];
z[0] = abb[7] * z[0];
z[18] = -abb[31] + z[3];
z[18] = abb[27] * z[18];
z[21] = -abb[33] + z[11];
z[18] = z[18] + -z[19] + -z[21] + -z[29] + -z[71];
z[19] = abb[4] * z[18];
z[34] = -abb[29] + z[3];
z[46] = -z[34] + -z[75];
z[46] = abb[27] * z[46];
z[17] = abb[32] * z[17];
z[54] = abb[31] * (T(-3) / T(2)) + z[41];
z[54] = abb[31] * z[54];
z[29] = z[17] + -z[21] + -2 * z[29] + z[46] + z[54];
z[29] = abb[14] * z[29];
z[19] = z[19] + -z[29];
z[29] = prod_pow(abb[27], 2);
z[8] = -z[8] + -z[14] + z[29];
z[29] = 3 * abb[9];
z[8] = z[8] * z[29];
z[29] = abb[27] * z[47];
z[21] = z[21] + z[29];
z[21] = (T(-13) / T(2)) * z[14] + 3 * z[21];
z[21] = abb[1] * z[21];
z[9] = -z[9] + -z[17];
z[9] = 3 * z[9] + z[15];
z[9] = abb[7] * z[9];
z[17] = -abb[4] + -abb[7] + abb[14];
z[29] = -z[17] * z[35];
z[9] = z[8] + z[9] + -3 * z[19] + z[21] + z[29] + z[45];
z[9] = abb[48] * z[9];
z[19] = -abb[1] * z[59];
z[21] = abb[33] + abb[57] + z[22];
z[21] = abb[13] * z[21];
z[22] = abb[13] * z[60];
z[29] = z[22] * z[69];
z[21] = z[21] + z[29];
z[29] = abb[13] * z[14];
z[21] = 3 * z[21] + z[29];
z[29] = z[21] + -z[26];
z[19] = z[19] + (T(1) / T(2)) * z[29] + -z[78];
z[19] = abb[44] * z[19];
z[29] = 2 * abb[27];
z[35] = -z[1] + z[29];
z[35] = abb[27] * z[35];
z[35] = (T(-13) / T(6)) * z[14] + z[35] + z[67] + -z[70];
z[35] = abb[0] * z[35];
z[8] = -z[8] + z[35];
z[46] = -z[3] + -z[62];
z[46] = abb[27] * z[46];
z[46] = (T(19) / T(6)) * z[14] + z[46] + -z[52] + z[65];
z[46] = abb[1] * z[46];
z[47] = -z[21] + -z[26];
z[46] = z[8] + z[46] + (T(1) / T(2)) * z[47];
z[46] = abb[50] * z[46];
z[36] = -abb[47] + z[36];
z[47] = abb[4] * (T(3) / T(2));
z[18] = -z[18] * z[36] * z[47];
z[54] = abb[44] + abb[50];
z[55] = -z[54] * z[86];
z[56] = -9 * abb[34] + -z[15] + 3 * z[82];
z[56] = abb[43] * z[56];
z[55] = z[55] + z[56];
z[55] = abb[2] * z[55];
z[34] = -abb[30] + z[34];
z[34] = abb[27] * z[34];
z[10] = z[10] + z[34] + -z[52];
z[10] = 3 * z[10] + (T(-7) / T(2)) * z[14];
z[10] = abb[1] * z[10];
z[10] = z[10] + -z[21] + z[78];
z[34] = -z[10] + -z[35];
z[34] = abb[42] * z[34];
z[35] = abb[28] + 3 * abb[30];
z[52] = abb[27] + -z[35];
z[52] = z[52] * z[60];
z[56] = abb[29] * z[58];
z[11] = abb[33] + 2 * abb[57] + z[11] + z[12] + z[52] + z[56];
z[11] = 3 * z[11] + -z[14];
z[11] = abb[5] * z[11];
z[12] = abb[27] * z[1];
z[12] = z[12] + -z[67] + -z[70];
z[12] = 3 * z[12] + z[15];
z[12] = abb[6] * z[12];
z[14] = abb[16] + abb[18];
z[15] = abb[17] + z[14];
z[52] = -z[15] * z[16];
z[10] = z[10] + z[11] + z[12] + z[52];
z[10] = abb[49] * z[10];
z[11] = z[21] + 3 * z[26];
z[11] = (T(1) / T(2)) * z[11] + z[78];
z[11] = abb[47] * z[11];
z[8] = -abb[43] * z[8];
z[12] = abb[17] + -abb[19];
z[21] = z[12] + -z[14];
z[21] = abb[50] * z[21];
z[26] = abb[19] + z[15];
z[26] = abb[47] * z[26];
z[52] = -abb[19] + z[15];
z[52] = abb[44] * z[52];
z[21] = z[21] + z[26] + z[52];
z[14] = abb[42] * z[14];
z[12] = abb[43] * z[12];
z[26] = abb[26] * z[38];
z[12] = -z[12] + z[14] + (T(1) / T(2)) * z[21] + -z[26];
z[14] = z[12] * z[16];
z[16] = -abb[47] + abb[50];
z[21] = -abb[46] + -z[16];
z[21] = z[21] * z[45];
z[26] = -abb[47] + z[54];
z[26] = -abb[43] + (T(1) / T(2)) * z[26];
z[26] = abb[15] * z[26] * z[28];
z[28] = abb[51] + abb[52];
z[24] = z[24] * z[28];
z[17] = -z[17] * z[36];
z[28] = abb[5] * z[37];
z[17] = z[17] + z[24] + z[28];
z[17] = abb[35] * z[17];
z[0] = abb[60] + z[0] + z[2] + z[4] + z[7] + z[8] + z[9] + z[10] + z[11] + z[14] + (T(3) / T(2)) * z[17] + z[18] + z[19] + z[21] + z[23] + z[26] + z[27] + z[30] + z[31] + (T(-1) / T(2)) * z[32] + z[34] + z[46] + z[55];
z[2] = abb[28] + z[58];
z[4] = z[2] + z[29] + z[40];
z[7] = abb[1] * m1_set::bc<T>[0];
z[4] = z[4] * z[7];
z[8] = 2 * abb[28] + z[87];
z[8] = m1_set::bc<T>[0] * z[8];
z[9] = 3 * abb[39];
z[10] = 3 * abb[38];
z[8] = z[8] + -z[9] + -z[10];
z[8] = z[8] * z[53];
z[11] = -abb[28] + -z[44] + z[64];
z[11] = m1_set::bc<T>[0] * z[11];
z[14] = 3 * abb[37];
z[11] = z[11] + z[14];
z[17] = abb[2] * z[11];
z[18] = -abb[28] + -abb[31] + z[41];
z[18] = m1_set::bc<T>[0] * z[18];
z[18] = abb[39] + z[18];
z[19] = 3 * abb[8];
z[18] = z[18] * z[19];
z[21] = z[2] + z[6];
z[21] = m1_set::bc<T>[0] * z[21];
z[21] = -z[10] + z[21];
z[21] = abb[5] * z[21];
z[23] = -abb[1] + abb[8] + abb[12];
z[23] = z[10] * z[23];
z[9] = z[9] + -z[14];
z[13] = m1_set::bc<T>[0] * z[13];
z[24] = z[9] + -z[13];
z[24] = abb[14] * z[24];
z[26] = abb[12] * m1_set::bc<T>[0] * z[74];
z[27] = 3 * z[26];
z[4] = z[4] + z[8] + -z[17] + z[18] + z[21] + z[23] + -z[24] + -z[27];
z[8] = abb[46] * z[4];
z[18] = abb[37] + -abb[39];
z[21] = abb[27] + abb[29];
z[23] = m1_set::bc<T>[0] * z[21];
z[24] = z[18] + z[23];
z[28] = -z[24] * z[51];
z[4] = z[4] + z[28];
z[4] = abb[45] * z[4];
z[28] = abb[28] * (T(3) / T(4));
z[30] = -z[3] + z[28] + z[42];
z[30] = m1_set::bc<T>[0] * z[30];
z[31] = abb[39] * (T(3) / T(2));
z[30] = z[30] + -z[31];
z[30] = abb[50] * z[30];
z[6] = abb[28] * (T(1) / T(4)) + z[6] + z[49] + z[50];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = abb[37] * (T(3) / T(2)) + z[6] + -z[31];
z[6] = abb[44] * z[6];
z[3] = abb[28] * (T(-5) / T(4)) + -z[3] + -z[73];
z[3] = m1_set::bc<T>[0] * z[3];
z[3] = z[3] + z[31];
z[32] = 3 * abb[47];
z[3] = z[3] * z[32];
z[11] = -abb[43] * z[11];
z[34] = -z[32] + z[54];
z[36] = abb[38] * (T(3) / T(2));
z[37] = -z[34] * z[36];
z[38] = -z[1] + z[60];
z[41] = z[29] + -z[38];
z[42] = abb[42] * m1_set::bc<T>[0] * z[41];
z[44] = abb[37] * z[16];
z[3] = z[3] + z[6] + z[11] + z[30] + z[37] + z[42] + (T(3) / T(2)) * z[44];
z[3] = abb[6] * z[3];
z[6] = abb[20] * abb[27];
z[11] = abb[24] * z[60];
z[6] = z[6] + z[11] + z[25];
z[6] = m1_set::bc<T>[0] * z[6];
z[11] = -z[20] + z[84];
z[11] = m1_set::bc<T>[0] * z[11];
z[20] = -abb[37] + z[11];
z[25] = abb[25] * z[20];
z[30] = abb[20] * abb[39];
z[37] = abb[20] * abb[37];
z[6] = z[6] + -z[25] + -z[30] + z[37];
z[25] = abb[22] + abb[23] + abb[24];
z[30] = abb[38] * (T(1) / T(2));
z[25] = z[25] * z[30];
z[5] = z[5] + z[38];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = -abb[39] + z[5];
z[30] = abb[21] * (T(1) / T(2));
z[5] = z[5] * z[30];
z[30] = z[60] + z[63];
z[30] = m1_set::bc<T>[0] * z[30];
z[30] = abb[39] * (T(-1) / T(2)) + z[30];
z[37] = abb[23] * z[30];
z[42] = z[1] + z[85];
z[45] = m1_set::bc<T>[0] * (T(1) / T(2));
z[46] = abb[22] * z[42] * z[45];
z[5] = -z[5] + (T(-1) / T(2)) * z[6] + z[25] + -z[37] + -z[46];
z[6] = abb[52] * z[5];
z[25] = abb[29] + z[2];
z[25] = z[7] * z[25];
z[1] = -abb[29] + z[1];
z[1] = abb[13] * z[1];
z[1] = z[1] + z[22];
z[22] = m1_set::bc<T>[0] * z[1];
z[22] = -z[22] + z[25] + -z[26];
z[25] = abb[28] + -abb[30];
z[25] = abb[6] * z[25] * z[45];
z[35] = -z[35] * z[45];
z[35] = 2 * abb[38] + z[35];
z[35] = abb[5] * z[35];
z[37] = 3 * abb[1] + -abb[12] + -abb[13];
z[46] = abb[38] * z[37];
z[15] = abb[40] * z[15];
z[15] = (T(-1) / T(2)) * z[15] + -z[22] + z[25] + z[35] + z[46];
z[15] = abb[49] * z[15];
z[21] = -z[7] * z[21];
z[25] = abb[9] * abb[27] * m1_set::bc<T>[0];
z[35] = abb[14] * z[18];
z[21] = z[21] + -z[25] + -z[35];
z[35] = abb[27] + -abb[29];
z[35] = m1_set::bc<T>[0] * z[35];
z[18] = z[18] + z[35];
z[35] = abb[4] * z[18];
z[46] = abb[11] * z[24];
z[21] = 2 * z[21] + z[35] + z[46];
z[21] = abb[48] * z[21];
z[11] = z[11] * z[16];
z[35] = abb[44] * z[20];
z[11] = z[11] + z[35] + -z[44];
z[20] = -abb[43] * z[20];
z[11] = (T(1) / T(2)) * z[11] + z[20];
z[11] = abb[15] * z[11];
z[6] = z[6] + z[11] + z[15] + z[21];
z[11] = abb[26] * abb[40];
z[5] = z[5] + (T(-1) / T(4)) * z[11];
z[11] = abb[51] + z[33];
z[11] = 3 * z[11];
z[5] = z[5] * z[11];
z[11] = -z[28] + z[29] + (T(-1) / T(2)) * z[61];
z[11] = abb[50] * z[11];
z[15] = -z[42] * z[43];
z[20] = -abb[43] * z[41];
z[11] = z[11] + z[15] + z[20];
z[11] = m1_set::bc<T>[0] * z[11];
z[15] = abb[28] * (T(5) / T(2)) + z[48];
z[15] = abb[44] * z[15] * z[45];
z[20] = abb[47] + abb[50];
z[21] = -3 * abb[44] + z[20];
z[21] = z[21] * z[36];
z[28] = -abb[27] + z[2];
z[29] = m1_set::bc<T>[0] * z[28];
z[29] = -z[10] + z[29];
z[29] = abb[42] * z[29];
z[11] = z[11] + z[15] + z[21] + 2 * z[29];
z[11] = abb[5] * z[11];
z[15] = abb[8] * z[30];
z[1] = z[1] * z[45];
z[21] = z[1] + -z[15];
z[29] = 4 * abb[27];
z[2] = -z[2] + z[29] + z[40];
z[2] = z[2] * z[7];
z[29] = z[29] + z[38];
z[29] = abb[0] * m1_set::bc<T>[0] * z[29];
z[25] = 6 * z[25] + -z[29];
z[2] = z[2] + 3 * z[21] + z[25];
z[2] = abb[50] * z[2];
z[21] = -z[10] * z[37];
z[21] = z[21] + 3 * z[22] + z[29];
z[21] = abb[42] * z[21];
z[22] = abb[39] + -z[13];
z[22] = z[22] * z[32];
z[29] = -abb[47] + 3 * abb[50];
z[14] = z[14] * z[29];
z[9] = z[9] + z[13];
z[9] = abb[44] * z[9];
z[13] = -9 * abb[39] + z[13];
z[13] = abb[50] * z[13];
z[9] = z[9] + z[13] + z[14] + z[22];
z[9] = abb[14] * z[9];
z[12] = abb[40] * z[12];
z[13] = z[19] * z[30];
z[13] = -z[1] + z[13] + z[26];
z[13] = z[13] * z[32];
z[1] = -z[1] + -z[15];
z[7] = z[7] * z[28];
z[1] = 3 * z[1] + 2 * z[7] + -z[27];
z[1] = abb[44] * z[1];
z[7] = abb[44] + -z[20];
z[7] = z[7] * z[18] * z[47];
z[14] = abb[13] + -z[19];
z[14] = -abb[12] + (T(1) / T(2)) * z[14];
z[14] = abb[47] * z[14];
z[15] = abb[8] + -abb[13];
z[15] = abb[1] + (T(1) / T(2)) * z[15];
z[15] = abb[50] * z[15];
z[18] = abb[8] + abb[13];
z[18] = -2 * abb[1] + abb[12] + (T(1) / T(2)) * z[18];
z[18] = abb[44] * z[18];
z[14] = z[14] + z[15] + z[18];
z[10] = z[10] * z[14];
z[14] = abb[28] + z[39];
z[14] = m1_set::bc<T>[0] * z[14];
z[14] = z[14] + -z[31] + -z[36];
z[14] = abb[7] * z[14] * z[34];
z[15] = -abb[39] + z[23];
z[15] = -z[15] * z[16];
z[16] = -abb[46] * z[24];
z[15] = z[15] + z[16] + -z[44];
z[15] = z[15] * z[51];
z[16] = -abb[43] * z[25];
z[18] = -3 * abb[43] + z[54];
z[17] = z[17] * z[18];
z[1] = abb[41] + z[1] + z[2] + z[3] + z[4] + z[5] + 3 * z[6] + z[7] + z[8] + (T(1) / T(2)) * z[9] + z[10] + z[11] + (T(3) / T(2)) * z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[21];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_512_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.450078917274434293101657209467872430583744502505932285414849113"),stof<T>("-53.134356214699376742634265603799826706847615954368603431025468477")}, std::complex<T>{stof<T>("-3.842303589037347753716178442501725703371338453839769594880634106"),stof<T>("-15.122073742449383876612057114347449851412820922669855048999498447")}, std::complex<T>{stof<T>("5.955619561115181136183194534859822576938147794007982362233604841"),stof<T>("-2.6802180037284676978423523679772994022041376239944463996023347556")}, std::complex<T>{stof<T>("10.196413297685457616930521855257939299806246635619880033109209743"),stof<T>("-16.440645940048484040486324987198986131844974154008383607881331246")}, std::complex<T>{stof<T>("10.196413297685457616930521855257939299806246635619880033109209743"),stof<T>("-16.440645940048484040486324987198986131844974154008383607881331246")}, std::complex<T>{stof<T>("50.988688193162936909068429124561912295980143336839418967543593764"),stof<T>("-5.910846882291830441500942051562465807976598915046520025025282327")}, std::complex<T>{stof<T>("-43.693299263115379353354045628413022018389543203864180985206756727"),stof<T>("49.092492404841541510823828740696764182871493937718239190702631764")}, std::complex<T>{stof<T>("-41.024978470615073700210068323774544434220028566552717566617134902"),stof<T>("25.031710826068782179829619406738751342025710693049350032508948328")}, std::complex<T>{stof<T>("8.0830973256076242344635057628998424262394372954516672657562390083"),stof<T>("1.3616458061293675339680844951257631217719843926559178407205019567")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_512_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_512_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[43] + -abb[44]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[30], 2);
z[1] = -prod_pow(abb[32], 2);
z[0] = z[0] + z[1];
z[1] = abb[43] + -abb[44];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_512_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_512_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-6.194762672576488621916568983164697542571121639990908593947942353"),stof<T>("46.288936587992042781314864263340869758586774024164777721917736206")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,61> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W20(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k), T{0}};
abb[41] = SpDLog_f_4_512_W_20_Im(t, path, abb);
abb[60] = SpDLog_f_4_512_W_20_Re(t, path, abb);

                    
            return f_4_512_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_512_DLogXconstant_part(base_point<T>, kend);
	value += f_4_512_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_512_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_512_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_512_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_512_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_512_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_512_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
