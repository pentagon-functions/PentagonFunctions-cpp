/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_144.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_144_abbreviated (const std::array<T,29>& abb) {
T z[42];
z[0] = abb[18] * (T(1) / T(2));
z[1] = abb[20] * (T(1) / T(2));
z[2] = abb[23] + z[0] + -z[1];
z[3] = abb[24] * (T(3) / T(2));
z[4] = 2 * abb[19] + abb[21];
z[5] = 2 * z[4];
z[6] = z[2] + z[3] + -z[5];
z[7] = abb[2] * z[6];
z[8] = abb[7] + abb[8];
z[9] = -abb[25] + abb[26];
z[10] = (T(3) / T(2)) * z[9];
z[8] = z[8] * z[10];
z[7] = z[7] + z[8];
z[8] = 4 * z[4];
z[11] = 3 * abb[24];
z[12] = z[8] + -z[11];
z[13] = abb[18] + abb[23];
z[14] = z[12] + -z[13];
z[15] = 4 * abb[1];
z[15] = z[14] * z[15];
z[16] = -abb[24] + z[5] + -z[13];
z[17] = 3 * abb[6];
z[17] = z[16] * z[17];
z[15] = z[15] + -z[17];
z[18] = z[5] + -z[11];
z[19] = 2 * abb[23];
z[20] = abb[18] + -abb[20] + z[19];
z[21] = z[18] + z[20];
z[21] = abb[0] * z[21];
z[22] = 8 * z[4];
z[23] = abb[24] * (T(-15) / T(2)) + -z[2] + z[22];
z[23] = abb[3] * z[23];
z[24] = z[13] + z[18];
z[25] = abb[5] * z[24];
z[21] = z[7] + -z[15] + z[21] + -z[23] + 2 * z[25];
z[23] = -abb[12] * z[21];
z[26] = 2 * abb[3];
z[27] = z[14] * z[26];
z[24] = abb[0] * z[24];
z[24] = z[15] + -z[24] + -z[25] + z[27];
z[27] = 2 * z[24];
z[28] = abb[14] * z[27];
z[29] = 3 * abb[23];
z[22] = abb[24] * (T(-9) / T(2)) + abb[18] * (T(-7) / T(2)) + -z[1] + z[22] + -z[29];
z[22] = abb[3] * z[22];
z[30] = abb[18] + z[5];
z[31] = -abb[20] + z[11] + -z[30];
z[31] = abb[0] * z[31];
z[15] = z[7] + z[15] + z[22] + z[31];
z[15] = abb[11] * z[15];
z[22] = -z[12] + z[20];
z[31] = abb[2] * z[22];
z[32] = 3 * z[9];
z[33] = abb[8] * z[32];
z[31] = z[31] + z[33];
z[33] = 2 * abb[1];
z[34] = z[14] * z[33];
z[12] = abb[18] + abb[20] + -z[12];
z[35] = -abb[0] * z[12];
z[36] = 4 * abb[3];
z[37] = -abb[18] + z[4];
z[38] = abb[20] + -z[37];
z[38] = z[36] * z[38];
z[35] = -z[31] + -z[34] + z[35] + z[38];
z[35] = abb[13] * z[35];
z[15] = z[15] + z[23] + -z[28] + z[35];
z[15] = abb[13] * z[15];
z[23] = abb[20] * (T(13) / T(3));
z[35] = 3 * abb[22];
z[38] = abb[24] + abb[23] * (T(-8) / T(3)) + z[23] + (T(-4) / T(3)) * z[30] + z[35];
z[38] = abb[0] * z[38];
z[39] = -8 * abb[18] + 5 * abb[23] + -z[4];
z[23] = z[11] + -z[23] + (T(1) / T(3)) * z[39];
z[23] = abb[3] * z[23];
z[39] = -abb[24] + abb[22] * (T(1) / T(2));
z[40] = -z[0] + z[4] + z[39];
z[40] = abb[4] * z[40];
z[20] = -z[4] + z[20];
z[20] = abb[2] * z[20];
z[41] = abb[6] * z[16];
z[0] = -abb[23] + z[0] + -z[5];
z[0] = (T(1) / T(3)) * z[0] + -z[39];
z[0] = abb[1] * z[0];
z[19] = -2 * abb[18] + 5 * z[4] + -z[19];
z[19] = -abb[24] + (T(1) / T(3)) * z[19];
z[19] = abb[5] * z[19];
z[0] = z[0] + 4 * z[19] + (T(13) / T(3)) * z[20] + z[23] + z[38] + 5 * z[40] + -z[41];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[11] = -z[11] + 2 * z[30] + -z[35];
z[19] = -abb[23] + z[11];
z[19] = abb[0] * z[19];
z[23] = -abb[18] + z[8];
z[30] = 9 * abb[24];
z[23] = 7 * abb[23] + -4 * z[23] + z[30] + z[35];
z[23] = z[23] * z[33];
z[4] = 10 * z[4] + -z[13] + -z[30];
z[4] = abb[5] * z[4];
z[5] = 2 * abb[24] + -z[5];
z[13] = -abb[18] + abb[22];
z[30] = z[5] + -z[13];
z[30] = abb[4] * z[30];
z[33] = 3 * z[30];
z[14] = -z[14] * z[36];
z[4] = z[4] + z[14] + z[17] + z[19] + z[23] + -z[33];
z[4] = prod_pow(abb[14], 2) * z[4];
z[14] = -z[17] + z[34];
z[17] = 2 * z[37];
z[19] = abb[20] + z[17] + -z[29];
z[19] = abb[0] * z[19];
z[12] = abb[3] * z[12];
z[23] = abb[5] * z[16];
z[32] = abb[7] * z[32];
z[12] = z[12] + -z[14] + -z[19] + -4 * z[20] + -3 * z[23] + -z[32];
z[12] = abb[12] * z[12];
z[19] = abb[11] * z[21];
z[19] = z[12] + z[19] + z[28];
z[19] = abb[12] * z[19];
z[5] = z[5] + z[13];
z[5] = abb[0] * z[5];
z[13] = z[16] * z[26];
z[5] = -z[5] + -z[13] + z[30] + -z[34] + 2 * z[41];
z[5] = 3 * z[5];
z[13] = -abb[27] * z[5];
z[6] = -abb[8] * z[6];
z[20] = -abb[0] + 2 * abb[10];
z[9] = z[9] * z[20];
z[20] = -abb[2] + -abb[3];
z[10] = z[10] * z[20];
z[16] = -abb[9] * z[16];
z[2] = abb[24] * (T(1) / T(2)) + -z[2];
z[2] = abb[7] * z[2];
z[2] = z[2] + z[6] + z[9] + z[10] + z[16];
z[2] = abb[15] * z[2];
z[1] = abb[18] * (T(5) / T(2)) + -z[1] + z[3] + -z[8] + z[29];
z[1] = abb[3] * z[1];
z[3] = -abb[20] + z[11];
z[3] = abb[0] * z[3];
z[1] = z[1] + z[3] + z[7] + -z[14] + z[33];
z[1] = abb[11] * z[1];
z[1] = z[1] + z[28];
z[1] = abb[11] * z[1];
z[3] = abb[28] * z[27];
z[0] = z[0] + z[1] + 3 * z[2] + z[3] + z[4] + z[13] + z[15] + z[19];
z[1] = abb[14] * z[24];
z[2] = -z[14] + z[25] + z[31];
z[3] = 2 * abb[20] + -abb[23];
z[4] = z[3] + -z[11];
z[4] = abb[0] * z[4];
z[6] = -abb[3] * z[22];
z[4] = -z[2] + z[4] + z[6] + -z[32] + -z[33];
z[4] = abb[11] * z[4];
z[3] = -abb[18] + -z[3] + -z[18];
z[3] = z[3] * z[26];
z[6] = abb[20] + abb[23] + -z[17];
z[6] = abb[0] * z[6];
z[2] = z[2] + z[3] + z[6];
z[2] = abb[13] * z[2];
z[1] = -z[1] + z[2] + z[4] + -z[12];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[5];
z[3] = abb[17] * z[27];
z[1] = 2 * z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_144_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-24.99956298879195724646334096950701232722700192918398818690128959"),stof<T>("-47.354596369017377296165638330941865319511630445591128968004822401")}, std::complex<T>{stof<T>("-14.862061020734381865570907662947353228716358449602989244427518"),stof<T>("206.80392685970067009658626671375513608524240666940609631979498808")}, std::complex<T>{stof<T>("-73.500543323593783447196206033191752221167670670468919847632667344"),stof<T>("-10.418592166951844358583751536142331540900590163798333987904556194")}, std::complex<T>{stof<T>("-7.431030510367190932785453831473676614358179224801494622213759"),stof<T>("103.401963429850335048293133356877568042621203334703048159897494039")}, std::complex<T>{stof<T>("31.378019525416473642312525441353061252554471972194097149142889437"),stof<T>("-40.821802037629766081497972504998765526290901285783586822188641969")}, std::complex<T>{stof<T>("79.878999860218299843045390505037801146495140713479028809874267191"),stof<T>("-77.757806239695299019079859299798299304901941567576381802288908176")}, std::complex<T>{stof<T>("1.052573973742674536936269359627627689030709181791385659972159153"),stof<T>("-15.225565023203191670629522520936937196818671603328332369704029669")}, std::complex<T>{stof<T>("-3.072668366318546599157628038775032785955441755808356421746132133"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}, std::complex<T>{stof<T>("3.072668366318546599157628038775032785955441755808356421746132133"),stof<T>("-47.246795076652142140372750836458559900495186049435789623190676203")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_144_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_144_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("106.481314249209121611074205382197676437821409470914134683314817812"),stof<T>("-25.893739974231519378713504071337890986924851543338068036664474094")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_144_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_144_DLogXconstant_part(base_point<T>, kend);
	value += f_4_144_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_144_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_144_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_144_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_144_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_144_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_144_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
