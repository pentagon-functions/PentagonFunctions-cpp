/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_311.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_311_abbreviated (const std::array<T,30>& abb) {
T z[23];
z[0] = abb[21] + -abb[24];
z[1] = abb[19] + z[0];
z[1] = abb[6] * z[1];
z[2] = -abb[22] + abb[24];
z[3] = abb[20] + z[2];
z[3] = abb[1] * z[3];
z[4] = z[1] + -z[3];
z[5] = abb[21] + abb[22];
z[6] = -abb[24] + (T(1) / T(2)) * z[5];
z[7] = abb[5] * z[6];
z[0] = abb[20] + z[0];
z[0] = abb[8] * z[0];
z[4] = -4 * z[0] + (T(-5) / T(2)) * z[4] + z[7];
z[8] = abb[23] + -abb[24];
z[9] = abb[20] + z[8];
z[9] = abb[3] * z[9];
z[10] = abb[9] * z[6];
z[5] = -2 * abb[24] + z[5];
z[11] = abb[7] * z[5];
z[2] = abb[19] + z[2];
z[2] = abb[0] * z[2];
z[12] = abb[4] * z[5];
z[4] = z[2] + (T(-1) / T(3)) * z[4] + (T(-13) / T(6)) * z[9] + z[10] + z[11] + (T(-4) / T(3)) * z[12];
z[13] = prod_pow(m1_set::bc<T>[0], 2);
z[4] = z[4] * z[13];
z[14] = 3 * z[0] + -z[3];
z[6] = abb[4] * z[6];
z[15] = z[6] + 2 * z[9] + z[10] + -z[11] + (T(-1) / T(2)) * z[14];
z[15] = abb[12] * z[15];
z[16] = abb[9] * z[5];
z[17] = abb[5] * z[5];
z[18] = z[16] + -z[17];
z[0] = z[0] + z[3];
z[3] = -z[0] + z[18];
z[3] = abb[13] * z[3];
z[19] = 2 * abb[9];
z[5] = z[5] * z[19];
z[5] = -z[5] + z[12] + z[17];
z[19] = -abb[10] * z[5];
z[15] = -z[3] + z[15] + z[19];
z[15] = abb[12] * z[15];
z[19] = z[1] + -z[2] + z[11] + -z[16];
z[20] = prod_pow(abb[11], 2);
z[19] = z[19] * z[20];
z[21] = -abb[25] + -abb[27] + -abb[29];
z[21] = z[5] * z[21];
z[10] = (T(1) / T(2)) * z[0] + z[7] + -z[10];
z[10] = prod_pow(abb[13], 2) * z[10];
z[12] = -z[12] + z[16];
z[1] = z[1] + z[2];
z[2] = z[1] + z[12];
z[22] = abb[26] * z[2];
z[6] = -z[1] + z[6] + -z[7];
z[6] = prod_pow(abb[10], 2) * z[6];
z[0] = z[0] + z[12];
z[7] = abb[28] * z[0];
z[12] = (T(1) / T(6)) * z[13] + -2 * z[20];
z[8] = abb[19] + z[8];
z[8] = abb[2] * z[8] * z[12];
z[4] = z[4] + z[6] + z[7] + z[8] + z[10] + z[15] + z[19] + z[21] + z[22];
z[6] = -4 * z[9] + 2 * z[11] + z[14] + -3 * z[16] + z[17];
z[6] = abb[12] * z[6];
z[1] = z[1] + -z[18];
z[1] = abb[10] * z[1];
z[1] = 2 * z[1] + z[3] + z[6];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[14] + -abb[16] + -abb[18];
z[3] = z[3] * z[5];
z[2] = abb[15] * z[2];
z[0] = abb[17] * z[0];
z[0] = z[0] + z[1] + z[2] + z[3];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_311_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.5262705065963816215023436345020548524166054253027933874835052858"),stof<T>("-13.607267345876588693832657501666255175430300428594528940729547323")}, std::complex<T>{stof<T>("6.119938861643658700220336319163661693018395636593371160328722637"),stof<T>("21.425544949660128255842337925115345478028247307172848569450604868")}, std::complex<T>{stof<T>("17.458658660964296746224542773871486417456705593181801145151573518"),stof<T>("-14.702286692619478470385886786200586808042653819325576029818457138")}, std::complex<T>{stof<T>("-1.8171866217972134221884082186983125657446427144846036583419075302"),stof<T>("0.2992242860058395584021838247438241850349200540025869931639492108")}, std::complex<T>{stof<T>("-13.682176927714233089694958307908192142599558096375827030648263697"),stof<T>("22.819788582408857590797751034393501295675520751906482651703463893")}, std::complex<T>{stof<T>("-1.9592951114528502343411762472649817091125047823213704561614022903"),stof<T>("-8.4167261757952186788140480729367386726677869865834936150489559661")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_311_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_311_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("14.767756789044405112375794197452598500686211422025117800753028537"),stof<T>("44.728756524234909363646572179493393994916959848566537482786833657")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_311_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_311_DLogXconstant_part(base_point<T>, kend);
	value += f_4_311_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_311_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_311_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_311_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_311_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_311_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_311_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
