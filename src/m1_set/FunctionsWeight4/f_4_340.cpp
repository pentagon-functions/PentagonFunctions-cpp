/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_340.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_340_abbreviated (const std::array<T,26>& abb) {
T z[39];
z[0] = abb[18] + abb[20];
z[1] = abb[19] + (T(1) / T(2)) * z[0];
z[2] = 4 * abb[25];
z[3] = abb[21] + z[1] + -z[2];
z[3] = abb[8] * z[3];
z[4] = 2 * abb[22];
z[5] = -abb[24] + z[4];
z[6] = abb[6] * z[5];
z[7] = -abb[22] + abb[24];
z[8] = abb[1] * z[7];
z[9] = z[6] + 6 * z[8];
z[10] = 7 * abb[20];
z[11] = 9 * abb[18] + z[10];
z[12] = -abb[21] + 7 * abb[25];
z[13] = 4 * abb[19] + -z[12];
z[11] = (T(1) / T(4)) * z[11] + z[13];
z[14] = abb[7] * z[11];
z[15] = abb[23] * (T(1) / T(2));
z[16] = 2 * abb[24];
z[17] = abb[22] * (T(7) / T(4)) + z[15] + -z[16];
z[18] = -abb[3] * z[17];
z[19] = abb[22] * (T(1) / T(4)) + z[15];
z[20] = -abb[24] + z[19];
z[20] = abb[0] * z[20];
z[21] = 3 * z[7];
z[22] = abb[23] + z[21];
z[22] = abb[2] * z[22];
z[23] = abb[23] + abb[24];
z[24] = abb[4] * z[23];
z[14] = z[3] + -z[9] + z[14] + z[18] + -z[20] + -z[22] + 2 * z[24];
z[14] = abb[12] * z[14];
z[18] = abb[22] + abb[23];
z[25] = -abb[24] + -z[18];
z[25] = abb[3] * z[25];
z[22] = z[22] + z[25];
z[25] = -abb[23] + z[7];
z[26] = abb[0] * z[25];
z[27] = -z[6] + z[26];
z[28] = 2 * abb[25];
z[29] = abb[19] + abb[20] + abb[21];
z[29] = -z[28] + (T(1) / T(2)) * z[29];
z[29] = abb[7] * z[29];
z[30] = 3 * z[8];
z[2] = abb[21] * (T(1) / T(2)) + abb[19] * (T(5) / T(2)) + -z[2];
z[31] = abb[18] + abb[20] * (T(3) / T(2)) + z[2];
z[31] = abb[8] * z[31];
z[22] = (T(1) / T(2)) * z[22] + z[24] + -z[27] + z[29] + z[30] + z[31];
z[22] = abb[11] * z[22];
z[10] = 5 * abb[18] + z[10];
z[29] = 3 * abb[25];
z[10] = 3 * abb[19] + (T(1) / T(4)) * z[10] + -z[29];
z[10] = abb[7] * z[10];
z[19] = abb[24] + z[19];
z[19] = abb[3] * z[19];
z[9] = -z[9] + z[10] + z[19];
z[10] = abb[5] * z[25];
z[19] = z[10] + z[24];
z[25] = abb[18] + -abb[20];
z[25] = abb[8] * z[25];
z[31] = 3 * abb[22];
z[32] = 4 * abb[24] + -z[31];
z[32] = abb[2] * z[32];
z[20] = -z[9] + -2 * z[19] + z[20] + (T(1) / T(2)) * z[25] + z[32];
z[20] = abb[13] * z[20];
z[23] = abb[3] * z[23];
z[23] = -z[6] + z[23];
z[0] = 2 * abb[19] + z[0];
z[25] = z[0] + -z[28];
z[28] = abb[7] * z[25];
z[32] = z[23] + z[28];
z[26] = 5 * z[8] + -z[19] + -z[26] + -z[32];
z[26] = abb[14] * z[26];
z[33] = abb[2] * abb[14];
z[21] = z[21] * z[33];
z[21] = z[21] + z[26];
z[21] = 2 * z[21];
z[14] = z[14] + z[20] + -z[21] + z[22];
z[14] = abb[11] * z[14];
z[20] = abb[8] * z[25];
z[22] = -z[16] + z[31];
z[26] = -abb[23] + -z[22];
z[26] = abb[2] * z[26];
z[34] = abb[9] * z[25];
z[35] = 4 * z[8];
z[36] = 2 * abb[23];
z[37] = -abb[24] + -z[36];
z[37] = abb[3] * z[37];
z[26] = -z[20] + z[26] + -z[27] + -z[28] + z[34] + z[35] + z[37];
z[26] = abb[16] * z[26];
z[27] = 2 * abb[21];
z[34] = -20 * abb[25] + 7 * z[0] + z[27];
z[34] = abb[10] * z[34];
z[37] = -7 * abb[18] + -9 * abb[20];
z[13] = -z[13] + (T(1) / T(4)) * z[37];
z[13] = abb[0] * z[13];
z[17] = abb[7] * z[17];
z[11] = -abb[3] * z[11];
z[37] = -abb[6] * z[25];
z[38] = abb[23] + abb[22] * (T(1) / T(2));
z[38] = abb[8] * z[38];
z[11] = z[11] + z[13] + z[17] + z[34] + z[37] + z[38];
z[11] = abb[17] * z[11];
z[13] = 3 * abb[23];
z[17] = -abb[24] + z[13] + z[31];
z[17] = abb[0] * z[17];
z[13] = -abb[24] + -z[13];
z[13] = abb[3] * z[13];
z[6] = z[6] + 7 * z[8] + z[13] + z[17] + -3 * z[19] + -z[28];
z[13] = prod_pow(abb[14], 2);
z[6] = z[6] * z[13];
z[4] = abb[24] * (T(3) / T(2)) + -z[4] + -z[15];
z[4] = abb[2] * z[4];
z[17] = abb[23] + -abb[24];
z[17] = abb[0] * z[17];
z[2] = -abb[20] + abb[18] * (T(-3) / T(2)) + -z[2];
z[2] = abb[8] * z[2];
z[2] = z[2] + z[4] + z[10] + (T(1) / T(2)) * z[17] + z[30] + -z[32];
z[2] = abb[13] * z[2];
z[4] = abb[22] * (T(-5) / T(4)) + z[15] + z[16];
z[4] = abb[0] * z[4];
z[15] = 3 * abb[24];
z[16] = 4 * abb[22] + abb[23] + -z[15];
z[16] = abb[2] * z[16];
z[3] = -z[3] + z[4] + z[9] + 2 * z[10] + z[16];
z[3] = abb[12] * z[3];
z[2] = z[2] + z[3] + -z[21];
z[2] = abb[13] * z[2];
z[1] = -abb[21] + -3 * z[1];
z[1] = (T(1) / T(2)) * z[1] + z[29];
z[1] = abb[7] * z[1];
z[3] = abb[2] * abb[22];
z[4] = abb[22] * (T(5) / T(2)) + -z[15];
z[4] = -abb[23] + (T(1) / T(2)) * z[4];
z[4] = abb[0] * z[4];
z[9] = -abb[24] + abb[22] * (T(3) / T(2));
z[9] = abb[23] + (T(3) / T(2)) * z[9];
z[9] = abb[3] * z[9];
z[1] = 3 * z[1] + (T(-7) / T(2)) * z[3] + z[4] + z[9] + z[30];
z[1] = abb[12] * z[1];
z[1] = z[1] + z[21];
z[1] = abb[12] * z[1];
z[4] = z[5] + z[36];
z[4] = abb[0] * z[4];
z[9] = abb[23] + 2 * z[7];
z[9] = abb[2] * z[9];
z[4] = z[4] + z[9] + z[20] + -z[23] + z[35];
z[9] = 2 * abb[15];
z[4] = z[4] * z[9];
z[15] = 2 * abb[14];
z[16] = abb[12] * z[15];
z[15] = abb[12] + -abb[13] + z[15];
z[17] = -abb[13] * z[15];
z[15] = abb[11] + -z[15];
z[15] = abb[11] * z[15];
z[9] = z[9] + z[13] + z[15] + z[16] + z[17];
z[9] = z[9] * z[25];
z[5] = -abb[17] * z[5];
z[5] = z[5] + z[9];
z[5] = abb[9] * z[5];
z[9] = -2 * z[0] + z[12];
z[9] = abb[17] * z[9];
z[12] = z[7] * z[13];
z[9] = 2 * z[9] + 5 * z[12];
z[9] = abb[2] * z[9];
z[12] = abb[3] * abb[23];
z[13] = abb[0] * z[18];
z[12] = z[12] + -z[13];
z[13] = (T(13) / T(2)) * z[12] + -z[28];
z[13] = (T(3) / T(2)) * z[3] + (T(1) / T(3)) * z[13];
z[13] = prod_pow(m1_set::bc<T>[0], 2) * z[13];
z[1] = z[1] + z[2] + z[4] + z[5] + z[6] + z[9] + z[11] + z[13] + z[14] + 2 * z[26];
z[2] = abb[3] * z[22];
z[0] = -12 * abb[25] + 3 * z[0] + z[27];
z[4] = abb[7] * z[0];
z[2] = z[2] + -z[4];
z[4] = abb[2] * z[22];
z[0] = abb[8] * z[0];
z[5] = -z[0] + z[2] + -z[4] + -4 * z[24] + z[35];
z[5] = abb[11] * z[5];
z[6] = abb[0] * z[22];
z[6] = z[6] + z[35];
z[0] = z[0] + -z[4] + z[6] + -4 * z[10];
z[0] = abb[13] * z[0];
z[4] = -z[8] + z[12] + z[19];
z[4] = abb[14] * z[4];
z[7] = -z[7] * z[33];
z[4] = z[4] + z[7];
z[2] = -z[2] + 6 * z[3] + -z[6];
z[2] = abb[12] * z[2];
z[0] = z[0] + z[2] + 4 * z[4] + z[5];
z[0] = m1_set::bc<T>[0] * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_340_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-2.277456436656512910362641542145638089061087210817153013104016671"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("-6.99364454510109970142484226071543943313248274691742632858447464"),stof<T>("-47.246795076652142140372750836458559900495186049435789623190676203")}, std::complex<T>{stof<T>("-4.716188108444586791062200718569801344071395536100273315480457969"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("-0.545815134096350854000914120653939512086372060568231495938732518"),stof<T>("-15.748931692217380713457583612152853300165062016478596541063558734")}, std::complex<T>{stof<T>("19.290484825800509970479523859702456242299991097990254538040044168"),stof<T>("35.169822170368074575692092204959757039227685651974971615237353058")}, std::complex<T>{stof<T>("40.614215557172530308621100506176619961754014125061722987148936838"),stof<T>("50.449277870921633058293523065004929001274705561988749570043678485")}, std::complex<T>{stof<T>("2.6242329143883413464874965604269788474198853665057292635365565296"),stof<T>("3.8464923781242264506896104868557398896931942601347611475297649083")}, std::complex<T>{stof<T>("8.631089947390152263427584622677257969391598928622120816400672193"),stof<T>("94.493590153304284280745501672917119800990372098871579246381352407")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_340_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_340_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-37.973426888200936887057340445594340160322069287914561317854447397"),stof<T>("20.256453509212722516548399890832411009549451206688510546110521099")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dl[0], dlog_W3(k,dl), dl[2], dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), rlog(kend.W[194].real()/k.W[194].real())};

                    
            return f_4_340_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_340_DLogXconstant_part(base_point<T>, kend);
	value += f_4_340_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_340_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_340_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_340_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_340_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_340_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_340_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
