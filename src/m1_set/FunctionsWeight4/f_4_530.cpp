/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_530.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_530_abbreviated (const std::array<T,30>& abb) {
T z[37];
z[0] = abb[19] + abb[21];
z[1] = 2 * abb[20] + z[0];
z[2] = 2 * abb[26];
z[3] = z[1] + -z[2];
z[4] = abb[9] * z[3];
z[5] = 2 * abb[23];
z[6] = -abb[25] + z[5];
z[7] = abb[6] * z[6];
z[4] = z[4] + z[7];
z[7] = abb[22] * (T(1) / T(2));
z[8] = 2 * abb[19];
z[9] = 10 * abb[26] + abb[20] * (T(-11) / T(2)) + abb[21] * (T(-7) / T(2)) + -z[8];
z[9] = -z[7] + (T(1) / T(3)) * z[9];
z[9] = abb[8] * z[9];
z[10] = 8 * abb[26];
z[11] = -abb[19] + abb[20] * (T(-7) / T(2)) + abb[21] * (T(-5) / T(2)) + z[10];
z[11] = -z[7] + (T(1) / T(3)) * z[11];
z[11] = abb[7] * z[11];
z[12] = abb[0] * abb[24];
z[13] = (T(1) / T(2)) * z[12];
z[14] = abb[24] + abb[25];
z[15] = abb[4] * z[14];
z[16] = abb[23] + -abb[25];
z[17] = abb[1] * z[16];
z[18] = abb[23] * (T(1) / T(2));
z[19] = abb[25] * (T(2) / T(3)) + -z[18];
z[19] = abb[0] * z[19];
z[20] = abb[23] + abb[25] * (T(1) / T(3));
z[20] = abb[24] * (T(1) / T(3)) + (T(1) / T(2)) * z[20];
z[20] = abb[3] * z[20];
z[21] = abb[24] * (T(1) / T(2));
z[22] = abb[23] + abb[25] * (T(-5) / T(2)) + -z[21];
z[22] = abb[2] * z[22];
z[9] = (T(-2) / T(3)) * z[4] + z[9] + z[11] + -z[13] + -z[15] + (T(5) / T(3)) * z[17] + z[19] + z[20] + (T(1) / T(3)) * z[22];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[11] = -abb[22] + 7 * abb[26];
z[19] = 2 * z[1] + -z[11];
z[20] = 2 * abb[2];
z[19] = z[19] * z[20];
z[20] = abb[6] * z[3];
z[22] = 2 * abb[25];
z[23] = abb[23] * (T(7) / T(4)) + z[21] + -z[22];
z[24] = abb[7] * z[23];
z[11] = -4 * abb[20] + z[11];
z[25] = abb[21] * (T(7) / T(4));
z[26] = abb[19] * (T(9) / T(4)) + -z[11] + z[25];
z[27] = abb[3] * z[26];
z[1] = -2 * abb[22] + 20 * abb[26] + -7 * z[1];
z[1] = abb[10] * z[1];
z[11] = abb[19] * (T(7) / T(4)) + abb[21] * (T(9) / T(4)) + -z[11];
z[11] = abb[0] * z[11];
z[18] = abb[24] + z[18];
z[18] = abb[8] * z[18];
z[28] = abb[9] * z[6];
z[1] = z[1] + z[11] + -z[18] + z[19] + z[20] + -z[24] + z[27] + z[28];
z[11] = -abb[28] * z[1];
z[18] = abb[23] * (T(1) / T(4));
z[19] = abb[25] + z[18] + z[21];
z[19] = abb[3] * z[19];
z[20] = -abb[20] + abb[26];
z[20] = abb[19] * (T(-5) / T(4)) + 3 * z[20] + -z[25];
z[20] = abb[7] * z[20];
z[19] = z[19] + -z[20];
z[20] = 4 * abb[26];
z[24] = -abb[22] + z[20];
z[0] = abb[20] + (T(1) / T(2)) * z[0];
z[25] = -z[0] + z[24];
z[25] = abb[8] * z[25];
z[27] = -z[4] + 6 * z[17];
z[28] = abb[23] * (T(-5) / T(4)) + z[22];
z[28] = abb[0] * z[28];
z[29] = 3 * abb[25];
z[30] = 4 * abb[23] + abb[24] + -z[29];
z[30] = abb[2] * z[30];
z[28] = z[13] + z[19] + z[25] + z[27] + z[28] + z[30];
z[28] = abb[12] * z[28];
z[2] = -z[2] + z[7];
z[30] = -abb[19] + -abb[20];
z[30] = -z[2] + (T(1) / T(2)) * z[30];
z[30] = abb[8] * z[30];
z[31] = -abb[23] + abb[25] * (T(1) / T(2));
z[21] = z[21] + -z[31];
z[21] = abb[2] * z[21];
z[32] = abb[3] * abb[24];
z[31] = abb[0] * z[31];
z[21] = -z[13] + z[17] + z[21] + z[30] + z[31] + z[32];
z[21] = abb[13] * z[21];
z[30] = abb[8] * z[3];
z[31] = z[15] + z[17] + -z[30] + -z[32];
z[31] = abb[14] * z[31];
z[33] = abb[2] * abb[14];
z[34] = -z[14] * z[33];
z[31] = z[31] + z[34];
z[18] = -abb[25] + z[18];
z[18] = abb[0] * z[18];
z[34] = 2 * z[15];
z[13] = z[13] + z[18] + -z[27] + -z[34];
z[18] = -abb[19] + abb[21];
z[27] = abb[8] * (T(1) / T(2));
z[18] = z[18] * z[27];
z[27] = 3 * abb[23];
z[35] = 4 * abb[25] + -z[27];
z[35] = abb[2] * z[35];
z[18] = z[13] + -z[18] + -z[19] + z[35];
z[19] = abb[11] * z[18];
z[19] = z[19] + z[21] + z[28] + 2 * z[31];
z[19] = abb[13] * z[19];
z[21] = 2 * z[12] + z[30];
z[28] = -abb[24] + 2 * z[16];
z[28] = abb[2] * z[28];
z[6] = abb[0] * z[6];
z[31] = 4 * z[17];
z[35] = abb[3] * z[14];
z[6] = -z[4] + -z[6] + -z[21] + z[28] + z[31] + z[35];
z[28] = abb[27] * z[6];
z[36] = abb[0] * z[16];
z[4] = z[4] + z[12] + z[36];
z[3] = abb[7] * z[3];
z[3] = -z[3] + z[4];
z[22] = abb[24] + -z[22] + z[27];
z[22] = abb[2] * z[22];
z[27] = 2 * abb[24] + abb[25];
z[27] = abb[3] * z[27];
z[22] = -z[3] + z[22] + z[27] + z[30] + z[31];
z[22] = abb[15] * z[22];
z[22] = z[22] + z[28];
z[27] = abb[24] + -z[5] + z[29];
z[27] = abb[2] * z[27];
z[28] = 3 * z[17];
z[5] = abb[0] * z[5];
z[5] = z[5] + -3 * z[15] + z[21] + z[27] + -z[28] + -z[32];
z[5] = prod_pow(abb[14], 2) * z[5];
z[21] = abb[0] * (T(1) / T(2));
z[27] = abb[23] * (T(5) / T(2)) + -z[29];
z[21] = z[21] * z[27];
z[0] = abb[26] + (T(-1) / T(2)) * z[0];
z[0] = 3 * z[0] + -z[7];
z[0] = abb[7] * z[0];
z[27] = -abb[25] + abb[23] * (T(3) / T(2));
z[27] = abb[24] + (T(3) / T(2)) * z[27];
z[27] = abb[3] * z[27];
z[29] = abb[2] * abb[23];
z[0] = 3 * z[0] + -z[12] + z[21] + z[27] + -z[28] + (T(-7) / T(2)) * z[29];
z[0] = abb[12] * z[0];
z[3] = -z[3] + z[15] + 5 * z[17] + z[35];
z[3] = abb[14] * z[3];
z[12] = 3 * z[16];
z[16] = z[12] * z[33];
z[3] = z[3] + z[16];
z[3] = 2 * z[3];
z[0] = z[0] + -z[3];
z[0] = abb[12] * z[0];
z[16] = abb[3] * z[23];
z[17] = abb[7] * z[26];
z[12] = -abb[24] + z[12];
z[12] = abb[2] * z[12];
z[13] = -z[12] + z[13] + z[16] + -z[17] + z[25];
z[13] = abb[12] * z[13];
z[3] = -z[3] + z[13];
z[4] = z[4] + -z[28];
z[13] = abb[23] + z[14];
z[13] = abb[3] * z[13];
z[12] = z[12] + z[13];
z[7] = abb[19] + abb[21] * (T(3) / T(2)) + abb[20] * (T(5) / T(2)) + z[7] + -z[20];
z[7] = abb[8] * z[7];
z[13] = abb[20] + abb[21];
z[2] = z[2] + (T(1) / T(2)) * z[13];
z[2] = abb[7] * z[2];
z[2] = z[2] + z[4] + z[7] + (T(-1) / T(2)) * z[12] + z[15];
z[2] = abb[11] * z[2];
z[2] = z[2] + -z[3];
z[2] = abb[11] * z[2];
z[0] = abb[29] + z[0] + z[2] + z[5] + z[9] + z[11] + z[19] + 2 * z[22];
z[1] = -abb[17] * z[1];
z[2] = abb[16] * z[6];
z[5] = -5 * abb[20] + -3 * abb[21] + -abb[22] + -z[8] + z[10];
z[5] = abb[8] * z[5];
z[6] = -z[13] + z[24];
z[6] = abb[7] * z[6];
z[4] = -2 * z[4] + z[5] + z[6] + z[12] + -z[34];
z[4] = abb[11] * z[4];
z[5] = -abb[13] * z[18];
z[3] = z[3] + z[4] + z[5];
z[3] = m1_set::bc<T>[0] * z[3];
z[1] = abb[18] + z[1] + 2 * z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_530_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("6.010479646775139612510559319446561556854164353811704578748747264"),stof<T>("5.6984702373224524557827481551823536381457454900028397604110836716")}, std::complex<T>{stof<T>("6.486564566330480177507646135237241659703242743474451220922990576"),stof<T>("18.828734743075619357385932462691519172963203864068194456575662089")}, std::complex<T>{stof<T>("0.4760849195553405649970868157906801028490783896627466421742433117"),stof<T>("13.1302645057531669016031843075091655348174583740653546961645784174")}, std::complex<T>{stof<T>("-3.7750428937635931319690940113069686134663500614979661092813990745"),stof<T>("6.147904513184628857106431949762537542394915181439860528724860466")}, std::complex<T>{stof<T>("-16.817212403773655425461292445421290644357567786312002481339098589"),stof<T>("5.717383165172064088655906324503324244022838995635456454029552053")}, std::complex<T>{stof<T>("-15.261238408254840353349329045448463772647232007235391470231262426"),stof<T>("11.674492254097347520326813678288346820037131762306513476022259354")}, std::complex<T>{stof<T>("-10.9958162173831464797979925367896019898430508081727037042133300232"),stof<T>("-6.4663568061930289117177549575834310708879088800277819997275569531")}, std::complex<T>{stof<T>("4.838564114960299218399635898683664180695807441019447106921206648"),stof<T>("-37.272448282629505928705228311979131800147949408387776042750243487")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real()), rlog(k.W[195].real()/kbase.W[195].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_530_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_530_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 + -7 * v[0] + -v[1] + -v[2] + 3 * v[3] + 4 * v[4] + 3 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + v[5])) / prod_pow(tend, 2);
c[1] = (-3 * m1_set::bc<T>[1] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[23] + abb[24] + -abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[4];
z[0] = abb[23] + abb[24] + -abb[25];
z[0] = abb[5] * z[0];
z[1] = -abb[11] + abb[12];
z[1] = z[0] * z[1];
z[2] = abb[13] * z[0];
z[3] = -2 * z[1] + -z[2];
z[3] = abb[13] * z[3];
z[1] = z[1] + -z[2];
z[0] = abb[14] * z[0];
z[0] = 3 * z[0] + 2 * z[1];
z[0] = abb[14] * z[0];
return z[0] + z[3];
}

}
template <typename T, typename TABB> T SpDLog_f_4_530_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[23] + abb[24] + -abb[25]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = -abb[13] + abb[14];
z[1] = abb[23] + abb[24] + -abb[25];
return 2 * abb[5] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_530_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-34.70268143613298085154672409662954861069520625988234795660829406"),stof<T>("-9.261224572104762345520056696232221396711615866973965193257661264")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W29(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_15(k), f_2_2_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), rlog(kend.W[195].real()/k.W[195].real()), f_2_2_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_530_W_17_Im(t, path, abb);
abb[29] = SpDLog_f_4_530_W_17_Re(t, path, abb);

                    
            return f_4_530_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_530_DLogXconstant_part(base_point<T>, kend);
	value += f_4_530_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_530_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_530_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_530_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_530_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_530_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_530_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
