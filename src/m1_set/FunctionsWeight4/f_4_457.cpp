/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_457.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_457_abbreviated (const std::array<T,69>& abb) {
T z[99];
z[0] = abb[33] * (T(1) / T(2));
z[1] = abb[36] + z[0];
z[2] = -abb[31] + z[1];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[34] * (T(1) / T(2));
z[4] = m1_set::bc<T>[0] * z[3];
z[2] = -abb[41] + z[2] + -z[4];
z[5] = (T(1) / T(2)) * z[2];
z[6] = abb[15] * z[5];
z[7] = -abb[35] + z[0];
z[8] = m1_set::bc<T>[0] * z[7];
z[9] = -abb[43] + z[4];
z[8] = z[8] + z[9];
z[10] = (T(1) / T(2)) * z[8];
z[11] = abb[16] * z[10];
z[12] = abb[20] * abb[44];
z[13] = abb[31] + -abb[35];
z[14] = m1_set::bc<T>[0] * z[13];
z[15] = abb[9] * z[14];
z[6] = z[6] + z[11] + (T(-1) / T(4)) * z[12] + -z[15];
z[11] = abb[7] * (T(1) / T(2));
z[16] = m1_set::bc<T>[0] * z[11];
z[17] = abb[0] * m1_set::bc<T>[0];
z[16] = z[16] + -z[17];
z[16] = z[3] * z[16];
z[18] = abb[6] + -abb[8];
z[19] = abb[42] * (T(1) / T(2));
z[20] = z[18] * z[19];
z[14] = abb[41] + z[14];
z[21] = abb[6] * (T(1) / T(2));
z[14] = z[14] * z[21];
z[22] = abb[2] * abb[41];
z[16] = -z[14] + z[16] + z[20] + -z[22];
z[20] = abb[32] * (T(1) / T(2));
z[23] = z[7] + z[20];
z[24] = abb[8] * z[23];
z[25] = abb[32] + -abb[36];
z[26] = -z[0] + z[25];
z[27] = abb[35] + -z[26];
z[27] = -abb[31] + (T(1) / T(2)) * z[27];
z[27] = abb[7] * z[27];
z[27] = z[24] + z[27];
z[27] = m1_set::bc<T>[0] * z[27];
z[28] = -abb[41] + abb[42];
z[29] = -abb[31] + z[0];
z[30] = -m1_set::bc<T>[0] * z[29];
z[30] = -z[9] + z[28] + z[30];
z[31] = abb[4] * (T(1) / T(2));
z[30] = z[30] * z[31];
z[32] = -abb[41] + z[19];
z[33] = abb[43] * (T(1) / T(2));
z[34] = z[32] + -z[33];
z[35] = abb[36] * (T(1) / T(2));
z[36] = abb[32] + -abb[35];
z[37] = abb[31] + -z[35] + z[36];
z[37] = m1_set::bc<T>[0] * z[37];
z[37] = -z[4] + -z[34] + z[37];
z[37] = abb[5] * z[37];
z[38] = m1_set::bc<T>[0] * z[36];
z[39] = -z[28] + z[38];
z[39] = abb[10] * z[39];
z[40] = z[13] + z[25];
z[41] = m1_set::bc<T>[0] * z[40];
z[42] = -z[28] + z[41];
z[42] = (T(1) / T(2)) * z[42];
z[43] = abb[17] * z[42];
z[44] = abb[19] * abb[44];
z[45] = abb[18] * abb[44];
z[46] = z[44] + -z[45];
z[47] = abb[8] * abb[43];
z[46] = (T(1) / T(2)) * z[46] + -z[47];
z[48] = abb[7] * abb[43];
z[49] = abb[47] + z[46] + -z[48];
z[17] = z[0] * z[17];
z[50] = abb[13] * z[38];
z[51] = -abb[31] + abb[34];
z[52] = m1_set::bc<T>[0] * z[51];
z[52] = -abb[43] + z[52];
z[52] = abb[1] * z[52];
z[17] = -z[6] + z[16] + z[17] + z[27] + z[30] + z[37] + z[39] + -z[43] + (T(1) / T(2)) * z[49] + -z[50] + z[52];
z[17] = abb[59] * z[17];
z[27] = z[19] + z[33];
z[30] = m1_set::bc<T>[0] * z[23];
z[37] = z[27] + -z[30];
z[49] = abb[25] * z[37];
z[52] = abb[36] + z[7];
z[52] = m1_set::bc<T>[0] * z[52];
z[52] = -abb[42] + -z[4] + z[52];
z[53] = abb[24] + abb[26];
z[54] = -z[52] * z[53];
z[55] = abb[29] * (T(1) / T(2));
z[56] = abb[44] * z[55];
z[54] = z[54] + z[56];
z[56] = abb[22] * z[5];
z[42] = abb[27] * z[42];
z[10] = abb[28] * z[10];
z[10] = -z[10] + z[42] + z[49] + (T(1) / T(2)) * z[54] + -z[56];
z[42] = -abb[49] + abb[51];
z[49] = -z[10] * z[42];
z[44] = z[44] + z[45];
z[12] = -z[12] + (T(1) / T(2)) * z[44] + z[47];
z[44] = z[29] + -z[36];
z[47] = m1_set::bc<T>[0] * (T(1) / T(2));
z[44] = z[44] * z[47];
z[47] = abb[34] * (T(1) / T(4));
z[54] = m1_set::bc<T>[0] * z[47];
z[32] = z[32] + z[44] + z[54];
z[32] = abb[4] * z[32];
z[44] = abb[31] + -abb[33];
z[56] = -abb[36] + z[44];
z[56] = m1_set::bc<T>[0] * z[56];
z[56] = abb[41] + z[56];
z[57] = abb[0] * (T(1) / T(2));
z[56] = z[56] * z[57];
z[2] = abb[15] * z[2];
z[2] = z[2] + (T(1) / T(2)) * z[12] + -z[16] + -z[32] + z[56];
z[12] = z[11] * z[29];
z[12] = z[12] + z[24];
z[12] = m1_set::bc<T>[0] * z[12];
z[16] = z[12] + -z[43];
z[24] = abb[33] + abb[36];
z[32] = abb[31] + (T(-1) / T(2)) * z[24];
z[32] = m1_set::bc<T>[0] * z[32];
z[32] = z[32] + -z[34];
z[32] = abb[5] * z[32];
z[32] = -z[2] + z[16] + z[32] + z[39];
z[32] = abb[54] * z[32];
z[34] = z[7] + z[35];
z[34] = m1_set::bc<T>[0] * z[34];
z[27] = -z[27] + z[34];
z[27] = abb[5] * z[27];
z[15] = -z[15] + z[27];
z[27] = -z[28] + -z[41];
z[28] = abb[17] * (T(1) / T(2));
z[27] = z[27] * z[28];
z[2] = -z[2] + z[12] + -z[15] + z[27] + z[50];
z[2] = abb[56] * z[2];
z[12] = abb[23] * z[37];
z[27] = -z[12] * z[42];
z[2] = z[2] + z[17] + z[27] + z[32] + z[49];
z[17] = abb[31] + -abb[36];
z[17] = m1_set::bc<T>[0] * z[17];
z[17] = abb[41] + z[17];
z[27] = z[17] * z[57];
z[32] = abb[6] + abb[8];
z[34] = -z[19] * z[32];
z[29] = -z[29] + -z[36];
z[29] = m1_set::bc<T>[0] * z[29];
z[29] = abb[42] + -z[4] + z[29];
z[29] = z[29] * z[31];
z[39] = -abb[16] * z[8];
z[43] = abb[7] * z[54];
z[14] = z[14] + -z[15] + z[16] + z[27] + z[29] + z[34] + z[39] + z[43] + (T(1) / T(2)) * z[46] + -z[48];
z[15] = abb[31] * (T(1) / T(2));
z[16] = -z[15] + z[23];
z[16] = m1_set::bc<T>[0] * z[16];
z[9] = z[9] + z[16] + -z[19];
z[9] = abb[1] * z[9];
z[16] = abb[14] * z[33];
z[9] = z[9] + (T(1) / T(2)) * z[14] + z[16];
z[9] = abb[60] * z[9];
z[14] = abb[33] + -abb[35];
z[23] = z[14] + z[25];
z[27] = -abb[31] + z[23];
z[27] = abb[7] * m1_set::bc<T>[0] * z[27];
z[17] = abb[0] * z[17];
z[17] = -abb[47] + z[17] + z[27] + -z[45] + -z[48];
z[27] = -abb[41] + abb[43] + -z[38];
z[27] = z[27] * z[31];
z[29] = -abb[17] * z[41];
z[33] = -abb[5] * z[52];
z[34] = abb[7] * z[4];
z[6] = -z[6] + (T(1) / T(2)) * z[17] + -z[22] + z[27] + z[29] + z[33] + z[34] + z[50];
z[17] = abb[1] * z[37];
z[6] = (T(1) / T(2)) * z[6] + -z[17];
z[6] = abb[58] * z[6];
z[10] = z[10] + z[12];
z[12] = -abb[48] + abb[52];
z[22] = (T(1) / T(2)) * z[12];
z[22] = z[10] * z[22];
z[27] = abb[15] + abb[16];
z[29] = abb[5] * (T(1) / T(2)) + (T(-1) / T(4)) * z[27] + -z[31] + -z[57];
z[29] = abb[44] * z[29];
z[33] = abb[18] + abb[19];
z[34] = -abb[43] * z[33];
z[37] = -abb[19] * abb[41];
z[39] = -abb[7] * abb[44];
z[34] = z[34] + z[37] + z[39];
z[37] = abb[18] * z[23];
z[39] = abb[32] + z[14];
z[39] = abb[19] * z[39];
z[37] = z[37] + z[39];
z[41] = -abb[19] + abb[18] * (T(-1) / T(2));
z[41] = abb[31] * z[41];
z[37] = (T(1) / T(2)) * z[37] + z[41];
z[37] = m1_set::bc<T>[0] * z[37];
z[4] = z[4] * z[33];
z[5] = abb[20] * z[5];
z[4] = z[4] + z[5] + z[29] + (T(1) / T(2)) * z[34] + z[37];
z[5] = abb[30] * abb[44];
z[4] = (T(1) / T(2)) * z[4] + z[5];
z[4] = abb[61] * z[4];
z[5] = -z[19] + z[30];
z[5] = abb[5] * z[5];
z[19] = z[31] * z[38];
z[5] = z[5] + -z[16] + z[17] + z[19] + (T(-1) / T(2)) * z[50];
z[16] = -abb[55] * z[5];
z[5] = abb[57] * z[5];
z[2] = (T(1) / T(2)) * z[2] + z[4] + -z[5] + z[6] + z[9] + z[16] + z[22];
z[4] = -abb[50] + abb[53];
z[5] = (T(-1) / T(16)) * z[4];
z[5] = z[5] * z[10];
z[6] = abb[61] * z[8];
z[8] = abb[58] + abb[59];
z[8] = abb[60] + (T(1) / T(2)) * z[8];
z[9] = abb[44] * z[8];
z[6] = z[6] + z[9];
z[9] = abb[21] * (T(1) / T(32));
z[6] = z[6] * z[9];
z[2] = abb[66] + abb[67] + (T(1) / T(8)) * z[2] + z[5] + z[6];
z[5] = -z[15] + z[25];
z[6] = z[5] + -z[14];
z[6] = z[6] * z[15];
z[10] = abb[35] + abb[36];
z[16] = z[10] + -z[20];
z[16] = abb[32] * z[16];
z[17] = prod_pow(abb[35], 2);
z[19] = (T(1) / T(2)) * z[17];
z[22] = z[16] + -z[19];
z[29] = abb[33] + z[35];
z[30] = abb[36] * z[29];
z[34] = -abb[64] + z[30];
z[37] = -z[22] + z[34];
z[38] = abb[40] * (T(1) / T(2));
z[41] = abb[63] * (T(1) / T(2));
z[43] = z[38] + -z[41];
z[45] = abb[39] * (T(1) / T(2));
z[46] = -z[43] + z[45];
z[48] = z[3] * z[13];
z[49] = abb[38] * (T(1) / T(2));
z[50] = prod_pow(m1_set::bc<T>[0], 2);
z[52] = (T(1) / T(3)) * z[50];
z[54] = abb[37] + z[52];
z[6] = z[6] + (T(1) / T(2)) * z[37] + -z[46] + z[48] + z[49] + -z[54];
z[6] = abb[5] * z[6];
z[37] = abb[32] * abb[36];
z[56] = abb[34] * z[25];
z[58] = (T(1) / T(6)) * z[50];
z[59] = prod_pow(abb[36], 2);
z[37] = abb[38] + abb[39] + -z[37] + z[56] + -z[58] + z[59];
z[56] = abb[3] * (T(1) / T(2));
z[37] = z[37] * z[56];
z[56] = abb[34] * z[40];
z[60] = abb[36] + z[14];
z[61] = abb[32] * z[60];
z[62] = abb[36] * z[24];
z[61] = z[61] + -z[62];
z[56] = z[56] + -z[61];
z[62] = abb[33] * abb[35];
z[63] = -z[17] + z[62];
z[64] = abb[63] + z[63];
z[65] = abb[31] * z[60];
z[66] = abb[62] + z[65];
z[67] = -abb[37] + abb[38];
z[68] = z[56] + z[64] + -z[66] + z[67];
z[28] = z[28] * z[68];
z[28] = -z[28] + z[37];
z[69] = -abb[36] + z[20];
z[70] = -z[20] * z[69];
z[71] = (T(1) / T(4)) * z[44];
z[72] = abb[31] * z[71];
z[70] = abb[64] + -z[38] + -z[49] + (T(-1) / T(4)) * z[59] + z[70] + -z[72];
z[70] = abb[7] * z[70];
z[73] = z[19] + -z[62];
z[74] = abb[32] * abb[33];
z[74] = z[73] + z[74];
z[66] = -z[30] + z[54] + z[66] + z[74];
z[57] = -z[57] * z[66];
z[75] = z[15] * z[44];
z[76] = -abb[35] + z[20];
z[77] = abb[32] * z[76];
z[78] = z[19] + z[77];
z[79] = z[3] * z[44];
z[80] = (T(5) / T(6)) * z[50];
z[81] = abb[40] + z[75] + z[78] + -z[79] + -z[80];
z[81] = z[31] * z[81];
z[13] = z[13] * z[51];
z[69] = abb[32] * z[69];
z[82] = (T(1) / T(2)) * z[59];
z[83] = abb[40] + z[82];
z[69] = abb[38] + z[69] + z[83];
z[13] = abb[37] + -z[13] + z[69];
z[13] = abb[9] * z[13];
z[59] = z[17] + z[59];
z[84] = abb[40] + z[67];
z[16] = z[16] + -z[84];
z[85] = -abb[32] + z[15];
z[86] = abb[31] * z[85];
z[59] = abb[62] + -z[16] + (T(1) / T(2)) * z[59] + -z[86];
z[21] = z[21] * z[59];
z[7] = z[7] + z[15];
z[59] = abb[34] * z[7];
z[87] = abb[31] * z[7];
z[54] = -abb[64] + -z[54] + z[59] + -z[87];
z[59] = abb[16] * z[54];
z[88] = abb[0] * z[40];
z[11] = z[11] * z[44];
z[89] = z[11] + z[88];
z[89] = z[3] * z[89];
z[90] = abb[32] * z[14];
z[91] = abb[64] + z[63] + -z[90];
z[91] = abb[8] * z[91];
z[92] = abb[7] + 5 * abb[8];
z[93] = (T(1) / T(12)) * z[50];
z[94] = z[92] * z[93];
z[95] = abb[18] + -abb[19];
z[96] = abb[65] * (T(1) / T(4));
z[97] = z[95] * z[96];
z[32] = -abb[4] + z[32];
z[32] = z[32] * z[41];
z[98] = -abb[64] + z[58];
z[98] = abb[14] * z[98];
z[32] = z[6] + z[13] + -z[21] + z[28] + z[32] + z[57] + z[59] + z[70] + z[81] + z[89] + (T(1) / T(2)) * z[91] + z[94] + z[97] + z[98];
z[57] = -z[3] + z[15];
z[57] = z[14] * z[57];
z[70] = -z[20] * z[76];
z[76] = (T(1) / T(2)) * z[50];
z[81] = abb[64] + z[76];
z[57] = abb[37] * (T(1) / T(2)) + (T(-1) / T(4)) * z[17] + -z[43] + z[57] + z[70] + z[81];
z[57] = abb[1] * z[57];
z[32] = (T(1) / T(2)) * z[32] + z[57];
z[32] = abb[60] * z[32];
z[57] = -z[15] * z[33];
z[70] = abb[18] * z[25];
z[39] = z[39] + z[57] + z[70];
z[39] = abb[31] * z[39];
z[1] = z[1] + -z[15];
z[1] = abb[31] * z[1];
z[57] = abb[39] + z[52] + -z[79];
z[70] = abb[33] * abb[36];
z[1] = abb[62] + z[1] + z[57] + -z[70];
z[79] = abb[20] * z[1];
z[74] = abb[37] + z[74];
z[70] = -abb[64] + -z[70] + z[74];
z[89] = -abb[18] * z[70];
z[94] = abb[62] + abb[64] + -z[82];
z[94] = abb[19] * z[94];
z[97] = -abb[39] * z[33];
z[39] = z[39] + z[79] + z[89] + z[94] + z[97];
z[79] = abb[0] + abb[4] + -abb[5] + abb[7];
z[79] = z[79] * z[96];
z[89] = abb[19] * (T(1) / T(3)) + abb[18] * (T(1) / T(4));
z[89] = z[50] * z[89];
z[94] = -abb[19] * z[23];
z[97] = abb[18] * z[44];
z[94] = z[94] + z[97];
z[47] = z[47] * z[94];
z[27] = -abb[30] + (T(1) / T(8)) * z[27];
z[27] = abb[65] * z[27];
z[27] = z[27] + (T(1) / T(4)) * z[39] + z[47] + z[79] + z[89];
z[27] = abb[61] * z[27];
z[27] = z[27] + z[32];
z[32] = abb[33] + abb[36] * (T(3) / T(2));
z[32] = z[32] * z[35];
z[29] = abb[35] * (T(3) / T(2)) + -z[29];
z[39] = abb[31] * (T(-1) / T(4)) + -z[20] + z[29];
z[39] = abb[31] * z[39];
z[47] = abb[31] + z[25];
z[79] = abb[33] + -3 * abb[35];
z[79] = z[47] + (T(1) / T(2)) * z[79];
z[79] = abb[34] * z[79];
z[89] = -abb[62] + z[58];
z[29] = abb[32] * (T(-1) / T(4)) + z[29];
z[29] = abb[32] * z[29];
z[29] = -abb[37] + abb[64] * (T(-1) / T(2)) + (T(-5) / T(4)) * z[17] + z[29] + z[32] + z[39] + z[46] + z[49] + z[62] + z[79] + z[89];
z[29] = abb[5] * z[29];
z[32] = -z[76] + z[78];
z[39] = abb[40] + abb[62] + -abb[63] + z[32];
z[39] = abb[10] * z[39];
z[28] = -z[28] + z[39];
z[24] = -z[20] + z[24];
z[24] = abb[32] * z[24];
z[39] = abb[33] * (T(3) / T(2)) + -z[47];
z[39] = abb[31] * z[39];
z[24] = abb[39] + -3 * abb[40] + z[24] + -z[34] + z[39] + -z[67] + z[73];
z[24] = abb[7] * z[24];
z[34] = z[58] * z[92];
z[24] = -abb[68] + z[24] + z[34] + z[91];
z[0] = -z[0] + -z[36];
z[0] = abb[31] * z[0];
z[7] = z[7] + z[25];
z[25] = abb[34] * z[7];
z[0] = abb[39] + abb[62] + z[0] + z[25] + -z[78] + -z[81] + z[83];
z[0] = z[0] * z[31];
z[25] = abb[15] * z[1];
z[31] = -z[25] + z[59];
z[34] = -z[69] + z[89];
z[34] = abb[2] * z[34];
z[31] = (T(-1) / T(2)) * z[31] + z[34];
z[14] = -z[14] * z[51];
z[39] = -abb[40] + z[58];
z[20] = abb[33] + -z[20];
z[20] = abb[32] * z[20];
z[14] = abb[37] + abb[64] + z[14] + z[20] + z[39] + z[73];
z[14] = abb[1] * z[14];
z[20] = abb[32] * abb[35];
z[20] = -abb[37] + z[19] + -z[20] + -z[86];
z[20] = abb[9] * z[20];
z[18] = abb[4] + z[18];
z[18] = z[18] * z[41];
z[18] = z[18] + -z[21];
z[21] = abb[39] + -z[74] + z[82] + -z[87];
z[21] = abb[0] * z[21];
z[46] = -abb[20] + z[95];
z[46] = z[46] * z[96];
z[47] = abb[13] * z[32];
z[7] = abb[0] * z[7];
z[51] = abb[7] * z[71];
z[7] = z[7] + z[51];
z[7] = abb[34] * z[7];
z[0] = z[0] + z[7] + z[14] + -z[18] + -z[20] + z[21] + (T(1) / T(2)) * z[24] + -z[28] + z[29] + -z[31] + z[46] + z[47];
z[0] = (T(1) / T(16)) * z[0];
z[0] = abb[59] * z[0];
z[7] = z[3] * z[40];
z[14] = -abb[64] + z[17] + -z[82];
z[17] = z[15] + z[36];
z[17] = z[15] * z[17];
z[21] = abb[62] * (T(1) / T(2));
z[14] = -z[7] + (T(1) / T(2)) * z[14] + z[17] + z[21] + -z[39] + -z[45] + z[77];
z[14] = abb[4] * z[14];
z[17] = abb[31] * z[26];
z[16] = -abb[63] + -z[16] + z[17] + z[19] + z[30] + -z[57];
z[17] = abb[5] * z[16];
z[19] = -abb[0] * z[66];
z[5] = abb[31] * z[5];
z[5] = -abb[39] + z[5] + z[50] + -z[70];
z[5] = abb[7] * z[5];
z[24] = abb[18] * abb[65];
z[5] = abb[68] + z[5] + z[19] + z[24];
z[19] = -z[64] + z[90];
z[24] = abb[64] + -z[19] + z[80];
z[26] = abb[1] * z[24];
z[26] = z[26] + -z[47];
z[29] = -z[63] + z[65];
z[30] = z[29] + -z[56] + -z[84];
z[30] = abb[17] * z[30];
z[36] = abb[7] * z[44];
z[36] = z[36] + z[88];
z[36] = z[3] * z[36];
z[39] = -abb[20] * z[96];
z[5] = (T(1) / T(2)) * z[5] + z[13] + z[14] + z[17] + z[26] + z[30] + -z[31] + z[36] + z[39];
z[5] = abb[58] * z[5];
z[13] = abb[34] * z[71];
z[13] = abb[62] + z[13] + z[38] + -z[72] + (T(1) / T(2)) * z[78] + -z[93];
z[13] = abb[4] * z[13];
z[14] = z[69] + z[75];
z[14] = abb[7] * z[14];
z[17] = abb[7] + abb[8] * (T(5) / T(3));
z[17] = z[17] * z[76];
z[30] = abb[20] * abb[65];
z[14] = z[14] + -z[17] + z[30] + -z[91];
z[10] = -abb[31] + z[10];
z[10] = z[10] * z[15];
z[17] = -abb[33] + z[35];
z[17] = abb[36] * z[17];
z[30] = z[17] + -z[74];
z[10] = abb[39] + z[10] + z[21] + (T(1) / T(2)) * z[30] + z[58];
z[10] = abb[0] * z[10];
z[21] = abb[0] * z[23];
z[11] = z[11] + z[21];
z[3] = z[3] * z[11];
z[11] = z[33] * z[96];
z[3] = -z[3] + -z[10] + -z[11] + -z[13] + (T(1) / T(2)) * z[14] + z[18] + z[25] + z[34];
z[10] = -abb[39] + -abb[64] + -z[17] + z[22];
z[11] = -z[60] + z[85];
z[11] = z[11] * z[15];
z[10] = -abb[62] + (T(1) / T(2)) * z[10] + z[11] + -z[43] + z[48] + -z[49] + -z[52];
z[10] = abb[5] * z[10];
z[10] = -z[3] + z[10] + -z[28];
z[10] = abb[54] * z[10];
z[11] = -abb[62] + z[29] + z[61] + -z[67];
z[7] = -abb[40] + -z[7] + (T(1) / T(2)) * z[11] + z[41];
z[7] = abb[17] * z[7];
z[3] = -z[3] + z[6] + z[7] + -z[20] + z[37] + -z[47];
z[3] = abb[56] * z[3];
z[3] = z[3] + z[5] + z[10];
z[5] = -z[16] * z[53];
z[6] = abb[27] * z[68];
z[1] = abb[22] * z[1];
z[7] = abb[28] * z[54];
z[10] = abb[25] * z[24];
z[11] = abb[65] * z[55];
z[1] = z[1] + z[5] + -z[6] + -z[7] + z[10] + z[11];
z[5] = (T(1) / T(32)) * z[42];
z[5] = z[1] * z[5];
z[6] = abb[23] * z[24];
z[1] = z[1] + z[6];
z[4] = -z[4] + z[12];
z[4] = (T(-1) / T(32)) * z[4];
z[1] = z[1] * z[4];
z[4] = z[19] + -z[50];
z[4] = abb[5] * z[4];
z[7] = abb[4] * z[32];
z[4] = z[4] + z[7] + z[26] + z[98];
z[7] = abb[55] + abb[57];
z[7] = (T(1) / T(16)) * z[7];
z[4] = z[4] * z[7];
z[7] = -abb[61] * z[54];
z[8] = -abb[65] * z[8];
z[7] = z[7] + z[8];
z[7] = z[7] * z[9];
z[6] = z[6] * z[42];
z[0] = abb[45] + abb[46] + z[0] + z[1] + (T(1) / T(16)) * z[3] + z[4] + z[5] + (T(1) / T(32)) * z[6] + z[7] + (T(1) / T(8)) * z[27];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_457_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.06056477147887062478373321163429803283291100248108698106600679549"),stof<T>("-0.36839802510583079515909488377541018596343621481210405529255070113")}, std::complex<T>{stof<T>("0.71139208474349293419636000264885361495551924473021304202603627221"),stof<T>("-0.90547547218371322812297658047063936132169816395277400480248082064")}, std::complex<T>{stof<T>("0.22940506489961756348659178222099663827009167439590952491032968114"),stof<T>("-0.26367326036473785817908447262022279222690225105576950507208231056")}, std::complex<T>{stof<T>("0.71139208474349293419636000264885361495551924473021304202603627221"),stof<T>("-0.90547547218371322812297658047063936132169816395277400480248082064")}, std::complex<T>{stof<T>("0.28718039780616141885503641389382964468056384651594173748568444241"),stof<T>("-0.27742818221093712295040652213027622210226779218889814987547634579")}, std::complex<T>{stof<T>("-0.26656263877907991845461319281555070891546579427963225953001072141"),stof<T>("-0.07984104287839893522719076974151389202289490888349452169639869681")}, std::complex<T>{stof<T>("0.26640950161229866346961975137106693819427239990143555149996375205"),stof<T>("-0.28780252440277404925946839373067646146252866688538590623613779667")}, std::complex<T>{stof<T>("0.17335515916075297192037827901654095267976292784665177052813618989"),stof<T>("-0.4283232412825870257892029896608431146211087430042532888348288131")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_457_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_457_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(128)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[54] + abb[56] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[54] + -abb[56] + -abb[58];
z[1] = abb[34] + -abb[36];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_457_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(256)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (8 + 11 * v[0] + v[1] + -v[2] + -5 * v[3] + -4 * v[4] + -2 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + m1_set::bc<T>[1]) * (T(1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[54] + abb[56] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[34] + -abb[36];
z[1] = -abb[31] + abb[33];
z[0] = z[0] * z[1];
z[0] = abb[39] + z[0];
z[1] = abb[54] + abb[56] + abb[58];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_457_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_457_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(256)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + -4 * v[0] + 3 * v[1] + -v[2] + -3 * v[3] + v[4] + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[56] + abb[58] + abb[59]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[32] + abb[36] * (T(1) / T(2));
z[0] = abb[36] * z[0];
z[1] = prod_pow(abb[32], 2);
z[0] = abb[38] + z[0] + (T(1) / T(2)) * z[1];
z[1] = -abb[56] + -abb[58] + -abb[59];
return abb[12] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_457_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.08185470931319214302098396445149171010454903495629637998694045074"),stof<T>("0.30396595199793837434803493689148570053049865254484109109333483208")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,69> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[45] = SpDLog_f_4_457_W_17_Im(t, path, abb);
abb[46] = SpDLog_f_4_457_W_20_Im(t, path, abb);
abb[47] = SpDLogQ_W_80(k,dl,dlr).imag();
abb[66] = SpDLog_f_4_457_W_17_Re(t, path, abb);
abb[67] = SpDLog_f_4_457_W_20_Re(t, path, abb);
abb[68] = SpDLogQ_W_80(k,dl,dlr).real();

                    
            return f_4_457_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_457_DLogXconstant_part(base_point<T>, kend);
	value += f_4_457_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_457_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_457_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_457_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_457_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_457_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_457_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
