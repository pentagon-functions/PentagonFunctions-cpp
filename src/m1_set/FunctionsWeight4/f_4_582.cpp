/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_582.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_582_abbreviated (const std::array<T,32>& abb) {
T z[79];
z[0] = abb[25] + abb[27];
z[1] = abb[28] * (T(1) / T(2));
z[2] = abb[20] * (T(1) / T(2));
z[3] = 2 * abb[21];
z[4] = -abb[24] + abb[22] * (T(-5) / T(2)) + z[0] + z[1] + -z[2] + z[3];
z[4] = abb[1] * z[4];
z[5] = 4 * abb[28];
z[6] = 3 * abb[27];
z[7] = z[5] + -z[6];
z[8] = abb[20] + abb[22];
z[9] = 3 * abb[24];
z[10] = -z[7] + z[8] + z[9];
z[10] = abb[4] * z[10];
z[11] = 2 * abb[27];
z[12] = -z[3] + z[11];
z[13] = 3 * abb[28];
z[14] = -z[12] + z[13];
z[15] = -abb[25] + -z[9] + z[14];
z[16] = 2 * abb[20];
z[17] = -abb[23] + z[16];
z[18] = abb[22] + z[15] + z[17];
z[18] = abb[0] * z[18];
z[18] = -z[10] + z[18];
z[19] = -abb[21] + abb[27];
z[1] = z[1] + -z[19];
z[20] = abb[22] * (T(1) / T(2));
z[21] = -z[2] + z[20];
z[22] = -abb[23] + abb[25];
z[23] = z[1] + z[21] + z[22];
z[23] = abb[6] * z[23];
z[24] = -abb[23] + abb[25] * (T(1) / T(2));
z[25] = abb[24] * (T(1) / T(2));
z[26] = 2 * abb[26];
z[27] = -abb[20] + z[26];
z[28] = 8 * abb[22] + 2 * abb[28] + abb[27] * (T(-15) / T(2)) + abb[21] * (T(11) / T(2)) + z[24] + -z[25] + -z[27];
z[28] = abb[3] * z[28];
z[29] = 2 * abb[22];
z[30] = abb[26] + z[29];
z[31] = z[6] + z[30];
z[32] = 7 * abb[28];
z[33] = z[3] + z[32];
z[34] = 4 * abb[24];
z[35] = z[31] + -z[33] + z[34];
z[36] = abb[25] + z[16];
z[35] = abb[23] + 2 * z[35] + z[36];
z[35] = abb[9] * z[35];
z[37] = 5 * abb[21];
z[38] = 3 * abb[22];
z[39] = -abb[27] + z[32] + -z[37] + -z[38];
z[40] = abb[25] * (T(5) / T(2));
z[17] = -z[17] + (T(1) / T(2)) * z[39] + -z[40];
z[17] = abb[8] * z[17];
z[39] = abb[22] + -abb[28];
z[41] = abb[20] + z[39];
z[41] = abb[2] * z[41];
z[4] = z[4] + z[17] + z[18] + z[23] + z[28] + z[35] + (T(1) / T(2)) * z[41];
z[4] = abb[11] * z[4];
z[13] = z[3] + z[13];
z[17] = -z[6] + z[29];
z[28] = -abb[26] + z[13] + z[17];
z[35] = 2 * abb[23];
z[42] = abb[20] + abb[24] + -2 * z[28] + z[35];
z[42] = abb[3] * z[42];
z[43] = 2 * abb[24];
z[44] = -z[3] + z[43];
z[45] = abb[20] + -abb[22];
z[46] = z[44] + -z[45];
z[47] = -abb[28] + z[35];
z[48] = -z[26] + z[46] + z[47];
z[48] = abb[0] * z[48];
z[49] = -abb[23] + z[33];
z[31] = 5 * abb[24] + z[31] + -z[49];
z[50] = 2 * abb[9];
z[31] = z[31] * z[50];
z[51] = 2 * z[10];
z[52] = z[41] + -z[51];
z[42] = -z[31] + z[42] + z[48] + -z[52];
z[48] = abb[13] * z[42];
z[30] = z[30] + z[36];
z[53] = -z[6] + -z[9] + -z[30] + z[33];
z[54] = -abb[9] * z[53];
z[55] = abb[23] + z[43];
z[56] = abb[20] + z[28] + -z[55];
z[56] = abb[3] * z[56];
z[57] = -z[36] + z[55];
z[58] = abb[8] * z[57];
z[18] = z[18] + z[54] + z[56] + z[58];
z[54] = 2 * abb[10];
z[18] = z[18] * z[54];
z[4] = z[4] + z[18] + z[48];
z[4] = abb[11] * z[4];
z[18] = -abb[25] + z[20];
z[48] = abb[24] * (T(11) / T(2));
z[56] = abb[28] * (T(9) / T(2));
z[59] = 5 * abb[27];
z[3] = z[3] + -z[18] + -z[48] + z[56] + -z[59];
z[3] = abb[1] * z[3];
z[60] = -abb[24] + z[2];
z[24] = (T(1) / T(2)) * z[19] + z[24] + -z[60];
z[24] = abb[3] * z[24];
z[61] = 9 * abb[28];
z[62] = 7 * abb[27];
z[63] = abb[21] + z[61] + -z[62];
z[64] = abb[22] + -z[63];
z[40] = 3 * abb[23] + z[34] + -z[40] + (T(1) / T(2)) * z[64];
z[40] = abb[8] * z[40];
z[57] = abb[9] * z[57];
z[64] = 5 * abb[28];
z[65] = -abb[22] + z[64];
z[60] = -abb[23] + -abb[25] + z[60] + (T(1) / T(2)) * z[65];
z[60] = abb[0] * z[60];
z[3] = z[3] + -z[10] + z[23] + z[24] + z[40] + -z[57] + z[60];
z[3] = abb[12] * z[3];
z[23] = z[34] + -z[64];
z[24] = 3 * abb[20];
z[40] = 2 * abb[25];
z[60] = abb[22] + z[24] + -z[40];
z[65] = z[12] + z[23] + z[35] + -z[60];
z[65] = abb[0] * z[65];
z[66] = z[12] + z[45];
z[67] = abb[28] + z[40];
z[68] = z[35] + z[66] + -z[67];
z[69] = abb[6] * z[68];
z[70] = z[51] + z[69];
z[65] = z[65] + z[70];
z[71] = -abb[25] + z[35];
z[37] = -4 * abb[22] + -6 * abb[28] + z[27] + z[34] + -z[37] + z[62] + z[71];
z[37] = abb[3] * z[37];
z[53] = z[50] * z[53];
z[62] = 4 * abb[23];
z[72] = z[34] + z[62];
z[73] = 3 * abb[21];
z[6] = abb[22] + -z[6] + z[73];
z[74] = 5 * abb[25];
z[75] = abb[28] + z[6] + z[16] + -z[72] + z[74];
z[75] = abb[8] * z[75];
z[76] = 4 * abb[21];
z[67] = -z[43] + z[67] + z[76];
z[77] = 4 * abb[27];
z[45] = -z[45] + z[67] + -z[77];
z[45] = abb[1] * z[45];
z[37] = z[37] + -z[45] + z[53] + z[65] + z[75];
z[37] = abb[11] * z[37];
z[15] = -abb[20] + -z[15];
z[15] = abb[0] * z[15];
z[15] = z[15] + z[57] + -z[58];
z[15] = z[15] * z[54];
z[53] = abb[20] + -abb[24];
z[75] = abb[1] * z[53];
z[75] = 3 * z[75];
z[78] = -abb[10] * z[75];
z[3] = z[3] + z[15] + z[37] + z[78];
z[3] = abb[12] * z[3];
z[15] = z[16] + -z[35];
z[37] = abb[26] + z[15];
z[0] = -z[0] + -z[9] + z[13] + z[37];
z[0] = abb[0] * z[0];
z[66] = 4 * abb[25] + -z[64] + z[66];
z[66] = abb[8] * z[66];
z[52] = -z[52] + z[66];
z[28] = -abb[24] + z[28] + -z[71];
z[28] = abb[3] * z[28];
z[66] = abb[24] + abb[27];
z[30] = -8 * abb[21] + -17 * abb[28] + z[30] + z[62] + 9 * z[66];
z[30] = abb[9] * z[30];
z[0] = z[0] + z[28] + z[30] + -z[52] + -z[69];
z[0] = abb[16] * z[0];
z[28] = abb[21] + z[32];
z[30] = abb[22] + z[35];
z[32] = -z[2] + -z[28] + z[30] + z[48] + z[77];
z[32] = abb[12] * z[32];
z[14] = -7 * abb[24] + 2 * z[14] + z[24] + -z[35];
z[14] = abb[10] * z[14];
z[24] = abb[22] + -abb[27];
z[24] = -abb[23] + abb[26] + 2 * z[24];
z[48] = abb[11] * z[24];
z[14] = z[14] + z[32] + 2 * z[48];
z[14] = abb[12] * z[14];
z[23] = -abb[21] + 5 * abb[26] + -z[16] + z[23] + z[29] + -z[35];
z[23] = abb[14] * z[23];
z[32] = abb[28] * (T(5) / T(2)) + -z[26];
z[48] = -abb[21] + abb[24];
z[21] = -z[21] + z[32] + -z[48];
z[62] = abb[11] * z[21];
z[46] = 4 * abb[26] + z[46] + -z[64];
z[66] = abb[13] * z[46];
z[24] = -z[24] * z[54];
z[24] = z[24] + z[62] + z[66];
z[24] = abb[11] * z[24];
z[54] = prod_pow(abb[13], 2);
z[21] = z[21] * z[54];
z[62] = -abb[15] + -abb[16];
z[46] = z[46] * z[62];
z[62] = z[39] + z[73];
z[66] = abb[20] * (T(-5) / T(2)) + abb[24] * (T(3) / T(2)) + -z[62];
z[73] = prod_pow(abb[10], 2);
z[66] = z[66] * z[73];
z[14] = z[14] + z[21] + z[23] + z[24] + z[46] + z[66];
z[14] = abb[5] * z[14];
z[21] = 4 * abb[20];
z[13] = -6 * abb[24] + -abb[27] + 2 * z[13] + z[21] + -z[30] + -z[40];
z[13] = abb[0] * z[13];
z[9] = z[9] + z[36] + -z[49] + z[59];
z[9] = z[9] * z[50];
z[23] = abb[3] * z[68];
z[17] = -abb[20] + abb[28] + z[17];
z[17] = abb[6] * z[17];
z[13] = z[9] + z[13] + z[17] + -z[23] + -z[52];
z[17] = abb[29] * z[13];
z[23] = -z[64] + z[77];
z[15] = -abb[21] + z[15] + z[23] + -z[29] + z[74];
z[15] = abb[8] * z[15];
z[24] = abb[20] + z[29];
z[29] = -z[19] + z[24] + -z[71];
z[29] = abb[3] * z[29];
z[9] = z[9] + -z[15] + z[29] + -z[65];
z[15] = -abb[30] * z[9];
z[29] = z[8] + -z[43];
z[30] = z[11] + z[26] + -z[29] + z[40] + -z[64];
z[30] = abb[16] * z[30];
z[26] = abb[24] + z[11] + z[16] + -z[26] + z[39] + -z[76];
z[26] = abb[14] * z[26];
z[24] = abb[27] + z[24] + -z[67];
z[36] = abb[30] * z[24];
z[39] = z[53] * z[73];
z[26] = z[26] + z[30] + z[36] + (T(-3) / T(2)) * z[39];
z[26] = abb[1] * z[26];
z[30] = -abb[15] * z[42];
z[36] = abb[24] * (T(2) / T(3)) + z[20];
z[28] = abb[27] * (T(-25) / T(2)) + 2 * z[28];
z[28] = -z[21] + (T(1) / T(3)) * z[28] + -z[36];
z[28] = abb[0] * z[28];
z[33] = abb[27] * (T(29) / T(2)) + -2 * z[33];
z[39] = abb[25] * (T(2) / T(3));
z[33] = abb[20] * (T(14) / T(3)) + (T(1) / T(3)) * z[33] + z[36] + -z[39];
z[33] = abb[1] * z[33];
z[1] = z[1] + -z[18];
z[1] = (T(1) / T(3)) * z[1] + -z[2];
z[1] = abb[3] * z[1];
z[18] = abb[21] + abb[25];
z[36] = abb[22] * (T(1) / T(3)) + abb[23] * (T(2) / T(3)) + -z[56];
z[18] = abb[20] * (T(-25) / T(6)) + abb[27] * (T(-23) / T(6)) + (T(1) / T(3)) * z[18] + -z[36];
z[18] = abb[8] * z[18];
z[36] = abb[21] * (T(-2) / T(3)) + abb[20] * (T(9) / T(2)) + abb[27] * (T(25) / T(6)) + z[36] + -z[39];
z[36] = abb[6] * z[36];
z[1] = z[1] + z[18] + z[28] + z[33] + z[36] + (T(1) / T(6)) * z[41];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[18] = -abb[21] + abb[20] * (T(-3) / T(2)) + z[20] + z[22] + -z[32] + z[43];
z[18] = abb[0] * z[18];
z[20] = abb[22] + z[21];
z[7] = -z[7] + z[20];
z[7] = abb[6] * z[7];
z[7] = z[7] + -z[10];
z[10] = z[7] + z[57];
z[2] = z[2] + z[22] + -z[25];
z[2] = abb[3] * z[2];
z[2] = z[2] + z[10] + z[18] + (T(-7) / T(2)) * z[41];
z[2] = z[2] * z[54];
z[18] = z[23] + z[27] + z[35] + -z[38] + z[44];
z[18] = abb[3] * z[18];
z[21] = -abb[22] + -z[37] + z[48];
z[21] = abb[0] * z[21];
z[18] = z[18] + z[21] + -z[31] + z[51];
z[18] = abb[14] * z[18];
z[21] = 5 * abb[20] + -z[22] + -z[34] + z[62];
z[21] = abb[0] * z[21];
z[22] = -z[22] + z[53];
z[22] = abb[3] * z[22];
z[10] = -z[10] + z[21] + z[22];
z[10] = z[10] * z[73];
z[0] = abb[31] + z[0] + z[1] + z[2] + z[3] + z[4] + z[10] + z[14] + z[15] + z[17] + z[18] + z[26] + z[30];
z[1] = abb[28] + z[8] + -z[34] + z[35] + z[76] + -z[77];
z[1] = abb[0] * z[1];
z[2] = 3 * abb[25];
z[3] = z[2] + -z[20] + z[63] + -z[72];
z[3] = abb[8] * z[3];
z[4] = abb[20] + -z[19] + -z[43] + z[71];
z[4] = abb[3] * z[4];
z[10] = 8 * abb[24] + 10 * abb[27] + z[60] + -z[61] + -z[76];
z[10] = abb[1] * z[10];
z[1] = z[1] + z[3] + z[4] + z[10] + z[70];
z[1] = abb[12] * z[1];
z[2] = -z[2] + -z[6] + z[16] + z[47];
z[2] = abb[8] * z[2];
z[3] = -abb[28] + z[12] + -z[29];
z[3] = abb[0] * z[3];
z[4] = -abb[20] + abb[25] + -z[19];
z[4] = abb[3] * z[4];
z[2] = z[2] + z[3] + z[4] + z[45] + -z[69];
z[2] = abb[11] * z[2];
z[3] = z[5] + -z[11];
z[4] = -abb[23] + z[3] + -z[20] + z[48];
z[4] = abb[0] * z[4];
z[4] = z[4] + z[7] + -z[22] + z[58] + z[75];
z[4] = abb[10] * z[4];
z[3] = abb[21] + -z[3] + z[8] + z[55];
z[5] = abb[10] + -abb[12];
z[3] = abb[5] * z[3] * z[5];
z[3] = z[3] + z[4];
z[1] = z[1] + z[2] + 2 * z[3];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[1] * z[24];
z[2] = z[2] + -z[9];
z[2] = abb[18] * z[2];
z[3] = abb[17] * z[13];
z[1] = abb[19] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_582_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("35.881146515695253381641327312563300687494043260027198574684767612"),stof<T>("4.886309469514209933913693010972943679493311506814639398811404918")}, std::complex<T>{stof<T>("-10.972033612210588548632612256091115817282202888799853086264473218"),stof<T>("-23.13695730471053245266948712553648080667758614114689582377241582")}, std::complex<T>{stof<T>("3.0442278693266216177700101488709647649680337233081369702766929848"),stof<T>("1.5596022949573823873336298906927556817236796196241565626809342694")}, std::complex<T>{stof<T>("-2.8678441070247730787333879921401465499116261780913804269390278609"),stof<T>("-7.697087755560848486565495271631640603160820555536720508313897441")}, std::complex<T>{stof<T>("0.049411863693811188222818650418788052670476535424848056975744924"),stof<T>("20.927861791109241245261683745125409332974749356866331401138725")}, std::complex<T>{stof<T>("-16.900508084290603617881854206630805896274362030943143607781593844"),stof<T>("5.028924956508673002287073092664459925396764709552659096640782361")}, std::complex<T>{stof<T>("32.886330510062442952094135814111123975196486072143909661383819551"),stof<T>("24.254568965666068791841746865405597330744381244056814237269195649")}, std::complex<T>{stof<T>("-16.162206188073687873248903764211136293978531586417522596939890831"),stof<T>("-23.146008461571275694896954577131172334704005017696909388277014838")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_582_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_582_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (-2 * (8 * v[0] + 7 * v[1] + 7 * v[2] + -3 * v[3] + -7 * v[4]) + m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]))) / prod_pow(tend, 2);
c[1] = ((T(-1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (-m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + 8 * (18 + 11 * v[0] + -3 * v[1] + 6 * v[2] + 4 * v[3] + -6 * v[4] + -4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -9 * v[5]))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (-8 * v[0] + -5 * v[1] + -5 * v[2] + v[3] + 5 * v[4] + -4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) + m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]))) / prod_pow(tend, 2);
c[3] = (m1_set::bc<T>[2] * (T(3) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[4] = ((-2 + 16 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[5] = ((-1 + 2 * m1_set::bc<T>[1] + m1_set::bc<T>[2]) * (T(-3) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * ((abb[21] + -abb[23] + abb[26]) * c[0] + -abb[22] * c[1] + abb[28] * c[1] + abb[20] * c[2] + -abb[24] * (c[1] + c[2])) + abb[21] * c[3] + -abb[23] * c[3] + abb[26] * c[3] + -abb[22] * c[4] + abb[28] * c[4] + abb[24] * c[5] + -abb[20] * (c[4] + c[5]);
	}
	{
T z[5];
z[0] = -abb[21] + abb[23] + abb[24] + -abb[26];
z[1] = abb[22] + -abb[28];
z[0] = -abb[20] + 2 * z[0] + z[1];
z[2] = abb[14] + -abb[16];
z[0] = z[0] * z[2];
z[2] = 2 * abb[20];
z[3] = abb[24] * (T(3) / T(2)) + (T(7) / T(2)) * z[1] + z[2];
z[3] = prod_pow(abb[13], 2) * z[3];
z[2] = 3 * abb[24] + z[1] + -z[2];
z[4] = -abb[15] * z[2];
z[2] = abb[13] * z[2];
z[1] = -abb[24] + -z[1];
z[1] = abb[11] * z[1];
z[1] = (T(9) / T(2)) * z[1] + z[2];
z[1] = abb[11] * z[1];
z[0] = z[0] + z[1] + z[3] + z[4];
return abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_582_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_582_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-29.028260175434297806617288591558910451815494007855018526069603182"),stof<T>("-17.253459379898855285858790641318694430521501367360294628783340432")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,32> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), f_1_1(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), f_2_2_re(k), f_2_10_re(k), T{0}};
abb[19] = SpDLog_f_4_582_W_20_Im(t, path, abb);
abb[31] = SpDLog_f_4_582_W_20_Re(t, path, abb);

                    
            return f_4_582_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_582_DLogXconstant_part(base_point<T>, kend);
	value += f_4_582_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_582_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_582_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_582_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_582_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_582_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_582_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
