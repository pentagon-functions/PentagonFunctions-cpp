/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_639.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_639_abbreviated (const std::array<T,71>& abb) {
T z[157];
z[0] = abb[46] + -abb[51];
z[1] = 2 * abb[49];
z[2] = -abb[55] + z[1];
z[3] = -abb[59] + abb[47] * (T(5) / T(2)) + z[2];
z[4] = abb[53] + abb[57];
z[5] = abb[50] * (T(5) / T(6));
z[6] = -abb[58] + z[5];
z[7] = 4 * abb[54];
z[8] = abb[56] * (T(1) / T(2));
z[9] = 2 * abb[52];
z[0] = abb[45] * (T(-1) / T(6)) + abb[48] * (T(17) / T(6)) + (T(-11) / T(6)) * z[0] + (T(1) / T(3)) * z[3] + (T(-2) / T(3)) * z[4] + -z[6] + -z[7] + -z[8] + z[9];
z[0] = abb[7] * z[0];
z[3] = 2 * abb[53];
z[4] = 2 * abb[45];
z[10] = -abb[51] + abb[56];
z[11] = -abb[50] + z[10];
z[12] = z[3] + -z[4] + z[11];
z[13] = abb[15] * z[12];
z[14] = -abb[60] + -abb[61] + abb[62];
z[15] = abb[26] * z[14];
z[13] = z[13] + z[15];
z[15] = abb[51] * (T(1) / T(2));
z[16] = -z[8] + z[15];
z[17] = abb[50] * (T(1) / T(2));
z[18] = z[16] + z[17];
z[19] = abb[46] + -abb[55] + z[18];
z[19] = abb[5] * z[19];
z[20] = abb[50] + -abb[51];
z[21] = abb[56] + z[20];
z[22] = abb[14] * z[21];
z[23] = abb[21] * z[14];
z[22] = z[22] + z[23];
z[23] = abb[14] * abb[52];
z[24] = abb[14] * abb[58];
z[23] = z[23] + -z[24];
z[25] = abb[24] * z[14];
z[26] = z[23] + 2 * z[25];
z[26] = z[13] + -z[19] + z[22] + 2 * z[26];
z[27] = abb[56] + abb[58];
z[28] = abb[46] + -abb[48];
z[29] = abb[59] * (T(2) / T(3));
z[30] = abb[49] + abb[55];
z[31] = abb[50] * (T(3) / T(2));
z[27] = abb[52] + abb[57] * (T(-5) / T(2)) + (T(-1) / T(3)) * z[27] + (T(1) / T(6)) * z[28] + z[29] + (T(3) / T(2)) * z[30] + -z[31];
z[27] = abb[1] * z[27];
z[32] = 2 * abb[51];
z[33] = 2 * abb[50];
z[34] = z[32] + -z[33];
z[35] = 4 * abb[57];
z[36] = z[34] + -z[35];
z[37] = -abb[52] + abb[54];
z[38] = abb[56] * (T(2) / T(3));
z[39] = 2 * abb[59];
z[40] = -7 * abb[49] + 5 * abb[58];
z[40] = abb[46] * (T(-7) / T(3)) + -z[36] + (T(-8) / T(3)) * z[37] + z[38] + -z[39] + (T(1) / T(3)) * z[40];
z[40] = abb[9] * z[40];
z[41] = abb[46] + -abb[57];
z[42] = abb[51] * (T(2) / T(3));
z[43] = 3 * abb[54];
z[44] = abb[48] * (T(35) / T(3)) + -3 * z[30];
z[41] = abb[59] * (T(-4) / T(3)) + abb[50] * (T(13) / T(6)) + z[38] + (T(-35) / T(6)) * z[41] + -z[42] + -z[43] + (T(1) / T(2)) * z[44];
z[41] = abb[3] * z[41];
z[44] = abb[45] + -abb[47];
z[45] = z[10] + z[44];
z[46] = abb[48] * (T(3) / T(2));
z[6] = abb[52] * (T(-4) / T(3)) + abb[46] * (T(3) / T(2)) + z[6] + -z[29] + z[43] + (T(5) / T(6)) * z[45] + -z[46];
z[6] = abb[4] * z[6];
z[29] = abb[46] + abb[57];
z[45] = abb[50] * (T(1) / T(6));
z[47] = abb[49] * (T(5) / T(2));
z[48] = 8 * abb[47] + abb[53] * (T(1) / T(2)) + z[47];
z[29] = -abb[52] + abb[45] * (T(-17) / T(6)) + (T(5) / T(6)) * z[29] + -z[38] + -z[45] + (T(1) / T(3)) * z[48];
z[29] = abb[0] * z[29];
z[38] = abb[48] + abb[49];
z[48] = z[9] + z[38];
z[49] = 2 * abb[57];
z[50] = z[48] + -z[49];
z[51] = 2 * abb[54];
z[52] = z[20] + z[51];
z[53] = z[50] + -z[52];
z[54] = abb[12] * z[53];
z[55] = 4 * z[54];
z[56] = abb[48] + abb[53];
z[47] = 4 * abb[47] + abb[54] + -z[47] + (T(-5) / T(2)) * z[56];
z[56] = abb[45] + -abb[57];
z[5] = z[5] + (T(1) / T(3)) * z[47] + (T(-1) / T(2)) * z[56];
z[5] = abb[2] * z[5];
z[47] = -z[9] + z[49];
z[42] = abb[59] + abb[54] * (T(-4) / T(3)) + abb[50] * (T(-2) / T(3)) + (T(-1) / T(3)) * z[38] + z[42] + -z[47];
z[42] = abb[16] * z[42];
z[56] = abb[51] + abb[56];
z[57] = -abb[59] + z[38];
z[45] = abb[54] + abb[52] * (T(-2) / T(3)) + z[45] + (T(-1) / T(6)) * z[56] + -z[57];
z[45] = abb[6] * z[45];
z[56] = abb[23] + abb[25];
z[58] = (T(1) / T(2)) * z[14];
z[59] = z[56] * z[58];
z[60] = 8 * abb[52];
z[61] = -z[35] + z[60];
z[62] = -abb[56] + z[20];
z[63] = -z[61] + z[62];
z[63] = z[51] + (T(1) / T(3)) * z[63];
z[63] = abb[8] * z[63];
z[64] = abb[22] * z[14];
z[65] = -abb[51] + abb[57];
z[66] = abb[53] + z[65];
z[66] = abb[47] * (T(-7) / T(2)) + abb[45] * (T(29) / T(6)) + (T(-4) / T(3)) * z[66];
z[66] = abb[10] * z[66];
z[65] = abb[55] + z[65];
z[65] = -abb[48] + abb[46] * (T(8) / T(3)) + (T(-5) / T(3)) * z[65];
z[65] = abb[13] * z[65];
z[0] = z[0] + z[5] + z[6] + (T(1) / T(3)) * z[26] + z[27] + z[29] + z[40] + z[41] + z[42] + z[45] + z[55] + z[59] + 4 * z[63] + (T(4) / T(3)) * z[64] + 2 * z[65] + z[66];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[5] = 2 * abb[58];
z[6] = 3 * abb[46];
z[26] = 3 * abb[52];
z[27] = -3 * abb[49] + z[5] + -z[6] + z[26] + -z[43];
z[29] = z[21] + z[49];
z[40] = 3 * abb[59];
z[41] = z[29] + -z[40];
z[42] = -z[27] + z[41];
z[42] = abb[9] * z[42];
z[45] = z[8] + z[15];
z[63] = -z[17] + z[45];
z[65] = -z[43] + z[63];
z[66] = 4 * abb[52];
z[67] = -z[49] + z[65] + z[66];
z[67] = abb[8] * z[67];
z[68] = 3 * abb[47];
z[69] = 5 * abb[45] + -z[3] + -z[68];
z[32] = z[32] + -z[49] + z[69];
z[32] = abb[10] * z[32];
z[70] = z[16] + -z[17];
z[71] = -abb[14] * z[70];
z[72] = abb[21] * z[58];
z[71] = z[23] + z[71] + z[72];
z[59] = z[59] + z[71];
z[72] = 2 * abb[47];
z[73] = -z[4] + z[72];
z[74] = -abb[56] + z[47];
z[75] = abb[46] + abb[49];
z[76] = z[73] + z[74] + z[75];
z[77] = -abb[0] * z[76];
z[78] = -abb[47] + abb[53] + z[57];
z[79] = abb[52] + -abb[58];
z[80] = z[10] + z[78] + z[79];
z[80] = abb[7] * z[80];
z[81] = 2 * abb[46];
z[82] = -abb[58] + z[81];
z[83] = z[47] + z[82];
z[84] = abb[59] + -z[1] + -z[44] + -z[83];
z[84] = abb[4] * z[84];
z[85] = abb[50] + -abb[53];
z[86] = -z[38] + z[85];
z[87] = abb[47] + abb[54] + z[86];
z[88] = abb[2] * z[87];
z[89] = -abb[22] + -abb[24];
z[89] = z[58] * z[89];
z[90] = abb[45] + -abb[53];
z[18] = z[18] + z[90];
z[91] = abb[15] * z[18];
z[92] = abb[26] * z[58];
z[91] = -z[91] + z[92];
z[92] = abb[57] + -abb[59];
z[93] = -abb[52] + z[75];
z[94] = z[92] + z[93];
z[95] = 2 * abb[1];
z[96] = -z[94] * z[95];
z[97] = abb[59] + z[70];
z[98] = abb[54] + -z[97];
z[98] = abb[3] * z[98];
z[42] = -z[32] + z[42] + -z[59] + z[67] + z[77] + z[80] + z[84] + -z[88] + z[89] + -z[91] + z[96] + z[98];
z[42] = abb[29] * z[42];
z[77] = z[37] + z[82];
z[80] = z[1] + -z[40];
z[29] = z[29] + z[77] + z[80];
z[84] = 2 * abb[9];
z[29] = z[29] * z[84];
z[89] = -abb[48] + abb[59];
z[96] = -abb[52] + z[89];
z[96] = -z[21] + z[81] + 2 * z[96];
z[98] = abb[7] * z[96];
z[99] = -abb[59] + z[48];
z[100] = -abb[56] + z[35];
z[99] = -z[52] + 2 * z[99] + -z[100];
z[101] = abb[16] * z[99];
z[56] = z[14] * z[56];
z[14] = abb[27] * z[14];
z[102] = -z[14] + z[56];
z[103] = z[101] + -z[102];
z[104] = abb[46] + abb[48];
z[105] = z[80] + z[104];
z[47] = abb[58] + z[47] + z[105];
z[47] = z[47] * z[95];
z[23] = z[22] + 2 * z[23];
z[106] = -abb[56] + z[39];
z[107] = -z[52] + z[106];
z[108] = abb[3] * z[107];
z[98] = z[23] + -z[29] + z[47] + z[98] + -z[103] + z[108];
z[98] = abb[34] * z[98];
z[109] = 2 * z[54];
z[110] = z[13] + -z[109];
z[101] = z[14] + z[101];
z[111] = z[43] + -z[49];
z[112] = z[4] + z[111];
z[113] = -abb[47] + z[86] + z[112];
z[114] = 2 * abb[2];
z[113] = z[113] * z[114];
z[74] = abb[59] + z[74];
z[115] = z[44] + z[74];
z[116] = 2 * abb[0];
z[115] = z[115] * z[116];
z[113] = z[113] + z[115];
z[115] = 2 * abb[7];
z[117] = z[87] * z[115];
z[117] = z[101] + z[108] + z[110] + z[113] + z[117];
z[117] = abb[30] * z[117];
z[118] = 4 * z[94];
z[119] = abb[34] * z[118];
z[120] = abb[30] * z[107];
z[119] = z[119] + z[120];
z[119] = abb[4] * z[119];
z[119] = z[117] + z[119];
z[87] = z[87] * z[114];
z[91] = z[87] + z[91];
z[121] = z[59] + -z[91];
z[122] = -abb[56] + z[81];
z[123] = -abb[47] + abb[49];
z[123] = abb[45] + 2 * z[123];
z[124] = abb[52] + z[85];
z[125] = z[122] + z[123] + -z[124];
z[125] = abb[0] * z[125];
z[37] = z[37] + -z[75];
z[75] = z[20] + z[49];
z[126] = z[37] + z[75];
z[127] = z[84] * z[126];
z[125] = -z[121] + z[125] + z[127];
z[45] = z[45] + z[82];
z[82] = -abb[53] + z[9];
z[128] = z[45] + -z[82];
z[123] = z[31] + -z[123] + -z[128];
z[123] = abb[7] * z[123];
z[73] = abb[52] + abb[58] + z[73];
z[129] = z[39] + z[70] + -z[73];
z[129] = abb[4] * z[129];
z[130] = -abb[58] + abb[59] + z[28];
z[130] = z[95] * z[130];
z[123] = z[101] + z[123] + z[125] + z[129] + z[130];
z[123] = abb[33] * z[123];
z[42] = z[42] + z[98] + z[119] + z[123];
z[42] = abb[29] * z[42];
z[123] = z[9] + -z[38] + z[40];
z[129] = 3 * abb[50];
z[131] = 3 * abb[51] + -z[129];
z[132] = 8 * abb[57];
z[123] = -abb[56] + -z[51] + 2 * z[123] + z[131] + -z[132];
z[123] = abb[16] * z[123];
z[123] = z[102] + z[123];
z[80] = -z[9] + z[80];
z[133] = 4 * abb[46];
z[134] = z[35] + z[133];
z[80] = abb[56] + z[52] + 2 * z[80] + z[134];
z[80] = abb[4] * z[80];
z[135] = 6 * abb[54];
z[62] = z[62] + z[135];
z[61] = z[61] + -z[62];
z[136] = abb[8] * z[61];
z[25] = z[25] + z[64];
z[64] = -z[25] + z[136];
z[77] = z[1] + z[77];
z[75] = -z[75] + z[77];
z[75] = z[75] * z[84];
z[126] = z[115] * z[126];
z[136] = abb[14] * z[9];
z[22] = -z[22] + 2 * z[24] + -z[47] + z[64] + z[75] + -z[80] + -z[109] + -z[123] + z[126] + -z[136];
z[24] = -abb[63] * z[22];
z[47] = 5 * abb[57];
z[75] = 5 * abb[48] + z[47] + -z[133];
z[16] = abb[55] + z[16] + -z[31] + z[39] + z[43] + -z[75];
z[16] = abb[3] * z[16];
z[54] = 3 * z[54];
z[25] = -z[25] + -z[56];
z[80] = abb[50] + -abb[55];
z[126] = abb[57] + z[80] + (T(1) / T(2)) * z[104] + -z[106];
z[126] = abb[1] * z[126];
z[136] = 6 * abb[57];
z[34] = abb[59] + z[34] + z[48] + -z[136];
z[137] = abb[16] * z[34];
z[138] = 2 * abb[48];
z[111] = abb[46] + abb[55] + -abb[59] + z[10] + z[111] + -z[138];
z[111] = abb[7] * z[111];
z[139] = 4 * abb[51];
z[140] = -z[35] + z[139];
z[141] = 4 * abb[55] + -z[140];
z[142] = abb[46] * (T(-11) / T(2)) + z[46] + z[141];
z[142] = abb[13] * z[142];
z[16] = z[16] + z[19] + (T(1) / T(2)) * z[25] + -z[54] + z[67] + z[111] + z[126] + z[137] + z[142];
z[16] = abb[31] * z[16];
z[25] = z[66] + -z[136];
z[66] = 2 * abb[55];
z[67] = -z[33] + z[66];
z[111] = abb[48] + z[39];
z[6] = z[6] + -z[25] + -z[67] + -z[111];
z[6] = abb[1] * z[6];
z[126] = 2 * abb[16];
z[34] = -z[34] * z[126];
z[137] = 3 * abb[48];
z[141] = -abb[46] + -z[137] + z[141];
z[141] = abb[13] * z[141];
z[36] = -abb[56] + z[36] + z[40];
z[36] = z[36] * z[84];
z[74] = z[28] + z[74];
z[142] = z[74] * z[115];
z[143] = abb[48] + z[80];
z[92] = -z[92] + -z[143];
z[144] = 2 * abb[3];
z[92] = z[92] * z[144];
z[6] = z[6] + z[34] + z[36] + z[92] + -z[141] + z[142];
z[6] = abb[34] * z[6];
z[34] = z[11] + z[66] + -z[81];
z[66] = abb[5] * z[34];
z[92] = z[66] + z[101];
z[142] = abb[4] * z[107];
z[145] = -z[109] + z[142];
z[146] = abb[54] + -abb[57];
z[147] = z[28] + z[146];
z[148] = 4 * abb[3];
z[147] = z[147] * z[148];
z[74] = z[74] * z[95];
z[149] = -abb[49] + z[80];
z[150] = abb[54] + z[149];
z[151] = z[115] * z[150];
z[74] = z[74] + z[92] + z[145] + z[147] + z[151];
z[74] = abb[33] * z[74];
z[99] = abb[7] * z[99];
z[151] = 3 * abb[57] + -abb[59];
z[152] = abb[54] + -z[9] + z[20] + z[151];
z[153] = 4 * abb[16];
z[152] = z[152] * z[153];
z[153] = z[36] + z[64];
z[99] = z[99] + -z[108] + -z[145] + -z[152] + -z[153];
z[145] = abb[29] * z[99];
z[6] = z[6] + z[16] + z[74] + z[145];
z[6] = abb[31] * z[6];
z[16] = abb[53] + z[138];
z[74] = -abb[45] + z[72];
z[45] = -z[16] + z[17] + z[45] + z[74];
z[45] = abb[7] * z[45];
z[124] = z[74] + -z[106] + z[124];
z[124] = abb[0] * z[124];
z[145] = abb[58] + z[70];
z[154] = -abb[52] + z[145];
z[155] = abb[4] * z[154];
z[45] = z[45] + z[59] + z[91] + z[108] + -z[124] + -z[130] + z[155];
z[45] = abb[33] * z[45];
z[59] = abb[1] * z[118];
z[29] = -z[29] + z[59] + z[124];
z[74] = -abb[58] + z[74] + z[82];
z[31] = abb[56] * (T(3) / T(2)) + z[31];
z[82] = z[15] + -z[31] + z[39] + -z[74];
z[82] = abb[7] * z[82];
z[118] = z[26] + z[145];
z[124] = 4 * abb[59];
z[130] = 4 * abb[49] + -z[118] + -z[124] + z[134];
z[130] = abb[4] * z[130];
z[82] = z[29] + z[82] + -z[101] + z[121] + z[130];
z[82] = abb[29] * z[82];
z[107] = abb[7] * z[107];
z[121] = -z[57] + z[146];
z[130] = z[121] * z[148];
z[103] = -z[103] + z[107] + z[109] + -z[130];
z[107] = -abb[30] * z[103];
z[145] = 4 * abb[4];
z[148] = -z[121] * z[145];
z[103] = z[103] + z[148];
z[103] = abb[31] * z[103];
z[148] = abb[0] + -abb[1];
z[148] = z[94] * z[148];
z[155] = z[28] + -z[44];
z[156] = abb[4] * z[155];
z[44] = z[44] + z[146];
z[44] = abb[2] * z[44];
z[146] = -abb[3] * z[121];
z[146] = z[44] + z[146] + z[148] + -z[156];
z[146] = abb[32] * z[146];
z[121] = abb[30] * z[121];
z[94] = -abb[34] * z[94];
z[94] = z[94] + z[121];
z[94] = z[94] * z[145];
z[121] = z[9] + 3 * z[57];
z[62] = -z[62] + 2 * z[121];
z[145] = abb[6] * z[62];
z[148] = abb[30] + -abb[31];
z[148] = z[145] * z[148];
z[45] = z[45] + z[82] + z[94] + -z[98] + z[103] + z[107] + 2 * z[146] + z[148];
z[45] = abb[32] * z[45];
z[82] = 6 * abb[52] + z[38];
z[94] = abb[50] * (T(5) / T(2));
z[98] = -abb[59] + abb[51] * (T(-5) / T(2)) + -z[8] + z[43] + -z[82] + z[94] + z[132];
z[98] = abb[16] * z[98];
z[3] = -3 * abb[45] + abb[47] + z[3] + z[25] + -z[33] + z[39];
z[3] = abb[0] * z[3];
z[25] = -4 * abb[45] + 5 * abb[47] + z[35] + -z[43] + z[86];
z[25] = z[25] * z[114];
z[43] = z[72] + z[97] + -z[112];
z[43] = abb[7] * z[43];
z[3] = z[3] + z[25] + z[32] + z[43] + z[54] + z[98] + (T(1) / T(2)) * z[102];
z[25] = prod_pow(abb[30], 2);
z[3] = z[3] * z[25];
z[43] = z[67] + z[124];
z[46] = -abb[49] + -abb[56] + abb[46] * (T(-9) / T(2)) + z[26] + z[43] + z[46] + -z[47];
z[46] = abb[1] * z[46];
z[47] = -abb[49] + -abb[54] + -z[67] + -z[81] + z[137] + z[151];
z[47] = abb[3] * z[47];
z[37] = -z[37] + z[41];
z[37] = abb[9] * z[37];
z[41] = abb[54] + -z[49] + 2 * z[57] + z[63];
z[41] = abb[16] * z[41];
z[54] = abb[52] + -abb[59] + -z[122] + z[138] + z[149];
z[57] = abb[7] * z[54];
z[63] = abb[27] * z[58];
z[28] = abb[13] * z[28];
z[19] = -z[19] + (T(9) / T(2)) * z[28] + z[37] + z[41] + z[46] + z[47] + z[57] + z[63];
z[28] = prod_pow(abb[34], 2);
z[19] = z[19] * z[28];
z[37] = z[54] * z[95];
z[41] = -z[93] + z[143];
z[46] = -z[41] * z[115];
z[47] = -z[144] * z[150];
z[37] = z[37] + z[46] + z[47] + -z[92] + -z[127];
z[37] = abb[34] * z[37];
z[41] = -abb[1] * z[41];
z[46] = abb[47] + z[85] + -z[93];
z[46] = abb[0] * z[46];
z[47] = -abb[7] * z[155];
z[54] = abb[3] * z[150];
z[41] = z[41] + z[46] + z[47] + z[54] + -z[88] + z[156];
z[41] = abb[33] * z[41];
z[46] = -abb[34] * z[96];
z[46] = z[46] + -z[120];
z[46] = abb[4] * z[46];
z[37] = z[37] + z[41] + z[46] + -z[117];
z[37] = abb[33] * z[37];
z[41] = abb[16] * z[61];
z[14] = -z[14] + -z[41] + z[64];
z[41] = 10 * abb[57] + -z[60];
z[46] = 2 * abb[56];
z[47] = z[41] + -z[46];
z[54] = z[33] + z[47] + z[69];
z[54] = abb[0] * z[54];
z[12] = abb[7] * z[12];
z[57] = abb[45] + -4 * abb[53] + z[68] + z[140];
z[57] = abb[10] * z[57];
z[12] = z[12] + -z[13] + z[14] + -6 * z[44] + -z[54] + -z[57];
z[44] = -abb[64] * z[12];
z[54] = -abb[55] + z[137];
z[54] = z[11] + -2 * z[54] + z[133] + z[135] + -z[136];
z[54] = abb[3] * z[54];
z[57] = 5 * abb[46];
z[47] = -z[47] + -z[57] + z[67] + z[137];
z[47] = abb[1] * z[47];
z[60] = abb[7] * z[34];
z[14] = z[14] + z[47] + -z[54] + z[60] + -z[66] + z[141];
z[47] = -abb[66] * z[14];
z[38] = 2 * z[38];
z[54] = z[38] + -z[40];
z[54] = z[21] + z[35] + -z[51] + 2 * z[54];
z[60] = abb[3] + abb[4];
z[54] = z[54] * z[60];
z[53] = 2 * z[53];
z[53] = abb[7] * z[53];
z[53] = z[53] + z[54] + z[55] + z[123] + -z[153];
z[54] = abb[65] * z[53];
z[8] = z[8] + z[17];
z[17] = -abb[17] + abb[19];
z[8] = z[8] * z[17];
z[17] = abb[17] + abb[19];
z[15] = z[15] * z[17];
z[17] = abb[20] * z[18];
z[18] = abb[18] * z[154];
z[61] = -abb[58] + z[90];
z[61] = abb[17] * z[61];
z[58] = abb[28] * z[58];
z[63] = abb[19] * z[79];
z[8] = z[8] + -z[15] + -z[17] + -z[18] + z[58] + -z[61] + z[63];
z[15] = abb[68] * z[8];
z[17] = z[23] + z[56];
z[18] = abb[50] + abb[51];
z[58] = -abb[55] + -abb[58];
z[58] = abb[56] + z[18] + 2 * z[58] + z[81];
z[58] = abb[7] * z[58];
z[34] = -abb[3] * z[34];
z[5] = -abb[46] + -abb[52] + -abb[56] + z[5] + -z[80];
z[5] = z[5] * z[95];
z[5] = z[5] + z[17] + z[34] + z[58] + z[66];
z[5] = abb[36] * z[5];
z[34] = abb[67] * z[99];
z[38] = -abb[59] + z[38];
z[38] = z[21] + -z[35] + -2 * z[38] + z[135];
z[38] = -z[38] * z[60];
z[11] = -z[11] + -2 * z[78];
z[11] = abb[7] * z[11];
z[11] = z[11] + z[13] + z[38] + z[56] + z[113];
z[11] = abb[35] * z[11];
z[13] = z[65] + z[121];
z[38] = prod_pow(abb[31], 2);
z[25] = -z[25] + z[38];
z[13] = z[13] * z[25];
z[25] = -abb[35] + -abb[65];
z[25] = z[25] * z[62];
z[13] = z[13] + z[25];
z[13] = abb[6] * z[13];
z[25] = abb[52] + -z[49] + z[70] + -z[105];
z[25] = z[25] * z[28];
z[21] = -z[21] + -2 * z[79];
z[21] = abb[36] * z[21];
z[21] = z[21] + z[25];
z[21] = abb[4] * z[21];
z[25] = z[50] + -z[106];
z[28] = abb[70] * z[25];
z[0] = abb[69] + z[0] + z[3] + z[5] + z[6] + z[11] + z[13] + z[15] + z[19] + z[21] + z[24] + z[28] + z[34] + z[37] + z[42] + z[44] + z[45] + z[47] + z[54];
z[3] = z[43] + -z[46] + -z[49] + -z[104];
z[3] = abb[1] * z[3];
z[5] = 4 * abb[50] + 12 * abb[57] + -z[40] + z[51] + -z[82] + -z[139];
z[5] = z[5] * z[126];
z[6] = -z[9] + -z[30] + z[111];
z[11] = z[7] + -z[132];
z[13] = 3 * abb[56];
z[15] = -z[13] + z[18];
z[6] = 2 * z[6] + -z[11] + z[15] + -z[81];
z[6] = abb[7] * z[6];
z[18] = abb[51] + -abb[55];
z[18] = 11 * abb[46] + 8 * z[18] + -z[132] + -z[137];
z[18] = abb[13] * z[18];
z[7] = -abb[59] + -z[7] + z[75] + z[80];
z[7] = z[7] * z[144];
z[3] = z[3] + z[5] + z[6] + z[7] + z[18] + z[36] + z[55] + z[56] + z[66] + z[142] + -z[145];
z[3] = abb[31] * z[3];
z[1] = -abb[47] + z[1];
z[5] = -abb[58] + z[1];
z[6] = z[5] + -z[9];
z[4] = z[4] + 2 * z[6] + -z[52] + z[100] + z[133];
z[4] = abb[4] * z[4];
z[6] = 6 * abb[59] + -z[46] + z[131] + -z[136];
z[7] = z[6] + z[27];
z[7] = z[7] * z[84];
z[5] = -z[5] + -z[16] + -z[26] + z[39];
z[5] = 2 * z[5] + z[15] + z[35] + z[51];
z[5] = abb[7] * z[5];
z[9] = z[32] + z[108];
z[15] = z[76] * z[116];
z[4] = z[4] + z[5] + z[7] + 2 * z[9] + z[15] + z[17] + z[59] + z[87] + z[110] + z[152];
z[4] = abb[29] * z[4];
z[5] = 2 * z[101] + -z[109];
z[7] = 8 * abb[59];
z[9] = -4 * abb[48] + -8 * abb[49] + z[7] + z[11] + z[118] + -z[133];
z[9] = abb[4] * z[9];
z[11] = abb[51] * (T(3) / T(2)) + -z[51] + z[124];
z[15] = abb[56] * (T(5) / T(2)) + -z[11] + z[74] + z[94];
z[15] = abb[7] * z[15];
z[9] = z[5] + z[9] + z[15] + -z[29] + (T(-3) / T(2)) * z[56] + -z[71] + z[91] + z[130] + z[145];
z[9] = abb[32] * z[9];
z[2] = -abb[58] + -z[2];
z[2] = -abb[48] + 2 * z[2] + z[7] + -z[33] + -z[41] + -z[57];
z[2] = abb[1] * z[2];
z[7] = abb[48] + -abb[55] + -z[39];
z[7] = 2 * z[7] + z[10] + z[49] + z[51] + z[129];
z[7] = abb[3] * z[7];
z[10] = z[26] + -2 * z[89];
z[10] = 2 * z[10] + z[13] + z[20] + -z[134];
z[10] = abb[7] * z[10];
z[6] = -z[6] + z[77];
z[6] = z[6] * z[84];
z[13] = abb[56] + -16 * abb[57] + -5 * z[20] + 4 * z[48] + -z[51];
z[13] = abb[16] * z[13];
z[2] = z[2] + z[6] + z[7] + z[10] + z[13] + -z[23] + -z[102] + z[141];
z[2] = abb[34] * z[2];
z[6] = -z[83] + -z[106] + z[138];
z[6] = z[6] * z[95];
z[1] = abb[55] + z[1];
z[1] = abb[45] + abb[50] * (T(-7) / T(2)) + 2 * z[1] + -z[51] + z[128];
z[1] = abb[7] * z[1];
z[7] = -z[11] + z[31] + z[73];
z[7] = abb[4] * z[7];
z[1] = z[1] + -z[5] + z[6] + z[7] + -z[66] + -z[125] + -z[147];
z[1] = abb[33] * z[1];
z[1] = z[1] + z[2] + z[3] + z[4] + z[9] + -z[119];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[37] * z[22];
z[3] = -abb[38] * z[12];
z[4] = abb[42] * z[8];
z[5] = z[53] + -z[145];
z[5] = abb[39] * z[5];
z[6] = -abb[40] * z[14];
z[7] = abb[41] * z[99];
z[8] = abb[44] * z[25];
z[1] = abb[43] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_639_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("24.100744672951019957833951438829635161254142234583186469650513184"),stof<T>("13.91300768368572772009465068814295555155981333917183113609439933")}, std::complex<T>{stof<T>("-7.453038353474326796558556191968674926058058888595805807487136502"),stof<T>("64.04209729914023530490019461280411042555154066205072648126593664")}, std::complex<T>{stof<T>("-12.965657685201312187551673847516739906553844473329630422451910049"),stof<T>("-14.717317565851156666902452181361696643512173869038551076784389515")}, std::complex<T>{stof<T>("17.468543557532954303971307873365427474443096261051764987349787513"),stof<T>("-67.941610198319301954129935275599261908653789574060472776748485994")}, std::complex<T>{stof<T>("-9.995029773791171767176705415877449041026376800712791480717387862"),stof<T>("20.199375289806753333762580829392497428111708869531784307351473659")}, std::complex<T>{stof<T>("1.8827408479976437532709400880418589086126969562613697022593861952"),stof<T>("-9.6285066854902476379015642293687694902048801435636308776649461313")}, std::complex<T>{stof<T>("29.262881117601863291600794600545237935499014978160937005519255813"),stof<T>("-15.274691385661001291898558756037620512961438167844619665859067067")}, std::complex<T>{stof<T>("-19.491378573675435230138371031232186001404288077511675779743200558"),stof<T>("-53.761772318579331095931319256470211697622266360644677308971367916")}, std::complex<T>{stof<T>("-11.1350869877497077702822775913128952547002977612535560471986031353"),stof<T>("0.8043098821654289468078014932187410919523605298667199406899901851")}, std::complex<T>{stof<T>("-8.524530339827257308590758979917258376302264784503566825563001984"),stof<T>("104.917860599706457074633101611784395342211933030509247010433432214")}, std::complex<T>{stof<T>("-20.010534977849799274589457097274201589411414173168750660580038873"),stof<T>("24.098888188985819982992321492187648911213957781541530602834023013")}, std::complex<T>{stof<T>("0.6687458038860240805037139439018829383487291920268642821032152032"),stof<T>("4.3436871840284605831045519526160562417641996288129021997010243338")}, std::complex<T>{stof<T>("0.635768643898473000399275498646065350820234840115675302046332924"),stof<T>("-45.509903580956372324704787828645332621833108645583580913267943362")}, std::complex<T>{stof<T>("0.3770995406714526367745592174686979133639260616043059036115821508"),stof<T>("-7.0856312627672268401954367563287616384305251855206658797846966884")}, std::complex<T>{stof<T>("7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("-7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}, std::complex<T>{stof<T>("-8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("2.689250513928415495653135252175836801630141918964692483472961784")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[39])) - rlog(abs(kbase.W[39])), rlog(abs(k.W[81])) - rlog(abs(kbase.W[81])), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_639_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_639_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[2] + v[3]) * (8 + -8 * v[0] + -4 * v[1] + -3 * v[2] + v[3] + 4 * v[4] + 2 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (abb[45] + -abb[47] + abb[48] + abb[49] + -abb[59]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = -abb[29] + abb[33];
z[1] = -abb[30] + z[0];
z[1] = abb[30] * z[1];
z[0] = -abb[32] + z[0];
z[0] = abb[32] * z[0];
z[0] = -abb[35] + -z[0] + z[1];
z[1] = -abb[45] + abb[47] + -abb[48] + -abb[49] + abb[59];
return 2 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_639_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[45] + -abb[47] + abb[48] + abb[49] + -abb[59]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[45] + -abb[47] + abb[48] + abb[49] + -abb[59];
z[1] = -abb[30] + abb[32];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_639_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-14.8606844294719202977083930117954607963308343868167817279897138"),stof<T>("59.184131980752512909628491886741642060475018320814916870299090332")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18, 39, 81});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,71> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[39])) - rlog(abs(k.W[39])), rlog(abs(kend.W[81])) - rlog(abs(k.W[81])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}};
abb[43] = SpDLog_f_4_639_W_19_Im(t, path, abb);
abb[44] = SpDLogQ_W_82(k,dl,dlr).imag();
abb[69] = SpDLog_f_4_639_W_19_Re(t, path, abb);
abb[70] = SpDLogQ_W_82(k,dl,dlr).real();

                    
            return f_4_639_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_639_DLogXconstant_part(base_point<T>, kend);
	value += f_4_639_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_639_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_639_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_639_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_639_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_639_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_639_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
