/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_276.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_276_abbreviated (const std::array<T,64>& abb) {
T z[149];
z[0] = 4 * abb[54];
z[1] = abb[47] + z[0];
z[2] = abb[43] * (T(5) / T(2));
z[3] = 12 * abb[45];
z[4] = abb[53] * (T(3) / T(2));
z[5] = 6 * abb[49];
z[6] = -16 * abb[44] + 3 * abb[52] + abb[48] * (T(-11) / T(2)) + z[1] + -z[2] + -z[3] + z[4] + z[5];
z[6] = abb[4] * z[6];
z[7] = 2 * abb[54];
z[8] = -abb[43] + z[7];
z[9] = 2 * abb[48];
z[10] = z[8] + -z[9];
z[11] = 6 * abb[44];
z[12] = abb[47] + abb[52];
z[13] = 4 * abb[45];
z[14] = 2 * abb[49];
z[15] = z[10] + -z[11] + z[12] + -z[13] + z[14];
z[15] = abb[0] * z[15];
z[16] = abb[43] * (T(1) / T(2));
z[4] = z[4] + -z[16];
z[17] = 2 * abb[44];
z[18] = 2 * abb[45];
z[19] = z[17] + z[18];
z[20] = -abb[47] + abb[52];
z[21] = -z[19] + z[20];
z[22] = abb[48] * (T(1) / T(2));
z[23] = z[4] + -z[7] + -z[21] + z[22];
z[23] = abb[7] * z[23];
z[24] = 3 * abb[54];
z[25] = -abb[46] + z[24];
z[26] = abb[48] + -abb[52];
z[27] = 7 * abb[44] + z[13] + -z[25] + 2 * z[26];
z[28] = 2 * abb[9];
z[27] = z[27] * z[28];
z[29] = abb[57] + abb[58] + abb[59] + abb[60];
z[30] = (T(1) / T(2)) * z[29];
z[31] = abb[26] * z[30];
z[32] = abb[43] + abb[48];
z[33] = -abb[53] + z[32];
z[34] = (T(1) / T(2)) * z[33];
z[35] = abb[15] * z[34];
z[36] = abb[25] * z[30];
z[31] = z[31] + z[35] + z[36];
z[35] = abb[48] + -abb[53];
z[37] = -abb[43] + -z[35];
z[37] = abb[2] * z[37];
z[38] = 2 * z[37];
z[39] = z[31] + z[38];
z[40] = z[14] + -z[19];
z[41] = z[34] + -z[40];
z[42] = abb[13] * z[41];
z[43] = abb[21] * z[30];
z[44] = abb[23] * z[30];
z[42] = -z[42] + z[43] + z[44];
z[43] = abb[46] + z[40];
z[45] = abb[47] + abb[53];
z[46] = -z[7] + z[43] + z[45];
z[47] = 2 * abb[16];
z[47] = z[46] * z[47];
z[48] = 6 * abb[45];
z[11] = z[11] + z[48];
z[49] = 2 * abb[46];
z[50] = 4 * abb[49];
z[51] = z[11] + -z[49] + -z[50];
z[52] = 3 * abb[48];
z[53] = -abb[52] + -z[45] + z[52];
z[54] = 2 * abb[43];
z[55] = z[51] + z[53] + z[54];
z[56] = -abb[8] * z[55];
z[57] = 8 * abb[45];
z[58] = 10 * abb[44] + -z[50] + z[57];
z[59] = -abb[53] + z[52];
z[60] = 2 * abb[52];
z[61] = z[59] + -z[60];
z[62] = -z[8] + z[58] + z[61];
z[63] = 2 * abb[1];
z[64] = -z[62] * z[63];
z[6] = z[6] + z[15] + z[23] + z[27] + z[39] + -z[42] + -z[47] + z[56] + z[64];
z[6] = abb[32] * z[6];
z[15] = z[24] + z[60];
z[23] = abb[43] + -abb[53];
z[56] = 5 * abb[44];
z[64] = abb[48] + z[56];
z[65] = -abb[46] + -z[14] + z[15] + -z[18] + z[23] + -z[64];
z[65] = abb[9] * z[65];
z[66] = abb[1] * z[62];
z[46] = abb[16] * z[46];
z[67] = 8 * abb[44];
z[68] = z[48] + z[67];
z[69] = z[14] + z[60];
z[70] = -abb[53] + -abb[54] + z[9] + z[68] + -z[69];
z[70] = abb[4] * z[70];
z[71] = -abb[49] + z[19];
z[72] = -z[12] + z[35];
z[73] = abb[46] + -abb[54];
z[72] = z[71] + (T(1) / T(2)) * z[72] + -z[73];
z[72] = abb[8] * z[72];
z[74] = abb[22] + abb[24];
z[74] = z[30] * z[74];
z[75] = -abb[54] + z[32];
z[76] = -abb[7] * z[75];
z[77] = abb[44] + -abb[47];
z[78] = abb[0] * z[77];
z[31] = z[31] + z[37] + z[42] + z[46] + z[65] + z[66] + z[70] + z[72] + z[74] + z[76] + z[78];
z[31] = abb[29] * z[31];
z[65] = 4 * abb[44];
z[70] = z[13] + z[65];
z[72] = -z[14] + -z[20] + z[70];
z[74] = abb[48] * (T(3) / T(2));
z[76] = abb[53] * (T(1) / T(2));
z[78] = -z[7] + z[16] + z[72] + z[74] + z[76];
z[78] = abb[4] * z[78];
z[79] = abb[44] + abb[47] + z[18];
z[80] = -z[14] + z[75] + z[79];
z[80] = z[63] * z[80];
z[81] = 2 * z[77];
z[82] = abb[11] * z[81];
z[80] = z[80] + z[82];
z[42] = z[42] + z[80];
z[4] = -z[4] + -z[21] + z[74];
z[4] = abb[7] * z[4];
z[83] = -z[14] + z[20];
z[84] = abb[43] + z[83];
z[84] = abb[0] * z[84];
z[85] = z[21] + -z[35];
z[86] = -abb[8] * z[85];
z[4] = z[4] + -z[39] + z[42] + z[78] + -z[84] + z[86];
z[4] = abb[33] * z[4];
z[78] = z[10] + z[21];
z[86] = abb[8] * z[78];
z[87] = abb[22] * z[29];
z[88] = abb[24] * z[29];
z[87] = z[87] + z[88];
z[89] = -abb[53] + z[7];
z[90] = -abb[48] + z[21] + z[89];
z[90] = abb[0] * z[90];
z[89] = -z[32] + z[89];
z[91] = abb[4] * z[89];
z[92] = abb[7] * z[33];
z[93] = -z[91] + 2 * z[92];
z[94] = abb[26] * z[29];
z[95] = abb[15] * z[33];
z[96] = z[94] + z[95];
z[97] = 4 * z[37] + z[96];
z[98] = abb[25] * z[29];
z[86] = z[86] + z[87] + z[90] + -z[93] + z[97] + z[98];
z[86] = abb[34] * z[86];
z[4] = z[4] + z[6] + z[31] + -z[86];
z[4] = abb[29] * z[4];
z[6] = -abb[49] + z[16];
z[31] = abb[47] * (T(1) / T(2));
z[90] = z[6] + -z[31];
z[99] = abb[52] * (T(1) / T(2));
z[100] = 3 * abb[44];
z[101] = z[18] + z[35] + z[90] + -z[99] + z[100];
z[101] = abb[0] * z[101];
z[102] = abb[43] * (T(3) / T(2));
z[58] = abb[48] * (T(7) / T(2)) + -z[15] + z[58] + -z[76] + z[102];
z[58] = abb[4] * z[58];
z[103] = 3 * abb[49];
z[73] = abb[43] + -abb[51] + 2 * z[35] + z[70] + -z[73] + -z[103];
z[73] = abb[8] * z[73];
z[61] = abb[43] + -z[14] + z[61];
z[104] = 9 * abb[44];
z[25] = z[25] + -z[48] + -z[61] + -z[104];
z[25] = abb[9] * z[25];
z[48] = 2 * abb[51];
z[79] = z[48] + -z[60] + z[79];
z[105] = abb[11] * z[79];
z[106] = -abb[53] + abb[54];
z[107] = abb[7] * z[106];
z[108] = z[46] + z[107];
z[109] = 11 * abb[44] + 10 * abb[45] + abb[47] + 4 * abb[48] + -z[5] + z[54];
z[60] = -z[0] + -z[60] + z[109];
z[60] = abb[1] * z[60];
z[110] = -abb[53] + z[20];
z[111] = -z[52] + z[110];
z[8] = z[8] + -z[71] + (T(1) / T(2)) * z[111];
z[8] = abb[14] * z[8];
z[71] = abb[6] * abb[51];
z[44] = z[44] + z[71];
z[71] = z[12] + z[35];
z[111] = abb[6] * z[71];
z[8] = z[8] + z[25] + z[44] + z[58] + z[60] + z[73] + z[101] + -z[105] + z[108] + (T(-1) / T(2)) * z[111];
z[8] = abb[30] * z[8];
z[25] = 2 * abb[53];
z[58] = -z[9] + z[25];
z[51] = abb[43] + z[7] + -z[12] + z[51] + -z[58];
z[60] = -abb[8] * z[51];
z[73] = z[47] + z[87];
z[101] = abb[7] * z[89];
z[112] = z[73] + z[101];
z[113] = -z[33] + z[50];
z[114] = -z[70] + z[113];
z[114] = abb[13] * z[114];
z[115] = abb[21] * z[29];
z[116] = z[114] + z[115];
z[117] = z[112] + z[116];
z[65] = -abb[47] + z[65];
z[118] = -abb[52] + z[18];
z[119] = z[35] + z[65] + z[118];
z[119] = abb[0] * z[119];
z[15] = -abb[53] + -z[15] + z[109];
z[15] = z[15] * z[63];
z[109] = 2 * abb[4];
z[62] = z[62] * z[109];
z[29] = abb[23] * z[29];
z[15] = z[15] + -z[27] + z[29] + -z[60] + z[62] + z[82] + z[117] + z[119];
z[27] = abb[29] + -abb[32];
z[15] = -z[15] * z[27];
z[60] = -z[19] + z[48];
z[62] = 2 * abb[47];
z[82] = z[60] + -z[62];
z[119] = abb[43] + z[52];
z[120] = 3 * abb[53];
z[121] = z[82] + -z[119] + z[120];
z[121] = abb[8] * z[121];
z[85] = abb[0] * z[85];
z[122] = z[63] * z[106];
z[123] = abb[6] * z[48];
z[123] = -z[111] + z[123];
z[124] = z[29] + z[123];
z[93] = z[85] + -z[93] + z[121] + z[122] + -z[124];
z[93] = abb[33] * z[93];
z[121] = abb[32] + -abb[33];
z[125] = -z[78] * z[121];
z[126] = -abb[29] * z[78];
z[126] = -z[125] + z[126];
z[126] = abb[14] * z[126];
z[8] = z[8] + z[15] + z[93] + z[126];
z[8] = abb[30] * z[8];
z[15] = -abb[50] + z[22] + z[76];
z[126] = -abb[52] + z[49];
z[127] = abb[44] + abb[45];
z[128] = z[7] + -z[15] + z[16] + -z[62] + -z[103] + -z[126] + z[127];
z[128] = abb[8] * z[128];
z[129] = 5 * abb[53];
z[130] = 6 * abb[54];
z[131] = z[129] + -z[130];
z[69] = 4 * abb[50] + -z[11] + -z[49] + z[69] + -z[119] + -z[131];
z[69] = abb[9] * z[69];
z[69] = z[37] + z[69];
z[132] = abb[45] + -abb[49];
z[1] = -abb[50] + abb[53] * (T(7) / T(2)) + -z[1] + z[56] + z[74] + z[102] + 5 * z[132];
z[1] = abb[14] * z[1];
z[51] = abb[4] * z[51];
z[74] = -z[33] + z[40];
z[132] = 2 * abb[50];
z[133] = -z[49] + z[132];
z[134] = z[74] + -z[133];
z[135] = abb[10] * z[134];
z[136] = abb[7] * z[78];
z[6] = abb[45] + z[6] + z[15] + z[77];
z[6] = abb[0] * z[6];
z[15] = 3 * abb[45];
z[77] = -abb[49] + -abb[50] + z[15] + z[26] + z[100];
z[77] = z[63] * z[77];
z[1] = z[1] + z[6] + z[51] + z[69] + z[73] + z[77] + z[128] + z[135] + z[136];
z[1] = abb[39] * z[1];
z[6] = 4 * abb[52];
z[51] = z[6] + z[130];
z[77] = 5 * abb[48];
z[3] = 18 * abb[44] + z[3] + z[23] + z[49] + -z[50] + -z[51] + z[77];
z[3] = abb[4] * z[3];
z[23] = -z[12] + z[57] + z[67] + -z[130];
z[5] = z[5] + -z[129] + z[132];
z[52] = z[5] + -z[23] + -z[52] + -z[54];
z[52] = abb[14] * z[52];
z[57] = z[6] + -z[13];
z[67] = -z[50] + z[54];
z[100] = 7 * abb[53];
z[9] = -13 * abb[44] + -5 * abb[50] + -z[9] + z[57] + z[67] + z[100];
z[9] = abb[9] * z[9];
z[128] = 3 * abb[43];
z[136] = z[120] + -z[128];
z[137] = 7 * abb[48];
z[51] = 24 * abb[44] + 18 * abb[45] + -10 * abb[49] + -z[51] + z[132] + -z[136] + z[137];
z[51] = abb[1] * z[51];
z[138] = -abb[50] + z[25];
z[64] = -abb[47] + z[64] + z[118] + -z[138];
z[64] = abb[0] * z[64];
z[61] = z[11] + z[61] + -z[132];
z[61] = abb[8] * z[61];
z[3] = z[3] + z[9] + z[29] + z[51] + z[52] + z[61] + z[64] + z[116] + -z[135];
z[3] = abb[36] * z[3];
z[9] = abb[19] + -abb[20];
z[51] = 3 * abb[17];
z[61] = abb[18] + z[9] + z[51];
z[64] = abb[33] * (T(1) / T(2));
z[61] = z[61] * z[64];
z[118] = 3 * abb[18];
z[9] = -abb[17] + z[9] + -z[118];
z[135] = abb[32] * (T(1) / T(2));
z[9] = z[9] * z[135];
z[139] = abb[19] + abb[20];
z[140] = abb[29] * z[139];
z[141] = abb[18] + abb[20];
z[142] = abb[34] * z[141];
z[9] = -z[9] + z[61] + (T(1) / T(2)) * z[140] + -z[142];
z[9] = abb[29] * z[9];
z[61] = abb[17] + abb[19];
z[61] = z[27] * z[61];
z[140] = abb[18] * abb[30];
z[143] = abb[18] * abb[33];
z[61] = z[61] + (T(1) / T(2)) * z[140] + -z[143];
z[61] = abb[30] * z[61];
z[140] = abb[17] + abb[18];
z[144] = abb[29] * z[140];
z[145] = abb[17] * abb[32];
z[144] = z[143] + -z[144] + z[145];
z[144] = abb[31] * z[144];
z[51] = z[51] + z[118] + z[139];
z[64] = z[51] * z[64];
z[118] = abb[17] * abb[34];
z[118] = z[64] + -z[118] + z[145];
z[118] = abb[32] * z[118];
z[142] = z[142] + -z[143];
z[142] = abb[33] * z[142];
z[139] = z[139] + z[140];
z[143] = prod_pow(m1_set::bc<T>[0], 2);
z[145] = (T(1) / T(3)) * z[143];
z[139] = z[139] * z[145];
z[145] = prod_pow(abb[34], 2);
z[146] = -abb[62] + (T(1) / T(2)) * z[145];
z[146] = abb[17] * z[146];
z[147] = abb[17] + -abb[20];
z[147] = abb[35] * z[147];
z[148] = -abb[18] + abb[19];
z[148] = abb[36] * z[148];
z[141] = abb[62] * z[141];
z[9] = -z[9] + z[61] + z[118] + -z[139] + -z[141] + -z[142] + z[144] + z[146] + z[147] + -z[148];
z[61] = -abb[55] * z[9];
z[118] = -abb[4] + -abb[7] + abb[13] + abb[15];
z[118] = -abb[8] + -2 * abb[28] + (T(1) / T(2)) * z[118];
z[139] = abb[63] * z[118];
z[9] = -z[9] + z[139];
z[9] = abb[56] * z[9];
z[139] = z[110] + z[137];
z[14] = -abb[43] + -z[14] + z[24] + (T(-1) / T(2)) * z[139];
z[14] = abb[5] * z[14];
z[139] = abb[43] + z[103];
z[141] = 4 * z[35] + z[139];
z[141] = abb[10] * z[141];
z[14] = z[14] + -z[36] + z[141];
z[36] = abb[47] + abb[54] + z[40] + -z[138];
z[36] = abb[14] * z[36];
z[36] = z[36] + -z[108];
z[108] = -abb[48] + -z[12] + z[129];
z[138] = abb[46] + -abb[50];
z[108] = abb[51] + -z[7] + (T(1) / T(2)) * z[108] + -z[138];
z[108] = abb[8] * z[108];
z[88] = -z[88] + z[111];
z[111] = -abb[0] * z[138];
z[142] = -abb[22] * z[30];
z[44] = z[14] + -z[36] + -z[44] + (T(1) / T(2)) * z[88] + z[108] + z[111] + z[122] + z[142];
z[44] = abb[31] * z[44];
z[88] = z[54] + z[137];
z[108] = -z[50] + -z[88] + -z[110] + z[130];
z[110] = abb[5] * z[108];
z[111] = -z[98] + z[110];
z[101] = z[47] + z[101] + z[111];
z[0] = z[0] + z[62];
z[11] = z[0] + -z[11];
z[49] = -9 * abb[48] + z[11] + z[49] + z[136];
z[49] = abb[8] * z[49];
z[59] = -z[7] + z[59] + z[128];
z[122] = z[59] * z[109];
z[55] = abb[0] * z[55];
z[49] = z[49] + -z[55] + -z[101] + -z[122];
z[55] = abb[32] * z[49];
z[122] = z[78] + z[133];
z[122] = abb[8] * z[122];
z[112] = z[91] + z[112] + z[122];
z[122] = abb[0] * z[74];
z[130] = 3 * z[106] + -z[138];
z[136] = z[28] * z[130];
z[136] = z[112] + z[122] + z[136];
z[136] = abb[29] * z[136];
z[5] = z[5] + z[11] + -z[32];
z[11] = -abb[29] * z[5];
z[11] = z[11] + z[125];
z[11] = abb[14] * z[11];
z[137] = -z[106] + z[138];
z[137] = abb[8] * z[137];
z[130] = -abb[9] * z[130];
z[106] = abb[1] * z[106];
z[36] = z[36] + z[106] + z[130] + z[137];
z[36] = abb[30] * z[36];
z[11] = z[11] + 2 * z[36] + z[44] + z[55] + -z[93] + z[136];
z[11] = abb[31] * z[11];
z[36] = -abb[4] * z[108];
z[23] = 11 * abb[48] + z[23] + -z[120] + -z[132] + z[139];
z[23] = abb[8] * z[23];
z[44] = -abb[0] * z[134];
z[54] = -z[35] + z[54] + -z[103];
z[54] = abb[10] * z[54];
z[23] = z[23] + z[36] + z[44] + z[52] + z[54] + -z[69] + -z[87] + z[111];
z[23] = abb[38] * z[23];
z[36] = 5 * abb[47];
z[18] = 4 * abb[43] + z[18] + z[36] + z[48] + -z[50] + -z[56] + -z[58];
z[18] = abb[1] * z[18];
z[13] = z[13] + z[17] + z[62] + -z[113];
z[13] = abb[4] * z[13];
z[17] = -4 * abb[51] + z[36] + z[57] + -z[104];
z[17] = abb[11] * z[17];
z[36] = z[35] + z[82] + z[128];
z[36] = abb[7] * z[36];
z[44] = -z[48] + z[71];
z[50] = abb[8] * z[44];
z[52] = abb[0] * z[81];
z[13] = -z[13] + z[17] + -z[18] + z[36] + z[50] + -z[52] + -z[87] + -z[116] + z[123];
z[17] = abb[55] + abb[56];
z[18] = abb[19] + z[140];
z[36] = z[17] * z[18];
z[36] = -z[13] + z[36];
z[36] = abb[61] * z[36];
z[45] = -z[7] + -3 * z[45] + z[70] + z[88] + -z[126];
z[45] = abb[8] * z[45];
z[52] = 5 * abb[43] + -z[7] + z[77] + -z[120];
z[52] = abb[4] * z[52];
z[54] = abb[7] * z[59];
z[55] = -abb[47] + abb[54] + z[33] + -z[43];
z[56] = 2 * abb[0];
z[55] = z[55] * z[56];
z[45] = z[45] + z[52] + -z[54] + z[55] + z[73] + z[97] + z[110];
z[52] = abb[62] * z[45];
z[54] = z[40] + z[89];
z[54] = abb[0] * z[54];
z[55] = -z[22] + z[76];
z[56] = -z[55] + z[102];
z[57] = z[21] + z[56];
z[57] = abb[7] * z[57];
z[55] = z[16] + z[55];
z[58] = -z[55] + z[72];
z[58] = abb[4] * z[58];
z[62] = abb[8] * z[89];
z[39] = z[39] + z[42] + z[54] + -z[57] + z[58] + z[62] + z[87];
z[42] = -abb[33] * z[39];
z[49] = -abb[34] * z[49];
z[53] = z[7] + -z[53] + -z[67] + -z[68];
z[53] = abb[0] * z[53];
z[15] = z[15] + z[65] + -z[128];
z[15] = z[15] * z[109];
z[54] = -abb[8] * z[59];
z[57] = abb[43] + z[21];
z[58] = abb[7] * z[57];
z[15] = z[15] + -z[37] + z[53] + z[54] + z[58] + z[66];
z[15] = abb[32] * z[15];
z[15] = z[15] + z[42] + z[49];
z[15] = abb[32] * z[15];
z[42] = -abb[44] + abb[51];
z[49] = abb[45] * (T(1) / T(2));
z[42] = -z[16] + -z[31] + -z[35] + (T(1) / T(2)) * z[42] + -z[49];
z[42] = abb[7] * z[42];
z[29] = z[29] + (T(55) / T(4)) * z[37] + z[42] + z[94] + z[95] + z[98] + z[114] + z[115];
z[42] = abb[54] * (T(11) / T(3));
z[53] = abb[43] * (T(1) / T(4));
z[54] = abb[47] * (T(1) / T(3)) + abb[48] * (T(5) / T(6)) + abb[53] * (T(13) / T(2));
z[54] = abb[49] * (T(-5) / T(6)) + abb[51] * (T(-1) / T(6)) + -z[42] + z[53] + (T(1) / T(2)) * z[54] + z[127];
z[54] = abb[1] * z[54];
z[2] = z[2] + -z[31] + z[35];
z[2] = abb[44] + abb[49] * (T(-4) / T(3)) + abb[45] * (T(5) / T(6)) + (T(1) / T(3)) * z[2];
z[2] = abb[4] * z[2];
z[58] = abb[47] + abb[53] * (T(-47) / T(2));
z[22] = -abb[49] + z[22] + (T(1) / T(3)) * z[58];
z[42] = z[42] + z[53];
z[22] = abb[44] * (T(1) / T(3)) + (T(1) / T(2)) * z[22] + z[42] + z[49];
z[22] = abb[0] * z[22];
z[49] = abb[48] + abb[53] * (T(-47) / T(3));
z[42] = z[42] + (T(1) / T(4)) * z[49];
z[42] = abb[8] * z[42];
z[6] = -z[6] + -z[31];
z[31] = abb[45] + abb[51];
z[6] = abb[44] * (T(3) / T(2)) + (T(1) / T(3)) * z[6] + (T(4) / T(3)) * z[31];
z[6] = abb[11] * z[6];
z[2] = z[2] + z[6] + z[22] + (T(1) / T(3)) * z[29] + z[42] + z[54] + (T(2) / T(3)) * z[87];
z[2] = z[2] * z[143];
z[6] = -z[33] + -z[79];
z[6] = abb[1] * z[6];
z[22] = abb[4] * z[57];
z[29] = 2 * abb[7];
z[31] = abb[43] + -abb[47] + abb[51] + -z[127];
z[29] = z[29] * z[31];
z[31] = abb[8] * z[33];
z[6] = z[6] + z[22] + z[29] + z[31] + z[37] + -z[85] + z[105];
z[6] = abb[33] * z[6];
z[6] = z[6] + z[86];
z[6] = abb[33] * z[6];
z[22] = -abb[7] * z[44];
z[29] = abb[51] + -abb[52];
z[31] = -abb[49] + -z[29];
z[31] = z[31] * z[63];
z[33] = -z[35] + z[83];
z[42] = -abb[14] * z[33];
z[22] = z[22] + z[31] + z[42] + -z[50] + -z[87] + -z[124];
z[22] = abb[37] * z[22];
z[25] = -z[7] + z[25] + z[90] + z[99] + z[138];
z[25] = abb[0] * z[25];
z[31] = -abb[48] + z[120];
z[16] = -abb[54] + -z[16] + (T(1) / T(2)) * z[31];
z[16] = abb[7] * z[16];
z[31] = abb[8] * z[138];
z[14] = -z[14] + z[16] + z[25] + z[31] + -z[38] + -z[46];
z[14] = z[14] * z[145];
z[16] = z[38] + z[84] + -z[92] + z[96] + z[98];
z[16] = abb[35] * z[16];
z[25] = abb[34] * z[78];
z[25] = z[25] + z[125];
z[25] = abb[29] * z[25];
z[31] = -abb[34] * z[125];
z[26] = 3 * abb[47] + z[26] + -z[129];
z[19] = abb[50] + abb[54] + -z[19] + (T(1) / T(2)) * z[26] + z[103];
z[19] = z[19] * z[145];
z[26] = -abb[35] * z[33];
z[19] = z[19] + z[25] + z[26] + z[31];
z[19] = abb[14] * z[19];
z[21] = z[21] + z[55];
z[21] = abb[17] * z[21];
z[25] = z[56] + z[83];
z[25] = abb[18] * z[25];
z[26] = abb[19] * z[41];
z[30] = abb[27] * z[30];
z[31] = abb[20] * z[34];
z[21] = z[21] + z[25] + -z[26] + z[30] + z[31];
z[25] = abb[55] * z[118];
z[25] = z[21] + z[25];
z[25] = abb[63] * z[25];
z[26] = -abb[29] * z[33];
z[20] = z[20] + -z[35];
z[20] = -abb[49] + (T(1) / T(2)) * z[20];
z[30] = abb[31] * z[20];
z[26] = z[26] + z[30];
z[26] = abb[31] * z[26];
z[30] = abb[35] + abb[36] + abb[37] + abb[38] + -abb[39];
z[30] = z[30] * z[33];
z[20] = prod_pow(abb[29], 2) * z[20];
z[20] = z[20] + z[26] + z[30];
z[20] = abb[3] * z[20];
z[26] = abb[33] * abb[34];
z[30] = abb[32] * z[121];
z[31] = abb[32] + -abb[34];
z[31] = abb[29] * z[31];
z[26] = abb[62] + z[26] + z[30] + z[31] + -z[145];
z[26] = 2 * z[26] + (T(11) / T(3)) * z[143];
z[26] = abb[12] * z[26] * z[75];
z[1] = z[1] + z[2] + z[3] + z[4] + z[6] + z[8] + z[9] + z[11] + z[14] + z[15] + z[16] + z[19] + z[20] + z[22] + z[23] + z[25] + z[26] + z[36] + z[52] + z[61];
z[2] = -abb[32] + -abb[33];
z[2] = z[2] * z[39];
z[3] = z[7] + z[32] + -z[40] + -z[120];
z[3] = abb[0] * z[3];
z[4] = abb[50] + -4 * abb[53] + z[24] + z[32] + -z[43];
z[4] = z[4] * z[28];
z[6] = -z[40] + z[75];
z[6] = abb[4] * z[6];
z[6] = z[6] + z[37] + -z[107];
z[7] = z[74] + z[133];
z[8] = -abb[8] * z[7];
z[3] = z[3] + -z[4] + 2 * z[6] + z[8] + -z[47] + z[80];
z[3] = abb[29] * z[3];
z[6] = z[40] + z[133];
z[8] = z[6] + -z[32] + -z[131];
z[8] = abb[0] * z[8];
z[9] = 2 * z[141];
z[8] = z[8] + z[9] + 8 * z[37] + z[96] + z[110] + z[112];
z[8] = abb[34] * z[8];
z[2] = z[2] + z[3] + z[8];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = -abb[40] * z[13];
z[6] = z[6] + z[10] + -z[12] + z[48];
z[6] = abb[8] * z[6];
z[8] = z[29] + z[127];
z[8] = abb[11] * z[8];
z[4] = z[4] + z[6] + 4 * z[8] + z[91] + z[117] + z[122] + -z[123];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = abb[14] * m1_set::bc<T>[0] * z[5];
z[4] = z[4] + -z[5];
z[4] = abb[30] * z[4];
z[6] = abb[41] * z[45];
z[0] = z[0] + -z[60] + -z[100] + z[119] + -z[133];
z[0] = abb[8] * z[0];
z[7] = -abb[0] * z[7];
z[0] = z[0] + z[7] + -z[9] + -z[91] + -z[101] + -6 * z[106] + z[124];
z[0] = m1_set::bc<T>[0] * z[0];
z[0] = z[0] + z[5];
z[0] = abb[31] * z[0];
z[7] = z[51] * z[135];
z[8] = abb[31] * z[140];
z[9] = abb[30] * z[18];
z[10] = abb[20] + z[140];
z[11] = abb[34] * z[10];
z[7] = z[7] + z[8] + -z[9] + -z[11] + z[64];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = abb[40] * z[18];
z[9] = abb[41] * z[10];
z[10] = abb[42] * z[118];
z[7] = z[7] + -z[8] + -z[9] + -z[10];
z[7] = -z[7] * z[17];
z[8] = abb[42] * z[21];
z[9] = abb[29] + -abb[34];
z[5] = z[5] * z[9];
z[9] = -abb[33] + 3 * abb[34] + z[27];
z[9] = m1_set::bc<T>[0] * z[9];
z[9] = abb[41] + z[9];
z[9] = abb[12] * z[9] * z[75];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + 2 * z[9];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_276_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.532805985296568558258419582201622705850006466615638653357974654"),stof<T>("28.062415966599068695583676699101002397539170265371126850982119055")}, std::complex<T>{stof<T>("14.521533137108452989133859689177581377840013971004840683827273705"),stof<T>("-10.964153963967064157749916248213591287272846733613794447531158382")}, std::complex<T>{stof<T>("7.1044137164848294515638606157421341960408399853239137232976637826"),stof<T>("-12.2068311953779058544154175351423179859362509158387705284279457599")}, std::complex<T>{stof<T>("2.8086132976916173322474317168312804008339989297102314482516180838"),stof<T>("-3.2494473301418394669434347166192070939473150011550730659251689265")}, std::complex<T>{stof<T>("-29.928959247206150111938180291725598953215112998830371588933144372"),stof<T>("-6.651296587080674320478201944218313237344574262775211036558367176")}, std::complex<T>{stof<T>("17.880196613735127873012373618527738439751851497586539174579738578"),stof<T>("11.481363581537996290343389604120085170446407177043150858113338545")}, std::complex<T>{stof<T>("3.0873289172921446051811979918010191262682311057066737835607784944"),stof<T>("5.8635027194198829203424317539818023897318912362420431099472343309")}, std::complex<T>{stof<T>("-4.6925936352181035204994613969195293736786346234932926727690137825"),stof<T>("7.3463101351597655110405205590701614752726809313370293032535760193")}, std::complex<T>{stof<T>("31.472211494394455128107256109418940386978529379426070389522817104"),stof<T>("19.743206069790808139077087813560942550894939419463439767378055971")}, std::complex<T>{stof<T>("-10.844352005338414741991104571217037588407226060059686985636678353"),stof<T>("-10.23772390910304947116730131382040163088840340873124857438806908")}, std::complex<T>{stof<T>("8.3075321699393547237441151517580690522593785452514223463901759753"),stof<T>("6.115686216971509290644860914921608122984118174113652718945276974")}, std::complex<T>{stof<T>("-24.303748446147996408504459090197558519166594349054900296452518855"),stof<T>("-21.693912603527431625085336361492647674755891281338759814387022612")}, std::complex<T>{stof<T>("-7.7143564516654021477305372678719799821591283604003080270983573661"),stof<T>("-7.2516632689748497845188292938738884372507784963632055609905540863")}, std::complex<T>{stof<T>("-7.7143564516654021477305372678719799821591283604003080270983573661"),stof<T>("-7.2516632689748497845188292938738884372507784963632055609905540863")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[72].real()/kbase.W[72].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[124].real()/kbase.W[124].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_276_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_276_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-4.554954773351524679618479847812886412248660606826008523361109936"),stof<T>("-59.421895605625464127334067227880406569468653425495146929522397508")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,64> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W18(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W73(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[72].real()/k.W[72].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[124].real()/k.W[124].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_276_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_276_DLogXconstant_part(base_point<T>, kend);
	value += f_4_276_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_276_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_276_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_276_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_276_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_276_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_276_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
