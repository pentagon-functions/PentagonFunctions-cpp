/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_45.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_45_abbreviated (const std::array<T,28>& abb) {
T z[45];
z[0] = abb[18] + abb[19];
z[1] = 2 * abb[20] + z[0];
z[2] = 2 * abb[25];
z[3] = z[1] + -z[2];
z[4] = -abb[6] * z[3];
z[5] = 2 * abb[24];
z[6] = abb[22] * (T(1) / T(2));
z[7] = abb[23] * (T(7) / T(4)) + z[5] + z[6];
z[8] = abb[8] * z[7];
z[9] = 7 * abb[19];
z[10] = 9 * abb[18] + z[9];
z[11] = 7 * abb[25];
z[12] = 4 * abb[20] + -z[11];
z[10] = (T(1) / T(4)) * z[10] + z[12];
z[13] = -abb[2] * z[10];
z[11] = -2 * z[1] + z[11];
z[14] = 2 * abb[3];
z[11] = z[11] * z[14];
z[15] = -7 * abb[18] + -9 * abb[19];
z[12] = -z[12] + (T(1) / T(4)) * z[15];
z[12] = abb[0] * z[12];
z[15] = 2 * abb[23];
z[16] = abb[24] + z[15];
z[17] = abb[9] * z[16];
z[18] = -20 * abb[25] + 7 * z[1];
z[18] = abb[10] * z[18];
z[19] = abb[23] * (T(1) / T(2));
z[20] = abb[22] + z[19];
z[20] = abb[7] * z[20];
z[14] = -abb[0] + -abb[2] + 2 * abb[10] + -z[14];
z[14] = abb[21] * z[14];
z[4] = z[4] + z[8] + z[11] + z[12] + z[13] + z[14] + z[17] + z[18] + z[20];
z[4] = abb[15] * z[4];
z[8] = abb[9] * z[3];
z[11] = abb[6] * z[16];
z[12] = z[8] + -z[11];
z[13] = abb[23] * (T(1) / T(4)) + z[6];
z[14] = abb[24] + z[13];
z[14] = abb[0] * z[14];
z[14] = -z[12] + z[14];
z[0] = abb[20] + (T(1) / T(2)) * z[0];
z[17] = 4 * abb[25];
z[18] = z[0] + -z[17];
z[18] = abb[7] * z[18];
z[10] = abb[8] * z[10];
z[7] = -abb[2] * z[7];
z[20] = abb[23] + abb[24];
z[21] = 3 * z[20];
z[22] = -abb[22] + z[21];
z[22] = abb[3] * z[22];
z[23] = abb[22] + -abb[24];
z[24] = abb[4] * z[23];
z[25] = 2 * z[24];
z[26] = abb[1] * z[20];
z[27] = 6 * z[26];
z[7] = z[7] + z[10] + -z[14] + z[18] + z[22] + z[25] + z[27];
z[7] = abb[11] * z[7];
z[10] = -abb[20] + abb[25];
z[9] = 5 * abb[18] + z[9];
z[9] = (T(1) / T(4)) * z[9] + -3 * z[10];
z[9] = abb[8] * z[9];
z[10] = -abb[24] + z[13];
z[10] = abb[2] * z[10];
z[9] = z[9] + z[10];
z[10] = abb[22] + z[20];
z[13] = abb[5] * z[10];
z[28] = 2 * z[13];
z[29] = -z[27] + z[28];
z[30] = abb[23] * (T(-5) / T(4)) + -z[5] + z[6];
z[30] = abb[0] * z[30];
z[31] = 3 * abb[24];
z[32] = abb[22] + 4 * abb[23] + z[31];
z[32] = abb[3] * z[32];
z[18] = z[9] + z[12] + -z[18] + -z[29] + z[30] + z[32];
z[18] = abb[13] * z[18];
z[30] = abb[23] * (T(5) / T(2));
z[31] = z[30] + z[31];
z[31] = -abb[22] + (T(1) / T(2)) * z[31];
z[31] = abb[0] * z[31];
z[32] = abb[8] * abb[21];
z[33] = 3 * z[26];
z[0] = abb[25] + (T(-1) / T(2)) * z[0];
z[0] = abb[8] * z[0];
z[34] = abb[24] + abb[23] * (T(3) / T(2));
z[34] = abb[22] + (T(3) / T(2)) * z[34];
z[34] = abb[2] * z[34];
z[35] = abb[3] * abb[23];
z[0] = 9 * z[0] + z[31] + (T(-3) / T(2)) * z[32] + -z[33] + z[34] + (T(-7) / T(2)) * z[35];
z[0] = abb[14] * z[0];
z[31] = abb[7] + abb[8];
z[34] = abb[11] * z[31];
z[35] = abb[7] * abb[13];
z[34] = z[34] + -z[35];
z[34] = abb[21] * z[34];
z[0] = z[0] + z[7] + z[18] + z[34];
z[0] = abb[14] * z[0];
z[7] = abb[8] * z[3];
z[7] = z[7] + z[8];
z[18] = abb[2] * z[23];
z[35] = z[11] + -z[18];
z[15] = abb[24] * (T(-3) / T(2)) + -z[6] + -z[15];
z[15] = abb[3] * z[15];
z[36] = abb[20] * (T(5) / T(2)) + -z[17];
z[37] = -abb[19] + abb[18] * (T(-3) / T(2)) + -z[36];
z[37] = abb[7] * z[37];
z[38] = abb[22] + abb[24];
z[38] = abb[0] * z[38];
z[15] = -z[7] + -z[13] + z[15] + -z[33] + z[35] + z[37] + (T(1) / T(2)) * z[38];
z[15] = abb[13] * z[15];
z[37] = 3 * abb[23];
z[38] = 4 * abb[24] + z[37];
z[38] = abb[3] * z[38];
z[39] = -z[29] + z[38];
z[40] = abb[18] + -abb[19];
z[40] = abb[7] * z[40];
z[9] = -z[9] + z[14] + -z[39] + (T(1) / T(2)) * z[40];
z[9] = abb[11] * z[9];
z[9] = z[9] + z[15];
z[9] = abb[13] * z[9];
z[10] = abb[0] * z[10];
z[14] = z[10] + z[11];
z[15] = z[7] + -z[14];
z[21] = abb[3] * z[21];
z[21] = -z[13] + z[15] + z[18] + z[21] + 5 * z[26];
z[40] = abb[11] + abb[13];
z[40] = z[21] * z[40];
z[21] = z[21] + z[24];
z[41] = -abb[14] * z[21];
z[42] = abb[11] * z[23];
z[43] = abb[13] * z[23];
z[44] = z[42] + z[43];
z[44] = abb[4] * z[44];
z[40] = z[40] + z[41] + z[44];
z[7] = z[7] + -z[11];
z[11] = z[13] + -z[24];
z[13] = 3 * abb[22];
z[24] = abb[24] + z[13] + z[37];
z[24] = abb[0] * z[24];
z[13] = abb[24] + -z[13];
z[13] = abb[2] * z[13];
z[41] = abb[3] * z[20];
z[13] = -z[7] + 3 * z[11] + z[13] + z[24] + -7 * z[26] + -5 * z[41];
z[13] = abb[12] * z[13];
z[13] = z[13] + 2 * z[40];
z[13] = abb[12] * z[13];
z[24] = 2 * abb[22];
z[16] = z[16] + z[24];
z[16] = abb[0] * z[16];
z[3] = abb[7] * z[3];
z[20] = -abb[22] + 2 * z[20];
z[20] = abb[3] * z[20];
z[40] = 4 * z[26];
z[16] = z[3] + -z[8] + z[16] + -z[20] + z[35] + -z[40];
z[20] = -abb[26] * z[16];
z[5] = abb[22] + z[5] + z[37];
z[5] = abb[3] * z[5];
z[35] = -abb[24] + z[24];
z[35] = abb[2] * z[35];
z[3] = z[3] + z[5] + z[15] + z[35] + z[40];
z[5] = abb[27] * z[3];
z[5] = z[5] + z[20];
z[15] = -abb[23] + -z[23];
z[15] = abb[2] * z[15];
z[15] = -z[15] + z[22];
z[20] = abb[19] + abb[20];
z[2] = -z[2] + (T(1) / T(2)) * z[20];
z[2] = abb[8] * z[2];
z[20] = abb[18] + abb[19] * (T(3) / T(2)) + z[36];
z[20] = abb[7] * z[20];
z[2] = z[2] + -z[8] + z[14] + (T(-1) / T(2)) * z[15] + z[20] + -z[33];
z[8] = prod_pow(abb[11], 2);
z[2] = z[2] * z[8];
z[14] = -abb[24] + -z[6] + -z[19];
z[14] = abb[0] * z[14];
z[6] = -abb[24] + z[6];
z[6] = abb[2] * z[6];
z[15] = -abb[24] + z[30];
z[15] = abb[3] * z[15];
z[6] = z[6] + z[7] + z[14] + z[15] + z[26];
z[6] = (T(1) / T(3)) * z[6] + z[11];
z[6] = prod_pow(m1_set::bc<T>[0], 2) * z[6];
z[11] = z[8] * z[23];
z[14] = abb[11] * z[43];
z[11] = z[11] + -2 * z[14];
z[11] = abb[4] * z[11];
z[8] = z[8] * z[31];
z[14] = -abb[7] * prod_pow(abb[13], 2);
z[8] = z[8] + z[14];
z[8] = abb[21] * z[8];
z[0] = z[0] + z[2] + z[4] + 2 * z[5] + z[6] + (T(1) / T(2)) * z[8] + z[9] + z[11] + z[13];
z[2] = z[7] + z[18];
z[4] = -z[1] + z[17];
z[5] = 3 * abb[7];
z[4] = z[4] * z[5];
z[5] = -abb[23] + z[24];
z[5] = abb[0] * z[5];
z[2] = 2 * z[2] + -z[4] + -z[5] + z[27] + z[28] + z[38];
z[2] = abb[13] * z[2];
z[6] = z[24] + z[37];
z[6] = abb[2] * z[6];
z[1] = 8 * abb[25] + -z[1];
z[1] = abb[8] * z[1];
z[1] = z[1] + z[6] + 2 * z[12];
z[6] = abb[3] * abb[24];
z[5] = -z[1] + z[5] + -6 * z[6] + -z[25] + z[29] + 2 * z[32];
z[5] = abb[14] * z[5];
z[6] = abb[12] * z[21];
z[7] = -z[42] + z[43];
z[7] = abb[4] * z[7];
z[6] = z[6] + -z[7] + z[34];
z[1] = z[1] + z[4] + -2 * z[10] + z[39];
z[1] = abb[11] * z[1];
z[1] = z[1] + z[2] + z[5] + -2 * z[6];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[16];
z[3] = abb[17] * z[3];
z[2] = z[2] + z[3];
z[1] = z[1] + 2 * z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_45_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-2.865328904619474081715472944552233878587323223191098412945701433"),stof<T>("-14.790524887528732158800596048134999895213462446584107193403461358")}, std::complex<T>{stof<T>("-3.734022653538348242028241987397446692859445569303201745468032835"),stof<T>("-14.790524887528732158800596048134999895213462446584107193403461358")}, std::complex<T>{stof<T>("-6.599351558157822323743714931949680571446768792494300158413734267"),stof<T>("-29.581049775057464317601192096269999790426924893168214386806922717")}, std::complex<T>{stof<T>("2.594260711642048586772666146503961369389999897773802443769487273"),stof<T>("-15.748931692217380713457583612152853300165062016478596541063558734")}, std::complex<T>{stof<T>("1.0971135466683091891187512411462399373839832322364616740792512508"),stof<T>("7.9585319918548376887258114424961864931164541537070773867302026508")}, std::complex<T>{stof<T>("-1.934594866684836173867324753321539370544038517578929764484496968"),stof<T>("40.342577113335883091145395800531439507234566718507787312691828735")}, std::complex<T>{stof<T>("0.0595399138788505968446909095678073730289804768815229191453087243"),stof<T>("4.3291837065732082306033280995678105702672268970487427456535828116")}, std::complex<T>{stof<T>("-1.183430576768323436574283507562203536723230900827107172894727553"),stof<T>("76.82784485170960645797394293272855969092211094260400400999759892")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real()), rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_45_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_45_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.921371099984550542952098330826017914883143617414096310320626613"),stof<T>("-6.3872498296452749588194783838788117516790378356316577496163908322")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), rlog(kend.W[194].real()/k.W[194].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_45_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_45_DLogXconstant_part(base_point<T>, kend);
	value += f_4_45_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_45_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_45_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_45_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_45_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_45_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_45_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
