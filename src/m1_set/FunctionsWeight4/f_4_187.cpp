/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_187.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_187_abbreviated (const std::array<T,26>& abb) {
T z[41];
z[0] = 2 * abb[20];
z[1] = 2 * abb[24];
z[2] = z[0] + z[1];
z[3] = 2 * abb[23];
z[4] = abb[22] + z[3];
z[5] = -abb[21] + z[4];
z[6] = 2 * abb[25];
z[7] = z[2] + z[5] + -z[6];
z[8] = abb[6] * z[7];
z[6] = 2 * abb[19] + -z[6];
z[5] = z[1] + -z[5] + z[6];
z[5] = abb[3] * z[5];
z[9] = -abb[19] + abb[25];
z[10] = -abb[24] + z[9];
z[11] = -abb[1] * z[10];
z[8] = z[5] + z[8] + 2 * z[11];
z[12] = -z[1] + z[9];
z[13] = 2 * abb[21];
z[14] = z[4] + -z[13];
z[15] = z[12] + -z[14];
z[16] = abb[8] * z[15];
z[17] = abb[19] * (T(1) / T(2));
z[18] = abb[23] + abb[22] * (T(1) / T(2));
z[19] = z[17] + z[18];
z[20] = -abb[24] + z[13];
z[21] = abb[25] * (T(1) / T(2));
z[22] = -abb[20] + z[21];
z[23] = z[19] + -z[20] + z[22];
z[23] = abb[0] * z[23];
z[24] = -abb[24] + z[18];
z[25] = -z[17] + z[24];
z[26] = -abb[20] + -abb[21] + abb[25] * (T(3) / T(2)) + z[25];
z[26] = abb[9] * z[26];
z[3] = abb[19] + -abb[22] + -z[3];
z[0] = -abb[25] + z[0];
z[27] = z[0] + -z[3];
z[27] = abb[2] * z[27];
z[28] = abb[21] + -abb[25];
z[29] = abb[20] + z[28];
z[30] = abb[7] * z[29];
z[31] = 2 * z[30];
z[28] = abb[19] + z[28];
z[32] = abb[5] * z[28];
z[23] = -z[8] + -z[16] + z[23] + -3 * z[26] + z[27] + -z[31] + z[32];
z[23] = abb[18] * z[23];
z[3] = -3 * abb[25] + z[2] + z[3] + z[13];
z[3] = abb[9] * z[3];
z[5] = -z[3] + z[5];
z[13] = z[5] + -z[27];
z[33] = abb[20] + -abb[25];
z[34] = 2 * z[4] + -z[20] + z[33];
z[34] = abb[6] * z[34];
z[7] = abb[4] * z[7];
z[20] = z[20] + z[33];
z[20] = abb[0] * z[20];
z[7] = z[7] + z[13] + z[16] + z[20] + z[34];
z[7] = abb[14] * z[7];
z[15] = abb[0] * z[15];
z[16] = -abb[21] + z[18];
z[20] = abb[24] * (T(-5) / T(2)) + -z[6] + -z[16];
z[20] = abb[8] * z[20];
z[8] = -z[3] + z[8] + z[15] + z[20];
z[8] = abb[16] * z[8];
z[15] = abb[24] * (T(1) / T(2));
z[20] = z[15] + -z[18] + z[28];
z[20] = abb[8] * z[20];
z[34] = z[11] + -z[32];
z[17] = z[17] + z[22];
z[22] = -z[17] + -z[24];
z[22] = abb[0] * z[22];
z[22] = z[20] + z[22] + z[26] + -z[34];
z[35] = prod_pow(abb[11], 2);
z[22] = z[22] * z[35];
z[19] = z[19] + -z[21];
z[19] = abb[0] * z[19];
z[36] = abb[24] + -z[4] + z[29];
z[36] = abb[6] * z[36];
z[26] = z[19] + z[26] + -z[27] + z[30] + z[36];
z[37] = prod_pow(abb[13], 2);
z[26] = z[26] * z[37];
z[38] = abb[24] * (T(3) / T(2));
z[0] = -abb[19] + z[0];
z[16] = z[0] + z[16] + z[38];
z[16] = abb[16] * z[16];
z[39] = abb[19] + abb[25];
z[15] = abb[20] + z[15] + (T(-1) / T(2)) * z[39];
z[15] = z[15] * z[35];
z[17] = z[17] + -z[18];
z[35] = -abb[21] + abb[24];
z[40] = z[17] + -z[35];
z[40] = abb[18] * z[40];
z[21] = -z[21] + -z[25];
z[21] = z[21] * z[37];
z[25] = -abb[15] * z[28];
z[15] = z[15] + z[16] + z[21] + z[25] + z[40];
z[15] = abb[4] * z[15];
z[16] = abb[21] * (T(1) / T(2));
z[21] = z[9] + -z[16] + z[24];
z[21] = abb[3] * z[21];
z[18] = -z[16] + z[18];
z[18] = abb[4] * z[18];
z[24] = abb[0] * z[29];
z[18] = z[18] + -z[21] + z[24] + -z[32] + -z[36];
z[18] = prod_pow(abb[10], 2) * z[18];
z[1] = -abb[21] + z[1] + z[33];
z[1] = abb[4] * z[1];
z[1] = z[1] + z[13] + 2 * z[24] + z[30] + -z[32];
z[1] = abb[17] * z[1];
z[13] = -z[16] + z[33] + z[38];
z[13] = abb[4] * z[13];
z[16] = -abb[0] * z[35];
z[13] = z[13] + z[16] + -z[20] + -z[21] + -z[30];
z[13] = abb[12] * z[13];
z[16] = z[0] + -z[14];
z[16] = abb[0] * z[16];
z[5] = z[5] + z[16];
z[2] = -z[2] + z[39];
z[16] = abb[21] + z[2];
z[16] = abb[4] * z[16];
z[16] = -z[5] + z[16];
z[16] = abb[10] * z[16];
z[13] = z[13] + z[16];
z[13] = abb[12] * z[13];
z[16] = 2 * z[34];
z[5] = z[5] + z[16] + z[31];
z[5] = abb[15] * z[5];
z[10] = abb[4] * z[10];
z[11] = z[10] + z[11];
z[17] = -abb[2] * z[17];
z[11] = (T(1) / T(2)) * z[11] + z[17] + -z[19];
z[11] = prod_pow(m1_set::bc<T>[0], 2) * z[11];
z[1] = z[1] + z[5] + z[7] + z[8] + (T(13) / T(6)) * z[11] + z[13] + z[15] + z[18] + z[22] + z[23] + z[26];
z[5] = -z[3] + z[31];
z[7] = z[4] + -z[9];
z[7] = abb[0] * z[7];
z[8] = 2 * z[36];
z[9] = -z[5] + -z[7] + -z[8] + 2 * z[27];
z[9] = abb[13] * z[9];
z[2] = z[2] + z[4];
z[11] = -abb[4] * z[2];
z[7] = -z[3] + -z[7] + z[8] + z[11] + 2 * z[32];
z[7] = abb[10] * z[7];
z[6] = -abb[24] + -z[6] + z[14];
z[6] = abb[8] * z[6];
z[2] = abb[0] * z[2];
z[2] = z[2] + z[6];
z[3] = z[2] + z[3] + z[16];
z[3] = abb[11] * z[3];
z[2] = -z[2] + z[5] + z[10];
z[2] = abb[12] * z[2];
z[4] = z[4] + z[12];
z[4] = abb[13] * z[4];
z[0] = -abb[24] + -z[0];
z[0] = abb[11] * z[0];
z[0] = z[0] + z[4];
z[0] = abb[4] * z[0];
z[0] = z[0] + z[2] + z[3] + z[7] + z[9];
z[0] = m1_set::bc<T>[0] * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_187_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-27.830626197982790328442925969079509796881321986096423340745505647"),stof<T>("21.953183378149330137424044300428716563149188661277619788268575774")}, std::complex<T>{stof<T>("30.881072697329204939134332225860000158723687388701896272151845205"),stof<T>("-21.299943681563369475671197538577460840907780099102469624081810496")}, std::complex<T>{stof<T>("-3.7313809460722080516539854009503700522837990681127403974030751188"),stof<T>("4.1250878519596201870296527229978940872860288119180415307706102621")}, std::complex<T>{stof<T>("9.0723770334290745344526005001593936310991679624567022960840469107"),stof<T>("9.8278456990236430996060110436943777568148724621960446689979796176")}, std::complex<T>{stof<T>("18.144754066858149068905201000318787262198335924913404592168093821"),stof<T>("19.655691398047286199212022087388755513629744924392089337995959235")}, std::complex<T>{stof<T>("-2.2905495880104518721072088424285332169730034917384889672746322345"),stof<T>("-13.2996938543973026248828170048410161218594927119389360355818246018")}, std::complex<T>{stof<T>("-3.0504464993464146106914062567804903618423654026054729314063395575"),stof<T>("-0.6532396965859606617528467618512557222414085621751501641867652778")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_187_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_187_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("37.392217451112904462920421444333446357456152956173547483246768031"),stof<T>("-11.584923961873986619372793114977582805021510236920141269325279203")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), f_1_1(k), f_1_3(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real())};

                    
            return f_4_187_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_187_DLogXconstant_part(base_point<T>, kend);
	value += f_4_187_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_187_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_187_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_187_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_187_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_187_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_187_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
