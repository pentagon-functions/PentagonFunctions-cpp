/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_573.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_573_abbreviated (const std::array<T,33>& abb) {
T z[15];
z[0] = -abb[4] + -abb[8] + 2 * abb[9];
z[1] = prod_pow(abb[11], 2);
z[2] = -abb[24] + abb[29];
z[0] = z[0] * z[1] * z[2];
z[3] = -abb[4] + abb[5];
z[4] = abb[6] + z[3];
z[4] = z[2] * z[4];
z[5] = -abb[25] + abb[29];
z[6] = abb[20] + -abb[26] + z[5];
z[6] = abb[0] * z[6];
z[4] = z[4] + -z[6];
z[4] = abb[10] * z[4];
z[7] = abb[9] * z[2];
z[8] = abb[4] * z[2];
z[7] = z[7] + -z[8];
z[9] = 2 * abb[11];
z[9] = z[7] * z[9];
z[10] = -z[4] + z[9];
z[10] = abb[10] * z[10];
z[3] = abb[8] + z[3];
z[3] = z[2] * z[3];
z[5] = abb[21] + -abb[28] + z[5];
z[5] = abb[1] * z[5];
z[3] = -z[3] + z[5];
z[11] = -abb[12] * z[3];
z[11] = -z[9] + z[11];
z[11] = abb[12] * z[11];
z[12] = -abb[27] + abb[29];
z[13] = -abb[22] + abb[26] + -z[12];
z[13] = abb[2] * z[13];
z[14] = abb[6] * z[2];
z[8] = -z[8] + z[13] + z[14];
z[8] = abb[13] * z[8];
z[8] = z[8] + -z[9];
z[8] = abb[13] * z[8];
z[9] = -abb[14] + abb[30] + -abb[31];
z[9] = z[7] * z[9];
z[2] = -abb[8] * z[2];
z[2] = z[2] + z[14];
z[14] = prod_pow(m1_set::bc<T>[0], 2);
z[2] = z[2] * z[14];
z[0] = z[0] + z[2] + z[8] + 2 * z[9] + z[10] + z[11];
z[2] = 13 * z[5] + -13 * z[6] + -z[13];
z[2] = z[2] * z[14];
z[5] = abb[15] + abb[16];
z[5] = 24 * z[5];
z[5] = z[5] * z[7];
z[1] = 12 * z[1] + -z[14];
z[6] = abb[23] + -abb[28] + z[12];
z[1] = abb[3] * z[1] * z[6];
z[0] = abb[32] + 12 * z[0] + z[1] + z[2] + z[5];
z[1] = abb[17] + -abb[18];
z[1] = z[1] * z[7];
z[2] = m1_set::bc<T>[0] * z[4];
z[3] = abb[12] * m1_set::bc<T>[0] * z[3];
z[1] = z[1] + z[2] + z[3];
z[1] = abb[19] + 24 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_573_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("38.465820901107017132791621168366399704479561354895964217352689771"),stof<T>("155.03138340149910087738157533550697601112644282942553847072269052")}, std::complex<T>{stof<T>("97.18759608960252167162726974367562446462811108130316857967000747"),stof<T>("-109.53047953893938742364957457028035779475019032507294071849736902")}, stof<T>("-32.956831045205618506819556344859225809804288248102477311593405295"), stof<T>("32.956831045205618506819556344859225809804288248102477311593405295"), std::complex<T>{stof<T>("60.620785967281217995169512692880189903064099659533592157439724144"),stof<T>("23.198661525710637199682439483685602446702398608675284738009014588")}, std::complex<T>{stof<T>("-135.65341699070953880441889091204202416910767243619913279702269724"),stof<T>("-45.50090386255971345373200076522661821637625250435259775222532149")}, std::complex<T>{stof<T>("-5.50898985590139862597206482350717389467527310679348690575928448"),stof<T>("-155.03138340149910087738157533550697601112644282942553847072269052")}, std::complex<T>{stof<T>("-130.14442713480814017844682608853485027443239932940564589126341276"),stof<T>("109.53047953893938742364957457028035779475019032507294071849736902")}, std::complex<T>{stof<T>("75.032631023428320809249378219161834266043572776665540639582973096"),stof<T>("22.302242336849076254049561281541015769673853895677313014216306907")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_573_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_573_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (3 * (-1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4]) * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (12 * m1_set::bc<T>[1] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[24] + -abb[29]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[11], 2);
z[1] = -prod_pow(abb[13], 2);
z[0] = z[0] + z[1];
z[1] = abb[24] + -abb[29];
return 12 * abb[7] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_573_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_573_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-2.154520679059784360990100623483204254609723531932218985972578178"),stof<T>("-83.334960797747767175709727208306861542691586359937135036251665966")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,33> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), f_1_1(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), f_2_2_re(k), f_2_10_re(k), T{0}};
abb[19] = SpDLog_f_4_573_W_20_Im(t, path, abb);
abb[32] = SpDLog_f_4_573_W_20_Re(t, path, abb);

                    
            return f_4_573_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_573_DLogXconstant_part(base_point<T>, kend);
	value += f_4_573_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_573_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_573_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_573_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_573_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_573_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_573_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
