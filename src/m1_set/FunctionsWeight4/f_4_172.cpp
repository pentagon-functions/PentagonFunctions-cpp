/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_172.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_172_abbreviated (const std::array<T,59>& abb) {
T z[132];
z[0] = -abb[29] + abb[32];
z[1] = abb[28] + z[0];
z[2] = abb[30] * (T(1) / T(2));
z[3] = z[1] * z[2];
z[4] = abb[31] * (T(1) / T(2));
z[5] = z[1] * z[4];
z[3] = z[3] + -z[5];
z[6] = z[2] + -z[4];
z[7] = abb[32] * (T(1) / T(2));
z[8] = -z[6] + z[7];
z[9] = 4 * abb[29];
z[10] = abb[28] * (T(1) / T(2));
z[11] = -z[8] + -z[9] + z[10];
z[11] = abb[27] * z[11];
z[12] = 3 * abb[36];
z[13] = prod_pow(abb[28], 2);
z[14] = 9 * abb[35] + z[12] + (T(7) / T(2)) * z[13];
z[15] = 4 * abb[37];
z[16] = abb[33] * (T(9) / T(2));
z[17] = abb[34] * (T(3) / T(2));
z[18] = prod_pow(abb[29], 2);
z[19] = 2 * abb[29];
z[20] = abb[32] * (T(5) / T(4)) + z[19];
z[20] = abb[32] * z[20];
z[11] = -z[3] + z[11] + (T(1) / T(2)) * z[14] + -z[15] + z[16] + z[17] + -z[18] + z[20];
z[11] = abb[13] * z[11];
z[14] = -abb[28] + abb[29];
z[20] = (T(1) / T(2)) * z[14];
z[21] = 2 * abb[32];
z[22] = z[20] + -z[21];
z[23] = abb[30] * (T(1) / T(4));
z[24] = abb[31] * (T(5) / T(4));
z[25] = abb[27] * (T(1) / T(2));
z[26] = -z[22] + -z[23] + z[24] + -z[25];
z[26] = abb[27] * z[26];
z[27] = abb[31] * (T(3) / T(2));
z[28] = -z[1] + z[27];
z[28] = z[2] * z[28];
z[29] = abb[33] * (T(3) / T(2));
z[30] = abb[37] * (T(1) / T(2));
z[31] = z[29] + z[30];
z[32] = 2 * abb[31];
z[33] = z[1] * z[32];
z[34] = prod_pow(abb[32], 2);
z[35] = (T(1) / T(2)) * z[34];
z[36] = abb[35] + z[35];
z[26] = -z[26] + -z[28] + z[31] + z[33] + (T(3) / T(2)) * z[36];
z[26] = abb[7] * z[26];
z[28] = z[10] + z[19];
z[37] = abb[30] + -z[4] + -z[21] + z[28];
z[37] = abb[30] * z[37];
z[38] = abb[31] + 2 * z[14];
z[39] = -z[7] + z[38];
z[39] = abb[31] * z[39];
z[40] = z[1] + -z[25];
z[41] = z[25] * z[40];
z[41] = -z[30] + z[41];
z[42] = abb[35] + -abb[36];
z[42] = 3 * z[42];
z[43] = (T(-7) / T(2)) * z[18] + z[42];
z[44] = -abb[29] + z[7];
z[44] = abb[32] * z[44];
z[37] = z[37] + -z[39] + -z[41] + (T(1) / T(2)) * z[43] + z[44];
z[37] = abb[8] * z[37];
z[43] = z[12] + (T(5) / T(2)) * z[13] + -z[18];
z[45] = 2 * abb[28];
z[24] = z[24] + -z[45];
z[46] = abb[29] * (T(1) / T(2)) + -z[7];
z[47] = abb[30] * (T(5) / T(4));
z[48] = -z[24] + z[46] + z[47];
z[48] = abb[27] * z[48];
z[49] = abb[31] * (T(3) / T(4));
z[50] = 2 * z[1];
z[51] = z[49] + -z[50];
z[51] = abb[30] * z[51];
z[5] = z[5] + z[30];
z[52] = z[5] + z[44];
z[48] = -z[17] + (T(-1) / T(2)) * z[43] + z[48] + z[51] + z[52];
z[48] = abb[4] * z[48];
z[53] = prod_pow(abb[31], 2);
z[54] = z[13] + z[53];
z[55] = -abb[31] + z[45];
z[56] = abb[30] + z[55];
z[56] = -abb[27] + 2 * z[56];
z[56] = abb[27] * z[56];
z[55] = abb[30] + 2 * z[55];
z[55] = abb[30] * z[55];
z[57] = 3 * abb[34];
z[55] = z[54] + -z[55] + z[56] + -z[57];
z[55] = abb[1] * z[55];
z[56] = -abb[30] + z[32];
z[58] = 3 * abb[32];
z[59] = -abb[27] + z[56] + z[58];
z[60] = abb[27] * z[59];
z[61] = abb[31] + z[58];
z[62] = abb[31] * z[61];
z[60] = z[60] + -z[62];
z[62] = abb[30] + abb[31];
z[63] = z[0] + -z[62];
z[63] = abb[30] * z[63];
z[64] = abb[29] + abb[32];
z[65] = abb[32] * z[64];
z[63] = z[18] + -z[60] + z[63] + z[65];
z[63] = abb[2] * z[63];
z[65] = abb[31] * z[14];
z[66] = (T(1) / T(2)) * z[13];
z[67] = (T(1) / T(2)) * z[18];
z[65] = -abb[35] + z[65] + z[66] + -z[67];
z[68] = abb[6] * z[65];
z[69] = -abb[28] + z[4];
z[70] = -z[2] + z[69];
z[71] = z[25] + z[70];
z[71] = abb[27] * z[71];
z[72] = abb[30] * z[69];
z[71] = abb[34] + z[71] + -z[72];
z[72] = abb[12] * z[71];
z[73] = z[68] + z[72];
z[74] = abb[31] + z[0];
z[75] = -z[25] + z[74];
z[75] = abb[27] * z[75];
z[75] = -z[44] + z[75];
z[76] = z[13] + -z[18];
z[77] = abb[31] * z[1];
z[78] = abb[37] + z[77];
z[79] = -z[75] + (T(-1) / T(2)) * z[76] + z[78];
z[79] = abb[15] * z[79];
z[80] = abb[2] * abb[33];
z[81] = 3 * z[80];
z[82] = -z[79] + z[81];
z[70] = -z[25] + z[70];
z[70] = abb[27] * z[70];
z[83] = abb[30] + -z[69];
z[83] = abb[30] * z[83];
z[54] = -z[54] + z[70] + z[83];
z[54] = abb[0] * z[54];
z[70] = abb[29] + z[7];
z[70] = abb[32] * z[70];
z[83] = -abb[35] + abb[37];
z[84] = -abb[33] + z[83];
z[85] = abb[29] + z[25];
z[85] = abb[27] * z[85];
z[85] = -z[70] + z[84] + z[85];
z[86] = 3 * abb[9];
z[85] = z[85] * z[86];
z[87] = abb[36] + z[67];
z[88] = z[83] + -z[87];
z[89] = -abb[29] + z[25];
z[89] = abb[27] * z[89];
z[89] = abb[33] + abb[34] + -z[88] + z[89];
z[89] = abb[3] * z[89];
z[90] = (T(3) / T(2)) * z[89];
z[91] = z[35] + -z[87];
z[92] = abb[30] * z[0];
z[92] = -z[91] + z[92];
z[93] = abb[5] * (T(3) / T(2));
z[94] = z[92] * z[93];
z[11] = z[11] + z[26] + z[37] + z[48] + z[54] + z[55] + z[63] + (T(3) / T(2)) * z[73] + z[82] + z[85] + -z[90] + z[94];
z[11] = abb[49] * z[11];
z[8] = z[8] + -z[28];
z[8] = abb[27] * z[8];
z[37] = 3 * abb[35];
z[43] = z[37] + z[43];
z[48] = abb[29] + abb[32] * (T(1) / T(4));
z[48] = abb[32] * z[48];
z[54] = 2 * abb[37];
z[3] = z[3] + z[8] + z[17] + z[29] + (T(1) / T(2)) * z[43] + z[48] + -z[54];
z[3] = abb[13] * z[3];
z[3] = z[3] + -z[90];
z[8] = z[1] + z[27];
z[8] = z[2] * z[8];
z[20] = -abb[27] + 4 * abb[32] + abb[31] * (T(13) / T(4)) + z[20] + -z[47];
z[20] = abb[27] * z[20];
z[29] = 4 * z[77];
z[8] = z[8] + -z[16] + z[20] + -z[29] + z[30] + (T(-9) / T(2)) * z[36];
z[8] = abb[7] * z[8];
z[16] = -z[13] + z[53];
z[20] = -abb[28] + abb[31];
z[43] = abb[27] * z[20];
z[48] = abb[30] * z[20];
z[43] = z[16] + z[43] + -z[48];
z[63] = abb[11] * z[43];
z[85] = 3 * abb[6];
z[90] = z[65] * z[85];
z[90] = z[72] + z[90];
z[90] = z[55] + z[63] + (T(3) / T(2)) * z[90];
z[95] = abb[30] + z[4];
z[96] = z[10] + -z[19] + z[21] + -z[95];
z[96] = abb[30] * z[96];
z[97] = z[7] + 4 * z[14] + z[32];
z[97] = abb[31] * z[97];
z[87] = -z[37] + z[87];
z[87] = -z[41] + (T(3) / T(2)) * z[87] + z[96] + z[97];
z[87] = abb[8] * z[87];
z[46] = -z[45] + z[46];
z[47] = abb[31] * (T(1) / T(4)) + z[25] + z[46] + -z[47];
z[47] = abb[27] * z[47];
z[96] = z[17] + z[30];
z[98] = abb[36] + z[66];
z[99] = z[77] + 3 * z[98];
z[47] = z[47] + -z[51] + z[96] + (T(1) / T(2)) * z[99];
z[47] = abb[4] * z[47];
z[51] = abb[31] + abb[32];
z[51] = abb[27] * z[51];
z[99] = abb[31] * abb[32];
z[100] = abb[33] + z[99];
z[51] = -z[34] + z[51] + -z[100];
z[51] = z[51] * z[86];
z[101] = abb[27] + z[62];
z[101] = z[25] * z[101];
z[95] = abb[30] * z[95];
z[95] = 2 * z[53] + -z[95] + z[101];
z[102] = abb[0] * z[95];
z[51] = z[51] + -z[102];
z[61] = z[32] * z[61];
z[102] = 2 * abb[27];
z[59] = z[59] * z[102];
z[103] = abb[30] + z[32];
z[103] = abb[30] * z[103];
z[59] = 3 * z[34] + -z[59] + z[61] + -z[103];
z[61] = -abb[2] * z[59];
z[103] = -abb[32] + z[6] + z[25];
z[103] = abb[27] * z[103];
z[100] = z[100] + z[103];
z[103] = abb[30] * z[4];
z[104] = z[100] + -z[103];
z[105] = abb[14] * (T(3) / T(2));
z[106] = z[104] * z[105];
z[106] = -z[94] + z[106];
z[107] = 6 * z[80];
z[8] = -z[3] + z[8] + z[47] + -z[51] + z[61] + -z[79] + z[87] + -z[90] + z[106] + -z[107];
z[8] = abb[45] * z[8];
z[47] = abb[30] * z[50];
z[61] = 6 * abb[33];
z[47] = z[47] + -z[61];
z[64] = z[21] * z[64];
z[87] = -abb[30] + abb[32] + z[20];
z[108] = -z[19] + -z[87];
z[108] = z[102] * z[108];
z[15] = 6 * abb[35] + -z[15] + z[33] + -z[47] + z[64] + z[76] + z[108];
z[15] = abb[13] * z[15];
z[56] = z[14] + z[21] + z[56];
z[56] = -abb[27] + 2 * z[56];
z[56] = abb[27] * z[56];
z[64] = 2 * abb[35];
z[108] = -z[34] + -z[64];
z[29] = -z[29] + z[47] + z[54] + z[56] + 3 * z[108];
z[29] = abb[7] * z[29];
z[47] = -abb[0] + abb[11];
z[56] = 2 * abb[15];
z[108] = abb[4] + -abb[8];
z[109] = -abb[7] + -z[47] + -z[56] + z[85] + z[108];
z[110] = abb[56] * z[109];
z[110] = z[63] + -z[110];
z[111] = -abb[32] + z[19];
z[111] = abb[32] * z[111];
z[112] = -z[54] + z[111];
z[113] = -abb[27] + 2 * z[74];
z[113] = abb[27] * z[113];
z[113] = z[112] + z[113];
z[33] = -z[33] + z[76] + z[113];
z[56] = -abb[4] + z[56];
z[33] = z[33] * z[56];
z[56] = z[18] + z[37] + z[48];
z[114] = abb[32] + z[38];
z[114] = z[32] * z[114];
z[50] = abb[27] + -z[50];
z[50] = abb[27] * z[50];
z[50] = z[50] + -2 * z[56] + -z[112] + z[114];
z[50] = abb[8] * z[50];
z[56] = -abb[29] + z[21];
z[56] = abb[32] * z[56];
z[74] = abb[30] * z[74];
z[56] = z[18] + -z[56] + z[60] + z[74];
z[74] = 2 * abb[2];
z[74] = z[56] * z[74];
z[32] = z[14] * z[32];
z[112] = -z[32] + z[64] + -z[76];
z[112] = z[85] * z[112];
z[115] = 2 * abb[0];
z[116] = z[43] * z[115];
z[117] = 2 * z[99];
z[113] = -z[64] + -z[113] + z[117];
z[113] = z[86] * z[113];
z[15] = z[15] + z[29] + z[33] + z[50] + z[74] + -z[107] + -2 * z[110] + z[112] + z[113] + z[116];
z[15] = abb[51] * z[15];
z[29] = abb[30] * z[1];
z[33] = abb[27] * z[87];
z[50] = abb[34] + z[98];
z[29] = -abb[33] + z[29] + z[33] + -z[36] + z[50] + -z[77];
z[33] = abb[13] * (T(1) / T(2));
z[29] = z[29] * z[33];
z[33] = z[75] + -z[83] + -z[99];
z[33] = abb[9] * z[33];
z[29] = z[29] + z[33] + z[79] + (T(-1) / T(2)) * z[89];
z[36] = abb[27] * z[40];
z[36] = z[36] + z[48] + -z[88] + -z[99];
z[40] = abb[8] * z[36];
z[40] = z[40] + z[73];
z[48] = -abb[27] + z[0] + z[2] + z[27];
z[48] = z[25] * z[48];
z[73] = -abb[36] + z[66];
z[74] = -abb[34] + -z[18] + z[73];
z[23] = -abb[31] * z[23];
z[23] = z[23] + z[48] + -z[52] + (T(1) / T(2)) * z[74];
z[23] = abb[4] * z[23];
z[48] = z[1] + -z[4];
z[52] = -abb[30] * z[48];
z[74] = z[6] + -z[14];
z[75] = abb[27] * z[74];
z[52] = z[35] + z[52] + z[75] + -z[84];
z[75] = abb[7] * (T(1) / T(2));
z[52] = z[52] * z[75];
z[77] = -abb[6] + abb[12];
z[79] = -abb[8] + z[77];
z[83] = abb[4] * (T(1) / T(2));
z[84] = -abb[15] + z[83];
z[79] = abb[0] + z[75] + (T(-1) / T(2)) * z[79] + z[84];
z[87] = -abb[56] * z[79];
z[88] = abb[5] * (T(1) / T(2));
z[98] = -abb[2] + z[88];
z[99] = z[92] * z[98];
z[107] = -abb[0] * z[71];
z[110] = abb[14] * (T(1) / T(2));
z[112] = z[104] * z[110];
z[113] = z[75] + -z[83];
z[116] = abb[2] + z[113];
z[118] = abb[8] * (T(1) / T(2)) + -z[88];
z[119] = z[116] + z[118];
z[120] = -z[110] + z[119];
z[121] = -abb[57] * z[120];
z[23] = z[23] + z[29] + (T(1) / T(2)) * z[40] + z[52] + z[87] + z[99] + z[107] + z[112] + z[121];
z[40] = 3 * abb[50];
z[23] = z[23] * z[40];
z[52] = -abb[4] + abb[7];
z[87] = 5 * abb[8];
z[99] = 5 * abb[0] + -7 * abb[1] + -3 * abb[2] + abb[12] + z[52] + z[87];
z[99] = abb[49] * z[99];
z[107] = abb[0] + abb[8];
z[52] = -abb[12] + abb[2] * (T(11) / T(3)) + z[52] + (T(-5) / T(3)) * z[107];
z[52] = abb[43] * z[52];
z[112] = abb[4] + abb[7];
z[121] = -9 * abb[8] + -abb[12] + -z[112];
z[121] = abb[44] * z[121];
z[122] = abb[44] + abb[43] * (T(1) / T(3));
z[122] = abb[11] * z[122];
z[123] = -abb[43] + abb[44] + -abb[49];
z[124] = abb[14] * z[123];
z[52] = z[52] + z[99] + z[121] + 11 * z[122] + z[124];
z[99] = abb[12] * (T(1) / T(2));
z[121] = abb[1] * (T(7) / T(2));
z[122] = abb[11] * (T(11) / T(6));
z[124] = z[99] + -z[121] + z[122];
z[125] = abb[4] * (T(-1) / T(3)) + abb[7] * (T(-1) / T(6)) + abb[8] * (T(8) / T(3)) + abb[2] * (T(11) / T(6)) + -z[110] + -z[124];
z[125] = abb[46] * z[125];
z[113] = -z[110] + z[113];
z[124] = abb[2] * (T(-1) / T(3)) + (T(-2) / T(3)) * z[107] + -z[113] + -z[124];
z[124] = abb[45] * z[124];
z[99] = z[99] + -z[107] + -z[113];
z[99] = abb[50] * z[99];
z[107] = abb[12] + abb[0] * (T(-11) / T(6)) + abb[7] * (T(2) / T(3)) + abb[4] * (T(5) / T(6)) + -z[121] + -z[122];
z[113] = -abb[41] + abb[48];
z[107] = z[107] * z[113];
z[121] = abb[20] + abb[22] + abb[24] + abb[25];
z[122] = -abb[52] + abb[53] + -abb[54] + abb[55];
z[126] = (T(1) / T(2)) * z[122];
z[121] = z[121] * z[126];
z[127] = abb[4] * (T(5) / T(2));
z[128] = -abb[7] + -10 * abb[8] + abb[2] * (T(-13) / T(2)) + -z[115] + z[127];
z[128] = abb[14] + (T(1) / T(3)) * z[128];
z[128] = abb[42] * z[128];
z[129] = -abb[7] + abb[14];
z[130] = -9 * abb[0] + 11 * abb[2];
z[130] = -z[129] + (T(1) / T(2)) * z[130];
z[130] = abb[47] * z[130];
z[131] = abb[23] * z[122];
z[47] = -abb[2] + abb[8] + -z[47];
z[47] = abb[51] * z[47];
z[47] = (T(11) / T(3)) * z[47] + (T(1) / T(2)) * z[52] + z[99] + z[107] + z[121] + z[124] + z[125] + z[128] + z[130] + z[131];
z[52] = prod_pow(m1_set::bc<T>[0], 2);
z[47] = z[47] * z[52];
z[3] = z[3] + -z[82];
z[82] = abb[31] * (T(5) / T(2));
z[99] = abb[30] * (T(3) / T(2));
z[107] = -z[0] + -z[82] + -z[99];
z[107] = abb[27] + (T(1) / T(2)) * z[107];
z[107] = abb[27] * z[107];
z[66] = z[12] + -z[66];
z[121] = abb[30] * z[49];
z[5] = z[5] + z[17] + z[18] + (T(1) / T(2)) * z[66] + z[107] + -z[111] + z[121];
z[5] = abb[4] * z[5];
z[17] = z[2] * z[20];
z[17] = z[17] + z[41];
z[20] = z[42] + z[67];
z[20] = -z[17] + (T(1) / T(2)) * z[20] + -z[39] + -z[44];
z[20] = abb[8] * z[20];
z[39] = -z[4] + z[45];
z[41] = abb[27] * (T(3) / T(2)) + -z[39] + -z[99];
z[41] = abb[27] * z[41];
z[39] = abb[30] * z[39];
z[39] = -z[16] + z[39] + z[41] + z[57];
z[41] = abb[0] * z[39];
z[42] = z[68] + -z[72];
z[12] = z[12] + z[44] + z[67];
z[57] = 2 * z[0];
z[66] = -abb[31] + z[57];
z[66] = abb[30] * z[66];
z[66] = z[12] + -z[60] + z[66];
z[66] = abb[2] * z[66];
z[5] = -z[3] + z[5] + z[20] + z[26] + z[41] + (T(3) / T(2)) * z[42] + z[66] + -z[94];
z[5] = abb[43] * z[5];
z[20] = -z[18] + 3 * z[73];
z[24] = -abb[30] * z[24];
z[26] = abb[30] * (T(3) / T(4)) + -z[49];
z[41] = abb[27] + -z[26] + z[46];
z[41] = abb[27] * z[41];
z[42] = -abb[31] + (T(1) / T(2)) * z[1];
z[42] = abb[31] * z[42];
z[20] = (T(1) / T(2)) * z[20] + z[24] + z[41] + z[42] + -z[44] + z[96];
z[20] = abb[4] * z[20];
z[24] = z[35] + -z[37];
z[41] = -z[1] + z[82];
z[42] = -abb[30] + (T(-1) / T(2)) * z[41];
z[42] = abb[30] * z[42];
z[22] = abb[27] + z[22] + z[26];
z[22] = abb[27] * z[22];
z[22] = z[22] + (T(3) / T(2)) * z[24] + z[31] + z[42] + z[114];
z[22] = abb[7] * z[22];
z[24] = abb[36] + -z[37];
z[24] = (T(-5) / T(2)) * z[18] + 3 * z[24];
z[24] = -z[17] + (T(1) / T(2)) * z[24] + z[97] + -z[111];
z[24] = abb[8] * z[24];
z[26] = -abb[2] * z[56];
z[3] = -z[3] + z[20] + z[22] + z[24] + z[26] + -z[90] + -z[106];
z[3] = abb[46] * z[3];
z[20] = -z[116] + z[118];
z[22] = z[20] + z[110];
z[22] = abb[45] * z[22];
z[24] = abb[5] + z[108];
z[26] = z[24] + -z[129];
z[31] = abb[46] * (T(1) / T(2));
z[26] = z[26] * z[31];
z[24] = abb[7] + z[24];
z[37] = abb[44] * (T(1) / T(2));
z[24] = z[24] * z[37];
z[37] = abb[43] * z[119];
z[20] = abb[49] * z[20];
z[42] = z[110] * z[123];
z[46] = -abb[2] + z[129];
z[49] = -abb[5] + abb[8] + z[46];
z[49] = abb[42] * z[49];
z[46] = abb[47] * z[46];
z[20] = -z[20] + z[22] + -z[24] + z[26] + z[37] + z[42] + -z[46] + z[49];
z[22] = abb[25] * z[126];
z[22] = z[20] + z[22];
z[22] = abb[57] * z[22];
z[24] = abb[45] + abb[46];
z[26] = -abb[44] + abb[47] + -abb[49] + z[24];
z[26] = abb[10] * z[26];
z[37] = z[26] * z[76];
z[22] = z[22] + z[37];
z[37] = abb[29] + -5 * abb[32];
z[37] = abb[32] * z[37];
z[42] = abb[30] + 3 * abb[31] + z[0];
z[42] = abb[30] * z[42];
z[37] = z[18] + z[37] + z[42] + 3 * z[60];
z[37] = abb[2] * z[37];
z[42] = z[4] + z[57];
z[42] = abb[30] * z[42];
z[12] = z[12] + z[42] + z[53] + -z[101];
z[12] = abb[4] * z[12];
z[42] = -z[59] + -z[61];
z[42] = abb[7] * z[42];
z[46] = -abb[30] + z[57];
z[46] = abb[30] * z[46];
z[46] = 2 * z[18] + z[46] + z[111];
z[46] = abb[8] * z[46];
z[49] = abb[14] * z[104];
z[53] = 3 * abb[5];
z[56] = -z[53] * z[92];
z[12] = z[12] + z[37] + z[42] + z[46] + 3 * z[49] + -z[51] + z[56] + -9 * z[80];
z[12] = abb[42] * z[12];
z[37] = -abb[36] + z[67];
z[42] = 2 * abb[33];
z[46] = abb[27] * z[19];
z[13] = -abb[34] + -z[13] + z[37] + -z[42] + z[46] + z[54] + -z[64] + -z[70];
z[13] = abb[13] * z[13];
z[21] = z[2] + -z[21] + z[25] + -z[27];
z[21] = abb[27] * z[21];
z[21] = z[21] + z[34] + z[42] + -z[103] + z[117];
z[21] = abb[7] * z[21];
z[34] = -abb[29] + abb[32] * (T(3) / T(2));
z[34] = abb[32] * z[34];
z[42] = -abb[30] * abb[31];
z[34] = z[34] + -z[37] + z[42] + -z[60];
z[34] = abb[2] * z[34];
z[37] = -abb[27] + z[62];
z[42] = -z[25] * z[37];
z[16] = abb[34] + -z[16] + z[42] + z[103];
z[16] = abb[0] * z[16];
z[13] = z[13] + z[16] + z[21] + z[33] + z[34] + -z[49] + z[81] + z[89];
z[16] = 3 * abb[47];
z[13] = z[13] * z[16];
z[21] = abb[4] * z[39];
z[33] = abb[0] * z[43];
z[34] = abb[7] * z[95];
z[21] = z[21] + z[33] + z[34] + -z[55] + z[63] + -3 * z[72];
z[21] = -z[21] * z[113];
z[33] = 5 * abb[35];
z[34] = abb[33] + z[33] + z[35];
z[35] = -z[2] * z[48];
z[25] = z[25] * z[74];
z[25] = z[25] + -z[30] + -z[32] + (T(1) / T(2)) * z[34] + z[35];
z[25] = abb[7] * z[25];
z[18] = -abb[36] + (T(3) / T(2)) * z[18] + z[33];
z[30] = -z[7] + -z[38];
z[30] = abb[31] * z[30];
z[17] = z[17] + (T(1) / T(2)) * z[18] + z[30] + -z[44];
z[17] = abb[8] * z[17];
z[6] = z[0] + -z[6];
z[6] = abb[27] * z[6];
z[6] = z[6] + z[50] + -z[78] + z[103];
z[6] = z[6] * z[83];
z[18] = 5 * z[68] + -z[72];
z[30] = -z[88] * z[92];
z[6] = z[6] + z[17] + (T(1) / T(2)) * z[18] + z[25] + z[29] + z[30];
z[17] = 3 * abb[44];
z[6] = z[6] * z[17];
z[18] = abb[6] + abb[12];
z[18] = 3 * z[18] + -z[87];
z[25] = abb[7] * (T(5) / T(2));
z[18] = (T(1) / T(2)) * z[18] + -z[25] + -z[84] + -z[115];
z[18] = abb[43] * z[18];
z[29] = 3 * z[77] + z[87];
z[30] = 4 * abb[1];
z[25] = abb[0] + -abb[15] + abb[4] * (T(-3) / T(2)) + z[25] + (T(1) / T(2)) * z[29] + -z[30];
z[25] = abb[49] * z[25];
z[29] = -abb[12] + z[85];
z[29] = -7 * abb[8] + 3 * z[29];
z[29] = -abb[11] + -abb[15] + abb[7] * (T(-7) / T(2)) + (T(1) / T(2)) * z[29] + z[30] + z[127];
z[24] = z[24] * z[29];
z[29] = 5 * abb[6] + -3 * abb[8] + abb[12];
z[29] = abb[7] * (T(-3) / T(2)) + (T(1) / T(2)) * z[29] + z[84];
z[29] = z[17] * z[29];
z[32] = abb[20] + abb[23];
z[33] = (T(3) / T(2)) * z[122];
z[32] = z[32] * z[33];
z[30] = abb[0] + 2 * abb[4] + abb[11] + -3 * abb[12] + z[30];
z[30] = z[30] * z[113];
z[33] = abb[43] + z[17];
z[33] = abb[11] * z[33];
z[18] = -z[18] + z[24] + z[25] + -z[29] + -z[30] + z[32] + z[33];
z[24] = abb[56] * z[18];
z[25] = -abb[17] + abb[18];
z[29] = abb[16] + z[25];
z[29] = abb[49] * z[29];
z[30] = abb[17] + abb[18];
z[32] = -abb[16] + z[30];
z[32] = abb[43] * z[32];
z[34] = abb[16] + z[30];
z[35] = abb[44] * z[34];
z[38] = -abb[26] * z[122];
z[39] = abb[19] * z[123];
z[29] = -z[29] + z[32] + z[35] + z[38] + -z[39];
z[32] = -abb[16] + abb[19];
z[25] = -z[25] + z[32];
z[35] = abb[45] * (T(1) / T(2));
z[25] = z[25] * z[35];
z[30] = z[30] + z[32];
z[35] = abb[50] * (T(1) / T(2));
z[30] = z[30] * z[35];
z[35] = abb[19] + z[34];
z[31] = z[31] * z[35];
z[34] = z[34] * z[113];
z[35] = abb[17] + abb[19];
z[35] = abb[42] * z[35];
z[32] = abb[47] * z[32];
z[25] = z[25] + (T(-1) / T(2)) * z[29] + z[30] + -z[31] + -z[32] + z[34] + z[35];
z[25] = (T(3) / T(2)) * z[25];
z[29] = abb[58] * z[25];
z[30] = z[0] + z[4];
z[30] = abb[30] * z[30];
z[30] = z[30] + -z[91] + -z[100];
z[31] = abb[24] * (T(3) / T(2));
z[30] = -z[30] * z[31];
z[32] = z[65] + z[71];
z[34] = abb[22] * (T(3) / T(2));
z[32] = z[32] * z[34];
z[30] = z[30] + z[32];
z[30] = z[30] * z[122];
z[32] = -z[36] * z[122];
z[35] = abb[57] * z[122];
z[35] = z[32] + z[35];
z[36] = abb[23] * (T(3) / T(2));
z[35] = z[35] * z[36];
z[38] = abb[56] + abb[57];
z[38] = z[38] * z[122];
z[32] = z[32] + z[38];
z[38] = z[52] * z[122];
z[32] = (T(3) / T(2)) * z[32] + z[38];
z[32] = abb[21] * z[32];
z[38] = z[105] * z[123];
z[39] = abb[25] * (T(3) / T(2));
z[39] = z[39] * z[122];
z[38] = z[38] + z[39];
z[39] = z[38] * z[104];
z[42] = z[33] * z[43];
z[43] = abb[20] * (T(3) / T(2));
z[43] = z[43] * z[122];
z[44] = z[43] * z[71];
z[3] = z[3] + z[5] + z[6] + z[8] + z[11] + z[12] + z[13] + z[15] + z[21] + 3 * z[22] + z[23] + z[24] + z[29] + z[30] + z[32] + z[35] + z[39] + z[42] + z[44] + z[47];
z[5] = z[0] * z[93];
z[6] = 3 * abb[28];
z[8] = z[6] + -z[37];
z[11] = abb[1] * z[8];
z[11] = 2 * z[11];
z[12] = z[5] + -z[11];
z[13] = z[27] + z[99];
z[15] = z[1] + z[13];
z[15] = (T(1) / T(2)) * z[15] + -z[102];
z[21] = abb[4] * z[15];
z[13] = -z[1] + z[13];
z[13] = -abb[27] + (T(1) / T(2)) * z[13];
z[22] = abb[7] * z[13];
z[21] = z[21] + -z[22];
z[4] = z[2] + z[4];
z[6] = -z[4] + z[6] + z[102];
z[6] = abb[0] * z[6];
z[7] = -z[4] + z[7];
z[10] = z[7] + z[10];
z[9] = z[9] + z[10];
z[9] = abb[8] * z[9];
z[2] = z[2] + z[69];
z[22] = abb[12] * z[2];
z[23] = abb[6] * z[14];
z[24] = -z[22] + z[23];
z[27] = 3 * abb[29];
z[29] = -z[27] + -z[37];
z[29] = abb[2] * z[29];
z[30] = abb[27] + -abb[32];
z[32] = abb[9] * z[30];
z[35] = -abb[27] + z[1];
z[39] = abb[13] * z[35];
z[42] = abb[15] * z[35];
z[6] = z[6] + z[9] + z[12] + z[21] + (T(3) / T(2)) * z[24] + z[29] + -6 * z[32] + -4 * z[39] + -z[42];
z[6] = abb[49] * z[6];
z[29] = z[14] * z[85];
z[44] = -z[22] + z[29];
z[46] = abb[11] * z[8];
z[44] = (T(3) / T(2)) * z[44] + z[46];
z[47] = -z[75] + -z[83];
z[41] = abb[30] * (T(5) / T(2)) + z[41];
z[41] = z[41] * z[47];
z[47] = -abb[32] + z[4];
z[48] = z[47] * z[105];
z[27] = z[27] + -z[37];
z[27] = abb[2] * z[27];
z[39] = 2 * z[39];
z[49] = z[39] + -z[42];
z[9] = z[5] + z[9] + z[11] + z[27] + z[41] + -z[44] + z[48] + z[49];
z[9] = abb[46] * z[9];
z[35] = z[35] * z[112];
z[41] = abb[0] * z[8];
z[30] = z[30] * z[86];
z[50] = -abb[28] + -abb[32] + z[62];
z[51] = z[19] + -z[50];
z[51] = abb[8] * z[51];
z[27] = -z[27] + -z[29] + -z[30] + z[35] + -z[39] + z[41] + -2 * z[42] + -z[46] + z[51];
z[29] = 2 * abb[51];
z[27] = z[27] * z[29];
z[10] = z[10] + -z[19];
z[10] = abb[8] * z[10];
z[35] = (T(1) / T(2)) * z[62];
z[51] = abb[27] + z[35];
z[52] = abb[0] * z[51];
z[10] = z[10] + z[49] + -z[52];
z[13] = -abb[4] * z[13];
z[15] = abb[7] * z[15];
z[49] = 2 * z[37];
z[54] = abb[2] * z[49];
z[12] = z[10] + -z[12] + z[13] + z[15] + z[30] + -z[44] + -z[48] + z[54];
z[12] = abb[45] * z[12];
z[13] = -z[1] + z[4];
z[15] = z[13] * z[75];
z[15] = z[15] + z[32] + z[42];
z[42] = z[0] * z[98];
z[44] = abb[8] * z[50];
z[24] = z[24] + z[44];
z[44] = abb[0] * z[2];
z[1] = -z[1] + -z[4];
z[1] = abb[27] + (T(1) / T(2)) * z[1];
z[1] = abb[4] * z[1];
z[48] = -z[47] * z[110];
z[1] = z[1] + z[15] + (T(1) / T(2)) * z[24] + z[42] + z[44] + z[48];
z[1] = z[1] * z[40];
z[24] = abb[7] * z[51];
z[42] = abb[4] * z[51];
z[11] = z[11] + 3 * z[22] + -z[24] + z[41] + -z[42] + z[46];
z[11] = -z[11] * z[113];
z[24] = z[22] + z[23];
z[41] = -z[37] + z[58];
z[41] = abb[2] * z[41];
z[5] = -z[5] + z[10] + z[21] + (T(3) / T(2)) * z[24] + z[41];
z[5] = abb[43] * z[5];
z[7] = -z[7] + -z[28];
z[7] = abb[8] * z[7];
z[10] = z[22] + 5 * z[23];
z[21] = -z[0] * z[88];
z[13] = z[13] * z[83];
z[7] = z[7] + (T(1) / T(2)) * z[10] + z[13] + z[15] + z[21];
z[7] = z[7] * z[17];
z[10] = abb[14] * z[47];
z[13] = -abb[8] * z[19];
z[15] = -abb[29] + z[37];
z[15] = abb[2] * z[15];
z[13] = z[10] + -z[13] + -z[15];
z[0] = -z[0] * z[53];
z[15] = abb[7] * z[49];
z[0] = z[0] + -3 * z[13] + z[15] + z[30] + z[42] + -z[52];
z[0] = abb[42] * z[0];
z[13] = abb[32] + z[19] + -z[37];
z[13] = abb[2] * z[13];
z[15] = -abb[27] + z[4] + -z[45];
z[15] = abb[0] * z[15];
z[17] = abb[27] + -z[35];
z[17] = abb[7] * z[17];
z[10] = z[10] + z[13] + z[15] + z[17] + z[32] + z[39];
z[10] = z[10] * z[16];
z[13] = -z[38] * z[47];
z[15] = z[31] + z[34];
z[4] = -abb[29] + z[4];
z[4] = -z[4] * z[15] * z[122];
z[2] = -z[2] * z[43];
z[8] = z[8] * z[33];
z[14] = z[14] * z[26];
z[15] = -z[50] * z[122];
z[16] = z[15] * z[36];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + 6 * z[14] + z[16] + z[27];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[23] + abb[25];
z[1] = z[1] * z[126];
z[2] = -abb[50] * z[120];
z[1] = z[1] + z[2] + z[20];
z[1] = abb[39] * z[1];
z[2] = z[29] * z[109];
z[4] = -z[40] * z[79];
z[2] = z[2] + z[4] + z[18];
z[2] = abb[38] * z[2];
z[4] = abb[40] * z[25];
z[5] = abb[38] + abb[39];
z[5] = z[5] * z[122];
z[6] = m1_set::bc<T>[0] * z[15];
z[5] = z[5] + z[6];
z[5] = abb[21] * z[5];
z[0] = z[0] + 3 * z[1] + z[2] + z[4] + (T(3) / T(2)) * z[5];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_172_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("-60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-50.753230340904010132424722396808666651715583559439082167006918678"),stof<T>("34.081682310324995686088672650457391383990622786459470662902339143")}, std::complex<T>{stof<T>("7.7691405800748865872202464486523257300927602883641503557166568482"),stof<T>("0.9279987808995460879225665469713760549679629066638179096498495597")}, std::complex<T>{stof<T>("-13.490678737724229576388000192776270368528148022631162466132111686"),stof<T>("-7.950728224719451816036195091289869905517934538622874076227542381")}, std::complex<T>{stof<T>("-6.4728848884311135370344688408294995525324252005990025093280016611"),stof<T>("-7.46249334498498083634381434235989191476518227252497846562749018")}, std::complex<T>{stof<T>("44.280345452472896595390253555979167099183158358840079657678917017"),stof<T>("-41.544175655309976522432486992817283298755805058984449128529829323")}, std::complex<T>{stof<T>("45.563567763947112572112705731626154617787502660050213118301287389"),stof<T>("30.572639609146429529044097999753893635241788726625451800264414552")}, std::complex<T>{stof<T>("-18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-5.6789893346428846672177607042692797070508719739284476388982577664"),stof<T>("-3.3844629567787349761988538383864319226127633681444014415660211259")}, std::complex<T>{stof<T>("4.3827336429991116170319830964464535294905368861632997925096025793"),stof<T>("9.9189575208641697246201016337749477824099827340055619975436617462")}, std::complex<T>{stof<T>("-24.303748446147996408504459090197558519166594349054900296452518855"),stof<T>("-21.693912603527431625085336361492647674755891281338759814387022612")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[80].real()/kbase.W[80].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_172_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_172_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-26.986672380774296321390070915721496290329239903904055530551261089"),stof<T>("42.839273860445306593362705682689067954951045754876709402369339523")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W81(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[80].real()/k.W[80].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_172_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_172_DLogXconstant_part(base_point<T>, kend);
	value += f_4_172_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_172_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_172_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_172_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_172_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_172_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_172_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
