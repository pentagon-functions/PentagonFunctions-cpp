/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_616.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_616_abbreviated (const std::array<T,31>& abb) {
T z[47];
z[0] = abb[2] * (T(3) / T(2));
z[1] = 5 * abb[1];
z[2] = abb[6] * (T(3) / T(2));
z[3] = 2 * abb[3];
z[4] = z[0] + -z[1] + z[2] + -z[3];
z[5] = abb[0] * (T(3) / T(2));
z[6] = z[4] + -z[5];
z[6] = abb[19] * z[6];
z[7] = 3 * abb[1];
z[8] = abb[6] * (T(1) / T(2));
z[9] = abb[2] * (T(-5) / T(4)) + z[7] + -z[8];
z[10] = abb[0] * (T(1) / T(2));
z[11] = abb[3] * (T(7) / T(4)) + z[9] + z[10];
z[11] = abb[23] * z[11];
z[12] = -abb[1] + z[2];
z[13] = -z[10] + z[12];
z[14] = abb[3] * (T(1) / T(2));
z[15] = z[13] + z[14];
z[15] = abb[22] * z[15];
z[16] = abb[25] + -abb[26] + abb[27];
z[17] = abb[7] * z[16];
z[18] = abb[8] * z[16];
z[19] = 3 * z[17] + z[18];
z[20] = abb[3] * (T(1) / T(4));
z[21] = abb[2] * (T(1) / T(4));
z[22] = z[20] + -z[21];
z[23] = 2 * abb[1];
z[24] = -abb[6] + z[23];
z[25] = abb[0] + z[22] + z[24];
z[26] = -abb[20] + abb[21];
z[25] = z[25] * z[26];
z[27] = 2 * abb[4];
z[28] = abb[19] + abb[22] + -abb[23];
z[29] = z[27] * z[28];
z[30] = -abb[2] + abb[3];
z[31] = z[24] + z[30];
z[31] = abb[24] * z[31];
z[6] = -z[6] + -z[11] + z[15] + (T(1) / T(4)) * z[19] + z[25] + -z[29] + z[31];
z[6] = abb[13] * z[6];
z[11] = 2 * abb[2];
z[15] = -z[3] + z[11];
z[19] = 3 * abb[6];
z[25] = -abb[1] + z[15] + -z[19];
z[25] = abb[22] * z[25];
z[29] = abb[24] + z[26];
z[32] = abb[1] + -abb[6];
z[33] = -z[29] * z[32];
z[1] = -abb[6] + z[1] + -z[15];
z[1] = abb[23] * z[1];
z[34] = 7 * abb[1] + -z[19];
z[15] = -z[15] + z[34];
z[15] = abb[19] * z[15];
z[35] = 2 * z[29];
z[36] = abb[19] + -5 * abb[22] + abb[23] + z[35];
z[36] = abb[4] * z[36];
z[1] = -z[1] + z[15] + -z[25] + -2 * z[33] + z[36];
z[15] = abb[12] * z[1];
z[6] = z[6] + -z[15];
z[25] = abb[2] * (T(3) / T(4));
z[20] = -z[5] + -z[20] + z[25];
z[33] = -abb[6] + z[20];
z[33] = -z[26] * z[33];
z[37] = z[2] + -z[11];
z[38] = abb[3] * (T(3) / T(2));
z[39] = -abb[1] + -z[5] + -z[37] + -z[38];
z[39] = abb[22] * z[39];
z[40] = abb[2] * (T(1) / T(2));
z[12] = z[5] + z[12] + -z[40];
z[12] = abb[19] * z[12];
z[41] = z[17] + z[18];
z[42] = (T(3) / T(4)) * z[41];
z[43] = abb[1] + -z[8] + z[22];
z[43] = abb[23] * z[43];
z[44] = 3 * abb[22];
z[35] = -3 * abb[19] + abb[23] + -z[35] + z[44];
z[45] = abb[4] * z[35];
z[46] = abb[6] + z[30];
z[46] = abb[24] * z[46];
z[12] = z[12] + z[33] + z[39] + z[42] + z[43] + z[45] + z[46];
z[12] = abb[11] * z[12];
z[12] = -z[6] + z[12];
z[12] = abb[11] * z[12];
z[4] = -z[4] + -z[10];
z[4] = abb[19] * z[4];
z[33] = abb[3] * (T(3) / T(4));
z[9] = -z[9] + z[10] + -z[33];
z[9] = abb[23] * z[9];
z[39] = abb[3] * (T(5) / T(4));
z[43] = -z[21] + z[39];
z[45] = z[24] + z[43];
z[45] = z[26] * z[45];
z[17] = z[17] + -z[18];
z[17] = (T(1) / T(4)) * z[17];
z[13] = z[13] + -z[38];
z[13] = abb[22] * z[13];
z[4] = z[4] + z[9] + z[13] + -z[17] + z[31] + z[45];
z[4] = abb[13] * z[4];
z[9] = z[10] + z[23];
z[8] = z[8] + -z[9] + z[25] + -z[39];
z[8] = abb[23] * z[8];
z[13] = abb[3] * (T(7) / T(2)) + z[9] + z[37];
z[13] = abb[22] * z[13];
z[2] = z[2] + -z[9] + z[40];
z[2] = abb[19] * z[2];
z[9] = abb[6] + z[43];
z[9] = z[9] * z[26];
z[2] = z[2] + -z[8] + z[9] + -z[13] + -z[17] + -z[36] + z[46];
z[8] = -abb[11] * z[2];
z[9] = abb[0] + -abb[2];
z[9] = -z[9] * z[26];
z[9] = z[9] + -z[18];
z[10] = abb[1] + -z[10] + -z[40];
z[10] = abb[23] * z[10];
z[13] = abb[0] + -abb[1];
z[13] = abb[22] * z[13];
z[17] = -abb[1] + abb[2];
z[17] = abb[19] * z[17];
z[9] = (T(1) / T(2)) * z[9] + z[10] + z[13] + z[17];
z[9] = abb[14] * z[9];
z[4] = z[4] + z[8] + z[9] + -z[15];
z[4] = abb[14] * z[4];
z[8] = abb[22] * (T(3) / T(2));
z[9] = abb[19] * (T(3) / T(2));
z[10] = abb[23] * (T(1) / T(2)) + z[8] + -z[9] + -z[29];
z[10] = abb[9] * z[10];
z[13] = abb[0] + -abb[10] + z[21] + z[33];
z[13] = z[13] * z[16];
z[15] = abb[7] + 5 * abb[8];
z[16] = abb[23] * (T(1) / T(4));
z[15] = z[15] * z[16];
z[16] = abb[7] + -abb[8];
z[17] = (T(1) / T(4)) * z[16];
z[17] = z[17] * z[26];
z[16] = abb[24] * z[16];
z[9] = abb[8] * z[9];
z[8] = abb[7] * z[8];
z[8] = -z[8] + -z[9] + -z[10] + z[13] + z[15] + z[16] + z[17];
z[9] = -abb[29] * z[8];
z[10] = 2 * abb[6];
z[13] = abb[1] + z[10];
z[13] = (T(1) / T(3)) * z[13];
z[15] = z[13] + -z[20];
z[15] = -z[15] * z[26];
z[16] = abb[6] + abb[1] * (T(-1) / T(2)) + z[22];
z[16] = abb[23] * z[16];
z[17] = abb[22] * (T(-13) / T(2)) + abb[23] * (T(-7) / T(2)) + abb[19] * (T(17) / T(2)) + 5 * z[29];
z[17] = abb[4] * z[17];
z[16] = z[16] + z[17];
z[13] = -z[13] + -z[30];
z[13] = abb[24] * z[13];
z[5] = abb[6] + z[5];
z[17] = abb[2] * (T(-5) / T(3)) + abb[1] * (T(5) / T(6)) + abb[3] * (T(7) / T(6)) + z[5];
z[17] = abb[22] * z[17];
z[5] = abb[3] * (T(-1) / T(3)) + abb[1] * (T(-1) / T(6)) + abb[2] * (T(5) / T(6)) + -z[5];
z[5] = abb[19] * z[5];
z[5] = z[5] + z[13] + z[15] + (T(1) / T(3)) * z[16] + z[17] + -z[42];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[13] = abb[3] + z[23];
z[13] = abb[19] * z[13];
z[15] = abb[1] + abb[3];
z[15] = abb[23] * z[15];
z[16] = abb[3] * abb[22];
z[17] = abb[1] * z[26];
z[18] = abb[4] * z[28];
z[13] = z[13] + -z[15] + z[16] + z[17] + -z[18];
z[15] = prod_pow(abb[12], 2);
z[16] = abb[12] * abb[13];
z[15] = z[15] + -z[16];
z[15] = -2 * z[15];
z[13] = z[13] * z[15];
z[3] = z[3] + z[7] + -z[10];
z[3] = z[3] * z[26];
z[7] = 3 * abb[3];
z[15] = z[7] + -z[11] + z[34];
z[15] = abb[19] * z[15];
z[16] = -abb[3] + -z[32];
z[16] = z[16] * z[44];
z[11] = -4 * abb[1] + -abb[3] + abb[6] + z[11];
z[11] = abb[23] * z[11];
z[3] = z[3] + z[11] + z[15] + z[16] + 2 * z[31];
z[3] = abb[15] * z[3];
z[11] = abb[28] * z[1];
z[3] = abb[30] + z[3] + z[4] + z[5] + z[9] + z[11] + z[12] + z[13];
z[4] = 3 * abb[0];
z[0] = -z[0] + z[4] + z[10] + z[14];
z[0] = -z[0] * z[26];
z[4] = z[4] + z[19];
z[5] = -4 * abb[2] + z[4] + z[7] + z[23];
z[5] = abb[22] * z[5];
z[7] = -z[14] + -z[24] + z[40];
z[7] = abb[23] * z[7];
z[4] = abb[2] + -z[4] + z[23];
z[4] = abb[19] * z[4];
z[9] = -z[27] * z[35];
z[0] = z[0] + z[4] + z[5] + z[7] + z[9] + (T(-3) / T(2)) * z[41] + -2 * z[46];
z[0] = abb[11] * z[0];
z[2] = abb[14] * z[2];
z[0] = z[0] + z[2] + z[6];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[17] * z[8];
z[1] = abb[16] * z[1];
z[0] = abb[18] + z[0] + z[1] + z[2];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_616_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("5.8441052091934394355957795944545083944060231559654381734011444864"),stof<T>("4.5595132367802453436896934862671728395635626532458245263118165742")}, std::complex<T>{stof<T>("3.2481853203630900633407033833170887234980389256704273032843753355"),stof<T>("-9.1722009068429313864008486576030179725601740273379980739751518286")}, std::complex<T>{stof<T>("-3.2481853203630900633407033833170887234980389256704273032843753355"),stof<T>("9.1722009068429313864008486576030179725601740273379980739751518286")}, std::complex<T>{stof<T>("11.9633763092481669255026271436199879280381749457019868763583130066"),stof<T>("-6.6992573141383905889165670726101014671262602159095057418537903947")}, std::complex<T>{stof<T>("-9.0922905295565294989364829777715971179040620816358654766855198219"),stof<T>("4.6126876700626860427111551713358451329966113740921735476633352544")}, std::complex<T>{stof<T>("0.3770995406714526367745592174686979133639260616043059036115821508"),stof<T>("-7.0856312627672268401954367563287616384305251855206658797846966884")}, std::complex<T>{stof<T>("-7.4219161741015956134980195093093657607382609948979409912152034895"),stof<T>("10.5057465250230288743736521272080806678691704747824452001159249788")}, std::complex<T>{stof<T>("7.4219161741015956134980195093093657607382609948979409912152034895"),stof<T>("-10.5057465250230288743736521272080806678691704747824452001159249788")}, std::complex<T>{stof<T>("-7.4219161741015956134980195093093657607382609948979409912152034895"),stof<T>("10.5057465250230288743736521272080806678691704747824452001159249788")}};
	
	std::vector<C> intdlogs = {rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(abs(k.W[43])) - rlog(abs(kbase.W[43])), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_616_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_616_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[2] + v[3]) * (8 + v[2] + v[3] + 4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = (-(1 + 2 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (2 * abb[19] + -abb[20] + abb[21] + -abb[23]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[12] + abb[13];
z[0] = abb[12] * z[0];
z[0] = -abb[15] + 2 * z[0];
z[1] = -2 * abb[19] + abb[20] + -abb[21] + abb[23];
return abb[5] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_616_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_616_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-4.8329766544215011532849006427545859612444678092320330616003380173"),stof<T>("2.0530709259527252973344697485800414858536982734356780881184735831")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18, 43});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_13(k), f_2_4_im(k), f_2_25_im(k), T{0}, rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(abs(kend.W[43])) - rlog(abs(k.W[43])), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_616_W_19_Im(t, path, abb);
abb[30] = SpDLog_f_4_616_W_19_Re(t, path, abb);

                    
            return f_4_616_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_616_DLogXconstant_part(base_point<T>, kend);
	value += f_4_616_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_616_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_616_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_616_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_616_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_616_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_616_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
