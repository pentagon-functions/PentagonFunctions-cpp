/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_600.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_600_abbreviated (const std::array<T,64>& abb) {
T z[131];
z[0] = 5 * abb[48];
z[1] = abb[47] + abb[50];
z[1] = -abb[45] + -z[0] + (T(-1) / T(3)) * z[1];
z[2] = abb[46] + abb[54];
z[3] = abb[44] * (T(-13) / T(6)) + abb[53] * (T(8) / T(3));
z[4] = abb[49] * (T(7) / T(6));
z[1] = abb[52] * (T(13) / T(6)) + (T(1) / T(2)) * z[1] + (T(1) / T(3)) * z[2] + z[3] + -z[4];
z[1] = abb[8] * z[1];
z[2] = -abb[48] + abb[54];
z[5] = -abb[50] + abb[47] * (T(1) / T(2));
z[2] = 7 * abb[46] + (T(-1) / T(2)) * z[2] + -z[5];
z[6] = 3 * abb[45];
z[2] = abb[51] + abb[49] * (T(1) / T(2)) + (T(1) / T(3)) * z[2] + -z[3] + -z[6];
z[2] = abb[3] * z[2];
z[3] = abb[55] * (T(1) / T(2)) + abb[51] * (T(2) / T(3));
z[7] = -abb[50] + abb[54];
z[4] = abb[45] * (T(-5) / T(3)) + z[3] + -z[4] + (T(1) / T(2)) * z[7];
z[4] = abb[2] * z[4];
z[8] = 2 * abb[53];
z[9] = abb[46] + -abb[48];
z[10] = z[8] + z[9];
z[11] = 2 * abb[49];
z[12] = -abb[55] + z[11];
z[13] = z[10] + z[12];
z[14] = abb[13] * z[13];
z[15] = abb[53] + -abb[55];
z[16] = abb[45] + z[15];
z[17] = abb[10] * z[16];
z[18] = -z[14] + z[17];
z[19] = -abb[47] + abb[48];
z[20] = -abb[50] + z[19];
z[20] = abb[54] + (T(1) / T(2)) * z[20];
z[21] = abb[46] * (T(1) / T(2));
z[22] = z[20] + -z[21];
z[23] = abb[17] * z[22];
z[24] = abb[45] + -abb[46];
z[25] = -abb[47] + abb[54];
z[26] = abb[49] + z[25];
z[24] = (T(7) / T(2)) * z[24] + (T(4) / T(3)) * z[26];
z[24] = abb[14] * z[24];
z[3] = abb[49] * (T(-17) / T(6)) + abb[48] * (T(-1) / T(6)) + abb[46] * (T(2) / T(3)) + abb[53] * (T(5) / T(3)) + -z[3] + z[5];
z[3] = abb[7] * z[3];
z[5] = 5 * abb[46];
z[26] = -abb[47] + abb[50];
z[27] = abb[48] + z[5] + -z[26];
z[28] = -z[11] + (T(1) / T(2)) * z[27];
z[29] = -abb[51] + (T(1) / T(3)) * z[28];
z[29] = abb[5] * z[29];
z[30] = abb[44] + -abb[55];
z[31] = abb[45] + z[30];
z[31] = abb[0] * z[31];
z[32] = abb[46] + -abb[52];
z[33] = -abb[55] + z[32];
z[34] = abb[49] + (T(1) / T(3)) * z[33];
z[34] = abb[53] * (T(-1) / T(3)) + abb[44] * (T(2) / T(3)) + (T(1) / T(2)) * z[34];
z[34] = abb[1] * z[34];
z[35] = abb[23] + abb[25];
z[36] = abb[27] + z[35];
z[37] = (T(1) / T(6)) * z[36];
z[38] = abb[57] * z[37];
z[1] = z[1] + z[2] + z[3] + z[4] + (T(8) / T(3)) * z[18] + (T(-1) / T(3)) * z[23] + z[24] + z[29] + (T(-13) / T(6)) * z[31] + 13 * z[34] + z[38];
z[2] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = z[1] * z[2];
z[3] = z[9] + z[26];
z[4] = (T(1) / T(2)) * z[3];
z[24] = abb[51] + -abb[55];
z[29] = abb[53] + z[4] + z[11] + z[24];
z[34] = abb[7] * z[29];
z[38] = -abb[45] + z[7];
z[39] = -z[24] + z[38];
z[40] = abb[2] * z[39];
z[30] = abb[53] + z[30] + z[32];
z[41] = abb[1] * z[30];
z[42] = abb[48] + z[26];
z[21] = -abb[52] + z[21];
z[43] = z[21] + (T(1) / T(2)) * z[42];
z[44] = abb[16] * z[43];
z[34] = -z[34] + -z[40] + z[41] + -z[44];
z[45] = 3 * abb[53];
z[46] = 3 * abb[51];
z[4] = 3 * abb[55] + -z[4] + -z[45] + -z[46];
z[47] = -abb[4] * z[4];
z[48] = abb[44] + -abb[45];
z[49] = z[32] + z[48];
z[49] = abb[3] * z[49];
z[50] = 3 * z[17];
z[51] = abb[44] + z[24];
z[52] = abb[9] * z[51];
z[53] = -abb[44] + -z[38];
z[53] = abb[8] * z[53];
z[54] = -abb[26] + abb[27];
z[55] = (T(1) / T(2)) * z[54];
z[56] = -abb[57] * z[55];
z[47] = z[23] + 2 * z[31] + z[34] + z[47] + z[49] + -z[50] + -3 * z[52] + z[53] + z[56];
z[47] = abb[29] * z[47];
z[49] = 2 * abb[55];
z[53] = -z[10] + z[49];
z[56] = 2 * abb[44];
z[57] = -z[26] + -z[53] + z[56];
z[57] = abb[8] * z[57];
z[58] = 2 * abb[51];
z[59] = z[26] + z[58];
z[60] = -z[10] + z[59];
z[61] = abb[15] * z[60];
z[62] = 2 * z[14] + z[61];
z[63] = 2 * abb[3];
z[64] = z[30] * z[63];
z[65] = 2 * abb[52];
z[66] = -abb[46] + z[65];
z[67] = -z[42] + z[66];
z[68] = abb[16] * z[67];
z[69] = 2 * z[52];
z[70] = z[68] + -z[69];
z[57] = z[57] + -z[62] + -z[64] + -z[70];
z[64] = abb[30] * z[57];
z[71] = abb[55] + z[11];
z[32] = abb[44] + -z[32] + -z[45] + z[71];
z[72] = abb[30] * z[32];
z[73] = abb[31] * z[30];
z[72] = -z[72] + z[73];
z[73] = 2 * abb[1];
z[72] = z[72] * z[73];
z[64] = -z[64] + z[72];
z[72] = -abb[3] + abb[8];
z[60] = z[60] * z[72];
z[73] = -abb[49] + abb[53];
z[74] = z[24] + z[73];
z[74] = abb[15] * z[74];
z[75] = z[14] + 2 * z[74];
z[76] = 6 * abb[53];
z[77] = 6 * abb[51];
z[3] = -6 * abb[55] + z[3] + z[76] + z[77];
z[78] = abb[4] * z[3];
z[60] = z[60] + 2 * z[75] + -z[78];
z[75] = abb[33] * z[60];
z[79] = 2 * abb[45];
z[80] = -z[49] + z[58] + z[79];
z[81] = -z[9] + z[26];
z[82] = z[80] + z[81];
z[82] = abb[3] * z[82];
z[71] = abb[45] + z[7] + -z[46] + z[71];
z[83] = 2 * abb[2];
z[71] = z[71] * z[83];
z[84] = abb[47] + z[9];
z[85] = 2 * abb[54];
z[86] = -abb[50] + z[85];
z[87] = z[84] + -z[86];
z[88] = abb[17] * z[87];
z[71] = z[71] + z[88];
z[89] = 2 * z[17] + z[61];
z[90] = 2 * abb[8];
z[91] = z[39] * z[90];
z[82] = z[71] + z[82] + z[89] + z[91];
z[82] = abb[34] * z[82];
z[91] = abb[47] + abb[48];
z[92] = abb[50] + z[91];
z[92] = (T(1) / T(2)) * z[92];
z[93] = -abb[54] + z[21] + z[92];
z[94] = -z[49] + z[56];
z[95] = z[79] + z[93] + z[94];
z[72] = -z[72] * z[95];
z[39] = z[39] * z[83];
z[83] = -z[23] + z[39];
z[69] = z[44] + z[69] + -z[72] + -z[83] + -z[89];
z[72] = abb[31] * z[69];
z[89] = 4 * abb[49];
z[95] = z[53] + -z[59] + -z[89];
z[96] = abb[34] * z[95];
z[97] = abb[30] * z[95];
z[98] = abb[33] * z[95];
z[98] = -z[96] + -z[97] + z[98];
z[98] = abb[7] * z[98];
z[99] = -z[35] + z[54];
z[100] = abb[22] + abb[24];
z[99] = (T(-1) / T(2)) * z[99] + z[100];
z[101] = -abb[57] * z[99];
z[41] = 2 * z[41] + -z[69] + z[101];
z[41] = abb[32] * z[41];
z[69] = abb[31] * (T(1) / T(2));
z[101] = abb[26] * z[69];
z[102] = -abb[34] + z[69];
z[103] = abb[27] * z[102];
z[104] = abb[26] * abb[34];
z[101] = z[101] + -z[103] + -z[104];
z[69] = abb[30] + z[69];
z[103] = -abb[33] + z[69];
z[103] = z[35] * z[103];
z[105] = abb[30] + abb[31];
z[106] = -abb[34] + z[105];
z[100] = z[100] * z[106];
z[100] = z[100] + z[101] + z[103];
z[103] = abb[57] * z[100];
z[41] = z[41] + z[47] + -z[64] + z[72] + z[75] + z[82] + z[98] + z[103];
z[41] = abb[29] * z[41];
z[47] = 5 * abb[50];
z[72] = abb[47] + -z[9] + z[47];
z[75] = z[72] + -z[76] + -z[89];
z[98] = abb[6] * z[75];
z[103] = -abb[3] * z[95];
z[107] = 3 * abb[50];
z[24] = z[8] + -z[24] + -z[107];
z[24] = z[24] * z[90];
z[24] = z[24] + z[62] + z[98] + z[103];
z[24] = abb[33] * z[24];
z[72] = -z[11] + -z[45] + (T(1) / T(2)) * z[72];
z[72] = abb[6] * z[72];
z[103] = 3 * abb[44];
z[108] = -z[103] + z[107];
z[109] = 2 * abb[48] + -z[11] + -z[65] + z[108];
z[109] = abb[12] * z[109];
z[72] = z[72] + -z[109];
z[20] = z[20] + z[21] + z[48];
z[48] = abb[3] * z[20];
z[110] = (T(1) / T(2)) * z[81];
z[111] = abb[45] + -z[45] + z[56] + z[110];
z[111] = abb[8] * z[111];
z[112] = abb[24] + z[35];
z[113] = (T(1) / T(2)) * z[112];
z[114] = -abb[57] * z[113];
z[34] = -z[31] + z[34] + z[48] + -z[72] + z[111] + z[114];
z[34] = abb[32] * z[34];
z[48] = -abb[54] + -z[56] + z[79];
z[107] = z[91] + -z[107];
z[21] = z[21] + -z[48] + (T(1) / T(2)) * z[107];
z[21] = abb[8] * z[21];
z[48] = abb[52] + abb[46] * (T(-3) / T(2)) + z[48] + z[92];
z[48] = abb[3] * z[48];
z[21] = z[21] + z[44] + z[48] + z[83];
z[21] = abb[31] * z[21];
z[48] = z[35] * z[69];
z[69] = abb[30] + abb[34];
z[83] = abb[24] * z[69];
z[92] = abb[33] + -abb[34];
z[107] = -abb[30] + z[92];
z[107] = abb[22] * z[107];
z[48] = z[48] + z[83] + -z[101] + -z[107];
z[83] = abb[57] * z[48];
z[101] = abb[31] * z[95];
z[97] = z[97] + z[101];
z[101] = -z[96] + z[97];
z[111] = 3 * abb[49];
z[114] = abb[51] + -abb[53];
z[115] = z[9] + z[111] + -z[114];
z[116] = 4 * abb[33];
z[115] = z[115] * z[116];
z[115] = -z[101] + z[115];
z[115] = abb[7] * z[115];
z[117] = 2 * abb[31];
z[118] = z[31] * z[117];
z[21] = z[21] + z[24] + z[34] + -z[64] + -z[82] + z[83] + z[115] + z[118];
z[21] = abb[32] * z[21];
z[24] = abb[33] * (T(1) / T(2)) + -z[105];
z[24] = abb[33] * z[24];
z[34] = abb[30] * z[105];
z[64] = prod_pow(abb[31], 2);
z[24] = -abb[38] + -z[24] + -z[34] + (T(-1) / T(2)) * z[64];
z[24] = z[24] * z[35];
z[83] = prod_pow(abb[34], 2);
z[115] = abb[37] + z[83];
z[118] = abb[31] * abb[34];
z[118] = abb[35] + -z[115] + z[118];
z[106] = abb[33] * z[106];
z[34] = -z[34] + z[106] + -z[118];
z[34] = abb[22] * z[34];
z[106] = abb[31] + -abb[34];
z[119] = abb[33] * z[106];
z[120] = abb[31] + abb[30] * (T(1) / T(2));
z[120] = abb[30] * z[120];
z[118] = z[118] + -z[119] + z[120];
z[118] = abb[24] * z[118];
z[102] = abb[31] * z[102];
z[102] = (T(1) / T(2)) * z[83] + z[102];
z[102] = abb[27] * z[102];
z[120] = abb[26] * abb[31];
z[120] = -z[104] + z[120];
z[120] = abb[33] * z[120];
z[115] = abb[26] * z[115];
z[121] = abb[28] * abb[39];
z[122] = abb[31] * z[104];
z[24] = z[24] + z[34] + -z[102] + z[115] + -z[118] + z[120] + (T(1) / T(2)) * z[121] + -z[122];
z[34] = abb[57] * z[24];
z[102] = z[49] + z[58];
z[115] = 3 * z[26];
z[118] = 8 * abb[49];
z[120] = -z[9] + -z[76] + z[102] + z[115] + z[118];
z[121] = -abb[34] * z[120];
z[122] = z[26] + z[77];
z[123] = 3 * abb[48];
z[124] = 3 * abb[46];
z[125] = z[123] + -z[124];
z[126] = z[8] + z[49];
z[118] = -z[118] + z[122] + z[125] + -z[126];
z[127] = abb[30] * z[118];
z[111] = z[26] + z[111] + z[114];
z[128] = abb[31] * z[111];
z[129] = abb[46] + abb[50];
z[91] = z[91] + 11 * z[129];
z[129] = 5 * abb[53];
z[130] = 5 * abb[51];
z[91] = 16 * abb[49] + -abb[55] + (T(1) / T(2)) * z[91] + -z[129] + -z[130];
z[91] = abb[33] * z[91];
z[91] = z[91] + z[121] + z[127] + 4 * z[128];
z[91] = abb[33] * z[91];
z[120] = abb[37] * z[120];
z[29] = -abb[31] * z[29];
z[29] = z[29] + -z[96];
z[29] = abb[31] * z[29];
z[96] = abb[30] * z[97];
z[97] = abb[38] + z[83];
z[97] = z[95] * z[97];
z[75] = abb[36] * z[75];
z[29] = z[29] + z[75] + z[91] + z[96] + z[97] + z[120];
z[29] = abb[7] * z[29];
z[75] = abb[32] * z[99];
z[55] = abb[29] * z[55];
z[55] = z[55] + z[75] + -z[100];
z[55] = abb[29] * z[55];
z[75] = abb[32] * z[113];
z[48] = -z[48] + z[75];
z[48] = abb[32] * z[48];
z[75] = abb[22] + -z[54] + z[112];
z[91] = abb[60] * z[75];
z[2] = z[2] * z[37];
z[37] = abb[36] * z[112];
z[2] = -z[2] + -z[24] + z[37] + z[48] + z[55] + z[91];
z[24] = -abb[59] * z[2];
z[37] = abb[62] * z[36];
z[2] = -z[2] + z[37];
z[37] = abb[58] * z[2];
z[48] = abb[22] + z[35];
z[55] = abb[61] * z[48];
z[2] = z[2] + z[55];
z[2] = abb[56] * z[2];
z[55] = abb[46] + z[8] + -z[26] + z[94];
z[65] = -z[55] + -z[65] + z[123];
z[65] = abb[8] * z[65];
z[91] = -abb[44] + 2 * abb[46] + -abb[48] + -abb[52] + -z[49] + z[129];
z[91] = abb[3] * z[91];
z[44] = 5 * z[14] + -z[44] + z[52] + z[61] + z[65] + z[72] + z[91];
z[44] = abb[30] * z[44];
z[57] = -abb[31] * z[57];
z[44] = z[44] + z[57];
z[44] = abb[30] * z[44];
z[15] = z[15] + -z[58] + z[124];
z[15] = z[15] * z[63];
z[57] = abb[8] * z[95];
z[15] = z[15] + z[57] + z[62];
z[57] = -abb[31] * z[15];
z[47] = -z[47] + -z[84] + z[126];
z[47] = abb[8] * z[47];
z[13] = z[13] * z[63];
z[62] = 4 * z[14];
z[13] = z[13] + z[47] + z[62] + z[98];
z[47] = 4 * abb[55] + z[89];
z[65] = -z[8] + z[47] + -z[77] + -z[81];
z[65] = abb[15] * z[65];
z[72] = -z[13] + z[65];
z[72] = abb[30] * z[72];
z[84] = z[9] + z[115];
z[91] = 6 * abb[49];
z[84] = (T(1) / T(2)) * z[84] + z[91] + z[114];
z[84] = abb[3] * z[84];
z[26] = z[26] + -z[125];
z[26] = (T(1) / T(2)) * z[26] + z[91] + -z[114];
z[26] = abb[8] * z[26];
z[26] = -z[14] + z[26] + 4 * z[74] + z[84];
z[26] = abb[33] * z[26];
z[47] = z[47] + -z[58] + -z[76] + z[81];
z[47] = abb[15] * z[47];
z[5] = -z[5] + -z[42] + z[102];
z[5] = abb[3] * z[5];
z[12] = z[12] + z[59];
z[12] = z[12] * z[90];
z[5] = z[5] + z[12] + -z[47];
z[12] = -abb[34] * z[5];
z[12] = z[12] + z[26] + z[57] + z[72];
z[12] = abb[33] * z[12];
z[26] = 10 * abb[50] + z[19] + -z[66] + -z[76] + z[91] + -z[103];
z[26] = abb[8] * z[26];
z[42] = 4 * abb[52];
z[57] = 4 * abb[48] + -z[42] + -z[89] + -z[108];
z[57] = abb[12] * z[57];
z[58] = -abb[3] * z[67];
z[67] = -abb[44] + z[73];
z[72] = abb[1] * z[67];
z[73] = -abb[57] * z[112];
z[26] = z[26] + z[57] + z[58] + z[68] + 6 * z[72] + z[73] + -z[98];
z[26] = abb[36] * z[26];
z[57] = 3 * abb[47] + -abb[50] + z[9] + -z[80] + -z[85];
z[57] = abb[3] * z[57];
z[49] = -abb[45] + -abb[47] + 2 * abb[50] + -abb[54] + -z[49] + z[130];
z[49] = abb[8] * z[49];
z[58] = 10 * abb[49];
z[7] = -11 * abb[45] + 9 * abb[51] + abb[55] + z[7] + -z[58];
z[7] = abb[2] * z[7];
z[7] = z[7] + z[17] + z[23] + z[49] + z[57] + -z[61];
z[7] = z[7] * z[83];
z[49] = abb[44] + -z[46] + z[79] + -z[110];
z[49] = abb[3] * z[49];
z[20] = -abb[8] * z[20];
z[20] = z[20] + z[23] + -z[40] + z[49];
z[20] = abb[31] * z[20];
z[20] = z[20] + z[82];
z[20] = abb[31] * z[20];
z[19] = 10 * abb[46] + -z[6] + -z[19] + -z[77] + -z[86] + z[91];
z[19] = abb[3] * z[19];
z[6] = z[6] + -z[124];
z[23] = -z[6] + 4 * z[25] + z[89];
z[23] = abb[14] * z[23];
z[40] = -abb[45] + -abb[49] + abb[51];
z[49] = 6 * abb[2];
z[40] = z[40] * z[49];
z[49] = abb[8] * z[87];
z[19] = z[19] + -z[23] + z[40] + z[49] + -z[88];
z[23] = z[27] + -z[77] + -z[89];
z[27] = abb[5] + -abb[7];
z[27] = z[23] * z[27];
z[40] = abb[57] + abb[59];
z[36] = z[36] * z[40];
z[27] = -z[19] + z[27] + z[36];
z[27] = abb[62] * z[27];
z[36] = abb[48] + z[59] + -z[66] + z[94];
z[36] = abb[3] * z[36];
z[49] = -z[51] * z[90];
z[36] = z[36] + z[47] + z[49] + z[70];
z[36] = abb[35] * z[36];
z[47] = z[3] * z[69];
z[49] = abb[33] * z[4];
z[47] = z[47] + z[49];
z[47] = abb[33] * z[47];
z[49] = abb[35] + -abb[37];
z[3] = z[3] * z[49];
z[49] = prod_pow(abb[30], 2);
z[49] = z[49] + z[83];
z[4] = z[4] * z[49];
z[3] = z[3] + z[4] + z[47];
z[3] = abb[4] * z[3];
z[4] = abb[38] * z[60];
z[47] = abb[47] + z[79] + -z[86];
z[49] = z[47] + -z[53];
z[49] = abb[8] * z[49];
z[16] = z[16] * z[63];
z[51] = abb[57] * z[75];
z[16] = -z[16] + z[49] + z[51];
z[49] = z[65] + z[78];
z[17] = z[14] + z[17];
z[17] = z[16] + -2 * z[17] + z[49] + -z[71];
z[51] = -abb[60] * z[17];
z[13] = z[13] + -z[49];
z[49] = abb[7] * z[118];
z[40] = abb[58] + z[40];
z[40] = z[40] * z[48];
z[40] = -z[13] + z[40] + z[49];
z[40] = abb[61] * z[40];
z[5] = abb[37] * z[5];
z[48] = -abb[18] + -abb[19];
z[48] = z[48] * z[93];
z[22] = -abb[21] * z[22];
z[43] = -abb[20] * z[43];
z[22] = z[22] + z[43] + z[48];
z[22] = abb[39] * z[22];
z[43] = abb[35] * z[32];
z[32] = -z[32] * z[117];
z[48] = -11 * abb[44] + 9 * abb[53] + -z[33] + -z[58];
z[48] = abb[30] * z[48];
z[32] = z[32] + z[48];
z[32] = abb[30] * z[32];
z[30] = z[30] * z[64];
z[30] = z[30] + z[32] + -2 * z[43];
z[30] = abb[1] * z[30];
z[32] = abb[37] + z[119];
z[32] = z[23] * z[32];
z[43] = z[64] + -z[83];
z[28] = z[28] + -z[46];
z[28] = -z[28] * z[43];
z[28] = z[28] + z[32];
z[28] = abb[5] * z[28];
z[6] = z[6] + z[11] + 2 * z[25];
z[6] = abb[14] * z[6];
z[11] = -z[6] * z[43];
z[25] = -z[31] * z[64];
z[1] = abb[63] + z[1] + z[2] + z[3] + z[4] + z[5] + z[7] + z[11] + z[12] + z[20] + z[21] + z[22] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[34] + z[36] + z[37] + z[40] + z[41] + z[44] + z[51];
z[2] = 4 * z[52] + z[61] + -z[78];
z[3] = -z[62] + z[109];
z[0] = -z[0] + z[42] + z[55];
z[0] = abb[8] * z[0];
z[4] = -4 * abb[53] + abb[55] + -z[9] + z[56];
z[4] = z[4] * z[63];
z[0] = z[0] + -z[2] + 2 * z[3] + z[4] + -z[98];
z[0] = abb[30] * z[0];
z[3] = -abb[33] + z[105];
z[3] = -z[3] * z[35];
z[4] = abb[26] * abb[33];
z[5] = abb[24] * z[92];
z[7] = abb[27] * z[106];
z[3] = z[3] + z[4] + z[5] + -z[7] + -z[104] + z[107];
z[4] = -abb[57] * z[3];
z[5] = z[8] + -z[38] + -z[103];
z[5] = abb[8] * z[5];
z[7] = -abb[45] + abb[53] + z[9];
z[7] = abb[3] * z[7];
z[5] = z[5] + z[7] + -z[18] + -z[109];
z[7] = z[39] + z[88];
z[8] = abb[7] * z[95];
z[9] = 4 * z[72];
z[11] = -abb[24] + z[54];
z[12] = -abb[57] * z[11];
z[5] = 2 * z[5] + -z[7] + z[8] + z[9] + z[12] + z[98];
z[5] = abb[32] * z[5];
z[8] = -4 * abb[45] + -z[10] + z[122];
z[8] = abb[3] * z[8];
z[10] = z[10] + -z[47];
z[10] = abb[8] * z[10];
z[8] = z[7] + z[8] + z[10] + 2 * z[18];
z[8] = abb[31] * z[8];
z[10] = abb[33] * z[15];
z[12] = -z[111] * z[116];
z[12] = z[12] + -z[101];
z[12] = abb[7] * z[12];
z[6] = z[6] * z[117];
z[15] = 5 * abb[44] + z[33] + -z[45] + z[89];
z[15] = abb[30] * z[15];
z[18] = -abb[31] * z[67];
z[15] = z[15] + z[18];
z[15] = abb[1] * z[15];
z[18] = abb[5] * z[23];
z[20] = abb[31] + -abb[33];
z[20] = z[18] * z[20];
z[0] = z[0] + z[4] + z[5] + z[6] + z[8] + z[10] + z[12] + 4 * z[15] + z[20] + -z[82];
z[0] = m1_set::bc<T>[0] * z[0];
z[4] = abb[32] * z[11];
z[3] = z[3] + z[4];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = abb[41] + abb[42];
z[4] = z[4] * z[35];
z[5] = abb[27] * abb[42];
z[6] = abb[22] * abb[41];
z[4] = z[4] + z[5] + z[6];
z[5] = abb[29] * m1_set::bc<T>[0];
z[6] = z[5] * z[75];
z[3] = z[3] + -z[4] + z[6];
z[6] = abb[40] * z[75];
z[6] = z[3] + z[6];
z[8] = -abb[56] + -abb[58];
z[6] = z[6] * z[8];
z[8] = z[18] + -z[19];
z[8] = abb[42] * z[8];
z[10] = z[14] + z[50];
z[2] = z[2] + z[7] + z[9] + 2 * z[10] + -z[16] + -4 * z[31];
z[2] = z[2] * z[5];
z[3] = -abb[59] * z[3];
z[5] = -abb[41] * z[13];
z[7] = -abb[59] * z[75];
z[7] = z[7] + -z[17];
z[7] = abb[40] * z[7];
z[9] = abb[41] * z[118];
z[10] = -abb[42] * z[23];
z[9] = z[9] + z[10];
z[9] = abb[7] * z[9];
z[4] = abb[57] * z[4];
z[0] = abb[43] + z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_600_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("65.879166082837796935572721603555096999493718859701565104530342898"),stof<T>("-62.043725838942075668588093994512772473442811046282338124687551489")}, std::complex<T>{stof<T>("11.818739223413243445745795987723437790666512983892190367890315121"),stof<T>("44.754399886010530852318458314120175237537482781549564337076155391")}, std::complex<T>{stof<T>("-13.021270994015708623865005644429316643937731182069318685197577669"),stof<T>("-40.770881982060263797409294286963742228363931147405017681445075007")}, std::complex<T>{stof<T>("-11.595246956722915450066907220024011012509770083197367548360795171"),stof<T>("-8.550799722671620200528872298323175527394160533667792478950411291")}, std::complex<T>{stof<T>("-5.0868616221304144532389323202907086744574045354542976972389888906"),stof<T>("-6.3356073919658355856693634081766835552946287252264899872358874776")}, std::complex<T>{stof<T>("-18.688491362182091527113373370845541383594268123669497648083374903"),stof<T>("-42.699832963396815448111803220809056052745503021421371719876539208")}, std::complex<T>{stof<T>("3.5233541977180355023414680173768846980898456852559759885164864596"),stof<T>("-11.5317422799914715205284351009741972532504716991494846242902983441")}, std::complex<T>{stof<T>("14.235614369537093677332968124518190850967208527247491741360852382"),stof<T>("24.811106568733956045617349238159990093643421220443962861420478247")}, std::complex<T>{stof<T>("-63.301908878788412641598797524714208892981284069846555517884539182"),stof<T>("63.539658901733851567275641798393917143682286505912596354835844172")}, std::complex<T>{stof<T>("28.047142876499479648595544777175144846893072748278675484952211386"),stof<T>("13.006971778289777983726806804056996473558405155221492303361916016")}, std::complex<T>{stof<T>("-15.249364149174099020409427900445254981112084466904179619284153667"),stof<T>("-8.439689959568424838107098532890253955337796255698246480042585109")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_600_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_600_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(112)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (2 * (112 * m1_set::bc<T>[1] + -84 * m1_set::bc<T>[2] + 56 * m1_set::bc<T>[4] + -49 * v[1] + -35 * v[2] + 21 * v[3] + 36 * v[4] + 7 * v[5]) + 7 * (6 * v[0] + 8 * m1_set::bc<T>[1] * v[0] + -21 * m1_set::bc<T>[2] * v[0] + 8 * m1_set::bc<T>[1] * v[1] + m1_set::bc<T>[2] * v[1] + 8 * m1_set::bc<T>[1] * v[2] + -9 * m1_set::bc<T>[2] * v[2] + -8 * m1_set::bc<T>[1] * v[3] + 3 * m1_set::bc<T>[2] * v[3] + -16 * m1_set::bc<T>[1] * v[4] + 12 * m1_set::bc<T>[2] * v[4] + 4 * m1_set::bc<T>[4] * (v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -8 * m1_set::bc<T>[1] * v[5] + 9 * m1_set::bc<T>[2] * v[5]))) / prod_pow(tend, 2);
c[1] = (-(4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[0] + -1 * v[1] + v[2] + v[3] + -1 * v[5])) / tend;


		return (abb[47] + -2 * abb[49] + -abb[50] + -2 * abb[51] + abb[55]) * (-2 * t * c[0] + c[1]) * (T(-1) / T(2));
	}
	{
T z[6];
z[0] = prod_pow(abb[34], 2);
z[0] = 2 * abb[35] + -2 * abb[38] + -5 * z[0];
z[1] = abb[49] + abb[51];
z[1] = abb[47] + -abb[50] + abb[55] + -2 * z[1];
z[0] = z[0] * z[1];
z[2] = abb[34] * z[1];
z[3] = abb[33] * z[1];
z[4] = z[2] + -z[3];
z[5] = abb[29] + abb[31] + -abb[32];
z[5] = 2 * z[5];
z[4] = z[4] * z[5];
z[2] = 4 * z[2] + z[3];
z[2] = abb[33] * z[2];
z[1] = 4 * z[1];
z[1] = -abb[37] * z[1];
z[0] = z[0] + z[1] + z[2] + z[4];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_600_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[47] + -2 * abb[49] + -abb[50] + -2 * abb[51] + abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[33] + -abb[34];
z[1] = abb[49] + abb[51];
z[1] = abb[47] + -abb[50] + abb[55] + -2 * z[1];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_600_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-91.839574159149954502048497615416143478191699154082212461626411046"),stof<T>("13.437294422386908445724416362317914335742764758470218670248136872")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,64> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k), T{0}};
abb[43] = SpDLog_f_4_600_W_17_Im(t, path, abb);
abb[63] = SpDLog_f_4_600_W_17_Re(t, path, abb);

                    
            return f_4_600_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_600_DLogXconstant_part(base_point<T>, kend);
	value += f_4_600_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_600_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_600_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_600_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_600_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_600_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_600_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
