/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_730.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_730_abbreviated (const std::array<T,66>& abb) {
T z[153];
z[0] = 11 * abb[55];
z[1] = abb[57] + z[0];
z[2] = 4 * abb[56];
z[3] = z[1] + z[2];
z[4] = abb[15] * z[3];
z[5] = abb[57] + abb[58];
z[6] = abb[55] + abb[56];
z[7] = z[5] + -z[6];
z[8] = abb[4] * z[7];
z[9] = z[4] + -z[8];
z[10] = 5 * abb[56];
z[11] = 2 * abb[57];
z[12] = z[10] + z[11];
z[12] = abb[55] + abb[58] * (T(2) / T(3)) + (T(1) / T(3)) * z[12];
z[12] = abb[5] * z[12];
z[13] = abb[56] + abb[58];
z[14] = abb[2] * z[13];
z[15] = 2 * z[14];
z[16] = abb[55] + abb[57];
z[17] = abb[1] * z[16];
z[12] = z[12] + z[15] + (T(5) / T(3)) * z[17];
z[18] = abb[25] * abb[59];
z[19] = abb[56] + abb[57];
z[20] = abb[55] + z[19];
z[21] = abb[58] + z[20];
z[22] = abb[9] * z[21];
z[18] = z[18] + -z[22];
z[23] = abb[12] * z[19];
z[24] = z[18] + z[23];
z[25] = 2 * abb[50] + -abb[54];
z[26] = -abb[53] + z[25];
z[27] = -abb[51] + -z[26];
z[28] = 4 * abb[60];
z[29] = abb[52] + z[28];
z[30] = z[27] + -z[29];
z[31] = abb[22] * z[30];
z[32] = abb[27] * abb[59];
z[32] = z[31] + z[32];
z[33] = abb[14] * z[20];
z[34] = -z[32] + z[33];
z[35] = abb[24] + abb[26];
z[36] = abb[23] + z[35];
z[37] = 2 * abb[59];
z[38] = z[36] * z[37];
z[39] = 10 * abb[55];
z[40] = 7 * abb[57];
z[41] = 20 * abb[56] + -z[40];
z[41] = abb[58] * (T(4) / T(3)) + z[39] + (T(1) / T(3)) * z[41];
z[41] = abb[6] * z[41];
z[42] = 5 * abb[55];
z[43] = 4 * abb[57];
z[44] = 13 * abb[56] + z[43];
z[44] = abb[58] * (T(16) / T(3)) + z[42] + (T(2) / T(3)) * z[44];
z[44] = abb[7] * z[44];
z[45] = abb[49] + abb[52];
z[26] = abb[51] * (T(17) / T(3)) + z[26];
z[26] = abb[60] * (T(-16) / T(3)) + 2 * z[26] + (T(29) / T(3)) * z[45];
z[26] = abb[19] * z[26];
z[46] = 4 * abb[58];
z[47] = abb[57] + z[46];
z[48] = 8 * abb[56];
z[49] = abb[55] * (T(19) / T(3)) + -z[47] + z[48];
z[49] = abb[0] * z[49];
z[50] = abb[55] + -abb[57];
z[51] = abb[3] * z[50];
z[52] = abb[55] + z[13];
z[53] = abb[13] * z[52];
z[54] = abb[55] + -abb[56];
z[55] = abb[17] * z[54];
z[56] = -abb[51] + z[25];
z[56] = 2 * z[56];
z[57] = abb[49] + abb[53] + z[56];
z[58] = abb[52] + (T(1) / T(3)) * z[57];
z[58] = abb[21] * z[58];
z[59] = 13 * abb[51] + 5 * z[25];
z[59] = -7 * abb[53] + 2 * z[59];
z[59] = 10 * abb[52] + abb[49] * (T(26) / T(3)) + (T(1) / T(3)) * z[59];
z[59] = abb[20] * z[59];
z[9] = (T(-2) / T(3)) * z[9] + 4 * z[12] + (T(10) / T(3)) * z[24] + z[26] + (T(4) / T(3)) * z[34] + z[38] + z[41] + z[44] + z[49] + (T(20) / T(3)) * z[51] + (T(-16) / T(3)) * z[53] + (T(8) / T(3)) * z[55] + 2 * z[58] + z[59];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[12] = -abb[55] + abb[58];
z[24] = z[12] + -z[19];
z[26] = abb[8] * z[24];
z[34] = z[12] + z[19];
z[41] = abb[16] * z[34];
z[44] = abb[28] + z[35];
z[49] = abb[59] * z[44];
z[49] = z[26] + z[41] + z[49];
z[58] = abb[11] * z[12];
z[59] = 2 * z[58];
z[12] = abb[5] * z[12];
z[12] = z[12] + z[15] + z[59];
z[60] = -abb[56] + abb[57];
z[61] = 3 * abb[55];
z[62] = 3 * abb[58] + -z[60] + z[61];
z[62] = abb[0] * z[62];
z[32] = -z[32] + 2 * z[55];
z[63] = abb[19] * z[30];
z[64] = 2 * abb[7];
z[65] = -z[54] * z[64];
z[12] = 2 * z[12] + z[32] + -z[49] + -z[62] + z[63] + z[65];
z[12] = abb[38] * z[12];
z[65] = abb[20] * z[30];
z[32] = z[32] + -z[65];
z[66] = 4 * z[53];
z[67] = 8 * z[14] + z[32] + -z[66];
z[68] = z[18] + -z[63];
z[69] = 3 * abb[56];
z[70] = 2 * abb[58];
z[71] = abb[55] + z[69] + z[70];
z[71] = z[64] * z[71];
z[72] = abb[5] * z[21];
z[73] = abb[59] * z[36];
z[74] = abb[0] * z[52];
z[75] = abb[56] + z[61];
z[76] = abb[6] * z[75];
z[71] = z[67] + z[68] + z[71] + z[72] + z[73] + -4 * z[74] + 2 * z[76];
z[72] = abb[63] * z[71];
z[73] = 2 * abb[56];
z[74] = z[50] + z[73];
z[74] = abb[18] * z[74];
z[76] = 2 * abb[55];
z[77] = -z[19] + -z[76];
z[77] = abb[6] * z[77];
z[78] = abb[7] * z[73];
z[79] = abb[10] * z[60];
z[80] = 3 * z[79];
z[81] = 2 * z[17];
z[77] = -z[23] + z[74] + z[77] + -z[78] + z[80] + -z[81];
z[77] = abb[42] * z[77];
z[82] = 2 * abb[51];
z[83] = 2 * abb[60] + -z[45] + -z[82];
z[84] = abb[19] * z[83];
z[85] = abb[21] * z[83];
z[86] = abb[57] + z[76];
z[87] = abb[15] * z[86];
z[85] = z[85] + z[87];
z[84] = z[84] + z[85];
z[83] = abb[20] * z[83];
z[87] = -z[23] + z[83];
z[88] = z[84] + z[87];
z[89] = 4 * abb[55];
z[90] = abb[56] + z[11] + z[89];
z[90] = abb[6] * z[90];
z[91] = -abb[57] + z[69];
z[91] = abb[7] * z[91];
z[20] = abb[0] * z[20];
z[20] = -5 * z[17] + 2 * z[20] + z[33] + z[88] + -z[90] + -z[91];
z[91] = abb[62] * z[20];
z[92] = z[13] + z[50];
z[92] = abb[5] * z[92];
z[93] = -z[15] + z[92];
z[94] = -abb[6] + abb[7];
z[94] = z[34] * z[94];
z[95] = 2 * z[23];
z[96] = z[26] + z[95];
z[97] = abb[59] * z[35];
z[94] = -z[59] + -z[93] + z[94] + z[96] + z[97];
z[94] = abb[43] * z[94];
z[98] = abb[7] * z[7];
z[59] = 2 * z[51] + -z[59] + z[98];
z[49] = z[8] + -z[49] + -z[59] + z[92];
z[92] = abb[40] * z[49];
z[21] = abb[6] * z[21];
z[21] = z[21] + -z[96];
z[93] = z[21] + z[93];
z[98] = abb[23] + abb[28];
z[99] = abb[59] * z[98];
z[99] = z[41] + z[93] + z[99];
z[100] = abb[64] * z[99];
z[101] = abb[41] + abb[43];
z[101] = -abb[42] + 5 * abb[61] + 2 * z[101];
z[101] = z[51] * z[101];
z[12] = z[12] + z[72] + z[77] + -z[91] + -z[92] + z[94] + z[100] + z[101];
z[72] = abb[25] * z[37];
z[22] = 2 * z[22] + -z[72];
z[72] = 4 * z[58];
z[77] = z[22] + z[63] + -z[72];
z[75] = -z[5] + z[75];
z[91] = abb[0] * z[75];
z[92] = 2 * z[41];
z[94] = abb[57] + -z[13] + -z[61];
z[94] = abb[5] * z[94];
z[100] = -z[54] + z[70];
z[100] = z[64] * z[100];
z[101] = -z[35] + -2 * z[98];
z[101] = abb[59] * z[101];
z[21] = 6 * z[14] + -z[21] + -z[66] + z[77] + -z[91] + -z[92] + z[94] + z[100] + z[101];
z[21] = abb[37] * z[21];
z[94] = z[18] + z[99];
z[99] = 2 * abb[36];
z[100] = z[94] * z[99];
z[21] = z[21] + z[100];
z[21] = abb[37] * z[21];
z[80] = 2 * z[74] + z[80];
z[7] = abb[5] * z[7];
z[7] = z[7] + z[80];
z[100] = 2 * z[8];
z[101] = 3 * abb[51] + -z[25];
z[102] = 2 * z[101];
z[29] = -abb[49] + abb[53] + z[29] + -z[102];
z[103] = abb[19] * z[29];
z[104] = 3 * abb[57];
z[105] = -z[61] + z[104];
z[106] = -z[73] + z[105];
z[107] = -abb[6] * z[106];
z[108] = -abb[55] + z[40];
z[109] = -z[2] + z[108];
z[109] = abb[7] * z[109];
z[7] = 2 * z[7] + -z[38] + -z[100] + -z[103] + z[107] + z[109];
z[7] = abb[41] * z[7];
z[107] = 2 * z[79];
z[109] = z[33] + z[107];
z[110] = abb[0] * abb[57];
z[111] = -abb[56] + z[11];
z[112] = abb[6] * z[111];
z[113] = -abb[7] * abb[56];
z[112] = z[17] + z[87] + -z[109] + z[110] + z[112] + z[113];
z[112] = prod_pow(abb[33], 2) * z[112];
z[113] = 6 * abb[56];
z[114] = 11 * abb[57];
z[115] = -z[61] + z[113] + z[114];
z[115] = abb[6] * z[115];
z[98] = z[37] * z[98];
z[116] = 2 * z[26];
z[98] = z[98] + -z[116];
z[117] = abb[7] * z[19];
z[118] = abb[5] * z[19];
z[115] = 10 * z[23] + -z[98] + -z[115] + 6 * z[117] + 4 * z[118];
z[117] = -z[92] + z[115];
z[117] = abb[61] * z[117];
z[59] = z[59] + -z[93] + z[97];
z[59] = prod_pow(abb[36], 2) * z[59];
z[93] = -abb[57] + z[2];
z[119] = z[42] + -z[70] + z[93];
z[120] = -abb[61] * z[119];
z[121] = abb[43] * z[6];
z[122] = 2 * z[6];
z[123] = abb[42] * z[122];
z[120] = z[120] + -4 * z[121] + z[123];
z[120] = abb[0] * z[120];
z[121] = abb[21] * z[29];
z[123] = 7 * abb[55] + z[93];
z[123] = abb[15] * z[123];
z[121] = z[121] + -z[123];
z[123] = abb[41] + abb[61];
z[123] = z[121] * z[123];
z[22] = -abb[64] * z[22];
z[124] = abb[42] + abb[61];
z[124] = z[33] * z[124];
z[29] = abb[20] * z[29];
z[125] = -abb[41] + abb[61];
z[125] = z[29] * z[125];
z[7] = z[7] + 2 * z[12] + z[21] + z[22] + z[59] + z[112] + z[117] + z[120] + z[123] + 4 * z[124] + z[125];
z[12] = 5 * abb[51];
z[21] = z[12] + -z[25];
z[22] = 3 * abb[49];
z[59] = 8 * abb[60];
z[112] = abb[52] + -abb[53];
z[21] = 2 * z[21] + z[22] + -z[59] + z[112];
z[21] = abb[21] * z[21];
z[105] = -z[2] + z[105];
z[105] = abb[15] * z[105];
z[21] = z[21] + -z[105];
z[105] = abb[27] * z[37];
z[31] = 2 * z[31] + -4 * z[55] + z[105];
z[55] = 4 * z[14];
z[105] = z[55] + z[81];
z[24] = abb[5] * z[24];
z[117] = z[24] + z[105] + z[107];
z[40] = -z[40] + z[73] + -z[76];
z[40] = abb[7] * z[40];
z[120] = 4 * abb[51];
z[123] = 4 * abb[52];
z[124] = -6 * abb[49] + abb[53] + z[28] + -z[120] + -z[123];
z[124] = abb[19] * z[124];
z[125] = 7 * abb[49];
z[126] = 5 * abb[52];
z[127] = 12 * abb[60];
z[128] = -2 * abb[53] + -z[125] + -z[126] + z[127];
z[128] = abb[20] * z[128];
z[129] = 8 * z[23];
z[130] = 8 * z[51] + z[129];
z[131] = 5 * abb[57];
z[132] = z[69] + z[131];
z[132] = z[61] + 2 * z[132];
z[132] = abb[6] * z[132];
z[133] = 8 * abb[58];
z[134] = z[48] + z[133];
z[1] = -z[1] + -z[134];
z[1] = abb[0] * z[1];
z[58] = 8 * z[58];
z[135] = abb[23] * z[37];
z[1] = z[1] + z[21] + -z[31] + z[40] + z[58] + -z[116] + 2 * z[117] + z[124] + z[128] + -z[130] + z[132] + z[135];
z[1] = abb[31] * z[1];
z[40] = 16 * z[51];
z[21] = -z[21] + z[40];
z[117] = 13 * abb[57];
z[124] = 12 * abb[58];
z[128] = z[48] + -z[89] + z[117] + z[124];
z[128] = abb[7] * z[128];
z[132] = 9 * abb[51] + z[25];
z[135] = 8 * abb[49];
z[136] = 3 * abb[53];
z[123] = -20 * abb[60] + z[123] + 2 * z[132] + z[135] + z[136];
z[123] = abb[19] * z[123];
z[137] = 2 * abb[28];
z[138] = z[36] + z[137];
z[138] = z[37] * z[138];
z[139] = 4 * z[41];
z[140] = 16 * z[23];
z[138] = z[138] + z[139] + -z[140];
z[141] = 5 * abb[49];
z[142] = 9 * abb[52];
z[143] = z[141] + z[142];
z[144] = 3 * z[25];
z[145] = 7 * abb[51] + abb[53] + z[144];
z[145] = -z[59] + z[143] + 2 * z[145];
z[145] = abb[20] * z[145];
z[146] = z[31] + 16 * z[53];
z[147] = z[55] + -z[81];
z[148] = abb[58] + z[69];
z[149] = z[16] + z[148];
z[150] = abb[5] * z[149];
z[150] = z[147] + z[150];
z[151] = -abb[55] + z[46];
z[152] = z[2] + z[131] + -z[151];
z[152] = abb[0] * z[152];
z[111] = 13 * abb[55] + z[46] + -8 * z[111];
z[111] = abb[6] * z[111];
z[111] = z[21] + z[111] + z[123] + z[128] + -z[138] + z[145] + -z[146] + 4 * z[150] + z[152];
z[111] = abb[35] * z[111];
z[123] = z[12] + z[45] + -z[144];
z[128] = 28 * abb[60] + -z[136];
z[123] = -2 * z[123] + z[128];
z[123] = abb[19] * z[123];
z[21] = -z[21] + z[123];
z[75] = abb[5] * z[75];
z[75] = z[75] + -z[147];
z[123] = abb[56] + z[43];
z[0] = -z[0] + z[46] + 4 * z[123];
z[0] = abb[6] * z[0];
z[39] = -z[39] + z[117];
z[117] = 12 * abb[56] + z[46];
z[123] = -z[39] + -z[117];
z[123] = abb[7] * z[123];
z[117] = 15 * abb[55] + z[117] + -z[131];
z[117] = abb[0] * z[117];
z[136] = 7 * abb[52];
z[101] = -abb[53] + -z[101];
z[101] = abb[49] + 16 * abb[60] + 2 * z[101] + -z[136];
z[101] = abb[20] * z[101];
z[0] = z[0] + z[21] + z[31] + 4 * z[75] + z[101] + z[117] + z[123] + z[138];
z[0] = abb[32] * z[0];
z[31] = abb[6] * z[122];
z[75] = abb[7] * z[122];
z[24] = -z[24] + z[26] + z[31] + -z[63] + -z[65] + z[75] + z[97];
z[24] = abb[36] * z[24];
z[26] = abb[28] * abb[59];
z[26] = z[26] + z[41];
z[34] = abb[5] * z[34];
z[13] = abb[7] * z[13];
z[13] = 4 * z[13] + -z[26] + z[31] + z[34] + -z[62] + z[67];
z[13] = abb[37] * z[13];
z[31] = abb[6] * z[16];
z[31] = z[31] + z[81];
z[62] = abb[7] * abb[57];
z[31] = -2 * z[31] + z[62] + z[84] + z[110];
z[31] = abb[33] * z[31];
z[24] = -z[13] + z[24] + z[31];
z[0] = z[0] + z[1] + 4 * z[24] + z[111];
z[0] = abb[31] * z[0];
z[1] = z[22] + z[56] + z[136];
z[1] = abb[30] * z[1];
z[22] = abb[51] + z[25];
z[24] = z[22] + z[28] + z[112];
z[24] = abb[5] * z[24];
z[54] = abb[22] * z[54];
z[1] = z[1] + z[24] + -z[54];
z[24] = abb[53] + z[132];
z[24] = abb[49] + -2 * z[24] + z[59] + z[126];
z[24] = abb[6] * z[24];
z[54] = 16 * abb[52] + z[102] + -z[128] + z[135];
z[54] = abb[7] * z[54];
z[56] = -9 * abb[49] + -abb[53] + z[127] + -z[142];
z[56] = abb[0] * z[56];
z[43] = z[43] + z[151];
z[43] = abb[20] * z[43];
z[62] = -z[46] + z[114];
z[62] = abb[19] * z[62];
z[30] = abb[17] * z[30];
z[3] = abb[21] * z[3];
z[63] = abb[29] * z[37];
z[57] = 3 * abb[52] + z[57];
z[67] = abb[15] * z[57];
z[1] = -4 * z[1] + -z[3] + z[24] + -2 * z[30] + -z[43] + z[54] + -z[56] + -z[62] + -z[63] + z[67];
z[3] = abb[65] * z[1];
z[24] = -z[92] + z[121];
z[30] = z[24] + z[130];
z[43] = abb[57] + z[70];
z[54] = z[43] + -z[61] + -z[73];
z[54] = abb[0] * z[54];
z[56] = z[2] + z[108];
z[56] = abb[7] * z[56];
z[62] = abb[6] * z[50];
z[34] = z[34] + 4 * z[62];
z[36] = abb[28] + z[36];
z[62] = z[36] * z[37];
z[34] = -z[30] + -2 * z[34] + -z[54] + -z[56] + z[62] + z[103];
z[56] = -abb[31] + abb[35];
z[56] = z[34] * z[56];
z[62] = -z[42] + z[73] + z[131];
z[62] = abb[6] * z[62];
z[54] = -z[29] + -z[54] + z[62] + -z[129];
z[62] = abb[5] * z[73];
z[62] = z[62] + -z[80];
z[63] = 4 * z[51];
z[48] = abb[7] * z[48];
z[67] = -abb[28] * z[37];
z[48] = z[48] + -z[54] + 2 * z[62] + z[63] + z[67] + -z[92] + z[100];
z[48] = abb[32] * z[48];
z[49] = z[49] * z[99];
z[48] = z[48] + z[49] + z[56];
z[49] = -z[50] + -z[148];
z[49] = abb[5] * z[49];
z[49] = z[49] + z[80];
z[10] = abb[58] + -z[10] + -z[50];
z[10] = z[10] * z[64];
z[35] = z[35] + z[137];
z[35] = z[35] * z[37];
z[10] = -4 * z[8] + z[10] + z[35] + 2 * z[49] + z[54] + -z[72] + z[116] + z[139];
z[10] = abb[34] * z[10];
z[10] = z[10] + 2 * z[48];
z[10] = abb[34] * z[10];
z[35] = -abb[5] * abb[56];
z[37] = -abb[6] * z[6];
z[35] = z[35] + z[37] + -z[78];
z[8] = -z[8] + z[26] + 2 * z[35] + z[65] + z[91];
z[8] = abb[36] * z[8];
z[26] = z[73] + z[86];
z[26] = abb[0] * z[26];
z[35] = abb[56] + z[76];
z[35] = abb[6] * z[35];
z[37] = abb[7] * z[69];
z[37] = z[17] + -z[26] + -z[33] + z[35] + z[37] + -z[87];
z[37] = abb[33] * z[37];
z[8] = z[8] + z[13] + z[37];
z[13] = 3 * z[17];
z[2] = -abb[5] * z[2];
z[2] = z[2] + -z[13] + z[55] + z[80];
z[37] = z[42] + -z[46] + -z[93];
z[37] = abb[0] * z[37];
z[48] = -z[25] + z[82];
z[48] = 4 * z[48] + -z[128] + z[143];
z[48] = abb[20] * z[48];
z[47] = -9 * abb[55] + -10 * abb[56] + -z[47];
z[47] = abb[6] * z[47];
z[2] = 2 * z[2] + 6 * z[33] + z[37] + z[47] + z[48] + -z[63] + -z[95];
z[2] = abb[32] * z[2];
z[2] = z[2] + 4 * z[8];
z[2] = abb[32] * z[2];
z[8] = -z[38] + z[146];
z[37] = 16 * abb[56] + z[124];
z[38] = -z[37] + z[39];
z[38] = abb[7] * z[38];
z[21] = z[8] + -z[21] + z[38];
z[38] = -8 * z[19] + -z[46] + z[61];
z[38] = abb[6] * z[38];
z[39] = 9 * abb[57];
z[47] = z[39] + -z[61] + z[134];
z[47] = abb[0] * z[47];
z[48] = z[15] + z[17];
z[49] = abb[5] * z[6];
z[50] = -z[48] + -z[49];
z[38] = z[21] + z[38] + z[47] + 8 * z[50] + -z[145];
z[38] = abb[32] * z[38];
z[47] = -abb[5] * z[76];
z[50] = -abb[6] * z[42];
z[15] = -z[15] + z[17] + z[47] + z[50];
z[17] = abb[55] + -z[70] + -z[73] + -z[104];
z[17] = abb[7] * z[17];
z[47] = -z[25] + -z[120];
z[47] = -abb[53] + z[28] + -z[45] + 2 * z[47];
z[47] = abb[19] * z[47];
z[50] = z[70] + -z[106];
z[50] = abb[0] * z[50];
z[15] = 2 * z[15] + z[17] + z[47] + z[50] + -z[63] + z[66];
z[15] = abb[35] * z[15];
z[17] = abb[5] * abb[55];
z[6] = abb[7] * z[6];
z[47] = abb[6] * z[76];
z[6] = z[6] + z[17] + z[47];
z[17] = abb[59] * z[36];
z[17] = z[17] + z[41] + z[68];
z[36] = abb[0] * z[149];
z[6] = 2 * z[6] + z[17] + -z[36];
z[36] = -abb[36] + abb[37];
z[6] = z[6] * z[36];
z[6] = z[6] + -z[31];
z[6] = 4 * z[6] + 2 * z[15] + z[38];
z[6] = abb[35] * z[6];
z[15] = z[13] + -z[74] + -z[107];
z[31] = abb[7] * z[60];
z[15] = 2 * z[15] + -z[26] + -3 * z[31] + z[83] + -z[85] + z[90];
z[15] = abb[39] * z[15];
z[0] = z[0] + z[2] + z[3] + z[6] + 2 * z[7] + z[9] + z[10] + 4 * z[15];
z[0] = 4 * z[0];
z[2] = z[5] + z[61] + z[69];
z[3] = -abb[5] * z[2];
z[3] = -z[3] + z[18] + z[105];
z[5] = -z[37] + -z[39] + -z[89];
z[5] = abb[7] * z[5];
z[6] = -z[12] + -z[25];
z[6] = abb[53] + 6 * z[6] + -12 * z[45] + z[127];
z[6] = abb[19] * z[6];
z[7] = z[61] + z[104] + z[134];
z[7] = abb[0] * z[7];
z[9] = -37 * abb[55] + -z[46] + 8 * z[60];
z[9] = abb[6] * z[9];
z[10] = -abb[21] * z[57];
z[3] = -4 * z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + -z[40] + -z[140] + -z[145];
z[3] = abb[35] * z[3];
z[4] = z[76] + z[113] + z[131] + z[133];
z[4] = abb[7] * z[4];
z[5] = 2 * z[22] + -z[28] + z[125] + z[136];
z[5] = abb[20] * z[5];
z[6] = z[48] + -z[79] + z[118];
z[7] = z[42] + z[43] + z[113];
z[7] = abb[0] * z[7];
z[8] = -abb[56] + -z[104];
z[8] = 2 * z[8] + z[42];
z[8] = abb[6] * z[8];
z[9] = 8 * abb[51] + 6 * abb[52] + -abb[53] + -z[59] + z[135];
z[9] = abb[19] * z[9];
z[4] = z[4] + z[5] + 4 * z[6] + z[7] + z[8] + z[9] + z[30] + -8 * z[53] + -z[58] + -z[98];
z[4] = abb[31] * z[4];
z[5] = abb[34] * z[34];
z[4] = z[4] + z[5];
z[2] = abb[6] * z[2];
z[2] = z[2] + z[75];
z[5] = abb[5] * z[52];
z[5] = z[5] + z[14];
z[6] = 2 * abb[23] + z[44];
z[6] = abb[59] * z[6];
z[7] = -abb[0] * z[46];
z[5] = z[2] + 2 * z[5] + z[6] + z[7] + z[32] + z[41] + -z[77] + -z[96];
z[5] = abb[37] * z[5];
z[6] = -abb[0] * z[11];
z[6] = z[6] + z[13] + -z[31] + z[35] + -z[88] + z[109];
z[6] = abb[33] * z[6];
z[7] = z[14] + -z[49];
z[2] = -z[2] + 2 * z[7] + -z[17] + z[65] + z[95];
z[2] = abb[36] * z[2];
z[2] = z[2] + z[5] + z[6];
z[5] = -abb[5] * z[122];
z[5] = z[5] + -z[13] + -z[55];
z[6] = -16 * z[19] + -z[42] + -z[46];
z[6] = abb[6] * z[6];
z[7] = 5 * z[16] + z[133];
z[7] = abb[0] * z[7];
z[8] = -abb[52] + -24 * abb[60] + 6 * z[27] + -z[141];
z[8] = abb[20] * z[8];
z[9] = 4 * z[33];
z[5] = 4 * z[5] + z[6] + z[7] + z[8] + z[9] + z[21] + 12 * z[23];
z[5] = abb[32] * z[5];
z[2] = 4 * z[2] + z[3] + 2 * z[4] + z[5];
z[2] = m1_set::bc<T>[0] * z[2];
z[1] = abb[48] * z[1];
z[3] = abb[46] * z[71];
z[4] = -abb[45] * z[20];
z[5] = abb[47] * z[94];
z[3] = z[3] + z[4] + z[5];
z[4] = -abb[0] * z[119];
z[4] = z[4] + z[9] + z[24] + z[29] + 10 * z[51] + z[115];
z[4] = abb[44] * z[4];
z[1] = z[1] + z[2] + 4 * z[3] + 2 * z[4];
z[1] = 4 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_730_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("366.71658182064655012549820212544697966821569689224242028300132026"),stof<T>("-463.99372989512251111778374526866507402308982708397407383569353053")}, std::complex<T>{stof<T>("56.71431038017606302345634935111493991315403614703649752380668806"),stof<T>("-241.87005465146099098073803902030214801537806703027983084766916971")}, std::complex<T>{stof<T>("544.80316047809251975993824838979684223902791103334672396793524643"),stof<T>("-431.38880921968659189978662376393167593427398053252833532901743695")}, std::complex<T>{stof<T>("278.37379203991850222830076086492774091209156509696147282302292301"),stof<T>("-445.47781284202420605560171910055876801560269121215127977501405098")}, std::complex<T>{stof<T>("-99.014921099286745437808825737091526416860544563524579689863615996"),stof<T>("78.626604988908556280975421599906192312117050439416943202185047484")}, std::complex<T>{stof<T>("-28.357155190088031511728174675557469956577018073518248761903344031"),stof<T>("120.935027325730495490369019510151074007689033515139915423834584853")}, std::complex<T>{stof<T>("-586.01525721670051081469039907855813953243690667700024463975687467"),stof<T>("-235.39343615488790355464049355610880801986912965348325083370554433")}, std::complex<T>{stof<T>("-521.13816757555245468945856655472200201630525095686719179080839255"),stof<T>("-266.50731386606554795050912481636341337020369359764791301070905687")}, std::complex<T>{stof<T>("-139.68247079326783874188966258283854681380990266480417346354699736"),stof<T>("38.36712119461887626899224608230766608849277592237019765053073279")}, std::complex<T>{stof<T>("-17.68839858434445104039361304492411391844401725586889177122439364"),stof<T>("275.77861724080758435233309495824509622497500637597110682504628185")}, std::complex<T>{stof<T>("-61.729515711079394132161346816278729277023458299091893854962041792"),stof<T>("4.158934778451126991283852975154926032255530177715632904202796214")}, std::complex<T>{stof<T>("-241.1876934912924867199857791276962622459319166736650719435822088"),stof<T>("192.68030636022999471292174024655703403475403106579618401563819707")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}, rlog(k.W[196].real()/kbase.W[196].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_730_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_730_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-1288.9977661334690134561900297372666870940718739956569136738676703"),stof<T>("1512.0564806732047622883034154190829996160098486171997371703590634")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,66> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W60(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_4_im(k), f_2_6_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), rlog(kend.W[196].real()/k.W[196].real()), f_2_4_re(k), f_2_6_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k)};

                    
            return f_4_730_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_730_DLogXconstant_part(base_point<T>, kend);
	value += f_4_730_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_730_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_730_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_730_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_730_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_730_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_730_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
