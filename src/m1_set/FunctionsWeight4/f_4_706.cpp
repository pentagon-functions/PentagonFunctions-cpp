/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_706.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_706_abbreviated (const std::array<T,66>& abb) {
T z[98];
z[0] = abb[4] + -abb[5];
z[1] = 2 * abb[2];
z[2] = 2 * abb[1];
z[3] = -2 * abb[3] + abb[10] + z[0] + -z[1] + z[2];
z[3] = abb[28] * z[3];
z[2] = abb[5] + z[2];
z[4] = 2 * abb[4];
z[5] = -abb[3] + -z[1] + z[2] + z[4];
z[5] = abb[29] * z[5];
z[6] = -abb[1] + abb[2];
z[7] = -abb[6] + abb[7];
z[8] = -z[6] + z[7];
z[9] = -abb[3] + abb[4];
z[10] = abb[9] + -abb[10];
z[11] = -2 * z[8] + z[9] + z[10];
z[11] = abb[24] * z[11];
z[12] = 2 * abb[7];
z[13] = abb[0] + abb[11];
z[14] = -abb[2] + z[12] + -z[13];
z[15] = abb[31] * z[14];
z[16] = 2 * abb[6];
z[17] = -abb[11] + z[16];
z[18] = abb[0] + abb[1];
z[19] = z[17] + -z[18];
z[20] = abb[25] * z[19];
z[15] = z[15] + -z[20];
z[21] = abb[10] * abb[31];
z[22] = -abb[29] + abb[30];
z[23] = abb[25] + z[22];
z[24] = abb[9] * z[23];
z[21] = z[15] + z[21] + -z[24];
z[25] = abb[10] + -2 * z[6] + z[9];
z[25] = abb[30] * z[25];
z[26] = z[12] + -z[16];
z[27] = -z[9] + z[26];
z[28] = z[6] + z[27];
z[28] = abb[26] * z[28];
z[29] = 2 * abb[27];
z[30] = z[8] * z[29];
z[3] = z[3] + z[5] + -z[11] + -z[21] + -z[25] + z[28] + -z[30];
z[5] = abb[53] + abb[55];
z[3] = -z[3] * z[5];
z[11] = -abb[0] + abb[1];
z[25] = abb[3] + -abb[5] + z[4] + 2 * z[11];
z[25] = abb[29] * z[25];
z[28] = -abb[6] + z[18];
z[28] = abb[11] + 2 * z[28];
z[30] = abb[4] + abb[5];
z[31] = -abb[9] + z[28] + -z[30];
z[31] = abb[24] * z[31];
z[32] = abb[2] + abb[4];
z[33] = -abb[1] + abb[5];
z[34] = -z[32] + 3 * z[33];
z[34] = abb[30] * z[34];
z[35] = z[6] + z[17];
z[35] = abb[27] * z[35];
z[36] = -abb[0] + abb[4];
z[37] = -z[33] + z[36];
z[38] = abb[28] * z[37];
z[38] = 2 * z[38];
z[24] = z[20] + z[24] + z[25] + z[31] + z[34] + z[35] + z[38];
z[25] = -abb[54] * z[24];
z[31] = 3 * abb[0];
z[34] = -z[4] + z[31];
z[35] = -abb[3] + -z[17] + z[33] + z[34];
z[39] = -abb[26] * z[35];
z[24] = -z[24] + z[39];
z[24] = abb[56] * z[24];
z[39] = abb[47] + -abb[52];
z[40] = abb[49] + -abb[50];
z[41] = z[39] + z[40];
z[42] = abb[30] * z[41];
z[43] = abb[29] * z[41];
z[44] = z[42] + -z[43];
z[45] = abb[24] * z[41];
z[46] = abb[25] * z[41];
z[45] = z[44] + -z[45] + z[46];
z[47] = -abb[21] * z[45];
z[48] = abb[17] * z[41];
z[49] = abb[19] * z[41];
z[48] = z[48] + z[49];
z[50] = abb[6] + abb[7];
z[51] = 2 * abb[0];
z[52] = -z[50] + z[51];
z[53] = 2 * abb[57];
z[54] = z[52] * z[53];
z[54] = -z[48] + z[54];
z[35] = -abb[54] * z[35];
z[35] = z[35] + z[54];
z[35] = abb[26] * z[35];
z[55] = -abb[48] + abb[51];
z[56] = 2 * z[39] + z[40] + -z[55];
z[57] = abb[26] * z[56];
z[58] = abb[24] + -abb[27];
z[59] = -z[56] * z[58];
z[57] = z[57] + -z[59];
z[60] = -z[39] + z[55];
z[61] = abb[31] * z[60];
z[62] = -z[46] + z[57] + z[61];
z[62] = abb[22] * z[62];
z[63] = z[40] + z[55];
z[64] = abb[26] * z[63];
z[65] = abb[24] * z[63];
z[64] = z[64] + -z[65];
z[66] = abb[30] * z[63];
z[66] = -z[43] + z[66];
z[67] = abb[28] * z[60];
z[68] = z[64] + z[66] + -z[67];
z[69] = -abb[18] * z[68];
z[70] = abb[24] * z[60];
z[71] = abb[30] * z[60];
z[70] = z[70] + -z[71];
z[43] = z[43] + z[70];
z[72] = abb[17] + abb[19];
z[43] = z[43] * z[72];
z[52] = z[52] * z[58];
z[73] = -abb[0] + abb[6];
z[74] = abb[25] * z[73];
z[75] = -abb[0] + abb[7];
z[76] = abb[31] * z[75];
z[52] = z[52] + z[74] + z[76];
z[52] = z[52] * z[53];
z[77] = -z[61] + z[70];
z[78] = z[67] + z[77];
z[79] = -abb[20] * z[78];
z[3] = z[3] + z[24] + z[25] + z[35] + z[43] + z[47] + z[52] + z[62] + z[69] + z[79];
z[3] = m1_set::bc<T>[0] * z[3];
z[24] = abb[3] + z[33];
z[25] = -z[13] + z[16] + z[24];
z[35] = abb[41] * z[25];
z[43] = -abb[9] + abb[11];
z[43] = abb[40] * z[43];
z[47] = abb[40] + abb[45];
z[52] = abb[4] * z[47];
z[35] = -z[35] + z[43] + z[52];
z[43] = -abb[2] + abb[3];
z[52] = abb[1] + -z[16] + z[43];
z[52] = abb[45] * z[52];
z[62] = -z[6] + z[30];
z[69] = -abb[9] + z[62];
z[69] = abb[43] * z[69];
z[79] = z[12] * z[47];
z[80] = z[14] + z[30];
z[81] = abb[44] * z[80];
z[82] = -abb[3] + -abb[5] + abb[10] + z[17];
z[82] = abb[39] * z[82];
z[83] = abb[5] * abb[40];
z[24] = abb[2] + -abb[10] + z[24];
z[84] = abb[42] * z[24];
z[52] = z[35] + -z[52] + -z[69] + -z[79] + z[81] + z[82] + z[83] + z[84];
z[52] = -z[5] * z[52];
z[69] = z[32] + -z[33];
z[79] = -abb[9] + z[69];
z[79] = abb[43] * z[79];
z[81] = z[16] + -z[18];
z[82] = abb[45] * z[81];
z[83] = 2 * z[33];
z[84] = abb[42] * z[83];
z[85] = abb[5] + -abb[6];
z[85] = abb[39] * z[85];
z[86] = abb[5] * z[47];
z[35] = z[35] + -z[79] + z[82] + z[84] + -2 * z[85] + -z[86];
z[79] = abb[54] + abb[56];
z[35] = -z[35] * z[79];
z[82] = -abb[41] + abb[45];
z[82] = z[73] * z[82];
z[47] = abb[7] * z[47];
z[84] = -abb[44] * z[75];
z[47] = z[47] + z[82] + z[84] + -z[85] + -z[86];
z[47] = z[47] * z[53];
z[82] = abb[42] * z[60];
z[84] = abb[39] * z[60];
z[82] = z[82] + -z[84];
z[85] = abb[44] * z[60];
z[86] = -abb[50] + z[39];
z[87] = abb[49] + z[86];
z[87] = abb[40] * z[87];
z[85] = z[85] + -z[87];
z[88] = abb[43] * z[41];
z[89] = abb[41] * z[41];
z[90] = z[82] + z[85] + z[88] + z[89];
z[91] = -abb[18] * z[90];
z[68] = -m1_set::bc<T>[0] * z[68];
z[68] = z[68] + -z[90];
z[68] = abb[16] * z[68];
z[84] = -z[84] + -z[85] + z[89];
z[84] = abb[22] * z[84];
z[85] = -abb[20] + z[72];
z[82] = -z[82] * z[85];
z[89] = -abb[41] * z[48];
z[90] = 2 * abb[53] + 2 * abb[55];
z[92] = z[79] + z[90];
z[93] = -abb[16] + -abb[18];
z[92] = z[92] * z[93];
z[93] = -z[5] * z[72];
z[92] = z[92] + z[93];
z[92] = abb[65] * z[92];
z[87] = -z[87] + z[88];
z[87] = -abb[21] * z[87];
z[3] = z[3] + z[35] + z[47] + z[52] + z[68] + z[82] + z[84] + z[87] + z[89] + z[91] + z[92];
z[3] = 4 * z[3];
z[35] = prod_pow(abb[27], 2);
z[47] = z[6] * z[35];
z[52] = -abb[0] + abb[5];
z[43] = z[43] + z[52];
z[68] = abb[36] * z[43];
z[82] = abb[35] + -abb[58];
z[84] = abb[34] + z[82];
z[84] = abb[3] * z[84];
z[87] = abb[34] + abb[62];
z[88] = abb[37] + z[87];
z[89] = abb[35] + z[88];
z[89] = abb[4] * z[89];
z[91] = -abb[5] + z[6];
z[91] = abb[62] * z[91];
z[92] = abb[2] * abb[35];
z[93] = abb[15] * abb[38];
z[94] = abb[34] * z[18];
z[17] = abb[58] * z[17];
z[95] = -abb[0] + z[33];
z[95] = abb[37] * z[95];
z[96] = abb[0] * abb[35];
z[97] = abb[5] * abb[58];
z[17] = z[17] + -z[47] + z[68] + z[84] + -z[89] + z[91] + z[92] + -z[93] + -z[94] + -z[95] + z[96] + -z[97];
z[47] = -abb[10] + z[6];
z[68] = abb[9] + -z[27] + z[47];
z[68] = abb[24] * z[68];
z[26] = -z[6] + z[26];
z[26] = abb[27] * z[26];
z[62] = abb[29] * z[62];
z[84] = abb[28] * z[24];
z[7] = abb[26] * z[7];
z[89] = abb[30] * z[47];
z[7] = z[7] + -z[21] + -z[26] + z[62] + -z[68] + -z[84] + z[89];
z[21] = 2 * abb[26];
z[7] = z[7] * z[21];
z[21] = -z[6] + z[9];
z[21] = abb[29] * z[21];
z[26] = z[33] + z[36];
z[68] = abb[24] * z[26];
z[84] = -abb[2] + -abb[3] + z[36];
z[84] = abb[31] * z[84];
z[91] = abb[27] * z[18];
z[92] = abb[30] * z[24];
z[93] = abb[27] + abb[31];
z[94] = abb[10] * z[93];
z[21] = -z[21] + z[68] + -z[84] + z[91] + -z[92] + -z[94];
z[26] = abb[28] * z[26];
z[21] = 2 * z[21] + z[26];
z[21] = abb[28] * z[21];
z[43] = abb[29] * z[43];
z[84] = abb[25] + abb[27];
z[91] = -abb[30] + z[84];
z[92] = abb[9] * z[91];
z[15] = z[15] + z[43] + z[89] + -z[92] + z[94];
z[89] = 2 * abb[24];
z[15] = z[15] * z[89];
z[89] = -abb[11] + z[12];
z[1] = z[1] + -z[9] + z[51] + -z[89];
z[1] = abb[31] * z[1];
z[14] = z[14] * z[29];
z[1] = z[1] + -z[14];
z[1] = abb[31] * z[1];
z[14] = -abb[25] + abb[29];
z[51] = -z[14] * z[84];
z[22] = -abb[27] + z[22];
z[22] = abb[30] * z[22];
z[22] = -abb[59] + -z[22] + z[51] + z[87];
z[51] = 2 * abb[9];
z[22] = z[22] * z[51];
z[84] = 2 * abb[60];
z[25] = z[25] * z[84];
z[22] = z[22] + -z[25];
z[18] = z[9] + z[18];
z[18] = abb[25] * z[18];
z[25] = abb[0] + abb[2];
z[84] = abb[27] * z[25];
z[18] = z[18] + z[84];
z[18] = 2 * z[18] + z[43];
z[18] = abb[29] * z[18];
z[43] = 2 * abb[10];
z[92] = -3 * z[6] + z[9] + z[43];
z[92] = abb[30] * z[92];
z[47] = abb[27] * z[47];
z[47] = z[47] + -z[62];
z[47] = 2 * z[47] + z[92];
z[47] = abb[30] * z[47];
z[28] = z[9] + z[28];
z[28] = abb[25] * z[28];
z[19] = z[19] * z[29];
z[28] = -z[19] + z[28];
z[28] = abb[25] * z[28];
z[62] = abb[31] * z[93];
z[62] = z[62] + z[82];
z[43] = z[43] * z[62];
z[30] = -z[30] + z[89];
z[62] = 2 * abb[59];
z[30] = z[30] * z[62];
z[27] = -z[6] + z[27];
z[82] = 2 * abb[64];
z[27] = z[27] * z[82];
z[89] = 2 * abb[46];
z[72] = z[72] * z[89];
z[92] = 2 * abb[63];
z[80] = z[80] * z[92];
z[6] = abb[33] * z[6];
z[1] = z[1] + -4 * z[6] + -z[7] + z[15] + 2 * z[17] + z[18] + -z[21] + z[22] + -z[27] + -z[28] + -z[30] + -z[43] + z[47] + -z[72] + z[80];
z[1] = z[1] * z[5];
z[6] = z[51] * z[91];
z[7] = 2 * z[20];
z[15] = -abb[8] + z[33];
z[17] = 2 * abb[30];
z[18] = z[15] * z[17];
z[20] = -abb[5] + abb[8] + z[13];
z[20] = abb[27] * z[20];
z[13] = -abb[8] + z[13];
z[21] = -abb[5] + z[13];
z[21] = abb[24] * z[21];
z[6] = z[6] + z[7] + -z[18] + -z[20] + z[21];
z[6] = abb[24] * z[6];
z[20] = z[17] * z[33];
z[21] = abb[29] * z[37];
z[27] = 2 * z[36];
z[28] = abb[31] * z[27];
z[20] = z[20] + z[21] + z[28] + -z[68];
z[20] = 2 * z[20] + -z[26];
z[20] = abb[28] * z[20];
z[21] = abb[6] * abb[58];
z[26] = z[21] + z[96] + -z[97];
z[11] = -abb[3] + -z[11];
z[11] = abb[34] * z[11];
z[28] = -abb[2] + z[33];
z[28] = abb[62] * z[28];
z[11] = z[11] + 2 * z[26] + z[28] + -z[95];
z[26] = abb[8] + z[32] + -4 * z[33];
z[26] = abb[30] * z[26];
z[28] = abb[29] * z[69];
z[30] = abb[27] * z[15];
z[28] = -z[28] + z[30];
z[26] = z[26] + 2 * z[28];
z[26] = abb[30] * z[26];
z[28] = -abb[1] + abb[6];
z[30] = abb[3] + abb[4];
z[32] = abb[11] + -2 * z[28] + z[30];
z[32] = abb[25] * z[32];
z[19] = -z[19] + z[32];
z[19] = abb[25] * z[19];
z[32] = 2 * abb[35] + z[88];
z[4] = z[4] * z[32];
z[32] = abb[1] + abb[3] + z[36];
z[33] = abb[25] * z[32];
z[33] = z[33] + z[84];
z[37] = 2 * abb[29];
z[33] = z[33] * z[37];
z[43] = z[0] + z[81];
z[43] = z[43] * z[82];
z[0] = abb[11] + z[0];
z[0] = z[0] * z[62];
z[47] = prod_pow(abb[31], 2);
z[27] = z[27] * z[47];
z[15] = z[15] + z[25];
z[25] = 2 * abb[33] + z[35];
z[15] = z[15] * z[25];
z[35] = abb[12] + abb[14] + abb[15];
z[35] = abb[38] * z[35];
z[0] = -z[0] + z[4] + z[6] + -2 * z[11] + z[15] + z[19] + -z[20] + -z[22] + -z[26] + z[27] + -z[33] + z[35] + -z[43];
z[4] = -abb[54] * z[0];
z[2] = 4 * abb[6] + -z[2];
z[6] = -abb[8] + -abb[11] + z[2] + -z[34] + z[51];
z[6] = abb[24] * z[6];
z[2] = -z[2] + z[13];
z[2] = abb[27] * z[2];
z[11] = z[23] * z[51];
z[13] = z[32] * z[37];
z[2] = -z[2] + -z[6] + z[7] + z[11] + z[13] + z[18] + z[38];
z[6] = 2 * z[73];
z[7] = -abb[11] + z[6] + z[30];
z[11] = abb[26] * z[7];
z[11] = -z[2] + z[11];
z[11] = abb[26] * z[11];
z[0] = -z[0] + z[11];
z[0] = abb[56] * z[0];
z[11] = abb[37] * z[60];
z[13] = abb[58] * z[60];
z[11] = z[11] + z[13];
z[15] = abb[36] + -abb[62];
z[15] = abb[49] * z[15];
z[18] = abb[36] * z[86];
z[19] = abb[62] * z[86];
z[15] = -z[11] + -z[15] + -z[18] + z[19];
z[18] = z[17] * z[63];
z[20] = abb[27] * z[63];
z[18] = z[18] + -z[20];
z[22] = 2 * z[41];
z[23] = abb[29] * z[22];
z[26] = z[18] + -z[23];
z[27] = z[26] + z[64] + -2 * z[67];
z[27] = abb[26] * z[27];
z[30] = z[67] + 2 * z[70];
z[30] = abb[28] * z[30];
z[20] = -z[20] + z[66];
z[17] = z[17] * z[20];
z[20] = abb[24] * z[26];
z[32] = abb[59] * z[22];
z[33] = z[60] * z[92];
z[32] = z[32] + -z[33];
z[33] = 4 * abb[46];
z[33] = z[5] * z[33];
z[34] = z[79] * z[89];
z[35] = prod_pow(abb[29], 2) * z[41];
z[25] = z[25] * z[63];
z[37] = abb[60] * z[22];
z[15] = -2 * z[15] + z[17] + -z[20] + z[25] + z[27] + z[30] + z[32] + z[33] + z[34] + z[35] + -z[37];
z[17] = -abb[18] * z[15];
z[20] = 2 * abb[61];
z[27] = z[20] * z[60];
z[30] = (T(1) / T(3)) * z[63];
z[33] = prod_pow(m1_set::bc<T>[0], 2);
z[34] = z[30] * z[33];
z[15] = -z[15] + z[27] + z[34];
z[15] = abb[16] * z[15];
z[27] = abb[25] * z[6];
z[27] = z[27] + 2 * z[76];
z[34] = -z[52] * z[58];
z[34] = z[27] + z[34];
z[34] = abb[24] * z[34];
z[21] = -z[21] + z[97];
z[35] = -z[29] * z[75];
z[35] = z[35] + -z[76];
z[35] = abb[31] * z[35];
z[38] = -abb[25] + -z[29];
z[38] = z[38] * z[74];
z[43] = -abb[5] + abb[7];
z[51] = -z[43] * z[62];
z[43] = -z[43] + -z[73];
z[43] = z[43] * z[82];
z[52] = z[75] * z[92];
z[6] = abb[60] * z[6];
z[62] = abb[12] * abb[38];
z[6] = z[6] + 2 * z[21] + z[34] + z[35] + z[38] + z[43] + z[51] + z[52] + z[62];
z[6] = z[6] * z[53];
z[2] = -abb[54] * z[2];
z[21] = 2 * abb[21];
z[34] = -z[21] * z[45];
z[26] = -z[26] + z[65];
z[26] = abb[19] * z[26];
z[12] = -abb[5] + z[12] + z[16] + -z[31];
z[12] = -z[12] * z[58];
z[12] = z[12] + z[27];
z[12] = z[12] * z[53];
z[7] = abb[54] * z[7];
z[7] = z[7] + z[54];
z[7] = abb[26] * z[7];
z[16] = abb[17] * z[23];
z[2] = z[2] + z[7] + z[12] + z[16] + z[26] + z[34];
z[2] = abb[26] * z[2];
z[7] = abb[25] * z[22];
z[7] = -z[7] + 2 * z[61];
z[12] = z[7] + -z[59];
z[12] = abb[24] * z[12];
z[7] = z[7] + z[57];
z[7] = abb[26] * z[7];
z[16] = -z[29] * z[60];
z[16] = z[16] + -z[61];
z[16] = abb[31] * z[16];
z[22] = abb[27] * z[22];
z[22] = z[22] + z[46];
z[22] = abb[25] * z[22];
z[7] = z[7] + z[12] + 2 * z[13] + z[16] + z[22] + -z[32] + -z[37];
z[7] = abb[22] * z[7];
z[12] = abb[27] * z[60];
z[16] = z[12] + z[61];
z[22] = z[16] + -z[71];
z[26] = abb[24] + abb[28];
z[22] = z[22] * z[26];
z[16] = -abb[31] * z[16];
z[12] = z[12] + -z[71];
z[12] = -abb[30] * z[12];
z[26] = -abb[26] * z[78];
z[27] = abb[35] * z[60];
z[12] = z[12] + z[13] + z[16] + z[22] + z[26] + -z[27];
z[12] = abb[20] * z[12];
z[13] = abb[8] + -abb[9] + z[36];
z[13] = z[13] * z[79];
z[10] = -z[9] + z[10];
z[10] = -z[5] * z[10];
z[16] = abb[17] + abb[20];
z[16] = z[16] * z[60];
z[22] = -abb[21] * z[41];
z[10] = z[10] + z[13] + z[16] + z[22] + -z[49];
z[10] = abb[32] * z[10];
z[10] = z[10] + z[12];
z[12] = abb[27] * z[41];
z[13] = z[12] + z[46];
z[14] = -z[13] * z[14];
z[12] = z[12] + -z[44];
z[12] = abb[30] * z[12];
z[13] = -z[13] + z[42];
z[13] = abb[24] * z[13];
z[16] = -abb[59] * z[41];
z[22] = abb[34] * z[86];
z[26] = abb[49] * z[87];
z[12] = z[12] + z[13] + z[14] + z[16] + z[19] + z[22] + z[26];
z[12] = z[12] * z[21];
z[13] = abb[22] * z[56];
z[13] = -z[13] + z[48];
z[9] = (T(-1) / T(3)) * z[9];
z[14] = abb[5] * (T(-2) / T(3)) + abb[11] * (T(1) / T(3)) + -z[9] + 6 * z[28];
z[14] = z[14] * z[79];
z[8] = 3 * z[8] + z[9];
z[8] = -z[8] * z[90];
z[9] = abb[0] * (T(8) / T(3)) + abb[5] * (T(10) / T(3)) + -3 * z[50];
z[9] = z[9] * z[53];
z[16] = abb[18] * z[30];
z[8] = z[8] + z[9] + (T(1) / T(3)) * z[13] + z[14] + z[16];
z[8] = z[8] * z[33];
z[9] = abb[34] * abb[49];
z[9] = -z[9] + z[11] + -z[22] + z[27];
z[11] = abb[25] * z[23];
z[13] = prod_pow(abb[25], 2) * z[41];
z[9] = 2 * z[9] + z[11] + -z[13] + -z[37];
z[11] = z[39] + -z[40] + -2 * z[55];
z[11] = abb[30] * z[11];
z[13] = z[29] * z[63];
z[11] = z[11] + z[13];
z[11] = abb[30] * z[11];
z[13] = -z[47] * z[60];
z[11] = -z[9] + z[11] + z[13] + -z[25];
z[11] = abb[19] * z[11];
z[13] = -prod_pow(abb[30], 2);
z[14] = prod_pow(abb[24], 2);
z[13] = z[13] + z[14] + -z[47];
z[13] = z[13] * z[60];
z[14] = z[67] + 2 * z[77];
z[16] = -abb[28] * z[14];
z[9] = -z[9] + z[13] + z[16];
z[9] = abb[17] * z[9];
z[5] = z[5] * z[24];
z[13] = z[79] * z[83];
z[16] = abb[18] + z[85];
z[16] = z[16] * z[60];
z[5] = z[5] + z[13] + z[16];
z[5] = z[5] * z[20];
z[13] = abb[23] * z[63];
z[16] = z[53] + -z[79];
z[16] = abb[13] * z[16];
z[13] = z[13] + z[16];
z[13] = abb[38] * z[13];
z[16] = abb[19] * z[18];
z[18] = -abb[24] * z[49];
z[16] = z[16] + z[18];
z[16] = abb[24] * z[16];
z[14] = -abb[19] * abb[28] * z[14];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + 2 * z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17];
z[0] = 2 * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_706_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("-12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("67.555959869039285206499315430607680372655358866090563467572435743"),stof<T>("7.113382302942150864198017200257924263635139596547738378190288397")}, std::complex<T>{stof<T>("67.555959869039285206499315430607680372655358866090563467572435743"),stof<T>("7.113382302942150864198017200257924263635139596547738378190288397")}, std::complex<T>{stof<T>("-11.900894076796136801904386879572424097608812135765383308254157777"),stof<T>("-14.855427600915246846542877066135400887977168833731406221930959934")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_706_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_706_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-38.110611841821838256654171749691395615376097025881289769791065523"),stof<T>("99.752436708742491903324638875885352772063604153708341344612993614")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,66> abb = {dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W22(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_14(k), f_2_19(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_10_im(k), f_2_11_im(k), f_2_12_im(k), f_2_22_im(k), f_2_26_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_10_re(k), f_2_11_re(k), f_2_12_re(k), f_2_22_re(k), f_2_26_re(k)};

                    
            return f_4_706_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_706_DLogXconstant_part(base_point<T>, kend);
	value += f_4_706_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_706_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_706_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_706_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_706_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_706_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_706_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
