/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_224.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_224_abbreviated (const std::array<T,55>& abb) {
T z[82];
z[0] = abb[48] + abb[49];
z[1] = abb[46] + abb[47];
z[2] = z[0] + -z[1];
z[3] = abb[3] * z[2];
z[4] = abb[8] * z[2];
z[4] = z[3] + -z[4];
z[5] = abb[19] * abb[51];
z[6] = -abb[46] + abb[47];
z[7] = (T(1) / T(2)) * z[6];
z[8] = abb[15] * z[7];
z[9] = abb[41] + abb[42] + -abb[44] + -abb[45];
z[10] = -abb[43] + (T(-1) / T(2)) * z[9];
z[11] = (T(1) / T(2)) * z[10];
z[12] = abb[19] * z[11];
z[5] = z[5] + -z[8] + -z[12];
z[8] = abb[46] + -abb[49];
z[12] = abb[10] * z[8];
z[13] = abb[47] + abb[49];
z[14] = abb[2] * z[13];
z[15] = z[12] + -z[14];
z[16] = z[0] + z[1];
z[17] = -abb[0] * z[16];
z[18] = abb[50] * (T(1) / T(4));
z[19] = abb[22] + -abb[24];
z[19] = z[18] * z[19];
z[20] = abb[13] * z[7];
z[21] = abb[47] + -abb[48];
z[22] = abb[9] * z[21];
z[23] = abb[46] + abb[48];
z[24] = abb[1] * z[23];
z[25] = abb[6] * abb[47];
z[26] = abb[5] * abb[46];
z[4] = (T(-1) / T(4)) * z[4] + z[5] + -z[15] + z[17] + z[19] + z[20] + -z[22] + z[24] + z[25] + z[26];
z[4] = abb[27] * z[4];
z[17] = abb[20] + abb[23];
z[19] = -abb[24] + z[17];
z[25] = -abb[21] + z[19];
z[25] = z[18] * z[25];
z[26] = -z[13] + z[23];
z[27] = (T(1) / T(2)) * z[26];
z[28] = abb[14] * z[27];
z[25] = z[25] + z[28];
z[29] = 2 * z[14];
z[5] = -z[5] + -z[29];
z[30] = 2 * z[24];
z[20] = z[20] + z[30];
z[31] = abb[48] + z[1];
z[32] = abb[12] * z[31];
z[33] = 2 * z[32];
z[34] = z[20] + -z[33];
z[35] = -abb[48] + abb[49];
z[36] = (T(1) / T(2)) * z[35];
z[37] = 2 * abb[46];
z[38] = -z[36] + z[37];
z[38] = abb[6] * z[38];
z[39] = 3 * abb[48] + abb[49];
z[40] = (T(1) / T(2)) * z[39];
z[41] = z[1] + z[40];
z[41] = abb[5] * z[41];
z[42] = 3 * abb[46];
z[43] = -abb[48] + z[13] + z[42];
z[44] = abb[0] * (T(1) / T(2));
z[43] = z[43] * z[44];
z[45] = abb[47] + z[42];
z[46] = -z[35] + z[45];
z[47] = abb[8] * (T(1) / T(2));
z[48] = z[46] * z[47];
z[11] = -abb[51] + z[11];
z[49] = abb[17] * z[11];
z[38] = z[5] + -z[25] + z[34] + z[38] + z[41] + z[43] + z[48] + -z[49];
z[38] = abb[28] * z[38];
z[41] = abb[8] * z[27];
z[41] = -z[28] + z[41];
z[43] = abb[21] + abb[22];
z[48] = abb[50] * (T(1) / T(2));
z[50] = z[43] * z[48];
z[51] = 3 * abb[47];
z[52] = abb[46] + z[51];
z[39] = z[39] + z[52];
z[39] = z[39] * z[44];
z[33] = 4 * z[24] + -z[33];
z[53] = abb[13] * z[6];
z[54] = z[33] + z[53];
z[55] = abb[5] * z[23];
z[56] = abb[6] * z[1];
z[50] = -z[39] + z[41] + z[50] + z[54] + 2 * z[55] + z[56];
z[50] = abb[29] * z[50];
z[55] = z[17] + z[43];
z[55] = z[48] * z[55];
z[57] = (T(1) / T(2)) * z[3];
z[58] = abb[5] * z[1];
z[59] = -2 * abb[51] + z[10];
z[60] = abb[17] * z[59];
z[58] = z[58] + -z[60];
z[2] = -z[2] * z[47];
z[2] = z[2] + z[55] + z[56] + z[57] + z[58];
z[2] = abb[30] * z[2];
z[2] = z[2] + z[4] + z[38] + -z[50];
z[2] = abb[27] * z[2];
z[4] = 3 * z[11];
z[38] = abb[17] * z[4];
z[61] = abb[49] + z[1];
z[62] = abb[11] * z[61];
z[63] = 2 * z[62];
z[5] = z[5] + z[38] + z[63];
z[38] = abb[48] + 3 * abb[49];
z[64] = (T(1) / T(2)) * z[38];
z[65] = z[1] + z[64];
z[65] = abb[6] * z[65];
z[66] = 2 * abb[47];
z[67] = z[36] + z[66];
z[67] = abb[5] * z[67];
z[23] = -abb[49] + z[23] + z[51];
z[23] = z[23] * z[44];
z[68] = z[35] + z[52];
z[69] = z[47] * z[68];
z[20] = -z[5] + -z[20] + z[23] + z[25] + z[65] + z[67] + z[69];
z[20] = abb[27] * z[20];
z[23] = z[37] + z[66];
z[25] = z[23] + z[40];
z[25] = abb[5] * z[25];
z[23] = z[23] + z[64];
z[23] = abb[6] * z[23];
z[40] = abb[21] + abb[23];
z[64] = abb[20] + -abb[24] + z[40];
z[64] = -abb[22] + (T(-1) / T(2)) * z[64];
z[65] = z[48] * z[64];
z[67] = abb[0] * z[0];
z[69] = abb[8] * z[1];
z[5] = z[5] + -z[23] + -z[25] + -z[34] + z[65] + z[67] + -z[69];
z[23] = abb[28] * z[5];
z[25] = z[44] * z[68];
z[34] = abb[8] * abb[46];
z[37] = abb[5] * z[37];
z[25] = -z[25] + z[34] + z[37];
z[34] = z[0] + z[42] + z[51];
z[34] = (T(1) / T(2)) * z[34];
z[37] = -abb[6] * z[34];
z[42] = abb[16] * z[59];
z[37] = -z[14] + z[24] + -z[25] + z[37] + z[42] + z[62];
z[37] = abb[31] * z[37];
z[51] = z[1] + z[35];
z[65] = abb[4] * z[51];
z[67] = (T(1) / T(2)) * z[65];
z[70] = -z[56] + z[67];
z[71] = abb[21] * z[48];
z[25] = z[25] + -z[70] + z[71];
z[71] = -z[25] + z[28];
z[71] = abb[30] * z[71];
z[72] = abb[30] * z[59];
z[73] = abb[28] * z[4];
z[74] = abb[29] * z[59];
z[72] = z[72] + z[73] + -z[74];
z[73] = abb[27] * z[11];
z[75] = z[72] + -z[73];
z[75] = abb[16] * z[75];
z[20] = z[20] + z[23] + z[37] + z[50] + z[71] + z[75];
z[20] = abb[31] * z[20];
z[32] = -z[24] + z[32];
z[37] = -z[22] + z[32];
z[26] = abb[14] * z[26];
z[26] = (T(1) / T(4)) * z[26];
z[35] = -z[1] + z[35];
z[50] = abb[7] * z[35];
z[71] = abb[46] * (T(1) / T(2));
z[75] = abb[47] * (T(1) / T(2));
z[76] = z[71] + -z[75];
z[77] = abb[48] + z[76];
z[77] = abb[5] * z[77];
z[78] = abb[0] * (T(1) / T(4));
z[68] = -z[68] * z[78];
z[79] = -abb[23] * z[18];
z[80] = -abb[47] * z[47];
z[49] = -z[26] + -z[37] + z[49] + (T(-1) / T(4)) * z[50] + z[68] + z[77] + z[79] + z[80];
z[49] = abb[29] * z[49];
z[68] = z[44] * z[46];
z[77] = abb[8] * abb[47];
z[66] = abb[6] * z[66];
z[66] = z[66] + -z[68] + z[77];
z[68] = (T(1) / T(2)) * z[50];
z[77] = z[58] + z[68];
z[79] = abb[23] * z[48];
z[79] = z[28] + z[66] + z[77] + z[79];
z[79] = abb[28] * z[79];
z[49] = z[49] + z[79];
z[49] = abb[29] * z[49];
z[30] = 2 * z[22] + -z[30] + -z[53];
z[80] = -z[17] * z[48];
z[81] = -abb[5] * z[6];
z[21] = -abb[8] * z[21];
z[21] = z[21] + -z[28] + -z[30] + -z[39] + -z[57] + z[60] + z[80] + z[81];
z[21] = abb[34] * z[21];
z[39] = z[12] + z[14];
z[62] = z[39] + -z[62];
z[76] = abb[49] + -z[76];
z[76] = abb[6] * z[76];
z[46] = -z[46] * z[78];
z[78] = -abb[21] * z[18];
z[80] = -abb[46] * z[47];
z[81] = abb[16] * z[11];
z[26] = z[26] + z[46] + z[62] + (T(1) / T(4)) * z[65] + z[76] + z[78] + z[80] + z[81];
z[26] = abb[32] * z[26];
z[46] = abb[19] * z[59];
z[76] = abb[15] * z[6];
z[46] = z[46] + z[76];
z[63] = z[46] + z[63];
z[13] = abb[6] * z[13];
z[13] = z[13] + z[29];
z[29] = abb[22] + z[19];
z[29] = z[29] * z[48];
z[38] = z[38] + z[45];
z[38] = z[38] * z[44];
z[13] = -2 * z[13] + -z[29] + z[38] + z[41] + -z[58] + z[63];
z[29] = abb[27] + -abb[28];
z[13] = z[13] * z[29];
z[29] = z[28] + z[42];
z[25] = z[25] + -z[29];
z[25] = abb[31] * z[25];
z[13] = z[13] + z[25] + z[26];
z[13] = abb[32] * z[13];
z[0] = -2 * z[0] + z[71] + z[75];
z[0] = abb[0] * z[0];
z[25] = z[42] + z[63];
z[26] = 4 * z[14] + -z[60];
z[41] = 2 * abb[49];
z[44] = abb[46] + z[41] + z[75];
z[44] = abb[6] * z[44];
z[75] = 2 * abb[48];
z[71] = abb[47] + z[71] + z[75];
z[71] = abb[5] * z[71];
z[64] = -abb[50] * z[64];
z[76] = -abb[18] * z[59];
z[0] = z[0] + -z[25] + z[26] + z[44] + z[54] + z[64] + z[71] + z[76];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[44] = z[16] * z[47];
z[44] = z[44] + -z[57];
z[54] = (T(1) / T(2)) * z[35];
z[54] = -abb[6] * z[54];
z[64] = -abb[20] + -z[43];
z[64] = z[48] * z[64];
z[12] = -z[12] + -z[24] + z[28] + z[44] + z[54] + z[64] + z[68];
z[12] = abb[35] * z[12];
z[54] = abb[22] + z[40];
z[54] = z[48] * z[54];
z[35] = z[35] * z[47];
z[35] = -z[35] + z[54];
z[54] = z[45] + z[75];
z[54] = abb[5] * z[54];
z[64] = abb[6] * z[52];
z[31] = 2 * z[31];
z[31] = abb[0] * z[31];
z[31] = -z[31] + z[33] + z[35] + z[54] + -z[60] + z[64] + z[68];
z[33] = z[31] + z[53];
z[33] = abb[52] * z[33];
z[15] = 2 * z[15] + z[46];
z[46] = -abb[21] + -abb[24];
z[46] = z[46] * z[48];
z[53] = abb[6] * z[6];
z[8] = -abb[8] * z[8];
z[8] = z[8] + -z[15] + z[29] + -z[38] + z[46] + z[53] + -z[57];
z[8] = abb[33] * z[8];
z[19] = z[19] + z[43];
z[19] = z[19] * z[48];
z[29] = z[47] * z[51];
z[19] = z[19] + z[29];
z[29] = z[41] + z[52];
z[29] = abb[6] * z[29];
z[38] = 2 * abb[0];
z[41] = z[38] * z[61];
z[43] = abb[5] * z[45];
z[26] = -z[19] + -z[26] + -z[29] + z[41] + -z[43] + z[67];
z[29] = -z[26] + -z[63];
z[29] = abb[53] * z[29];
z[22] = -z[22] + z[44];
z[41] = abb[5] + -abb[6];
z[27] = z[27] * z[41];
z[41] = -abb[0] * z[1];
z[24] = -z[22] + z[24] + z[27] + z[39] + z[41] + z[55];
z[24] = abb[37] * z[24];
z[27] = -abb[5] + -abb[6] + abb[15];
z[10] = z[10] * z[27];
z[39] = abb[19] * z[6];
z[10] = z[10] + z[39];
z[39] = z[1] + z[36];
z[39] = abb[17] * z[39];
z[9] = 2 * abb[43] + 4 * abb[51] + z[9];
z[9] = abb[26] * z[9];
z[41] = abb[8] * z[59];
z[18] = abb[25] * z[18];
z[27] = abb[51] * z[27];
z[9] = -z[9] + (T(-1) / T(2)) * z[10] + -z[18] + z[27] + -z[39] + z[41];
z[1] = -z[1] + z[36];
z[10] = abb[16] * z[1];
z[18] = -abb[13] * z[11];
z[10] = z[9] + z[10] + z[18];
z[10] = abb[54] * z[10];
z[18] = (T(1) / T(2)) * z[51];
z[18] = abb[5] * z[18];
z[17] = -abb[22] + -z[17];
z[17] = z[17] * z[48];
z[17] = -z[14] + z[17] + z[18] + z[22] + -z[28] + -z[67];
z[17] = abb[36] * z[17];
z[18] = -abb[5] * z[34];
z[14] = z[14] + z[18] + z[32] + z[60] + -z[66];
z[14] = prod_pow(abb[28], 2) * z[14];
z[16] = abb[8] * z[16];
z[18] = -abb[20] + -abb[22];
z[18] = abb[50] * z[18];
z[3] = -z[3] + z[16] + z[18] + z[50] + -z[65];
z[3] = abb[30] * z[3];
z[3] = (T(1) / T(4)) * z[3] + -z[79];
z[3] = abb[30] * z[3];
z[16] = -abb[27] * z[72];
z[18] = -abb[52] + -abb[53];
z[18] = z[18] * z[59];
z[16] = z[16] + z[18];
z[16] = abb[16] * z[16];
z[18] = abb[28] * z[11];
z[18] = z[18] + -z[74];
z[22] = z[18] + z[73];
z[27] = -abb[27] + abb[31];
z[22] = z[22] * z[27];
z[27] = -abb[34] + -abb[52];
z[27] = z[27] * z[59];
z[28] = abb[54] * z[7];
z[22] = z[22] + z[27] + z[28];
z[22] = abb[18] * z[22];
z[0] = (T(1) / T(3)) * z[0] + z[2] + z[3] + z[8] + z[10] + z[12] + z[13] + z[14] + z[16] + z[17] + z[20] + z[21] + z[22] + z[24] + z[29] + z[33] + z[49];
z[2] = abb[16] * z[4];
z[2] = z[2] + z[5];
z[2] = abb[31] * z[2];
z[3] = -abb[49] * z[38];
z[3] = z[3] + -z[15] + z[19] + -z[42] + z[58] + -z[70];
z[3] = abb[32] * z[3];
z[4] = -abb[48] * z[38];
z[4] = z[4] + -z[30] + z[35] + z[56] + z[77];
z[4] = abb[29] * z[4];
z[5] = -z[40] * z[48];
z[5] = z[5] + -z[69] + z[70] + -z[77];
z[5] = abb[30] * z[5];
z[8] = abb[6] * abb[49];
z[10] = abb[5] * abb[48];
z[8] = z[8] + z[10] + -z[37] + z[62];
z[8] = abb[27] * z[8];
z[10] = abb[16] * z[72];
z[12] = abb[31] * z[11];
z[12] = z[12] + z[18];
z[12] = abb[18] * z[12];
z[2] = z[2] + z[3] + z[4] + z[5] + 2 * z[8] + z[10] + z[12] + z[23];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[38] * z[31];
z[4] = abb[40] * z[9];
z[5] = -z[25] + -z[26];
z[5] = abb[39] * z[5];
z[6] = abb[38] * z[6];
z[8] = -abb[40] * z[11];
z[6] = z[6] + z[8];
z[6] = abb[13] * z[6];
z[1] = abb[40] * z[1];
z[8] = abb[38] * z[59];
z[1] = z[1] + -z[8];
z[1] = abb[16] * z[1];
z[7] = abb[40] * z[7];
z[7] = z[7] + -z[8];
z[7] = abb[18] * z[7];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_224_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("-3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("-3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("-3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("-7.7143564516654021477305372678719799821591283604003080270983573661"),stof<T>("-7.2516632689748497845188292938738884372507784963632055609905540863")}, std::complex<T>{stof<T>("3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("-28.065593408028772133635407567885755680874875087910720549766606817"),stof<T>("-10.134279212974814860169624191733463868627374893137719822959739056")}, std::complex<T>{stof<T>("-28.065593408028772133635407567885755680874875087910720549766606817"),stof<T>("-10.134279212974814860169624191733463868627374893137719822959739056")}, std::complex<T>{stof<T>("5.0640616850712605728445510775568457438497106153224516270917389906"),stof<T>("9.7872971614052357116164436201066045743353814609176417256353631691")}, std::complex<T>{stof<T>("5.0640616850712605728445510775568457438497106153224516270917389906"),stof<T>("9.7872971614052357116164436201066045743353814609176417256353631691")}, std::complex<T>{stof<T>("-0.62875951357982689959604956510979502283569846153696176805265663139"),stof<T>("0.631211800731221965993284197375092772384272312334023096282934671")}, std::complex<T>{stof<T>("-15.428712903330804295461074535743959964318256720800616054196714732"),stof<T>("-14.503326537949699569037658587747776874501556992726411121981108173")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}, rlog(k.W[196].real()/kbase.W[196].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_224_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_224_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-54.465920497743083947987469236870500063587739576331002124048561988"),stof<T>("44.653865674998764678053119695030997310737867420909465989709073009")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), rlog(kend.W[196].real()/k.W[196].real()), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_224_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_224_DLogXconstant_part(base_point<T>, kend);
	value += f_4_224_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_224_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_224_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_224_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_224_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_224_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_224_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
