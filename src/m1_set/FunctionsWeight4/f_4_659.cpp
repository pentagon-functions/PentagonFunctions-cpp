/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_659.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_659_abbreviated (const std::array<T,68>& abb) {
T z[158];
z[0] = 4 * abb[46];
z[1] = 4 * abb[50];
z[2] = z[0] + z[1];
z[3] = abb[48] * (T(5) / T(2));
z[4] = abb[54] * (T(5) / T(2));
z[5] = -abb[47] + abb[56];
z[6] = abb[55] * (T(1) / T(2));
z[7] = abb[45] + z[6];
z[8] = 3 * abb[51];
z[9] = abb[49] * (T(1) / T(2));
z[10] = 2 * abb[53];
z[11] = z[2] + z[3] + -z[4] + z[5] + z[7] + -z[8] + z[9] + -z[10];
z[11] = abb[4] * z[11];
z[12] = 3 * abb[46] + -abb[51];
z[13] = 2 * abb[50];
z[14] = -abb[48] + abb[54];
z[6] = abb[45] * (T(1) / T(2)) + -z[6] + -z[9] + z[12] + z[13] + -z[14];
z[6] = abb[0] * z[6];
z[15] = 3 * abb[56];
z[16] = 2 * abb[55];
z[17] = z[15] + z[16];
z[18] = 3 * abb[45];
z[19] = 7 * abb[48];
z[20] = -abb[54] + z[18] + z[19];
z[21] = 8 * abb[50];
z[22] = 10 * abb[46] + z[21];
z[23] = 4 * abb[51];
z[20] = -z[17] + (T(1) / T(2)) * z[20] + z[22] + -z[23];
z[20] = abb[6] * z[20];
z[24] = 3 * abb[48];
z[25] = z[16] + -z[24];
z[26] = -abb[45] + abb[54];
z[27] = z[25] + z[26];
z[28] = 2 * abb[51];
z[29] = z[27] + z[28];
z[30] = 9 * abb[46];
z[31] = -abb[47] + z[15];
z[32] = 6 * abb[50];
z[33] = z[29] + -z[30] + z[31] + -z[32];
z[33] = abb[9] * z[33];
z[34] = 3 * abb[55];
z[35] = 5 * abb[51];
z[36] = 2 * abb[48];
z[37] = abb[45] + z[36];
z[38] = 11 * abb[46] + abb[49] + 10 * abb[50] + 2 * z[37];
z[39] = 4 * abb[56];
z[40] = abb[53] + -z[34] + -z[35] + z[38] + -z[39];
z[40] = abb[1] * z[40];
z[41] = abb[46] + abb[49];
z[42] = z[13] + z[41];
z[43] = z[10] + -z[16] + z[42];
z[44] = abb[12] * z[43];
z[45] = 2 * abb[46];
z[46] = z[13] + z[45];
z[47] = -abb[49] + abb[55];
z[48] = -z[46] + z[47];
z[49] = 2 * abb[56];
z[50] = z[37] + -z[49];
z[51] = -z[48] + z[50];
z[52] = abb[14] * z[51];
z[53] = 3 * abb[54];
z[54] = -abb[55] + z[53];
z[55] = abb[48] + abb[49];
z[56] = z[54] + -z[55];
z[56] = abb[53] + -abb[56] + (T(1) / T(2)) * z[56];
z[56] = abb[5] * z[56];
z[57] = abb[22] + abb[25];
z[58] = abb[20] + z[57];
z[59] = abb[21] + abb[23];
z[60] = (T(1) / T(2)) * z[59];
z[61] = z[58] + z[60];
z[62] = abb[59] + abb[60];
z[61] = z[61] * z[62];
z[63] = z[14] + z[47];
z[64] = -abb[51] + (T(1) / T(2)) * z[63];
z[64] = abb[7] * z[64];
z[65] = abb[49] + abb[55];
z[66] = z[10] + -z[65];
z[67] = z[14] + z[66];
z[68] = abb[8] * z[67];
z[69] = abb[16] * (T(1) / T(2));
z[70] = -abb[57] * z[69];
z[6] = z[6] + z[11] + z[20] + z[33] + z[40] + -z[44] + -z[52] + -z[56] + z[61] + -z[64] + z[68] + z[70];
z[6] = abb[33] * z[6];
z[11] = z[14] + z[65];
z[20] = -z[0] + z[11] + -z[13];
z[20] = abb[0] * z[20];
z[33] = z[23] + z[49];
z[22] = -z[22] + z[27] + z[33];
z[27] = 2 * abb[6];
z[40] = z[22] * z[27];
z[61] = -abb[48] + abb[55];
z[70] = -7 * abb[46] + -z[1] + z[31] + 2 * z[61];
z[71] = 2 * abb[9];
z[70] = z[70] * z[71];
z[72] = -abb[54] + z[49];
z[73] = abb[45] + abb[48];
z[74] = z[72] + -z[73];
z[75] = abb[5] * z[74];
z[76] = -z[52] + z[75];
z[77] = -abb[45] + z[14];
z[78] = z[23] + z[77];
z[79] = -z[2] + z[78];
z[79] = abb[15] * z[79];
z[80] = abb[46] + -abb[49];
z[81] = abb[12] * z[80];
z[20] = z[20] + z[40] + -z[70] + -z[76] + -z[79] + -2 * z[81];
z[40] = -abb[29] * z[20];
z[82] = 6 * abb[46];
z[83] = z[32] + z[82];
z[84] = 2 * abb[47];
z[85] = z[83] + -z[84];
z[86] = -z[23] + -z[65] + z[85];
z[87] = 2 * abb[54];
z[37] = -z[37] + z[87];
z[88] = z[37] + -z[49] + -z[86];
z[89] = abb[4] * z[88];
z[20] = z[20] + z[89];
z[20] = abb[28] * z[20];
z[89] = abb[55] + z[49];
z[55] = -z[10] + -z[53] + z[55] + z[89];
z[90] = -abb[5] * z[55];
z[91] = abb[52] + z[31] + -z[53];
z[91] = z[71] * z[91];
z[63] = -z[28] + z[63];
z[92] = abb[7] * z[63];
z[91] = z[91] + -z[92];
z[93] = 6 * abb[51];
z[94] = 5 * abb[54];
z[95] = 2 * abb[52];
z[96] = z[93] + -z[94] + z[95];
z[97] = -3 * abb[49] + z[2] + -z[49];
z[98] = -z[61] + z[96] + -z[97];
z[98] = abb[14] * z[98];
z[90] = -z[68] + z[90] + -z[91] + z[98];
z[90] = abb[32] * z[90];
z[98] = -z[84] + z[95];
z[55] = -z[55] + -z[98];
z[55] = abb[32] * z[55];
z[99] = -abb[29] * z[88];
z[55] = z[55] + z[99];
z[55] = abb[4] * z[55];
z[99] = abb[6] * z[74];
z[100] = z[52] + z[99];
z[101] = abb[5] * z[77];
z[102] = z[100] + 2 * z[101];
z[103] = -z[14] + -z[48];
z[104] = abb[0] * z[103];
z[105] = z[68] + -z[102] + z[104];
z[106] = abb[45] + z[24];
z[107] = -z[46] + z[53] + -z[106];
z[108] = 2 * abb[49];
z[109] = -z[10] + z[108];
z[110] = z[107] + -z[109];
z[111] = abb[4] * z[110];
z[111] = -z[105] + z[111];
z[111] = abb[31] * z[111];
z[112] = abb[20] + z[59];
z[57] = z[57] + z[112];
z[113] = abb[31] * z[57];
z[114] = abb[28] + -abb[29];
z[115] = abb[24] + abb[25];
z[115] = z[114] * z[115];
z[116] = abb[32] * z[58];
z[113] = z[113] + z[115] + z[116];
z[113] = -z[62] * z[113];
z[17] = abb[54] + z[17] + -z[38] + z[93];
z[17] = z[17] * z[114];
z[38] = -abb[54] + abb[56];
z[115] = abb[53] + -abb[55];
z[116] = abb[51] + z[115];
z[117] = z[38] + -z[116];
z[118] = abb[32] * z[117];
z[119] = abb[31] * z[38];
z[17] = z[17] + z[118] + z[119];
z[119] = 2 * abb[1];
z[17] = z[17] * z[119];
z[120] = abb[17] + abb[19];
z[120] = z[114] * z[120];
z[121] = abb[16] * abb[31];
z[120] = z[120] + -z[121];
z[122] = -abb[57] * z[120];
z[6] = z[6] + z[17] + z[20] + z[40] + z[55] + z[90] + z[111] + z[113] + z[122];
z[6] = abb[33] * z[6];
z[17] = abb[20] + -abb[24];
z[20] = abb[28] * (T(1) / T(2));
z[17] = z[17] * z[20];
z[40] = abb[29] + -abb[31];
z[55] = abb[24] + z[112];
z[55] = abb[25] + (T(1) / T(2)) * z[55];
z[40] = z[40] * z[55];
z[55] = abb[32] * z[59];
z[90] = abb[25] + z[112];
z[90] = abb[30] * z[90];
z[17] = z[17] + z[40] + z[55] + -z[90];
z[17] = abb[28] * z[17];
z[40] = abb[32] * z[60];
z[55] = abb[25] + z[59];
z[111] = abb[30] * z[55];
z[113] = abb[25] * abb[29];
z[40] = z[40] + -z[111] + z[113];
z[40] = abb[32] * z[40];
z[111] = -abb[24] + z[112];
z[122] = abb[29] * (T(1) / T(2));
z[123] = z[111] * z[122];
z[57] = abb[32] * z[57];
z[57] = z[57] + -z[90] + z[123];
z[90] = abb[31] * z[57];
z[123] = abb[25] + z[60];
z[124] = prod_pow(abb[30], 2);
z[123] = z[123] * z[124];
z[125] = abb[35] + abb[37];
z[59] = z[59] * z[125];
z[125] = abb[24] + z[55];
z[125] = abb[34] * z[125];
z[126] = abb[25] * abb[35];
z[127] = abb[63] * z[58];
z[113] = abb[30] * z[113];
z[128] = abb[26] * (T(1) / T(2));
z[129] = abb[64] * z[128];
z[130] = abb[20] + abb[25];
z[131] = abb[61] * z[130];
z[17] = z[17] + z[40] + z[59] + -z[90] + -z[113] + z[123] + -z[125] + z[126] + z[127] + -z[129] + -z[131];
z[17] = -z[17] * z[62];
z[40] = 8 * abb[46];
z[36] = -abb[54] + -abb[56] + -z[16] + -z[28] + z[32] + z[36] + z[40];
z[36] = abb[6] * z[36];
z[59] = z[16] + -z[28];
z[90] = 5 * abb[46];
z[113] = abb[48] + z[90];
z[123] = -z[13] + -z[26] + z[31] + z[59] + -z[113];
z[123] = abb[9] * z[123];
z[11] = -abb[51] + z[5] + (T(-1) / T(2)) * z[11] + z[46];
z[11] = abb[4] * z[11];
z[125] = -z[28] + z[46];
z[126] = (T(1) / T(2)) * z[77];
z[127] = -z[125] + z[126];
z[129] = abb[15] * z[127];
z[131] = abb[0] * z[80];
z[132] = abb[13] * z[126];
z[133] = abb[2] * z[77];
z[134] = -abb[56] + z[73];
z[135] = -abb[5] * z[134];
z[11] = z[11] + z[36] + z[64] + z[123] + z[129] + z[131] + -z[132] + z[133] + z[135];
z[11] = abb[28] * z[11];
z[36] = z[2] + -z[28] + -z[47];
z[123] = (T(1) / T(2)) * z[106];
z[135] = abb[54] * (T(1) / T(2));
z[136] = z[36] + -z[49] + z[123] + z[135];
z[136] = abb[6] * z[136];
z[137] = -z[52] + z[129];
z[138] = abb[54] * (T(3) / T(2));
z[123] = -z[48] + z[123] + -z[138];
z[123] = abb[5] * z[123];
z[139] = z[81] + -z[133];
z[139] = 2 * z[139];
z[140] = z[132] + z[139];
z[141] = -abb[45] + z[28];
z[142] = -z[47] + z[141];
z[143] = abb[0] * z[142];
z[103] = abb[4] * z[103];
z[103] = z[103] + z[123] + z[136] + z[137] + z[140] + z[143];
z[103] = abb[31] * z[103];
z[123] = 5 * abb[45];
z[136] = 11 * abb[48];
z[144] = -z[123] + -z[136];
z[145] = abb[49] + z[39];
z[146] = 12 * abb[50];
z[34] = -16 * abb[46] + z[34] + z[93] + z[138] + (T(1) / T(2)) * z[144] + z[145] + -z[146];
z[34] = abb[6] * z[34];
z[93] = abb[45] + -abb[48];
z[144] = (T(1) / T(2)) * z[93];
z[147] = -z[48] + -z[49] + z[138] + -z[144];
z[147] = abb[5] * z[147];
z[148] = 2 * z[133];
z[34] = z[34] + -z[70] + -z[132] + -z[137] + z[147] + z[148];
z[34] = abb[29] * z[34];
z[39] = z[39] + z[108];
z[70] = -z[39] + z[73] + z[83] + -z[96];
z[70] = abb[14] * z[70];
z[137] = -z[77] + z[125];
z[147] = abb[0] * z[137];
z[75] = z[70] + z[75] + z[91] + z[99] + -z[147];
z[75] = abb[32] * z[75];
z[50] = -z[1] + z[28] + -z[50] + z[65] + -z[82];
z[50] = abb[29] * z[50];
z[82] = -abb[48] + z[48] + z[72];
z[82] = abb[30] * z[82];
z[50] = z[50] + -z[82];
z[50] = abb[0] * z[50];
z[91] = 2 * abb[45];
z[99] = -abb[54] + z[91];
z[149] = z[24] + z[99];
z[86] = z[86] + z[149];
z[150] = abb[29] * z[86];
z[151] = abb[30] * z[51];
z[152] = -z[51] + z[98];
z[152] = abb[32] * z[152];
z[152] = -z[150] + z[151] + z[152];
z[152] = abb[4] * z[152];
z[153] = abb[13] * z[77];
z[154] = 4 * z[133] + -z[153];
z[102] = z[102] + z[154];
z[102] = abb[30] * z[102];
z[11] = z[11] + z[34] + z[50] + z[75] + -z[102] + z[103] + z[152];
z[11] = abb[28] * z[11];
z[34] = abb[46] + abb[50];
z[50] = abb[55] * (T(1) / T(3));
z[75] = abb[53] * (T(5) / T(6));
z[103] = abb[56] * (T(1) / T(3));
z[3] = -abb[45] + -z[3];
z[3] = (T(1) / T(3)) * z[3] + -z[9] + (T(-1) / T(6)) * z[34] + -z[50] + z[75] + z[103] + z[135];
z[3] = abb[5] * z[3];
z[152] = z[46] + -z[49];
z[16] = -abb[47] + -4 * abb[49] + 5 * abb[53] + abb[54] * (T(5) / T(4)) + -z[16] + (T(7) / T(4)) * z[73] + z[152];
z[16] = abb[4] * z[16];
z[16] = z[16] + z[79] + z[153];
z[155] = abb[48] * (T(7) / T(2)) + -z[9];
z[4] = -z[4] + -z[23] + z[123] + z[155];
z[4] = abb[46] + abb[50] * (T(5) / T(6)) + (T(1) / T(3)) * z[4] + -z[103];
z[4] = abb[6] * z[4];
z[123] = abb[49] * (T(1) / T(3));
z[156] = abb[45] + abb[48] * (T(5) / T(3));
z[138] = z[123] + -z[138] + (T(1) / T(2)) * z[156];
z[75] = abb[51] * (T(-3) / T(2)) + abb[55] * (T(2) / T(3)) + z[34] + -z[75] + z[103] + (T(1) / T(2)) * z[138];
z[75] = abb[1] * z[75];
z[103] = abb[45] + abb[48] * (T(7) / T(3));
z[103] = -abb[51] + (T(1) / T(2)) * z[103] + z[123] + -z[135];
z[50] = abb[46] + abb[47] * (T(-1) / T(3)) + abb[50] * (T(7) / T(6)) + -z[50] + (T(1) / T(2)) * z[103];
z[50] = abb[0] * z[50];
z[103] = abb[18] + abb[19];
z[123] = abb[16] + abb[17] * (T(5) / T(3));
z[123] = (T(1) / T(3)) * z[103] + (T(1) / T(2)) * z[123];
z[138] = abb[57] + abb[58];
z[123] = z[123] * z[138];
z[60] = abb[24] * (T(-1) / T(3)) + abb[20] * (T(1) / T(2)) + abb[22] * (T(5) / T(6)) + z[60];
z[60] = -z[60] * z[62];
z[156] = 4 * abb[55];
z[9] = -z[9] + -z[156];
z[157] = abb[50] + abb[53];
z[9] = abb[46] * (T(3) / T(2)) + (T(1) / T(3)) * z[9] + (T(4) / T(3)) * z[157];
z[9] = abb[12] * z[9];
z[157] = -z[14] + z[65];
z[157] = -abb[53] + (T(1) / T(2)) * z[157];
z[157] = abb[8] * z[157];
z[3] = z[3] + z[4] + z[9] + (T(1) / T(3)) * z[16] + z[50] + z[60] + z[75] + z[123] + (T(-5) / T(12)) * z[133] + (T(5) / T(3)) * z[157];
z[4] = prod_pow(m1_set::bc<T>[0], 2);
z[3] = z[3] * z[4];
z[9] = -abb[52] + z[135];
z[16] = -z[8] + -z[9] + z[34] + -z[84] + z[89] + -z[108] + z[144];
z[16] = abb[37] * z[16];
z[39] = z[39] + z[53] + -z[85] + -3 * z[106];
z[50] = abb[29] * z[39];
z[8] = abb[45] + z[8] + z[136];
z[60] = 7 * abb[54];
z[75] = -z[8] + z[60] + z[65];
z[75] = -z[2] + z[5] + (T(1) / T(2)) * z[75] + z[95];
z[75] = abb[32] * z[75];
z[40] = z[40] + -z[65];
z[85] = 6 * abb[56];
z[21] = z[21] + z[40] + -z[85];
z[8] = z[8] + -z[53];
z[89] = z[8] + z[21] + -z[95];
z[123] = abb[30] * z[89];
z[50] = z[50] + z[75] + z[123];
z[50] = abb[32] * z[50];
z[39] = -abb[30] * z[39];
z[75] = -abb[54] + -z[49] + 3 * z[73];
z[123] = -abb[29] * z[75];
z[39] = z[39] + z[123];
z[39] = abb[29] * z[39];
z[89] = -abb[35] * z[89];
z[29] = -z[29] + z[83] + -z[95];
z[29] = abb[34] * z[29];
z[8] = -z[8] + z[65];
z[2] = abb[47] + -z[2] + (T(1) / T(2)) * z[8] + z[15];
z[2] = z[2] * z[124];
z[8] = abb[63] * z[67];
z[2] = z[2] + z[8] + z[16] + z[29] + z[39] + z[50] + z[89];
z[2] = abb[4] * z[2];
z[8] = -z[84] + z[85];
z[16] = 4 * abb[52] + z[8] + z[25] + -z[83] + -z[94] + z[141];
z[25] = abb[9] * z[16];
z[29] = z[92] + -z[133];
z[25] = z[25] + -z[29];
z[35] = 5 * abb[50] + -abb[52] + abb[54] * (T(7) / T(2)) + -z[35] + (T(3) / T(2)) * z[73] + z[90] + -z[145];
z[35] = abb[14] * z[35];
z[39] = -abb[6] * z[88];
z[50] = -abb[5] * z[51];
z[9] = abb[50] + -abb[51] + z[9] + (T(1) / T(2)) * z[73] + z[80];
z[9] = abb[0] * z[9];
z[9] = z[9] + z[25] + z[35] + z[39] + z[50];
z[9] = abb[37] * z[9];
z[35] = z[135] + z[144];
z[36] = -z[35] + z[36];
z[36] = abb[6] * z[36];
z[39] = abb[48] + z[18];
z[39] = (T(1) / T(2)) * z[39] + -z[135];
z[50] = z[39] + z[48];
z[50] = abb[5] * z[50];
z[36] = -z[36] + z[50] + -z[129];
z[50] = z[81] + z[133];
z[50] = -z[36] + 2 * z[50] + -z[132];
z[50] = abb[29] * z[50];
z[51] = abb[32] * z[110];
z[74] = abb[29] * z[74];
z[51] = z[51] + z[74] + z[151];
z[51] = abb[4] * z[51];
z[74] = abb[54] + -z[28] + z[73] + z[152];
z[74] = abb[29] * z[74];
z[74] = z[74] + z[82];
z[74] = abb[0] * z[74];
z[50] = z[50] + z[51] + -z[74] + -z[102];
z[51] = -abb[45] + -z[48];
z[74] = -abb[6] * z[51];
z[41] = abb[45] + -abb[50] + abb[53] + -z[41];
z[41] = abb[5] * z[41];
z[81] = -abb[4] * z[77];
z[41] = 2 * z[41] + z[44] + z[74] + z[81] + z[104] + z[133];
z[41] = abb[31] * z[41];
z[44] = abb[32] * z[105];
z[41] = z[41] + z[44] + -z[50];
z[41] = abb[31] * z[41];
z[44] = z[19] + -z[85];
z[74] = 24 * abb[46] + 18 * abb[50] + -10 * abb[51] + z[18] + z[44] + -z[53] + z[95] + -z[156];
z[74] = abb[34] * z[74];
z[81] = abb[29] * z[22];
z[42] = -z[28] + z[42];
z[82] = z[42] + z[134];
z[83] = abb[31] * z[82];
z[81] = z[81] + z[83];
z[83] = -abb[28] * z[22];
z[81] = 2 * z[81] + z[83];
z[81] = abb[28] * z[81];
z[22] = -prod_pow(abb[29], 2) * z[22];
z[83] = z[49] + -z[87] + z[116];
z[83] = prod_pow(abb[32], 2) * z[83];
z[43] = -z[43] + z[77];
z[43] = abb[31] * z[43];
z[85] = -abb[29] * z[82];
z[38] = -abb[32] * z[38];
z[38] = z[38] + z[85];
z[38] = 2 * z[38] + z[43];
z[38] = abb[31] * z[38];
z[43] = 3 * abb[50];
z[12] = z[12] + z[43];
z[61] = -abb[52] + z[12] + -z[61];
z[61] = abb[37] * z[61];
z[85] = 2 * z[116];
z[88] = -abb[63] * z[85];
z[22] = z[22] + z[38] + 2 * z[61] + z[74] + z[81] + z[83] + z[88];
z[22] = abb[1] * z[22];
z[38] = -3 * abb[16] + -abb[17] + z[103];
z[38] = z[38] * z[122];
z[61] = abb[16] + 3 * abb[17] + z[103];
z[74] = abb[31] * (T(1) / T(2));
z[61] = z[61] * z[74];
z[81] = abb[18] + -abb[19];
z[20] = z[20] * z[81];
z[81] = abb[16] + abb[17];
z[83] = abb[32] * z[81];
z[88] = -abb[16] + abb[18];
z[88] = abb[30] * z[88];
z[83] = z[83] + z[88];
z[20] = z[20] + z[38] + -z[61] + -z[83];
z[20] = abb[28] * z[20];
z[38] = abb[19] + 3 * z[81];
z[61] = -abb[18] + z[38];
z[61] = z[61] * z[122];
z[89] = abb[16] * abb[32];
z[88] = z[61] + z[88] + z[89] + z[121];
z[88] = abb[31] * z[88];
z[89] = abb[17] * abb[30];
z[102] = abb[17] * abb[29];
z[89] = z[89] + -z[102];
z[89] = abb[29] * z[89];
z[103] = -abb[16] + abb[19];
z[103] = abb[34] * z[103];
z[105] = abb[17] * z[124];
z[102] = abb[32] * z[102];
z[110] = -abb[5] + -abb[6] + abb[13] + abb[15];
z[110] = -abb[4] + -2 * abb[27] + (T(1) / T(2)) * z[110];
z[116] = abb[64] * z[110];
z[20] = z[20] + z[88] + -z[89] + z[102] + -z[103] + (T(1) / T(2)) * z[105] + -z[116];
z[88] = -abb[57] * z[20];
z[19] = -z[19] + z[54] + z[84] + -z[91] + -z[97];
z[19] = abb[4] * z[19];
z[54] = z[49] + z[53] + -5 * z[73];
z[54] = abb[6] * z[54];
z[84] = z[62] * z[112];
z[89] = abb[5] * z[75];
z[5] = abb[49] + -z[5] + -z[137];
z[97] = 2 * abb[0];
z[5] = z[5] * z[97];
z[97] = -abb[18] + z[81];
z[102] = -abb[57] * z[97];
z[5] = z[5] + z[19] + z[54] + z[84] + z[89] + z[102] + -z[154];
z[5] = abb[36] * z[5];
z[13] = -z[13] + z[87];
z[19] = abb[48] + z[91];
z[54] = 5 * abb[49];
z[10] = -z[10] + z[13] + -2 * z[19] + z[23] + -z[54] + z[90];
z[19] = -abb[1] * z[10];
z[45] = z[1] + z[45] + -z[78] + z[108];
z[45] = abb[6] * z[45];
z[78] = 4 * abb[53];
z[30] = -z[30] + z[54] + -z[78] + z[156];
z[30] = abb[12] * z[30];
z[54] = abb[12] * z[1];
z[54] = z[54] + z[79];
z[30] = z[30] + -z[45] + -z[54] + z[68];
z[14] = -z[14] + z[18] + -z[46] + -z[109];
z[45] = -abb[5] * z[14];
z[46] = abb[4] * z[67];
z[84] = abb[22] + z[111];
z[87] = -z[62] * z[84];
z[81] = abb[19] + z[81];
z[89] = abb[57] * z[81];
z[19] = z[19] + -z[30] + z[45] + z[46] + z[87] + z[89] + 2 * z[131];
z[19] = abb[62] * z[19];
z[45] = -abb[33] * z[69];
z[45] = z[45] + -z[120];
z[45] = abb[33] * z[45];
z[69] = abb[17] + abb[18];
z[87] = abb[61] * z[69];
z[90] = -abb[36] * z[97];
z[97] = abb[62] * z[81];
z[20] = -z[20] + z[45] + z[87] + z[90] + z[97];
z[20] = abb[58] * z[20];
z[16] = abb[35] * z[16];
z[1] = -13 * abb[46] + -5 * abb[52] + -z[1] + -z[23] + z[60] + 2 * z[93] + z[156];
z[1] = abb[34] * z[1];
z[1] = z[1] + z[16];
z[1] = abb[9] * z[1];
z[16] = -abb[48] + z[47] + z[94];
z[16] = (T(1) / T(2)) * z[16] + -z[34] + -z[49] + -z[98];
z[16] = z[16] * z[124];
z[32] = -z[32] + z[33] + -z[40] + -z[149];
z[32] = abb[29] * z[32];
z[33] = abb[30] * z[86];
z[32] = z[32] + z[33];
z[32] = abb[29] * z[32];
z[33] = z[98] + z[137];
z[40] = -abb[35] * z[33];
z[13] = abb[52] + -z[13] + -z[65] + z[113];
z[13] = abb[34] * z[13];
z[13] = z[13] + z[16] + z[32] + z[40];
z[13] = abb[0] * z[13];
z[16] = z[21] + z[24] + z[91] + -z[96];
z[16] = abb[14] * z[16];
z[21] = z[23] + z[44] + z[47] + z[99];
z[24] = abb[6] * z[21];
z[24] = -z[16] + z[24] + -z[25];
z[24] = abb[30] * z[24];
z[25] = z[94] + z[106];
z[12] = abb[55] + -z[12] + (T(-1) / T(2)) * z[25] + z[31] + z[95];
z[12] = abb[9] * z[12];
z[7] = -z[7] + -z[28] + z[135] + -z[155];
z[15] = z[7] + z[15];
z[15] = abb[6] * z[15];
z[12] = z[12] + z[15] + -z[64];
z[15] = abb[51] + -z[34] + z[126];
z[15] = abb[0] * z[15];
z[15] = z[12] + z[15] + -z[56] + z[70] + (T(1) / T(2)) * z[133];
z[15] = abb[32] * z[15];
z[25] = abb[30] * z[33];
z[25] = z[25] + -z[150];
z[25] = abb[0] * z[25];
z[31] = z[27] * z[75];
z[31] = z[31] + z[76];
z[32] = -abb[29] * z[31];
z[15] = z[15] + z[24] + z[25] + z[32];
z[15] = abb[32] * z[15];
z[8] = 18 * abb[46] + 5 * abb[48] + -z[8] + -z[23] + -z[26] + z[146] + -z[156];
z[8] = abb[34] * z[8];
z[23] = -abb[35] * z[21];
z[8] = z[8] + z[23];
z[8] = abb[6] * z[8];
z[23] = z[53] + -z[73];
z[23] = -abb[56] + (T(1) / T(2)) * z[23];
z[23] = abb[5] * z[23];
z[12] = z[12] + z[23] + z[52] + (T(-3) / T(2)) * z[133];
z[12] = z[12] * z[124];
z[23] = -abb[29] * abb[30];
z[24] = abb[29] + -abb[30];
z[24] = abb[32] * z[24];
z[23] = abb[35] + abb[36] + z[23] + z[24] + z[124];
z[21] = z[21] * z[23];
z[7] = abb[56] + (T(1) / T(3)) * z[7];
z[4] = z[4] * z[7];
z[4] = z[4] + z[21];
z[4] = abb[3] * z[4];
z[7] = z[16] + -z[29];
z[7] = abb[35] * z[7];
z[21] = abb[14] * z[63];
z[21] = z[21] + -z[92];
z[23] = abb[5] * z[67];
z[23] = z[21] + -z[23] + z[68];
z[24] = -abb[63] * z[23];
z[16] = -z[16] + z[79] + z[92];
z[16] = abb[34] * z[16];
z[25] = z[35] + z[48];
z[25] = abb[17] * z[25];
z[26] = -z[28] + z[39] + z[47];
z[26] = abb[16] * z[26];
z[28] = abb[19] * z[127];
z[29] = abb[18] * z[126];
z[25] = z[25] + z[26] + z[28] + z[29];
z[26] = abb[64] * z[25];
z[28] = abb[30] * z[31];
z[0] = -abb[49] + z[0] + -z[18] + z[43];
z[0] = z[0] * z[27];
z[18] = -abb[5] * z[51];
z[0] = z[0] + z[18] + -z[133];
z[0] = abb[29] * z[0];
z[0] = z[0] + z[28];
z[0] = abb[29] * z[0];
z[18] = -z[21] + z[148];
z[21] = z[101] + -z[153];
z[28] = abb[57] * z[69];
z[28] = -z[18] + -z[21] + z[28] + z[143];
z[28] = abb[61] * z[28];
z[29] = abb[47] + abb[49] + -z[72] + -z[125];
z[31] = -abb[67] * z[29];
z[0] = abb[65] + abb[66] + z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[11] + z[12] + z[13] + z[15] + z[16] + z[17] + z[19] + z[20] + z[22] + z[24] + z[26] + z[28] + z[31] + z[41] + z[88];
z[1] = z[37] + z[66];
z[2] = abb[5] * z[1];
z[2] = z[2] + z[100] + -z[147];
z[3] = -z[59] + z[78] + z[107] + -z[108];
z[3] = abb[4] * z[3];
z[4] = abb[20] + abb[22];
z[4] = -abb[24] + 2 * z[4] + z[55];
z[4] = -z[4] * z[62];
z[5] = z[71] * z[137];
z[6] = z[117] * z[119];
z[7] = abb[46] + z[115];
z[7] = abb[12] * z[7];
z[3] = z[2] + z[3] + z[4] + z[5] + z[6] + 4 * z[7] + z[54] + -2 * z[68] + z[89] + z[92];
z[3] = abb[33] * z[3];
z[4] = abb[4] + -z[71];
z[4] = z[4] * z[137];
z[5] = z[125] + z[134];
z[5] = z[5] * z[27];
z[4] = z[4] + z[5] + -z[21] + -z[52] + -z[92] + z[104] + z[139];
z[4] = abb[28] * z[4];
z[1] = -abb[4] * z[1];
z[1] = z[1] + z[36] + z[68] + -z[140] + z[147];
z[1] = abb[31] * z[1];
z[2] = -z[2] + z[68];
z[2] = abb[32] * z[2];
z[5] = z[82] * z[114];
z[6] = -z[42] + z[77];
z[6] = abb[31] * z[6];
z[5] = z[5] + z[6] + -z[118];
z[5] = z[5] * z[119];
z[6] = abb[18] + z[38];
z[6] = z[6] * z[74];
z[7] = abb[28] * z[69];
z[6] = -z[6] + z[7] + -z[61] + -z[83];
z[7] = abb[57] * z[6];
z[8] = abb[33] * z[81];
z[6] = z[6] + z[8];
z[6] = abb[58] * z[6];
z[8] = abb[22] + (T(1) / T(2)) * z[111];
z[8] = abb[31] * z[8];
z[9] = abb[28] * z[130];
z[8] = z[8] + z[9] + z[57];
z[8] = z[8] * z[62];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + -z[50];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[39] * z[10];
z[3] = -abb[40] * z[85];
z[2] = z[2] + z[3];
z[2] = abb[1] * z[2];
z[3] = -abb[39] * z[30];
z[4] = -abb[39] * z[14];
z[5] = -abb[38] * z[77];
z[4] = z[4] + z[5];
z[4] = abb[5] * z[4];
z[5] = -abb[40] * z[23];
z[6] = z[110] * z[138];
z[7] = z[62] * z[128];
z[6] = z[6] + z[7] + z[25];
z[6] = abb[41] * z[6];
z[7] = abb[40] * z[58];
z[8] = abb[39] * z[84];
z[9] = abb[38] * z[130];
z[7] = z[7] + z[8] + -z[9];
z[7] = -z[7] * z[62];
z[8] = -z[18] + z[153];
z[8] = abb[38] * z[8];
z[9] = abb[39] * z[81];
z[10] = abb[38] * z[69];
z[9] = z[9] + z[10];
z[9] = z[9] * z[138];
z[10] = -abb[44] * z[29];
z[11] = abb[38] * z[142];
z[12] = 2 * z[80];
z[12] = abb[39] * z[12];
z[11] = z[11] + z[12];
z[11] = abb[0] * z[11];
z[12] = abb[39] + abb[40];
z[12] = z[12] * z[46];
z[1] = abb[42] + abb[43] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_659_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("1.6998821841373349780908535252282326406651977555360099955793770223"),stof<T>("-0.19425610876090151741340666725705101861638578849696195204159848")}, std::complex<T>{stof<T>("-2.754362249599921985189522502564216264468658200496839632231558848"),stof<T>("4.5758817812285534414847759957584789506505660580871473303792318232")}, stof<T>("7.2903750035089039573830922628708475851031178573555162241913142345"), std::complex<T>{stof<T>("-2.0156640832988733719606137261277754849443773192917153730660807468"),stof<T>("-9.8101203061006967871590423959948370570987047854916615491555962234")}, std::complex<T>{stof<T>("-5.8960233692386057588512942962128207380279398191183881792731974402"),stof<T>("-10.8585414287506369664111370156665127371457231792196756780107851216")}, std::complex<T>{stof<T>("-10.1714816702235455227595215759996634462678321861777665927611687704"),stof<T>("3.333204549817711744819274708829752251987161875862171249482444445")}, std::complex<T>{stof<T>("6.0666391264617025789854559254392443014633108622730192198673867702"),stof<T>("-5.0497755833144549446096779290700272941909024874627115469137068309")}, std::complex<T>{stof<T>("5.379982675798251584941789086443810122136937441715955438686577521"),stof<T>("17.515157361182847339700868237235297034760897382388858896796733101")}, std::complex<T>{stof<T>("-3.6698300063490346345036642997310546118443251055516215333383161093"),stof<T>("4.7102964200013096593830241595124050741098753502435860467147967252")}, std::complex<T>{stof<T>("0.3892962763256345937225983992044110191949462490770220042483242311"),stof<T>("-7.8992931638430520699552325084975109962785783853941592996827353574")}, std::complex<T>{stof<T>("-1.6048809138609959509188142370120174883144154325121793177869173784"),stof<T>("5.0998238860993871277760182364824319829888294352480755024407994981")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(abs(k.W[77])) - rlog(abs(kbase.W[77])), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_659_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_659_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (-v[3] + v[5]) * (-8 + 12 * v[0] + -4 * v[1] + v[3] + -5 * v[5] + 2 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return (abb[45] + abb[48] + -abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[28] + -abb[31];
z[1] = abb[29] + z[0];
z[1] = abb[29] * z[1];
z[0] = abb[30] + z[0];
z[0] = abb[30] * z[0];
z[0] = -abb[36] + -z[0] + z[1];
z[1] = abb[45] + abb[48] + -abb[56];
return 2 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_659_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return (abb[45] + abb[48] + -abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[29] + abb[30];
z[1] = abb[45] + abb[48] + -abb[56];
return 2 * abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_659_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[2] + v[3]) * (2 * (8 * v[0] + 4 * v[1] + 7 * v[2] + 3 * v[3] + -4 * v[4]) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[1] = ((T(1) / T(32)) * (v[2] + v[3]) * (2 * (8 * v[0] + 4 * v[1] + 5 * v[2] + v[3] + -4 * v[4] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5])) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(32)) * (v[2] + v[3]) * (4 * (-36 + 4 * v[0] + 2 * v[1] + 13 * v[2] + 11 * v[3] + -2 * v[4] + -8 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -18 * v[5]) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[3] = (m1_set::bc<T>[2] * (T(-3) / T(2)) * (v[2] + v[3])) / tend;
c[4] = ((4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(1) / T(4)) * (v[2] + v[3])) / tend;
c[5] = ((-1 + 2 * m1_set::bc<T>[1] + m1_set::bc<T>[2]) * (T(3) / T(2)) * (v[2] + v[3])) / tend;


		return t * ((abb[46] + -abb[47] + abb[50] + -abb[51] + abb[52]) * c[0] + abb[45] * c[1] + abb[48] * c[2] + -abb[54] * c[2] + abb[51] * (-c[1] + c[2])) + abb[46] * c[3] + -abb[47] * c[3] + abb[50] * c[3] + abb[52] * c[3] + abb[45] * c[4] + abb[51] * c[5] + abb[48] * (c[3] + c[4] + c[5]) + -abb[54] * (c[3] + c[4] + c[5]);
	}
	{
T z[8];
z[0] = 2 * abb[45];
z[1] = -abb[48] + abb[54];
z[2] = z[0] + z[1];
z[3] = -3 * abb[51] + z[2];
z[3] = abb[32] * z[3];
z[0] = abb[51] * (T(-3) / T(2)) + -z[0] + (T(7) / T(2)) * z[1];
z[0] = abb[30] * z[0];
z[0] = z[0] + z[3];
z[0] = abb[30] * z[0];
z[3] = prod_pow(abb[32], 2);
z[3] = (T(9) / T(2)) * z[3];
z[4] = abb[37] + -z[3];
z[4] = z[1] * z[4];
z[5] = abb[50] + abb[52];
z[5] = 2 * z[5];
z[6] = abb[34] + -abb[37];
z[5] = z[5] * z[6];
z[2] = -abb[35] * z[2];
z[3] = 3 * abb[35] + z[3] + -2 * z[6];
z[3] = abb[51] * z[3];
z[7] = -abb[37] * abb[45];
z[1] = abb[45] + -z[1];
z[1] = abb[34] * z[1];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[7];
z[0] = abb[11] * z[0];
z[1] = abb[46] + -abb[47];
z[2] = 2 * abb[11];
z[1] = z[1] * z[2] * z[6];
return z[0] + z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_659_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_659_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("8.2739611769218150643371769714977306890112298652685725958243571042"),stof<T>("-3.257769207872990923870296949427624775879145575046020273411930133")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18, 77});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,68> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W23(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(abs(kend.W[77])) - rlog(abs(k.W[77])), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[42] = SpDLog_f_4_659_W_16_Im(t, path, abb);
abb[43] = SpDLog_f_4_659_W_19_Im(t, path, abb);
abb[44] = SpDLogQ_W_78(k,dl,dlr).imag();
abb[65] = SpDLog_f_4_659_W_16_Re(t, path, abb);
abb[66] = SpDLog_f_4_659_W_19_Re(t, path, abb);
abb[67] = SpDLogQ_W_78(k,dl,dlr).real();

                    
            return f_4_659_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_659_DLogXconstant_part(base_point<T>, kend);
	value += f_4_659_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_659_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_659_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_659_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_659_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_659_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_659_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
