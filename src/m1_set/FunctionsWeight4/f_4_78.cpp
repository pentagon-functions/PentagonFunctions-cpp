/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_78.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_78_abbreviated (const std::array<T,58>& abb) {
T z[123];
z[0] = abb[28] + -abb[31];
z[1] = -abb[29] + z[0];
z[2] = abb[30] * (T(1) / T(2));
z[3] = z[1] + z[2];
z[4] = abb[30] * z[3];
z[5] = abb[26] * (T(1) / T(2));
z[6] = -abb[29] + z[5];
z[6] = abb[26] * z[6];
z[7] = abb[29] + abb[31];
z[8] = abb[29] * z[7];
z[6] = z[6] + z[8];
z[9] = -abb[26] + abb[28];
z[10] = abb[29] + z[9];
z[11] = abb[27] * (T(1) / T(2));
z[12] = z[10] + -z[11];
z[12] = abb[27] * z[12];
z[13] = abb[34] + abb[35];
z[14] = prod_pow(abb[31], 2);
z[15] = (T(1) / T(2)) * z[14];
z[16] = abb[33] + z[15];
z[17] = abb[26] + abb[31];
z[18] = -abb[28] + z[17];
z[19] = abb[28] * z[18];
z[4] = z[4] + z[6] + -z[12] + z[13] + z[16] + -z[19];
z[20] = abb[6] * (T(1) / T(2));
z[21] = -z[4] * z[20];
z[22] = -abb[32] + z[13];
z[12] = z[12] + -z[22];
z[23] = prod_pow(abb[29], 2);
z[24] = (T(1) / T(2)) * z[23];
z[25] = z[12] + -z[24];
z[26] = prod_pow(abb[26], 2);
z[27] = prod_pow(abb[28], 2);
z[28] = (T(1) / T(2)) * z[27];
z[29] = (T(1) / T(2)) * z[26] + -z[28];
z[30] = z[25] + -z[29];
z[30] = abb[9] * z[30];
z[31] = z[15] + -z[28];
z[31] = abb[11] * z[31];
z[32] = -abb[11] * z[0];
z[33] = abb[27] * z[32];
z[34] = abb[11] * abb[33];
z[31] = z[31] + z[33] + -z[34];
z[33] = z[30] + -z[31];
z[35] = abb[28] * (T(1) / T(2));
z[36] = -z[17] + z[35];
z[36] = abb[28] * z[36];
z[37] = z[5] * z[17];
z[38] = -abb[28] + (T(1) / T(2)) * z[17];
z[39] = abb[30] * z[38];
z[36] = abb[32] + z[16] + z[36] + z[37] + -z[39];
z[36] = abb[13] * z[36];
z[37] = abb[26] * z[7];
z[40] = -abb[27] + z[10];
z[41] = abb[30] * z[40];
z[37] = -z[8] + -z[19] + z[37] + z[41];
z[41] = -abb[32] + abb[34];
z[42] = abb[27] * z[1];
z[42] = -z[37] + z[41] + z[42];
z[43] = abb[12] * (T(1) / T(2));
z[42] = z[42] * z[43];
z[44] = abb[8] * (T(1) / T(2));
z[45] = -abb[1] + z[44];
z[46] = z[20] + z[45];
z[47] = abb[7] * (T(1) / T(2));
z[48] = abb[5] * (T(1) / T(2));
z[49] = -z[43] + -z[46] + z[47] + z[48];
z[50] = -abb[55] * z[49];
z[51] = abb[31] + z[5];
z[51] = abb[26] * z[51];
z[51] = z[15] + z[51];
z[52] = 3 * abb[32];
z[53] = 3 * abb[33];
z[54] = z[52] + z[53];
z[55] = -abb[35] + z[54];
z[56] = z[51] + z[55];
z[57] = 2 * abb[28];
z[58] = -z[17] + z[57];
z[59] = abb[27] + z[58];
z[60] = z[2] + z[59];
z[60] = abb[30] * z[60];
z[60] = z[56] + z[60];
z[61] = z[11] + -z[58];
z[61] = abb[27] * z[61];
z[62] = -abb[28] + 2 * z[17];
z[62] = abb[28] * z[62];
z[61] = -z[60] + z[61] + z[62];
z[61] = abb[1] * z[61];
z[63] = abb[27] * z[0];
z[64] = z[16] + -z[28] + z[63];
z[65] = -z[47] * z[64];
z[66] = -z[9] + z[11];
z[66] = abb[27] * z[66];
z[67] = -abb[35] + z[19];
z[66] = z[16] + z[66] + -z[67];
z[68] = abb[27] + z[38];
z[69] = z[2] + z[68];
z[70] = abb[30] * z[69];
z[71] = abb[31] * (T(1) / T(2));
z[72] = abb[26] + z[71];
z[72] = abb[26] * z[72];
z[73] = -z[70] + z[72];
z[74] = -z[66] + -z[73];
z[74] = z[44] * z[74];
z[25] = z[25] + z[29];
z[29] = -z[25] * z[48];
z[75] = abb[29] + z[71];
z[76] = -z[5] + z[75];
z[77] = z[2] + z[76];
z[77] = abb[30] * z[77];
z[77] = -z[14] + z[77];
z[78] = abb[26] * z[76];
z[79] = abb[29] * (T(1) / T(2));
z[80] = abb[31] + z[79];
z[80] = abb[29] * z[80];
z[81] = z[77] + z[78] + -z[80];
z[82] = abb[0] * (T(1) / T(2));
z[81] = z[81] * z[82];
z[83] = abb[15] + abb[17];
z[84] = abb[36] * (T(3) / T(4));
z[85] = z[83] * z[84];
z[21] = z[21] + z[29] + z[33] + (T(3) / T(2)) * z[36] + z[42] + z[50] + z[61] + z[65] + z[74] + z[81] + z[85];
z[29] = 3 * abb[49];
z[21] = z[21] * z[29];
z[42] = abb[28] * (T(5) / T(2));
z[50] = 2 * abb[31];
z[61] = -z[5] + z[42] + -z[50];
z[65] = abb[27] * (T(5) / T(4));
z[74] = -z[61] + z[65];
z[74] = abb[27] * z[74];
z[35] = -z[11] + z[35];
z[81] = 2 * abb[29];
z[86] = z[35] + z[81];
z[87] = abb[31] * (T(3) / T(4));
z[88] = abb[30] * (T(3) / T(4));
z[89] = abb[26] * (T(-5) / T(4)) + z[86] + z[87] + -z[88];
z[89] = abb[30] * z[89];
z[90] = 2 * z[8];
z[91] = (T(1) / T(2)) * z[19];
z[92] = (T(3) / T(2)) * z[14];
z[93] = abb[35] + -z[92];
z[94] = abb[31] * (T(5) / T(4)) + z[81];
z[95] = abb[26] * z[94];
z[74] = abb[33] * (T(-3) / T(2)) + z[74] + z[89] + -z[90] + -z[91] + (T(1) / T(2)) * z[93] + z[95];
z[74] = abb[8] * z[74];
z[89] = abb[7] * (T(3) / T(2));
z[93] = z[64] * z[89];
z[95] = abb[5] * (T(3) / T(2));
z[96] = z[25] * z[95];
z[93] = z[93] + -z[96];
z[96] = z[11] + z[58];
z[96] = abb[27] * z[96];
z[18] = z[18] * z[57];
z[60] = -z[18] + z[60] + z[96];
z[96] = abb[1] * z[60];
z[97] = (T(1) / T(2)) * z[36];
z[98] = -z[30] + -z[97];
z[99] = z[5] * z[7];
z[100] = z[2] * z[40];
z[99] = (T(1) / T(2)) * z[8] + z[91] + -z[99] + -z[100];
z[100] = 4 * abb[27];
z[101] = abb[28] * (T(7) / T(2));
z[102] = 9 * abb[29] + abb[31];
z[102] = -4 * abb[26] + -z[100] + z[101] + (T(1) / T(2)) * z[102];
z[102] = abb[27] * z[102];
z[102] = -4 * abb[35] + (T(-9) / T(2)) * z[41] + -z[99] + z[102];
z[102] = abb[12] * z[102];
z[6] = z[6] + z[15] + -z[67];
z[103] = 3 * abb[34];
z[104] = z[53] + z[103];
z[105] = z[6] + -z[104];
z[106] = abb[29] * (T(3) / T(2));
z[107] = abb[27] * (T(7) / T(4));
z[61] = -z[61] + z[106] + -z[107];
z[61] = abb[27] * z[61];
z[108] = (T(1) / T(2)) * z[1];
z[109] = 2 * abb[27];
z[110] = z[108] + -z[109];
z[111] = abb[30] * (T(1) / T(4));
z[112] = z[110] + z[111];
z[112] = abb[30] * z[112];
z[61] = z[61] + (T(1) / T(2)) * z[105] + z[112];
z[61] = abb[6] * z[61];
z[61] = z[61] + z[74] + z[93] + z[96] + 3 * z[98] + z[102];
z[61] = abb[48] * z[61];
z[74] = abb[27] + 2 * z[58];
z[74] = abb[27] * z[74];
z[98] = abb[30] + 2 * z[59];
z[98] = abb[30] * z[98];
z[102] = 6 * abb[33];
z[105] = z[14] + z[102];
z[113] = abb[26] + z[50];
z[113] = abb[26] * z[113];
z[114] = 2 * abb[35];
z[74] = 6 * abb[32] + -4 * z[19] + z[74] + z[98] + z[105] + z[113] + -z[114];
z[98] = abb[1] * z[74];
z[33] = -z[33] + -z[36];
z[36] = 2 * abb[26];
z[113] = z[36] + z[109];
z[115] = 3 * abb[29];
z[116] = abb[28] + abb[31] + -z[113] + z[115];
z[116] = abb[27] * z[116];
z[37] = z[37] + z[52] + -z[103] + -z[114] + z[116];
z[52] = abb[12] * z[37];
z[116] = -abb[26] + z[50];
z[117] = -abb[28] + z[116];
z[118] = z[115] + z[117];
z[119] = abb[27] * (T(5) / T(2)) + -z[118];
z[119] = abb[27] * z[119];
z[3] = z[3] + z[109];
z[3] = abb[30] * z[3];
z[3] = z[3] + z[6] + z[104] + z[119];
z[3] = abb[6] * z[3];
z[104] = -z[11] + -z[117];
z[104] = abb[27] * z[104];
z[53] = -z[15] + z[53] + -z[67] + z[73] + z[104];
z[53] = abb[8] * z[53];
z[73] = z[5] + z[75];
z[73] = abb[26] * z[73];
z[104] = -abb[31] + z[79];
z[104] = abb[29] * z[104];
z[104] = z[73] + z[77] + z[104];
z[119] = -abb[0] * z[104];
z[120] = abb[36] * z[83];
z[3] = z[3] + 3 * z[33] + z[52] + z[53] + z[98] + z[119] + (T(-3) / T(2)) * z[120];
z[3] = abb[43] * z[3];
z[33] = -abb[30] * z[58];
z[52] = 2 * abb[33];
z[53] = -abb[26] * z[17];
z[33] = -2 * abb[32] + -z[14] + z[33] + -z[52] + z[53] + z[62];
z[33] = abb[13] * z[33];
z[53] = z[23] + z[26];
z[62] = -abb[32] + abb[34] + abb[35];
z[10] = abb[27] + -2 * z[10];
z[10] = abb[27] * z[10];
z[10] = z[10] + -z[27] + z[53] + 2 * z[62];
z[10] = abb[9] * z[10];
z[62] = z[14] + -z[27];
z[62] = abb[11] * z[62];
z[98] = z[32] * z[109];
z[10] = -z[10] + -z[33] + 2 * z[34] + -z[62] + -z[98] + z[120];
z[33] = 2 * abb[1] + abb[3];
z[34] = z[33] * z[74];
z[18] = z[18] + -z[114];
z[62] = 5 * abb[27] + -2 * z[118];
z[62] = abb[27] * z[62];
z[74] = z[1] + z[109];
z[74] = abb[30] + 2 * z[74];
z[74] = abb[30] * z[74];
z[98] = -abb[26] + z[81];
z[118] = -abb[26] * z[98];
z[62] = 6 * abb[34] + -z[18] + z[62] + z[74] + z[90] + z[105] + z[118];
z[62] = abb[6] * z[62];
z[58] = -abb[30] + z[58] + -z[109];
z[58] = abb[30] * z[58];
z[74] = abb[31] + z[36];
z[74] = abb[26] * z[74];
z[105] = -abb[27] + -2 * z[117];
z[105] = abb[27] * z[105];
z[18] = -z[14] + -z[18] + z[58] + z[74] + z[102] + z[105];
z[18] = abb[8] * z[18];
z[58] = 2 * abb[12];
z[37] = z[37] * z[58];
z[74] = -z[17] + -z[81];
z[74] = abb[26] * z[74];
z[102] = abb[26] + -abb[31];
z[105] = -z[81] + z[102];
z[117] = -abb[30] + z[105];
z[117] = abb[30] * z[117];
z[118] = -abb[29] + z[50];
z[118] = abb[29] * z[118];
z[74] = 2 * z[14] + z[74] + z[117] + z[118];
z[74] = abb[0] * z[74];
z[10] = -3 * z[10] + z[18] + z[34] + z[37] + z[62] + z[74];
z[10] = abb[42] * z[10];
z[18] = -z[8] + z[103];
z[34] = -abb[29] + z[71];
z[37] = abb[26] * z[34];
z[37] = -z[18] + z[37] + z[55] + z[92];
z[62] = abb[29] + abb[31] * (T(3) / T(2));
z[71] = -z[5] + -z[62];
z[74] = abb[27] + -abb[28];
z[71] = (T(1) / T(2)) * z[71] + -z[74] + z[88];
z[71] = abb[30] * z[71];
z[88] = -z[17] + z[115];
z[65] = abb[28] + -z[65] + (T(1) / T(2)) * z[88];
z[65] = abb[27] * z[65];
z[65] = -z[19] + z[65];
z[37] = (T(1) / T(2)) * z[37] + z[65] + z[71];
z[37] = abb[48] * z[37];
z[62] = abb[26] + z[62];
z[62] = abb[26] * z[62];
z[18] = -z[15] + z[18] + z[55] + z[62];
z[55] = abb[26] * (T(3) / T(2));
z[34] = -z[34] + -z[55];
z[34] = abb[28] + (T(1) / T(2)) * z[34] + z[109] + -z[111];
z[34] = abb[30] * z[34];
z[62] = -z[17] + -z[115];
z[62] = abb[28] + (T(1) / T(2)) * z[62] + z[107];
z[62] = abb[27] * z[62];
z[18] = (T(1) / T(2)) * z[18] + -z[19] + z[34] + z[62];
z[18] = abb[44] * z[18];
z[34] = abb[43] * z[60];
z[13] = z[13] + z[24] + -z[51] + -z[54];
z[51] = -abb[29] + z[17];
z[51] = -abb[28] + abb[27] * (T(1) / T(4)) + (T(1) / T(2)) * z[51];
z[51] = abb[27] * z[51];
z[13] = (T(1) / T(2)) * z[13] + z[19] + z[39] + z[51];
z[13] = z[13] * z[29];
z[19] = (T(-3) / T(2)) * z[23] + z[56] + -z[103];
z[19] = (T(1) / T(2)) * z[19] + z[65] + -z[70];
z[19] = abb[45] * z[19];
z[23] = -z[14] + z[73];
z[39] = abb[30] * (T(-5) / T(2)) + z[76];
z[39] = abb[30] * z[39];
z[51] = abb[31] + abb[29] * (T(5) / T(2));
z[51] = abb[29] * z[51];
z[39] = z[23] + z[39] + -z[51];
z[54] = abb[41] * (T(1) / T(2));
z[39] = z[39] * z[54];
z[56] = abb[36] * abb[50];
z[13] = z[13] + z[18] + z[19] + z[34] + z[37] + z[39] + (T(-9) / T(4)) * z[56];
z[13] = abb[3] * z[13];
z[18] = 5 * abb[1];
z[19] = abb[6] * (T(7) / T(2)) + -z[18];
z[34] = (T(1) / T(3)) * z[19] + z[44];
z[34] = abb[48] * z[34];
z[37] = z[46] + z[82];
z[37] = abb[49] * z[37];
z[39] = abb[43] * (T(5) / T(3));
z[46] = -abb[48] + abb[44] * (T(-7) / T(3)) + abb[41] * (T(13) / T(3));
z[46] = abb[45] * (T(5) / T(3)) + -z[39] + (T(1) / T(2)) * z[46];
z[46] = abb[3] * z[46];
z[34] = z[34] + z[37] + z[46];
z[18] = abb[6] * (T(-17) / T(2)) + abb[8] * (T(7) / T(2)) + -z[18];
z[18] = abb[44] * z[18];
z[37] = 5 * abb[48] + abb[41] * (T(13) / T(2));
z[37] = abb[0] * z[37];
z[19] = abb[0] * (T(-13) / T(2)) + abb[8] * (T(23) / T(2)) + z[19];
z[19] = abb[45] * z[19];
z[18] = z[18] + z[19] + z[37];
z[19] = abb[45] + -abb[49];
z[37] = abb[44] + -abb[48];
z[46] = z[19] + -z[37];
z[56] = -abb[4] * z[46];
z[60] = abb[20] + abb[22];
z[62] = abb[21] + z[60];
z[65] = abb[19] + -abb[23] + z[62];
z[70] = abb[53] * z[65];
z[56] = z[56] + -z[70];
z[65] = abb[52] * z[65];
z[70] = abb[41] + z[37];
z[71] = abb[14] * z[70];
z[73] = -z[56] + z[65] + z[71];
z[45] = -z[20] + z[45] + z[82];
z[39] = z[39] * z[45];
z[45] = -abb[1] + abb[8];
z[88] = 3 * abb[47];
z[92] = -z[45] * z[88];
z[117] = -abb[6] + abb[8];
z[33] = abb[0] + -z[33] + z[117];
z[33] = abb[42] * z[33];
z[118] = abb[8] * abb[41];
z[119] = abb[16] + abb[18];
z[120] = (T(1) / T(4)) * z[119];
z[121] = abb[50] * z[120];
z[18] = (T(1) / T(6)) * z[18] + (T(5) / T(3)) * z[33] + (T(1) / T(2)) * z[34] + z[39] + (T(1) / T(4)) * z[73] + z[92] + (T(-4) / T(3)) * z[118] + z[121];
z[33] = prod_pow(m1_set::bc<T>[0], 2);
z[18] = z[18] * z[33];
z[25] = abb[5] * z[25];
z[34] = prod_pow(abb[27], 2);
z[27] = z[27] + z[34];
z[27] = abb[1] * z[27];
z[34] = -abb[27] * z[40];
z[34] = z[22] + z[34];
z[34] = z[34] * z[58];
z[39] = abb[0] * z[53];
z[53] = -abb[0] + abb[1];
z[53] = z[33] * z[53];
z[58] = abb[5] + -abb[12];
z[73] = abb[1] + z[58];
z[92] = abb[55] * z[73];
z[121] = abb[0] + z[58];
z[122] = -abb[54] * z[121];
z[25] = z[25] + -z[27] + z[30] + z[34] + z[39] + z[53] + z[92] + z[122];
z[30] = 3 * abb[46];
z[25] = z[25] * z[30];
z[4] = abb[20] * z[4];
z[34] = -z[2] + z[76];
z[34] = abb[30] * z[34];
z[39] = abb[26] * z[75];
z[39] = -z[8] + z[34] + z[39];
z[11] = abb[26] + -z[7] + z[11];
z[11] = abb[27] * z[11];
z[11] = z[11] + z[16] + z[22] + -z[39];
z[11] = abb[19] * z[11];
z[16] = -z[2] + z[68];
z[16] = abb[30] * z[16];
z[22] = abb[31] * z[5];
z[16] = z[16] + -z[22] + -z[66];
z[16] = abb[21] * z[16];
z[12] = z[12] + -z[28] + z[39];
z[12] = abb[22] * z[12];
z[22] = -z[34] + z[80];
z[28] = -z[22] + z[78];
z[34] = abb[23] * z[28];
z[39] = abb[55] * z[62];
z[53] = abb[36] * (T(1) / T(2));
z[66] = abb[24] * z[53];
z[4] = -z[4] + -z[11] + z[12] + z[16] + -z[34] + z[39] + z[66];
z[11] = abb[52] + abb[53];
z[12] = (T(3) / T(2)) * z[11];
z[4] = z[4] * z[12];
z[16] = -abb[31] + z[115];
z[16] = (T(1) / T(2)) * z[16] + z[42] + -z[113];
z[16] = abb[27] * z[16];
z[16] = z[16] + (T(-3) / T(2)) * z[41] + z[99] + -z[114];
z[16] = abb[12] * z[16];
z[34] = -z[31] + z[97];
z[16] = z[16] + 3 * z[34] + z[93] + -z[96];
z[34] = abb[26] * (T(3) / T(4)) + z[35] + -z[94] + z[111];
z[34] = abb[30] * z[34];
z[39] = 4 * abb[31];
z[41] = -z[5] + z[39];
z[42] = z[41] + -z[101];
z[66] = z[42] + z[107];
z[76] = abb[27] * z[66];
z[78] = -z[87] + -z[98];
z[78] = abb[26] * z[78];
z[15] = abb[35] + z[15];
z[15] = abb[33] * (T(9) / T(2)) + (T(1) / T(2)) * z[15] + z[34] + -z[76] + z[78] + z[90] + -z[91];
z[15] = abb[8] * z[15];
z[34] = 9 * abb[33];
z[6] = z[6] + z[34];
z[78] = 9 * abb[34] + z[6];
z[42] = abb[29] * (T(-9) / T(2)) + abb[27] * (T(17) / T(4)) + -z[42];
z[42] = abb[27] * z[42];
z[80] = z[100] + z[108];
z[87] = z[80] + z[111];
z[87] = abb[30] * z[87];
z[42] = z[42] + (T(1) / T(2)) * z[78] + z[87];
z[42] = abb[6] * z[42];
z[15] = z[15] + -z[16] + z[42];
z[15] = abb[44] * z[15];
z[6] = z[6] + -z[103];
z[42] = -z[66] + z[106];
z[42] = abb[27] * z[42];
z[6] = (T(1) / T(2)) * z[6] + z[42] + z[112];
z[6] = abb[6] * z[6];
z[34] = (T(-7) / T(2)) * z[14] + z[34] + -z[67] + z[72];
z[42] = -z[2] * z[69];
z[34] = (T(1) / T(2)) * z[34] + z[42] + -z[76];
z[34] = abb[8] * z[34];
z[42] = abb[26] * (T(5) / T(2)) + -z[75];
z[42] = abb[26] * z[42];
z[42] = z[42] + z[51] + -z[77];
z[51] = z[42] * z[82];
z[66] = abb[6] + -abb[7];
z[58] = abb[8] + z[58];
z[67] = z[58] + z[66];
z[69] = (T(3) / T(2)) * z[67];
z[72] = abb[55] * z[69];
z[6] = z[6] + -z[16] + z[34] + z[51] + z[72] + -z[85];
z[6] = abb[45] * z[6];
z[16] = z[37] * z[117];
z[34] = z[71] + -z[118];
z[51] = abb[15] + z[119];
z[71] = abb[50] * z[51];
z[72] = abb[6] * z[19];
z[75] = abb[3] * z[70];
z[16] = z[16] + -z[34] + z[56] + -z[65] + -z[71] + z[72] + -z[75];
z[56] = abb[56] * (T(3) / T(2));
z[65] = -z[16] * z[56];
z[71] = abb[30] + 2 * z[105];
z[71] = abb[30] * z[71];
z[72] = abb[54] + abb[56];
z[39] = abb[29] + z[39];
z[39] = abb[29] * z[39];
z[75] = abb[31] + z[81];
z[76] = -abb[26] + 2 * z[75];
z[76] = abb[26] * z[76];
z[39] = z[14] + (T(1) / T(3)) * z[33] + z[39] + z[71] + -3 * z[72] + -z[76];
z[39] = abb[2] * z[39] * z[70];
z[64] = abb[7] * z[64];
z[71] = abb[27] + -2 * z[0];
z[71] = abb[27] * z[71];
z[52] = z[14] + -z[52] + z[71];
z[52] = abb[8] * z[52];
z[63] = -abb[33] + -z[63];
z[63] = abb[6] * z[63];
z[27] = -z[27] + -z[31] + z[52] + 2 * z[63] + z[64];
z[27] = z[27] * z[88];
z[31] = z[37] * z[58];
z[19] = z[19] * z[121];
z[52] = abb[0] * abb[41];
z[58] = -abb[15] + abb[18];
z[63] = abb[50] * z[58];
z[19] = -z[19] + -z[31] + z[34] + z[52] + z[63];
z[31] = abb[21] + abb[23];
z[11] = -z[11] * z[31];
z[11] = z[11] + z[19];
z[11] = abb[54] * z[11];
z[8] = z[8] + z[14];
z[14] = z[5] + -z[75];
z[14] = abb[26] * z[14];
z[34] = z[2] + z[105];
z[34] = abb[30] * z[34];
z[8] = 2 * z[8] + z[14] + z[34];
z[8] = z[8] * z[118];
z[14] = -z[83] + -z[119];
z[14] = abb[44] * z[14];
z[34] = -abb[18] * abb[41];
z[52] = abb[18] + -z[83];
z[52] = abb[48] * z[52];
z[63] = -abb[41] + abb[48];
z[63] = abb[16] * z[63];
z[14] = z[14] + z[34] + z[52] + z[63];
z[14] = z[14] * z[84];
z[34] = -z[42] * z[54];
z[42] = -abb[48] * z[104];
z[34] = z[34] + z[42];
z[34] = abb[0] * z[34];
z[22] = -z[22] + z[23];
z[22] = abb[16] * z[22];
z[23] = abb[18] * z[28];
z[42] = prod_pow(abb[30], 2);
z[26] = -z[26] + z[42];
z[26] = abb[15] * z[26];
z[22] = -z[22] + -z[23] + z[26];
z[22] = (T(1) / T(2)) * z[22];
z[23] = -abb[8] + 2 * abb[25];
z[26] = abb[36] * z[23];
z[42] = -abb[0] * z[84];
z[26] = -z[22] + z[26] + z[42];
z[42] = 3 * abb[50];
z[26] = z[26] * z[42];
z[52] = -abb[0] + -abb[3];
z[52] = z[52] * z[84];
z[23] = abb[14] * (T(-1) / T(4)) + z[23];
z[23] = abb[36] * z[23];
z[63] = abb[54] * z[58];
z[22] = -z[22] + z[23] + z[52] + (T(1) / T(2)) * z[63];
z[23] = z[33] * z[120];
z[33] = z[51] * z[56];
z[22] = 3 * z[22] + z[23] + z[33];
z[22] = abb[51] * z[22];
z[23] = z[45] + z[66];
z[23] = abb[47] * z[23];
z[33] = (T(1) / T(2)) * z[67];
z[33] = z[33] * z[37];
z[23] = z[23] + -z[33];
z[33] = abb[55] * z[23];
z[28] = z[28] * z[70];
z[37] = -abb[50] * z[53];
z[28] = z[28] + z[37];
z[37] = abb[14] * (T(3) / T(2));
z[28] = z[28] * z[37];
z[45] = -abb[27] + z[2];
z[45] = abb[30] * z[45];
z[52] = abb[27] * abb[29];
z[24] = -abb[34] + -z[24] + z[45] + z[52];
z[45] = abb[4] * (T(3) / T(2));
z[45] = z[45] * z[46];
z[24] = -z[24] * z[45];
z[3] = abb[57] + z[3] + z[4] + z[6] + z[8] + z[10] + (T(3) / T(2)) * z[11] + z[13] + z[14] + z[15] + z[18] + z[21] + z[22] + z[24] + z[25] + z[26] + z[27] + z[28] + -3 * z[33] + z[34] + z[39] + z[61] + z[65];
z[4] = abb[9] * z[9];
z[6] = z[4] + -z[32];
z[8] = z[40] * z[43];
z[10] = -z[0] * z[47];
z[1] = abb[30] + z[1];
z[11] = z[1] * z[20];
z[13] = abb[30] + z[74];
z[14] = abb[26] + -z[13];
z[14] = z[14] * z[44];
z[15] = -z[9] * z[48];
z[17] = -abb[30] + z[17];
z[18] = -abb[29] + z[17];
z[18] = z[18] * z[82];
z[17] = abb[27] + -z[17];
z[17] = abb[1] * z[17];
z[10] = -z[6] + z[8] + z[10] + z[11] + z[14] + z[15] + z[17] + z[18];
z[10] = z[10] * z[29];
z[11] = z[13] + z[116];
z[11] = abb[8] * z[11];
z[13] = z[1] + z[109];
z[13] = abb[6] * z[13];
z[14] = abb[29] + -abb[31];
z[15] = abb[26] + abb[30];
z[17] = z[14] + z[15];
z[18] = abb[0] * z[17];
z[20] = abb[30] + z[59];
z[21] = abb[1] * z[20];
z[22] = abb[12] * z[40];
z[6] = -3 * z[6] + -z[11] + z[13] + -z[18] + 2 * z[21] + z[22];
z[11] = -abb[43] * z[6];
z[13] = -z[0] + z[2] + -z[79] + -z[109];
z[13] = abb[44] * z[13];
z[18] = abb[30] * (T(3) / T(2));
z[22] = -abb[27] + z[9];
z[24] = -z[18] + -z[22] + z[79];
z[24] = abb[48] * z[24];
z[25] = -abb[29] + 5 * abb[30] + -z[102];
z[25] = z[25] * z[54];
z[26] = -abb[43] * z[20];
z[27] = -z[29] * z[38];
z[28] = abb[30] + z[68];
z[28] = abb[45] * z[28];
z[13] = z[13] + z[24] + z[25] + z[26] + z[27] + z[28];
z[13] = abb[3] * z[13];
z[20] = -abb[3] * z[20];
z[6] = -z[6] + z[20];
z[6] = abb[42] * z[6];
z[8] = z[8] + z[21];
z[20] = z[9] * z[95];
z[21] = z[0] * z[89];
z[20] = z[20] + -z[21];
z[5] = z[5] + z[18] + -z[86];
z[5] = abb[8] * z[5];
z[18] = z[2] + z[110];
z[18] = abb[6] * z[18];
z[5] = 3 * z[4] + z[5] + -z[8] + -z[18] + -z[20];
z[5] = abb[48] * z[5];
z[8] = z[8] + -z[20] + 3 * z[32];
z[20] = -z[2] + 2 * z[7] + -z[35] + -z[55];
z[20] = abb[8] * z[20];
z[21] = -z[2] + -z[80];
z[21] = abb[6] * z[21];
z[20] = -z[8] + z[20] + z[21];
z[20] = abb[44] * z[20];
z[2] = z[2] + -z[35] + z[41];
z[2] = abb[8] * z[2];
z[21] = -5 * abb[26] + abb[30] + z[14];
z[24] = z[21] * z[82];
z[2] = z[2] + -z[8] + -z[18] + z[24];
z[2] = abb[45] * z[2];
z[7] = -z[7] + z[15];
z[8] = abb[23] * z[7];
z[1] = z[1] * z[60];
z[18] = abb[30] + z[22];
z[18] = abb[21] * z[18];
z[22] = -abb[29] + abb[30];
z[24] = abb[19] * z[22];
z[1] = z[1] + -z[8] + z[18] + z[24];
z[1] = z[1] * z[12];
z[8] = abb[1] * z[57];
z[0] = abb[7] * z[0];
z[18] = -abb[8] * z[50];
z[0] = z[0] + z[8] + z[18] + z[32];
z[0] = z[0] * z[88];
z[18] = -z[21] * z[54];
z[17] = abb[48] * z[17];
z[17] = z[17] + z[18];
z[17] = abb[0] * z[17];
z[18] = z[22] + -z[102];
z[21] = abb[16] * (T(1) / T(2));
z[18] = z[18] * z[21];
z[21] = abb[18] * z[7];
z[22] = -abb[26] + abb[30];
z[22] = abb[15] * z[22];
z[18] = z[18] + (T(1) / T(2)) * z[21] + z[22];
z[21] = z[18] * z[42];
z[7] = z[7] * z[37] * z[70];
z[14] = 2 * z[14] + -z[15];
z[14] = z[14] * z[118];
z[22] = -abb[27] + abb[30];
z[22] = z[22] * z[45];
z[0] = z[0] + z[1] + z[2] + z[5] + 2 * z[6] + z[7] + z[10] + z[11] + z[13] + z[14] + z[17] + z[20] + z[21] + z[22];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[39] * z[16];
z[2] = abb[37] * z[19];
z[1] = -z[1] + z[2];
z[2] = -z[29] * z[49];
z[5] = abb[45] * z[69];
z[2] = z[2] + z[5] + -3 * z[23];
z[2] = abb[38] * z[2];
z[5] = abb[5] * z[9];
z[6] = -abb[0] * z[36];
z[4] = -z[4] + z[5] + z[6] + z[8];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = abb[38] * z[73];
z[6] = -abb[37] * z[121];
z[4] = z[4] + z[5] + z[6];
z[4] = z[4] * z[30];
z[5] = m1_set::bc<T>[0] * z[18];
z[6] = abb[37] * z[58];
z[7] = abb[39] * z[51];
z[6] = z[6] + z[7];
z[5] = z[5] + (T(1) / T(2)) * z[6];
z[5] = abb[51] * z[5];
z[6] = abb[38] * z[62];
z[7] = abb[37] * z[31];
z[6] = z[6] + -z[7];
z[6] = z[6] * z[12];
z[7] = -z[15] + z[75];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = -abb[37] + -abb[39];
z[7] = 2 * z[7] + 3 * z[8];
z[7] = abb[2] * z[7] * z[70];
z[0] = abb[40] + z[0] + (T(3) / T(2)) * z[1] + z[2] + z[4] + 3 * z[5] + z[6] + z[7];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_78_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-12.647469062186067486841910742974404868797376292554273300051175834"),stof<T>("-32.658337759093888618082200129294428401278554834609311333167076193")}, std::complex<T>{stof<T>("-10.293053808775832717721009899336879750883574606940403753481863196"),stof<T>("27.731619358422268971912884190114824923332565285318170185721753227")}, std::complex<T>{stof<T>("-5.146526904387916358860504949668439875441787303470201876740931598"),stof<T>("13.865809679211134485956442095057412461666282642659085092860876613")}, std::complex<T>{stof<T>("2.5950042358985557438643716361737929768100917418995840993366333551"),stof<T>("11.9707155639956360629457743922804315937884199868170465049396740598")}, std::complex<T>{stof<T>("22.27159359148321089997389334597558122403465204867827240790978262"),stof<T>("14.927533426237727127980115252271100110822023273063294500677031204")}, std::complex<T>{stof<T>("27.020867969250850125997294606705381003227719642862572249811799095"),stof<T>("19.452185123951643192620569008360557671995532244014171037750863862")}, std::complex<T>{stof<T>("-15.639725824704900363543386068086837941856187743739759434226724311"),stof<T>("-26.238591946164474130431078670427989972227183207816396208172040982")}, std::complex<T>{stof<T>("-7.7415311402864721027248765858422328522518790453697859760775649528"),stof<T>("1.8950941152154984230106677027769808678778626558420385879212025536")}, std::complex<T>{stof<T>("-1.4853408623903941775700023282203034067366770014683110969421267106"),stof<T>("-2.5547511592843874835054786769005226002611227079059833853658668355")}, std::complex<T>{stof<T>("-4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("-16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("-4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("-16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_78_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_78_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(-3) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[3] + -v[5])) / tend;


		return (abb[44] + abb[46] + -abb[48]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -prod_pow(abb[29], 2);
z[1] = prod_pow(abb[27], 2);
z[0] = z[0] + z[1];
z[1] = -abb[44] + -abb[46] + abb[48];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_78_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_78_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("13.4617138864491369806609356118891406511044252740502085718060189675"),stof<T>("-4.3112481116084540729719503268057823180106757399237961327991304953")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W16(k,dl), dlog_W21(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[40] = SpDLog_f_4_78_W_16_Im(t, path, abb);
abb[57] = SpDLog_f_4_78_W_16_Re(t, path, abb);

                    
            return f_4_78_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_78_DLogXconstant_part(base_point<T>, kend);
	value += f_4_78_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_78_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_78_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_78_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_78_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_78_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_78_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
