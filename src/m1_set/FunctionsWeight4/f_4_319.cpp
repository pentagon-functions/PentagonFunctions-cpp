/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_319.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_319_abbreviated (const std::array<T,64>& abb) {
T z[130];
z[0] = 2 * abb[54];
z[1] = 2 * abb[48];
z[2] = z[0] + z[1];
z[3] = 2 * abb[45];
z[4] = abb[50] + z[3];
z[5] = abb[46] * (T(1) / T(2));
z[6] = abb[55] * (T(1) / T(2));
z[7] = -z[2] + z[4] + -z[5] + -z[6];
z[8] = abb[47] * (T(2) / T(3));
z[7] = -abb[51] + (T(1) / T(3)) * z[7] + -z[8];
z[7] = abb[7] * z[7];
z[9] = abb[58] + abb[59] + abb[60];
z[10] = abb[27] * z[9];
z[11] = abb[27] * abb[57];
z[9] = abb[57] + z[9];
z[12] = abb[26] * z[9];
z[13] = z[10] + z[11] + z[12];
z[14] = abb[47] + -abb[48];
z[15] = -abb[45] + abb[50];
z[16] = z[14] + z[15];
z[17] = abb[15] * z[16];
z[18] = abb[22] * z[9];
z[17] = z[17] + z[18];
z[18] = abb[24] * z[9];
z[19] = z[17] + z[18];
z[20] = z[13] + z[19];
z[21] = 10 * abb[47];
z[22] = 2 * abb[50];
z[23] = abb[46] + abb[49];
z[24] = 4 * abb[45] + -10 * abb[48] + 7 * abb[53] + -z[21] + z[22] + -z[23];
z[24] = abb[8] * z[24];
z[25] = -abb[49] + z[15];
z[26] = -abb[46] + abb[55];
z[25] = z[2] + 2 * z[25] + -z[26];
z[27] = abb[51] + -z[8] + (T(1) / T(3)) * z[25];
z[27] = abb[17] * z[27];
z[28] = abb[49] * (T(1) / T(2));
z[4] = z[4] + -z[28];
z[29] = abb[44] + abb[55];
z[4] = abb[48] * (T(-2) / T(3)) + abb[46] * (T(-1) / T(4)) + abb[54] * (T(3) / T(4)) + (T(1) / T(3)) * z[4] + -z[8] + (T(-1) / T(6)) * z[29];
z[4] = abb[4] * z[4];
z[8] = abb[8] * abb[52];
z[29] = -abb[50] + abb[53];
z[30] = abb[1] * z[29];
z[31] = z[8] + -z[30];
z[32] = abb[23] + abb[25];
z[32] = z[9] * z[32];
z[33] = abb[18] + abb[19];
z[34] = abb[56] * z[33];
z[35] = abb[0] * abb[51];
z[36] = z[34] + (T(13) / T(3)) * z[35];
z[37] = 7 * abb[50];
z[38] = -4 * abb[46] + 7 * abb[47] + -abb[49] + abb[55] + -z[0] + -z[37];
z[38] = -abb[44] + (T(1) / T(3)) * z[38];
z[38] = abb[14] * z[38];
z[39] = -abb[0] + abb[7];
z[40] = abb[44] * z[39];
z[41] = abb[48] + -abb[50];
z[42] = abb[13] * z[41];
z[37] = abb[49] * (T(13) / T(2)) + z[37];
z[37] = abb[55] + abb[54] * (T(-65) / T(12)) + abb[51] * (T(-19) / T(6)) + abb[52] * (T(-7) / T(3)) + abb[46] * (T(9) / T(4)) + abb[44] * (T(20) / T(3)) + (T(1) / T(3)) * z[37];
z[37] = abb[2] * z[37];
z[4] = z[4] + z[7] + (T(2) / T(3)) * z[20] + (T(1) / T(3)) * z[24] + z[27] + (T(7) / T(3)) * z[31] + (T(4) / T(3)) * z[32] + (T(1) / T(2)) * z[36] + z[37] + 2 * z[38] + (T(13) / T(6)) * z[40] + (T(14) / T(3)) * z[42];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[7] = -abb[49] + abb[54];
z[20] = z[7] + -z[14];
z[24] = abb[51] * (T(3) / T(2));
z[27] = z[15] + z[24];
z[31] = z[5] + -z[6];
z[36] = z[20] + z[27] + z[31];
z[37] = abb[17] * z[36];
z[13] = z[13] + z[37];
z[37] = z[13] + z[19];
z[38] = z[15] + z[23];
z[43] = abb[47] + abb[48];
z[44] = -abb[53] + z[43];
z[45] = -z[38] + z[44];
z[46] = abb[8] * z[45];
z[47] = z[32] + z[46];
z[48] = 3 * abb[52];
z[49] = 3 * abb[53];
z[50] = z[48] + z[49];
z[51] = 3 * abb[45] + abb[50] + -z[23] + z[43] + -z[50];
z[51] = abb[3] * z[51];
z[52] = -z[6] + -z[7] + -z[15];
z[52] = abb[7] * z[52];
z[53] = abb[51] * (T(1) / T(2));
z[31] = abb[44] + z[31] + z[53];
z[54] = -abb[45] + abb[53];
z[55] = z[31] + z[54];
z[55] = abb[10] * z[55];
z[56] = -abb[45] + abb[52];
z[57] = abb[9] * z[56];
z[58] = z[55] + z[57];
z[59] = -abb[50] + abb[52];
z[60] = -abb[51] + -z[7] + z[59];
z[61] = abb[44] + z[60];
z[62] = abb[2] * z[61];
z[62] = -z[8] + z[62];
z[63] = -abb[44] + abb[55] + -z[5] + -z[27];
z[63] = abb[4] * z[63];
z[64] = 2 * abb[0] + abb[7];
z[64] = abb[44] * z[64];
z[52] = z[30] + (T(-1) / T(2)) * z[34] + -2 * z[35] + z[37] + z[47] + -z[51] + z[52] + -3 * z[58] + z[62] + z[63] + z[64];
z[52] = abb[30] * z[52];
z[58] = z[32] + z[51];
z[63] = 2 * abb[53];
z[64] = -z[1] + z[38] + z[63];
z[65] = abb[12] * z[64];
z[66] = 2 * abb[47];
z[67] = 2 * abb[52];
z[68] = z[66] + -z[67];
z[69] = -z[38] + z[68];
z[70] = abb[11] * z[69];
z[71] = abb[4] + -abb[7];
z[72] = abb[53] + z[14];
z[73] = -abb[52] + z[72];
z[74] = z[71] * z[73];
z[75] = z[29] + z[56];
z[76] = 2 * abb[16];
z[77] = z[75] * z[76];
z[74] = -z[8] + z[46] + z[58] + z[65] + -z[70] + z[74] + z[77];
z[78] = abb[32] * z[74];
z[73] = abb[16] * z[73];
z[79] = -z[65] + z[73];
z[80] = z[8] + z[57];
z[81] = z[79] + z[80];
z[82] = 3 * z[30];
z[83] = z[19] + z[82];
z[84] = abb[4] * z[29];
z[72] = -abb[45] + z[72];
z[85] = abb[7] * z[72];
z[47] = -z[47] + z[81] + -z[83] + -z[84] + z[85];
z[47] = abb[31] * z[47];
z[78] = z[47] + z[78];
z[52] = z[52] + 2 * z[78];
z[52] = abb[30] * z[52];
z[78] = abb[18] * (T(1) / T(2));
z[84] = abb[19] + z[78];
z[84] = abb[56] * z[84];
z[85] = abb[0] * z[6];
z[84] = z[35] + z[84] + -z[85];
z[84] = (T(1) / T(2)) * z[84];
z[85] = 2 * z[30];
z[86] = z[84] + z[85];
z[87] = abb[45] + abb[50];
z[88] = z[43] + -z[87];
z[89] = -abb[54] + z[88];
z[90] = abb[55] * (T(1) / T(4));
z[91] = -z[53] + z[89] + -z[90];
z[91] = abb[7] * z[91];
z[92] = 2 * abb[44];
z[93] = abb[51] + z[92];
z[89] = -abb[55] + z[89] + z[93];
z[89] = abb[4] * z[89];
z[94] = 2 * abb[2];
z[61] = z[61] * z[94];
z[93] = -z[26] + z[93];
z[54] = 2 * z[54] + z[93];
z[95] = abb[10] * z[54];
z[96] = abb[7] * z[92];
z[96] = -z[95] + z[96];
z[73] = 2 * z[73] + z[96];
z[97] = 2 * z[57];
z[13] = z[13] + -z[19] + z[61] + z[73] + -z[86] + -z[89] + z[91] + z[97];
z[13] = abb[30] * z[13];
z[19] = 2 * z[32];
z[37] = z[19] + z[37];
z[89] = abb[8] * z[67];
z[91] = z[37] + -z[89];
z[98] = 2 * abb[49];
z[27] = abb[54] + z[27] + z[43] + -z[90] + -z[98];
z[27] = abb[7] * z[27];
z[43] = -abb[54] + z[43];
z[99] = z[15] + -z[26] + z[43] + z[92];
z[99] = abb[4] * z[99];
z[100] = abb[0] * abb[55];
z[78] = -abb[19] + z[78];
z[101] = -abb[56] * z[78];
z[101] = -z[35] + (T(-3) / T(2)) * z[100] + z[101];
z[39] = -z[39] * z[92];
z[27] = z[27] + z[39] + -2 * z[46] + -z[61] + -z[85] + -z[91] + z[99] + (T(1) / T(2)) * z[101];
z[27] = abb[34] * z[27];
z[39] = z[22] + -z[67];
z[61] = abb[46] * (T(3) / T(2));
z[92] = 4 * abb[44];
z[99] = abb[55] * (T(-3) / T(2)) + -z[7] + z[39] + z[53] + z[61] + z[92];
z[99] = abb[2] * z[99];
z[101] = abb[44] * (T(3) / T(2)) + -z[6];
z[102] = abb[50] + abb[46] * (T(3) / T(4)) + z[28];
z[103] = -abb[47] + z[102];
z[104] = abb[54] * (T(1) / T(4));
z[105] = -z[101] + -z[103] + z[104];
z[105] = abb[14] * z[105];
z[106] = z[22] + z[23];
z[107] = -abb[53] + z[106];
z[108] = 3 * abb[47];
z[109] = z[107] + -z[108];
z[110] = abb[8] * z[109];
z[110] = z[89] + z[110];
z[111] = abb[18] * abb[56];
z[100] = z[100] + z[111];
z[100] = -z[35] + (T(3) / T(2)) * z[100];
z[111] = abb[55] * (T(3) / T(4));
z[112] = abb[50] + z[5] + -z[111];
z[112] = abb[7] * z[112];
z[102] = abb[54] * (T(-17) / T(4)) + abb[44] * (T(7) / T(2)) + z[102];
z[102] = abb[4] * z[102];
z[99] = z[30] + z[40] + z[99] + (T(1) / T(2)) * z[100] + z[102] + 3 * z[105] + z[110] + z[112];
z[99] = abb[33] * z[99];
z[79] = -z[70] + z[79] + z[89];
z[100] = 3 * abb[50];
z[102] = z[98] + z[100];
z[105] = 2 * abb[46];
z[112] = z[102] + z[105];
z[113] = abb[45] + abb[48];
z[114] = 4 * abb[47] + -z[112] + z[113];
z[115] = -z[48] + z[114];
z[115] = abb[5] * z[115];
z[116] = z[12] + z[115];
z[117] = -z[66] + z[107];
z[118] = 2 * abb[8];
z[117] = z[117] * z[118];
z[45] = -abb[52] + z[45];
z[119] = -abb[7] * z[45];
z[109] = z[67] + z[109];
z[109] = abb[4] * z[109];
z[109] = z[79] + z[109] + z[116] + z[117] + z[119];
z[109] = abb[32] * z[109];
z[47] = -z[47] + z[109];
z[27] = z[13] + z[27] + 2 * z[47] + z[99];
z[27] = abb[33] * z[27];
z[47] = 4 * abb[48] + -z[49];
z[99] = abb[45] + abb[47];
z[109] = z[47] + z[99] + -z[112];
z[112] = abb[6] * z[109];
z[18] = z[18] + z[112];
z[117] = z[18] + z[58];
z[23] = z[23] + z[100];
z[119] = 3 * abb[48];
z[120] = -abb[53] + -z[23] + z[48] + -z[99] + z[119];
z[120] = abb[8] * z[120];
z[121] = 2 * z[65];
z[64] = -abb[4] * z[64];
z[122] = -z[14] + z[48];
z[123] = 2 * z[87];
z[124] = abb[53] + z[122] + -z[123];
z[125] = abb[16] * z[124];
z[126] = abb[53] + -z[1] + z[106];
z[127] = abb[47] + -z[126];
z[127] = abb[7] * z[127];
z[64] = z[64] + -z[117] + z[120] + -z[121] + -z[125] + z[127];
z[64] = abb[38] * z[64];
z[74] = abb[40] * z[74];
z[120] = z[14] + z[49];
z[123] = abb[52] + z[120] + -z[123];
z[123] = abb[16] * z[123];
z[127] = -z[14] + z[59];
z[127] = abb[4] * z[127];
z[56] = -abb[7] * z[56];
z[56] = -z[51] + z[56] + -z[57] + z[70] + z[83] + -z[123] + z[127];
z[56] = abb[37] * z[56];
z[56] = z[56] + z[64] + z[74];
z[64] = 3 * z[65];
z[74] = -abb[48] + z[106];
z[83] = -abb[8] * z[74];
z[127] = abb[45] + z[22];
z[120] = z[120] + -z[127];
z[120] = abb[4] * z[120];
z[128] = 5 * z[42];
z[41] = abb[7] * z[41];
z[41] = z[18] + 6 * z[30] + z[41] + z[64] + z[80] + z[83] + z[120] + -z[123] + -z[128];
z[41] = prod_pow(abb[31], 2) * z[41];
z[80] = -z[63] + z[119];
z[83] = z[80] + -z[106];
z[106] = abb[8] * z[83];
z[120] = -abb[46] + -3 * abb[49] + abb[50];
z[120] = abb[7] * z[120];
z[129] = -abb[44] + abb[50] + z[7];
z[129] = abb[4] * z[129];
z[40] = z[35] + z[40] + 3 * z[42] + z[62] + -z[85] + -z[106] + z[120] + z[129];
z[40] = abb[34] * z[40];
z[62] = -abb[50] + z[80];
z[62] = abb[7] * z[62];
z[72] = -abb[4] * z[72];
z[62] = -z[18] + z[42] + z[62] + z[72] + z[81] + z[106];
z[62] = abb[31] * z[62];
z[72] = -abb[52] + -z[83];
z[72] = abb[7] * z[72];
z[80] = z[118] * z[126];
z[45] = -abb[4] * z[45];
z[45] = z[18] + z[45] + z[72] + -z[79] + z[80];
z[45] = abb[32] * z[45];
z[45] = z[45] + z[62];
z[13] = -z[13] + z[40] + 2 * z[45];
z[13] = abb[34] * z[13];
z[40] = 3 * z[70] + -z[116];
z[31] = -z[31] + z[122] + -z[127];
z[31] = abb[7] * z[31];
z[45] = abb[44] * (T(-1) / T(2)) + z[53] + -z[103] + -z[104];
z[45] = abb[4] * z[45];
z[62] = 6 * abb[52];
z[53] = -8 * abb[44] + -abb[49] + -6 * abb[50] + 4 * abb[54] + abb[46] * (T(-7) / T(2)) + z[6] + z[53] + z[62];
z[53] = abb[2] * z[53];
z[72] = abb[47] + -abb[50];
z[28] = abb[54] * (T(7) / T(4)) + abb[46] * (T(11) / T(4)) + z[28] + -5 * z[72] + z[101];
z[28] = abb[14] * z[28];
z[72] = abb[47] + -z[107];
z[72] = abb[8] * z[72];
z[28] = z[28] + z[31] + -z[40] + z[45] + z[53] + z[55] + z[72] + -z[125];
z[28] = abb[35] * z[28];
z[31] = 2 * abb[51];
z[45] = abb[44] + z[31];
z[7] = z[7] + -z[26] + z[45] + -z[48] + z[100];
z[7] = z[7] * z[94];
z[3] = -z[1] + z[3];
z[53] = z[3] + z[68];
z[55] = z[53] + -z[93];
z[55] = abb[4] * z[55];
z[25] = 3 * abb[51] + z[25] + -z[66];
z[68] = abb[17] * z[25];
z[11] = 2 * z[11] + z[68];
z[19] = z[11] + z[19];
z[68] = z[19] + -z[89];
z[72] = 2 * z[70];
z[79] = 2 * z[12];
z[73] = -z[72] + z[73] + z[79];
z[46] = z[10] + z[46];
z[80] = 2 * abb[7];
z[60] = z[60] * z[80];
z[46] = -z[7] + 2 * z[46] + z[55] + z[60] + z[68] + z[73];
z[55] = -abb[30] + abb[34];
z[46] = z[46] * z[55];
z[55] = 3 * abb[44] + -abb[55];
z[60] = abb[49] + z[55];
z[81] = abb[54] * (T(5) / T(2)) + z[5] + z[22] + -z[60] + -z[66];
z[81] = abb[14] * z[81];
z[83] = 2 * z[115];
z[85] = z[81] + z[83];
z[53] = -abb[51] + z[26] + z[53];
z[53] = abb[7] * z[53];
z[100] = abb[54] * (T(1) / T(2));
z[22] = z[22] + z[100];
z[101] = 4 * abb[52];
z[61] = -abb[44] + 6 * abb[47] + -abb[49] + abb[51] + -z[22] + -z[61] + -z[101];
z[61] = abb[4] * z[61];
z[26] = abb[51] + z[26] + -z[92];
z[0] = z[0] + z[26] + -z[98];
z[0] = abb[2] * z[0];
z[0] = z[0] + z[53] + z[61] + -z[73] + -z[85] + -2 * z[110];
z[0] = abb[33] * z[0];
z[0] = z[0] + z[28] + z[46];
z[0] = abb[35] * z[0];
z[28] = z[38] + -z[50] + z[108] + z[119];
z[28] = abb[8] * z[28];
z[38] = -abb[48] + -z[48] + z[66] + z[87];
z[38] = abb[7] * z[38];
z[1] = -abb[47] + z[1] + -z[49] + z[87];
z[1] = abb[4] * z[1];
z[1] = z[1] + z[28] + z[38] + z[40] + -z[64] + -z[117];
z[1] = prod_pow(abb[32], 2) * z[1];
z[28] = -z[10] + z[51] + z[65];
z[20] = -abb[51] + -z[20] + z[29];
z[20] = z[20] * z[80];
z[38] = -abb[4] * z[54];
z[40] = -z[76] * z[124];
z[7] = -z[7] + z[11] + z[20] + -2 * z[28] + z[38] + z[40] + z[79] + z[96];
z[7] = abb[36] * z[7];
z[11] = abb[21] * z[36];
z[20] = -z[24] + z[87];
z[20] = abb[18] * z[20];
z[28] = abb[20] * z[16];
z[33] = z[33] * z[43];
z[6] = z[6] * z[78];
z[36] = -abb[4] + abb[29] + abb[0] * (T(-3) / T(4)) + abb[7] * (T(-1) / T(4));
z[36] = abb[56] * z[36];
z[9] = abb[28] * z[9];
z[38] = abb[19] * z[87];
z[6] = z[6] + z[9] + z[11] + z[20] + z[28] + -z[33] + -z[36] + z[38];
z[9] = abb[63] * z[6];
z[11] = -z[23] + z[49] + z[108] + -z[113];
z[11] = abb[8] * z[11];
z[12] = -z[12] + z[72];
z[20] = abb[7] * z[69];
z[23] = -abb[52] + z[66] + -z[74];
z[23] = abb[4] * z[23];
z[11] = -z[8] + z[11] + z[12] + z[20] + z[23] + -z[58] + -z[123];
z[11] = 2 * z[11] + -z[83];
z[11] = abb[39] * z[11];
z[20] = -8 * abb[47] + abb[46] * (T(5) / T(2)) + -z[3] + z[22] + z[60] + z[62];
z[20] = abb[4] * z[20];
z[22] = abb[7] * z[25];
z[23] = z[39] + z[93];
z[25] = abb[2] * z[23];
z[28] = abb[8] * z[114];
z[28] = z[10] + z[28];
z[19] = 6 * z[8] + -z[19] + z[20] + z[22] + 3 * z[25] + -2 * z[28] + z[85];
z[20] = -abb[62] * z[19];
z[17] = -z[17] + -z[32] + z[112];
z[22] = abb[8] * z[109];
z[15] = -abb[47] + -z[15] + z[47];
z[15] = abb[7] * z[15];
z[16] = abb[4] * z[16];
z[15] = z[15] + -z[16] + -z[17] + z[22] + z[42] + z[82];
z[15] = 2 * z[15];
z[16] = abb[61] * z[15];
z[0] = z[0] + z[1] + z[4] + z[7] + z[9] + z[11] + z[13] + z[16] + z[20] + z[27] + z[41] + z[52] + 2 * z[56];
z[1] = -z[14] + -z[63] + z[87];
z[1] = abb[4] * z[1];
z[4] = -z[29] + z[99];
z[7] = abb[8] * z[4];
z[9] = abb[45] + -abb[47] + -z[29];
z[9] = abb[7] * z[9];
z[1] = z[1] + z[7] + z[9] + -z[17] + z[77] + -z[82] + -z[89] + -z[97] + -z[121] + z[128];
z[1] = abb[31] * z[1];
z[7] = abb[49] + z[87];
z[2] = -z[2] + 2 * z[7] + -z[26] + z[66] + -z[101];
z[2] = abb[7] * z[2];
z[5] = abb[49] + z[5] + z[100];
z[3] = abb[55] + z[3] + z[5] + z[39] + -z[45];
z[3] = abb[4] * z[3];
z[7] = abb[16] * z[75];
z[7] = -4 * z[7] + 2 * z[95];
z[9] = -abb[46] + abb[51];
z[9] = 14 * abb[44] + -10 * abb[54] + abb[55] + -5 * z[9] + -z[62] + 2 * z[102];
z[9] = abb[2] * z[9];
z[11] = z[63] + -z[113];
z[13] = -abb[50] + z[11];
z[14] = -abb[8] * z[13];
z[10] = z[10] + z[14];
z[14] = -10 * abb[50] + abb[46] * (T(-11) / T(2)) + abb[54] * (T(-7) / T(2)) + z[21] + -z[60];
z[14] = abb[14] * z[14];
z[2] = z[2] + z[3] + -z[7] + z[9] + 2 * z[10] + z[14] + z[68] + 4 * z[70] + -z[83];
z[2] = abb[35] * z[2];
z[3] = -z[44] * z[118];
z[3] = z[3] + z[65] + z[89];
z[9] = -z[4] + z[67];
z[9] = abb[7] * z[9];
z[10] = z[11] + z[59];
z[10] = abb[4] * z[10];
z[3] = 2 * z[3] + z[9] + z[10] + -z[12] + z[18] + -z[77] + z[115];
z[3] = abb[32] * z[3];
z[9] = z[23] * z[94];
z[10] = abb[54] + z[88];
z[11] = z[10] + z[24];
z[12] = -4 * abb[49] + z[11] + -z[90] + -z[105];
z[12] = abb[7] * z[12];
z[4] = -z[4] * z[118];
z[14] = abb[46] + z[98];
z[10] = z[10] + -z[14];
z[10] = abb[4] * z[10];
z[4] = z[4] + 4 * z[8] + z[9] + z[10] + z[12] + -z[37] + -2 * z[42] + -z[86];
z[4] = abb[34] * z[4];
z[8] = z[11] + -z[14] + -z[111];
z[8] = abb[7] * z[8];
z[5] = -z[5] + z[55] + z[88];
z[5] = abb[4] * z[5];
z[10] = z[13] * z[118];
z[11] = 4 * z[30];
z[5] = z[5] + z[8] + z[10] + -z[11] + z[25] + z[81] + z[84] + -z[91];
z[5] = abb[33] * z[5];
z[8] = z[35] + z[57];
z[10] = -abb[55] + z[31];
z[10] = z[10] * z[71];
z[12] = -abb[0] * z[92];
z[7] = z[7] + 4 * z[8] + -z[9] + z[10] + z[11] + z[12] + z[34];
z[7] = abb[30] * z[7];
z[1] = 2 * z[1] + z[2] + 2 * z[3] + z[4] + z[5] + z[7];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[43] * z[6];
z[3] = -abb[42] * z[19];
z[4] = abb[41] * z[15];
z[1] = z[1] + z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_319_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("44.120529970733413815580724688696692232506243835713588493808950806"),stof<T>("-41.6650865301287942482805691217113402577275975395557653209603274")}, std::complex<T>{stof<T>("6.1573873748872583519239783897300042197028820134579747615081595661"),stof<T>("2.4778205314052879790712501848253428701450504204173758651694920326")}, std::complex<T>{stof<T>("-10.946169949363962630849954545458779446119013857957243646800214215"),stof<T>("-11.136326983776302161489339624934026349272606336270581303461304892")}, std::complex<T>{stof<T>("-8.1751738551215361882714853331493183425783411006947839878649066351"),stof<T>("-6.682971224307476653127710937854515806139530013937284518894762629")}, std::complex<T>{stof<T>("-8.1751738551215361882714853331493183425783411006947839878649066351"),stof<T>("-6.682971224307476653127710937854515806139530013937284518894762629")}, std::complex<T>{stof<T>("-14.185039018188726431855800391446253717410347863249169132076395032"),stof<T>("-5.338816067085086689750388445157237459589453082396068928974236024")}, std::complex<T>{stof<T>("-16.803752245079753554180418634314690113975501015854993187761585427"),stof<T>("-31.389855269529747287385331424596287607766468759872811490727255486")}, std::complex<T>{stof<T>("-21.160959375218185046798604894489504992692371121408418086891751401"),stof<T>("-5.865673783083879973420461566111248426459407782879456858558975723")}, std::complex<T>{stof<T>("17.271625219662090912607795267992703281726162263884459014102458665"),stof<T>("21.138988593369706307284751557739988174950239183665002331673644356")}, std::complex<T>{stof<T>("17.271625219662090912607795267992703281726162263884459014102458665"),stof<T>("21.138988593369706307284751557739988174950239183665002331673644356")}, std::complex<T>{stof<T>("-41.671210471216439960646767544592887799558137536380299880627764827"),stof<T>("48.82031718907354949328563421406743929959674341837653891158122691")}, std::complex<T>{stof<T>("7.2019855428186803732271569856355454235062877516923616663825340205"),stof<T>("-3.5435338962760453716617773530108197525464456749079145532744963274")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("-2.524847202924887863973136789500371089537089249336092385131738684")}, std::complex<T>{stof<T>("2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("-2.524847202924887863973136789500371089537089249336092385131738684")}, std::complex<T>{stof<T>("2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("-2.524847202924887863973136789500371089537089249336092385131738684")}, std::complex<T>{stof<T>("2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("-2.524847202924887863973136789500371089537089249336092385131738684")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_319_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_319_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-20.697690430771752120085340668351448250369592861714514685254815506"),stof<T>("19.791166694351523537418024076372387402292279625730583692916575651")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,64> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_319_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_319_DLogXconstant_part(base_point<T>, kend);
	value += f_4_319_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_319_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_319_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_319_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_319_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_319_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_319_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
