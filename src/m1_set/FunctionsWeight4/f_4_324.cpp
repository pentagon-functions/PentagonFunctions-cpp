/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_324.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_324_abbreviated (const std::array<T,28>& abb) {
T z[38];
z[0] = abb[18] + abb[19];
z[1] = abb[20] + -2 * abb[25] + abb[21] * (T(1) / T(4));
z[2] = (T(3) / T(4)) * z[0] + z[1];
z[3] = abb[8] + -abb[9];
z[3] = z[2] * z[3];
z[4] = abb[18] * (T(3) / T(2)) + abb[19] * (T(3) / T(2));
z[5] = 2 * abb[20] + -4 * abb[25] + abb[21] * (T(1) / T(2));
z[6] = z[4] + z[5];
z[7] = abb[7] * z[6];
z[8] = abb[23] + abb[24];
z[9] = abb[1] * z[8];
z[10] = abb[22] + -abb[24];
z[10] = abb[4] * z[10];
z[11] = (T(3) / T(4)) * z[8];
z[12] = abb[6] * z[11];
z[13] = (T(1) / T(2)) * z[8];
z[14] = -abb[3] * z[13];
z[15] = 5 * abb[23] + abb[24];
z[15] = abb[22] + (T(1) / T(4)) * z[15];
z[15] = abb[0] * z[15];
z[16] = abb[2] * z[8];
z[3] = z[3] + z[7] + -z[9] + z[10] + z[12] + z[14] + z[15] + (T(-1) / T(4)) * z[16];
z[3] = abb[11] * z[3];
z[12] = abb[12] * (T(1) / T(2));
z[14] = -abb[14] + z[12];
z[15] = z[4] * z[14];
z[16] = abb[12] * z[1];
z[17] = abb[14] * z[5];
z[15] = z[15] + z[16] + -z[17];
z[18] = abb[13] * z[2];
z[19] = -z[15] + z[18];
z[20] = abb[8] + abb[9];
z[19] = z[19] * z[20];
z[21] = abb[2] * (T(1) / T(2));
z[22] = abb[6] * (T(3) / T(2));
z[23] = abb[0] * (T(-1) / T(2)) + -z[21] + -z[22];
z[14] = z[8] * z[14];
z[24] = abb[13] * z[8];
z[25] = (T(1) / T(2)) * z[24];
z[26] = -z[14] + z[25];
z[23] = z[23] * z[26];
z[26] = 2 * abb[14];
z[27] = -abb[12] + z[26];
z[27] = z[8] * z[27];
z[28] = abb[1] * z[27];
z[29] = abb[1] * z[24];
z[30] = z[28] + z[29];
z[31] = z[24] + z[27];
z[31] = abb[3] * z[31];
z[3] = z[3] + z[19] + z[23] + 2 * z[30] + z[31];
z[3] = abb[11] * z[3];
z[19] = abb[3] * abb[23];
z[23] = -25 * abb[23] + abb[24];
z[23] = -13 * abb[22] + (T(1) / T(2)) * z[23];
z[23] = abb[0] * z[23];
z[19] = z[19] + -z[23];
z[23] = abb[22] + z[8];
z[23] = abb[5] * z[23];
z[30] = (T(-7) / T(2)) * z[9] + -4 * z[23];
z[31] = 3 * z[2];
z[32] = -abb[8] * z[31];
z[33] = 4 * abb[20] + abb[21] + -8 * abb[25];
z[34] = -z[0] + (T(-1) / T(3)) * z[33];
z[34] = abb[7] * z[34];
z[35] = (T(-1) / T(2)) * z[0] + (T(-1) / T(3)) * z[5];
z[35] = abb[9] * z[35];
z[36] = (T(3) / T(2)) * z[8];
z[37] = abb[22] * (T(5) / T(3)) + z[36];
z[21] = z[21] * z[37];
z[37] = abb[6] * z[13];
z[19] = (T(-1) / T(6)) * z[19] + z[21] + (T(1) / T(3)) * z[30] + z[32] + 2 * z[34] + z[35] + z[37];
z[19] = prod_pow(m1_set::bc<T>[0], 2) * z[19];
z[21] = z[6] * z[20];
z[30] = abb[3] + -abb[6];
z[30] = z[30] * z[36];
z[13] = -abb[22] + z[13];
z[13] = abb[2] * z[13];
z[7] = z[7] + 3 * z[9] + -z[13] + z[21] + z[23] + z[30];
z[13] = -abb[26] * z[7];
z[11] = z[11] * z[20];
z[0] = 3 * z[0] + z[33];
z[21] = -abb[3] + 2 * abb[10];
z[21] = z[0] * z[21];
z[23] = abb[0] + abb[2];
z[23] = z[23] * z[31];
z[2] = abb[6] * z[2];
z[2] = -z[2] + z[11] + z[21] + -z[23];
z[11] = abb[27] * z[2];
z[21] = prod_pow(abb[14], 2);
z[23] = (T(1) / T(2)) * z[21];
z[30] = abb[15] + z[23];
z[31] = abb[14] + z[12];
z[31] = abb[12] * z[31];
z[32] = z[30] + -z[31];
z[33] = z[4] * z[32];
z[1] = z[1] * z[21];
z[16] = -z[16] + -z[17];
z[16] = abb[12] * z[16];
z[34] = abb[15] * z[5];
z[1] = z[1] + z[16] + z[33] + z[34];
z[1] = abb[7] * z[1];
z[16] = -abb[13] * z[6];
z[16] = z[15] + z[16];
z[16] = abb[8] * z[16];
z[16] = z[16] + -2 * z[28] + -z[29];
z[16] = abb[13] * z[16];
z[28] = 3 * abb[15] + (T(5) / T(2)) * z[21] + -z[31];
z[33] = -z[8] * z[28];
z[34] = abb[13] * abb[23];
z[27] = -z[27] + -2 * z[34];
z[27] = abb[13] * z[27];
z[27] = z[27] + (T(1) / T(2)) * z[33];
z[27] = abb[3] * z[27];
z[28] = abb[23] * z[28];
z[32] = abb[24] * z[32];
z[28] = z[28] + z[32];
z[32] = z[14] + -z[24];
z[33] = abb[13] * (T(1) / T(2));
z[32] = z[32] * z[33];
z[33] = abb[15] + z[21];
z[33] = abb[22] * z[33];
z[28] = (T(1) / T(2)) * z[28] + -z[32] + z[33];
z[28] = abb[0] * z[28];
z[33] = abb[13] * z[15];
z[6] = -abb[15] * z[6];
z[6] = z[6] + z[33];
z[6] = abb[9] * z[6];
z[23] = -z[23] + -z[31];
z[23] = abb[22] * z[23];
z[23] = z[23] + -z[32];
z[23] = abb[2] * z[23];
z[12] = abb[5] * z[12];
z[32] = abb[5] * abb[14];
z[12] = z[12] + z[32];
z[12] = abb[12] * z[12];
z[33] = abb[5] * z[21];
z[12] = z[12] + (T(-3) / T(2)) * z[33];
z[33] = -abb[23] * z[12];
z[21] = -abb[15] + z[21];
z[21] = abb[4] * z[21];
z[34] = -z[12] + z[21];
z[34] = abb[24] * z[34];
z[30] = 3 * z[30] + -z[31];
z[30] = -z[9] * z[30];
z[12] = -z[12] + -z[21];
z[12] = abb[22] * z[12];
z[21] = -abb[13] * z[14];
z[31] = abb[15] * z[8];
z[21] = z[21] + z[31];
z[21] = z[21] * z[22];
z[1] = z[1] + z[3] + z[6] + z[11] + z[12] + z[13] + z[16] + z[19] + z[21] + z[23] + z[27] + z[28] + z[30] + z[33] + z[34];
z[3] = abb[12] * (T(1) / T(4));
z[6] = z[3] + -z[26];
z[6] = abb[23] * z[6];
z[11] = (T(1) / T(4)) * z[24];
z[3] = abb[24] * z[3];
z[12] = -abb[22] * z[26];
z[3] = z[3] + z[6] + -z[11] + z[12];
z[3] = abb[0] * z[3];
z[6] = -abb[22] + -abb[23];
z[6] = abb[0] * z[6];
z[6] = z[6] + -z[9] + -z[10];
z[10] = abb[2] + -abb[3];
z[10] = z[8] * z[10];
z[12] = -abb[7] + -abb[8];
z[0] = z[0] * z[12];
z[0] = z[0] + 2 * z[6] + z[10];
z[0] = abb[11] * z[0];
z[6] = z[15] + z[18];
z[6] = z[6] * z[20];
z[10] = abb[12] + -abb[14];
z[4] = z[4] * z[10];
z[5] = abb[12] * z[5];
z[4] = z[4] + z[5] + -z[17];
z[4] = abb[7] * z[4];
z[5] = abb[12] + abb[14];
z[12] = abb[22] * z[5];
z[11] = -z[11] + z[12] + (T(-1) / T(2)) * z[14];
z[11] = abb[2] * z[11];
z[12] = abb[5] * abb[12];
z[12] = -z[12] + 3 * z[32];
z[13] = -abb[23] * z[12];
z[15] = abb[4] * z[26];
z[16] = -z[12] + -z[15];
z[16] = abb[24] * z[16];
z[9] = z[9] * z[10];
z[10] = -z[12] + z[15];
z[10] = abb[22] * z[10];
z[12] = -z[14] + -z[25];
z[12] = z[12] * z[22];
z[5] = z[5] * z[8];
z[5] = (T(1) / T(2)) * z[5] + z[24];
z[5] = abb[3] * z[5];
z[0] = z[0] + z[3] + z[4] + z[5] + z[6] + z[9] + z[10] + z[11] + z[12] + z[13] + z[16] + 2 * z[29];
z[0] = m1_set::bc<T>[0] * z[0];
z[3] = -abb[16] * z[7];
z[2] = abb[17] * z[2];
z[0] = z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_324_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("-16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("-22.016267870406207206036389460595750494483588005016862370172604249"),stof<T>("32.084191598263246525868100778790269250979847871215110171924729879")}, std::complex<T>{stof<T>("-5.5040669676015518015090973651489376236208970012542155925431510624"),stof<T>("8.0210478995658116314670251946975673127449619678037775429811824698")}, std::complex<T>{stof<T>("-13.093045971976897949696811342608841326472912807615378063040304344"),stof<T>("34.431332471128323727950187960133729684733292832949884130199757723")}, std::complex<T>{stof<T>("-11.052893233437306251840309980891036075031890184252142115348691291"),stof<T>("39.865039799353086553061944597723656909339116909493784364888416289")}, std::complex<T>{stof<T>("-8.3700495902655746395980028974981409844630604166790347796949506283"),stof<T>("4.9991322365560406883045951921522429717004219758465177448084778744")}, std::complex<T>{stof<T>("44.032535740812414412072778921191500988967176010033724740345208499"),stof<T>("-64.168383196526493051736201557580538501959695742430220343849459758")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real()), rlog(k.W[195].real()/kbase.W[195].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_324_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_324_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("30.641176348420316462503289968784729361491157412298973165804344496"),stof<T>("-2.176463920073504076255182631935741880284249783188972787563658822")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dlog_W3(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), rlog(kend.W[195].real()/k.W[195].real()), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_324_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_324_DLogXconstant_part(base_point<T>, kend);
	value += f_4_324_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_324_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_324_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_324_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_324_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_324_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_324_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
