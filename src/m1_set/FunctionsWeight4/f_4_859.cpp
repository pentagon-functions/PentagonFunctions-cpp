/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_859.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_859_abbreviated (const std::array<T,84>& abb) {
T z[132];
z[0] = abb[34] + -abb[37];
z[1] = -abb[40] + z[0];
z[2] = m1_set::bc<T>[0] * z[1];
z[3] = abb[38] * m1_set::bc<T>[0];
z[4] = -abb[52] + z[3];
z[2] = z[2] + z[4];
z[5] = -abb[51] + z[2];
z[6] = 4 * abb[9];
z[5] = z[5] * z[6];
z[6] = abb[39] * m1_set::bc<T>[0];
z[7] = 2 * abb[52];
z[8] = -z[6] + z[7];
z[9] = 2 * abb[40];
z[10] = -abb[34] + z[9];
z[11] = m1_set::bc<T>[0] * z[10];
z[12] = z[8] + z[11];
z[13] = abb[14] * z[12];
z[14] = abb[18] + abb[23];
z[15] = abb[53] * z[14];
z[16] = abb[54] + abb[55];
z[5] = -abb[56] + z[5] + z[13] + z[15] + z[16];
z[13] = abb[33] * m1_set::bc<T>[0];
z[15] = -z[6] + z[13];
z[17] = abb[35] * m1_set::bc<T>[0];
z[17] = abb[50] + z[17];
z[18] = abb[36] * m1_set::bc<T>[0];
z[18] = -abb[49] + -z[15] + -z[17] + z[18];
z[19] = 4 * abb[10];
z[18] = z[18] * z[19];
z[19] = 2 * abb[36];
z[20] = -abb[34] + z[19];
z[21] = m1_set::bc<T>[0] * z[20];
z[22] = 2 * abb[33];
z[23] = m1_set::bc<T>[0] * z[22];
z[21] = -z[6] + -z[21] + z[23];
z[24] = 2 * abb[49];
z[25] = z[21] + z[24];
z[26] = 2 * abb[0];
z[25] = z[25] * z[26];
z[27] = abb[19] * abb[53];
z[25] = z[25] + z[27];
z[28] = -z[6] + z[17];
z[29] = 2 * m1_set::bc<T>[0];
z[30] = abb[38] * z[29];
z[31] = 2 * abb[37];
z[32] = abb[40] + z[31];
z[33] = -abb[34] + z[32];
z[33] = m1_set::bc<T>[0] * z[33];
z[33] = abb[52] + -z[28] + -z[30] + z[33];
z[34] = 2 * abb[7];
z[33] = z[33] * z[34];
z[35] = 2 * abb[35];
z[36] = m1_set::bc<T>[0] * z[35];
z[36] = 2 * abb[50] + z[36];
z[37] = abb[34] * m1_set::bc<T>[0];
z[37] = z[6] + -z[36] + z[37];
z[37] = abb[12] * z[37];
z[38] = -z[12] + z[30];
z[36] = z[36] + -z[38];
z[39] = -abb[4] + abb[5];
z[39] = z[36] * z[39];
z[40] = -abb[40] * m1_set::bc<T>[0];
z[4] = z[4] + z[40];
z[40] = 2 * abb[3];
z[4] = z[4] * z[40];
z[41] = -z[3] + z[17];
z[41] = abb[6] * z[41];
z[42] = abb[22] * abb[53];
z[42] = -abb[57] + z[42];
z[43] = abb[7] * abb[51];
z[4] = z[4] + z[5] + -z[18] + -z[25] + z[33] + z[37] + z[39] + 2 * z[41] + -z[42] + 4 * z[43];
z[4] = abb[71] * z[4];
z[33] = abb[2] + -abb[15];
z[37] = abb[36] + -abb[37];
z[29] = -z[29] * z[33] * z[37];
z[39] = z[20] + -z[31];
z[41] = z[9] + z[39];
z[41] = m1_set::bc<T>[0] * z[41];
z[8] = z[8] + z[41];
z[8] = abb[5] * z[8];
z[21] = abb[12] * z[21];
z[43] = abb[4] + abb[7] + -abb[12] + -abb[15];
z[24] = z[24] * z[43];
z[43] = -abb[37] + abb[40];
z[43] = m1_set::bc<T>[0] * z[43];
z[43] = abb[52] + z[43];
z[15] = z[15] + z[43];
z[15] = z[15] * z[34];
z[30] = -z[23] + z[30];
z[34] = -abb[15] * z[30];
z[44] = -abb[5] + abb[7];
z[33] = abb[3] + -z[33] + z[44];
z[45] = 2 * abb[51];
z[46] = z[33] * z[45];
z[2] = z[2] * z[40];
z[2] = z[2] + -z[5] + -z[8] + z[15] + -z[21] + z[24] + z[29] + -z[34] + -z[42] + -z[46];
z[5] = -z[23] + z[38];
z[5] = abb[4] * z[5];
z[5] = -z[2] + z[5] + -z[25];
z[8] = abb[66] + abb[68];
z[15] = abb[67] + abb[70];
z[21] = -z[8] + -z[15];
z[5] = z[5] * z[21];
z[21] = abb[34] + z[9];
z[21] = m1_set::bc<T>[0] * z[21];
z[6] = z[6] + z[7] + -4 * z[17] + z[21] + -z[30];
z[6] = abb[4] * z[6];
z[2] = z[2] + z[6] + -z[18] + -z[27];
z[2] = abb[69] * z[2];
z[6] = -abb[49] + abb[51] + -z[13];
z[18] = m1_set::bc<T>[0] * z[0];
z[18] = -z[6] + -z[17] + z[18];
z[18] = abb[60] * z[18];
z[21] = -abb[34] + abb[40];
z[21] = m1_set::bc<T>[0] * z[21];
z[21] = abb[52] + z[21] + z[28];
z[23] = abb[59] + -abb[64];
z[21] = -z[21] * z[23];
z[6] = z[6] + -z[43];
z[24] = -abb[61] + abb[63];
z[6] = z[6] * z[24];
z[25] = abb[39] * z[24];
z[27] = -m1_set::bc<T>[0] * z[25];
z[6] = z[6] + z[18] + z[21] + -z[27];
z[6] = abb[27] * z[6];
z[18] = m1_set::bc<T>[0] * z[37];
z[3] = -abb[51] + z[3] + z[18];
z[13] = abb[49] + -z[3] + z[13];
z[21] = -abb[60] + z[24];
z[13] = -abb[30] * z[13] * z[21];
z[6] = z[6] + z[13];
z[13] = -z[7] + -z[41] + z[45];
z[13] = z[13] * z[24];
z[13] = z[13] + -z[27];
z[3] = z[3] + -z[17];
z[17] = 2 * abb[60];
z[3] = z[3] * z[17];
z[28] = z[23] * z[36];
z[3] = z[3] + z[13] + -z[28];
z[28] = abb[26] + abb[28];
z[3] = z[3] * z[28];
z[12] = z[12] * z[23];
z[18] = -abb[51] + z[18];
z[17] = z[17] * z[18];
z[13] = -z[12] + z[13] + z[17];
z[13] = abb[25] * z[13];
z[7] = z[7] + z[11];
z[7] = z[7] * z[24];
z[7] = z[7] + z[12] + z[27];
z[7] = abb[29] * z[7];
z[11] = -abb[64] + z[24];
z[12] = 2 * abb[62];
z[17] = z[11] + -z[12];
z[18] = abb[65] + z[17];
z[18] = abb[83] * z[18];
z[27] = z[23] + z[24];
z[27] = abb[32] * abb[53] * z[27];
z[16] = abb[56] + 3 * abb[57] + z[16];
z[16] = abb[72] * z[16];
z[2] = z[2] + z[3] + z[4] + z[5] + 2 * z[6] + z[7] + z[13] + z[16] + z[18] + z[27];
z[3] = 3 * abb[36];
z[4] = abb[38] + -abb[40];
z[5] = abb[34] + -z[3] + z[4] + z[22];
z[5] = abb[39] * z[5];
z[1] = abb[40] * z[1];
z[6] = 2 * abb[77];
z[7] = z[1] + z[6];
z[13] = abb[34] + abb[37];
z[16] = abb[34] * z[13];
z[18] = 2 * abb[46];
z[16] = z[16] + z[18];
z[3] = z[3] + -z[32];
z[3] = abb[38] * z[3];
z[27] = -abb[37] + abb[38];
z[28] = 2 * abb[34];
z[29] = -z[27] + -z[28];
z[29] = abb[33] * z[29];
z[30] = -abb[35] + z[28];
z[34] = abb[35] * z[30];
z[36] = prod_pow(m1_set::bc<T>[0], 2);
z[38] = (T(2) / T(3)) * z[36];
z[41] = abb[36] * z[0];
z[42] = prod_pow(abb[37], 2);
z[3] = z[3] + z[5] + -z[7] + -z[16] + z[29] + z[34] + z[38] + 3 * z[41] + 2 * z[42];
z[3] = abb[7] * z[3];
z[5] = 3 * abb[37];
z[29] = 3 * abb[38] + -z[5] + -z[9];
z[43] = 4 * abb[36] + -z[28] + z[29] + -z[35];
z[43] = abb[33] * z[43];
z[45] = z[19] + z[27];
z[46] = z[35] + z[45];
z[46] = abb[38] * z[46];
z[47] = abb[40] + z[28];
z[47] = abb[40] * z[47];
z[48] = 2 * abb[41];
z[46] = z[46] + -z[47] + -z[48];
z[47] = -abb[36] + z[4];
z[49] = abb[33] + z[47];
z[50] = 2 * abb[39];
z[51] = z[49] * z[50];
z[52] = -abb[34] + abb[36];
z[53] = -abb[37] + z[52];
z[54] = z[19] * z[53];
z[55] = (T(4) / T(3)) * z[36];
z[56] = abb[37] * z[28];
z[57] = abb[35] + z[31];
z[57] = abb[35] * z[57];
z[58] = 2 * abb[42];
z[43] = 4 * abb[74] + z[43] + -z[46] + z[51] + -z[54] + z[55] + -z[56] + z[57] + z[58];
z[43] = abb[0] * z[43];
z[51] = abb[33] + -abb[36];
z[51] = z[50] * z[51];
z[59] = prod_pow(abb[34], 2);
z[59] = z[18] + z[59];
z[60] = -abb[33] + 2 * z[52];
z[60] = abb[33] * z[60];
z[61] = -abb[36] + z[28];
z[62] = abb[36] * z[61];
z[63] = 2 * abb[74];
z[51] = z[34] + z[51] + z[55] + -z[59] + z[60] + z[62] + z[63];
z[60] = 2 * abb[10];
z[51] = z[51] * z[60];
z[64] = abb[19] * abb[78];
z[51] = z[51] + -z[64];
z[65] = 3 * abb[34];
z[66] = -abb[37] + z[19];
z[67] = z[65] + -z[66];
z[67] = abb[36] * z[67];
z[68] = 4 * abb[45];
z[16] = z[16] + -z[67] + z[68];
z[67] = z[4] + z[37];
z[69] = -z[35] + z[67];
z[69] = abb[38] * z[69];
z[69] = z[16] + z[69];
z[70] = z[22] + -z[35];
z[71] = -abb[34] + z[31];
z[72] = -z[70] + z[71];
z[72] = abb[33] * z[72];
z[73] = -abb[35] + z[0];
z[74] = z[35] * z[73];
z[75] = 2 * abb[43];
z[76] = z[74] + -z[75];
z[77] = z[28] + z[47];
z[78] = -abb[33] + z[77];
z[79] = abb[39] * z[78];
z[80] = -z[38] + z[79];
z[72] = z[7] + -z[48] + -z[69] + z[72] + z[76] + -z[80];
z[81] = -abb[5] * z[72];
z[82] = z[50] * z[52];
z[83] = abb[34] * abb[37];
z[84] = abb[77] + -z[83];
z[85] = -abb[40] + 2 * z[0];
z[85] = abb[40] * z[85];
z[86] = abb[37] + abb[40];
z[87] = -abb[36] + z[86];
z[88] = -abb[38] + 2 * z[87];
z[88] = abb[38] * z[88];
z[68] = -z[54] + -z[68] + z[82] + 2 * z[84] + z[85] + z[88];
z[68] = abb[3] * z[68];
z[77] = -abb[37] + z[77];
z[77] = abb[38] * z[77];
z[82] = abb[34] + z[47];
z[82] = abb[39] * z[82];
z[84] = abb[40] * z[0];
z[85] = z[82] + z[84];
z[77] = -z[16] + z[77] + -z[85];
z[88] = 2 * abb[1];
z[89] = -abb[2] + z[88];
z[89] = z[77] * z[89];
z[90] = (T(1) / T(3)) * z[36];
z[91] = z[42] + -z[90];
z[92] = z[41] + -z[83];
z[93] = z[91] + z[92];
z[94] = abb[38] * z[87];
z[7] = z[7] + -z[82] + -z[93] + z[94];
z[82] = 2 * abb[9];
z[7] = z[7] * z[82];
z[94] = abb[33] + -abb[34];
z[95] = -z[9] + z[94];
z[95] = abb[33] * z[95];
z[96] = abb[34] * abb[40];
z[96] = abb[77] + z[96];
z[97] = z[90] + z[96];
z[95] = z[48] + z[95] + 2 * z[97];
z[98] = abb[39] * z[94];
z[99] = z[95] + z[98];
z[100] = abb[14] * z[99];
z[7] = -abb[80] + abb[81] + z[7] + -z[100];
z[100] = abb[40] * z[13];
z[101] = z[6] + z[100];
z[16] = -z[16] + z[58] + z[101];
z[102] = 2 * abb[38];
z[103] = -z[9] + z[102];
z[104] = -abb[34] + z[103];
z[104] = abb[33] * z[104];
z[105] = z[34] + z[104];
z[47] = -abb[37] + z[47];
z[47] = abb[38] * z[47];
z[106] = z[47] + -z[80];
z[107] = z[16] + z[105] + z[106];
z[108] = abb[4] * z[107];
z[109] = 2 * abb[75];
z[110] = -abb[38] + z[28];
z[110] = abb[38] * z[110];
z[110] = -z[34] + -z[75] + -z[109] + z[110];
z[110] = abb[6] * z[110];
z[53] = abb[36] * z[53];
z[53] = z[53] + z[83];
z[83] = 2 * abb[45];
z[111] = z[53] + z[83];
z[112] = abb[35] + z[52];
z[113] = abb[38] * z[112];
z[113] = z[111] + z[113];
z[114] = abb[35] * z[73];
z[115] = -abb[42] + z[90] + z[113] + z[114];
z[116] = -abb[33] + z[35];
z[117] = -z[102] + z[116];
z[118] = z[71] + z[117];
z[118] = abb[33] * z[118];
z[119] = -abb[33] + z[20];
z[120] = abb[39] * z[119];
z[115] = 2 * z[115] + z[118] + -z[120];
z[115] = abb[12] * z[115];
z[118] = abb[3] + -abb[4];
z[121] = 2 * abb[44];
z[122] = -z[118] * z[121];
z[44] = abb[4] + abb[12] + z[44] + -z[60];
z[44] = z[44] * z[109];
z[60] = abb[22] + -z[14];
z[60] = abb[78] * z[60];
z[123] = -abb[7] + abb[9];
z[123] = abb[76] * z[123];
z[3] = z[3] + z[7] + z[43] + z[44] + -z[51] + z[60] + z[68] + z[81] + z[89] + z[108] + z[110] + z[115] + z[122] + 4 * z[123];
z[3] = abb[71] * z[3];
z[44] = -abb[38] + -z[0] + z[116];
z[60] = abb[33] * z[44];
z[60] = z[60] + z[114];
z[68] = z[58] + z[121];
z[81] = z[4] + z[73];
z[81] = abb[38] * z[81];
z[89] = -abb[35] + z[4];
z[108] = abb[33] + z[89];
z[110] = -abb[39] * z[108];
z[81] = -z[60] + z[68] + z[81] + -z[84] + z[110];
z[81] = abb[13] * z[81];
z[110] = abb[33] + -z[112];
z[110] = abb[39] * z[110];
z[60] = -z[58] + z[60] + z[110] + z[113];
z[60] = abb[12] * z[60];
z[60] = z[60] + z[81];
z[44] = z[22] * z[44];
z[44] = z[44] + z[74];
z[74] = abb[34] * z[0];
z[41] = -z[41] + z[74];
z[74] = z[35] + z[67];
z[74] = abb[38] * z[74];
z[81] = z[18] + z[41] + -z[44] + -z[74] + z[84];
z[52] = z[4] + z[52];
z[110] = -z[52] + z[70];
z[113] = abb[39] * z[110];
z[113] = -z[81] + z[113];
z[114] = 4 * abb[44];
z[115] = 2 * abb[47];
z[122] = z[114] + -z[115];
z[123] = z[113] + z[122];
z[124] = abb[7] * z[123];
z[125] = 4 * abb[42];
z[113] = -z[113] + z[125];
z[113] = z[26] * z[113];
z[44] = z[44] + -z[69] + z[84] + -z[115];
z[69] = 2 * abb[8];
z[126] = z[44] * z[69];
z[70] = z[52] + z[70];
z[127] = abb[39] * z[70];
z[44] = z[44] + z[127];
z[127] = -abb[5] * z[44];
z[128] = -z[77] + -z[115] + z[125];
z[128] = abb[4] * z[128];
z[88] = z[77] * z[88];
z[122] = z[122] + z[125];
z[125] = -z[77] + z[122];
z[125] = abb[2] * z[125];
z[129] = abb[8] * z[50];
z[70] = z[70] * z[129];
z[67] = abb[38] * z[67];
z[84] = -z[84] + z[111];
z[67] = z[67] + z[84];
z[130] = abb[39] * z[52];
z[130] = -z[67] + z[130];
z[40] = z[40] * z[130];
z[130] = -abb[47] + z[68];
z[131] = 4 * abb[16];
z[130] = z[130] * z[131];
z[131] = -z[114] * z[118];
z[40] = -abb[80] + -abb[81] + z[40] + 2 * z[60] + z[70] + z[88] + z[113] + z[124] + z[125] + z[126] + z[127] + z[128] + -z[130] + z[131];
z[40] = abb[72] * z[40];
z[60] = z[4] * z[50];
z[70] = prod_pow(abb[40], 2);
z[70] = -z[6] + z[70];
z[88] = prod_pow(abb[38], 2);
z[56] = -z[56] + z[60] + z[62] + z[70] + -z[83] + -z[88] + z[91];
z[56] = abb[3] * z[56];
z[60] = z[42] + z[92];
z[62] = -z[6] + -z[60] + z[83] + -z[90];
z[39] = abb[33] + z[39];
z[39] = abb[33] * z[39];
z[92] = abb[40] + z[37];
z[92] = abb[38] * z[92];
z[1] = -z[1] + z[39] + z[48] + z[62] + -z[92];
z[39] = z[1] + z[79];
z[39] = abb[5] * z[39];
z[79] = -abb[36] + z[31];
z[79] = abb[36] * z[79];
z[42] = z[42] + -z[63] + -z[79] + -z[90];
z[42] = abb[15] * z[42];
z[90] = abb[45] + abb[74] + z[90];
z[92] = abb[33] * z[119];
z[90] = 2 * z[90] + z[92] + -z[120];
z[90] = abb[12] * z[90];
z[33] = z[33] + -z[82];
z[82] = 2 * abb[76];
z[33] = z[33] * z[82];
z[92] = abb[33] + -abb[38];
z[113] = abb[15] * z[92];
z[119] = abb[15] * z[66];
z[113] = z[113] + -z[119];
z[113] = abb[33] * z[113];
z[14] = abb[22] + z[14];
z[14] = abb[78] * z[14];
z[119] = abb[38] * z[119];
z[7] = -z[7] + z[14] + z[33] + -z[39] + -z[42] + -z[56] + z[90] + -z[113] + -z[119];
z[14] = -abb[33] + z[102];
z[33] = -z[10] + z[14];
z[33] = abb[33] * z[33];
z[39] = z[63] + z[101];
z[33] = z[33] + z[39] + z[106] + -z[111];
z[33] = abb[4] * z[33];
z[42] = z[52] * z[129];
z[33] = -z[7] + z[33] + -z[42] + z[64];
z[42] = abb[36] + z[4];
z[52] = abb[34] + -z[22] + z[42];
z[56] = abb[39] * z[52];
z[36] = z[36] + z[60];
z[60] = z[36] + -z[56];
z[64] = -abb[38] + z[9];
z[90] = -abb[37] + z[64];
z[106] = abb[33] + z[90];
z[111] = -abb[35] + z[106];
z[111] = z[22] * z[111];
z[113] = -abb[36] + -abb[38] + -z[35] + 3 * z[86];
z[113] = abb[38] * z[113];
z[119] = z[57] + z[75];
z[5] = abb[34] + abb[40] + z[5];
z[5] = abb[40] * z[5];
z[5] = -z[5] + -z[6] + -z[60] + -z[63] + z[111] + z[113] + -z[114] + z[119];
z[5] = abb[7] * z[5];
z[4] = z[4] * z[22];
z[6] = -abb[38] + 2 * z[86];
z[6] = abb[38] * z[6];
z[111] = abb[40] * z[32];
z[4] = z[4] + -z[6] + -z[68] + z[111] + z[115];
z[6] = 2 * abb[11];
z[4] = z[4] * z[6];
z[6] = -z[83] + -z[85] + z[93];
z[32] = -abb[36] + z[32] + -z[102];
z[32] = abb[38] * z[32];
z[85] = -z[28] + z[45];
z[93] = abb[33] * z[85];
z[93] = z[58] + z[93];
z[113] = -z[75] + z[93];
z[32] = -z[6] + z[32] + z[113];
z[32] = abb[2] * z[32];
z[114] = -z[27] + z[116];
z[114] = abb[33] * z[114];
z[120] = z[27] + -z[35];
z[124] = abb[38] * z[120];
z[114] = -z[58] + z[114] + -z[119] + -z[124];
z[114] = abb[13] * z[114];
z[125] = 4 * abb[8] + -z[118];
z[125] = z[121] * z[125];
z[4] = z[4] + z[5] + -z[32] + z[114] + -z[125] + z[130];
z[5] = -abb[35] + z[27];
z[5] = abb[33] + 2 * z[5];
z[5] = abb[33] * z[5];
z[5] = z[5] + z[57] + -z[58] + -z[74] + -z[84] + z[115];
z[5] = z[5] * z[69];
z[32] = abb[21] + abb[24];
z[57] = -abb[20] + z[32];
z[57] = abb[48] * z[57];
z[5] = abb[79] + -abb[82] + z[4] + z[5] + -z[33] + -z[43] + -z[57];
z[5] = z[5] * z[15];
z[15] = abb[39] * z[49];
z[15] = z[15] + z[38];
z[30] = z[30] * z[35];
z[43] = abb[33] + -z[65] + -z[103];
z[43] = abb[33] * z[43];
z[49] = 4 * abb[46];
z[57] = -abb[37] + z[28];
z[58] = -abb[34] * z[57];
z[65] = -abb[36] + -abb[37] + z[65];
z[65] = abb[36] * z[65];
z[30] = z[15] + z[30] + -z[39] + z[43] + -z[47] + -z[49] + z[58] + z[65] + -z[83];
z[30] = abb[4] * z[30];
z[43] = z[50] * z[110];
z[47] = z[19] + -z[28];
z[0] = z[0] * z[47];
z[47] = abb[33] + abb[34];
z[29] = 6 * abb[35] + -z[29] + -4 * z[47];
z[29] = abb[33] * z[29];
z[47] = -3 * abb[35] + 2 * z[57];
z[47] = abb[35] * z[47];
z[0] = -6 * abb[42] + z[0] + z[29] + z[43] + z[46] + z[47] + -z[49];
z[0] = abb[0] * z[0];
z[29] = z[81] + z[115];
z[29] = z[29] * z[69];
z[43] = -z[110] * z[129];
z[46] = abb[4] + -abb[10];
z[46] = abb[75] * z[46];
z[0] = z[0] + z[4] + z[7] + z[29] + z[30] + z[43] + 4 * z[46] + -z[51];
z[0] = abb[69] * z[0];
z[4] = -z[9] + z[27];
z[7] = z[4] + z[20];
z[7] = abb[33] * z[7];
z[20] = abb[34] + z[86];
z[20] = abb[40] * z[20];
z[27] = abb[36] + abb[40];
z[27] = abb[38] * z[27];
z[7] = z[7] + z[15] + z[20] + -z[27] + z[48] + -z[53] + z[63];
z[7] = z[7] * z[26];
z[15] = -z[28] + z[66] + z[92];
z[15] = abb[33] * z[15];
z[20] = -z[31] + z[42];
z[20] = abb[38] * z[20];
z[6] = -z[6] + z[15] + -z[20] + z[48] + -z[121];
z[6] = abb[2] * z[6];
z[15] = abb[33] + z[4];
z[15] = abb[33] * z[15];
z[4] = abb[38] * z[4];
z[26] = z[111] + z[121];
z[4] = z[4] + z[15] + z[26] + z[48];
z[4] = abb[13] * z[4];
z[15] = abb[33] * z[106];
z[15] = z[15] + -z[20] + -z[39];
z[27] = z[15] + -z[60] + -z[121];
z[27] = abb[7] * z[27];
z[29] = z[67] * z[69];
z[30] = -z[69] + z[118];
z[30] = z[30] * z[121];
z[4] = z[4] + -z[6] + -z[7] + z[27] + -z[29] + z[30] + -z[33];
z[4] = z[4] * z[8];
z[6] = -z[18] + z[76];
z[7] = -z[35] + z[87];
z[7] = abb[38] * z[7];
z[18] = abb[40] + z[73];
z[18] = abb[33] + 2 * z[18];
z[18] = abb[33] * z[18];
z[7] = -z[6] + z[7] + z[18] + z[41] + -z[55] + z[56] + -z[101];
z[7] = abb[64] * z[7];
z[18] = -z[28] + -z[120];
z[18] = abb[33] * z[18];
z[27] = abb[34] * z[71];
z[6] = z[6] + z[18] + z[27] + -z[63] + -z[91] + -z[124];
z[6] = abb[60] * z[6];
z[18] = -z[12] * z[123];
z[27] = z[75] + z[88];
z[29] = z[27] + z[121];
z[30] = abb[47] + z[38] + z[96];
z[31] = -abb[34] + abb[35];
z[31] = z[31] * z[50];
z[33] = abb[33] + 2 * z[89];
z[33] = abb[33] * z[33];
z[30] = -z[29] + 2 * z[30] + z[31] + z[33];
z[30] = abb[59] * z[30];
z[15] = z[15] + -z[36];
z[15] = -z[15] * z[24];
z[31] = abb[60] + z[23];
z[31] = z[31] * z[109];
z[33] = z[11] * z[121];
z[36] = z[31] + z[33];
z[39] = z[21] * z[82];
z[41] = -z[25] * z[52];
z[6] = z[6] + z[7] + z[15] + z[18] + z[30] + z[36] + -z[39] + z[41];
z[6] = abb[27] * z[6];
z[7] = abb[34] + -z[35] + z[102];
z[7] = abb[33] * z[7];
z[10] = z[10] + z[117];
z[10] = abb[39] * z[10];
z[7] = z[7] + z[10] + z[38] + -z[48] + -z[70] + -z[75] + z[115];
z[7] = abb[59] * z[7];
z[10] = -abb[64] * z[72];
z[1] = -z[1] * z[24];
z[15] = -z[12] * z[44];
z[18] = z[54] + z[59] + z[83];
z[30] = z[18] + z[91];
z[37] = abb[35] + -z[37];
z[37] = -abb[38] + 2 * z[37];
z[37] = abb[38] * z[37];
z[38] = -abb[33] + 2 * z[112];
z[38] = abb[33] * z[38];
z[37] = -z[30] + z[37] + z[38] + z[76];
z[37] = abb[60] * z[37];
z[38] = -z[25] * z[78];
z[38] = z[38] + -z[39];
z[1] = z[1] + z[7] + z[10] + z[15] + z[31] + z[37] + z[38];
z[1] = abb[26] * z[1];
z[7] = -z[13] + z[19] + z[64];
z[7] = abb[33] * z[7];
z[7] = z[7] + -z[20] + z[62] + -z[100];
z[7] = z[7] * z[24];
z[10] = z[77] + z[122];
z[12] = z[10] * z[12];
z[7] = z[7] + z[12] + -z[38];
z[12] = -z[61] + z[86];
z[12] = abb[38] * z[12];
z[12] = z[12] + -z[16] + z[75] + z[80] + -z[104];
z[12] = abb[64] * z[12];
z[13] = abb[47] + z[97];
z[15] = -abb[42] + z[13];
z[15] = 2 * z[15] + -z[29] + z[98] + z[104];
z[15] = abb[59] * z[15];
z[16] = -abb[38] * z[85];
z[16] = z[16] + -z[30] + z[113];
z[16] = abb[60] * z[16];
z[12] = -z[7] + z[12] + z[15] + z[16] + z[33];
z[12] = abb[25] * z[12];
z[15] = -abb[38] * z[28];
z[13] = 2 * z[13] + z[15] + -z[68] + z[98] + z[105];
z[13] = abb[59] * z[13];
z[15] = -abb[64] * z[107];
z[16] = abb[38] * z[66];
z[16] = z[16] + z[91];
z[18] = -z[16] + -z[18] + z[34] + z[93];
z[18] = abb[60] * z[18];
z[7] = -z[7] + z[13] + z[15] + z[18] + z[36];
z[7] = abb[28] * z[7];
z[13] = -abb[37] + z[89];
z[13] = z[13] * z[102];
z[15] = -abb[37] + z[108];
z[15] = z[15] * z[22];
z[13] = z[13] + z[15] + z[48] + z[68] + z[111] + z[119];
z[13] = abb[21] * z[13];
z[9] = abb[37] + z[9] + -z[35];
z[9] = abb[38] * z[9];
z[15] = z[90] + -z[116];
z[15] = abb[33] * z[15];
z[9] = z[9] + z[15] + -z[26] + z[119];
z[9] = abb[17] * z[9];
z[14] = -abb[33] * z[14];
z[14] = z[14] + z[27] + z[48] + -z[68];
z[14] = abb[20] * z[14];
z[9] = z[9] + z[13] + z[14];
z[9] = abb[73] * z[9];
z[13] = abb[17] + z[32];
z[13] = -z[8] * z[13];
z[14] = -abb[31] * z[21];
z[15] = -abb[0] + abb[7];
z[15] = abb[73] * z[15];
z[18] = -abb[69] * z[32];
z[19] = abb[17] * abb[71];
z[8] = abb[69] + z[8];
z[20] = abb[20] * z[8];
z[13] = z[13] + z[14] + z[15] + z[18] + z[19] + z[20];
z[13] = abb[48] * z[13];
z[14] = abb[27] * z[123];
z[15] = abb[25] + abb[28];
z[10] = z[10] * z[15];
z[15] = abb[26] * z[44];
z[10] = abb[58] + z[10] + z[14] + z[15];
z[10] = abb[65] * z[10];
z[14] = -abb[33] + z[45];
z[14] = abb[33] * z[14];
z[14] = z[14] + -z[16] + z[63] + z[79];
z[14] = z[14] * z[21];
z[14] = z[14] + z[39];
z[14] = abb[30] * z[14];
z[15] = -z[24] * z[95];
z[16] = -z[23] * z[99];
z[18] = -z[25] * z[94];
z[15] = z[15] + z[16] + z[18];
z[15] = abb[29] * z[15];
z[16] = abb[58] * z[17];
z[11] = -abb[59] + -z[11];
z[11] = abb[32] * abb[78] * z[11];
z[17] = -abb[71] + -abb[72] + z[8];
z[17] = abb[79] * z[17];
z[8] = -abb[71] + -3 * abb[72] + -z[8];
z[8] = abb[82] * z[8];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[40];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_859_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("-2.524847202924887863973136789500371089537089249336092385131738684")}, std::complex<T>{stof<T>("-2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("-5.1156137591021159324521833062370106254853630667671669020404408748"),stof<T>("8.1590970221738957400031467760741852677013306533114953925001605429")}, std::complex<T>{T(0),stof<T>("4.8838051494255641203781278846932830704884750971992815173794895611")}, std::complex<T>{stof<T>("5.1156137591021159324521833062370106254853630667671669020404408748"),stof<T>("-8.1590970221738957400031467760741852677013306533114953925001605429")}, std::complex<T>{stof<T>("-2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("4.9667497776376699241622007318470126247813267979357331438214834646")}, std::complex<T>{T(0),stof<T>("-2.4419025747127820601890639423466415352442375485996407586897447806")}, std::complex<T>{stof<T>("13.842052801351284171667853128630002617836076455602366060010560602"),stof<T>("9.889730798974770082178024667461046681760135504376391620493800699")}, std::complex<T>{stof<T>("11.139132027333647379583828047185743614487900634439029317301528792"),stof<T>("9.889730798974770082178024667461046681760135504376391620493800699")}, std::complex<T>{stof<T>("13.842052801351284171667853128630002617836076455602366060010560602"),stof<T>("9.889730798974770082178024667461046681760135504376391620493800699")}, std::complex<T>{stof<T>("-2.4939003900426225558292045134710842137374617248643126052941063816"),stof<T>("3.0920666745614964014334366177485018354415681810202102422555504278")}, std::complex<T>{stof<T>("11.139132027333647379583828047185743614487900634439029317301528792"),stof<T>("9.889730798974770082178024667461046681760135504376391620493800699")}, std::complex<T>{stof<T>("0.7044327373668627606996857815859311756068744341555137305507785022"),stof<T>("-1.5075010738479827813992849862854081364808437073584047108021113766")}, stof<T>("11.0940356532336271997514588314622372152655935607305737954051615464"), stof<T>("-4.8774633435761477613991183528483265100206166505662406047528825957")};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[93].real()/kbase.W[93].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[160]) - log_iphi_im(kbase.W[160]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_859_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_859_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.1765302386471297128623954798335858204546452489373033659790159279"),stof<T>("-5.0812086329262811649309628951172101118924354693940436858887864507")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({201});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,84> abb = {dl[0], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W58(k,dl), dlog_W118(k,dv), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_16(k), f_2_17(k), f_2_21(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_10_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[93].real()/k.W[93].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[160]) - log_iphi_im(k.W[160])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_10_re(k), f_2_24_re(k), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W168(k,dv) * f_2_33(k);
abb[79] = c.real();
abb[54] = c.imag();
SpDLog_Sigma5<T,201,167>(k, dv, abb[79], abb[54], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W170(k,dv) * f_2_33(k);
abb[80] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,201,169>(k, dv, abb[80], abb[55], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W181(k,dv) * f_2_33(k);
abb[81] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,201,180>(k, dv, abb[81], abb[56], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W184(k,dv) * f_2_33(k);
abb[82] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,201,183>(k, dv, abb[82], abb[57], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W192(k,dv) * f_2_33(k);
abb[83] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,201,191>(k, dv, abb[83], abb[58], f_2_33_series_coefficients<T>);
}

                    
            return f_4_859_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_859_DLogXconstant_part(base_point<T>, kend);
	value += f_4_859_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_859_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_859_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_859_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_859_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_859_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_859_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
