/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_181.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_181_abbreviated (const std::array<T,17>& abb) {
T z[30];
z[0] = -abb[12] + abb[13];
z[1] = -3 * z[0];
z[2] = -abb[11] + abb[14];
z[3] = abb[15] + z[2];
z[4] = 2 * z[3];
z[5] = z[1] + z[4];
z[6] = abb[7] * z[5];
z[7] = (T(-1) / T(2)) * z[0];
z[8] = -z[3] + z[7];
z[8] = abb[6] * z[8];
z[6] = z[6] + z[8];
z[6] = abb[6] * z[6];
z[8] = 2 * abb[15];
z[1] = z[1] + z[8];
z[9] = -z[1] + -3 * z[2];
z[9] = abb[7] * z[9];
z[10] = (T(-3) / T(2)) * z[0];
z[11] = (T(1) / T(2)) * z[2];
z[12] = abb[15] + z[10] + z[11];
z[12] = abb[8] * z[12];
z[13] = abb[6] * z[0];
z[9] = z[9] + z[12] + 2 * z[13];
z[9] = abb[8] * z[9];
z[12] = 2 * abb[9];
z[13] = z[3] * z[12];
z[14] = -abb[9] * z[0];
z[13] = z[13] + z[14];
z[15] = prod_pow(abb[7], 2);
z[16] = (T(3) / T(2)) * z[2];
z[17] = z[15] * z[16];
z[1] = z[1] + z[2];
z[18] = -abb[16] * z[1];
z[19] = prod_pow(m1_set::bc<T>[0], 2);
z[20] = z[2] * z[19];
z[6] = z[6] + z[9] + -z[13] + z[17] + z[18] + (T(1) / T(3)) * z[20];
z[6] = abb[1] * z[6];
z[9] = z[0] + -z[8] + z[16];
z[9] = abb[0] * z[9];
z[17] = z[3] + z[7];
z[17] = abb[2] * z[17];
z[18] = z[0] + -z[2];
z[20] = abb[5] * z[18];
z[7] = z[4] + z[7];
z[7] = abb[3] * z[7];
z[16] = -abb[4] * z[16];
z[7] = z[7] + z[9] + z[16] + -z[17] + (T(-1) / T(2)) * z[20];
z[7] = abb[8] * z[7];
z[9] = -z[0] + z[4];
z[16] = abb[2] * z[9];
z[5] = -abb[3] * z[5];
z[21] = -abb[15] + z[0];
z[22] = abb[0] * z[21];
z[22] = 2 * z[22];
z[5] = z[5] + z[16] + -z[20] + -z[22];
z[5] = abb[7] * z[5];
z[23] = 4 * abb[15];
z[24] = 2 * abb[12] + z[23];
z[25] = z[2] + z[24];
z[25] = abb[4] * z[25];
z[26] = -z[2] + z[8];
z[27] = abb[0] * z[26];
z[28] = abb[4] * abb[13];
z[29] = 2 * z[28];
z[4] = -abb[3] * z[4];
z[4] = z[4] + z[25] + z[27] + -z[29];
z[4] = abb[6] * z[4];
z[4] = z[4] + z[5] + z[7];
z[4] = abb[8] * z[4];
z[5] = z[11] + -z[24];
z[5] = abb[4] * z[5];
z[7] = -z[11] + -z[21];
z[7] = abb[0] * z[7];
z[11] = z[0] + -z[3];
z[11] = abb[3] * z[11];
z[5] = z[5] + z[7] + z[11] + z[17] + z[29];
z[5] = abb[6] * z[5];
z[7] = abb[0] + -abb[3];
z[7] = z[0] * z[7];
z[7] = 2 * z[7] + -z[16];
z[7] = abb[7] * z[7];
z[5] = z[5] + z[7];
z[5] = abb[6] * z[5];
z[7] = -abb[9] * z[26];
z[11] = abb[12] * z[12];
z[17] = -abb[13] * z[12];
z[7] = z[7] + z[11] + z[17];
z[7] = abb[0] * z[7];
z[10] = z[3] + z[10];
z[10] = abb[3] * z[10];
z[17] = abb[0] * abb[15];
z[10] = z[10] + -z[17] + (T(3) / T(2)) * z[20];
z[10] = z[10] * z[15];
z[15] = abb[2] + -abb[3];
z[15] = z[9] * z[15];
z[15] = z[15] + z[20];
z[22] = -z[15] + z[22];
z[24] = -abb[16] * z[22];
z[25] = (T(-1) / T(3)) * z[0] + (T(3) / T(2)) * z[3];
z[25] = abb[3] * z[25];
z[17] = (T(-3) / T(2)) * z[17] + (T(1) / T(3)) * z[20] + z[25];
z[17] = z[17] * z[19];
z[19] = z[2] + z[23];
z[19] = -abb[9] * z[19];
z[11] = -z[11] + z[19];
z[11] = abb[4] * z[11];
z[12] = z[12] * z[28];
z[3] = abb[9] * z[3];
z[3] = z[3] + -z[14];
z[3] = abb[3] * z[3];
z[13] = abb[2] * z[13];
z[3] = 2 * z[3] + z[4] + z[5] + z[6] + z[7] + z[10] + z[11] + z[12] + z[13] + z[17] + z[24];
z[4] = abb[12] + z[26];
z[4] = abb[4] * z[4];
z[4] = z[4] + -z[28];
z[5] = abb[3] * z[9];
z[0] = z[0] + -z[26];
z[0] = abb[0] * z[0];
z[0] = z[0] + z[4] + z[5];
z[0] = 2 * z[0] + -z[16];
z[0] = abb[6] * z[0];
z[6] = -abb[0] * z[8];
z[5] = z[5] + z[6] + z[20];
z[5] = abb[7] * z[5];
z[0] = z[0] + z[5];
z[0] = m1_set::bc<T>[0] * z[0];
z[5] = -z[2] + -z[21];
z[5] = abb[0] * z[5];
z[4] = -z[4] + z[5];
z[4] = 2 * z[4] + z[15];
z[5] = abb[8] * m1_set::bc<T>[0];
z[4] = z[4] * z[5];
z[6] = abb[6] * z[9];
z[2] = abb[7] * z[2];
z[2] = z[2] + z[6];
z[2] = m1_set::bc<T>[0] * z[2];
z[6] = -z[8] + z[18];
z[5] = z[5] * z[6];
z[2] = z[2] + z[5];
z[2] = abb[1] * z[2];
z[1] = -abb[1] * z[1];
z[1] = z[1] + -z[22];
z[1] = abb[10] * z[1];
z[0] = z[0] + z[1] + z[2] + z[4];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_181_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-28.682709507252924038150088820615867802099975833381699897357870163"),stof<T>("20.786517313818547426646312610250515364634369966666037758505747914")}, std::complex<T>{stof<T>("-1.3725045939133174391960661484225417765815808843972237452346983228"),stof<T>("4.5544760363047657185650386681565981304782305373677721383891353621")}, std::complex<T>{stof<T>("1.3725045939133174391960661484225417765815808843972237452346983228"),stof<T>("-4.5544760363047657185650386681565981304782305373677721383891353621")}, std::complex<T>{stof<T>("28.682709507252924038150088820615867802099975833381699897357870163"),stof<T>("-20.786517313818547426646312610250515364634369966666037758505747914")}, std::complex<T>{stof<T>("3.329811958466160968998434387206254043411841238899987442028638633"),stof<T>("19.322285125895090055085858194221942249904257934174452249475877721")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[17].real()/kbase.W[17].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_181_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_181_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("26.312310827701989814811624859546773126810407033334666734753840204"),stof<T>("-7.090702456575315878875230790352795075593001081379978524445607308")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,17> abb = {dlog_W3(k,dl), dlog_W7(k,dl), dlog_W8(k,dl), dl[4], dlog_W18(k,dl), dlog_W26(k,dl), f_1_6(k), f_1_8(k), f_1_10(k), f_2_8(k), f_2_11_im(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[17].real()/k.W[17].real()), f_2_11_re(k)};

                    
            return f_4_181_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_181_DLogXconstant_part(base_point<T>, kend);
	value += f_4_181_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_181_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_181_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_181_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_181_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_181_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_181_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
