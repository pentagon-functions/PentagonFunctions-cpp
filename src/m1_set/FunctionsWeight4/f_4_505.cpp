/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_505.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_505_abbreviated (const std::array<T,59>& abb) {
T z[95];
z[0] = -abb[28] + abb[29];
z[1] = abb[24] + abb[25];
z[1] = -z[0] * z[1];
z[2] = abb[23] * (T(1) / T(2));
z[3] = abb[21] * (T(1) / T(2)) + z[2];
z[4] = abb[20] + abb[25];
z[5] = abb[22] + z[4];
z[6] = -z[3] + -z[5];
z[6] = abb[33] * z[6];
z[7] = abb[32] * z[5];
z[1] = z[1] + z[6] + z[7];
z[1] = abb[33] * z[1];
z[6] = abb[21] + abb[23];
z[7] = abb[20] + z[6];
z[8] = -abb[24] + z[7];
z[9] = (T(1) / T(2)) * z[8];
z[10] = abb[29] * z[9];
z[11] = abb[25] + z[7];
z[12] = abb[22] + z[11];
z[13] = abb[32] * z[12];
z[11] = abb[30] * z[11];
z[10] = z[10] + -z[11] + z[13];
z[13] = abb[24] + z[7];
z[13] = abb[25] + (T(1) / T(2)) * z[13];
z[13] = abb[28] * z[13];
z[12] = abb[33] * z[12];
z[12] = -z[10] + z[12] + -z[13];
z[12] = abb[31] * z[12];
z[14] = prod_pow(abb[30], 2);
z[15] = abb[25] + (T(1) / T(2)) * z[6];
z[15] = z[14] * z[15];
z[16] = abb[20] + -abb[24];
z[16] = abb[28] * z[16];
z[11] = -z[11] + (T(1) / T(2)) * z[16];
z[11] = abb[28] * z[11];
z[16] = abb[32] * (T(1) / T(2));
z[17] = abb[28] + z[16];
z[17] = z[6] * z[17];
z[18] = abb[25] + z[6];
z[19] = -abb[30] * z[18];
z[17] = z[17] + z[19];
z[17] = abb[32] * z[17];
z[19] = -abb[30] + abb[32];
z[20] = abb[25] * z[19];
z[13] = z[13] + z[20];
z[13] = abb[29] * z[13];
z[20] = abb[20] + abb[21];
z[2] = abb[24] * (T(-1) / T(3)) + abb[22] * (T(5) / T(6)) + z[2] + (T(1) / T(2)) * z[20];
z[20] = prod_pow(m1_set::bc<T>[0], 2);
z[2] = z[2] * z[20];
z[21] = -abb[24] + -z[18];
z[21] = abb[34] * z[21];
z[22] = abb[56] * z[5];
z[18] = abb[35] * z[18];
z[6] = abb[37] * z[6];
z[8] = abb[22] + z[8];
z[23] = abb[55] * z[8];
z[7] = -abb[36] * z[7];
z[1] = z[1] + z[2] + z[6] + z[7] + z[11] + z[12] + z[13] + z[15] + z[17] + z[18] + z[21] + z[22] + z[23];
z[2] = abb[52] * (T(1) / T(2));
z[1] = z[1] * z[2];
z[6] = -abb[48] + abb[49];
z[7] = abb[13] * z[6];
z[11] = abb[44] + abb[45] + -abb[46];
z[12] = abb[43] + (T(1) / T(2)) * z[11];
z[13] = 2 * abb[53] + abb[47] * (T(-1) / T(2)) + z[12];
z[15] = abb[18] * z[13];
z[7] = z[7] + -z[15];
z[15] = abb[19] * z[13];
z[17] = abb[15] * z[6];
z[18] = abb[49] + abb[50];
z[21] = abb[10] * z[18];
z[22] = -z[7] + z[15] + -z[17] + z[21];
z[23] = -abb[48] + abb[50];
z[24] = abb[2] * z[23];
z[25] = abb[49] + abb[51];
z[26] = abb[48] + z[25];
z[27] = abb[12] * z[26];
z[28] = z[24] + -z[27];
z[29] = abb[50] + abb[51];
z[30] = (T(1) / T(3)) * z[29];
z[31] = abb[48] * (T(1) / T(2));
z[32] = abb[49] * (T(5) / T(6)) + z[30] + z[31];
z[32] = abb[4] * z[32];
z[33] = (T(2) / T(3)) * z[25] + z[31];
z[33] = abb[6] * z[33];
z[34] = abb[48] + abb[49];
z[35] = z[29] + -z[34];
z[36] = abb[3] * z[35];
z[37] = abb[50] + z[26];
z[38] = abb[8] * z[37];
z[12] = abb[53] + abb[47] * (T(-1) / T(4)) + (T(1) / T(2)) * z[12];
z[39] = abb[16] * z[12];
z[40] = abb[1] * z[25];
z[41] = (T(1) / T(2)) * z[29];
z[42] = abb[49] * (T(1) / T(3)) + -z[41];
z[42] = abb[0] * z[42];
z[30] = abb[48] + z[30];
z[30] = abb[49] + (T(1) / T(2)) * z[30];
z[30] = abb[5] * z[30];
z[43] = abb[17] * z[12];
z[22] = (T(1) / T(3)) * z[22] + (T(2) / T(3)) * z[28] + z[30] + z[32] + z[33] + (T(1) / T(12)) * z[36] + (T(-5) / T(12)) * z[38] + z[39] + z[40] + z[42] + (T(5) / T(3)) * z[43];
z[20] = z[20] * z[22];
z[22] = z[21] + -z[40];
z[30] = abb[50] + -abb[51];
z[32] = z[30] + z[34];
z[33] = abb[7] * z[32];
z[36] = abb[48] + -abb[51];
z[42] = abb[9] * z[36];
z[43] = (T(1) / T(4)) * z[33] + -z[42];
z[32] = abb[4] * z[32];
z[44] = (T(1) / T(2)) * z[6];
z[45] = abb[13] * z[44];
z[46] = abb[18] * z[12];
z[45] = z[45] + -z[46];
z[46] = z[23] + -z[25];
z[47] = abb[0] * z[46];
z[48] = abb[15] * z[44];
z[49] = abb[6] * abb[49];
z[50] = abb[5] * abb[48];
z[47] = -z[22] + (T(-1) / T(4)) * z[32] + z[43] + z[45] + z[47] + -z[48] + z[49] + z[50];
z[47] = abb[28] * z[47];
z[49] = z[23] + z[25];
z[50] = abb[4] * z[49];
z[51] = 3 * abb[49];
z[52] = abb[48] + z[51];
z[53] = 3 * abb[50] + -abb[51] + -z[52];
z[54] = abb[0] * z[53];
z[50] = z[50] + -z[54];
z[54] = abb[6] * z[34];
z[50] = -z[7] + (T(1) / T(2)) * z[50] + -z[54];
z[55] = abb[30] * z[50];
z[56] = abb[14] * (T(1) / T(2));
z[56] = z[49] * z[56];
z[57] = abb[30] * z[56];
z[58] = 2 * z[23];
z[59] = abb[30] * z[58];
z[60] = abb[5] * z[59];
z[60] = -z[57] + z[60];
z[55] = z[55] + z[60];
z[47] = z[47] + z[55];
z[47] = abb[28] * z[47];
z[61] = 3 * abb[48];
z[62] = z[25] + z[61];
z[63] = abb[50] + -z[62];
z[63] = abb[4] * z[63];
z[64] = abb[49] + z[61];
z[65] = -z[29] + z[64];
z[66] = abb[0] * z[65];
z[67] = abb[5] * z[37];
z[63] = z[63] + -z[66] + -z[67];
z[37] = (T(1) / T(2)) * z[37];
z[66] = abb[8] * z[37];
z[67] = -z[56] + z[66];
z[31] = abb[49] * (T(1) / T(2)) + -z[31];
z[68] = abb[51] + z[31];
z[68] = abb[6] * z[68];
z[43] = (T(1) / T(2)) * z[21] + -z[27] + -z[39] + (T(3) / T(2)) * z[40] + -z[43] + (T(1) / T(4)) * z[63] + z[67] + z[68];
z[43] = abb[33] * z[43];
z[27] = 2 * z[27];
z[63] = -z[17] + -z[27] + 4 * z[40];
z[68] = abb[48] + abb[51];
z[68] = abb[49] + -abb[50] + 3 * z[68];
z[69] = abb[0] * (T(1) / T(2));
z[68] = z[68] * z[69];
z[70] = abb[4] * (T(1) / T(2));
z[71] = z[49] * z[70];
z[72] = abb[5] * z[34];
z[73] = -z[56] + z[72];
z[25] = abb[6] * z[25];
z[25] = 2 * z[25] + z[63] + -z[68] + z[71] + z[73];
z[0] = z[0] * z[25];
z[25] = abb[4] * z[46];
z[25] = z[25] + -z[33];
z[71] = z[21] + z[40];
z[74] = abb[5] * z[37];
z[74] = -z[71] + z[74];
z[25] = (T(-1) / T(2)) * z[25] + -z[67] + z[74];
z[75] = abb[32] * z[25];
z[0] = z[0] + z[43] + z[75];
z[0] = abb[33] * z[0];
z[43] = z[45] + z[56];
z[75] = 2 * z[40];
z[27] = -z[27] + z[75];
z[48] = z[27] + -z[48];
z[76] = z[29] + z[52];
z[77] = z[70] * z[76];
z[78] = -z[29] + z[52];
z[78] = z[69] * z[78];
z[79] = abb[50] * (T(1) / T(2));
z[80] = abb[51] * (T(3) / T(2)) + -z[79];
z[81] = z[34] + z[80];
z[81] = abb[6] * z[81];
z[82] = 2 * abb[49];
z[41] = z[41] + z[82];
z[41] = abb[5] * z[41];
z[41] = z[41] + -z[43] + z[48] + z[77] + z[78] + z[81];
z[41] = abb[28] * z[41];
z[77] = 2 * abb[48];
z[78] = z[77] + z[82];
z[80] = z[78] + z[80];
z[80] = abb[6] * z[80];
z[39] = 3 * z[39];
z[48] = z[39] + z[48] + z[80];
z[80] = abb[51] * (T(1) / T(2));
z[81] = abb[50] * (T(3) / T(2)) + -z[80];
z[83] = -z[78] + z[81];
z[83] = abb[5] * z[83];
z[84] = abb[0] * z[30];
z[85] = abb[4] * z[34];
z[83] = z[45] + z[48] + -z[83] + z[84] + z[85];
z[83] = abb[29] * z[83];
z[84] = abb[28] * z[23];
z[86] = abb[29] * z[23];
z[59] = -z[59] + z[84] + z[86];
z[87] = 2 * abb[2];
z[59] = z[59] * z[87];
z[59] = -z[59] + z[83];
z[76] = z[69] * z[76];
z[83] = abb[4] * abb[48];
z[87] = abb[5] * z[77];
z[83] = -z[76] + z[83] + z[87];
z[61] = z[51] + z[61];
z[30] = -z[30] + z[61];
z[87] = abb[6] * (T(1) / T(2));
z[88] = -z[30] * z[87];
z[89] = abb[16] * z[13];
z[28] = -z[28] + -z[40] + -z[83] + z[88] + -z[89];
z[28] = abb[31] * z[28];
z[54] = z[54] + z[89];
z[83] = z[54] + -z[67] + z[83];
z[88] = -abb[32] + abb[33];
z[83] = z[83] * z[88];
z[90] = abb[29] * z[12];
z[91] = abb[28] * z[12];
z[92] = z[90] + -z[91];
z[93] = -3 * abb[17] + -abb[19];
z[92] = z[92] * z[93];
z[93] = abb[30] * z[13];
z[93] = z[91] + z[93];
z[93] = abb[16] * z[93];
z[28] = z[28] + z[41] + -z[55] + -z[59] + z[83] + z[92] + z[93];
z[28] = abb[31] * z[28];
z[41] = 2 * abb[51];
z[55] = z[41] + z[52];
z[55] = abb[6] * z[55];
z[83] = abb[17] * z[13];
z[92] = z[83] + z[89];
z[37] = abb[4] * z[37];
z[93] = 2 * z[26];
z[93] = abb[0] * z[93];
z[94] = abb[5] * z[64];
z[15] = z[15] + z[37] + z[55] + z[63] + -z[66] + z[92] + -z[93] + z[94];
z[37] = abb[55] * z[15];
z[55] = (T(1) / T(2)) * z[35];
z[63] = abb[3] * z[55];
z[93] = abb[4] * z[55];
z[94] = -abb[50] + z[34];
z[94] = abb[0] * z[94];
z[52] = -abb[6] * z[52];
z[64] = 2 * abb[50] + -z[64];
z[64] = abb[5] * z[64];
z[52] = -z[7] + 4 * z[24] + z[52] + -z[63] + z[64] + -z[92] + z[93] + 2 * z[94];
z[52] = abb[36] * z[52];
z[64] = 2 * z[42];
z[92] = z[17] + z[64];
z[93] = (T(1) / T(2)) * z[33];
z[36] = -abb[4] * z[36];
z[94] = abb[6] * z[6];
z[36] = z[36] + -z[56] + -z[68] + z[75] + -z[92] + z[93] + z[94];
z[36] = abb[34] * z[36];
z[42] = -z[42] + z[93];
z[51] = -abb[51] + z[23] + -z[51];
z[51] = z[51] * z[70];
z[51] = -z[42] + z[51] + -z[76];
z[35] = abb[6] * z[35];
z[31] = -abb[50] + -z[31];
z[31] = abb[5] * z[31];
z[31] = z[21] + z[31] + (T(1) / T(4)) * z[35] + (T(1) / T(2)) * z[51] + z[56] + -z[63];
z[31] = z[14] * z[31];
z[35] = abb[4] * z[65];
z[51] = abb[50] + z[62];
z[51] = abb[0] * z[51];
z[17] = z[17] + z[35] + z[51];
z[35] = z[79] + z[80];
z[51] = -z[35] + z[77];
z[51] = abb[6] * z[51];
z[62] = z[34] + -z[81];
z[62] = abb[5] * z[62];
z[17] = (T(1) / T(2)) * z[17] + z[39] + z[43] + z[51] + z[62] + -z[75];
z[17] = abb[28] * z[17];
z[39] = z[65] * z[69];
z[43] = abb[4] * abb[49];
z[51] = abb[6] * z[82];
z[39] = -z[39] + z[43] + z[51];
z[43] = z[39] + z[63];
z[51] = z[43] + z[72];
z[51] = abb[30] * z[51];
z[43] = -z[43] + -z[73];
z[43] = abb[32] * z[43];
z[62] = abb[5] * (T(1) / T(2));
z[30] = -z[30] * z[62];
z[30] = z[30] + -z[39] + z[40];
z[30] = abb[29] * z[30];
z[17] = z[17] + z[30] + z[43] + z[51] + -z[57];
z[17] = abb[29] * z[17];
z[30] = abb[5] + abb[6] + -abb[13] + -abb[15];
z[30] = z[12] * z[30];
z[39] = z[34] + z[35];
z[39] = abb[17] * z[39];
z[40] = -z[34] + z[35];
z[40] = abb[16] * z[40];
z[43] = abb[18] + abb[19];
z[43] = z[43] * z[44];
z[11] = 2 * abb[43] + -abb[47] + 4 * abb[53] + z[11];
z[11] = abb[27] * z[11];
z[44] = abb[4] * z[13];
z[11] = z[11] + z[30] + z[39] + -z[40] + z[43] + z[44];
z[30] = abb[26] * abb[52];
z[30] = -z[11] + (T(-1) / T(4)) * z[30];
z[30] = abb[57] * z[30];
z[39] = z[56] + z[93];
z[40] = z[53] * z[69];
z[18] = abb[4] * z[18];
z[6] = abb[5] * z[6];
z[6] = -z[6] + z[7] + -z[18] + -2 * z[21] + z[39] + z[40];
z[2] = z[2] * z[4];
z[7] = abb[2] * z[58];
z[2] = -z[2] + -z[6] + z[7] + z[83];
z[18] = abb[54] * z[2];
z[21] = z[46] * z[70];
z[21] = z[21] + -z[42];
z[40] = abb[6] * z[55];
z[40] = z[21] + z[40];
z[42] = z[40] + -z[63];
z[43] = -abb[30] * z[42];
z[32] = z[32] + -z[33];
z[32] = (T(1) / T(2)) * z[32] + z[54] + z[72];
z[32] = abb[28] * z[32];
z[33] = z[40] + -z[74];
z[33] = z[16] * z[33];
z[32] = z[32] + z[33] + z[43] + -z[57];
z[32] = abb[32] * z[32];
z[33] = -z[62] + z[87];
z[33] = z[33] * z[49];
z[34] = -abb[0] * z[34];
z[21] = z[21] + -z[24] + z[33] + z[34] + z[71];
z[21] = abb[37] * z[21];
z[33] = abb[29] * z[13];
z[34] = abb[28] * z[13];
z[33] = z[33] + -z[34];
z[33] = abb[33] * z[33];
z[40] = -z[12] * z[14];
z[19] = -abb[29] + -z[19];
z[19] = z[13] * z[19];
z[19] = z[19] + z[91];
z[19] = abb[29] * z[19];
z[43] = abb[32] * z[34];
z[19] = z[19] + z[33] + z[40] + z[43];
z[19] = abb[17] * z[19];
z[40] = abb[56] * z[25];
z[16] = abb[30] + -z[16];
z[16] = abb[32] * z[16];
z[14] = (T(-3) / T(2)) * z[14] + z[16];
z[14] = z[14] * z[23];
z[16] = abb[30] * z[23];
z[16] = 4 * z[16] + -z[84];
z[16] = abb[28] * z[16];
z[23] = -abb[28] * z[58];
z[23] = z[23] + z[86];
z[23] = abb[29] * z[23];
z[14] = z[14] + z[16] + z[23];
z[14] = abb[2] * z[14];
z[16] = abb[34] * z[13];
z[23] = prod_pow(abb[28], 2) * z[12];
z[43] = -abb[29] * z[91];
z[23] = z[16] + z[23] + z[33] + z[43];
z[23] = abb[19] * z[23];
z[24] = -z[24] + z[42] + z[56];
z[24] = abb[35] * z[24];
z[33] = -abb[30] * z[34];
z[16] = -z[16] + z[33];
z[16] = abb[16] * z[16];
z[0] = abb[58] + z[0] + z[1] + z[14] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[23] + z[24] + z[28] + z[30] + z[31] + z[32] + z[36] + z[37] + z[40] + z[47] + z[52];
z[1] = -z[35] + -z[78];
z[1] = abb[5] * z[1];
z[14] = z[29] + z[61];
z[16] = -z[14] * z[70];
z[17] = abb[0] * z[29];
z[18] = 3 * z[12];
z[19] = -abb[17] * z[18];
z[12] = -abb[19] * z[12];
z[1] = z[1] + -z[7] + z[12] + z[16] + z[17] + z[19] + z[45] + -z[48] + z[66];
z[1] = abb[31] * z[1];
z[7] = z[14] * z[62];
z[7] = z[7] + z[54];
z[12] = abb[4] * z[26];
z[14] = -abb[0] * z[41];
z[12] = z[7] + z[12] + z[14] + -z[22] + -z[38] + z[39] + -z[92];
z[12] = abb[33] * z[12];
z[14] = abb[6] * z[41];
z[6] = -z[6] + z[14] + z[27] + z[64];
z[6] = abb[28] * z[6];
z[7] = -z[7] + z[67] + z[71] + -z[85];
z[7] = abb[32] * z[7];
z[14] = -z[50] + z[89];
z[14] = abb[30] * z[14];
z[16] = z[13] * z[88];
z[17] = -abb[29] * z[18];
z[16] = z[16] + z[17] + z[34];
z[16] = abb[17] * z[16];
z[13] = abb[33] * z[13];
z[13] = z[13] + -z[90];
z[13] = abb[19] * z[13];
z[1] = z[1] + z[6] + z[7] + z[12] + z[13] + z[14] + z[16] + -z[59] + -z[60];
z[1] = m1_set::bc<T>[0] * z[1];
z[6] = abb[39] * z[15];
z[7] = -abb[41] * z[11];
z[2] = abb[38] * z[2];
z[11] = abb[40] * z[25];
z[5] = abb[40] * z[5];
z[12] = abb[26] * abb[41];
z[8] = abb[39] * z[8];
z[5] = z[5] + z[8] + (T(-1) / T(2)) * z[12];
z[4] = -abb[28] * z[4];
z[8] = -abb[22] + -z[9];
z[8] = abb[31] * z[8];
z[4] = z[4] + z[8] + -z[10];
z[8] = -abb[24] + abb[25];
z[3] = abb[20] + abb[22] + z[3] + (T(1) / T(2)) * z[8];
z[3] = abb[33] * z[3];
z[3] = z[3] + (T(1) / T(2)) * z[4];
z[3] = m1_set::bc<T>[0] * z[3];
z[3] = z[3] + (T(1) / T(2)) * z[5];
z[3] = abb[52] * z[3];
z[1] = abb[42] + z[1] + z[2] + z[3] + z[6] + z[7] + z[11];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_505_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("-3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("-3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-19.251487639891229114804122255455639533884839238370546186370837468"),stof<T>("2.702791952986176229640274038496251748496755566419030395721309865")}, std::complex<T>{stof<T>("-21.606546010172162322515379700341139867996638041966289214523326148"),stof<T>("-2.702000933968601905048161264218677370994974637217526244113750991")}, std::complex<T>{stof<T>("3.2904641725129597777725651834777845479008968023777904580793849356"),stof<T>("8.6310949606228472502995409892366960969985099394394082902086153971")}, std::complex<T>{stof<T>("-0.5527624557607640950123004076538785599513755392459028678507623014"),stof<T>("8.6180817887752370110104092174451592570304689492490970882826963077")}, std::complex<T>{stof<T>("-1.929047365971231066630042088008710289906983071846621682967563806"),stof<T>("0.1299667118265977184776204054735914385079853180536135282563373817")}, std::complex<T>{stof<T>("3.544644398761003938966021834444683744572127259189781095237918004"),stof<T>("-15.116878415716311936296127438768884250961129189392489427979323107")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}, rlog(k.W[196].real()/kbase.W[196].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_505_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_505_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (-v[3] + v[5]) * (-16 + -12 * v[0] + 4 * v[1] + -7 * v[3] + -v[5] + 4 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return (abb[48] + abb[49] + -abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[4];
z[0] = abb[48] + abb[49] + -abb[50];
z[0] = abb[11] * z[0];
z[1] = -abb[28] + abb[31];
z[1] = z[0] * z[1];
z[1] = 2 * z[1];
z[2] = abb[29] * z[0];
z[2] = z[1] + z[2];
z[2] = abb[29] * z[2];
z[3] = -abb[30] * z[0];
z[1] = -z[1] + z[3];
z[1] = abb[30] * z[1];
z[0] = abb[36] * z[0];
return 2 * z[0] + z[1] + z[2];
}

}
template <typename T, typename TABB> T SpDLog_f_4_505_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[3] + v[5])) / tend;


		return (abb[48] + abb[49] + -abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[29] + -abb[30];
z[1] = abb[48] + abb[49] + -abb[50];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_505_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-14.19964576974122959598944279035209578005745742925733362181650657"),stof<T>("38.621420869721328157496920574462338387013500108456424778024073909")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W23(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), rlog(kend.W[196].real()/k.W[196].real()), f_2_4_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), T{0}};
abb[42] = SpDLog_f_4_505_W_16_Im(t, path, abb);
abb[58] = SpDLog_f_4_505_W_16_Re(t, path, abb);

                    
            return f_4_505_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_505_DLogXconstant_part(base_point<T>, kend);
	value += f_4_505_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_505_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_505_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_505_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_505_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_505_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_505_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
