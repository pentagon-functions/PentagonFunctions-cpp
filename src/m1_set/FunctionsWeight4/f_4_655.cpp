/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_655.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_655_abbreviated (const std::array<T,64>& abb) {
T z[113];
z[0] = abb[47] + abb[53];
z[1] = 3 * abb[52];
z[2] = (T(11) / T(3)) * z[0] + -z[1];
z[3] = abb[44] + -abb[51];
z[4] = abb[46] * (T(1) / T(4));
z[5] = abb[48] * (T(3) / T(4));
z[6] = abb[54] * (T(1) / T(3));
z[2] = abb[50] + abb[49] * (T(-13) / T(12)) + abb[45] * (T(-1) / T(3)) + (T(1) / T(4)) * z[2] + (T(-2) / T(3)) * z[3] + z[4] + -z[5] + -z[6];
z[2] = abb[7] * z[2];
z[7] = abb[46] * (T(1) / T(2));
z[8] = z[3] + z[7];
z[9] = abb[48] + abb[49];
z[10] = -abb[53] + z[9];
z[11] = -abb[47] + abb[52];
z[12] = z[10] + -z[11];
z[13] = (T(1) / T(2)) * z[12];
z[14] = z[8] + z[13];
z[15] = abb[12] * z[14];
z[16] = abb[55] + -abb[56];
z[17] = (T(1) / T(2)) * z[16];
z[18] = abb[19] * z[17];
z[15] = z[15] + z[18];
z[18] = abb[21] + abb[23];
z[18] = -z[16] * z[18];
z[18] = (T(1) / T(2)) * z[18];
z[19] = -z[15] + z[18];
z[20] = abb[46] * (T(3) / T(4));
z[21] = z[3] + z[20];
z[22] = 3 * abb[53];
z[23] = abb[52] + abb[47] * (T(-5) / T(3)) + -z[22];
z[23] = abb[48] * (T(-5) / T(12)) + abb[45] * (T(4) / T(3)) + abb[49] * (T(11) / T(12)) + z[6] + -z[21] + (T(1) / T(4)) * z[23];
z[23] = abb[4] * z[23];
z[24] = abb[45] + abb[48];
z[25] = 2 * abb[50];
z[6] = abb[52] * (T(-5) / T(2)) + abb[49] * (T(-1) / T(3)) + abb[53] * (T(1) / T(2)) + abb[47] * (T(7) / T(3)) + z[6] + (T(-1) / T(6)) * z[24] + z[25];
z[6] = abb[1] * z[6];
z[24] = abb[20] + abb[22];
z[26] = (T(5) / T(4)) * z[24];
z[26] = z[16] * z[26];
z[27] = abb[48] * (T(1) / T(2));
z[28] = abb[49] * (T(1) / T(2));
z[29] = -abb[45] + z[27] + -z[28];
z[30] = -z[7] + z[29];
z[31] = -abb[52] + z[0];
z[32] = (T(1) / T(2)) * z[31];
z[33] = abb[50] + z[30] + z[32];
z[34] = abb[13] * z[33];
z[35] = abb[25] * z[17];
z[36] = -z[34] + z[35];
z[37] = abb[53] + z[11];
z[30] = z[30] + (T(1) / T(2)) * z[37];
z[37] = abb[8] * z[30];
z[10] = z[3] + z[10];
z[38] = abb[2] * z[10];
z[39] = 2 * abb[48];
z[40] = abb[53] + z[39];
z[41] = 2 * abb[45];
z[42] = abb[47] + -z[40] + z[41];
z[42] = abb[46] + abb[54] * (T(-2) / T(3)) + (T(1) / T(3)) * z[42];
z[42] = abb[5] * z[42];
z[43] = abb[45] + abb[49];
z[44] = -abb[53] + z[43];
z[45] = -abb[50] + z[11];
z[44] = (T(-2) / T(3)) * z[44] + z[45];
z[44] = abb[0] * z[44];
z[46] = 3 * abb[49];
z[47] = 3 * abb[48] + z[46];
z[48] = abb[52] + z[0];
z[49] = 5 * abb[46] + -z[47] + z[48];
z[50] = -abb[54] + (T(1) / T(4)) * z[49];
z[51] = -abb[3] * z[50];
z[2] = z[2] + z[6] + -z[19] + z[23] + z[26] + z[36] + (T(5) / T(2)) * z[37] + (T(-1) / T(3)) * z[38] + z[42] + z[44] + z[51];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[6] = -abb[46] + z[12];
z[6] = abb[50] + (T(1) / T(2)) * z[6];
z[12] = abb[6] * z[6];
z[15] = -z[12] + z[15];
z[23] = 2 * abb[49];
z[26] = -z[0] + z[23];
z[42] = 3 * abb[50];
z[44] = 2 * abb[54];
z[51] = z[42] + -z[44];
z[52] = z[26] + -z[51];
z[53] = 3 * abb[45] + -abb[48] + z[52];
z[54] = abb[1] * z[53];
z[55] = abb[25] * z[16];
z[55] = (T(-3) / T(2)) * z[34] + (T(3) / T(4)) * z[55];
z[56] = -z[54] + z[55];
z[57] = 3 * abb[46];
z[58] = z[1] + z[57];
z[59] = -z[0] + -z[9] + z[58];
z[60] = abb[54] + (T(-1) / T(4)) * z[59];
z[60] = abb[5] * z[60];
z[61] = (T(-1) / T(2)) * z[3];
z[62] = abb[50] * (T(3) / T(2));
z[63] = z[41] + z[61] + -z[62];
z[64] = abb[48] + -abb[49] + -abb[54] + (T(1) / T(2)) * z[0] + -z[63];
z[64] = abb[7] * z[64];
z[65] = -abb[52] + z[3];
z[66] = abb[47] + abb[48];
z[65] = abb[54] + abb[45] * (T(-1) / T(2)) + z[28] + (T(3) / T(2)) * z[65] + z[66];
z[65] = abb[4] * z[65];
z[67] = abb[46] + abb[52];
z[68] = abb[50] + z[67];
z[69] = -abb[53] + z[68];
z[69] = -abb[54] + (T(1) / T(2)) * z[69];
z[70] = 3 * abb[9];
z[71] = z[69] * z[70];
z[72] = 3 * z[11];
z[73] = -z[42] + z[72];
z[74] = abb[49] + -abb[53];
z[75] = abb[45] + z[74];
z[76] = z[73] + -z[75];
z[77] = abb[0] * (T(1) / T(2));
z[78] = -z[76] * z[77];
z[60] = (T(-3) / T(2)) * z[15] + z[38] + z[56] + z[60] + z[64] + z[65] + -z[71] + z[78];
z[60] = abb[27] * z[60];
z[64] = z[0] + -z[1] + z[43];
z[64] = z[3] + z[44] + -z[62] + (T(1) / T(2)) * z[64];
z[64] = abb[0] * z[64];
z[65] = 2 * z[38];
z[78] = (T(3) / T(2)) * z[19] + z[65];
z[64] = z[64] + z[78];
z[5] = z[5] + z[20];
z[79] = 5 * abb[47];
z[80] = 5 * abb[53];
z[81] = z[79] + z[80];
z[82] = -z[1] + -z[81];
z[83] = 4 * abb[54];
z[61] = 4 * abb[45] + abb[50] * (T(-9) / T(2)) + abb[49] * (T(13) / T(4)) + -z[5] + -z[61] + (T(1) / T(4)) * z[82] + z[83];
z[61] = abb[7] * z[61];
z[82] = -abb[45] + abb[50];
z[84] = -abb[49] + z[82];
z[85] = -z[44] + z[67] + z[84];
z[85] = z[70] * z[85];
z[59] = -z[44] + (T(1) / T(2)) * z[59];
z[86] = abb[14] * z[59];
z[87] = (T(3) / T(2)) * z[16];
z[88] = abb[24] * z[87];
z[89] = z[86] + -z[88];
z[90] = abb[46] * (T(3) / T(2));
z[91] = abb[48] * (T(3) / T(2)) + z[90];
z[28] = z[28] + z[91];
z[92] = 9 * abb[52] + -z[81];
z[93] = -abb[45] + z[3];
z[92] = z[28] + (T(1) / T(2)) * z[92] + z[93];
z[92] = -z[44] + (T(1) / T(2)) * z[92];
z[92] = abb[4] * z[92];
z[61] = 2 * z[54] + -z[55] + z[61] + z[64] + z[85] + z[89] + z[92];
z[61] = abb[30] * z[61];
z[92] = abb[4] * z[59];
z[94] = z[89] + z[92];
z[95] = 2 * abb[7];
z[96] = z[53] * z[95];
z[97] = abb[5] * z[59];
z[34] = 3 * z[34];
z[98] = abb[25] * z[87];
z[98] = -z[34] + z[98];
z[85] = -3 * z[54] + -z[85] + -z[94] + -z[96] + -z[97] + z[98];
z[85] = abb[31] * z[85];
z[96] = abb[7] * z[59];
z[99] = z[96] + z[97];
z[100] = 2 * abb[47] + -z[1];
z[101] = abb[53] + z[44];
z[102] = z[3] + z[100] + z[101];
z[103] = abb[0] * z[102];
z[19] = 3 * z[19] + 4 * z[38];
z[10] = abb[4] * z[10];
z[10] = 2 * z[10] + z[19] + z[86] + -z[99] + z[103];
z[10] = abb[32] * z[10];
z[103] = abb[32] * z[88];
z[10] = z[10] + -z[103];
z[104] = 2 * abb[53];
z[105] = abb[47] + z[9];
z[106] = -z[51] + z[104] + -z[105];
z[107] = 2 * abb[14];
z[106] = z[106] * z[107];
z[99] = z[92] + z[99] + z[106];
z[12] = -z[12] + z[18];
z[18] = 3 * z[12];
z[68] = z[68] + -z[101];
z[101] = z[68] * z[70];
z[101] = -z[18] + z[101];
z[107] = -z[99] + -z[101];
z[107] = abb[28] * z[107];
z[60] = -z[10] + z[60] + z[61] + -z[85] + z[107];
z[60] = abb[27] * z[60];
z[61] = abb[48] * (T(1) / T(4));
z[107] = abb[49] * (T(3) / T(4));
z[4] = -abb[54] + z[4] + (T(1) / T(4)) * z[48] + z[61] + z[82] + -z[107];
z[4] = abb[7] * z[4];
z[82] = -abb[47] + z[1];
z[40] = -5 * abb[45] + z[40] + z[42] + -z[46] + z[82] + -z[83];
z[40] = abb[1] * z[40];
z[46] = -z[80] + z[82];
z[83] = z[20] + z[107];
z[61] = abb[54] + z[61];
z[107] = abb[45] + (T(1) / T(4)) * z[46] + -z[61] + z[83];
z[108] = abb[4] * z[107];
z[108] = (T(3) / T(2)) * z[12] + -z[108];
z[109] = abb[20] * z[87];
z[87] = abb[22] * z[87];
z[109] = 3 * z[37] + z[87] + z[109];
z[110] = z[89] + z[109];
z[107] = -abb[5] * z[107];
z[111] = abb[50] + abb[52] + abb[53];
z[43] = abb[54] + -z[7] + z[43] + (T(-1) / T(2)) * z[111];
z[43] = z[43] * z[70];
z[4] = 3 * z[4] + z[40] + z[43] + z[107] + z[108] + -z[110];
z[4] = abb[31] * z[4];
z[40] = abb[49] * (T(3) / T(2));
z[27] = -z[27] + z[40] + z[41];
z[43] = -z[44] + z[90];
z[107] = z[27] + z[43] + (T(1) / T(2)) * z[46];
z[111] = abb[4] + abb[5];
z[107] = z[107] * z[111];
z[111] = 5 * z[9];
z[112] = 7 * abb[53];
z[58] = z[58] + -z[79] + -z[111] + z[112];
z[58] = -6 * abb[50] + z[44] + (T(1) / T(2)) * z[58];
z[58] = abb[14] * z[58];
z[58] = z[58] + -z[88] + z[101] + z[107] + z[109];
z[58] = abb[28] * z[58];
z[88] = -abb[45] + abb[48] + -abb[53] + -z[44];
z[100] = z[88] + -z[100];
z[100] = abb[1] * z[100];
z[101] = abb[28] * z[100];
z[4] = z[4] + z[58] + -z[101];
z[4] = abb[31] * z[4];
z[26] = z[26] + z[39] + z[44] + -z[57];
z[39] = abb[5] * z[26];
z[52] = z[3] + z[41] + z[52];
z[57] = abb[0] * z[52];
z[58] = -abb[4] + z[95];
z[95] = abb[48] + z[93];
z[58] = z[58] * z[95];
z[57] = -z[38] + z[39] + -z[54] + z[57] + z[58];
z[57] = abb[30] * z[57];
z[26] = abb[7] * z[26];
z[26] = z[26] + z[39];
z[39] = -z[44] + (T(1) / T(2)) * z[49];
z[49] = abb[3] * z[39];
z[26] = 2 * z[26] + 3 * z[49];
z[58] = -z[26] + -z[86] + -z[92];
z[58] = abb[32] * z[58];
z[26] = z[26] + z[94];
z[26] = abb[28] * z[26];
z[26] = z[26] + z[57] + z[58] + z[85] + z[103];
z[26] = abb[30] * z[26];
z[40] = abb[48] * (T(5) / T(2)) + (T(-3) / T(2)) * z[31] + z[40] + -z[90] + z[93];
z[57] = abb[4] * (T(1) / T(2));
z[40] = z[40] * z[57];
z[58] = abb[48] * (T(5) / T(4)) + (T(3) / T(4)) * z[31] + -z[63] + -z[83];
z[58] = abb[7] * z[58];
z[40] = z[40] + z[56] + z[58] + z[64] + -z[97];
z[40] = abb[30] * z[40];
z[64] = 2 * z[75];
z[83] = abb[4] * z[64];
z[64] = abb[5] * z[64];
z[83] = z[64] + z[83] + -z[96];
z[85] = z[83] + z[110];
z[90] = abb[28] * z[85];
z[10] = -z[10] + z[40] + z[90] + -z[101];
z[0] = z[0] + z[1];
z[0] = abb[49] * (T(-5) / T(4)) + (T(1) / T(4)) * z[0] + z[5] + -z[44] + -z[63];
z[0] = abb[7] * z[0];
z[5] = z[73] + z[75];
z[5] = z[5] * z[77];
z[5] = z[5] + -z[78];
z[40] = z[72] + z[80];
z[63] = -3 * z[3];
z[40] = -abb[45] + abb[49] * (T(-5) / T(2)) + (T(1) / T(2)) * z[40] + z[63] + -z[91];
z[40] = z[40] * z[57];
z[0] = z[0] + z[5] + z[40] + z[56] + -z[89];
z[0] = abb[27] * z[0];
z[40] = z[85] + -z[100];
z[40] = abb[31] * z[40];
z[56] = -abb[0] + abb[1] + -abb[5];
z[56] = z[56] * z[75];
z[77] = -abb[7] * z[95];
z[56] = z[38] + z[56] + z[77];
z[56] = abb[29] * z[56];
z[0] = z[0] + -z[10] + z[40] + z[56];
z[0] = abb[29] * z[0];
z[40] = abb[5] * z[30];
z[33] = abb[7] * z[33];
z[56] = -z[9] + z[31];
z[77] = abb[46] + z[56];
z[57] = z[57] * z[77];
z[33] = -z[33] + z[40] + z[57];
z[31] = z[31] + z[84];
z[31] = abb[1] * z[31];
z[40] = abb[20] * z[17];
z[37] = z[37] + z[40];
z[40] = abb[22] * z[17];
z[40] = z[37] + z[40];
z[57] = z[31] + -z[33] + z[36] + z[40];
z[57] = abb[59] * z[57];
z[14] = abb[4] * z[14];
z[78] = abb[0] * z[45];
z[14] = -z[14] + z[15] + -z[38] + z[78];
z[6] = abb[14] * z[6];
z[78] = z[6] + z[14];
z[78] = abb[57] * z[78];
z[67] = z[22] + -z[67] + -z[105];
z[25] = z[25] + -z[44];
z[67] = -z[25] + (T(1) / T(2)) * z[67];
z[67] = abb[14] * z[67];
z[67] = -z[12] + z[67];
z[80] = abb[5] + abb[7];
z[39] = z[39] * z[80];
z[39] = z[39] + z[67];
z[84] = abb[34] * z[39];
z[85] = prod_pow(abb[32], 2);
z[90] = abb[34] + z[85];
z[91] = abb[35] + z[90];
z[91] = z[49] * z[91];
z[57] = z[57] + z[78] + -z[84] + z[91];
z[78] = -z[82] + -z[112];
z[20] = abb[49] * (T(9) / T(4)) + -z[20] + z[41] + z[61] + (T(1) / T(4)) * z[78];
z[20] = abb[5] * z[20];
z[61] = abb[7] * z[50];
z[20] = z[20] + -3 * z[61] + -z[71] + -z[106] + z[108];
z[20] = abb[28] * z[20];
z[71] = abb[9] * z[68];
z[78] = abb[24] * z[17];
z[39] = z[39] + -z[49] + z[71] + z[78];
z[39] = abb[32] * z[39];
z[20] = z[20] + 3 * z[39];
z[20] = abb[28] * z[20];
z[30] = abb[4] * z[30];
z[39] = abb[5] * z[77];
z[6] = z[6] + z[78];
z[45] = abb[1] * z[45];
z[30] = -z[6] + -z[12] + -z[30] + (T(-1) / T(2)) * z[39] + z[40] + -z[45];
z[30] = 3 * z[30];
z[39] = abb[58] * z[30];
z[40] = -abb[5] * z[50];
z[12] = (T(1) / T(2)) * z[12] + z[40] + -z[61];
z[40] = z[46] + z[47];
z[21] = -abb[54] + z[21] + (T(1) / T(4)) * z[40];
z[21] = abb[4] * z[21];
z[12] = 3 * z[12] + z[21] + -z[38] + z[86];
z[12] = z[12] * z[85];
z[21] = z[18] + -z[99];
z[21] = abb[36] * z[21];
z[1] = -9 * abb[46] + z[1] + 7 * z[9] + -z[81];
z[1] = (T(1) / T(2)) * z[1] + z[44];
z[1] = z[1] * z[80];
z[9] = z[22] + z[82] + -z[111];
z[9] = -2 * z[3] + (T(1) / T(2)) * z[9] + z[43];
z[9] = abb[4] * z[9];
z[1] = z[1] + z[9] + -z[19];
z[1] = abb[35] * z[1];
z[9] = z[7] + z[25] + -z[27] + (T(1) / T(2)) * z[48];
z[9] = abb[7] * z[9];
z[19] = abb[9] * z[75];
z[9] = z[9] + z[19] + z[36] + z[67] + z[78];
z[19] = 3 * abb[33];
z[9] = z[9] * z[19];
z[22] = -abb[16] + abb[17] + abb[18];
z[7] = z[7] * z[22];
z[13] = z[3] + z[13];
z[13] = abb[17] * z[13];
z[22] = z[29] + z[32];
z[25] = z[3] + z[22];
z[25] = abb[16] * z[25];
z[7] = z[7] + z[13] + -z[25];
z[13] = -abb[50] + -z[22];
z[13] = abb[18] * z[13];
z[17] = abb[26] * z[17];
z[13] = z[7] + z[13] + z[17];
z[17] = abb[50] + -z[8] + (T(1) / T(2)) * z[56];
z[25] = abb[15] * (T(3) / T(2));
z[17] = z[17] * z[25];
z[13] = (T(3) / T(2)) * z[13] + z[17];
z[13] = abb[60] * z[13];
z[25] = -abb[63] * z[59];
z[27] = -abb[35] * z[102];
z[29] = -abb[53] + z[51];
z[3] = abb[47] + -z[3] + z[29];
z[3] = z[3] * z[85];
z[3] = z[3] + z[27];
z[3] = abb[0] * z[3];
z[27] = -abb[34] + -abb[36];
z[27] = z[27] * z[68];
z[32] = -z[69] * z[85];
z[27] = z[27] + z[32];
z[27] = z[27] * z[70];
z[19] = -z[19] * z[53];
z[29] = abb[45] + -z[29] + -z[66];
z[29] = prod_pow(abb[28], 2) * z[29];
z[19] = z[19] + z[29];
z[19] = abb[1] * z[19];
z[29] = abb[57] + -z[90];
z[29] = abb[24] * z[16] * z[29];
z[0] = abb[61] + abb[62] + z[0] + z[1] + z[2] + z[3] + z[4] + z[9] + z[12] + z[13] + z[19] + z[20] + z[21] + z[25] + z[26] + z[27] + (T(3) / T(2)) * z[29] + z[39] + 3 * z[57] + z[60];
z[1] = z[70] * z[75];
z[1] = z[1] + -z[89];
z[2] = -abb[46] + abb[48] + z[11] + -z[41] + -z[74];
z[2] = abb[8] * z[2];
z[3] = -6 * abb[52] + z[42] + z[79] + -z[88];
z[3] = abb[1] * z[3];
z[4] = 3 * z[24];
z[4] = z[4] * z[16];
z[2] = -z[1] + 3 * z[2] + z[3] + z[4] + -z[18] + z[83] + z[98];
z[2] = abb[31] * z[2];
z[3] = -abb[7] * z[52];
z[4] = abb[53] + z[72];
z[4] = abb[45] + (T(1) / T(2)) * z[4] + -z[28] + z[63];
z[4] = abb[4] * z[4];
z[9] = abb[0] * z[76];
z[1] = z[1] + z[3] + z[4] + z[9] + 3 * z[15] + -z[54] + -z[65];
z[1] = abb[27] * z[1];
z[3] = z[23] + z[41] + z[73] + -z[104];
z[3] = abb[1] * z[3];
z[4] = z[8] + z[22];
z[4] = abb[4] * z[4];
z[3] = z[3] + (T(3) / T(2)) * z[4] + -z[5] + -z[55] + -z[58] + -z[64] + -z[109];
z[3] = abb[29] * z[3];
z[1] = z[1] + z[2] + z[3] + -z[10];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[38] * z[30];
z[3] = -abb[18] * z[22];
z[3] = z[3] + z[7];
z[4] = -abb[18] * z[62];
z[5] = abb[26] * z[16];
z[3] = (T(3) / T(2)) * z[3] + z[4] + (T(3) / T(4)) * z[5] + z[17];
z[3] = abb[40] * z[3];
z[4] = z[31] + -z[33] + z[35] + z[37];
z[4] = 3 * z[4] + -z[34] + z[87];
z[4] = abb[39] * z[4];
z[5] = z[6] + z[14];
z[5] = abb[37] * z[5];
z[6] = -abb[43] * z[59];
z[1] = abb[41] + abb[42] + z[1] + z[2] + z[3] + z[4] + 3 * z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_655_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.9199712708610032646570607360206978943387699704876487992275079596"),stof<T>("-2.3683656031641206184443785616499261670486199736651258493215837874")}, std::complex<T>{stof<T>("-1.926546076142315960506474136442282937262294152631277069540486659"),stof<T>("-15.797566346568351308716995489891197084094153174661371118569168943")}, std::complex<T>{stof<T>("3.5756564364446030331447494388019930518598719780176358625104872301"),stof<T>("-3.2101481916672582729114674947237806255119606920142127017833134406")}, std::complex<T>{stof<T>("2.728986533072813935965881254948291681419501868887366728433945348"),stof<T>("13.2476544035186577448289863716499810925997384570373333673487691944")}, std::complex<T>{stof<T>("0.8090152622118106713088205189275937870807318983997179292064373884"),stof<T>("10.879288800354537126384607810000054925551118483372207518027185407")}, std::complex<T>{stof<T>("7.1070834264479149445700378122562934774365815631610922498378912673"),stof<T>("7.4875942688017059080295097586849844755933630473990829143450560048")}, std::complex<T>{stof<T>("-2.7732159795141050576853423202959843077026642617615462036170285409"),stof<T>("0.6602362486175647090234583764825646340175459743901749505629136915")}, std::complex<T>{stof<T>("-1.6114557191423086467682276374336025312379396146558075880998960776"),stof<T>("-8.329376857304843562496598691758838934056703765748169766806785658")}, std::complex<T>{stof<T>("-1.6048809138609959509188142370120174883144154325121793177869173784"),stof<T>("5.0998238860993871277760182364824319829888294352480755024407994981")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(abs(k.W[79])) - rlog(abs(kbase.W[79])), C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_655_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_655_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (8 + -7 * v[0] + 3 * v[1] + 5 * v[2] + v[3] + -4 * v[4] + -2 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -5 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (-(1 + m1_set::bc<T>[1]) * (v[0] + -1 * v[1] + v[2] + v[3] + -1 * v[5])) / tend;


		return (abb[44] + 3 * abb[46] + abb[47] + -abb[48] + -abb[49] + -abb[51] + -2 * abb[54]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[27] + abb[29];
z[1] = -abb[32] + z[0];
z[1] = abb[32] * z[1];
z[0] = -abb[30] + z[0];
z[0] = abb[30] * z[0];
z[0] = -abb[35] + -z[0] + z[1];
z[1] = abb[44] + 3 * abb[46] + abb[47] + -abb[48] + -abb[49] + -abb[51] + -2 * abb[54];
return abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_655_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(8)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[44] + 3 * abb[46] + abb[47] + -abb[48] + -abb[49] + -abb[51] + -2 * abb[54]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[44] + -3 * abb[46] + -abb[47] + abb[48] + abb[49] + abb[51] + 2 * abb[54];
z[1] = abb[30] + -abb[32];
return abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_655_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[46] + -abb[48] + -abb[49] + -abb[50] + abb[53]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[32], 2);
z[1] = -prod_pow(abb[28], 2);
z[0] = z[0] + z[1];
z[1] = -abb[46] + abb[48] + abb[49] + abb[50] + -abb[53];
return 3 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_655_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_655_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2.7324371176930167648795330290797422262601471755726559175486423585"),stof<T>("-3.2220739863461210789907795984768614188744419325826183336426377993")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19, 79});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,64> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(abs(kend.W[79])) - rlog(abs(k.W[79])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[41] = SpDLog_f_4_655_W_17_Im(t, path, abb);
abb[42] = SpDLog_f_4_655_W_20_Im(t, path, abb);
abb[43] = SpDLogQ_W_80(k,dl,dlr).imag();
abb[61] = SpDLog_f_4_655_W_17_Re(t, path, abb);
abb[62] = SpDLog_f_4_655_W_20_Re(t, path, abb);
abb[63] = SpDLogQ_W_80(k,dl,dlr).real();

                    
            return f_4_655_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_655_DLogXconstant_part(base_point<T>, kend);
	value += f_4_655_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_655_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_655_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_655_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_655_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_655_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_655_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
