/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_458.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_458_abbreviated (const std::array<T,69>& abb) {
T z[122];
z[0] = abb[52] + abb[54];
z[1] = abb[33] * z[0];
z[2] = abb[32] * z[0];
z[3] = z[1] + -z[2];
z[4] = abb[32] + -abb[33];
z[5] = -abb[30] + z[4];
z[6] = abb[29] + z[5];
z[7] = abb[58] * z[6];
z[8] = abb[29] * z[0];
z[9] = -abb[30] * z[0];
z[9] = -z[3] + z[7] + z[8] + z[9];
z[9] = abb[10] * z[9];
z[10] = abb[30] * (T(1) / T(2));
z[11] = abb[47] + -abb[48] + abb[49] + -abb[50];
z[12] = z[10] * z[11];
z[13] = abb[33] * z[11];
z[14] = abb[32] * z[11];
z[15] = z[13] + -z[14];
z[16] = z[12] + z[15];
z[17] = abb[34] * (T(1) / T(2));
z[18] = z[11] * z[17];
z[19] = -z[4] + z[10];
z[20] = -z[17] + z[19];
z[21] = -abb[46] * z[20];
z[21] = -z[16] + z[18] + z[21];
z[21] = abb[27] * z[21];
z[22] = -abb[56] + z[0];
z[23] = z[10] * z[22];
z[24] = abb[56] * z[4];
z[24] = z[3] + z[24];
z[25] = z[23] + z[24];
z[26] = abb[56] + z[0];
z[27] = z[17] * z[26];
z[28] = -abb[31] * z[0];
z[28] = z[25] + z[27] + z[28];
z[28] = abb[16] * z[28];
z[29] = 5 * abb[32] + abb[33];
z[29] = abb[56] * z[29];
z[29] = z[1] + -3 * z[2] + z[29];
z[30] = abb[29] + -abb[34];
z[31] = 3 * abb[56] + -z[0];
z[32] = -z[30] * z[31];
z[29] = (T(1) / T(2)) * z[29] + z[32];
z[32] = abb[34] * abb[58];
z[31] = -abb[58] + (T(-1) / T(2)) * z[31];
z[31] = abb[31] * z[31];
z[29] = (T(1) / T(2)) * z[29] + z[31] + z[32];
z[29] = abb[7] * z[29];
z[31] = abb[33] * abb[56];
z[33] = abb[29] + -abb[32];
z[34] = abb[33] + -z[33];
z[34] = abb[58] * z[34];
z[35] = abb[56] + abb[58];
z[36] = -abb[31] * z[35];
z[34] = z[31] + z[34] + z[36];
z[34] = abb[1] * z[34];
z[36] = (T(1) / T(2)) * z[4];
z[37] = -abb[30] + z[36];
z[38] = abb[29] + z[37];
z[39] = -abb[56] * z[38];
z[7] = -z[7] + z[39];
z[7] = abb[0] * z[7];
z[39] = z[3] + z[8];
z[40] = abb[58] + z[0];
z[41] = abb[31] * z[40];
z[41] = -z[39] + z[41];
z[42] = -abb[29] + z[4];
z[43] = abb[58] * z[42];
z[43] = z[41] + z[43];
z[43] = abb[13] * z[43];
z[44] = abb[33] * (T(1) / T(2));
z[45] = abb[18] * z[44];
z[46] = abb[18] * (T(1) / T(2));
z[47] = abb[17] + z[46];
z[48] = abb[32] * z[47];
z[45] = z[45] + z[48];
z[48] = abb[17] + abb[18];
z[49] = -abb[29] * z[48];
z[49] = z[45] + z[49];
z[50] = abb[59] * (T(1) / T(2));
z[49] = z[49] * z[50];
z[51] = abb[32] * abb[56];
z[52] = z[2] + z[51];
z[53] = -abb[31] * z[26];
z[53] = z[52] + z[53];
z[54] = abb[6] * (T(1) / T(2));
z[53] = z[53] * z[54];
z[55] = abb[0] * z[36];
z[56] = -abb[7] * z[33];
z[55] = z[55] + z[56];
z[56] = abb[1] * z[33];
z[55] = (T(1) / T(2)) * z[55] + z[56];
z[55] = abb[51] * z[55];
z[7] = z[7] + z[9] + z[21] + z[28] + z[29] + z[34] + z[43] + z[49] + z[53] + z[55];
z[9] = -abb[32] + 3 * abb[33];
z[9] = abb[56] * z[9];
z[9] = -3 * z[1] + z[2] + z[9];
z[21] = abb[29] * z[22];
z[28] = abb[30] * z[22];
z[9] = (T(1) / T(2)) * z[9] + z[21] + -z[28];
z[29] = (T(1) / T(2)) * z[22];
z[34] = -abb[58] + z[29];
z[43] = abb[31] * (T(1) / T(2));
z[49] = z[34] * z[43];
z[53] = z[17] + -z[33];
z[53] = abb[58] * z[53];
z[55] = 3 * abb[32];
z[57] = -abb[33] + z[55];
z[58] = (T(1) / T(4)) * z[57];
z[59] = -abb[34] + abb[29] * (T(3) / T(2)) + -z[58];
z[59] = abb[51] * z[59];
z[9] = (T(1) / T(4)) * z[9] + z[49] + z[53] + (T(1) / T(2)) * z[59];
z[9] = abb[4] * z[9];
z[7] = (T(1) / T(2)) * z[7] + z[9];
z[1] = -z[1] + z[31];
z[9] = -abb[31] * z[22];
z[9] = -z[1] + z[9];
z[53] = abb[5] * (T(1) / T(16));
z[9] = z[9] * z[53];
z[7] = (T(1) / T(4)) * z[7] + z[9];
z[7] = m1_set::bc<T>[0] * z[7];
z[9] = abb[46] + z[11];
z[59] = abb[27] * z[9];
z[60] = (T(1) / T(2)) * z[59];
z[61] = 3 * z[0];
z[62] = -abb[56] + z[61];
z[63] = abb[16] * z[62];
z[64] = abb[13] * z[40];
z[34] = abb[8] * z[34];
z[65] = (T(1) / T(2)) * z[26];
z[66] = abb[58] + z[65];
z[67] = -abb[7] * z[66];
z[68] = -abb[1] * z[35];
z[69] = abb[4] * z[40];
z[70] = -abb[5] * z[29];
z[67] = z[34] + z[60] + (T(-1) / T(2)) * z[63] + 3 * z[64] + z[67] + z[68] + z[69] + z[70];
z[67] = abb[40] * z[67];
z[68] = abb[37] + abb[39];
z[69] = z[26] * z[68];
z[70] = abb[38] * z[40];
z[71] = abb[37] * abb[51];
z[69] = (T(1) / T(2)) * z[69] + z[70] + z[71];
z[69] = abb[4] * z[69];
z[64] = z[34] + z[64];
z[70] = -abb[7] * z[29];
z[71] = abb[4] * z[66];
z[70] = z[64] + z[70] + z[71];
z[70] = abb[41] * z[70];
z[71] = abb[39] * z[22];
z[72] = abb[37] * z[22];
z[73] = z[71] + z[72];
z[74] = abb[58] * z[68];
z[75] = (T(1) / T(2)) * z[73] + -z[74];
z[75] = abb[8] * z[75];
z[67] = z[67] + z[69] + z[70] + z[75];
z[59] = -z[59] + z[63];
z[63] = abb[0] * (T(1) / T(2));
z[69] = z[35] * z[63];
z[70] = abb[7] * (T(1) / T(2));
z[66] = z[66] * z[70];
z[75] = -abb[10] + abb[2] * (T(1) / T(2));
z[76] = abb[58] * z[75];
z[77] = abb[10] * z[0];
z[59] = (T(-1) / T(4)) * z[59] + (T(1) / T(2)) * z[64] + -z[66] + -z[69] + -z[76] + z[77];
z[66] = abb[38] * z[59];
z[69] = -abb[58] + z[22];
z[76] = z[43] * z[69];
z[78] = abb[29] * abb[56];
z[79] = abb[56] + (T(-1) / T(2)) * z[0];
z[79] = abb[34] * z[79];
z[80] = abb[58] * z[17];
z[81] = abb[32] + abb[34];
z[82] = -abb[29] + (T(1) / T(2)) * z[81];
z[82] = abb[51] * z[82];
z[76] = (T(1) / T(2)) * z[51] + z[76] + -z[78] + z[79] + z[80] + z[82];
z[76] = m1_set::bc<T>[0] * z[76];
z[79] = -abb[51] + -z[0];
z[79] = abb[37] * z[79];
z[71] = -z[71] + z[74] + z[79];
z[74] = abb[31] + -abb[34];
z[74] = m1_set::bc<T>[0] * z[74];
z[74] = -abb[41] + -z[68] + z[74];
z[79] = abb[53] + abb[55];
z[80] = (T(1) / T(2)) * z[79];
z[74] = z[74] * z[80];
z[80] = abb[41] * (T(1) / T(2));
z[82] = -z[69] * z[80];
z[71] = (T(1) / T(2)) * z[71] + z[74] + z[76] + z[82];
z[71] = abb[9] * z[71];
z[74] = abb[4] * (T(1) / T(2));
z[75] = z[63] + z[70] + -z[74] + z[75];
z[76] = -abb[38] * z[75];
z[82] = -abb[29] + abb[33];
z[82] = z[70] * z[82];
z[83] = abb[29] * (T(1) / T(2));
z[84] = -abb[30] + z[83];
z[85] = z[58] + z[84];
z[85] = abb[4] * z[85];
z[6] = abb[10] * z[6];
z[86] = (T(3) / T(4)) * z[4];
z[87] = -abb[29] + abb[30] + -z[86];
z[87] = abb[0] * z[87];
z[6] = z[6] + z[82] + z[85] + z[87];
z[6] = m1_set::bc<T>[0] * z[6];
z[82] = abb[42] * z[47];
z[6] = (T(1) / T(2)) * z[6] + z[76] + (T(-1) / T(4)) * z[82];
z[6] = abb[57] * z[6];
z[6] = z[6] + z[66] + z[71];
z[36] = abb[34] + z[36];
z[66] = -abb[31] + z[36];
z[66] = z[66] * z[70];
z[71] = abb[31] + -abb[32];
z[71] = z[54] * z[71];
z[20] = abb[16] * z[20];
z[76] = abb[0] * z[4];
z[20] = -z[20] + -z[56] + z[66] + z[71] + (T(-1) / T(4)) * z[76];
z[56] = abb[31] + -abb[33];
z[66] = abb[5] * (T(1) / T(4));
z[56] = z[56] * z[66];
z[55] = abb[33] + z[55];
z[71] = abb[30] + z[55];
z[71] = abb[34] + (T(1) / T(2)) * z[71];
z[71] = -abb[29] + abb[31] * (T(-3) / T(4)) + (T(1) / T(2)) * z[71];
z[71] = abb[4] * z[71];
z[20] = (T(1) / T(2)) * z[20] + -z[56] + z[71];
z[20] = m1_set::bc<T>[0] * z[20];
z[56] = abb[8] + abb[16];
z[71] = abb[4] + z[56];
z[68] = z[68] * z[71];
z[71] = abb[7] + -abb[8];
z[76] = abb[16] + z[71];
z[82] = abb[38] * z[76];
z[85] = abb[6] * abb[39];
z[68] = abb[45] + -z[68] + z[82] + z[85];
z[82] = m1_set::bc<T>[0] * z[38];
z[82] = abb[38] + z[82];
z[87] = abb[15] * (T(1) / T(4));
z[82] = z[82] * z[87];
z[88] = abb[17] + -abb[20];
z[89] = abb[19] + z[88];
z[90] = abb[42] * (T(1) / T(8));
z[89] = z[89] * z[90];
z[90] = -abb[5] + z[76];
z[91] = abb[40] * (T(1) / T(4));
z[90] = z[90] * z[91];
z[91] = abb[4] + z[71];
z[92] = abb[41] * (T(1) / T(4));
z[91] = z[91] * z[92];
z[20] = z[20] + (T(1) / T(4)) * z[68] + -z[82] + z[89] + z[90] + z[91];
z[68] = (T(1) / T(4)) * z[79];
z[20] = -z[20] * z[68];
z[57] = z[30] + (T(-1) / T(2)) * z[57];
z[57] = abb[46] * z[57];
z[82] = -z[13] + 3 * z[14];
z[89] = abb[31] * z[9];
z[90] = abb[29] * z[11];
z[91] = abb[34] * z[11];
z[57] = -z[57] + (T(1) / T(2)) * z[82] + -z[89] + -z[90] + z[91];
z[57] = m1_set::bc<T>[0] * z[57];
z[82] = abb[39] * z[9];
z[89] = abb[37] * z[9];
z[82] = z[82] + z[89];
z[57] = z[57] + -z[82];
z[92] = abb[23] * (T(1) / T(16));
z[93] = abb[25] * (T(-1) / T(16)) + -z[92];
z[57] = z[57] * z[93];
z[93] = abb[20] + z[48];
z[93] = z[0] * z[93];
z[94] = -abb[18] + z[88];
z[94] = abb[56] * z[94];
z[95] = abb[28] * z[9];
z[93] = -z[93] + -z[94] + z[95];
z[94] = -abb[0] + abb[4];
z[94] = abb[59] * z[94];
z[95] = abb[18] * abb[51];
z[96] = -abb[19] * z[22];
z[94] = z[93] + z[94] + z[95] + z[96];
z[94] = abb[42] * z[94];
z[95] = abb[34] * z[22];
z[96] = z[28] + z[95];
z[97] = -abb[31] * abb[58];
z[96] = z[21] + z[32] + (T(-1) / T(2)) * z[96] + z[97];
z[96] = m1_set::bc<T>[0] * z[96];
z[97] = -abb[39] + abb[40];
z[98] = abb[30] + 3 * abb[34];
z[98] = -abb[29] + -abb[31] + (T(1) / T(2)) * z[98];
z[98] = m1_set::bc<T>[0] * z[98];
z[80] = z[80] + (T(-1) / T(2)) * z[97] + z[98];
z[80] = -z[79] * z[80];
z[97] = -abb[39] + -abb[41];
z[97] = z[29] * z[97];
z[98] = abb[58] + z[29];
z[99] = -abb[40] * z[98];
z[80] = z[80] + z[96] + z[97] + z[99];
z[96] = abb[3] * (T(1) / T(8));
z[80] = z[80] * z[96];
z[73] = abb[16] * z[73];
z[85] = z[26] * z[85];
z[73] = z[73] + -z[85];
z[85] = abb[38] + abb[40] + abb[41];
z[85] = z[9] * z[85];
z[85] = z[82] + z[85];
z[97] = abb[22] + abb[24];
z[99] = (T(1) / T(16)) * z[97];
z[85] = z[85] * z[99];
z[99] = (T(1) / T(2)) * z[15];
z[100] = abb[30] * z[11];
z[101] = z[99] + z[100];
z[38] = abb[46] * z[38];
z[38] = z[38] + z[90] + -z[101];
z[38] = m1_set::bc<T>[0] * z[38];
z[102] = abb[38] * z[9];
z[38] = z[38] + z[102];
z[102] = abb[26] * (T(1) / T(16));
z[38] = z[38] * z[102];
z[103] = z[21] + -z[95];
z[104] = (T(1) / T(2)) * z[24];
z[105] = -z[103] + -z[104];
z[105] = m1_set::bc<T>[0] * z[105];
z[106] = -abb[29] + z[36];
z[107] = m1_set::bc<T>[0] * z[106];
z[107] = -abb[37] + z[107];
z[107] = z[79] * z[107];
z[72] = -z[72] + z[105] + z[107];
z[105] = abb[14] * (T(1) / T(16));
z[72] = z[72] * z[105];
z[99] = -z[91] + z[99];
z[106] = -abb[46] * z[106];
z[106] = z[90] + z[99] + z[106];
z[106] = m1_set::bc<T>[0] * z[106];
z[89] = z[89] + z[106];
z[106] = abb[21] * (T(1) / T(16));
z[89] = z[89] * z[106];
z[82] = (T(-1) / T(16)) * z[82];
z[82] = abb[27] * z[82];
z[107] = z[28] + z[104];
z[21] = -z[21] + z[107];
z[21] = m1_set::bc<T>[0] * z[21];
z[108] = -abb[38] * z[22];
z[21] = z[21] + z[108];
z[108] = abb[15] * (T(1) / T(16));
z[21] = z[21] * z[108];
z[109] = (T(1) / T(16)) * z[35];
z[109] = abb[45] * z[109];
z[6] = abb[66] + abb[67] + (T(1) / T(4)) * z[6] + z[7] + z[20] + z[21] + z[38] + z[57] + (T(1) / T(8)) * z[67] + z[72] + (T(1) / T(16)) * z[73] + z[80] + z[82] + z[85] + z[89] + (T(1) / T(32)) * z[94] + z[109];
z[7] = abb[34] * z[29];
z[20] = z[1] + z[7];
z[20] = abb[34] * z[20];
z[21] = z[2] * z[44];
z[38] = abb[30] * z[1];
z[57] = abb[35] * z[22];
z[38] = z[38] + z[57];
z[67] = abb[64] * z[0];
z[72] = abb[32] * abb[33];
z[73] = (T(1) / T(2)) * z[72];
z[80] = -abb[64] + z[73];
z[80] = abb[56] * z[80];
z[80] = -z[20] + -z[21] + -z[38] + -z[67] + z[80];
z[82] = -abb[29] * z[26];
z[82] = z[1] + z[82];
z[85] = z[0] * z[43];
z[89] = -abb[58] * z[33];
z[82] = (T(1) / T(2)) * z[82] + z[85] + z[89];
z[82] = abb[31] * z[82];
z[85] = -abb[34] + abb[29] * (T(3) / T(4));
z[89] = -z[58] + z[85];
z[89] = abb[29] * z[89];
z[94] = abb[32] * abb[34];
z[109] = -abb[60] + z[94];
z[89] = (T(-1) / T(4)) * z[72] + z[89] + z[109];
z[89] = abb[51] * z[89];
z[8] = z[8] + -z[107];
z[8] = z[8] * z[83];
z[110] = -abb[32] + z[10];
z[111] = abb[30] * z[110];
z[111] = abb[35] + z[111];
z[112] = -z[94] + z[111];
z[113] = -abb[63] + -abb[64] + z[112];
z[114] = -abb[34] + z[83];
z[115] = abb[32] + -z[114];
z[115] = abb[29] * z[115];
z[115] = z[113] + z[115];
z[115] = abb[58] * z[115];
z[116] = abb[62] * (T(1) / T(2));
z[117] = -z[26] * z[116];
z[118] = -abb[63] * z[0];
z[65] = -abb[60] * z[65];
z[119] = abb[65] * (T(1) / T(4));
z[120] = -abb[59] * z[119];
z[121] = -abb[36] * z[29];
z[8] = z[8] + z[65] + (T(1) / T(2)) * z[80] + z[82] + z[89] + z[115] + z[117] + z[118] + z[120] + z[121];
z[8] = abb[4] * z[8];
z[65] = -z[2] + z[51];
z[23] = -z[23] + -z[65];
z[23] = abb[30] * z[23];
z[55] = abb[56] * z[55];
z[55] = z[3] + z[55];
z[55] = (T(1) / T(2)) * z[55] + -z[78] + -z[95];
z[55] = abb[29] * z[55];
z[78] = abb[34] * abb[56];
z[80] = -z[65] + -z[78];
z[80] = abb[34] * z[80];
z[82] = -abb[64] + -z[73];
z[82] = abb[56] * z[82];
z[89] = abb[63] * z[26];
z[21] = -z[21] + z[23] + z[55] + -z[57] + z[67] + z[80] + z[82] + z[89];
z[23] = z[22] * z[83];
z[32] = z[23] + z[32];
z[55] = z[35] * z[43];
z[57] = z[32] + -z[55] + (T(1) / T(2)) * z[65] + z[78];
z[57] = abb[31] * z[57];
z[80] = prod_pow(abb[34], 2);
z[80] = (T(1) / T(2)) * z[80];
z[82] = -abb[63] + z[80];
z[89] = -abb[58] * z[82];
z[21] = (T(1) / T(2)) * z[21] + z[57] + z[89];
z[21] = abb[7] * z[21];
z[16] = abb[30] * z[16];
z[18] = z[15] + -z[18];
z[18] = abb[34] * z[18];
z[57] = -z[15] + z[91] + -z[100];
z[57] = abb[29] * z[57];
z[89] = z[4] + z[17];
z[89] = abb[34] * z[89];
z[19] = abb[30] * z[19];
z[91] = abb[34] + z[5];
z[91] = abb[29] * z[91];
z[89] = abb[63] + -z[19] + z[89] + -z[91];
z[91] = -abb[46] * z[89];
z[100] = -abb[63] * z[11];
z[16] = z[16] + z[18] + z[57] + z[91] + z[100];
z[18] = abb[46] * z[4];
z[15] = -z[15] + z[18];
z[15] = z[15] * z[43];
z[15] = z[15] + (T(1) / T(2)) * z[16];
z[15] = abb[27] * z[15];
z[16] = -abb[30] * z[25];
z[18] = -z[24] + -z[27];
z[18] = abb[34] * z[18];
z[24] = z[24] + z[28] + -z[95];
z[24] = abb[29] * z[24];
z[25] = abb[63] * z[62];
z[16] = z[16] + z[18] + z[24] + z[25];
z[18] = abb[34] + -z[43];
z[18] = z[0] * z[18];
z[18] = z[18] + z[104];
z[18] = abb[31] * z[18];
z[24] = -abb[60] * z[29];
z[16] = (T(1) / T(2)) * z[16] + z[18] + z[24];
z[16] = abb[16] * z[16];
z[18] = -abb[34] * z[0];
z[24] = -abb[34] + z[42];
z[24] = abb[58] * z[24];
z[18] = z[18] + z[24] + z[41];
z[18] = abb[31] * z[18];
z[24] = abb[34] * z[39];
z[25] = -abb[63] * z[61];
z[27] = -abb[34] * z[42];
z[27] = -3 * abb[63] + -abb[64] + z[27];
z[27] = abb[58] * z[27];
z[18] = z[18] + z[24] + z[25] + z[27] + -z[67];
z[18] = abb[13] * z[18];
z[24] = z[0] * z[10];
z[24] = z[3] + z[24];
z[24] = abb[30] * z[24];
z[25] = z[0] * z[84];
z[3] = -z[3] + z[25];
z[3] = abb[29] * z[3];
z[3] = z[3] + z[24];
z[3] = abb[10] * z[3];
z[24] = abb[56] * abb[64];
z[24] = z[24] + -z[67];
z[25] = prod_pow(abb[29], 2);
z[27] = z[25] * z[29];
z[39] = abb[63] * z[22];
z[27] = z[24] + z[27] + -z[39];
z[41] = abb[29] * abb[58];
z[41] = -z[23] + z[41] + z[49];
z[41] = abb[31] * z[41];
z[25] = -abb[64] + (T(1) / T(2)) * z[25];
z[42] = -abb[63] + z[25];
z[49] = -abb[58] * z[42];
z[27] = (T(1) / T(2)) * z[27] + z[41] + z[49];
z[27] = abb[8] * z[27];
z[41] = abb[33] * abb[58];
z[41] = z[31] + z[41] + -z[55];
z[41] = abb[31] * z[41];
z[49] = abb[56] * z[17];
z[49] = -z[31] + z[49];
z[49] = abb[34] * z[49];
z[55] = -abb[32] + z[83];
z[57] = abb[29] * z[55];
z[61] = prod_pow(abb[32], 2);
z[61] = (T(1) / T(2)) * z[61];
z[57] = z[57] + z[61];
z[62] = -abb[33] + z[17];
z[62] = abb[34] * z[62];
z[91] = abb[63] + -z[57] + z[62];
z[91] = abb[58] * z[91];
z[100] = abb[56] * abb[63];
z[41] = z[41] + z[49] + z[91] + z[100];
z[41] = abb[1] * z[41];
z[10] = z[10] * z[26];
z[10] = z[10] + -z[52];
z[10] = abb[30] * z[10];
z[49] = -z[26] * z[43];
z[49] = z[49] + z[52];
z[49] = abb[31] * z[49];
z[52] = abb[35] + abb[62];
z[52] = z[26] * z[52];
z[10] = z[10] + z[49] + z[52];
z[10] = z[10] * z[54];
z[5] = z[5] + z[83];
z[5] = abb[29] * z[5];
z[19] = z[5] + z[19];
z[19] = abb[10] * z[19];
z[49] = z[61] + z[111];
z[52] = abb[2] * z[49];
z[19] = z[19] + -z[52];
z[52] = abb[58] * z[19];
z[91] = abb[30] * abb[33];
z[91] = -abb[35] + z[91];
z[5] = -z[5] + z[61] + -z[91];
z[5] = abb[58] * z[5];
z[37] = z[37] + z[83];
z[37] = abb[29] * z[37];
z[61] = abb[35] + -z[37] + z[73];
z[61] = abb[56] * z[61];
z[31] = -abb[30] * z[31];
z[5] = z[5] + z[31] + z[61];
z[5] = abb[0] * z[5];
z[31] = -z[93] * z[119];
z[61] = -z[48] * z[83];
z[45] = z[45] + z[61];
z[45] = abb[29] * z[45];
z[61] = -abb[18] * abb[33];
z[93] = -abb[17] * abb[32];
z[61] = z[61] + z[93];
z[93] = abb[32] * (T(1) / T(2));
z[61] = z[61] * z[93];
z[100] = abb[65] * z[63];
z[45] = z[45] + z[61] + z[100];
z[45] = z[45] * z[50];
z[50] = -z[4] * z[33] * z[63];
z[61] = -abb[7] * z[57];
z[46] = -abb[65] * z[46];
z[46] = z[46] + -z[50] + z[61];
z[57] = abb[1] * z[57];
z[61] = abb[1] * abb[36];
z[46] = (T(1) / T(2)) * z[46] + z[57] + z[61];
z[46] = abb[51] * z[46];
z[34] = -z[34] + z[60];
z[60] = abb[60] * z[34];
z[61] = -abb[16] * z[29];
z[34] = z[34] + z[61];
z[34] = abb[62] * z[34];
z[61] = -z[26] * z[70];
z[63] = abb[1] * abb[56];
z[61] = z[61] + z[63];
z[61] = abb[36] * z[61];
z[63] = abb[68] * z[35];
z[3] = z[3] + z[5] + z[8] + z[10] + z[15] + z[16] + z[18] + z[21] + z[27] + z[31] + z[34] + z[41] + z[45] + z[46] + z[52] + z[60] + z[61] + (T(-1) / T(2)) * z[63];
z[5] = z[36] + -z[83];
z[5] = abb[29] * z[5];
z[5] = z[5] + z[73];
z[8] = z[5] + z[112];
z[8] = abb[46] * z[8];
z[10] = z[9] * z[43];
z[15] = abb[32] * abb[46];
z[15] = z[10] + -z[14] + -z[15];
z[15] = abb[31] * z[15];
z[16] = z[11] * z[83];
z[18] = z[16] + z[99];
z[18] = abb[29] * z[18];
z[21] = abb[36] * z[9];
z[27] = abb[34] * z[14];
z[18] = z[18] + -z[21] + z[27];
z[12] = z[12] + -z[14];
z[12] = abb[30] * z[12];
z[14] = prod_pow(m1_set::bc<T>[0], 2);
z[21] = z[9] * z[14];
z[27] = z[11] * z[73];
z[11] = abb[35] * z[11];
z[11] = z[11] + z[27];
z[31] = abb[62] * z[9];
z[34] = abb[60] * z[9];
z[31] = z[31] + z[34];
z[8] = -z[8] + -z[11] + -z[12] + z[15] + z[18] + (T(-1) / T(2)) * z[21] + -z[31];
z[12] = abb[25] * z[8];
z[15] = abb[2] + abb[10];
z[21] = -abb[58] * z[15];
z[21] = z[21] + -z[77];
z[35] = abb[0] * z[35];
z[21] = (T(1) / T(2)) * z[21] + z[35];
z[35] = abb[16] * z[0];
z[21] = (T(1) / T(4)) * z[21] + (T(1) / T(8)) * z[35] + -z[64];
z[35] = abb[56] + (T(5) / T(3)) * z[0];
z[35] = abb[58] * (T(1) / T(3)) + (T(1) / T(2)) * z[35];
z[35] = abb[7] * z[35];
z[36] = -abb[1] + z[70];
z[41] = abb[51] * z[36];
z[35] = z[35] + z[41];
z[41] = abb[59] * z[48];
z[45] = abb[58] + abb[56] * (T(1) / T(4));
z[45] = abb[1] * z[45];
z[46] = abb[6] * z[26];
z[48] = -5 * abb[56] + -11 * z[0];
z[48] = -abb[58] + abb[51] * (T(-7) / T(4)) + (T(1) / T(4)) * z[48];
z[48] = abb[4] * z[48];
z[21] = (T(1) / T(3)) * z[21] + (T(1) / T(8)) * z[35] + (T(1) / T(16)) * z[41] + (T(1) / T(6)) * z[45] + (T(1) / T(48)) * z[46] + (T(1) / T(12)) * z[48];
z[21] = z[14] * z[21];
z[35] = abb[19] * abb[65];
z[41] = z[22] * z[35];
z[3] = (T(1) / T(4)) * z[3] + (T(1) / T(8)) * z[12] + z[21] + (T(1) / T(16)) * z[41];
z[12] = -z[25] + z[80];
z[21] = abb[31] * z[30];
z[21] = abb[60] + abb[62] + z[12] + (T(7) / T(6)) * z[14] + z[21];
z[21] = z[21] * z[79];
z[25] = -z[26] * z[83];
z[25] = z[25] + z[51] + z[78];
z[25] = abb[29] * z[25];
z[7] = z[7] + -z[51];
z[7] = abb[34] * z[7];
z[12] = -abb[58] * z[12];
z[26] = -abb[58] * z[30];
z[26] = z[26] + z[103];
z[26] = abb[31] * z[26];
z[30] = abb[34] + -z[33];
z[30] = abb[29] * z[30];
z[30] = z[30] + -z[109];
z[30] = abb[51] * z[30];
z[41] = (T(1) / T(3)) * z[14];
z[0] = -abb[58] + z[0];
z[45] = -abb[56] + abb[51] * (T(5) / T(2)) + (T(7) / T(2)) * z[0];
z[45] = z[41] * z[45];
z[0] = abb[60] * z[0];
z[46] = abb[62] * z[69];
z[0] = z[0] + z[7] + z[12] + z[21] + -z[24] + z[25] + z[26] + z[30] + z[45] + z[46];
z[0] = abb[9] * z[0];
z[7] = -z[84] + -z[86];
z[7] = abb[29] * z[7];
z[12] = abb[32] + abb[33];
z[21] = abb[32] * z[12];
z[7] = z[7] + (T(1) / T(4)) * z[21] + -z[91];
z[7] = abb[0] * z[7];
z[24] = -abb[33] + -z[93];
z[24] = z[24] * z[93];
z[25] = abb[33] + -z[83];
z[25] = z[25] * z[83];
z[24] = z[24] + z[25] + -z[111];
z[24] = abb[7] * z[24];
z[25] = -abb[30] + abb[29] * (T(1) / T(4)) + z[58];
z[25] = abb[29] * z[25];
z[25] = z[25] + (T(-3) / T(4)) * z[72] + z[91];
z[25] = abb[4] * z[25];
z[26] = abb[65] * (T(1) / T(2));
z[30] = z[26] * z[47];
z[45] = abb[4] + -abb[7];
z[15] = abb[0] + (T(-1) / T(2)) * z[15] + (T(-1) / T(4)) * z[45];
z[15] = z[15] * z[41];
z[7] = z[7] + z[15] + z[19] + z[24] + z[25] + z[30];
z[7] = abb[57] * z[7];
z[0] = z[0] + z[7];
z[7] = z[8] * z[92];
z[8] = abb[34] + (T(-1) / T(2)) * z[12] + z[83];
z[8] = abb[29] * z[8];
z[12] = abb[31] * z[33];
z[8] = z[8] + -z[12] + (T(1) / T(2)) * z[21] + z[113];
z[8] = abb[7] * z[8];
z[4] = abb[31] * z[4];
z[4] = -abb[60] + -z[4] + z[89];
z[4] = abb[16] * z[4];
z[12] = -abb[29] + z[43];
z[12] = abb[31] * z[12];
z[12] = -abb[60] + z[12] + z[42];
z[12] = abb[8] * z[12];
z[15] = z[26] * z[88];
z[4] = abb[68] + z[4] + -z[8] + z[12] + z[15] + -z[50];
z[8] = z[43] + -z[44] + z[55];
z[8] = abb[31] * z[8];
z[12] = abb[30] + abb[32];
z[12] = (T(1) / T(2)) * z[12] + -z[85];
z[12] = abb[29] * z[12];
z[15] = -z[44] + z[110];
z[15] = abb[30] * z[15];
z[15] = abb[35] * (T(3) / T(2)) + z[15];
z[19] = -abb[64] + z[72];
z[24] = abb[60] + z[19];
z[25] = abb[32] + abb[34] * (T(-1) / T(4)) + z[44];
z[25] = abb[34] * z[25];
z[26] = abb[36] * (T(1) / T(2)) + z[116];
z[8] = -z[8] + z[12] + z[15] + (T(1) / T(2)) * z[24] + -z[25] + z[26];
z[8] = abb[4] * z[8];
z[12] = -abb[32] + z[43];
z[12] = abb[31] * z[12];
z[12] = -abb[62] + z[12] + -z[111];
z[12] = z[12] * z[54];
z[24] = z[56] * z[116];
z[25] = abb[36] * z[36];
z[4] = (T(-1) / T(2)) * z[4] + z[8] + z[12] + z[24] + z[25] + -z[57];
z[8] = -abb[33] + z[43];
z[8] = abb[31] * z[8];
z[12] = abb[36] + abb[63] + (T(1) / T(6)) * z[14];
z[8] = z[8] + -z[12] + -z[62];
z[8] = z[8] * z[66];
z[24] = z[37] + -z[73] + z[91];
z[25] = abb[61] + z[41];
z[30] = -z[24] + z[25];
z[30] = z[30] * z[87];
z[33] = abb[6] * (T(-1) / T(24)) + abb[1] * (T(1) / T(4)) + abb[4] * (T(1) / T(6)) + (T(-1) / T(3)) * z[71];
z[14] = z[14] * z[33];
z[33] = abb[61] * (T(1) / T(4));
z[36] = z[33] * z[76];
z[4] = (T(1) / T(2)) * z[4] + -z[8] + z[14] + z[30] + (T(-1) / T(8)) * z[35] + -z[36];
z[4] = -z[4] * z[68];
z[8] = abb[34] * z[65];
z[14] = z[23] + -z[95];
z[28] = z[14] + -z[28];
z[28] = abb[29] * z[28];
z[2] = abb[33] * z[2];
z[19] = abb[56] * z[19];
z[19] = -z[2] + -z[8] + z[19] + z[28] + -z[38] + z[39] + z[67];
z[28] = -abb[30] + z[114];
z[28] = z[28] * z[83];
z[17] = z[17] * z[81];
z[21] = abb[63] + -abb[64] + z[21];
z[30] = abb[31] * (T(1) / T(4)) + z[114];
z[30] = abb[31] * z[30];
z[15] = -z[15] + z[17] + (T(-1) / T(2)) * z[21] + z[26] + z[28] + z[30];
z[15] = z[15] * z[79];
z[17] = -z[43] * z[98];
z[17] = z[17] + z[32];
z[17] = abb[31] * z[17];
z[21] = -abb[36] + abb[62];
z[21] = z[21] * z[29];
z[26] = z[49] + -z[82];
z[26] = abb[58] * z[26];
z[28] = abb[58] + z[22];
z[28] = z[28] * z[41];
z[15] = z[15] + z[17] + (T(1) / T(2)) * z[19] + z[21] + z[26] + z[28];
z[15] = z[15] * z[96];
z[17] = abb[57] * z[75];
z[19] = -z[40] * z[74];
z[21] = abb[26] * z[9];
z[17] = z[17] + z[19] + (T(-1) / T(4)) * z[21] + -z[59];
z[17] = z[17] * z[33];
z[14] = -z[14] + -z[104];
z[14] = abb[29] * z[14];
z[19] = abb[56] * z[72];
z[2] = -z[2] + z[19];
z[2] = (T(1) / T(2)) * z[2];
z[5] = z[5] + -z[94];
z[19] = abb[36] + abb[60] + z[41];
z[21] = z[5] + z[19];
z[21] = z[21] * z[79];
z[19] = z[19] * z[22];
z[8] = -z[2] + z[8] + z[14] + z[19] + z[21];
z[8] = z[8] * z[105];
z[14] = z[16] + -z[101];
z[14] = abb[29] * z[14];
z[16] = abb[46] * z[24];
z[19] = z[9] * z[41];
z[13] = abb[30] * z[13];
z[11] = -z[11] + z[13] + z[14] + z[16] + -z[19];
z[11] = z[11] * z[102];
z[13] = -z[23] + z[107];
z[13] = abb[29] * z[13];
z[14] = z[22] * z[25];
z[2] = -z[2] + z[13] + z[14] + z[38];
z[2] = z[2] * z[108];
z[5] = -abb[46] * z[5];
z[5] = z[5] + z[18] + -z[19] + -z[27] + -z[34];
z[5] = z[5] * z[106];
z[13] = abb[29] * abb[46];
z[10] = z[10] + -z[13] + -z[90];
z[10] = abb[31] * z[10];
z[13] = z[9] * z[42];
z[10] = z[10] + z[13] + -z[31];
z[9] = z[9] * z[33];
z[9] = -z[9] + (T(1) / T(4)) * z[10] + -z[19];
z[10] = (T(1) / T(4)) * z[97];
z[9] = z[9] * z[10];
z[10] = z[12] * z[22];
z[12] = -abb[31] * z[29];
z[1] = -z[1] + z[12];
z[1] = abb[31] * z[1];
z[1] = z[1] + z[10] + z[20];
z[1] = z[1] * z[53];
z[0] = abb[43] + abb[44] + (T(1) / T(8)) * z[0] + z[1] + z[2] + (T(1) / T(2)) * z[3] + z[4] + z[5] + z[7] + z[8] + z[9] + z[11] + z[15] + z[17];
return {z[6], z[0]};
}


template <typename T> std::complex<T> f_4_458_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("-0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("-0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.19141333882654874593799512278862648300113369431982448913281564563"),stof<T>("-0.30134686089915515147631581969909472554977252473577097248262239332")}, std::complex<T>{stof<T>("0.3534022627704206375530181841998584312133997630126665802989932014"),stof<T>("-0.8831176249736222280545759260227220689452418419254082746111154505")}, std::complex<T>{stof<T>("0.06066389867311492039061135562264461865164418393519863554285381691"),stof<T>("-0.27109334205345297061234596881496738140284794592009951787227089186")}, std::complex<T>{stof<T>("0.3534022627704206375530181841998584312133997630126665802989932014"),stof<T>("-0.8831176249736222280545759260227220689452418419254082746111154505")}, std::complex<T>{stof<T>("0.06066389867311492039061135562264461865164418393519863554285381691"),stof<T>("-0.27109334205345297061234596881496738140284794592009951787227089186")}, std::complex<T>{stof<T>("-0.0417966127428765050314821214938676836467955745016790176314509502"),stof<T>("-0.27148044900177878644403449703850351511026247680080638748131402086")}, std::complex<T>{stof<T>("0.25788446378157078644169071588347303237168339037386471393954120566"),stof<T>("-0.67938639824574491084493826156685035800529820202748709595826682305")}, std::complex<T>{stof<T>("-0.14446840926348375417976620277548555690378164296056929535601261713"),stof<T>("-0.65503482590672700762138937335045907925812608019796635462475139406")}, std::complex<T>{stof<T>("0.34589967045123744046959203147849259087531787275930987103590649702"),stof<T>("-0.46448714177691965286377725952042574354198205525390718348459342161")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_458_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_458_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_458_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (8 + 3 * v[0] + v[1] + 3 * v[2] + -v[3] + -4 * v[4] + -3 * v[5]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[51] + abb[53] + abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[1];
z[0] = -abb[51] + -abb[53] + -abb[55];
return abb[11] * abb[36] * (T(1) / T(8)) * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_458_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(64)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(-1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[53] + abb[55] + abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[53] + abb[55] + abb[57];
z[1] = abb[30] + -abb[32];
return abb[12] * m1_set::bc<T>[0] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_458_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(64)) * (v[2] + v[3]) * (-4 * v[0] + -2 * v[1] + -m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 2 * (-4 + v[2] + 2 * v[3] + v[4] + -2 * v[5]))) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[53] + abb[55] + abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = -abb[29] + abb[32] + abb[30] * (T(-1) / T(2));
z[0] = abb[30] * z[0];
z[1] = -abb[29] + abb[32] * (T(1) / T(2));
z[1] = abb[32] * z[1];
z[2] = abb[30] + -abb[32];
z[2] = abb[33] * z[2];
z[0] = z[0] + -z[1] + z[2];
z[0] = -abb[35] + (T(1) / T(2)) * z[0];
z[1] = -abb[53] + -abb[55] + -abb[57];
return abb[12] * (T(1) / T(4)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_458_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.13029787601440188555632100409204674007789509773543688927051093925"),stof<T>("0.15717338947652149304431790075785243189046388844135253608558841654")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,69> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[43] = SpDLog_f_4_458_W_17_Im(t, path, abb);
abb[44] = SpDLog_f_4_458_W_19_Im(t, path, abb);
abb[45] = SpDLogQ_W_82(k,dl,dlr).imag();
abb[66] = SpDLog_f_4_458_W_17_Re(t, path, abb);
abb[67] = SpDLog_f_4_458_W_19_Re(t, path, abb);
abb[68] = SpDLogQ_W_82(k,dl,dlr).real();

                    
            return f_4_458_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_458_DLogXconstant_part(base_point<T>, kend);
	value += f_4_458_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_458_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_458_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_458_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_458_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_458_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_458_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
