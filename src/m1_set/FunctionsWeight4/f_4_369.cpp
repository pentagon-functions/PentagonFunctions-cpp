/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_369.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_369_abbreviated (const std::array<T,56>& abb) {
T z[73];
z[0] = abb[29] * m1_set::bc<T>[0];
z[1] = -abb[28] + abb[31];
z[2] = m1_set::bc<T>[0] * z[1];
z[3] = abb[30] * m1_set::bc<T>[0];
z[4] = z[0] + -z[2] + -z[3];
z[5] = abb[38] + -abb[39];
z[6] = z[4] + z[5];
z[7] = abb[13] * (T(1) / T(2));
z[6] = z[6] * z[7];
z[8] = m1_set::bc<T>[0] * (T(1) / T(2));
z[9] = abb[33] * z[8];
z[10] = -z[3] + z[9];
z[11] = abb[29] * z[8];
z[12] = z[10] + z[11];
z[13] = abb[40] * (T(1) / T(2));
z[14] = -z[12] + z[13];
z[15] = abb[39] * (T(1) / T(2));
z[16] = z[14] + z[15];
z[17] = abb[8] * z[16];
z[6] = z[6] + z[17];
z[18] = abb[16] + abb[17] + abb[19];
z[19] = abb[18] + z[18];
z[20] = abb[41] * (T(1) / T(4));
z[21] = z[19] * z[20];
z[22] = abb[32] * (T(1) / T(2));
z[23] = -z[1] + z[22];
z[23] = m1_set::bc<T>[0] * z[23];
z[23] = -z[9] + z[23];
z[24] = abb[38] + z[23];
z[25] = (T(1) / T(2)) * z[24];
z[26] = abb[15] * z[25];
z[21] = -z[6] + z[21] + z[26];
z[27] = abb[31] + -abb[32];
z[28] = m1_set::bc<T>[0] * z[27];
z[29] = -abb[39] + abb[40];
z[30] = z[28] + z[29];
z[31] = abb[6] * (T(1) / T(2));
z[30] = z[30] * z[31];
z[32] = abb[28] * m1_set::bc<T>[0];
z[33] = -z[3] + z[32];
z[34] = abb[9] * z[33];
z[5] = z[5] + z[33];
z[33] = abb[5] * (T(1) / T(2));
z[5] = z[5] * z[33];
z[30] = -z[5] + z[30] + z[34];
z[34] = 3 * z[3];
z[22] = -abb[31] + z[22];
z[22] = m1_set::bc<T>[0] * z[22];
z[35] = z[22] + z[34];
z[36] = abb[33] * m1_set::bc<T>[0];
z[37] = z[0] + -z[35] + (T(3) / T(2)) * z[36];
z[37] = -abb[38] + -z[13] + (T(1) / T(2)) * z[37];
z[37] = abb[4] * z[37];
z[38] = abb[32] * z[8];
z[38] = -abb[40] + z[10] + z[38];
z[39] = abb[14] * (T(1) / T(2));
z[40] = z[38] * z[39];
z[41] = -abb[28] + abb[32] * (T(3) / T(2));
z[41] = m1_set::bc<T>[0] * z[41];
z[41] = -z[9] + z[41];
z[42] = abb[38] + -z[29] + z[41];
z[43] = abb[7] * (T(1) / T(2));
z[42] = z[42] * z[43];
z[44] = -abb[32] + z[1];
z[45] = m1_set::bc<T>[0] * z[44];
z[46] = -abb[38] + z[36];
z[47] = z[45] + z[46];
z[48] = -abb[0] * z[47];
z[49] = abb[2] * abb[38];
z[37] = z[21] + -z[30] + z[37] + -z[40] + z[42] + z[48] + z[49];
z[37] = abb[46] * z[37];
z[2] = -abb[40] + z[2] + -z[3] + z[46];
z[3] = abb[21] * (T(1) / T(2));
z[2] = z[2] * z[3];
z[3] = abb[39] + -z[10] + z[22];
z[3] = (T(1) / T(2)) * z[3];
z[10] = abb[22] + abb[24];
z[3] = -z[3] * z[10];
z[22] = abb[20] * z[38];
z[42] = abb[26] * (T(1) / T(2));
z[46] = abb[41] * z[42];
z[22] = z[22] + -z[46];
z[16] = abb[23] * z[16];
z[25] = abb[25] * z[25];
z[2] = z[2] + z[3] + -z[16] + (T(1) / T(2)) * z[22] + -z[25];
z[3] = -abb[42] + abb[44];
z[16] = -abb[43] + z[3];
z[16] = (T(-1) / T(2)) * z[16];
z[2] = z[2] * z[16];
z[11] = abb[39] + -z[11] + z[13];
z[13] = -z[11] + (T(-1) / T(2)) * z[35] + (T(3) / T(4)) * z[36];
z[13] = abb[4] * z[13];
z[16] = 3 * abb[39];
z[22] = -abb[40] + z[16];
z[25] = -abb[38] + z[22] + z[41];
z[25] = z[25] * z[43];
z[22] = z[22] + -z[28];
z[22] = z[22] * z[31];
z[5] = -z[5] + z[13] + z[21] + z[22] + z[25];
z[13] = -abb[28] + abb[32];
z[8] = z[8] * z[13];
z[8] = z[8] + -z[12];
z[12] = abb[38] * (T(-1) / T(2)) + abb[39] * (T(3) / T(2)) + z[8];
z[12] = abb[12] * z[12];
z[21] = abb[14] * (T(1) / T(4));
z[22] = z[21] * z[38];
z[12] = z[12] + z[22];
z[5] = (T(1) / T(2)) * z[5] + -z[12];
z[5] = abb[48] * z[5];
z[22] = abb[0] * (T(1) / T(2));
z[25] = z[22] * z[47];
z[28] = abb[32] * m1_set::bc<T>[0];
z[28] = z[28] + -z[36];
z[28] = abb[38] + (T(1) / T(2)) * z[28];
z[35] = z[28] * z[43];
z[38] = -abb[38] + (T(-1) / T(2)) * z[23];
z[38] = abb[4] * z[38];
z[41] = z[18] * z[20];
z[26] = -z[25] + z[26] + z[35] + z[38] + z[41] + z[49];
z[35] = abb[50] * (T(1) / T(2));
z[26] = z[26] * z[35];
z[38] = m1_set::bc<T>[0] * z[13];
z[29] = -z[29] + z[38];
z[29] = z[29] * z[43];
z[20] = abb[18] * z[20];
z[20] = z[20] + -z[25] + z[29] + -z[30];
z[25] = -z[32] + z[34];
z[9] = -z[9] + z[11] + (T(1) / T(2)) * z[25];
z[9] = abb[4] * z[9];
z[4] = 3 * abb[38] + z[4] + -z[16];
z[4] = z[4] * z[7];
z[4] = -z[4] + -z[9] + z[17] + z[20] + z[49];
z[4] = (T(1) / T(2)) * z[4] + -z[12];
z[9] = abb[47] + abb[49];
z[4] = z[4] * z[9];
z[0] = -abb[40] + z[0] + -z[25] + z[36];
z[9] = abb[4] * (T(1) / T(2));
z[0] = z[0] * z[9];
z[0] = z[0] + -z[6] + z[20] + -z[40];
z[6] = z[8] + z[15];
z[8] = -abb[1] * z[6];
z[11] = abb[11] * z[14];
z[0] = (T(1) / T(2)) * z[0] + z[8] + z[11];
z[0] = abb[45] * z[0];
z[8] = abb[17] * z[23];
z[11] = -abb[16] * z[28];
z[12] = -abb[19] * z[24];
z[8] = z[8] + z[11] + z[12];
z[11] = abb[4] + abb[7] + abb[15];
z[11] = -abb[27] + (T(1) / T(4)) * z[11];
z[12] = -abb[41] * z[11];
z[8] = (T(1) / T(2)) * z[8] + z[12];
z[12] = abb[51] * (T(1) / T(2));
z[8] = z[8] * z[12];
z[15] = abb[1] * abb[46];
z[6] = -z[6] * z[15];
z[16] = abb[46] + abb[48];
z[17] = abb[11] * z[16];
z[14] = z[14] * z[17];
z[20] = z[36] + z[45];
z[20] = -abb[38] + (T(1) / T(2)) * z[20];
z[23] = abb[46] + abb[50];
z[23] = abb[10] * z[23];
z[20] = z[20] * z[23];
z[0] = z[0] + z[2] + z[4] + z[5] + z[6] + z[8] + z[14] + z[20] + z[26] + (T(1) / T(2)) * z[37];
z[0] = (T(1) / T(8)) * z[0];
z[2] = abb[30] * (T(1) / T(2));
z[4] = z[2] + z[13];
z[5] = abb[30] * z[4];
z[6] = abb[33] * z[27];
z[8] = abb[31] * (T(1) / T(2));
z[14] = -abb[28] + z[8];
z[14] = abb[31] * z[14];
z[20] = -abb[35] + z[14];
z[24] = prod_pow(abb[28], 2);
z[25] = (T(1) / T(2)) * z[24];
z[6] = abb[54] + z[5] + z[6] + z[20] + z[25];
z[26] = abb[29] * (T(1) / T(2));
z[28] = -abb[30] + z[26];
z[29] = -z[1] + z[28];
z[29] = abb[29] * z[29];
z[30] = abb[36] + abb[37];
z[32] = z[29] + z[30];
z[34] = -abb[53] + z[32];
z[36] = z[6] + z[34];
z[36] = z[31] * z[36];
z[9] = -abb[9] + z[7] + z[9] + z[33];
z[9] = abb[34] * z[9];
z[37] = prod_pow(abb[31], 2);
z[37] = -z[25] + (T(1) / T(2)) * z[37];
z[38] = z[30] + z[37];
z[40] = prod_pow(abb[30], 2);
z[41] = (T(1) / T(2)) * z[40];
z[29] = z[29] + z[38] + z[41];
z[29] = abb[9] * z[29];
z[34] = z[34] + z[41];
z[37] = abb[52] + z[34] + z[37];
z[33] = z[33] * z[37];
z[37] = abb[31] * z[27];
z[41] = abb[29] * z[27];
z[45] = prod_pow(m1_set::bc<T>[0], 2);
z[46] = (T(1) / T(6)) * z[45];
z[37] = abb[35] + abb[36] + z[37] + -z[41] + -z[46];
z[47] = abb[3] * (T(1) / T(2));
z[37] = z[37] * z[47];
z[47] = abb[54] * z[43];
z[9] = z[9] + z[29] + -z[33] + z[36] + z[37] + z[47];
z[29] = abb[2] * abb[52];
z[36] = z[9] + -z[29];
z[47] = abb[28] + abb[32];
z[48] = -abb[31] + (T(1) / T(2)) * z[47];
z[49] = abb[30] + z[48];
z[49] = abb[33] * z[49];
z[50] = (T(3) / T(2)) * z[40];
z[51] = abb[28] * abb[32];
z[52] = (T(1) / T(2)) * z[51];
z[14] = -z[14] + -z[30] + (T(7) / T(6)) * z[45] + z[49] + -z[50] + -z[52];
z[14] = (T(1) / T(2)) * z[14];
z[30] = abb[33] * (T(1) / T(2));
z[49] = -abb[30] + abb[29] * (T(1) / T(4)) + z[30];
z[53] = (T(1) / T(2)) * z[1] + -z[49];
z[53] = abb[29] * z[53];
z[53] = abb[54] * (T(1) / T(2)) + z[53];
z[54] = abb[35] * (T(1) / T(2));
z[55] = abb[52] + z[54];
z[56] = z[14] + z[53] + z[55];
z[56] = abb[4] * z[56];
z[57] = -abb[30] + abb[33];
z[27] = z[27] + z[57];
z[27] = abb[29] * z[27];
z[58] = abb[30] + z[13];
z[58] = abb[30] * z[58];
z[59] = abb[30] + z[1];
z[59] = abb[33] * z[59];
z[44] = abb[31] * z[44];
z[44] = abb[36] + z[44];
z[27] = z[27] + -z[44] + z[58] + -z[59];
z[58] = -abb[52] + z[51];
z[59] = -abb[53] + z[27] + -z[58];
z[59] = z[7] * z[59];
z[60] = (T(5) / T(6)) * z[45];
z[61] = abb[30] * abb[33];
z[62] = z[60] + z[61];
z[57] = abb[29] * z[57];
z[40] = abb[54] + -z[40] + -z[57] + z[62];
z[57] = abb[53] + z[40];
z[63] = abb[8] * z[57];
z[63] = (T(1) / T(2)) * z[63];
z[59] = z[59] + z[63];
z[64] = z[24] + z[51];
z[65] = abb[28] * abb[31];
z[48] = abb[33] * z[48];
z[45] = (T(1) / T(3)) * z[45];
z[64] = z[45] + z[48] + (T(-1) / T(2)) * z[64] + z[65];
z[65] = abb[35] + z[64];
z[66] = abb[52] + z[65];
z[67] = abb[15] * (T(1) / T(2));
z[67] = z[66] * z[67];
z[68] = z[59] + -z[67];
z[24] = z[24] + -z[51];
z[30] = z[13] * z[30];
z[69] = abb[30] * z[13];
z[69] = abb[54] + z[69];
z[70] = abb[34] + z[45];
z[24] = (T(1) / T(2)) * z[24] + -z[30] + z[69] + z[70];
z[39] = z[24] * z[39];
z[28] = -z[13] + z[28];
z[71] = abb[29] * z[28];
z[5] = abb[37] + z[5] + z[60] + z[71];
z[71] = -abb[32] + z[8];
z[71] = abb[31] * z[71];
z[30] = abb[35] + z[30] + z[52] + z[71];
z[71] = -abb[52] + z[30];
z[72] = -abb[53] + z[5] + z[71];
z[72] = z[43] * z[72];
z[8] = -z[8] + z[47];
z[8] = abb[31] * z[8];
z[8] = z[8] + -z[25] + z[46];
z[1] = abb[33] * z[1];
z[25] = z[1] + z[58];
z[47] = z[8] + -z[25];
z[58] = -abb[0] * z[47];
z[56] = z[36] + -z[39] + z[56] + z[58] + z[68] + z[72];
z[56] = abb[46] * z[56];
z[17] = -z[17] * z[40];
z[17] = z[17] + z[56];
z[56] = abb[53] + z[53];
z[14] = z[14] + z[54] + z[56];
z[14] = abb[4] * z[14];
z[54] = 3 * abb[53];
z[58] = -abb[52] + z[54];
z[5] = abb[54] + z[5] + z[30] + -z[58];
z[5] = abb[7] * z[5];
z[30] = abb[4] + -abb[5] + abb[13];
z[30] = abb[34] * z[30];
z[5] = z[5] + z[30];
z[6] = z[6] + z[32] + -z[54];
z[6] = z[6] * z[31];
z[5] = (T(1) / T(2)) * z[5] + z[6] + z[14] + z[33] + z[37] + z[68];
z[6] = z[21] * z[24];
z[14] = abb[30] * (T(3) / T(2)) + z[13];
z[14] = abb[30] * z[14];
z[14] = abb[37] + z[14] + -z[46] + -z[61];
z[21] = -z[14] + z[58];
z[13] = (T(1) / T(2)) * z[13] + -z[49];
z[13] = abb[29] * z[13];
z[21] = z[13] + (T(1) / T(2)) * z[21];
z[21] = abb[12] * z[21];
z[6] = -z[6] + z[21];
z[5] = (T(1) / T(2)) * z[5] + z[6];
z[5] = abb[48] * z[5];
z[2] = z[2] * z[4];
z[4] = z[26] * z[28];
z[21] = -abb[37] + abb[53];
z[2] = z[2] + z[4] + (T(-1) / T(2)) * z[21] + z[45];
z[2] = abb[7] * z[2];
z[4] = z[22] * z[47];
z[2] = z[2] + -z[4];
z[21] = abb[11] * z[40];
z[22] = z[38] + z[50] + -z[62];
z[22] = (T(1) / T(2)) * z[22];
z[26] = -z[22] + z[53];
z[26] = abb[4] * z[26];
z[28] = abb[18] * abb[55];
z[9] = z[2] + z[9] + -z[21] + z[26] + (T(-1) / T(4)) * z[28] + -z[39] + z[59];
z[14] = -abb[53] + z[14];
z[13] = -z[13] + (T(1) / T(2)) * z[14];
z[14] = -abb[1] * z[13];
z[9] = (T(1) / T(2)) * z[9] + z[14];
z[9] = abb[45] * z[9];
z[14] = -z[20] + -z[34] + z[48] + -z[52] + z[70];
z[10] = -z[10] * z[14];
z[14] = -abb[34] + z[25] + -z[41] + z[44] + -z[60] + -z[69];
z[14] = abb[21] * z[14];
z[20] = abb[25] * z[66];
z[21] = abb[20] * z[24];
z[24] = abb[23] * z[57];
z[10] = z[10] + z[14] + -z[20] + -z[21] + -z[24];
z[14] = abb[55] * z[42];
z[14] = z[10] + -z[14];
z[3] = (T(1) / T(4)) * z[3];
z[3] = z[3] * z[14];
z[14] = 3 * abb[52] + z[27] + -z[51] + -z[54];
z[7] = z[7] * z[14];
z[14] = z[22] + -z[56];
z[14] = abb[4] * z[14];
z[2] = -z[2] + -z[7] + z[14] + -z[36] + z[63];
z[2] = (T(1) / T(2)) * z[2] + -z[6];
z[6] = -abb[47] * z[2];
z[7] = abb[43] * z[10];
z[2] = -z[2] + (T(-1) / T(8)) * z[28];
z[2] = abb[49] * z[2];
z[10] = z[55] + (T(1) / T(2)) * z[64];
z[10] = abb[4] * z[10];
z[14] = z[46] + z[71];
z[20] = z[14] * z[43];
z[4] = -z[4] + z[10] + z[20] + -z[29] + -z[67];
z[4] = z[4] * z[35];
z[10] = -abb[17] * z[65];
z[20] = abb[19] * z[66];
z[14] = -abb[16] * z[14];
z[10] = z[10] + z[14] + z[20];
z[11] = abb[55] * z[11];
z[10] = (T(1) / T(2)) * z[10] + z[11];
z[10] = z[10] * z[12];
z[11] = -z[16] * z[19];
z[12] = -abb[18] * abb[47];
z[14] = abb[26] * abb[43];
z[16] = -abb[50] * z[18];
z[11] = z[11] + z[12] + z[14] + z[16];
z[11] = abb[55] * z[11];
z[12] = -z[13] * z[15];
z[1] = -z[1] + z[8] + -z[51];
z[1] = abb[52] + (T(1) / T(2)) * z[1];
z[1] = z[1] * z[23];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + (T(-1) / T(4)) * z[7] + z[9] + z[10] + (T(1) / T(8)) * z[11] + z[12] + (T(1) / T(2)) * z[17];
z[1] = (T(1) / T(8)) * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_369_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.291843602455546826087180463468023247692871276616896645131994913"),stof<T>("0.09369875472481212921793210863360899099745451746447368638248116819")}, std::complex<T>{stof<T>("-0.55589943398860493275193564048908228623291265802151148788344462642"),stof<T>("0.13084868474382308812447636641671047724188407706095609728336974593")}, std::complex<T>{stof<T>("-0.37259504071376252632951439941423041827751171803601550333809067048"),stof<T>("0.0844818583483663323216856088761904258822870784314020382309202719")}, std::complex<T>{stof<T>("-0.61348282752157547646374427215219578217372305830043282512188025377"),stof<T>("0.21068533998168753596003361767643260923996475665770237145134642135")}, std::complex<T>{stof<T>("-0.37259504071376252632951439941423041827751171803601550333809067048"),stof<T>("0.0844818583483663323216856088761904258822870784314020382309202719")}, std::complex<T>{stof<T>("-0.26405583153305810666475517702105903854004138140461484275144971343"),stof<T>("0.03714993001901095890654425778310148624442955959648241090088857774")}, std::complex<T>{stof<T>("-0.22562877110985045053470213419117117506337863168958019860872027894"),stof<T>("0.1131138072917163758247987163378884159966013081904407578211399125")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_369_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_369_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.12110146007774777718476632199633141368121401034271083731883968209"),stof<T>("-0.19986542962265779901715053579122830042542252176732194609127685089")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k)};

                    
            return f_4_369_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_369_DLogXconstant_part(base_point<T>, kend);
	value += f_4_369_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_369_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_369_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_369_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_369_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_369_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_369_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
