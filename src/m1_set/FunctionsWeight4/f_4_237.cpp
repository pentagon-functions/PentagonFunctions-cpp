/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_237.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_237_abbreviated (const std::array<T,16>& abb) {
T z[16];
z[0] = 2 * abb[12];
z[1] = abb[13] + z[0];
z[2] = abb[11] + -z[1];
z[3] = abb[8] * z[2];
z[4] = abb[1] + -abb[3];
z[5] = -z[3] * z[4];
z[6] = -abb[3] + abb[5];
z[6] = abb[7] * z[2] * z[6];
z[1] = -abb[14] + z[1];
z[1] = abb[7] * z[1];
z[1] = z[1] + z[3];
z[3] = 2 * abb[2];
z[1] = z[1] * z[3];
z[1] = z[1] + z[5] + z[6];
z[1] = abb[7] * z[1];
z[5] = abb[5] * z[2];
z[0] = abb[0] * z[0];
z[6] = abb[11] + -abb[12];
z[7] = abb[4] * z[6];
z[0] = z[0] + z[5] + z[7];
z[8] = abb[12] + abb[13] * (T(1) / T(2));
z[9] = abb[11] * (T(1) / T(2)) + -z[8];
z[9] = abb[1] * z[9];
z[10] = abb[11] + abb[13];
z[10] = abb[3] * z[10];
z[11] = (T(1) / T(2)) * z[10];
z[12] = abb[0] * abb[14];
z[13] = -z[0] + z[9] + z[11] + 2 * z[12];
z[13] = prod_pow(abb[8], 2) * z[13];
z[14] = abb[0] * abb[12];
z[14] = -z[12] + z[14];
z[15] = -abb[12] + -7 * abb[13];
z[15] = -abb[11] + (T(1) / T(6)) * z[15];
z[15] = abb[3] * z[15];
z[8] = abb[14] * (T(1) / T(2)) + -z[8];
z[8] = abb[2] * z[8];
z[5] = z[5] + (T(1) / T(3)) * z[8] + (T(13) / T(6)) * z[14] + z[15];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[8] = -abb[1] + abb[2];
z[8] = z[2] * z[8];
z[14] = abb[3] + abb[4];
z[6] = z[6] * z[14];
z[6] = z[6] + z[8];
z[6] = abb[9] * z[6];
z[8] = z[7] + -z[9] + z[11];
z[8] = abb[6] * z[8];
z[3] = -z[3] + z[4];
z[3] = z[2] * z[3];
z[4] = abb[7] * z[3];
z[4] = z[4] + z[8];
z[4] = abb[6] * z[4];
z[8] = -abb[15] * z[3];
z[1] = z[1] + z[4] + z[5] + z[6] + z[8] + z[13];
z[2] = abb[1] * z[2];
z[0] = 2 * z[0] + -z[2] + -z[10] + -4 * z[12];
z[0] = abb[8] * z[0];
z[2] = z[2] + -2 * z[7] + -z[10];
z[2] = abb[6] * z[2];
z[0] = z[0] + z[2];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[10] * z[3];
z[0] = z[0] + z[2];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_237_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.8851066797865090121045382553432042033443533330780297410706893654"),stof<T>("-4.2652683322429439786853419176585110096129822091780198741212434611")}, std::complex<T>{stof<T>("3.6829813829162597425126257087210567251836075161072208182775494951"),stof<T>("-11.9791235274951074822690799539702078075941909156745399809233309644")}, std::complex<T>{stof<T>("-19.892863152671605965999716894711289190566145705205083238003708664"),stof<T>("2.010688063418513109653840556751340815251191929326263598038320412")}, std::complex<T>{stof<T>("-21.690737855801356696407804348089141712405399888234274315210568794"),stof<T>("18.255079923156564570608262428380059632458365054178823453082894837")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[22].real()/kbase.W[22].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_237_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_237_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-5.4964366298235505513183220296284724901068060406509121251377469976"),stof<T>("11.3816813901908646538128467122095886100821096564933159719895461214")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,16> abb = {dlog_W3(k,dl), dlog_W7(k,dl), dlog_W8(k,dl), dl[4], dlog_W18(k,dl), dlog_W23(k,dl), f_1_6(k), f_1_7(k), f_1_10(k), f_2_8(k), f_2_10_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[22].real()/k.W[22].real()), f_2_10_re(k)};

                    
            return f_4_237_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_237_DLogXconstant_part(base_point<T>, kend);
	value += f_4_237_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_237_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_237_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_237_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_237_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_237_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_237_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
