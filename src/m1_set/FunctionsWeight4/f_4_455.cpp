/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_455.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_455_abbreviated (const std::array<T,65>& abb) {
T z[83];
z[0] = abb[32] + -abb[33];
z[1] = (T(1) / T(2)) * z[0];
z[2] = abb[35] + z[1];
z[3] = m1_set::bc<T>[0] * z[2];
z[4] = abb[30] * m1_set::bc<T>[0];
z[5] = z[3] + -z[4];
z[6] = abb[2] * z[5];
z[7] = abb[34] * m1_set::bc<T>[0];
z[8] = z[4] + -z[7];
z[9] = abb[9] * (T(1) / T(2));
z[10] = z[8] * z[9];
z[11] = abb[31] * m1_set::bc<T>[0];
z[11] = -z[7] + z[11];
z[12] = abb[13] * (T(1) / T(2));
z[13] = z[11] * z[12];
z[6] = z[6] + -z[10] + -z[13];
z[14] = -abb[33] + abb[35];
z[15] = abb[31] + abb[32];
z[16] = -z[14] + z[15];
z[16] = m1_set::bc<T>[0] * z[16];
z[16] = -z[4] + -z[7] + z[16];
z[17] = abb[7] * (T(1) / T(4));
z[16] = z[16] * z[17];
z[18] = m1_set::bc<T>[0] * (T(1) / T(2));
z[19] = z[15] * z[18];
z[19] = -z[7] + z[19];
z[20] = abb[8] * z[19];
z[21] = abb[4] * (T(1) / T(2));
z[22] = abb[2] + z[21];
z[23] = abb[40] * z[22];
z[20] = -z[20] + z[23];
z[23] = abb[5] * (T(1) / T(4));
z[14] = -z[14] * z[23];
z[24] = abb[0] * z[0];
z[14] = z[14] + (T(1) / T(8)) * z[24];
z[14] = m1_set::bc<T>[0] * z[14];
z[25] = abb[1] * z[19];
z[26] = -abb[35] + (T(-1) / T(4)) * z[0];
z[26] = m1_set::bc<T>[0] * z[26];
z[26] = z[4] + z[26];
z[26] = z[21] * z[26];
z[27] = -abb[31] + abb[35];
z[28] = m1_set::bc<T>[0] * z[27];
z[28] = -z[8] + z[28];
z[29] = -abb[40] + 3 * z[28];
z[29] = abb[17] * z[29];
z[30] = abb[42] * (T(1) / T(2));
z[31] = abb[5] + abb[7];
z[32] = abb[4] + abb[8] + -z[31];
z[32] = -abb[1] + (T(1) / T(2)) * z[32];
z[32] = z[30] * z[32];
z[33] = -abb[19] + abb[20] + -abb[21];
z[34] = abb[43] * (T(1) / T(8));
z[34] = -z[33] * z[34];
z[35] = abb[17] * (T(1) / T(2));
z[36] = -abb[4] + abb[5] + abb[8];
z[36] = -abb[1] + z[35] + (T(1) / T(2)) * z[36];
z[37] = abb[41] * (T(1) / T(2));
z[36] = z[36] * z[37];
z[38] = abb[32] + abb[33];
z[39] = z[18] * z[38];
z[40] = -abb[42] + -z[7] + z[39];
z[41] = abb[16] * (T(1) / T(4));
z[42] = -z[40] * z[41];
z[5] = -abb[40] + z[5];
z[43] = abb[15] * (T(1) / T(4));
z[44] = z[5] * z[43];
z[45] = -abb[40] + abb[41];
z[46] = z[8] + -z[45];
z[47] = abb[6] * (T(1) / T(4));
z[46] = z[46] * z[47];
z[48] = abb[46] * (T(1) / T(4));
z[14] = -z[6] + z[14] + z[16] + (T(1) / T(2)) * z[20] + z[25] + z[26] + (T(1) / T(4)) * z[29] + z[32] + z[34] + z[36] + z[42] + z[44] + z[46] + -z[48];
z[16] = abb[54] * (T(1) / T(8));
z[14] = z[14] * z[16];
z[20] = abb[35] * (T(1) / T(2));
z[26] = abb[10] * z[20];
z[29] = abb[2] * z[2];
z[26] = z[26] + -z[29];
z[32] = m1_set::bc<T>[0] * z[26];
z[34] = abb[10] * (T(1) / T(2));
z[36] = -abb[2] + z[34];
z[42] = z[4] * z[36];
z[32] = z[32] + -z[42];
z[42] = abb[7] * (T(1) / T(2));
z[44] = -z[21] + z[42];
z[44] = z[28] * z[44];
z[46] = abb[35] + z[0];
z[49] = m1_set::bc<T>[0] * z[46];
z[49] = -z[4] + z[49];
z[50] = abb[0] * (T(1) / T(2));
z[49] = z[49] * z[50];
z[51] = abb[31] + z[46];
z[51] = z[18] * z[51];
z[51] = -z[7] + z[51];
z[51] = abb[5] * z[51];
z[52] = abb[31] + z[0];
z[53] = -m1_set::bc<T>[0] * z[52];
z[53] = -z[4] + z[53];
z[53] = z[7] + (T(1) / T(2)) * z[53];
z[53] = abb[1] * z[53];
z[54] = abb[0] + -abb[2];
z[55] = abb[40] * z[54];
z[56] = (T(1) / T(2)) * z[55];
z[10] = z[10] + -z[13] + z[32] + z[44] + z[48] + z[49] + z[51] + z[53] + -z[56];
z[13] = abb[55] * (T(1) / T(8));
z[10] = z[10] * z[13];
z[44] = abb[5] * z[19];
z[48] = -abb[4] + abb[13];
z[11] = z[11] * z[48];
z[49] = abb[1] + -abb[5];
z[37] = z[37] * z[49];
z[51] = abb[1] + -abb[14];
z[30] = z[30] * z[51];
z[11] = (T(-1) / T(2)) * z[11] + -z[25] + z[30] + z[37] + z[44];
z[25] = abb[51] + abb[53];
z[30] = (T(-1) / T(8)) * z[25];
z[11] = z[11] * z[30];
z[18] = z[18] * z[24];
z[24] = abb[33] * m1_set::bc<T>[0];
z[24] = -z[4] + z[24];
z[30] = -abb[4] * z[24];
z[18] = z[18] + z[30];
z[30] = -z[4] + -z[39];
z[30] = z[7] + (T(1) / T(2)) * z[30];
z[30] = abb[7] * z[30];
z[37] = abb[1] * z[24];
z[8] = abb[9] * z[8];
z[44] = abb[18] * abb[43];
z[51] = -abb[42] * z[51];
z[8] = z[8] + (T(1) / T(2)) * z[18] + z[30] + z[37] + (T(-1) / T(4)) * z[44] + z[51];
z[18] = abb[56] * (T(1) / T(16));
z[8] = z[8] * z[18];
z[3] = abb[41] + -z[3] + z[7];
z[7] = abb[24] + abb[26];
z[7] = (T(1) / T(32)) * z[7];
z[3] = -z[3] * z[7];
z[30] = abb[23] + abb[25];
z[37] = (T(1) / T(16)) * z[30];
z[19] = z[19] * z[37];
z[37] = z[28] + z[45];
z[45] = abb[27] * (T(1) / T(32));
z[37] = z[37] * z[45];
z[51] = abb[28] * (T(1) / T(32));
z[40] = z[40] * z[51];
z[3] = z[3] + z[19] + z[37] + z[40];
z[19] = -abb[47] + -abb[48] + abb[49];
z[3] = z[3] * z[19];
z[37] = -abb[35] + abb[33] * (T(1) / T(2));
z[40] = m1_set::bc<T>[0] * z[37];
z[40] = (T(1) / T(2)) * z[4] + z[40];
z[40] = z[21] * z[40];
z[53] = abb[35] + (T(3) / T(4)) * z[0];
z[57] = m1_set::bc<T>[0] * z[53];
z[57] = -z[4] + z[57];
z[57] = z[50] * z[57];
z[4] = -z[4] + z[39];
z[39] = z[4] * z[17];
z[39] = z[39] + z[40] + z[57];
z[28] = z[28] * z[35];
z[6] = -z[6] + z[28] + z[39] + (T(1) / T(8)) * z[44] + -z[56];
z[28] = abb[52] * (T(1) / T(8));
z[6] = z[6] * z[28];
z[32] = z[32] + z[39];
z[32] = (T(1) / T(8)) * z[32] + (T(-1) / T(16)) * z[55];
z[32] = abb[50] * z[32];
z[4] = abb[18] * z[4];
z[24] = abb[19] * z[24];
z[4] = z[4] + z[24];
z[24] = abb[57] * (T(1) / T(32));
z[4] = z[4] * z[24];
z[39] = -abb[0] + abb[7];
z[39] = abb[57] * z[39];
z[40] = abb[18] * abb[50];
z[44] = abb[29] * z[19];
z[39] = z[39] + z[40] + -z[44];
z[39] = (T(1) / T(64)) * z[39];
z[40] = abb[43] * z[39];
z[44] = abb[22] * (T(1) / T(32));
z[44] = z[19] * z[44];
z[5] = z[5] * z[44];
z[55] = z[19] * z[30];
z[56] = abb[42] * z[55];
z[49] = abb[55] * z[49];
z[49] = z[49] + (T(-1) / T(2)) * z[55];
z[49] = abb[41] * z[49];
z[3] = abb[62] + abb[63] + z[3] + z[4] + z[5] + z[6] + z[8] + z[10] + z[11] + z[14] + z[32] + z[40] + (T(1) / T(16)) * z[49] + (T(-1) / T(32)) * z[56];
z[4] = prod_pow(abb[33], 2);
z[5] = (T(1) / T(2)) * z[4];
z[6] = abb[58] + z[5];
z[8] = abb[32] * abb[33];
z[10] = (T(1) / T(2)) * z[8];
z[11] = abb[31] * (T(1) / T(2));
z[14] = -abb[33] + z[11];
z[14] = abb[31] * z[14];
z[32] = abb[33] + z[20];
z[32] = abb[35] * z[32];
z[14] = abb[59] + -abb[60] + -z[6] + z[10] + z[14] + z[32];
z[32] = abb[30] + -abb[33];
z[40] = abb[34] * (T(1) / T(2));
z[49] = -abb[31] + z[40];
z[55] = -z[32] + z[49];
z[56] = z[40] * z[55];
z[57] = abb[39] * (T(1) / T(2));
z[58] = prod_pow(m1_set::bc<T>[0], 2);
z[59] = (T(1) / T(3)) * z[58];
z[38] = abb[31] + (T(-1) / T(2)) * z[38];
z[38] = -abb[35] + abb[30] * (T(3) / T(4)) + (T(1) / T(2)) * z[38];
z[38] = abb[30] * z[38];
z[14] = (T(1) / T(2)) * z[14] + z[38] + z[56] + z[57] + -z[59];
z[14] = z[14] * z[21];
z[38] = z[8] + -z[58];
z[56] = abb[30] * (T(1) / T(2));
z[60] = -abb[33] + z[56];
z[61] = -z[27] + -z[60];
z[61] = abb[30] * z[61];
z[62] = abb[32] + z[20];
z[63] = -abb[31] + z[62];
z[63] = abb[35] * z[63];
z[64] = abb[60] + z[63];
z[65] = -abb[32] + z[11];
z[65] = abb[31] * z[65];
z[40] = abb[32] + -z[40];
z[40] = abb[34] * z[40];
z[40] = abb[39] + -z[38] + z[40] + z[61] + z[64] + z[65];
z[17] = z[17] * z[40];
z[40] = z[27] + -z[56];
z[40] = abb[30] * z[40];
z[40] = abb[59] + z[40];
z[55] = abb[34] * z[55];
z[61] = prod_pow(abb[31], 2);
z[65] = abb[39] + (T(1) / T(2)) * z[61];
z[55] = -abb[38] + -z[8] + -z[40] + z[55] + z[64] + z[65];
z[23] = z[23] * z[55];
z[55] = abb[58] + -abb[59];
z[52] = abb[31] * z[52];
z[52] = -abb[37] + z[52] + z[55];
z[64] = -abb[30] + 3 * abb[35] + z[0];
z[64] = z[56] * z[64];
z[66] = abb[31] + -z[0];
z[66] = -abb[35] + (T(1) / T(2)) * z[66];
z[66] = abb[35] * z[66];
z[67] = abb[30] + z[0];
z[68] = -3 * abb[31] + -z[67];
z[68] = abb[34] + (T(1) / T(2)) * z[68];
z[68] = abb[34] * z[68];
z[52] = (T(1) / T(2)) * z[52] + z[64] + z[66] + z[68];
z[52] = z[35] * z[52];
z[64] = z[56] * z[67];
z[66] = abb[60] + z[59];
z[68] = abb[34] * z[32];
z[64] = -z[10] + z[64] + z[66] + -z[68];
z[69] = -abb[36] + -z[64];
z[41] = z[41] * z[69];
z[69] = (T(1) / T(6)) * z[58];
z[70] = -abb[31] * abb[33];
z[71] = abb[33] + -z[27];
z[71] = abb[35] * z[71];
z[70] = -abb[37] + -abb[38] + z[69] + z[70] + z[71];
z[70] = abb[3] * z[70];
z[15] = -abb[34] + z[15];
z[15] = abb[34] * z[15];
z[71] = abb[31] * abb[32];
z[15] = -abb[59] + -z[15] + z[71];
z[71] = (T(5) / T(6)) * z[58];
z[72] = -abb[60] + z[15] + -z[71];
z[73] = abb[8] * z[72];
z[31] = abb[37] * z[31];
z[31] = z[31] + z[70] + z[73];
z[70] = abb[2] * z[56];
z[29] = -z[29] + z[70];
z[29] = abb[30] * z[29];
z[62] = abb[35] * z[62];
z[6] = -z[6] + z[62];
z[6] = abb[2] * z[6];
z[62] = abb[2] * z[71];
z[62] = -z[6] + z[62];
z[29] = -z[29] + (T(1) / T(2)) * z[62];
z[62] = abb[30] * z[0];
z[62] = z[4] + -z[8] + z[62];
z[70] = abb[0] * (T(1) / T(8));
z[70] = z[62] * z[70];
z[10] = abb[38] + z[10] + z[59];
z[2] = z[2] + -z[56];
z[2] = abb[30] * z[2];
z[73] = abb[32] * abb[35];
z[73] = -abb[58] + z[73];
z[2] = z[2] + z[10] + -z[73];
z[43] = z[2] * z[43];
z[20] = -abb[31] + z[20];
z[20] = abb[35] * z[20];
z[74] = -abb[31] + z[56];
z[74] = abb[30] * z[74];
z[20] = abb[37] + z[20] + -z[74];
z[49] = abb[34] * z[49];
z[65] = z[49] + z[65];
z[74] = -abb[59] + z[65];
z[75] = abb[36] + -abb[58] + -z[20] + -z[74];
z[47] = z[47] * z[75];
z[75] = abb[1] * z[72];
z[76] = -abb[7] + abb[17];
z[76] = abb[9] + (T(1) / T(2)) * z[76];
z[76] = abb[36] * z[76];
z[76] = z[75] + -z[76];
z[77] = -z[58] + z[61];
z[77] = z[49] + (T(1) / T(2)) * z[77];
z[12] = z[12] * z[77];
z[78] = abb[64] * (T(1) / T(4)) + -z[12];
z[22] = -z[22] + -z[42];
z[79] = abb[38] * (T(1) / T(2));
z[22] = z[22] * z[79];
z[80] = abb[30] * z[32];
z[80] = -z[68] + z[80];
z[81] = z[9] * z[80];
z[82] = abb[61] * (T(1) / T(8));
z[33] = z[33] * z[82];
z[14] = z[14] + z[17] + z[22] + z[23] + -z[29] + (T(1) / T(4)) * z[31] + z[33] + z[41] + z[43] + z[47] + z[52] + z[70] + (T(-1) / T(2)) * z[76] + z[78] + z[81];
z[14] = z[14] * z[16];
z[11] = -z[0] + -z[11];
z[11] = abb[31] * z[11];
z[16] = -abb[33] * abb[35];
z[11] = z[5] + z[11] + z[16] + z[40] + z[71];
z[16] = abb[31] + abb[34] * (T(-3) / T(4)) + z[1] + z[56];
z[16] = abb[34] * z[16];
z[11] = (T(1) / T(2)) * z[11] + z[16] + -z[57] + z[79];
z[11] = abb[5] * z[11];
z[16] = -abb[10] + abb[2] * (T(5) / T(3));
z[17] = (T(1) / T(2)) * z[58];
z[16] = z[16] * z[17];
z[22] = prod_pow(abb[35], 2);
z[23] = z[22] * z[34];
z[6] = -z[6] + z[16] + z[23];
z[16] = z[36] * z[56];
z[16] = z[16] + -z[26];
z[16] = abb[30] * z[16];
z[6] = (T(1) / T(2)) * z[6] + z[16];
z[16] = z[27] + -z[32];
z[16] = abb[30] * z[16];
z[23] = -abb[33] * z[27];
z[16] = -abb[39] + z[16] + z[23] + z[68] + -z[69];
z[16] = z[16] * z[42];
z[20] = z[20] + z[65];
z[23] = abb[36] + -z[20];
z[23] = z[9] * z[23];
z[26] = -z[59] + z[73];
z[31] = z[46] + -z[56];
z[31] = abb[30] * z[31];
z[31] = z[5] + -z[26] + z[31];
z[31] = z[31] * z[50];
z[33] = z[22] + -z[61];
z[34] = -abb[35] + z[56];
z[34] = abb[30] * z[34];
z[33] = (T(1) / T(2)) * z[33] + z[34] + -z[49];
z[34] = z[21] * z[33];
z[17] = z[5] + z[17];
z[36] = abb[30] * z[60];
z[40] = z[15] + -z[17] + -z[36];
z[40] = abb[1] * z[40];
z[41] = abb[7] + z[54];
z[41] = z[41] * z[79];
z[11] = -z[6] + z[11] + z[16] + z[23] + z[31] + z[34] + (T(1) / T(2)) * z[40] + z[41] + -z[78];
z[11] = z[11] * z[13];
z[8] = z[4] + z[8];
z[13] = z[53] + -z[56];
z[13] = abb[30] * z[13];
z[8] = (T(1) / T(4)) * z[8] + z[13] + -z[26];
z[8] = z[8] * z[50];
z[13] = z[17] + -z[22];
z[16] = abb[30] * (T(1) / T(4)) + z[37];
z[16] = abb[30] * z[16];
z[13] = (T(-1) / T(2)) * z[13] + z[16];
z[13] = z[13] * z[21];
z[16] = z[54] * z[79];
z[17] = -abb[32] + z[32];
z[17] = abb[30] * z[17];
z[17] = z[17] + z[38];
z[22] = abb[7] * z[17];
z[8] = z[8] + z[13] + z[16] + (T(-1) / T(8)) * z[22];
z[13] = -z[33] * z[35];
z[9] = -z[9] * z[20];
z[16] = abb[18] * abb[61];
z[9] = z[8] + z[9] + -z[12] + z[13] + (T(-1) / T(8)) * z[16] + -z[29];
z[9] = z[9] * z[28];
z[12] = abb[0] * z[62];
z[12] = z[12] + z[16];
z[5] = -z[5] + -z[36] + z[66];
z[5] = abb[1] * z[5];
z[4] = z[4] + -z[58];
z[4] = (T(1) / T(2)) * z[4] + z[36];
z[13] = z[4] * z[21];
z[16] = -abb[32] + -3 * z[32];
z[16] = abb[30] * z[16];
z[16] = z[16] + z[38];
z[16] = (T(1) / T(4)) * z[16] + z[68];
z[16] = abb[7] * z[16];
z[20] = abb[9] * z[80];
z[21] = abb[60] + -z[69];
z[21] = abb[14] * z[21];
z[5] = z[5] + (T(1) / T(4)) * z[12] + z[13] + z[16] + z[20] + -z[21];
z[5] = z[5] * z[18];
z[6] = -z[6] + z[8];
z[6] = abb[50] * z[6];
z[8] = z[0] + z[27];
z[8] = abb[35] * z[8];
z[12] = abb[31] + -abb[34] + z[67];
z[12] = abb[34] * z[12];
z[0] = abb[31] * z[0];
z[13] = abb[30] * z[46];
z[0] = -abb[37] + z[0] + -z[8] + -z[12] + z[13] + z[55];
z[0] = z[0] * z[19];
z[8] = abb[36] * z[19];
z[0] = z[0] + z[8];
z[0] = z[0] * z[45];
z[1] = z[1] + z[27];
z[1] = abb[30] * z[1];
z[1] = abb[37] + -z[1] + -z[10] + z[63] + z[74];
z[1] = z[1] * z[19];
z[1] = z[1] + -z[8];
z[1] = -z[1] * z[7];
z[7] = z[15] + -z[58];
z[7] = abb[5] * z[7];
z[10] = z[48] * z[77];
z[7] = -z[7] + z[10] + z[21] + z[75];
z[10] = (T(-1) / T(16)) * z[25];
z[7] = z[7] * z[10];
z[10] = z[19] * z[64];
z[8] = z[8] + z[10];
z[8] = z[8] * z[51];
z[10] = (T(-1) / T(32)) * z[30];
z[10] = z[10] * z[19] * z[72];
z[12] = abb[18] * z[17];
z[4] = -abb[19] * z[4];
z[4] = z[4] + (T(-1) / T(2)) * z[12];
z[4] = z[4] * z[24];
z[12] = -abb[61] * z[39];
z[2] = z[2] * z[44];
z[13] = abb[9] * abb[52];
z[15] = -abb[7] + abb[9];
z[15] = abb[56] * z[15];
z[13] = z[13] + z[15];
z[13] = abb[36] * z[13];
z[0] = abb[44] + abb[45] + z[0] + z[1] + z[2] + z[4] + z[5] + (T(1) / T(8)) * z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + (T(1) / T(16)) * z[13] + z[14];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_455_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.0258587171738000399774921044042944740259148750644638551516258157"),stof<T>("-0.40174967964374244643557595952862298073520209320976199727113019872")}, std::complex<T>{stof<T>("0.71139208474349293419636000264885361495551924473021304202603627221"),stof<T>("-0.90547547218371322812297658047063936132169816395277400480248082064")}, std::complex<T>{stof<T>("0.12999797950105475637272322004832218410277322408176031662063887682"),stof<T>("-0.41085942189242198338479599540315379086897984976985439939950975612")}, std::complex<T>{stof<T>("0.71139208474349293419636000264885361495551924473021304202603627221"),stof<T>("-0.90547547218371322812297658047063936132169816395277400480248082064")}, std::complex<T>{stof<T>("0.18777331240759861174116785172115519051324539620179252919599363809"),stof<T>("-0.42461434373862124815611804491320722074434539090298304420290379136")}, std::complex<T>{stof<T>("-0.41319192711908536673096417744608257878214000263055639754336055018"),stof<T>("0.03548055235680441926591014528407026326521948207777702247680579092")}, std::complex<T>{stof<T>("0.24262455055120729895431117267383898166367269247833256352496631353"),stof<T>("-0.14526319143004323559361219370897325063142983277059844657387255908")}, std::complex<T>{stof<T>("0.17294983522561872023479601573924629543765893637965493551795324851"),stof<T>("-0.23224357088845982643188862976021287177099102762695359174229671081")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_455_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_455_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(128)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[50] + abb[52] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[50] + -abb[52] + -abb[54];
z[1] = abb[33] + -abb[35];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_455_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (-5 * v[0] + -v[1] + -v[2] + v[3] + 2 * v[4] + m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[1] * (T(1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[50] + abb[52] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + abb[33] * (T(1) / T(2));
z[0] = abb[33] * z[0];
z[1] = -abb[30] + abb[35] * (T(1) / T(2));
z[1] = abb[35] * z[1];
z[0] = z[0] + -z[1];
z[1] = abb[50] + abb[52] + abb[54];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_455_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[52] + abb[54] + abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[52] + abb[54] + abb[55];
z[1] = abb[31] + -abb[35];
return abb[12] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_455_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(512)) * (v[1] + v[2] + -v[3] + -v[4]) * (m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + 4 * (-4 + -3 * v[1] + -3 * v[2] + v[3] + 3 * v[4] + m1_set::bc<T>[1] * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])))) / prod_pow(tend, 2);
c[1] = ((2 + 2 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(-1) / T(64)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[52] + abb[54] + abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + -abb[31] + abb[35];
z[0] = abb[35] * z[0];
z[1] = abb[30] * abb[31];
z[0] = -abb[36] + abb[37] + abb[39] + z[0] + z[1];
z[1] = abb[52] + abb[54] + abb[55];
return abb[12] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_455_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.35831535424739108749052910012598616494601268057555492893757463245"),stof<T>("0.50909878581567110859496318142889521344901398312785653535431398459")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,65> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[44] = SpDLog_f_4_455_W_17_Im(t, path, abb);
abb[45] = SpDLog_f_4_455_W_20_Im(t, path, abb);
abb[46] = SpDLogQ_W_80(k,dl,dlr).imag();
abb[62] = SpDLog_f_4_455_W_17_Re(t, path, abb);
abb[63] = SpDLog_f_4_455_W_20_Re(t, path, abb);
abb[64] = SpDLogQ_W_80(k,dl,dlr).real();

                    
            return f_4_455_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_455_DLogXconstant_part(base_point<T>, kend);
	value += f_4_455_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_455_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_455_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_455_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_455_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_455_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_455_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
