/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_191.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_191_abbreviated (const std::array<T,48>& abb) {
T z[52];
z[0] = 3 * abb[22];
z[1] = 2 * abb[24];
z[2] = abb[21] + -z[0] + z[1];
z[2] = abb[6] * z[2];
z[3] = abb[0] * abb[22];
z[4] = abb[21] + -abb[22];
z[5] = abb[4] * z[4];
z[6] = -abb[0] + -abb[5];
z[6] = abb[21] * z[6];
z[7] = 2 * abb[21];
z[8] = -abb[22] + -abb[24] + z[7];
z[8] = abb[1] * z[8];
z[9] = abb[5] * abb[25];
z[10] = abb[22] + -abb[25] + z[7];
z[10] = abb[2] * z[10];
z[2] = z[2] + z[3] + z[5] + z[6] + z[8] + z[9] + z[10];
z[6] = 2 * abb[6];
z[8] = 4 * abb[1];
z[9] = -abb[13] + z[8];
z[10] = 2 * abb[4];
z[11] = z[6] + z[9] + -z[10];
z[12] = 2 * abb[2];
z[13] = z[11] + -z[12];
z[13] = abb[18] * z[13];
z[14] = 4 * abb[3];
z[15] = abb[22] + -abb[24];
z[16] = z[14] * z[15];
z[17] = 2 * abb[20];
z[18] = abb[0] + abb[13];
z[19] = -5 * abb[1] + -abb[3] + -abb[6] + z[18];
z[19] = z[17] * z[19];
z[20] = abb[0] + abb[6];
z[21] = abb[2] + 3 * abb[4] + -z[20];
z[21] = abb[23] * z[21];
z[2] = 2 * z[2] + z[13] + z[16] + z[19] + z[21];
z[2] = abb[23] * z[2];
z[13] = 2 * abb[3];
z[16] = 2 * abb[1];
z[6] = -abb[13] + -z[6] + z[13] + z[16];
z[6] = abb[23] * z[6];
z[19] = abb[1] + abb[6];
z[21] = abb[21] + -abb[24];
z[21] = -z[19] * z[21];
z[22] = abb[21] + abb[24];
z[13] = -z[13] * z[22];
z[23] = abb[3] + z[19];
z[17] = z[17] * z[23];
z[23] = -abb[18] * z[9];
z[24] = abb[19] * z[16];
z[6] = z[6] + z[13] + z[17] + 2 * z[21] + z[23] + z[24];
z[6] = abb[19] * z[6];
z[13] = abb[2] + -abb[12];
z[17] = -abb[21] + abb[25];
z[13] = z[13] * z[17];
z[17] = abb[8] + -z[16];
z[17] = z[4] * z[17];
z[21] = abb[10] * z[15];
z[23] = 2 * abb[22];
z[24] = -abb[24] + z[23];
z[25] = -3 * abb[21] + z[24];
z[25] = abb[6] * z[25];
z[13] = -z[5] + z[13] + 2 * z[17] + -z[21] + z[25];
z[17] = -2 * abb[0] + 6 * abb[1] + 3 * abb[8] + -abb[13];
z[25] = -abb[2] + -abb[4] + -abb[6] + -z[17];
z[25] = abb[18] * z[25];
z[13] = 2 * z[13] + z[25];
z[13] = abb[18] * z[13];
z[25] = 3 * abb[6];
z[26] = 2 * abb[8];
z[27] = 7 * abb[1] + -abb[3] + z[10] + z[25] + -z[26];
z[27] = z[4] * z[27];
z[28] = 4 * abb[6] + z[10];
z[18] = 12 * abb[1] + -z[18] + -z[26] + z[28];
z[29] = abb[18] * z[18];
z[27] = z[27] + z[29];
z[29] = abb[0] + -20 * abb[1] + -4 * abb[4] + -8 * abb[6] + 7 * abb[8];
z[29] = abb[20] * z[29];
z[27] = 2 * z[27] + z[29];
z[27] = abb[20] * z[27];
z[1] = abb[22] + z[1];
z[1] = abb[22] * z[1];
z[29] = prod_pow(abb[24], 2);
z[1] = -z[1] + 3 * z[29];
z[29] = z[7] * z[15];
z[30] = 2 * abb[26];
z[29] = -z[1] + z[29] + -z[30];
z[29] = abb[10] * z[29];
z[31] = prod_pow(abb[22], 2);
z[32] = -z[30] + z[31];
z[33] = abb[21] + z[23];
z[34] = abb[21] * z[33];
z[35] = -abb[25] + z[4];
z[35] = abb[25] * z[35];
z[35] = z[32] + z[34] + 2 * z[35];
z[35] = abb[2] * z[35];
z[36] = abb[24] + z[4];
z[36] = z[7] * z[36];
z[1] = z[1] + -z[30] + z[36];
z[1] = abb[3] * z[1];
z[36] = abb[21] * z[4];
z[37] = abb[18] + z[4];
z[38] = abb[18] * z[37];
z[39] = abb[20] + -2 * z[37];
z[39] = abb[20] * z[39];
z[36] = z[36] + z[38] + z[39];
z[38] = 2 * abb[14];
z[36] = z[36] * z[38];
z[39] = abb[15] + abb[17];
z[39] = abb[29] * z[39];
z[40] = abb[2] + -abb[5];
z[40] = abb[27] * z[40];
z[39] = abb[47] + z[39] + z[40];
z[40] = abb[0] * z[32];
z[41] = abb[0] + -abb[5];
z[42] = prod_pow(abb[21], 2);
z[43] = -z[41] * z[42];
z[0] = z[0] + -z[22];
z[0] = abb[21] * z[0];
z[22] = -abb[22] * z[24];
z[0] = z[0] + z[22];
z[0] = z[0] * z[16];
z[22] = -abb[21] + z[23];
z[22] = abb[21] * z[22];
z[22] = z[22] + -z[32];
z[22] = abb[4] * z[22];
z[23] = 3 * abb[25] + -2 * z[4];
z[23] = abb[25] * z[23];
z[23] = z[23] + -z[34];
z[23] = abb[12] * z[23];
z[31] = 6 * abb[26] + -4 * abb[39] + -z[31] + z[34];
z[31] = abb[6] * z[31];
z[32] = 2 * abb[28];
z[34] = -abb[0] + z[9];
z[34] = z[32] * z[34];
z[44] = 2 * abb[13];
z[28] = -3 * abb[0] + 18 * abb[1] + abb[8] + z[28] + -z[44];
z[45] = 4 * abb[14];
z[46] = z[28] + -z[45];
z[46] = abb[40] * z[46];
z[47] = -abb[5] * prod_pow(abb[25], 2);
z[48] = -abb[16] * abb[45];
z[19] = -abb[8] + z[19];
z[49] = abb[44] * z[19];
z[50] = abb[3] + -abb[6];
z[51] = abb[42] * z[50];
z[0] = z[0] + z[1] + z[2] + z[6] + z[13] + z[22] + z[23] + z[27] + z[29] + z[31] + z[34] + z[35] + z[36] + 2 * z[39] + z[40] + z[43] + z[46] + z[47] + z[48] + -4 * z[49] + -6 * z[51];
z[1] = -z[4] * z[15];
z[2] = abb[20] + -z[4];
z[2] = abb[20] * z[2];
z[6] = -abb[20] + -z[15];
z[6] = abb[23] * z[6];
z[13] = abb[20] + -abb[21];
z[22] = abb[23] + -abb[24] + -z[13];
z[22] = abb[19] * z[22];
z[1] = z[1] + z[2] + z[6] + z[22] + z[30] + z[32];
z[1] = abb[7] * z[1];
z[2] = abb[19] + -abb[23];
z[6] = 2 * z[13];
z[6] = -z[2] * z[6];
z[13] = abb[39] + abb[44];
z[22] = prod_pow(m1_set::bc<T>[0], 2);
z[23] = abb[41] + z[22];
z[27] = abb[18] * z[7];
z[29] = -2 * abb[18] + 3 * abb[20];
z[29] = abb[20] * z[29];
z[6] = z[6] + 2 * z[13] + 4 * z[23] + z[27] + z[29] + -3 * z[42];
z[6] = abb[11] * z[6];
z[1] = z[1] + z[6];
z[6] = -4 * abb[0] + -abb[5] + -z[44];
z[6] = -35 * abb[1] + -20 * abb[2] + -26 * abb[6] + 61 * abb[8] + 8 * abb[12] + 2 * z[6] + -z[10] + -z[14] + -z[45];
z[6] = z[6] * z[22];
z[10] = -abb[4] + abb[12];
z[13] = 3 * abb[2] + -abb[5] + -z[10] + -z[20] + z[26];
z[13] = 8 * z[13];
z[20] = abb[43] * z[13];
z[16] = abb[3] + -abb[4] + abb[14] + -z[16];
z[16] = 16 * z[16];
z[22] = -abb[41] * z[16];
z[0] = abb[46] + 4 * z[0] + 8 * z[1] + (T(2) / T(3)) * z[6] + z[20] + z[22];
z[1] = -z[10] + z[12] + z[17] + z[25];
z[1] = abb[18] * z[1];
z[6] = -abb[20] * z[18];
z[8] = z[8] + -z[26];
z[4] = z[4] * z[8];
z[8] = -z[7] + 3 * z[15];
z[8] = abb[3] * z[8];
z[10] = abb[20] + -z[37];
z[10] = z[10] * z[38];
z[12] = abb[21] * z[41];
z[15] = abb[21] + abb[22] + abb[25];
z[15] = abb[12] * z[15];
z[17] = -abb[25] + -z[33];
z[17] = abb[2] * z[17];
z[18] = abb[21] + -z[24];
z[18] = abb[6] * z[18];
z[1] = z[1] + -z[3] + z[4] + z[5] + z[6] + z[8] + z[10] + z[12] + z[15] + z[17] + z[18] + -2 * z[21];
z[3] = -4 * abb[2] + 2 * abb[5] + -z[11];
z[3] = abb[23] * z[3];
z[4] = z[9] + z[14];
z[4] = abb[19] * z[4];
z[1] = 2 * z[1] + z[3] + z[4];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[16] * abb[36];
z[1] = -z[1] + z[3];
z[2] = -abb[18] + abb[20] + -z[2] + z[7];
z[2] = m1_set::bc<T>[0] * z[2];
z[2] = abb[30] + 2 * abb[32] + abb[35] + z[2];
z[2] = abb[11] * z[2];
z[3] = abb[6] * abb[30];
z[4] = abb[35] * z[19];
z[2] = -z[2] + z[3] + z[4];
z[3] = abb[34] * z[13];
z[4] = -16 * abb[14] + 4 * z[28];
z[4] = abb[31] * z[4];
z[5] = abb[33] * z[50];
z[6] = -abb[32] * z[16];
z[1] = abb[37] + 8 * abb[38] + -4 * z[1] + -16 * z[2] + z[3] + z[4] + -24 * z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_191_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_191_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_191_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(2)) * (-v[3] + v[5]) * (-12 * v[0] + 4 * v[1] + 6 * m1_set::bc<T>[1] * (4 + v[3] + v[5]) + -3 * (8 + 3 * v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (-4 * (-1 + 3 * m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[23];
z[1] = abb[20] + -z[0];
z[1] = abb[20] * z[1];
z[0] = -abb[19] + z[0];
z[0] = abb[19] * z[0];
z[0] = -abb[28] + z[0] + z[1];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_191_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (-1 * v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[19] + abb[20];
return 8 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_191_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("108.73230113517543633539838165421209896434774473090023053018916545"),stof<T>("125.06160585247811893602798510999260552014628689035106292502106774")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 201});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,48> abb = {dl[0], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W18(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W33(k,dl), dlog_W123(k,dv), dlog_W126(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_8(k), f_2_14(k), f_2_16(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_10_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_10_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}};
abb[37] = SpDLog_f_3_191_W_16_Im(t, path, abb);
abb[46] = SpDLog_f_3_191_W_16_Re(t, path, abb);
{
auto c = dlog_W184(k,dv) * f_2_33(k);
abb[47] = c.real();
abb[38] = c.imag();
SpDLog_Sigma5<T,201,183>(k, dv, abb[47], abb[38], f_2_33_series_coefficients<T>);
}

                    
            return f_3_191_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_191_DLogXconstant_part(base_point<T>, kend);
	value += f_3_191_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_191_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_191_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_191_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_191_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_191_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_191_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
