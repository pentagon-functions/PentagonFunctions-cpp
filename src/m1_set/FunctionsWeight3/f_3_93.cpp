/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_93.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_93_abbreviated (const std::array<T,28>& abb) {
T z[28];
z[0] = 2 * abb[4];
z[1] = 2 * abb[9];
z[2] = z[0] + -z[1];
z[3] = 2 * abb[6];
z[4] = abb[1] + abb[2];
z[5] = z[2] + z[3] + z[4];
z[6] = abb[3] + -abb[8];
z[7] = abb[0] + z[6];
z[8] = z[5] + z[7];
z[8] = abb[14] * z[8];
z[9] = 2 * abb[1];
z[0] = abb[2] + -abb[9] + z[0] + z[9];
z[10] = -abb[0] + z[0];
z[10] = abb[16] * z[10];
z[0] = -z[0] + -z[6];
z[0] = abb[13] * z[0];
z[11] = abb[2] * (T(1) / T(2));
z[3] = -abb[0] + abb[9] + abb[1] * (T(1) / T(2)) + -z[3] + z[11];
z[3] = abb[15] * z[3];
z[12] = abb[18] * z[7];
z[7] = -abb[17] * z[7];
z[0] = z[0] + z[3] + z[7] + z[8] + z[10] + z[12];
z[0] = abb[15] * z[0];
z[3] = abb[9] + abb[10];
z[7] = 2 * abb[3];
z[10] = 3 * abb[5];
z[13] = abb[0] + -abb[8];
z[11] = -5 * abb[4] + abb[1] * (T(-11) / T(2)) + z[3] + -z[7] + z[10] + -z[11] + -z[13];
z[11] = abb[14] * z[11];
z[14] = 3 * abb[3];
z[15] = 2 * abb[10];
z[16] = z[14] + -z[15];
z[17] = 4 * abb[5] + -z[16];
z[13] = 10 * abb[1] + 8 * abb[4] + -z[1] + z[13] + -z[17];
z[18] = -abb[16] * z[13];
z[19] = abb[8] + -abb[10];
z[19] = abb[17] * z[19];
z[11] = z[11] + z[18] + 2 * z[19];
z[11] = abb[14] * z[11];
z[18] = 3 * abb[8];
z[19] = abb[0] + abb[3];
z[20] = z[15] + -z[18] + z[19];
z[20] = abb[17] * z[20];
z[21] = 2 * abb[0];
z[22] = -abb[9] + z[21];
z[23] = 6 * abb[4];
z[17] = 8 * abb[1] + -abb[2] + -abb[8] + -z[17] + z[22] + z[23];
z[17] = abb[16] * z[17];
z[13] = abb[14] * z[13];
z[24] = 3 * abb[4];
z[25] = 4 * abb[1] + abb[3] + z[24];
z[3] = abb[5] + z[3] + -z[25];
z[3] = abb[13] * z[3];
z[3] = z[3] + -z[12] + z[13] + z[17] + z[20];
z[3] = abb[13] * z[3];
z[13] = abb[5] * (T(3) / T(2));
z[17] = abb[0] * (T(1) / T(2));
z[7] = abb[10] + z[7] + z[13] + z[17] + -z[18];
z[7] = abb[17] * z[7];
z[18] = abb[0] + 4 * z[6] + z[10];
z[20] = -abb[18] * z[18];
z[7] = z[7] + z[20];
z[7] = abb[17] * z[7];
z[20] = 2 * abb[5];
z[26] = -abb[0] + z[20];
z[14] = 9 * abb[1] + abb[7] + -4 * abb[8] + -z[1] + z[14] + z[23] + -z[26];
z[23] = abb[25] * z[14];
z[24] = -3 * abb[1] + abb[2] + abb[7] + abb[8] * (T(-5) / T(2)) + abb[3] * (T(-1) / T(2)) + z[10] + -z[17] + -z[24];
z[24] = abb[20] * z[24];
z[13] = z[6] + z[13] + -z[17];
z[13] = prod_pow(abb[18], 2) * z[13];
z[16] = 3 * abb[0] + -abb[8] + z[16];
z[16] = abb[17] * z[16];
z[17] = abb[2] + abb[10] + z[21] + -z[25];
z[17] = abb[16] * z[17];
z[12] = -z[12] + z[16] + z[17];
z[12] = abb[16] * z[12];
z[16] = abb[0] + abb[4];
z[17] = abb[3] + abb[7];
z[25] = -abb[9] + abb[10];
z[16] = abb[1] * (T(-11) / T(6)) + abb[2] * (T(-3) / T(4)) + abb[6] * (T(5) / T(3)) + abb[5] * (T(7) / T(3)) + (T(-13) / T(12)) * z[16] + (T(-1) / T(6)) * z[17] + (T(1) / T(3)) * z[25];
z[16] = prod_pow(m1_set::bc<T>[0], 2) * z[16];
z[17] = abb[26] * z[5];
z[25] = abb[19] + abb[20];
z[15] = z[15] * z[25];
z[25] = -abb[27] * z[18];
z[19] = abb[19] * z[19];
z[27] = abb[11] + abb[12];
z[27] = abb[21] * z[27];
z[0] = z[0] + z[3] + z[7] + z[11] + z[12] + z[13] + z[15] + z[16] + z[17] + -2 * z[19] + z[23] + z[24] + z[25] + z[27];
z[2] = 2 * abb[2] + z[2] + z[6] + z[9] + z[26];
z[2] = abb[13] * z[2];
z[3] = -z[6] + -z[10] + z[21];
z[6] = -abb[17] + abb[18];
z[3] = z[3] * z[6];
z[6] = -abb[4] + -z[4] + z[20] + -z[22];
z[6] = abb[16] * z[6];
z[1] = 4 * abb[6] + -z[1] + -z[4] + z[21];
z[1] = abb[15] * z[1];
z[1] = z[1] + z[2] + z[3] + 2 * z[6] + -z[8];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[22] * z[14];
z[3] = abb[23] * z[5];
z[4] = -abb[24] * z[18];
z[1] = z[1] + z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_93_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_93_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_93_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-2.693539898146208872607940396210012816450004735333933301116605323"),stof<T>("36.149934647915943889682006694308761796355242626495063442425384573")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dl[5], dl[2], dlog_W7(k,dl), dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W92(k,dl), dlog_W123(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_8(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k)};

                    
            return f_3_93_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_93_DLogXconstant_part(base_point<T>, kend);
	value += f_3_93_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_93_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_93_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_93_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_93_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_93_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_93_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
