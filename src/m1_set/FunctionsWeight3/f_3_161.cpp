/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_161.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_161_abbreviated (const std::array<T,48>& abb) {
T z[57];
z[0] = -abb[18] + 4 * abb[20];
z[1] = 3 * abb[19];
z[2] = 3 * abb[21];
z[3] = -z[0] + z[1] + z[2];
z[3] = abb[3] * z[3];
z[4] = -abb[18] + abb[20];
z[5] = -abb[21] + z[4];
z[6] = 2 * abb[10];
z[7] = z[5] * z[6];
z[8] = abb[19] + -abb[20];
z[9] = 3 * abb[6];
z[10] = z[8] * z[9];
z[11] = 2 * abb[20];
z[1] = abb[18] + -z[1] + z[11];
z[12] = -abb[5] * z[1];
z[13] = 2 * abb[21];
z[1] = z[1] + -z[13];
z[1] = abb[11] * z[1];
z[13] = -3 * z[8] + z[13];
z[13] = abb[2] * z[13];
z[14] = -abb[4] * z[5];
z[15] = 3 * abb[7];
z[16] = abb[21] * z[15];
z[1] = z[1] + z[3] + z[7] + z[10] + z[12] + z[13] + z[14] + z[16];
z[3] = 2 * abb[11];
z[10] = z[3] + z[6];
z[12] = 7 * abb[4];
z[13] = -abb[5] + z[15];
z[14] = -abb[2] + -8 * abb[3] + z[10] + -z[12] + z[13];
z[14] = abb[23] * z[14];
z[1] = 2 * z[1] + z[14];
z[1] = abb[23] * z[1];
z[14] = 4 * abb[7];
z[15] = z[9] + z[14];
z[16] = 5 * abb[3];
z[12] = -4 * abb[2] + abb[5] + -z[12] + z[15] + -z[16];
z[12] = abb[23] * z[12];
z[17] = 3 * abb[12];
z[18] = -z[9] + z[17];
z[18] = abb[18] * z[18];
z[19] = 2 * abb[9];
z[20] = z[5] * z[19];
z[21] = abb[2] + -abb[5];
z[22] = 2 * abb[18];
z[23] = abb[20] + z[22];
z[21] = z[21] * z[23];
z[0] = abb[21] + -z[0];
z[0] = abb[4] * z[0];
z[24] = 2 * z[4];
z[25] = abb[21] + z[24];
z[25] = abb[7] * z[25];
z[4] = abb[21] + -3 * z[4];
z[4] = abb[3] * z[4];
z[26] = abb[5] + abb[6];
z[27] = -abb[12] + z[26];
z[28] = abb[4] + z[27];
z[29] = -abb[2] + z[28];
z[29] = abb[25] * z[29];
z[0] = z[0] + z[4] + z[12] + z[18] + z[20] + z[21] + z[25] + 3 * z[29];
z[4] = 2 * abb[5] + z[9];
z[12] = abb[9] + z[17];
z[12] = -abb[2] + -10 * abb[4] + -abb[7] + -z[4] + 2 * z[12] + -z[16];
z[12] = abb[22] * z[12];
z[0] = 2 * z[0] + z[12];
z[0] = abb[22] * z[0];
z[12] = 4 * z[8];
z[16] = z[2] + -z[12];
z[16] = abb[21] * z[16];
z[18] = 2 * abb[43];
z[20] = -abb[18] + z[8];
z[20] = abb[19] * z[20];
z[21] = 4 * z[20];
z[25] = prod_pow(abb[18], 2);
z[29] = -6 * abb[18] + abb[20];
z[29] = abb[20] * z[29];
z[16] = 6 * abb[31] + z[16] + z[18] + -z[21] + z[25] + z[29];
z[16] = abb[3] * z[16];
z[29] = 4 * abb[9] + -z[13] + -z[17];
z[30] = 5 * abb[4];
z[31] = abb[3] + z[30];
z[32] = 2 * abb[2];
z[33] = z[29] + z[31] + z[32];
z[34] = abb[22] * z[33];
z[35] = 2 * abb[3];
z[36] = -abb[4] + abb[7];
z[37] = 3 * abb[0] + abb[2] + -abb[10] + -abb[11] + z[35] + -z[36];
z[37] = z[5] * z[37];
z[38] = -abb[9] * z[5];
z[37] = z[37] + z[38];
z[39] = -z[9] + z[32];
z[31] = 7 * abb[7] + -4 * abb[10] + -abb[11] + -z[31] + -z[39];
z[40] = -abb[23] * z[31];
z[34] = z[34] + 2 * z[37] + z[40];
z[37] = abb[9] + abb[10];
z[40] = 4 * abb[11];
z[15] = 18 * abb[0] + 7 * abb[3] + -abb[4] + -abb[5] + z[15] + z[32] + -10 * z[37] + -z[40];
z[15] = abb[24] * z[15];
z[15] = z[15] + 2 * z[34];
z[15] = abb[24] * z[15];
z[34] = abb[6] + abb[7];
z[41] = 3 * abb[13];
z[34] = -4 * abb[1] + abb[11] + z[6] + -3 * z[34] + z[41];
z[42] = abb[41] * z[34];
z[17] = -z[17] + z[19];
z[43] = 4 * abb[8];
z[44] = -z[41] + z[43];
z[45] = abb[5] + abb[7] + z[17] + -z[44];
z[46] = abb[40] * z[45];
z[6] = abb[3] + abb[4] + -abb[5] + abb[11] + -z[6];
z[6] = abb[30] * z[6];
z[47] = abb[14] + abb[16];
z[47] = -abb[15] + -abb[17] + 2 * z[47];
z[48] = abb[39] * z[47];
z[6] = z[6] + -z[42] + -z[46] + -z[48];
z[2] = -z[2] + -z[12];
z[2] = abb[21] * z[2];
z[12] = -3 * abb[20] + z[22];
z[12] = abb[20] * z[12];
z[2] = -12 * abb[27] + -4 * abb[31] + z[2] + z[12] + -z[21] + -3 * z[25];
z[2] = abb[7] * z[2];
z[12] = -abb[20] * z[23];
z[23] = abb[18] + abb[20];
z[42] = -2 * abb[19] + z[23];
z[46] = -abb[21] + 2 * z[42];
z[46] = abb[21] * z[46];
z[12] = z[12] + -z[21] + -z[25] + z[46];
z[21] = 2 * abb[1];
z[12] = z[12] * z[21];
z[46] = abb[18] * abb[20];
z[20] = z[20] + z[46];
z[46] = 2 * abb[31];
z[48] = 2 * z[8];
z[49] = abb[21] + -z[48];
z[49] = abb[21] * z[49];
z[49] = 3 * abb[29] + z[18] + -2 * z[20] + z[46] + z[49];
z[49] = z[32] * z[49];
z[50] = 2 * abb[0];
z[27] = -abb[11] + z[27] + z[50];
z[27] = abb[26] * z[27];
z[28] = abb[29] * z[28];
z[51] = abb[2] + abb[6] + -abb[13] + z[36];
z[52] = abb[45] * z[51];
z[27] = -z[27] + z[28] + -z[52];
z[28] = -z[9] + z[41];
z[52] = abb[20] * z[22];
z[53] = prod_pow(abb[19], 2);
z[52] = z[52] + -z[53];
z[52] = -z[28] * z[52];
z[22] = -abb[20] + z[22];
z[54] = abb[20] * z[22];
z[54] = -z[25] + z[54];
z[55] = prod_pow(abb[21], 2);
z[55] = -z[54] + z[55];
z[55] = abb[4] * z[55];
z[56] = abb[20] * z[23];
z[25] = z[25] + z[56];
z[25] = 2 * z[25] + -3 * z[53];
z[25] = abb[5] * z[25];
z[17] = abb[4] + -z[4] + -z[17];
z[17] = z[17] * z[46];
z[29] = z[29] + z[30];
z[18] = z[18] * z[29];
z[29] = -abb[18] + abb[19];
z[30] = abb[21] * z[29];
z[30] = z[20] + z[30];
z[30] = z[30] * z[40];
z[24] = -abb[21] + z[24];
z[40] = -abb[21] * z[24];
z[40] = 6 * abb[27] + z[40] + -z[54];
z[40] = z[40] * z[50];
z[8] = abb[21] * z[8];
z[8] = z[8] + z[20];
z[8] = abb[8] * z[8];
z[20] = abb[13] + -z[26];
z[20] = prod_pow(abb[25], 2) * z[20];
z[0] = z[0] + z[1] + z[2] + 2 * z[6] + 8 * z[8] + z[12] + z[15] + z[16] + z[17] + z[18] + 3 * z[20] + z[25] + -6 * z[27] + z[30] + z[40] + z[49] + z[52] + z[55];
z[1] = 3 * abb[3] + -abb[5];
z[2] = -z[1] + z[36] + -z[39] + z[44];
z[6] = abb[46] * z[2];
z[8] = -abb[1] + abb[8];
z[12] = 7 * abb[2] + abb[3];
z[8] = -3 * z[8] + z[12] + z[13] + -z[41];
z[13] = abb[42] * z[8];
z[15] = abb[44] * z[31];
z[6] = z[6] + z[13] + -z[15];
z[13] = abb[6] + -abb[13] + abb[9] * (T(8) / T(3));
z[13] = -27 * abb[2] + abb[3] * (T(-139) / T(6)) + abb[0] * (T(-52) / T(3)) + abb[7] * (T(-47) / T(6)) + abb[1] * (T(-1) / T(3)) + abb[4] * (T(23) / T(6)) + abb[10] * (T(32) / T(3)) + abb[8] * (T(94) / T(3)) + 4 * z[13];
z[13] = prod_pow(m1_set::bc<T>[0], 2) * z[13];
z[15] = abb[2] + -abb[3] + abb[11] + -z[26];
z[15] = abb[28] * z[15];
z[0] = 2 * z[0] + 4 * z[6] + z[13] + 12 * z[15];
z[6] = 3 * abb[4];
z[1] = -12 * abb[0] + -6 * abb[7] + -z[1] + z[3] + z[6] + -z[9] + 8 * z[37];
z[1] = abb[24] * z[1];
z[9] = -abb[4] + abb[6];
z[9] = abb[5] + 2 * z[9];
z[9] = 3 * z[9] + -z[10] + -z[12] + z[14];
z[9] = abb[23] * z[9];
z[4] = -5 * abb[2] + 2 * abb[7] + z[4] + -z[6] + -z[19] + -z[35];
z[4] = abb[22] * z[4];
z[6] = -3 * abb[5] + z[28];
z[6] = z[6] * z[23];
z[3] = z[3] * z[29];
z[10] = -abb[21] + z[42];
z[10] = z[10] * z[21];
z[12] = -abb[19] + -abb[21] + z[22];
z[12] = z[12] * z[32];
z[13] = abb[21] + z[29];
z[13] = z[13] * z[43];
z[14] = -abb[4] * z[24];
z[15] = -abb[21] + -z[48];
z[15] = abb[7] * z[15];
z[5] = abb[0] * z[5];
z[11] = 3 * abb[18] + -abb[19] + -z[11];
z[11] = -abb[21] + 2 * z[11];
z[11] = abb[3] * z[11];
z[1] = z[1] + z[3] + z[4] + -4 * z[5] + z[6] + z[7] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + -2 * z[38];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[38] * z[2];
z[3] = abb[34] * z[8];
z[4] = abb[35] * z[33];
z[5] = -abb[33] * z[34];
z[6] = -abb[32] * z[45];
z[7] = -abb[36] * z[31];
z[8] = abb[47] * z[47];
z[9] = abb[37] * z[51];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + 3 * z[9];
z[1] = 4 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_161_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_161_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_161_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-7.51248222655846273724437061564510622645001589519470063777217179"),stof<T>("675.8712729906897502283737695621190500758504689017568104844749918")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,48> abb = {dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W22(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_14(k), f_2_19(k), f_2_20(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_10_im(k), f_2_11_im(k), f_2_12_im(k), f_2_22_im(k), f_2_26_im(k), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_10_re(k), f_2_11_re(k), f_2_12_re(k), f_2_22_re(k), f_2_26_re(k)};

                    
            return f_3_161_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_161_DLogXconstant_part(base_point<T>, kend);
	value += f_3_161_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_161_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_161_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_161_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_161_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_161_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_161_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
