/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_193.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_193_abbreviated (const std::array<T,46>& abb) {
T z[46];
z[0] = 3 * abb[2];
z[1] = 2 * abb[7];
z[2] = z[0] + -z[1];
z[3] = 4 * abb[1];
z[4] = -abb[6] + abb[13];
z[5] = -abb[5] + abb[10] + -z[2] + -z[3] + z[4];
z[5] = abb[18] * z[5];
z[6] = abb[20] + abb[24];
z[7] = 2 * abb[21];
z[8] = -z[6] + z[7];
z[8] = abb[1] * z[8];
z[9] = abb[19] * z[1];
z[10] = abb[19] + -abb[21];
z[11] = 2 * z[10];
z[12] = abb[5] * z[11];
z[13] = abb[2] * abb[20];
z[14] = abb[2] + abb[6];
z[15] = abb[21] * z[14];
z[16] = -abb[12] * abb[24];
z[17] = -abb[20] + abb[24];
z[17] = abb[3] * z[17];
z[6] = abb[8] * z[6];
z[18] = abb[2] + -abb[6];
z[19] = abb[19] * z[18];
z[20] = abb[0] * z[10];
z[21] = abb[0] + -abb[6] + abb[10];
z[22] = abb[1] + 2 * abb[5] + abb[7] + -z[21];
z[22] = abb[23] * z[22];
z[5] = z[5] + z[6] + z[8] + -z[9] + -z[12] + -z[13] + z[15] + z[16] + z[17] + z[19] + z[20] + z[22];
z[5] = abb[23] * z[5];
z[6] = 3 * abb[1];
z[8] = abb[3] + abb[8];
z[15] = abb[5] + abb[12];
z[16] = -abb[10] + z[15];
z[17] = abb[2] + -z[4] + z[6] + -z[8] + z[16];
z[17] = abb[23] * z[17];
z[19] = 2 * abb[2];
z[20] = abb[6] + z[19];
z[22] = 2 * abb[1];
z[23] = abb[0] + z[15] + -z[20] + -z[22];
z[23] = abb[22] * z[23];
z[0] = -abb[0] + abb[6] + z[0];
z[24] = abb[1] + -2 * abb[3] + abb[8] + z[0];
z[25] = -abb[21] + abb[24];
z[24] = z[24] * z[25];
z[3] = -abb[13] + z[3] + z[20];
z[26] = -z[1] + z[3];
z[27] = -abb[12] + z[26];
z[27] = abb[18] * z[27];
z[28] = abb[19] + z[25];
z[28] = abb[5] * z[28];
z[29] = abb[10] * abb[19];
z[17] = z[17] + z[23] + z[24] + z[27] + -z[28] + z[29];
z[17] = abb[22] * z[17];
z[23] = abb[15] * abb[43];
z[5] = abb[45] + z[5] + z[17] + z[23];
z[17] = 2 * abb[6];
z[23] = 2 * abb[13];
z[24] = -7 * abb[1] + -z[2] + z[8] + -z[17] + z[23];
z[24] = abb[22] * z[24];
z[27] = 6 * abb[1];
z[30] = z[20] + z[27];
z[23] = abb[14] + z[23];
z[31] = abb[11] + z[23];
z[30] = abb[0] + z[1] + -2 * z[30] + z[31];
z[32] = -abb[18] * z[30];
z[33] = abb[2] * abb[21];
z[13] = -z[13] + z[33];
z[33] = abb[0] + abb[11] + abb[14];
z[8] = 5 * abb[1] + z[8] + -z[33];
z[34] = abb[20] + -abb[21];
z[8] = z[8] * z[34];
z[2] = abb[3] + -abb[8] + z[2] + z[6];
z[2] = abb[23] * z[2];
z[2] = z[2] + z[8] + -z[13] + z[24] + z[32];
z[6] = 2 * abb[0];
z[8] = 4 * abb[2];
z[24] = -10 * abb[1] + -abb[3] + -abb[6] + -z[8];
z[24] = 7 * abb[7] + 3 * abb[11] + abb[14] + z[6] + 2 * z[24];
z[24] = abb[25] * z[24];
z[2] = 2 * z[2] + z[24];
z[2] = abb[25] * z[2];
z[24] = prod_pow(abb[24], 2);
z[32] = 2 * abb[42];
z[35] = prod_pow(abb[21], 2);
z[36] = prod_pow(abb[19], 2);
z[37] = prod_pow(abb[20], 2);
z[37] = z[24] + -z[32] + -z[35] + -z[36] + z[37];
z[37] = abb[0] * z[37];
z[38] = z[22] * z[34];
z[39] = abb[21] * z[18];
z[40] = -abb[2] + abb[12];
z[40] = abb[24] * z[40];
z[41] = abb[11] * abb[20];
z[29] = z[29] + z[38] + -z[39] + -z[40] + -z[41];
z[39] = 3 * abb[7];
z[6] = abb[5] + -z[6] + z[27] + z[39];
z[27] = -z[6] + -z[14] + z[23];
z[27] = abb[18] * z[27];
z[40] = abb[14] * z[34];
z[12] = z[12] + z[27] + -2 * z[29] + z[40];
z[12] = abb[18] * z[12];
z[27] = 2 * abb[27];
z[11] = abb[23] * z[11];
z[11] = z[11] + z[27] + z[35] + -z[36];
z[11] = abb[4] * z[11];
z[29] = abb[20] * z[7];
z[41] = abb[24] + -2 * z[34];
z[41] = abb[24] * z[41];
z[29] = z[29] + -3 * z[35] + z[41];
z[29] = abb[3] * z[29];
z[4] = -abb[3] + -z[4] + z[22];
z[4] = abb[29] * z[4];
z[41] = -abb[1] + -abb[2] + abb[7];
z[41] = abb[30] * z[41];
z[4] = z[4] + z[41];
z[41] = 2 * abb[10] + z[1] + -z[18];
z[36] = z[36] * z[41];
z[41] = 3 * abb[6] + z[19];
z[35] = z[35] * z[41];
z[41] = -abb[6] + -2 * abb[12];
z[41] = abb[24] * z[41];
z[13] = -2 * z[13] + z[41];
z[13] = abb[24] * z[13];
z[41] = abb[20] * z[34];
z[42] = -z[32] + z[41];
z[43] = abb[24] + z[34];
z[44] = -abb[24] * z[43];
z[44] = -z[42] + z[44];
z[45] = 2 * abb[8];
z[44] = z[44] * z[45];
z[34] = abb[24] * z[34];
z[34] = 4 * abb[42] + z[34] + -z[41];
z[34] = z[22] * z[34];
z[41] = abb[14] * z[42];
z[7] = -abb[20] + -z[7];
z[7] = abb[20] * z[7];
z[7] = z[7] + -z[32];
z[7] = abb[11] * z[7];
z[24] = z[24] + -z[27];
z[24] = abb[5] * z[24];
z[20] = -9 * abb[1] + -z[20] + z[23];
z[20] = 3 * abb[0] + -abb[7] + 2 * z[20];
z[23] = -abb[39] * z[20];
z[19] = -abb[20] * abb[21] * z[19];
z[27] = abb[26] * z[8];
z[32] = abb[2] + -abb[3];
z[32] = abb[28] * z[32];
z[42] = -abb[17] * abb[31];
z[2] = z[2] + 4 * z[4] + 2 * z[5] + z[7] + z[11] + z[12] + z[13] + z[19] + z[23] + z[24] + z[27] + z[29] + 6 * z[32] + z[34] + z[35] + z[36] + z[37] + z[41] + z[42] + z[44];
z[4] = z[15] + -z[45];
z[0] = abb[3] + -z[0] + z[4];
z[5] = abb[41] * z[0];
z[1] = abb[2] + abb[4] + -3 * abb[5] + -z[1] + z[21];
z[7] = abb[40] * z[1];
z[5] = z[5] + -z[7];
z[7] = abb[10] + -abb[12];
z[8] = -abb[6] + -abb[14] + -z[7] + z[8] + z[45];
z[11] = -abb[11] + abb[13];
z[8] = 13 * abb[1] + 2 * z[8] + -8 * z[11];
z[8] = abb[0] * (T(-8) / T(3)) + (T(1) / T(3)) * z[8] + z[39];
z[8] = prod_pow(m1_set::bc<T>[0], 2) * z[8];
z[11] = 8 * abb[16];
z[12] = -abb[43] * z[11];
z[2] = abb[44] + 4 * z[2] + -8 * z[5] + 2 * z[8] + z[12];
z[5] = abb[2] + z[17];
z[6] = z[5] + z[6] + z[7] + -z[31];
z[6] = abb[18] * z[6];
z[3] = -abb[10] + z[3] + z[4];
z[3] = abb[23] * z[3];
z[4] = abb[25] * z[30];
z[7] = z[43] * z[45];
z[8] = z[16] + -z[26];
z[8] = abb[22] * z[8];
z[5] = -abb[21] * z[5];
z[12] = abb[12] + z[14];
z[12] = abb[24] * z[12];
z[13] = -abb[3] * z[25];
z[14] = abb[20] + abb[21];
z[14] = abb[11] * z[14];
z[15] = -abb[10] + z[18];
z[15] = abb[19] * z[15];
z[16] = abb[19] + -z[43];
z[16] = abb[0] * z[16];
z[10] = abb[4] * z[10];
z[3] = z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + -z[9] + z[10] + z[12] + z[13] + z[14] + z[15] + z[16] + -z[28] + z[38] + -z[40];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = abb[8] + z[22];
z[4] = 2 * z[4] + -z[33];
z[4] = abb[35] * z[4];
z[3] = z[3] + z[4];
z[0] = -abb[34] * z[0];
z[1] = abb[33] * z[1];
z[4] = abb[15] * abb[36];
z[0] = abb[38] + z[0] + z[1] + z[4];
z[1] = -abb[32] * z[20];
z[0] = 2 * z[0] + z[1];
z[1] = -abb[36] * z[11];
z[0] = abb[37] + 4 * z[0] + z[1] + 8 * z[3];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_3_193_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_193_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_193_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(14)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (2 * (112 * m1_set::bc<T>[1] + -84 * m1_set::bc<T>[2] + 56 * m1_set::bc<T>[4] + -49 * v[1] + -35 * v[2] + 21 * v[3] + 36 * v[4] + 7 * v[5]) + 7 * (6 * v[0] + 8 * m1_set::bc<T>[1] * v[0] + -21 * m1_set::bc<T>[2] * v[0] + 8 * m1_set::bc<T>[1] * v[1] + m1_set::bc<T>[2] * v[1] + 8 * m1_set::bc<T>[1] * v[2] + -9 * m1_set::bc<T>[2] * v[2] + -8 * m1_set::bc<T>[1] * v[3] + 3 * m1_set::bc<T>[2] * v[3] + -16 * m1_set::bc<T>[1] * v[4] + 12 * m1_set::bc<T>[2] * v[4] + 4 * m1_set::bc<T>[4] * (v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -8 * m1_set::bc<T>[1] * v[5] + 9 * m1_set::bc<T>[2] * v[5]))) / prod_pow(tend, 2);
c[1] = (-4 * (4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[3];
z[0] = -abb[26] + 2 * abb[29] + abb[30];
z[1] = abb[18] + abb[20] + -abb[21];
z[2] = 5 * abb[25] + -2 * z[1];
z[2] = abb[25] * z[2];
z[1] = -2 * abb[25] + z[1];
z[1] = -abb[23] + 2 * z[1];
z[1] = abb[23] * z[1];
z[0] = 2 * z[0] + z[1] + z[2];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_193_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-2 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (-8 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[23] + abb[25];
return 16 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_193_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-55.599466336862840895947729581561863840716166685773996729621081368"),stof<T>("51.219747214480611570610758475011899739850558685514477065621386162")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 202});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,46> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W24(k,dl), dlog_W25(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W125(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_11_im(k), f_2_12_im(k), f_2_25_im(k), T{0}, T{0}, f_2_2_re(k), f_2_9_re(k), f_2_11_re(k), f_2_12_re(k), f_2_25_re(k), T{0}, T{0}};
abb[37] = SpDLog_f_3_193_W_17_Im(t, path, abb);
abb[44] = SpDLog_f_3_193_W_17_Re(t, path, abb);
{
auto c = dlog_W175(k,dv) * f_2_34(k);
abb[45] = c.real();
abb[38] = c.imag();
SpDLog_Sigma5<T,202,174>(k, dv, abb[45], abb[38], f_2_34_series_coefficients<T>);
}

                    
            return f_3_193_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_193_DLogXconstant_part(base_point<T>, kend);
	value += f_3_193_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_193_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_193_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_193_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_193_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_193_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_193_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
