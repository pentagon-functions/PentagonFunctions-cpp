/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_186.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_186_abbreviated (const std::array<T,46>& abb) {
T z[39];
z[0] = -abb[6] + abb[13];
z[1] = abb[11] + -abb[12];
z[2] = abb[2] + z[1];
z[3] = 3 * abb[1];
z[4] = abb[4] + abb[7];
z[5] = -abb[5] + z[4];
z[6] = -z[0] + z[2] + z[3] + -z[5];
z[6] = abb[23] * z[6];
z[7] = 4 * abb[1];
z[8] = 2 * abb[8];
z[9] = z[7] + -z[8];
z[10] = 2 * abb[5];
z[11] = z[0] + -z[10];
z[12] = -abb[11] + z[9] + -z[11];
z[12] = abb[18] * z[12];
z[13] = abb[0] + -abb[6];
z[14] = abb[2] + z[13];
z[15] = 2 * abb[1];
z[16] = abb[11] + -z[10] + z[14] + -z[15];
z[16] = abb[24] * z[16];
z[17] = 3 * abb[5];
z[18] = z[14] + -z[17];
z[19] = -abb[1] + 2 * abb[4] + -abb[7] + z[18];
z[20] = abb[22] * z[19];
z[6] = z[6] + z[12] + z[16] + z[20];
z[6] = abb[24] * z[6];
z[12] = abb[0] + abb[14];
z[16] = abb[10] + z[12];
z[20] = 2 * abb[7];
z[21] = -z[7] + z[16] + -z[20];
z[22] = -abb[40] * z[21];
z[23] = -abb[4] + -z[0] + z[15];
z[23] = abb[29] * z[23];
z[24] = abb[26] + -abb[30];
z[24] = abb[5] * z[24];
z[23] = z[23] + z[24];
z[24] = abb[30] + -abb[42];
z[24] = z[8] * z[24];
z[18] = abb[4] + abb[11] + z[18] + -z[20];
z[25] = -abb[41] * z[18];
z[26] = 2 * abb[2];
z[27] = abb[1] + abb[8] + -abb[12] + -z[13] + z[26];
z[27] = prod_pow(abb[23], 2) * z[27];
z[13] = abb[3] + z[13];
z[28] = abb[5] + z[13];
z[29] = abb[12] + z[28];
z[29] = abb[42] * z[29];
z[30] = abb[3] * abb[28];
z[31] = -abb[16] * abb[43];
z[32] = -abb[30] * z[15];
z[33] = -abb[28] + -3 * abb[42];
z[33] = abb[2] * z[33];
z[34] = abb[4] + -abb[5];
z[35] = abb[27] * z[34];
z[36] = abb[15] * abb[43];
z[6] = abb[45] + z[6] + z[22] + 2 * z[23] + z[24] + z[25] + z[27] + z[29] + z[30] + z[31] + z[32] + z[33] + -3 * z[35] + z[36];
z[17] = -z[8] + z[17];
z[22] = -abb[2] + abb[12];
z[0] = z[0] + -z[7] + -z[17] + z[22];
z[23] = 2 * abb[23];
z[0] = z[0] * z[23];
z[24] = -abb[14] + z[7];
z[25] = abb[5] + -abb[6];
z[25] = z[24] + 2 * z[25] + -z[26];
z[25] = abb[22] * z[25];
z[27] = 2 * abb[10];
z[24] = -z[24] + z[27];
z[24] = abb[21] * z[24];
z[29] = 2 * abb[0];
z[30] = 3 * abb[8];
z[31] = 6 * abb[1] + abb[2] + abb[5] + -z[29] + z[30];
z[32] = 2 * abb[13];
z[33] = -abb[6] + abb[14] + z[32];
z[35] = -z[31] + z[33];
z[35] = abb[18] * z[35];
z[0] = z[0] + z[24] + z[25] + z[35];
z[0] = abb[18] * z[0];
z[24] = -abb[4] + abb[7];
z[3] = z[3] + z[17] + -z[24];
z[3] = abb[23] * z[3];
z[25] = -12 * abb[1] + z[8] + 2 * z[11] + z[16];
z[35] = -abb[18] * z[25];
z[36] = 2 * abb[6];
z[32] = -z[32] + z[36];
z[17] = -7 * abb[1] + z[4] + -z[17] + -z[32];
z[17] = abb[24] * z[17];
z[37] = -5 * abb[1] + -abb[5] + -z[4] + z[16];
z[38] = -abb[21] + abb[22];
z[37] = z[37] * z[38];
z[3] = z[3] + z[17] + z[35] + z[37];
z[17] = 4 * abb[5];
z[35] = -abb[4] + -abb[6] + -z[17];
z[29] = -20 * abb[1] + 7 * abb[8] + 3 * abb[10] + abb[14] + z[29] + 2 * z[35];
z[29] = abb[20] * z[29];
z[3] = 2 * z[3] + z[29];
z[3] = abb[20] * z[3];
z[13] = abb[5] + -z[13] + z[15] + z[26];
z[13] = z[13] * z[23];
z[27] = -abb[14] + 2 * z[5] + z[15] + -z[27];
z[27] = abb[21] * z[27];
z[29] = -abb[4] + abb[6];
z[29] = -abb[0] + abb[3] + z[10] + 3 * z[29];
z[29] = abb[22] * z[29];
z[13] = z[13] + z[27] + z[29];
z[13] = abb[22] * z[13];
z[5] = -abb[1] + z[5];
z[5] = z[5] * z[38];
z[19] = -abb[24] * z[19];
z[4] = -abb[1] + -abb[11] + z[4];
z[4] = abb[23] * z[4];
z[27] = -abb[5] + abb[11];
z[27] = abb[18] * z[27];
z[4] = z[4] + z[5] + z[19] + z[27];
z[5] = z[14] + -z[20];
z[14] = abb[4] + -2 * abb[11] + z[5];
z[14] = abb[19] * z[14];
z[4] = 2 * z[4] + z[14];
z[4] = abb[19] * z[4];
z[12] = abb[10] + -z[12] + z[15] + z[20];
z[12] = abb[21] * z[12];
z[14] = -abb[1] + -abb[5] + z[24];
z[14] = z[14] * z[23];
z[14] = -z[12] + z[14];
z[14] = abb[21] * z[14];
z[19] = -abb[18] + abb[24];
z[19] = z[19] * z[22];
z[8] = -z[8] + z[28];
z[22] = z[8] + -z[26];
z[22] = abb[23] * z[22];
z[19] = z[19] + z[22];
z[22] = 2 * abb[12];
z[23] = -z[8] + z[22];
z[23] = abb[25] * z[23];
z[19] = 2 * z[19] + z[23];
z[19] = abb[25] * z[19];
z[23] = abb[17] * abb[31];
z[0] = z[0] + z[3] + z[4] + 2 * z[6] + z[13] + z[14] + z[19] + z[23];
z[3] = abb[0] + -abb[10] + abb[13];
z[3] = -abb[6] + -abb[14] + -4 * z[3] + z[17] + z[20];
z[3] = 13 * abb[1] + 2 * z[3] + -z[22];
z[3] = abb[11] * (T(2) / T(3)) + (T(1) / T(3)) * z[3] + z[30];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[0] = 2 * z[0] + z[3];
z[3] = -z[10] + z[33];
z[3] = 3 * abb[0] + -18 * abb[1] + -abb[8] + 2 * z[3];
z[3] = 4 * z[3];
z[4] = -abb[39] * z[3];
z[0] = abb[44] + 2 * z[0] + z[4];
z[4] = -abb[2] + z[20];
z[6] = -abb[3] + -z[4] + -z[15] + z[16] + z[34] + -z[36];
z[6] = abb[22] * z[6];
z[4] = z[1] + -z[4] + z[7] + -z[11];
z[4] = abb[23] * z[4];
z[1] = -abb[10] + -abb[14] + -z[1] + z[31] + z[32];
z[1] = abb[18] * z[1];
z[2] = -abb[6] + abb[13] + z[2] + -z[9] + -z[10];
z[2] = abb[24] * z[2];
z[7] = abb[20] * z[25];
z[9] = -abb[2] + -abb[12] + z[8];
z[9] = abb[25] * z[9];
z[5] = abb[11] + -z[5] + -z[34];
z[5] = abb[19] * z[5];
z[1] = z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[9] + z[12];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[33] * z[21];
z[4] = -3 * abb[2] + abb[12] + z[8];
z[4] = abb[35] * z[4];
z[5] = abb[34] * z[18];
z[6] = abb[15] + -abb[16];
z[6] = abb[36] * z[6];
z[1] = -abb[38] + -z[1] + -z[2] + -z[4] + z[5] + -z[6];
z[2] = -abb[32] * z[3];
z[1] = abb[37] + -8 * z[1] + z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_186_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_186_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_186_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(14)) * (v[2] + v[3]) * (56 * (-4 * m1_set::bc<T>[1] + -2 * m1_set::bc<T>[4] + m1_set::bc<T>[2] * (3 + v[0])) + -42 * v[2] + -42 * v[3] + 40 * v[4] + 56 * (m1_set::bc<T>[1] * (v[2] + v[3]) + v[5] + -2 * m1_set::bc<T>[1] * v[5]) + 7 * (4 * (-2 + m1_set::bc<T>[2]) * v[1] + 4 * m1_set::bc<T>[4] * (v[2] + v[3] + -2 * v[5]) + m1_set::bc<T>[2] * (v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5])))) / prod_pow(tend, 2);
c[1] = (-4 * (4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[3];
z[0] = -abb[26] + 2 * abb[29] + abb[30];
z[1] = abb[18] + abb[21] + -abb[22];
z[2] = -abb[23] + 2 * z[1];
z[2] = abb[23] * z[2];
z[1] = -2 * abb[23] + -z[1];
z[1] = 5 * abb[20] + 2 * z[1];
z[1] = abb[20] * z[1];
z[0] = 2 * z[0] + z[1] + z[2];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_186_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (2 * m1_set::bc<T>[0] * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-8 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = abb[20] + -abb[23];
return 16 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_186_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-55.599466336862840895947729581561863840716166685773996729621081368"),stof<T>("51.219747214480611570610758475011899739850558685514477065621386162")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18, 198});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,46> abb = {dl[0], dl[5], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W122(k,dv), dlog_W128(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_5(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, T{0}, f_2_4_re(k), f_2_7_re(k), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k), T{0}, T{0}};
abb[37] = SpDLog_f_3_186_W_19_Im(t, path, abb);
abb[44] = SpDLog_f_3_186_W_19_Re(t, path, abb);
{
auto c = dlog_W183(k,dv) * f_2_30(k);
abb[45] = c.real();
abb[38] = c.imag();
SpDLog_Sigma5<T,198,182>(k, dv, abb[45], abb[38], f_2_30_series_coefficients<T>);
}

                    
            return f_3_186_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_186_DLogXconstant_part(base_point<T>, kend);
	value += f_3_186_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_186_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_186_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_186_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_186_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_186_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_186_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
