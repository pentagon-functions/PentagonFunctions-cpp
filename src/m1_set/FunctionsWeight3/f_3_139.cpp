/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_139.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_139_abbreviated (const std::array<T,24>& abb) {
T z[14];
z[0] = abb[11] + -abb[14] + abb[12] * (T(3) / T(2));
z[1] = abb[13] * (T(1) / T(2));
z[2] = -z[0] + -z[1];
z[2] = abb[10] * z[2];
z[3] = abb[17] + -abb[22];
z[4] = -abb[11] + abb[14] + abb[15];
z[5] = abb[13] + -z[4];
z[5] = abb[13] * z[5];
z[6] = prod_pow(m1_set::bc<T>[0], 2);
z[7] = prod_pow(abb[15], 2);
z[8] = abb[13] * (T(3) / T(2));
z[9] = abb[12] * z[8];
z[2] = z[2] + z[3] + z[5] + (T(-5) / T(6)) * z[6] + (T(1) / T(2)) * z[7] + z[9];
z[2] = abb[6] * z[2];
z[5] = abb[12] * (T(1) / T(2));
z[7] = -z[1] + z[5];
z[9] = abb[10] * (T(1) / T(2));
z[10] = abb[14] + -z[7] + -z[9];
z[10] = abb[10] * z[10];
z[11] = (T(1) / T(3)) * z[6];
z[12] = -abb[13] * abb[14];
z[13] = abb[12] * z[1];
z[10] = -abb[16] + -abb[22] + z[10] + -z[11] + z[12] + z[13];
z[10] = abb[8] * z[10];
z[12] = prod_pow(abb[14], 2);
z[6] = z[6] + -z[12];
z[4] = abb[12] + -z[4] + z[8];
z[4] = abb[12] * z[4];
z[12] = -abb[11] + abb[15] + -z[5] + -z[8];
z[12] = abb[10] * z[12];
z[3] = abb[16] + z[3] + z[4] + (T(-1) / T(2)) * z[6] + z[12];
z[3] = abb[5] * z[3];
z[4] = abb[15] + z[7];
z[6] = -z[4] + z[9];
z[6] = abb[10] * z[6];
z[7] = -abb[15] + z[1];
z[7] = -abb[12] * z[7];
z[6] = -abb[17] + z[6] + z[7] + -z[11];
z[6] = abb[7] * z[6];
z[7] = -abb[0] + -abb[2] + abb[3] + abb[4];
z[7] = abb[1] + 2 * abb[9] + (T(-1) / T(2)) * z[7];
z[9] = abb[23] * z[7];
z[11] = -abb[6] + -abb[7];
z[11] = abb[21] * z[11];
z[2] = z[2] + z[3] + z[6] + z[9] + z[10] + z[11];
z[1] = -abb[14] + z[1] + z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[1] = -abb[19] + z[1];
z[1] = abb[8] * z[1];
z[3] = m1_set::bc<T>[0] * z[4];
z[4] = abb[10] * m1_set::bc<T>[0];
z[4] = abb[18] + z[4];
z[3] = z[3] + -z[4];
z[3] = abb[7] * z[3];
z[0] = z[0] + z[8];
z[5] = m1_set::bc<T>[0] * z[0];
z[4] = -abb[19] + -z[4] + z[5];
z[4] = abb[6] * z[4];
z[0] = -abb[15] + z[0];
z[0] = m1_set::bc<T>[0] * z[0];
z[0] = -abb[19] + z[0];
z[0] = abb[5] * z[0];
z[5] = abb[20] * z[7];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_3_139_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_139_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_139_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("7.5584392078581559681480637193844421254805645946962447139896615533")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,24> abb = {dlog_W7(k,dl), dlog_W8(k,dl), dlog_W10(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_15(k), f_2_2_im(k), f_2_11_im(k), f_2_25_im(k), f_2_2_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_3_139_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_139_DLogXconstant_part(base_point<T>, kend);
	value += f_3_139_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_139_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_139_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_139_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_139_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_139_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_139_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
