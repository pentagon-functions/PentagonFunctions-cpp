/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_201.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_201_abbreviated (const std::array<T,50>& abb) {
T z[31];
z[0] = 3 * abb[1];
z[1] = 3 * abb[3];
z[2] = 2 * abb[8];
z[3] = z[0] + z[1] + -z[2];
z[3] = abb[21] * z[3];
z[4] = abb[4] + -abb[12];
z[5] = -abb[5] + abb[13];
z[6] = z[4] + -z[5];
z[7] = 4 * abb[1];
z[8] = -z[2] + z[7];
z[9] = -z[1] + -z[6] + -z[8];
z[9] = abb[18] * z[9];
z[10] = abb[0] + -abb[5];
z[2] = abb[3] + -z[2] + z[10];
z[11] = abb[6] + z[2];
z[12] = 2 * abb[4];
z[13] = z[11] + -z[12];
z[13] = abb[20] * z[13];
z[14] = -abb[1] + z[10];
z[15] = abb[8] + -abb[12] + z[12] + -z[14];
z[15] = abb[19] * z[15];
z[16] = -abb[23] + abb[24];
z[17] = abb[3] * z[16];
z[18] = abb[5] + -abb[6] + z[12];
z[19] = -abb[0] + z[18];
z[19] = abb[24] * z[19];
z[20] = abb[23] + abb[25];
z[21] = 2 * abb[24] + -z[20];
z[21] = abb[1] * z[21];
z[22] = -abb[21] + z[20];
z[22] = abb[7] * z[22];
z[3] = z[3] + z[9] + z[13] + z[15] + z[17] + z[19] + z[21] + z[22];
z[3] = abb[19] * z[3];
z[9] = 7 * abb[1];
z[13] = abb[8] + z[5];
z[13] = -z[1] + -z[9] + 2 * z[13];
z[13] = abb[21] * z[13];
z[15] = 2 * abb[3];
z[19] = -z[5] + z[15];
z[8] = z[8] + z[19];
z[21] = abb[18] * z[8];
z[22] = 2 * abb[1];
z[10] = abb[4] + z[10] + -z[15] + -z[22];
z[10] = abb[22] * z[10];
z[1] = abb[4] + -z[1] + z[14];
z[14] = abb[24] + -abb[25];
z[1] = z[1] * z[14];
z[0] = abb[3] + -abb[7] + z[0] + z[6];
z[0] = abb[19] * z[0];
z[6] = -abb[20] * z[4];
z[23] = abb[21] + -z[14];
z[23] = abb[7] * z[23];
z[0] = z[0] + z[1] + z[6] + z[10] + z[13] + z[21] + z[23];
z[0] = abb[22] * z[0];
z[1] = prod_pow(abb[25], 2);
z[6] = 2 * abb[28] + z[1];
z[10] = 2 * abb[27];
z[13] = -z[6] + z[10];
z[21] = abb[23] * abb[25];
z[23] = abb[24] * z[20];
z[24] = abb[21] + -abb[23];
z[25] = -abb[24] + z[24];
z[25] = abb[21] * z[25];
z[23] = z[13] + -z[21] + z[23] + z[25];
z[23] = abb[7] * z[23];
z[20] = abb[23] * z[20];
z[25] = abb[23] + -abb[25];
z[25] = abb[24] * z[25];
z[20] = 4 * abb[27] + z[20] + z[25];
z[20] = abb[1] * z[20];
z[0] = abb[49] + z[0] + z[3] + z[20] + z[23];
z[1] = -abb[30] + z[1];
z[3] = -abb[23] + 2 * abb[25];
z[20] = -abb[24] * z[3];
z[23] = -abb[21] + -z[16];
z[23] = abb[21] * z[23];
z[24] = abb[25] + z[24];
z[24] = abb[19] * z[24];
z[25] = -abb[19] + abb[21] + 2 * z[14];
z[25] = abb[22] * z[25];
z[1] = abb[28] + 3 * abb[29] + 2 * z[1] + z[20] + -z[21] + z[23] + z[24] + z[25];
z[20] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = 2 * z[1] + (T(-1) / T(3)) * z[20];
z[1] = abb[2] * z[1];
z[23] = -abb[5] + 2 * abb[13] + abb[14];
z[24] = -2 * abb[0] + 3 * abb[8];
z[25] = 6 * abb[1];
z[26] = abb[3] + -z[23] + z[24] + z[25];
z[27] = -abb[4] + -z[26];
z[27] = abb[18] * z[27];
z[19] = -abb[8] + z[19] + z[25];
z[25] = abb[0] + abb[14];
z[19] = 2 * z[19] + -z[25];
z[28] = 2 * abb[21];
z[19] = z[19] * z[28];
z[28] = abb[14] * z[16];
z[19] = z[19] + -z[28];
z[16] = z[16] * z[22];
z[14] = abb[3] * z[14];
z[14] = z[14] + z[16];
z[16] = -abb[4] + -abb[5];
z[16] = abb[24] * z[16];
z[16] = z[14] + z[16];
z[16] = 2 * z[16] + z[19] + z[27];
z[16] = abb[18] * z[16];
z[25] = abb[24] * z[25];
z[27] = abb[23] + -5 * abb[24];
z[27] = abb[1] * z[27];
z[17] = -z[17] + z[25] + z[27];
z[25] = -abb[0] + 2 * abb[5];
z[27] = -16 * abb[1] + -8 * abb[3] + 7 * abb[8] + -z[25];
z[27] = abb[21] * z[27];
z[17] = 2 * z[17] + z[27];
z[17] = abb[21] * z[17];
z[27] = abb[4] + -abb[6];
z[28] = -abb[24] * z[27];
z[29] = abb[18] * z[4];
z[28] = z[28] + z[29];
z[2] = 2 * abb[6] + z[2];
z[29] = abb[4] + 2 * abb[12] + -z[2];
z[29] = abb[20] * z[29];
z[28] = 2 * z[28] + z[29];
z[28] = abb[20] * z[28];
z[29] = prod_pow(abb[24], 2);
z[30] = abb[4] + 3 * abb[5];
z[30] = z[29] * z[30];
z[13] = -z[13] + -z[29];
z[13] = abb[0] * z[13];
z[29] = -abb[23] * abb[24];
z[10] = -z[10] + z[29];
z[10] = abb[14] * z[10];
z[29] = -abb[5] * z[6];
z[1] = z[1] + z[10] + z[13] + z[16] + z[17] + z[28] + z[29] + z[30];
z[10] = 4 * abb[4];
z[13] = 5 * abb[6] + -abb[12] + -4 * abb[13];
z[9] = abb[14] * (T(-4) / T(3)) + abb[3] * (T(14) / T(3)) + z[9] + -z[10] + (T(2) / T(3)) * z[13] + z[24];
z[9] = z[9] * z[20];
z[5] = -z[5] + z[22];
z[5] = abb[30] * z[5];
z[13] = -abb[1] + -abb[3] + abb[8];
z[13] = abb[31] * z[13];
z[16] = abb[3] * abb[26];
z[5] = z[5] + z[13] + z[16];
z[13] = -2 * abb[29] + -z[6];
z[3] = -abb[24] + 2 * z[3];
z[3] = abb[24] * z[3];
z[3] = z[3] + 3 * z[13] + 2 * z[21];
z[13] = 4 * abb[3];
z[3] = z[3] * z[13];
z[6] = z[6] * z[10];
z[10] = 3 * abb[4] + -abb[12];
z[11] = -z[10] + z[11];
z[11] = 8 * z[11];
z[16] = abb[42] * z[11];
z[13] = -3 * abb[0] + 18 * abb[1] + abb[8] + z[13] + -2 * z[23];
z[13] = 4 * z[13];
z[17] = abb[41] * z[13];
z[20] = 4 * abb[17];
z[21] = -abb[45] * z[20];
z[22] = 8 * z[27];
z[23] = -abb[43] * z[22];
z[24] = -abb[15] + abb[16];
z[24] = 8 * z[24];
z[27] = -abb[44] * z[24];
z[0] = abb[46] + abb[47] + abb[48] + 8 * z[0] + 4 * z[1] + z[3] + 16 * z[5] + z[6] + 2 * z[9] + z[16] + z[17] + z[21] + z[23] + z[27];
z[1] = -abb[6] + -abb[13] + z[7] + z[10] + z[15] + z[25];
z[1] = abb[19] * z[1];
z[3] = abb[12] + z[26];
z[3] = abb[18] * z[3];
z[4] = z[4] + -z[8];
z[4] = abb[22] * z[4];
z[2] = -abb[12] + z[2] + -z[12];
z[2] = abb[20] * z[2];
z[5] = abb[24] * z[18];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + -z[14];
z[1] = 2 * z[1] + -z[19];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[33] * z[11];
z[3] = abb[32] * z[13];
z[4] = -abb[36] * z[20];
z[5] = -abb[35] * z[24];
z[6] = -abb[34] * z[22];
z[1] = abb[37] + abb[38] + abb[39] + 8 * abb[40] + 4 * z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_201_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_201_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_201_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (-v[3] + v[5]) * (2 * (-4 * v[0] + 8 * v[1] + 4 * v[2] + 3 * v[3] + -4 * v[4] + -3 * v[5] + -6 * m1_set::bc<T>[1] * (4 + v[3] + v[5])) + m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (-4 * (4 + 6 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (-v[3] + v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[3];
z[0] = -abb[26] + 2 * abb[30] + abb[31];
z[1] = abb[18] + abb[23] + -abb[24];
z[2] = 5 * abb[21] + -2 * z[1];
z[2] = abb[21] * z[2];
z[1] = -2 * abb[21] + z[1];
z[1] = -abb[19] + 2 * z[1];
z[1] = abb[19] * z[1];
z[0] = 2 * z[0] + z[1] + z[2];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_201_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-2 * m1_set::bc<T>[0] * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (8 * m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[19] + abb[21];
return 16 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_201_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[2] + v[3]) * (24 + 8 * v[0] + 4 * v[1] + -5 * v[2] + -9 * v[3] + -4 * v[4] + 6 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 12 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * (-1 + 3 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[24];
z[1] = -abb[23] + z[0];
z[1] = abb[23] * z[1];
z[0] = abb[21] + -z[0];
z[0] = abb[21] * z[0];
z[0] = -abb[27] + z[0] + z[1];
return 8 * abb[10] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_201_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = abb[21] + -abb[23];
return 8 * abb[10] * m1_set::bc<T>[0] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_201_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (-24 + -20 * v[0] + 5 * v[1] + -7 * v[2] + -9 * v[3] + 7 * v[4] + 6 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 12 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * (-1 + 3 * m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[19];
z[1] = -abb[25] + z[0];
z[1] = abb[25] * z[1];
z[0] = abb[22] + -z[0];
z[0] = abb[22] * z[0];
z[0] = abb[28] + z[0] + z[1];
return 8 * abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_201_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (v[1] + v[2] + -1 * v[3] + -1 * v[4]) * (-4 + -2 * v[0] + v[1] + -1 * v[2] + -1 * v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = abb[22] + -abb[25];
return 8 * abb[11] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_201_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.940313008484691411040722732728746671583986051814330128150943921"),stof<T>("48.773029441727147112869838373396201907101315918547307019843048692")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18, 19, 203});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,50> abb = {dl[0], dl[5], dl[2], dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W20(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W120(k,dv), dlog_W126(k,dv), dlog_W128(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_13(k), f_2_14(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, T{0}, f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k), f_2_25_re(k), T{0}, T{0}, T{0}, T{0}};
abb[37] = SpDLog_f_3_201_W_16_Im(t, path, abb);
abb[38] = SpDLog_f_3_201_W_19_Im(t, path, abb);
abb[39] = SpDLog_f_3_201_W_20_Im(t, path, abb);
abb[46] = SpDLog_f_3_201_W_16_Re(t, path, abb);
abb[47] = SpDLog_f_3_201_W_19_Re(t, path, abb);
abb[48] = SpDLog_f_3_201_W_20_Re(t, path, abb);
{
auto c = dlog_W180(k,dv) * f_2_35(k);
abb[49] = c.real();
abb[40] = c.imag();
SpDLog_Sigma5<T,203,179>(k, dv, abb[49], abb[40], f_2_35_series_coefficients<T>);
}

                    
            return f_3_201_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_201_DLogXconstant_part(base_point<T>, kend);
	value += f_3_201_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_201_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_201_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_201_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_201_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_201_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_201_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
