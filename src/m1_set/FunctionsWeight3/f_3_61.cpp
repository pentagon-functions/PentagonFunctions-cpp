/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_61.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_61_abbreviated (const std::array<T,24>& abb) {
T z[14];
z[0] = abb[1] + abb[3];
z[1] = abb[10] + abb[11];
z[2] = -abb[8] + -abb[12] + z[1];
z[2] = -z[0] * z[2];
z[3] = abb[2] + abb[4];
z[1] = -abb[9] + (T(1) / T(2)) * z[1];
z[1] = -z[1] * z[3];
z[4] = abb[10] * (T(1) / T(2));
z[5] = abb[11] * (T(1) / T(2));
z[6] = z[4] + z[5];
z[7] = abb[12] + -z[6];
z[7] = abb[5] * z[7];
z[6] = abb[8] + -z[6];
z[6] = abb[0] * z[6];
z[1] = z[1] + z[2] + z[6] + z[7];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[18] + abb[19];
z[2] = z[0] * z[2];
z[6] = abb[6] * (T(1) / T(2));
z[7] = abb[20] * z[6];
z[8] = abb[5] * abb[19];
z[9] = abb[0] * abb[18];
z[1] = z[1] + z[2] + z[7] + z[8] + z[9];
z[1] = (T(1) / T(16)) * z[1];
z[2] = abb[9] * (T(1) / T(2));
z[7] = -abb[7] + z[2];
z[7] = abb[9] * z[7];
z[8] = abb[7] * (T(1) / T(2));
z[9] = -abb[12] + z[8];
z[10] = abb[7] * z[9];
z[11] = abb[8] + -abb[11];
z[11] = abb[10] * z[11];
z[12] = abb[11] * abb[12];
z[12] = abb[22] + z[12];
z[13] = abb[7] * abb[8];
z[7] = -abb[15] + -abb[16] + abb[17] + -z[7] + z[10] + z[11] + z[12] + -z[13];
z[7] = -z[0] * z[7];
z[5] = -z[5] + z[9];
z[9] = -abb[7] * z[5];
z[10] = -abb[7] + abb[11];
z[4] = z[4] * z[10];
z[4] = -abb[13] + z[4] + z[9] + -z[12];
z[4] = abb[5] * z[4];
z[9] = -abb[7] + -abb[10];
z[5] = z[5] * z[9];
z[9] = -abb[11] + abb[12] * (T(-1) / T(2));
z[9] = abb[12] * z[9];
z[10] = -abb[10] + z[2];
z[10] = abb[9] * z[10];
z[5] = -abb[13] + abb[16] + z[5] + z[9] + z[10];
z[5] = abb[4] * z[5];
z[9] = abb[7] + abb[11];
z[8] = z[8] * z[9];
z[10] = -abb[8] + (T(1) / T(2)) * z[9];
z[10] = abb[10] * z[10];
z[8] = -abb[14] + -z[8] + z[10];
z[9] = abb[8] * (T(-1) / T(2)) + z[9];
z[9] = abb[8] * z[9];
z[2] = -abb[11] + z[2];
z[2] = abb[9] * z[2];
z[2] = abb[15] + z[2] + z[8] + z[9];
z[2] = abb[2] * z[2];
z[8] = z[8] + z[13];
z[8] = abb[0] * z[8];
z[6] = -abb[23] * z[6];
z[9] = -abb[0] + -z[0];
z[9] = abb[21] * z[9];
z[2] = z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9];
z[3] = abb[0] + abb[5] + z[3];
z[0] = -z[0] + (T(-1) / T(2)) * z[3];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[0] = (T(1) / T(3)) * z[0] + (T(1) / T(2)) * z[2];
z[0] = (T(1) / T(8)) * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_61_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_61_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_61_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,24> abb = {dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_3_61_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_61_DLogXconstant_part(base_point<T>, kend);
	value += f_3_61_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_61_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_61_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_61_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_61_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_61_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_61_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
