/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_72.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_72_abbreviated (const std::array<T,33>& abb) {
T z[29];
z[0] = abb[12] * (T(1) / T(2));
z[1] = 2 * abb[2];
z[2] = z[0] + -z[1];
z[3] = abb[3] * (T(1) / T(2));
z[4] = -z[2] + z[3];
z[5] = 2 * abb[1];
z[6] = -abb[0] + z[5];
z[7] = abb[5] * (T(1) / T(2));
z[8] = abb[10] * (T(1) / T(2));
z[9] = z[4] + z[6] + z[7] + -z[8];
z[9] = abb[20] * z[9];
z[10] = 2 * abb[9];
z[5] = z[5] + -z[10];
z[11] = z[5] + -z[8];
z[12] = 2 * abb[6];
z[4] = -abb[0] + abb[5] * (T(-5) / T(2)) + -z[4] + -z[11] + -z[12];
z[4] = abb[21] * z[4];
z[13] = abb[1] + abb[3];
z[0] = abb[2] + abb[7] + -z[0] + z[8] + -z[13];
z[0] = abb[17] * z[0];
z[8] = 4 * abb[2];
z[14] = 2 * abb[5];
z[15] = abb[6] + z[14];
z[16] = -abb[3] + z[8] + z[15];
z[16] = abb[22] * z[16];
z[17] = abb[12] * abb[22];
z[16] = z[16] + -z[17];
z[18] = 4 * abb[1] + -abb[10] + -z[10];
z[19] = abb[5] + abb[6];
z[20] = 2 * abb[3] + z[19];
z[21] = z[18] + z[20];
z[21] = abb[18] * z[21];
z[22] = abb[18] + -abb[19];
z[23] = -abb[20] + abb[21];
z[24] = abb[22] + -z[22] + z[23];
z[24] = abb[11] * z[24];
z[25] = abb[0] + z[19];
z[26] = abb[3] + z[25];
z[27] = -abb[19] * z[26];
z[0] = z[0] + z[4] + z[9] + -z[16] + z[21] + z[24] + z[27];
z[0] = abb[17] * z[0];
z[4] = 3 * abb[3];
z[9] = 2 * abb[0];
z[24] = 2 * abb[4];
z[12] = -5 * abb[5] + z[4] + -z[9] + -z[12] + z[18] + z[24];
z[18] = -abb[30] * z[12];
z[27] = abb[0] + abb[6];
z[2] = abb[3] * (T(3) / T(2)) + z[2] + z[7] + z[11] + z[27];
z[7] = abb[20] * z[2];
z[9] = z[9] + 3 * z[15];
z[10] = abb[2] + -z[9] + z[10] + z[13];
z[10] = abb[21] * z[10];
z[11] = abb[3] + z[24];
z[9] = z[9] + -z[11];
z[9] = z[9] * z[22];
z[7] = z[7] + z[9] + z[10] + z[16];
z[7] = abb[21] * z[7];
z[9] = -abb[4] + abb[8];
z[10] = abb[0] * (T(1) / T(2));
z[3] = -z[3] + z[5] + -z[9] + z[10];
z[3] = prod_pow(abb[18], 2) * z[3];
z[15] = abb[19] + -abb[22];
z[15] = z[15] * z[20];
z[16] = -abb[1] + -abb[2] + z[20];
z[16] = abb[20] * z[16];
z[15] = z[15] + z[16] + -z[21];
z[15] = abb[20] * z[15];
z[14] = z[14] + z[27];
z[16] = 2 * abb[8];
z[20] = abb[1] + z[14] + z[16] + -z[24];
z[20] = abb[25] * z[20];
z[21] = abb[22] + z[22];
z[22] = -z[21] * z[23];
z[23] = 2 * abb[23];
z[24] = prod_pow(abb[22], 2);
z[22] = -2 * abb[25] + abb[26] * (T(3) / T(2)) + z[22] + -z[23] + -z[24];
z[22] = abb[11] * z[22];
z[27] = abb[6] + abb[5] * (T(3) / T(2)) + z[1] + z[10];
z[27] = z[24] * z[27];
z[10] = abb[6] * (T(1) / T(2)) + z[10];
z[13] = abb[5] + -abb[8] + z[10] + -z[13];
z[13] = abb[26] * z[13];
z[23] = abb[25] + -abb[26] + z[23] + -z[24];
z[23] = abb[7] * z[23];
z[4] = -abb[5] + abb[12] + z[4] + -z[8];
z[8] = -abb[31] * z[4];
z[24] = abb[10] + -abb[12];
z[10] = abb[3] * (T(-1) / T(3)) + abb[2] * (T(1) / T(2)) + abb[5] * (T(5) / T(3)) + z[10];
z[10] = abb[1] * (T(-55) / T(12)) + abb[9] * (T(11) / T(3)) + (T(1) / T(2)) * z[10] + (T(1) / T(3)) * z[24];
z[10] = prod_pow(m1_set::bc<T>[0], 2) * z[10];
z[24] = abb[15] + -abb[16];
z[28] = abb[14] * (T(1) / T(2)) + abb[13] * (T(3) / T(2)) + (T(1) / T(2)) * z[24];
z[28] = abb[32] * z[28];
z[9] = prod_pow(abb[19], 2) * z[9];
z[19] = 3 * abb[2] + abb[8] + -abb[12] + z[19];
z[19] = abb[23] * z[19];
z[6] = -abb[3] + abb[10] + -z[6];
z[6] = abb[24] * z[6];
z[0] = z[0] + z[3] + z[6] + z[7] + z[8] + z[9] + z[10] + z[13] + z[15] + z[18] + z[19] + z[20] + z[22] + z[23] + z[27] + z[28];
z[3] = abb[20] + abb[21];
z[2] = z[2] * z[3];
z[3] = 2 * abb[7];
z[1] = -abb[11] + z[1] + -z[3] + -z[5] + z[14];
z[1] = abb[17] * z[1];
z[5] = z[11] + -z[16] + z[25];
z[6] = abb[19] * z[5];
z[5] = -8 * abb[1] + 6 * abb[9] + abb[10] + -z[5];
z[5] = abb[18] * z[5];
z[3] = z[3] + -z[26];
z[3] = abb[22] * z[3];
z[7] = abb[11] * z[21];
z[1] = z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + -z[17];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[27] * z[12];
z[3] = -abb[28] * z[4];
z[4] = 3 * abb[13] + abb[14] + z[24];
z[4] = abb[29] * z[4];
z[1] = z[1] + z[2] + z[3] + (T(1) / T(2)) * z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_72_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_72_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_72_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("18.532805985296568558258419582201622705850006466615638653357974654"),stof<T>("28.062415966599068695583676699101002397539170265371126850982119055")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,33> abb = {dl[0], dl[1], dlog_W3(k,dl), dlog_W7(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_3_72_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_72_DLogXconstant_part(base_point<T>, kend);
	value += f_3_72_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_72_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_72_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_72_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_72_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_72_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_72_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
