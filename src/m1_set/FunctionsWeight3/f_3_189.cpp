/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_189.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_189_abbreviated (const std::array<T,46>& abb) {
T z[35];
z[0] = abb[3] + -abb[10];
z[1] = -abb[6] + abb[13];
z[2] = z[0] + -z[1];
z[3] = 3 * abb[4];
z[4] = 2 * abb[7];
z[5] = z[3] + -z[4];
z[6] = 4 * abb[1];
z[7] = -z[2] + -z[5] + -z[6];
z[7] = abb[18] * z[7];
z[8] = 3 * abb[1];
z[9] = abb[5] + abb[8];
z[10] = -abb[4] + z[9];
z[2] = abb[12] + z[2] + z[8] + -z[10];
z[2] = abb[19] * z[2];
z[11] = abb[5] + -abb[8];
z[8] = z[5] + z[8] + z[11];
z[8] = abb[25] * z[8];
z[12] = 2 * abb[1];
z[13] = abb[4] + z[12];
z[14] = 2 * abb[3];
z[15] = abb[0] + -abb[6];
z[16] = abb[2] + z[15];
z[17] = z[13] + z[14] + -z[16];
z[17] = abb[22] * z[17];
z[18] = abb[1] + abb[4];
z[11] = -z[11] + -z[18];
z[11] = abb[21] * z[11];
z[16] = abb[4] + -z[4] + z[16];
z[19] = -z[14] + z[16];
z[19] = abb[24] * z[19];
z[20] = abb[1] + abb[7] + -abb[10] + z[14] + -z[15];
z[20] = abb[23] * z[20];
z[21] = -abb[1] + -abb[12] + z[9];
z[21] = abb[20] * z[21];
z[2] = z[2] + z[7] + z[8] + z[11] + z[17] + z[19] + z[20] + z[21];
z[2] = abb[23] * z[2];
z[7] = abb[8] + z[12];
z[8] = abb[0] + abb[14];
z[11] = abb[11] + z[8];
z[7] = 2 * z[7] + -z[11];
z[17] = abb[42] * z[7];
z[19] = 2 * abb[8];
z[15] = abb[3] + z[15];
z[20] = -abb[5] + -z[15] + z[19];
z[21] = -abb[12] + z[3] + z[20];
z[22] = abb[40] * z[21];
z[23] = 3 * abb[27];
z[24] = -abb[5] * z[23];
z[23] = 2 * abb[26] + z[23];
z[23] = abb[4] * z[23];
z[25] = abb[15] + -abb[16];
z[26] = abb[43] * z[25];
z[2] = abb[45] + z[2] + z[17] + z[22] + z[23] + z[24] + z[26];
z[17] = 2 * abb[5];
z[22] = abb[1] + abb[8];
z[3] = -z[3] + z[15] + z[17] + -z[22];
z[23] = 2 * abb[19];
z[24] = z[3] * z[23];
z[13] = -abb[6] + z[13];
z[13] = -abb[14] + 2 * z[13] + -z[14];
z[13] = abb[18] * z[13];
z[26] = -abb[4] + z[11];
z[27] = 5 * abb[1] + z[9] + -z[26];
z[28] = abb[25] * z[27];
z[29] = 2 * abb[4];
z[30] = -abb[5] + abb[6];
z[30] = -abb[0] + abb[2] + z[29] + 3 * z[30];
z[30] = abb[22] * z[30];
z[31] = abb[1] + -abb[11] + z[10];
z[31] = -abb[14] + 2 * z[31];
z[31] = abb[21] * z[31];
z[13] = z[13] + z[24] + -2 * z[28] + z[30] + z[31];
z[13] = abb[22] * z[13];
z[24] = 3 * abb[7];
z[28] = 2 * abb[13];
z[30] = 2 * abb[0];
z[31] = 6 * abb[1];
z[32] = abb[3] + abb[4] + -abb[14] + z[24] + -z[28] + -z[30] + z[31];
z[33] = -abb[6] + -z[32];
z[33] = abb[18] * z[33];
z[34] = abb[11] + -z[12];
z[34] = abb[14] + 2 * z[34];
z[34] = abb[21] * z[34];
z[33] = z[33] + z[34];
z[33] = abb[18] * z[33];
z[34] = 2 * abb[6];
z[5] = -7 * abb[1] + -z[5] + z[9] + z[28] + -z[34];
z[5] = abb[19] * z[5];
z[9] = -abb[7] + -z[1] + z[31];
z[28] = 4 * abb[4];
z[9] = 2 * z[9] + -z[11] + z[28];
z[11] = abb[18] * z[9];
z[27] = abb[21] * z[27];
z[5] = z[5] + z[11] + z[27];
z[11] = -20 * abb[1] + -8 * abb[4] + 7 * abb[7] + 3 * abb[11] + abb[14] + -z[17] + z[30] + -z[34];
z[11] = abb[25] * z[11];
z[5] = 2 * z[5] + z[11];
z[5] = abb[25] * z[5];
z[3] = -abb[19] * z[3];
z[11] = -abb[21] + abb[22];
z[10] = -abb[1] + z[10];
z[10] = z[10] * z[11];
z[11] = abb[4] + -abb[12];
z[11] = -abb[18] * z[11];
z[3] = z[3] + z[10] + z[11];
z[10] = -2 * abb[12] + -z[20];
z[10] = abb[20] * z[10];
z[3] = 2 * z[3] + z[10];
z[3] = abb[20] * z[3];
z[10] = abb[12] + -z[12] + z[15] + -z[29];
z[10] = abb[19] * z[10];
z[6] = z[6] + z[29];
z[11] = -z[1] + z[6];
z[15] = -abb[12] + -z[4] + z[11];
z[15] = abb[18] * z[15];
z[10] = z[10] + z[15];
z[10] = z[10] * z[23];
z[15] = abb[18] + -abb[19];
z[15] = z[0] * z[15];
z[17] = 2 * abb[10] + -z[16];
z[17] = abb[24] * z[17];
z[15] = 2 * z[15] + z[17];
z[15] = abb[24] * z[15];
z[1] = -abb[5] + -z[1] + z[12];
z[1] = abb[29] * z[1];
z[12] = abb[7] + -z[18];
z[12] = abb[30] * z[12];
z[1] = z[1] + z[12];
z[8] = abb[11] + -z[8] + 2 * z[22];
z[12] = -prod_pow(abb[21], 2) * z[8];
z[17] = 3 * abb[3] + -abb[10] + -z[16];
z[17] = 2 * z[17];
z[18] = -abb[41] * z[17];
z[23] = 3 * abb[0] + -18 * abb[1] + -abb[7] + 4 * abb[13] + 2 * abb[14] + -z[28] + -z[34];
z[27] = -abb[39] * z[23];
z[14] = 2 * abb[2] + -z[14];
z[14] = abb[28] * z[14];
z[28] = -abb[17] * abb[31];
z[1] = 4 * z[1] + 2 * z[2] + z[3] + z[5] + z[10] + z[12] + z[13] + z[14] + z[15] + z[18] + z[27] + z[28] + z[33];
z[2] = abb[0] + -abb[4] + -abb[11] + abb[13];
z[3] = -abb[10] + abb[12];
z[5] = -abb[6] + z[3];
z[10] = -abb[14] + z[5];
z[2] = abb[8] * (T(4) / T(3)) + abb[1] * (T(13) / T(3)) + (T(-8) / T(3)) * z[2] + (T(2) / T(3)) * z[10] + z[24];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[1] = 2 * z[1] + z[2];
z[1] = abb[44] + 2 * z[1];
z[2] = -abb[11] + -z[3] + z[32] + z[34];
z[2] = abb[18] * z[2];
z[3] = abb[3] + abb[13] + z[4] + z[5] + -z[6];
z[3] = abb[19] * z[3];
z[4] = -abb[25] * z[9];
z[0] = abb[12] + z[0] + z[11] + -z[19];
z[0] = abb[23] * z[0];
z[5] = abb[21] * z[8];
z[6] = -abb[6] + -z[22];
z[6] = -abb[2] + abb[3] + abb[5] + 2 * z[6] + z[26];
z[6] = abb[22] * z[6];
z[8] = abb[4] + abb[12] + z[20];
z[8] = abb[20] * z[8];
z[9] = -abb[3] + -abb[10] + z[16];
z[9] = abb[24] * z[9];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[8] + z[9];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[33] * z[21];
z[0] = z[0] + z[2];
z[2] = abb[35] * z[7];
z[3] = abb[36] * z[25];
z[2] = abb[38] + z[2] + z[3];
z[3] = -abb[32] * z[23];
z[4] = -abb[34] * z[17];
z[2] = 2 * z[2] + z[3] + z[4];
z[0] = abb[37] + 8 * z[0] + 4 * z[2];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_189_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_189_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_189_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(14)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (-112 * m1_set::bc<T>[1] + 84 * m1_set::bc<T>[2] + -56 * m1_set::bc<T>[4] + -49 * v[1] + 7 * v[2] + 21 * v[3] + 13 * v[4] + -28 * v[5]) + 7 * (4 * (2 + -4 * m1_set::bc<T>[1] + 5 * m1_set::bc<T>[2] + -2 * m1_set::bc<T>[4]) * v[0] + 8 * m1_set::bc<T>[1] * v[1] + m1_set::bc<T>[2] * v[1] + -8 * m1_set::bc<T>[1] * v[2] + 13 * m1_set::bc<T>[2] * v[2] + -8 * m1_set::bc<T>[1] * v[3] + 3 * m1_set::bc<T>[2] * v[3] + 8 * m1_set::bc<T>[1] * v[4] + -13 * m1_set::bc<T>[2] * v[4] + 16 * m1_set::bc<T>[1] * v[5] + -12 * m1_set::bc<T>[2] * v[5] + 4 * m1_set::bc<T>[4] * (v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])))) / prod_pow(tend, 2);
c[1] = (-4 * (4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[3];
z[0] = -abb[26] + 2 * abb[29] + abb[30];
z[1] = abb[18] + abb[21] + -abb[22];
z[2] = 5 * abb[25] + -2 * z[1];
z[2] = abb[25] * z[2];
z[1] = -2 * abb[25] + z[1];
z[1] = -abb[23] + 2 * z[1];
z[1] = abb[23] * z[1];
z[0] = 2 * z[0] + z[1] + z[2];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_189_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (2 * m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (8 * m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[23] + abb[25];
return 16 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_189_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-55.599466336862840895947729581561863840716166685773996729621081368"),stof<T>("51.219747214480611570610758475011899739850558685514477065621386162")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19, 199});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,46> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W25(k,dl), dlog_W27(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W121(k,dv), dlog_W127(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_6_im(k), f_2_10_im(k), f_2_12_im(k), f_2_24_im(k), T{0}, T{0}, f_2_2_re(k), f_2_6_re(k), f_2_10_re(k), f_2_12_re(k), f_2_24_re(k), T{0}, T{0}};
abb[37] = SpDLog_f_3_189_W_20_Im(t, path, abb);
abb[44] = SpDLog_f_3_189_W_20_Re(t, path, abb);
{
auto c = dlog_W177(k,dv) * f_2_31(k);
abb[45] = c.real();
abb[38] = c.imag();
SpDLog_Sigma5<T,199,176>(k, dv, abb[45], abb[38], f_2_31_series_coefficients<T>);
}

                    
            return f_3_189_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_189_DLogXconstant_part(base_point<T>, kend);
	value += f_3_189_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_189_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_189_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_189_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_189_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_189_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_189_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
