/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_162.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_162_abbreviated (const std::array<T,52>& abb) {
T z[65];
z[0] = 3 * abb[3];
z[1] = 3 * abb[5];
z[2] = z[0] + z[1];
z[3] = 2 * abb[0];
z[4] = 3 * abb[14];
z[5] = z[3] + -z[4];
z[6] = 4 * abb[10];
z[7] = z[5] + z[6];
z[8] = abb[7] + abb[8];
z[9] = 2 * abb[16];
z[10] = z[2] + z[7] + -z[8] + -z[9];
z[11] = -abb[49] * z[10];
z[12] = abb[6] + abb[8];
z[13] = abb[3] + abb[4];
z[14] = 2 * abb[15];
z[5] = -4 * abb[2] + -z[5] + -3 * z[12] + z[13] + z[14];
z[15] = abb[46] * z[5];
z[16] = 2 * abb[7];
z[17] = 3 * abb[6];
z[18] = z[16] + z[17];
z[19] = -abb[13] + z[14];
z[20] = 6 * abb[9];
z[21] = z[18] + -z[19] + -z[20];
z[22] = 2 * abb[4];
z[23] = 4 * abb[12];
z[24] = 2 * abb[3];
z[25] = z[21] + -z[22] + z[23] + -z[24];
z[26] = abb[34] * z[25];
z[27] = -abb[7] + abb[12];
z[28] = -abb[2] + abb[10];
z[29] = 6 * abb[0] + -z[4];
z[30] = 3 * abb[8];
z[31] = z[27] + -3 * z[28] + z[29] + z[30];
z[32] = abb[45] * z[31];
z[33] = abb[6] + abb[7];
z[34] = abb[13] + z[33];
z[35] = 2 * abb[1];
z[36] = -abb[12] + z[24] + -z[34] + z[35];
z[36] = abb[31] * z[36];
z[37] = abb[4] + abb[5];
z[12] = -z[12] + z[37];
z[38] = -abb[13] + -z[12] + z[35];
z[38] = abb[47] * z[38];
z[34] = -abb[3] + -abb[5] + z[34];
z[39] = abb[33] * z[34];
z[36] = z[36] + z[38] + z[39];
z[38] = 2 * abb[8];
z[39] = 3 * abb[12];
z[40] = z[9] + z[38] + -z[39];
z[41] = -abb[7] + z[20] + z[40];
z[42] = 4 * abb[13];
z[43] = z[41] + -z[42];
z[44] = abb[35] * z[43];
z[45] = -abb[8] + abb[13];
z[46] = -abb[7] + -z[17] + z[39] + z[45];
z[7] = -z[7] + z[46];
z[47] = abb[50] * z[7];
z[48] = abb[2] + -abb[4];
z[49] = -abb[0] + abb[1];
z[50] = z[48] + z[49];
z[51] = prod_pow(abb[26], 2) * z[50];
z[52] = abb[7] + z[1];
z[30] = -z[30] + z[52];
z[53] = 6 * abb[1];
z[54] = -3 * abb[11] + abb[12] + -z[30] + z[53];
z[55] = abb[48] * z[54];
z[11] = -z[11] + -z[15] + -z[26] + z[32] + -3 * z[36] + z[44] + -z[47] + -z[51] + -z[55];
z[15] = 5 * abb[12];
z[26] = 2 * abb[13];
z[30] = abb[4] + z[14] + -z[15] + z[20] + -z[26] + -z[30] + z[35];
z[30] = abb[29] * z[30];
z[25] = abb[23] * z[25];
z[32] = 2 * abb[9];
z[36] = -abb[12] + z[13];
z[44] = z[32] + -z[33] + z[36];
z[47] = abb[24] * z[44];
z[19] = -z[13] + z[19] + -z[27];
z[19] = abb[25] * z[19];
z[27] = 4 * abb[11];
z[51] = z[27] + z[35];
z[55] = -z[1] + z[51];
z[56] = 3 * abb[4];
z[46] = z[46] + -z[55] + z[56];
z[46] = abb[27] * z[46];
z[57] = abb[3] + -abb[8];
z[58] = abb[12] + -z[57];
z[59] = -abb[4] + 2 * abb[11];
z[60] = z[58] + -z[59];
z[60] = abb[26] * z[60];
z[19] = z[19] + z[25] + z[30] + z[46] + 3 * z[47] + -2 * z[60];
z[0] = z[0] + z[53];
z[25] = 4 * abb[4];
z[1] = -z[0] + z[1] + z[15] + z[21] + -z[25] + z[27] + -z[38];
z[1] = abb[28] * z[1];
z[1] = z[1] + 2 * z[19];
z[1] = abb[28] * z[1];
z[15] = -z[17] + z[20];
z[19] = 3 * abb[7] + z[23];
z[21] = 4 * abb[3];
z[30] = 4 * abb[8] + -z[21];
z[28] = -z[15] + z[19] + -z[25] + -8 * z[28] + z[29] + z[30];
z[28] = abb[24] * z[28];
z[29] = 2 * abb[10];
z[46] = 2 * abb[2];
z[47] = abb[8] + z[46];
z[36] = z[29] + z[36] + -z[47];
z[53] = abb[26] * z[36];
z[28] = z[28] + -4 * z[53];
z[28] = abb[24] * z[28];
z[53] = -abb[1] + abb[9];
z[61] = abb[15] + abb[16];
z[62] = 2 * abb[12];
z[53] = -abb[4] + z[24] + z[45] + -3 * z[53] + -z[61] + z[62];
z[63] = abb[29] * z[53];
z[36] = abb[24] * z[36];
z[50] = -abb[26] * z[50];
z[50] = z[36] + z[50] + z[63];
z[63] = z[9] + z[14];
z[64] = 4 * abb[1] + -z[63];
z[21] = z[21] + z[64];
z[4] = z[4] + z[46];
z[6] = 4 * abb[0] + -z[4] + z[6];
z[15] = -abb[7] + -abb[12] + -z[6] + z[15] + -z[21] + -z[45];
z[15] = abb[23] * z[15];
z[15] = z[15] + 2 * z[50];
z[22] = -z[22] + z[24] + z[64];
z[16] = -abb[13] + z[3] + z[16] + -z[22] + -z[39] + -z[47];
z[24] = -abb[25] * z[16];
z[15] = 2 * z[15] + z[24];
z[15] = abb[25] * z[15];
z[16] = -abb[23] * z[16];
z[24] = -abb[2] + z[29] + z[49];
z[29] = z[24] + -z[58];
z[29] = abb[26] * z[29];
z[29] = z[29] + z[36];
z[16] = z[16] + 4 * z[29];
z[16] = abb[23] * z[16];
z[29] = -abb[23] * z[53];
z[29] = z[29] + z[60];
z[39] = 12 * abb[9];
z[42] = 8 * abb[1] + -z[39] + z[42] + z[52] + -z[63];
z[46] = -abb[6] + -z[38];
z[27] = 6 * abb[3] + -abb[4] + 8 * abb[12] + -z[27] + z[42] + 3 * z[46];
z[27] = abb[29] * z[27];
z[27] = z[27] + 4 * z[29];
z[27] = abb[29] * z[27];
z[2] = -abb[13] + -z[2] + z[18] + z[40];
z[2] = abb[25] * z[2];
z[29] = -abb[23] * z[43];
z[40] = 6 * abb[12];
z[9] = -z[9] + z[40] + -z[51];
z[12] = -5 * abb[13] + -z[9] + -3 * z[12] + z[20];
z[12] = abb[29] * z[12];
z[32] = z[32] + -z[34];
z[32] = abb[30] * z[32];
z[2] = z[2] + z[12] + z[29] + 3 * z[32];
z[0] = 7 * abb[13] + -z[0] + -z[41] + z[56];
z[0] = abb[27] * z[0];
z[0] = z[0] + 2 * z[2];
z[0] = abb[27] * z[0];
z[2] = abb[32] * z[44];
z[3] = -abb[14] + z[3];
z[12] = -abb[6] + z[45];
z[29] = z[3] + -z[12];
z[32] = abb[44] * z[29];
z[2] = z[2] + z[32];
z[3] = z[3] + z[33];
z[32] = prod_pow(abb[30], 2);
z[3] = z[3] * z[32];
z[34] = abb[31] + -abb[33];
z[32] = -z[32] + 2 * z[34];
z[20] = z[20] * z[32];
z[0] = z[0] + z[1] + -6 * z[2] + 3 * z[3] + -2 * z[11] + z[15] + z[16] + z[20] + z[27] + z[28];
z[1] = -abb[3] + z[61];
z[2] = 2 * abb[6];
z[3] = -abb[8] + -z[2];
z[1] = 23 * abb[0] + -27 * abb[1] + 26 * abb[11] + 4 * abb[14] + abb[10] * (T(-94) / T(3)) + abb[13] * (T(2) / T(3)) + (T(4) / T(3)) * z[1] + 2 * z[3] + (T(1) / T(3)) * z[48] + z[62];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[3] = abb[18] + abb[20] + abb[22];
z[11] = abb[17] + abb[19];
z[3] = abb[21] + 3 * z[3] + 2 * z[11];
z[11] = 4 * abb[43];
z[11] = -z[3] * z[11];
z[0] = 2 * z[0] + z[1] + z[11];
z[0] = 4 * z[0];
z[1] = 8 * abb[0] + -z[4] + z[17] + z[19] + z[21] + z[26] + -z[38] + -z[39];
z[1] = abb[23] * z[1];
z[4] = -z[14] + -z[18] + z[23] + z[25] + z[26] + -z[55] + z[57];
z[4] = abb[28] * z[4];
z[6] = z[6] + -z[22] + -z[26] + 3 * z[33] + -z[40];
z[6] = abb[25] * z[6];
z[11] = -abb[4] + 8 * abb[11] + -10 * abb[12] + z[17] + z[30] + -z[42];
z[11] = abb[29] * z[11];
z[2] = 2 * abb[5] + -z[2] + -z[8] + z[13];
z[2] = 3 * z[2] + z[9] + z[26];
z[2] = abb[27] * z[2];
z[8] = -z[24] + z[59];
z[8] = abb[26] * z[8];
z[8] = z[8] + -z[36];
z[1] = z[1] + z[2] + z[4] + z[6] + 2 * z[8] + z[11];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[37] * z[31];
z[4] = abb[38] * z[5];
z[5] = -abb[41] * z[10];
z[6] = -z[12] + z[35] + -z[37];
z[6] = abb[39] * z[6];
z[8] = abb[36] * z[29];
z[6] = z[6] + -z[8];
z[7] = abb[42] * z[7];
z[8] = abb[40] * z[54];
z[3] = abb[51] * z[3];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + 3 * z[6] + z[7] + z[8];
z[1] = 16 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_162_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_162_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_162_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-546.12110979713402788733526351453758030182834021593287065670424854"),stof<T>("269.98941026483938148447451604023318929462958269594595015620026086")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,52> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W76(k,dl), dlog_W77(k,dl), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_13(k), f_2_14(k), f_2_19(k), f_2_20(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_10_im(k), f_2_11_im(k), f_2_12_im(k), f_2_22_im(k), f_2_26_im(k), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_10_re(k), f_2_11_re(k), f_2_12_re(k), f_2_22_re(k), f_2_26_re(k)};

                    
            return f_3_162_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_162_DLogXconstant_part(base_point<T>, kend);
	value += f_3_162_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_162_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_162_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_162_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_162_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_162_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_162_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
