/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_213.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_213_abbreviated (const std::array<T,35>& abb) {
T z[21];
z[0] = 2 * abb[8];
z[1] = -abb[12] + z[0];
z[2] = 3 * abb[9];
z[3] = z[1] + -z[2];
z[4] = 3 * abb[10];
z[5] = abb[13] + abb[14] + -z[3] + -z[4];
z[5] = abb[13] * z[5];
z[6] = abb[10] + abb[14];
z[7] = -abb[11] + z[3] + z[6];
z[7] = abb[11] * z[7];
z[8] = 2 * abb[10] + z[3];
z[8] = abb[10] * z[8];
z[3] = abb[14] * z[3];
z[9] = abb[29] + abb[31];
z[10] = 4 * abb[30];
z[11] = prod_pow(m1_set::bc<T>[0], 2);
z[11] = (T(14) / T(3)) * z[11];
z[3] = -z[3] + -z[5] + z[7] + -z[8] + 4 * z[9] + z[10] + z[11];
z[5] = abb[27] * z[3];
z[7] = -abb[9] + z[0];
z[9] = 3 * abb[12];
z[12] = z[7] + -z[9];
z[13] = 2 * abb[14] + z[12];
z[13] = abb[14] * z[13];
z[13] = -4 * abb[31] + z[13];
z[14] = abb[9] * z[7];
z[15] = abb[12] * z[1];
z[14] = 4 * abb[29] + z[14] + z[15];
z[15] = abb[9] + abb[12];
z[16] = -z[6] + z[15];
z[17] = abb[13] * z[16];
z[16] = abb[11] * z[16];
z[7] = abb[12] + z[7];
z[7] = abb[10] * z[7];
z[11] = 4 * abb[32] + z[11];
z[7] = -z[7] + z[11] + -z[13] + z[14] + -3 * z[16] + -z[17];
z[18] = abb[26] * z[7];
z[19] = 3 * abb[14];
z[20] = abb[10] + abb[11] + -z[12] + -z[19];
z[20] = abb[11] * z[20];
z[6] = -abb[13] + z[6] + z[12];
z[6] = abb[13] * z[6];
z[12] = abb[10] * z[12];
z[10] = z[10] + z[11];
z[6] = -z[6] + -z[10] + z[12] + z[13] + z[20];
z[11] = -abb[25] * z[6];
z[1] = abb[9] + z[1];
z[1] = abb[14] * z[1];
z[1] = -z[1] + -z[8] + z[10] + z[14] + -z[16] + -3 * z[17];
z[8] = abb[28] * z[1];
z[10] = abb[11] + abb[13];
z[0] = -z[0] + 2 * z[10];
z[4] = abb[14] + z[4];
z[2] = abb[12] + z[0] + z[2] + -z[4];
z[2] = m1_set::bc<T>[0] * z[2];
z[10] = abb[19] + abb[21];
z[12] = 2 * abb[20];
z[2] = z[2] + -2 * z[10] + -z[12];
z[10] = abb[17] * z[2];
z[13] = abb[10] + z[19];
z[14] = -abb[8] + z[15];
z[14] = 2 * z[14];
z[15] = -3 * abb[11] + -abb[13] + z[13] + -z[14];
z[15] = m1_set::bc<T>[0] * z[15];
z[16] = abb[19] + abb[22];
z[17] = abb[21] + z[16];
z[15] = z[15] + 2 * z[17];
z[17] = abb[16] * z[15];
z[10] = -z[10] + z[17];
z[4] = -abb[11] + -3 * abb[13] + z[4] + -z[14];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = z[4] + z[12] + 2 * z[16];
z[4] = 2 * z[4];
z[14] = -abb[18] * z[4];
z[0] = abb[9] + z[0] + z[9] + -z[13];
z[0] = m1_set::bc<T>[0] * z[0];
z[9] = abb[21] + abb[22];
z[0] = z[0] + -2 * z[9] + -z[12];
z[0] = 2 * z[0];
z[9] = abb[15] * z[0];
z[12] = abb[0] + abb[6] + abb[7];
z[12] = abb[1] + abb[2] + abb[3] + abb[4] + 4 * abb[5] + 2 * z[12];
z[13] = abb[33] * z[12];
z[5] = -8 * abb[34] + z[5] + z[8] + z[9] + -2 * z[10] + z[11] + z[13] + z[14] + z[18];
z[5] = 16 * z[5];
z[3] = abb[17] * z[3];
z[1] = abb[18] * z[1];
z[7] = abb[16] * z[7];
z[6] = -abb[15] * z[6];
z[2] = -abb[27] * z[2];
z[8] = abb[26] * z[15];
z[2] = -4 * abb[24] + z[2] + z[8];
z[0] = -abb[25] * z[0];
z[4] = abb[28] * z[4];
z[8] = abb[23] * z[12];
z[0] = z[0] + z[1] + 2 * z[2] + z[3] + z[4] + z[6] + z[7] + z[8];
z[0] = 16 * z[0];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_3_213_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {stof<T>("5053.2374533577516128432593919365893812806140965072848007236348806")};
	
	std::vector<C> intdlogs = {(clog(k.W[200]) - clog(kbase.W[200]))};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_213_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_213_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-382.7359263206109614955686322248478353506483223053340931154569432"),stof<T>("1696.3628792974031948660274808319899610083248323796477117371859043")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({200});

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
        if constexpr (std::is_same_v<T,double>){
            if(always_switch_to_HP_for_sqrt_divergences and path.can_letters_vanish_Q({200})) {
                return {std::nan(""), std::numeric_limits<double>::max()};
            }
        }
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,35> abb = {dl[0], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), dlog_W165(k,dv).imag(), dlog_W167(k,dv).imag(), dlog_W171(k,dv).imag(), dlog_W173(k,dv).imag(), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_11_im(k), f_2_32_im(k), T{0}, dlog_W165(k,dv).real(), dlog_W167(k,dv).real(), dlog_W171(k,dv).real(), dlog_W173(k,dv).real(), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_11_re(k), f_2_32_re(k), T{0}};
SpDLogSigma5_W_201(k, dv, dlr, abb[34], abb[24]);

                    
            return f_3_213_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_213_DLogXconstant_part(base_point<T>, kend);
	value += f_3_213_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_213_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_213_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_213_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_213_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_213_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_213_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
