/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_208.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_208_abbreviated (const std::array<T,33>& abb) {
T z[22];
z[0] = 3 * abb[9] + -abb[10];
z[1] = 2 * abb[8];
z[2] = -abb[13] + z[1];
z[3] = -3 * abb[12] + -abb[14] + z[0] + z[2];
z[3] = abb[14] * z[3];
z[4] = -abb[9] + 3 * abb[10];
z[5] = 3 * abb[11];
z[6] = z[4] + -z[5];
z[7] = 2 * abb[12] + z[6];
z[7] = abb[12] * z[7];
z[8] = abb[9] + abb[10];
z[9] = -abb[11] + z[8];
z[10] = z[1] * z[9];
z[11] = 4 * abb[30];
z[10] = z[10] + -z[11];
z[4] = -abb[11] + z[4];
z[4] = abb[11] * z[4];
z[12] = prod_pow(abb[10], 2);
z[13] = abb[13] * z[9];
z[14] = 4 * abb[15];
z[15] = 4 * abb[29];
z[3] = z[3] + -z[4] + z[7] + -z[10] + 2 * z[12] + z[13] + z[14] + -z[15];
z[4] = abb[28] * z[3];
z[7] = abb[12] + z[1];
z[12] = -abb[11] + z[7] + -z[8];
z[12] = abb[12] * z[12];
z[16] = z[2] + z[9];
z[16] = abb[13] * z[16];
z[17] = z[1] * z[8];
z[14] = -z[14] + z[17];
z[17] = abb[11] * z[8];
z[8] = -abb[12] + z[8];
z[18] = abb[13] + -z[8];
z[19] = abb[14] * z[18];
z[20] = 4 * abb[16];
z[11] = z[11] + z[12] + -z[14] + z[16] + z[17] + -3 * z[19] + -z[20];
z[12] = abb[27] * z[11];
z[5] = -z[0] + -z[5] + z[7];
z[5] = abb[12] * z[5];
z[2] = z[2] + z[6];
z[2] = abb[13] * z[2];
z[6] = 2 * abb[11];
z[0] = z[0] + z[6];
z[0] = abb[11] * z[0];
z[7] = z[15] + z[20];
z[15] = prod_pow(abb[9], 2);
z[0] = z[0] + z[2] + z[5] + -z[7] + -z[14] + 2 * z[15] + -z[19];
z[2] = abb[25] * z[0];
z[1] = -3 * abb[13] + -abb[14] + z[1] + z[8];
z[1] = abb[14] * z[1];
z[5] = abb[11] + -abb[12];
z[5] = z[5] * z[9];
z[1] = -z[1] + z[5] + z[7] + z[10] + -3 * z[13];
z[5] = -abb[26] * z[1];
z[7] = abb[18] + abb[20];
z[8] = -abb[14] + z[9];
z[9] = -z[7] * z[8];
z[10] = m1_set::bc<T>[0] * (T(1) / T(3));
z[13] = abb[26] + abb[28];
z[14] = -abb[25] + z[13];
z[14] = z[10] * z[14];
z[15] = abb[19] * z[18];
z[6] = -abb[9] + abb[10] + abb[12] + abb[13] + -z[6];
z[16] = -abb[17] * z[6];
z[9] = z[9] + z[14] + z[15] + z[16];
z[14] = 2 * m1_set::bc<T>[0];
z[9] = z[9] * z[14];
z[15] = 4 * abb[22];
z[16] = -abb[19] + -z[7];
z[16] = z[15] * z[16];
z[17] = 4 * abb[21];
z[19] = abb[17] + z[7];
z[19] = z[17] * z[19];
z[20] = abb[0] + abb[6] + abb[7];
z[20] = abb[1] + abb[2] + 4 * abb[3] + abb[4] + abb[5] + 2 * z[20];
z[21] = abb[31] * z[20];
z[2] = -8 * abb[32] + z[2] + z[4] + z[5] + z[9] + z[12] + z[16] + z[19] + z[21];
z[2] = 16 * z[2];
z[3] = abb[20] * z[3];
z[4] = abb[19] * z[11];
z[0] = abb[17] * z[0];
z[1] = -abb[18] * z[1];
z[5] = z[8] * z[13];
z[7] = -abb[17] + z[7];
z[7] = z[7] * z[10];
z[8] = -abb[27] * z[18];
z[6] = abb[25] * z[6];
z[5] = z[5] + z[6] + z[7] + z[8];
z[5] = z[5] * z[14];
z[6] = abb[27] + z[13];
z[6] = z[6] * z[15];
z[7] = -abb[25] + -z[13];
z[7] = z[7] * z[17];
z[8] = abb[23] * z[20];
z[0] = -8 * abb[24] + z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
z[0] = 16 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_3_208_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_208_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_208_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-21.75082764978550645525901505194521149675355701828600444345493197"),stof<T>("224.62837885902485067552380688085135434818368597629457998962706754")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({198});

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
        if constexpr (std::is_same_v<T,double>){
            if(always_switch_to_HP_for_sqrt_divergences and path.can_letters_vanish_Q({198})) {
                return {std::nan(""), std::numeric_limits<double>::max()};
            }
        }
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,33> abb = {dl[0], dl[2], dlog_W7(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), f_1_1(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_2_5(k), f_2_13(k), dlog_W162(k,dv).imag(), dlog_W164(k,dv).imag(), dlog_W183(k,dv).imag(), dlog_W185(k,dv).imag(), f_2_7_im(k), f_2_9_im(k), f_2_30_im(k), T{0}, dlog_W162(k,dv).real(), dlog_W164(k,dv).real(), dlog_W183(k,dv).real(), dlog_W185(k,dv).real(), f_2_7_re(k), f_2_9_re(k), f_2_30_re(k), T{0}};
SpDLogSigma5_W_199(k, dv, dlr, abb[32], abb[24]);

                    
            return f_3_208_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_208_DLogXconstant_part(base_point<T>, kend);
	value += f_3_208_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_208_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_208_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_208_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_208_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_208_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_208_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
