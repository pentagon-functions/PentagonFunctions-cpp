#pragma once
#include "Kin.h"
#include "FunctionID.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_173_evaluator();

extern template FunctionObjectType<double, KinType::m1> get_f_3_173_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
extern template FunctionObjectType<dd_real, KinType::m1> get_f_3_173_evaluator();
extern template FunctionObjectType<qd_real, KinType::m1> get_f_3_173_evaluator();
#endif

} // namespace m1_set
} // namespace PentagonFunctions
