/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_21.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_21_abbreviated (const std::array<T,11>& abb) {
T z[8];
z[0] = -abb[8] + -abb[10];
z[1] = -abb[1] + abb[2];
z[2] = abb[0] + z[1];
z[0] = z[0] * z[2];
z[3] = -abb[6] + abb[7];
z[3] = z[2] * z[3];
z[4] = -abb[2] + abb[4];
z[5] = -abb[5] * z[4];
z[3] = 2 * z[3] + z[5];
z[3] = abb[5] * z[3];
z[5] = -abb[1] + abb[3] + abb[4];
z[6] = prod_pow(abb[6], 2) * z[5];
z[4] = -prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[1] = abb[3] + z[1];
z[7] = -prod_pow(abb[7], 2) * z[1];
z[0] = 2 * z[0] + z[3] + z[4] + z[6] + z[7];
z[3] = -abb[6] * z[5];
z[1] = abb[7] * z[1];
z[1] = z[1] + z[3];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[9] * z[2];
z[1] = z[1] + z[2];
z[1] = 2 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_21_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_21_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_21_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2.8111327179967931607384621082221675934641574062881713182712087439"),stof<T>("3.2938068479286826319182992924288602607157204455706257746403788839")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,11> abb = {dl[3], dlog_W10(k,dl), dl[4], dlog_W21(k,dl), dlog_W27(k,dl), f_1_2(k), f_1_3(k), f_1_6(k), f_2_5(k), f_2_6_im(k), f_2_6_re(k)};

                    
            return f_3_21_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_21_DLogXconstant_part(base_point<T>, kend);
	value += f_3_21_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_21_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_21_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_21_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_21_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_21_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_21_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
