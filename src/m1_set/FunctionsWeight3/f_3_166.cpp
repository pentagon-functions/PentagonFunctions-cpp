/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_166.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_166_abbreviated (const std::array<T,44>& abb) {
T z[39];
z[0] = 3 * abb[1] + -abb[10];
z[1] = 2 * abb[4];
z[2] = z[0] + z[1];
z[3] = -abb[5] + 2 * abb[8];
z[4] = 2 * abb[9];
z[5] = abb[2] + abb[3];
z[6] = -z[2] + z[3] + z[4] + -z[5];
z[7] = abb[40] * z[6];
z[8] = abb[0] + abb[2];
z[9] = -abb[8] + z[8];
z[9] = abb[26] * z[9];
z[10] = -abb[1] + abb[10];
z[11] = abb[43] * z[10];
z[12] = abb[0] + abb[3];
z[13] = -abb[12] + z[12];
z[13] = abb[27] * z[13];
z[8] = -abb[11] + z[8];
z[8] = abb[28] * z[8];
z[7] = z[7] + z[8] + 2 * z[9] + z[11] + z[13];
z[8] = 3 * abb[4];
z[9] = abb[8] + -z[5];
z[0] = -abb[11] + -abb[12] + z[0] + z[4] + z[8] + 2 * z[9];
z[0] = prod_pow(abb[24], 2) * z[0];
z[9] = 3 * abb[2];
z[11] = 2 * abb[5];
z[13] = 2 * abb[12];
z[14] = -abb[3] + -abb[9] + z[9] + z[11] + z[13];
z[15] = 5 * abb[8] + -z[14];
z[16] = abb[31] * z[15];
z[17] = abb[17] + -abb[20];
z[18] = 2 * abb[1];
z[19] = -abb[0] + abb[4] + -abb[10] + z[18];
z[17] = z[17] * z[19];
z[19] = -abb[4] + z[10];
z[20] = abb[24] * z[19];
z[21] = abb[19] * z[19];
z[17] = z[17] + 2 * z[20] + -z[21];
z[17] = abb[19] * z[17];
z[22] = abb[8] + abb[9];
z[23] = z[5] + -z[22];
z[24] = 2 * abb[0];
z[25] = -z[23] + -z[24];
z[25] = abb[33] * z[25];
z[26] = 2 * abb[11];
z[11] = z[11] + z[26];
z[27] = 3 * abb[3];
z[28] = -abb[2] + -abb[8] + z[11] + z[27];
z[29] = 5 * abb[9] + -z[28];
z[30] = abb[32] * z[29];
z[31] = -abb[5] + z[24];
z[32] = -abb[3] + 2 * abb[6] + z[31];
z[33] = prod_pow(abb[23], 2) * z[32];
z[34] = -abb[43] * z[1];
z[12] = -abb[9] + z[12];
z[12] = abb[25] * z[12];
z[35] = -abb[2] + 2 * abb[7] + z[31];
z[36] = prod_pow(abb[18], 2) * z[35];
z[37] = -abb[15] + -abb[16];
z[37] = abb[35] * z[37];
z[38] = abb[13] + abb[14];
z[38] = abb[34] * z[38];
z[0] = z[0] + 2 * z[7] + 4 * z[12] + z[16] + z[17] + z[25] + z[30] + z[33] + z[34] + z[36] + z[37] + z[38];
z[7] = z[13] + -z[22];
z[12] = 4 * abb[1];
z[13] = z[7] + -z[12];
z[16] = 2 * abb[10];
z[9] = -4 * abb[5] + z[1] + z[9] + -z[13] + -z[16] + -z[26] + z[27];
z[9] = abb[17] * z[9];
z[17] = abb[5] + abb[11];
z[2] = -abb[12] + z[2] + -z[17] + z[22];
z[25] = abb[24] * z[2];
z[9] = z[9] + -4 * z[25];
z[9] = abb[17] * z[9];
z[15] = abb[17] * z[15];
z[3] = abb[2] + -z[3];
z[25] = abb[18] * z[3];
z[27] = abb[1] + -abb[9];
z[30] = 2 * abb[2] + abb[5] + -3 * abb[8] + abb[12] + z[27];
z[30] = abb[24] * z[30];
z[25] = z[25] + z[30];
z[30] = -abb[2] + abb[3];
z[7] = z[7] + -z[30];
z[7] = abb[20] * z[7];
z[18] = -z[18] + -z[23];
z[18] = abb[21] * z[18];
z[7] = z[7] + z[15] + z[18] + 2 * z[25];
z[15] = 4 * abb[7];
z[14] = 9 * abb[8] + -z[12] + -z[14] + -z[15];
z[14] = abb[22] * z[14];
z[7] = 2 * z[7] + z[14];
z[7] = abb[22] * z[7];
z[14] = 2 * abb[24];
z[2] = z[2] * z[14];
z[13] = -z[5] + z[13];
z[8] = -3 * abb[0] + abb[10] + -z[8] + z[11] + z[13];
z[8] = abb[17] * z[8];
z[2] = z[2] + z[8];
z[8] = -z[13] + -z[24] + -z[26];
z[8] = abb[20] * z[8];
z[2] = 2 * z[2] + z[8];
z[2] = abb[20] * z[2];
z[8] = -z[22] + z[26] + z[30];
z[8] = abb[20] * z[8];
z[11] = abb[1] + -abb[8];
z[14] = 2 * abb[3] + -3 * abb[9] + z[11] + z[17];
z[14] = abb[24] * z[14];
z[4] = abb[3] + abb[5] + -z[4];
z[17] = abb[23] * z[4];
z[14] = z[14] + z[17];
z[17] = abb[17] * z[29];
z[8] = z[8] + 2 * z[14] + z[17];
z[14] = 4 * abb[6];
z[12] = 9 * abb[9] + -z[12] + -z[14] + -z[28];
z[12] = abb[21] * z[12];
z[8] = 2 * z[8] + z[12];
z[8] = abb[21] * z[8];
z[12] = 10 * abb[5] + -z[13] + z[16];
z[12] = 4 * abb[4] + abb[11] * (T(-2) / T(3)) + (T(1) / T(3)) * z[12] + -z[14] + -z[15];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[13] = abb[42] * z[4];
z[14] = abb[41] * z[3];
z[13] = z[13] + z[14];
z[11] = -abb[2] + -z[11];
z[11] = abb[30] * z[11];
z[14] = -abb[3] + -z[27];
z[14] = abb[29] * z[14];
z[11] = z[11] + z[14];
z[0] = 2 * z[0] + z[2] + z[7] + z[8] + z[9] + 8 * z[11] + z[12] + 4 * z[13];
z[0] = 8 * z[0];
z[2] = abb[5] + -z[5] + z[10] + z[24];
z[2] = abb[17] * z[2];
z[1] = z[1] + -z[10] + z[31];
z[1] = abb[20] * z[1];
z[5] = -abb[23] * z[32];
z[7] = -abb[18] * z[35];
z[1] = z[1] + z[2] + z[5] + z[7] + -z[20] + z[21];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[36] * z[6];
z[4] = abb[38] * z[4];
z[5] = abb[39] * z[19];
z[3] = abb[37] * z[3];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
z[1] = 32 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_166_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_166_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_166_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("37.009253017594597469461294220412827787228855198799705558912825398"),stof<T>("69.75861582402055809624458205424509132618783746675399810429836622")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,44> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dl[3], dl[4], dlog_W14(k,dl), dlog_W23(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W86(k,dl), dlog_W87(k,dl), dlog_W118(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W135(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_21(k), f_2_23(k), f_2_29_im(k), f_2_2_im(k), f_2_9_im(k), f_2_10_im(k), f_2_12_im(k), f_2_2_re(k), f_2_9_re(k), f_2_10_re(k), f_2_12_re(k)};

                    
            return f_3_166_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_166_DLogXconstant_part(base_point<T>, kend);
	value += f_3_166_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_166_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_166_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_166_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_166_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_166_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_166_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
