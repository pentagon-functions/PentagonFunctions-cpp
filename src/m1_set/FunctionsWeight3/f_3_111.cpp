/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_111.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_111_abbreviated (const std::array<T,16>& abb) {
T z[11];
z[0] = abb[2] + -abb[5];
z[1] = prod_pow(abb[9], 2);
z[2] = -z[0] * z[1];
z[3] = prod_pow(m1_set::bc<T>[0], 2);
z[4] = abb[4] * (T(-11) / T(3)) + abb[3] * (T(9) / T(2)) + (T(-5) / T(3)) * z[0];
z[4] = z[3] * z[4];
z[1] = -z[1] + (T(-5) / T(2)) * z[3];
z[1] = abb[0] * z[1];
z[1] = z[1] + z[2] + z[4];
z[2] = abb[1] + abb[4] + abb[0] * (T(1) / T(2));
z[4] = 2 * abb[3];
z[5] = (T(1) / T(2)) * z[0] + -z[2] + z[4];
z[6] = abb[15] * z[5];
z[7] = abb[6] + -abb[7];
z[8] = abb[3] + z[0];
z[7] = z[7] * z[8];
z[8] = abb[0] + z[0];
z[8] = abb[8] * z[8];
z[8] = z[7] + (T(1) / T(2)) * z[8];
z[8] = abb[8] * z[8];
z[2] = abb[3] * (T(5) / T(2)) + z[0] + -z[2];
z[9] = abb[13] * z[2];
z[4] = -abb[0] + z[0] + z[4];
z[10] = abb[14] * z[4];
z[3] = abb[1] * z[3];
z[1] = (T(1) / T(2)) * z[1] + (T(-11) / T(6)) * z[3] + z[6] + z[8] + z[9] + z[10];
z[3] = abb[12] * z[5];
z[0] = -abb[0] + -abb[3] + -2 * z[0];
z[0] = abb[8] * z[0];
z[0] = z[0] + -z[7];
z[0] = m1_set::bc<T>[0] * z[0];
z[4] = abb[11] * z[4];
z[2] = abb[10] * z[2];
z[0] = z[0] + z[2] + z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_111_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_111_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_111_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-10.06342691681288063712113859634388210525671849263526456059770017"),stof<T>("33.465379026645607095681214879921287040501941913339209954365611879")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,16> abb = {dl[0], dl[5], dl[2], dlog_W15(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_4_im(k), f_2_12_im(k), f_2_22_im(k), f_2_4_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_3_111_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_111_DLogXconstant_part(base_point<T>, kend);
	value += f_3_111_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_111_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_111_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_111_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_111_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_111_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_111_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
