/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_114.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_114_abbreviated (const std::array<T,27>& abb) {
T z[20];
z[0] = abb[1] + abb[3];
z[1] = -abb[5] + z[0];
z[2] = abb[0] + abb[2];
z[3] = z[1] + z[2];
z[4] = abb[17] * z[3];
z[5] = -abb[4] + z[3];
z[6] = abb[15] * z[5];
z[7] = abb[0] + z[0];
z[8] = abb[19] * z[7];
z[9] = abb[16] + abb[19];
z[9] = abb[2] * z[9];
z[10] = abb[5] * abb[16];
z[11] = abb[6] * (T(1) / T(2));
z[12] = -abb[20] * z[11];
z[13] = abb[5] + z[2];
z[13] = abb[18] * z[13];
z[4] = z[4] + z[6] + z[8] + z[9] + z[10] + z[12] + z[13];
z[6] = abb[4] + abb[5];
z[6] = abb[12] * z[6];
z[8] = abb[5] + -z[7];
z[8] = abb[9] * z[8];
z[9] = z[6] + -z[8];
z[10] = -abb[8] + abb[11];
z[10] = abb[4] * z[10];
z[12] = abb[8] * z[7];
z[13] = z[10] + z[12];
z[14] = abb[10] * (T(1) / T(2));
z[15] = -z[0] * z[14];
z[16] = abb[7] * (T(1) / T(2));
z[17] = -abb[4] + -z[7];
z[17] = z[16] * z[17];
z[18] = abb[5] * abb[8];
z[19] = abb[0] + 3 * z[0];
z[19] = -abb[5] + (T(1) / T(4)) * z[19];
z[19] = abb[11] * z[19];
z[9] = (T(1) / T(2)) * z[9] + (T(-1) / T(4)) * z[13] + z[15] + z[17] + z[18] + z[19];
z[9] = m1_set::bc<T>[0] * z[9];
z[4] = (T(1) / T(2)) * z[4] + z[9];
z[4] = (T(1) / T(8)) * z[4];
z[8] = z[8] + (T(1) / T(2)) * z[12];
z[9] = z[0] + z[2];
z[12] = -abb[10] * z[9];
z[13] = abb[2] + -abb[4];
z[13] = z[13] * z[16];
z[15] = -abb[5] + (T(1) / T(2)) * z[7];
z[15] = abb[11] * z[15];
z[6] = z[6] + -z[8] + (T(-1) / T(2)) * z[10] + z[12] + z[13] + z[15] + z[18];
z[6] = abb[7] * z[6];
z[10] = -abb[11] + abb[12] * (T(1) / T(2));
z[10] = z[1] * z[10];
z[12] = -abb[4] * abb[8];
z[10] = z[10] + z[12] + -z[18];
z[10] = abb[12] * z[10];
z[2] = z[2] * z[14];
z[1] = abb[11] * z[1];
z[1] = z[1] + z[2] + z[18];
z[1] = abb[10] * z[1];
z[2] = -abb[2] + -abb[5];
z[12] = abb[22] + abb[24];
z[2] = z[2] * z[12];
z[3] = -abb[23] * z[3];
z[12] = abb[9] * (T(1) / T(2));
z[12] = abb[5] * z[12];
z[12] = z[12] + -z[18];
z[12] = abb[9] * z[12];
z[8] = abb[11] * z[8];
z[5] = -abb[21] * z[5];
z[9] = -abb[25] * z[9];
z[13] = -abb[0] * abb[24];
z[14] = abb[13] * z[0];
z[15] = abb[8] * abb[11];
z[15] = abb[13] + (T(1) / T(2)) * z[15];
z[15] = abb[4] * z[15];
z[11] = abb[26] * z[11];
z[7] = abb[14] * z[7];
z[1] = z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15];
z[0] = -abb[0] + (T(-5) / T(6)) * z[0];
z[0] = abb[2] * (T(-1) / T(3)) + abb[4] * (T(1) / T(12)) + (T(1) / T(4)) * z[0];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[0] = z[0] + (T(1) / T(4)) * z[1];
z[0] = (T(1) / T(4)) * z[0];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_3_114_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_114_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_114_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("-0.1680781571205259684783209532609898001018838699352932802170601115")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k)};

                    
            return f_3_114_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_114_DLogXconstant_part(base_point<T>, kend);
	value += f_3_114_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_114_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_114_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_114_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_114_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_114_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_114_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
