/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_168.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_168_abbreviated (const std::array<T,42>& abb) {
T z[41];
z[0] = 5 * abb[22];
z[1] = 2 * abb[17];
z[2] = -z[0] + -z[1];
z[2] = abb[17] * z[2];
z[3] = 2 * abb[22];
z[4] = abb[17] + z[3];
z[5] = 2 * abb[24];
z[6] = z[4] * z[5];
z[7] = prod_pow(abb[22], 2);
z[8] = 3 * z[7];
z[9] = abb[17] + abb[22];
z[10] = 2 * z[9];
z[11] = -abb[20] + z[10];
z[11] = abb[20] * z[11];
z[2] = -2 * abb[31] + z[2] + z[6] + -z[8] + z[11];
z[2] = abb[0] * z[2];
z[6] = abb[3] + abb[5];
z[11] = 9 * abb[8];
z[12] = 2 * abb[10];
z[13] = 4 * z[6] + -z[11] + z[12];
z[14] = abb[39] * z[13];
z[15] = -abb[17] + z[3];
z[15] = abb[17] * z[15];
z[16] = abb[24] + -z[3];
z[16] = abb[24] * z[16];
z[16] = -2 * abb[32] + z[15] + z[16];
z[16] = abb[12] * z[16];
z[17] = 2 * abb[7];
z[18] = 2 * abb[27];
z[19] = -abb[32] + z[18];
z[19] = z[17] * z[19];
z[20] = abb[7] + abb[12];
z[21] = abb[17] + -abb[22];
z[22] = -z[20] * z[21];
z[23] = 3 * abb[7];
z[24] = -abb[12] + z[23];
z[24] = abb[21] * z[24];
z[22] = 2 * z[22] + z[24];
z[22] = abb[21] * z[22];
z[24] = -abb[14] * abb[41];
z[25] = abb[13] * abb[41];
z[26] = -abb[15] + -abb[16];
z[26] = abb[33] * z[26];
z[2] = -z[2] + z[14] + -z[16] + -z[19] + -z[22] + -z[24] + z[25] + -z[26];
z[14] = 3 * abb[18];
z[16] = -z[3] + -z[14];
z[16] = abb[18] * z[16];
z[0] = -abb[17] + -z[0] + -z[14];
z[19] = 3 * abb[21];
z[0] = 2 * z[0] + -z[19];
z[0] = abb[21] * z[0];
z[22] = abb[31] + abb[32];
z[22] = 2 * z[22];
z[24] = 12 * abb[28];
z[25] = 2 * abb[30];
z[26] = abb[18] + -abb[22];
z[26] = -abb[17] + 2 * z[26];
z[26] = abb[17] * z[26];
z[4] = abb[24] * z[4];
z[0] = 8 * abb[38] + z[0] + 4 * z[4] + -z[8] + z[16] + -z[22] + z[24] + z[25] + z[26];
z[0] = abb[5] * z[0];
z[4] = z[7] + z[25];
z[8] = abb[18] + abb[22];
z[16] = abb[17] + 2 * z[8];
z[16] = abb[17] * z[16];
z[16] = z[4] + z[16];
z[10] = -abb[24] + -z[10];
z[10] = z[5] * z[10];
z[26] = -abb[31] + z[18];
z[27] = 5 * abb[32] + -z[26];
z[28] = -abb[18] + z[3];
z[29] = -abb[18] * z[28];
z[8] = 5 * abb[17] + z[8];
z[8] = abb[21] + 2 * z[8];
z[8] = abb[21] * z[8];
z[8] = z[8] + z[10] + z[16] + -z[24] + 2 * z[27] + z[29];
z[8] = abb[11] * z[8];
z[10] = abb[18] + z[19];
z[19] = z[1] + -z[3] + z[10];
z[19] = abb[6] * z[19];
z[20] = abb[10] + z[20];
z[20] = z[20] * z[21];
z[24] = -abb[3] + -abb[9] + abb[12];
z[24] = abb[21] * z[24];
z[10] = abb[8] * z[10];
z[27] = abb[5] * abb[18];
z[29] = abb[18] + abb[21];
z[30] = -abb[11] * z[29];
z[19] = z[10] + z[19] + z[20] + z[24] + z[27] + z[30];
z[20] = abb[5] + -abb[12];
z[23] = 2 * abb[3] + -5 * abb[6] + -abb[11] + z[20] + -z[23];
z[11] = -z[11] + 2 * z[23];
z[11] = abb[19] * z[11];
z[11] = z[11] + 4 * z[19];
z[11] = abb[19] * z[11];
z[19] = 2 * abb[18];
z[23] = -abb[20] + z[19];
z[24] = -abb[21] + -abb[22] + -z[23];
z[24] = abb[3] * z[24];
z[27] = 2 * abb[21] + abb[22] + -abb[24];
z[27] = abb[5] * z[27];
z[24] = z[24] + -z[27];
z[27] = -abb[7] * abb[21];
z[30] = abb[6] * abb[17];
z[31] = -abb[17] + abb[24];
z[32] = abb[21] + -z[31];
z[33] = -abb[11] * z[32];
z[27] = -z[24] + z[27] + z[30] + z[33];
z[30] = 3 * abb[8];
z[33] = 2 * abb[9];
z[34] = z[30] + z[33];
z[35] = abb[3] + abb[6];
z[36] = 2 * abb[5];
z[37] = abb[7] + abb[11] + -z[35] + -z[36];
z[37] = z[34] + 2 * z[37];
z[37] = abb[19] * z[37];
z[38] = abb[17] + abb[18] + -abb[20];
z[33] = z[33] * z[38];
z[10] = 2 * z[10] + z[33];
z[5] = 2 * abb[20] + z[5];
z[9] = -z[5] + z[9];
z[9] = abb[0] * z[9];
z[33] = -abb[10] * z[21];
z[39] = -abb[19] + z[21];
z[40] = 4 * abb[21] + z[39];
z[40] = abb[4] * z[40];
z[9] = z[9] + -z[10] + 2 * z[27] + z[33] + z[37] + z[40];
z[27] = abb[0] + z[36];
z[33] = -5 * abb[4] + 2 * z[27] + z[30];
z[33] = abb[23] * z[33];
z[9] = 2 * z[9] + z[33];
z[9] = abb[23] * z[9];
z[33] = 6 * abb[21];
z[28] = abb[17] + 2 * z[28] + -z[33];
z[28] = abb[17] * z[28];
z[36] = abb[28] + -abb[32];
z[37] = prod_pow(abb[18], 2);
z[23] = abb[20] * z[23];
z[23] = z[23] + -z[25] + z[28] + 6 * z[36] + -z[37];
z[25] = 2 * abb[6];
z[23] = z[23] * z[25];
z[28] = z[19] + -z[21];
z[28] = -abb[21] + 2 * z[28];
z[28] = abb[21] * z[28];
z[18] = abb[32] + z[18];
z[3] = abb[18] + z[3];
z[3] = abb[18] * z[3];
z[19] = abb[22] + -z[19];
z[19] = abb[17] * z[19];
z[4] = -4 * abb[38] + -5 * abb[39] + z[3] + -z[4] + -2 * z[18] + z[19] + z[28];
z[18] = z[21] + -z[29];
z[19] = 3 * abb[19];
z[18] = 4 * z[18] + z[19];
z[18] = abb[19] * z[18];
z[4] = 2 * z[4] + z[18];
z[4] = abb[4] * z[4];
z[18] = abb[0] + abb[7] + -abb[11];
z[18] = abb[25] * z[18];
z[28] = abb[9] + -z[35];
z[28] = abb[29] * z[28];
z[25] = 6 * abb[1] + abb[4] + -abb[9] + -abb[10] + -3 * abb[11] + z[25];
z[25] = abb[26] * z[25];
z[20] = abb[0] + z[20];
z[35] = abb[40] * z[20];
z[18] = z[18] + z[25] + z[28] + z[35];
z[25] = abb[22] + z[38];
z[28] = 4 * abb[20];
z[25] = z[25] * z[28];
z[3] = -z[3] + -z[16] + z[25];
z[14] = -z[14] + z[21];
z[14] = -abb[21] + 2 * z[14];
z[14] = abb[21] * z[14];
z[16] = abb[32] + z[26];
z[14] = z[3] + z[14] + 2 * z[16];
z[14] = abb[3] * z[14];
z[16] = abb[18] + z[21];
z[16] = abb[21] + 2 * z[16];
z[16] = abb[21] * z[16];
z[3] = -z[3] + z[16] + z[22];
z[3] = abb[9] * z[3];
z[16] = -abb[18] + z[21];
z[16] = abb[21] * z[16];
z[7] = abb[27] + abb[32] + abb[39] + z[7] + -z[15] + z[16];
z[15] = z[29] + z[39];
z[16] = -abb[23] + 2 * z[15];
z[16] = abb[23] * z[16];
z[22] = abb[21] + 2 * z[21];
z[22] = -7 * abb[19] + 4 * z[22];
z[22] = abb[19] * z[22];
z[7] = -2 * z[7] + -z[16] + z[22];
z[16] = 4 * abb[1];
z[22] = -z[7] * z[16];
z[7] = 6 * abb[26] + -z[7];
z[25] = 2 * abb[2];
z[7] = z[7] * z[25];
z[21] = -abb[17] * z[12] * z[21];
z[26] = 5 * abb[3] + 7 * abb[5] + abb[9] + abb[11];
z[26] = -abb[0] + 3 * abb[4] + -6 * abb[8] + abb[10] * (T(-4) / T(3)) + abb[2] * (T(5) / T(3)) + abb[1] * (T(10) / T(3)) + (T(1) / T(3)) * z[26];
z[26] = prod_pow(m1_set::bc<T>[0], 2) * z[26];
z[0] = z[0] + -2 * z[2] + z[3] + z[4] + z[7] + z[8] + z[9] + z[11] + z[14] + 4 * z[18] + z[21] + z[22] + z[23] + z[26];
z[0] = 4 * z[0];
z[2] = 2 * abb[11];
z[3] = -z[2] * z[32];
z[4] = -abb[12] * z[31];
z[7] = -abb[12] + z[17];
z[8] = -abb[21] * z[7];
z[3] = z[3] + z[4] + z[8] + -z[24];
z[4] = z[16] + z[25];
z[8] = -abb[23] + z[15];
z[8] = z[4] * z[8];
z[2] = z[2] + -z[6] + z[7];
z[2] = 2 * z[2] + -z[12] + z[34];
z[2] = abb[19] * z[2];
z[1] = abb[22] + z[1] + -z[5];
z[1] = abb[0] * z[1];
z[5] = -abb[22] + -z[19] + z[33];
z[5] = abb[4] * z[5];
z[6] = -4 * abb[4] + abb[10] + z[27] + z[30];
z[6] = abb[23] * z[6];
z[7] = abb[10] * abb[22];
z[1] = z[1] + z[2] + 2 * z[3] + z[5] + z[6] + z[7] + z[8] + -z[10];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = z[4] + -z[13];
z[2] = abb[35] * z[2];
z[3] = -abb[13] + -abb[14];
z[3] = abb[37] * z[3];
z[4] = 4 * abb[34];
z[5] = abb[5] * z[4];
z[4] = -5 * abb[35] + -z[4];
z[4] = abb[4] * z[4];
z[6] = abb[36] * z[20];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + 2 * z[6];
z[1] = 8 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_168_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_168_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_168_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("74.154641676792018346869118468696445371683790093427244811691426156"),stof<T>("55.547468842577761992396315371334888965067282070012368742228760553")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,42> abb = {dl[0], dl[1], dlog_W7(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W73(k,dl), dlog_W119(k,dv), dlog_W125(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_21(k), f_2_28_im(k), f_2_6_im(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), f_2_6_re(k), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_3_168_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_168_DLogXconstant_part(base_point<T>, kend);
	value += f_3_168_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_168_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_168_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_168_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_168_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_168_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_168_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
