/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_52.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_52_abbreviated (const std::array<T,31>& abb) {
T z[28];
z[0] = -abb[6] + abb[3] * (T(3) / T(2));
z[1] = abb[7] * (T(3) / T(2));
z[2] = z[0] + -z[1];
z[3] = abb[0] + abb[2];
z[4] = 3 * abb[8];
z[5] = z[3] + -z[4];
z[6] = 2 * abb[5];
z[7] = 2 * abb[4];
z[8] = z[2] + -z[5] + -z[6] + z[7];
z[9] = abb[15] * z[8];
z[10] = abb[2] * (T(1) / T(2));
z[11] = abb[4] + abb[6] * (T(-1) / T(2)) + abb[3] * (T(3) / T(4)) + -z[10];
z[12] = abb[0] * (T(1) / T(2));
z[13] = abb[10] + abb[1] * (T(1) / T(2)) + abb[7] * (T(9) / T(4)) + -z[4] + -z[11] + z[12];
z[13] = abb[14] * z[13];
z[14] = 2 * abb[10];
z[15] = abb[2] + z[14];
z[16] = -abb[1] + z[4] + -z[15];
z[16] = abb[16] * z[16];
z[5] = z[5] + z[14];
z[5] = abb[13] * z[5];
z[17] = abb[5] + -abb[10];
z[18] = abb[0] + z[17];
z[18] = 2 * z[18];
z[19] = abb[18] * z[18];
z[5] = z[5] + z[9] + z[13] + z[16] + z[19];
z[5] = abb[14] * z[5];
z[9] = abb[28] * z[8];
z[13] = abb[3] + z[7];
z[16] = 3 * abb[1] + -abb[9] + -abb[10] + z[13];
z[20] = 2 * abb[16];
z[16] = z[16] * z[20];
z[20] = -abb[4] + abb[9];
z[21] = 2 * abb[1] + -z[20];
z[22] = -abb[0] + z[21];
z[22] = abb[17] * z[22];
z[16] = z[16] + -z[22];
z[22] = 4 * abb[1] + 2 * abb[3] + 3 * abb[4] + -abb[9];
z[14] = abb[0] + -z[14] + z[22];
z[14] = abb[18] * z[14];
z[21] = -abb[10] + z[21];
z[23] = -abb[6] + abb[3] * (T(-1) / T(2)) + -z[10] + -z[21];
z[23] = abb[13] * z[23];
z[14] = z[14] + z[16] + z[23];
z[14] = abb[13] * z[14];
z[23] = abb[2] + 2 * abb[6];
z[24] = 2 * abb[9];
z[4] = 5 * abb[1] + abb[7] + -z[4] + z[13] + z[23] + -z[24];
z[13] = abb[27] * z[4];
z[7] = z[0] + z[7] + z[12];
z[12] = -abb[1] + abb[7] + abb[8] * (T(-5) / T(2)) + -z[7] + z[15];
z[12] = abb[22] * z[12];
z[7] = abb[10] + abb[1] * (T(-5) / T(2)) + -z[7] + z[10];
z[7] = prod_pow(abb[16], 2) * z[7];
z[10] = 2 * abb[18];
z[15] = abb[0] + -abb[1];
z[10] = z[10] * z[15];
z[10] = z[10] + -z[16];
z[10] = abb[18] * z[10];
z[15] = -abb[10] + z[6];
z[11] = abb[7] * (T(-3) / T(4)) + -z[11] + z[15];
z[11] = abb[15] * z[11];
z[11] = z[11] + -z[19];
z[11] = abb[15] * z[11];
z[19] = abb[3] + abb[7];
z[19] = 4 * abb[4] + -5 * abb[5] + abb[6] + abb[0] * (T(-1) / T(4)) + abb[1] * (T(17) / T(4)) + (T(5) / T(2)) * z[19] + -z[24];
z[19] = prod_pow(m1_set::bc<T>[0], 2) * z[19];
z[24] = abb[11] + abb[12];
z[25] = -abb[30] * z[24];
z[3] = abb[8] + -z[3];
z[3] = abb[19] * z[3];
z[26] = abb[1] + abb[2] + -abb[8];
z[26] = abb[21] * z[26];
z[27] = -abb[29] * z[18];
z[20] = abb[1] + -z[20];
z[20] = abb[20] * z[20];
z[3] = z[3] + z[5] + z[7] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + (T(1) / T(3)) * z[19] + 2 * z[20] + z[25] + z[26] + z[27];
z[5] = abb[4] + -z[15];
z[0] = -abb[2] + z[0] + z[1] + 2 * z[5];
z[0] = abb[15] * z[0];
z[1] = abb[0] + z[6] + -z[22];
z[1] = abb[18] * z[1];
z[5] = abb[3] + 2 * z[21] + z[23];
z[5] = abb[13] * z[5];
z[6] = -abb[4] + z[17];
z[2] = -z[2] + 2 * z[6];
z[2] = abb[14] * z[2];
z[0] = z[0] + z[1] + z[2] + z[5] + -z[16];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[23] * z[4];
z[2] = abb[24] * z[8];
z[4] = -abb[26] * z[24];
z[5] = -abb[25] * z[18];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_3_52_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_52_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_52_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-24.89477730353180478042627473227128517171704751141128618134633834"),stof<T>("2.654793899918186853379126191813447075753359855666673284926142546")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dl[5], dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W91(k,dl), dlog_W119(k,dv), dlog_W128(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k)};

                    
            return f_3_52_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_52_DLogXconstant_part(base_point<T>, kend);
	value += f_3_52_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_52_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_52_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_52_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_52_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_52_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_52_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
