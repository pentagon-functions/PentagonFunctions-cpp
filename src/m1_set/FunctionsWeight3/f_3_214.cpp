/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_214.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_214_abbreviated (const std::array<T,38>& abb) {
T z[14];
z[0] = abb[5] + abb[7];
z[1] = 2 * abb[11] + -abb[12];
z[2] = abb[15] + z[1];
z[2] = -z[0] * z[2];
z[3] = abb[4] + abb[6];
z[4] = -abb[11] + abb[15];
z[4] = z[3] * z[4];
z[5] = abb[10] + abb[14];
z[5] = abb[8] * z[5];
z[2] = z[2] + z[4] + z[5];
z[2] = m1_set::bc<T>[0] * z[2];
z[4] = -abb[8] + z[0];
z[5] = abb[3] + z[3];
z[6] = z[4] + z[5];
z[6] = abb[22] * z[6];
z[4] = z[3] + z[4];
z[4] = abb[25] * z[4];
z[7] = -abb[27] * z[3];
z[8] = -abb[27] + -abb[28];
z[8] = abb[8] * z[8];
z[9] = -abb[6] + -abb[8];
z[9] = abb[23] * z[9];
z[10] = -abb[0] + -abb[1] + -abb[2] + 2 * abb[9];
z[11] = -abb[37] * z[10];
z[12] = -abb[26] * z[5];
z[13] = abb[6] + z[0];
z[13] = abb[24] * z[13];
z[2] = z[2] + z[4] + z[6] + z[7] + z[8] + z[9] + z[11] + z[12] + z[13];
z[2] = 24 * z[2];
z[4] = abb[34] + abb[35];
z[6] = abb[30] + abb[33];
z[7] = abb[20] + abb[31] + -abb[32] + z[4] + -z[6];
z[7] = abb[6] * z[7];
z[8] = -abb[17] + abb[19] + z[6];
z[4] = z[4] + -z[8];
z[4] = abb[4] * z[4];
z[9] = -abb[6] * abb[11];
z[11] = abb[15] * z[5];
z[9] = z[9] + z[11];
z[9] = abb[12] * z[9];
z[11] = abb[17] + -abb[18] + -abb[30] + abb[34];
z[11] = abb[3] * z[11];
z[5] = -abb[21] * z[5];
z[10] = abb[29] * z[10];
z[4] = z[4] + z[5] + z[7] + z[9] + -z[10] + z[11];
z[5] = abb[13] + -abb[14];
z[7] = abb[11] * z[5];
z[7] = abb[18] + abb[21] + abb[32] + -z[7] + z[8];
z[8] = -abb[10] + abb[16];
z[9] = abb[11] + z[8];
z[9] = -abb[15] + 2 * z[9];
z[9] = abb[15] * z[9];
z[1] = abb[12] * z[1];
z[1] = z[1] + 2 * z[7] + -z[9];
z[0] = -z[0] * z[1];
z[1] = abb[10] * abb[14];
z[1] = abb[31] + abb[35] + abb[36] + z[1] + z[6];
z[6] = prod_pow(abb[16], 2);
z[7] = prod_pow(abb[13], 2);
z[1] = 2 * z[1] + -z[6] + -z[7];
z[1] = abb[8] * z[1];
z[5] = abb[4] * z[5];
z[6] = abb[10] + -abb[14];
z[7] = abb[6] * z[6];
z[5] = z[5] + z[7];
z[7] = -abb[4] + abb[6];
z[7] = abb[11] * z[7];
z[5] = 2 * z[5] + z[7];
z[5] = abb[11] * z[5];
z[6] = -z[3] * z[6];
z[7] = abb[3] * z[8];
z[6] = z[6] + z[7];
z[3] = -2 * abb[3] + -z[3];
z[3] = abb[15] * z[3];
z[3] = z[3] + 2 * z[6];
z[3] = abb[15] * z[3];
z[0] = z[0] + z[1] + z[3] + 2 * z[4] + z[5];
z[1] = abb[8] * prod_pow(m1_set::bc<T>[0], 2);
z[0] = 3 * z[0] + 2 * z[1];
z[0] = 4 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_3_214_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_214_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_214_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-536.35177262486378494881452151823616271611194397733401242027767537"),stof<T>("-51.37164717923753261100963073284981183353724928451243156771197783")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,38> abb = {dlog_W7(k,dl), dlog_W8(k,dl), dl[4], dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlr[0], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_27_im(k), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_27_re(k)};

                    
            return f_3_214_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_214_DLogXconstant_part(base_point<T>, kend);
	value += f_3_214_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_214_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_214_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_214_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_214_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_214_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_214_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
