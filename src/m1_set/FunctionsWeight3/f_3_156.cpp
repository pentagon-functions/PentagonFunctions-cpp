/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_156.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_156_abbreviated (const std::array<T,51>& abb) {
T z[62];
z[0] = 6 * abb[9];
z[1] = 2 * abb[15];
z[2] = z[0] + z[1];
z[3] = 2 * abb[5];
z[4] = 3 * abb[4];
z[5] = z[3] + z[4];
z[6] = z[2] + -z[5];
z[7] = 2 * abb[6];
z[8] = -abb[13] + z[7];
z[9] = 4 * abb[12];
z[10] = 2 * abb[3];
z[11] = -z[6] + -z[8] + z[9] + -z[10];
z[12] = abb[33] * z[11];
z[13] = abb[3] + abb[6];
z[14] = abb[4] + abb[8];
z[15] = 4 * abb[2] + -z[1] + -z[13] + 3 * z[14];
z[16] = 3 * abb[14];
z[17] = 2 * abb[0];
z[18] = z[16] + -z[17];
z[19] = -z[15] + z[18];
z[19] = abb[46] * z[19];
z[20] = 3 * abb[3];
z[21] = 2 * abb[16];
z[22] = z[20] + -z[21];
z[23] = 3 * abb[7];
z[24] = z[22] + z[23];
z[25] = 4 * abb[10];
z[18] = -abb[5] + -abb[8] + -z[18] + z[24] + z[25];
z[26] = abb[48] * z[18];
z[27] = -z[4] + z[16];
z[25] = -z[25] + z[27];
z[28] = -abb[5] + z[25];
z[29] = 3 * abb[12];
z[30] = -z[17] + z[29];
z[31] = -abb[8] + abb[13];
z[32] = z[28] + z[30] + z[31];
z[33] = abb[49] * z[32];
z[34] = 3 * abb[9] + abb[16];
z[35] = 2 * abb[13];
z[36] = abb[8] + z[34] + -z[35];
z[36] = 2 * z[36];
z[37] = abb[5] + -z[36];
z[37] = abb[34] * z[37];
z[38] = 2 * abb[1];
z[39] = -abb[7] + z[38];
z[40] = abb[8] + -abb[11] + z[39];
z[41] = -abb[5] + abb[12];
z[40] = 3 * z[40] + z[41];
z[42] = abb[45] * z[40];
z[43] = abb[17] + abb[18] + abb[19] + -2 * abb[20] + abb[21];
z[44] = abb[42] * z[43];
z[12] = z[12] + z[19] + -z[26] + z[33] + z[37] + z[42] + z[44];
z[19] = z[1] + z[21];
z[26] = 4 * abb[1] + -z[19];
z[33] = 2 * abb[2];
z[37] = 4 * abb[0] + -z[33];
z[42] = 4 * abb[3];
z[28] = -abb[12] + z[0] + -z[26] + z[28] + -z[31] + -z[37] + -z[42];
z[28] = abb[22] * z[28];
z[44] = 2 * abb[8];
z[24] = -abb[13] + z[5] + -z[24] + -z[29] + z[44];
z[24] = abb[28] * z[24];
z[34] = -3 * abb[1] + abb[6] + abb[15] + -z[10] + -z[31] + z[34];
z[34] = abb[24] * z[34];
z[45] = abb[12] * abb[24];
z[34] = z[34] + -2 * z[45];
z[34] = 2 * z[34];
z[46] = 2 * abb[10];
z[47] = -abb[12] + z[46];
z[48] = abb[8] + z[33];
z[49] = z[13] + z[47] + -z[48];
z[50] = 2 * abb[25];
z[50] = z[49] * z[50];
z[41] = -abb[13] + z[1] + -z[13] + -z[41];
z[41] = abb[23] * z[41];
z[51] = abb[2] + -abb[6];
z[52] = -abb[0] + abb[1];
z[53] = z[51] + z[52];
z[54] = 2 * abb[27];
z[55] = -z[53] * z[54];
z[24] = z[24] + z[28] + -z[34] + z[41] + z[50] + z[55];
z[28] = z[10] + z[26];
z[3] = z[3] + z[8] + -z[28] + -z[30] + -z[48];
z[8] = -abb[26] * z[3];
z[8] = z[8] + 2 * z[24];
z[8] = abb[26] * z[8];
z[24] = 4 * abb[11];
z[30] = z[24] + z[38];
z[41] = -z[23] + z[30];
z[48] = 3 * abb[6];
z[55] = -abb[5] + -z[4] + z[29] + z[31] + -z[41] + z[48];
z[55] = abb[28] * z[55];
z[56] = abb[5] + z[23];
z[2] = abb[6] + 3 * abb[8] + z[2] + -z[35] + z[38] + -z[56];
z[2] = abb[24] * z[2];
z[2] = z[2] + -5 * z[45] + z[55];
z[20] = 6 * abb[1] + z[20] + z[44];
z[55] = 4 * abb[6];
z[6] = 5 * abb[12] + abb[13] + -z[6] + -z[20] + z[23] + z[24] + -z[55];
z[6] = abb[23] * z[6];
z[2] = 2 * z[2] + z[6];
z[2] = abb[23] * z[2];
z[6] = 3 * abb[5];
z[23] = z[6] + z[9] + -z[27];
z[27] = -abb[2] + abb[10];
z[57] = 4 * abb[8];
z[58] = 6 * abb[0] + -z[0] + z[23] + -8 * z[27] + -z[42] + -z[55] + z[57];
z[58] = abb[25] * z[58];
z[59] = 2 * abb[9];
z[60] = -abb[4] + -abb[5] + z[59];
z[13] = z[13] + z[60];
z[61] = -abb[12] + z[13];
z[61] = abb[23] * z[61];
z[58] = z[58] + 6 * z[61];
z[58] = abb[25] * z[58];
z[11] = abb[23] * z[11];
z[29] = abb[5] + z[29];
z[36] = z[29] + -z[36];
z[36] = abb[28] * z[36];
z[11] = z[11] + z[34] + z[36] + z[50];
z[3] = -abb[22] * z[3];
z[3] = z[3] + 2 * z[11];
z[3] = abb[22] * z[3];
z[11] = -abb[13] + z[60];
z[10] = -abb[12] + z[10] + z[11] + z[38];
z[10] = abb[30] * z[10];
z[13] = -abb[32] * z[13];
z[31] = -abb[4] + z[31];
z[34] = -abb[6] + -z[31] + z[39];
z[36] = abb[47] * z[34];
z[31] = -z[17] + z[31];
z[38] = abb[14] + z[31];
z[38] = abb[43] * z[38];
z[11] = abb[3] + abb[7] + z[11];
z[39] = abb[31] * z[11];
z[10] = z[10] + z[13] + z[36] + z[38] + -z[39];
z[0] = 7 * abb[13] + -z[0] + -z[20] + -z[21] + z[29] + z[48];
z[0] = abb[28] * z[0];
z[13] = -abb[6] + z[14] + z[59];
z[13] = -5 * abb[13] + 3 * z[13] + z[21] + z[41];
z[13] = abb[24] * z[13];
z[11] = abb[29] * z[11];
z[11] = 3 * z[11] + z[13] + -6 * z[45];
z[0] = z[0] + 2 * z[11];
z[0] = abb[28] * z[0];
z[11] = -abb[25] * z[49];
z[13] = -abb[2] + z[52];
z[14] = abb[3] + -abb[8];
z[20] = z[13] + z[14] + z[47];
z[20] = abb[22] * z[20];
z[21] = -abb[6] + 2 * abb[11];
z[29] = z[14] + z[21];
z[36] = -abb[24] * z[29];
z[29] = -abb[12] + z[29];
z[29] = abb[23] * z[29];
z[11] = z[11] + z[20] + z[29] + z[36] + z[45];
z[20] = abb[27] * z[53];
z[11] = 2 * z[11] + z[20];
z[11] = z[11] * z[54];
z[19] = 8 * abb[1] + 4 * abb[13] + -z[19] + z[56];
z[20] = 12 * abb[9];
z[29] = -abb[4] + -z[44];
z[24] = 6 * abb[3] + -abb[6] + z[19] + -z[20] + -z[24] + 3 * z[29];
z[29] = prod_pow(abb[24], 2);
z[24] = z[24] * z[29];
z[27] = abb[8] + z[17] + -z[27];
z[27] = -abb[5] + 3 * z[27];
z[36] = 6 * abb[14] + -2 * z[27];
z[36] = abb[44] * z[36];
z[38] = -abb[14] + z[17] + -z[60];
z[38] = prod_pow(abb[29], 2) * z[38];
z[39] = abb[32] + abb[34];
z[29] = -abb[44] + 4 * z[29] + 3 * z[39];
z[39] = 2 * abb[12];
z[29] = z[29] * z[39];
z[0] = z[0] + z[2] + z[3] + z[8] + 6 * z[10] + z[11] + 2 * z[12] + z[24] + z[29] + z[36] + 3 * z[38] + z[58];
z[2] = -abb[3] + abb[15] + abb[16];
z[3] = 2 * abb[4] + abb[8];
z[2] = 23 * abb[0] + -27 * abb[1] + 26 * abb[11] + 4 * abb[14] + abb[10] * (T(-94) / T(3)) + abb[13] * (T(2) / T(3)) + (T(4) / T(3)) * z[2] + -2 * z[3] + z[39] + (T(1) / T(3)) * z[51];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[0] = 2 * z[0] + z[2];
z[0] = 4 * z[0];
z[1] = -z[1] + -z[5] + z[9] + z[14] + z[35] + -z[41] + z[55];
z[1] = abb[23] * z[1];
z[2] = z[20] + -z[42];
z[5] = 8 * abb[0] + -z[2] + z[23] + z[26] + -z[33] + z[35] + -z[44];
z[5] = abb[22] * z[5];
z[6] = 6 * abb[12] + -z[6] + z[35];
z[7] = -z[6] + z[7] + -z[25] + -z[28] + z[37];
z[7] = abb[26] * z[7];
z[2] = -abb[6] + 8 * abb[11] + z[2] + z[4] + -z[19] + z[57];
z[2] = abb[24] * z[2];
z[3] = abb[6] + -z[3];
z[3] = 6 * abb[7] + 3 * z[3] + z[6] + z[22] + -z[30];
z[3] = abb[28] * z[3];
z[4] = -z[13] + z[21] + -z[46];
z[4] = z[4] * z[54];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[7] + -10 * z[45] + -z[50];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[35] * z[31];
z[3] = abb[39] * z[34];
z[2] = z[2] + z[3];
z[3] = -z[15] + -z[17];
z[3] = abb[38] * z[3];
z[4] = abb[41] * z[32];
z[5] = -abb[40] * z[18];
z[6] = -abb[12] + -z[27];
z[6] = abb[36] * z[6];
z[7] = abb[35] + abb[36] + abb[38];
z[7] = z[7] * z[16];
z[8] = abb[37] * z[40];
z[9] = -abb[50] * z[43];
z[1] = z[1] + 3 * z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9];
z[1] = 16 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_156_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_156_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_156_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-546.12110979713402788733526351453758030182834021593287065670424854"),stof<T>("269.98941026483938148447451604023318929462958269594595015620026086")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,51> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W33(k,dl), dlog_W70(k,dl), dlog_W71(k,dl), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_27_im(k), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_27_re(k)};

                    
            return f_3_156_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_156_DLogXconstant_part(base_point<T>, kend);
	value += f_3_156_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_156_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_156_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_156_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_156_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_156_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_156_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
