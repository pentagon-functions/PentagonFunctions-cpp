/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_73.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_73_abbreviated (const std::array<T,33>& abb) {
T z[34];
z[0] = abb[12] * (T(1) / T(2));
z[1] = 2 * abb[2];
z[2] = z[0] + -z[1];
z[3] = abb[3] * (T(-1) / T(2)) + z[2];
z[4] = abb[10] * (T(1) / T(2));
z[5] = 2 * abb[1];
z[6] = z[4] + -z[5];
z[7] = 2 * abb[9];
z[8] = abb[5] * (T(3) / T(2)) + -z[3] + z[6] + -z[7];
z[9] = abb[21] * z[8];
z[10] = 2 * abb[3];
z[11] = -abb[11] + z[10];
z[12] = 4 * abb[1] + -abb[10];
z[13] = z[11] + z[12];
z[13] = abb[18] * z[13];
z[14] = abb[2] + abb[5];
z[15] = abb[1] + -6 * abb[3] + z[7] + z[14];
z[15] = abb[20] * z[15];
z[16] = abb[19] + -abb[22];
z[17] = -abb[18] + z[16];
z[18] = abb[5] * z[17];
z[19] = abb[11] * z[16];
z[20] = abb[3] * z[16];
z[21] = 2 * z[16];
z[22] = abb[4] * z[21];
z[9] = z[9] + z[13] + z[15] + z[18] + z[19] + -6 * z[20] + z[22];
z[9] = abb[20] * z[9];
z[15] = abb[12] * abb[22];
z[15] = z[15] + -z[19] + z[20];
z[19] = abb[22] * z[7];
z[20] = abb[2] * abb[22];
z[20] = 4 * z[20];
z[19] = z[15] + z[19] + -z[20];
z[6] = -abb[11] + abb[5] * (T(1) / T(2)) + -z[6];
z[3] = -z[3] + z[6];
z[3] = abb[21] * z[3];
z[2] = abb[3] * (T(-5) / T(2)) + z[2] + -z[6] + z[7];
z[2] = abb[20] * z[2];
z[0] = abb[1] + abb[7] + z[0] + -z[4] + -z[14];
z[0] = abb[17] * z[0];
z[4] = abb[18] + -abb[19] + 2 * abb[22];
z[4] = abb[5] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + -z[13] + -z[19];
z[0] = abb[17] * z[0];
z[2] = 2 * abb[20];
z[3] = -abb[18] + -z[2] + -z[16];
z[3] = abb[17] * z[3];
z[4] = prod_pow(m1_set::bc<T>[0], 2);
z[6] = abb[25] + 2 * abb[31] + abb[26] * (T(1) / T(2)) + (T(1) / T(4)) * z[4];
z[13] = prod_pow(abb[18], 2);
z[17] = abb[21] + z[17];
z[22] = abb[21] * z[17];
z[23] = abb[20] + z[16];
z[23] = abb[18] + abb[21] + -3 * z[23];
z[23] = abb[20] * z[23];
z[3] = z[3] + z[6] + z[13] + z[22] + z[23];
z[3] = abb[6] * z[3];
z[22] = -abb[4] + abb[8];
z[23] = 2 * abb[11];
z[24] = abb[2] + abb[7];
z[10] = z[10] + 2 * z[22] + -z[23] + z[24];
z[10] = abb[25] * z[10];
z[25] = abb[3] + -abb[11];
z[26] = -abb[18] * z[25];
z[27] = -abb[1] + -abb[2] + abb[3] + 2 * abb[5];
z[27] = abb[21] * z[27];
z[19] = 2 * z[18] + z[19] + z[26] + z[27];
z[19] = abb[21] * z[19];
z[26] = prod_pow(abb[22], 2);
z[27] = z[13] + z[26];
z[2] = abb[21] + -z[2] + -z[21];
z[2] = abb[20] * z[2];
z[21] = abb[20] + abb[21];
z[28] = -abb[19] + -z[21];
z[28] = abb[17] * z[28];
z[2] = abb[23] + z[2] + z[6] + (T(1) / T(2)) * z[27] + z[28];
z[2] = abb[0] * z[2];
z[6] = prod_pow(abb[19], 2);
z[6] = z[6] + -z[26];
z[6] = z[6] * z[22];
z[27] = z[1] * z[26];
z[5] = -abb[7] + -abb[11] + abb[3] * (T(3) / T(2)) + z[5];
z[5] = z[5] * z[13];
z[13] = abb[31] + -z[26];
z[13] = z[7] * z[13];
z[28] = abb[30] + abb[31];
z[26] = (T(-1) / T(2)) * z[26] + -3 * z[28];
z[26] = abb[5] * z[26];
z[28] = -abb[10] + abb[12] + abb[2] * (T(-55) / T(4));
z[28] = abb[5] * (T(-1) / T(6)) + abb[1] * (T(1) / T(4)) + abb[3] * (T(5) / T(6)) + abb[9] * (T(11) / T(3)) + (T(1) / T(3)) * z[28];
z[4] = z[4] * z[28];
z[28] = 2 * abb[7] + -abb[10];
z[23] = 3 * abb[1] + abb[3] + abb[6] + abb[8] + -z[23] + z[28];
z[23] = abb[24] * z[23];
z[29] = abb[13] + -abb[15] + abb[16];
z[29] = abb[14] * (T(3) / T(2)) + (T(1) / T(2)) * z[29];
z[30] = abb[32] * z[29];
z[12] = abb[3] + z[12];
z[31] = abb[30] * z[12];
z[32] = abb[4] + z[1];
z[32] = 5 * abb[3] + abb[12] + -2 * z[32];
z[33] = abb[31] * z[32];
z[14] = abb[3] + -abb[7] + -abb[8] + abb[11] * (T(3) / T(2)) + -z[14];
z[14] = abb[26] * z[14];
z[1] = -abb[5] + abb[12] + -z[1];
z[1] = abb[23] * z[1];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[9] + z[10] + z[13] + z[14] + z[19] + z[23] + z[26] + z[27] + z[30] + z[31] + z[33];
z[1] = z[8] * z[21];
z[2] = abb[1] + -z[24];
z[2] = 2 * z[2] + z[7] + z[11];
z[2] = abb[17] * z[2];
z[3] = -z[16] * z[22];
z[3] = z[3] + -z[20];
z[4] = -z[25] + z[28];
z[4] = abb[18] * z[4];
z[5] = abb[9] * abb[22];
z[1] = z[1] + z[2] + 2 * z[3] + z[4] + 6 * z[5] + z[15] + z[18];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = 3 * abb[5];
z[3] = -z[2] + z[7] + z[32];
z[3] = abb[28] * z[3];
z[4] = abb[0] + abb[6];
z[5] = abb[17] + abb[20] + z[17];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = 2 * abb[28] + z[5];
z[4] = z[4] * z[5];
z[2] = -z[2] + z[12];
z[2] = abb[27] * z[2];
z[5] = abb[29] * z[29];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_73_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_73_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_73_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("18.532805985296568558258419582201622705850006466615638653357974654"),stof<T>("28.062415966599068695583676699101002397539170265371126850982119055")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,33> abb = {dl[0], dl[1], dlog_W3(k,dl), dlog_W7(k,dl), dlog_W8(k,dl), dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W18(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_3_73_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_73_DLogXconstant_part(base_point<T>, kend);
	value += f_3_73_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_73_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_73_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_73_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_73_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_73_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_73_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
