/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_79.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_79_abbreviated (const std::array<T,34>& abb) {
T z[38];
z[0] = abb[12] * (T(1) / T(2));
z[1] = 2 * abb[2];
z[2] = z[0] + -z[1];
z[3] = abb[5] + z[2];
z[4] = abb[10] * (T(1) / T(2));
z[5] = 2 * abb[1];
z[6] = z[4] + -z[5];
z[7] = 2 * abb[9];
z[8] = z[6] + z[7];
z[9] = abb[3] * (T(3) / T(2)) + -z[8];
z[10] = abb[4] * (T(1) / T(2));
z[11] = z[3] + z[9] + z[10];
z[11] = abb[20] * z[11];
z[12] = -abb[5] + abb[12];
z[13] = abb[11] + z[12];
z[14] = 4 * abb[2];
z[15] = 2 * abb[4];
z[16] = z[13] + -z[14] + -z[15];
z[16] = abb[22] * z[16];
z[17] = -abb[19] + abb[21];
z[18] = -abb[22] + z[17];
z[18] = abb[3] * z[18];
z[11] = z[11] + -z[16] + z[18];
z[2] = -abb[11] + abb[3] * (T(1) / T(2)) + -z[2];
z[19] = 2 * abb[5];
z[8] = abb[4] * (T(-5) / T(2)) + -z[2] + z[8] + -z[19];
z[8] = abb[17] * z[8];
z[20] = 2 * abb[6];
z[21] = z[17] * z[20];
z[22] = abb[1] + abb[3];
z[23] = abb[2] + -6 * abb[4] + -3 * abb[5] + z[7] + z[22];
z[23] = abb[18] * z[23];
z[24] = abb[5] * z[17];
z[25] = abb[4] * z[17];
z[26] = abb[11] * z[17];
z[8] = z[8] + z[11] + z[21] + z[23] + -3 * z[24] + -6 * z[25] + z[26];
z[8] = abb[18] * z[8];
z[21] = z[25] + -z[26];
z[23] = 4 * abb[1] + -z[7];
z[25] = -abb[19] * z[23];
z[26] = abb[10] * abb[19];
z[24] = z[21] + z[24] + z[25] + z[26];
z[2] = z[2] + -z[6] + z[10];
z[2] = abb[20] * z[2];
z[0] = abb[2] + abb[7] + -z[0] + z[4] + -z[22];
z[0] = abb[17] * z[0];
z[4] = 2 * abb[19] + -abb[21];
z[6] = abb[22] + z[4];
z[6] = abb[3] * z[6];
z[0] = z[0] + z[2] + z[6] + z[16] + -z[24];
z[0] = abb[17] * z[0];
z[2] = abb[5] + -abb[11];
z[6] = -abb[4] + -z[2];
z[6] = abb[22] * z[6];
z[10] = -abb[1] + -abb[2] + 2 * abb[3] + abb[4] + abb[5];
z[10] = abb[20] * z[10];
z[6] = z[6] + z[10] + 2 * z[18] + z[24];
z[6] = abb[20] * z[6];
z[10] = -abb[19] + abb[21] * (T(1) / T(2));
z[10] = abb[21] * z[10];
z[16] = abb[25] * (T(1) / T(2)) + -z[10];
z[18] = prod_pow(abb[22], 2);
z[22] = -abb[20] + -abb[21];
z[22] = abb[17] * z[22];
z[24] = abb[18] + z[17];
z[24] = -abb[17] + abb[20] + -2 * z[24];
z[24] = abb[18] * z[24];
z[22] = abb[23] + z[16] + (T(1) / T(2)) * z[18] + z[22] + z[24];
z[22] = abb[0] * z[22];
z[24] = 3 * abb[3];
z[19] = 2 * abb[0] + 5 * abb[4] + abb[10] + z[19] + -z[20] + -z[23] + -z[24];
z[23] = abb[31] * z[19];
z[25] = 2 * abb[8] + -z[20];
z[27] = abb[1] + abb[7];
z[28] = 2 * abb[11];
z[15] = abb[0] + abb[5] + z[15] + z[25] + z[27] + -z[28];
z[29] = abb[32] * z[15];
z[30] = abb[4] + 2 * abb[7];
z[12] = -3 * abb[2] + -abb[8] + z[12] + z[28] + -z[30];
z[28] = abb[30] * z[12];
z[31] = abb[21] * z[4];
z[32] = prod_pow(abb[19], 2);
z[31] = z[31] + -z[32];
z[33] = abb[4] + -abb[11];
z[31] = z[31] * z[33];
z[33] = abb[19] * abb[21];
z[33] = -z[32] + z[33];
z[33] = z[25] * z[33];
z[34] = -z[10] + (T(3) / T(2)) * z[32];
z[34] = abb[1] * z[34];
z[35] = (T(1) / T(2)) * z[32];
z[10] = z[10] + z[35];
z[10] = -abb[7] * z[10];
z[16] = z[16] + -z[35];
z[16] = abb[5] * z[16];
z[2] = -abb[7] + abb[4] * (T(3) / T(2)) + z[1] + z[2];
z[2] = z[2] * z[18];
z[18] = -z[7] * z[32];
z[14] = -abb[4] + abb[12] + -z[14] + z[24];
z[14] = abb[24] * z[14];
z[24] = abb[4] + -abb[8] + abb[11] * (T(3) / T(2)) + -z[27];
z[24] = abb[25] * z[24];
z[27] = abb[10] + -z[5];
z[27] = abb[23] * z[27];
z[32] = -abb[23] + -abb[25] + -z[35];
z[32] = abb[3] * z[32];
z[35] = 3 * abb[13] + abb[14] + abb[15] + abb[16];
z[35] = (T(1) / T(2)) * z[35];
z[36] = abb[33] * z[35];
z[37] = -abb[4] + abb[7] + -abb[10] + -abb[12];
z[37] = abb[1] * (T(-17) / T(4)) + abb[2] * (T(-7) / T(12)) + abb[6] * (T(-5) / T(3)) + abb[3] * (T(-2) / T(3)) + abb[5] * (T(-1) / T(4)) + abb[0] * (T(1) / T(12)) + abb[9] * (T(11) / T(3)) + abb[8] * (T(11) / T(6)) + (T(-1) / T(3)) * z[37];
z[37] = prod_pow(m1_set::bc<T>[0], 2) * z[37];
z[0] = z[0] + z[2] + z[6] + z[8] + z[10] + z[14] + z[16] + z[18] + z[22] + z[23] + z[24] + z[27] + z[28] + z[29] + z[31] + z[32] + z[33] + z[34] + z[36] + z[37];
z[1] = -z[1] + -z[5] + z[7] + z[13] + -z[30];
z[1] = abb[17] * z[1];
z[2] = abb[4] * (T(-7) / T(2)) + -z[3] + z[9] + z[20];
z[2] = abb[18] * z[2];
z[3] = z[4] * z[25];
z[4] = -7 * abb[19] + -abb[21];
z[4] = abb[1] * z[4];
z[5] = -abb[7] * z[17];
z[6] = abb[9] * abb[19];
z[7] = -abb[18] + abb[20];
z[7] = abb[0] * z[7];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + 6 * z[6] + z[7] + z[11] + -z[21] + z[26];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[28] * z[15];
z[3] = abb[27] * z[19];
z[4] = abb[26] * z[12];
z[5] = abb[29] * z[35];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_79_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_79_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_79_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("8.332498121433851188010996181799423035828358241979703065562506468"),stof<T>("17.241546354651752069069038392202319790094035965076567946230686824")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,34> abb = {dl[0], dl[1], dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k)};

                    
            return f_3_79_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_79_DLogXconstant_part(base_point<T>, kend);
	value += f_3_79_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_79_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_79_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_79_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_79_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_79_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_79_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
