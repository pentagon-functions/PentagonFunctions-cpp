/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_160.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_160_abbreviated (const std::array<T,46>& abb) {
T z[54];
z[0] = 2 * abb[19];
z[1] = abb[16] + -abb[18];
z[2] = z[0] + -z[1];
z[2] = abb[5] * z[2];
z[3] = 3 * abb[7];
z[4] = -abb[19] + abb[22];
z[4] = z[3] * z[4];
z[5] = abb[5] * abb[22];
z[2] = -z[2] + -z[4] + 3 * z[5];
z[6] = -abb[22] + z[1];
z[7] = abb[13] * z[6];
z[8] = -z[2] + -z[7];
z[9] = abb[10] + abb[11];
z[10] = 3 * abb[4];
z[11] = 3 * abb[3];
z[12] = 4 * abb[5] + z[9] + -z[10] + -z[11];
z[12] = abb[21] * z[12];
z[13] = 2 * abb[22];
z[14] = -z[1] + z[13];
z[15] = -abb[10] * z[14];
z[16] = 4 * abb[22];
z[17] = -5 * abb[16] + -abb[18] + z[0] + z[16];
z[18] = -abb[11] * z[17];
z[19] = z[0] + z[1];
z[20] = abb[3] * z[19];
z[21] = abb[19] + z[1];
z[22] = z[13] + -z[21];
z[23] = 2 * abb[8];
z[24] = z[22] * z[23];
z[25] = -abb[4] + abb[11];
z[26] = abb[23] * z[25];
z[27] = 4 * abb[19];
z[28] = -abb[16] + -5 * abb[18] + z[27];
z[29] = abb[4] * z[28];
z[8] = 2 * z[8] + z[12] + z[15] + z[18] + z[20] + z[24] + -6 * z[26] + z[29];
z[12] = abb[5] + -abb[13];
z[11] = abb[10] + -z[11] + 2 * z[12] + z[23] + 9 * z[25];
z[11] = abb[20] * z[11];
z[8] = 2 * z[8] + z[11];
z[8] = abb[20] * z[8];
z[11] = 2 * abb[1];
z[12] = abb[2] + z[11];
z[15] = 2 * abb[5];
z[18] = z[12] + -z[15];
z[20] = abb[3] + -abb[11];
z[24] = z[18] + z[20] + z[23];
z[29] = abb[41] * z[24];
z[30] = abb[16] + abb[18];
z[31] = abb[19] + -z[30];
z[32] = z[25] * z[31];
z[32] = z[26] + z[32];
z[32] = abb[23] * z[32];
z[33] = 2 * abb[9];
z[34] = -abb[10] + z[33];
z[18] = abb[4] + z[18] + z[34];
z[18] = abb[42] * z[18];
z[35] = -abb[9] + abb[10];
z[36] = abb[39] * z[35];
z[37] = -abb[0] + -abb[3];
z[37] = abb[40] * z[37];
z[38] = -abb[0] + -abb[8] + abb[11];
z[38] = abb[38] * z[38];
z[39] = abb[4] + -abb[13];
z[40] = -abb[0] + -z[39];
z[40] = abb[43] * z[40];
z[18] = z[18] + z[29] + z[32] + z[36] + z[37] + z[38] + z[40];
z[19] = abb[4] * z[19];
z[22] = z[22] * z[33];
z[29] = 6 * abb[17];
z[17] = -z[17] + -z[29];
z[17] = abb[10] * z[17];
z[14] = -abb[11] * z[14];
z[28] = z[28] + z[29];
z[28] = abb[3] * z[28];
z[2] = -2 * z[2] + z[14] + z[17] + z[19] + z[22] + z[28];
z[14] = abb[3] + -abb[10];
z[10] = abb[11] + -z[10] + -9 * z[14] + z[15] + z[33];
z[10] = abb[21] * z[10];
z[2] = 2 * z[2] + z[10];
z[2] = abb[21] * z[2];
z[10] = -z[0] + -5 * z[1] + z[16];
z[10] = z[10] * z[13];
z[13] = -abb[22] + z[21];
z[17] = -abb[21] + z[13];
z[17] = -abb[20] + 2 * z[17];
z[17] = abb[20] * z[17];
z[13] = -abb[21] + 2 * z[13];
z[13] = abb[21] * z[13];
z[13] = z[13] + z[17];
z[17] = 2 * abb[18];
z[19] = -abb[16] + z[17];
z[19] = abb[16] * z[19];
z[22] = prod_pow(abb[18], 2);
z[28] = z[19] + -z[22];
z[29] = z[0] * z[1];
z[32] = 2 * abb[28];
z[36] = prod_pow(m1_set::bc<T>[0], 2);
z[37] = 2 * abb[29];
z[10] = z[10] + z[13] + -3 * z[28] + z[29] + z[32] + (T(-11) / T(3)) * z[36] + z[37];
z[29] = 2 * abb[2];
z[10] = z[10] * z[29];
z[38] = abb[16] * abb[18];
z[40] = abb[22] * z[6];
z[38] = abb[24] + -abb[26] + -abb[27] + -abb[29] + z[38] + z[40];
z[40] = prod_pow(abb[17], 2);
z[41] = prod_pow(abb[23], 2);
z[42] = -abb[16] + abb[22];
z[43] = abb[17] + z[42];
z[43] = -abb[21] + 2 * z[43];
z[43] = abb[21] * z[43];
z[44] = abb[23] + z[42];
z[44] = -abb[20] + 2 * z[44];
z[44] = abb[20] * z[44];
z[38] = -z[32] + 2 * z[38] + -z[40] + -z[41] + z[43] + z[44];
z[38] = abb[6] * z[38];
z[40] = 3 * abb[25] + 4 * abb[29] + -5 * z[28];
z[41] = 8 * z[1];
z[43] = abb[19] + z[41];
z[43] = abb[19] * z[43];
z[44] = -11 * abb[19] + -20 * z[1];
z[44] = 41 * abb[22] + 2 * z[44];
z[44] = abb[22] * z[44];
z[13] = 8 * abb[28] + 4 * z[13] + -19 * z[36] + 2 * z[40] + z[43] + z[44];
z[13] = abb[1] * z[13];
z[40] = 2 * z[1];
z[43] = -abb[22] + z[40];
z[43] = abb[22] * z[43];
z[44] = (T(1) / T(3)) * z[36];
z[45] = -abb[21] + -2 * z[6];
z[45] = abb[21] * z[45];
z[28] = 2 * abb[40] + z[28] + -z[32] + z[43] + z[44] + z[45];
z[45] = 2 * abb[12];
z[28] = z[28] * z[45];
z[46] = z[22] + z[37];
z[19] = z[19] + -z[46];
z[47] = z[16] * z[21];
z[48] = prod_pow(abb[19], 2);
z[49] = 12 * abb[25];
z[47] = z[19] + z[47] + -4 * z[48] + -z[49];
z[47] = abb[5] * z[47];
z[50] = 2 * abb[0];
z[51] = -z[14] + z[25] + -z[50];
z[52] = abb[44] * z[51];
z[19] = z[19] + z[43];
z[19] = abb[13] * z[19];
z[43] = abb[14] + abb[15];
z[53] = abb[37] * z[43];
z[19] = z[19] + z[47] + z[52] + -z[53];
z[47] = abb[19] * z[16];
z[27] = abb[16] * z[27];
z[52] = abb[16] + z[17];
z[52] = abb[16] * z[52];
z[27] = z[27] + -z[47] + -z[52];
z[47] = abb[17] + z[31];
z[53] = 4 * abb[17];
z[47] = z[47] * z[53];
z[53] = -z[27] + z[46] + z[47];
z[53] = abb[10] * z[53];
z[17] = -abb[19] + z[17];
z[17] = z[0] * z[17];
z[17] = z[17] + -z[49] + -z[52];
z[47] = z[17] + -z[22] + z[37] + -z[47];
z[47] = abb[3] * z[47];
z[49] = -abb[16] + -3 * abb[18];
z[49] = abb[16] * z[49];
z[52] = abb[19] * z[30];
z[49] = -abb[39] + -z[22] + z[49] + z[52];
z[36] = (T(8) / T(3)) * z[36];
z[31] = 2 * z[31];
z[52] = abb[17] + -z[31];
z[52] = abb[17] * z[52];
z[31] = abb[23] + -z[31];
z[31] = abb[23] * z[31];
z[31] = z[31] + z[36] + 2 * z[49] + z[52];
z[31] = z[31] * z[50];
z[49] = 12 * abb[27];
z[17] = z[17] + -z[46] + -z[49];
z[17] = abb[4] * z[17];
z[22] = 10 * abb[29] + z[22] + -z[27] + z[49];
z[22] = abb[11] * z[22];
z[27] = 5 * abb[22] + -2 * z[21];
z[27] = abb[22] * z[27];
z[36] = -z[27] + z[36] + -z[37];
z[23] = z[23] * z[36];
z[15] = -abb[4] + z[15];
z[20] = 5 * abb[10] + -z[15] + -z[20] + -z[33];
z[20] = z[20] * z[32];
z[12] = -z[9] + z[12];
z[12] = abb[24] * z[12];
z[32] = abb[26] * z[14];
z[12] = -z[12] + z[32];
z[0] = -abb[22] + -z[0];
z[0] = abb[22] * z[0];
z[0] = 10 * abb[25] + z[0] + 3 * z[48];
z[0] = z[0] * z[3];
z[3] = -z[27] * z[33];
z[9] = abb[3] + abb[4] + -abb[5] + 16 * abb[9] + 2 * abb[13] + z[9];
z[9] = z[9] * z[44];
z[0] = z[0] + z[2] + z[3] + z[8] + z[9] + z[10] + -12 * z[12] + z[13] + z[17] + 4 * z[18] + 2 * z[19] + z[20] + z[22] + z[23] + z[28] + z[31] + 6 * z[38] + z[47] + z[53];
z[0] = 4 * z[0];
z[1] = abb[19] + -z[1];
z[1] = abb[5] * z[1];
z[1] = z[1] + -z[5];
z[2] = -z[16] + z[21];
z[3] = -abb[9] * z[2];
z[5] = abb[5] + -abb[8] + -z[14] + z[39];
z[5] = abb[20] * z[5];
z[8] = abb[4] * abb[16];
z[9] = abb[18] + abb[22];
z[10] = abb[17] + -z[9];
z[10] = abb[10] * z[10];
z[9] = -abb[11] * z[9];
z[12] = abb[16] + -abb[17];
z[12] = abb[3] * z[12];
z[13] = abb[3] + abb[5] + -abb[9] + z[25];
z[13] = abb[21] * z[13];
z[1] = 2 * z[1] + z[3] + z[4] + z[5] + -z[7] + z[8] + z[9] + z[10] + z[12] + z[13] + z[26];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[33] * z[24];
z[4] = -z[15] + z[34];
z[4] = abb[34] * z[4];
z[2] = -m1_set::bc<T>[0] * z[2];
z[2] = -abb[30] + z[2];
z[2] = abb[8] * z[2];
z[5] = -abb[35] * z[39];
z[7] = -abb[3] * abb[32];
z[8] = abb[11] * abb[30];
z[9] = -abb[0] + z[35];
z[9] = abb[31] * z[9];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[7] + z[8] + z[9];
z[2] = abb[36] * z[51];
z[3] = -abb[19] + 2 * z[30];
z[3] = -abb[17] + -abb[23] + 2 * z[3];
z[3] = m1_set::bc<T>[0] * z[3];
z[3] = -abb[30] + -abb[32] + -abb[35] + z[3];
z[3] = z[3] * z[50];
z[4] = abb[19] + -3 * abb[22] + z[40];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = abb[34] + 2 * z[4];
z[4] = z[4] * z[29];
z[5] = 5 * abb[19] + -15 * abb[22] + z[41];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = 2 * abb[34] + z[5];
z[5] = z[5] * z[11];
z[6] = -abb[21] + -z[6];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = abb[32] + z[6];
z[6] = z[6] * z[45];
z[7] = abb[45] * z[43];
z[8] = abb[6] * m1_set::bc<T>[0] * z[42];
z[1] = 2 * z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + 12 * z[8];
z[1] = 8 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_160_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_160_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_160_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-219.79938189925117073934360115235950825900716817480459513278814313"),stof<T>("1049.97603205975223713086781883187679401294229794465264088020437439")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,46> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W76(k,dl), dlog_W77(k,dl), dlog_W135(k,dv), dlog_W136(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_14(k), f_2_19(k), f_2_20(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_10_im(k), f_2_11_im(k), f_2_12_im(k), f_2_22_im(k), f_2_26_im(k), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_10_re(k), f_2_11_re(k), f_2_12_re(k), f_2_22_re(k), f_2_26_re(k)};

                    
            return f_3_160_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_160_DLogXconstant_part(base_point<T>, kend);
	value += f_3_160_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_160_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_160_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_160_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_160_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_160_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_160_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
