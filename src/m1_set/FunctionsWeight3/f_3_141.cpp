/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_141.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_141_abbreviated (const std::array<T,21>& abb) {
T z[28];
z[0] = 2 * abb[9];
z[1] = 3 * abb[6] + -z[0];
z[2] = 2 * abb[2];
z[3] = z[1] + -z[2];
z[4] = abb[0] + -abb[7];
z[5] = 2 * abb[1];
z[6] = abb[5] + abb[8];
z[7] = z[3] + -z[4] + -z[5] + z[6];
z[8] = abb[11] * z[7];
z[1] = abb[5] + z[1];
z[9] = 2 * abb[8];
z[10] = abb[2] + abb[3];
z[11] = z[1] + -z[9] + z[10];
z[12] = -abb[13] * z[11];
z[13] = abb[3] + abb[5];
z[3] = 3 * abb[1] + -abb[8] + -z[3] + -z[13];
z[14] = abb[14] * z[3];
z[15] = -abb[8] + z[10];
z[16] = z[4] + z[15];
z[17] = abb[15] * z[16];
z[18] = -abb[9] + abb[3] * (T(1) / T(2));
z[19] = abb[6] * (T(3) / T(2));
z[20] = z[18] + z[19];
z[21] = abb[2] + -abb[5];
z[21] = (T(1) / T(2)) * z[21];
z[22] = abb[1] + z[21];
z[23] = -z[20] + z[22];
z[23] = abb[10] * z[23];
z[24] = -abb[1] + -abb[2] + abb[8];
z[25] = abb[12] * z[24];
z[8] = z[8] + z[12] + -z[14] + z[17] + z[23] + -z[25];
z[8] = abb[10] * z[8];
z[0] = 3 * abb[4] + -z[0];
z[2] = 2 * abb[3] + -z[0] + z[2] + -z[6];
z[6] = -abb[11] * z[2];
z[12] = 3 * abb[2];
z[23] = -abb[5] + -z[12];
z[9] = -abb[1] + z[9] + -z[20] + (T(1) / T(2)) * z[23];
z[9] = abb[13] * z[9];
z[23] = abb[1] + -abb[3];
z[1] = -abb[8] + z[1] + -z[23];
z[1] = abb[14] * z[1];
z[26] = -abb[4] + abb[6] + z[15];
z[26] = 3 * z[26];
z[27] = abb[15] * z[26];
z[1] = z[1] + z[6] + z[9] + -z[25] + z[27];
z[1] = abb[13] * z[1];
z[6] = abb[4] * (T(3) / T(2));
z[9] = (T(-3) / T(2)) * z[4] + -z[6] + -z[18] + z[22];
z[9] = abb[11] * z[9];
z[18] = z[4] + -z[23];
z[18] = abb[12] * z[18];
z[9] = z[9] + z[18];
z[9] = abb[11] * z[9];
z[6] = (T(1) / T(2)) * z[4] + z[6] + -z[15] + -z[19];
z[6] = abb[15] * z[6];
z[15] = abb[11] + -abb[12];
z[15] = z[15] * z[16];
z[6] = z[6] + z[15];
z[6] = abb[15] * z[6];
z[10] = abb[5] + z[0] + z[4] + -z[10];
z[15] = abb[20] * z[10];
z[12] = -abb[5] + z[12];
z[12] = -abb[8] + z[5] + (T(1) / T(2)) * z[12] + -z[20];
z[12] = abb[14] * z[12];
z[3] = abb[11] * z[3];
z[3] = z[3] + z[12] + z[25];
z[3] = abb[14] * z[3];
z[11] = -abb[18] * z[11];
z[12] = -abb[9] + -z[21];
z[12] = abb[3] + abb[1] * (T(-7) / T(6)) + abb[4] * (T(1) / T(2)) + (T(4) / T(3)) * z[4] + (T(1) / T(3)) * z[12];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[16] = abb[16] * z[24];
z[19] = -abb[17] * z[26];
z[1] = z[1] + z[3] + z[6] + z[8] + z[9] + z[11] + z[12] + z[15] + -3 * z[16] + z[19];
z[0] = -abb[2] + z[0] + 3 * z[4] + -z[5] + z[13];
z[0] = abb[11] * z[0];
z[3] = -abb[10] * z[7];
z[2] = abb[13] * z[2];
z[0] = z[0] + z[2] + z[3] + -z[14] + -z[17] + -z[18];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[19] * z[10];
z[0] = z[0] + z[2];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_141_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_141_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_141_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-19.081329061487545452916746905051417586085018676922561610732145826"),stof<T>("7.823641037201360741789647253628714272947397205084019668974641657")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,21> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W8(k,dl), dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W25(k,dl), dlog_W32(k,dl), dlog_W77(k,dl), f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_20(k), f_2_12_im(k), f_2_12_re(k)};

                    
            return f_3_141_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_141_DLogXconstant_part(base_point<T>, kend);
	value += f_3_141_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_141_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_141_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_141_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_141_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_141_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_141_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
