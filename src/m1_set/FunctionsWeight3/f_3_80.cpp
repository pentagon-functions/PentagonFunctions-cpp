/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_80.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_80_abbreviated (const std::array<T,35>& abb) {
T z[33];
z[0] = abb[12] * (T(1) / T(2));
z[1] = -abb[5] + z[0];
z[2] = abb[3] * (T(3) / T(2));
z[3] = z[1] + -z[2];
z[4] = abb[4] * (T(1) / T(2));
z[5] = abb[11] * (T(1) / T(2));
z[6] = z[4] + z[5];
z[7] = 2 * abb[0];
z[8] = -z[3] + -z[6] + z[7];
z[8] = abb[17] * z[8];
z[9] = 2 * abb[10];
z[6] = z[6] + -z[9];
z[3] = abb[0] + -z[3] + z[6];
z[10] = -abb[18] * z[3];
z[11] = -abb[20] + -abb[21] + abb[22];
z[12] = -abb[0] + abb[4];
z[11] = z[11] * z[12];
z[13] = -abb[11] + z[9];
z[12] = -z[12] + z[13];
z[14] = -abb[19] * z[12];
z[8] = z[8] + z[10] + z[11] + z[14];
z[8] = abb[20] * z[8];
z[10] = abb[5] * (T(1) / T(2));
z[11] = abb[8] + z[10];
z[14] = abb[2] * (T(1) / T(2));
z[15] = abb[4] + -z[0] + z[5] + -z[11] + z[14];
z[16] = prod_pow(abb[17], 2);
z[15] = z[15] * z[16];
z[17] = abb[3] * (T(1) / T(2));
z[0] = abb[5] + z[0] + z[6] + z[17];
z[0] = abb[17] * z[0];
z[6] = abb[0] + 4 * abb[4];
z[18] = -abb[3] + 2 * abb[5];
z[19] = z[6] + -z[9] + z[18];
z[19] = abb[18] * z[19];
z[0] = z[0] + z[19];
z[0] = abb[18] * z[0];
z[19] = 2 * abb[6];
z[20] = -z[9] + z[19];
z[11] = -abb[3] + z[11];
z[21] = -abb[0] + z[11];
z[21] = -3 * abb[1] + abb[4] + abb[7] + abb[9] * (T(9) / T(4)) + -z[14] + -z[20] + (T(1) / T(2)) * z[21];
z[21] = abb[19] * z[21];
z[18] = z[18] + -z[19];
z[6] = z[6] + z[18];
z[6] = abb[18] * z[6];
z[12] = abb[17] * z[12];
z[12] = -z[6] + z[12] + z[21];
z[12] = abb[19] * z[12];
z[19] = abb[9] * (T(3) / T(2)) + -z[19];
z[21] = abb[5] * (T(3) / T(2));
z[22] = abb[2] + -abb[8];
z[23] = -2 * abb[4] + -z[19] + -z[21] + z[22];
z[24] = abb[19] * z[23];
z[14] = abb[7] * (T(1) / T(2)) + -z[14];
z[21] = abb[8] + z[21];
z[21] = abb[4] + abb[9] * (T(-3) / T(4)) + z[14] + (T(1) / T(2)) * z[21];
z[21] = abb[21] * z[21];
z[25] = abb[2] + -abb[5];
z[26] = -abb[3] + z[25];
z[27] = abb[4] + -z[26];
z[27] = abb[17] * z[27];
z[6] = z[6] + z[21] + z[24] + z[27];
z[6] = abb[21] * z[6];
z[21] = abb[33] * z[23];
z[24] = abb[0] * (T(1) / T(2));
z[4] = abb[8] + -z[4] + z[14] + -z[24];
z[4] = abb[22] * z[4];
z[14] = abb[17] + -abb[18];
z[27] = -abb[0] + abb[12];
z[28] = -abb[3] + z[27];
z[14] = z[14] * z[28];
z[29] = -abb[2] + abb[7];
z[30] = -abb[21] * z[29];
z[4] = z[4] + z[14] + z[30];
z[4] = abb[22] * z[4];
z[14] = abb[8] + abb[11] + abb[12] + abb[5] * (T(1) / T(4));
z[14] = -2 * abb[9] + abb[10] * (T(-11) / T(3)) + abb[4] * (T(-5) / T(6)) + abb[6] * (T(5) / T(3)) + abb[1] * (T(13) / T(4)) + (T(1) / T(3)) * z[14] + z[17] + -z[24];
z[14] = prod_pow(m1_set::bc<T>[0], 2) * z[14];
z[7] = 3 * abb[4] + z[7] + z[13] + z[18];
z[13] = -abb[32] * z[7];
z[17] = -abb[4] + 2 * abb[8] + -z[25] + z[27];
z[18] = abb[31] * z[17];
z[24] = 2 * abb[7];
z[25] = -abb[11] + z[24];
z[26] = -abb[0] + -z[25] + z[26];
z[26] = abb[23] * z[26];
z[27] = -abb[0] + -z[22];
z[27] = abb[26] * z[27];
z[30] = -abb[3] + -abb[4] + abb[12];
z[30] = abb[24] * z[30];
z[16] = abb[26] + -z[16];
z[16] = abb[7] * z[16];
z[31] = -3 * abb[13] + -abb[14] + abb[15] + abb[16];
z[31] = (T(1) / T(2)) * z[31];
z[32] = abb[34] * z[31];
z[29] = abb[25] * z[29];
z[0] = z[0] + z[4] + z[6] + z[8] + z[12] + z[13] + z[14] + z[15] + z[16] + z[18] + z[21] + z[26] + z[27] + z[29] + z[30] + z[32];
z[1] = abb[0] + abb[4] * (T(7) / T(2)) + -z[1] + -z[2] + -z[5] + -z[20];
z[1] = abb[18] * z[1];
z[2] = -abb[1] + abb[10];
z[2] = abb[3] + -abb[4] + 4 * abb[6] + abb[9] * (T(-9) / T(2)) + -6 * z[2] + -z[10] + z[22] + -z[25];
z[2] = abb[19] * z[2];
z[4] = -z[9] + z[17] + z[24];
z[4] = abb[17] * z[4];
z[3] = -abb[20] * z[3];
z[5] = abb[4] + z[11] + z[19];
z[5] = abb[21] * z[5];
z[6] = -abb[22] * z[28];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[29] * z[23];
z[3] = -abb[28] * z[7];
z[4] = abb[27] * z[17];
z[5] = abb[30] * z[31];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_80_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_80_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_80_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.7408392207032396266297041761370598395165056912275364397835719006"),stof<T>("-8.9308333310384727529536656585204441148538827565376585205061201932")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,35> abb = {dl[0], dl[1], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k)};

                    
            return f_3_80_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_80_DLogXconstant_part(base_point<T>, kend);
	value += f_3_80_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_80_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_80_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_80_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_80_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_80_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_80_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
