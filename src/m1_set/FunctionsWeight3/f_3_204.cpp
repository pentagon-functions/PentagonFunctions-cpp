/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_204.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_204_abbreviated (const std::array<T,32>& abb) {
T z[19];
z[0] = abb[13] + abb[14];
z[1] = 2 * abb[11];
z[2] = z[0] + -z[1];
z[3] = abb[10] + abb[12];
z[4] = 2 * abb[15];
z[5] = -4 * abb[9] + z[2] + 3 * z[3] + -z[4];
z[5] = m1_set::bc<T>[0] * z[5];
z[6] = abb[18] + abb[20];
z[5] = z[5] + -2 * z[6];
z[7] = abb[2] + abb[4];
z[5] = z[5] * z[7];
z[8] = 2 * abb[14];
z[9] = -2 * abb[13] + z[1] + z[3] + -z[8];
z[9] = m1_set::bc<T>[0] * z[9];
z[10] = abb[19] + -abb[20];
z[9] = z[9] + 2 * z[10];
z[9] = abb[1] * z[9];
z[10] = -z[3] + z[4];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = 2 * abb[20] + z[10];
z[10] = abb[5] * z[10];
z[11] = z[0] + -z[4];
z[11] = -abb[6] * m1_set::bc<T>[0] * z[11];
z[0] = -z[0] + z[3];
z[0] = m1_set::bc<T>[0] * z[0];
z[0] = abb[19] + z[0] + -z[6];
z[6] = 2 * abb[3];
z[0] = z[0] * z[6];
z[2] = -m1_set::bc<T>[0] * z[2];
z[2] = 2 * abb[19] + z[2];
z[2] = abb[0] * z[2];
z[12] = abb[7] * abb[23];
z[13] = abb[8] * abb[22];
z[14] = abb[1] + abb[3];
z[15] = abb[6] + z[14];
z[15] = abb[21] * z[15];
z[0] = -abb[31] + z[0] + z[2] + z[5] + z[9] + z[10] + z[11] + z[12] + z[13] + 2 * z[15];
z[2] = abb[11] + -2 * abb[12] + abb[15];
z[2] = abb[10] * z[2];
z[3] = -2 * abb[9] + abb[13] + z[3];
z[3] = abb[9] * z[3];
z[5] = abb[9] + -abb[10];
z[9] = abb[11] + -abb[15] + z[5];
z[9] = abb[14] * z[9];
z[10] = -abb[12] + abb[15];
z[11] = -abb[11] + z[10];
z[11] = abb[13] * z[11];
z[12] = abb[11] + -abb[12];
z[12] = abb[11] * z[12];
z[13] = abb[17] + -abb[25];
z[15] = abb[15] * z[10];
z[16] = 2 * abb[16];
z[17] = prod_pow(m1_set::bc<T>[0], 2);
z[2] = z[2] + z[3] + z[9] + z[11] + -z[12] + -2 * z[13] + -z[15] + -z[16] + (T(11) / T(3)) * z[17];
z[2] = z[2] * z[7];
z[3] = abb[13] + -abb[15];
z[8] = z[3] * z[8];
z[9] = abb[11] * abb[13];
z[11] = abb[17] + z[9];
z[11] = 2 * abb[26] + 2 * z[11];
z[12] = (T(2) / T(3)) * z[17];
z[13] = 2 * abb[28] + z[12];
z[15] = -abb[12] + z[4];
z[17] = abb[10] * z[15];
z[1] = -abb[9] + z[1];
z[18] = -abb[10] + abb[12] + z[1];
z[18] = abb[9] * z[18];
z[8] = z[8] + -z[11] + -z[13] + z[17] + z[18];
z[8] = abb[1] * z[8];
z[5] = -z[5] + z[15];
z[5] = abb[9] * z[5];
z[5] = z[5] + -z[12] + -z[16] + -z[17];
z[5] = abb[5] * z[5];
z[15] = abb[11] * abb[12];
z[10] = abb[10] * z[10];
z[3] = abb[14] * z[3];
z[3] = abb[25] + -abb[26] + -abb[28] + z[3] + -z[9] + z[10] + z[15];
z[3] = z[3] * z[6];
z[1] = abb[13] + z[1];
z[1] = abb[9] * z[1];
z[6] = abb[9] + -abb[13];
z[6] = -abb[14] * z[6];
z[1] = z[1] + z[6] + -z[11] + -z[12];
z[1] = abb[0] * z[1];
z[6] = abb[9] + -abb[14];
z[4] = -abb[9] + -abb[13] + z[4];
z[4] = z[4] * z[6];
z[4] = z[4] + -z[13] + -z[16];
z[4] = abb[6] * z[4];
z[6] = -abb[5] + z[7] + z[14];
z[6] = abb[27] * z[6];
z[7] = -abb[7] * abb[30];
z[9] = -abb[8] * abb[29];
z[1] = -abb[24] + z[1] + z[2] + z[3] + z[4] + z[5] + 2 * z[6] + z[7] + z[8] + z[9];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_204_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {stof<T>("39.478417604357434475337963999504604541254797628963162505653397505")};
	
	std::vector<C> intdlogs = {(log3(k.W[190]) - log3(kbase.W[190]))};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_204_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_204_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("21.320386546457785579583542635939367404963811062742711707591249931"),stof<T>("41.907621805331554588925262824653324268568311782159758858123251945")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({200});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,32> abb = {dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), f_2_25_im(k), T{0}, f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), f_2_25_re(k), T{0}};
{
auto c = dlog_W191(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[31] = c.real();
abb[24] = c.imag();
SpDLog_Sigma5<T,200,190>(k, dv, abb[31], abb[24], f_2_32_series_coefficients<T>);
}

                    
            return f_3_204_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_204_DLogXconstant_part(base_point<T>, kend);
	value += f_3_204_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_204_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_204_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_204_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_204_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_204_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_204_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
