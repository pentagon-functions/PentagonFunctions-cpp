/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_175.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_175_abbreviated (const std::array<T,25>& abb) {
T z[35];
z[0] = 3 * abb[6];
z[1] = 2 * abb[3];
z[2] = z[0] + z[1];
z[3] = 7 * abb[8] + -z[2];
z[4] = 2 * abb[4];
z[5] = 2 * abb[5];
z[6] = -abb[0] + abb[1];
z[7] = z[3] + -z[4] + -z[5] + -z[6];
z[7] = abb[14] * z[7];
z[8] = 2 * abb[8];
z[9] = abb[3] + abb[9];
z[10] = -abb[1] + abb[5] + z[8] + -z[9];
z[10] = abb[13] * z[10];
z[11] = 4 * abb[9];
z[12] = 4 * abb[8] + -3 * z[6] + -z[11];
z[12] = abb[11] * z[12];
z[7] = z[7] + 2 * z[10] + z[12];
z[10] = 3 * abb[0];
z[12] = 4 * abb[1];
z[13] = 3 * abb[9];
z[14] = -z[10] + z[12] + z[13];
z[15] = 11 * abb[8];
z[14] = z[2] + -z[5] + 2 * z[14] + -z[15];
z[14] = abb[12] * z[14];
z[7] = 2 * z[7] + z[14];
z[7] = abb[12] * z[7];
z[14] = abb[0] + abb[9];
z[16] = 5 * abb[8];
z[17] = 4 * abb[3];
z[14] = 3 * abb[2] + -abb[4] + -z[0] + 2 * z[14] + -z[16] + z[17];
z[18] = abb[22] * z[14];
z[19] = 3 * abb[4];
z[20] = abb[0] + 4 * abb[2] + z[1] + -z[19];
z[21] = 3 * abb[5];
z[22] = 2 * abb[10] + -z[21];
z[11] = z[11] + z[22];
z[23] = -z[11] + z[20];
z[23] = abb[17] * z[23];
z[24] = abb[2] + -abb[4];
z[25] = -z[6] + -z[24];
z[25] = prod_pow(abb[13], 2) * z[25];
z[18] = z[18] + z[23] + z[25];
z[23] = 2 * abb[9];
z[4] = -abb[0] + z[4] + z[23];
z[25] = 2 * abb[2];
z[26] = -abb[1] + -z[1] + z[4] + z[5] + -z[25];
z[26] = abb[11] * z[26];
z[27] = -abb[4] + z[9];
z[28] = 2 * abb[6];
z[29] = z[27] + -z[28];
z[30] = abb[2] + -abb[5];
z[31] = abb[0] + z[29] + z[30];
z[31] = abb[13] * z[31];
z[26] = z[26] + 4 * z[31];
z[26] = abb[11] * z[26];
z[2] = abb[1] + z[2];
z[31] = 3 * abb[8] + -z[25];
z[22] = 6 * abb[9] + z[22];
z[19] = 2 * abb[0] + z[2] + -z[19] + -z[22] + -z[31];
z[19] = abb[14] * z[19];
z[32] = -z[6] + z[8];
z[9] = abb[2] + z[9] + -z[32];
z[9] = abb[13] * z[9];
z[33] = z[8] + -z[28];
z[34] = abb[4] + -abb[9] + -z[30] + -z[33];
z[34] = abb[11] * z[34];
z[9] = z[9] + z[34];
z[9] = 4 * z[9] + z[19];
z[9] = abb[14] * z[9];
z[19] = z[25] + z[29];
z[25] = abb[11] + -abb[13];
z[25] = z[19] * z[25];
z[29] = -abb[0] + abb[4] + -4 * abb[6] + z[22];
z[29] = abb[14] * z[29];
z[25] = 2 * z[25] + z[29];
z[20] = 8 * abb[6] + -z[20] + -z[22];
z[20] = abb[15] * z[20];
z[20] = z[20] + 2 * z[25];
z[20] = abb[15] * z[20];
z[7] = z[7] + z[9] + 2 * z[18] + z[20] + z[26];
z[2] = abb[4] + -abb[10] + z[2] + -z[13] + -z[16] + z[21];
z[2] = abb[18] * z[2];
z[0] = 3 * abb[1] + z[0] + z[4] + -z[15] + z[17];
z[4] = abb[23] * z[0];
z[9] = -5 * abb[0] + -abb[4] + -z[1] + z[11] + z[12];
z[9] = abb[16] * z[9];
z[2] = z[2] + -z[4] + z[9];
z[4] = (T(1) / T(3)) * z[6] + (T(7) / T(3)) * z[24] + z[33];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[2] = abb[24] + 16 * z[2] + 4 * z[4] + 8 * z[7];
z[4] = -z[24] + z[28] + -z[32];
z[4] = abb[13] * z[4];
z[7] = -abb[0] + 2 * abb[1] + -z[8] + z[27] + z[30];
z[7] = abb[11] * z[7];
z[8] = abb[15] * z[19];
z[4] = z[4] + z[7] + -z[8];
z[1] = -abb[6] + z[1] + z[6] + z[23] + -z[31];
z[1] = abb[14] * z[1];
z[3] = -5 * abb[1] + z[3] + z[5] + z[10] + -z[23];
z[3] = abb[12] * z[3];
z[1] = z[1] + z[3] + 2 * z[4];
z[1] = m1_set::bc<T>[0] * z[1];
z[0] = -abb[20] * z[0];
z[3] = abb[19] * z[14];
z[0] = z[0] + z[1] + z[3];
z[0] = abb[21] + 16 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_3_175_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_175_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_175_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 * (-8 * v[0] + v[1] + 5 * (-2 + -v[2] + v[4] + v[5])) + 3 * m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (12 * (-2 + 3 * m1_set::bc<T>[2]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[3];
z[0] = -abb[16] + abb[17] + abb[18];
z[1] = abb[11] + -abb[13];
z[2] = 7 * abb[14] + 4 * z[1];
z[2] = abb[14] * z[2];
z[1] = -5 * abb[14] + -2 * z[1];
z[1] = 3 * abb[15] + 2 * z[1];
z[1] = abb[15] * z[1];
z[0] = 6 * z[0] + z[1] + z[2];
return 8 * abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_175_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-4 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (-16 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[14] + abb[15];
return 32 * abb[7] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_175_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-5.771634444810701331808733572703257485691286571769691187447890765"),stof<T>("-24.969459559618275138399237733011339683276915727151721666349331211")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,25> abb = {dl[0], dl[1], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W59(k,dl), f_1_1(k), f_1_3(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_9_im(k), T{0}, f_2_2_re(k), f_2_9_re(k), T{0}};
abb[21] = SpDLog_f_3_175_W_17_Im(t, path, abb);
abb[24] = SpDLog_f_3_175_W_17_Re(t, path, abb);

                    
            return f_3_175_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_175_DLogXconstant_part(base_point<T>, kend);
	value += f_3_175_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_175_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_175_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_175_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_175_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_175_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_175_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
