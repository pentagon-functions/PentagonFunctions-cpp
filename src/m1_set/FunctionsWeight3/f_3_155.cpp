/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_155.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_155_abbreviated (const std::array<T,48>& abb) {
T z[53];
z[0] = 2 * abb[7];
z[1] = 2 * abb[9];
z[2] = z[0] + z[1];
z[3] = 3 * abb[5];
z[4] = 2 * abb[2];
z[5] = z[3] + z[4];
z[6] = abb[6] + z[5];
z[7] = 2 * abb[3];
z[8] = 3 * abb[4];
z[9] = z[7] + z[8];
z[10] = z[2] + -z[6] + z[9];
z[11] = -abb[31] * z[10];
z[12] = -abb[1] + abb[8];
z[13] = 3 * abb[7];
z[14] = -abb[3] + z[13];
z[15] = 7 * abb[2] + abb[5];
z[16] = 3 * abb[13];
z[12] = -3 * z[12] + z[14] + z[15] + -z[16];
z[17] = abb[43] * z[12];
z[18] = -z[8] + z[16];
z[19] = 4 * abb[8];
z[20] = -z[18] + z[19];
z[21] = -abb[6] + abb[7];
z[5] = abb[3] + -z[5] + z[20] + z[21];
z[22] = abb[46] * z[5];
z[23] = 2 * abb[1];
z[24] = -abb[10] + z[23];
z[24] = abb[11] + -z[13] + z[18] + -2 * z[24];
z[25] = abb[41] * z[24];
z[26] = abb[4] + -abb[13];
z[27] = -abb[2] + -z[21] + -z[26];
z[28] = -abb[45] * z[27];
z[29] = abb[3] + abb[4];
z[30] = -abb[2] + z[29];
z[31] = abb[5] + -abb[11] + z[30];
z[32] = -abb[29] * z[31];
z[28] = z[28] + z[32];
z[19] = -z[1] + z[19];
z[16] = abb[3] + abb[7] + z[16] + -z[19];
z[32] = -abb[40] * z[16];
z[33] = abb[15] + abb[17];
z[34] = abb[14] + abb[16];
z[33] = 3 * z[33] + 2 * z[34];
z[34] = -abb[39] * z[33];
z[35] = 6 * abb[7];
z[36] = -abb[27] * z[35];
z[11] = z[11] + z[17] + z[22] + -z[25] + 3 * z[28] + z[32] + z[34] + z[36];
z[17] = 5 * abb[6] + z[4];
z[22] = abb[5] + abb[11];
z[25] = -7 * abb[7] + 4 * abb[10] + -z[8] + z[17] + z[22];
z[28] = abb[20] * z[25];
z[32] = 2 * abb[10];
z[34] = 2 * abb[11];
z[36] = z[32] + z[34];
z[37] = z[6] + z[13] + -z[36];
z[37] = abb[23] * z[37];
z[38] = 4 * abb[7];
z[39] = z[8] + z[38];
z[40] = 7 * abb[6];
z[41] = 5 * abb[5];
z[42] = -4 * abb[2] + abb[3] + z[39] + -z[40] + -z[41];
z[42] = abb[24] * z[42];
z[43] = 3 * abb[2] + -4 * abb[5] + -abb[6] + -z[9] + z[36];
z[43] = abb[22] * z[43];
z[22] = abb[3] + -abb[6] + -z[22] + z[32];
z[44] = -abb[18] * z[22];
z[31] = abb[21] * z[31];
z[28] = z[28] + 3 * z[31] + z[37] + z[42] + z[43] + z[44];
z[31] = -abb[2] + -8 * abb[5] + z[14] + z[36] + -z[40];
z[31] = abb[19] * z[31];
z[28] = 2 * z[28] + z[31];
z[28] = abb[19] * z[28];
z[31] = 3 * abb[3];
z[36] = z[31] + z[38];
z[37] = abb[5] + z[23];
z[38] = -abb[2] + abb[11];
z[40] = -z[37] + z[38];
z[40] = 8 * abb[8] + z[18] + -z[36] + 4 * z[40];
z[40] = prod_pow(abb[21], 2) * z[40];
z[42] = 2 * abb[5];
z[43] = -abb[10] + z[42];
z[38] = -abb[9] + -z[21] + -z[38] + z[43];
z[38] = abb[20] * z[38];
z[44] = abb[0] * abb[20];
z[38] = z[38] + 3 * z[44];
z[45] = abb[2] + abb[7];
z[46] = 2 * abb[8] + -z[45];
z[47] = -abb[11] + z[37] + -z[46];
z[47] = abb[21] * z[47];
z[48] = z[38] + z[47];
z[49] = 2 * abb[0];
z[13] = -z[13] + -z[23] + z[49];
z[50] = z[6] + z[13];
z[50] = abb[23] * z[50];
z[50] = -4 * z[48] + z[50];
z[50] = abb[23] * z[50];
z[51] = -abb[1] + abb[11];
z[52] = abb[0] + -z[51];
z[52] = abb[23] * z[52];
z[38] = -z[38] + z[47] + z[52];
z[52] = abb[5] + abb[6];
z[7] = z[7] + z[13] + z[52];
z[13] = abb[18] * z[7];
z[13] = z[13] + 4 * z[38];
z[13] = abb[18] * z[13];
z[10] = -abb[18] * z[10];
z[14] = -abb[5] + -4 * abb[9] + z[14] + -z[17];
z[17] = -abb[20] * z[14];
z[38] = abb[7] + -z[1] + z[52];
z[38] = abb[23] * z[38];
z[30] = abb[6] + z[30];
z[52] = abb[25] * z[30];
z[10] = z[10] + z[17] + z[38] + 3 * z[52];
z[9] = -z[1] + z[9];
z[17] = -10 * abb[6] + -z[9] + -z[41] + -z[45];
z[17] = abb[24] * z[17];
z[10] = 2 * z[10] + z[17];
z[10] = abb[24] * z[10];
z[17] = z[5] + -z[23] + z[34] + -z[49];
z[17] = abb[18] * z[17];
z[23] = -abb[0] + abb[1] + abb[5] + -z[46];
z[23] = abb[23] * z[23];
z[23] = z[23] + z[48];
z[38] = abb[2] + -abb[3] + -4 * abb[6] + z[2] + -z[3];
z[38] = abb[24] * z[38];
z[17] = z[17] + 2 * z[23] + z[38];
z[7] = abb[22] * z[7];
z[7] = z[7] + 2 * z[17];
z[7] = abb[22] * z[7];
z[17] = abb[9] + abb[10];
z[23] = -abb[3] + 7 * abb[5] + -abb[6] + -4 * abb[11] + z[4] + -10 * z[17] + z[39];
z[38] = prod_pow(abb[20], 2);
z[23] = z[23] * z[38];
z[38] = 2 * abb[27] + 3 * z[38];
z[38] = abb[0] * z[38];
z[7] = z[7] + z[10] + 2 * z[11] + z[13] + z[23] + z[28] + 6 * z[38] + z[40] + z[50];
z[10] = abb[42] * z[25];
z[11] = abb[30] * z[22];
z[13] = abb[44] * z[14];
z[10] = -z[10] + z[11] + z[13];
z[11] = -abb[11] + -abb[12] + z[29] + z[49];
z[11] = abb[26] * z[11];
z[13] = abb[28] * z[30];
z[22] = abb[18] + -abb[20] + abb[24] + -abb[25];
z[22] = abb[24] * z[22];
z[22] = abb[28] + abb[31] + abb[40] + -abb[44] + z[22];
z[22] = abb[12] * z[22];
z[11] = -z[11] + z[13] + -z[22];
z[13] = -abb[1] + 32 * abb[10] + abb[6] * (T(23) / T(2));
z[13] = -27 * abb[2] + abb[5] * (T(-139) / T(6)) + abb[0] * (T(-52) / T(3)) + abb[7] * (T(-47) / T(6)) + abb[9] * (T(32) / T(3)) + abb[8] * (T(94) / T(3)) + (T(1) / T(3)) * z[13] + 4 * z[26];
z[13] = prod_pow(m1_set::bc<T>[0], 2) * z[13];
z[22] = abb[13] + -z[29];
z[22] = prod_pow(abb[25], 2) * z[22];
z[7] = 2 * z[7] + -4 * z[10] + -12 * z[11] + z[13] + 6 * z[22];
z[10] = 3 * abb[6];
z[3] = abb[3] + -z[3] + -z[8] + z[10] + 8 * z[17] + z[34] + -z[35];
z[3] = abb[20] * z[3];
z[8] = 4 * abb[0];
z[4] = -z[4] + z[8] + z[19] + -z[21] + -z[32] + -z[37];
z[4] = abb[23] * z[4];
z[6] = -abb[10] + z[6] + -z[51];
z[1] = -z[1] + 2 * z[6] + z[8] + -z[20] + -z[31];
z[1] = abb[18] * z[1];
z[6] = abb[1] + -abb[2] + -abb[6] + -z[43];
z[2] = z[2] + 2 * z[6] + -z[8] + z[18] + -z[31];
z[2] = abb[22] * z[2];
z[0] = -5 * abb[2] + z[0] + z[9] + -z[10] + -z[42];
z[0] = abb[24] * z[0];
z[6] = -abb[10] + -z[10];
z[6] = 6 * abb[4] + 2 * z[6] + -z[15] + -z[34] + z[36];
z[6] = abb[19] * z[6];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[6] + -12 * z[44] + -2 * z[47];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[35] * z[12];
z[2] = -abb[33] * z[24];
z[3] = abb[34] * z[25];
z[4] = abb[38] * z[5];
z[5] = abb[37] * z[27];
z[6] = abb[32] + -abb[36];
z[6] = abb[12] * z[6];
z[5] = z[5] + -z[6];
z[6] = -abb[36] * z[14];
z[8] = abb[47] * z[33];
z[9] = -abb[32] * z[16];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + -3 * z[5] + z[6] + z[8] + z[9];
z[0] = 4 * z[0];
return {z[7], z[0]};
}


template <typename T> std::complex<T> f_3_155_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_155_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_155_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-7.51248222655846273724437061564510622645001589519470063777217179"),stof<T>("675.8712729906897502283737695621190500758504689017568104844749918")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,48> abb = {dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W22(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W33(k,dl), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_27_im(k), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_27_re(k)};

                    
            return f_3_155_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_155_DLogXconstant_part(base_point<T>, kend);
	value += f_3_155_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_155_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_155_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_155_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_155_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_155_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_155_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
