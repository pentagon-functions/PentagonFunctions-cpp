/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_109.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_109_abbreviated (const std::array<T,22>& abb) {
T z[21];
z[0] = abb[0] * (T(1) / T(2));
z[1] = abb[6] + z[0];
z[2] = 3 * abb[7];
z[3] = -abb[2] + z[2];
z[4] = 3 * abb[3];
z[3] = abb[1] + -abb[4] + -z[1] + (T(3) / T(2)) * z[3] + -z[4];
z[5] = abb[21] * z[3];
z[6] = 2 * abb[5];
z[7] = 5 * abb[7];
z[8] = abb[2] + abb[6] * (T(11) / T(2)) + -z[7];
z[8] = abb[1] * (T(-9) / T(4)) + z[0] + z[6] + (T(1) / T(2)) * z[8];
z[8] = abb[9] * z[8];
z[9] = abb[2] + abb[7];
z[10] = abb[1] * (T(3) / T(2));
z[9] = abb[6] * (T(5) / T(2)) + -2 * z[9] + -z[10];
z[11] = -z[6] + -z[9];
z[11] = abb[10] * z[11];
z[8] = z[8] + z[11];
z[8] = abb[9] * z[8];
z[11] = 2 * abb[0];
z[12] = -z[6] + z[11];
z[13] = -abb[2] + abb[7];
z[14] = -abb[3] + z[13];
z[14] = 3 * z[14];
z[15] = -z[12] + z[14];
z[15] = abb[10] * z[15];
z[16] = abb[0] + -abb[5];
z[16] = abb[9] * z[16];
z[17] = abb[3] + -abb[4];
z[17] = abb[8] * z[17];
z[15] = z[15] + 2 * z[16] + (T(3) / T(2)) * z[17];
z[15] = abb[8] * z[15];
z[7] = -abb[2] + -z[4] + z[7];
z[1] = abb[4] * (T(3) / T(2)) + z[1] + -z[7];
z[16] = -abb[17] * z[1];
z[2] = abb[1] + 2 * abb[2] + -z[2] + z[4];
z[4] = -abb[18] * z[2];
z[7] = abb[0] + abb[4] + abb[1] * (T(-1) / T(2)) + abb[6] * (T(3) / T(2)) + -z[7];
z[17] = -abb[19] * z[7];
z[18] = abb[7] + abb[2] * (T(1) / T(2));
z[18] = -4 * abb[6] + 13 * z[18];
z[18] = -4 * abb[3] + abb[4] * (T(-1) / T(12)) + abb[1] * (T(4) / T(3)) + abb[0] * (T(7) / T(4)) + -z[6] + (T(1) / T(3)) * z[18];
z[18] = prod_pow(m1_set::bc<T>[0], 2) * z[18];
z[19] = abb[20] * z[14];
z[0] = -abb[2] + -abb[7] + abb[6] * (T(-1) / T(4)) + abb[3] * (T(3) / T(2)) + abb[1] * (T(3) / T(4)) + -z[0];
z[0] = prod_pow(abb[10], 2) * z[0];
z[13] = -abb[4] + z[13];
z[20] = -prod_pow(abb[11], 2) * z[13];
z[0] = z[0] + z[4] + z[5] + z[8] + z[15] + z[16] + z[17] + z[18] + z[19] + (T(3) / T(2)) * z[20];
z[4] = 3 * abb[0] + 5 * abb[2] + -abb[7] + abb[6] * (T(1) / T(2)) + -z[6] + -z[10];
z[4] = abb[10] * z[4];
z[5] = 4 * abb[5] + z[9] + -z[11];
z[5] = abb[9] * z[5];
z[6] = z[12] + -3 * z[13];
z[6] = abb[8] * z[6];
z[4] = z[4] + z[5] + z[6];
z[4] = m1_set::bc<T>[0] * z[4];
z[3] = abb[16] * z[3];
z[2] = -abb[13] * z[2];
z[1] = -abb[12] * z[1];
z[5] = -abb[14] * z[7];
z[6] = abb[15] * z[14];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_109_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_109_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_109_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-48.687558142832112572474703134621964433386893326132366931060663238"),stof<T>("-74.950406428809135433230772700306699163741158502820902963804630011")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,22> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_3_109_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_109_DLogXconstant_part(base_point<T>, kend);
	value += f_3_109_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_109_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_109_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_109_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_109_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_109_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_109_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
