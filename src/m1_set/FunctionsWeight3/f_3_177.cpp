/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_177.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_177_abbreviated (const std::array<T,45>& abb) {
T z[44];
z[0] = 3 * abb[21];
z[1] = -3 * abb[24] + z[0];
z[2] = abb[6] + abb[8];
z[1] = z[1] * z[2];
z[3] = abb[4] + abb[6];
z[4] = 2 * abb[2];
z[5] = 3 * z[3] + -z[4];
z[6] = 3 * abb[3];
z[7] = -z[5] + z[6];
z[7] = abb[22] * z[7];
z[8] = 2 * abb[16];
z[9] = abb[19] + -abb[24];
z[10] = -abb[22] + z[9];
z[11] = -z[8] * z[10];
z[12] = 2 * abb[22] + -z[9];
z[13] = 3 * abb[14];
z[14] = -z[12] * z[13];
z[15] = 6 * abb[10];
z[16] = -abb[19] + abb[21] + abb[22];
z[16] = z[15] * z[16];
z[17] = 2 * abb[24];
z[18] = abb[19] + z[17];
z[19] = -z[0] + z[18];
z[19] = abb[7] * z[19];
z[20] = 3 * abb[5];
z[21] = -abb[21] + abb[22] + abb[24];
z[21] = z[20] * z[21];
z[22] = 4 * abb[19] + -5 * abb[22] + -abb[24];
z[23] = -z[0] + z[22];
z[23] = abb[12] * z[23];
z[24] = 2 * z[9];
z[25] = -abb[3] * z[24];
z[1] = z[1] + z[7] + z[11] + z[14] + z[16] + z[19] + z[21] + z[23] + z[25];
z[7] = -abb[4] + abb[8];
z[11] = -z[4] + -z[7];
z[14] = 2 * abb[3] + z[15];
z[11] = abb[7] + 7 * abb[12] + -z[8] + 3 * z[11] + z[13] + -z[14];
z[11] = abb[20] * z[11];
z[1] = 2 * z[1] + z[11];
z[1] = abb[20] * z[1];
z[11] = -abb[3] + abb[12];
z[5] = -abb[7] + z[5] + z[11] + z[13] + -z[20];
z[5] = abb[20] * z[5];
z[13] = 3 * abb[6];
z[16] = -abb[4] + z[13];
z[19] = z[4] + z[6] + -z[16];
z[19] = abb[22] * z[19];
z[21] = 2 * abb[19] + abb[24];
z[23] = 3 * abb[26];
z[25] = z[21] + -z[23];
z[26] = abb[4] + abb[8];
z[27] = -z[25] * z[26];
z[22] = z[22] + -z[23];
z[22] = abb[14] * z[22];
z[28] = 2 * abb[15];
z[29] = -z[10] * z[28];
z[30] = -abb[19] + abb[26];
z[31] = abb[22] + z[30];
z[32] = z[15] * z[31];
z[25] = -abb[22] + z[25];
z[25] = abb[7] * z[25];
z[33] = -z[20] * z[30];
z[12] = -abb[12] * z[12];
z[5] = z[5] + z[12] + z[19] + z[22] + z[25] + z[27] + z[29] + z[32] + z[33];
z[12] = abb[12] + z[20];
z[13] = -6 * abb[2] + -4 * abb[4] + 2 * abb[7] + -3 * abb[8] + 5 * abb[14] + z[12] + z[13] + -z[14] + -z[28];
z[13] = abb[25] * z[13];
z[5] = 2 * z[5] + z[13];
z[5] = abb[25] * z[5];
z[13] = prod_pow(abb[26], 2);
z[14] = prod_pow(abb[21], 2);
z[13] = z[13] + z[14];
z[14] = 3 * z[13];
z[19] = prod_pow(abb[24], 2);
z[22] = abb[19] + abb[24];
z[25] = -abb[19] * z[22];
z[25] = abb[33] + -z[19] + z[25];
z[27] = 2 * abb[31];
z[29] = prod_pow(abb[22], 2);
z[32] = 6 * abb[41];
z[33] = 2 * abb[29];
z[34] = 2 * abb[35];
z[25] = z[14] + 2 * z[25] + z[27] + z[29] + z[32] + -z[33] + -z[34];
z[25] = abb[7] * z[25];
z[35] = -abb[24] + z[30];
z[36] = abb[26] * z[35];
z[37] = 2 * z[36];
z[38] = prod_pow(abb[19], 2);
z[38] = -z[19] + -z[37] + -z[38];
z[38] = abb[4] * z[38];
z[39] = 4 * abb[24];
z[40] = abb[19] + -z[39];
z[40] = abb[19] * z[40];
z[37] = z[19] + -z[37] + z[40];
z[37] = abb[8] * z[37];
z[18] = abb[19] * z[18];
z[40] = 4 * z[36];
z[18] = z[18] + z[19] + z[40];
z[41] = abb[1] * z[18];
z[42] = -abb[19] + z[17];
z[42] = abb[19] * z[42];
z[42] = -z[19] + z[42];
z[43] = -z[4] * z[42];
z[3] = z[3] + -z[4];
z[3] = abb[32] * z[3];
z[3] = 3 * z[3] + z[37] + z[38] + z[41] + z[43];
z[37] = 2 * abb[33];
z[38] = z[19] + z[37];
z[10] = abb[22] * z[10];
z[41] = 3 * abb[19] + -z[17];
z[41] = abb[19] * z[41];
z[27] = 6 * abb[35] + -8 * z[10] + -z[27] + 3 * z[38] + z[40] + z[41];
z[27] = abb[14] * z[27];
z[38] = z[33] + -z[34];
z[39] = -abb[19] + -z[39];
z[39] = abb[19] * z[39];
z[14] = z[14] + -z[19] + z[38] + z[39];
z[19] = 2 * abb[0];
z[14] = z[14] * z[19];
z[39] = 2 * abb[41] + z[13];
z[17] = abb[19] * z[17];
z[29] = -2 * abb[32] + -z[17] + -z[29] + -z[34] + z[39];
z[29] = z[20] * z[29];
z[34] = 6 * abb[32] + z[34];
z[40] = 8 * abb[33] + -4 * z[10] + z[32] + z[34] + -z[42];
z[40] = abb[12] * z[40];
z[18] = -4 * abb[33] + z[18] + -z[34];
z[18] = abb[3] * z[18];
z[34] = abb[3] + abb[4];
z[34] = z[9] * z[34];
z[41] = abb[2] * z[9];
z[43] = -abb[8] * z[24];
z[34] = z[34] + -3 * z[41] + z[43];
z[43] = abb[3] + -abb[8];
z[16] = 8 * abb[2] + z[16] + -6 * z[43];
z[16] = abb[22] * z[16];
z[16] = z[16] + 4 * z[34];
z[16] = abb[22] * z[16];
z[13] = -z[13] + z[17] + -z[38];
z[17] = 3 * abb[13];
z[13] = z[13] * z[17];
z[24] = -abb[22] + z[24];
z[24] = abb[22] * z[24];
z[24] = z[24] + z[42];
z[34] = z[24] + -z[33] + -z[37];
z[8] = z[8] * z[34];
z[34] = abb[19] * abb[24];
z[10] = -abb[33] + z[10] + z[34];
z[10] = 2 * z[10] + -z[39];
z[10] = z[10] * z[15];
z[4] = -abb[3] + abb[6] + -z[4];
z[4] = abb[31] * z[4];
z[37] = abb[7] + -abb[8];
z[38] = abb[5] + -2 * abb[10] + z[37];
z[39] = -abb[4] + abb[14] + z[38];
z[42] = abb[43] * z[39];
z[4] = z[4] + -z[42];
z[2] = -abb[3] + 3 * z[2];
z[2] = z[2] * z[33];
z[24] = z[24] * z[28];
z[33] = abb[2] + abb[8];
z[42] = abb[14] + -2 * z[33];
z[32] = z[32] * z[42];
z[42] = 2 * abb[17] + 6 * abb[18];
z[42] = abb[36] * z[42];
z[1] = z[1] + z[2] + 2 * z[3] + 6 * z[4] + z[5] + z[8] + z[10] + z[13] + z[14] + z[16] + z[18] + z[24] + z[25] + z[27] + z[29] + z[32] + z[40] + z[42];
z[2] = z[17] + -z[20];
z[3] = 4 * abb[1] + -z[2] + z[6] + z[19] + -z[26] + -z[28];
z[3] = abb[30] * z[3];
z[4] = 2 * abb[26] + -z[22];
z[4] = abb[1] * z[4];
z[5] = -abb[24] + abb[26];
z[5] = abb[4] * z[5];
z[6] = -abb[3] + z[7];
z[7] = abb[22] * z[6];
z[4] = -z[4] + z[5] + z[7] + -z[41];
z[5] = -2 * abb[9] + z[43];
z[5] = z[5] * z[30];
z[6] = -abb[14] + z[6];
z[6] = abb[25] * z[6];
z[7] = abb[14] * z[31];
z[8] = -abb[0] * z[9];
z[5] = -z[4] + z[5] + z[6] + z[7] + z[8];
z[7] = abb[0] + -abb[1];
z[8] = abb[2] + -abb[4] + -z[7];
z[8] = abb[23] * z[8];
z[5] = 2 * z[5] + z[8];
z[5] = abb[23] * z[5];
z[9] = 2 * abb[14];
z[10] = -abb[4] + -3 * abb[10] + -abb[15] + z[9] + z[37];
z[10] = 2 * z[10] + z[12];
z[10] = abb[34] * z[10];
z[12] = abb[1] + abb[3] + -abb[9] + -abb[13];
z[12] = 6 * abb[0] + -abb[7] + abb[14] + 3 * z[12];
z[12] = abb[27] * z[12];
z[3] = z[3] + z[5] + z[10] + z[12];
z[5] = -abb[8] + abb[15] + abb[16];
z[10] = abb[5] + -abb[6];
z[12] = abb[7] + abb[12];
z[13] = abb[3] + z[12];
z[5] = abb[2] + -4 * abb[10] + abb[4] * (T(7) / T(3)) + (T(-4) / T(3)) * z[5] + (T(13) / T(3)) * z[7] + z[9] + 2 * z[10] + (T(2) / T(3)) * z[13];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[7] = abb[5] + -abb[13] + -z[11] + z[19];
z[7] = abb[28] * z[7];
z[10] = abb[29] + -abb[35] + -z[34] + -z[36];
z[10] = abb[9] * z[10];
z[11] = -abb[6] + abb[12] + z[38];
z[11] = 48 * z[11];
z[13] = -abb[42] * z[11];
z[1] = abb[44] + 8 * z[1] + 16 * z[3] + 4 * z[5] + 48 * z[7] + 64 * z[10] + z[13];
z[3] = -z[30] * z[43];
z[3] = -z[3] + -z[4] + z[6] + z[8];
z[0] = -z[0] + 2 * z[21] + -z[23];
z[0] = z[0] * z[19];
z[2] = -3 * abb[7] + z[2] + z[15];
z[4] = abb[21] + z[35];
z[2] = z[2] * z[4];
z[4] = -z[9] * z[31];
z[5] = abb[9] * z[30];
z[0] = z[0] + z[2] + -2 * z[3] + z[4] + 4 * z[5];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[5] + abb[14] + z[12];
z[3] = abb[10] + z[33];
z[2] = 48 * z[2] + -96 * z[3];
z[2] = abb[37] * z[2];
z[3] = abb[39] * z[39];
z[4] = -abb[38] * z[11];
z[0] = abb[40] + 16 * z[0] + z[2] + -48 * z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_177_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_177_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_177_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((v[2] + v[3]) * (-56 + 27 * v[2] + 27 * v[3] + -8 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -28 * v[5])) / prod_pow(tend, 2);
c[1] = (8 * (-3 + 4 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[20] + -abb[23];
z[0] = 2 * z[0];
z[1] = abb[25] + -z[0];
z[1] = abb[25] * z[1];
z[0] = -abb[22] + z[0];
z[0] = abb[22] * z[0];
z[0] = z[0] + z[1];
z[0] = 3 * abb[31] + 2 * z[0];
return 16 * abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_177_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (8 * m1_set::bc<T>[0] * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-32 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = abb[22] + -abb[25];
return 64 * abb[11] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_177_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-81.38735113289923039824140361415409872186364832624223321233762375"),stof<T>("201.43905565506692712566916940894645015861396124229108664963793791")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,45> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W92(k,dl), dlog_W93(k,dl), dlog_W131(k,dv), dlog_W136(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_21(k), f_2_28_im(k), f_2_4_im(k), f_2_6_im(k), f_2_11_im(k), T{0}, f_2_4_re(k), f_2_6_re(k), f_2_11_re(k), T{0}};
abb[40] = SpDLog_f_3_177_W_19_Im(t, path, abb);
abb[44] = SpDLog_f_3_177_W_19_Re(t, path, abb);

                    
            return f_3_177_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_177_DLogXconstant_part(base_point<T>, kend);
	value += f_3_177_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_177_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_177_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_177_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_177_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_177_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_177_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
