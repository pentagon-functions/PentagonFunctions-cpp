/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_128.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_128_abbreviated (const std::array<T,33>& abb) {
T z[30];
z[0] = 2 * abb[10];
z[1] = 2 * abb[1];
z[2] = abb[4] + z[1];
z[3] = abb[7] + -z[2];
z[4] = 2 * abb[6];
z[5] = z[0] + -z[3] + -z[4];
z[6] = abb[22] * z[5];
z[7] = abb[6] + -abb[10];
z[8] = 2 * abb[20];
z[9] = -z[7] * z[8];
z[10] = -abb[2] + abb[9];
z[11] = abb[6] + -z[0] + z[10];
z[11] = abb[19] * z[11];
z[12] = -abb[0] * abb[21];
z[8] = -abb[21] + -z[8];
z[8] = abb[3] * z[8];
z[6] = z[6] + z[8] + z[9] + z[11] + z[12];
z[8] = 2 * abb[5];
z[9] = 2 * abb[4];
z[11] = z[8] + z[9];
z[12] = 7 * abb[2];
z[13] = z[11] + z[12];
z[14] = 4 * abb[3];
z[15] = 9 * abb[8] + -z[14];
z[16] = 4 * abb[1] + -z[15];
z[17] = 2 * abb[7];
z[18] = z[4] + -z[13] + -z[16] + z[17];
z[18] = abb[17] * z[18];
z[19] = -abb[2] + abb[8];
z[19] = -abb[5] + 2 * z[19];
z[20] = abb[7] + z[19];
z[21] = 2 * abb[14];
z[20] = z[20] * z[21];
z[22] = abb[9] * abb[16];
z[20] = z[20] + z[22];
z[23] = -abb[0] + z[4];
z[24] = z[10] + -z[23];
z[24] = abb[13] * z[24];
z[25] = abb[8] * (T(9) / T(2));
z[26] = 2 * abb[3];
z[27] = abb[1] + z[26];
z[28] = -abb[0] + abb[4] + abb[2] * (T(9) / T(2)) + -z[25] + z[27];
z[28] = abb[15] * z[28];
z[29] = abb[0] + abb[2];
z[29] = abb[16] * z[29];
z[18] = z[18] + -z[20] + z[24] + z[28] + z[29];
z[18] = abb[15] * z[18];
z[5] = abb[13] * z[5];
z[24] = z[3] + -z[26];
z[24] = abb[16] * z[24];
z[5] = z[5] + z[24];
z[14] = 4 * abb[10] + z[3] + -z[4] + -z[14];
z[14] = abb[14] * z[14];
z[5] = 2 * z[5] + z[14];
z[5] = abb[14] * z[5];
z[4] = -z[1] + z[4];
z[0] = -abb[4] + -z[0] + z[4] + z[17] + z[19];
z[0] = abb[14] * z[0];
z[10] = -abb[6] + z[10];
z[14] = z[3] + -z[10];
z[14] = abb[13] * z[14];
z[3] = -abb[2] + -abb[6] + -z[3];
z[3] = abb[16] * z[3];
z[0] = z[0] + z[3] + z[14] + z[22];
z[3] = 7 * abb[1] + -abb[6] + -5 * abb[7] + abb[2] * (T(5) / T(2)) + z[11] + -z[25] + z[26];
z[3] = abb[17] * z[3];
z[0] = 2 * z[0] + z[3];
z[0] = abb[17] * z[0];
z[3] = -abb[0] + abb[2] + z[4] + -z[9];
z[3] = abb[16] * z[3];
z[4] = abb[1] + abb[4];
z[10] = z[4] + z[10];
z[10] = abb[13] * z[10];
z[3] = z[3] + z[10] + -z[22];
z[3] = abb[13] * z[3];
z[10] = 2 * abb[9];
z[13] = -6 * abb[8] + -z[10] + z[13] + z[27];
z[14] = -abb[30] * z[13];
z[2] = abb[10] + -z[2] + -z[17];
z[8] = -2 * z[2] + -z[8];
z[8] = abb[29] * z[8];
z[19] = abb[0] + abb[3];
z[4] = z[4] + -z[19];
z[4] = prod_pow(abb[16], 2) * z[4];
z[12] = 8 * abb[7] + -z[12];
z[22] = -abb[0] + -abb[4];
z[22] = z[12] + (T(1) / T(2)) * z[22];
z[24] = -abb[3] + abb[9];
z[22] = -3 * abb[1] + abb[5] * (T(-5) / T(3)) + abb[8] * (T(7) / T(2)) + (T(1) / T(3)) * z[22] + (T(2) / T(3)) * z[24];
z[22] = prod_pow(m1_set::bc<T>[0], 2) * z[22];
z[7] = abb[3] + z[7];
z[7] = z[7] * z[21];
z[19] = abb[16] * z[19];
z[7] = z[7] + z[19];
z[19] = -abb[3] + -z[23];
z[19] = abb[18] * z[19];
z[7] = 2 * z[7] + z[19];
z[7] = abb[18] * z[7];
z[19] = -abb[0] + -abb[7] + abb[10];
z[19] = 2 * z[19];
z[21] = abb[28] * z[19];
z[23] = abb[11] + abb[12];
z[24] = abb[31] * z[23];
z[0] = abb[32] + z[0] + z[3] + z[4] + z[5] + 2 * z[6] + z[7] + z[8] + z[14] + z[18] + z[21] + z[22] + z[24];
z[3] = -10 * abb[1] + -4 * abb[5] + -z[9] + z[10] + z[12] + z[15];
z[3] = abb[17] * z[3];
z[4] = abb[0] + z[17];
z[5] = 8 * abb[2] + -abb[9] + -z[4] + z[11] + z[16];
z[5] = abb[15] * z[5];
z[1] = abb[2] + -z[1] + z[4];
z[1] = abb[16] * z[1];
z[4] = abb[1] + -abb[7];
z[4] = abb[13] * z[4];
z[1] = z[1] + z[3] + 2 * z[4] + z[5] + -z[20];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[25] * z[13];
z[2] = -abb[5] + -z[2];
z[2] = abb[24] * z[2];
z[4] = abb[23] * z[19];
z[5] = abb[26] * z[23];
z[1] = abb[27] + z[1] + 2 * z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_128_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_128_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_128_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("16.23579277579495103446174992244501426280837789539327600674154715"),stof<T>("19.056396740070512176536334131103083073646404275196192384124772861")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,33> abb = {dl[0], dlog_W3(k,dl), dlog_W7(k,dl), dlog_W8(k,dl), dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W122(k,dv), dlog_W128(k,dv), f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}};
abb[27] = SpDLogQ_W_80(k,dl,dlr).imag();
abb[32] = SpDLogQ_W_80(k,dl,dlr).real();

                    
            return f_3_128_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_128_DLogXconstant_part(base_point<T>, kend);
	value += f_3_128_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_128_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_128_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_128_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_128_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_128_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_128_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
