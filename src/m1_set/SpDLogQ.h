#pragma once

#include "Kin.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> SpDLogQ_W_72(const Kin<T, KinType::m1>& k, const std::array<T, 6>& dl, const std::array<T,10>& dlr);
template <typename T> std::complex<T> SpDLogQ_W_74(const Kin<T, KinType::m1>& k, const std::array<T, 6>& dl, const std::array<T,10>& dlr);
template <typename T> std::complex<T> SpDLogQ_W_78(const Kin<T, KinType::m1>& k, const std::array<T, 6>& dl, const std::array<T,10>& dlr);
template <typename T> std::complex<T> SpDLogQ_W_80(const Kin<T, KinType::m1>& k, const std::array<T, 6>& dl, const std::array<T,10>& dlr);
template <typename T> std::complex<T> SpDLogQ_W_82(const Kin<T, KinType::m1>& k, const std::array<T, 6>& dl, const std::array<T,10>& dlr);
template <typename T> std::complex<T> SpDLogQ_W_84(const Kin<T, KinType::m1>& k, const std::array<T, 6>& dl, const std::array<T,10>& dlr);
template <typename T> std::complex<T> SpDLogQ_W_88(const Kin<T, KinType::m1>& k, const std::array<T, 6>& dl, const std::array<T,10>& dlr);
template <typename T> std::complex<T> SpDLogQ_W_90(const Kin<T, KinType::m1>& k, const std::array<T, 6>& dl, const std::array<T,10>& dlr);

} // namespace m1_set
} // namespace PentagonFunctions
