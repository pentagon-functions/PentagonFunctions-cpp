#pragma once

#include "PentagonFunctions_config.h"

#include "Kin.h"
#include "Constants.h"

#ifdef PENTAGON_FUNCTIONS_M0_ENABLED 
#include "include_functions.hpp"
#endif // PENTAGON_FUNCTIONS_M0_ENABLED



#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
#include "m1_set/include_list.hpp"
#endif // PENTAGON_FUNCTIONS_M1_ENABLED

#include "Integrator/tanh_sinh.hpp"

namespace PentagonFunctions {

template <typename T, typename TC = std::complex<T>, typename F> TC integrate(F f_integrand, const Kin<T>& kin) {
    using namespace boost::math::quadrature;

    // we will use just this integrator for now, however it seems to work seamlessly for all functions
    tanh_sinh<T> integrator;

    auto integrand = f_integrand(kin);
    TC value = integrator.integrate(integrand, 0, 1, IntegrationTolerance<T>);

    return value;
}

template <typename T, typename IntegratorType = boost::math::quadrature::tanh_sinh<T>, typename TC = std::complex<T>, typename F>
auto get_integrator(F f_integrand) {
    // we will use just this integrator for now, however it seems to work seamlessly for all functions
    IntegratorType integrator;

    return [f_integrand = std::move(f_integrand), integrator = std::move(integrator)] (const Kin<T>& kin) mutable {

        auto integrand = f_integrand(kin);
        TC value = integrator.integrate(integrand, 0, 1, IntegrationTolerance<T>);

        return value;
    };
}

} // namespace PentagonFunctions
