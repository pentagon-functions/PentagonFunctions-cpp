#pragma once

#include "Kin.h"
#include "PolyLog.h"

namespace PentagonFunctions {
    

#ifdef PENTAGON_FUNCTIONS_M0_ENABLED 
template <typename T> T f_1_1_1 (const Kin<T>& k) {
    return rlog(k.v[0]);
}

template <typename T> T f_1_1_2 (const Kin<T>& k) {
    return rlog(-k.v[1]);
}

template <typename T> T f_1_1_3 (const Kin<T>& k) {
    return rlog(k.v[2]);
}

template <typename T> T f_1_1_4 (const Kin<T>& k) {
    return rlog(k.v[3]);
}

template <typename T> T f_1_1_5 (const Kin<T>& k) {
    return rlog(-k.v[4]);
}

template <typename T> T f_1_1_6 (const Kin<T>& k) {
    return rlog(-k.v[3] + k.v[0] + k.v[1]);
}

template <typename T> T f_1_1_7 (const Kin<T>& k) {
    return rlog(-k.v[4] + k.v[1] + k.v[2]);
}

template <typename T> T f_1_1_8 (const Kin<T>& k) {
    return rlog(-k.v[2] + -k.v[3] + k.v[0]);
}

template <typename T> T f_1_1_9 (const Kin<T>& k) {
    return rlog(-k.v[1] + k.v[3] + k.v[4]);
}

template <typename T> T f_1_1_10 (const Kin<T>& k) {
    return rlog(-k.v[2] + k.v[0] + k.v[4]);
}

template <typename T> T f_1_2_1 (const Kin<T>& k) {
    return rlog(k.v[2] + k.v[3]);
}

template <typename T> T f_1_2_2 (const Kin<T>& k) {
    return rlog(k.v[0] + k.v[4]);
}

template <typename T> T f_1_2_3 (const Kin<T>& k) {
    return rlog(k.v[0] + k.v[1]);
}

template <typename T> T f_1_2_4 (const Kin<T>& k) {
    return rlog(-k.v[3] + k.v[0]);
}

template <typename T> T f_1_2_5 (const Kin<T>& k) {
    return rlog(-k.v[2] + k.v[0]);
}

template <typename T> T f_1_2_6 (const Kin<T>& k) {
    return rlog(-k.v[1] + k.v[3]);
}

template <typename T> T f_1_2_7 (const Kin<T>& k) {
    return rlog(-k.v[4] + k.v[2]);
}

template <typename T> T f_1_2_8 (const Kin<T>& k) {
    return rlog(-k.v[3] + -k.v[4] + k.v[0] + k.v[1]);
}

template <typename T> T f_1_2_9 (const Kin<T>& k) {
    return rlog(-k.v[1] + -k.v[2] + k.v[0] + k.v[4]);
}

template <typename T> std::complex<T> f_1_2_10 (const Kin<T>& k) {
    using std::log;
    return log(k.W[30]);
}


// F[1][3][1] -> I/3*Pi - I*Pi*delta[a[26]] + ((-2*I)*Pi + Log[W[25]])*Theta[-a[26]] + Log[W[25]]*Theta[a[26]]
// a[26] -> v[0]*v[1] - v[1]*v[2] + v[2]*v[3] - v[0]*v[4] - v[3]*v[4]
template <typename T> std::complex<T> f_1_3_1(const Kin<T>& k) {
    const auto& v = k.v;

    const auto& W = k.W[25];
    T a = v[0] * v[1] - v[1] * v[2] + v[2] * v[3] - v[0] * v[4] - v[3] * v[4];

    int s = PentagonFunctions::sign(a);
    switch (s) {
        case 0 :
            //return std::complex<T>{0,pi<T>()*T(-2)/T(3)};
            return static_cast<T>(0);
        case 1:
            return std::complex<T>{0,pi<T>()/T(3)} + log(W);
        case -1:
            return std::complex<T>{0,pi<T>()*T(-5)/T(3)} + log(W);
    }
    return {};
}

// F[1][3][2] -> I/3*Pi - I*Pi*delta[a[27]] + ((-2*I)*Pi + Log[W[26]])*Theta[-a[27]] + Log[W[26]]*Theta[a[27]]
// a[27] -> -(v[0]*v[1]) + v[1]*v[2] - v[2]*v[3] - v[0]*v[4] + v[3]*v[4]
template <typename T> std::complex<T> f_1_3_2(const Kin<T>& k) {
    const auto& v = k.v;

    const auto& W = k.W[26];
    T a = -(v[0] * v[1]) + v[1] * v[2] - v[2] * v[3] - v[0] * v[4] + v[3] * v[4];

    int s = PentagonFunctions::sign(a);
    switch (s) {
        case 0 :
            //return std::complex<T>{0,pi<T>()*T(-2)/T(3)};
            return static_cast<T>(0);
        case 1:
            return std::complex<T>{0,pi<T>()/T(3)} + log(W);
        case -1:
            return std::complex<T>{0,pi<T>()*T(-5)/T(3)} + log(W);
    }
    return {};
}

// F[1][3][3] -> I/3*Pi - I*Pi*delta[a[28]] + ((-2*I)*Pi + Log[W[27]])*Theta[-a[28]] + Log[W[27]]*Theta[a[28]]
// a[28] -> -(v[0]*v[1]) - v[1]*v[2] + v[2]*v[3] + v[0]*v[4] - v[3]*v[4]
template <typename T> std::complex<T> f_1_3_3(const Kin<T>& k) {
    const auto& v = k.v;

    const auto& W = k.W[27];
    T a = -(v[0] * v[1]) - v[1] * v[2] + v[2] * v[3] + v[0] * v[4] - v[3] * v[4];

    int s = PentagonFunctions::sign(a);
    switch (s) {
        case 0 :
            //return std::complex<T>{0,pi<T>()*T(-2)/T(3)};
            return static_cast<T>(0);
        case 1:
            return std::complex<T>{0,pi<T>()/T(3)} + log(W);
        case -1:
            return std::complex<T>{0,pi<T>()*T(-5)/T(3)} + log(W);
    }
    return {};
}

// F[1][3][4] -> (-2*I)/3*Pi + I*Pi*delta[a[29]] + Log[W[28]]*Theta[-a[29]] + ((2*I)*Pi + Log[W[28]])*Theta[a[29]]
// a[29] -> v[0]*v[1] - v[1]*v[2] - v[2]*v[3] - v[0]*v[4] + v[3]*v[4]
template <typename T> std::complex<T> f_1_3_4(const Kin<T>& k) {
    const auto& v = k.v;

    const auto& W = k.W[28];
    T a = v[0] * v[1] - v[1] * v[2] - v[2] * v[3] - v[0] * v[4] + v[3] * v[4];

    int s = PentagonFunctions::sign(a);
    switch (s) {
        case 0 :
            //return std::complex<T>{0,pi<T>()/T(3)};
            return static_cast<T>(0);
        case 1:
            return std::complex<T>{0,pi<T>()*T(4)/T(3)} + log(W);
        case -1:
            return std::complex<T>{0,pi<T>()*T(-2)/T(3)} + log(W);
    }
    return {};
}

// F[1][3][5] -> (-2*I)/3*Pi + I*Pi*delta[a[30]] + Log[W[29]]*Theta[-a[30]] + ((2*I)*Pi + Log[W[29]])*Theta[a[30]]
// a[30] -> -(v[0]*v[1]) + v[1]*v[2] - v[2]*v[3] + v[0]*v[4] - v[3]*v[4]
template <typename T> std::complex<T> f_1_3_5(const Kin<T>& k) {
    const auto& v = k.v;

    const auto& W = k.W[29];
    T a = -(v[0] * v[1]) + v[1] * v[2] - v[2] * v[3] + v[0] * v[4] - v[3] * v[4];

    int s = PentagonFunctions::sign(a);
    switch (s) {
        case 0 :
            //return std::complex<T>{0,pi<T>()/T(3)};
            return static_cast<T>(0);
        case 1:
            return std::complex<T>{0,pi<T>()*T(4)/T(3)} + log(W);
        case -1:
            return std::complex<T>{0,pi<T>()*T(-2)/T(3)} + log(W);
    }
    return {};
}

#endif // PENTAGON_FUNCTIONS_M0_ENABLED



#ifdef PENTAGON_FUNCTIONS_M1_ENABLED

namespace m1_set {

template <typename T> T f_1_1(const Kin<T, KinType::m1>& k) { return rlog((k.W[0]).real()); }
template <typename T> T f_1_2(const Kin<T, KinType::m1>& k) { return rlog(-(k.W[8]).real()); }
template <typename T> T f_1_3(const Kin<T, KinType::m1>& k) { return rlog((k.W[1]).real()); }
template <typename T> T f_1_4(const Kin<T, KinType::m1>& k) { return rlog(-(k.W[4]).real()); }
template <typename T> T f_1_5(const Kin<T, KinType::m1>& k) { return rlog((k.W[5]).real()); }
template <typename T> T f_1_6(const Kin<T, KinType::m1>& k) { return rlog((k.W[10]).real()); }
template <typename T> T f_1_7(const Kin<T, KinType::m1>& k) { return rlog(-(k.W[7]).real()); }
template <typename T> T f_1_8(const Kin<T, KinType::m1>& k) { return rlog(-(k.W[6]).real()); }
template <typename T> T f_1_9(const Kin<T, KinType::m1>& k) { return rlog(-(k.W[9]).real()); }
template <typename T> T f_1_10(const Kin<T, KinType::m1>& k) { return rlog((k.W[2]).real()); }
template <typename T> T f_1_11(const Kin<T, KinType::m1>& k) { return rlog(-(k.W[3]).real()); }

} // namespace m1_set

#endif // PENTAGON_FUNCTIONS_M1_ENABLED

} // PentagonFunctions



