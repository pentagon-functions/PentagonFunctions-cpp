#pragma once

#include "PentagonFunctions_config.h"

#include "FunctionID.h"
#include "Kin.h"

#include <array>
#include <complex>
#include <utility>
#include <vector>
#include <iostream>

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
#include <qd/qd_real.h>
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

#ifdef PENTAGON_FUNCTIONS_COLLECT_STATS 
#include "CollectStats.h"
#endif // PENTAGON_FUNCTIONS_COLLECT_STATS


#include "Constants.h"

namespace  PentagonFunctions {


inline double to_double(double x) {return x;}

template <typename T, size_t N> std::array<double,N> to_double(const std::array<T,N>& a) {
    if constexpr (std::is_same_v<T,double>) {
        return a;
    }
    else {
        std::array<double,N> r;
        for (size_t i = 0; i < a.size(); ++i) {
            r[i] = to_double(a[i]);
        }
        return r;
    }
}

template <typename T, size_t N> std::array<T,N> to_HP(const std::array<double,N>& a) {
    if constexpr (std::is_same_v<T,double>) {
        return a;
    }
    else {
        std::array<T,N> r;
        for (size_t i = 0; i < a.size(); ++i) {
            r[i] = a[i];
        }
        return r;
    }
}

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template <typename T> constexpr auto raise_precision(T x) {
    if constexpr (std::is_same_v<T, double>)
        return static_cast<dd_real>(x);
    else if constexpr (std::is_same_v<T, dd_real>)
        return static_cast<qd_real>(x);
    else if constexpr (std::is_same_v<T, qd_real>)
        return x;
    else
        static_assert(!std::is_same_v<T,T>);
}
template <typename T> auto lower_precision(T x) {
    if constexpr (std::is_same_v<T, double>)
        return x;
    else if constexpr (std::is_same_v<T, dd_real>)
        return to_double(x);
    else if constexpr (std::is_same_v<T, qd_real>)
        return to_dd_real(x);
    else
        static_assert(!std::is_same_v<T,T>);
}
#else
template <typename T> constexpr auto raise_precision(T x) { return x; }
template <typename T> constexpr auto lower_precision(T x) { return x; }
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

template <typename T> auto raise_precision(const std::complex<T>& x) {
    return std::complex{raise_precision(x.real()), raise_precision(x.imag())};
}
template <typename T> auto lower_precision(const std::complex<T>& x) {
    return std::complex{lower_precision(x.real()), lower_precision(x.imag())};
}

template <typename T> using raise_precision_t =  decltype(raise_precision(std::declval<T>()));
template <typename T> using lower_precision_t =  decltype(lower_precision(std::declval<T>()));


template <typename T, size_t N> auto raise_precision(const std::array<T, N>& x) {
    using T2 = raise_precision_t<T>;

    std::array<T2, N> result;

    for (size_t i = 0; i < N; ++i) { result[i] = raise_precision(x[i]); }
    return result;
}

template <typename T> auto raise_precision(const std::vector<T>& x) {
    using T2 = raise_precision_t<T>;

    std::vector<T2> result;
    result.reserve(x.size());

    for (auto& xi : x) result.push_back(raise_precision(xi));

    return result;
}


template <typename T, size_t N> auto lower_precision(const std::array<T, N>& x) {
    using T2 = lower_precision_t<T>;

    std::array<T2, N> result;

    for (size_t i = 0; i < N; ++i) { result[i] = lower_precision(x[i]); }
    return result;
}

template <typename T> auto lower_precision(const std::vector<T>& x) {
    using T2 = lower_precision_t<T>;

    std::vector<T2> result;
    result.reserve(x.size());

    for (auto& xi : x) result.push_back(lower_precision(xi));

    return result;
}


template <typename T, KinType KT, typename FF, typename FFH> FunctionObjectType<T, KT> get_HP_improved_evaluator(FF evaluator, FFH evaluator_hp) {

    using TH = raise_precision_t<T>;

    if (!evaluator_hp) {
        return [evaluator](const Kin<T,KT>& k){return evaluator(k).result;};
    }
    else {
        return [evaluator, evaluator_hp](const Kin<T,KT>& k) -> std::complex<T> {
#ifdef PENTAGON_FUNCTIONS_COLLECT_STATS
            static count_n_hp_recompute_calls counter{typeid(T).name()};;
            counter.n_calls++;
#endif
            bool exception = false;
            
            integration_result<T> res;

            try{
                res = integration_result<T>{evaluator(k)};
            }
            catch (std::runtime_error & e) {
                std::cerr << "PentagonFunctions++ caught tanh_sinh integrator exception, retrying higher precision before giving up." << std::endl;
                exception = true;
            }

            if (exception or (res.abs_error > SwitchToHPThreshold<T>)) {
#ifdef PENTAGON_FUNCTIONS_COLLECT_STATS
                counter.n_hp_recompute_calls++;
#endif // PENTAGON_FUNCTIONS_COLLECT_STATS

                Kin<TH, KT> k_hp{raise_precision(k.v)};
                auto [res_hp, error_hp] = evaluator_hp(k_hp);

#ifdef PENTAGON_FUNCTIONS_COLLECT_STATS
                if (error_hp > to_double(IntegrationTolerance<T>)) {
                    counter.n_hp_failed++;
                    std::cerr << "WARNING: after an attempted HP evaluation the estimated error is " << error_hp << std::endl;
                }
#endif // PENTAGON_FUNCTIONS_COLLECT_STATS
                return lower_precision(res_hp);
            }
            else {
                return res.result;
            }
        };
    }
}

template <typename T, typename FF, typename FI>
integration_result<T> integrate_multiple_path_segments(FF&& f_integrand, FI& integrator, const std::vector<T>& path_break_point, const T& tolerance,
                                                       const std::shared_ptr<std::vector<double>>& l1_measures) {
    std::complex<T> value{};
    double error{0};

    bool use_l1 = false;

    if (l1_measures && (l1_measures->size() == (path_break_point.size() - 1))) { use_l1 = true; }

    for (size_t i = 0; i < path_break_point.size() - 1; ++i) {
        T err;
        if (use_l1) { value += integrator.integrate(f_integrand, path_break_point.at(i), path_break_point.at(i + 1), tolerance / l1_measures->at(i), &err); }
        else {
            T set_l1;
            value += integrator.integrate(f_integrand, path_break_point.at(i), path_break_point.at(i + 1), tolerance, &err, &set_l1);
            l1_measures->push_back(to_double(set_l1));
        }
        error += to_double(err * err);
    }

    return {value, error};
}


} // namespace PentagonFunctions
