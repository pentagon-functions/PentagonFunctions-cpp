#include "FunctionID.h"

#include <vector>
#include <iostream>

int main () {

    using namespace PentagonFunctions;

    // in this example we use double precision
    // if you compiled with high-precision support, you can try switching to dd_real or qd_real
    using T = double;

    /**
     * Construction and initialization phase.
     * Can be done only once.
     */

    // Choose kinematics type:
    // KinType::m0 -- massless five-point kinematics
    // KinType::m1 -- one-external-mass kinematics
    constexpr KinType KT = KinType::m0;

    // A vector of FunID, where we put indices of all pentagon functions, which we are interested in.
    // Note that the functions are indexed differently for different kinematics.
    //
    // For instance, for m1 kinematics weight 1 and weight 2 functions are labeled by two indices instead of three.
    // Also the ranges of indices (number of pentagon functions) at each weight are different,
    // so in case you get an error that some functions are not found, check if the indices make sense.
    std::vector<FunID<KT>> needed_functions = {
        {1,1,1}, {1,3,1}, {1,2,10},
        {2,1,3}, {2,2,1},
        {3,3}, {3,17}, {3,111},
        {4,17}, {4,122}, {4,436}, {4,466}, {4,472},
    };

    // a vector which will store evaluator function objects (callable by kinematic points)
    std::vector<FunctionObjectType<T,KT>> function_evaluators;

    // construct evaluator objects

    std::cout << "Construction: " << std::endl;
    for (auto f : needed_functions) {
        function_evaluators.push_back(f.get_evaluator<T>()); 
        std::cout << f << "\n";
    }

    std::cout << std::endl;


    /**
     * Evaluation phase.
     * Evaluation can be repeated any number of times using the evaluator objects function_evaluators.
     */

    // a kinematical point is given by Mandelstam invariants, the number of invariants depends on kinematics type
    constexpr size_t n_vars = Kin<T,KT>::Nvis;

    std::array<T,n_vars> sij = {
            169.00000000000000000000000000000000000000000000000000000000000024,
            -127.03554269209390121978365556633554285695735563298040778575769793,
            33.796369695873051618118516726995190978444086393722898206354653227, 
            34.135578250263955969284362230088631173450982210677708512608176643,
            -112.93867208272743252147419350392861691229747936905042006487121976,
    };

    /*For one-mass kinematics (KinType::m1) try e.g. this point:*/
    /*
     *std::array<T, n_vars> sij = {153.6336165048543689320388349514563106796116504854368932038834951456311,
     *                             369.4884403343410990574426462742308376311577449759914636315134269962653,
     *                             103.2909137620964288772801233193457223601952556307270703091547486511947,
     *                             -70.45251361672822888624553934764915795404745508044825643273023226695048,
     *                             666,
     *                             -45.97237858358423877470873150935986385652572326220709516952480691189946};
     */

    Kin<T,KT> k(sij);

    // evaluation is done by invoking the function objects

    std::cout << "Evaluation: " << std::endl;

    for (size_t i = 0; i < function_evaluators.size(); ++i) {
        auto ri = function_evaluators.at(i)(k);
        std::cout << needed_functions.at(i) << " = " << ri << "\n";
    }

    std::cout << std::endl;
    
    return 0;
}
