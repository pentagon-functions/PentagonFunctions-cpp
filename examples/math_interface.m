Needs["PentagonFunctions`"];

(* This is all of weight 4 functions and a couple of others. *)
listOfFunctions = {
    F[1,1,1],F[2,1,1],F[3,2],
    Table[F[4,i],{i,1,472}]
} // Flatten;


(* 
    First launch an interactive process with a list of functions. 
    In the first argument the set of functions can be specified.
    "m0" -- massless pentagon functions
    "m1" -- pentagon functions with one external mass

    Optionally choose precision (defaults to double) and number of threads (defaults to all available).
*)

evaluator = StartEvaluatorProcess["m0",listOfFunctions, "Precision" -> "double", "NThreads" -> Automatic];

(* Now evaluate any number of times *) 

Print["Evaluating first time..."];

{timing, values} = EvaluateFunctions[evaluator, {1.1, -0.07, 0.09, 0.48, -0.03}] // AbsoluteTiming;

Print["took ", timing,"s"];
Print["\n..., ",values[[10;;20]],", ...\n"];

Print["Evaluating second time..."];

{timing, values} = EvaluateFunctions[evaluator, {2.3, -0.17, 0.29, 0.35, -0.09}] // AbsoluteTiming;

Print["took ", timing,"s"];
Print["\n..., ",values[[10;;20]],", ...\n"];
