#pragma once
#include <chrono>
#include <iostream>
#include <string>

#ifdef TIMING_ON

//! start (create if necessary) counter
#define START_TIMER(NAME)                                                                                                                                      \
    static timing::counter __timing_counter_##NAME(#NAME);                                                                                                     \
    __timing_counter_##NAME.start();

#define STOP_TIMER(NAME) __timing_counter_##NAME.stop();

#define TIMING(what, expr)                                                                                                                                     \
    {                                                                                                                                                          \
        std::cout << what << "..." << std::endl;                                                                                                               \
        timing::counter __tmp_counter(#what);                                                                                                                  \
        __tmp_counter.start();                                                                                                                                 \
        expr \
        __tmp_counter.stop();                                                                                                                             \
    }

#else

#define START_TIMER(NAME)
#define STOP_TIMER(NAME)
#define TIMING(what, expr) { expr }

#endif

namespace timing {

class counter {
  private:
    using clock = std::chrono::high_resolution_clock;
    using duration_type = std::chrono::duration<double>;

    std::string name;
    std::string type{};

    duration_type total{0};
    decltype(clock::now()) interval_begin;

  public:
    void start();
    void stop();
    void report();
    duration_type get_duration() const;

    counter() = delete;
    counter(std::string&& n) : name(n) {}
    counter(std::string&& n, std::string&& t) : name(n), type(t) {}
    ~counter();
};

inline void counter::start() { interval_begin = clock::now(); }
inline void counter::stop() {
    auto end = clock::now();
    total += std::chrono::duration_cast<duration_type>(end - interval_begin);
}

inline void counter::report() {
    using namespace std::chrono;
    auto ns = total;
    auto h = duration_cast<hours>(ns);
    ns -= h;
    auto m = duration_cast<minutes>(ns);
    ns -= m;
    auto s = duration_cast<seconds>(ns);
    ns -= s;
    auto ms = duration_cast<milliseconds>(ns);

    std::cout << name;
    if (type != "") { std::cout << "<" << type << ">"; }
    std::cout << " took\n\t";
    if (h.count() > 0) { std::cout << h.count() << "h "; }
    if (h.count() > 0 or m.count() > 0) { std::cout << m.count() << "m "; }
    if (h.count() > 0 or m.count() > 0 or s.count() > 0) { std::cout << s.count() << "s "; }
    std::cout << ms.count() << "ms" << std::endl;
}

inline counter::duration_type counter::get_duration() const { return total; }

inline counter::~counter() { report(); }

} // namespace timing
