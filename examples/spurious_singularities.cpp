#include "FunctionID.h"
#include "Kin.h"
#include "Constants.h"

#include "../tests/test_utilities.h"

#include <iomanip>

/* 
 * Here  we take a look at how our functions behave while approaching spurious singularities.
 * We use higher precision evaluations to measure the accuracy.
 * We demonstrate that our implementation is numerically robust.
 */


template <typename T, size_t D> std::array<double, D> to_double(const std::array<T, D>& a) {
    std::array<double, D> a_double;
    for (size_t i = 0; i < D; ++i) { a_double[i] = to_double(a[i]); }
    return a_double;
}

/*
 * We get exponentially close to the singularity, and hit it because of rounding in double precision
 */
template <typename T> T path(T t) {
    const T sp = 17 * log(T(10));
    if (t <= 0)  return -exp(-sp * (t+1)); 
    if (t > 0) return -path(T(0))*exp(sp*t);
    throw;
}

int main() {

    using namespace PentagonFunctions;

    using T = double;
    using std::cout;
    using std::endl;
    using std::setprecision;

    using TH = dd_real;

    // We choose a function that contains a vanishing letter in its integrand,
    // W[10] in this example

    // This function stays finite and precise
    auto fID = FunctionID{4,466};

    // This function goes to zero with the vanishing letter.
    // Hence, very close to zero relative error diverges, but absolute error stays low.
    //auto fID = FunctionID{4,328};

    FunctionObjectType<T> f =  fID.get_evaluator<T>();
    FunctionObjectType<TH> f_hp =  fID.get_evaluator<TH>();

    // this point has singular letter W[10]
    const std::array<TH,5> singular_point({stof<TH>("151.0000000000000000000000000000000000000000000000000000000000000000000"),
            stof<TH>("-7.894736842105263157894736842105263157894736842105263157894736842105263"),
            stof<TH>("7.894736842105263157894736842105263157894736842105263157894736842105263"),
            stof<TH>("4.545454545454545454545454545454545454545454545454545454545454545454545"),
            stof<TH>("-10.27777777777777777777777777777777777777777777777777777777777777777778")});

    {
        cout << "Here are our letters:\n";
        size_t i = 1;
        Kin<TH> k(singular_point);
        for(auto& wi : k.W){
            cout << "W["<<i<<"] = "<<wi <<"\n";
            ++i;
        }
    }

    // we will evaluate the function over a trajectory which crosses the spurious singularity

    cout << "Looking at function " << fID << endl;
    
    cout << endl << std::scientific << setprecision(14);

    cout << "We go over the singular surface starting very far from it:" << endl;

    // we set the integration tolerance for higher precision lower,
    // because we only really need 16 digits correct to compare with doulbe-precision evaluations
    IntegrationTolerance<TH> = 1e-14;

    detail::VanishingDLogThreshold<double> = 1e-14;

    //TH worst_accuracy = 100;

    int Npoints = 20;

    for (TH t = TH(-1); t <= TH(1); t += TH(2)/(Npoints-1) ) {
        TH x = TH(0.3)*path(t);
        auto point = singular_point;
        point[2] += x;

        Kin<TH> kin_hp(point);
        Kin<T> kin(to_double(point));

        cout << "W["<<10<<"] = "<< kin_hp.W[9].real();

        auto value_hp = f_hp(kin_hp);
        auto value = f(kin);
        std::complex<T> lifted{to_double(value_hp.real()),to_double(value_hp.imag())};

        auto compute_absolute_error = [](auto a, auto b) {
            using std::abs;
            using std::log10;
            return -log10(abs(a-b));
        };

        auto accuracy = compute_accuracy(value, lifted);

        //if (accuracy < worst_accuracy) worst_accuracy = accuracy;

        cout << ";\tf = " << value  << ";\tf_hp = " << value_hp << 
            ";\trel digits: " << setprecision(2) << std::fixed << accuracy <<
                ";\tabs digits: " << setprecision(2) << compute_absolute_error(value,lifted) << 
                std::scientific << setprecision(14) << "\n";
    }

    //cout << "worst accuracy: " << worst_accuracy;

    cout << endl;
}

