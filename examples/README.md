All executables for the examples of the C++ programs can
be found in the build directory, e.g. in **build/examples/**.


#### example_simple.cpp

This program demonstrates the most basic usage of the library.

Run the executable **build/examples/example_simple**.


#### spurious_singularities.cpp

This example requires the qd library to be configured.

In this example we use evaluations in quadruple precision to gauge the behavior of
double-precision evaluations of the pentagon functions as they approach a spurious singularity.

We choose a path in the physical phase space which approaches exponentially and crosses the 
spurious singularity. We choose a weight-4 pentagon function whose integrand contains the letter vanishing on the chosen spurious singularity.
We then compute relative and absolute errors of the function on a number of points along the path.

Run the executable **build/examples/spurious_singularities**.


#### math_interface.m

This is a *Mathematica* program which explains the usage of the *Mathematica* interface.
Run the example with

```
math -script examples/math_interface.m
```

This obviously requires *Mathematica* to be available with the command `math`.
Alternatively, open the file with the *Mathematica* frontend. 

