#include "catch.hpp"

#include "Kin.h"
#include "Constants.h"
#include "FunctionID.h"
#include "test_utilities.h"

#include <unordered_set>

namespace PentagonFunctions {

#include "test_point1.hpp"

template <typename T>
const std::vector<std::pair<FunctionID, std::complex<T>>> targets =
#include "targets_w3.hpp"
;

} // PentagonFunctions


TEST_CASE("Weight 3 functions oct precision", "[W3:oct]"){
    using namespace PentagonFunctions;
    using T = std::complex<qd_real>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    // It takes too long to always do this, so we check just a couple
    const std::unordered_set<FunctionID> to_test = {
        FunctionID{3,1},
        FunctionID{3,17},
        FunctionID{3,66},
        FunctionID{3,75},
        FunctionID{3,105},
        FunctionID{3,109},
        FunctionID{3,111},
    };

    for(auto& fi_data : targets<TR>){
        FunctionID fID = fi_data.first;

        if(to_test.count(fID) == 0) continue;

        T target = fi_data.second;

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point<TR>);

        CHECK(check(value, target, 59));

        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}


