#include "catch.hpp"

#include "Kin.h"
#include "Constants.h"
#include "FunctionID.h"
#include "test_utilities.h"

namespace PentagonFunctions {
    
template <typename T>
const Kin<T,KinType::m1> test_point {stof<T>("3.631592101974506373406648337915521119720069982504373906523369157710572"),stof<T>("4.155934426229508196721311475409836065573770491803278688524590163934426"),stof<T>("0.06354247353472760134262845339530080041311644719855409243480506067647818"),stof<T>("-0.5503326894183301137583172354582528439579308864563210989482721614080275"),stof<T>("5"),stof<T>("-1.019758232596915381408920383493122134222592746977907461442267611504794")};

template <typename T>
const std::vector<std::pair<FunID<KinType::m1>, std::complex<T>>> targets =
#include "targets_m1_w34_inv.hpp"
;

} // PentagonFunctions



TEST_CASE("Weight 4 functions, double precision, invisible point", "[M1:W4:double:inv]"){
    using namespace PentagonFunctions;

    using T = std::complex<double>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    for(auto& fi_data : targets<TR>){
        FunID<KinType::m1> fID = fi_data.first;
        T target = fi_data.second;

        std::cout << fID << "\n";

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point<TR>);

        CHECK(check(value, target, 10.0));

        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}


