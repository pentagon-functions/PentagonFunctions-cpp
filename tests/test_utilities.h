#pragma once

#include <complex>
#include <iomanip>
#include <iostream>
#include <limits>
#include <type_traits>

#include "PolyLog.h"

namespace PentagonFunctions {

/**
 * Make non-complex type if it was complex
 */
template <typename T, typename = void> struct remove_complex;
template <typename T> struct remove_complex<T, typename std::enable_if<!is_complex<T>::value>::type> { typedef T type; };
template <typename T> struct remove_complex<T, typename std::enable_if<is_complex<T>::value>::type> { typedef typename T::value_type type; };
template <typename T> using remove_complex_t = typename remove_complex<T>::type;

template <typename T, typename TR = remove_complex_t<T>> TR compute_accuracy(const T& x, const T& target) {
    using std::abs;
    using std::log10;
    if (abs(x - target) == TR(0)) return std::numeric_limits<TR>::digits10;

    if (abs(target) > TR(10) * std::numeric_limits<TR>::epsilon()) { return -log10(abs(x - target) / abs(target)); }
    else
        return -log10(abs(x - target));
}

template <typename T, typename TR = remove_complex_t<T>, typename... Ts> bool check(const T& x, const T& target, double tol = 12.5) {
    auto acc = compute_accuracy(x, target);
    if (acc >= TR(tol)) return true;

    std::cerr << std::setprecision(std::numeric_limits<TR>::digits10);

    std::cerr << x << "  !=  " << target << "\t(target)\n";
    std::cerr << "\t accuracy : " << acc << "\n";
    std::cerr << "\t tolerance : " << tol << std::endl;

    return false;
}

} // namespace PentagonFunctions
