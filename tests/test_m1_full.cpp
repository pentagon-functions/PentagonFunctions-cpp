#include "catch.hpp"

#include "Kin.h"
#include "Constants.h"
#include "FunctionID.h"
#include "test_utilities.h"
#include <set>

namespace PentagonFunctions {
    
template <typename T>
const Kin<T,KinType::m1> test_point_pt1 {stof<T>("0.02308027289882421251270140804180577732617215851357236173610103062853825"),stof<T>("0.1532741664667786039817702086831374430319021348045094746941712640921084"),stof<T>("0.1761481761481761481761481761481761481761481761481761481761481761481761"),stof<T>("-0.6509904733101466807802812641766218055345531528806895508846212006653561"),stof<T>("1"),stof<T>("-0.6747927497541098777574820851482366165519179429534916397358437543908950")};

template <typename T>
const std::vector<std::pair<FunID<KinType::m1>, std::complex<T>>> targets_pt1 =
#include "targets_m1_full_pt1.hpp"
;

template <typename T>
const Kin<T,KinType::m1> test_point_s1 {stof<T>("1.000001000000000000000000000000000000000000000000000000000000000000000"),stof<T>("1.999997000000000000000000000000000000000000000000000000000000000000000"),stof<T>("1.000002000000000000000000000000000000000000000000000000000000000000000"),stof<T>("-1.428567428571428571428571428571428571428571428571428571428571428571429"),stof<T>("6.999996000000000000000000000000000000000000000000000000000000000000000"),stof<T>("-0.9999920000000000000000000000000000000000000000000000000000000000000000")};

template <typename T>
const std::vector<std::pair<FunID<KinType::m1>, std::complex<T>>> targets_s1 =
#include "targets_m1_full_s1.hpp"
;

template <typename T>
const Kin<T,KinType::m1> test_point_s2  {stof<T>("0.9999994039535522460937500000000000000000000000000000000000000000000000"),stof<T>("1.999999105930328369140625000000000000000000000000000000000000000000000"),stof<T>("0.9999991655349731445312500000000000000000000000000000000000000000000000"),stof<T>("-1.428572143827165876116071428571428571428571428571428571428571428571429"),stof<T>("7.000000119209289550781250000000000000000000000000000000000000000000000"),stof<T>("-1.000000655651092529296875000000000000000000000000000000000000000000000")};

template <typename T>
const std::vector<std::pair<FunID<KinType::m1>, std::complex<T>>> targets_s2 =
#include "targets_m1_full_s2.hpp"
;

} // PentagonFunctions


TEST_CASE("Weight 4 functions, double precision, pt1", "[m1:full:double]"){
    using namespace PentagonFunctions;

    using T = std::complex<double>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    for(auto& fi_data : targets_pt1<TR>){
        FunID<KinType::m1> fID = fi_data.first;
        T target = fi_data.second;

        std::cout << fID << "\n";

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point_pt1<TR>);

        CHECK(check(value, target, 12));

        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}


TEST_CASE("Weight 4 functions, double precision, s1", "[m1:full:double]"){
    using namespace PentagonFunctions;

    using T = std::complex<double>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    for(auto& fi_data : targets_s1<TR>){
        FunID<KinType::m1> fID = fi_data.first;
        T target = fi_data.second;

        std::cout << fID << "\n";

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point_s1<TR>);

        CHECK(check(value, target, 10));

        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}


TEST_CASE("Weight 4 functions, double precision, s2", "[m1:full:double]"){
    using namespace PentagonFunctions;

    using T = std::complex<double>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    // these functions involve DLog[sqrtSigma5[3]], so have worse errors
    std::set<FunID<KinType::m1>>  sigma5_3_letters = {{3,213},{4,946},{4,947},{4,948},{4,949},{4,950},{4,980}};

    for(auto& fi_data : targets_s2<TR>){
        FunID<KinType::m1> fID = fi_data.first;
        T target = fi_data.second;

        std::cout << fID << "\n";

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point_s2<TR>);

        if (sigma5_3_letters.count(fID) > 0) {
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
            CHECK(check(value, target, 12));
#else
            CHECK(check(value, target, 6.5));
#endif // PENTAGON_FUNCTIONS_M1_ENABLED
        }
        else {
            CHECK(check(value, target, 12));
        }


        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}


