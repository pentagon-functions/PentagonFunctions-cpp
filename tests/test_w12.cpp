#include "catch.hpp"

#include "Kin.h"
#include "Constants.h"
#include "FunctionID.h"
#include "test_utilities.h"

namespace PentagonFunctions {

#include "test_point1.hpp"

template <typename T>
const std::vector<std::pair<FunctionID, std::complex<T>>> targets =
#include "targets_w12.hpp"
;

} // PentagonFunctions



TEST_CASE("Weight 1 and 2 functions, double precision", "[W12:double]"){
    using namespace PentagonFunctions;
    using T = std::complex<double>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    for(auto& fi_data : targets<TR>){
        FunctionID fID = fi_data.first;
        T target = fi_data.second;

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point<TR>);

        CHECK(check(value, target, 14.5));

        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
TEST_CASE("Weight 1 and 2 functions, quadruple precision", "[W12:quad]"){
    using namespace PentagonFunctions;
    using T = std::complex<dd_real>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    for(auto& fi_data : targets<TR>){
        FunctionID fID = fi_data.first;
        T target = fi_data.second;

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point<TR>);

        CHECK(check(value, target, 30));

        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}

TEST_CASE("Weight 1 and 2 functions, octuple precision", "[W12:oct]"){
    using namespace PentagonFunctions;
    using T = std::complex<qd_real>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    for(auto& fi_data : targets<TR>){
        FunctionID fID = fi_data.first;
        T target = fi_data.second;

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point<TR>);

        CHECK(check(value, target, 60));

        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED


