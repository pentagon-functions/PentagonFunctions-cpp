(* ::Package:: *)
BeginPackage["PentagonFunctions`"];


$EvaluatorEXE::usage = "The location of the Mathematica interface executable.
It should have been already configured correctly during installation, but can be adjusted manually."

StartEvaluatorProcess::usage = "StartEvaluatorProcess[funcList_]
launches an evaluator program with a list of functions `funcList`.
Waits until the construction process for all functions is finished.
Returns a list of `ProcessObject` and `funcList.


StartEvaluatorProcess[setName_, funcList_]
Same as StartEvaluatorProcess[funcList], but explicitly choose a set of functions by it's label setName (see README.md for details),
e.g. m0, m1
";


EvaluateFunctions::usage = "EvaluateFunctions[process, point]
evaluates the functions constructed in `process` on a `point`.

The point is given by Mandelstam invariants:
{s12, s23, s34, s45, s15} for m0
{p1s, s12, s23, s34, s45, s15} for m1

Return a list of values for constructed functions.


EvaluateFunctions[{process, funcList}, point]
Same as EvaluateFunctions[process,point], but returns a list of replacement rules for `funcList`.


EvaluateFunctions[setName, funcList, point]
evaluates the functions in funcList from set setName.
Compbines together StartEvaluatorProcess and EvaluateFunctions.
Options for StartEvaluatorProcess will be forwarded.
";



F::usage = "A abstract head representing a pentagon function.";

$QDenabled::usage = "A boolean variable which is True if PentagonFunctions++ was configured with quadruple and octuple precision support";

Begin["`Private`"];

Unprotect["PentagonFunctions`StartEvaluatorProcess"];
Unprotect["PentagonFunctions`EvaluateFunctions"];

SetAttributes[F, NHoldAll];

$EvaluatorEXE = "@EvaluatorEXE@";
SetEnvironment["@LD_LIBRARY_PATH_name@" -> "@LD_LIBRARY_PATH@"];
$QDenabled = ToExpression["@QD@"];

getInputTypes[x_Symbol]:=(First/@DownValues[x])/.HoldPattern->HoldForm;
addTerminationPattern[head_] := head[args___] := (
    Print["ERROR: invocation of \n",HoldForm@head[args]];
    Print["didn't match any of definitions"];
    Print[Most[getInputTypes[head]]];
    Print["Aborting!"];
    Print["Stack: ", Stack[]];
    $DEBUG["TYPEFAILURE"]=Hold[head[args];$DEBUG["TYPEFAILURESTACK"]=Stack[];];
    Abort[]
);

StartEvaluatorProcess::flnch = "Failed to start the evaluator process!";
StartEvaluatorProcess::noqd = "Requested precision `1`, but PentagonFunctions++ was not configured to support it.";

Options[StartEvaluatorProcess] = {"Precision" -> "double", "NThreads" -> Automatic};

precisionMap["double"] := "d";
If[$QDenabled,
    precisionMap["quadruple"] := "q";
    precisionMap["octuple"] := "o";
    ,
    precisionMap["quadruple"] := (Message[StartEvaluatorProcess::noqd,"quadruple"]; Abort[]);
    precisionMap["octuple"] := (Message[StartEvaluatorProcess::noqd,"octuple"]; Abort[]);
];
addTerminationPattern[precisionMap];

StartEvaluatorProcess[funcList:{F[_Integer?Positive..]..}, opt:OptionsPattern[]]:= StartEvaluatorProcess["m0", funcList, opt];

StartEvaluatorProcess[setName_String,funcList:{F[_Integer?Positive..]..}, opt:OptionsPattern[]]:= With[
        {intList = Replace[F[is__]:>{is}] /@ funcList},
        {string = StringRiffle[StringRiffle /@ intList,{"",";",";"}]<>"E"},
        {nthreads = OptionValue["NThreads"]},
        {pro = StartProcess[{$EvaluatorEXE, setName, precisionMap[OptionValue["Precision"]], If[nthreads===Automatic, 0, nthreads]}]},

        WriteString[pro, string];
        (* wait until constructed *)
        Module[{stderr=""},
            TimeConstrained[
                While[!StringMatchQ[stderr,"Constructed"~~__],
                        stderr = stderr <>ReadString[ProcessConnection[pro,"StandardError"],EndOfBuffer];
                        Pause[0.1];
                ];
            ,20];
            Print[StringTrim[stderr]];
        ];

        If[!TrueQ[ProcessStatus[pro,"Running"]],
            Message[StartEvaluatorProcess::flnch]; Abort[],
            {pro,funcList}
        ]
];

addTerminationPattern[StartEvaluatorProcess];

Clear[EvaluateFunctions];

EvaluateFunctions::exactpoint = "Warning: evaluation point `1` is exact!
EvaluateFunctions doesn't know how many digits to use and will proceed with double precision.
Provide evaluation point as floating point numbers with appropriate precision to suppress the warning."

EvaluateFunctions[pro_?(ProcessStatus[#,"Running"]&),pointin:{_?NumericQ..}]:= Block[{result, point = pointin, stderr = ""},
    Unprotect[EvaluateFunctions]; 

    If[!FreeQ[point,_Integer|_Rational], 
        Message[EvaluateFunctions::exactpoint,point];
        point = N[point, 17];
    ];

    result = (EvaluateFunctions[pro,point] = With[
        {in = StringRiffle[ ToString@*CForm /@ point]},

        WriteLine[pro,in];
        Module[{result=""},
            While[!StringEndsQ[result,"\n\n"],
                    result = result <>ReadString[ProcessConnection[pro,"StandardOutput"],EndOfBuffer];
                    stderr = stderr <>ReadString[ProcessConnection[pro,"StandardError"],EndOfBuffer];
                    Pause[0.1];
            ];
            If[stderr =!= "",
                EchoEvaluation[stderr];
            ];
            result
        ]//StringTrim//ToExpression
    ]);

    Protect[EvaluateFunctions];

    result
];

EvaluateFunctions[{pro_?(ProcessStatus[#,"Running"]&),funcList:{_F..}}, point:{_?NumericQ..}] := Thread[funcList->EvaluateFunctions[pro,point]] ;

EvaluateFunctions[setName_String, funcList:{_F..}, point:{_?NumericQ..}, opt:OptionsPattern[]] := Block[{process},
    (* WithCleanup was introuduced in 12.2, so we switch to an unsafe trivial 'implementation' of it.
     * We rely on the order of evaluation of arguments to actually kill the process (if no errors happen).
     *)
    If[!ValueQ[WithCleanup[_,_]],
        WithCleanup[expr_, _] := expr;
    ];

    WithCleanup[
        process = StartEvaluatorProcess[setName, funcList, opt]//First;
        EvaluateFunctions[{process, funcList}, point]
        ,
        KillProcess[process];
    ]
];

addTerminationPattern[EvaluateFunctions];

End[];

Protect["PentagonFunctions`StartEvaluatorProcess"];
Protect["PentagonFunctions`EvaluateFunctions"];

EndPackage[];
