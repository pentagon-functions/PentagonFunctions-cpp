#include "FunctionID.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

#include "ThreadPool/ThreadPool.h"

std::vector<std::string> split(const std::string& s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter)) { tokens.push_back(token); }
    return tokens;
}

std::string math_string(const double& x){
  std::ostringstream ss;
  ss << std::setprecision(16) << x;
  auto s = ss.str();
  auto pos = s.find("e");
  if (pos != std::string::npos){
    s.replace(s.find("e"), 1, "`16*^");
  }
  else{
    s+= "`16";
  }
  return s;
}


#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
std::string math_string(const qd_real& x){
  std::ostringstream ss;
  ss << std::setprecision(64) << x;
  auto s = ss.str();
  auto pos = s.find("e");
  if (pos != std::string::npos){
    s.replace(s.find("e"), 1, "`64*^");
  }
  else{
    s+= "`64";
  }
  return s;
}

std::string math_string(const dd_real& x){
  std::ostringstream ss;
  ss << std::setprecision(32) << x;
  auto s = ss.str();
  auto pos = s.find("e");
  if (pos != std::string::npos){
    s.replace(s.find("e"), 1, "`32*^");
  }
  else{
    s+= "`32";
  }
  return s;
}

#endif

template <typename T> std::string math_string (const std::complex<T>& z) {
    T re = z.real();
    T im = z.imag();

    std::string out;
    
    if (re != 0 and im !=0) {
        out = "(" + math_string(re) + " + I*" + math_string(im) + ")";
    }
    else if (re == 0 and im != 0) {
        out = "(I*"+math_string(im)+")";
    }
    else {
        out = math_string(re);
    }
    return out;
}

namespace PentagonFunctions {

template <typename T, KinType KT> std::vector<FunctionObjectType<T,KT>> construct(const std::vector<FunID<KT>>& functions_list) {

    size_t N = functions_list.size();

    std::vector<FunctionObjectType<T,KT>> all_functions;
    all_functions.reserve(N);
    all_functions.shrink_to_fit();

    for(const auto& it : functions_list ) {
        all_functions.push_back(it.template get_evaluator<T>());
    }

    std::cerr << "Constructed " << N << " functions." << std::endl;

    return all_functions;
}

template <typename T, KinType KT> void cycle_points(const std::vector<FunctionObjectType<T,KT>>& all_functions, const int nthreads) {

    const size_t N = all_functions.size();
    using C = std::complex<T>;

    while (true) {

        constexpr size_t number_of_variables = Kin<T,KT>::Nvis;

        std::array<T, number_of_variables> point;
        for (size_t i = 0; i < point.size(); ++i) { std::cin >> point[i]; }

        std::vector<C> results;
        results.reserve(N);

        try{
            ThreadPool pool(nthreads);

            std::vector<std::future<C>> futures;
            futures.reserve(N);

            auto k = Kin<T,KT>(std::move(point));
            for (auto& f : all_functions) { futures.push_back(pool.enqueue(f,k)); }

            for(auto&& r : futures){
                results.push_back(r.get());
            }
        }
        catch (const std::exception& e) {
            std::cerr << e.what() << std::endl;
            std::cout << "Throw[\"" << e.what() << "\"]\n\n" << std::endl;
            continue;
        }

        std::stringstream ss;
        auto& output = ss;

        output << "{\n";

        for (size_t i = 0; i < N; ++i) {
            if (i != 0) output << ",\n";
            output << /*functions_list.at(i) << " -> " <<*/ math_string(results.at(i));
        }

        output << "\n}";

        std::cout << ss.str() << "\n\n" << std::endl;
    }

}

template <KinType KT> void run_main(const char* prec_char, int nthreads) {
    using std::vector;
    using std::string;

    auto read_functionIDs = [&](auto& functions_list) {
        std::string in_str;
        // we read the input until the character 'E' is encountered
        std::getline(std::cin, in_str, 'E');

        // individual functions are split by ';'
        vector<string> all_funcs = split(in_str, ';');
        for (auto&& fstr : all_funcs) {
            // incides are split by spaces
            vector<string> inds_str = split(fstr, ' ');
            switch (inds_str.size()) {
                case 2: functions_list.emplace_back(std::stoi(inds_str[0]), std::stoi(inds_str[1])); break;
                case 3: functions_list.emplace_back(std::stoi(inds_str[0]), std::stoi(inds_str[1]), std::stoi(inds_str[2])); break;
                default: std::cerr << "ERROR: in parsing " << fstr << std::endl; std::exit(1);
            }
        }

        if (functions_list.size() == 0) {
            std::cerr << "ERROR: the function list is empty!" << std::endl;
            std::exit(1);
        }
    };
    

    vector<FunID<KT>> functions_list;

    read_functionIDs(functions_list);

    switch (*prec_char) {
        case 'd': cycle_points(construct<double>(functions_list), nthreads); break;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
        case 'q': cycle_points(construct<dd_real>(functions_list), nthreads); break;
        case 'o': cycle_points(construct<qd_real>(functions_list), nthreads); break;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
        default: std::cerr << "Precision " << *prec_char << " is not implemented!" << std::endl; std::exit(1);
    };

}

} // PentagonFunctions


/*
 * Expected 3 arguments:
 * [set name] [precision] [# threads (0 for using all available)]
 */
int main(int argc, char* argv[]) {
    using namespace PentagonFunctions;

    std::string set_string(argv[1]);

    if (argc != 4 ) {
        std::cerr << "Expected 3 arguments, got " << argc << std::endl;
        std::exit(1);
    }

    int nthreads;

    try {
        nthreads = std::stoi(std::string(argv[3]));
    }
    catch(...) {
        std::cerr << "Invalid number of threads: " << std::string(argv[3]) << std::endl;
        std::exit(1);
    }
    if (nthreads == 0) {
        nthreads = std::thread::hardware_concurrency();
    }


    if(false){ }
#ifdef PENTAGON_FUNCTIONS_M0_ENABLED 
    else if(set_string == "m0") {
        run_main<KinType::m0>(argv[2], nthreads);
    }
#endif // PENTAGON_FUNCTIONS_M0_ENABLED
#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
    else if (set_string == "m1") {
        run_main<KinType::m1>(argv[2], nthreads);
    }
#endif // PENTAGON_FUNCTIONS_M1_ENABLED

    return 0;
}

